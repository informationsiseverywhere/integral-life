package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Coprpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int plnsfx;
	private String validflag;
	private int tranno;
	private BigDecimal sumins;
	private BigDecimal singp;
	private BigDecimal zstpduty01;
	private BigDecimal riskprem;
	private BigDecimal commprem;
	private BigDecimal instprem;
	private BigDecimal zbinstprem;
	private BigDecimal zlinstprem;
	private int instfrom;
	private int instto;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private Timestamp createdAt;
	
	public Coprpf() {
		super();
	}
	
	public Coprpf(Coprpf coprpf) {
		this.uniqueNumber=coprpf.uniqueNumber;
		this.chdrcoy=coprpf.chdrcoy;
		this.chdrnum=coprpf.chdrnum;
		this.life=coprpf.life;
		this.jlife=coprpf.jlife;
		this.coverage=coprpf.coverage;
		this.rider=coprpf.rider;
		this.plnsfx=coprpf.plnsfx;
		this.validflag=coprpf.validflag;
		this.tranno=coprpf.tranno;
		this.sumins=coprpf.sumins;
		this.singp=coprpf.singp;
		this.zstpduty01=coprpf.zstpduty01;
		this.riskprem=coprpf.riskprem;
		this.commprem=coprpf.commprem;
		this.instprem=coprpf.instprem;
		this.zbinstprem=coprpf.zbinstprem;
		this.zlinstprem=coprpf.zlinstprem;
		this.instfrom=coprpf.instfrom;
		this.instto=coprpf.instto;
		this.usrprf=coprpf.usrprf;
		this.jobnm=coprpf.jobnm;
		this.datime=coprpf.datime;
		this.createdAt=coprpf.createdAt;
	}
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getSumins() {
		return sumins;
	}
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}
	public BigDecimal getSingp() {
		return singp;
	}
	public void setSingp(BigDecimal singp) {
		this.singp = singp;
	}
	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}
	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}
	public BigDecimal getRiskprem() {
		return riskprem;
	}
	public void setRiskprem(BigDecimal riskprem) {
		this.riskprem = riskprem;
	}
	public BigDecimal getCommprem() {
		return commprem;
	}
	public void setCommprem(BigDecimal commprem) {
		this.commprem = commprem;
	}
	public BigDecimal getInstprem() {
		return instprem;
	}
	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}
	public BigDecimal getZbinstprem() {
		return zbinstprem;
	}
	public void setZbinstprem(BigDecimal zbinstprem) {
		this.zbinstprem = zbinstprem;
	}
	public BigDecimal getZlinstprem() {
		return zlinstprem;
	}
	public void setZlinstprem(BigDecimal zlinstprem) {
		this.zlinstprem = zlinstprem;
	}
	public int getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(int instfrom) {
		this.instfrom = instfrom;
	}
	public int getInstto() {
		return instto;
	}
	public void setInstto(int instto) {
		this.instto = instto;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	
	
}
