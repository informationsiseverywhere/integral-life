/*
 * File: Unlspcad.java
 * Date: 30 August 2009 2:52:06
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLSPCAD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5535rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      UNIT LINKED GENERIC ISSUE SUBROUTINE FOR SINGLE PREMIUM
*                      COMPONENT ADDITION.
*
* OVERVIEW.
* ---------
*
* This program is a' PART' clone of the subroutine UNLISS. The
* sections which were cloned from UNLISS deal specifially with
* Single Premium components.
*
* UNLSPCAD is the generic  subroutine which will be called to
* perform the processing necessary forthe add of a SP Unit linked
* generic component. It will be called (via T5671, Coverage/Rider
* Switching) from within the AT module P5132AT for each Unit
* linked single premuium component been created on a contract.
*
* UNLSPCAD will handle SP Coverages created by Component Add.    e
*
* It will perform the following functions:
*
* 1. Write unit Fund transaction records:
*      UTRN record for the uninvested portion of the premium,
*      UTRN record for the Initial Units,
*      UTRN record for the Accumulation Units,
*
* 2. Update the Coverage/Rider record via COVRMJA.
*
*  Create An ACMV record for the PREMIUM amount invested.
*
*  Note: the subroutine LIFACMV will be called to add this
*  record, call with a function of PSTW so that ACBL records are
*  created in the subroutine.
*
*  The key will be the batch transaction key passed in the
*  linkage section. The remaining fields will be set as
*  follows:
*
*  . ACMV-RLDGCOY           -  Contract Company
*  . ACMV-SACSCODE          -  from T5645
*  . ACMV-RLDGACCT          -  Agent number
*  . ACMV-ORIGCURR          -  Contract Currency
*  . ACMV-SACSTYP           -  from T5645
*  . ACMV-RDOCNUM           -  Contract Number
*  . ACMV-TRANNO            -  The new TRANNO
*  . ACMV-JRNSEQ            -  blank
*  . ACMV-ORIGAMT           -  amount of clawback in Contract
*                              Currency - negative for the 1st.
*                              ACMV and positive for the second
*                              (Earned minus Paid).
*  . ACMV-TRANREF           -
*  . ACMV-CRATE             -  Currency Conversion Rate
*  . ACMV-ACCTAMT           -  ORIGAMT * CRATE
*  . ACMV-GENLCOY           -  FSU Company
*  . ACMV-GENLCUR           -
*  . ACMV-GLCODE            -  from T5645
*  . ACMV-GLSIGN            -  from T5645
*  . ACMV-POSTYEAR          -  from Batch Transaction details
*  . ACMV-POSTMONTH         -    "    "        "         "
*  . ACMV-EFFDATE           -  Effective Date of change.
*  . ACMV-RCAMT             -
*  . ACMV-FRCDATE           -  99999999
*  . ACMV-TRANSACTION-DATE  -  Current System Date
*  . ACMV-TRANSACTION-TIME  -  Current System Time
*  . ACMV-USER              -  User Id. passed in linkage
*  . ACMV-TERMID            -  Term Id. passed in linkage
*
* PROCESSING.
* -----------
*
* The processing takes place in four phases:
*
*      1. Obtain Existing Information.
*      2. Allocation - create UTRN records.
*      3. Update the newly created Coverage record with relevant
*         data fields.
*      4. Create ACMV.
*
* 1. Obtain Existing Information.
* -------------------------------
*
* Read the Contract Header using  the  RETRV  function  and  the
* logical via CHDRMJA. This will  return  the  Contract  Header
* details for the component being processed.
*
* Read the Coverage/Rider record using the  key  passed  in  the
* linkage area,  a  function  of  READH  and  the  logical  view
* COVRMJA.
*
* The following dated tables are used by this program:
*
* T5687 - read using  the  Coverage/Rider code (COVR-CRTABLE) as
* key.
*
* T5540 - read using the Coverage/Rider code (as above).
*
* T5519 - if the Initial  Unit  Discount  Basis  from  T5540  is
* non-blank then use it to read T5519.
*
* T5535 - if the Loyalty/Persistency code  (LTYPST)  from  T5540
* is non-blank then use it to read T5535.
*
* Read T6647 with the Transaction Code (passed  in  the linkage)
* and CHDR-Contract Type as a key.   This  is  the  Unit  Linked
* Contract Details table and holds the  rules  specific  to that
* product and transaction.
*
* Read T5537 with a key of allocation basis  (from  T5540),  Sex
* and transaction code. If the full key is not found,  then  '*'
* is substituted for sex and the  table  read  again.  If  still
* unsuccessful, Allocation Basis,  Sex  and Transaction Code set
* as '****' is searched for. If still  unsuccessful,  Allocation
* Basis, Sex as '*' and Transaction code as '****'  is  searched
* for. If still unsuccessful, '********' is searched  for.  This
* table holds a matrix of  Allocation  Basis  codes  held  in  a
* two-dimensional table with  Age as the vertical ('y') axis and
* Term as the horizontal ('x')  axis.  Locate  the occurrence of
* Age that is greater than or equal to the  Age  Next  Birthday.
* Locate the occurrence of Term that is greater  than  or  equal
* to the Term. Locate the appropriate method.
* Move the Item Code at  this  position  to  a  working  storage
* variable for Allocation Basis  (WSAA-ALLOC-BASIS)  for  future
* use.
*
* Read T5536 with a key of the Allocation Basis just found.
* Find the Billing Frequency on the table that matches  the
* billing frequency from the contract header (BILLFREQ) and move
* its associated group of three items: MAX-PERIODS,
* PC-UNITS and PC-INIT-UNITS to store for future use. Calculate
* PC-ACCUM-UNITS as  (100 - PC-INIT-UNITS),  i.e.  if  not 100%
* allocated to initial units, the remainder becomes
* accumulation units.
*****************************************************************
* </pre>
*/
public class Unlspcad extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLSPCAD";
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5537Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBasisSelect = new FixedLengthStringData(3).isAPartOf(wsaaT5537Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Key, 3).init(SPACES);
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5537Key, 4).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaContract = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4);
	private FixedLengthStringData wsaaJlifeExists = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLifeCltsex = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaLifeAnbAtCcd = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaJlifeCltsex = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaJlifeAnbAtCcd = new ZonedDecimalData(2, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTref, 15);
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
		/*                                                 VALUE ZE<D96NUM>*/
	private PackedDecimalData wsaaSingPrem = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaEnhancePerc = new PackedDecimalData(5, 2).init(ZERO);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5545Item, 4);

		/* WSAA-T5536-VALUES */
	private FixedLengthStringData wsaaMaxPeriods = new FixedLengthStringData(16);
	private ZonedDecimalData[] wsaaMaxPeriod = ZDArrayPartOfStructure(4, 4, 0, wsaaMaxPeriods, 0);

	private FixedLengthStringData wsaaPcUnits = new FixedLengthStringData(20);
	private ZonedDecimalData[] wsaaPcUnit = ZDArrayPartOfStructure(4, 5, 2, wsaaPcUnits, 0);

	private FixedLengthStringData wsaaPcInitUnits = new FixedLengthStringData(20);
	private ZonedDecimalData[] wsaaPcInitUnit = ZDArrayPartOfStructure(4, 5, 2, wsaaPcInitUnits, 0);
	private ZonedDecimalData wsaaDateSplit = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaDateSplit, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaYearSplit = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
	private PackedDecimalData wsaaRiskCessTerm = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaFact = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
		/* WSAA-TAXS */
	private ZonedDecimalData[] wsaaTax = ZDInittedArray(2, 17, 2);
	private PackedDecimalData wsaaTotCd = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String e616 = "E616";
	private static final String e706 = "E706";
	private static final String e707 = "E707";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private UlnkrnlTableDAM ulnkrnlIO = new UlnkrnlTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Itemkey wsaaItemkey = new Itemkey();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private T5687rec t5687rec = new T5687rec();
	private T6646rec t6646rec = new T6646rec();
	private T5539rec t5539rec = new T5539rec();
	private T5540rec t5540rec = new T5540rec();
	private T6647rec t6647rec = new T6647rec();
	private T5519rec t5519rec = new T5519rec();
	private T5537rec t5537rec = new T5537rec();
	private T5536rec t5536rec = new T5536rec();
	private T5545rec t5545rec = new T5545rec();
	private T5515rec t5515rec = new T5515rec();
	private T5535rec t5535rec = new T5535rec();
	private T5645rec t5645rec = new T5645rec();
	private T6659rec t6659rec = new T6659rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Getdescrec getdescrec = new Getdescrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Isuallrec isuallrec = new Isuallrec();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1460, 
		exit1490, 
		readItem1515, 
		cont1520, 
		yOrdinate1540, 
		xOrdinate1580, 
		exit1590, 
		exit1649, 
		fundAllocation2050, 
		exit2090, 
		exit2290, 
		nearExit2380, 
		next2450, 
		accumUnit2460, 
		next2470, 
		nearExit2480, 
		exit2490, 
		fixedTerm2530, 
		exit2590, 
		extraAllocDate3040, 
		unitStmntDate3050, 
		updateCovrmja3070, 
		exit3090, 
		b400WriteTax2, 
		b490Exit
	}

	public Unlspcad() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		isuallrec.statuz.set(varcom.oK);
		obtainInfo1000();
		if (isNE(isuallrec.statuz, varcom.oK)) {
			return ;
		}
		singlePremiumProcess2000();
		if (isNE(isuallrec.statuz, varcom.oK)) {
			return ;
		}
		b100ProcessTax();
		if (isNE(isuallrec.statuz, varcom.oK)) {
			return ;
		}
		updateCovr3000();
		if (isNE(isuallrec.statuz, varcom.oK)) {
			return ;
		}
		callLifacmv4000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void obtainInfo1000()
	{
		para1010();
	}

protected void para1010()
	{
		varcom.vrcmTime.set(getCobolTime());
		getFiles1100();
		getT56871200();
		getT55401300();
		getT66471400();
		getT55371500();
		getT55361600();
		if (isNE(t5540rec.iuDiscBasis,SPACES)) {
			getT55191650();
		}
		if (isNE(t5540rec.ltypst,SPACES)) {
			getT55351700();
		}
		getT56451750();
		if (isNE(t6647rec.unitStatMethod,SPACES)) {
			getT66591800();
		}
		else {
			t6659rec.t6659Rec.set(SPACES);
		}
		getT56881900();
	}

protected void getFiles1100()
	{
		para1110();
	}

protected void para1110()
	{
		/* Read the CHDRMJA logical.*/
		chdrmjaIO.setDataKey(SPACES);
		chdrmjaIO.setChdrcoy(isuallrec.company);
		chdrmjaIO.setChdrnum(isuallrec.chdrnum);
		chdrmjaIO.setStatuz(varcom.oK);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			dbError8100();
			return ;
		}
		/* Retrieve LIFE details.*/
		wsaaLifeCltsex.set(SPACES);
		wsaaJlifeExists.set(SPACES);
		wsaaLifeAnbAtCcd.set(0);
		wsaaJlifeAnbAtCcd.set(99);
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(isuallrec.company);
		lifelnbIO.setChdrnum(isuallrec.chdrnum);
		lifelnbIO.setLife(isuallrec.life);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			dbError8100();
			return ;
		}
		/* Need to see if a joint life exists. If so, use the younger of*/
		/* the two lives to find the unit allocation basis.*/
		wsaaLifeCltsex.set(lifelnbIO.getCltsex());
		wsaaLifeAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		lifelnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			dbError8100();
			return ;
		}
		if (isNE(isuallrec.company,lifelnbIO.getChdrcoy())
		|| isNE(isuallrec.chdrnum,lifelnbIO.getChdrnum())
		|| isNE(isuallrec.life,lifelnbIO.getLife())
		|| isEQ(lifelnbIO.getStatuz(),varcom.endp)) {
			wsaaJlifeExists.set("N");
		}
		else {
			wsaaJlifeExists.set("Y");
		}
		if (isEQ(wsaaJlifeExists,"Y")) {
			wsaaJlifeAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		}
		/* LIFELNB-CLTSEX and LIFELNB-ANB-AT-CCD are used to access*/
		/* T5536 and T5537, so we have to make sure that they have the*/
		/* correct informations in them.*/
		if (isGTE(wsaaJlifeAnbAtCcd,wsaaLifeAnbAtCcd)) {
			lifelnbIO.setCltsex(wsaaLifeCltsex);
			lifelnbIO.setAnbAtCcd(wsaaLifeAnbAtCcd);
		}
		/*  Read and hold coverage/rider record.*/
		covrmjaIO.setChdrcoy(isuallrec.company);
		covrmjaIO.setChdrnum(isuallrec.chdrnum);
		covrmjaIO.setLife(isuallrec.life);
		covrmjaIO.setCoverage(isuallrec.coverage);
		covrmjaIO.setRider(isuallrec.rider);
		covrmjaIO.setPlanSuffix(isuallrec.planSuffix);
		covrmjaIO.setFunction(varcom.readh);
		covrmjaIO.setFormat("COVRMJAREC");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			covrmjaIO.setParams(covrmjaIO.getParams());
			covrmjaIO.setStatuz(covrmjaIO.getStatuz());
			dbError8100();
			return ;
		}
		wsaaSingPrem.set(covrmjaIO.getSingp());
	}

protected void getT56871200()
	{
		para1210();
	}

protected void para1210()
	{
		/*  Read T5687 for general coverage/rider details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5687);
			itdmIO.setItemitem(covrmjaIO.getCrtable());
			itdmIO.setItmfrm(covrmjaIO.getCrrcd());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT55401300()
	{
		para11210();
	}

protected void para11210()
	{
		/*  Read T5540 for general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5540)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5540);
			itdmIO.setItemitem(covrmjaIO.getCrtable());
			itdmIO.setItmfrm(covrmjaIO.getCrrcd());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT66471400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1310();
					cont1450();
				case cont1460: 
					cont1460();
				case exit1490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1310()
	{
		/* Read T6647 for unit linked contract details.*/
		/* Key is transaction code plus contract type.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTrCde.set(isuallrec.batctrcde);
		wsaaContract.set(chdrmjaIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1490);
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6647)
		|| isNE(itdmIO.getItemitem(),wsaaT6647Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.cont1460);
		}
	}

protected void cont1450()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTrCde.set(isuallrec.batctrcde);
		wsaaContract.set("***");
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1490);
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6647)
		|| isNE(itdmIO.getItemitem(),wsaaT6647Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t6647);
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItmfrm(chdrmjaIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1490);
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void cont1460()
	{
		/* Calculate the monies date.*/
		if (isNE(covrmjaIO.getReserveUnitsDate(),ZERO)
		&& isNE(covrmjaIO.getReserveUnitsDate(),varcom.vrcmMaxDate)) {
			wsaaMoniesDate.set(covrmjaIO.getReserveUnitsDate());
			return ;
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError8000();
			return ;
		}
		wsaaMoniesDate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode,"DD")) {
			wsaaMoniesDate.set(chdrmjaIO.getOccdate());
		}
		if (isEQ(t6647rec.efdcode,"RD")) {
			wsaaMoniesDate.set(isuallrec.runDate);
		}
		if (isEQ(t6647rec.efdcode,"LO")) {
			if (isGT(chdrmjaIO.getOccdate(),wsaaMoniesDate)) {
				wsaaMoniesDate.set(chdrmjaIO.getOccdate());
			}
		}
	}

protected void getT55371500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1500();
				case readItem1515: 
					readItem1515();
				case cont1520: 
					cont1520();
				case yOrdinate1540: 
					yOrdinate1540();
					increment1560();
				case xOrdinate1580: 
					xOrdinate1580();
					increment1585();
				case exit1590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1500()
	{
		/* Read T5537 for unit allocation basis.*/
		wsaaBasisSelect.set(t5540rec.allbas);
		wsaaTranCode.set(isuallrec.batctrcde);
		wsaaT5537Sex.set(lifelnbIO.getCltsex());
	}

protected void readItem1515()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5537);
		itemIO.setItemitem(wsaaT5537Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1590);
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.cont1520);
		}
		/* If NO record found then try first with the basis, * for Sex and*/
		/* Transaction code.*/
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1590);
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			/*      GO TO 1450-CONT                                          */
			goTo(GotoLabel.cont1520);
		}
		/* If NO record found then try secondly with the basis, Sex and*/
		/* '****' Transaction code.*/
		wsaaT5537Sex.set(lifelnbIO.getCltsex());
		wsaaTranCode.fill("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1590);
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.cont1520);
		}
		/* If no match is found, try again with the global transaction*/
		/* code.*/
		wsaaTranCode.fill("*");
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1590);
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.cont1520);
		}
		/* And again if no match is found to prevent system falling over*/
		/* read T5537 with global key.*/
		itemIO.setItemitem("********");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1590);
		}
	}

protected void cont1520()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Work out the array's Y index based on ANB-AT-CCD.
	* </pre>
	*/
protected void yOrdinate1540()
	{
		wsaaZ.set(0);
	}

protected void increment1560()
	{
		wsaaZ.add(1);
		/* Check the continuation field for a key to re-read table.*/
		if (isGT(wsaaZ,10)
		&& isNE(t5537rec.agecont,SPACES)) {
			readAgecont6000();
			goTo(GotoLabel.yOrdinate1540);
		}
		else {
			if (isGT(wsaaZ,10)
			&& isEQ(t5537rec.agecont,SPACES)) {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(e706);
				dbError8100();
				goTo(GotoLabel.exit1590);
			}
		}
		if (isLTE(wsaaZ,10)) {
			/*        IF  LIFELNB-ANB-AT-CCD  >  T5537-TOAGE(WSAA-Z)           */
			if (isGT(lifelnbIO.getAnbAtCcd(), t5537rec.ageIssageTo[wsaaZ.toInt()])) {
				increment1560();
				return ;
			}
			else {
				wsaaY.set(wsaaZ);
			}
		}
		else {
			wsaaT5537Key.set(t5537rec.agecont);
			goTo(GotoLabel.readItem1515);
		}
	}

	/**
	* <pre>
	*  Work out the array's X index based on TERM.
	* </pre>
	*/
protected void xOrdinate1580()
	{
		wsaaZ.set(0);
	}

protected void increment1585()
	{
		/* Check the term continuation field for a key to re-read table.*/
		wsaaZ.add(1);
		if (isGT(wsaaZ,9)
		&& isNE(t5537rec.trmcont,SPACES)) {
			readTermcont7000();
			goTo(GotoLabel.xOrdinate1580);
		}
		else {
			if (isGT(wsaaZ,9)
			&& isEQ(t5537rec.trmcont,SPACES)) {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(e707);
				dbError8100();
				return ;
			}
		}
		/* Always calculate the term.*/
		calculateTerm1950();
		wsaaTerm.set(wsaaRiskCessTerm);
		if (isLTE(wsaaZ,9)) {
			if (isGT(wsaaTerm,t5537rec.toterm[wsaaZ.toInt()])) {
				increment1585();
				return ;
			}
			else {
				wsaaX.set(wsaaZ);
			}
		}
		else {
			wsaaT5537Key.set(t5537rec.trmcont);
			goTo(GotoLabel.readItem1515);
		}
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the basis.*/
		/* All we have to do now is to work out the corresponding position*/
		/* in the copybook by using these co-ordinates.(The copybook is*/
		/* defined as one-dimensional.)*/
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY,9));
		wsaaIndex.add(wsaaX);
		wsaaAllocBasis.set(t5537rec.actAllocBasis[wsaaIndex.toInt()]);
	}

protected void getT55361600()
	{
		try {
			para1610();
			gotRecord1620();
			nextFreq1640();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para1610()
	{
		/* Read T5536 for unit allocation basis details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5536);
		itdmIO.setItemitem(wsaaAllocBasis);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1649);
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5536)
		|| isNE(itdmIO.getItemitem(),wsaaAllocBasis)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5536);
			itdmIO.setItemitem(wsaaAllocBasis);
			itdmIO.setItmfrm(covrmjaIO.getCrrcd());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit1649);
		}
		else {
			t5536rec.t5536Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	* There are 6 sets of details in the table, each set is led by
	* the billing frequency. So to find the correct set of details,
	* match each of these frequencies with the billing frequency
	* specified in the contract header record.
	* </pre>
	*/
protected void gotRecord1620()
	{
		wsaaZ.set(0);
	}

protected void nextFreq1640()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ,6)) {
			isuallrec.statuz.set(e616);
			return ;
			/****      GO TO 090-EXIT                                           */
		}
		/* This is a single premium component get the frequency*/
		/* of '00' from the allocation table.*/
		if (isNE(t5536rec.billfreq[wsaaZ.toInt()],"00")
		|| isEQ(t5536rec.billfreq[wsaaZ.toInt()],SPACES)) {
			nextFreq1640();
			return ;
		}
		/* The appropriate allocation basis details found.*/
		if (isEQ(wsaaZ,1)) {
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsas);
			wsaaPcUnits.set(t5536rec.pcUnitsas);
		}
		if (isEQ(wsaaZ,2)) {
			wsaaPcUnits.set(t5536rec.pcUnitsbs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsbs);
		}
		if (isEQ(wsaaZ,3)) {
			wsaaPcUnits.set(t5536rec.pcUnitscs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitscs);
		}
		if (isEQ(wsaaZ,4)) {
			wsaaPcUnits.set(t5536rec.pcUnitsds);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsds);
		}
		if (isEQ(wsaaZ,5)) {
			wsaaPcUnits.set(t5536rec.pcUnitses);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitses);
		}
		if (isEQ(wsaaZ,6)) {
			wsaaPcUnits.set(t5536rec.pcUnitsfs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsfs);
		}
	}

protected void getT55191650()
	{
		para1650();
	}

protected void para1650()
	{
		/*  Read T5519 for initial unit discount basis.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5519);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5519)
		|| isNE(itdmIO.getItemitem(),t5540rec.iuDiscBasis)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5519);
			itdmIO.setItemitem(t5540rec.iuDiscBasis);
			itdmIO.setItmfrm(covrmjaIO.getCrrcd());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
			return ;
		}
		else {
			t5519rec.t5519Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT55351700()
	{
		para1700();
	}

protected void para1700()
	{
		/*  Read T5535 for loyalty allocation.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5535);
		itdmIO.setItemitem(t5540rec.ltypst);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5535)
		|| isNE(itdmIO.getItemitem(),t5540rec.ltypst)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5535);
			itdmIO.setItemitem(t5540rec.ltypst);
			itdmIO.setItmfrm(chdrmjaIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		else {
			t5535rec.t5535Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT56451750()
	{
		para1750();
	}

protected void para1750()
	{
		/* Read T5645 for transaction accounting rules.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
			return ;
		}
		else {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5645);
		descIO.setLanguage(isuallrec.language);
		descIO.setDescitem(wsaaSubr);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError8100();
			return ;
		}
	}

protected void getT66591800()
	{
		para1800();
	}

protected void para1800()
	{
		/* Read T6659 to get the frequency by using the unit statement*/
		/* method from T6647 as the key.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6659)
		|| isNE(itdmIO.getItemitem(),t6647rec.unitStatMethod)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t6659);
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			itdmIO.setItmfrm(covrmjaIO.getCrrcd());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT56881900()
	{
		para1910();
	}

protected void para1910()
	{
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrmjaIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			dbError8100();
			return ;
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void calculateTerm1950()
	{
		/*PARA*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrmjaIO.getCrrcd());
		datcon3rec.intDate2.set(covrmjaIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor,wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
		/*EXIT*/
	}

protected void singlePremiumProcess2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2010();
				case fundAllocation2050: 
					fundAllocation2050();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2010()
	{
		wsaaAmountHoldersInner.wsaaEnhanceAlloc.set(0);
		wsaaJrnseq.set(0);
		/* Calculate the uninvested allocation.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 3).setRounded(div((mult(wsaaSingPrem, (sub(100, wsaaPcUnit[1])))), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaNonInvestPrem.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying initial units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 3).setRounded(div(mult((sub(wsaaSingPrem, wsaaAmountHoldersInner.wsaaNonInvestPrem)), wsaaPcInitUnit[1]), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying accumulation units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 3).setRounded(sub(wsaaSingPrem, (add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaAllocInit))));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		/* Check for any enhancement for these preimums from T6647.*/
		/* (T6647 has been read in 10400-GET-T6647 section.)*/
		if (isEQ(t6647rec.enhall,SPACES)) {
			goTo(GotoLabel.fundAllocation2050);
		}
		/* Read T5545 to get the enhancement percentage.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5545);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		wsaaT5545Cntcurr.set(chdrmjaIO.getCntcurr());
		wsaaT5545Enhall.set(t6647rec.enhall);
		itdmIO.setItemitem(wsaaT5545Item);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5545)
		|| isNE(itdmIO.getItemitem(),wsaaT5545Item)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5545rec.t5545Rec.set(itdmIO.getGenarea());
		if (isEQ(t5545rec.t5545Rec,SPACES)) {
			goTo(GotoLabel.fundAllocation2050);
		}
		/* Look for the correct percentage.*/
		wsaaEnhancePerc.set(0);
		wsaaIndex.set(1);
		while ( !((isGT(wsaaIndex,6))
		|| (isNE(wsaaEnhancePerc,0)))) {
			matchEnhanceBasis2300();
		}
		
		if (isEQ(wsaaEnhancePerc,0)) {
			goTo(GotoLabel.fundAllocation2050);
		}
		/* Calculate the enhanced premiums.*/
		wsaaAmountHoldersInner.wsaaOldAllocInit.set(wsaaAmountHoldersInner.wsaaAllocInit);
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 3).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(sub(wsaaAmountHoldersInner.wsaaOldAllocInit, wsaaAmountHoldersInner.wsaaAllocInit));
		wsaaAmountHoldersInner.wsaaOldAllocAccum.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 3).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaEnhancePerc), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(add(wsaaAmountHoldersInner.wsaaEnhanceAlloc, (sub(wsaaAmountHoldersInner.wsaaOldAllocAccum, wsaaAmountHoldersInner.wsaaAllocAccum))));
	}

protected void fundAllocation2050()
	{
		/* Read ULNKRNL to get the fund split for the coverage/rider.*/
		ulnkrnlIO.setDataKey(SPACES);
		ulnkrnlIO.setChdrcoy(isuallrec.company);
		ulnkrnlIO.setChdrnum(isuallrec.chdrnum);
		ulnkrnlIO.setLife(isuallrec.life);
		ulnkrnlIO.setCoverage(isuallrec.coverage);
		ulnkrnlIO.setRider(isuallrec.rider);
		ulnkrnlIO.setPlanSuffix(isuallrec.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(formatsInner.ulnkrnlrec);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ulnkrnlIO.getStatuz());
			syserrrec.params.set(ulnkrnlIO.getParams());
			dbError8100();
			return ;
		}
		/* Calculate initial unit discount factor.*/
		/* Write a UTRN record for each fund split.*/
		initUnitDiscFactor2500();
		wsaaIndex.set(1);
		wsaaAmountHoldersInner.wsaaRunInitTot.set(0);
		wsaaAmountHoldersInner.wsaaRunAccumTot.set(0);
		while ( !((isGT(wsaaIndex,10))
		|| (isEQ(ulnkrnlIO.getUalfnd(wsaaIndex),SPACES)))) {
			fundSplitAllocation2400();
		}
		
		/* Write a UTRN record for the uninvested premium.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0).set(add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaEnhanceAlloc));
		if (isEQ(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0)) {
			return ;
		}
		/* MOVE SPACE                  TO UTRN-PARAMS           <INTBR> */
		/* PERFORM 2600-CHECK-EXISTING-UTRN.                    <INTBR> */
		/* IF UTRN-STATUZ = O-K                                 <INTBR> */
		/*     ADD WSAA-NON-INVEST-PREM TO UTRN-CONTRACT-AMOUNT <INTBR> */
		/*     PERFORM 5000-REWRITE-EXISTING-UTRN               <INTBR> */
		/* ELSE                                                 <INTBR> */
		/*     MOVE 'NVST'             TO UTRN-UNIT-SUB-ACCOUNT <INTBR> */
		/*     MOVE WSAA-NON-INVEST-PREM TO UTRN-CONTRACT-AMOUNT<INTBR> */
		/*     MOVE 0                  TO UTRN-DISCOUNT-FACTOR  <INTBR> */
		/*     PERFORM 2200-SET-UP-WRITE-UTRN                   <INTBR> */
		/* END-IF.                                              <INTBR> */
		if (isGT(wsaaAmountHoldersInner.wsaaTotUlSplit, 0)) {
			utrnIO.setParams(SPACES);
			checkExistingUtrn2600();
			if (isEQ(utrnIO.getStatuz(),varcom.oK)) {
				compute(wsaaAmountHoldersInner.wsaaContractAmount, 0).set(div(mult(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaTotUlSplit), 100));
				zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaContractAmount);
				a000CallRounding();
				wsaaAmountHoldersInner.wsaaContractAmount.set(zrdecplrec.amountOut);
				setPrecision(utrnIO.getContractAmount(), 2);
				utrnIO.setContractAmount(add(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaContractAmount));
				rewriteExistingUtrn5000();
			}
			else {
				utrnIO.setUnitSubAccount("NVST");
				compute(wsaaAmountHoldersInner.wsaaContractAmount, 0).set(div(mult(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaTotUlSplit), 100));
				zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaContractAmount);
				a000CallRounding();
				wsaaAmountHoldersInner.wsaaContractAmount.set(zrdecplrec.amountOut);
				utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaContractAmount);
				utrnIO.setDiscountFactor(0);
				setUpWriteUtrn2200();
			}
			if (isGT(wsaaAmountHoldersInner.wsaaTotIbSplit, 0)) {
				hitrIO.setParams(SPACES);
				a300CheckExistingHitr();
				if (isEQ(hitrIO.getStatuz(),varcom.oK)) {
					compute(wsaaAmountHoldersInner.wsaaContractAmount, 2).set(sub(wsaaAmountHoldersInner.wsaaNonInvestPrem, utrnIO.getContractAmount()));
					setPrecision(hitrIO.getContractAmount(), 2);
					hitrIO.setContractAmount(add(hitrIO.getContractAmount(), wsaaAmountHoldersInner.wsaaContractAmount));
					a400RewriteExistingHitr();
				}
				else {
					setPrecision(hitrIO.getContractAmount(), 2);
					hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaNonInvestPrem, utrnIO.getContractAmount()));
					a200SetUpWriteHitr();
				}
			}
		}
		else {
			hitrIO.setParams(SPACES);
			a300CheckExistingHitr();
			if (isEQ(hitrIO.getStatuz(),varcom.oK)) {
				setPrecision(hitrIO.getContractAmount(), 2);
				hitrIO.setContractAmount(add(hitrIO.getContractAmount(), wsaaAmountHoldersInner.wsaaNonInvestPrem));
				a400RewriteExistingHitr();
			}
			else {
				hitrIO.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPrem);
				a200SetUpWriteHitr();
			}
		}
	}

protected void setUpWriteUtrn2200()
	{
		try {
		para2210();
		cont2250();
	}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para2210()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
			goTo(GotoLabel.exit2290);
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(),utrnIO.getUnitVirtualFund())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		utrnIO.setChdrcoy(isuallrec.company);
		utrnIO.setChdrnum(isuallrec.chdrnum);
		utrnIO.setCoverage(isuallrec.coverage);
		utrnIO.setLife(isuallrec.life);
		utrnIO.setRider(isuallrec.rider);
		utrnIO.setPlanSuffix(isuallrec.planSuffix);
		utrnIO.setTranno(chdrmjaIO.getTranno());
		utrnIO.setTransactionTime(varcom.vrcmTime);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		utrnIO.setTransactionDate(datcon1rec.intDate);
		utrnIO.setUser(isuallrec.user);
		utrnIO.setBatccoy(isuallrec.batccoy);
		utrnIO.setBatcbrn(isuallrec.batcbrn);
		utrnIO.setBatcactyr(isuallrec.batcactyr);
		utrnIO.setBatcactmn(isuallrec.batcactmn);
		utrnIO.setBatctrcde(isuallrec.batctrcde);
		utrnIO.setBatcbatch(isuallrec.batcbatch);
		utrnIO.setCrtable(covrmjaIO.getCrtable());
		utrnIO.setCntcurr(chdrmjaIO.getCntcurr());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
				utrnIO.setSacscode(t5645rec.sacscode01);
				utrnIO.setSacstyp(t5645rec.sacstype01);
				utrnIO.setGenlcde(t5645rec.glmap01);
			}
			else {
				utrnIO.setSacscode(t5645rec.sacscode02);
				utrnIO.setSacstyp(t5645rec.sacstype02);
				utrnIO.setGenlcde(t5645rec.glmap02);
			}
		}
		else {
			if (isEQ(utrnIO.getUnitSubAccount(),"NVST")) {
				utrnIO.setSacscode(t5645rec.sacscode03);
				utrnIO.setSacstyp(t5645rec.sacstype03);
				utrnIO.setGenlcde(t5645rec.glmap03);
			}
			else {
				utrnIO.setSacscode(t5645rec.sacscode04);
				utrnIO.setSacstyp(t5645rec.sacstype04);
				utrnIO.setGenlcde(t5645rec.glmap04);
			}
		}
	}

protected void cont2250()
	{
		utrnIO.setContractType(chdrmjaIO.getCnttype());
		utrnIO.setSvp(1);
		utrnIO.setCrComDate(covrmjaIO.getCrrcd());
		utrnIO.setFundCurrency(t5515rec.currcode);
		if (isLT(utrnIO.getContractAmount(),0)) {
			utrnIO.setNowDeferInd(t6647rec.dealin);
		}
		else {
			utrnIO.setNowDeferInd(t6647rec.aloind);
		}
		utrnIO.setMoniesDate(wsaaMoniesDate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setJobnoPrice(0);
		utrnIO.setFundRate(0);
		utrnIO.setStrpdate(0);
		utrnIO.setUstmno(0);
		utrnIO.setNofUnits(0);
		utrnIO.setNofDunits(0);
		utrnIO.setPriceDateUsed(0);
		utrnIO.setPriceUsed(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setInciPerd01(0);
		utrnIO.setInciPerd02(0);
		utrnIO.setInciprm01(0);
		utrnIO.setInciprm02(0);
		utrnIO.setFundAmount(0);
		utrnIO.setSurrenderPercent(0);
		utrnIO.setInciNum(0);
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(formatsInner.utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			dbError8100();
		}
	}

protected void matchEnhanceBasis2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2310();
					enhana2300();
					enhanb2320();
					enhanc2330();
					enhand2340();
					enhane2350();
					enhanf2360();
				case nearExit2380: 
					nearExit2380();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2310()
	{
		/* First match the billing frequency.*/
		/* We are only dealing with Single Prems here, so check for '00'*/
		if (isNE(t5545rec.billfreq[wsaaIndex.toInt()],"00")
		|| isEQ(t5545rec.billfreq[wsaaIndex.toInt()],SPACES)) {
			goTo(GotoLabel.nearExit2380);
		}
	}

	/**
	* <pre>
	* Once the billing frequency is matched, match the premium.
	* There are 5 premiums in each billing frequency set, so
	* match each premium individually.
	* </pre>
	*/
protected void enhana2300()
	{
		if (isEQ(wsaaIndex,1)) {
			wsaaEnhancePerc.set(t5545rec.enhanaPc01);
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanaPrm05)
			&& isNE(t5545rec.enhanaPrm05,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanaPc05);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanaPrm04)
			&& isNE(t5545rec.enhanaPrm04,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanaPc04);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanaPrm03)
			&& isNE(t5545rec.enhanaPrm03,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanaPc03);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanaPrm02)
			&& isNE(t5545rec.enhanaPrm02,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanaPc02);
			}
			if (isLT(isuallrec.covrSingp,t5545rec.enhancPrm02)
			&& isNE(t5545rec.enhancPc01,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc01);
			}
			goTo(GotoLabel.nearExit2380);
		}
	}

protected void enhanb2320()
	{
		if (isEQ(wsaaIndex,2)) {
			wsaaEnhancePerc.set(t5545rec.enhanbPc01);
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanbPrm05)
			&& isNE(t5545rec.enhanbPrm05,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanbPc05);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanbPrm04)
			&& isNE(t5545rec.enhanbPrm04,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanbPc04);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanbPrm03)
			&& isNE(t5545rec.enhanbPrm03,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanbPc03);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanbPrm02)
			&& isNE(t5545rec.enhanbPrm02,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanbPc02);
			}
			if (isLT(isuallrec.covrSingp,t5545rec.enhancPrm02)
			&& isNE(t5545rec.enhancPc01,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc01);
			}
			goTo(GotoLabel.nearExit2380);
		}
	}

protected void enhanc2330()
	{
		if (isEQ(wsaaIndex,3)) {
			wsaaEnhancePerc.set(t5545rec.enhancPc01);
			if (isGTE(isuallrec.covrSingp,t5545rec.enhancPrm05)
			&& isNE(t5545rec.enhancPrm05,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc05);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhancPrm04)
			&& isNE(t5545rec.enhancPrm04,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc04);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhancPrm03)
			&& isNE(t5545rec.enhancPrm03,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc03);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhancPrm02)
			&& isNE(t5545rec.enhancPrm02,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc02);
			}
			if (isLT(isuallrec.covrSingp,t5545rec.enhancPrm02)
			&& isNE(t5545rec.enhancPc01,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc01);
			}
			goTo(GotoLabel.nearExit2380);
		}
	}

protected void enhand2340()
	{
		if (isEQ(wsaaIndex,4)) {
			wsaaEnhancePerc.set(t5545rec.enhandPc01);
			if (isGTE(isuallrec.covrSingp,t5545rec.enhandPrm05)
			&& isNE(t5545rec.enhandPrm05,0)) {
				wsaaEnhancePerc.set(t5545rec.enhandPc05);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhandPrm04)
			&& isNE(t5545rec.enhandPrm04,0)) {
				wsaaEnhancePerc.set(t5545rec.enhandPc04);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhandPrm03)
			&& isNE(t5545rec.enhandPrm03,0)) {
				wsaaEnhancePerc.set(t5545rec.enhandPc03);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhandPrm02)
			&& isNE(t5545rec.enhandPrm02,0)) {
				wsaaEnhancePerc.set(t5545rec.enhandPc02);
			}
			if (isLT(isuallrec.covrSingp,t5545rec.enhancPrm02)
			&& isNE(t5545rec.enhancPc01,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc01);
			}
			goTo(GotoLabel.nearExit2380);
		}
	}

protected void enhane2350()
	{
		if (isEQ(wsaaIndex,5)) {
			wsaaEnhancePerc.set(t5545rec.enhanePc01);
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanePrm05)
			&& isNE(t5545rec.enhanePrm05,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanePc05);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanePrm04)
			&& isNE(t5545rec.enhanePrm04,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanePc04);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanePrm03)
			&& isNE(t5545rec.enhanePrm03,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanePc03);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanePrm02)
			&& isNE(t5545rec.enhanePrm02,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanePc02);
			}
			if (isLT(isuallrec.covrSingp,t5545rec.enhancPrm02)
			&& isNE(t5545rec.enhancPc01,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc01);
			}
			goTo(GotoLabel.nearExit2380);
		}
	}

protected void enhanf2360()
	{
		if (isEQ(wsaaIndex,6)) {
			wsaaEnhancePerc.set(t5545rec.enhanfPc01);
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanePrm05)
			&& isNE(t5545rec.enhanfPrm05,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanfPc05);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanePrm04)
			&& isNE(t5545rec.enhanfPrm04,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanfPc04);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanePrm03)
			&& isNE(t5545rec.enhanfPrm03,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanfPc03);
			}
			if (isGTE(isuallrec.covrSingp,t5545rec.enhanePrm02)
			&& isNE(t5545rec.enhanfPrm02,0)) {
				wsaaEnhancePerc.set(t5545rec.enhanfPc02);
			}
			if (isLT(isuallrec.covrSingp,t5545rec.enhancPrm02)
			&& isNE(t5545rec.enhancPc01,0)) {
				wsaaEnhancePerc.set(t5545rec.enhancPc01);
			}
		}
	}

protected void nearExit2380()
	{
		wsaaIndex.add(1);
		/*EXIT*/
	}

protected void fundSplitAllocation2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initUnit2410();
				case next2450: 
					next2450();
				case accumUnit2460: 
					accumUnit2460();
				case next2470: 
					next2470();
				case nearExit2480: 
					nearExit2480();
				case exit2490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initUnit2410()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setItemitem(ulnkrnlIO.getUalfnd(wsaaIndex));
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.exit2490);
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(),ulnkrnlIO.getUalfnd(wsaaIndex))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isEQ(t5515rec.zfundtyp,"D")) {
			compute(wsaaAmountHoldersInner.wsaaAllocTot, 0).set(add(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaAllocAccum));
			wsaaAmountHoldersInner.wsaaTotIbSplit.add(ulnkrnlIO.getUalprc(wsaaIndex));
			if (isNE(wsaaAmountHoldersInner.wsaaAllocTot, 0)) {
				a100ProcessHitr();
			}
			goTo(GotoLabel.nearExit2480);
		}
		wsaaAmountHoldersInner.wsaaTotUlSplit.add(ulnkrnlIO.getUalprc(wsaaIndex));
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		/* Invest the initial units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocInit, 0)) {
			goTo(GotoLabel.accumUnit2460);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaIndex,10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaRunInitTot));
			goTo(GotoLabel.next2450);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex,1)),SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaRunInitTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
		}
	}

protected void next2450()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		a000CallRounding();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunInitTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaUtrnContractAmount.set(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
		utrnIO.setUnitType("I");
		checkExistingUtrn2600();
		if (isEQ(utrnIO.getStatuz(),varcom.oK)) {
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(add(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaUtrnContractAmount));
			rewriteExistingUtrn5000();
		}
		else {
			utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaUtrnContractAmount);
			utrnIO.setUnitSubAccount("INIT");
			utrnIO.setDiscountFactor(wsaaAmountHoldersInner.wsaaInitUnitDiscFact);
			setUpWriteUtrn2200();
		}
	}

protected void accumUnit2460()
	{
		/* Invest the accumulation units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocAccum, 0)) {
			goTo(GotoLabel.nearExit2480);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaIndex,10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaAmountHoldersInner.wsaaRunAccumTot));
			goTo(GotoLabel.next2470);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex,1)),SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaAmountHoldersInner.wsaaRunAccumTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
		}
	}

protected void next2470()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		a000CallRounding();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaUtrnContractAmount.set(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
		utrnIO.setUnitType("A");
		checkExistingUtrn2600();
		if (isEQ(utrnIO.getStatuz(),varcom.oK)) {
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(add(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaUtrnContractAmount));
			rewriteExistingUtrn5000();
		}
		else {
			utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaUtrnContractAmount);
			utrnIO.setUnitSubAccount("ACUM");
			utrnIO.setDiscountFactor(0);
			setUpWriteUtrn2200();
		}
	}

protected void nearExit2480()
	{
		wsaaIndex.add(1);
	}

protected void initUnitDiscFactor2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2510();
					wholeOfLife2510();
					termToRun2520();
				case fixedTerm2530: 
					fixedTerm2530();
				case exit2590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2510()
	{
		wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(1);
		if (isEQ(t5540rec.iuDiscBasis,SPACES)
		|| isEQ(t5540rec.iuDiscFact,SPACES)
		&& isEQ(t5540rec.wholeIuDiscFact,SPACES)) {
			goTo(GotoLabel.exit2590);
		}
	}

protected void wholeOfLife2510()
	{
		/* There are 3 methods to obtain the discount factor, which one to*/
		/* use is dependent on the field entries in T5540 & T5519 which hav*/
		/* been read at inialisation stage.(Only one method is allowed.)*/
		/* If whole of life(T5540-WOL-IU-DISC-FACT) factor is not space,*/
		/* then use the age at next birthday to read off T6646 to obtain*/
		/* the discount factor.*/
		if (isEQ(t5540rec.wholeIuDiscFact,SPACES)) {
			if (isEQ(covrmjaIO.getAnbAtCcd(),0)) {
				goTo(GotoLabel.exit2590);
			}
			if (isNE(t5540rec.wholeIuDiscFact,SPACES)) {
				getT66462700();
			}
			if (isLT(covrmjaIO.getAnbAtCcd(), 100)) {
				wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.dfact[covrmjaIO.getAnbAtCcd().toInt()]);
			}
			else {
				compute(wsaaFact, 0).set(sub(covrmjaIO.getAnbAtCcd(), 99));
				wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaFact.toInt()]);
			}
			compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
			goTo(GotoLabel.exit2590);
		}
	}

protected void termToRun2520()
	{
		/* If the discount basis(T5540-IU-DISC-BASIS) is not space, then*/
		/* use this basis to access T5519. If the fixed term(T5519-FIXDTRM)*/
		/* is not zero, use it to read off T5539 to obtain the discount*/
		/* factor, otherwise use the term left to run to read off T5539 to*/
		/* obtain the discount factor. T5539 is accessed by the discount*/
		/* factor(T5540-IU-DISC-FACT) fro T5540.*/
		if (isNE(t5519rec.fixdtrm,0)
		&& isGT(wsaaRiskCessTerm,t5519rec.fixdtrm)) {
			goTo(GotoLabel.fixedTerm2530);
		}
		/* Always Calculate The Term Left.*/
		/* Calculate the term left to run if risk cessation term is 0.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrmjaIO.getCrrcd());
		datcon3rec.intDate2.set(covrmjaIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor,wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
		if (isNE(t5540rec.iuDiscFact,SPACES)) {
			getT55392800();
		}
		/*    MOVE T5539-DFACT(WSAA-RISK-CESS-TERM) TO                     */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(wsaaRiskCessTerm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[wsaaRiskCessTerm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(wsaaRiskCessTerm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit2590);
	}

protected void fixedTerm2530()
	{
		/* use this fixed term to read off T5539 to obtain the*/
		/* discount factor.*/
		if (isNE(t5540rec.iuDiscFact,SPACES)) {
			getT55392800();
		}
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                           */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
	}

protected void checkExistingUtrn2600()
	{
		para2610();
	}

protected void para2610()
	{
		utrnIO.setChdrcoy(isuallrec.company);
		utrnIO.setChdrnum(isuallrec.chdrnum);
		utrnIO.setCoverage(isuallrec.coverage);
		utrnIO.setLife(isuallrec.life);
		utrnIO.setRider(isuallrec.rider);
		utrnIO.setPlanSuffix(isuallrec.planSuffix);
		utrnIO.setTranno(chdrmjaIO.getTranno());
		utrnIO.setFunction(varcom.readh);
		utrnIO.setFormat(formatsInner.utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)
		&& isNE(utrnIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			dbError8100();
		}
	}

protected void getT66462700()
	{
		para2710();
	}

protected void para2710()
	{
		/* Read T6646 for initial unit discount factor for whole of life.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t6646);
		itemIO.setItemitem(t5540rec.wholeIuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
			return ;
		}
		t6646rec.t6646Rec.set(itemIO.getGenarea());
	}

protected void getT55392800()
	{
		para2810();
	}

protected void para2810()
	{
		/*  Read T5539 for initial unit discount factor for term.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5539);
		itemIO.setItemitem(t5540rec.iuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
			return ;
		}
		else {
			t5539rec.t5539Rec.set(itemIO.getGenarea());
		}
	}

protected void updateCovr3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					convertAnniversaryDate3010();
					unitCancellationDate3010();
					unitConversionDate3020();
				case extraAllocDate3040: 
					extraAllocDate3040();
				case unitStmntDate3050: 
					unitStmntDate3050();
					iuIncrDate3060();
				case updateCovrmja3070: 
					updateCovrmja3070();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void convertAnniversaryDate3010()
	{
		datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
			goTo(GotoLabel.exit3090);
		}
		datcon2rec.intDate1.set(datcon2rec.intDate2);
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
			goTo(GotoLabel.exit3090);
		}
		covrmjaIO.setAnnivProcDate(datcon2rec.intDate2);
	}

protected void unitCancellationDate3010()
	{
		if (isEQ(t5540rec.iuDiscBasis,SPACES)) {
			goTo(GotoLabel.extraAllocDate3040);
		}
		if (isEQ(t5519rec.unitdeduc,"Y")) {
			datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
			datcon2rec.frequency.set(t5519rec.initUnitChargeFreq);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.params.set(datcon2rec.datcon2Rec);
				systemError8000();
				goTo(GotoLabel.exit3090);
			}
			else {
				covrmjaIO.setInitUnitCancDate(datcon2rec.intDate2);
				goTo(GotoLabel.extraAllocDate3040);
			}
		}
	}

protected void unitConversionDate3020()
	{
		if (isNE(t5519rec.unitadj,"Y")) {
			goTo(GotoLabel.extraAllocDate3040);
		}
		if (isEQ(t5519rec.fixdtrm,0)) {
			covrmjaIO.setConvertInitialUnits(covrmjaIO.getRiskCessDate());
			goTo(GotoLabel.extraAllocDate3040);
		}
		wsaaDateSplit.set(chdrmjaIO.getOccdate());
		wsaaYearSplit.add(t5519rec.fixdtrm);
		covrmjaIO.setConvertInitialUnits(wsaaDateSplit);
	}

protected void extraAllocDate3040()
	{
		if (isEQ(t5540rec.ltypst,SPACES)) {
			goTo(GotoLabel.unitStmntDate3050);
		}
		datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon2rec.freqFactor.set(t5535rec.unitAfterDur01);
		datcon2rec.frequency.set("12");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit3090);
		}
		covrmjaIO.setReviewProcessing(datcon2rec.intDate2);
	}

protected void unitStmntDate3050()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		/* Use the frequency from T6659 to calculate the unit statement*/
		/* date. Increment the risk commencement date by the frequency*/
		/* obtained, this will give the unit statement date.*/
		if (isEQ(t6659rec.freq,SPACES)) {
			datcon2rec.frequency.set("00");
		}
		else {
			datcon2rec.frequency.set(t6659rec.freq);
		}
		datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon2rec.intDate2.set(0);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
			goTo(GotoLabel.exit3090);
		}
		covrmjaIO.setUnitStatementDate(datcon2rec.intDate2);
	}

protected void iuIncrDate3060()
	{
		if (isEQ(t5540rec.iuDiscBasis,SPACES)) {
			goTo(GotoLabel.updateCovrmja3070);
		}
		if (isNE(t5519rec.diffprice,"Y")
		|| isNE(t5519rec.unitadj,"Y")) {
			goTo(GotoLabel.updateCovrmja3070);
		}
		covrmjaIO.setInitUnitIncrsDate(covrmjaIO.getAnnivProcDate());
	}

protected void updateCovrmja3070()
	{
		/* Update COVRMJA.*/
		covrmjaIO.setChdrcoy(isuallrec.company);
		covrmjaIO.setChdrnum(isuallrec.chdrnum);
		covrmjaIO.setLife(isuallrec.life);
		covrmjaIO.setCoverage(isuallrec.coverage);
		covrmjaIO.setRider(isuallrec.rider);
		covrmjaIO.setPlanSuffix(isuallrec.planSuffix);
		setPrecision(covrmjaIO.getCoverageDebt(), 2);
		covrmjaIO.setCoverageDebt(add(covrmjaIO.getCoverageDebt(), add(wsaaTax[1], wsaaTax[2])));
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			covrmjaIO.setParams(covrmjaIO.getParams());
			covrmjaIO.setStatuz(covrmjaIO.getStatuz());
			dbError8100();
		}
	}

protected void callLifacmv4000()
	{
			lifacmv4010();
		}

protected void lifacmv4010()
	{
		wsaaBatchkey.set(isuallrec.batchkey);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(wsaaBatchkey.batcBatccoy);
		lifacmvrec.batcbrn.set(wsaaBatchkey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatchkey.batcBatcactyr);
		lifacmvrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		lifacmvrec.batcactmn.set(wsaaBatchkey.batcBatcactmn);
		lifacmvrec.batcbatch.set(wsaaBatchkey.batcBatcbatch);
		lifacmvrec.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec.tranno.set(chdrmjaIO.getTranno());
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.rldgcoy.set(wsaaBatchkey.batcBatccoy);
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		lifacmvrec.origamt.set(covrmjaIO.getSingp());
		lifacmvrec.tranref.set(chdrmjaIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(ZERO);
		compute(lifacmvrec.acctamt, 9).set(mult(lifacmvrec.origamt, lifacmvrec.crate));
		lifacmvrec.genlcoy.set(isuallrec.company);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.postyear.set(wsaaBatchkey.batcBatcactyr);
		lifacmvrec.postmonth.set(wsaaBatchkey.batcBatcactmn);
		lifacmvrec.effdate.set(covrmjaIO.getCurrfrom());
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.transactionDate.set(isuallrec.transactionDate);
		lifacmvrec.transactionTime.set(isuallrec.transactionTime);
		lifacmvrec.user.set(isuallrec.user);
		lifacmvrec.termid.set(isuallrec.termid);
		/*        Set up new RLDGACCT field for Component Level Accounting*/
		if ((isEQ(t5688rec.comlvlacc,"Y"))) {
			lifacmvrec.rldgacct.set(SPACES);
			wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum());
			wsaaRldgLife.set(covrmjaIO.getLife());
			wsaaRldgCoverage.set(covrmjaIO.getCoverage());
			wsaaRldgRider.set(covrmjaIO.getRider());
			wsaaRldgPlanSuffix.set(covrmjaIO.getPlanSuffix());
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(covrmjaIO.getCrtable());
		}
		else {
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError8000();
			return ;
		}
	}

protected void rewriteExistingUtrn5000()
	{
		/*PARA*/
		utrnIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void readAgecont6000()
	{
		/*READ-AGECONT*/
		itemIO.setItemitem(t5537rec.agecont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
			return ;
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		wsaaIndex.subtract(99);
		/*EXIT*/
	}

protected void readTermcont7000()
	{
		/*READ-TERMCONT*/
		itemIO.setItemitem(t5537rec.trmcont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
			return ;
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void a100ProcessHitr()
	{
		a110Hitr();
	}

protected void a110Hitr()
	{
		hitrIO.setParams(SPACES);
		if (isEQ(wsaaIndex,10)) {
			setPrecision(hitrIO.getContractAmount(), 0);
			hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
		}
		else {
			if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex,1)),SPACES)) {
				setPrecision(hitrIO.getContractAmount(), 0);
				hitrIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocTot, wsaaAmountHoldersInner.wsaaRunTot));
			}
			else {
				setPrecision(hitrIO.getContractAmount(), 1);
				hitrIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocTot, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
				compute(wsaaAmountHoldersInner.wsaaInitAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, ulnkrnlIO.getUalprc(wsaaIndex)), 100));
				compute(wsaaAmountHoldersInner.wsaaAccumAmount, 1).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, ulnkrnlIO.getUalprc(wsaaIndex)), 100));
			}
		}
		zrdecplrec.amountIn.set(hitrIO.getContractAmount());
		a000CallRounding();
		hitrIO.setContractAmount(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaInitAmount);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaInitAmount.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAccumAmount);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAccumAmount.set(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunTot.add(hitrIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaRunInitTot.add(wsaaAmountHoldersInner.wsaaInitAmount);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(wsaaAmountHoldersInner.wsaaAccumAmount);
		hitrIO.setZintbfnd(ulnkrnlIO.getUalfnd(wsaaIndex));
		wsaaAmountHoldersInner.wsaaHitrContractAmount.set(hitrIO.getContractAmount());
		a300CheckExistingHitr();
		if (isEQ(hitrIO.getStatuz(),varcom.oK)) {
			setPrecision(hitrIO.getContractAmount(), 2);
			hitrIO.setContractAmount(add(hitrIO.getContractAmount(), wsaaAmountHoldersInner.wsaaHitrContractAmount));
			a400RewriteExistingHitr();
		}
		else {
			hitrIO.setContractAmount(wsaaAmountHoldersInner.wsaaHitrContractAmount);
			a200SetUpWriteHitr();
		}
	}

protected void a200SetUpWriteHitr()
	{
		a210Para();
	}

protected void a210Para()
	{
		hitrIO.setChdrcoy(isuallrec.company);
		hitrIO.setChdrnum(isuallrec.chdrnum);
		hitrIO.setCoverage(isuallrec.coverage);
		hitrIO.setLife(isuallrec.life);
		hitrIO.setRider("00");
		hitrIO.setPlanSuffix(isuallrec.planSuffix);
		hitrIO.setTranno(chdrmjaIO.getTranno());
		hitrIO.setBatccoy(isuallrec.batccoy);
		hitrIO.setBatcbrn(isuallrec.batcbrn);
		hitrIO.setBatcactyr(isuallrec.batcactyr);
		hitrIO.setBatcactmn(isuallrec.batcactmn);
		hitrIO.setBatctrcde(isuallrec.batctrcde);
		hitrIO.setBatcbatch(isuallrec.batcbatch);
		hitrIO.setCrtable(covrmjaIO.getCrtable());
		hitrIO.setCntcurr(chdrmjaIO.getCntcurr());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			if (isEQ(hitrIO.getZintbfnd(),SPACES)) {
				hitrIO.setSacscode(t5645rec.sacscode08);
				hitrIO.setSacstyp(t5645rec.sacstype08);
				hitrIO.setGenlcde(t5645rec.glmap08);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode09);
				hitrIO.setSacstyp(t5645rec.sacstype09);
				hitrIO.setGenlcde(t5645rec.glmap09);
			}
		}
		else {
			if (isEQ(hitrIO.getZintbfnd(),SPACES)) {
				hitrIO.setSacscode(t5645rec.sacscode10);
				hitrIO.setSacstyp(t5645rec.sacstype10);
				hitrIO.setGenlcde(t5645rec.glmap10);
			}
			else {
				hitrIO.setSacscode(t5645rec.sacscode11);
				hitrIO.setSacstyp(t5645rec.sacstype11);
				hitrIO.setGenlcde(t5645rec.glmap11);
			}
		}
		hitrIO.setCnttyp(chdrmjaIO.getCnttype());
		hitrIO.setSvp(1);
		hitrIO.setFundCurrency(t5515rec.currcode);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciNum(ZERO);
		if (isEQ(hitrIO.getZintbfnd(),SPACES)) {
			hitrIO.setZrectyp("N");
		}
		else {
			hitrIO.setZrectyp("P");
		}
		hitrIO.setEffdate(isuallrec.effdate);
		hitrIO.setZintappind(SPACES);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZintrate(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setFunction(varcom.writr);
		hitrIO.setFormat(formatsInner.hitrrec);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			syserrrec.statuz.set(hitrIO.getStatuz());
			dbError8100();
		}
	}

protected void a300CheckExistingHitr()
	{
		a310Para();
	}

protected void a310Para()
	{
		hitrIO.setChdrcoy(isuallrec.company);
		hitrIO.setChdrnum(isuallrec.chdrnum);
		hitrIO.setCoverage(isuallrec.coverage);
		hitrIO.setLife(isuallrec.life);
		hitrIO.setRider("00");
		hitrIO.setPlanSuffix(isuallrec.planSuffix);
		hitrIO.setTranno(chdrmjaIO.getTranno());
		hitrIO.setFunction(varcom.readh);
		hitrIO.setFormat(formatsInner.hitrrec);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)
		&& isNE(hitrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hitrIO.getParams());
			syserrrec.statuz.set(hitrIO.getStatuz());
			dbError8100();
		}
	}

protected void a400RewriteExistingHitr()
	{
		/*A410-PARA*/
		hitrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			syserrrec.statuz.set(hitrIO.getStatuz());
			dbError8100();
		}
		/*A490-EXIT*/
	}

protected void b100ProcessTax()
	{
		b110Start();
	}

protected void b110Start()
	{
		wsaaTax[1].set(0);
		wsaaTax[2].set(0);
		if (isNE(wsaaAmountHoldersInner.wsaaNonInvestPrem, ZERO)) {
			return ;
		}
		/* Read table TR52D using CHDRMJA-REGISTER as key                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrmjaIO.getRegister());
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
			return ;
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItemcoy(isuallrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setItempfx("IT");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				dbError8100();
				return ;
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		wsaaTr52eCrtable.set(covrmjaIO.getCrtable());
		b200ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			b200ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b200ReadTr52e();
		}
		/* Call TR52D tax subroutine                                       */
		if (isNE(tr52erec.taxind05, "Y")
		|| isEQ(wsaaAmountHoldersInner.wsaaNonInvestPrem, ZERO)) {
			return ;
		}
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		txcalcrec.life.set(covrmjaIO.getLife());
		txcalcrec.coverage.set(covrmjaIO.getCoverage());
		txcalcrec.rider.set(covrmjaIO.getRider());
		txcalcrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		txcalcrec.crtable.set(covrmjaIO.getCrtable());
		txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
		txcalcrec.register.set(chdrmjaIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		wsaaCntCurr.set(chdrmjaIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(isuallrec.effdate);
		txcalcrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		txcalcrec.tranno.set(chdrmjaIO.getTranno());
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.jrnseq.set(ZERO);
		txcalcrec.transType.set("NINV");
		txcalcrec.batckey.set(isuallrec.batchkey);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			syserrrec.params.set(txcalcrec.linkRec);
			dbError8100();
			return ;
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		taxdIO.setChdrnum(chdrmjaIO.getChdrnum());
		taxdIO.setLife(covrmjaIO.getLife());
		taxdIO.setCoverage(covrmjaIO.getCoverage());
		taxdIO.setRider(covrmjaIO.getRider());
		taxdIO.setPlansfx(covrmjaIO.getPlanSuffix());
		taxdIO.setEffdate(isuallrec.effdate);
		taxdIO.setInstfrom(isuallrec.effdate);
		taxdIO.setInstto(varcom.vrcmMaxDate);
		taxdIO.setBillcd(varcom.vrcmMaxDate);
		taxdIO.setTranno(chdrmjaIO.getTranno());
		taxdIO.setTrantype("NINV");
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			dbError8100();
			return ;
		}
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTax[1].set(0);
		}
		else {
			wsaaTax[1].set(txcalcrec.taxAmt[1]);
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTax[2].set(0);
		}
		else {
			wsaaTax[2].set(txcalcrec.taxAmt[2]);
		}
		/* Create ZRST record                                              */
		zrstIO.setDataArea(SPACES);
		zrstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		zrstIO.setChdrnum(covrmjaIO.getChdrnum());
		zrstIO.setLife(covrmjaIO.getLife());
		zrstIO.setCoverage(SPACES);
		zrstIO.setRider(SPACES);
		zrstIO.setFormat(formatsInner.zrstrec);
		zrstIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
			b300ReadZrst();
		}
		
		compute(wsaaTotCd, 2).add(add(wsaaTax[1], wsaaTax[2]));
		b400WriteZrst();
		/* Update ZRST record                                              */
		zrstIO.setDataArea(SPACES);
		zrstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		zrstIO.setChdrnum(covrmjaIO.getChdrnum());
		zrstIO.setLife(covrmjaIO.getLife());
		zrstIO.setCoverage(SPACES);
		zrstIO.setRider(SPACES);
		zrstIO.setFormat(formatsInner.zrstrec);
		zrstIO.setFunction(varcom.begn);
		while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
			b500UpdateZrst();
		}
		
		/* Post coverage debt                                              */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(isuallrec.company);
		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
		wsaaItemkey.itemItemitem.set(isuallrec.batctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set("E");
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(isuallrec.chdrnum);
		lifacmvrec.jrnseq.set(txcalcrec.jrnseq);
		lifacmvrec.batccoy.set(isuallrec.company);
		lifacmvrec.rldgcoy.set(isuallrec.company);
		lifacmvrec.genlcoy.set(isuallrec.company);
		lifacmvrec.batcactyr.set(isuallrec.batcactyr);
		lifacmvrec.batctrcde.set(isuallrec.batctrcde);
		lifacmvrec.batcactmn.set(isuallrec.batcactmn);
		lifacmvrec.batcbatch.set(isuallrec.batcbatch);
		lifacmvrec.batcbrn.set(isuallrec.batcbrn);
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		compute(lifacmvrec.origamt, 2).set(add(wsaaTax[1], wsaaTax[2]));
		lifacmvrec.rcamt.set(0);
		lifacmvrec.tranno.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrmjaIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(isuallrec.effdate);
		/* Set up TRANREF to contain the Full component key of the         */
		/* Coverage/Rider that is Creating the debt so that if a           */
		/* reversal is required at a later date, the correct component     */
		/* can be identified.                                              */
		wsaaTref.set(SPACES);
		wsaaTrefChdrcoy.set(isuallrec.company);
		wsaaTrefChdrnum.set(isuallrec.chdrnum);
		wsaaPlan.set(isuallrec.planSuffix);
		wsaaTrefLife.set(isuallrec.life);
		wsaaTrefCoverage.set(isuallrec.coverage);
		wsaaTrefRider.set(isuallrec.rider);
		wsaaTrefPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaTref);
		lifacmvrec.user.set(isuallrec.user);
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		/* Check for Component Level accounting and act accordingly        */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode13);
			lifacmvrec.sacstyp.set(t5645rec.sacstype13);
			lifacmvrec.glsign.set(t5645rec.sign13);
			lifacmvrec.glcode.set(t5645rec.glmap13);
			lifacmvrec.contot.set(t5645rec.cnttot13);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(isuallrec.chdrnum);
			wsaaPlan.set(isuallrec.planSuffix);
			wsaaRldgLife.set(isuallrec.life);
			wsaaRldgCoverage.set(isuallrec.coverage);
			/* Post ACMV against the Coverage, not Rider since the debt is also*/
			/* set against Coverage.                                           */
			wsaaRldgRider.set("00");
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/* Set correct substitution code                                   */
			lifacmvrec.substituteCode[6].set(covrmjaIO.getCrtable());
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode12);
			lifacmvrec.sacstyp.set(t5645rec.sacstype12);
			lifacmvrec.glsign.set(t5645rec.sign12);
			lifacmvrec.glcode.set(t5645rec.glmap12);
			lifacmvrec.contot.set(t5645rec.cnttot12);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(isuallrec.chdrnum);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifacmvrec.transactionDate.set(isuallrec.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError8000();
			return ;
		}
	}

protected void b200ReadTr52e()
	{
		b210Start();
	}

protected void b210Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		if (((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			return ;
		}
		if (((isEQ(itdmIO.getItemcoy(), isuallrec.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void b300ReadZrst()
	{
		b310Start();
	}

protected void b310Start()
	{
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)
		&& isNE(zrstIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zrstIO.getStatuz());
			syserrrec.params.set(zrstIO.getParams());
			dbError8100();
			return ;
		}
		if (isNE(zrstIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(zrstIO.getChdrnum(), covrmjaIO.getChdrnum())
		|| isNE(zrstIO.getLife(), covrmjaIO.getLife())
		|| isEQ(zrstIO.getStatuz(), varcom.endp)) {
			zrstIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstIO.getFeedbackInd(), SPACES)) {
			wsaaTotCd.add(zrstIO.getZramount01());
		}
		zrstIO.setFunction(varcom.nextr);
	}

protected void b400WriteZrst()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b410Start();
				case b400WriteTax2: 
					b400WriteTax2();
				case b490Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b410Start()
	{
		zrstIO.setParams(SPACES);
		zrstIO.setZramount01(0);
		zrstIO.setZramount02(0);
		zrstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		zrstIO.setChdrnum(covrmjaIO.getChdrnum());
		zrstIO.setLife(covrmjaIO.getLife());
		zrstIO.setJlife(SPACES);
		zrstIO.setCoverage(covrmjaIO.getCoverage());
		zrstIO.setRider(covrmjaIO.getRider());
		zrstIO.setBatctrcde(isuallrec.batctrcde);
		zrstIO.setTrandate(isuallrec.effdate);
		zrstIO.setTranno(chdrmjaIO.getTranno());
		wsaaSeqno.add(1);
		zrstIO.setXtranno(ZERO);
		zrstIO.setSeqno(wsaaSeqno);
		zrstIO.setUstmno(ZERO);
		zrstIO.setFormat(formatsInner.zrstrec);
		if (isEQ(wsaaTax[1], 0)) {
			goTo(GotoLabel.b400WriteTax2);
		}
		zrstIO.setZramount01(wsaaTax[1]);
		zrstIO.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (1)      TO ZRST-SACSTYP.         <LA4895>*/
		zrstIO.setSacstyp(txcalcrec.taxSacstyp[1]);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			dbError8100();
			goTo(GotoLabel.b490Exit);
		}
	}

protected void b400WriteTax2()
	{
		if (isEQ(wsaaTax[2], 0)) {
			return ;
		}
		zrstIO.setZramount01(wsaaTax[2]);
		zrstIO.setZramount02(wsaaTotCd);
		/*    MOVE TXCL-TAX-TYPE (2)      TO ZRST-SACSTYP.         <LA4895>*/
		zrstIO.setSacstyp(txcalcrec.taxSacstyp[2]);
		zrstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(zrstIO.getParams());
			syserrrec.statuz.set(zrstIO.getStatuz());
			dbError8100();
		}
	}

protected void b500UpdateZrst()
	{
		b510Start();
	}

protected void b510Start()
	{
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)
		&& isNE(zrstIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zrstIO.getStatuz());
			syserrrec.params.set(zrstIO.getParams());
			dbError8100();
			return ;
		}
		if (isNE(zrstIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(zrstIO.getChdrnum(), covrmjaIO.getChdrnum())
		|| isNE(zrstIO.getLife(), covrmjaIO.getLife())
		|| isEQ(zrstIO.getStatuz(), varcom.endp)) {
			zrstIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(zrstIO.getFeedbackInd(), SPACES)) {
			zrstIO.setZramount02(wsaaTotCd);
			zrstIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, zrstIO);
			if (isNE(zrstIO.getStatuz(), varcom.oK)
			&& isNE(zrstIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(zrstIO.getParams());
				syserrrec.statuz.set(zrstIO.getStatuz());
				dbError8100();
				return ;
			}
		}
		zrstIO.setFunction(varcom.nextr);
	}

protected void systemError8000()
	{
		se8000();
		seExit8090();
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		isuallrec.statuz.set(varcom.bomb);
		/*EXIT*/
	}

protected void dbError8100()
	{
					db8100();
					dbExit8190();
				}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		isuallrec.statuz.set(varcom.bomb);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(isuallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
		zrdecplrec.batctrcde.set(isuallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError8000();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
 */
private static final class WsaaAmountHoldersInner { 
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocInit = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRunInitTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaAllocAccum = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRunAccumTot = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOldAllocAccum = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOldAllocInit = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaUtrnContractAmount = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaHitrContractAmount = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaTotUlSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaTotIbSplit = new ZonedDecimalData(5, 2).init(0);
	private ZonedDecimalData wsaaContractAmount = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaInitAmount = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAccumAmount = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaAllocTot = new ZonedDecimalData(18, 2).init(0);
	private ZonedDecimalData wsaaRunTot = new ZonedDecimalData(18, 2).init(0);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData ulnkrnlrec = new FixedLengthStringData(10).init("ULNKRNLREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hitrrec = new FixedLengthStringData(10).init("HITRREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
	private FixedLengthStringData zrstrec = new FixedLengthStringData(10).init("ZRSTREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5519 = new FixedLengthStringData(5).init("T5519");
	private FixedLengthStringData t5535 = new FixedLengthStringData(5).init("T5535");
	private FixedLengthStringData t5536 = new FixedLengthStringData(5).init("T5536");
	private FixedLengthStringData t5537 = new FixedLengthStringData(5).init("T5537");
	private FixedLengthStringData t5539 = new FixedLengthStringData(5).init("T5539");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5545 = new FixedLengthStringData(5).init("T5545");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6646 = new FixedLengthStringData(5).init("T6646");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t6659 = new FixedLengthStringData(5).init("T6659");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	}
}
