package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:58
 * Description:
 * Copybook name: AGCMBCHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmbchkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmbchFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmbchKey = new FixedLengthStringData(64).isAPartOf(agcmbchFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmbchChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmbchKey, 0);
  	public FixedLengthStringData agcmbchChdrnum = new FixedLengthStringData(8).isAPartOf(agcmbchKey, 1);
  	public FixedLengthStringData agcmbchLife = new FixedLengthStringData(2).isAPartOf(agcmbchKey, 9);
  	public FixedLengthStringData agcmbchCoverage = new FixedLengthStringData(2).isAPartOf(agcmbchKey, 11);
  	public FixedLengthStringData agcmbchRider = new FixedLengthStringData(2).isAPartOf(agcmbchKey, 13);
  	public PackedDecimalData agcmbchPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmbchKey, 15);
  	public FixedLengthStringData agcmbchAgntnum = new FixedLengthStringData(8).isAPartOf(agcmbchKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(agcmbchKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmbchFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmbchFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}