package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:32
 * Description:
 * Copybook name: ACBLBK3KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acblbk3key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acblbk3FileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData acblbk3Key = new FixedLengthStringData(256).isAPartOf(acblbk3FileKey, 0, REDEFINE);
  	public FixedLengthStringData acblbk3Rldgcoy = new FixedLengthStringData(1).isAPartOf(acblbk3Key, 0);
  	public FixedLengthStringData acblbk3Rldgacct = new FixedLengthStringData(16).isAPartOf(acblbk3Key, 1);
  	public FixedLengthStringData acblbk3Sacscode = new FixedLengthStringData(2).isAPartOf(acblbk3Key, 17);
  	public FixedLengthStringData acblbk3Sacstyp = new FixedLengthStringData(2).isAPartOf(acblbk3Key, 19);
  	public FixedLengthStringData acblbk3Origcurr = new FixedLengthStringData(3).isAPartOf(acblbk3Key, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(232).isAPartOf(acblbk3Key, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acblbk3FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acblbk3FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}