package com.csc.life.contractservicing.dataaccess.dao;

import com.csc.life.contractservicing.dataaccess.model.Cvuwpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CvuwpfDAO extends BaseDAO<Cvuwpf> {
	public Cvuwpf getCvuwpfRecord(String company,String chdrnum);
}
