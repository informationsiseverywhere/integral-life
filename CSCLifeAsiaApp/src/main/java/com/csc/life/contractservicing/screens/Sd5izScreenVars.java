package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Sd5izScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(152);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(54+10).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public ZonedDecimalData mthscompadd = DD.mthscompadd.copyToZonedDecimal().isAPartOf(dataFields,44); 
	public ZonedDecimalData mthscompmod = DD.mthscompmod.copyToZonedDecimal().isAPartOf(dataFields,54); 
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 54+10);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
//	public FixedLengthStringData mthscompaddErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
//	public FixedLengthStringData mthscompmodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60+12).isAPartOf(dataArea, 70+10);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] mthscompaddOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] mthscompmodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sd5izscreenWritten = new LongData(0);
	public LongData Sd5izprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5izScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, mthscompadd, mthscompmod};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, mthscompaddOut, mthscompmodOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5izscreen.class;
		protectRecord = Sd5izprotect.class;
	}

}
