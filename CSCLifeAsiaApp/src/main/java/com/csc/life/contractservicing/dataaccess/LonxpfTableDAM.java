package com.csc.life.contractservicing.dataaccess;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.util.QPUtilities;
import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;


/**
 * 	
 * File: LoanpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:45
 * Class transformed from LOANPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LonxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 160;
	public FixedLengthStringData loanrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData loanpfRecord = loanrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(loanrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(loanrec);
	public PackedDecimalData loanNumber = DD.loannumber.copy().isAPartOf(loanrec);
	public FixedLengthStringData loanType = DD.loantype.copy().isAPartOf(loanrec);
	public FixedLengthStringData loanCurrency = DD.loancurr.copy().isAPartOf(loanrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(loanrec);
	public PackedDecimalData firstTranno = DD.ftranno.copy().isAPartOf(loanrec);
	public PackedDecimalData lastTranno = DD.ltranno.copy().isAPartOf(loanrec);
	public PackedDecimalData loanOriginalAmount = DD.loanorigam.copy().isAPartOf(loanrec);
	public PackedDecimalData loanStartDate = DD.loanstdate.copy().isAPartOf(loanrec);
	public PackedDecimalData lastCapnLoanAmt = DD.lstcaplamt.copy().isAPartOf(loanrec);
	public PackedDecimalData lastCapnDate = DD.lstcapdate.copy().isAPartOf(loanrec);
	public PackedDecimalData nextCapnDate = DD.nxtcapdate.copy().isAPartOf(loanrec);
	public PackedDecimalData lastIntBillDate = DD.lstintbdte.copy().isAPartOf(loanrec);
	public PackedDecimalData nextIntBillDate = DD.nxtintbdte.copy().isAPartOf(loanrec);	
	//public PackedDecimalData lonuniquenumber = CustomDD.LONUniquenumber.copy().isAPartOf(loanrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LonxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for LoanpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LonxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LoanpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LonxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LoanpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LonxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LOANPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LOANNUMBER, " +
							"LOANTYPE, " +
							"LOANCURR, " +
							"VALIDFLAG, " +
							"FTRANNO, " +
							"LTRANNO, " +
							"LOANORIGAM, " +
							"LOANSTDATE, " +
							"LSTCAPLAMT, " +
							"LSTCAPDATE, " +
							"NXTCAPDATE, " +
							"LSTINTBDTE, " +
							"NXTINTBDTE, " +						
							//"LONUNIQUENUMBER,"+	
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     loanNumber,
                                     loanType,
                                     loanCurrency,
                                     validflag,
                                     firstTranno,
                                     lastTranno,
                                     loanOriginalAmount,
                                     loanStartDate,
                                     lastCapnLoanAmt,
                                     lastCapnDate,
                                     nextCapnDate,
                                     lastIntBillDate,
                                     nextIntBillDate,                                    
                                     //lonuniquenumber,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		loanNumber.clear();
  		loanType.clear();
  		loanCurrency.clear();
  		validflag.clear();
  		firstTranno.clear();
  		lastTranno.clear();
  		loanOriginalAmount.clear();
  		loanStartDate.clear();
  		lastCapnLoanAmt.clear();
  		lastCapnDate.clear();
  		nextCapnDate.clear();
  		lastIntBillDate.clear();
  		nextIntBillDate.clear();
  		//lonuniquenumber.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLoanrec() {
  		return loanrec;
	}

	public FixedLengthStringData getLoanpfRecord() {
  		return loanpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLoanrec(what);
	}

	public void setLoanrec(Object what) {
  		this.loanrec.set(what);
	}

	public void setLoanpfRecord(Object what) {
  		this.loanpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(loanrec.getLength());
		result.set(loanrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}