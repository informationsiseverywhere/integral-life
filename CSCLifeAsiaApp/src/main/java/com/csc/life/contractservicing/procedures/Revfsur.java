/*
 * File: Revfsur.java
 * Date: 30 August 2009 2:07:50
 * Author: Quipoz Limited
 *
 * Class transformed from REVFSUR.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.List;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrvsTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.dataaccess.RtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
*  REVFSUR - Full Surrender Reversal.
*  ----------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*
*  REVERSE SURRENDER CLAIM HEADER
*
*  Only reverse the contract if reversing from a Full Surrender.
*
*       READH on the Surrender Claim Header record (SURHCLM) for
*            the relevant contract.
*
*       Store the transaction number.
*
*       DELET this record.
*
*  REVERSE CONTRACT HEADER
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*            relevant contract. Add 1 to the tranno and store.
*
*       DELET this record.
*
*       NEXTR on  the  Contract  Header record (CHDRLIF) for the
*            relevant  contract.  This should read a record with
*            valid flag of '2', if not, then this is an error.
*
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*            altering  the  valid  flag from '2' to '1' and with
*            the  stored  tranno  as  this  is to re-instate the
*            contract prior to the surrender.
*
*  REVERSE PAYR
*
*       READH on  the Payr file  (use PAYRLIF to select all
*            records).  The Payr records are arranged LIFO so
*            this will retrieve the validflag 1 record.
*
*       DELET this record.
*
*       READH on  the Payr file again (CHDRLIF) where this will
*            retrieve the next most recent record (the last
*            validflag 2).
*
*       REWRT this payr record with a validflag 1.  The tranno
*            will be updated in the generic reversal subroutine
*            REVGENAT.
*
*  REVERSE LIVES
*
*       If the  entire  Plan  was surrendered, then the LIFE has
*            had a status change and its valid flag changed from
*            "1" to "2".
*
*       READH on  the  Life  details  record  (LIFE)     for the
*            relevant  contract  and  the  life  from  the  SURH
*            record.
*
*       DELET this record.
*
*       NEXTR on the Life details record (LIFE)    should read a
*            record with valid flag of '2', if not, then this is
*            an error.
*
*       REWRT this  Life details record (LIFE)    after altering
*            the  valid  flag  from  '2'  to  '1'  as this is to
*            re-instate the life prior to the surrender.
*
*       Skip over all other valid flag '2' records until a valid
*            flag  '1'  record  is found for each coverage/rider
*            reinstated, look up generic subroutine on T5671 and
*            call if not blank.
*
*  REVERSE COMPONENTS.
*
*  Read all  the Coverage/Rider records (COVR) for this contract
*  and update each COVR record as follows:
*
*       READH on  the  COVR  records  (COVR)    for the relevant
*            contract.
*
*       DELET this record.
*
*       NEXTR on  the  COVR  file (COVR)    should read a record
*            with  valid  flag  of  '2', if not, then this is an
*            error.
*
*       REWRT this  COVR  detail record (COVR)    after altering
*            the  valid  flag  from  '2'  to  '1'  as this is to
*            re-instate the coverage prior to the surrender.
*
*
*  REVERSE CLAIM DETAIL RECORDS
*
*       READH on  the  Surrender  Claim details record (SURDCLM)
*            for the relevant contract.
*
*       DELET all records.
*
*
*  REVERSAL OF UTRNS
*
*       Call the T5671 generic subroutine to reverse Unit
*       transactions for the TRANNO being reversed.
*       The subroutine GREVUTRN should be entered in the
*       relevant table entry.
*
*
*  CONTRACT REVERSAL ACCOUNTING
*
*       Do a BEGNH on the ACMV file for this transaction no. and
*       call LIFACMV to post the reversal to the sub-account.
*
*
*  REVERSE PAID OFF LOANS
*  (Reinstate LOANs if paid off at surrender)
*
*       Do a BEGNH on the LOANENQ file for the tranno being
*        reversed and see if any Loans ( contract or APLs )
*        were paid off during the Death Claim processing
*        - check this by comparing the Last-Tranno field with
*          the tranno we are trying to reverse now
*        - if so, we must re-instate the Loans affected
*        Set the LOANENQ record Validflags back to '1' and
*         zeroise the Last-Tranno field
*
*
*****************************************************
* </pre>
*/
public class Revfsur extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVFSUR";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaStoredSurhTranno = new ZonedDecimalData(5, 0);
		/* WSAA-SUBSCRIPTS */
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaT5671 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 4);
	private PackedDecimalData wsaaReversedTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private PackedDecimalData wsaaLstInterestDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNxtInterestDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";

	private FixedLengthStringData wsaaAcmvTrcde = new FixedLengthStringData(4).init(SPACES);
	private Validator loanInterestBilling = new Validator(wsaaAcmvTrcde, "BA69");
	private Validator loanInterestCapital = new Validator(wsaaAcmvTrcde, "BA68");
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t6633 = "T6633";
		/* ERRORS */
	private static final String e723 = "E723";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private AgcmrvsTableDAM agcmrvsIO = new AgcmrvsTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private RtrnrevTableDAM rtrnrevIO = new RtrnrevTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private T5645rec t5645rec = new T5645rec();
	private T6633rec t6633rec = new T6633rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Greversrec greversrec = new Greversrec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	private HdivpfDAO hdivpfDAO = getApplicationContext().getBean("hdivpfDAO", HdivpfDAO.class);
	private Hdivpf hdivpf = null;
	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextAcmv4650,
		callAgcmrvsio7020,
		findRecordToReverse7120
	}

	public Revfsur() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		/* Get todays date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

protected void process2000()
	{
		process2001();
	}

protected void process2001()
	{
		reverseContractHeader2100();
		reversePayr2200();
		reverseClaimsHeader2300();
		if (isEQ(reverserec.planSuffix, 0)) {
			processLifeRecords2700();
		}
		processComponents3300();
		reverseClaimDetails4100();
		processAcvms4500();
		processAcmvOptical4510();
		checkLoans5000();
		processRtrns6000();
		processAgcms7000();
		deleteTax8000();
		processHdiv9000();
	}

protected void reverseContractHeader2100()
	{
		readContractHeader2101();
	}

protected void readContractHeader2101()
	{
		/*  delete valid flag 1 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getValidflag(), 1)) {
			syserrrec.params.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setFunction("DELET");
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		chdrlifIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getValidflag(), 2)) {
			syserrrec.params.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setValidflag("1");
		chdrlifIO.setFunction("UPDAT");
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
	}

protected void reversePayr2200()
	{
		start2210();
	}

protected void start2210()
	{
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setFunction(varcom.delet);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setValidflag("1");
		payrlifIO.setFunction(varcom.rewrt);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
	}

protected void reverseClaimsHeader2300()
	{
		readContractHeader2301();
	}

protected void readContractHeader2301()
	{
		surhclmIO.setDataArea(SPACES);
		surhclmIO.setChdrcoy(reverserec.company);
		surhclmIO.setChdrnum(reverserec.chdrnum);
		wsaaStoredSurhTranno.set(reverserec.tranno);
		surhclmIO.setPlanSuffix(reverserec.planSuffix);
		surhclmIO.setTranno(wsaaStoredSurhTranno);
		surhclmIO.setFunction("READH");
		surhclmIO.setFormat(formatsInner.surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			xxxxFatalError();
		}
		surhclmIO.setFunction("DELET");
		surhclmIO.setFormat(formatsInner.surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			xxxxFatalError();
		}
	}

protected void processLifeRecords2700()
	{
		lives2701();
	}

protected void lives2701()
	{
		lifeIO.setParams(SPACES);
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setChdrcoy(reverserec.company);
		lifeIO.setFormat(formatsInner.liferec);
		/* MOVE 'BEGNH'                TO LIFE-FUNCTION.                */
		lifeIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(), varcom.oK))
		&& (isNE(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(lifeIO.getStatuz(), varcom.endp))
		|| (isNE(lifeIO.getChdrnum(), chdrlifIO.getChdrnum()))
		|| (isNE(reverserec.company, lifeIO.getChdrcoy()))) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		if (isNE(lifeIO.getValidflag(), "1")) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		while ( !(isEQ(lifeIO.getStatuz(), varcom.endp))) {
			deleteAndUpdateLife2800();
		}

	}

protected void deleteAndUpdateLife2800()
	{
		deleteRecs2805();
	}

protected void deleteRecs2805()
	{
		/* valid flag equal to 1*/
		lifeIO.setFunction(varcom.readh);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		lifeIO.setFunction(varcom.delet);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		lifeIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(lifeIO.getStatuz(), varcom.endp))
		|| (isNE(lifeIO.getChdrnum(), chdrlifIO.getChdrnum()))
		|| (isNE(reverserec.company, lifeIO.getChdrcoy()))) {
			lifeIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(lifeIO.getValidflag(), "2")) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		/* MOVE REVE-TRANS-DATE        TO LIFE-TRANSACTION-DATE         */
		/* MOVE REVE-TRANS-TIME        TO LIFE-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO LIFE-USER.                    */
		/* MOVE REVE-TERMID            TO LIFE-TERMID.                  */
		lifeIO.setValidflag("1");
		lifeIO.setCurrto(varcom.vrcmMaxDate);
		lifeIO.setFunction("UPDAT");
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		lifeIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(), varcom.oK))
		&& (isNE(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(lifeIO.getStatuz(), varcom.endp))
		|| (isNE(lifeIO.getChdrnum(), chdrlifIO.getChdrnum()))
		|| (isNE(reverserec.company, lifeIO.getChdrcoy()))) {
			lifeIO.setStatuz(varcom.endp);
			return ;
		}
		while ( !(isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isEQ(lifeIO.getValidflag(), 1))) {
			getNextLife2900();
		}

	}

protected void getNextLife2900()
	{
		/*GET-RECS*/
		lifeIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(), varcom.oK))
		&& (isNE(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(lifeIO.getStatuz(), varcom.endp))
		|| (isNE(lifeIO.getChdrnum(), chdrlifIO.getChdrnum()))
		|| (isNE(reverserec.company, lifeIO.getChdrcoy()))) {
			lifeIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processComponents3300()
	{
		covers3301();
	}

protected void covers3301()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrnum(chdrlifIO.getChdrnum());
		covrIO.setPlanSuffix(reverserec.planSuffix);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setTranno(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		/* MOVE 'BEGNH'                TO COVR-FUNCTION.                */
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isEQ(covrIO.getTranno(), reverserec.tranno)))) {
			SmartFileCode.execute(appVars, covrIO);
			if ((isNE(covrIO.getStatuz(), varcom.oK))
			&& (isNE(covrIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(covrIO.getParams());
				xxxxFatalError();
			}
			if ((isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum()))
			|| (isNE(covrIO.getChdrcoy(), reverserec.company))) {
				covrIO.setStatuz(varcom.endp);
				syserrrec.params.set(covrIO.getParams());
				xxxxFatalError();
			}
			covrIO.setFunction(varcom.nextr);
		}

		if (isNE(covrIO.getValidflag(), "1")) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			deleteAndUpdatComp3400();
		}

	}

protected void deleteAndUpdatComp3400()
	{
		deleteRecs3405();
	}

protected void deleteRecs3405()
	{
		/* valid flag equal to 1*/
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		/* Check for a increase record created by the forward           */
		/* transaction and delete it if one exists.                     */
		checkIncr3600();
		readGenericProcTab4400();
		covrIO.setFunction(varcom.delet);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(), varcom.oK))
		&& (isNE(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum()))
		|| (isNE(covrIO.getChdrcoy(), reverserec.company))) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(covrIO.getValidflag(), "2")) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		/* MOVE REVE-TRANS-DATE        TO COVR-TRANSACTION-DATE         */
		/* MOVE REVE-TRANS-TIME        TO COVR-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO COVR-USER.                    */
		/* MOVE REVE-TERMID            TO COVR-TERMID.                  */
		covrIO.setValidflag("1");
		/* MOVE REVE-NEW-TRANNO        TO COVR-TRANNO.                  */
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction("UPDAT");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(), varcom.oK))
		&& (isNE(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum()))
		|| (isNE(reverserec.company, covrIO.getChdrcoy()))) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		while ( !((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isEQ(covrIO.getValidflag(), 1))
		&& (isEQ(covrIO.getTranno(), reverserec.tranno)))) {
			getNextCovr3500();
		}

	}

protected void getNextCovr3500()
	{
		/*GET-RECS*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(), varcom.oK))
		&& (isNE(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum()))
		|| (isNE(reverserec.company, covrIO.getChdrcoy()))) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkIncr3600()
	{
		readh3610();
	}

protected void readh3610()
	{
		/* Look for a historic INCR record for the forward transaction. */
		/* If one exists for this component, delete it, thereby         */
		/* reinstating the previous record.                             */
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(covrIO.getChdrcoy());
		incrselIO.setChdrnum(covrIO.getChdrnum());
		incrselIO.setLife(covrIO.getLife());
		incrselIO.setCoverage(covrIO.getCoverage());
		incrselIO.setRider(covrIO.getRider());
		incrselIO.setPlanSuffix(covrIO.getPlanSuffix());
		incrselIO.setTranno(reverserec.tranno);
		incrselIO.setFunction(varcom.readh);
		incrselIO.setFormat(formatsInner.incrselrec);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)
		&& isNE(incrselIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(incrselIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrselIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
	}

protected void reverseClaimDetails4100()
	{
		/*PROCESS-CLAIMS*/
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(reverserec.company);
		surdclmIO.setChdrnum(chdrlifIO.getChdrnum());
		surdclmIO.setPlanSuffix(reverserec.planSuffix);
		surdclmIO.setTranno(wsaaStoredSurhTranno);
		/* MOVE BEGNH                  TO SURDCLM-FUNCTION.             */
		surdclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");
		surdclmIO.setFormat(formatsInner.surdclmrec);
		while ( !(isEQ(surdclmIO.getStatuz(), varcom.endp))) {
			reverseClaims4300();
		}

		/*EXIT*/
	}

protected void reverseClaims4300()
	{
		update4301();
	}

protected void update4301()
	{
		SmartFileCode.execute(appVars, surdclmIO);
		if ((isNE(surdclmIO.getStatuz(), varcom.oK))
		&& (isNE(surdclmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(surdclmIO.getStatuz());
			xxxxFatalError();
		}
		if ((isEQ(surdclmIO.getStatuz(), varcom.endp))
		|| (isNE(chdrlifIO.getChdrnum(), surdclmIO.getChdrnum()))
		|| (isNE(reverserec.company, surdclmIO.getChdrcoy()))
		|| (isNE(wsaaStoredSurhTranno, surdclmIO.getTranno()))) {
			surdclmIO.setStatuz(varcom.endp);
			return ;
		}
		/* MOVE 'DELET'                TO SURDCLM-FUNCTION.             */
		surdclmIO.setFunction(varcom.deltd);
		surdclmIO.setFormat(formatsInner.surdclmrec);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surdclmIO.getStatuz());
			xxxxFatalError();
		}
		surdclmIO.setFunction(varcom.nextr);
	}

protected void readGenericProcTab4400()
	{
		readStatusCodes4401();
	}

protected void readStatusCodes4401()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Batc.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), SPACES)||isEQ(itemIO.getStatuz(), varcom.mrnf) ) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(wsaaStoredSurhTranno);
		/* MOVE REVE-PLAN-SUFFIX       TO GREV-PLAN-SUFFIX.             */
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.life.set(covrIO.getLife());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callSubprog4450();
		}
	}

protected void callSubprog4450()
	{
		/*GO*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(greversrec.statuz);
				xxxxFatalError();
			}
		}
		/*EXIT*/
	}

protected void processAcvms4500()
	{
		readSubAcctTab4501();
	}

protected void readSubAcctTab4501()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(surhclmIO.getChdrnum());
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		acmvrevIO.setBatctrcde(SPACES);
		while ( !((isEQ(acmvrevIO.getStatuz(), varcom.endp))
		|| (isEQ(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)))) {
			SmartFileCode.execute(appVars, acmvrevIO);
			if ((isNE(acmvrevIO.getStatuz(), varcom.oK))
			&& (isNE(acmvrevIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
			if ((isNE(acmvrevIO.getRldgcoy(), reverserec.company))
			|| (isNE(acmvrevIO.getRdocnum(), surhclmIO.getChdrnum()))
			|| (isNE(acmvrevIO.getTranno(), reverserec.tranno))) {
				acmvrevIO.setStatuz(varcom.endp);
			}
			acmvrevIO.setFunction(varcom.nextr);
		}

		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs4600();
		}

	}

protected void processAcmvOptical4510()
	{
		start4510();
	}

protected void start4510()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			xxxxFatalError();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs4600();
		}

		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void reverseAcmvRecs4600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readAcmv4601();
				case nextAcmv4650:
					nextAcmv4650();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readAcmv4601()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		/*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.                  */
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		/* MOVE ZERO                   TO LIFA-JRNSEQ.                  */
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		//lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());//ILIFE-5863
		lifacmvrec.origcurr.set(surhclmIO.getCurrcd());   //ILIFE-5863
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(wsaaStoredSurhTranno);
		descIO.setDescitem(wsaaStoredSurhTranno);
		getDescription4800();
		/*    MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE '99999999'             TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void nextAcmv4650()
	{
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'         USING ACMVREV-PARAMS.               */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if ((isNE(acmvrevIO.getStatuz(), varcom.oK))
		&& (isNE(acmvrevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if ((isEQ(acmvrevIO.getStatuz(), varcom.endp))
		|| (isNE(acmvrevIO.getRldgcoy(), reverserec.company))
		|| (isNE(acmvrevIO.getRdocnum(), surhclmIO.getChdrnum()))) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			goTo(GotoLabel.nextAcmv4650);
		}
	}

protected void getDescription4800()
	{
		para4801();
	}

protected void para4801()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		/*MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			xxxxFatalError();
		}
		/*MOVE DESC-SHORTDESC         TO LIFA-TRANDESC.           <004>*/
		/*MOVE DESC-LONGDESC          TO WSAA-REV-TRANDESC.       <004>*/
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaRevTrandesc.fill("?");
			lifacmvrec.trandesc.fill("?");
		}
		else {
			wsaaRevTrandesc.set(descIO.getLongdesc());
			lifacmvrec.trandesc.set(descIO.getLongdesc());
			/*****   MOVE DESC-SHORTDESC       TO LIFA-TRANDESC         <CAS1.0>*/
		}
	}

protected void checkLoans5000()
	{
		/*START*/
		loanenqIO.setParams(SPACES);
		loanenqIO.setChdrcoy(reverserec.company);
		loanenqIO.setChdrnum(reverserec.chdrnum);
		loanenqIO.setLoanNumber(ZERO);
		/* MOVE BEGNH                  TO LOANENQ-FUNCTION.     <LA3993>*/
		loanenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loanenqIO.setFormat(formatsInner.loanenqrec);
		while ( !(isEQ(loanenqIO.getStatuz(), varcom.endp))) {
			processLoans5100();
		}

		/*EXIT*/
	}

protected void processLoans5100()
	{
		start5100();
	}

protected void start5100()
	{
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(), varcom.oK)
		&& isNE(loanenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			xxxxFatalError();
		}
		/* If no Loan records found for this contract, then  EXIT section  */
		if (isNE(reverserec.company, loanenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, loanenqIO.getChdrnum())
		|| isEQ(loanenqIO.getStatuz(), varcom.endp)) {
			loanenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* get here, so we have found a Loan record.......                 */
		/*  does it need changing ???                                      */
		/*  check this by comparing the Last Tranno on the Loan record     */
		/* IF it matches, we will update record, otherwise we will NEXTR   */
		/*    IF LOANENQ-LAST-TRANNO      = WSAA-REVERSED-TRANNO      <002>*/
		if (isEQ(loanenqIO.getLastTranno(), reverserec.tranno)) {
			obtainInterestDates5200();
			loanenqIO.setLastIntBillDate(wsaaLstInterestDate);
			loanenqIO.setNextIntBillDate(wsaaNxtInterestDate);
			loanenqIO.setValidflag("1");
			loanenqIO.setLastTranno(ZERO);
			/*     MOVE REWRT              TO LOANENQ-FUNCTION      <LA3993>*/
			loanenqIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, loanenqIO);
			if (isNE(loanenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(loanenqIO.getParams());
				syserrrec.statuz.set(loanenqIO.getStatuz());
				xxxxFatalError();
			}
		}
		loanenqIO.setFunction(varcom.nextr);
	}

protected void obtainInterestDates5200()
	{
		start5200();
	}

protected void start5200()
	{
		readT5645Table5300();
		/* Use entries on the T5645 record read to get Loan interest       */
		/*  ACMVs for this loan number.                                    */
		if (isEQ(loanenqIO.getLoanType(), "P")) {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstype.set(t5645rec.sacstype01);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstype.set(t5645rec.sacstype02);
		}
		wsaaRldgacct.set(SPACES);
		wsaaLstInterestDate.set(ZERO);
		wsaaChdrnum.set(loanenqIO.getChdrnum());
		wsaaLoanNumber.set(loanenqIO.getLoanNumber());
		/*  We will begin reading on the ACMV original surrender           */
		/*  transactions and read backwards to get the movements           */
		/*  before the original surrender (nextp)                          */
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(surhclmIO.getChdrnum());
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			searchAcmvRecs5400();
		}

		/*  if any interest records for the loan being processed           */
		/*  (before the surrender movements) i.e there is a value          */
		/*  in WSAA-LST-INTEREST-DATE use this otherwise no interest       */
		/*  has been calculated since the loan start date                  */
		if (isEQ(wsaaLstInterestDate, ZERO)) {
			wsaaLstInterestDate.set(loanenqIO.getLoanStartDate());
		}
		/*  calculate next interest date from the last interest            */
		/*  date retrieved (i.e the effective date on the relevant         */
		/*  acmv                                                           */
		readT6633Table5500();
		calcNextInterestDate5600();
	}

protected void readT5645Table5300()
	{
		start5300();
	}

protected void start5300()
	{
		/*  Read the accounting rules table                                */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(loanenqIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void searchAcmvRecs5400()
	{
		start5400();
	}

protected void start5400()
	{
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(acmvrevIO.getRldgcoy(), loanenqIO.getChdrcoy())
		|| isNE(acmvrevIO.getRdocnum(), loanenqIO.getChdrnum())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		/*  Read the accounting rules table                                */
		wsaaAcmvTrcde.set(acmvrevIO.getBatctrcde());
		if (isLT(acmvrevIO.getTranno(), reverserec.tranno)
		&& isEQ(acmvrevIO.getSacscode(), wsaaSacscode)
		&& isEQ(acmvrevIO.getSacstyp(), wsaaSacstype)
		&& isEQ(acmvrevIO.getRldgacct(), wsaaRldgacct)
		&& (loanInterestBilling.isTrue()
		|| loanInterestCapital.isTrue())) {
			wsaaLstInterestDate.set(acmvrevIO.getEffdate());
			acmvrevIO.setStatuz(varcom.endp);
		}
		else {
			acmvrevIO.setFunction(varcom.nextp);
		}
	}

protected void readT6633Table5500()
	{
		start5500();
	}

protected void start5500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(loanenqIO.getChdrcoy());
		itdmIO.setItemtabl(t6633);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(wsaaLstInterestDate);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), loanenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6633)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e723);
			xxxxFatalError();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
	}

protected void calcNextInterestDate5600()
	{
		start5600();
	}

protected void start5600()
	{
		/* Check the interest  details on T6633 in the following           */
		/*  order: i) Calculate interest on Loan anniv ... Y/N             */
		/*        ii) Calculate interest on Policy anniv.. Y/N             */
		/*       iii) Check Int freq & whether a specific Day is chosen    */
		wsaaLoanDate.set(loanenqIO.getLoanStartDate());
		wsaaEffdate.set(wsaaLstInterestDate);
		wsaaContractDate.set(chdrlifIO.getOccdate());
		/* Check for loan anniversary flag set                             */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next loan anniv  */
		/*     date after the Effective date we are using now.             */
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(loanenqIO.getLoanStartDate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
				callDatcon45700();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}

			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Check for contract anniversary flag set                         */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next contract    */
		/*     anniversary date after the Effective date we are using now. */
		if (isEQ(t6633rec.policyAnnivInterest, "Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(chdrlifIO.getOccdate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
				callDatcon45700();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}

			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Get here so the next interest calc. date isn't based on loan    */
		/*  or contract anniversarys.                                      */
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified     */
		/* ...if not, use the Loan day                                     */
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet5800();
		}
		else {
			if (isNE(t6633rec.interestDay, ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate2.set(ZERO);
		datcon4rec.intDate1.set(wsaaNewDate);
		datcon4rec.freqFactor.set(1);
		/* check if table T6633 has a fixed frequency for interest calcs,  */
		/* ...if not, use 1 year as the default interest calc. frequency.  */
		if (isEQ(t6633rec.interestFrequency, SPACES)) {
			datcon4rec.frequency.set("01");
		}
		else {
			datcon4rec.frequency.set(t6633rec.interestFrequency);
		}
		while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
			callDatcon45700();
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}

		wsaaNxtInterestDate.set(datcon4rec.intDate2);
	}

protected void callDatcon45700()
	{
		/*START*/
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void dateSet5800()
	{
		/*START*/
		/* we have to check that the date we are going to call Datcon4     */
		/*  with is a valid from-date... ie IF the interest/capn day in    */
		/*  T6633 is > 28, we have to make sure the from-date isn't        */
		/*  something like 31/02/nnnn or 31/06/nnnn                        */
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
	}

protected void processRtrns6000()
	{
		start6001();
	}

protected void start6001()
	{
		rtrnrevIO.setParams(SPACES);
		rtrnrevIO.setRldgcoy(reverserec.company);
		rtrnrevIO.setRdocnum(reverserec.chdrnum);
		rtrnrevIO.setTranno(reverserec.tranno);
		rtrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rtrnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(rtrnrevIO.getStatuz(), varcom.endp))) {
			reverseRtrns6100();
		}

	}

protected void reverseRtrns6100()
	{
		start6100();
		readNextRtrn6180();
	}

protected void start6100()
	{
		if (isNE(rtrnrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(ZERO);
		/*                                LIFR-JRNSEQ.                  */
		lifrtrnrec.jrnseq.set(rtrnrevIO.getTranseq());
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(reverserec.batchkey);
		lifrtrnrec.rdocnum.set(rtrnrevIO.getRdocnum());
		lifrtrnrec.tranno.set(reverserec.newTranno);
		lifrtrnrec.tranref.set(reverserec.newTranno);
		lifrtrnrec.sacscode.set(rtrnrevIO.getSacscode());
		lifrtrnrec.sacstyp.set(rtrnrevIO.getSacstyp());
		lifrtrnrec.glcode.set(rtrnrevIO.getGlcode());
		lifrtrnrec.glsign.set(rtrnrevIO.getGlsign());
		lifrtrnrec.rldgcoy.set(rtrnrevIO.getRldgcoy());
		lifrtrnrec.genlcoy.set(rtrnrevIO.getGenlcoy());
		lifrtrnrec.origcurr.set(rtrnrevIO.getOrigccy());
		lifrtrnrec.rldgacct.set(rtrnrevIO.getRldgacct());
		compute(lifrtrnrec.acctamt, 2).set(mult(rtrnrevIO.getAcctamt(), -1));
		compute(lifrtrnrec.origamt, 2).set(mult(rtrnrevIO.getOrigamt(), -1));
		lifrtrnrec.genlcur.set(rtrnrevIO.getGenlcur());
		lifrtrnrec.crate.set(rtrnrevIO.getCrate());
		lifrtrnrec.postyear.set(SPACES);
		lifrtrnrec.postmonth.set(SPACES);
		lifrtrnrec.trandesc.set(wsaaRevTrandesc);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE REVE-EFFDATE-2         TO LIFR-EFFDATE.            <004>*/
		/* MOVE WSAA-TODAY             TO LIFR-EFFDATE.                 */
		lifrtrnrec.effdate.set(rtrnrevIO.getEffdate());
		lifrtrnrec.termid.set(reverserec.termid);
		lifrtrnrec.user.set(reverserec.user);
		lifrtrnrec.transactionTime.set(reverserec.transTime);
		lifrtrnrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserrrec.statuz.set(lifrtrnrec.statuz);
			xxxxFatalError();
		}
	}

protected void readNextRtrn6180()
	{
		/*  Read the next RTRN record.                                     */
		rtrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAgcms7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start7010();
				case callAgcmrvsio7020:
					callAgcmrvsio7020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7010()
	{
		/* Read the first AGCM record for this contract and tranno.        */
		agcmrvsIO.setParams(SPACES);
		agcmrvsIO.setChdrcoy(reverserec.company);
		agcmrvsIO.setChdrnum(reverserec.chdrnum);
		agcmrvsIO.setTranno(reverserec.tranno);
		agcmrvsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrvsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		agcmrvsIO.setFormat(formatsInner.agcmrvsrec);
	}

protected void callAgcmrvsio7020()
	{
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(), varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		/* If the end of the AGCMRVS file has been reached or a            */
		/* record was read but not for the contract/tranno being           */
		/* processed, then exit the section.                               */
		if (isEQ(agcmrvsIO.getStatuz(), varcom.endp)
		|| isNE(agcmrvsIO.getChdrcoy(), reverserec.company)
		|| isNE(agcmrvsIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(agcmrvsIO.getTranno(), reverserec.tranno)) {
			return ;
		}
		updateAgcmFile7100();
		/* Loop round to see if there any other AGCMRVS records to         */
		/* process for the contract/tranno.                                */
		agcmrvsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgcmrvsio7020);
	}

protected void updateAgcmFile7100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					delet7110();
				case findRecordToReverse7120:
					findRecordToReverse7120();
					rewrt7130();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* previous processing loop on AGCMRVS is not upset. Delete the
	* and reinstate the record which has valid-flag = 2.
	* </pre>
	*/
protected void delet7110()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/* then there is a problem as this file is specified as LIFO and   */
		/* the last record written would have been the valid-flag = "1".   */
		if (isNE(agcmdbcIO.getValidflag(), 1)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmdbcIO.setFunction(varcom.delet);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/*    valid flag 2 record that was changed by the original         */
		/*    surrender transaction.  If this contract has had single      */
		/*    premium top-ups or lump sums at contract issue, we may find  */
		/*    a valid flag 1 record for the key we are reading.  Ignore    */
		/*    these records and continue the quest for the valid flag 2    */
		/*    record.                                                      */
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmdbcIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void findRecordToReverse7120()
	{
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getChdrcoy(), agcmrvsIO.getChdrcoy())
		|| isNE(agcmdbcIO.getChdrnum(), agcmrvsIO.getChdrnum())
		|| isNE(agcmdbcIO.getAgntnum(), agcmrvsIO.getAgntnum())
		|| isNE(agcmdbcIO.getLife(), agcmrvsIO.getLife())
		|| isNE(agcmdbcIO.getCoverage(), agcmrvsIO.getCoverage())
		|| isNE(agcmdbcIO.getRider(), agcmrvsIO.getRider())
		|| isNE(agcmdbcIO.getPlanSuffix(), agcmrvsIO.getPlanSuffix())
		|| isNE(agcmdbcIO.getSeqno(), agcmrvsIO.getSeqno())) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getValidflag(), "2")) {
			agcmdbcIO.setFunction(varcom.nextr);
			goTo(GotoLabel.findRecordToReverse7120);
		}
	}

protected void rewrt7130()
	{
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrfrom(wsaaToday);
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writd);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void deleteTax8000()
	{
		/*START*/
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(ZERO);
		taxdrevIO.setFormat(formatsInner.taxdrevrec);
		taxdrevIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			delTax8100();
		}

		/*EXIT*/
	}

protected void delTax8100()
	{
		start8110();
	}

protected void start8110()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(taxdrevIO.getStatuz(), varcom.endp)
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isNE(reverserec.company, taxdrevIO.getChdrcoy())) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(reverserec.company, taxdrevIO.getChdrcoy())
		&& isEQ(reverserec.chdrnum, taxdrevIO.getChdrnum())
		&& isEQ(reverserec.tranno, taxdrevIO.getTranno())) {
			taxdrevIO.setStatuz(varcom.oK);
			taxdrevIO.setFunction(varcom.deltd);
			taxdrevIO.setFormat(formatsInner.taxdrevrec);
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdrevIO.getParams());
				xxxxFatalError();
			}
		}
		taxdrevIO.setFunction(varcom.nextr);
	}
protected void processHdiv9000(){
	hdivpf = new Hdivpf();
	hdivpf.setChdrcoy(reverserec.company.toString());
	hdivpf.setChdrnum(reverserec.chdrnum.toString());
	hdivpf.setBatctrcde(wsaaT5671Batc.toString());
	List<Hdivpf> hdivpfList =  hdivpfDAO.getHdivintRecord(hdivpf);
	if(hdivpfList.isEmpty()){
		return;
	}
	else{
		hdivpfDAO.deleteHdivpfBasedOnTranno(hdivpf);
	}
}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData surhclmrec = new FixedLengthStringData(10).init("SURHCLMREC");
	private FixedLengthStringData surdclmrec = new FixedLengthStringData(10).init("SURDCLMREC");
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData loanenqrec = new FixedLengthStringData(10).init("LOANENQREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData agcmrvsrec = new FixedLengthStringData(10).init("AGCMRVSREC");
	private FixedLengthStringData agcmdbcrec = new FixedLengthStringData(10).init("AGCMREC");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData incrselrec = new FixedLengthStringData(10).init("INCRSELREC");
	private FixedLengthStringData taxdrevrec = new FixedLengthStringData(10).init("TAXDREVREC");
}
}
