package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:52
 * Description:
 * Copybook name: REVERSEREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Reverserec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData reverseRec = new FixedLengthStringData(118);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(reverseRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(reverseRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(reverseRec, 5);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(reverseRec, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(reverseRec, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(reverseRec, 17);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(reverseRec, 19);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(19).isAPartOf(reverseRec, 22);
  	public FixedLengthStringData contkey = new FixedLengthStringData(14).isAPartOf(batchkey, 0);
  	public FixedLengthStringData batcpfx = new FixedLengthStringData(2).isAPartOf(contkey, 0);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(contkey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(contkey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(contkey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(contkey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(contkey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batchkey, 14);
  	public ZonedDecimalData effdate1 = new ZonedDecimalData(8, 0).isAPartOf(reverseRec, 41).setUnsigned();
  	public ZonedDecimalData effdate2 = new ZonedDecimalData(8, 0).isAPartOf(reverseRec, 49).setUnsigned();
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(reverseRec, 57);
  	public PackedDecimalData newTranno = new PackedDecimalData(5, 0).isAPartOf(reverseRec, 60);
  	public FixedLengthStringData oldBatctrcde = new FixedLengthStringData(4).isAPartOf(reverseRec, 63);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(reverseRec, 67);
  	public PackedDecimalData transDate = new PackedDecimalData(6, 0).isAPartOf(reverseRec, 68);
  	public PackedDecimalData transTime = new PackedDecimalData(6, 0).isAPartOf(reverseRec, 72);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(reverseRec, 76).setUnsigned();
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(reverseRec, 82);
  	public PackedDecimalData ptrneff = new PackedDecimalData(8, 0).isAPartOf(reverseRec, 86);
  	public FixedLengthStringData ptrnBatchkey = new FixedLengthStringData(19).isAPartOf(reverseRec, 91);
  	public FixedLengthStringData ptrnBatcpfx = new FixedLengthStringData(2).isAPartOf(ptrnBatchkey, 0);
  	public FixedLengthStringData ptrnBatccoy = new FixedLengthStringData(1).isAPartOf(ptrnBatchkey, 2);
  	public FixedLengthStringData ptrnBatcbrn = new FixedLengthStringData(2).isAPartOf(ptrnBatchkey, 3);
  	public PackedDecimalData ptrnBatcactyr = new PackedDecimalData(4, 0).isAPartOf(ptrnBatchkey, 5);
  	public PackedDecimalData ptrnBatcactmn = new PackedDecimalData(2, 0).isAPartOf(ptrnBatchkey, 8);
  	public FixedLengthStringData ptrnBatctrcde = new FixedLengthStringData(4).isAPartOf(ptrnBatchkey, 10);
  	public FixedLengthStringData ptrnBatcbatch = new FixedLengthStringData(5).isAPartOf(ptrnBatchkey, 14);
  	public ZonedDecimalData bbldat = new ZonedDecimalData(8, 0).isAPartOf(reverseRec, 110).setUnsigned();


	public void initialize() {
		COBOLFunctions.initialize(reverseRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		reverseRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}