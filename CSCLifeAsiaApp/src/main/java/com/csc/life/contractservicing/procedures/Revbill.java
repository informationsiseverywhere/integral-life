/*
 * File: Revbill.java
 * Date: 30 August 2009 2:05:46
 * Author: Quipoz Limited
 * 
 * Class transformed from REVBILL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.general.dataaccess.BextrevTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.LinsrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.ChdrrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*       REVBILL - BILLING REVERSAL
*       --------------------------
*
*       The parameters passed to this program are set up in the
*       copybook REVERSEREC.
*
*       Read Contract Header
*       Firstly read the contract header record (CHDRPF) with the
*       logical view CHDRRNL using the company/contract key as
*       passed in linkage.
*
*       Delete Media records.
*       Read the media file (BEXTPF) with the logical view
*       BEXTREV and delete any record found from the file for
*       this company/contract.
*
*       Delete Instalment Records.
*       Now the relevant instalment records which has been
*       preiously created need to be deleted. To do this read
*       the instlament file (LINSPF) with a logical view of
*       LINSREV for the company/contract key. This view will
*       only select instalments which have not been paid. For
*       each instalment relevant to this contract.
*
*       If the instalment from date is greater than or equal to
*       the new billed to date:
*
*          Subtract the instalment amount from the outstanding
*          amount on the contract header record.
*          Delete the instalment record from the file.
*
*       Update the Contract Header.
*       Once all of the instalments are deleted amend the bill to
*       date on the contract to the new bill to date (effective
*       date 1 from linkage).
*
*          If effective date 2 in linkage is not spaces:
*
*            Move 'Y' to the billing suppress flag.
*            Move effective date 1 to the bill suppress from date
*            Move effective date 2 to the bill suppress to date.
*
*          Add one to the transaction number and rewrite the
*          record back to the database. Release the softlock on
*          the record.
*
******* THE FOLLOWING PROCESSING IS DONE IN P5150AT ********
*
*       Create a policy history record.
*       Finally a policy transaction record will need to be set
*       up as follows:
*
*           Write transaction PTRN record.
*                - contract key from contract header,
*                - transaction number from contract header,
*                - transaction effective date to today,
*                - batch key as set up earlier in the program.
*
*           WRITS call to BATCUP.
*
*****************************************************************
* </pre>
*/
public class Revbill extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVBILL";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaBillspfrm = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaBillcd = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();
		/* FORMATS */
	private static final String bextrevrec = "BEXTREVREC";
	private static final String chdrrnlrec = "CHDRRNLREC";
	private static final String payrrec = "PAYRREC";
	private static final String linsrec = "LINSREC";
	private BextrevTableDAM bextrevIO = new BextrevTableDAM();
	protected ChdrrnlTableDAM chdrrnlIO = new ChdrrnlTableDAM();
	private LinsTableDAM linsIO = new LinsTableDAM();
	protected LinsrevTableDAM linsrevIO = new LinsrevTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	protected TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Batckey wsaaBatckey = new Batckey();
	protected Varcom varcom = new Varcom();
	protected Reverserec reverserec = new Reverserec();
	private Freqcpy freqcpy = new Freqcpy();
	private PackedDecimalData wsaabillDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaabillcd = new PackedDecimalData(8, 0);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private boolean BTPRO028Permission  = false;
	private T6654rec t6654rec = new T6654rec();
	private static final String t6654 = "T6654";
	protected int intT6654Leaddays = 0;
	private static final String BTPRO036 = "BTPRO036";
	private boolean BTPRO036Permission  = false;

	public Revbill() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		wsaaBatckey.set(reverserec.batchkey);
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		/* get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		readContractHeader1000();
		readPayrFile1050();
		deleteBext1100();
		processLins1200();
		updateContractHeader1500();
		updatePayrFile1900();
	}

protected void exit090()
	{
		exitProgram();
	}

	/**
	* <pre>
	*   Read contract header
	* </pre>
	*/
protected void readContractHeader1000()
	{
		/*PARA*/
		chdrrnlIO.setParams(SPACES);
		chdrrnlIO.setChdrcoy(reverserec.company);
		chdrrnlIO.setChdrnum(reverserec.chdrnum);
		chdrrnlIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrrnlIO);
		if (isNE(chdrrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrnlIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void readPayrFile1050()
	{
		readh1060();
	}

protected void readh1060()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(reverserec.company);
		payrIO.setChdrnum(chdrrnlIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError8100();
		}
	}

	/**
	* <pre>
	*   Delete Media Records.
	* </pre>
	*/
protected void deleteBext1100()
	{
		para1100();
	}

protected void para1100()
	{
		/* Only delete the BEXT recors for this contract where the         */
		/* BTDATE is greater than the reverse date.                        */
		bextrevIO.setParams(SPACES);
		bextrevIO.setChdrcoy(reverserec.company);
		bextrevIO.setChdrnum(reverserec.chdrnum);
		bextrevIO.setBtdate(99999999);
		bextrevIO.setFormat(bextrevrec);
		bextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, bextrevIO);
		if (isNE(bextrevIO.getStatuz(),varcom.oK)
		&& isNE(bextrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(bextrevIO.getParams());
			dbError8100();
		}
		/*IF BEXTREV-STATUZ = MRNF                                     */
		/*IF BEXTREV-STATUZ           = ENDP                      <005>*/
		/*   GO TO 1190-EXIT.                                     <005>*/
		/* PERFORM 1700-DELETE-BEXT-RECS UNTIL BEXTREV-STATUZ = ENDP.<05*/
		while ( !(isEQ(bextrevIO.getStatuz(),varcom.endp)
		|| isNE(bextrevIO.getChdrcoy(),reverserec.company)
		|| isNE(bextrevIO.getChdrnum(),reverserec.chdrnum)
		|| isLT(bextrevIO.getBtdate(),reverserec.effdate1))) {
			deleteBextRecs1700();
		}
		
	}

	/**
	* <pre>
	*   Delete O/S instalments for a contract
	* </pre>
	*/
protected void processLins1200()
	{
		/*PARA*/
		linsrevIO.setParams(SPACES);
		linsrevIO.setChdrcoy(reverserec.company);
		linsrevIO.setChdrnum(reverserec.chdrnum);
		linsrevIO.setInstto(ZERO);
		linsrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		linsrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		linsrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		callLinsrevio1600();
		wsaabillDate.set(ZERO);
		wsaabillcd.set(ZERO);
		/*    Put in further checks for COMPANY or CHDRNUM. Change         */
		/*    to stop deleting LINS records for other contracts.           */
		if (isEQ(linsrevIO.getStatuz(),varcom.endp)
		|| isNE(linsrevIO.getChdrcoy(),reverserec.company)
		|| isNE(linsrevIO.getChdrnum(),reverserec.chdrnum)) {
			return ;
		}
		while ( !(isEQ(linsrevIO.getStatuz(),varcom.endp)
		|| isNE(linsrevIO.getChdrcoy(),reverserec.company)
		|| isNE(linsrevIO.getChdrnum(),reverserec.chdrnum))) {
			deleteOsInstalments1300();
		}
		
		/*EXIT*/
	}

protected void deleteOsInstalments1300()
	{
			para1300();
		}

protected void para1300()
	{
		if (isLTE(linsrevIO.getInstto(),reverserec.effdate1)) {
			linsrevIO.setFunction(varcom.nextr);
			callLinsrevio1600();
		}
		if (isNE(linsrevIO.getStatuz(),varcom.oK)) {
			return ;
		}
		if (isGT(linsrevIO.getInstto(),reverserec.effdate1)) {
			linsrevIO.setFunction(varcom.readh);
			callLinsrevio1600();
			if (isNE(linsrevIO.getStatuz(),varcom.oK)) {
				return ;
			}
			linsIO.setParams(SPACES);
			linsIO.setRrn(linsrevIO.getRrn());
			linsIO.setFunction(varcom.readd);
			linsIO.setFormat(linsrec);
			SmartFileCode.execute(appVars, linsIO);
			if (isNE(linsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(linsIO.getParams());
				dbError8100();
			}
			if(isEQ(wsaabillDate, ZERO) || isLT(linsIO.getInstfrom(), wsaabillDate)) {
			 wsaabillDate.set(linsIO.getInstfrom());
			}
			if(isEQ(wsaabillcd, ZERO) || isLT(linsIO.getBillcd(), wsaabillcd)) {
			 wsaabillcd.set(linsIO.getBillcd());
			}
			setPrecision(chdrrnlIO.getOutstamt(), 2);
			chdrrnlIO.setOutstamt(sub(chdrrnlIO.getOutstamt(),linsrevIO.getInstamt06()));
			/* Update the Outstanding Amount on the PAYR record                */
			setPrecision(payrIO.getOutstamt(), 2);
			payrIO.setOutstamt(sub(payrIO.getOutstamt(),linsrevIO.getInstamt06()));
			callDatcon21400();
			linsrevIO.setFunction(varcom.delet);
			callLinsrevio1600();
			if (isNE(linsrevIO.getStatuz(),varcom.oK)) {
				return ;
			}
			}
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(ZERO);
		taxdrevIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			deleteTaxdrev1300b();
		}
		
		/*PERFORM 1600-CALL-LINSREVIO.                              */
		/*  Begin on next record so as to release the hold on the record*/
		/*  This should position on the record after the deleted record*/
		linsrevIO.setFunction(varcom.begn);
		callLinsrevio1600();
	}

protected void deleteTaxdrev1300b()
	{
		para1300b();
	}

protected void para1300b()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(reverserec.company, taxdrevIO.getChdrcoy())
		&& isEQ(reverserec.chdrnum, taxdrevIO.getChdrnum())
		&& isEQ(linsIO.getInstfrom(), taxdrevIO.getInstfrom())
		&& isEQ(linsIO.getInstto(), taxdrevIO.getInstto())) {
			taxdrevIO.setStatuz(varcom.oK);
			taxdrevIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdrevIO.getParams());
				syserrrec.statuz.set(taxdrevIO.getStatuz());
				dbError8100();
			}
		}
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void callDatcon21400()
	{
		/*PARA*/
		datcon2rec.statuz.set(SPACES);
		datcon2rec.intDate1.set(linsrevIO.getInstto());
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			dbError8100();
		}
		/* MOVE DTC2-INT-DATE-2        TO CHDRRNL-BILLSPFROM.           */
		wsaaBillspfrm.set(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void updateContractHeader1500()
	{
	start3301();	
	para1500();
	}

protected void start3301()
{
	FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
	FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
	FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);
	
	BTPRO028Permission  = FeaConfg.isFeatureExist("2", "BTPRO028", appVars, "IT");
	if(BTPRO028Permission) {
		String key= payrIO.getBillchnl().toString().trim()+chdrrnlIO.getCnttype().toString().trim()+payrIO.getBillfreq().toString().trim();
		if(!readT6654(key)) {
			key= payrIO.getBillchnl().toString().trim().concat(chdrrnlIO.getCnttype().toString().trim()).concat("**");
			if(!readT6654(key)) {
				key= payrIO.getBillchnl().toString().trim().concat("*****");
				if(!readT6654(key)) {
					syserrrec.params.set(t6654);
					syserrrec.statuz.set("MRNF");
					dbError8100();
				}
			}
		}
	}
	else {
		wsaaT6654Item.set(SPACES);
		wsaaT6654Billchnl.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrrnlIO.getCnttype());
		List<Itempf> itemList = itemDAO.getAllitemsbyCurrency("IT",payrIO.getChdrcoy().toString(),t6654,wsaaT6654Item.toString());
		if (itemList == null || itemList.isEmpty()) {
			syserrrec.params.set(wsaaBatckey.batcBatctrcde);
			syserrrec.statuz.set("MRNF");
			dbError8100();
		}
	
		if (itemList != null && !itemList.isEmpty()) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
			return ;
		}
		wsaaT6654Cnttype.set("***");
		itemList = itemDAO.getAllitemsbyCurrency("IT",payrIO.getChdrcoy().toString(),t6654,wsaaT6654Item.toString());
		if (itemList == null || itemList.isEmpty()) {
			syserrrec.params.set(t6654);
			syserrrec.statuz.set("MRNF");
			dbError8100();
			return;
		}
		t6654rec.t6654Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
	}
	intT6654Leaddays = t6654rec.leadDays.toInt();
}

protected boolean readT6654(String key) {
	Itempf itempf = new Itempf();
	ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	itempf.setItempfx("IT");
	itempf.setItemcoy(payrIO.getChdrcoy().toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void para1500()
	{
	BTPRO036Permission = FeaConfg.isFeatureExist(reverserec.company.toString(), BTPRO036, appVars, "IT");
		if (isEQ(chdrrnlIO.getBillfreq(),freqcpy.fortnightly) && BTPRO036Permission ) {
			chdrrnlIO.setBillcd(wsaabillcd);
			chdrrnlIO.setBtdate(wsaabillDate);
			payrIO.setBillcd(wsaabillcd);
			payrIO.setBtdate(wsaabillDate);
		//	datcon2rec.datcon2Rec.set(SPACES);
			compute(datcon2rec.freqFactor, 0).set((sub(ZERO, intT6654Leaddays)));
			datcon2rec.frequency.set("DY");
			datcon2rec.intDate1.set(payrIO.getBillcd());
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				dbError8100();
			}
			payrIO.setNextdate(datcon2rec.intDate2.toInt());
		} else {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.statuz.set(SPACES);
			datcon3rec.intDate1.set(reverserec.effdate1);
			datcon3rec.intDate2.set(payrIO.getBtdate()); //PINNACLE-2948
			datcon3rec.frequency.set(chdrrnlIO.getBillfreq());
			datcon3rec.freqFactor.set(ZERO);
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				syserrrec.statuz.set(datcon3rec.statuz);
				/*MOVE DTC3-STATUZ       TO SYSR-PARAMS              <003>*/
				dbError8100();
			}
			/*    MOVE SPACES                 TO DTC2-DATCON2-REC.     <LA4308>*/
			/*    MOVE CHDRRNL-BILLCD         TO DTC2-INT-DATE-1.      <LA4308>*/
			/*    MOVE 99999999               TO DTC2-INT-DATE-2.      <LA4308>*/
			/*    MOVE CHDRRNL-BILLFREQ       TO DTC2-FREQUENCY.       <LA4308>*/
			/*    COMPUTE DTC2-FREQ-FACTOR    =  DTC3-FREQ-FACTOR * -1.<LA4308>*/
			/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC.  <LA4308>*/
			/*    IF DTC2-STATUZ          NOT =  O-K                   <LA4308>*/
			/*         MOVE DTC2-DATCON2-REC  TO SYSR-PARAMS           <LA4308>*/
			/*         MOVE DTC2-STATUZ       TO SYSR-STATUZ           <LA4308>*/
			/*MOVE DTC2-STATUZ       TO SYSR-PARAMS              <003>*/
			/*    MOVE DTC2-INT-DATE-2        TO CHDRRNL-BILLCD.       <LA4308>*/
			/*    MOVE DTC2-INT-DATE-2        TO CHDRRNL-BTDATE.       <LA4308>*/
			/* MOVE REVE-EFFDATE-1         TO CHDRRNL-BTDATE.               */
			initialize(datcon4rec.datcon4Rec);
			datcon4rec.intDate1.set(payrIO.getBillcd());
			datcon4rec.frequency.set(chdrrnlIO.getBillfreq());
			compute(datcon4rec.freqFactor, 5).set(mult(datcon3rec.freqFactor,-1));
			datcon4rec.billmonth.set(payrIO.getBillmonth());
			datcon4rec.billday.set(payrIO.getBillday());
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				dbError8100();
			}
			wsaaBillcd.set(datcon4rec.intDate2);
			initialize(datcon4rec.datcon4Rec);
			datcon4rec.intDate1.set(payrIO.getBtdate());//PINNACLE-2948//ILIFE-7854
			datcon4rec.frequency.set(chdrrnlIO.getBillfreq());
			compute(datcon4rec.freqFactor, 5).set(mult(datcon3rec.freqFactor,-1));
			datcon4rec.billmonth.set(payrIO.getDuemm());
			datcon4rec.billday.set(payrIO.getDuedd());
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				dbError8100();
			}
			chdrrnlIO.setBillcd(datcon4rec.intDate2);
			chdrrnlIO.setBtdate(datcon4rec.intDate2);
			/* Update the BILLCD and the BTDATE on the PAYR record             */
			/* MOVE DTC2-INT-DATE-2        TO PAYR-BILLCD.          <LA4308>*/
			/* MOVE DTC2-INT-DATE-2        TO PAYR-BTDATE.          <LA4308>*/
			payrIO.setBillcd(wsaaBillcd);
			payrIO.setBtdate(datcon4rec.intDate2);
			/* MOVE REVE-EFFDATE-1         TO PAYR-BTDATE.                  */
			/* Wind back PAYR-NEXTDATE the same number of instalments as       */
			/* the billing date. (PAYR-BILLCD).                                */
			/*    MOVE SPACES                 TO DTC2-DATCON2-REC.     <LA4351>*/
			/*    MOVE PAYR-NEXTDATE          TO DTC2-INT-DATE-1.      <LA4351>*/
			/*    MOVE 99999999               TO DTC2-INT-DATE-2.      <LA4351>*/
			/*    MOVE PAYR-BILLFREQ          TO DTC2-FREQUENCY.       <LA4351>*/
			/*    COMPUTE DTC2-FREQ-FACTOR    =  DTC3-FREQ-FACTOR * -1.<LA4351>*/
			/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC.  <LA4351>*/
			/*    IF DTC2-STATUZ          NOT =  O-K                   <LA4351>*/
			/*         MOVE DTC2-DATCON2-REC  TO SYSR-PARAMS           <LA4351>*/
			/*         MOVE DTC2-STATUZ       TO SYSR-STATUZ           <LA4351>*/
			/*    MOVE DTC2-INT-DATE-2        TO PAYR-NEXTDATE.        <LA4351>*/
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate1.set(payrIO.getNextdate());
			wsaaOccdate.set(payrIO.getNextdate());
			datcon4rec.billdayNum.set(wsaaOccDd);
			datcon4rec.billmonthNum.set(wsaaOccMm);
			datcon4rec.intDate2.set(99999999);
			datcon4rec.frequency.set(payrIO.getBillfreq());
			compute(datcon4rec.freqFactor, 5).set(mult(datcon3rec.freqFactor, -1));
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				dbError8100();
			}
			payrIO.setNextdate(datcon4rec.intDate2);
		}
		/* Update Billing Suppression To Date and Billing Suppression      */
		/* Flag on the PAYR record                                         */
		if (isNE(reverserec.effdate2,99999999)) {
			payrIO.setBillspto(reverserec.effdate2);
			payrIO.setBillspfrom(wsaaBillspfrm);
			payrIO.setBillsupr("Y");
		}
		if (isNE(reverserec.effdate2,99999999)) {
			chdrrnlIO.setBillspto(reverserec.effdate2);
			chdrrnlIO.setBillspfrom(wsaaBillspfrm);
			chdrrnlIO.setBillsupr("Y");
		}
		/*ADD  +1                     TO REVE-TRANNO.                  */
		/*MOVE REVE-TRANNO            TO CHDRRNL-TRANNO.               */
		chdrrnlIO.setTranno(reverserec.newTranno);
		chdrrnlIO.setFormat(chdrrnlrec);
		chdrrnlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrrnlIO);
		if (isNE(chdrrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrnlIO.getParams());
			dbError8100();
		}
	}

protected void callLinsrevio1600()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, linsrevIO);
		if (isNE(linsrevIO.getStatuz(),varcom.oK)
		&& isNE(linsrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(linsrevIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.company,linsrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,linsrevIO.getChdrnum())) {
			linsrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void deleteBextRecs1700()
	{
		/*PARA*/
		/* IF (REVE-COMPANY        NOT = BEXTREV-CHDRCOY) OR       <005>*/
		/*    (REVE-CHDRNUM        NOT = BEXTREV-CHDRNUM)          <005>*/
		/*    MOVE NEXTR               TO BEXTREV-FUNCTION         <005>*/
		/*    PERFORM 1800-CALL-BEXTREVIO                          <005>*/
		/*    IF BEXTREV-STATUZ        = ENDP                      <005>*/
		/*       GO TO 1790-EXIT.                                  <005>*/
		bextrevIO.setFunction(varcom.readh);
		callBextrevio1800();
		bextrevIO.setFunction(varcom.delet);
		callBextrevio1800();
		/* MOVE BEGN                   TO BEXTREV-FUNCTION.        <005>*/
		bextrevIO.setFunction(varcom.nextr);
		callBextrevio1800();
		/*EXIT*/
	}

protected void callBextrevio1800()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, bextrevIO);
		if ((isNE(bextrevIO.getStatuz(),varcom.oK))
		&& (isNE(bextrevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(bextrevIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void updatePayrFile1900()
	{
		/*REWRT*/
		/* Update the PAYR record with any changes that may have           */
		/* made.                                                           */
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void systemError8000()
	{
			se8000();
			seExit8090();
		}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
			db8100();
			dbExit8190();
		}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}
}
