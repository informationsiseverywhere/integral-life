/*
 * File: Pr579.java
 * Date: 30 August 2009 1:46:19
 * Author: Quipoz Limited
 * 
 * Class transformed from PR579.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Undactncpy;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.screens.Sr579ScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtunlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Centre;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*       COMPONENT ADD - SINGLE PREMIUM UNIT-LINKED GENERIC.
*
* This program will be invoked for both Single Premium Component
* Add-ons and Single Premium Component Enquirys.
* This is a new program which was "Cloned from program P5151",
* Standard Unit Linked Generic Entry Screen.
* It will specifically cater for the Addition and Enquiry of a
* Single Premium Component.
*
* INITIALISE (1000- SECTION)
*
* Skip this section if returning from a program further down the
* stack, (current stack position action flag = '*').
*
* Perform a RETRV on the COVRMJA I/O module. This will return
* the first COVRMJA record on the selected policy.
*
* The details of the contract being processed will be stored in
* the CHDRMJA I/O module. Retrieve the details user RETRV.
*
* Retrive the PAYR to get the Billing details.
*
* Get the following descriptions and names:
*    - Component Long description from table (T5687).
*    - Coverage/Rider details            (T5687)
*    - Premium calculation subroutine    (T5675)
*      - Premium tolerance T5667
*    - Owner Client        (CLTS)
*    - First Life Assured  (LIFE)
*    - Joint Life Assured  (LIFE)
* Get the following details.
*    - Total Premium amount of other Single premium COVT records
*      which have been created within this transaction (COVTUNL.
*    - Suspense account code and Type     (T5645)
*    - Total Contract Suspense available  (ACBL)
*    - Default the Effective date on the screen to todays date.
*    - If TODAYs date is before the Contract Risk commencement
*    - date default the Effective date to the Contract Risk
*    - date.
*
*
* VALIDATION (2000- SECTION)
*
* Skip this section if returning from a program further down the
* stack, (current stack position action flag = '*').
*
* The effective date entered on the screen must not exceed the
* Risk and Premium cessation dates of the COVR record attached to
* this life that will expire Last.
*
* The effective date entered may not be in the past.
*
* The Effective date entered cannot be before the contract Risk
* commencement date.
*
* The Single Premium Amount entered must not be greater then the
* Suspense amount available which is displayed on the screen.
*
* Validate the Premium and Risk,Terms or Ages or Dates entered
*
* If Special terms are chosen, i.e Special terms Indicator of 'X'
* then the Special terms program P5131 will be called.
*
* If Reassurance is chosen, i.e Reasurrance Indicator of 'X'
* then the Reassurance program P6267 will be called.
*
* The Commission Indicator may only have a spaces or 'X'.
* If Commission details is chosen i.e Commission Indicator of 'X'
* then the Commission program P5153 will be called.
*
* If a Premium calculation Method exists then the premium
* calculation subroutine will be called.
*
* 3000 SECTION.
*
* Create/Update the COVTMJA record with detials entered on the
* screen.
*
* If no PCDT record exists, Create Commission Split record PCDT.
*
* This program will not create nor maintain Coverage/Rider
* records directly on to the COVR data set. Instead it will
* create Transaction record(s) which will have a similar format
* to the COVR records and these will be converted to COVR records
* by the AT module which will follow.
*
* The Effective date entered on this screen will be used as the
* Risk Commencement date of the COVR record for this component
* The screen effective date will also be used as theCurrent from
* dates on the COVR, AGCM and RTRN records which will be
* generated as a result of this transaction.
*
*****************************************************************
* </pre>
*/
public class Pr579 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR579");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final int wsaaMaxOcc = 8;

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private Validator premNotReqd = new Validator(wsaaPremStatuz, "N");
	private Validator userPremEntered = new Validator(wsaaPremStatuz, "U");

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator addComp = new Validator(wsaaFlag, "A");
	private Validator modifyComp = new Validator(wsaaFlag, "M");
	private Validator compEnquiry = new Validator(wsaaFlag, "I");
	private Validator compApp = new Validator(wsaaFlag, "S","P");
	private Validator addApp = new Validator(wsaaFlag, "P");
	private Validator revComp = new Validator(wsaaFlag, "R");
	private Validator addLife = new Validator(wsaaFlag, "D");

	private FixedLengthStringData wsaaIfEndOfPlan = new FixedLengthStringData(1);
	private Validator wsaaEndOfPlan = new Validator(wsaaIfEndOfPlan, "Y");

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator nonplan = new Validator(wsaaPlanproc, "N");
	private Validator plan = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaIfSuminsChanged = new FixedLengthStringData(1);
	private Validator wsaaSuminsChanged = new Validator(wsaaIfSuminsChanged, "I","D");
	private Validator wsaaSuminsIncreased = new Validator(wsaaIfSuminsChanged, "I");
	private Validator wsaaSuminsDecreased = new Validator(wsaaIfSuminsChanged, "D");

	private FixedLengthStringData wsaaIfPremChanged = new FixedLengthStringData(1);
	private Validator wsaaPremChanged = new Validator(wsaaIfPremChanged, "I","D");
	private Validator wsaaPremIncreased = new Validator(wsaaIfPremChanged, "I");
	private Validator wsaaPremDecreased = new Validator(wsaaIfPremChanged, "D");

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private PackedDecimalData wsaaCovrPlnsfx = new PackedDecimalData(4, 0).isAPartOf(wsaaCovrKey, 15);
	private FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(wsaaCovrKey, 18, FILLER).init(SPACES);
	private PackedDecimalData wsaaStoreSumin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private PackedDecimalData wsaaSumin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaOldPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSuminsDiff = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaNextSeqnbr = new PackedDecimalData(3, 0).init(999);

	private FixedLengthStringData wsaaValidItem = new FixedLengthStringData(1).init("N");
	private Validator validItemFound = new Validator(wsaaValidItem, "Y");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreZbinstprem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreSingp = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreInstprem = new ZonedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaOptext = new FixedLengthStringData(1);
	private Validator optextYes = new Validator(wsaaOptext, "Y");
	private Validator optextNo = new Validator(wsaaOptext, "N");

	private FixedLengthStringData wsaaT5645Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5645Prog = new FixedLengthStringData(5).isAPartOf(wsaaT5645Item, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(3).isAPartOf(wsaaT5645Item, 5, FILLER).init(SPACES);
	private PackedDecimalData wsaaDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaWorkingAnb = new PackedDecimalData(3, 0);
		/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaAnbAtCcd2 = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaSex2 = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaPremCessDate = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaRiskCessDate = new PackedDecimalData(8, 0).init(0);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzMatage = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzMattrm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(0).setUnsigned();
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(22, 5);

	private FixedLengthStringData wsaaCentreString = new FixedLengthStringData(80);
	private FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(wsaaCentreString, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCentreHeading = new FixedLengthStringData(32).isAPartOf(wsaaCentreString, 24);
	private FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(wsaaCentreString, 56, FILLER).init(SPACES);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler6, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler8 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler8, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData sub3 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaMaxMort = new PackedDecimalData(3, 0).init(6).setUnsigned();
	private PackedDecimalData wsaaChdrSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtPremiums = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
		/* TABLES */
	private static final String t2240 = "T2240";
	private static final String t5551 = "T5551";
	private static final String t5645 = "T5645";
	private static final String t5667 = "T5667";
	private static final String t5671 = "T5671";
	private static final String t5675 = "T5675";
	private static final String t5687 = "T5687";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private CovtunlTableDAM covtunlIO = new CovtunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T2240rec t2240rec = new T2240rec();
	private T5551rec t5551rec = new T5551rec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Gensswrec gensswrec = new Gensswrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Undactncpy undactncpy = new Undactncpy();
	private T5667rec t5667rec = new T5667rec();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Wssplife wssplife = new Wssplife();
	private Sr579ScreenVars sv = ScreenProgram.getScreenVars( Sr579ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setScreen1070, 
		exit1090, 
		continue2070, 
		redisplay2080, 
		exit2090, 
		checkSuminCont2126, 
		checkMaturityFields2130, 
		checkMatAgeTerm2140, 
		checkPremAgeTerm2150, 
		check2152, 
		check2153, 
		checkMortcls2160, 
		checkLiencd2170, 
		exit2190, 
		exit2790, 
		checkPcdt3020, 
		exit3090, 
		exit5090, 
		skipTr52e5800
	}

	public Pr579() {
		super();
		screenVars = sv;
		new ScreenModel("Sr579", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1000();
					premmeth1020();
					readT5675T56671030();
					loadHeading1040();
					loop11050();
					loop21055();
					getLifeAssured1060();
				case setScreen1070: 
					setScreen1070();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		if (isEQ(wsspcomn.flag, "J")) {
			wsspcomn.flag.set("I");
		}
		wsaaFlag.set(wsspcomn.flag);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		undactncpy.undActions.set(wsspcomn.undAction);
		wsaaFlag.set(wsspcomn.flag);
		/* Initialise numeric and various fields on the screen.*/
		sv.comind.set(SPACES);
		sv.optextind.set(SPACES);
		sv.singlePremium.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		sv.sumin.set(ZERO);
		sv.susamt.set(ZERO);
		sv.polinc.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.matage.set(ZERO);
		sv.mattrm.set(ZERO);
		sv.premcess.set(varcom.vrcmMaxDate);
		sv.mattcess.set(varcom.vrcmMaxDate);
		sv.currcd.set(wsspcomn.trancurr);
		wsaaSumin.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaOldPrem.set(ZERO);
		wsaaOldSumins.set(ZERO);
		wsaaWorkingAnb.set(ZERO);
		wsaaPremDiff.set(ZERO);
		sv.taxamt.set(ZERO);
		wsaaSuminsDiff.set(ZERO);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		covtmjaIO.setReserveUnitsDate(ZERO);
		wsaaFirstTaxCalc.set("Y");
		sv.effdate.set(varcom.vrcmMaxDate);
		/* Read CHDRMJA (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t2240);
		/* MOVE CHDRMJA-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrmjaIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				} //MTL002
			}
		}
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrmjaIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
		sv.polinc.set(chdrmjaIO.getPolinc());
		covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isEQ(covrmjaIO.getStatuz(), varcom.mrnf)) {
			covrmjaIO.setChdrnum(SPACES);
			goTo(GotoLabel.exit1090);
		}
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		/* Read the PAYR record to get the Billing Details.*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(covrmjaIO.getPayrseqno());
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.currcd.set(payrIO.getCntcurr());
		wsaaPlanSuffix.set(covrmjaIO.getPlanSuffix());
		wsaaCrtable.set(covrmjaIO.getCrtable());
		/* Release the coverage details for processing of Whole Plan.*/
		/* i.e. If entering on an action of Component ADD then the*/
		/*   Component to be added must exist for all policies*/
		/*   within the plan.*/
		/* i.e. If entering on an action of Enquiry*/
		/*   and Whole Plan selected then we will process the*/
		/*   component for all policies within the plan.*/
		if (isEQ(wsaaPlanSuffix, ZERO)) {
			covrmjaIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				fatalError600();
			}
		}
		/* Obtain the long description of the component from DESC*/
		/* using DESCIO.*/
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5687");
		descIO.setDescitem(wsaaCrtable);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		wsaaHedline.set(descIO.getLongdesc());
	}

protected void premmeth1020()
	{
		/* Obtain the general Coverage/Rider details (including Premium*/
		/* Calculation Method details from T5687 using the coverage/rider*/
		/* code as part of the key (COVTLNB-CRTABLE).*/
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5687");
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(payrIO.getPtdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())
		|| isNE(itdmIO.getValidflag(), "1")) {
			syserrrec.statuz.set(errorsInner.e366);
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/* IF  T5687-RIIND             =  SPACES OR                     */
		/*     T5687-RIIND             =  'N'                           */
		/*     MOVE SPACES             TO SR579-RATYPIND                */
		/*     MOVE 'Y'                TO SR579-RATYPIND-OUT(ND)        */
		/* END-IF.                                                      */
		/* If modify of Component requested and the component*/
		/* in question is a Single premium Component then we move 'y'*/
		/* to comp enquiry and spaces to comp modify and an error*/
		/* informing the user that single premium component cannot*/
		/* be modified.*/
		if (modifyComp.isTrue()) {
			sv.effdateOut[varcom.pr.toInt()].set("Y");
			sv.optextindOut[varcom.pr.toInt()].set("Y");
			sv.comindOut[varcom.pr.toInt()].set("Y");
			scrnparams.errorCode.set(errorsInner.f008);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Default Todays date to the Screen.*/
		/* If TODAYs date is before the Contract Risk commencement date*/
		/* default the Effective date to the Contract Risk Commencement*/
		/* date.*/
		if (addComp.isTrue()
		|| addApp.isTrue()) {
			if (isLT(datcon1rec.intDate, chdrmjaIO.getOccdate())) {
				sv.effdate.set(chdrmjaIO.getOccdate());
			}
			else {
				sv.effdate.set(datcon1rec.intDate);
			}
		}
	}

protected void readT5675T56671030()
	{
		/* use the premium-method selected from T5687, if not blank, to*/
		/* access T5675. This gives the subroutine to use for the*/
		/* calculation.*/
		wsaaPremStatuz.set("N");
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t5687rec.premmeth);
		itemIO.setItemtabl(t5675);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/* If item found then Read the latest premium tollerance allowed.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
			t5675rec.t5675Rec.set(itemIO.getGenarea());
			itemIO.setDataArea(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(t5667);
			wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
			wsbbT5667Curr.set(payrIO.getCntcurr());
			itemIO.setItemitem(wsbbT5667Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				t5667rec.t5667Rec.set(SPACES);
			}
			else {
				t5667rec.t5667Rec.set(itemIO.getGenarea());
			}
		}
	}

protected void loadHeading1040()
	{
		wsaaCentreHeading.set(wsaaHedline);
		callProgram(Centre.class, wsaaCentreString);
		wsaaHeading.set(wsaaCentreHeading);
		wsaaHeadingChar[1].set(SPACES);
		wsaaHeadingChar[32].set(SPACES);
		sub3.set(1);
	}

protected void loop11050()
	{
		if (isEQ(wsaaHeadingChar[sub3.toInt()], SPACES)) {
			sub3.add(1);
			loop11050();
			return ;
		}
		sub3.subtract(1);
		wsaaHeadingChar[sub3.toInt()].set(wsaaStartUnderline);
		sub3.set(32);
	}

protected void loop21055()
	{
		if (isEQ(wsaaHeadingChar[sub3.toInt()], SPACES)) {
			sub3.subtract(1);
			loop21055();
			return ;
		}
		sub3.add(1);
		wsaaHeadingChar[sub3.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
	}

protected void getLifeAssured1060()
	{
		/* Get Life Insured and joint Life details needed for premium*/
		/* calculation and for showing the Life Name on the screen.*/
		readLife18000();
		/* For Component add for a single premium then we require*/
		/* todays date to be used to calculate age next birthday*/
		/* instead of CHDRMJA-PTDATE.*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.intDate2.set(datcon1rec.intDate);
		calcAge8200();
		wsaaAnbAtCcd.set(wsaaWorkingAnb);
		sv.anbAtCcd.set(lifemjaIO.getAnbAtCcd());
		wsaaCltdob.set(lifemjaIO.getCltdob());
		wsaaSex.set(lifemjaIO.getCltsex());
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/* Check for Existance of Joint Life details.*/
		readJointLife8100();
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaAnbAtCcd2.set(0);
			sv.jlifcnum.set("NONE");
			sv.jlinsname.set("NONE");
			goTo(GotoLabel.setScreen1070);
		}
		if (addComp.isTrue()) {
			initialize(agecalcrec.agecalcRec);
			agecalcrec.intDate2.set(datcon1rec.intDate);
			calcAge8200();
			wsaaAnbAtCcd2.set(wsaaWorkingAnb);
		}
		else {
			wsaaAnbAtCcd2.set(lifemjaIO.getAnbAtCcd());
		}
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		wsaaSex2.set(lifemjaIO.getCltsex());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void setScreen1070()
	{
		/* Set up the screen header details which are constant for all*/
		/* policies within the plan.*/
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.polinc.set(chdrmjaIO.getPolinc());
		sv.life.set(lifemjaIO.getLife());
		sv.coverage.set(covrmjaIO.getCoverage());
		sv.rider.set(covrmjaIO.getRider());
		sv.anbAtCcd.set(lifemjaIO.getAnbAtCcd());
		sv.currcd.set(payrIO.getCntcurr());
		wsaaCovrKey.set(covrmjaIO.getDataKey());
		covtmjaIO.setDataKey(covrmjaIO.getDataKey());
		/*    IF  NOT ADD-COMP    AND*/
		if (!addComp.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()
		&& isEQ(wsaaPlanSuffix, ZERO)) {
			covrmjaIO.setPlanSuffix(9999);
			covtmjaIO.setPlanSuffix(9999);
			covrmjaIO.setStatuz(SPACES);
			covrmjaIO.setFunction(varcom.begn);
			covtmjaIO.setFunction(varcom.begn);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanproc.set("Y");
		}
		else {
			wsaaPlanproc.set("N");
			covtmjaIO.setDataKey(covrmjaIO.getDataKey());
			covtmjaIO.setFunction(varcom.readr);
			sv.planSuffix.set(covrmjaIO.getPlanSuffix());
		}
		if (addComp.isTrue()) {
			wsaaCovtPremiums.set(0);
			covtunlIO.setParams(SPACES);
			covtunlIO.setSeqnbr(0);
			covtunlIO.setFunction("BEGN");
			covtunlIO.setChdrcoy(chdrmjaIO.getChdrcoy());
			covtunlIO.setChdrnum(chdrmjaIO.getChdrnum());
			readCovts1300();
			calcSuspense1200();
		}
		/* The following 5000-section relates to the main coverage details*/
		/* for each individual policy and as such will initially be loaded*/
		/* from here. But, if the Whole Plan has been selected or adding*/
		/* the Component then the 5000-section will be repeated in the*/
		/* 4000-section to load each policy.*/
		policyLoad5000();
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax5800();
		}
	}

protected void calcSuspense1200()
	{
		para1200();
	}

protected void para1200()
	{
		/* Line 1, item PR579 on table T5645 is read and the right*/
		/* suspense account is chosen.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5645);
		wsaaT5645Item.set(SPACES);
		wsaaT5645Prog.set(wsaaProg);
		itemIO.setItemitem(wsaaT5645Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*    Read  the sub account balance for  the contract*/
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		acblIO.setParams(SPACES);
		acblIO.setRldgacct(ZERO);
		acblIO.setRldgcoy(wsspcomn.company);
		acblIO.setRldgacct(chdrmjaIO.getChdrnum());
		acblIO.setOrigcurr(chdrmjaIO.getCntcurr());
		acblIO.setSacscode(t5645rec.sacscode[1]);
		acblIO.setSacstyp(t5645rec.sacstype[1]);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			wsaaChdrSuspense.set(ZERO);
		}
		else {
			compute(wsaaChdrSuspense, 2).set(mult(acblIO.getSacscurbal(), -1));
			wsaaChdrSuspense.subtract(wsaaCovtPremiums);
		}
		/* Move the Suspense amount available to the screen.*/
		sv.susamt.set(wsaaChdrSuspense);
	}

protected void readCovts1300()
	{
		/*PARA*/
		/* Read through the COVTs to find the total premium of any other*/
		/* Single Premium Component which may have been created within*/
		/* this transaction.*/
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)
		&& isNE(covtunlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtunlIO.getParams());
			fatalError600();
		}
		if (isNE(covtunlIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covtunlIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(covtunlIO.getStatuz(), varcom.endp)) {
			covtunlIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(covtunlIO.getBillfreq(), "00")
		&& isNE(covtunlIO.getSingp(), ZERO)) {
			wsaaCovtPremiums.add(covtunlIO.getSingp());
		}
		covtunlIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		wsaaOldSumins.set(sv.sumin);
		wsaaOldPrem.set(sv.singlePremium);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
				case continue2070: 
					continue2070();
				case redisplay2080: 
					redisplay2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'SR579IO'           USING SCRN-SCREEN-PARAMS            */
		/*                                   SR579-DATA-AREA.              */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If CF11 (KILL) is requested skip all validation.*/
		wsaaIfSuminsChanged.set("N");
		wsaaIfPremChanged.set("N");
		if (modifyComp.isTrue()) {
			scrnparams.errorCode.set(errorsInner.f008);
			goTo(GotoLabel.redisplay2080);
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(payrIO.getBtdate(), wsspcomn.currfrom)) {
			sv.chdrnumErr.set(errorsInner.f990);
			goTo(GotoLabel.continue2070);
		}
		if (addComp.isTrue()) {
			validateScreen2100();
		}
		/* If  options/extras already exist, there will be a '+' in this*/
		/* field. A request to access the details is made by entering 'X'.*/
		/* No other values (other than blank) are allowed. If Options and*/
		/* extras are requested, DO NOT CALCULATE THE premium this time.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		if (isNE(sv.comind, " ")
		&& isNE(sv.comind, "+")
		&& isNE(sv.comind, "X")) {
			sv.comindErr.set(errorsInner.g620);
		}
		/* If reassurance already exists, there will be a '+' in this*/
		/* field. A request to access the details is made by entering 'X'.*/
		/* No other values (other than blank) are allowed.*/
		/* Tax indicator must either be ' ', '+' or 'X'                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		/* IF  SR579-RATYPIND     NOT  =  ' ' AND                       */
		/*     SR579-RATYPIND     NOT  =  '+' AND                       */
		/*     SR579-RATYPIND     NOT  =  'X'                           */
		/*     MOVE G620               TO SR579-RATYPIND-ERR            */
		/* END-IF.                                                      */
		if (!addComp.isTrue()) {
			goTo(GotoLabel.continue2070);
		}
		if (isNE(sv.sumin, wsaaOldSumins)) {
			compute(wsaaSuminsDiff, 2).set(sub(sv.sumin, wsaaOldSumins));
			if (isLT(wsaaSuminsDiff, 0)) {
				compute(wsaaSuminsDiff, 2).set(mult(wsaaSuminsDiff, -1));
				wsaaIfSuminsChanged.set("D");
			}
			else {
				wsaaIfSuminsChanged.set("I");
			}
		}
		if (isNE(sv.singlePremium, wsaaOldPrem)) {
			compute(wsaaPremDiff, 2).set(sub(sv.singlePremium, wsaaOldPrem));
			if (isLT(wsaaPremDiff, 0)) {
				compute(wsaaPremDiff, 2).set(mult(wsaaPremDiff, -1));
				wsaaIfPremChanged.set("D");
			}
			else {
				wsaaIfPremChanged.set("I");
			}
		}
		/* If everything else is O-K calculate the Instalment Premium.*/
		if (isNE(sv.optextind, "X")
		&& isNE(sv.comind, "X")
		&& isEQ(sv.errorIndicators, SPACES)) {
			calcPremium2700();
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax5800();
		}
		/* IF  SR579-SINGLE-PREMIUM    >  SR579-SUSAMT                  */
		if ((setPrecision(sv.susamt, 2)
		&& isGT((add(sv.singlePremium, wsaaTaxamt)), sv.susamt))) {
			sv.susamtErr.set(errorsInner.e751);
			wsspcomn.edterror.set("Y");
		}
	}

protected void continue2070()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.redisplay2080);
		}
		/* If 'CALC' was entered then re-display the screen.*/
		if (isNE(scrnparams.statuz, varcom.calc)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void redisplay2080()
	{
		wsspcomn.edterror.set("Y");
	}

protected void validateScreen2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateEffdate2110();
					checkSumin2125();
				case checkSuminCont2126: 
					checkSuminCont2126();
				case checkMaturityFields2130: 
					checkMaturityFields2130();
				case checkMatAgeTerm2140: 
					checkMatAgeTerm2140();
				case checkPremAgeTerm2150: 
					checkPremAgeTerm2150();
				case check2152: 
					check2152();
				case check2153: 
					check2153();
					checkComplete2155();
				case checkMortcls2160: 
					checkMortcls2160();
				case checkLiencd2170: 
					checkLiencd2170();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateEffdate2110()
	{
		a100CheckLimit();
		if (isEQ(sv.effdate, 99999999)) {
			sv.effdateErr.set(errorsInner.f665);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
		/* Read through existing COVR records to find the COVERAGE record*/
		/* with the latest Premium and Risk cessation dates.*/
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs2200();
		}
		
		/* If the Effective date is greater then either highest Risk or*/
		/* premium cessation dates on an existing COVR then display an*/
		/* error message.*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
		if (isGT(sv.effdate, wsaaPremCessDate)) {
			sv.effdateErr.set(errorsInner.e983);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
		if (isGT(sv.effdate, wsaaRiskCessDate)) {
			sv.effdateErr.set(errorsInner.e983);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
		/* The effective date cannot be before the Contract Risk*/
		/* commencement date.*/
		if (isLT(sv.effdate, chdrmjaIO.getOccdate())) {
			sv.effdateErr.set(errorsInner.h359);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSumin2125()
	{
		if (isEQ(covtmjaIO.getSumins(), sv.sumin)) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkSuminCont2126);
		}
		if (plan.isTrue()
		&& isLTE(sv.planSuffix, chdrmjaIO.getPolsum())
		&& isGT(chdrmjaIO.getPolsum(), ZERO)) {
			compute(wsaaSumin, 3).setRounded((div(mult(sv.sumin, sv.polinc), chdrmjaIO.getPolsum())));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
	}

protected void checkSuminCont2126()
	{
		/* Validate the Sum assured against table T5551.*/
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)) {
			goTo(GotoLabel.checkMaturityFields2130);
		}
		if (isLT(wsaaSumin, t5551rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin, t5551rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

protected void checkMaturityFields2130()
	{
		if (isEQ(isGT(sv.matage, 0), true)
		&& isEQ(isGT(sv.mattrm, 0), false)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), false)){
			goTo(GotoLabel.checkMatAgeTerm2140);
		}
		else if (isEQ(isGT(sv.matage, 0), false)
		&& isEQ(isGT(sv.mattrm, 0), true)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), false)){
			goTo(GotoLabel.checkMatAgeTerm2140);
		}
		else if (isEQ(isGT(sv.matage, 0), false)
		&& isEQ(isGT(sv.mattrm, 0), false)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), true)){
			goTo(GotoLabel.checkMatAgeTerm2140);
		}
		else if (isEQ(isGT(sv.matage, 0), true)
		&& isEQ(isGT(sv.mattrm, 0), false)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), true)){
			wsaaDate.set(sv.mattcess);
			mattcess7100();
			if (isNE(wsaaDate, sv.mattcess)) {
				sv.mattcess.set(wsaaDate);
				sv.matageErr.set(errorsInner.u029);
				sv.mattcessErr.set(errorsInner.u029);
			}
		}
		else if (isEQ(isGT(sv.matage, 0), false)
		&& isEQ(isGT(sv.mattrm, 0), true)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), true)){
			wsaaDate.set(sv.mattcess);
			mattcess7100();
			if (isNE(wsaaDate, sv.mattcess)) {
				sv.mattcess.set(wsaaDate);
				sv.mattrmErr.set(errorsInner.u029);
				sv.mattcessErr.set(errorsInner.u029);
			}
			/** More than one field is entered.*/
		}
		else{
			sv.matageErr.set(errorsInner.f220);
			sv.mattrmErr.set(errorsInner.f220);
			sv.mattcessErr.set(errorsInner.f220);
		}
	}

protected void checkMatAgeTerm2140()
	{
		if (isEQ(isGT(sv.premCessAge, 0), true)
		&& isEQ(isGT(sv.premCessTerm, 0), false)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), false)){
			goTo(GotoLabel.checkPremAgeTerm2150);
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), true)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), false)){
			goTo(GotoLabel.checkPremAgeTerm2150);
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), false)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), true)){
			goTo(GotoLabel.checkPremAgeTerm2150);
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), false)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), false)){
			sv.premCessAge.set(sv.matage);
			sv.premCessTerm.set(sv.mattrm);
			sv.premcess.set(sv.mattcess);
		}
		else if (isEQ(isGT(sv.premCessAge, 0), true)
		&& isEQ(isGT(sv.premCessTerm, 0), false)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), true)){
			wsaaDate.set(sv.premcess);
			premcess7150();
			if (isNE(wsaaDate, sv.premcess)) {
				sv.premcess.set(wsaaDate);
				sv.pcessageErr.set(errorsInner.u029);
				sv.premcessErr.set(errorsInner.u029);
			}
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), true)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), true)){
			wsaaDate.set(sv.premcess);
			premcess7150();
			if (isNE(wsaaDate, sv.premcess)) {
				sv.premcess.set(wsaaDate);
				sv.pcesstrmErr.set(errorsInner.u029);
				sv.premcessErr.set(errorsInner.u029);
			}
			/** More than one field is entered.*/
		}
		else{
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
			sv.premcessErr.set(errorsInner.f220);
		}
	}

protected void checkPremAgeTerm2150()
	{
		if ((isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.matageErr, SPACES))
		|| (isNE(sv.mattrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2160);
		}
		/* To get this far, everything must be consistant, now cross check*/
		/* /calculate the risk and premium cessasion dates. Cross validate*/
		/* these calculated dates against the edit table (T5551).*/
		if (isEQ(sv.mattcess, varcom.vrcmMaxDate)) {
			mattcess7100();
		}
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			premcess7150();
		}
		if (isNE(sv.mattcessErr, SPACES)
		|| isNE(sv.premcessErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2160);
		}
		/* The RIsk or Premium Cessation dates of This COVR record cannot*/
		/* be greater then the RIsk or Premium Cessation dates on an*/
		/* existing COVR record.*/
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			sv.premcess.set(sv.mattcess);
		}
		if (isGT(sv.premcess, sv.mattcess)) {
			sv.premcessErr.set(errorsInner.i152);
			sv.mattcessErr.set(errorsInner.i152);
		}
		if (isGT(sv.mattcess, wsaaRiskCessDate)) {
			sv.mattcessErr.set(errorsInner.i152);
		}
		if (isGT(sv.premcess, wsaaPremCessDate)) {
			sv.premcessErr.set(errorsInner.i152);
		}
		if (isNE(sv.mattcessErr, SPACES)
		|| isNE(sv.premcessErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2160);
		}
		/*  Calculate cessasion age and term.*/
		if (isEQ(sv.mattcess, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.check2152);
		}
		datcon3rec.intDate1.set(wszzCltdob);
		datcon3rec.intDate2.set(sv.mattcess);
		callDatcon32600();
		wszzMatage.set(datcon3rec.freqFactor);
		datcon3rec.intDate2.set(sv.premcess);
		callDatcon32600();
		wszzPremCessAge.set(datcon3rec.freqFactor);
	}

protected void check2152()
	{
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.check2153);
		}
		datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon3rec.intDate2.set(sv.mattcess);
		callDatcon32600();
		wszzMattrm.set(datcon3rec.freqFactor);
		datcon3rec.intDate2.set(sv.premcess);
		callDatcon32600();
		wszzPremCessTerm.set(datcon3rec.freqFactor);
	}

protected void check2153()
	{
		/*  Assume the dates are invalid until proved otherwise.*/
		sv.matageErr.set(errorsInner.e519);
		sv.mattrmErr.set(errorsInner.e551);
		sv.pcessageErr.set(errorsInner.e562);
		sv.pcesstrmErr.set(errorsInner.e563);
		x.set(0);
		/* Check each possible option.*/
		for (x.set(1); !(isGT(x, wsaaMaxOcc)); x.add(1)){
			if ((isEQ(t5551rec.ageIssageFrm[x.toInt()], 0)
			&& isEQ(t5551rec.ageIssageTo[x.toInt()], 0))
			|| isLT(wszzAnbAtCcd, t5551rec.ageIssageFrm[x.toInt()])
			|| isGT(wszzAnbAtCcd, t5551rec.ageIssageTo[x.toInt()])) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isGTE(wszzMatage, t5551rec.maturityAgeFrom[x.toInt()])
				&& isLTE(wszzMatage, t5551rec.maturityAgeTo[x.toInt()])) {
					sv.matageErr.set(SPACES);
				}
				if (isGTE(wszzPremCessAge, t5551rec.premCessageFrom[x.toInt()])
				&& isLTE(wszzPremCessAge, t5551rec.premCessageTo[x.toInt()])) {
					sv.pcessageErr.set(SPACES);
				}
			}
			if ((isEQ(t5551rec.termIssageFrm[x.toInt()], 0)
			&& isEQ(t5551rec.termIssageTo[x.toInt()], 0))
			|| isLT(wszzAnbAtCcd, t5551rec.termIssageFrm[x.toInt()])
			|| isGT(wszzAnbAtCcd, t5551rec.termIssageTo[x.toInt()])) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isGTE(wszzMattrm, t5551rec.maturityTermFrom[x.toInt()])
				&& isLTE(wszzMattrm, t5551rec.maturityTermTo[x.toInt()])) {
					sv.mattrmErr.set(SPACES);
				}
				if (isGTE(wszzPremCessTerm, t5551rec.premCesstermFrom[x.toInt()])
				&& isLTE(wszzPremCessTerm, t5551rec.premCesstermTo[x.toInt()])) {
					sv.pcesstrmErr.set(SPACES);
				}
			}
		}
	}

protected void checkComplete2155()
	{
		if (isNE(sv.mattrmErr, SPACES)
		&& isEQ(sv.mattrm, ZERO)) {
			sv.mattcessErr.set(sv.mattrmErr);
			sv.mattrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr, SPACES)
		&& isEQ(sv.premCessTerm, ZERO)) {
			sv.premcessErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.matageErr, SPACES)
		&& isEQ(sv.matage, ZERO)) {
			sv.mattcessErr.set(sv.matageErr);
			sv.matageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr, SPACES)
		&& isEQ(sv.premCessAge, ZERO)) {
			sv.premcessErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2160()
	{
		/*  Mortality-Class,  if the mortality class appears on a coverage*/
		/*  /rider screen it is a compulsory field because it will be used*/
		/*  in calculating the premium amount. The mortality class entered*/
		/*  one of the ones in the edit rules table.*/
		if (isEQ(sv.mortclsOut[varcom.pr.toInt()], "Y")) {
			goTo(GotoLabel.checkLiencd2170);
		}
		wsaaValidItem.set("N");
		for (x.set(1); !(isGT(x, wsaaMaxMort)
		|| validItemFound.isTrue()); x.add(1)){
			if (isEQ(t5551rec.mortcls[x.toInt()], sv.mortcls)
			&& isNE(sv.mortcls, SPACES)) {
				wsaaValidItem.set("Y");
			}
		}
		if (isEQ(wsaaValidItem, "N")) {
			sv.mortclsErr.set(errorsInner.e420);
		}
	}

protected void checkLiencd2170()
	{
		/* Validate the LIEN code.*/
		if (isEQ(sv.liencdOut[varcom.pr.toInt()], "Y")
		|| isEQ(sv.liencd, SPACES)) {
			return ;
		}
		wsaaValidItem.set("N");
		for (x.set(1); !(isGT(x, wsaaMaxMort)); x.add(1)){
			if (isEQ(t5551rec.liencd[x.toInt()], SPACES)
			|| isNE(t5551rec.liencd[x.toInt()], sv.liencd)) {
				compute(x, 0).set(add(wsaaMaxMort, 1));
				wsaaValidItem.set("Y");
			}
		}
		if (isEQ(wsaaValidItem, "N")) {
			sv.liencdErr.set(errorsInner.e531);
		}
	}

protected void processCovrs2200()
	{
		start2200();
	}

protected void start2200()
	{
		/* Find the current COVR record attached to this contract which*/
		/* has the Highest risk and Premium cessation dates.*/
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), covrenqIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), covrenqIO.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(covrenqIO.getRider(), "00")
		|| isEQ(covrenqIO.getRider(), SPACES)) {
			if (isGT(covrenqIO.getPremCessDate(), wsaaPremCessDate)) {
				wsaaPremCessDate.set(covrenqIO.getPremCessDate());
			}
			if (isGT(covrenqIO.getRiskCessDate(), wsaaRiskCessDate)) {
				wsaaRiskCessDate.set(covrenqIO.getRiskCessDate());
			}
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void callDatcon32600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcPremium2700()
	{
		try {
			para2700();
			calc2710();
			adjustPrem2725();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para2700()
	{
		/* If plan processing, and no policies applicable,*/
		/*   skip the validation as this COVTMJA is to be deleted.*/
		/* PREMIUM CALCULATION*/
		/* The  premium amount is  required  on  all  products  and  all*/
		/* validating  must  be  successfully  completed  before  it  is*/
		/* calculated. If there is  no  premium  method defined (i.e the*/
		/* relevant code was blank), the premium amount must be entered.*/
		/* Otherwise, it is optional and always calculated.*/
		/* Note that a premium calculation subroutine may calculate the*/
		/* premium from the sum insured OR the sum insured from the*/
		/* premium.*/
		/* If there is no premium calculation method, set premium*/
		/* required field to YES.*/
		if (isEQ(t5675rec.premsubr, SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			wsaaPremStatuz.set("N");
		}
		/* If a premium is required check if this premium = 0*/
		if (premReqd.isTrue()) {
			if (isEQ(sv.singlePremium, 0)) {
				sv.singprmErr.set(errorsInner.g818);
			}
			goTo(GotoLabel.exit2790);
		}
		if (isGT(sv.singlePremium, 0)
		&& premReqd.isTrue()) {
			if (!wsaaPremChanged.isTrue()) {
				goTo(GotoLabel.exit2790);
			}
		}
	}

protected void calc2710()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		/*  Effective date to be current billed to date, as increase will*/
		/*  be paid as part of next billing*/
		premiumrec.effectdt.set(sv.effdate);
		premiumrec.termdate.set(sv.premcess);
		premiumrec.currcode.set(payrIO.getCntcurr());
		/*  (wsaa-sumin already adjusted for plan processing)*/
		wsaaStoreSumin.set(wsaaSumin);
		if (plan.isTrue()
		&& isLTE(sv.planSuffix, chdrmjaIO.getPolsum())
		&& isGT(chdrmjaIO.getPolsum(), ZERO)) {
			compute(wsaaSumin, 3).setRounded(div(mult(sv.sumin, sv.polinc), chdrmjaIO.getPolsum()));
			premiumrec.sumin.set(wsaaSumin);
		}
		else {
			premiumrec.sumin.set(wsaaSumin);
		}
		if (isEQ(premiumrec.sumin, ZERO)) {
			if (optextYes.isTrue()) {
				premiumrec.sumin.set(wsaaStoreSumin);
			}
		}
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.billfreq.set("00");
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrmjaIO.getOccdate());
		premiumrec.reRateDate.set(chdrmjaIO.getOccdate());
		premiumrec.calcPrem.set(sv.singlePremium);
		premiumrec.calcBasPrem.set(sv.singlePremium);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsaaSex2);
		premiumrec.jlage.set(wsaaAnbAtCcd2);
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		callDatcon32600();
		compute(premiumrec.duration, 5).set(add(datcon3rec.freqFactor, .99999));
		if (isLTE(sv.planSuffix, chdrmjaIO.getPolsum())
		&& plan.isTrue()
		&& isGT(chdrmjaIO.getPolsum(), ZERO)) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.polinc), chdrmjaIO.getPolsum())));
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.polinc), chdrmjaIO.getPolsum())));
		}
		/* As this is a unit-linked program, there is no need for the*/
		/* linkage to be set up with ANNY details.  Therefore, all*/
		/* fields are initialised before the call to the subroutine.*/
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			premiumrec.cnttype.set(chdrmjaIO.getCnttype());
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		//****Ticket #ILIFE-2005 end
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.singprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2790);
		}
	}

protected void adjustPrem2725()
	{
		/* Adjust premium calculated for plan processing and*/
		/* possibly put calculated sum insured back on the screen.*/
		if (plan.isTrue()
		&& isLTE(sv.planSuffix, chdrmjaIO.getPolsum())
		&& isGT(sv.polinc, ZERO)
		&& isGT(chdrmjaIO.getPolsum(), ZERO)) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, chdrmjaIO.getPolsum()), sv.polinc)));
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, chdrmjaIO.getPolsum()), sv.polinc)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, chdrmjaIO.getPolsum()), sv.polinc)));
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin, chdrmjaIO.getPolsum()), sv.polinc)));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/* Having calculated it, the  entered value, if any, is compared*/
		/* with it to check that  it  is within acceptable limits of the*/
		/* automatically calculated figure.  If  it  is  less  than  the*/
		/* amount calculated and  within  tolerance  then  the  manually*/
		/* entered amount is allowed.  If  the entered value exceeds the*/
		/* calculated one, the calculated value is used.*/
		if (isEQ(sv.singlePremium, 0)) {
			sv.singlePremium.set(premiumrec.calcPrem);
			return ;
		}
		if (isGT(sv.singlePremium, premiumrec.calcPrem)) {
			sv.singlePremium.set(premiumrec.calcPrem);
			return ;
		}
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, sv.singlePremium));
		/* Calculate tolerance Limit and Check whether it is greater*/
		/*   than the maximum tolerance amount in table T5667.*/
		sv.singprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)
		|| isEQ(sv.singprmErr, SPACES)); wsaaSub.add(1)){
			if (isEQ(payrIO.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtol[wsaaSub.toInt()])), 100));
				if (isLTE(wsaaDiff, wsaaTol)
				&& isLTE(wsaaDiff, t5667rec.maxAmount[wsaaSub.toInt()])) {
					sv.singprmErr.set(SPACES);
				}
				if (isNE(t5667rec.maxamt[wsaaSub.toInt()], ZERO)) {
					compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtoln[wsaaSub.toInt()])), 100));
					if (isLTE(wsaaDiff, wsaaTol)
					&& isLTE(wsaaDiff, t5667rec.maxamt[wsaaSub.toInt()])) {
						sv.singprmErr.set(SPACES);
					}
				}
			}
			/*****    IF  WSAA-DIFF       NOT  >  WSAA-TOL  AND                 */
			/*****        WSAA-DIFF       NOT  >  T5667-MAX-AMOUNT(WSAA-SUB)    */
			/*****        MOVE SPACES          TO SR579-SINGPRM-ERR             */
			/*****    END-IF                                                    */
		}
		/* If the premium is within the tolerance limits, then we have to  */
		/* adjust the basic premium (the loaded premium will not change).  */
		if (isEQ(sv.singprmErr, SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.singlePremium, sv.zlinstprem));
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loadWsspFields3010();
				case checkPcdt3020: 
					checkPcdt3020();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loadWsspFields3010()
	{
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);
		}
		/* If the 'KILL' function key was pressed or if in enquiry mode,*/
		/* skip the updating.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit3090);
		}
		keepCovr3100();
		/* IF  COMP-ENQUIRY    OR                                       */
		/*     SR579-OPTEXTIND         =  'X'                           */
		/*    IF  COMP-ENQUIRY                                             */
		if (compEnquiry.isTrue()
		|| revComp.isTrue()
		|| isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3090);
		}
		if (compApp.isTrue()) {
			goTo(GotoLabel.checkPcdt3020);
		}
		/* If the Options/Extras check box has been selected,           */
		/* do not leave the section if it has been selected in          */
		/* conjunction with either the Commission or the                */
		/* Reassurance check box.                                       */
		if (isEQ(sv.optextind, "X")
		&& isNE(sv.comind, "X")) {
			goTo(GotoLabel.exit3090);
		}
		/* Do not skip the updating of the COVT record if the           */
		/* reassurance check box has also been selected.                */
		/*    IF  SR579-COMIND            =  'X'*/
		/*        GO TO 3020-CHECK-PCDT*/
		/*    END-IF.*/
		/* Release the COVTMJA record.*/
		covtmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (addComp.isTrue()) {
			covtmjaIO.setPlanSuffix(ZERO);
			setupcovt3200();
			covtmjaIO.setFunction(varcom.updat);
			updateCovtmja3300();
		}
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		covtmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		/* If the whole plan selected then do a KEEPS on the COVR file so*/
		/* that if there is no COVT record the COVR record can be retrived*/
		/* to show FUND DIRECTION.*/
		keepCovr3100();
	}

protected void checkPcdt3020()
	{
		/* If no PCDT record is to be created then the existing agent is*/
		/* to receive commission, therefore set up current details in*/
		/* PCDT to make AT processing uniform throughout.*/
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		pcdtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrmjaIO.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			defaultPcdt3800();
		}
	}

protected void keepCovr3100()
	{
		/*PARA*/
		if (isEQ(wsaaPlanSuffix, ZERO)) {
			covrmjaIO.setDataKey(wsaaCovrKey);
			covrmjaIO.setPlanSuffix(wsaaPlanSuffix);
			covrmjaIO.setFunction("KEEPS");
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void setupcovt3200()
	{
		initialise3200();
		setup3210();
	}

protected void initialise3200()
	{
		/* Before creating a  new COVTMJA record initialise the coverage*/
		/* fields.*/
		covtmjaIO.setCrtable(wsaaCrtable);
		covtmjaIO.setRiskCessAge(0);
		covtmjaIO.setBenCessAge(0);
		covtmjaIO.setPremCessAge(0);
		covtmjaIO.setRiskCessTerm(0);
		covtmjaIO.setBenCessTerm(0);
		covtmjaIO.setPremCessTerm(0);
		covtmjaIO.setPolinc(0);
		covtmjaIO.setInstprem(0);
		covtmjaIO.setZbinstprem(0);
		covtmjaIO.setZlinstprem(0);
		covtmjaIO.setSingp(0);
		covtmjaIO.setAnbccd(1, 0);
		covtmjaIO.setAnbccd(2, 0);
		covtmjaIO.setSumins(0);
		covtmjaIO.setSex(1, SPACES);
		covtmjaIO.setSex(2, SPACES);
		covtmjaIO.setRiskCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setPremCessDate(varcom.vrcmMaxDate);
	}

protected void setup3210()
	{
		covtmjaIO.setSeqnbr(wsaaNextSeqnbr);
		wsaaNextSeqnbr.subtract(1);
		covtmjaIO.setEffdate(sv.effdate);
		covtmjaIO.setPayrseqno(1);
		covtmjaIO.setCrtable(wsaaCrtable);
		covtmjaIO.setPlanSuffix(0);
		covtmjaIO.setRiskCessDate(sv.mattcess);
		covtmjaIO.setPremCessDate(sv.premcess);
		covtmjaIO.setRiskCessAge(sv.matage);
		covtmjaIO.setPremCessAge(sv.premCessAge);
		covtmjaIO.setRiskCessTerm(sv.mattrm);
		covtmjaIO.setPremCessTerm(sv.premCessTerm);
		covtmjaIO.setSumins(sv.sumin);
		covtmjaIO.setSingp(sv.singlePremium);
		covtmjaIO.setZbinstprem(sv.zbinstprem);
		covtmjaIO.setZlinstprem(sv.zlinstprem);
		covtmjaIO.setInstprem(0);
		covtmjaIO.setMortcls(sv.mortcls);
		covtmjaIO.setLiencd(sv.liencd);
		covtmjaIO.setPolinc(sv.polinc);
		covtmjaIO.setBillfreq("00");
		covtmjaIO.setBillchnl(payrIO.getBillchnl());
		/* If the effective date has changed then recalculate the Age*/
		/* next birthday at Risk Commencement date.*/
		if (isNE(covtmjaIO.getEffdate(), datcon1rec.intDate)) {
			readLife18000();
			initialize(agecalcrec.agecalcRec);
			agecalcrec.intDate2.set(covtmjaIO.getEffdate());
			calcAge8200();
			wsaaAnbAtCcd.set(wsaaWorkingAnb);
		}
		covtmjaIO.setAnbccd(1, wsaaAnbAtCcd);
		covtmjaIO.setSex(1, wsaaSex);
		if (isEQ(sv.jlifcnum, "NONE")) {
			covtmjaIO.setAnbccd(2, ZERO);
			covtmjaIO.setSex(2, SPACES);
		}
		else {
			readJointLife8100();
			initialize(agecalcrec.agecalcRec);
			agecalcrec.intDate2.set(covtmjaIO.getEffdate());
			calcAge8200();
			wsaaAnbAtCcd2.set(wsaaWorkingAnb);
			covtmjaIO.setAnbccd(2, wsaaAnbAtCcd2);
			covtmjaIO.setSex(2, wsaaSex2);
		}
	}

protected void updateCovtmja3300()
	{
		para3300();
	}

protected void para3300()
	{
		covtmjaIO.setBenCessAge(ZERO);
		covtmjaIO.setBenCessTerm(ZERO);
		covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setReserveUnitsDate(ZERO);
		covtmjaIO.setTermid(varcom.vrcmTermid);
		covtmjaIO.setUser(varcom.vrcmUser);
		covtmjaIO.setTransactionDate(varcom.vrcmDate);
		covtmjaIO.setTransactionTime(varcom.vrcmTime);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void defaultPcdt3800()
	{
		moveValues3800();
		call3830();
	}

protected void moveValues3800()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		pcdtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setInstprem(ZERO);
		wsaaIndex.set(1);
	}

protected void call3830()
	{
		pcdtmjaIO.setAgntnum(wsaaIndex, chdrmjaIO.getAgntnum());
		pcdtmjaIO.setSplitc(wsaaIndex, 100);
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/* If 'KILL' has been requested, (CF11), then move spaces to the*/
		/* current program entry in the program stack and exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/* Determine processing according to whether we are selecting*/
		/* or returning from the Options and Extras data screen.*/
		if (isEQ(sv.optextind, "X")) {
			optionsExe4100();
			return ;
		}
		else {
			if (isEQ(sv.optextind, "?")) {
				optionsRet4200();
				return ;
			}
		}
		if (isEQ(sv.comind, "X")) {
			commExe4300();
			return ;
		}
		else {
			if (isEQ(sv.comind, "?")) {
				commRet4400();
				return ;
			}
		}
		if (isEQ(sv.taxind, "?")) {
			taxRet4b50();
			return ;
		}
		if (isEQ(sv.optextind, "?")) {
			sv.optextind.set("+");
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe4b00();
			return ;
		}
		/* IF  SR579-RATYPIND          =  'X'                           */
		/*     PERFORM 4500-REASSURANCE-EXE                             */
		/*     GO TO 4090-EXIT                                          */
		/* ELSE                                                         */
		/*     IF  SR579-RATYPIND      =  '?'                           */
		/*         PERFORM 4600-REASSURANCE-RET                         */
		/*         GO TO 4090-EXIT                                      */
		/*     END-IF                                                   */
		/* END-IF.                                                      */
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		wsaaIfEndOfPlan.set("N");
		if (plan.isTrue()) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				covrmjaIO.setFunction(varcom.nextr);
				clearScreen4900();
				policyLoad5000();
				if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
					wsspcomn.nextprog.set(wsaaProg);
					wsaaIfEndOfPlan.set("Y");
					wayout4800();
				}
				else {
					wsspcomn.nextprog.set(scrnparams.scrname);
				}
			}
			else {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
				wayout4800();
			}
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4800();
		}
	}

protected void optionsExe4100()
	{
		keepCovr4110();
	}

protected void keepCovr4110()
	{
		/* Keep the COVRMJA record if Whole Plan selected or Add component*/
		/* as this will have been released within the 1000-section to*/
		/* enable all policies to be processed.*/
		covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		/* The first thing to consider is the handling of an Options/*/
		/* Extras request. If the indicator is 'X', a request to visit*/
		/* options and extras has been made. In this case:*/
		/*    - change the options/extras request indicator to '?',*/
		sv.optextind.set("?");
		/* Save the next 8 programs from the program stack.*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			save4730();
		}
		/*  - call GENSSWCH with and  action  of 'A' to retrieve the*/
		/*    program switching required,  and  move  them to the stack*/
		gensswrec.function.set("A");
		gensww4710();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			load4750();
		}
		/* Set the current stack "action" to '*'.*/
		/* Add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void optionsRet4200()
	{
		para4200();
	}

protected void para4200()
	{
		/* On return from this  request, the current stack "action" will*/
		/*   be '*' and the options/extras indicator  will  be  '?'.  To*/
		/*   handle the return from options and extras:*/
		/* - calculate the premium as described above in the 'Validation'*/
		/*   section, and check that it is within the tolerance limit.*/
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			wsaaOptext.set("Y");
			calcPremium2700();
		}
		wsaaOptext.set("N");
		/* Restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			restore4770();
		}
		/* Blank out the stack "action".*/
		/* Only blank out the action and set the screen name if         */
		/* no other check boxes have been selected.                     */
		/* MOVE ' '         TO WSSP-SEC-ACTN (WSSP-PROGRAM-PTR).        */
		if (isEQ(sv.optextind, "?")) {
			sv.optextind.set("+");
		}
		/* Set WSSP-NEXTPROG to the current screen name (thus returning*/
		/* returning to re-display the screen).*/
		/* MOVE SCRN-SCRNAME           TO WSSP-NEXTPROG.                */
		if (isNE(sv.comind, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void commExe4300()
	{
		para4300();
	}

protected void para4300()
	{
		/* The first  thing  to   consider   is   the  handling  of  an*/
		/* options/extras request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		wsspcomn.tranrate.set(sv.singlePremium);
		if (addComp.isTrue()) {
			wssplife.effdate.set(sv.effdate);
		}
		else {
			wssplife.effdate.set(payrIO.getBtdate());
		}
		covrmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		sv.comind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			save4730();
		}
		/* Call GENSSWCH with and  action  of 'B' to retrieve the*/
		/* program switching required,  and  move  them to the stack.*/
		gensswrec.function.set("B");
		gensww4710();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			load4750();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void commRet4400()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			restore4770();
		}
		/* Only blank out the action and set the screen name if         */
		/* the reassurance check box has not been selected.             */
		/* MOVE ' '                TO WSSP-SEC-ACTN (WSSP-PROGRA<A06068>*/
		if (isEQ(sv.comind, "?")) {
			sv.comind.set("+");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*4500-REASSURANCE-EXE SECTION.                                    
	*4500-PARA.                                                       
	**Keep the COVRMJA record if Whole Plan selected or Add component 
	**as this will have been released within the B000-section to      
	**enable all policies to be processed.                            
	**** MOVE KEEPS                  TO COVRMJA-FUNCTION.             
	**** CALL 'COVRMJAIO'         USING COVRMJA-PARAMS.               
	**** IF  COVRMJA-STATUZ     NOT  =  O-K                           
	****     MOVE COVRMJA-PARAMS     TO SYSR-PARAMS                   
	****     MOVE COVRMJA-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 600-FATAL-ERROR                                  
	**** END-IF.                                                      
	**If the reassurance details have been selected,(value - 'X'),    
	**then set an asterisk in the program stack action field tor      
	**ensure that control returns here, set the parameters for        
	**generalised secondary switching and save the original           
	**programs from the program stack.                                
	**** MOVE '?'                    TO SR579-RATYPIND.               
	**** COMPUTE SUB1                =  WSSP-PROGRAM-PTR + 1.         
	**** MOVE 1                      TO SUB2.                         
	**** PERFORM 4730-SAVE           8  TIMES.                        
	**** MOVE 'C'                    TO GENS-FUNCTION.                
	**** PERFORM 4710-GENSWW.                                         
	**** COMPUTE SUB1                =  WSSP-PROGRAM-PTR + 1.         
	**** MOVE 1                      TO SUB2.                         
	**** PERFORM 4750-LOAD           8  TIMES.                        
	**** MOVE '*'                 TO WSSP-SEC-ACTN(WSSP-PROGRAM-PTR). 
	**** ADD 1                       TO WSSP-PROGRAM-PTR.             
	*4590-EXIT.                                                       
	****  EXIT.                                                       
	*4600-REASSURANCE-RET SECTION.                                    
	*4610-PARA.                                                       
	**** COMPUTE SUB1                =  WSSP-PROGRAM-PTR + 1.         
	**** MOVE 1                      TO SUB2.                         
	**** PERFORM 4770-RESTORE        8  TIMES.                        
	**** MOVE ' '            TO WSSP-SEC-ACTN(WSSP-PROGRAM-PTR).      
	**** PERFORM 7500-CHECK-RACT.                                     
	**Set WSSP-NEXTPROG to the current screen name (thus returning    
	**returning to re-display the screen).                            
	**** MOVE SCRN-SCRNAME           TO WSSP-NEXTPROG.                
	*4690-EXIT.                                                       
	****  EXIT.                                                       
	* </pre>
	*/
protected void gensww4710()
	{
		para4711();
	}

protected void para4711()
	{
		/*   If a value has been placed in the GENS-FUNCTION then call*/
		/*   the generalised secondary switching module to obtain the*/
		/*   next 8 programs and load them into the program stack.*/
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextrevIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void save4730()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4750()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void restore4770()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4800()
	{
		/*PARA*/
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/* If 'KILL' has been requested, (CF11), then move spaces to the*/
		/* current program entry in the program stack and exit.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (wsaaEndOfPlan.isTrue()) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void clearScreen4900()
	{
		/*PARA*/
		sv.comind.set(SPACES);
		sv.optextind.set(SPACES);
		sv.singlePremium.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		sv.sumin.set(ZERO);
		sv.polinc.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.matage.set(ZERO);
		sv.mattrm.set(ZERO);
		sv.premcess.set(varcom.vrcmMaxDate);
		sv.mattcess.set(varcom.vrcmMaxDate);
		/*EXIT*/
	}

protected void taxExe4b00()
	{
		start4b00();
	}

protected void start4b00()
	{
		/*  - Keep the CHDR/COVT record                                    */
		chdrmjaIO.setFunction("KEEPS");
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		wsaaStoreZbinstprem.set(covtmjaIO.getZbinstprem());
		wsaaStoreSingp.set(covtmjaIO.getSingp());
		wsaaStoreInstprem.set(covtmjaIO.getInstprem());
		covtmjaIO.setZbinstprem(sv.zbinstprem);
		covtmjaIO.setSingp(sv.singlePremium);
		covtmjaIO.setInstprem(0);
		covtmjaIO.setFunction("KEEPS");
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		wssplife.effdate.set(sv.effdate);
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.taxind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'B' to retrieve the       */
		/*       program switching required,  and  move  them to the       */
		/*       stack,                                                    */
		gensswrec.function.set("F");
		gensww4710();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet4b50()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.taxind.set("+");
		covtmjaIO.setZbinstprem(wsaaStoreZbinstprem);
		covtmjaIO.setSingp(wsaaStoreSingp);
		covtmjaIO.setInstprem(wsaaStoreInstprem);
		/* Restore the saved programs to the program stack                 */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			restore4770();
		}
		/* Blank out the stack "action".                                   */
		/* Set WSSP-NEXTPROG to the current screen name (thus returning    */
		/*   returning to re-display the screen).                          */
		/* MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.                   */
		if (isNE(sv.taxind, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		/*EXIT*/
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void policyLoad5000()
	{
		try {
			para5000();
			readCovt5020();
			fieldsToScreen5080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para5000()
	{
		/* If Whole plan selected or component ADD, then we need to read*/
		/*   the COVRMJA file to find either the record to be changed or*/
		/*   the policies to which the component must be added.*/
		/* If COVRMJA statuz = ENDP then all policies have been processed*/
		/*   so exit this section.*/
		/* Else attempt a search of the COVTMJA temporary file for detail*/
		/*   changes.*/
		if (plan.isTrue()) {
			readCovr5100();
			if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
				goTo(GotoLabel.exit5090);
			}
			else {
				covtmjaIO.setFunction(varcom.readr);
				if (addComp.isTrue()) {
					covtmjaIO.setCoverage(wsaaCovrCoverage);
					covtmjaIO.setRider(wsaaCovrRider);
					covtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
				}
				else {
					covtmjaIO.setDataKey(wsaaCovrKey);
					if (isNE(covrmjaIO.getPlanSuffix(), ZERO)) {
						covtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
					}
				}
			}
		}
		/* The header is set to show which policy is being actioned.*/
		/* If all policies are within the summarised record then display*/
		/* the number of policies summarised and set the Hi-light on*/
		/* Plan suffix to display the literal - 'Policy Number : 10 to 1'*/
		if (plan.isTrue()) {
			if (isEQ(covrmjaIO.getPlanSuffix(), ZERO)) {
				sv.planSuffix.set(chdrmjaIO.getPolsum());
				sv.plnsfxOut[varcom.hi.toInt()].set("Y");
			}
			else {
				sv.planSuffix.set(covrmjaIO.getPlanSuffix());
			}
		}
		if (!addComp.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			recordToScreen6000();
			goTo(GotoLabel.exit5090);
		}
	}

protected void readCovt5020()
	{
		/* Read the Temporary COVT record which will contain any previous*/
		/* changes before AT submission creates the COVRMJA record.*/
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.mrnf)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(), covrmjaIO.getChdrnum())
		|| isNE(covtmjaIO.getLife(), covrmjaIO.getLife())
		|| isNE(covtmjaIO.getCoverage(), covrmjaIO.getCoverage())
		|| isNE(covtmjaIO.getRider(), covrmjaIO.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(), covrmjaIO.getPlanSuffix())
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		/* If entering on Component Modify then just Display the details*/
		/*   as no defaults are necessary.*/
		/* Else we must load the Tables required for defaults.*/
		/*   note - Both statuz's are checked as we may previously have*/
		/*          read the file directly or sequentially.*/
		if (isEQ(covtmjaIO.getStatuz(), varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			covtmjaIO.setNonKey(SPACES);
			covtmjaIO.setAnbccd(1, ZERO);
			covtmjaIO.setAnbccd(2, ZERO);
			covtmjaIO.setSingp(ZERO);
			covtmjaIO.setZbinstprem(ZERO);
			covtmjaIO.setZlinstprem(ZERO);
			covtmjaIO.setPlanSuffix(ZERO);
			covtmjaIO.setInstprem(ZERO);
			covtmjaIO.setPremCessAge(ZERO);
			covtmjaIO.setPremCessTerm(ZERO);
			covtmjaIO.setRiskCessAge(ZERO);
			covtmjaIO.setRiskCessTerm(ZERO);
			covtmjaIO.setBenCessAge(ZERO);
			covtmjaIO.setBenCessTerm(ZERO);
			covtmjaIO.setEffdate(ZERO);
			covtmjaIO.setSumins(ZERO);
			covtmjaIO.setPremCessDate(varcom.vrcmMaxDate);
			covtmjaIO.setRiskCessDate(varcom.vrcmMaxDate);
			covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
			covtmjaIO.setPolinc(chdrmjaIO.getPolinc());
			covtmjaIO.setCrtable(wsaaCrtable);
			tableLoads7000();
		}
		else {
			tableLoads7000();
		}
	}

protected void fieldsToScreen5080()
	{
		/* Move fields to screen.*/
		sv.planSuffix.set(covtmjaIO.getPlanSuffix());
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.premcess.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.mattcess.set(covtmjaIO.getRiskCessDate());
		sv.matage.set(covtmjaIO.getRiskCessAge());
		sv.mattrm.set(covtmjaIO.getRiskCessTerm());
		sv.polinc.set(covtmjaIO.getPolinc());
		sv.planSuffix.set(covtmjaIO.getPolinc());
		sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		sv.singlePremium.set(covtmjaIO.getSingp());
		sv.zbinstprem.set(covtmjaIO.getZbinstprem());
		sv.zlinstprem.set(covtmjaIO.getZlinstprem());
		sv.sumin.set(covtmjaIO.getSumins());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/* If both the maximum and minimum values for the Sum insured on*/
		/* T5551 are zeros then no entry is allowed in that field protect*/
		/* and non-display it.*/
		if ((isEQ(t5551rec.sumInsMax, ZERO)
		&& isEQ(t5551rec.sumInsMin, ZERO))) {
			sv.sumin.set(ZERO);
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)
		&& isEQ(t5551rec.sumInsMax, 0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5551rec.sumInsMax);
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(sv.sumin, (sub(chdrmjaIO.getPolsum(), 1))), chdrmjaIO.getPolsum())));
			}
		}
		/* If all the valid mortality classes are Blank, non-display and*/
		/* protect this field. If there is only one mortality class,*/
		/* display and protect it (no validation will be required).*/
		if (isEQ(t5551rec.mortclss, SPACES)) {
			sv.mortcls.set(SPACES);
			sv.mortclsOut[varcom.nd.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5551rec.mortcls01, SPACES)
		&& isEQ(t5551rec.mortcls02, SPACES)
		&& isEQ(t5551rec.mortcls03, SPACES)
		&& isEQ(t5551rec.mortcls04, SPACES)
		&& isEQ(t5551rec.mortcls05, SPACES)
		&& isEQ(t5551rec.mortcls06, SPACES)) {
			sv.mortcls.set(t5551rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/* If all the valid lien codes are blank, non-display and protect*/
		/* this field. Otherwise, if there is only one lien ocde then move*/
		/* this to the screen, protect it and display it. If there are*/
		/* more than two lien codes leave unprotected for the user to*/
		/* enter their choice.*/
		if (isEQ(t5551rec.liencds, SPACES)) {
			sv.liencd.set(SPACES);
			sv.liencdOut[varcom.nd.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5551rec.liencd01, SPACES)
		&& isEQ(t5551rec.liencd02, SPACES)
		&& isEQ(t5551rec.liencd03, SPACES)
		&& isEQ(t5551rec.liencd04, SPACES)
		&& isEQ(t5551rec.liencd05, SPACES)
		&& isEQ(t5551rec.liencd06, SPACES)) {
			sv.liencd.set(t5551rec.liencd01);
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		/* Using the age next birthday (ANB at RCD) from the  applicable*/
		/* life (see above) look up Issue Age on the AGE and TERM section.*/
		/* If the age fits into a "slot" in  one of these sections, and*/
		/* the risk cessation limits are the same, default and protect the*/
		/* risk cessation fields. Also do the same for the premium cessatio*/
		/* details in this case also calculate the risk and premium dates.*/
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		checkDefaults7300();
		mattcess7100();
		premcess7150();
		if (isNE(t5551rec.eaage, SPACES)) {
			sv.mattcessOut[varcom.pr.toInt()].set("Y");
			sv.premcessOut[varcom.pr.toInt()].set("Y");
		}
		/* If options and extras are  not  allowed non display and protect*/
		/* the fields. Otherwise, read the option and extras details.*/
		if (isEQ(t5551rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext7400();
		}
		/* If reassurance is not allowed non-display and protect the field*/
		/* Otherwise,  read the  reassurance details  for  the  current*/
		/* coverage/rider.*/
		/* IF T5687-RIIND              = 'N' OR SPACES                  */
		/*    MOVE 'Y'                 TO SR579-RATYPIND-OUT (ND)       */
		/* ELSE                                                         */
		/*    PERFORM 7500-CHECK-RACT                                   */
		/* END-IF.                                                      */
		checkAgnt7600();
	}

protected void readCovr5100()
	{
		/*READ-COVRMJA*/
		/* Read the Coverage file.*/
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), wsaaCovrChdrcoy)
		|| isNE(covrmjaIO.getChdrnum(), wsaaCovrChdrnum)
		|| isNE(covrmjaIO.getLife(), wsaaCovrLife)
		|| isNE(covrmjaIO.getCoverage(), wsaaCovrCoverage)
		|| isNE(covrmjaIO.getRider(), wsaaCovrRider)
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void calcPremium5200()
	{
		/*CALC-PREMIUM*/
		/* Use the premium-method selected from T5687, if not blank THEN*/
		/* access T5675. This gives the subroutine for the calculation.*/
		/* If the benefit billing method is not blank, non-display and*/
		/* protect the premium field (so skip the following).*/
		wsaaPremStatuz.set("N");
		/*METHOD-TABLE*/
		itemIO.setItemtabl(t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
			t5675rec.t5675Rec.set(itemIO.getGenarea());
		}
		/*EXIT*/
	}

protected void checkCalcTax5800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5800();
				case skipTr52e5800: 
					skipTr52e5800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5800()
	{
		wsaaTaxamt.set(0);
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.skipTr52e5800);
		}
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		wsaaTr52eCrtable.set(covrmjaIO.getCrtable());
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
	}

protected void skipTr52e5800()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
			txcalcrec.life.set(covrmjaIO.getLife());
			txcalcrec.coverage.set(covrmjaIO.getCoverage());
			txcalcrec.rider.set(covrmjaIO.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covrmjaIO.getCrtable());
			txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
			txcalcrec.register.set(chdrmjaIO.getRegister());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
			wsaaCntCurr.set(chdrmjaIO.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			txcalcrec.amountIn.set(sv.singlePremium);
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(sv.effdate);
			txcalcrec.tranno.set(chdrmjaIO.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				compute(sv.taxamt, 2).set(add(sv.singlePremium, wsaaTaxamt));
				if (isEQ(sv.taxind, SPACES)) {
					sv.taxind.set("+");
				}
				sv.taxamtOut[varcom.nd.toInt()].set("N");
				sv.taxindOut[varcom.nd.toInt()].set("N");
				sv.taxamtOut[varcom.pr.toInt()].set("N");
				sv.taxindOut[varcom.pr.toInt()].set("N");
			}
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void recordToScreen6000()
	{
		fieldsToScreen6020();
		enquiryProtect6030();
		restOfEnquiry6060();
	}

protected void fieldsToScreen6020()
	{
		/* If a summarised record then breakout the Inst.Premium & Sumin*/
		/*   for the number of summarised policies within the Plan, this*/
		/*   is only applicable if a COVT does not already exist in which*/
		/*   case it has already been broken down before.*/
		/* Note - the 1st policy in the summarised record will contain*/
		/*        any division remainder.*/
		if (isLTE(sv.planSuffix, chdrmjaIO.getPolsum())
		&& !plan.isTrue()) {
			if (isEQ(sv.planSuffix, 1)) {
				compute(sv.sumin, 2).set((sub(covrmjaIO.getSumins(), (div(mult(covrmjaIO.getSumins(), (sub(chdrmjaIO.getPolsum(), 1))), chdrmjaIO.getPolsum())))));
				compute(sv.singlePremium, 2).set((sub(covrmjaIO.getInstprem(), (div(mult(covrmjaIO.getInstprem(), (sub(chdrmjaIO.getPolsum(), 1))), chdrmjaIO.getPolsum())))));
				compute(sv.zbinstprem, 2).set((sub(covrmjaIO.getZbinstprem(), (div(mult(covrmjaIO.getZbinstprem(), (sub(chdrmjaIO.getPolsum(), 1))), chdrmjaIO.getPolsum())))));
				compute(sv.zlinstprem, 2).set((sub(covrmjaIO.getZlinstprem(), (div(mult(covrmjaIO.getZlinstprem(), (sub(chdrmjaIO.getPolsum(), 1))), chdrmjaIO.getPolsum())))));
			}
			else {
				compute(sv.sumin, 3).setRounded(div(covrmjaIO.getSumins(), chdrmjaIO.getPolsum()));
				compute(sv.singlePremium, 3).setRounded(div(covrmjaIO.getInstprem(), chdrmjaIO.getPolsum()));
				compute(sv.zbinstprem, 3).setRounded(div(covrmjaIO.getZbinstprem(), chdrmjaIO.getPolsum()));
				compute(sv.zlinstprem, 3).setRounded(div(covrmjaIO.getZlinstprem(), chdrmjaIO.getPolsum()));
			}
		}
		else {
			sv.sumin.set(covrmjaIO.getSumins());
			sv.singlePremium.set(covrmjaIO.getSingp());
			sv.zbinstprem.set(covrmjaIO.getZbinstprem());
			sv.zlinstprem.set(covrmjaIO.getZlinstprem());
		}
		sv.liencd.set(covrmjaIO.getLiencd());
		sv.mortcls.set(covrmjaIO.getMortcls());
		sv.premcess.set(covrmjaIO.getPremCessDate());
		sv.premCessAge.set(covrmjaIO.getPremCessAge());
		sv.premCessTerm.set(covrmjaIO.getPremCessTerm());
		sv.mattcess.set(covrmjaIO.getRiskCessDate());
		sv.matage.set(covrmjaIO.getRiskCessAge());
		sv.mattrm.set(covrmjaIO.getRiskCessTerm());
		sv.polinc.set(chdrmjaIO.getPolinc());
	}

protected void enquiryProtect6030()
	{
		/* Enquiry mode, set the indicator to protect all  input capable*/
		/* fields except the indicators controlling where to switch to*/
		/* next (options and extras indicators).*/
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.matageOut[varcom.pr.toInt()].set("Y");
			sv.mattcessOut[varcom.pr.toInt()].set("Y");
			sv.mattrmOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.premcessOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.singprmOut[varcom.pr.toInt()].set("Y");
			sv.plnsfxOut[varcom.pr.toInt()].set("Y");
			sv.stfundOut[varcom.pr.toInt()].set("Y");
			sv.stsectOut[varcom.pr.toInt()].set("Y");
			sv.effdateOut[varcom.pr.toInt()].set("Y");
			sv.susamtOut[varcom.pr.toInt()].set("Y");
			sv.stssectOut[varcom.pr.toInt()].set("Y");
		}
		/*ALL-THE-REST*/
		/* Load all the tables required for Validation in the 2000-section.*/
		if (!addComp.isTrue()) {
			tableLoads7000();
		}
		/* If options and extras are not allowed non display and protect*/
		/* the fields. Otherwise, read the option and extras details.*/
		if (isEQ(t5551rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext7400();
		}
		/* If reassurance is not allowed non-display and protect the field*/
		/* Otherwise, read the reassurance details for the current coverage*/
		/* IF  T5687-RIIND             =  'N' OR SPACES                 */
		/*     MOVE 'Y'                TO SR579-RATYPIND-OUT (ND)       */
		/* ELSE                                                         */
		/*     PERFORM 7500-CHECK-RACT                                  */
		/* END-IF.                                                      */
		checkAgnt7600();
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
	}

protected void restOfEnquiry6060()
	{
		if (isEQ(sv.sumin, ZERO)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(sv.mortcls, SPACES)) {
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(sv.liencd, SPACES)) {
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(sv.singlePremium, ZERO)) {
			sv.singprmOut[varcom.pr.toInt()].set("Y");
		}
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
	}

protected void tableLoads7000()
	{
		t5671Load7010();
	}

protected void t5671Load7010()
	{
		/* Coverage/Rider Switching.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(wsaaCrtable);
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		/* Match this program with program on T5551.*/
		wsaaValidItem.set("N");
		for (x.set(1); !(isGT(x, 4)
		|| isEQ(wsaaValidItem, "Y")); x.add(1)){
			if (isEQ(t5671rec.pgm[x.toInt()], wsaaProg)) {
				wsbbTran.set(t5671rec.edtitm[x.toInt()]);
				wsaaValidItem.set("Y");
			}
		}
		if (isNE(wsaaValidItem, "Y")) {
			scrnparams.errorCode.set(errorsInner.e302);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*    Read T5551 to obtain the Unit Linked Edit Rules.*/
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setItemtabl(t5551);
		wsbbCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			if (isNE(itdmIO.getValidflag(), "1")) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
		}
		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5551)
		|| isNE(itdmIO.getItemitem(), wsbbTranCurrency)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void mattcess7100()
	{
		para7100();
	}

protected void para7100()
	{
		if (isEQ(sv.matage, ZERO)
		&& isEQ(sv.mattrm, ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.matage, 0), true)
		&& isEQ(isGT(sv.mattrm, 0), false)){
			if (isEQ(t5551rec.eaage, "A")) {
				if (addComp.isTrue()
				|| addApp.isTrue()) {
					datcon2rec.intDate1.set(sv.effdate);
				}
				else {
					datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.matage, wszzAnbAtCcd));
			}
			if (isEQ(t5551rec.eaage, "E")
			|| isEQ(t5551rec.eaage, SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.matage);
			}
		}
		else if (isEQ(isGT(sv.matage, 0), false)
		&& isEQ(isGT(sv.mattrm, 0), true)){
			if (isEQ(t5551rec.eaage, "A")
			|| isEQ(t5551rec.eaage, SPACES)) {
				if (addComp.isTrue()
				|| addApp.isTrue()) {
					datcon2rec.intDate1.set(sv.effdate);
				}
				else {
					datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.mattrm);
			}
			if (isEQ(t5551rec.eaage, "E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.mattrm, wszzAnbAtCcd), 1));
			}
		}
		callDatcon27200();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.mattcessErr.set(datcon2rec.statuz);
		}
		else {
			sv.mattcess.set(datcon2rec.intDate2);
		}
	}

protected void premcess7150()
	{
		para7150();
	}

protected void para7150()
	{
		if (isEQ(sv.premCessAge, ZERO)
		&& isEQ(sv.premCessTerm, ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.premCessAge, 0), true)
		&& isEQ(isGT(sv.premCessTerm, 0), false)){
			if (isGT(t5551rec.eaage, "A")) {
				if (addComp.isTrue()
				|| addApp.isTrue()) {
					datcon2rec.intDate1.set(sv.effdate);
				}
				else {
					datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge, wszzAnbAtCcd));
			}
			if (isEQ(t5551rec.eaage, "E")
			|| isEQ(t5551rec.eaage, SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.premCessAge);
			}
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), true)){
			if (isEQ(t5551rec.eaage, "A")
			|| isEQ(t5551rec.eaage, SPACES)) {
				if (addComp.isTrue()
				|| addApp.isTrue()) {
					datcon2rec.intDate1.set(sv.effdate);
				}
				else {
					datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.premCessTerm);
			}
			if (isEQ(t5551rec.eaage, "E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.premCessTerm, wszzAnbAtCcd), 1));
			}
		}
		callDatcon27200();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.premcessErr.set(datcon2rec.statuz);
		}
		else {
			sv.premcess.set(datcon2rec.intDate2);
		}
	}

protected void callDatcon27200()
	{
		/*PARA*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkDefaults7300()
	{
		para7300();
	}

protected void para7300()
	{
		/* Check the Maximum and Minimum values for the Sum Insured on*/
		/* T5551. If both maximum and minimum values are zero then no*/
		/* entry is allowed so protect and non-display the field and its*/
		/* prompt on the screen.*/
		if (isEQ(t5551rec.sumInsMin, ZERO)
		&& isEQ(t5551rec.sumInsMax, ZERO)) {
			sv.sumin.set(ZERO);
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		/* Check the first entry on T5551 for the 'From' and 'To' values*/
		/* for both Premium Term and Maturity Term. If the 'From' and*/
		/* 'To' values are the same for either Premium Term or Maturity*/
		/* term then protect the corresponding 'Age' and Date fields on*/
		/* the screen, display the value of the term in the Term field*/
		/* and calculate and display the corresponding Cessation Date.*/
		if (isEQ(t5551rec.maturityTermFrom01, t5551rec.maturityTermTo01)) {
			sv.matage.set(ZERO);
			sv.matageOut[varcom.pr.toInt()].set("Y");
			sv.mattrm.set(t5551rec.maturityTermTo01);
			sv.mattrmOut[varcom.pr.toInt()].set("Y");
			datcon2rec.freqFactor.set(t5551rec.maturityTermTo01);
			datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
			callDatcon27200();
			sv.mattcess.set(datcon2rec.intDate2);
			sv.mattcessOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5551rec.premCesstermFrom01, t5551rec.premCesstermTo01)) {
			sv.premCessAge.set(ZERO);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.premCessTerm.set(t5551rec.premCesstermTo01);
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			datcon2rec.freqFactor.set(t5551rec.premCesstermTo01);
			datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
			callDatcon27200();
			sv.premcess.set(datcon2rec.intDate2);
			sv.premcessOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkLext7400()
	{
		readLext7410();
	}

protected void readLext7410()
	{
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lextrevIO.setChdrnum(chdrmjaIO.getChdrnum());
		lextrevIO.setLife(sv.life);
		lextrevIO.setCoverage(sv.coverage);
		lextrevIO.setRider(sv.rider);
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.oK)
		&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), lextrevIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), lextrevIO.getChdrnum())
		|| isNE(sv.life, lextrevIO.getLife())
		|| isNE(sv.coverage, lextrevIO.getCoverage())
		|| isNE(sv.rider, lextrevIO.getRider())) {
			lextrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextrevIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
		/* Extra check in here to see if any LEXT records associated with*/
		/*  this component have been amended whilst we have been here in*/
		/*  component Add/Modify - check TRANNOs on LEXTREV records*/
		/*  against the current TRANNO held on the Contract Header we*/
		/*  are currently using in the CHDRMJA logical.*/
		if (isNE(lextrevIO.getStatuz(), varcom.endp)) {
			if (isGT(lextrevIO.getTranno(), chdrmjaIO.getTranno())) {
				lextrevIO.setStatuz(varcom.endp);
			}
		}
		while ( !(isEQ(lextrevIO.getStatuz(), varcom.endp))) {
			lextrevIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, lextrevIO);
			if (isNE(lextrevIO.getStatuz(), varcom.oK)
			&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(lextrevIO.getParams());
				syserrrec.statuz.set(lextrevIO.getStatuz());
				fatalError600();
			}
			if (isNE(chdrmjaIO.getChdrcoy(), lextrevIO.getChdrcoy())
			|| isNE(chdrmjaIO.getChdrnum(), lextrevIO.getChdrnum())
			|| isNE(sv.life, lextrevIO.getLife())
			|| isNE(sv.coverage, lextrevIO.getCoverage())
			|| isNE(sv.rider, lextrevIO.getRider())) {
				lextrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isGT(lextrevIO.getTranno(), chdrmjaIO.getTranno())) {
					lextrevIO.setStatuz(varcom.endp);
				}
			}
		}
		
	}

	/**
	* <pre>
	*7500-CHECK-RACT SECTION.                                         
	*7500-READ-RACT.                                                  
	**Read the reassurance details for the current coverage/rider. If 
	**any records exist, put a '+' in the reassurance indicator       
	**(to show that there is one).                                    
	**** MOVE SPACES                 TO RACT-PARAMS.                  
	**** MOVE CHDRMJA-CHDRCOY        TO RACT-CHDRCOY.                 
	**** MOVE CHDRMJA-CHDRNUM        TO RACT-CHDRNUM.                 
	**** MOVE SR579-LIFE             TO RACT-LIFE.                    
	**** MOVE SR579-COVERAGE         TO RACT-COVERAGE.                
	**** MOVE SR579-RIDER            TO RACT-RIDER.                   
	**** MOVE RACTREC                TO RACT-FORMAT.                  
	**** MOVE BEGN                   TO RACT-FUNCTION.                
	**** CALL 'RACTIO'            USING RACT-PARAMS.                  
	**** IF  RACT-STATUZ        NOT  =  O-K AND                       
	****     RACT-STATUZ        NOT  =  ENDP                          
	****     MOVE RACT-PARAMS        TO SYSR-PARAMS                   
	****     PERFORM 600-FATAL-ERROR                                  
	**** END-IF.                                                      
	**** IF  CHDRMJA-CHDRCOY    NOT  =  RACT-CHDRCOY  OR              
	****     CHDRMJA-CHDRNUM    NOT  =  RACT-CHDRNUM  OR              
	****     SR579-LIFE         NOT  =  RACT-LIFE     OR              
	****     SR579-COVERAGE     NOT  =  RACT-COVERAGE OR              
	****     SR579-RIDER        NOT  =  RACT-RIDER                    
	****     MOVE ENDP               TO RACT-STATUZ                   
	**** END-IF.                                                      
	**** IF  RACT-STATUZ             =  ENDP                          
	****     MOVE ' '                TO SR579-RATYPIND                
	**** ELSE                                                         
	****     MOVE '+'                TO SR579-RATYPIND                
	**** END-IF.                                                      
	**Extra check in here to see if any RACT records associated with  
	***this component have been amended whilst we have been here in   
	***component Add/Modify - check TRANNOs on RACTRCC records        
	***against the current TRANNO held on the Contract Header we      
	***are currently using in the CHDRMJA logical.                    
	**** IF  RACT-STATUZ        NOT  =  ENDP                          
	****     IF  RACT-TRANNO         >  CHDRMJA-TRANNO                
	****         MOVE ENDP           TO RACT-STATUZ                   
	****         GO TO 7590-EXIT                                      
	****     END-IF                                                   
	**** END-IF.                                                      
	**** MOVE SPACES                 TO RACTRCC-PARAMS.               
	**** MOVE CHDRMJA-CHDRCOY        TO RACTRCC-CHDRCOY.              
	**** MOVE CHDRMJA-CHDRNUM        TO RACTRCC-CHDRNUM.              
	**** MOVE SR579-LIFE             TO RACTRCC-LIFE.                 
	**** MOVE SR579-COVERAGE         TO RACTRCC-COVERAGE.             
	**** MOVE SR579-RIDER            TO RACTRCC-RIDER.                
	**** MOVE SPACES                 TO RACTRCC-RASNUM.               
	**** MOVE SPACES                 TO RACTRCC-RATYPE.               
	**** MOVE SPACES                 TO RACTRCC-VALIDFLAG.            
	**** MOVE RACTRCCREC             TO RACTRCC-FORMAT.               
	**** MOVE BEGN                   TO RACTRCC-FUNCTION.             
	**** CALL 'RACTRCCIO'         USING RACTRCC-PARAMS.               
	**** IF  RACTRCC-STATUZ     NOT  =  O-K  AND                      
	****     RACTRCC-STATUZ     NOT  =  ENDP                          
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 600-FATAL-ERROR                                  
	**** END-IF.                                                      
	**** IF  CHDRMJA-CHDRCOY    NOT  =  RACTRCC-CHDRCOY  OR           
	****     CHDRMJA-CHDRNUM    NOT  =  RACTRCC-CHDRNUM  OR           
	****     SR579-LIFE         NOT  =  RACTRCC-LIFE     OR           
	****     SR579-COVERAGE     NOT  =  RACTRCC-COVERAGE OR           
	****     SR579-RIDER        NOT  =  RACTRCC-RIDER                 
	****     MOVE ENDP               TO RACTRCC-STATUZ                
	**** END-IF.                                                      
	**** PERFORM                  UNTIL RACTRCC-STATUZ = ENDP         
	****     MOVE NEXTR              TO RACTRCC-FUNCTION              
	****     CALL 'RACTRCCIO'     USING RACTRCC-PARAMS                
	****     IF  RACTRCC-STATUZ  NOT =  O-K   AND                     
	****         RACTRCC-STATUZ  NOT =  ENDP                          
	****         MOVE RACTRCC-PARAMS TO SYSR-PARAMS                   
	****         MOVE RACTRCC-STATUZ TO SYSR-STATUZ                   
	****         PERFORM 600-FATAL-ERROR                              
	****     END-IF                                                   
	****     IF  CHDRMJA-CHDRCOY NOT =  RACTRCC-CHDRCOY   OR          
	****         CHDRMJA-CHDRNUM NOT =  RACTRCC-CHDRNUM   OR          
	****         SR579-LIFE      NOT =  RACTRCC-LIFE      OR          
	****         SR579-COVERAGE  NOT =  RACTRCC-COVERAGE  OR          
	****         SR579-RIDER     NOT =  RACTRCC-RIDER                 
	****         MOVE ENDP           TO RACTRCC-STATUZ                
	****     ELSE                                                     
	****         IF  RACTRCC-TRANNO  >  CHDRMJA-TRANNO                
	****             MOVE ENDP       TO RACTRCC-STATUZ                
	****         END-IF                                               
	****     END-IF                                                   
	**** END-PERFORM.                                                 
	*7590-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void checkAgnt7600()
	{
		readPcdtmja7600();
	}

protected void readPcdtmja7600()
	{
		pcdtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		pcdtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(covrmjaIO.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			sv.comind.set(" ");
		}
		else {
			sv.comind.set("+");
		}
	}

protected void readLife18000()
	{
		/*PARA*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readJointLife8100()
	{
		/*PARA*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			lifemjaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void calcAge8200()
	{
		/*INIT*/
		/* Routine to calculate Age next/nearest/last birthday*/
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
		agecalcrec.intDate1.set(lifemjaIO.getCltdob());
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaWorkingAnb.set(agecalcrec.agerating);
		/*EXIT*/
	}

protected void a100CheckLimit()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		chkrlrec.clntnum.set(sv.lifenum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.currcd);
		chkrlrec.cnttype.set(chdrmjaIO.getCnttype());
		chkrlrec.crtable.set(covtmjaIO.getCrtable());
		chkrlrec.sumins.set(sv.sumin);
		chkrlrec.life.set(covtmjaIO.getLife());
		chkrlrec.chdrnum.set(chdrmjaIO.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz, varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.suminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e302 = new FixedLengthStringData(4).init("E302");
	private FixedLengthStringData e366 = new FixedLengthStringData(4).init("E366");
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData i152 = new FixedLengthStringData(4).init("I152");
	private FixedLengthStringData e751 = new FixedLengthStringData(4).init("E751");
	private FixedLengthStringData f665 = new FixedLengthStringData(4).init("F665");
	private FixedLengthStringData e983 = new FixedLengthStringData(4).init("E983");
	private FixedLengthStringData f008 = new FixedLengthStringData(4).init("F008");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h359 = new FixedLengthStringData(4).init("H359");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData u029 = new FixedLengthStringData(4).init("U029");
	private FixedLengthStringData f990 = new FixedLengthStringData(4).init("F990");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
}
}
