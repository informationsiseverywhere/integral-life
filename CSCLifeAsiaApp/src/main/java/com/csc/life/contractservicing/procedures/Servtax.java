/*
 * File: Servtax.java
 * Date: December 3, 2013 3:55:10 AM ICT
 * Author: CSC
 * 
 * Class transformed from SERVTAX.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Tr52frec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(C) Copyright CSC Corporation Limited 1986 - 2000.
 *    All rights reserved. CSC Confidential.
 *
 *REMARKS.
 *   This subroutine will be called whenever necessary to return
 *   the tax amounts. (This subroutine should use the linkage
 *   copybook of TXCALCREC). The tax will be calculated and based
 *   on the amount and the tax category passed in this subroutine.
 *
 *   This routine will cater 2 types of taxes i.e the Service Tax
 *   and the Education Cess.
 *
 *   The following functions will be passed for this routine;
 *     CALC - When this function is passed the subroutine will
 *            calculate the taxes on the input amount.
 *     POST - When this function is passed the routine will create
 *            the accounting entries. Skip the calculation of Tax
 *            and will post the amounts provided in the TXCL-TAXAMT .
 *     CPST - When this function is passed the subroutine will
 *            calculate the taxes on the input amount and will
 *            create the accounting entries.
 *
 *     The following statuses will be returned to the calling
 *         program as follows:
 *
 *     **** - The subroutine has successfully processed the
 *            function.
 *     BOMB - Has encountered a system error during the process.
 *
 *     F109 - No rates can be found.
 *
 *
 *
 ****************************************************************** ****
 * </pre>
 */
public class Servtax extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("SERVTAX");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaItmitm = new FixedLengthStringData(8);
	private int wsaaCount = 0;
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaBatckey = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaPfx = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 0);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaBatckey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaBatckey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaBatckey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaBatckey, 14);
	/* FORMATS */
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String covrenqrec = "COVRENQREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String tr52e = "TR52E";
	private static final String tr52f = "TR52F";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
//	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
//	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52erec tr52erec = new Tr52erec();
	private Tr52frec tr52frec = new Tr52frec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private ExternalisedRules er = new ExternalisedRules();

	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Descpf descpf;
	
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);

	/**
	 * Contains all possible labels used by goTo action.
	 */

	public Servtax() {
		super();
	}



	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		txcalcrec.linkRec = convertAndSetParam(txcalcrec.linkRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startSubr010()
	{
		para010();
		exit010();
	}

	protected void para010()
	{
		txcalcrec.statuz.set("****");
		syserrrec.subrname.set(wsaaProg);
		wsaaItmitm.set(SPACES);
		if (isEQ(txcalcrec.register, SPACES)) {
			readChdrenq100();
		}
		if (isEQ(txcalcrec.taxrule, SPACES)) {
			taxruleCrit200();
		}
		mainProc400();
	}

	protected void exit010()
	{
		exitProgram();
	}

	protected void readChdrenq100()
	{
		Chdrpf chdrenqIO = chdrpfDAO.getChdrenqRecord(txcalcrec.chdrcoy.toString(), txcalcrec.chdrnum.toString());
		if (chdrenqIO == null) {
			syserrrec.params.set(txcalcrec.chdrnum);
			syserrrec.statuz.set("MRNF");
			databaseError9500();
		}
		txcalcrec.register.set(chdrenqIO.getReg());
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
	}

	protected void taxruleCrit200()
	{
		start200();
	}

	protected void start200()
	{
		if (isEQ(txcalcrec.crtable, SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(txcalcrec.txcode, SPACES);
			stringVariable1.addExpression(txcalcrec.cnttype, SPACES);
			stringVariable1.addExpression("****", SPACES);
			stringVariable1.setStringInto(wsaaItmitm);
			readTr52e300();
			if (isEQ(tr52erec.tr52eRec, SPACES)) {
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression(txcalcrec.txcode, SPACES);
				stringVariable2.addExpression("***", SPACES);
				stringVariable2.addExpression("****", SPACES);
				stringVariable2.setStringInto(wsaaItmitm);
				readTr52e300();
			}
		}
		if (isNE(txcalcrec.crtable, SPACES)) {
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression(txcalcrec.txcode, SPACES);
			stringVariable3.addExpression(txcalcrec.cnttype, SPACES);
			stringVariable3.addExpression(txcalcrec.crtable, SPACES);
			stringVariable3.setStringInto(wsaaItmitm);
			readTr52e300();
			if (isEQ(tr52erec.tr52eRec, SPACES)) {
				StringUtil stringVariable4 = new StringUtil();
				stringVariable4.addExpression(txcalcrec.txcode, SPACES);
				stringVariable4.addExpression(txcalcrec.cnttype, SPACES);
				stringVariable4.addExpression("****", SPACES);
				stringVariable4.setStringInto(wsaaItmitm);
				readTr52e300();
				if (isEQ(tr52erec.tr52eRec, SPACES)) {
					StringUtil stringVariable5 = new StringUtil();
					stringVariable5.addExpression(txcalcrec.txcode, SPACES);
					stringVariable5.addExpression("***", SPACES);
					stringVariable5.addExpression("****", SPACES);
					stringVariable5.setStringInto(wsaaItmitm);
					readTr52e300();
				}
			}
		}
		txcalcrec.taxrule.set(wsaaItmitm);
		StringUtil stringVariable6 = new StringUtil();
		stringVariable6.addExpression(txcalcrec.ccy, SPACES);
		stringVariable6.addExpression(tr52erec.txitem, SPACES);
		stringVariable6.setStringInto(txcalcrec.rateItem);
	}

	protected void readTr52e300()
	{
		read300();
	}

	protected void read300()
	{
		List<Itempf> itemList = null;
		itemList = itempfDAO.getAllItemitemByDateFrm("IT",txcalcrec.chdrcoy.toString(),tr52e,wsaaItmitm.toString(), txcalcrec.effdate.toInt());
		if(itemList.size() > 0)
		tr52erec.tr52eRec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
	}

	protected void mainProc400()
	{
		if (isEQ(wsaaItmitm, SPACES)) {
			wsaaItmitm.set(txcalcrec.taxrule);
			readTr52e300();
		}
		if (isEQ(txcalcrec.transType, "PREM")){
			wsaaCount = 1;
		}
		else if (isEQ(txcalcrec.transType, "CNTF")){
			wsaaCount = 2;
		}
		else if (isEQ(txcalcrec.transType, "SURF")){
			wsaaCount = 3;
		}
		else if (isEQ(txcalcrec.transType, "COMM")){
			wsaaCount = 4;
		}
		else if (isEQ(txcalcrec.transType, "NINV")){
			wsaaCount = 5;
		}
		else if (isEQ(txcalcrec.transType, "MCHG")){
			wsaaCount = 6;
		}
		else if (isEQ(txcalcrec.transType, "PERF")){
			wsaaCount = 7;
		}
		else if (isEQ(txcalcrec.transType, "ISSF")){
			wsaaCount = 8;
		}
		else if (isEQ(txcalcrec.transType, "ADMF")){
			wsaaCount = 9;
		}
		else if (isEQ(txcalcrec.transType, "TOPF")){
			wsaaCount = 10;
		}
		else if (isEQ(txcalcrec.transType, "TOPA")){
			wsaaCount = 10;
		}
		else if (isEQ(txcalcrec.transType, "SWCF")){
			wsaaCount = 11;
		}
		else if (isEQ(txcalcrec.transType, "RSTF")){
			wsaaCount = 12;
		}
		else{
			wsaaCount = 0;
		}
		if ((wsaaCount != 0
				&& isNE(tr52erec.taxind[wsaaCount], "Y"))
				|| wsaaCount == 0) {
			txcalcrec.taxAmt[1].set(0);
			txcalcrec.taxAmt[2].set(0);
			return;
		}
		readTr52f500();
		calcCpst400();
		return;
	}

	protected void calcCpst400()
	{
		if (isEQ(txcalcrec.function, "CALC")
				|| isEQ(txcalcrec.function, "CPST")) {
			if (isEQ(txcalcrec.transType, "PREM")
					&& isEQ(txcalcrec.amountIn, 0)) {
				readCovrenq600();
			}
			//IVE-868 LIFE Service Tax Calculation Rework- Integration with latest PA compatible models Starts
			//ILIFE-3035 TWL:Unable to perform Fast Track issue transaction, getting system software error
			//ILIFE-3389 SUM Screen bomb (Updated 1 line)
			//ILIFE-7312
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable()  && er.isCallExternal("VPMSERVTAX") && isNE(txcalcrec.ccy, SPACES) && er.isExternalized(txcalcrec.cnttype.toString(), txcalcrec.crtable.toString()))) 
			{
				for (ix.set(1); !(isGT(ix, 5)); ix.add(1)){
					if (isGTE(tr52frec.dtyamt[ix.toInt()], txcalcrec.amountIn)) {
						if (isNE(tr52frec.txratea[ix.toInt()], 0)) {
							compute(txcalcrec.taxAmt[1], 3).setRounded(div(mult(txcalcrec.amountIn, tr52frec.txratea[ix.toInt()]), 100));
						}
						if (isEQ(tr52frec.txratea[ix.toInt()], 0)) {
							txcalcrec.taxAmt[1].set(tr52frec.txfxamta[ix.toInt()]);
						}
						txcalcrec.taxType[1].set(tr52frec.txtype[1]);
						txcalcrec.taxAbsorb[1].set(tr52frec.txabsind[1]);
						if (isNE(tr52frec.txrateb[ix.toInt()], 0)) {
							compute(txcalcrec.taxAmt[2], 3).setRounded(div(mult(txcalcrec.amountIn, tr52frec.txrateb[ix.toInt()]), 100));
						}
						if (isEQ(tr52frec.txrateb[ix.toInt()], 0)) {
							txcalcrec.taxAmt[2].set(tr52frec.txfxamtb[ix.toInt()]);
						}
						if (isNE(txcalcrec.taxAmt[1], ZERO)
								&& isNE(txcalcrec.ccy, SPACES)) {
							zrdecplrec.amountIn.set(txcalcrec.taxAmt[1]);
							a000CallRounding();
							txcalcrec.taxAmt[1].set(zrdecplrec.amountOut);
						}
						if (isNE(txcalcrec.taxAmt[2], ZERO)
								&& isNE(txcalcrec.ccy, SPACES)) {
							zrdecplrec.amountIn.set(txcalcrec.taxAmt[2]);
							a000CallRounding();
							txcalcrec.taxAmt[2].set(zrdecplrec.amountOut);
						}
						txcalcrec.taxType[2].set(tr52frec.txtype[2]);
						txcalcrec.taxAbsorb[2].set(tr52frec.txabsind[2]);
						ix.set(5);
					}
				}
			}
			else
			{
				/*ILIFE-3271 Code Promotion for VPMS externalization of LIFE Service Tax calculations Start*/
				//ILIFE-6006 start
				txcalcrec.vpmtaxrule.set("");
				txcalcrec.txcode.set("S");
				txcalcrec.item.set(txcalcrec.ccy);
				/*ILIFE-3271 End*/
				callProgram("VPMSERVTAX", txcalcrec.linkRec);
				//ILIFE-6006 end
			}
			//IVE-868 LIFE Service Tax Calculation Rework- Integration with latest PA compatible models end			
		}
		if (isEQ(txcalcrec.function, "POST")
				|| isEQ(txcalcrec.function, "CPST")) {
			/*NEXT_SENTENCE*/
		}
		else {
			return;
		}
		readT5645700();
		readT5688800();
		if (isEQ(t5688rec.comlvlacc, "Y")
				&& isNE(txcalcrec.life, SPACES)
				&& isNE(txcalcrec.coverage, SPACES)
				&& isNE(txcalcrec.rider, SPACES)) {
			wsaaRldgChdrnum.set(txcalcrec.chdrnum);
			wsaaRldgLife.set(txcalcrec.life);
			wsaaRldgCoverage.set(txcalcrec.coverage);
			wsaaRldgRider.set(txcalcrec.rider);
			wsaaPlan.set(txcalcrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
		}
		else {
			wsaaRldgacct.set(SPACES);
			wsaaRldgacct.set(txcalcrec.chdrnum);
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			if (isEQ(txcalcrec.cntTaxInd, "Y")) {
				contractLevelPosting400();
				return;
			}
		}
		else {
			contractLevelPosting400();
			return;
		}

		componentLevelPosting400();
		return;
	}

	protected void componentLevelPosting400()
	{
		lifacmvrec.function.set("PSTW");
		lifacmvrec.origamt.set(txcalcrec.taxAmt[1]);
		lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
		txcalcrec.taxSacstyp[1].set(t5645rec.sacstype[1]);
		lifacmvrec.glcode.set(t5645rec.glmap[1]);
		lifacmvrec.glsign.set(t5645rec.sign[1]);
		lifacmvrec.contot.set(t5645rec.cnttot[1]);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[1].set(txcalcrec.cnttype);
		lifacmvrec.substituteCode[6].set(txcalcrec.crtable);
		writeAcmv900();
		lifacmvrec.function.set("PSTW");
		lifacmvrec.origamt.set(txcalcrec.taxAmt[2]);
		lifacmvrec.sacscode.set(t5645rec.sacscode[2]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[2]);
		txcalcrec.taxSacstyp[2].set(t5645rec.sacstype[2]);
		lifacmvrec.glcode.set(t5645rec.glmap[2]);
		lifacmvrec.glsign.set(t5645rec.sign[2]);
		lifacmvrec.contot.set(t5645rec.cnttot[2]);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[1].set(txcalcrec.cnttype);
		lifacmvrec.substituteCode[6].set(txcalcrec.crtable);
		writeAcmv900();
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			lifacmvrec.function.set("PSTW");
			lifacmvrec.origamt.set(txcalcrec.taxAmt[1]);
			lifacmvrec.sacscode.set(t5645rec.sacscode[5]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[5]);
			lifacmvrec.glcode.set(t5645rec.glmap[5]);
			lifacmvrec.glsign.set(t5645rec.sign[5]);
			lifacmvrec.contot.set(t5645rec.cnttot[5]);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(txcalcrec.cnttype);
			lifacmvrec.substituteCode[6].set(txcalcrec.crtable);
			writeAcmv900();
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			lifacmvrec.function.set("PSTW");
			lifacmvrec.origamt.set(txcalcrec.taxAmt[2]);
			lifacmvrec.sacscode.set(t5645rec.sacscode[6]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[6]);
			lifacmvrec.glcode.set(t5645rec.glmap[6]);
			lifacmvrec.glsign.set(t5645rec.sign[6]);
			lifacmvrec.contot.set(t5645rec.cnttot[6]);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(txcalcrec.cnttype);
			lifacmvrec.substituteCode[6].set(txcalcrec.crtable);
			writeAcmv900();
		}
		return;
	}

	protected void contractLevelPosting400()
	{
		lifacmvrec.function.set("PSTW");
		lifacmvrec.origamt.set(txcalcrec.taxAmt[1]);
		lifacmvrec.sacscode.set(t5645rec.sacscode[3]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[3]);
		txcalcrec.taxSacstyp[1].set(t5645rec.sacstype[3]);
		lifacmvrec.glcode.set(t5645rec.glmap[3]);
		lifacmvrec.glsign.set(t5645rec.sign[3]);
		lifacmvrec.contot.set(t5645rec.cnttot[3]);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[1].set(txcalcrec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		writeAcmv900();
		lifacmvrec.function.set("PSTW");
		lifacmvrec.origamt.set(txcalcrec.taxAmt[2]);
		lifacmvrec.sacscode.set(t5645rec.sacscode[4]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[4]);
		txcalcrec.taxSacstyp[2].set(t5645rec.sacstype[4]);
		lifacmvrec.glcode.set(t5645rec.glmap[4]);
		lifacmvrec.glsign.set(t5645rec.sign[4]);
		lifacmvrec.contot.set(t5645rec.cnttot[4]);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[1].set(txcalcrec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		writeAcmv900();
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			lifacmvrec.function.set("PSTW");
			lifacmvrec.origamt.set(txcalcrec.taxAmt[1]);
			lifacmvrec.sacscode.set(t5645rec.sacscode[7]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[7]);
			lifacmvrec.glcode.set(t5645rec.glmap[7]);
			lifacmvrec.glsign.set(t5645rec.sign[7]);
			lifacmvrec.contot.set(t5645rec.cnttot[7]);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.rldgacct.set(txcalcrec.chdrnum);
			lifacmvrec.substituteCode[1].set(txcalcrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv900();
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			lifacmvrec.function.set("PSTW");
			lifacmvrec.origamt.set(txcalcrec.taxAmt[2]);
			lifacmvrec.sacscode.set(t5645rec.sacscode[8]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[8]);
			lifacmvrec.glcode.set(t5645rec.glmap[8]);
			lifacmvrec.glsign.set(t5645rec.sign[8]);
			lifacmvrec.contot.set(t5645rec.cnttot[8]);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(txcalcrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv900();
		}
	}

	protected void readTr52f500()
	{
		read500();
	}

	protected void read500()
	{

		List<Itempf> itemList = null;
		itemList = itempfDAO.getAllItemitemByDateFrm("IT",txcalcrec.chdrcoy.toString(),tr52f,txcalcrec.rateItem.toString(), txcalcrec.effdate.toInt());
		if(itemList.size() > 0)
		tr52frec.tr52fRec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));

	}

	protected void readCovrenq600()
	{
		Covrpf covrenqIO = new Covrpf();
		covrenqIO.setChdrcoy(txcalcrec.chdrcoy.toString());
		covrenqIO.setChdrnum(txcalcrec.chdrnum.toString());
		covrenqIO.setLife(txcalcrec.life.toString());
		covrenqIO.setCoverage(txcalcrec.coverage.toString());
		covrenqIO.setRider(txcalcrec.rider.toString());
		covrenqIO.setPlanSuffix(0);
		Covrpf covr = covrpfDAO.readCovrenqData(covrenqIO);
		
		if (covr == null) {
			txcalcrec.amountIn.set(0);
			return ;
		}
		if (covr.getInstprem().compareTo(BigDecimal.ZERO) == 0) {
			txcalcrec.amountIn.set(covr.getSingp());
		}
		else {
			if (isEQ(tr52erec.zbastyp, "Y")) {
				compute(txcalcrec.amountIn, 2).set(covr.getInstprem().subtract(covr.getZlinstprem()));
			}
			else {
				txcalcrec.amountIn.set(covr.getInstprem());
			}
		}
	}


	protected void readT5645700()
	{
		read700();
	}

	protected void read700()
	{

		List<Itempf> itemList = null;
		itemList = itempfDAO.getAllItemitemByDateFrm("IT",txcalcrec.chdrcoy.toString(),t5645,wsaaProg.toString(), txcalcrec.effdate.toInt());
		if(itemList.size() > 0) 
		t5645rec.t5645Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));


		descpf=descDAO.getdescData("IT", t5645, wsaaProg.toString(), txcalcrec.chdrcoy.toString(), txcalcrec.language.toString());
		if (null==descpf){ 
			descpf = new Descpf();
			descpf.setLongdesc("??????????????????????????????");
		}
	}

	protected void readT5688800()
	{
		read800();
	}

	protected void read800()
	{


		List<Itempf> itemList = null;
		itemList = itempfDAO.getAllItemitemByDateFrm("IT",txcalcrec.chdrcoy.toString(),t5688,txcalcrec.cnttype.toString(), txcalcrec.effdate.toInt());
		if(itemList.size() > 0) 
		t5688rec.t5688Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
	}

	protected void writeAcmv900()
	{
		read900();
	}

	protected void read900()
	{
		if (isEQ(lifacmvrec.origamt, 0)) {
			return ;
		}
		lifacmvrec.crate.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.user.set(0);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(txcalcrec.effdate);
		txcalcrec.jrnseq.add(1);
		lifacmvrec.batckey.set(txcalcrec.batckey);
		lifacmvrec.jrnseq.set(txcalcrec.jrnseq);
		lifacmvrec.tranno.set(txcalcrec.tranno);
		lifacmvrec.rdocnum.set(txcalcrec.chdrnum);
		lifacmvrec.batccoy.set(txcalcrec.chdrcoy);
		lifacmvrec.rldgcoy.set(txcalcrec.chdrcoy);
		lifacmvrec.genlcoy.set(txcalcrec.chdrcoy);
		lifacmvrec.trandesc.set(descpf.getLongdesc());
		lifacmvrec.origcurr.set(txcalcrec.ccy);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError9000();
		}
	}

	protected void systemError9000()
	{
		start9000();
		exit9490();
	}

	protected void start9000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		/*    MOVE 1                  TO SYSR-SYSERR-TYPE.                 */
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit9490()
	{
		txcalcrec.statuz.set(varcom.bomb);
		exit010();
	}

	protected void databaseError9500()
	{
		start9500();
		exit9990();
	}

	protected void start9500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit9990()
	{
		txcalcrec.statuz.set(varcom.bomb);
		exit010();
	}

	protected void a000CallRounding()
	{
		a010Call();
	}

	protected void a010Call()
	{
		zrdecplrec.function.set(SPACES);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.company.set(txcalcrec.chdrcoy);
		zrdecplrec.currency.set(txcalcrec.ccy);
		wsaaBatckey.set(txcalcrec.batckey);
		zrdecplrec.batctrcde.set(wsaaBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError9000();
		}
	}
}
