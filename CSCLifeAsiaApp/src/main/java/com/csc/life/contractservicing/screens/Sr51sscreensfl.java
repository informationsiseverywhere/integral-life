package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr51sscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 5;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {3}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 15, 4, 71}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51sScreenVars sv = (Sr51sScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr51sscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr51sscreensfl, 
			sv.Sr51sscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr51sScreenVars sv = (Sr51sScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr51sscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr51sScreenVars sv = (Sr51sScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr51sscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr51sscreensflWritten.gt(0))
		{
			sv.sr51sscreensfl.setCurrentIndex(0);
			sv.Sr51sscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr51sScreenVars sv = (Sr51sScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr51sscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr51sScreenVars screenVars = (Sr51sScreenVars) pv;
			if (screenVars.nonForfeitMethod.getFieldName() == null) {
				screenVars.nonForfeitMethod.setFieldName("nonForfeitMethod");
				screenVars.pumeth.setFieldName("pumeth");
				screenVars.premsubr01.setFieldName("premsubr01");
				screenVars.crstat01.setFieldName("crstat01");
				screenVars.cpstat01.setFieldName("cpstat01");
				screenVars.premsubr04.setFieldName("premsubr04");
				screenVars.planSuffix.setFieldName("planSuffix");
				screenVars.crstat04.setFieldName("crstat04");
				screenVars.cpstat04.setFieldName("cpstat04");
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.sel.setFieldName("sel");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.crtabled.setFieldName("crtabled");
				screenVars.crtable.setFieldName("crtable");
				screenVars.statcde.setFieldName("statcde");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.eror.setFieldName("eror");
				screenVars.sumins.setFieldName("sumins");
				screenVars.instprem.setFieldName("instprem");
			}
			screenVars.nonForfeitMethod.set(dm.getField("nonForfeitMethod"));
			screenVars.pumeth.set(dm.getField("pumeth"));
			screenVars.premsubr01.set(dm.getField("premsubr01"));
			screenVars.crstat01.set(dm.getField("crstat01"));
			screenVars.cpstat01.set(dm.getField("cpstat01"));
			screenVars.premsubr04.set(dm.getField("premsubr04"));
			screenVars.planSuffix.set(dm.getField("planSuffix"));
			screenVars.crstat04.set(dm.getField("crstat04"));
			screenVars.cpstat04.set(dm.getField("cpstat04"));
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.sel.set(dm.getField("sel"));
			screenVars.life.set(dm.getField("life"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.crtabled.set(dm.getField("crtabled"));
			screenVars.crtable.set(dm.getField("crtable"));
			screenVars.statcde.set(dm.getField("statcde"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.eror.set(dm.getField("eror"));
			screenVars.sumins.set(dm.getField("sumins"));
			screenVars.instprem.set(dm.getField("instprem"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr51sScreenVars screenVars = (Sr51sScreenVars) pv;
			if (screenVars.nonForfeitMethod.getFieldName() == null) {
				screenVars.nonForfeitMethod.setFieldName("nonForfeitMethod");
				screenVars.pumeth.setFieldName("pumeth");
				screenVars.premsubr01.setFieldName("premsubr01");
				screenVars.crstat01.setFieldName("crstat01");
				screenVars.cpstat01.setFieldName("cpstat01");
				screenVars.premsubr04.setFieldName("premsubr04");
				screenVars.planSuffix.setFieldName("planSuffix");
				screenVars.crstat04.setFieldName("crstat04");
				screenVars.cpstat04.setFieldName("cpstat04");
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.sel.setFieldName("sel");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.crtabled.setFieldName("crtabled");
				screenVars.crtable.setFieldName("crtable");
				screenVars.statcde.setFieldName("statcde");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.eror.setFieldName("eror");
				screenVars.sumins.setFieldName("sumins");
				screenVars.instprem.setFieldName("instprem");
			}
			dm.getField("nonForfeitMethod").set(screenVars.nonForfeitMethod);
			dm.getField("pumeth").set(screenVars.pumeth);
			dm.getField("premsubr01").set(screenVars.premsubr01);
			dm.getField("crstat01").set(screenVars.crstat01);
			dm.getField("cpstat01").set(screenVars.cpstat01);
			dm.getField("premsubr04").set(screenVars.premsubr04);
			dm.getField("planSuffix").set(screenVars.planSuffix);
			dm.getField("crstat04").set(screenVars.crstat04);
			dm.getField("cpstat04").set(screenVars.cpstat04);
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("sel").set(screenVars.sel);
			dm.getField("life").set(screenVars.life);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("crtabled").set(screenVars.crtabled);
			dm.getField("crtable").set(screenVars.crtable);
			dm.getField("statcde").set(screenVars.statcde);
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("eror").set(screenVars.eror);
			dm.getField("sumins").set(screenVars.sumins);
			dm.getField("instprem").set(screenVars.instprem);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr51sscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr51sScreenVars screenVars = (Sr51sScreenVars)pv;
		screenVars.nonForfeitMethod.clearFormatting();
		screenVars.pumeth.clearFormatting();
		screenVars.premsubr01.clearFormatting();
		screenVars.crstat01.clearFormatting();
		screenVars.cpstat01.clearFormatting();
		screenVars.premsubr04.clearFormatting();
		screenVars.planSuffix.clearFormatting();
		screenVars.crstat04.clearFormatting();
		screenVars.cpstat04.clearFormatting();
		screenVars.screenIndicArea.clearFormatting();
		screenVars.sel.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.crtabled.clearFormatting();
		screenVars.crtable.clearFormatting();
		screenVars.statcde.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.eror.clearFormatting();
		screenVars.sumins.clearFormatting();
		screenVars.instprem.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr51sScreenVars screenVars = (Sr51sScreenVars)pv;
		screenVars.nonForfeitMethod.setClassString("");
		screenVars.pumeth.setClassString("");
		screenVars.premsubr01.setClassString("");
		screenVars.crstat01.setClassString("");
		screenVars.cpstat01.setClassString("");
		screenVars.premsubr04.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.crstat04.setClassString("");
		screenVars.cpstat04.setClassString("");
		screenVars.screenIndicArea.setClassString("");
		screenVars.sel.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.crtabled.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.statcde.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.eror.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.instprem.setClassString("");
	}

/**
 * Clear all the variables in Sr51sscreensfl
 */
	public static void clear(VarModel pv) {
		Sr51sScreenVars screenVars = (Sr51sScreenVars) pv;
		screenVars.nonForfeitMethod.clear();
		screenVars.pumeth.clear();
		screenVars.premsubr01.clear();
		screenVars.crstat01.clear();
		screenVars.cpstat01.clear();
		screenVars.premsubr04.clear();
		screenVars.planSuffix.clear();
		screenVars.crstat04.clear();
		screenVars.cpstat04.clear();
		screenVars.screenIndicArea.clear();
		screenVars.sel.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.crtabled.clear();
		screenVars.crtable.clear();
		screenVars.statcde.clear();
		screenVars.pstatcode.clear();
		screenVars.eror.clear();
		screenVars.sumins.clear();
		screenVars.instprem.clear();
	}
}
