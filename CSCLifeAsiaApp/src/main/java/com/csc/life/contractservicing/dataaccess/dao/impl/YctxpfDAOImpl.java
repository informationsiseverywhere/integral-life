package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.YctxpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Yctxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class YctxpfDAOImpl extends BaseDAOImpl<Yctxpf> implements YctxpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(YctxpfDAOImpl.class);
	
	public Yctxpf fetchRecord(String chdrcoy, String chdrnum) {
		Yctxpf yctxpf = null;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM YCTXPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG='1'");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			rs = ps.executeQuery();
			while(rs.next()) {
				yctxpf = new Yctxpf();
				yctxpf.setChdrcoy(rs.getString("CHDRCOY"));
				yctxpf.setChdrnum(rs.getString("CHDRNUM"));
				yctxpf.setCnttype(rs.getString("CNTTYPE"));
				yctxpf.setValidFlag(rs.getString("VALIDFLAG"));
				yctxpf.setStaxdt(rs.getInt("STAXDT"));
				yctxpf.setStaxdt(rs.getInt("ETAXDT"));
				yctxpf.setTranno(rs.getInt("TRANNO"));
				yctxpf.setBatctrcde(rs.getString("BATCTRCDE"));
				yctxpf.setTotalTaxAmt(rs.getBigDecimal("TOTALTAXAMT"));
				yctxpf.setRpoffset(rs.getBigDecimal("RPOFFSET"));
				yctxpf.setEffDate(rs.getInt("EFFDATE"));
				yctxpf.setNotrcvd(rs.getString("NOTRCVD"));
				yctxpf.setUsrprf(rs.getString("USRPRF"));
				yctxpf.setJobnm(rs.getString("JOBNM"));
			}
		}
		catch(SQLException e) {
			LOGGER.error("fetchRecords()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return yctxpf;
	}
	
	public boolean updateRecord(List<Yctxpf> yctxpf) {
		boolean isUpdated = false;
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE YCTXPF SET VALIDFLAG=? WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG='1'");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Yctxpf yctx : yctxpf) {
				ps.setString(1, yctx.getValidFlag());
				ps.setString(2, yctx.getChdrcoy());
				ps.setString(3, yctx.getChdrnum());
				ps.addBatch();
			}
			if(ps.executeBatch().length > 0) {
				isUpdated = true;
			}
		}
		catch(SQLException e){
			LOGGER.error("updateRecords()", e);
			throw new SQLRuntimeException(e);
		}
		return isUpdated;
	}
	
	public boolean insertRecord(List<Yctxpf> yctxpf) {
		boolean isInserted = false;
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO YCTXPF (CHDRCOY, CHDRNUM, CNTTYPE, VALIDFLAG, STAXDT, ETAXDT, TRANNO, BATCTRCDE, TOTALTAXAMT, RPOFFSET, EFFDATE,NOTRCVD, USRPRF, JOBNM, DATIME)");
		sb.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Yctxpf yctx : yctxpf) {
				int i=0;
				ps.setString(++i, yctx.getChdrcoy());
				ps.setString(++i, yctx.getChdrnum());
				ps.setString(++i, yctx.getCnttype());
				ps.setString(++i, yctx.getValidFlag());
				ps.setInt(++i, yctx.getStaxdt());
				ps.setInt(++i, yctx.getEtaxdt());
				ps.setInt(++i, yctx.getTranno());
				ps.setString(++i, yctx.getBatctrcde());
				ps.setBigDecimal(++i, yctx.getTotalTaxAmt());
				ps.setBigDecimal(++i, yctx.getRpoffset());
				ps.setInt(++i, yctx.getEffDate());
				ps.setString(++i,yctx.getNotrcvd());
				ps.setString(++i, getUsrprf());
				ps.setString(++i, getJobnm());
				ps.setTimestamp(++i, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			if(ps.executeBatch().length > 0) {
				isInserted = true;
			}
		}
		catch(SQLException e) {
			LOGGER.error("insertRecord()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps,null);
		}
		return isInserted;
	}
	
	public Yctxpf fetchRecord(String chdrcoy, String chdrnum, int startdate) {
		Yctxpf yctxpf = null;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM YCTXPF WHERE CHDRCOY=? AND CHDRNUM=? AND STAXDT =? AND VALIDFLAG='1'");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setInt(3, startdate);
			rs = ps.executeQuery();
			while(rs.next()) {
				yctxpf = new Yctxpf();
				yctxpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				yctxpf.setChdrcoy(rs.getString("CHDRCOY"));
				yctxpf.setChdrnum(rs.getString("CHDRNUM"));
				yctxpf.setCnttype(rs.getString("CNTTYPE"));
				yctxpf.setValidFlag(rs.getString("VALIDFLAG"));
				yctxpf.setStaxdt(rs.getInt("STAXDT"));
				yctxpf.setEtaxdt(rs.getInt("ETAXDT"));
				yctxpf.setTranno(rs.getInt("TRANNO"));
				yctxpf.setBatctrcde(rs.getString("BATCTRCDE"));
				yctxpf.setTotalTaxAmt(rs.getBigDecimal("TOTALTAXAMT"));
				yctxpf.setRpoffset(rs.getBigDecimal("RPOFFSET"));
				yctxpf.setEffDate(rs.getInt("EFFDATE"));
				yctxpf.setNotrcvd(rs.getString("NOTRCVD"));
				yctxpf.setUsrprf(rs.getString("USRPRF"));
				yctxpf.setJobnm(rs.getString("JOBNM"));
			}
		}
		catch(SQLException e) {
			LOGGER.error("fetchRecords()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return yctxpf;
	}
	
}
