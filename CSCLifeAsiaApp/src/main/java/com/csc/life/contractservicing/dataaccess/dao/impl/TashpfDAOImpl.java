package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.TashpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Tashpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class TashpfDAOImpl extends BaseDAOImpl<Tashpf> implements TashpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(TashpfDAOImpl.class);
	private static final String SQLForInsertTashsByList = "insert into Tashpf " +
			" (CHDRCOY, CHDRNUM, TRANNO, VALIDFLAG, ASGNPFX, ASGNNUM, SEQNO, COMMFROM," +
			" COMMTO, TRANCDE, USRPRF, JOBNM, DATIME) " +
//			" values ('2', '9999',9,'9','9','9',9,9999,9999,'9','test','test', GETDATE());";
			" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	@Override
	public int insertTashsByList(List<Tashpf> tashpfs) {
		try (PreparedStatement prep = getPrepareStatement(SQLForInsertTashsByList)) {
			for (Tashpf tashpf : tashpfs) {
				prep.setString(1, tashpf.getChdrcoy());
				prep.setString(2, tashpf.getChdrnum());
				prep.setInt(3, tashpf.getTranno());
				prep.setString(4, tashpf.getValidflag());
				prep.setString(5, tashpf.getAsgnpfx());
				prep.setString(6,tashpf.getAsgnnum() != null ? tashpf.getAsgnnum().trim() : " ");
				prep.setInt(7, tashpf.getSeqno());
				prep.setInt(8, tashpf.getCommfrom());
				prep.setInt(9, tashpf.getCommto());
				prep.setString(10, tashpf.getTrancde() != null ? tashpf.getTrancde().trim() : " ");
				prep.setString(11, tashpf.getUsrprf());
				prep.setString(12, tashpf.getJobnm());
				prep.setTimestamp(13, new Timestamp(System.currentTimeMillis()));
				prep.addBatch();
			}
			return prep.executeBatch().length;
		} catch (SQLException e) {
			LOGGER.error("getAsgnpfByChdrNum()", e);
			throw new SQLRuntimeException(e);
		}
	}

}
