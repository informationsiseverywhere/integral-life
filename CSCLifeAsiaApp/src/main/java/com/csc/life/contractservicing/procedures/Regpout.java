/*
 * File: Regpout.java
 * Date: 30 August 2009 2:04:58
 * Author: Quipoz Limited
 * 
 * Class transformed from REGPOUT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.RegpbrkTableDAM;
import com.csc.life.contractservicing.recordstructures.Genoutrec;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpenqTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  This is  a  new  subroutine  which  forms  part  of  the  9405
*  Annuities  Development.    It  will  be  called  from T5671 by
*  BRKOUT, the existing Break Out  subroutine,  when  a  contract
*  with regular payments (REGPs) is being broken out.
*
*  This  processing  is  not  restricted to Annuity contracts, it
*  applies to any contract with regular  payments  that  requires
*  Break Out processing.  The REGP records will be broken down in
*  the  same  proportion  as the component to which they apply is
*  being broken down.
*
*****************************************************************
* </pre>
*/
public class Regpout extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REGPOUT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTotals = new FixedLengthStringData(65);
	private PackedDecimalData wsaaOrigPymt = new PackedDecimalData(17, 2).isAPartOf(wsaaTotals, 0);
	private PackedDecimalData wsaaBrkPymt = new PackedDecimalData(17, 2).isAPartOf(wsaaTotals, 9);
	private PackedDecimalData wsaaTotalPymt = new PackedDecimalData(17, 2).isAPartOf(wsaaTotals, 18);
	private PackedDecimalData wsaaOrigTotamnt = new PackedDecimalData(18, 2).isAPartOf(wsaaTotals, 27);
	private PackedDecimalData wsaaBrkTotamnt = new PackedDecimalData(18, 2).isAPartOf(wsaaTotals, 37);
	private PackedDecimalData wsaaTotalTotamnt = new PackedDecimalData(18, 2).isAPartOf(wsaaTotals, 47);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTotals, 57);
	private ZonedDecimalData wsaaNextRgpynum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTotals, 60).setUnsigned();
		/* FORMATS */
	private static final String regprec = "REGPREC";
	private static final String regpbrkrec = "REGPBRKREC";
	private static final String regpenqrec = "REGPENQREC";
	private static final String chdrmatrec = "CHDRMATREC";
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private RegpbrkTableDAM regpbrkIO = new RegpbrkTableDAM();
	private RegpenqTableDAM regpenqIO = new RegpenqTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Genoutrec genoutrec = new Genoutrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Regpout() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		genoutrec.outRec = convertAndSetParam(genoutrec.outRec, parmArray, 0);
		try {
			initialise1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		/*INIT*/
		genoutrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaTotals.set(ZERO);
		/* Read contract header to obtain the current transaction*/
		/* number.*/
		chdrmatIO.setChdrcoy(genoutrec.chdrcoy);
		chdrmatIO.setChdrnum(genoutrec.chdrnum);
		chdrmatIO.setFormat(chdrmatrec);
		chdrmatIO.setFunction("READR");
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
		/* Read the REGP file to obtain the first attached REGP record.*/
		regpIO.setDataArea(SPACES);
		regpIO.setChdrcoy(genoutrec.chdrcoy);
		regpIO.setChdrnum(genoutrec.chdrnum);
		regpIO.setLife(ZERO);
		regpIO.setCoverage(ZERO);
		regpIO.setRider(ZERO);
		regpIO.setPlanSuffix(ZERO);
		regpIO.setRgpynum(ZERO);
		regpIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)
		&& isNE(regpIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		if (isNE(regpIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(regpIO.getChdrcoy(), genoutrec.chdrcoy)
		|| isNE(regpIO.getPlanSuffix(), ZERO)) {
			regpIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(regpIO.getStatuz(), varcom.endp))) {
			processRegpbrk2000();
		}
		
		exitProgram();
	}

protected void processRegpbrk2000()
	{
		setup2010();
	}

protected void setup2010()
	{
		wsaaOrigPymt.set(ZERO);
		wsaaBrkPymt.set(ZERO);
		wsaaTotalPymt.set(ZERO);
		wsaaOrigTotamnt.set(ZERO);
		wsaaBrkTotamnt.set(ZERO);
		wsaaTotalTotamnt.set(ZERO);
		wsaaOrigPymt.set(regpIO.getPymt());
		wsaaOrigTotamnt.set(regpIO.getTotamnt());
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix, genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			writeRegpbrkRecord2100();
		}
		writeNewSummary2200();
		/* Read the next REGP record.*/
		regpIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)
		&& isNE(regpIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(regpIO.getStatuz());
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		if (isEQ(regpIO.getStatuz(), varcom.endp)) {
			return ;
		}
		while ( !(isEQ(regpIO.getPlanSuffix(), ZERO)
		|| isEQ(regpIO.getStatuz(), varcom.endp))) {
			getNextRecord2300();
		}
		
	}

protected void writeRegpbrkRecord2100()
	{
		write2100();
	}

protected void write2100()
	{
		compute(wsaaBrkPymt, 3).setRounded(div(wsaaOrigPymt, genoutrec.oldSummary));
		zrdecplrec.amountIn.set(wsaaBrkPymt);
		a000CallRounding();
		wsaaBrkPymt.set(zrdecplrec.amountOut);
		compute(wsaaTotalPymt, 3).setRounded(add(wsaaTotalPymt, wsaaBrkPymt));
		if (isNE(wsaaOrigTotamnt, ZERO)) {
			compute(wsaaBrkTotamnt, 3).setRounded(div(wsaaOrigTotamnt, genoutrec.oldSummary));
			zrdecplrec.amountIn.set(wsaaBrkTotamnt);
			a000CallRounding();
			wsaaBrkTotamnt.set(zrdecplrec.amountOut);
			compute(wsaaTotalTotamnt, 3).setRounded(add(wsaaTotalTotamnt, wsaaBrkTotamnt));
		}
		regpbrkIO.setDataArea(SPACES);
		regpbrkIO.setPymt(wsaaBrkPymt);
		regpbrkIO.setTotamnt(wsaaBrkTotamnt);
		writeRegpbrk2110();
	}

protected void writeRegpbrk2110()
	{
		write2110();
	}

protected void write2110()
	{
		/* Write a REGPBRK record with a new PLAN-SUFFIX and the*/
		/* broken-out values for total amount and payment.*/
		regpbrkIO.setChdrcoy(regpIO.getChdrcoy());
		regpbrkIO.setChdrnum(regpIO.getChdrnum());
		regpbrkIO.setPlanSuffix(wsaaPlanSuffix);
		regpbrkIO.setTranno(chdrmatIO.getTranno());
		regpbrkIO.setLife(regpIO.getLife());
		regpbrkIO.setCoverage(regpIO.getCoverage());
		regpbrkIO.setRider(regpIO.getRider());
		begnRegp2120();
		regpbrkIO.setRgpynum(wsaaNextRgpynum);
		regpbrkIO.setValidflag(regpIO.getValidflag());
		regpbrkIO.setSacscode(regpIO.getSacscode());
		regpbrkIO.setSacstype(regpIO.getSacstype());
		regpbrkIO.setGlact(regpIO.getGlact());
		regpbrkIO.setDebcred(regpIO.getDebcred());
		regpbrkIO.setDestkey(regpIO.getDestkey());
		regpbrkIO.setPaycoy(regpIO.getPaycoy());
		regpbrkIO.setPayclt(regpIO.getPayclt());
		regpbrkIO.setRgpymop(regpIO.getRgpymop());
		regpbrkIO.setRegpayfreq(regpIO.getRegpayfreq());
		regpbrkIO.setCurrcd(regpIO.getCurrcd());
		regpbrkIO.setPrcnt(regpIO.getPrcnt());
		regpbrkIO.setRgpystat(regpIO.getRgpystat());
		regpbrkIO.setPayreason(regpIO.getPayreason());
		regpbrkIO.setClaimevd(regpIO.getClaimevd());
		regpbrkIO.setBankkey(regpIO.getBankkey());
		regpbrkIO.setBankacckey(regpIO.getBankacckey());
		regpbrkIO.setCrtdate(regpIO.getCrtdate());
		regpbrkIO.setAprvdate(regpIO.getAprvdate());
		regpbrkIO.setFirstPaydate(regpIO.getFirstPaydate());
		regpbrkIO.setNextPaydate(regpIO.getNextPaydate());
		regpbrkIO.setLastPaydate(regpIO.getLastPaydate());
		regpbrkIO.setRevdte(regpIO.getRevdte());
		regpbrkIO.setLastPaydate(regpIO.getLastPaydate());
		regpbrkIO.setFinalPaydate(regpIO.getFinalPaydate());
		regpbrkIO.setAnvdate(regpIO.getAnvdate());
		regpbrkIO.setCancelDate(regpIO.getCancelDate());
		regpbrkIO.setRecvdDate(regpIO.getRecvdDate());
		regpbrkIO.setIncurdt(regpIO.getIncurdt());
		regpbrkIO.setRgpytype(regpIO.getRgpytype());
		regpbrkIO.setCrtable(regpIO.getCrtable());
		regpbrkIO.setCertdate(regpIO.getCertdate());
		regpbrkIO.setCurrcd(regpIO.getCurrcd());
		regpbrkIO.setFunction(varcom.writr);
		regpbrkIO.setFormat(regpbrkrec);
		SmartFileCode.execute(appVars, regpbrkIO);
		if (isNE(regpbrkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpbrkIO.getParams());
			fatalError600();
		}
	}

protected void begnRegp2120()
	{
		/*FIND*/
		wsaaNextRgpynum.set(ZERO);
		regpenqIO.setParams(SPACES);
		regpenqIO.setChdrcoy(genoutrec.chdrcoy);
		regpenqIO.setChdrnum(genoutrec.chdrnum);
		regpenqIO.setLife(ZERO);
		regpenqIO.setCoverage(ZERO);
		regpenqIO.setRider(ZERO);
		regpenqIO.setPlanSuffix(ZERO);
		regpenqIO.setRgpynum(99999);
		regpenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		regpenqIO.setFormat(regpenqrec);
		while ( !(isEQ(regpenqIO.getStatuz(), varcom.endp))) {
			findNextRgpynum2130();
		}
		
		compute(wsaaNextRgpynum, 0).set(add(wsaaNextRgpynum, 1));
		/*EXIT*/
	}

protected void findNextRgpynum2130()
	{
		find2135();
	}

protected void find2135()
	{
		SmartFileCode.execute(appVars, regpenqIO);
		if ((isNE(regpenqIO.getStatuz(), varcom.oK))
		&& (isNE(regpenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(regpenqIO.getParams());
			fatalError600();
		}
		if (isEQ(regpenqIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if ((isEQ(genoutrec.chdrcoy, regpenqIO.getChdrcoy()))
		&& (isEQ(genoutrec.chdrnum, regpenqIO.getChdrnum()))) {
			if (isGT(regpenqIO.getRgpynum(), wsaaNextRgpynum)) {
				wsaaNextRgpynum.set(regpenqIO.getRgpynum());
			}
		}
		else {
			regpenqIO.setStatuz(varcom.endp);
		}
		regpenqIO.setFunction(varcom.nextr);
	}

protected void writeNewSummary2200()
	{
		/*WRITE*/
		/* If GEN-NEW-SUMMARY is 1 or 0, delete the summary REGP since*/
		/* the last record has been broken out.*/
		/* Then rewrite the last record, moving in the remainder values*/
		/* of TOTAMNT and PYMT to avoid possible rounding errors from*/
		/* earlier calculations.*/
		if (isLT(genoutrec.newSummary, 2)) {
			lastRecordBrokenOut2220();
		}
		else {
			newSummaryRegp2240();
		}
		/*EXIT*/
	}

protected void lastRecordBrokenOut2220()
	{
		write2230();
	}

protected void write2230()
	{
		regpIO.setFunction(varcom.delet);
		regpIO.setFormat(regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		wsaaPlanSuffix.set(1);
		regpbrkIO.setDataArea(SPACES);
		setPrecision(regpbrkIO.getTotamnt(), 3);
		regpbrkIO.setTotamnt(sub(wsaaOrigTotamnt, wsaaTotalTotamnt), true);
		setPrecision(regpbrkIO.getPymt(), 3);
		regpbrkIO.setPymt(sub(wsaaOrigPymt, wsaaTotalPymt), true);
		writeRegpbrk2110();
	}

protected void newSummaryRegp2240()
	{
		/*WRITE*/
		setPrecision(regpIO.getTotamnt(), 3);
		regpIO.setTotamnt(sub(wsaaOrigTotamnt, wsaaTotalTotamnt), true);
		setPrecision(regpIO.getPymt(), 3);
		regpIO.setPymt(sub(wsaaOrigPymt, wsaaTotalPymt), true);
		regpIO.setFormat(regprec);
		regpIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getNextRecord2300()
	{
		find2310();
	}

protected void find2310()
	{
		/* Read the next REGT record (where PLAN-SUFFIX = 0).*/
		regpIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)
		&& isNE(regpIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		if (isEQ(regpIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Rewrite the unwanted record to release the BEGNH.*/
		if (isNE(regpIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(regpIO.getChdrcoy(), genoutrec.chdrcoy)) {
			regpIO.setFunction(varcom.rewrt);
			regpIO.setFormat(regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			else {
				regpIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isNE(regpIO.getChdrnum(), genoutrec.chdrnum)
		|| isNE(regpIO.getChdrcoy(), genoutrec.chdrcoy)
		|| isNE(regpIO.getPlanSuffix(), ZERO)) {
			regpIO.setFunction(varcom.rewrt);
			regpIO.setFormat(regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					error600();
				case errorProg610: 
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		genoutrec.statuz.set("BOMB");
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(genoutrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(genoutrec.cntcurr);
		zrdecplrec.batctrcde.set(genoutrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A000-EXIT*/
	}
}
