/*
 * File: P6763at.java
 * Date: 30 August 2009 0:56:33
 * Author: Quipoz Limited
 *
 * Class transformed from P6763AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Bchgallrec;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.IncrhstTableDAM;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;

/**
* <pre>
*
*REMARKS.
*       P6763AT - Billing Transaction AT Module
*       ---------------------------------------
* *************************************************************
*       Cloned without processing changes from P6668AT.
* *************************************************************
*   This AT module is called by the Billing Transaction program
*   P6763 and the contract number is passed in the AT
*   parameters area as the 'primary key'.
*
*   PROCESS CONTRACT HEADER
*    Update the contract header as follows:-
*      - set the valid flag to '2' and set the effective
*        date to default to todays date and rewrite the
*        record using the REWRT function.
*      - write a new version of the contract header
*        as follows:-
*      - set the valid flag to '1'
*      - update the transaction number, increment by +1 and
*        update the transaction date using the default of
*        todays date
*      - update the contract status code with the status
*        code from T5679 (keyed by transaction number)
*        only if there is an entry on T5679
*      - update the premium status field if the status
*        field from T5679 has entries. If entries are
*        present, then check the Bill frequency from the
*        the AT parameters. If it is '00', i.e. single
*        premium, otherwise use the regular premium status
*        field
*      - set the 'Effective From' date to todays date
*      - set the 'Effective To' date to VRCM-MAX-DATE
*      - if the billing frequency has changed, you should
*        not be here unless it has changed, set the billing
*        frequency to the value from the AT linkage
*      - if the billing method has changed, set the billing
*        method to the value from the AT linkage
*      - if the billing day has changed, set the billing day
*        of BILLCD to the value from the AT linkage
*      - if the Payor has changed, set to the value passed
*        in AT linkage
*      - if Group number and member number have changed,
*        then set to the value from AT linkage
*      - if Mandate reference has changed, then set to the
*        value from AT linkage
*      - write the new CHDR record using the WRITR function.
*
*   PROCESS COMPONENTS
*
*    Read all the Coverage/Rider records (COVRMJA) for this
*    contract and check their status against T5679 (keyed by
*    Transaction number) and only update the Component if a
*    match exists on T5679. Also, only update the Component if
*    the INSTPREM is greater than 0.
*
*   Update each COVR record as follows:-
*      - BEGNH on the COVR record for the relevant contract,
*
*    DOWHILE there are COVRs for this contract
*      - set the valid flag to '2' and the 'Effective To'
*        date to todays date and UPDAT the record,
*      - calculate the new premium as follows, read T5687
*        using CRTABLE as the item to get the frequency
*        alteration basis which is then used to read T5541
*        Both the old and the new frequencies are read to
*        obtain their respective loading factors.
*        When they are found and no errors appear, the premium
*        is calculated as follows :
*
*        New Premium =    Old Premium
*                       * (Old frequency / New Frequency)
*                       * (New Freq loading factor /
*                          Old Freq loading factor  )
*
*      - update the status code, if an entry exists on
*        T5679. Use the regular premium status code.
*      - write a new COVR record as follows, update the
*        transaction number with the transaction number from
*        the contract header. Set the CURRFROM to BTD
*        set the CURRTO to VRCM-MAX-DATE and set
*        the valid flag to '1'. Write the record with a
*        WRITR function.
*      - Set AGCMBCH-PARAMS to space.
*        Set AGCMBCH-CHDRCOY to COVRMJA-CHDRCOY.
*        Set AGCMBCH-CHDRNUM to COVRMJA-CHDRNUM.
*        Set AGCMBCH-LIFE to COVRMJA-LIFE.
*        Set AGCMBCH-COVERAGE to COVRMJA-COVERAGE.
*        Set AGCMBCH-RIDER to COVRMJA-RIDER.
*        Set AGCMBCH-PLAN-SUFFIX to COVRMJA-PLAN-SUFFIX.
*        Set AGCMBCH-FORMAT to 'AGCMBCHREC'.
*        Set AGCMBCH-FUNCTION to BEGNH.
*    Call 'AGCMBCHIO' to read and hold the record.
*        Perform normal error checking.
*        If end of file
*            next sentence
*        Else
*            If key break(Up to and include plan suffix)
*                Rewrite the record with function 'REWRT'
*                Perform normal error checking
*                Set AGCMBCH-STATUZ to ENDP.
*        Perform PROCESS-READ-AGCMBCH
*            until AGCMBCH-STATUZ = ENDP.
*      - GENERIC SUBROUTINE, access T5671 (keyed by
*        Transaction number and CRTABLE) for each
*        component and obtain the Generic subroutine. If an
*        entry is found, call the relevant program to
*        process the generic components
*      - NEXTR on the COVRs
*     ENDDO
*
*   GENERAL HOUSEKEEPING
*
*   1) Write a PTRN record:
*      - contract key from contract header
*      - transaction number from contract header
*      - transaction effective date is todays date
*   2) Update the batch header by calling BATCUP with a
*      function of WRITS and the following parameters:
*       - Transaction count equals 1
*       - zeroise all amounts
*       - batch key from the AT linkage.
*   3) Release Softlock
*       - Release the 'SFTLOCK' using the UNLK function.
*
*   PROCESS-READ-AGCMBCH Section
*   ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
*   Set AGCMBCH-CURRTO to EFFDATE.
*   Set AGCMBCH-VALIDFLAG to '2'.
*   Set AGCMBCH-FUNCTION to REWRT.
*   Call 'AGCMBCHIO' to rewrite the record.
*   Perform normal error checking.
*   Set AGCMBCH-TRANNO to the new TRANNO.
*   Set AGCMBCH-VALIDFLAG to '1'.
*   Set AGCMBCH-CURRFROM to BTD
*   Set AGCMBCH-CURRTO to 99999999.
*   Compute AGCMBCH-ANNPREM rounded = AGCMBCH-ANNPREM *
*                                  (New loading factor/
*                                   Old loading factor)
*   Note that old factor and new factor must be numeric.
*   Set AGCMBCH-FUNCTION to WRITR.
*   Call 'AGCMBCHIO' to write the new record.
*   Perform normal error checking.
*   Set AGCMBCH-FUNCTION to NEXTR.
*   Call 'AGCMBCHIO' to read and hold the next record.
*   Perform normal error checking.
*   If end of file
*     Next sentence
*   Else÷
*    If key break(Up to and include plan suffix)
*        Rewrite the record with function 'REWRT'
*        Perform normal error checking
*        Set AGCMBCH-STATUZ to ENDP.
*
* A000-STATISTICS SECTION.
* ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ¯¯¯¯¯¯¯
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class P6763at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("P6763AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(16);
	private ZonedDecimalData wsaaTermid = new ZonedDecimalData(4, 0).isAPartOf(wsaaTranid, 0).setUnsigned();
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaBillDate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaBillDate9 = new FixedLengthStringData(8).isAPartOf(wsaaBillDate, 0, REDEFINE);
	private ZonedDecimalData wsaaBillMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillDate9, 4).setUnsigned();
	private ZonedDecimalData wsaaBillDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillDate9, 6).setUnsigned();
	private ZonedDecimalData wsaaBtdate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaBtdate9 = new FixedLengthStringData(8).isAPartOf(wsaaBtdate, 0, REDEFINE);
	private ZonedDecimalData wsaaBtMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBtdate9, 4).setUnsigned();
	private ZonedDecimalData wsaaBtDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBtdate9, 6).setUnsigned();
	private PackedDecimalData wsaaShortTransDate = new PackedDecimalData(6, 0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Mop = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);
		/* FORMATS */
	private static final String ptrnrec = "PTRNREC";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String agcmbchrec = "AGCMBCHREC";
	private static final String payrrec = "PAYRREC";
	private static final String incrhstrec = "INCRHSTREC";
		/* ERRORS */
	private static final String f294 = "F294";
	private static final String f290 = "F290";
	private static final String f151 = "F151";
	private static final String t5541 = "T5541";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t5671 = "T5671";
	private static final String t5688 = "T5688";
	private static final String t5674 = "T5674";
	private static final String t6654 = "T6654";
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private IncrhstTableDAM incrhstIO = new IncrhstTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Varcom varcom = new Varcom();
	private Bchgallrec bchgallrec = new Bchgallrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5679rec t5679rec = new T5679rec();
	private T5541rec t5541rec = new T5541rec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T5674rec t5674rec = new T5674rec();
	private T6654rec t6654rec = new T6654rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Atmodrec atmodrec = new Atmodrec();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private WsaaTransactionRecInner wsaaTransactionRecInner = new WsaaTransactionRecInner();
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	protected Itempf itempf = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		updateStatus386,
		retrieveNextRecord393
	}

	public P6763at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise100();
		processContractHeader200();
		processPayrRecord250();
		getFirstComponent300();
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			processComponent330();
		}

		accumInstpremWrtChdrmja400();
		writeHistoryRlseLocks500();
		a000Statistics();
		/*EXIT*/
		exitProgram();
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxx1ErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxx1ErrorProg()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		initVariables110();
		retrieveContractHeader120();
		convertTransactionDate130();
	}

protected void initVariables110()
	{
		wsaaMiscellaneousInner.wsaaTotInstprem.set(ZERO);
	}

protected void retrieveContractHeader120()
	{
		wsaaTransactionRecInner.wsaaTransactionRec.set(atmodrec.transArea);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(wsaaChdrcoy);
		chdrmjaIO.setChdrnum(wsaaChdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if ((isNE(chdrmjaIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(wsaaChdrcoy);
		payrIO.setChdrnum(wsaaChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if ((isNE(payrIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		covrmjaIO.setParams(SPACES);
		wsaaMiscellaneousInner.sub.set(9);
	}

protected void convertTransactionDate130()
	{
		/* Truncate left two digits of date, the century, to leave YYMMDD**/
		/* format.  WSAA-AT-TRANSACTION-DATE then be in CCYYMMDD format, **/
		/* whilst WSAA-SHORT-TRANS-DATE will be in YYMMDD format.        **/
		wsaaShortTransDate.set(wsaaTransactionRecInner.wsaaAtTransactionDate);
		/*EXIT1*/
	}

protected void processContractHeader200()
	{
		updateOriginalChdr210();
		createNewContractHeader240();
	}

protected void updateOriginalChdr210()
	{
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void createNewContractHeader240()
	{
		/*    Read Table T5679                                     **/
		wsaaBatchkey.set(atmodrec.batchKey);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatchkey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		/*   Update the Status field                               **/
		if (isNE(t5679rec.setCnRiskStat,SPACES)) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}
		/*   Update the Premium Status field                       **/
		if (isNE(t5679rec.setCnPremStat,SPACES)) {
			chdrmjaIO.setPstatcode(t5679rec.setCnPremStat);
		}
		/*   Update relevant billing fields                        **/
		chdrmjaIO.setValidflag("1");
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		wsaaDate.set(wsaaShortTransDate);
		wsaaTermid.set(wsaaTransactionRecInner.wsaaAtTermid);
		wsaaUser.set(wsaaTransactionRecInner.wsaaAtUser);
		chdrmjaIO.setTranid(wsaaTranid);
		chdrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		wsaaMiscellaneousInner.wsaaCompFreq.set(chdrmjaIO.getBillfreq());
		chdrmjaIO.setBillfreq(wsaaTransactionRecInner.wsaaAtBillfreq);
		if (isNE(chdrmjaIO.getBillchnl(), wsaaTransactionRecInner.wsaaAtBillchnl)) {
			chdrmjaIO.setBillchnl(wsaaTransactionRecInner.wsaaAtBillchnl);
		}
		wsaaBillDate.set(chdrmjaIO.getBillcd());
		if (isNE(wsaaBillDd, wsaaTransactionRecInner.wsaaAtBillday)) {
			wsaaBillDd.set(wsaaTransactionRecInner.wsaaAtBillday);
			chdrmjaIO.setBillcd(wsaaBillDate);
		}
		if ((isNE(chdrmjaIO.getPayrnum(), wsaaTransactionRecInner.wsaaAtPayrnum))
		&& (isNE(wsaaTransactionRecInner.wsaaAtPayrnum, SPACES))) {
			chdrmjaIO.setPayrnum(wsaaTransactionRecInner.wsaaAtPayrnum);
		}
		if ((isNE(chdrmjaIO.getGrupkey(), wsaaTransactionRecInner.wsaaAtGrupkey))
		&& (isNE(wsaaTransactionRecInner.wsaaAtGrupkey, SPACES))) {
			chdrmjaIO.setGrupkey(wsaaTransactionRecInner.wsaaAtGrupkey);
		}
		if (isNE(chdrmjaIO.getMandref(), wsaaTransactionRecInner.wsaaAtMandref)) {
			chdrmjaIO.setMandref(wsaaTransactionRecInner.wsaaAtMandref);
		}
	}

protected void processPayrRecord250()
	{
		updateOriginalPayr210();
		createNewPayrRecord240();
	}

protected void updateOriginalPayr210()
	{
		payrIO.setValidflag("2");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void createNewPayrRecord240()
	{
		/*   Update the Premium Status field                       **/
		if (isNE(t5679rec.setCnPremStat,SPACES)) {
			payrIO.setPstatcode(t5679rec.setCnPremStat);
		}
		/*   Update relevant billing fields                        **/
		payrIO.setValidflag("1");
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setTransactionDate(wsaaShortTransDate);
		payrIO.setTermid(wsaaTransactionRecInner.wsaaAtTermid);
		payrIO.setUser(wsaaTransactionRecInner.wsaaAtUser);
		wsaaMiscellaneousInner.wsaaCompFreq.set(payrIO.getBillfreq());
		if (isNE(wsaaTransactionRecInner.wsaaAtBillfreq, payrIO.getBillfreq())) {
			payrIO.setBillfreq(wsaaTransactionRecInner.wsaaAtBillfreq);
			payrIO.setBillday(wsaaBillDd);
			payrIO.setBillmonth(wsaaBillMm);
			wsaaBtdate.set(payrIO.getBtdate());
			payrIO.setDuedd(wsaaBtDd);
			payrIO.setDuemm(wsaaBtMm);
		}
		if (isNE(payrIO.getBillchnl(), wsaaTransactionRecInner.wsaaAtBillchnl)) {
			payrIO.setBillchnl(wsaaTransactionRecInner.wsaaAtBillchnl);
		}
		wsaaBillDate.set(payrIO.getBillcd());
		if (isNE(wsaaBillDd, wsaaTransactionRecInner.wsaaAtBillday)) {
			wsaaBillDd.set(wsaaTransactionRecInner.wsaaAtBillday);
			payrIO.setBillday(wsaaTransactionRecInner.wsaaAtBillday);
			payrIO.setBillmonth(wsaaBillMm);
			payrIO.setBillcd(wsaaBillDate);
			updateNextdate370();
		}
		if ((isNE(payrIO.getGrupnum(), wsaaTransactionRecInner.wsaaAtGrupkey))
		&& (isNE(wsaaTransactionRecInner.wsaaAtGrupkey, SPACES))) {
			payrIO.setGrupnum(wsaaTransactionRecInner.wsaaAtGrupkey);
		}
		if (isNE(payrIO.getMandref(), wsaaTransactionRecInner.wsaaAtMandref)) {
			payrIO.setMandref(wsaaTransactionRecInner.wsaaAtMandref);
		}
		payrIO.setEffdate(payrIO.getBtdate());
	}

protected void getFirstComponent300()
	{
		begin310();
	}

protected void begin310()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrmjaIO);
		if ((isNE(covrmjaIO.getStatuz(),varcom.oK))
		&& (isNE(covrmjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum()))
		|| (isNE(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy()))
		|| (isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			covrmjaIO.setStatuz(varcom.endp);
		}
	}

protected void processComponent330()
	{
		readT5687340();
		checkStatusAndAction350();
	}

protected void readT5687340()
	{
		wsaaBatchkey.set(atmodrec.batchKey);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(itdmIO.getItemcoy(),atmodrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5687))
		|| (isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkStatusAndAction350()
	{
		/*    Check if component is to be updated.                    **/
		wsaaMiscellaneousInner.wsaaProcessComponent.set("N");
		if (isGT(covrmjaIO.getInstprem(),0)
		&& isNE(t5687rec.singlePremInd,"Y")) {
			checkComponent360();
			if (wsaaMiscellaneousInner.processComponent.isTrue()) {
				updateComponent380();
			}
		}
		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if ((isNE(covrmjaIO.getStatuz(),varcom.oK))
		&& (isNE(covrmjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(covrmjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy()))
		|| (isNE(covrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum()))) {
			covrmjaIO.setStatuz(varcom.endp);
		}
	}

protected void checkComponent360()
	{
		check360();
	}

protected void check360()
	{
		/*  Check for match on Premium and Risk Statii for Coverage   **/
		wsaaMiscellaneousInner.wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(),"00")) {
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.covPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.covRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
		else {
			/*   Check for match on Premium and Risk Statii for Rider     **/
			wsaaMiscellaneousInner.wsaaStatCount.set(1);
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.ridPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
	}

protected void updateNextdate370()
	{
		nextdate370();
	}

protected void nextdate370()
	{
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		if (BTPRO028Permission) {
			String key= payrIO.getBillchnl().toString().trim()+chdrmjaIO.getCnttype().toString().trim()+payrIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key= payrIO.getBillchnl().toString().trim().concat(chdrmjaIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key= payrIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						syserrrec.params.set(t6654);
						syserrrec.statuz.set("MRNF");
						xxxxFatalError();
					}
				}
			}
		}
		else {
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6654);
		wsaaT6654Key.set(SPACES);
		wsaaT6654Mop.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrmjaIO.getCnttype());
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		else {
			wsaaT6654Cnttype.set("***");
			itemIO.setItemitem(wsaaT6654Key);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
			else {
				t6654rec.t6654Rec.set(itemIO.getGenarea());
			}
		}
		}
		datcon2rec.intDate1.set(payrIO.getBillcd());
		datcon2rec.frequency.set("DY");
		compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays,-1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		payrIO.setNextdate(datcon2rec.intDate2);
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrmjaIO.getChdrcoy().toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}
protected void updateComponent380()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateOriginalCovr382();
					createNewCovr385();
				case updateStatus386:
					updateStatus386();
					findGenericSubroutine387();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateOriginalCovr382()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		covrmjaIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void createNewCovr385()
	{
		/*    Read table T5541 to obtain loading factors for both     **/
		/*    old and new frequencies.                                **/
		wsaaMiscellaneousInner.wsaaInstpremFound.set(SPACES);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5541);
		itdmIO.setItemitem(t5687rec.xfreqAltBasis);
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5541)
		|| isNE(itdmIO.getItemitem(),t5687rec.xfreqAltBasis)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
			goTo(GotoLabel.updateStatus386);
		}
		else {
			t5541rec.t5541Rec.set(itdmIO.getGenarea());
		}
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaTransactionRecInner.wsaaAtBillfreq);
		wsaaMiscellaneousInner.wsaaCalcOldfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
		/*    Get the Frequency Conversion Multiplication Factor      **/
		/*    Calculate the new INSTPREM                              **/
		/*    Calculate the accumulating INSTPREM for SINSTAMT01      **/
		wsaaMiscellaneousInner.wsaaNewFreqFound.set(SPACES);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 12)
		|| isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaMiscellaneousInner.wsaaCalcNewfreq)) {
				wsaaMiscellaneousInner.wsaaNewLfact.set(t5541rec.lfact[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaNewFreqFound.set("Y");
			}
		}
		wsaaMiscellaneousInner.wsaaOldFreqFound.set(SPACES);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 12)
		|| isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaMiscellaneousInner.wsaaCalcOldfreq)) {
				wsaaMiscellaneousInner.wsaaOldLfact.set(t5541rec.lfact[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaOldFreqFound.set("Y");
			}
		}
		if (isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")
		&& isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")) {
			wsaaMiscellaneousInner.wsaaInstpremFound.set("Y");
			setPrecision(covrmjaIO.getInstprem(), 3);
			covrmjaIO.setInstprem(mult(mult(covrmjaIO.getInstprem(), (div(wsaaMiscellaneousInner.wsaaCalcOldfreq, wsaaMiscellaneousInner.wsaaCalcNewfreq))), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(covrmjaIO.getInstprem());
			b000CallRounding();
			covrmjaIO.setInstprem(zrdecplrec.amountOut);
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
		}
		else {
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
		}
	}

protected void updateStatus386()
	{
		/*   Update the Risk Status Codes                             **/
		if (isEQ(covrmjaIO.getRider(),"00")) {
			if (isNE(t5679rec.setCovRiskStat,SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat,SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
			}
		}
		/*          Update Premium Status Code                        **/
		if (isEQ(covrmjaIO.getRider(),"00")) {
			if (isNE(t5679rec.setCovPremStat,SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidPremStat,SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		/*             Write the updated record                       **/
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setTransactionDate(wsaaShortTransDate);
		covrmjaIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*   Check for a non-refused/reversed historical INCR record  **/
		/* for this component.  If one is found, write a new one with **/
		/*             the new instalment premium details.            **/
		checkForIncrease600();
		/*  Update the Agent Commission Records   (AGCM)              **/
		/*  Retrieve all AGCM records for each valid COVR record read **/
		agcmbchIO.setParams(SPACES);
		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
		agcmbchIO.setLife(covrmjaIO.getLife());
		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
		agcmbchIO.setRider(covrmjaIO.getRider());
		agcmbchIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		agcmbchIO.setFormat(agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmbchIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, agcmbchIO);
		if ((isNE(agcmbchIO.getStatuz(),varcom.oK))
		&& (isNE(agcmbchIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
		/*   Check if valid AGCM record retrived                      **/
		if (isEQ(agcmbchIO.getStatuz(),varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if ((isNE(agcmbchIO.getChdrcoy(),covrmjaIO.getChdrcoy()))
			|| (isNE(agcmbchIO.getChdrnum(),covrmjaIO.getChdrnum()))
			|| (isNE(agcmbchIO.getLife(),covrmjaIO.getLife()))
			|| (isNE(agcmbchIO.getCoverage(),covrmjaIO.getCoverage()))
			|| (isNE(agcmbchIO.getRider(),covrmjaIO.getRider()))
			|| (isNE(agcmbchIO.getPlanSuffix(),covrmjaIO.getPlanSuffix()))) {
				agcmbchIO.setStatuz(varcom.endp);
			}
		}
		/*   Process AGCM records until end of file                   **/
		while ( !(isEQ(agcmbchIO.getStatuz(),varcom.endp))) {
			processReadAgcmbch390();
		}

	}

protected void findGenericSubroutine387()
	{
		wsaaMiscellaneousInner.wsaaCompCrtable.set(covrmjaIO.getCrtable());
		wsaaMiscellaneousInner.wsaaCompTrancode.set(wsaaBatchkey.batcBatctrcde);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5671);
		itemIO.setItemitem(wsaaMiscellaneousInner.wsaaCompKey2);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itemIO.getItemcoy(),atmodrec.company)
		|| isNE(itemIO.getItemtabl(),t5671)
		|| isNE(itemIO.getItemitem(), wsaaMiscellaneousInner.wsaaCompKey2)
		|| isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setStatuz(varcom.mrnf);
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		/*    Call all Non-blank Subroutines                          **/
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			bchgallrec.function.set("BCHG ");
			bchgallrec.company.set(covrmjaIO.getChdrcoy());
			bchgallrec.chdrnum.set(covrmjaIO.getChdrnum());
			bchgallrec.life.set(covrmjaIO.getLife());
			bchgallrec.coverage.set(covrmjaIO.getCoverage());
			bchgallrec.rider.set(covrmjaIO.getRider());
			bchgallrec.planSuffix.set(covrmjaIO.getPlanSuffix());
			bchgallrec.oldBillfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
			bchgallrec.newBillfreq.set(wsaaTransactionRecInner.wsaaAtBillfreq);
			bchgallrec.effdate.set(wsaaTransactionRecInner.wsaaAtToday);
			bchgallrec.tranno.set(covrmjaIO.getTranno());
			bchgallrec.crtable.set(covrmjaIO.getCrtable());
			bchgallrec.trancode.set(wsaaBatchkey.batcBatctrcde);
			bchgallrec.cntcurr.set(covrmjaIO.getSicurr());
			wsaaMiscellaneousInner.sub.set(1);
			while ( !(isGT(wsaaMiscellaneousInner.sub, 4))) {
				if (isNE(t5671rec.subprog[wsaaMiscellaneousInner.sub.toInt()], SPACES)) {
					callProgram(t5671rec.subprog[wsaaMiscellaneousInner.sub.toInt()], bchgallrec.bchgallRec);
					if (isNE(bchgallrec.statuz,varcom.oK)) {
						syserrrec.params.set(bchgallrec.bchgallRec);
						syserrrec.statuz.set(bchgallrec.statuz);
						xxxxFatalError();
					}
				}
				wsaaMiscellaneousInner.sub.add(1);
			}

		}
	}

protected void processReadAgcmbch390()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					setUpDataArea391();
					createNewRecord397();
				case retrieveNextRecord393:
					retrieveNextRecord393();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setUpDataArea391()
	{
		if (isEQ(agcmbchIO.getPtdate(),ZERO)) {
			goTo(GotoLabel.retrieveNextRecord393);
		}
		agcmbchIO.setCurrto(payrIO.getBtdate());
		agcmbchIO.setValidflag("2");
		agcmbchIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void createNewRecord397()
	{
		/*   Calculate new annual premium*/
		/*   The COVR calculation for the new premium uses the following*/
		/*   formula:*/
		/*   X = P*(A/C)*(D/B)   where P = the old premium*/
		/*                             A = old frequency*/
		/*                             B = old loading factor*/
		/*                             C = new frequency*/
		/*                             D = new loading factor*/
		/*                             X = new premium*/
		/*   the new annualised premium, Y, should therefore be X*C so*/
		/*   Y = P*(A/C)*(D/B)*C*/
		/*     = P*A*(D/B)*/
		/*     = Z*(D/B)         where Z = the old annualised premium*/
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaTransactionRecInner.wsaaAtBillfreq);
		wsaaMiscellaneousInner.wsaaCalcOldfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
		if (isEQ(wsaaMiscellaneousInner.wsaaInstpremFound, "Y")) {
			setPrecision(agcmbchIO.getAnnprem(), 3);
			agcmbchIO.setAnnprem(mult(agcmbchIO.getAnnprem(), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
		}
		zrdecplrec.amountIn.set(agcmbchIO.getAnnprem());
		b000CallRounding();
		agcmbchIO.setAnnprem(zrdecplrec.amountOut);
		/*                 Write new AGCM record                      **/
		agcmbchIO.setTransactionDate(wsaaShortTransDate);
		agcmbchIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		agcmbchIO.setUser(wsaaTransactionRecInner.wsaaAtUser);
		agcmbchIO.setTranno(chdrmjaIO.getTranno());
		agcmbchIO.setValidflag("1");
		agcmbchIO.setCurrto(99999999);
		agcmbchIO.setCurrfrom(payrIO.getBtdate());
		agcmbchIO.setFormat(agcmbchrec);
		agcmbchIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void retrieveNextRecord393()
	{
		agcmbchIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if ((isNE(agcmbchIO.getStatuz(),varcom.oK))
		&& (isNE(agcmbchIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
		/*            Check if valid record retrieved                 **/
		if (isEQ(agcmbchIO.getStatuz(),varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if ((isNE(covrmjaIO.getChdrcoy(),agcmbchIO.getChdrcoy()))
			|| (isNE(covrmjaIO.getChdrnum(),agcmbchIO.getChdrnum()))
			|| (isNE(covrmjaIO.getLife(),agcmbchIO.getLife()))
			|| (isNE(covrmjaIO.getCoverage(),agcmbchIO.getCoverage()))
			|| (isNE(covrmjaIO.getRider(),agcmbchIO.getRider()))
			|| (isNE(covrmjaIO.getPlanSuffix(),agcmbchIO.getPlanSuffix()))) {
				agcmbchIO.setStatuz(varcom.endp);
			}
		}
		if (isEQ(agcmbchIO.getTranno(),chdrmjaIO.getTranno())
		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.retrieveNextRecord393);
		}
	}

protected void accumInstpremWrtChdrmja400()
	{
		accumInstPrem410();
	}

protected void accumInstPrem410()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(f290);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		if (isNE(t5688rec.feemeth,SPACES)) {
			calcFee1200();
		}
		chdrmjaIO.setSinstamt01(wsaaMiscellaneousInner.wsaaTotInstprem);
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(),chdrmjaIO.getSinstamt02()),chdrmjaIO.getSinstamt03()),chdrmjaIO.getSinstamt04()),chdrmjaIO.getSinstamt05()));
		chdrmjaIO.setFunction(varcom.writr);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*                   Write the PAYR record.                      **/
		payrIO.setSinstamt01(wsaaMiscellaneousInner.wsaaTotInstprem);
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(),payrIO.getSinstamt02()),chdrmjaIO.getSinstamt03()),chdrmjaIO.getSinstamt04()),chdrmjaIO.getSinstamt05()));
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void writeHistoryRlseLocks500()
	{
		writePtrnRecord510();
		updateBatchHeader530();
		releaseSoftlock550();
	}

protected void writePtrnRecord510()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionDate(wsaaShortTransDate);
		ptrnIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(wsaaTransactionRecInner.wsaaAtToday);
		ptrnIO.setDatesub(wsaaTransactionRecInner.wsaaAtToday);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void updateBatchHeader530()
	{
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
	}

protected void releaseSoftlock550()
	{
		/*         Release the soft lock on the contract.           **/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.user.set(wsaaTransactionRecInner.wsaaAtUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void checkForIncrease600()
	{
			begn610();
		}

protected void begn610()
	{
		incrhstIO.setParams(SPACES);
		incrhstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		incrhstIO.setChdrnum(covrmjaIO.getChdrnum());
		incrhstIO.setLife(covrmjaIO.getLife());
		incrhstIO.setCoverage(covrmjaIO.getCoverage());
		incrhstIO.setRider(covrmjaIO.getRider());
		incrhstIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incrhstIO.setFormat(incrhstrec);
		incrhstIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, incrhstIO);
		if (isNE(incrhstIO.getStatuz(),varcom.oK)
		&& isNE(incrhstIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(incrhstIO.getParams());
			syserrrec.statuz.set(incrhstIO.getStatuz());
			xxxxFatalError();
		}
		/*  If no record is found, do no further Increase processing.  **/
		if (isEQ(incrhstIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		/*    Write a new historical INCR record with the instalment   **/
		/*  premium fields updated in line with the change in billing  **/
		/*    frequency.                                               **/
		if (isEQ(wsaaMiscellaneousInner.wsaaInstpremFound, "Y")) {
			setPrecision(incrhstIO.getOrigInst(), 3);
			incrhstIO.setOrigInst(mult(mult(incrhstIO.getOrigInst(), (div(wsaaMiscellaneousInner.wsaaCalcOldfreq, wsaaMiscellaneousInner.wsaaCalcNewfreq))), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getOrigInst());
			b000CallRounding();
			incrhstIO.setOrigInst(zrdecplrec.amountOut);
			setPrecision(incrhstIO.getLastInst(), 3);
			incrhstIO.setLastInst(mult(mult(incrhstIO.getLastInst(), (div(wsaaMiscellaneousInner.wsaaCalcOldfreq, wsaaMiscellaneousInner.wsaaCalcNewfreq))), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getLastInst());
			b000CallRounding();
			incrhstIO.setLastInst(zrdecplrec.amountOut);
			setPrecision(incrhstIO.getNewinst(), 3);
			incrhstIO.setNewinst(mult(mult(incrhstIO.getNewinst(), (div(wsaaMiscellaneousInner.wsaaCalcOldfreq, wsaaMiscellaneousInner.wsaaCalcNewfreq))), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getNewinst());
			b000CallRounding();
			incrhstIO.setNewinst(zrdecplrec.amountOut);
		}
		incrhstIO.setTranno(chdrmjaIO.getTranno());
		incrhstIO.setStatcode(covrmjaIO.getStatcode());
		incrhstIO.setPstatcode(covrmjaIO.getPstatcode());
		incrhstIO.setTransactionDate(wsaaShortTransDate);
		incrhstIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		incrhstIO.setTermid(wsaaTransactionRecInner.wsaaAtTermid);
		incrhstIO.setUser(wsaaTransactionRecInner.wsaaAtUser);
		incrhstIO.setFormat(incrhstrec);
		incrhstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrhstIO);
		if (isNE(incrhstIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(incrhstIO.getStatuz());
			syserrrec.params.set(incrhstIO.getParams());
			xxxxFatalError();
		}
	}

protected void calcFee1200()
	{
			readSubroutineTable1210();
		}

protected void readSubroutineTable1210()
	{
		/*  Reference T5674 to obtain the subroutine required to work  **/
		/*    out the Fee amount by the correct method.                **/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(f151);
			return ;
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		/*     Check subroutine NOT = SPACES before attempting call.   **/
		if (isEQ(t5674rec.commsubr,SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrmjaIO.getCnttype());
		mgfeelrec.billfreq.set(chdrmjaIO.getBillfreq());
		mgfeelrec.effdate.set(chdrmjaIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrmjaIO.getCntcurr());
		mgfeelrec.company.set(chdrmjaIO.getChdrcoy());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)
		&& isNE(mgfeelrec.statuz,varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(mgfeelrec.mgfee);
		b000CallRounding();
		mgfeelrec.mgfee.set(zrdecplrec.amountOut);
		chdrmjaIO.setSinstamt02(mgfeelrec.mgfee);
		payrIO.setSinstamt02(mgfeelrec.mgfee);
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatchkey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatchkey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatchkey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatchkey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatchkey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void b000CallRounding()
	{
		/*B100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(atmodrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			xxxxFatalError();
		}
		/*B900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-REC--INNER
 */
private static final class WsaaTransactionRecInner {

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(66);
	private PackedDecimalData wsaaAtTransactionDate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaAtTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 5);
	private PackedDecimalData wsaaAtUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 9);
	private FixedLengthStringData wsaaAtTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 13);
	private FixedLengthStringData wsaaAtBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 17);
	private FixedLengthStringData wsaaAtBillchnl = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData wsaaAtBillday = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 21);
	private FixedLengthStringData wsaaAtPayrnum = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 23);
	private FixedLengthStringData wsaaAtMandref = new FixedLengthStringData(5).isAPartOf(wsaaTransactionRec, 31);
	private FixedLengthStringData wsaaAtGrupkey = new FixedLengthStringData(12).isAPartOf(wsaaTransactionRec, 36);
	private FixedLengthStringData wsaaAtMembsel = new FixedLengthStringData(10).isAPartOf(wsaaTransactionRec, 48);
	private ZonedDecimalData wsaaAtToday = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 58);
}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner {
		/* WSAA-MISCELLANEOUS */
	private ZonedDecimalData sub = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaStatCount = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaCompKey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCompFreq = new FixedLengthStringData(2).isAPartOf(wsaaCompKey, 4);

	private FixedLengthStringData wsaaCompKey2 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompTrancode = new FixedLengthStringData(4).isAPartOf(wsaaCompKey2, 0);
	private FixedLengthStringData wsaaCompCrtable = new FixedLengthStringData(4).isAPartOf(wsaaCompKey2, 4);

	private FixedLengthStringData wsaaCalcFreqs = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaCalcOldfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalcFreqs, 0).setUnsigned();
	private ZonedDecimalData wsaaCalcNewfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalcFreqs, 2).setUnsigned();

	private FixedLengthStringData wsaaProcessComponent = new FixedLengthStringData(1);
	private Validator processComponent = new Validator(wsaaProcessComponent, "Y");
	private Validator notProcessComponent = new Validator(wsaaProcessComponent, "N");
	private FixedLengthStringData wsaaInstpremFound = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTotInstprem = new PackedDecimalData(13, 2);
	private FixedLengthStringData wsaaNewFreqFound = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOldFreqFound = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaNewLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
}
}
