package com.csc.life.contractservicing.screens;


import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sr60tscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 16;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 21, 6, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr60tScreenVars sv = (Sr60tScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr60tscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr60tscreensfl, 
			sv.Sr60tscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr60tScreenVars sv = (Sr60tScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr60tscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr60tScreenVars sv = (Sr60tScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr60tscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr60tscreensflWritten.gt(0))
		{
			sv.sr60tscreensfl.setCurrentIndex(0);
			sv.Sr60tscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr60tScreenVars sv = (Sr60tScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr60tscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr60tScreenVars screenVars = (Sr60tScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.clntnumSfl.setFieldName("clntnumSfl");
				screenVars.schmnoSfl.setFieldName("schmnoSfl");
				screenVars.cnttype.setFieldName("cnttype");				
				screenVars.taxPeriodFrom.setFieldName("taxPeriodFrom");
				screenVars.taxPeriodTo.setFieldName("taxPeriodTo");
				screenVars.taxPeriodFromDisp.setFieldName("taxPeriodFromDisp");
				screenVars.taxPeriodToDisp.setFieldName("taxPeriodToDisp");
				screenVars.statcode.setFieldName("statcode");				
				screenVars.pstatcode.setFieldName("pstatcode");	
				screenVars.totamnt.setFieldName("totamnt");
				screenVars.dflag.setFieldName("dflag");
				screenVars.schdrnum.setFieldName("schdrnum");
				screenVars.scnttype.setFieldName("scnttype");	
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.clntnumSfl.set(dm.getField("clntnumSfl"));			
			screenVars.schmnoSfl.set(dm.getField("schmnoSfl"));
			screenVars.cnttype.set(dm.getField("cnttype"));
			screenVars.taxPeriodFrom.set(dm.getField("taxPeriodFrom"));
			screenVars.taxPeriodTo.set(dm.getField("taxPeriodTo"));
			screenVars.taxPeriodFromDisp.set(dm.getField("taxPeriodFromDisp"));
			screenVars.taxPeriodToDisp.set(dm.getField("taxPeriodToDisp"));
			screenVars.statcode.set(dm.getField("statcode"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.totamnt.set(dm.getField("totamnt"));
			screenVars.dflag.set(dm.getField("dflag"));
			screenVars.schdrnum.set(dm.getField("schdrnum"));
			screenVars.scnttype.set(dm.getField("scnttype"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr60tScreenVars screenVars = (Sr60tScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.clntnumSfl.setFieldName("clntnumSfl");
				screenVars.schmnoSfl.setFieldName("schmnoSfl");
				screenVars.cnttype.setFieldName("cnttype");		
				screenVars.taxPeriodFrom.setFieldName("taxPeriodFrom");
				screenVars.taxPeriodTo.setFieldName("taxPeriodTo");
				screenVars.taxPeriodFromDisp.setFieldName("taxPeriodFromDisp");
				screenVars.statcode.setFieldName("statcode");		
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.totamnt.setFieldName("totamnt");
				screenVars.dflag.setFieldName("dflag");
				screenVars.schdrnum.setFieldName("schdrnum");
				screenVars.scnttype.setFieldName("scnttype");	
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("clntnumSfl").set(screenVars.clntnumSfl);
			dm.getField("schmnoSfl").set(screenVars.schmnoSfl);
			dm.getField("cnttype").set(screenVars.cnttype);			
			dm.getField("taxPeriodFrom").set(screenVars.taxPeriodFrom);
			dm.getField("taxPeriodTo").set(screenVars.taxPeriodTo);
			dm.getField("taxPeriodFromDisp").set(screenVars.taxPeriodFromDisp);
			dm.getField("taxPeriodToDisp").set(screenVars.taxPeriodToDisp);
			dm.getField("statcode").set(screenVars.statcode);			
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("totamnt").set(screenVars.totamnt);
			dm.getField("dflag").set(screenVars.dflag);
			dm.getField("schdrnum").set(screenVars.schdrnum);
			dm.getField("scnttype").set(screenVars.scnttype);	
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr60tscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr60tScreenVars screenVars = (Sr60tScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.clntnumSfl.clearFormatting();
		screenVars.schmnoSfl.clearFormatting();
		screenVars.cnttype.clearFormatting();
		screenVars.taxPeriodFrom.clearFormatting();
		screenVars.taxPeriodTo.clearFormatting();
		screenVars.taxPeriodFromDisp.clearFormatting();
		screenVars.taxPeriodToDisp.clearFormatting();
		screenVars.statcode.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.totamnt.clearFormatting();
		screenVars.dflag.clearFormatting();
		screenVars.schdrnum.clearFormatting();
		screenVars.scnttype.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr60tScreenVars screenVars = (Sr60tScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.clntnumSfl.setClassString("");
		screenVars.schmnoSfl.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.taxPeriodFrom.setClassString("");
		screenVars.taxPeriodTo.setClassString("");
		screenVars.taxPeriodFromDisp.setClassString("");
		screenVars.taxPeriodToDisp.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.totamnt.setClassString("");
		screenVars.dflag.setClassString("");
		screenVars.schdrnum.setClassString("");
		screenVars.scnttype.setClassString("");
	}

/**
 * Clear all the variables in Sr60tscreensfl
 */
	public static void clear(VarModel pv) {
		Sr60tScreenVars screenVars = (Sr60tScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.chdrnum.clear();
		screenVars.clntnumSfl.clear();
		screenVars.schmnoSfl.clear();
		screenVars.cnttype.clear();		
		screenVars.taxPeriodFrom.clear();
		screenVars.taxPeriodTo.clear();
		screenVars.taxPeriodFromDisp.clear();
		screenVars.taxPeriodToDisp.clear();
		screenVars.statcode.clear();
		screenVars.pstatcode.clear();
		screenVars.totamnt.clear();
		screenVars.dflag.clear();
		screenVars.schdrnum.clear();
		screenVars.scnttype.clear();	
	}
}
