package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.contractservicing.dataaccess.dao.CprppfDAO;
import com.csc.life.contractservicing.dataaccess.model.Cprppf;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CprppfDAOImpl extends BaseDAOImpl<Cprppf> implements CprppfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CprppfDAOImpl.class);	
	
	public int updateOrInstCprppf(Cprppf cprppf){
		int result = this.updateCprppfRecord(cprppf);
		
		if(result<=0){
			result = this.insertCprppfRecord(cprppf);
		}
		
		return result;
	}

	public int updateCprppfRecord(Cprppf cprppf) {
		StringBuilder sb = new StringBuilder("UPDATE CPRPPF SET TRANNO=?, INSTPREM=?, VALIDFLAG='1',JOBNM=?, USRPRF=?, DATIME=? ")
				.append(" WHERE CHDRPFX=? AND CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? ");
		ResultSet rs = null;		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		
		int result = 0;
		try{
			ps.setInt(1, cprppf.getTranno());
			ps.setDouble(2, cprppf.getInstprem());
			ps.setString(3, getJobnm());
			ps.setString(4, getUsrprf());
			ps.setTimestamp(5, getDatime());	
			ps.setString(6, cprppf.getChdrpfx());
			ps.setString(7, cprppf.getChdrcoy());
			ps.setString(8, cprppf.getChdrnum());
			ps.setString(9, cprppf.getLife());
			ps.setString(10, cprppf.getCoverage());
			ps.setString(11, cprppf.getRider());
			
			result = ps.executeUpdate();
		
			
		}catch (SQLException e) {
			LOGGER.error("updateCprppfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		   return result;
	}
	
	public int insertCprppfRecord(Cprppf cprppf) {
		StringBuilder sb = new StringBuilder("INSERT INTO Cprppf(CHDRPFX,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,TRANNO,INSTPREM,VALIDFLAG,USRPRF,JOBNM,DATIME) ")
				.append("VALUES(?,?,?,?,?,?,?,?,'1',?,?,?)");
		ResultSet rs = null;		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		
		int result = 0;
		try{
			ps.setString(1, cprppf.getChdrpfx());
			ps.setString(2, cprppf.getChdrcoy());
			ps.setString(3, cprppf.getChdrnum());
			ps.setString(4, cprppf.getLife());
			ps.setString(5, cprppf.getCoverage());
			ps.setString(6, cprppf.getRider());
			ps.setInt(7, cprppf.getTranno());
			ps.setDouble(8, cprppf.getInstprem());
			ps.setString(9, getUsrprf());
			ps.setString(10, getJobnm());
			ps.setTimestamp(11, getDatime());	
	
			
			result = ps.executeUpdate();
		
			
		}catch (SQLException e) {
			LOGGER.error("insertCprppfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		   return result;
	}

	
	public Cprppf getCprpRecord(Cprppf cprppf) {
		StringBuilder sb = new StringBuilder("select CHDRPFX,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,TRANNO,INSTPREM,VALIDFLAG ");
		sb.append("from Cprppf where CHDRPFX=? and CHDRCOY=? and CHDRNUM=? and LIFE=? and COVERAGE=? and RIDER=? ");
		sb.append("order by CHDRPFX ASC,CHDRCOY ASC,CHDRNUM ASC,LIFE ASC,COVERAGE ASC,RIDER ASC,UNIQUE_NUMBER DESC");
		ResultSet rs = null;
		Cprppf result = null;
		PreparedStatement ps = getPrepareStatement(sb.toString());

		try{
			ps.setString(1, cprppf.getChdrpfx());
			ps.setString(2, cprppf.getChdrcoy());
			ps.setString(3, cprppf.getChdrnum());
			ps.setString(4, cprppf.getLife());
			ps.setString(5, cprppf.getCoverage());
			ps.setString(6, cprppf.getRider());
		    rs = ps.executeQuery();
			if(rs.next()) {
				result = new Cprppf();
				result.setChdrpfx(rs.getString("CHDRPFX"));
				result.setChdrcoy(rs.getString("CHDRCOY"));
				result.setChdrnum(rs.getString("CHDRNUM"));
				result.setLife(rs.getString("LIFE"));
				result.setCoverage(rs.getString("COVERAGE"));
				result.setRider(rs.getString("RIDER"));
				result.setTranno(rs.getInt("TRANNO"));
				result.setInstprem(rs.getDouble("INSTPREM"));
				result.setValidflag(rs.getString("VALIDFLAG"));
			}
		}catch (SQLException e) {
			LOGGER.error("insertCprppfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return result;
		  
	}
	
	public int updateCprppfForTrannoAndInstprem(Cprppf cprppf) {
		StringBuilder sb = new StringBuilder("UPDATE CPRPPF SET LIFE=?, COVERAGE=?, RIDER=?, TRANNO=?, VALIDFLAG=?, INSTPREM=?,JOBNM=?, USRPRF=?, DATIME=? ")
				.append(" WHERE CHDRPFX=? AND CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=?");
		ResultSet rs = null;		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		
		int result = 0;
		try{
			ps.setString(1, cprppf.getLife());
			ps.setString(2, cprppf.getCoverage());
			ps.setString(3, cprppf.getRider());
			ps.setInt(4, cprppf.getTranno());
			ps.setString(5, cprppf.getValidflag());
			ps.setDouble(6, cprppf.getInstprem());
			ps.setString(7, getJobnm());
			ps.setString(8, getUsrprf());
			ps.setTimestamp(9, getDatime());	
			ps.setString(10, cprppf.getChdrpfx());
			ps.setString(11, cprppf.getChdrcoy());
			ps.setString(12, cprppf.getChdrnum());
			ps.setString(13, cprppf.getLife());
			ps.setString(14, cprppf.getCoverage());
			ps.setString(15, cprppf.getRider());
			
			result = ps.executeUpdate();
		
			
		}catch (SQLException e) {
			LOGGER.error("updateCprppfForTrannoAndInstprem()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		   return result;
	}
	public void delCprpRecord(String chdrpfx,String chdrcoy,String chdrnum){
		  StringBuilder sql = new StringBuilder();
	        sql.append("DELETE CPRPPF WHERE CHDRPFX =? AND CHDRCOY=? AND CHDRNUM=?");//ILIFE-7722
	        PreparedStatement ps = getPrepareStatement(sql.toString());
	        List<Cprppf> cprppfList = new ArrayList<Cprppf>();
	        ResultSet rs = null;
	        try {
	         /*   ps.setString(1, chdrpfx.trim());
	            ps.setString(2, chdrcoy.trim());
	            ps.setString(3, chdrnum.trim());*/	        	
	        	ps.setString(1, StringUtil.fillSpace(chdrpfx.trim(),DD.chdrpfx.length));
	        	ps.setString(2, StringUtil.fillSpace(chdrcoy.trim(),DD.chdrcoy.length));
	        	ps.setString(3, StringUtil.fillSpace(chdrnum.trim(),DD.chdrnum.length));
	            ps.executeUpdate();
	        } catch (SQLException e) {
	            LOGGER.error("delCprpRecord()", e);//IJTI-1561
	        } finally {
	            close(ps, rs);
	        }
	}
}
