package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr569screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 15, 24, 16, 1, 2, 12, 3, 21}; 
	public static int maxRecords = 13;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 1, 2, 3, 4, 5, 6, 7, 99, 89}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr569ScreenVars sv = (Sr569ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr569screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr569screensfl, 
			sv.Sr569screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr569ScreenVars sv = (Sr569ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr569screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr569ScreenVars sv = (Sr569ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr569screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr569screensflWritten.gt(0))
		{
			sv.sr569screensfl.setCurrentIndex(0);
			sv.Sr569screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr569ScreenVars sv = (Sr569ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr569screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr569ScreenVars screenVars = (Sr569ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hlifcnum.setFieldName("hlifcnum");
				screenVars.hlifeno.setFieldName("hlifeno");
				screenVars.hsuffix.setFieldName("hsuffix");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.linetype.setFieldName("linetype");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.select.setFieldName("select");
				screenVars.component.setFieldName("component");
				screenVars.cmpntnum.setFieldName("cmpntnum");
				screenVars.deit.setFieldName("deit");
				screenVars.asterisk.setFieldName("asterisk");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hlifcnum.set(dm.getField("hlifcnum"));
			screenVars.hlifeno.set(dm.getField("hlifeno"));
			screenVars.hsuffix.set(dm.getField("hsuffix"));
			screenVars.hcoverage.set(dm.getField("hcoverage"));
			screenVars.hrider.set(dm.getField("hrider"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.linetype.set(dm.getField("linetype"));
			screenVars.statcode.set(dm.getField("statcode"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.select.set(dm.getField("select"));
			screenVars.component.set(dm.getField("component"));
			screenVars.cmpntnum.set(dm.getField("cmpntnum"));
			screenVars.deit.set(dm.getField("deit"));
			screenVars.asterisk.set(dm.getField("asterisk"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr569ScreenVars screenVars = (Sr569ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hlifcnum.setFieldName("hlifcnum");
				screenVars.hlifeno.setFieldName("hlifeno");
				screenVars.hsuffix.setFieldName("hsuffix");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.linetype.setFieldName("linetype");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.select.setFieldName("select");
				screenVars.component.setFieldName("component");
				screenVars.cmpntnum.setFieldName("cmpntnum");
				screenVars.deit.setFieldName("deit");
				screenVars.asterisk.setFieldName("asterisk");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hlifcnum").set(screenVars.hlifcnum);
			dm.getField("hlifeno").set(screenVars.hlifeno);
			dm.getField("hsuffix").set(screenVars.hsuffix);
			dm.getField("hcoverage").set(screenVars.hcoverage);
			dm.getField("hrider").set(screenVars.hrider);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("linetype").set(screenVars.linetype);
			dm.getField("statcode").set(screenVars.statcode);
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("select").set(screenVars.select);
			dm.getField("component").set(screenVars.component);
			dm.getField("cmpntnum").set(screenVars.cmpntnum);
			dm.getField("deit").set(screenVars.deit);
			dm.getField("asterisk").set(screenVars.asterisk);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr569screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr569ScreenVars screenVars = (Sr569ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hlifcnum.clearFormatting();
		screenVars.hlifeno.clearFormatting();
		screenVars.hsuffix.clearFormatting();
		screenVars.hcoverage.clearFormatting();
		screenVars.hrider.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.linetype.clearFormatting();
		screenVars.statcode.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.component.clearFormatting();
		screenVars.cmpntnum.clearFormatting();
		screenVars.deit.clearFormatting();
		screenVars.asterisk.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr569ScreenVars screenVars = (Sr569ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hlifcnum.setClassString("");
		screenVars.hlifeno.setClassString("");
		screenVars.hsuffix.setClassString("");
		screenVars.hcoverage.setClassString("");
		screenVars.hrider.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.linetype.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.select.setClassString("");
		screenVars.component.setClassString("");
		screenVars.cmpntnum.setClassString("");
		screenVars.deit.setClassString("");
		screenVars.asterisk.setClassString("");
	}

/**
 * Clear all the variables in Sr569screensfl
 */
	public static void clear(VarModel pv) {
		Sr569ScreenVars screenVars = (Sr569ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hlifcnum.clear();
		screenVars.hlifeno.clear();
		screenVars.hsuffix.clear();
		screenVars.hcoverage.clear();
		screenVars.hrider.clear();
		screenVars.hcrtable.clear();
		screenVars.linetype.clear();
		screenVars.statcode.clear();
		screenVars.pstatcode.clear();
		screenVars.select.clear();
		screenVars.component.clear();
		screenVars.cmpntnum.clear();
		screenVars.deit.clear();
		screenVars.asterisk.clear();
	}
}
