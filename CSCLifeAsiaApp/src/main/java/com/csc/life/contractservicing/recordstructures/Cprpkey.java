package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:31
 * Description:
 * Copybook name: CPRPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cprpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData cprpFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData cprpKey = new FixedLengthStringData(64).isAPartOf(cprpFileKey, 0, REDEFINE);
  	public FixedLengthStringData cprpChdrpfx = new FixedLengthStringData(2).isAPartOf(cprpKey, 0);
  	public FixedLengthStringData cprpChdrcoy = new FixedLengthStringData(1).isAPartOf(cprpKey, 2);
  	public FixedLengthStringData cprpChdrnum = new FixedLengthStringData(8).isAPartOf(cprpKey, 3);
  	public FixedLengthStringData cprpLife = new FixedLengthStringData(2).isAPartOf(cprpKey, 11);
  	public FixedLengthStringData cprpCoverage = new FixedLengthStringData(2).isAPartOf(cprpKey, 13);
  	public FixedLengthStringData cprpRider = new FixedLengthStringData(2).isAPartOf(cprpKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(cprpKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(cprpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		cprpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}