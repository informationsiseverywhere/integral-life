package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;

public class Lhdrpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String loantype;
	private String loancurr;
	private BigDecimal loanorigam;
	private Integer loanstdate;
	private String termid;
	private Integer user_T;
	private Integer trdt;
	private Integer trtm;
	
	public Lhdrpf() {
		this.chdrcoy = "";
		this.chdrnum = "";
		this.loantype = "";
		this.loancurr = "";
		this.loanorigam = BigDecimal.ZERO;
		this.loanstdate = 0;
		this.termid = "";
		this.user_T = 0;
		this.trdt = 0;
		this.trtm = 0;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}
	public String getLoancurr() {
		return loancurr;
	}
	public void setLoancurr(String loancurr) {
		this.loancurr = loancurr;
	}
	public BigDecimal getLoanorigam() {
		return loanorigam;
	}
	public void setLoanorigam(BigDecimal loanorigam) {
		this.loanorigam = loanorigam;
	}
	public Integer getLoanstdate() {
		return loanstdate;
	}
	public void setLoanstdate(Integer loanstdate) {
		this.loanstdate = loanstdate;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getUser_T() {
		return user_T;
	}
	public void setUser_T(Integer user_T) {
		this.user_T = user_T;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	
}
