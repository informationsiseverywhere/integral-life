package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class Sr60sscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr60sScreenVars sv = (Sr60sScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr60sscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr60sScreenVars screenVars = (Sr60sScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.action.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.ownersel.setClassString("");
		screenVars.schmno.setClassString("");
	}

/**
 * Clear all the variables in Sr60sscreen
 */
	public static void clear(VarModel pv) {
		Sr60sScreenVars screenVars = (Sr60sScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.action.clear();
		screenVars.chdrsel.clear();
		screenVars.ownersel.clear();
		screenVars.schmno.clear();
	}
}
