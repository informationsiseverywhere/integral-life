/*
 * File: Annyinc.java
 * Date: 29 August 2009 20:14:23
 * Author: Quipoz Limited
 * 
 * Class transformed from ANNYINC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.tablestructures.T5648rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* OVERVIEW
*
* This Subroutine is called via the Batch Process B5033 -
* Automatic Increase. It will be called using the ISUALLREC *CPY
* which passes key information.
*
* The subroutine uses this information to process ANNY records
* in two separate ways:
*
* 1) If T6658-ADDNEW field is not spaces then the subroutine
*    Writes a Valid-Flag 1 record with a new tranno and capital
*    content value equal to the Automatic Increase portion
*    calculated in B5033.
*
* 2) If T6658-ADDNEW field is spaces, then the subroutine
*    Modifies the ANNY record: The existing ANNY is valid flagged
*    2 and a new ANNY record is written with a new tranno and
*    a capital content value increased by the Automatic Increase
*    percentage.
*
*****************************************************************
* </pre>
*/
public class Annyinc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData temporaryStorageArea = new FixedLengthStringData(6);
	private PackedDecimalData wsaaPctinc = new PackedDecimalData(11, 4).isAPartOf(temporaryStorageArea, 0);
		/* TABLES */
	private static final String t6658 = "T6658";
	private static final String t5648 = "T5648";
	private static final String t5687 = "T5687";
		/* FORMATS */
	private static final String covrrec = "COVRREC";
	private static final String annyrec = "ANNYREC";
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private T6658rec t6658rec = new T6658rec();
	private T5648rec t5648rec = new T5648rec();
	private T5687rec t5687rec = new T5687rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Isuallrec isuallrec = new Isuallrec();

	public Annyinc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*STARTS*/
		initialize1000();
		if (isNE(t6658rec.addnew, SPACES)) {
			addNewAnny2000();
		}
		else {
			modifyAnny3000();
		}
		exitProgram();
	}

protected void initialize1000()
	{
		starts1000();
	}

protected void starts1000()
	{
		initialize(temporaryStorageArea);
		isuallrec.statuz.set(varcom.oK);
		/* Use ISUA values to read COVR to get CRTABLE                 H*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(isuallrec.company);
		covrIO.setLife(isuallrec.life);
		covrIO.setCoverage(isuallrec.coverage);
		covrIO.setRider(isuallrec.rider);
		covrIO.setChdrnum(isuallrec.chdrnum);
		covrIO.setPlanSuffix(isuallrec.planSuffix);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		/* Read T5687 to get ANNIVERSARY-METHOD:*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/* Read T6658 to obtain T6658-ADD-NEW-ANNY. This field is*/
		/* very important to this program as it will decide wether*/
		/* to add or modify the anny record.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setItemtabl(t6658);
		itdmIO.setItemitem(t5687rec.anniversaryMethod);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), t6658)
		|| isNE(itdmIO.getItemitem(), t5687rec.anniversaryMethod)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t6658rec.t6658Rec.set(itdmIO.getGenarea());
		/* Read T5648 to get PCTINC which is used in the CAPCONT calcs:*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setItemtabl(t5648);
		itdmIO.setItemitem(t5687rec.anniversaryMethod);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), t5648)
		|| isNE(itdmIO.getItemitem(), t5687rec.anniversaryMethod)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5648rec.t5648Rec.set(itdmIO.getGenarea());
	}

protected void addNewAnny2000()
	{
		starts2000();
	}

protected void starts2000()
	{
		/* Read the ANNY using values held in ISUA:*/
		/* Note: It is read with ISUA-OLD values to get the original*/
		/* ANNY. The NEW one written has the NEW covr, rider & life.*/
		annyIO.setDataArea(SPACES);
		annyIO.setChdrnum(isuallrec.chdrnum);
		annyIO.setChdrcoy(isuallrec.company);
		annyIO.setLife(isuallrec.life);
		annyIO.setCoverage(isuallrec.oldcovr);
		annyIO.setRider(isuallrec.oldrider);
		annyIO.setPlanSuffix(isuallrec.planSuffix);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(annyIO.getStatuz());
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
		/* Move in new ANNY values (TRANNO, VALIDFLAG, CAPCONT) and*/
		/* Write new ANNY:*/
		compute(wsaaPctinc, 5).setRounded(div(t5648rec.pctinc, 100));
		setPrecision(annyIO.getCapcont(), 5);
		annyIO.setCapcont(mult(wsaaPctinc, annyIO.getCapcont()), true);
		zrdecplrec.amountIn.set(annyIO.getCapcont());
		a000CallRounding();
		annyIO.setCapcont(zrdecplrec.amountOut);
		annyIO.setValidflag("1");
		annyIO.setTranno(isuallrec.newTranno);
		annyIO.setLife(isuallrec.life);
		annyIO.setCoverage(isuallrec.coverage);
		annyIO.setRider(isuallrec.rider);
		annyIO.setFunction(varcom.writr);
		annyIO.setFormat(annyrec);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(annyIO.getStatuz());
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
	}

protected void modifyAnny3000()
	{
		starts3000();
	}

protected void starts3000()
	{
		/* Read the ANNY using values held in ISUA:*/
		annyIO.setDataArea(SPACES);
		annyIO.setChdrnum(isuallrec.chdrnum);
		annyIO.setChdrcoy(isuallrec.company);
		annyIO.setLife(isuallrec.life);
		annyIO.setCoverage(isuallrec.coverage);
		annyIO.setRider(isuallrec.rider);
		annyIO.setPlanSuffix(isuallrec.planSuffix);
		annyIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(annyIO.getStatuz());
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
		/* Valid-flag 2 the record and Rewrite:*/
		annyIO.setValidflag("2");
		annyIO.setFormat(annyrec);
		annyIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(annyIO.getStatuz());
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
		/* Move in new ANNY values (TRANNO, VALIDFLAG, CAPCONT)*/
		annyIO.setValidflag("1");
		annyIO.setTranno(isuallrec.newTranno);
		compute(wsaaPctinc, 5).setRounded(div(t5648rec.pctinc, 100));
		setPrecision(annyIO.getCapcont(), 5);
		annyIO.setCapcont(add((mult(wsaaPctinc, annyIO.getCapcont())), annyIO.getCapcont()), true);
		zrdecplrec.amountIn.set(annyIO.getCapcont());
		a000CallRounding();
		annyIO.setCapcont(zrdecplrec.amountOut);
		/* Write new ANNY:*/
		annyIO.setFunction(varcom.writr);
		annyIO.setFormat(annyrec);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(annyIO.getStatuz());
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(isuallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(covrIO.getSicurr());
		zrdecplrec.batctrcde.set(isuallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A090-EXIT*/
	}

protected void fatalError600()
	{
		error600();
		exit602();
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		isuallrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
