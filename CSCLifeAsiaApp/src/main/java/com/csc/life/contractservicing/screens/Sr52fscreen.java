package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr52fscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52fScreenVars sv = (Sr52fScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52fscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52fScreenVars screenVars = (Sr52fScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.txtype01.setClassString("");
		screenVars.shortds01.setClassString("");
		screenVars.txtype02.setClassString("");
		screenVars.shortds02.setClassString("");
		screenVars.dtyamt01.setClassString("");
		screenVars.txratea01.setClassString("");
		screenVars.txfxamta1.setClassString("");
		screenVars.txrateb01.setClassString("");
		screenVars.txfxamtb1.setClassString("");
		screenVars.dtyamt02.setClassString("");
		screenVars.txratea02.setClassString("");
		screenVars.txfxamta2.setClassString("");
		screenVars.txrateb02.setClassString("");
		screenVars.txfxamtb2.setClassString("");
		screenVars.dtyamt03.setClassString("");
		screenVars.txratea03.setClassString("");
		screenVars.txfxamta3.setClassString("");
		screenVars.txrateb03.setClassString("");
		screenVars.txfxamtb3.setClassString("");
		screenVars.dtyamt04.setClassString("");
		screenVars.txratea04.setClassString("");
		screenVars.txfxamta4.setClassString("");
		screenVars.txrateb04.setClassString("");
		screenVars.txfxamtb4.setClassString("");
		screenVars.dtyamt05.setClassString("");
		screenVars.txratea05.setClassString("");
		screenVars.txfxamta5.setClassString("");
		screenVars.txrateb05.setClassString("");
		screenVars.txfxamtb5.setClassString("");
		screenVars.txabsind01.setClassString("");
		screenVars.txabsind02.setClassString("");
	}

/**
 * Clear all the variables in Sr52fscreen
 */
	public static void clear(VarModel pv) {
		Sr52fScreenVars screenVars = (Sr52fScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.txtype01.clear();
		screenVars.shortds01.clear();
		screenVars.txtype02.clear();
		screenVars.shortds02.clear();
		screenVars.dtyamt01.clear();
		screenVars.txratea01.clear();
		screenVars.txfxamta1.clear();
		screenVars.txrateb01.clear();
		screenVars.txfxamtb1.clear();
		screenVars.dtyamt02.clear();
		screenVars.txratea02.clear();
		screenVars.txfxamta2.clear();
		screenVars.txrateb02.clear();
		screenVars.txfxamtb2.clear();
		screenVars.dtyamt03.clear();
		screenVars.txratea03.clear();
		screenVars.txfxamta3.clear();
		screenVars.txrateb03.clear();
		screenVars.txfxamtb3.clear();
		screenVars.dtyamt04.clear();
		screenVars.txratea04.clear();
		screenVars.txfxamta4.clear();
		screenVars.txrateb04.clear();
		screenVars.txfxamtb4.clear();
		screenVars.dtyamt05.clear();
		screenVars.txratea05.clear();
		screenVars.txfxamta5.clear();
		screenVars.txrateb05.clear();
		screenVars.txfxamtb5.clear();
		screenVars.txabsind01.clear();
		screenVars.txabsind02.clear();
	}
}
