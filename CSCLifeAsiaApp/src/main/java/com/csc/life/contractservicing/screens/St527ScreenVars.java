package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for ST527
 * @version 1.0 generated on 30/08/09 07:27
 * @author Quipoz
 */
public class St527ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(300);
	public FixedLengthStringData dataFields = new FixedLengthStringData(140).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,27);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData ownernum = DD.ownernum.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,112);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,122);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,130);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 140);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ownernumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 180);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ownernumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(198);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(100).isAPartOf(subfileArea, 0);
	public FixedLengthStringData clntname = DD.clntnme.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(subfileFields,47);
	public FixedLengthStringData clrole = DD.clrole.copy().isAPartOf(subfileFields,55);
	public FixedLengthStringData clrrrole = DD.clrrrole.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData forenum = DD.forenum.copy().isAPartOf(subfileFields,69);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,99);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 100);
	public FixedLengthStringData clntnmeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData clroleErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData clrrroleErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData forenumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 124);
	public FixedLengthStringData[] clntnmeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] clroleOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] clrrroleOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] forenumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 196);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData St527screensflWritten = new LongData(0);
	public LongData St527screenctlWritten = new LongData(0);
	public LongData St527screenWritten = new LongData(0);
	public LongData St527protectWritten = new LongData(0);
	public GeneralTable st527screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return st527screensfl;
	}

	public St527ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {forenum, select, clntnum, clntname, clrrrole, clrole};
		screenSflOutFields = new BaseData[][] {forenumOut, selectOut, clntnumOut, clntnmeOut, clrrroleOut, clroleOut};
		screenSflErrFields = new BaseData[] {forenumErr, selectErr, clntnumErr, clntnmeErr, clrrroleErr, clroleErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {rstate, chdrnum, cownnum, ownernum, cnttype, ctypedes, occdate, btdate, ptdate, pstate};
		screenOutFields = new BaseData[][] {rstateOut, chdrnumOut, cownnumOut, ownernumOut, cnttypeOut, ctypedesOut, occdateOut, btdateOut, ptdateOut, pstateOut};
		screenErrFields = new BaseData[] {rstateErr, chdrnumErr, cownnumErr, ownernumErr, cnttypeErr, ctypedesErr, occdateErr, btdateErr, ptdateErr, pstateErr};
		screenDateFields = new BaseData[] {occdate, btdate, ptdate};
		screenDateErrFields = new BaseData[] {occdateErr, btdateErr, ptdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, btdateDisp, ptdateDisp};
		
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = St527screen.class;
		screenSflRecord = St527screensfl.class;
		screenCtlRecord = St527screenctl.class;
		initialiseSubfileArea();
		protectRecord = St527protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(St527screenctl.lrec.pageSubfile);
	}
}
