package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface TaxdpfDAO extends BaseDAO<Taxdpf> {
	
    public List<Taxdpf> readTaxdbilData(Taxdpf taxdbilpfModel);
    public List<Taxdpf> searchTaxdpfRecord(Taxdpf taxdpf) throws SQLRuntimeException;
    public Map<String,List<Taxdpf>> searchTaxdRecord(String coy,List<String> chdrnum);
    public void updateTaxdpf(List<Taxdpf> taxdpfList);	
    public boolean insertTaxdPF(List<Taxdpf> taxdList);

    // Ticket#ILIFE-4404
    public Map<String, List<Taxdpf>> searchTaxdbilRecord(List<String> chdrnumlist);
    public List<Taxdpf> searchTaxdbilRecordByChdr(String coy,String chdrnum, Integer instFrom);
    
	public List<Taxdpf> searchTaxdrevRecord(String coy, String chdrnum);	
	public void deleteTaxdpfRecord(List<Taxdpf> taxdpfList);
}
