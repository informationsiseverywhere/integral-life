/*
 * File: Unladd.java
 * Date: 30 August 2009 2:49:34
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLADD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      UNIT LINKED ADD UNLT SUBROUTINE
*
* OVERVIEW.
* ---------
*
* UNLADD is the subroutine which will be called to write a UNLT
* record. ie. a temporary ULNK record.                                 be
*
*****************************************************************
* </pre>
*/
public class Unladd extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
		/* FORMATS */
	private String ulnkrec = "ULNKREC";
	private String unltunlrec = "UNLTUNLREC";
	private String chdrlnbrec = "CHDRLNBREC";
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Isuallrec isuallrec = new Isuallrec();
	private Syserrrec syserrrec = new Syserrrec();
		/*Unit Linked Fund Direction.*/
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
		/*Unit Linked transaction details*/
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seExit8090, 
		dbExit8190
	}

	public Unladd() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		chdrlnbIO.setDataArea(SPACES);
		chdrlnbIO.setChdrcoy(isuallrec.company);
		chdrlnbIO.setChdrnum(isuallrec.chdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			dbError8100();
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			dbError8100();
		}
		unltunlIO.setDataArea(SPACES);
		unltunlIO.setChdrcoy(isuallrec.company);
		unltunlIO.setChdrnum(isuallrec.chdrnum);
		unltunlIO.setLife(isuallrec.life);
		unltunlIO.setCoverage(isuallrec.coverage);
		unltunlIO.setRider(isuallrec.rider);
		unltunlIO.setSeqnbr(isuallrec.planSuffix);
		getDefaultUlnk1000();
		unltunlIO.setUalprcs(ulnkIO.getUalprcs());
		unltunlIO.setUalfnds(ulnkIO.getUalfnds());
		unltunlIO.setUspcprs(ulnkIO.getUspcprs());
		unltunlIO.setCurrfrom(isuallrec.effdate);
		unltunlIO.setCurrto(99999999);
		unltunlIO.setTranno(isuallrec.newTranno);
		unltunlIO.setPercOrAmntInd(ulnkIO.getPercOrAmntInd());
		unltunlIO.setNumapp(chdrlnbIO.getPolsum());
		unltunlIO.setFunction(varcom.writr);
		unltunlIO.setFormat(unltunlrec);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(),varcom.oK)) {
			syserrrec.dbparams.set(unltunlIO.getParams());
			syserrrec.statuz.set(unltunlIO.getStatuz());
			dbError8100();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void getDefaultUlnk1000()
	{
		/*START*/
		ulnkIO.setFunction(varcom.begn);
		ulnkIO.setChdrcoy(isuallrec.company);
		ulnkIO.setChdrnum(isuallrec.chdrnum);
		ulnkIO.setPlanSuffix(ZERO);
		ulnkIO.setFormat(ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(),varcom.oK)) {
			syserrrec.dbparams.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		isuallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		isuallrec.statuz.set(varcom.bomb);
		exit090();
	}
}
