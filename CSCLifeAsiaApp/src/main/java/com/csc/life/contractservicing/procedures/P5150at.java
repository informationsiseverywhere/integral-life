/*
 * File: P5150at.java
 * Date: 30 August 2009 0:15:16
 * Author: Quipoz Limited
 *
 * Class transformed from P5150AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.ArrayList;
import java.util.List;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* P5150AT - Billing Reversal AT Module
* ------------------------------------
*
* This module is  the  controlling  program for Billing Reversal.
* It is normally requested from program P5150.
*
* The CHDR record is read and the transaction number noted.
* Similarly, the PAYR record is read and its details noted.
*
* Table T6661 is read in order to obtain the name(s) of the
* reversal subroutine(s) to be executed. The subroutine(s)
* are then executed one after the other.
*
* The CHDR record is then updated with the new transaction
* number. Similarly, an update is performed on the PAYR record.
*
* A new reversal PTRN record is created in a new batch and
* the softlock on this contract is released.
*
*
*****************************************************************
* </pre>
*/
public class P5150at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "P5150AT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private PackedDecimalData wsaaPayrBillcd = new PackedDecimalData(8, 0);
		/* TABLES */
	private static final String t6654 = "T6654";
	private static final String t6661 = "T6661";
	private static final String t7508 = "T7508";
		/* FORMATS */
	private static final String payrrec = "PAYRREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String revtranscode="B521";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
//	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
//	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Syserrrec syserrrec = new Syserrrec();
	private T6654rec t6654rec = new T6654rec();
	private T6661rec t6661rec = new T6661rec();
	private T7508rec t7508rec = new T7508rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
//	private Datcon2rec datcon2rec = new Datcon2rec();
	private Reverserec reverserec = new Reverserec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Varcom varcom = new Varcom();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
//	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private Chdrpf chdrlif = null;
	private Payrpf payr = null;
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
	
	public P5150at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		callGenericProcessing2100();
		processChdrPayr3000();
		housekeeping4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaTransAreaInner.wsaaTransArea.set(atmodrec.transArea);
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		readChdr1100();
		readPayr1200();
		/*EXIT*/
	}

protected void readChdr1100()
	{
		/*START*/
		chdrlif = chdrpfDAO.getChdrlifRecord(atmodrec.company.toString(), wsaaPrimaryChdrnum.toString());
		if (chdrlif == null) {
			syserrrec.params.set(wsaaPrimaryChdrnum);
			syserrrec.statuz.set("CHDR MRNF");
			xxxxFatalError();
		}
		compute(wsaaTransAreaInner.wsaaNewTranno, 0).set(add(chdrlif.getTranno(), 1));
		/*EXIT*/
	}

protected void readPayr1200()
	{
		Payrpf payrpfModel = new Payrpf();
		payrpfModel.setChdrcoy(chdrlif.getChdrcoy().toString());
		payrpfModel.setChdrnum(chdrlif.getChdrnum());
		payrpfModel.setPayrseqno(1);
		payrpfModel.setValidflag("1");
		List<Payrpf> payrList = payrpfDAO.readPayrData(payrpfModel);
		if (payrList == null || payrList.isEmpty()) {
			syserrrec.params.set(chdrlif.getChdrnum());
			syserrrec.statuz.set("PAYR MRNF");
			xxxxFatalError();
			return;
		}
		payr = payrList.get(0);
		wsaaPayrBillcd.set(payr.getBillcd());
	}


protected void callGenericProcessing2100()
	{
		List<Itempf> itemList = itemDAO.getAllitemsbyCurrency("IT",atmodrec.company.toString(),t6661,wsaaBatckey.batcBatctrcde.toString());
		if (itemList == null || itemList.isEmpty()) {
			syserrrec.params.set(wsaaBatckey.batcBatctrcde);
			syserrrec.statuz.set("MRNF");
			xxxxFatalError();
			return;
		}
		t6661rec.t6661Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		reverserec.chdrnum.set(chdrlif.getChdrnum());
		reverserec.company.set(chdrlif.getChdrcoy());
		reverserec.tranno.set(chdrlif.getTranno());
		reverserec.newTranno.set(wsaaTransAreaInner.wsaaNewTranno);
		reverserec.language.set(atmodrec.language);
		reverserec.batchkey.set(atmodrec.batchKey);
		reverserec.transDate.set(wsaaTransAreaInner.wsaaTransactionDate);
		reverserec.transTime.set(wsaaTransAreaInner.wsaaTransactionTime);
		reverserec.user.set(wsaaTransAreaInner.wsaaUser);
		reverserec.termid.set(wsaaTransAreaInner.wsaaTermid);
		reverserec.life.set(wsaaTransAreaInner.wsaaLife);
		reverserec.coverage.set(wsaaTransAreaInner.wsaaCoverage);
		reverserec.rider.set(wsaaTransAreaInner.wsaaRider);
		reverserec.effdate1.set(wsaaTransAreaInner.wsaaEffdate1);
		reverserec.effdate2.set(wsaaTransAreaInner.wsaaEffdate2);
		reverserec.planSuffix.set(wsaaTransAreaInner.wsaaPlanSuffix);
		reverserec.ptrneff.set(ZERO);
		/* This is the old Billing Reversal before PTRN's were written at  */
		/* billing time. Therefore, no ACMV's were written at Billing      */
		/* either. Simply initialise new fields to spaces or zeroes.       */
		reverserec.ptrnBatcpfx.set(SPACES);
		reverserec.ptrnBatccoy.set(SPACES);
		reverserec.ptrnBatcbrn.set(SPACES);
		reverserec.ptrnBatctrcde.set(SPACES);
		reverserec.ptrnBatcbatch.set(SPACES);
		reverserec.ptrnBatcactyr.set(ZERO);
		reverserec.ptrnBatcactmn.set(ZERO);
		reverserec.statuz.set(Varcom.oK);
		if (isNE(t6661rec.subprog01, SPACES)) {
			callProgram(t6661rec.subprog01, reverserec.reverseRec);
			if (isNE(reverserec.statuz, Varcom.oK)) {
				syserrrec.params.set(reverserec.reverseRec);
				syserrrec.statuz.set(reverserec.statuz);
				xxxxFatalError();
			}
		}
		reverserec.statuz.set(Varcom.oK);
		if (isNE(t6661rec.subprog02, SPACES)) {
			callProgram(t6661rec.subprog02, reverserec.reverseRec);
			if (isNE(reverserec.statuz, Varcom.oK)) {
				syserrrec.params.set(reverserec.reverseRec);
				syserrrec.statuz.set(reverserec.statuz);
				xxxxFatalError();
			}
		}

	}

protected void processChdrPayr3000()
	{
		readhChdr3010();
		readhPayr3030();
		updateFields3040();
		rewrtChdr3050();
		rewrtPayr3070();
	}

protected void readhChdr3010()
	{
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction(Varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)
		|| isNE(chdrlifIO.getValidflag(), 1)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readhPayr3030()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(Varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void updateFields3040()
	{
		if (isNE(payrIO.getBillcd(), wsaaPayrBillcd)) {
			updatePayrNextdate3300();
		}
	}

protected void rewrtChdr3050()
	{
		chdrlifIO.setTranno(wsaaTransAreaInner.wsaaNewTranno);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		varcom.vrcmDate.set(wsaaTransAreaInner.wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransAreaInner.wsaaTransactionTime);
		varcom.vrcmUser.set(wsaaTransAreaInner.wsaaUser);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		varcom.vrcmCompTermid.set(wsaaTransAreaInner.wsaaTermid);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rewrtPayr3070()
	{
		payrIO.setTranno(wsaaTransAreaInner.wsaaNewTranno);
		payrIO.setTransactionDate(wsaaTransAreaInner.wsaaTransactionDate);
		payrIO.setTransactionTime(wsaaTransAreaInner.wsaaTransactionTime);
		payrIO.setTermid(wsaaTransAreaInner.wsaaTermid);
		payrIO.setUser(wsaaTransAreaInner.wsaaUser);
		payrIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void updatePayrNextdate3300()
	{
		start3301();
		continue3340();
	}

protected void start3301()
	{
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		if(BTPRO028Permission) {
			String key= payrIO.getBillchnl().toString().trim()+chdrlifIO.getCnttype().toString().trim()+payrIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key= payrIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key= payrIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						syserrrec.params.set(t6654);
						syserrrec.statuz.set("MRNF");
						xxxxFatalError();
					}
				}
			}
		}
		else {
		wsaaT6654Item.set(SPACES);
		wsaaT6654Billchnl.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrlifIO.getCnttype());
		List<Itempf> itemList = itemDAO.getAllitemsbyCurrency("IT",payrIO.getChdrcoy().toString(),t6654,wsaaT6654Item.toString());
		if (itemList == null || itemList.isEmpty()) {
			syserrrec.params.set(wsaaBatckey.batcBatctrcde);
			syserrrec.statuz.set("MRNF");
			xxxxFatalError();
		}

		if (itemList != null && !itemList.isEmpty()) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
			return ;
		}
		wsaaT6654Cnttype.set("***");
		itemList = itemDAO.getAllitemsbyCurrency("IT",payrIO.getChdrcoy().toString(),t6654,wsaaT6654Item.toString());
		if (itemList == null || itemList.isEmpty()) {
			syserrrec.params.set(t6654);
			syserrrec.statuz.set("MRNF");
			xxxxFatalError();
			return;
		}
		t6654rec.t6654Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		}
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(payrIO.getChdrcoy().toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void continue3340()
	{
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setIntDate1(payrIO.getBillcd().toString());
		datcon2Pojo.setFrequency("DY");
		datcon2Pojo.setFreqFactor(mult(t6654rec.leadDays, -1).toInt());
		datcon2Utils.calDatcon2(datcon2Pojo);
		payrIO.setNextdate(datcon2Pojo.getIntDate2());
		/*EXIT*/
	}
 
protected void housekeeping4000()
	{
		/*START*/
	   //ILIFE-1109
		updatePtrn4000();
		addNewPtrn4100();
		addNewBatch4500();
		dryProcessing8000();
		releaseSoftlock4800();
		/*EXIT*/
	}
/**
 * ILIFE-1109
 *  method is added for  billing reversal transaction 
 */
protected void updatePtrn4000()
{
	//IBPLIFE-13249 
//		ptrnpfDAO.updateValidPtrnrevRecordByEffdat(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString(),
//				revtranscode, chdrlifIO.getBtdate().toInt());
}

protected void addNewPtrn4100()
	{
		Ptrnpf ptrnIO = new Ptrnpf();
		PtrnTableDAM pTemp = new PtrnTableDAM();
		pTemp.setDataKey(atmodrec.batchKey);
		
		ptrnIO.setBatcpfx(pTemp.batcpfx.toString());
		ptrnIO.setBatccoy(pTemp.batccoy.toString());
		ptrnIO.setBatcbrn(pTemp.batcbrn.toString());
		ptrnIO.setBatcactyr(pTemp.batcactyr.toInt());
		ptrnIO.setBatcactmn(pTemp.batcactmn.toInt());
		ptrnIO.setBatctrcde(pTemp.batctrcde.toString());
		ptrnIO.setBatcbatch(pTemp.batcbatch.toString());
	
		ptrnIO.setTranno(wsaaTransAreaInner.wsaaNewTranno.toInt());
		ptrnIO.setPtrneff(wsaaTransAreaInner.wsaaEffdate1.toInt());
		ptrnIO.setDatesub(wsaaToday.toInt());
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx().toString());
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum().toString());
		ptrnIO.setTrdt(wsaaTransAreaInner.wsaaTransactionDate.toInt());
		ptrnIO.setTrtm(wsaaTransAreaInner.wsaaTransactionTime.toInt());
		ptrnIO.setUserT(wsaaTransAreaInner.wsaaUser.toInt());
		ptrnIO.setTermid(wsaaTransAreaInner.wsaaTermid.toString());
		ptrnIO.setValidflag("1");
		List<Ptrnpf> pList = new ArrayList<>();
		pList.add(ptrnIO);
		ptrnpfDAO.insertPtrnPF(pList);
	}

protected void addNewBatch4500()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(Varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, Varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void releaseSoftlock4800()
	{
		/*START*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set(chdrlifIO.getChdrpfx());
		sftlockrec.company.set(chdrlifIO.getChdrcoy());
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void dryProcessing8000()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlifIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		Itempf item = readT75088100();
		/* If item not found no Diary Update Processing is Required.       */
		if (item == null) {
			return ;
		}
		t7508rec.t7508Rec.set(StringUtil.rawToString(item.getGenarea()));
		
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(wsaaPrimaryChdrnum);
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaTransAreaInner.wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(ZERO);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypAplsupto.set(ZERO);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, Varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected Itempf readT75088100()
	{
		List<Itempf> itemList = itemDAO.getAllitemsbyCurrency("IT", atmodrec.company.toString(), t7508,
				wsaaT7508Key.toString());
		Itempf result = null;
		if (itemList == null || itemList.isEmpty()) {
			/* If item not found try other types of contract.                  */
			wsaaT7508Cnttype.set("***");
			itemList = itemDAO.getAllitemsbyCurrency("IT", atmodrec.company.toString(), t7508, wsaaT7508Key.toString());
			if (itemList != null && !itemList.isEmpty()) {
				result = itemList.get(0);
			}
		} else {
			result = itemList.get(0);
		}
		return result;
	}

protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorBomb();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, Varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		atmodrec.statuz.set(Varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
 */
private static final class WsaaTransAreaInner {

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(193);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 16);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 18);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 20);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTransArea, 22);
	private ZonedDecimalData wsaaEffdate1 = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 25).setUnsigned();
	private ZonedDecimalData wsaaEffdate2 = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 33).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaTransArea, 41);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaTransArea, 44);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 47);
	private FixedLengthStringData filler = new FixedLengthStringData(145).isAPartOf(wsaaTransArea, 48, FILLER).init(SPACES);
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	private PackedDecimalData drypRcesdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 61);
	private PackedDecimalData drypCbunst = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 66);
}
}
