/*
 * File: P2478lag.java
 * Date: 29 August 2009 23:32:49
 * Author: Quipoz Limited
 * 
 * Class transformed from P2478LAG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.recordstructures.Hcltrolcpy;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This subroutine is used to 'KEEPS' AGLF for subsequent inquiry
* screen.
*
* Functions:
*   There are no functions for this subroutine.
*
* Statii:
*      BOMB - System error occurred
*
* Linkage Area:
*        FUNCTION           PIC X(05).
*        STATUZ             PIC X(04).
*        HCLT-KEY           PIC X(10).  OCCURS 10 TIMES.
*  (These fields are contained in HCLTROLCPY)
*
*****************************************************
* </pre>
*/
public class P2478lag extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
		/* FORMATS */
	private String aglfrec = "AGLFREC";
	private String agntlagrec = "AGNTLAGREC";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private Hcltrolcpy hcltrolcpy = new Hcltrolcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Wssplife wssplife = new Wssplife();

	public P2478lag() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		hcltrolcpy.clntRoleInqRec = convertAndSetParam(hcltrolcpy.clntRoleInqRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		start000();
		exit000();
	}

protected void start000()
	{
		hcltrolcpy.statuz.set(varcom.oK);
		aglfIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			databaseError900();
		}
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(subString(hcltrolcpy.key[1], 1, 1));
		aglfIO.setAgntnum(subString(hcltrolcpy.key[2], 1, 8));
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			databaseError900();
		}
		agntlagIO.setParams(SPACES);
		agntlagIO.setAgntcoy(subString(hcltrolcpy.key[1], 1, 1));
		agntlagIO.setAgntnum(subString(hcltrolcpy.key[2], 1, 8));
		agntlagIO.setFormat(agntlagrec);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(agntlagIO.getStatuz());
			syserrrec.params.set(agntlagIO.getParams());
			databaseError900();
		}
		aglfIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			databaseError900();
		}
	}

protected void exit000()
	{
		exitProgram();
	}

protected void databaseError900()
	{
		/*DATABASE-ERROR*/
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		hcltrolcpy.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void systemError1000()
	{
		/*SYSTEM-ERROR*/
		syserrrec.syserrStatuz.set(hcltrolcpy.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		hcltrolcpy.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
