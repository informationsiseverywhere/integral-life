package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5h1Screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {23, 23, 2, 24}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5h1ScreenVars sv = (Sd5h1ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5h1screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5h1ScreenVars screenVars = (Sd5h1ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");		
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.cownnum.setClassString("");	
		screenVars.ownername.setClassString("");	
		screenVars.lifenum.setClassString("");	
		screenVars.lifename.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.ptdate.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.btdate.setClassString("");
		screenVars.currfromDisp.setClassString("");	
		screenVars.currfrom.setClassString("");	
		screenVars.effdateDisp.setClassString("");
		screenVars.effdate.setClassString("");
		screenVars.repymop.setClassString("");	
		screenVars.bankkey.setClassString("");
		screenVars.bnkbrndesc.setClassString("");
		screenVars.totalplprncpl.setClassString("");
		screenVars.totalplint.setClassString("");
		screenVars.totalaplprncpl.setClassString("");
		screenVars.totalaplint.setClassString("");
		screenVars.remngplprncpl.setClassString("");
		screenVars.remngplint.setClassString("");
		screenVars.remngaplprncpl.setClassString("");
		screenVars.remngaplint.setClassString("");
		screenVars.susbalnce.setClassString("");
		screenVars.repaymentamt.setClassString("");
		screenVars.crcind.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.refcode.setClassString("");
		screenVars.ansrepy.setClassString("");
		screenVars.descn.setClassString("");
		screenVars.rpcurr.setClassString("");
				
	}

/**
 * Clear all the variables in Sd5h1screen
 */
	public static void clear(VarModel pv) {
		Sd5h1ScreenVars screenVars = (Sd5h1ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();	
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.currcd.clear();
		screenVars.cownnum.clear();	
		screenVars.ownername.clear();	
		screenVars.lifenum.clear();	
		screenVars.lifename.clear();	
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();	
		screenVars.repymop.clear();	
		screenVars.bankkey.clear();
		screenVars.bnkbrndesc.clear();
		screenVars.totalplprncpl.clear();
		screenVars.totalplint.clear();
		screenVars.totalaplprncpl.clear();
		screenVars.totalaplint.clear();
		screenVars.remngplprncpl.clear();
		screenVars.remngplint.clear();
		screenVars.remngaplprncpl.clear();
		screenVars.remngaplint.clear();
		screenVars.susbalnce.clear();
		screenVars.repaymentamt.clear();
		screenVars.crcind.clear();
		screenVars.ddind.clear();
		screenVars.refcode.clear();
		screenVars.ansrepy.clear();
		screenVars.descn.clear();
		screenVars.rpcurr.clear();
	
	}
}


