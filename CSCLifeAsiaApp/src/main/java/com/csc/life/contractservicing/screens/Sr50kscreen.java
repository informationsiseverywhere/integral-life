package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50kscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50kScreenVars sv = (Sr50kScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50kscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50kScreenVars screenVars = (Sr50kScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.salutl01.setClassString("");
		screenVars.salutl02.setClassString("");
		screenVars.cltsex01.setClassString("");
		screenVars.cltsex02.setClassString("");
		screenVars.cltdob01Disp.setClassString("");
		screenVars.cltdob02Disp.setClassString("");
		screenVars.occpcode01.setClassString("");
		screenVars.occpcode02.setClassString("");
		screenVars.statcode01.setClassString("");
		screenVars.statcode02.setClassString("");
		screenVars.smoking01.setClassString("");
		screenVars.smoking02.setClassString("");
	}

/**
 * Clear all the variables in Sr50kscreen
 */
	public static void clear(VarModel pv) {
		Sr50kScreenVars screenVars = (Sr50kScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.lifcnum.clear();
		screenVars.lifename.clear();
		screenVars.salutl01.clear();
		screenVars.salutl02.clear();
		screenVars.cltsex01.clear();
		screenVars.cltsex02.clear();
		screenVars.cltdob01Disp.clear();
		screenVars.cltdob01.clear();
		screenVars.cltdob02Disp.clear();
		screenVars.cltdob02.clear();
		screenVars.occpcode01.clear();
		screenVars.occpcode02.clear();
		screenVars.statcode01.clear();
		screenVars.statcode02.clear();
		screenVars.smoking01.clear();
		screenVars.smoking02.clear();
	}
}
