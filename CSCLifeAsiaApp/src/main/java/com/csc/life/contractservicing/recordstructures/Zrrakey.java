package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:22
 * Description:
 * Copybook name: ZRRAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrrakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrraFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zrraKey = new FixedLengthStringData(64).isAPartOf(zrraFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrraChdrcoy = new FixedLengthStringData(1).isAPartOf(zrraKey, 0);
  	public FixedLengthStringData zrraChdrnum = new FixedLengthStringData(8).isAPartOf(zrraKey, 1);
  	public FixedLengthStringData zrraLife = new FixedLengthStringData(2).isAPartOf(zrraKey, 9);
  	public FixedLengthStringData zrraCoverage = new FixedLengthStringData(2).isAPartOf(zrraKey, 11);
  	public FixedLengthStringData zrraRider = new FixedLengthStringData(2).isAPartOf(zrraKey, 13);
  	public PackedDecimalData zrraPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(zrraKey, 15);
  	public FixedLengthStringData zrraBillfreq = new FixedLengthStringData(2).isAPartOf(zrraKey, 18);
  	public PackedDecimalData zrraInstfrom = new PackedDecimalData(8, 0).isAPartOf(zrraKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(zrraKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrraFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrraFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}