/*
 * File: Revcoll.java
 * Date: 30 August 2009 2:06:08
 * Author: Quipoz Limited
 * 
 * Class transformed from REVCOLL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.ObjectUtils;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZctnrevTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.dataaccess.ZptnrevTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrvsTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LinprevTableDAM;
import com.csc.life.contractservicing.dataaccess.RtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CoprpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.model.Coprpf;
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* REVCOLL
* ~~~~~~~
* The parameters passed to this program are set up in the copybook
* REVESEREC.
*
* Delete paid instalments for a contract:-
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*     Read the life instalments file for this contract (LINSPF) with
*     the logical view LINPREV using the company/contract number as the
*     key. This view will only select paid contracts as unpaid
*     contracts are dealt with by billing reversal. If the instalment
*     from date is greater than or equal to the reverse to date
*     (effective date 1 from linkage) delete the record.
*          
*     Read the contract header record (CHDRPF) using the logical view
*     CHDR. Subtract the LINS amount from the outstanding balance. Move
*     the LINS from date (- 1 day) to the paid to date. Rewrite the
*     record.
*        
*     Repeat the process until all relevant paid instalments for this
*     contract are deleted.
*
* Put receipt amount back into suspense:-
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*     Read the receipt file (RTRNPF) using the logical view RTRNREV
*     with a key of contract number/transaction number from linkage.
*
*     For each record found:
*         Overwrite the accounting month/year with the batch
*         accounting month/year from linkage.
*         Overwrite the tranno with the NEW tranno from linkage.
*         Overwrite the batch key with the new batch key from linkage.
*         Multiply the original currency amount by -1.
*         Multiply the accounting currency amount by -1.
*         Overwrite the transaction reference with the NEW tranno from
*         linkage.
*         Overwrite the transaction description with the long
*         description of the new tranno from T1688.
*         Overwrite the effective date with todays date.
*         Write the new record to the database by calling the
*         subroutine 'LIFRTRN'.
*
* Reverse the account movements from the ledger:-
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*     Read the Account movements file (ACMVPF) using the logical view
*     ACMVREV with a key of contract number/transaction number from
*     linkage.
*
*     For each record found:
*         Overwrite the accounting month/year with the batch
*         accounting month/year from linkage.
*         Overwrite the tranno with the NEW tranno from linkage.
*         Overwrite the batch key with the new batch key from linkage.
*         Multiply the original currency amount by -1.
*         Multiply the accounting currency amount by -1.
*         Overwrite the transaction reference with the NEW tranno from
*         linkage.
*         Overwrite the transaction description with the long
*         description of the new tranno from T1688.
*         Overwrite the effective date with todays date.
*         Write the new record to the database by calling the
*         subroutine 'LIFACMV'.
*
* Update Agent commission records:-
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*     Read the Agents commission file (AGCMPF) with the logical
*     view AGCMREV using the contract number as the key.
*
*     For each record found carry out the following processing:-
*         If the effective date of the record is less than or equal to
*         the new paid to date (effective date 1 from linkage) and the
*         paid to date of the record is greater than the new paid to
*         date:-
*              Set up the details required in the COMLINKREC copybook
*              and call the relevant reversal subroutine. This is
*              obtained from T5644 using the method from the record
*              for this type of commission as found earlier.
*               
*              On return from the subroutine move the relevant
*              earned/paid amounts over the existing AGCM fields.
*              Change the paid to date to the effective date 1 from
*              linkage. Set the valid flag on the record to 1. Rewrite
*              the record.
*
*****************************************************************
* </pre>
*/
public class Revcoll extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVCOLL";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaT6661Exist = new FixedLengthStringData(1);
	private Validator t6661Found = new Validator(wsaaT6661Exist, "Y");
	private Validator t6661Nfnd = new Validator(wsaaT6661Exist, "N");

	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0).setUnsigned();

	private FixedLengthStringData wsaaT5671 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 4);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaPostyearX = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPostyear = new ZonedDecimalData(4, 0).isAPartOf(wsaaPostyearX, 0).setUnsigned();

	private FixedLengthStringData wsaaPostmonthX = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPostmonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaPostmonthX, 0).setUnsigned();
		/* 88 INITIAL-COMMISSION       VALUE 1.                         
		 88 RENEWAL-COMMISSION       VALUE 2.                         
		 88 SERVICING-COMMISSION     VALUE 3.                         
		 88 OVERRIDE-COMMISSION      VALUE 4.                         */
	private ZonedDecimalData wsaaCommType = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);

	private FixedLengthStringData wsaaNextAgcmFlag = new FixedLengthStringData(1);
	private Validator getNextAgcm = new Validator(wsaaNextAgcmFlag, "Y");
	private Validator dontGetNextAgcm = new Validator(wsaaNextAgcmFlag, "N");
		/* WSAA-SAVED-AMOUNTS */
	private PackedDecimalData[] wsaaPayamnt = PDInittedArray(3, 17, 2);
	private PackedDecimalData[] wsaaErndamt = PDInittedArray(3, 17, 2);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();

	private FixedLengthStringData wsaaTaxTranType = new FixedLengthStringData(4);
	private Validator wsaaTaxTranValid = new Validator(wsaaTaxTranType, "PREM","CNTF","NINV");
	private Validator wsaaTaxTranCollect = new Validator(wsaaTaxTranType, "PREM","CNTF");
	private Validator wsaaTaxTranNvst = new Validator(wsaaTaxTranType, "NINV");
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5644 = "T5644";
	private static final String t5671 = "T5671";
	private static final String t5687 = "T5687";
	private static final String t6661 = "T6661";
	private static final String th605 = "TH605";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmrevTableDAM agcmrevIO = new AgcmrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrrevTableDAM covrrevIO = new CovrrevTableDAM();
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private HdivrevTableDAM hdivrevIO = new HdivrevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LinprevTableDAM linprevIO = new LinprevTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private RtrnrevTableDAM rtrnrevIO = new RtrnrevTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZctnrevTableDAM zctnrevIO = new ZctnrevTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZptnrevTableDAM zptnrevIO = new ZptnrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Batckey wsaaBatckey = new Batckey();
	private T5644rec t5644rec = new T5644rec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private Th605rec th605rec = new Th605rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Greversrec greversrec = new Greversrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private boolean riskPremflag = false; //ILIFE-7845 
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845
	List<Rskppf> rskppfList = new ArrayList<Rskppf>();
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	private Premiumrec premiumrec = new Premiumrec();
	private List<Rskppf> insertRiskPremList = new ArrayList<>();
	private ZonedDecimalData wsaaRiskDate = new ZonedDecimalData(8, 0);
	private boolean isPaidtodate = false; //ILIFE-7845
	Chdrpf chdrpf = null;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Map<String, List<Itempf>> t5567Map;
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao",ItemDAO.class);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 3);
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private T5567rec t5567rec = new T5567rec();
	private ZonedDecimalData wsaapolFee = new ZonedDecimalData(8, 2);
	private RertpfDAO rertpfDAO = getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	private List<Rertpf> rertList = new ArrayList<Rertpf>();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private CoprpfDAO coprpfDAO = getApplicationContext().getBean("coprpfDAO", CoprpfDAO.class);
	private List<Covrpf> covrpfList = new ArrayList<Covrpf>();
	/*Agent Commission Dets- Billing Change*/
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
		/*Logical File for Reversals*/
	private AgcmrvsTableDAM agcmrvsIO = new AgcmrvsTableDAM();
	private LinspfDAO linspfDAO  =  getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private List<Linspf> linspfList = new ArrayList<Linspf>();
	
	private boolean BTPRO033Permission;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		a1050ReadNextLinp, 
		b1050Next, 
		b1050Exit, 
		nextAcmv1750, 
		call4020, 
		exit4090, 
		call5020, 
		exit5090
	}

	public Revcoll() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		wsaaBatckey.set(reverserec.batchkey);
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		/* MOVE ZERO                   TO WSAA-COMMISSION-TYPE.         */
		comlinkrec.currto.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		/* get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		//ILIFE-7845 starts
		if ((null!= reverserec.ptrnBatctrcde) &&(isEQ(reverserec.ptrnBatctrcde,"T536"))) {
			isPaidtodate=true;
		}
		//ILIFE-7845 ends
		readTables600();
		readContractHeader800();
		readPayrFile900();
		processLins1000();
		lastInstdate1100();
		updateContractHeader1200();
		updatePayrFile1300();
		updateHdivFile1360();
		readReceiptFile1400();
		reverseAcctMovements1600();
		processAcmvOptical1610();
		updateAgentCommission2000();
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setLife("01");
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setCoverage("01");
		covrenqIO.setRider("00");
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setStatuz(varcom.oK);
		/* Reverse units at component level                                */
		while ( !(isEQ(covrenqIO.getStatuz(), varcom.endp))) {
			reverseUnits3000();
		}
		
		/* PERFORM 3000-REVERSE-UNITS.                                  */
		/* Read table TH605 to see if Bonus Workbench Extraction used      */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(th605);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError8000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench  *                                              */
		processZptn4000();
		processZctn5000();
	}

	protected void updateRiskPrem() {
		Rskppf rskppf = new Rskppf();
		rskppfList = new ArrayList<Rskppf>();
		if (isPaidtodate) {
			rskppfList = rskppfDAO.fetchRecordReversal(reverserec.company.toString(), reverserec.chdrnum.toString(),
					reverserec.effdate1.toInt());
		} else {
			rskppfList = rskppfDAO.fetchRecordReversal(reverserec.company.toString(), reverserec.chdrnum.toString(),
					wsaaRiskDate.toInt());
		}
		if (!ObjectUtils.isEmpty(rskppfList)) {
			for (int i = 0; i < rskppfList.size(); i++) {
				rskppf = new Rskppf();
				premiumrec.cnttype.set(rskppfList.get(i).getCnttype());
				premiumrec.crtable.set(rskppfList.get(i).getCrtable());
				premiumrec.calcTotPrem.set(rskppfList.get(i).getCovrinstprem());
				callProgram("RISKPREMIUM", premiumrec.premiumRec);
				rskppf.setUnique_Number(rskppfList.get(i).getUnique_Number());
				rskppf.setChdrcoy(rskppfList.get(i).getChdrcoy());
				rskppf.setChdrnum(rskppfList.get(i).getChdrnum());
				rskppf.setLife(rskppfList.get(i).getLife());
				rskppf.setCoverage(rskppfList.get(i).getCoverage());
				rskppf.setRider(rskppfList.get(i).getRider());
				rskppf.setCnttype(rskppfList.get(i).getCnttype());
				rskppf.setCrtable(rskppfList.get(i).getCrtable());
				rskppf.setDatefrm(rskppfList.get(i).getDatefrm());
				rskppf.setDateto(rskppfList.get(i).getDateto());
				rskppf.setRiskprem(rskppfList.get(i).getRiskprem().subtract(premiumrec.riskPrem.getbigdata()));
				readT5567();
				rskppf.setPolfee(rskppfList.get(i).getPolfee().subtract(wsaapolFee.getbigdata()));
				insertRiskPremList.add(rskppf);
			}
		}
		if (insertRiskPremList != null && !insertRiskPremList.isEmpty())
			rskppfDAO.updateRiskPrem(insertRiskPremList);
	}

protected void readT5567() {
		chdrpf = chdrpfDAO.getchdrRecordData(reverserec.company.toString(),reverserec.chdrnum.toString());
		t5567Map = itemDao.loadSmartTable("IT", reverserec.company.toString(), "T5567");
		wsaaCnttype.set(chdrpf.getCnttype());/* IJTI-1523 */
		wsaaCntcurr.set(chdrpf.getCntcurr());
		if(t5567Map.containsKey(wsaaItemitem.toString())) {
			List<Itempf> t5567List = t5567Map.get(wsaaItemitem.toString());
	        for(Itempf t5567Item: t5567List) {
	             	t5567rec.t5567Rec.set(StringUtil.rawToString(t5567Item.getGenarea()));           
	        }
		}else {
			t5567rec.t5567Rec.set(SPACES);
		}
		
		for (wsaaSub2.set(1); !(isGT(wsaaSub2,10)); wsaaSub2.add(1)){
			if (isEQ(t5567rec.billfreq[wsaaSub2.toInt()],chdrpf.getBillfreq())) {
				wsaapolFee.set(t5567rec.cntfee[wsaaSub2.toInt()]);
				wsaaSub2.set(11);
			}else {
				wsaapolFee.set(ZERO);
			}
		}
		
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTables600()
	{
		para600();
	}

protected void para600()
	{
		/*read transaction accounting rules table T5645                */
		/*MOVE REVE-COMPANY           TO ITEM-ITEMCOY.                 */
		/*MOVE T5645                  TO ITEM-ITEMTABL.                */
		/*MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/*MOVE WSAA-SUBR              TO ITEM-ITEMITEM.                */
		/*MOVE 'READR'                TO ITEM-FUNCTION.                */
		/*CALL 'ITEMIO' USING         ITEM-PARAMS.                     */
		/*IF ITEM-STATUZ              NOT = '****'                     */
		/*   MOVE ITEM-PARAMS         TO SYSR-PARAMS                   */
		/*   PERFORM 8000-SYSTEM-ERROR.                                */
		/*MOVE ITEM-GENAREA           TO T5645-T5645-REC.              */
		/*MOVE 'IT'                   TO DESC-DESCPFX.                 */
		/*MOVE T5645                  TO DESC-DESCTABL.                */
		/*MOVE WSAA-SUBR              TO DESC-DESCITEM.                */
		/*MOVE REVE-COMPANY           TO DESC-DESCCOY.                 */
		/*MOVE 'E'                    TO DESC-LANGUAGE.           <010>*/
		/*MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.           <010>*/
		/*MOVE REVE-LANGUAGE          TO DESC-LANGUAGE.           <010>*/
		/*MOVE 'READR'                TO DESC-FUNCTION.                */
		/*CALL 'DESCIO' USING DESC-PARAMS.                             */
		/*IF DESC-STATUZ              NOT = '****'                     */
		/*                            AND NOT = 'MRNF'                 */
		/*    MOVE DESC-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 8000-SYSTEM-ERROR.                               */
		/* Obtain transaction description from T1688                       */
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t1688);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError8000();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

	/**
	* <pre>
	*   Read contract header
	* </pre>
	*/
protected void readContractHeader800()
	{
		/*PARA*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void readPayrFile900()
	{
		para910();
	}

protected void para910()
	{
		/* Read the PAYR file with a function READH - at the moment        */
		/* we assume that there is only one PAYR record per contract.      */
		/* This code will be changed in the future to cater for            */
		/* multiple PAYR records.......17/09/91                            */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(reverserec.company);
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError8100();
		}
	}

	/**
	* <pre>
	*   Delete paid instalments for a contract
	* </pre>
	*/
protected void processLins1000()
	{
		para1000();
	}

protected void para1000()
	{
		linprevIO.setParams(SPACES);
		linprevIO.setChdrcoy(reverserec.company);
		linprevIO.setChdrnum(reverserec.chdrnum);
		linprevIO.setInstfrom(99999999);
		/*    MOVE BEGN                   TO LINPREV-FUNCTION.*/
		/* MOVE BEGNH                  TO LINPREV-FUNCTION.             */
		linprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		linprevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		linprevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, linprevIO);
		if (isNE(linprevIO.getStatuz(), varcom.oK)
		&& isNE(linprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(linprevIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.company, linprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, linprevIO.getChdrnum())) {
			linprevIO.setStatuz(varcom.endp);
		}
		BTPRO033Permission = FeaConfg.isFeatureExist(reverserec.company.toString(), "BTPRO033", appVars,"IT");
		while ( !(isEQ(linprevIO.getStatuz(), varcom.endp))) {
			a1050UpdatePaidInstalments();
			//ILIFE-7845 starts
			if (isPaidtodate) {
				riskPremflag = FeaConfg.isFeatureExist(reverserec.company.toString(), RISKPREM_FEATURE_ID, appVars,
						"IT");
				if (riskPremflag) {
					updateRiskPrem();
				}
			}
			//ILIFE-7845 ends
		}
		
	}

	/**
	* <pre>
	******THIS SECTION IS NO LONGER PERFORMED*********************    
	*1050-DELETE-PAID-INSTALMENTS   SECTION.                          
	*1050-PARA.                                                       
	*****IF LINPREV-INSTFROM < REVE-EFFDATE-1                         
	*****      MOVE NEXTR                  TO LINPREV-FUNCTION        
	*****ELSE                                                         
	*****      MOVE READH                  TO LINPREV-FUNCTION        
	*****      CALL 'LINPREVIO'            USING SYSR-PARAMS          
	*****      IF LINPREV-STATUZ           NOT = O-K                  
	*****          MOVE LINPREV-PARAMS     TO SYSR-PARAMS             
	*****      ELSE                                                   
	*****          SUBTRACT LINPREV-INSTAMT(1) FROM CHDRLIF-OUTSTAMT  
	*****          PERFORM 1060-CALL-DATCON2                          
	*****          MOVE DELET               TO LINPREV-FUNCTION       
	*****          CALL 'LINPREVIO'         USING LINPREV-PARAMS      
	*****          IF LINPREV-STATUZ        NOT = O-K                 
	*****             MOVE LINPREV-PARAMS   TO SYSR-PARAMS            
	*****          ELSE                                               
	***Begin on next record so as to release the hold on the record   
	***This should position on the record after the deleted record    
	*****             MOVE BEGN              TO LINPREV-FUNCTION.     
	*****CALL 'LINPREVIO'            USING LINPREV-PARAMS.            
	*****IF LINPREV-STATUZ           NOT = O-K AND NOT = ENDP         
	*****   MOVE LINPREV-PARAMS      TO SYSR-PARAMS                   
	*****IF REVE-COMPANY             NOT = LINPREV-CHDRCOY OR         
	*****   REVE-CHDRNUM             NOT = LINPREV-CHDRNUM            
	*****   MOVE ENDP                TO LINPREV-STATUZ.               
	*1059-EXIT.                                                       
	*****EXIT.                                                        
	* </pre>
	*/
protected void a1050UpdatePaidInstalments()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a1050Para();
				case a1050ReadNextLinp: 
					a1050ReadNextLinp();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a1050Para()
	{
		if (isNE(linprevIO.getPayflag(), "P")) {
			goTo(GotoLabel.a1050ReadNextLinp);
		}
		/* DISPLAY 'LINPREV-INSTFROM = ' LINPREV-INSTFROM.*/
		/* DISPLAY 'LINPREV-PAYFLAG = ' LINPREV-PAYFLAG.*/
		/* DISPLAY 'PTDATE  = ' CHDRLIF-PTDATE.*/
		/* DISPLAY 'OUTSTAMT   = ' CHDRLIF-OUTSTAMT.*/
		/* IF LINPREV-INSTFROM < REVE-EFFDATE-1                         */
		if (isLT(linprevIO.getInstfrom(), reverserec.ptrneff)) {
			goTo(GotoLabel.a1050ReadNextLinp);
		}
		linprevIO.setPayflag("O");
		/* ADD   LINPREV-INSTAMT(1) TO CHDRLIF-OUTSTAMT.                */
		setPrecision(chdrlifIO.getOutstamt(), 2);
		chdrlifIO.setOutstamt(add(chdrlifIO.getOutstamt(), linprevIO.getInstamt(6)));
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			setPrecision(chdrlifIO.getInsttot(wsaaSub), 0);
			chdrlifIO.setInsttot(wsaaSub, sub(chdrlifIO.getInsttot(wsaaSub), linprevIO.getInstamt(wsaaSub)));
		}
		/* Update the Outstanding Amount field on the PAYR record          */
		/* ADD   LINPREV-INSTAMT(1) TO PAYR-OUTSTAMT.           <CAS1.0>*/
		setPrecision(payrIO.getOutstamt(), 2);
		payrIO.setOutstamt(add(payrIO.getOutstamt(), linprevIO.getInstamt(6)));
		/* MOVE SPACES                 TO DTC2-STATUZ.                  */
		/* MOVE CHDRLIF-PTDATE        TO DTC2-INT-DATE-1.               */
		/* MOVE -1                     TO DTC2-FREQ-FACTOR.             */
		/* MOVE CHDRLIF-BILLFREQ       TO DTC2-FREQUENCY.               */
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC.                       */
		/* IF DTC2-STATUZ              NOT = '****'                     */
		/*     MOVE DTC2-STATUZ        TO SYSR-STATUZ                   */
		/* MOVE DTC2-INT-DATE-2        TO CHDRLIF-PTDATE.               */
		/* Update the PTDATE field on the PAYR record                      */
		/* MOVE DTC2-INT-DATE-2        TO PAYR-PTDATE.             <016>*/
		chdrlifIO.setPtdate(linprevIO.getInstfrom());
		payrIO.setPtdate(linprevIO.getInstfrom());
		/* DISPLAY 'UPDATED PTDATE  = ' CHDRLIF-PTDATE.*/
		/* DISPLAY 'UPDATED OUTSTAMT   = ' CHDRLIF-OUTSTAMT.*/
		linprevIO.setFormat(formatsInner.linprevrec);
		/* MOVE REWRT                  TO LINPREV-FUNCTION              */
		linprevIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, linprevIO);
		if (isNE(linprevIO.getStatuz(), varcom.oK)
		&& isNE(linprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(linprevIO.getParams());
			dbError8100();
		}
		if(BTPRO033Permission && isEQ(linprevIO.getProraterec(),"Y")) {
			Coprpf coprpf = new Coprpf();
			coprpf.setChdrcoy(linprevIO.getChdrcoy().toString());
			coprpf.setChdrnum(linprevIO.getChdrnum().toString());
			coprpf.setInstfrom(linprevIO.getInstfrom().toInt());
			coprpf.setInstto(linprevIO.getInstto().toInt());
			coprpf.setValidflag("2");
			List<Coprpf> coprList = coprpfDAO.getCoprpfRecord(coprpf);
			List<Coprpf> updateCoprList = new ArrayList<Coprpf>();
			if(!coprList.isEmpty()) {
				for(Coprpf c:coprList) {
					if(isEQ(c.getTranno(),reverserec.tranno)) {
						Coprpf copr = new Coprpf(c);
						copr.setValidflag("1");
						copr.setTranno(reverserec.newTranno.toInt());
						updateCoprList.add(copr);
					}
				}
				if (!updateCoprList.isEmpty()) {
					coprpfDAO.updateCoprpf(updateCoprList);
				}
			}
			coprList.clear();
			updateCoprList.clear();
		}
		rertList = rertpfDAO.getRertpfList(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString(),"2", reverserec.tranno.toInt());
		List<Covrpf> covrpfList = covrpfDAO.getCovrpfByTranno(reverserec.company.toString(),reverserec.chdrnum.toString());
		List<Covrpf> deleteCovrList = new ArrayList<Covrpf>();
		List<Covrpf> updateCovrList = new ArrayList<Covrpf>();
		Boolean rerateFlag = false;
		if (!rertList.isEmpty()) {
			List<Rertpf> updateRertList = new ArrayList<Rertpf>();
			Integer tranNoTemp = 0;
			for (Rertpf r : rertList) {
				if(isEQ(linprevIO.getInstfrom(), r.getEffdate()) && (isEQ(r.getTranno(),reverserec.tranno)))
					r.setValidflag("1");
					r.setTranno(reverserec.newTranno.toInt());
					updateRertList.add(r);
					Boolean updateflag = true;
					rerateFlag = true;
					if(covrpfList != null && covrpfList.size() > 0)
					{
						for(Covrpf covr : covrpfList) {
							if (r.getLife().equals(covr.getLife()) && r.getCoverage().equals(covr.getCoverage())
										&& r.getRider().equals(covr.getRider())) {
								
								/*    If this component has a TRANNO equal to the TRANNO we are*/
								/*    reversing, then it has been Lapsed/Paid-up and must now be*/
								/*    reversed.  If not, then find the next component.*/
								if (isEQ(covr.getValidflag(),"1") && isEQ(covr.getTranno(), reverserec.tranno)) {
									deleteCovrList.add(covr);
								}
								if(isEQ(covr.getValidflag(),"2") && updateflag && isNE(covr.getTranno(), reverserec.tranno)){
									if(tranNoTemp == 0){
									    	covr.setCurrto(varcom.vrcmMaxDate.toInt()); //PINNACLE-2568
										updateCovrList.add(covr);
										tranNoTemp = covr.getTranno();
									}else if(isEQ(tranNoTemp,covr.getTranno())){
									    	covr.setCurrto(varcom.vrcmMaxDate.toInt()); //PINNACLE-2568
										updateCovrList.add(covr);
									}else{
										updateflag = false;
									}
									break;
								}
							}
						}
					}
					
				
			}
			if (!updateRertList.isEmpty()) {
				rertpfDAO.updateRertList(updateRertList);
			}
			if(deleteCovrList !=null && updateCovrList != null){
				/* delete the lapsed one */
				covrpfDAO.deleteCovrpfRecord(deleteCovrList);
				/*change the latest tranno validflag to 1*/
				covrpfDAO.updateCovrValidFlagTo1(updateCovrList);
			}
			if(rerateFlag){
				processAgcms5000();
			}
		}
		
		
	}
protected void processAgcms5000()
{
	begn5010();
}

protected void begn5010()
{
	agcmrvsIO.setParams(SPACES);
	agcmrvsIO.setChdrcoy(reverserec.company);
	agcmrvsIO.setChdrnum(reverserec.chdrnum);
	agcmrvsIO.setTranno(reverserec.tranno);
	agcmrvsIO.setFunction(varcom.begn);
	//performance improvement -- Anjali
	agcmrvsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	agcmrvsIO.setFormat(formatsInner.agcmrvsrec);
	SmartFileCode.execute(appVars, agcmrvsIO);
	if (isNE(agcmrvsIO.getStatuz(),varcom.oK)
	&& isNE(agcmrvsIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(agcmrvsIO.getParams());
		syserrrec.statuz.set(agcmrvsIO.getStatuz());
		systemError8000();
	}
	if (isNE(agcmrvsIO.getChdrcoy(),reverserec.company)
	|| isNE(agcmrvsIO.getChdrnum(),reverserec.chdrnum)
	|| isNE(agcmrvsIO.getTranno(),reverserec.tranno)) {
		agcmrvsIO.setStatuz(varcom.endp);
	}
	while ( !(isEQ(agcmrvsIO.getStatuz(),varcom.endp))) {
		deleteReinstateAgcms5100();
	}
	
}

protected void deleteReinstateAgcms5100()
{
	begnh5110();
}

protected void begnh5110()
{
	agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
	agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
	agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
	agcmdbcIO.setLife(agcmrvsIO.getLife());
	agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
	agcmdbcIO.setRider(agcmrvsIO.getRider());
	agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
	agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
	agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
	agcmdbcIO.setFunction(varcom.readh);
	SmartFileCode.execute(appVars, agcmdbcIO);
	if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(agcmdbcIO.getParams());
		syserrrec.statuz.set(agcmdbcIO.getStatuz());
		systemError8000();
	}
	if (isNE(agcmdbcIO.getChdrcoy(),agcmrvsIO.getChdrcoy())
	|| isNE(agcmdbcIO.getChdrnum(),agcmrvsIO.getChdrnum())
	|| isNE(agcmdbcIO.getAgntnum(),agcmrvsIO.getAgntnum())
	|| isNE(agcmdbcIO.getLife(),agcmrvsIO.getLife())
	|| isNE(agcmdbcIO.getCoverage(),agcmrvsIO.getCoverage())
	|| isNE(agcmdbcIO.getRider(),agcmrvsIO.getRider())
	|| isNE(agcmdbcIO.getPlanSuffix(),agcmrvsIO.getPlanSuffix())
	|| isNE(agcmdbcIO.getSeqno(),agcmrvsIO.getSeqno())) {
		syserrrec.statuz.set(agcmdbcIO.getStatuz());
		syserrrec.params.set(agcmdbcIO.getParams());
		systemError8000();
	}
	if (!isEQ(agcmdbcIO.getTranno(),reverserec.tranno)) { 
		readNextRec();
	}
	agcmdbcIO.setFunction(varcom.delet);
	SmartFileCode.execute(appVars, agcmdbcIO);
	if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(agcmdbcIO.getParams());
		syserrrec.statuz.set(agcmdbcIO.getStatuz());
		systemError8000();
	}
	agcmdbcIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, agcmdbcIO);
	if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(agcmdbcIO.getParams());
		syserrrec.statuz.set(agcmdbcIO.getStatuz());
		systemError8000();
	}
	if (isNE(agcmdbcIO.getChdrcoy(),agcmrvsIO.getChdrcoy())
	|| isNE(agcmdbcIO.getChdrnum(),agcmrvsIO.getChdrnum())
	|| isNE(agcmdbcIO.getAgntnum(),agcmrvsIO.getAgntnum())
	|| isNE(agcmdbcIO.getLife(),agcmrvsIO.getLife())
	|| isNE(agcmdbcIO.getCoverage(),agcmrvsIO.getCoverage())
	|| isNE(agcmdbcIO.getRider(),agcmrvsIO.getRider())
	|| isNE(agcmdbcIO.getPlanSuffix(),agcmrvsIO.getPlanSuffix())
	|| isNE(agcmdbcIO.getSeqno(),agcmrvsIO.getSeqno())) {
		syserrrec.statuz.set(agcmdbcIO.getStatuz());
		syserrrec.params.set(agcmdbcIO.getParams());
		systemError8000();
	}
	agcmdbcIO.setValidflag("1");
	agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
	agcmdbcIO.setFunction(varcom.writd);
	agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
	SmartFileCode.execute(appVars, agcmdbcIO);
	if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(agcmdbcIO.getParams());
		syserrrec.statuz.set(agcmdbcIO.getStatuz());
		systemError8000();
	}
	readNextRec();
}
protected void readNextRec() {
	agcmrvsIO.setFunction(varcom.nextr);
	agcmrvsIO.setFormat(formatsInner.agcmrvsrec);
	SmartFileCode.execute(appVars, agcmrvsIO);
	if (isNE(agcmrvsIO.getStatuz(),varcom.oK)
	&& isNE(agcmrvsIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(agcmrvsIO.getParams());
		syserrrec.statuz.set(agcmrvsIO.getStatuz());
		systemError8000();
	}
	if (isNE(agcmrvsIO.getChdrcoy(),reverserec.company)
	|| isNE(agcmrvsIO.getChdrnum(),reverserec.chdrnum)
	|| isNE(agcmrvsIO.getTranno(),reverserec.tranno)) {
		agcmrvsIO.setStatuz(varcom.endp);
	}
}
protected void a1050ReadNextLinp()
	{
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(reverserec.effdate1);
		taxdrevIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			b1050DeleteTaxdrev();
		}
		
		linprevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, linprevIO);
		if (isNE(linprevIO.getStatuz(), varcom.oK)
		&& isNE(linprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(linprevIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.company, linprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, linprevIO.getChdrnum())) {
			linprevIO.setStatuz(varcom.endp);
		}
	}

protected void b1050DeleteTaxdrev()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					b1050Para();
				case b1050Next: 
					b1050Next();
				case b1050Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b1050Para()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.b1050Exit);
		}
		wsaaTaxTranType.set(taxdrevIO.getTrantype());
		if (wsaaTaxTranValid.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.b1050Next);
		}
		if (wsaaTaxTranCollect.isTrue()
		&& isEQ(linprevIO.getInstfrom(), taxdrevIO.getInstfrom())) {
			taxdrevIO.setPostflg(SPACES);
			taxdrevIO.setFunction(varcom.writd);
			c1050Update();	//ILIFE-2161 starts by slakkala
		}
		if (wsaaTaxTranNvst.isTrue()) {
			if (isNE(taxdrevIO.getTxabsind01(), "Y")
			|| isNE(taxdrevIO.getTxabsind01(), "Y")) {
				b1060UpdateCovr();
				
			}
			taxdrevIO.setFunction(varcom.deltd);
/*ILIFE-2161 starts by slakala */
		c1050Update(); 
		}
	}
	/*	SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			dbError8100();
		}
	} */

protected void c1050Update()
{
	SmartFileCode.execute(appVars, taxdrevIO);
	if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(taxdrevIO.getParams());
		syserrrec.statuz.set(taxdrevIO.getStatuz());
		dbError8100();
	}
}

/*ILIFE--2161 ends */
protected void b1050Next()
	{
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void b1060UpdateCovr()
	{
		b1060Start();
	}

protected void b1060Start()
	{
		covrunlIO.setParams(SPACES);
		covrunlIO.setChdrcoy(taxdrevIO.getChdrcoy());
		covrunlIO.setChdrnum(taxdrevIO.getChdrnum());
		covrunlIO.setLife(taxdrevIO.getLife());
		covrunlIO.setCoverage(taxdrevIO.getCoverage());
		covrunlIO.setRider(taxdrevIO.getRider());
		covrunlIO.setPlanSuffix(taxdrevIO.getPlansfx());
		covrunlIO.setFunction(varcom.readh);
		covrunlIO.setFormat(formatsInner.covrunlrec);
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrunlIO.getParams());
			syserrrec.statuz.set(covrunlIO.getStatuz());
			dbError8100();
		}
		if (isNE(taxdrevIO.getTxabsind01(), "Y")) {
			setPrecision(covrunlIO.getCoverageDebt(), 2);
			covrunlIO.setCoverageDebt(sub(covrunlIO.getCoverageDebt(), taxdrevIO.getTaxamt01()));
		}
		if (isNE(taxdrevIO.getTxabsind02(), "Y")) {
			setPrecision(covrunlIO.getCoverageDebt(), 2);
			covrunlIO.setCoverageDebt(sub(covrunlIO.getCoverageDebt(), taxdrevIO.getTaxamt02()));
		}
		covrunlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrunlIO.getParams());
			syserrrec.statuz.set(covrunlIO.getStatuz());
			dbError8100();
		}
	}

	/**
	* <pre>
	******THIS SECTION IS NO LONGER PERFORMED*********************    
	*1060-CALL-DATCON2             SECTION.                           
	*1061-PARA.                                                       
	*****MOVE SPACES                 TO DTC2-STATUZ.                  
	*****MOVE LINPREV-INSTFROM       TO DTC2-INT-DATE-1.              
	*****MOVE -1                     TO DTC2-FREQ-FACTOR.             
	*****MOVE 'DY'                   TO DTC2-FREQUENCY.               
	*****IF DTC2-STATUZ              NOT = '****'                     
	*****    MOVE DTC2-STATUZ        TO SYSR-STATUZ                   
	*****MOVE DTC2-INT-DATE-2        TO CHDRLIF-PTDATE.               
	*1070-EXIT.                                                       
	*****EXIT.                                                        
	* </pre>
	*/
protected void lastInstdate1100()
	{
		start1110();
	}

protected void start1110()
	{
	linspfList = linspfDAO.getPaidLinspfRecordList(chdrlifIO.getChdrnum().toString());
	if(!linspfList.isEmpty()) {
	chdrlifIO.setInstfrom(linspfList.get(0).getInstfrom());
	}
	else {
	chdrlifIO.setInstfrom(0);
	}
		/*datcon2rec.statuz.set(SPACES);
		datcon2rec.intDate1.set(chdrlifIO.getPtdate());
		datcon2rec.freqFactor.set(-1);*/
		/* MOVE CHDRLIF-BILLFREQ       TO DTC2-FREQUENCY.               */
		/*datcon2rec.frequency.set(payrIO.getBillfreq());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, "****")) {
			syserrrec.statuz.set(datcon2rec.statuz);
			dbError8100();
		}
		if (isGT(datcon2rec.intDate2, chdrlifIO.getOccdate())) {
			chdrlifIO.setInstfrom(datcon2rec.intDate2);
		}
		else {
			chdrlifIO.setInstfrom(chdrlifIO.getOccdate());
		}*/
	}

protected void updateContractHeader1200()
	{
		/*PARA*/
		chdrlifIO.setInstto(chdrlifIO.getPtdate());
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void updatePayrFile1300()
	{
		/*REWRT*/
		/* Update the PAYR record with any changes that may have           */
		/* made.                                                           */
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void updateHdivFile1360()
	{
		rewrt1360();
	}

protected void rewrt1360()
	{
		hdivrevIO.setDataKey(SPACES);
		hdivrevIO.setChdrcoy(reverserec.company);
		hdivrevIO.setChdrnum(reverserec.chdrnum);
		hdivrevIO.setTranno(reverserec.tranno);
		hdivrevIO.setFormat(formatsInner.hdivrevrec);
		/* MOVE BEGNH                  TO HDIVREV-FUNCTION.     <LA3993>*/
		hdivrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hdivrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, hdivrevIO);
		if (isNE(hdivrevIO.getStatuz(), varcom.oK)
		&& isNE(hdivrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hdivrevIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.company, hdivrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, hdivrevIO.getChdrnum())
		|| isNE(reverserec.tranno, hdivrevIO.getTranno())) {
			hdivrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivrevIO.getStatuz(), varcom.endp))) {
			if (isEQ(hdivrevIO.getDivdStmtNo(), ZERO)) {
				/*       MOVE DELET            TO HDIVREV-FUNCTION      <LA3993>*/
				hdivrevIO.setFunction(varcom.deltd);
				SmartFileCode.execute(appVars, hdivrevIO);
				if (isNE(hdivrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(hdivrevIO.getParams());
					dbError8100();
				}
			}
			else {
				/* Otherwise write a mirror HDIV to counter off it               */
				hdivIO.setDataArea(SPACES);
				hdivIO.setChdrcoy(hdivrevIO.getChdrcoy());
				hdivIO.setChdrnum(hdivrevIO.getChdrnum());
				hdivIO.setLife(hdivrevIO.getLife());
				hdivIO.setJlife(hdivrevIO.getJlife());
				hdivIO.setCoverage(hdivrevIO.getCoverage());
				hdivIO.setRider(hdivrevIO.getRider());
				hdivIO.setPlanSuffix(hdivrevIO.getPlanSuffix());
				hdivIO.setPuAddNbr(hdivrevIO.getPuAddNbr());
				hdivIO.setTranno(reverserec.newTranno);
				hdivIO.setBatccoy(reverserec.batccoy);
				hdivIO.setBatcbrn(reverserec.batcbrn);
				hdivIO.setBatcactyr(reverserec.batcactyr);
				hdivIO.setBatcactmn(reverserec.batcactmn);
				hdivIO.setBatctrcde(reverserec.batctrcde);
				hdivIO.setBatcbatch(reverserec.batcbatch);
				hdivIO.setCntcurr(hdivrevIO.getCntcurr());
				hdivIO.setEffdate(hdivrevIO.getEffdate());
				hdivIO.setDivdAllocDate(reverserec.effdate1);
				hdivIO.setDivdType(hdivrevIO.getDivdType());
				setPrecision(hdivIO.getDivdAmount(), 2);
				hdivIO.setDivdAmount(mult(hdivrevIO.getDivdAmount(), -1));
				hdivIO.setDivdRate(hdivrevIO.getDivdRate());
				hdivIO.setDivdRtEffdt(hdivrevIO.getDivdRtEffdt());
				hdivIO.setZdivopt(hdivrevIO.getZdivopt());
				hdivIO.setZcshdivmth(hdivrevIO.getZcshdivmth());
				hdivIO.setDivdOptprocTranno(hdivrevIO.getDivdOptprocTranno());
				hdivIO.setDivdCapTranno(hdivrevIO.getDivdCapTranno());
				hdivIO.setDivdIntCapDate(hdivrevIO.getDivdIntCapDate());
				hdivIO.setDivdStmtNo(0);
				hdivIO.setFormat(formatsInner.hdivrec);
				hdivIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivIO);
				if (isNE(hdivIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(hdivIO.getParams());
					dbError8100();
				}
			}
			hdivrevIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivrevIO);
			if (isNE(hdivrevIO.getStatuz(), varcom.oK)
			&& isNE(hdivrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(hdivrevIO.getParams());
				dbError8100();
			}
			if (isNE(hdivrevIO.getChdrcoy(), reverserec.company)
			|| isNE(hdivrevIO.getChdrnum(), reverserec.chdrnum)
			|| isNE(hdivrevIO.getTranno(), reverserec.tranno)) {
				hdivrevIO.setStatuz(varcom.endp);
			}
		}
		
	}

protected void readReceiptFile1400()
	{
		para1400();
	}

protected void para1400()
	{
		/*PERFORM 1750-GET-TRANNO-DESC.                                */
		rtrnrevIO.setParams(SPACES);
		rtrnrevIO.setRldgcoy(reverserec.company);
		rtrnrevIO.setRdocnum(reverserec.chdrnum);
		rtrnrevIO.setTranno(reverserec.tranno);
		rtrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rtrnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, rtrnrevIO);
		/*IF RTRNREV-STATUZ           NOT = O-K AND ENDP               */
		if ((isNE(rtrnrevIO.getStatuz(), varcom.oK))
		&& (isNE(rtrnrevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(rtrnrevIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())
		|| isNE(reverserec.oldBatctrcde, rtrnrevIO.getBatctrcde())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(rtrnrevIO.getStatuz(), varcom.endp))) {
			overwriteReceipts1500();
			
			if (!isPaidtodate) {// ILIFE-7845
				riskPremflag = FeaConfg.isFeatureExist(reverserec.company.toString(), RISKPREM_FEATURE_ID, appVars,
						"IT");
				if (riskPremflag) {
					updateRiskPrem();
				}
			}
		}
		
	}

protected void overwriteReceipts1500()
	{
		para1501();
	}

protected void para1501()
	{
		/*     Write a new record to the database by calling subroutine*/
		/*     LIFRTRN*/
		/* DISPLAY ' REVE-TRANNO = ' REVE-TRANNO.*/
		/* DISPLAY ' RTRNREV-SACSCODE = ' RTRNREV-SACSCODE.*/
		/* DISPLAY ' RTRNREV-SACSTYP  = ' RTRNREV-SACSTYP.*/
		/* DISPLAY ' RTRNREV-BATCTRCDE = ' RTRNREV-BATCTRCDE.*/
		/* DISPLAY ' RTRNREV-ORIGAMT = ' RTRNREV-ORIGAMT.*/
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(reverserec.batchkey);
		lifrtrnrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifrtrnrec.rldgacct.set(chdrlifIO.getChdrnum());
		lifrtrnrec.tranno.set(reverserec.newTranno);
		lifrtrnrec.jrnseq.set(rtrnrevIO.getTranseq());
		lifrtrnrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifrtrnrec.sacscode.set(rtrnrevIO.getSacscode());
		lifrtrnrec.sacstyp.set(rtrnrevIO.getSacstyp());
		/*MOVE DESC-LONGDESC          TO LIFR-TRANDESC.                */
		lifrtrnrec.trandesc.set(wsaaTransDesc);
		lifrtrnrec.crate.set(rtrnrevIO.getCrate());
		compute(lifrtrnrec.acctamt, 2).set(mult(rtrnrevIO.getAcctamt(), -1));
		lifrtrnrec.transactionTime.set(rtrnrevIO.getTrandate());
		lifrtrnrec.transactionDate.set(rtrnrevIO.getTrantime());
		compute(lifrtrnrec.origamt, 2).set(mult(rtrnrevIO.getOrigamt(), -1));
		lifrtrnrec.genlcur.set(rtrnrevIO.getGenlcur());
		lifrtrnrec.origcurr.set(rtrnrevIO.getOrigccy());
		lifrtrnrec.genlcoy.set(rtrnrevIO.getGenlcoy());
		lifrtrnrec.glcode.set(rtrnrevIO.getGlcode());
		lifrtrnrec.glsign.set(rtrnrevIO.getGlsign());
		lifrtrnrec.postyear.set(rtrnrevIO.getPostyear());
		lifrtrnrec.postmonth.set(rtrnrevIO.getPostmonth());
		/* MOVE WSAA-TODAY             TO LIFR-EFFDATE.                 */
		lifrtrnrec.effdate.set(rtrnrevIO.getEffdate());
		wsaaRiskDate.set(rtrnrevIO.getEffdate());
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.rcamt.set(0);
		lifrtrnrec.contot.set(1);
		lifrtrnrec.substituteCode[1].set(chdrlifIO.getCnttype());
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			systemError8000();
		}
		rtrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, rtrnrevIO);
		/*IF RTRNREV-STATUZ           NOT = O-K AND ENDP               */
		if ((isNE(rtrnrevIO.getStatuz(), varcom.oK))
		&& (isNE(rtrnrevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(rtrnrevIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())
		|| isNE(reverserec.oldBatctrcde, rtrnrevIO.getBatctrcde())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
	}

	/**
	* <pre>
	*  Reverse the account movements from the ledger
	* </pre>
	*/
protected void reverseAcctMovements1600()
	{
		para1600();
	}

protected void para1600()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		wsaaT6661Exist.set(SPACES);
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setBatctrcde(SPACES);
		while ( !((isEQ(acmvrevIO.getStatuz(), varcom.endp))
		|| (isEQ(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde))
		|| (t6661Nfnd.isTrue()))) {
			SmartFileCode.execute(appVars, acmvrevIO);
			if ((isNE(acmvrevIO.getStatuz(), varcom.oK))
			&& (isNE(acmvrevIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(acmvrevIO.getParams());
				dbError8100();
			}
			if ((isNE(acmvrevIO.getRldgcoy(), reverserec.company))
			|| (isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum))
			|| (isNE(acmvrevIO.getTranno(), reverserec.tranno))) {
				acmvrevIO.setStatuz(varcom.endp);
			}
			readT66618200();
			acmvrevIO.setFunction(varcom.nextr);
		}
		
		/*CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		/*IF ACMVREV-STATUZ           NOT = O-K AND ENDP               */
		/*MOVE ACMVREV-PARAMS   TO SYSR-PARAMS                   */
		/*IF REVE-COMPANY             NOT = ACMVREV-RLDGCOY OR         */
		/*REVE-TRANNO              NOT = ACMVREV-TRANNO OR          */
		/*REVE-CHDRNUM             NOT  = ACMVREV-RDOCNUM      <012>*/
		/*REVE-CHDRNUM             NOT  = ACMVREV-RDOCNUM OR   <012>*/
		/*REVE-BATCTRCDE           NOT  = ACMVREV-BATCTRCDE 012<010>*/
		/*REVE-OLD-BATCTRCDE       NOT  = ACMVREV-BATCTRCDE    <012>*/
		/*MOVE ENDP                TO ACMVREV-STATUZ.               */
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			readAcmvs1700();
		}
		
	}

protected void processAcmvOptical1610()
	{
		start1610();
	}

protected void start1610()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			dbError8100();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			readAcmvs1700();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				dbError8100();
			}
		}
	}

protected void readAcmvs1700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go1700();
				case nextAcmv1750: 
					nextAcmv1750();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go1700()
	{
		/* DISPLAY ' ACMVREV-TRANNO = ' ACMVREV-TRANNO.*/
		/* DISPLAY ' ACMVREV-SACSCODE = ' ACMVREV-SACSCODE.*/
		/* DISPLAY ' ACMVREV-SACSTYP  = ' ACMVREV-SACSTYP.*/
		/* DISPLAY ' ACMVREV-BATCTRCDE = ' ACMVREV-BATCTRCDE.*/
		/* DISPLAY ' ACMVREV-ORIGAMT = ' ACMVREV-ORIGAMT.*/
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.batcactmn.set(reverserec.batcactmn);
		lifacmvrec.batcactyr.set(reverserec.batcactyr);
		lifacmvrec.tranno.set(reverserec.newTranno);
		/*    MOVE REVE-NEW-TRANNO            TO LIFA-TRANREF.*/
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		/*MOVE DESC-LONGDESC              TO LIFA-TRANDESC.            */
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE WSAA-TODAY                 TO LIFA-EFFDATE.             */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		lifacmvrec.termid.set(acmvrevIO.getTermid());
		lifacmvrec.user.set(acmvrevIO.getUser());
		lifacmvrec.transactionTime.set(acmvrevIO.getTransactionTime());
		lifacmvrec.transactionDate.set(acmvrevIO.getTransactionDate());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		/*    MOVE REVE-NEW-TRANNO            TO LIFA-TRANREF.*/
		/*    MOVE ACMVREV-TRANDESC           TO LIFA-TRANDESC.*/
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		/*    MOVE ACMVREV-POSTYEAR           TO LIFA-POSTYEAR.*/
		/*    MOVE ACMVREV-POSTMONTH          TO LIFA-POSTMONTH.*/
		wsaaPostyear.set(reverserec.batcactyr);
		wsaaPostmonth.set(reverserec.batcactmn);
		lifacmvrec.postyear.set(wsaaPostyearX);
		lifacmvrec.postmonth.set(wsaaPostmonthX);
		/* MOVE WSAA-TODAY                 TO LIFA-EFFDATE.             */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* COMPUTE LIFA-RCAMT = ACMVREV-RCAMT * -1.                     */
		/*    MOVE ACMVREV-RCAMT              TO LIFA-RCAMT.*/
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		/* MOVE ACMVREV-FRCDATE            TO LIFA-FRCDATE.             */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError8000();
		}
	}

	/**
	* <pre>
	**check if there is any agent commission                          
	*****IF ((ACMVREV-SACSCODE           = T5645-SACSCODE(1))         
	******  AND (ACMVREV-SACSTYP         = T5645-SACSTYPE(1)))        
	******  MOVE 1 TO WSAA-COMMISSION-TYPE                            
	******  PERFORM 2000-UPDATE-AGENT-COMMISSION                      
	*****ELSE                                                         
	*****IF ((ACMVREV-SACSCODE           = T5645-SACSCODE(2))         
	******  AND (ACMVREV-SACSTYP         = T5645-SACSTYPE(2)))        
	******  MOVE 2 TO WSAA-COMMISSION-TYPE                            
	******  PERFORM 2000-UPDATE-AGENT-COMMISSION                      
	*****ELSE                                                         
	*****IF ((ACMVREV-SACSCODE           = T5645-SACSCODE(3))         
	******  AND (ACMVREV-SACSTYP         = T5645-SACSTYPE(3)))        
	******  MOVE 3 TO WSAA-COMMISSION-TYPE                            
	******  PERFORM 2000-UPDATE-AGENT-COMMISSION                      
	*****ELSE                                                    <008>
	*****IF ((ACMVREV-SACSCODE           = T5645-SACSCODE(4))    <008>
	******  AND (ACMVREV-SACSTYP         = T5645-SACSTYPE(4)))   <008>
	******  MOVE 4 TO WSAA-COMMISSION-TYPE                       <008>
	******  PERFORM 2000-UPDATE-AGENT-COMMISSION.                <008>
	* </pre>
	*/
protected void nextAcmv1750()
	{
		/*  read the next acmv record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'                USING ACMVREV-PARAMS.        */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			systemError8000();
		}
		if (isNE(acmvrevIO.getRldgcoy(), reverserec.company)
		|| isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum)
		|| isNE(acmvrevIO.getTranno(), reverserec.tranno)
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		readT66618200();
		if (t6661Nfnd.isTrue()) {
			return ;
		}
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			goTo(GotoLabel.nextAcmv1750);
		}
	}

	/**
	* <pre>
	*1750-GET-TRANNO-DESC   SECTION.                                  
	*1751-GO.                                                         
	*****MOVE 'IT'                   TO DESC-DESCPFX.                 
	*****MOVE T1688                  TO DESC-DESCTABL.                
	*****MOVE REVE-TRANNO            TO DESC-DESCITEM.                
	*****MOVE REVE-COMPANY           TO DESC-DESCCOY.                 
	*****MOVE 'E'                    TO DESC-LANGUAGE.                
	*****MOVE 'READR'                TO DESC-FUNCTION.                
	*****CALL 'DESCIO' USING DESC-PARAMS.                             
	*****IF DESC-STATUZ              NOT = '****'                     
	*****                            AND NOT = 'MRNF'                 
	*********MOVE DESC-PARAMS        TO SYSR-PARAMS                   
	*********PERFORM 8000-SYSTEM-ERROR.                               
	*1790-EXIT.                                                       
	*****EXIT.                                                        
	* </pre>
	*/
protected void updateAgentCommission2000()
	{
		go2001();
	}

protected void go2001()
	{
		agcmrevIO.setParams(SPACES);
		agcmrevIO.setChdrnum(reverserec.chdrnum);
		agcmrevIO.setChdrcoy(reverserec.company);
		/* MOVE REVE-LIFE               TO AGCMREV-LIFE.*/
		/* MOVE REVE-COVERAGE           TO AGCMREV-COVERAGE.*/
		/* MOVE REVE-RIDER              TO AGCMREV-RIDER.*/
		/* MOVE REVE-PLAN-SUFFIX        TO AGCMREV-PLAN-SUFFIX.*/
		agcmrevIO.setPlanSuffix(ZERO);
		/* MOVE BEGNH                   TO AGCMREV-FUNCTION.    <LA3993>*/
		/*MOVE BEGN                    TO AGCMREV-FUNCTION.            */
		agcmrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, agcmrevIO);
		/*IF AGCMREV-STATUZ            NOT = ENDP AND O-K              */
		if (isNE(agcmrevIO.getStatuz(), varcom.endp)
		&& isNE(agcmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrevIO.getParams());
			systemError8000();
		}
		if (isNE(reverserec.chdrnum, agcmrevIO.getChdrnum())
		|| isNE(reverserec.company, agcmrevIO.getChdrcoy())) {
			/*    REVE-LIFE                  NOT = AGCMREV-LIFE OR*/
			/*    REVE-COVERAGE              NOT = AGCMREV-COVERAGE OR*/
			/*    REVE-RIDER                 NOT = AGCMREV-RIDER OR*/
			/*    REVE-PLAN-SUFFIX           NOT = AGCMREV-PLAN-SUFFIX*/
			agcmrevIO.setStatuz(varcom.endp);
		}
		/* MOVE 'N'                      TO WSAA-NEXT-AGCM-FLAG.   <014>*/
		while ( !(isEQ(agcmrevIO.getStatuz(), varcom.endp))) {
			callCommissionSub2100();
		}
		
	}

protected void callCommissionSub2100()
	{
		go2101();
		nextAgcm2120();
	}

protected void go2101()
	{
		wsaaNextAgcmFlag.set("Y");
		/* When a dormant AGCM record is encountered, do not process    */
		/* the record but rewrite it to release the record lock and go  */
		/* on to the next record.                                       */
		if (isEQ(agcmrevIO.getDormantFlag(), "Y")) {
			/*     MOVE REWRT              TO AGCMREV-FUNCTION      <LA3993>*/
			/*     MOVE AGCMREVREC         TO AGCMREV-FORMAT        <LA3993>*/
			/*     CALL 'AGCMREVIO'        USING AGCMREV-PARAMS     <LA3993>*/
			/*     IF AGCMREV-STATUZ    NOT = O-K                   <LA3993>*/
			/*         MOVE AGCMREV-STATUZ TO SYSR-STATUZ           <LA3993>*/
			/*         MOVE AGCMREV-PARAMS TO SYSR-PARAMS           <LA3993>*/
			/*         PERFORM 8000-SYSTEM-ERROR                    <LA3993>*/
			/*     END-IF                                           <LA3993>*/
			return ;
		}
		if (isLTE(agcmrevIO.getEfdate(), reverserec.effdate1)
		&& isGT(agcmrevIO.getPtdate(), reverserec.effdate1)) {
			for (wsaaCommType.set(1); !(isEQ(wsaaCommType, 4)); wsaaCommType.add(1)){
				callSubroutine2200();
			}
			if (!getNextAgcm.isTrue()) {
				rewriteAgcm2400();
			}
		}
	}

	/**
	* <pre>
	*****   IF GET-NEXT-AGCM                                          
	*****      MOVE 'N'          TO WSAA-NEXT-AGCM-FLAG               
	*****      GO TO 2120-NEXT-AGCM                                   
	*****   END-IF                                                    
	* </pre>
	*/
protected void nextAgcm2120()
	{
		agcmrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmrevIO);
		/*IF AGCMREV-STATUZ            NOT = ENDP AND O-K              */
		if (isNE(agcmrevIO.getStatuz(), varcom.endp)
		&& isNE(agcmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrevIO.getParams());
			systemError8000();
		}
		if (isNE(reverserec.chdrnum, agcmrevIO.getChdrnum())
		|| isNE(reverserec.company, agcmrevIO.getChdrcoy())) {
			/*    REVE-LIFE                  NOT = AGCMREV-LIFE OR*/
			/*    REVE-COVERAGE              NOT = AGCMREV-COVERAGE OR*/
			/*    REVE-RIDER                 NOT = AGCMREV-RIDER OR*/
			/*    REVE-PLAN-SUFFIX           NOT = AGCMREV-PLAN-SUFFIX*/
			agcmrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void callSubroutine2200()
	{
		go2201();
	}

protected void go2201()
	{
		/*                                                        <014>*/
		/*If*the Commission Type is Renewal or Servicing and the     <014>*/
		/*relevant payment pattern is blank - go to exit.            <014>*/
		/*                                                        <014>*/
		/*IF RENEWAL-COMMISSION                                   <014>*/
		/*   IF (AGCMREV-RNWCPY       = SPACES)                   <014>*/
		/*       MOVE 'Y'             TO WSAA-NEXT-AGCM-FLAG      <014>*/
		/*       GO TO 2290-EXIT                                  <014>*/
		/*   END-IF                                               <014>*/
		/*END-IF.                                                 <014>*/
		/*                                                        <014>*/
		/*IF SERVICING-COMMISSION                                 <014>*/
		/*   IF (AGCMREV-SRVCPY       = SPACES)                   <014>*/
		/*       MOVE 'Y'             TO WSAA-NEXT-AGCM-FLAG      <014>*/
		/*       GO TO 2290-EXIT                                  <014>*/
		/*   END-IF                                               <014>*/
		/*END-IF.                                                 <014>*/
		/* IF INITIAL-COMMISSION OR                                     */
		/*    OVERRIDE-COMMISSION                                  <008>*/
		/*     MOVE AGCMREV-BASCPY     TO ITEM-ITEMITEM                 */
		/* ELSE                                                         */
		/* IF RENEWAL-COMMISSION                                        */
		/*     MOVE AGCMREV-RNWCPY     TO ITEM-ITEMITEM                 */
		/* ELSE                                                         */
		/*     MOVE AGCMREV-SRVCPY     TO ITEM-ITEMITEM.                */
		wsaaPayamnt[wsaaCommType.toInt()].set(ZERO);
		wsaaErndamt[wsaaCommType.toInt()].set(ZERO);
		if (isEQ(wsaaCommType, 1)) {
			if (isEQ(agcmrevIO.getBascpy(), SPACES)) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmrevIO.getBascpy());
			}
		}
		if (isEQ(wsaaCommType, 2)) {
			if (isEQ(agcmrevIO.getRnwcpy(), SPACES)) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmrevIO.getRnwcpy());
			}
		}
		if (isEQ(wsaaCommType, 3)) {
			if (isEQ(agcmrevIO.getSrvcpy(), SPACES)) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmrevIO.getSrvcpy());
			}
		}
		comlinkrec.method.set(itemIO.getItemitem());
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5644);
		itemIO.setItempfx("IT");
		/* MOVE WSAA-SUBR              TO ITEM-ITEMITEM.*/
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), "****")) {
			syserrrec.params.set(itemIO.getParams());
			systemError8000();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		/* set up linkage for the commission reversal subroutine*/
		comlinkrec.function.set(SPACES);
		comlinkrec.statuz.set(SPACES);
		comlinkrec.ptdate.set(0);
		comlinkrec.agent.set(agcmrevIO.getAgntnum());
		comlinkrec.chdrcoy.set(reverserec.company);
		comlinkrec.chdrnum.set(reverserec.chdrnum);
		comlinkrec.language.set(reverserec.language);
		comlinkrec.life.set(agcmrevIO.getLife());
		comlinkrec.coverage.set(agcmrevIO.getCoverage());
		comlinkrec.rider.set(agcmrevIO.getRider());
		comlinkrec.planSuffix.set(agcmrevIO.getPlanSuffix());
		comlinkrec.seqno.set(agcmrevIO.getSeqno());
		getCrtable2300();
		comlinkrec.crtable.set(covrrevIO.getCrtable());
		comlinkrec.jlife.set(covrrevIO.getJlife());
		comlinkrec.agentClass.set(agcmrevIO.getAgentClass());
		/*    MOVE CHDRLIF-OCCDATE        TO CLNK-OCCDATE.*/
		/*    MOVE AGCMREV-EFDATE         TO CLNK-OCCDATE.*/
		comlinkrec.effdate.set(agcmrevIO.getEfdate());
		/* MOVE CHDRLIF-BILLFREQ       TO CLNK-BILLFREQ.                */
		/* MOVE CHDRLIF-BILLFREQ       TO WSAA-BILLFREQ.                */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			comlinkrec.billfreq.set("00");
			wsaaBillfreq.set("01");
		}
		else {
			comlinkrec.billfreq.set(payrIO.getBillfreq());
			wsaaBillfreq.set(payrIO.getBillfreq());
		}
		comlinkrec.annprem.set(agcmrevIO.getAnnprem());
		compute(comlinkrec.instprem, 2).set(div(agcmrevIO.getAnnprem(), wsaaBillfreq9));
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		/* IF INITIAL-COMMISSION  OR                                    */
		/*    OVERRIDE-COMMISSION                                  <008>*/
		if (isEQ(wsaaCommType, 1)) {
			comlinkrec.icommtot.set(agcmrevIO.getInitcom());
			comlinkrec.icommpd.set(agcmrevIO.getCompay());
			comlinkrec.icommernd.set(agcmrevIO.getComern());
		}
		/* DISPLAY 'CLNK-ANNPREM =' CLNK-ANNPREM.*/
		/* DISPLAY 'CLNK-INSTPREM =' CLNK-INSTPREM.*/
		/* DISPLAY 'T5644-SUBREV =' T5644-SUBREV.*/
		if (isNE(t5644rec.subrev, SPACES)) {
			callProgram(t5644rec.subrev, comlinkrec.clnkallRec);
		}
		else {
			return ;
		}
		if (isNE(comlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			systemError8000();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		callRounding9000();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		callRounding9000();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaPayamnt[wsaaCommType.toInt()].set(comlinkrec.payamnt);
		wsaaErndamt[wsaaCommType.toInt()].set(comlinkrec.erndamt);
		wsaaNextAgcmFlag.set("N");
	}

	/**
	* <pre>
	*   Read COVR file in order to get  the CRTABLE
	* </pre>
	*/
protected void getCrtable2300()
	{
		go2301();
		readT56872320();
	}

protected void go2301()
	{
		covrrevIO.setParams(SPACES);
		covrrevIO.setChdrcoy(agcmrevIO.getChdrcoy());
		covrrevIO.setChdrnum(agcmrevIO.getChdrnum());
		covrrevIO.setLife(agcmrevIO.getLife());
		covrrevIO.setCoverage(agcmrevIO.getCoverage());
		covrrevIO.setRider(agcmrevIO.getRider());
		covrrevIO.setPlanSuffix(agcmrevIO.getPlanSuffix());
		covrrevIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrrevIO);
		if (isNE(covrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrevIO.getParams());
			dbError8100();
		}
	}

protected void readT56872320()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrrevIO.getCrtable());
		itdmIO.setItmfrm(covrrevIO.getCrrcd());
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			systemError8000();
		}
		if (isNE(itdmIO.getItemcoy(), reverserec.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrrevIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			systemError8000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void rewriteAgcm2400()
	{
		go2401();
	}

protected void go2401()
	{
		/*IF  CLNK-PAYAMNT = +0                                        */
		/*        AND                                                  */
		/*    CLNK-ERNDAMT = +0                                        */
		/*    GO TO 2490-EXIT.                                         */
		/*   The READH has been commented out as when records have         */
		/*   the same key (in the case of override commission) the         */
		/*   the record being read for update was not always the           */
		/*   correct one. The BEGNH coded previously instead of            */
		/*   BEGN has now held the correct record for update.              */
		/*MOVE READH                  TO AGCMREV-FUNCTION.             */
		/*MOVE AGCMREVREC             TO AGCMREV-FORMAT.               */
		/*CALL 'AGCMREVIO'            USING AGCMREV-PARAMS.            */
		/*IF AGCMREV-STATUZ           NOT = O-K                        */
		/*    MOVE AGCMREV-PARAMS     TO SYSR-PARAMS                   */
		/*    PERFORM 8000-SYSTEM-ERROR.                               */
		/* DISPLAY 'AGCM-KEY = ' AGCMREV-DATA-KEY.*/
		/* DISPLAY 'AGCM-PLAN-SUFFIX = ' AGCMREV-PLAN-SUFFIX.*/
		/* DISPLAY 'BEFORE'.*/
		/* IF INITIAL-COMMISSION OR                                     */
		/*    OVERRIDE-COMMISSION                                   <008*/
		/*    COMPUTE AGCMREV-COMPAY = AGCMREV-COMPAY + CLNK-PAYAMNT    */
		/*    COMPUTE AGCMREV-COMERN = AGCMREV-COMERN + CLNK-ERNDAMT    */
		/* ELSE                                                         */
		/* IF RENEWAL-COMMISSION                                        */
		/*    COMPUTE AGCMREV-RNLCDUE  = AGCMREV-RNLCDUE - CLNK-PAYAMNT */
		/*    COMPUTE AGCMREV-RNLCEARN = AGCMREV-RNLCEARN               */
		/*                               - CLNK-ERNDAMT                 */
		/* ELSE                                                         */
		/*    COMPUTE AGCMREV-SCMDUE  = AGCMREV-SCMDUE - CLNK-PAYAMNT   */
		/*    COMPUTE AGCMREV-SCMEARN = AGCMREV-SCMEARN                 */
		/*                               - CLNK-ERNDAMT.                */
		if (isNE(agcmrevIO.getBascpy(), SPACES)) {
			/*    MOVE  WSAA-PAYAMNT (1)     TO  AGCMREV-COMPAY     <LA2108>*/
			/*    MOVE  WSAA-ERNDAMT (1)     TO  AGCMREV-COMERN     <LA2108>*/
			setPrecision(agcmrevIO.getCompay(), 2);
			agcmrevIO.setCompay(sub(agcmrevIO.getCompay(), wsaaPayamnt[1]));
			setPrecision(agcmrevIO.getComern(), 2);
			agcmrevIO.setComern(sub(agcmrevIO.getComern(), wsaaErndamt[1]));
		}
		if (isNE(agcmrevIO.getRnwcpy(), SPACES)) {
			/*     MOVE  WSAA-PAYAMNT (2)     TO  AGCMREV-RNLCDUE    <LA2108>*/
			/*     MOVE  WSAA-ERNDAMT (2)     TO  AGCMREV-RNLCEARN   <LA2108>*/
			setPrecision(agcmrevIO.getRnlcdue(), 2);
			agcmrevIO.setRnlcdue(sub(agcmrevIO.getRnlcdue(), wsaaPayamnt[2]));
			setPrecision(agcmrevIO.getRnlcearn(), 2);
			agcmrevIO.setRnlcearn(sub(agcmrevIO.getRnlcearn(), wsaaErndamt[2]));
		}
		if (isNE(agcmrevIO.getSrvcpy(), SPACES)) {
			/*     MOVE  WSAA-PAYAMNT (3)     TO  AGCMREV-SCMDUE     <LA2108>*/
			/*     MOVE  WSAA-ERNDAMT (3)     TO  AGCMREV-SCMEARN    <LA2108>*/
			setPrecision(agcmrevIO.getScmdue(), 2);
			agcmrevIO.setScmdue(sub(agcmrevIO.getScmdue(), wsaaPayamnt[3]));
			setPrecision(agcmrevIO.getScmearn(), 2);
			agcmrevIO.setScmearn(sub(agcmrevIO.getScmearn(), wsaaErndamt[3]));
		}
		agcmrevIO.setPtdate(chdrlifIO.getPtdate());
		//IJS-315 START
		if(isEQ(agcmrevIO.ptdate,agcmrevIO.efdate)){
			agcmrevIO.setCompay(ZERO);
			agcmrevIO.setComern(ZERO);
			agcmrevIO.setScmdue(ZERO);
			agcmrevIO.setScmearn(ZERO);
		}
		//IJS-315 END
		/* MOVE '1'                    TO AGCMREV-VALIDFLAG.*/
		/* MOVE REWRT                  TO AGCMREV-FUNCTION.             */
		agcmrevIO.setFunction(varcom.writd);
		agcmrevIO.setFormat(formatsInner.agcmrevrec);
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrevIO.getParams());
			systemError8000();
		}
	}

protected void reverseUnits3000()
	{
		start3000();
	}

protected void start3000()
	{
		/*CALL 'REVUNIT'            USING REVE-REVERSE-REC.            */
		/*IF REVE-STATUZ   NOT = O-K                                   */
		/*   MOVE REVE-STATUZ            TO SYSR-SYSERR-STATUZ         */
		/*    MOVE REVE-REVERSE-REC   TO SYSR-PARAMS                   */
		/*    PERFORM 8000-SYSTEM-ERROR.                               */
		/* MOVE SPACES                 TO COVRREV-PARAMS.       <CAS1.0>*/
		/* MOVE REVE-COMPANY           TO COVRREV-CHDRCOY.      <CAS1.0>*/
		/* MOVE REVE-CHDRNUM           TO COVRREV-CHDRNUM.      <CAS1.0>*/
		/* MOVE '01'                   TO COVRREV-LIFE.         <CAS1.0>*/
		/* MOVE '01'                   TO COVRREV-COVERAGE.     <CAS1.0>*/
		/* MOVE '00'                   TO COVRREV-RIDER.        <CAS1.0>*/
		/* MOVE ZEROES                 TO COVRREV-PLAN-SUFFIX.  <CAS1.0>*/
		/* MOVE BEGN                   TO COVRREV-FUNCTION.     <CAS1.0>*/
		/* CALL 'COVRREVIO'            USING COVRREV-PARAMS.    <CAS1.0>*/
		/* IF COVRREV-STATUZ           NOT = O-K                <CAS1.0>*/
		/*       MOVE COVRREV-PARAMS   TO SYSR-PARAMS           <CAS1.0>*/
		/*       GO TO 3000-EXIT.                               <CAS1.0>*/
		SmartFileCode.execute(appVars, covrenqIO);
		if ((isNE(covrenqIO.getStatuz(), varcom.oK))
		&& (isNE(covrenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrenqIO.getParams());
			dbError8100();
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Batc.set(reverserec.oldBatctrcde);
		/* MOVE COVRREV-CRTABLE        TO WSAA-T5671-CRTABLE.           */
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError8000();
		}
		/* IF ITEM-STATUZ            = SPACES                   <CAS1.0>*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		greversrec.chdrcoy.set(reverserec.company);
		greversrec.chdrnum.set(reverserec.chdrnum);
		greversrec.tranno.set(reverserec.tranno);
		/* MOVE REVE-PLAN-SUFFIX       TO GREV-PLAN-SUFFIX.     <CAS1.0>*/
		greversrec.newTranno.set(reverserec.newTranno);
		/* MOVE REVE-COVERAGE          TO GREV-COVERAGE.        <CAS1.0>*/
		/* MOVE REVE-RIDER             TO GREV-RIDER.           <CAS1.0>*/
		greversrec.life.set(covrenqIO.getLife());
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.effdate.set(reverserec.ptrneff);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callSubprog3100();
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void callSubprog3100()
	{
		/*CALL*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(greversrec.statuz);
				systemError8000();
			}
		}
		/*EXIT*/
	}

protected void processZptn4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc4010();
				case call4020: 
					call4020();
					writ4030();
					nextr4080();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc4010()
	{
		zptnrevIO.setDataArea(SPACES);
		zptnrevIO.setChdrcoy(reverserec.company);
		zptnrevIO.setChdrnum(reverserec.chdrnum);
		zptnrevIO.setTranno(reverserec.tranno);
		zptnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zptnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zptnrevIO.setFormat(formatsInner.zptnrevrec);
	}

protected void call4020()
	{
		SmartFileCode.execute(appVars, zptnrevIO);
		if (isNE(zptnrevIO.getStatuz(), varcom.oK)
		&& isNE(zptnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zptnrevIO.getStatuz());
			syserrrec.params.set(zptnrevIO.getParams());
			dbError8100();
		}
		if (isEQ(zptnrevIO.getStatuz(), varcom.endp)
		|| isNE(zptnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zptnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zptnrevIO.getTranno(), reverserec.tranno)) {
			zptnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit4090);
		}
	}

protected void writ4030()
	{
		zptnIO.setDataArea(SPACES);
		zptnIO.setChdrcoy(zptnrevIO.getChdrcoy());
		zptnIO.setChdrnum(zptnrevIO.getChdrnum());
		zptnIO.setLife(zptnrevIO.getLife());
		zptnIO.setCoverage(zptnrevIO.getCoverage());
		zptnIO.setRider(zptnrevIO.getRider());
		zptnIO.setTranno(reverserec.newTranno);
		setPrecision(zptnIO.getOrigamt(), 2);
		zptnIO.setOrigamt(mult(zptnrevIO.getOrigamt(), -1));
		zptnIO.setTransCode(reverserec.batctrcde);
		zptnIO.setEffdate(zptnrevIO.getEffdate());
		zptnIO.setBillcd(zptnrevIO.getBillcd());
		zptnIO.setInstfrom(zptnrevIO.getInstfrom());
		zptnIO.setInstto(zptnrevIO.getInstto());
		zptnIO.setTrandate(wsaaToday);
		zptnIO.setZprflg(zptnrevIO.getZprflg());
		zptnIO.setFormat(formatsInner.zptnrec);
		zptnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zptnIO.getStatuz());
			syserrrec.params.set(zptnIO.getParams());
			dbError8100();
		}
	}

protected void nextr4080()
	{
		zptnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call4020);
	}

protected void processZctn5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc5010();
				case call5020: 
					call5020();
					writ5030();
					nextr5080();
				case exit5090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc5010()
	{
		zctnrevIO.setDataArea(SPACES);
		zctnrevIO.setChdrcoy(reverserec.company);
		zctnrevIO.setChdrnum(reverserec.chdrnum);
		zctnrevIO.setTranno(reverserec.tranno);
		zctnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zctnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zctnrevIO.setFormat(formatsInner.zctnrevrec);
	}

protected void call5020()
	{
		SmartFileCode.execute(appVars, zctnrevIO);
		if (isNE(zctnrevIO.getStatuz(), varcom.oK)
		&& isNE(zctnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zctnrevIO.getStatuz());
			syserrrec.params.set(zctnrevIO.getParams());
			dbError8100();
		}
		if (isEQ(zctnrevIO.getStatuz(), varcom.endp)
		|| isNE(zctnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zctnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zctnrevIO.getTranno(), reverserec.tranno)) {
			zctnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5090);
		}
	}

protected void writ5030()
	{
		zctnIO.setDataArea(SPACES);
		zctnIO.setAgntcoy(zctnrevIO.getAgntcoy());
		zctnIO.setAgntnum(zctnrevIO.getAgntnum());
		zctnIO.setChdrcoy(zctnrevIO.getChdrcoy());
		zctnIO.setChdrnum(zctnrevIO.getChdrnum());
		zctnIO.setLife(zctnrevIO.getLife());
		zctnIO.setCoverage(zctnrevIO.getCoverage());
		zctnIO.setRider(zctnrevIO.getRider());
		zctnIO.setTranno(reverserec.newTranno);
		setPrecision(zctnIO.getCommAmt(), 2);
		zctnIO.setCommAmt(mult(zctnrevIO.getCommAmt(), -1));
		setPrecision(zctnIO.getPremium(), 2);
		zctnIO.setPremium(mult(zctnrevIO.getPremium(), -1));
		zctnIO.setSplitBcomm(zctnrevIO.getSplitBcomm());
		zctnIO.setTransCode(reverserec.batctrcde);
		zctnIO.setEffdate(zctnrevIO.getEffdate());
		zctnIO.setTrandate(wsaaToday);
		zctnIO.setZprflg(zctnrevIO.getZprflg());
		zctnIO.setFormat(formatsInner.zctnrec);
		zctnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zctnIO);
		if (isNE(zctnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zctnIO.getStatuz());
			syserrrec.params.set(zctnIO.getParams());
			dbError8100();
		}
	}

protected void nextr5080()
	{
		zctnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call5020);
	}

protected void systemError8000()
	{
		se8000();
		seExit8090();
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		db8100();
		dbExit8190();
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}

protected void readT66618200()
	{
		read8201();
	}

protected void read8201()
	{
		itemIO.setParams(SPACES);
		wsaaT6661Exist.set(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(acmvrevIO.getRldgcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(acmvrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT6661Exist.set("N");
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				wsaaT6661Exist.set("Y");
			}
		}
	}

protected void callRounding9000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(reverserec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(acmvrevIO.getOrigcurr());
		zrdecplrec.batctrcde.set(reverserec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError8000();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covrunlrec = new FixedLengthStringData(10).init("COVRUNLREC");
	private FixedLengthStringData linprevrec = new FixedLengthStringData(10).init("LINPREVREC");
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData agcmrevrec = new FixedLengthStringData(10).init("AGCMREVREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
	private FixedLengthStringData hdivrevrec = new FixedLengthStringData(10).init("HDIVREVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData zptnrevrec = new FixedLengthStringData(10).init("ZPTNREVREC");
	private FixedLengthStringData zctnrevrec = new FixedLengthStringData(10).init("ZCTNREVREC");
	private FixedLengthStringData agcmrvsrec = new FixedLengthStringData(10).init("AGCMRVSREC");
	private FixedLengthStringData agcmdbcrec = new FixedLengthStringData(10).init("AGCMREC");
}
}
