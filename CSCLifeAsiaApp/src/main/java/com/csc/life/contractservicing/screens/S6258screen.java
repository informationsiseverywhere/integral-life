package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6258screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6258ScreenVars sv = (S6258ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6258screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6258ScreenVars screenVars = (S6258ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.cntinst.setClassString("");
		screenVars.sacscurbal.setClassString("");
		screenVars.payToDateAdvanceDisp.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.cpiDateDisp.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.prmdepst.setClassString("");
		screenVars.taxamt.setClassString("");
	}

/**
 * Clear all the variables in S6258screen
 */
	public static void clear(VarModel pv) {
		S6258ScreenVars screenVars = (S6258ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.payrnum.clear();
		screenVars.payorname.clear();
		screenVars.agntnum.clear();
		screenVars.agentname.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.cntcurr.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.billfreq.clear();
		screenVars.cntinst.clear();
		screenVars.sacscurbal.clear();
		screenVars.payToDateAdvanceDisp.clear();
		screenVars.payToDateAdvance.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.cpiDateDisp.clear();
		screenVars.cpiDate.clear();
		screenVars.billcurr.clear();
		screenVars.prmdepst.clear();
		screenVars.taxamt.clear();
	}
}
