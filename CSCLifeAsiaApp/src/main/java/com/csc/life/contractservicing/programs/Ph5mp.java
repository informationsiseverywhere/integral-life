/*
 * File: Ph5mp.java
 * Date: 30 August 2009 0:39:54
 * Author: Quipoz Limited
 * 
 * Class transformed from Ph5mp.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Billreq1;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Billreqrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CoprpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Coprpf;
import com.csc.life.contractservicing.screens.Sh5mpScreenVars;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Pay to Date Advance main-line front-end to the renewal type
* processing of the pay to date advance AT module.
*
* This program displays relevent information about the contract
* and accepts the pay-to-date to which the contract should be
* advanced to.
*
* Validation :
*
* ! The pay-to-date Advance must be entered.
*
* ! The date is greater than the bill to date and equally
*   the pay-to-date on the contract (after 'windback').
*
* ! The number of frequencies (ie number of months if
*   the billing frequency is 12, etc) must be a whole number.
*   This can be calculated as  the absolute difference between
*   the  bill to date and the date  entered using  the DATCON3
*   subroutine and checking that it is an integer.
*
* If there is  a paid to date advance  performed on a contract
* there  is  the possibility that the contract may  be due for
* increase at a  point  during this  process.  As the  correct
* percentage increase may not be available at this time  it is
* proposed that the following occur when PTD Advance is taking
* place:-
*
* - The  next increase   date  will  be  displayed  on the PTD
*   Advance screen.
*
* - If  the  date  chosen to  advance to  is past the increase
*   date the transaction can not be performed.
*
* In order  for  increases  to  be offered to a contract where
* the PTD Advance  advances past an Increase date  some manual
* process will need to be introduced.
*
* a) Advance PTD to Increase Date
*
* b) Refuse Increase (new online transaction)
*
*    Run batch job to process increase
*
* c) Perform PTD Advance transaction.
*
* After successful entry of the date the softlocked contract
* should be passed to the AT module using a softlock
* function of TOAT, before the AT module is called.
 *
 *****************************************************************
 * </pre>
 */
public class Ph5mp extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH5MP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaNetPrem = new PackedDecimalData(17, 2);
	/* WSAA-FREQ-FACTOR-WS */
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(11, 5).setUnsigned();
	private FixedLengthStringData wsaaFreqFactorFmt = new FixedLengthStringData(11).isAPartOf(wsaaFreqFactor, 0,REDEFINE);
	private ZonedDecimalData wsaaFreqDecimal = new ZonedDecimalData(5, 5).isAPartOf(wsaaFreqFactorFmt, 6);

	private FixedLengthStringData filler = new FixedLengthStringData(11).isAPartOf(wsaaFreqFactor, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaFreqFactorRem = new ZonedDecimalData(5, 0).isAPartOf(filler, 6).setUnsigned();

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(192);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 5);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 9);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 13);
	private PackedDecimalData wsaaAdvptdat = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 17);
	private PackedDecimalData wsaaNoOfFreqs = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 22).setUnsigned();
	private PackedDecimalData wsaaUnitEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 26);
	private PackedDecimalData wsaaApaRequired = new PackedDecimalData(15, 2).isAPartOf(wsaaTransArea, 31);
	private FixedLengthStringData wsaaNewBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 46);
	private PackedDecimalData wsaaProDaysPrem = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 48 );
	private PackedDecimalData wsaaProDaysStpduty = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 65);
	private PackedDecimalData wsaaProDaysTotPrem = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 82); 
	private PackedDecimalData wsaaProDaysFee = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 99 ); 
	private PackedDecimalData wsaaTaxRelAmt = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 116 );
	private FixedLengthStringData wsaaChdrpf = new FixedLengthStringData(37).isAPartOf(wsaaTransArea, 133 );
	private FixedLengthStringData wsaaChdrpfGroupkey = new FixedLengthStringData(12).isAPartOf(wsaaChdrpf, 0);
	private FixedLengthStringData wsaaChdrpfMandref = new FixedLengthStringData(5).isAPartOf(wsaaChdrpf, 12);
	private FixedLengthStringData wsaaChdrpfMembsel = new FixedLengthStringData(10).isAPartOf(wsaaChdrpf, 17);
	private FixedLengthStringData wsaaChdrpfPayrnum = new FixedLengthStringData(8).isAPartOf(wsaaChdrpf, 27);
	private FixedLengthStringData wsaaChdrpfBillchnl = new FixedLengthStringData(2).isAPartOf(wsaaChdrpf, 35);
	private FixedLengthStringData wsaaPayrpf = new FixedLengthStringData(12).isAPartOf(wsaaTransArea, 170 );
	private FixedLengthStringData wsaaPayrpfGrupcoy = new FixedLengthStringData(1).isAPartOf(wsaaPayrpf, 0);
	private FixedLengthStringData wsaaPayrpfIncomeSeqNo = new FixedLengthStringData(2).isAPartOf(wsaaPayrpf, 1 );
	private FixedLengthStringData wsaaPayrpfBillday = new FixedLengthStringData(2).isAPartOf(wsaaPayrpf, 3);
	private FixedLengthStringData wsaaPayrpfBillmonth = new FixedLengthStringData(2).isAPartOf(wsaaPayrpf, 5);
	private PackedDecimalData wsaaPayrpfOrigBillcd = new PackedDecimalData(8, 0).isAPartOf(wsaaPayrpf, 7 );
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaTransArea, 182, FILLER).init(SPACES);
	
	private PackedDecimalData wsaaCurFreqLstInst = new PackedDecimalData(8, 0);

	private ZonedDecimalData wsaaNetBillamt = new ZonedDecimalData(17, 2);

	private PackedDecimalData wsaaLastRerateDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaRerateDate = new ZonedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	/* WSAA-COMM-TOTALS */
	private ZonedDecimalData wsaaBascpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaBascpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaBascpyPay = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSrvcpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSrvcpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRnwcpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRnwcpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyPay = new ZonedDecimalData(17, 2).init(0);

	private ZonedDecimalData wsaaLowRerateDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaNextCpiDate = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaBtdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBillcd = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNextdt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaSuspAvail = new PackedDecimalData(17, 2);

	private ZonedDecimalData wsaaNetCbillamt = new ZonedDecimalData(17, 2);
	

	/* WSAA-MISCELLANEOUS-FLAGS */
	private ZonedDecimalData wsaaLinsBtdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEomDate = new ZonedDecimalData(8, 0).setUnsigned();
	protected FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30);

	private FixedLengthStringData wsaaEomDateFmt = new FixedLengthStringData(8).isAPartOf(wsaaEomDate, 0, REDEFINE);
	private ZonedDecimalData wsaaEomYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaEomDateFmt, 0).setUnsigned();
	private FixedLengthStringData wsaaEomYearFmt = new FixedLengthStringData(4).isAPartOf(wsaaEomYear, 0, REDEFINE);
	private ZonedDecimalData wsaaCenturyYear = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomYearFmt, 2).setUnsigned();
	private ZonedDecimalData wsaaEomMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomDateFmt, 4).setUnsigned();
	private ZonedDecimalData wsaaEomDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomDateFmt, 6).setUnsigned();
	private static final String wsaaDaysInMonthNorm = "312831303130313130313031";
	private static final String wsaaDaysInMonthLeap = "312931303130313130313031";
	private FixedLengthStringData wsaaDaysInMonth = new FixedLengthStringData(24);

	private FixedLengthStringData wsaaDaysInMonthRed = new FixedLengthStringData(24).isAPartOf(wsaaDaysInMonth, 0,REDEFINE);
	private ZonedDecimalData[] wsaaMonthDays = ZDArrayPartOfStructure(12, 2, 0, wsaaDaysInMonthRed, 0, UNSIGNED_TRUE);
	private String wsaaLeapYear = "";
	private String wsaaEndOfMonth = "";
	private FixedLengthStringData wsaaEom1stDate = new FixedLengthStringData(1);
	private PackedDecimalData wsaaResult = new PackedDecimalData(5, 0).setUnsigned();

	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrmjaTableDAM chdrlifIO = new ChdrmjaTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();

	protected PayrTableDAM payrIO = new PayrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();

	private Rdockey wsaaRdockey = new Rdockey();
	private Batckey wsaaBatckey = new Batckey();

	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T3695rec t3695rec = new T3695rec();
	private T6687rec t6687rec = new T6687rec();
	private T5688rec t5688rec = new T5688rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Prasrec prasrec = new Prasrec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Atreqrec atreqrec = new Atreqrec();

	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private Sh5mpScreenVars sv = ScreenProgram.getScreenVars(Sh5mpScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();

	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private RertpfDAO rertpfDAO = getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	private CoprpfDAO coprpfDAO = getApplicationContext().getBean("coprpfDAO", CoprpfDAO.class);
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Itempf itempf = new Itempf();
	private Clrrpf clrrpf;
	private Payrpf payrpf;
	private Linspf linspf;
	private Clntpf clntpf;
	private Covrpf covrpf;
	private Chdrpf chdrpf;
	private Ptrnpf ptrnpf;
	private Clrfpf clrfpf;
	private List<Rertpf> rertList = new ArrayList<>();
	private List<Covrpf> updateCovrList = new ArrayList<>();
	private List<Covrpf> insertCovrList = new ArrayList<>();
	private List<Coprpf> coprList = new ArrayList<>();
	private List<Racdpf> updateRacdList = new ArrayList<>();
	private List<Agcmpf> updateAgcmList = new ArrayList<>();
	private List<Agcmpf> insertAgcmList = new ArrayList<>();

	private ZonedDecimalData wsaaRerateEffDate = new ZonedDecimalData(8, 0).setUnsigned();
	private boolean rerateChgFlag;
	private boolean billChgFlag;
	private String cntDteFeature = "NBPRP113";
	private boolean isbillday;
	private boolean isFirstCovr = true;
	private boolean isProcesFreqChange;
	private boolean proratPremCalcFlag;

	private Map<String, Descpf> descMap = new HashMap<>();

	private FixedLengthStringData chdrlifchdrnum = new FixedLengthStringData(16);

	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaCurrptdt = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCurrentPayuptoDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray(15, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);

	/* WSAA-T5645-32TH-VALUE */
	private ZonedDecimalData wsaaT5645Cnttot32 = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaT5645Glmap32 = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaT5645Sacscode32 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sacstype32 = new FixedLengthStringData(2);

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccdateMm = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4).setUnsigned();
	private ZonedDecimalData wsaaOccdateDd = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6).setUnsigned();
	private ZonedDecimalData wsaaPtdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPtdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPtdateYy = new ZonedDecimalData(4, 0).isAPartOf(filler4, 0).setUnsigned();
	private ZonedDecimalData wsaaLastAnndate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaLastAnndate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaLastAnndateYy = new ZonedDecimalData(4, 0).isAPartOf(filler5, 0).setUnsigned();
	private ZonedDecimalData wsaaLastAnndateMm = new ZonedDecimalData(2, 0).isAPartOf(filler5, 4).setUnsigned();
	private ZonedDecimalData wsaaLastAnndateDd = new ZonedDecimalData(2, 0).isAPartOf(filler5, 6).setUnsigned();
	protected T5687rec t5687rec = new T5687rec();
	private T5675rec t5675rec = new T5675rec();
	protected Tr386rec tr386rec = new Tr386rec();

	private Premiumrec premiumrec = new Premiumrec();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0);
	private AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	private boolean stampDutyflag;
	private boolean lnkgFlag;

	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected Hpadpf hpadIO;
	protected Mrtapf mrtaIO;
	private PackedDecimalData wsaaOutstPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOutstStpdty = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOutstPremWithStpdty = new PackedDecimalData(17, 2);
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T6654rec t6654rec = new T6654rec();
	private static final String t6654 = "T6654";
	private static final String t5674 = "T5674";
	private static final String t3623 = "T3623";
	private static final String t3695 = "T3695";
	private static final String t5645 = "T5645";
	private static final String t5675 = "T5675";
	private static final String t5688 = "T5688";
	private static final String t3588 = "T3588";
	private static final String IT = "IT";
	protected int intT6654Leaddays = 0;
	protected Mgfeelrec mgfeelrec = new Mgfeelrec();
	private T5674rec t5674rec = new T5674rec();
	private Billreqrec billreqrec = new Billreqrec();
	private T3629rec t3629rec = new T3629rec();
	protected String strT6687TaxRelSub;
	protected PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	protected String strT3629BankCode;
	protected FixedLengthStringData wsaaSbmaction = new FixedLengthStringData(1);
	protected Validator quotation = new Validator(wsaaSbmaction, "B");
	
	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private Batcuprec batcuprec = new Batcuprec();
	private String wsaaNotEnoughCash = "N";
	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
	private T5667rec t5667rec = new T5667rec();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTol2 = new PackedDecimalData(17, 2).init(0);
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private String wsaaAgtTerminatedFlag = "";
	private String wsaaToleranceFlag = "";
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator statusValid = new Validator(wsaaValidStatus, "Y");
	private Validator statusNotValid = new Validator(wsaaValidStatus, "N");
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	boolean CollectPrortePremSeparately;
	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, tableLoop3020, loopCheck3040
	}

	public Ph5mp() {
		super();
		screenVars = sv;
		new ScreenModel("Sh5mp", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	@Override
	protected void initialise1000() {
		//PINNACLE-3179 START
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		//PINNACLE-3179 END
		CollectPrortePremSeparately  = FeaConfg.isFeatureExist("2", "CSBIL007", appVars, IT);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		sv.dataArea.set(SPACES);
		wsaaSbmaction.set(wsspcomn.sbmaction);
		wsaaBatckey.set(wsspcomn.batchkey);
		boolean cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, IT);
		billChgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSBIL005", appVars, IT);
		if (!cntDteFlag) {
			sv.occdateOut[Varcom.nd.toInt()].set("Y");
		}
		stampDutyflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPROP01", appVars, IT);
		lnkgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP055", appVars, IT);
		isbillday = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP011", appVars, IT);
		sv.advptdat.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.billcd.set(varcom.vrcmMaxDate);
		prasrec.taxrelamt.set(ZERO);
		
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setFunction(Varcom.retrv);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}

		payrIO.setRecKeyData(SPACES);
		payrIO.setRecNonKeyData(SPACES);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(Varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		
		payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(chdrlifIO.getChdrcoy().toString(),chdrlifIO.getChdrnum().toString());
		if (payrpf == null) {
			syserrrec.statuz.set(Varcom.mrnf);
			syserrrec.params.set("2".concat(chdrlifIO.getChdrnum().toString()));
			fatalError600();
		}
		
		rertList = rertpfDAO.getRertpfList(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString(),"1",0);
		
		clrrpf = clrrpfDAO.getClrfRecord(chdrlifIO.getChdrpfx().toString(), payrpf.getChdrcoy(),(payrpf.getChdrnum() + payrpf.getPayrseqno()).substring(0, 9), "PY");
		if (clrrpf == null) {
			syserrrec.statuz.set(Varcom.mrnf);
			syserrrec.params.set(payrpf.getChdrcoy().concat(payrpf.getChdrnum() + payrpf.getPayrseqno()));
			fatalError600();
		}

		linspf = linspfDAO.getLinsRecordByCoyAndNumAndflag(payrpf.getChdrcoy(), payrpf.getChdrnum(), payrpf.getValidflag());
		if (linspf != null) {
			wsaaLinsBtdate.set(linspf.getInstfrom());
			if (isNE(linspf.getBillchnl(), payrpf.getBillfreq())) {
				wsaaLinsBtdate.set(payrpf.getBtdate());
			}
		} else {
			wsaaLinsBtdate.set(payrpf.getBtdate());
			if (isNE("", payrpf.getBillfreq())) {
				wsaaLinsBtdate.set(payrpf.getBtdate());
			}
		}

		sv.occdate.set(chdrlifIO.getOccdate());
		sv.chdrnum.set(chdrlifIO.getChdrnum());
		sv.agntnum.set(chdrlifIO.getAgntnum());
		sv.cnttype.set(chdrlifIO.getCnttype());

		findDesc1300();
		if (descMap == null || descMap.size() <= 0 || descMap.get(t5688) == null) {
			sv.ctypdesc.fill("?");
		} else {
			sv.ctypdesc.set(descMap.get(t5688).getLongdesc());
		}

		sv.cownnum.set(chdrlifIO.getCownnum());

		clntpf = new Clntpf();
		clntpf.setClntnum(chdrlifIO.getCownnum().toString());
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf = clntpfDAO.selectClient(clntpf);

		if (clntpf == null || clntpf.getClntnum() == null || clntpf.getClntnum().trim().equals("")
				|| !clntpf.getValidflag().trim().equals("1")) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		} else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		if (clntpf != null) {
			sv.payrnum.set(clrrpf.getClntnum());
			clntpf.setClntnum(clrrpf.getClntnum());
			clntpf.setClntpfx("CN");
			clntpf.setClntcoy(wsspcomn.fsuco.toString());
			clntpf = clntpfDAO.selectClient(clntpf);
		}

		if (clntpf == null || clntpf.getClntnum() == null || clntpf.getClntnum().trim().equals("")
				|| !clntpf.getValidflag().trim().equals("1")) {
			sv.payornameErr.set(errorsInner.e304);
			sv.payorname.set(SPACES);
		} else {
			plainname();
			sv.payorname.set(wsspcomn.longconfname);
		}

		if (isNE(sv.agntnum, SPACES)) {
			getAgentName1200();
		}

		sv.btdate.set(chdrlifIO.getBtdate());
		sv.ptdate.set(chdrlifIO.getPtdate());

		if (descMap == null || descMap.size() <= 0 || descMap.get(t3623) == null) {
			sv.rstate.fill("?");
		} else {
			sv.rstate.set(descMap.get(t3623).getLongdesc());
		}

		/* Look up premium status */
		if (descMap == null || descMap.size() <= 0 || descMap.get(t3588) == null) {
			sv.pstate.fill("?");
		} else {
			sv.pstate.set(descMap.get(t3588).getLongdesc());
		}

		sv.cntcurr.set(chdrlifIO.getCntcurr());
		sv.billcurr.set(payrpf.getBillcurr());
		sv.pmtfreq.set(payrpf.getBillfreq());
		sv.billfreq.set(chdrlifIO.getBillfreq());
		traceProjectedPtd();
		wsaaNewBillfreq.set(sv.billfreq);

		/* calculate the no of frequency days */

		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(payrpf.getBtdate());
		datcon3rec.intDate2.set(sv.advptdat);
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callDatcon32400();
		wsaaNoOfFreqs.set(wsaaFreqFactor);
		if (isNE(wsaaFreqFactorRem, ZERO)) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(payrpf.getBtdate());
			datcon2rec.frequency.set(payrpf.getBillfreq());
			datcon2rec.freqFactor.set(wsaaNoOfFreqs);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, Varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaaCurFreqLstInst.set(datcon2rec.intDate2);
		} else {
			wsaaCurFreqLstInst.set(sv.advptdat);
		}
		checkNextAnnvdt();
		if (isEQ(datcon4rec.intDate2, sv.advptdat)) {
			wsaaProDaysPrem.set(ZERO);
			if (stampDutyflag) {
				wsaaProDaysStpduty.set(ZERO);
			}
		}
		proratPremCalcFlag = true;
		List<Covrpf> covrList = covrpfDAO.getCovrmjaByComAndNum(chdrlifIO.getChdrcoy().toString(),
				chdrlifIO.getChdrnum().toString());
		Covrpf covr;
		if (covrList.isEmpty()) {
			return;
		} else {
			covr = covrList.get(0);
		}
		callPmexSubroutine(covr);
		wsaaOutstPrem.set(premiumrec.calcPrem);
		wsaaOutstStpdty.set(premiumrec.zstpduty01);
		if (stampDutyflag) {
			compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
		}
		sv.instPrem.set(premiumrec.calcPrem);
		wsaaProDaysPrem.set(sv.instPrem);
		wsaaProDaysStpduty.set(premiumrec.zstpduty01);
		wsaaOutstPremWithStpdty.set(premiumrec.calcPrem);
		proratPremCalcFlag = false;
		
		isFirstCovr = false;
		for (int i = 0; i < covrList.size(); i++) {
			Covrpf c = covrList.get(i);
			callPmexSubroutine(c);
			if (stampDutyflag) {
				compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
				c.setZstpduty01(premiumrec.zstpduty01.getbigdata());
			}
			c.setInstprem(premiumrec.calcPrem.getbigdata());
			c.setZbinstprem(premiumrec.calcBasPrem.getbigdata());
			c.setZlinstprem(premiumrec.calcLoaPrem.getbigdata());
			c.setCommprem(premiumrec.commissionPrem.getbigdata());
			addCopr(c);
			if (i + 1 < covrList.size()) {
				isFirstCovr = false;
			} else {
				isFirstCovr = true;
			}
		}

		/* Get the tax relief method from T5688. */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(Varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK) && isNE(itdmIO.getStatuz(), Varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company) || isNE(itdmIO.getItemtabl(), t5688)
				|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype()) || isEQ(itdmIO.getStatuz(), Varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.f290);
		} else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/

		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5674);
		itempf.setItemitem(t5688rec.feemeth.toString());
		itempf = itemDao.getItempfRecord(itempf);


		if (null == itempf) {
			syserrrec.params.set(itemIO.getParams());
			scrnparams.errorCode.set(errorsInner.f151);
			wsspcomn.edterror.set("Y");
			return ;
		}
		t5674rec.t5674Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* Check subroutine NOT = SPACES before attempting call.           */
		
		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T6687");
		itempf.setItemitem(t5688rec.taxrelmth.toString());

		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf != null) {
			t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		} else {
			t6687rec.t6687Rec.set(SPACES);
		}
		if (isNE(t5674rec.commsubr, SPACES)) {
			mgfeelrec.mgfeelRec.set(SPACES);
			mgfeelrec.effdate.set(ZERO);
			mgfeelrec.mgfee.set(ZERO);
			mgfeelrec.cnttype.set(chdrlifIO.getCnttype());
			/* MOVE CHDRLNB-BILLFREQ       TO MGFL-BILLFREQ.                */
			mgfeelrec.billfreq.set(payrpf.getBillfreq());
			mgfeelrec.effdate.set(chdrlifIO.getOccdate());
			mgfeelrec.bilfrmdt.set(chdrlifIO.getPtdate());
			mgfeelrec.biltodt.set(sv.advptdat);
			mgfeelrec.policyRCD.set(chdrlifIO.getOccdate());
			mgfeelrec.cntcurr.set(chdrlifIO.getCntcurr());
			mgfeelrec.company.set(wsspcomn.company);
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
			if (isNE(mgfeelrec.statuz, Varcom.oK)
					&& isNE(mgfeelrec.statuz, Varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				fatalError600();
			}
			zrdecplrec.amountIn.set(mgfeelrec.mgfee);
			zrdecplrec.currency.set(chdrlifIO.getCntcurr());
			a000CallRounding();
			mgfeelrec.mgfee.set(zrdecplrec.amountOut);
		} else {
			mgfeelrec.mgfee.set(0);
		}
		wsaaProDaysFee.set(mgfeelrec.mgfee);
		sv.instPrem.set(add(sv.instPrem, mgfeelrec.mgfee));
		/* If the tax relief method is not spaces calculate the tax */
		/* relief amount and deduct it from the premium. */

		if (isNE(t5688rec.taxrelmth, SPACES)) {
			prasrec.clntnum.set(clrrpf.getClntnum());
			prasrec.clntcoy.set(clrrpf.getClntcoy());
			prasrec.incomeSeqNo.set(payrpf.getIncomeSeqNo());
			prasrec.cnttype.set(chdrlifIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			prasrec.effdate.set(payrpf.getBillcd());
			prasrec.company.set(payrpf.getChdrcoy());
			prasrec.grossprem.set(sv.instPrem);
			prasrec.statuz.set(Varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
		}
		zrdecplrec.amountIn.set(prasrec.taxrelamt);
		a000CallRounding();
		prasrec.taxrelamt.set(zrdecplrec.amountOut);
		compute(wsaaNetPrem, 2).set(sub(sv.instPrem, prasrec.taxrelamt));
		sv.instPrem.set(wsaaNetPrem);
		wsaaTaxRelAmt.set(prasrec.taxrelamt);
		calculateSuspense1600();
		calculateApa1800();
		wsaaLowRerateDate.set(ZERO);

		List<Covrpf> list = covrpfDAO.getCovrmjaByComAndNum(chdrlifIO.getChdrcoy().toString(),
				chdrlifIO.getChdrnum().toString());
		wsaaLowRerateDate.set(varcom.vrcmMaxDate);
		wsaaNextCpiDate.set(varcom.vrcmMaxDate);
		if (list != null && !list.isEmpty()) {
			for (int i = 0; i < list.size(); i++) {
				covrpf = list.get(i);
				lowestRerateDate1700();
			}
		}
		sv.cpiDate.set(wsaaNextCpiDate);
	}
	
	protected void addCopr(Covrpf c) {
		Coprpf copr = new Coprpf();
		copr.setChdrcoy(c.getChdrcoy());
		copr.setChdrnum(c.getChdrnum());
		copr.setLife(c.getLife());
		copr.setJlife(c.getJlife());
		copr.setCoverage(c.getCoverage());
		copr.setRider(c.getRider());
		copr.setPlnsfx(c.getPlanSuffix());
		copr.setValidflag(c.getValidflag());
		copr.setSumins(c.getSumins());
		copr.setSingp(c.getSingp());
		copr.setZstpduty01(c.getZstpduty01());
		copr.setInstprem(c.getInstprem());
		copr.setZbinstprem(c.getZbinstprem());
		copr.setZlinstprem(c.getZlinstprem());
		copr.setCommprem(c.getCommprem());
		copr.setInstfrom(sv.btdate.toInt());
		copr.setInstto(sv.advptdat.toInt());
		
		coprList.add(copr);
	}

	protected void readTables(Covrpf c) {
		Itempf item = new Itempf();
		item.setItempfx(IT);
		item.setItemcoy(chdrlifIO.getChdrcoy().toString());
		item.setItemtabl("T5687");
		item.setItemitem(c.getCrtable());
		item.setItmfrm(chdrlifIO.getOccdate().getbigdata());
		item.setItmto(chdrlifIO.getOccdate().getbigdata());
		List<Itempf> t5687ItemList = itemDao.findByItemDates(item);
		if (t5687ItemList.isEmpty()) {
			t5687rec.t5687Rec.set(SPACES);
			syserrrec.params.set(IT.concat(chdrlifIO.getChdrcoy().toString()).concat(t5675)
					.concat(t5687rec.premmeth.toString()));
			fatalError600();
		} else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(t5687ItemList.get(0).getGenarea()));
		}
		List<Itempf> t5675ItempfList = itemDao.getAllItemitem(IT, chdrlifIO.getChdrcoy().toString(), t5675,
				t5687rec.premmeth.toString());
		if (t5675ItempfList.isEmpty()) {
			t5675rec.t5675Rec.set(SPACES);
			syserrrec.params.set(IT.concat(chdrlifIO.getChdrcoy().toString()).concat(t5675)
					.concat(t5687rec.premmeth.toString()));
			fatalError600();
		} else {
			t5675rec.t5675Rec.set(StringUtil.rawToString(t5675ItempfList.get(0).getGenarea()));
		}
	}

	protected void calculateAnb3260(Lifepf lifepf) {
		wsaaAnb.set(ZERO);
		AgecalcPojo agecalcPojo = new AgecalcPojo();
		agecalcPojo.setFunction("CALCP");
		agecalcPojo.setLanguage(wsspcomn.language.toString());
		agecalcPojo.setCnttype(chdrlifIO.getCnttype().toString());
		agecalcPojo.setIntDate1(Integer.toString(lifepf.getCltdob()));
		agecalcPojo.setIntDate2(premiumrec.effectdt.toString());
		agecalcPojo.setCompany(wsspcomn.fsuco.toString());
		agecalcUtils.calcAge(agecalcPojo);
		if (isNE(agecalcPojo.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(agecalcPojo.toString());
			syserrrec.statuz.set(agecalcPojo.getStatuz());
			fatalError600();
		}
		wsaaAnb.set(agecalcPojo.getAgerating());
	}

	protected void getAnny3280(Covrpf c) {
		Annypf annypf = annypfDAO.getAnnyRecordByAnnyKey(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(),
				c.getRider(), c.getPlanSuffix());
		if (annypf != null) {
			premiumrec.freqann.set(annypf.getFreqann());
			premiumrec.advance.set(annypf.getAdvance());
			premiumrec.arrears.set(annypf.getArrears());
			premiumrec.guarperd.set(annypf.getGuarperd());
			premiumrec.intanny.set(annypf.getIntanny());
			premiumrec.capcont.set(annypf.getCapcont());
			premiumrec.withprop.set(annypf.getWithprop());
			premiumrec.withoprop.set(annypf.getWithoprop());
			premiumrec.ppind.set(annypf.getPpind());
			premiumrec.nomlife.set(annypf.getNomlife());
			premiumrec.dthpercn.set(annypf.getDthpercn());
			premiumrec.dthperco.set(annypf.getDthperco());
		} else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

	protected void getRcvdpf(Covrpf covrpf) {
		boolean dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", appVars, IT);
		boolean incomeProtectionflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP02", appVars, IT);
		boolean premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", appVars, IT);
		if (incomeProtectionflag || premiumflag || dialdownFlag) {
			Rcvdpf rcvdPFObject = new Rcvdpf();
			rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
			rcvdPFObject.setChdrnum(covrpf.getChdrnum());
			rcvdPFObject.setLife(covrpf.getLife());
			rcvdPFObject.setCoverage(covrpf.getCoverage());
			rcvdPFObject.setRider(covrpf.getRider());
			rcvdPFObject.setCrtable(covrpf.getCrtable());
			rcvdPFObject = rcvdDAO.readRcvdpf(rcvdPFObject);
			premiumrec.bentrm.set(rcvdPFObject.getBentrm());
			if (rcvdPFObject.getPrmbasis() != null && isEQ("S", rcvdPFObject.getPrmbasis())) {
				premiumrec.prmbasis.set("Y");
			} else {
				premiumrec.prmbasis.set("");
			}
			premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
			premiumrec.occpcode.set("");
			premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
			premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
		}
	}

	protected void getAgentName1200() {

		if (isEQ(sv.agntnum, SPACES)) {
			sv.agntnumErr.set(errorsInner.e186);
			return;
		}

		clntpf = clntpfDAO.getAgntAndClnt("AG", wsspcomn.company.toString(), sv.agntnum.toString(),
				wsspcomn.fsuco.toString());

		sv.agentname.set(SPACES);
		if (clntpf == null) {
			return;
		}

		if (clntpf.getClntnum().trim().equals("")) {
			syserrrec.params.set("AG".concat(wsspcomn.company.toString()).concat(sv.agntnum.toString()));
			fatalError600();
		}
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

	protected void findDesc1300() {
		/* READ */
		Map<String, String> itemMap = new HashMap<>();
		itemMap.put(t5688, chdrlifIO.getCnttype().toString());
		itemMap.put(t3623, chdrlifIO.getStatcode().toString());
		itemMap.put(t3588, payrpf.getPstatcode());/* IJTI-1523 */

		descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		/* EXIT */
	}

	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

	protected void payeename() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return;
		}
		if (isEQ(clntpf.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

	protected void corpname() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		StringUtil stringVariable1 = new StringUtil();
		if (clntpf.getLsurname() != null && clntpf.getLgivname() != null) {
			stringVariable1.addExpression(clntpf.getLsurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		}
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/* CORP-EXIT */
	}

	protected void checkNextAnnvdt() {
		datcon4rec.intDate1.set(wsaaLastAnndate);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set("01");
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
	}

	private void traceProjectedPtd() {
		if (isEQ(sv.billfreqErr, SPACES)) {
			findLastAnnivesary1410();
			effdate1450();
		}
	}

	private void findLastAnnivesary1410() {
		wsaaOccdate.set(chdrlifIO.getOccdate());
		wsaaPtdate.set(sv.ptdate);
		wsaaLastAnndateYy.set(wsaaPtdateYy);
		wsaaLastAnndateMm.set(wsaaOccdateMm);
		wsaaLastAnndateDd.set(wsaaOccdateDd);
		datcon1rec.function.set(Varcom.conv);
		datcon1rec.statuz.set(SPACES);
		while (!(isEQ(datcon1rec.statuz, Varcom.oK))) {
			datcon1rec.intDate.set(wsaaLastAnndate);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, Varcom.oK)) {
				wsaaLastAnndateDd.subtract(1);
			}
		}

		if (isGT(wsaaLastAnndate, sv.ptdate)) {
			datcon4rec.intDate1.set(wsaaLastAnndate);
			datcon4rec.billday.set(payrpf.getDuedd());
			datcon4rec.billmonth.set(payrpf.getDuemm());
			datcon4rec.freqFactor.set(-1);
			datcon4rec.frequency.set("01");
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, Varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastAnndate.set(datcon4rec.intDate2);
		}
	}

	private void effdate1450() {
		datcon4rec.intDate1.set(wsaaLastAnndate);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(sv.billfreq);
		if (isEQ(sv.billfreq, "01")) {
			datcon4rec.billday.set(payrpf.getDuedd());
			datcon4rec.billmonth.set(payrpf.getDuemm());
		}
		while (!(isGTE(datcon4rec.intDate1, sv.ptdate))) {
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, Varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}
		//PINNACLE-3179 START move projected PTDATE to cover backdated billing
		if(isLT(datcon4rec.intDate1,wsaaToday)) {
			while (!(isGTE(datcon4rec.intDate1, wsaaToday))) {
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
					if (isNE(datcon4rec.statuz, Varcom.oK)) {
						syserrrec.params.set(datcon4rec.datcon4Rec);
						syserrrec.statuz.set(datcon4rec.statuz);
						fatalError600();
						}
				datcon4rec.intDate1.set(datcon4rec.intDate2);
				}
		}
				//PINNACLE-3179 END
		sv.advptdat.set(datcon4rec.intDate1);
	}

	protected void calculateSuspense1600() {
		calculateSuspense1610();
		getSign1020();
	}

	protected void calculateSuspense1610() {
		itempf = new Itempf();

		itempf.setItempfx(IT);
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq("  ");
		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf == null) {
			syserrrec.params.set(IT.concat(wsspcomn.company.toString()).concat(t5645).concat(wsaaProg.toString()));
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

	protected void getSign1020() {
		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t3695);
		itempf.setItemitem(t5645rec.sacstype01.toString().trim());
		itempf = itemDao.getItemRecordByItemkey(itempf);
		chdrlifchdrnum.set(chdrlifIO.getChdrnum()); // ILIFE-7082

		if (itempf == null) {
			syserrrec.params.set(
					IT.concat(wsspcomn.company.toString()).concat(t3695).concat(t5645rec.sacstype01.toString()));
			fatalError600();
		}
		t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		Acblpf acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode01.toString(),
				chdrlifchdrnum.toString(), payrpf.getBillcurr(), t5645rec.sacstype01.toString());

		if (acblpf == null) {
			sv.sacscurbal.set(ZERO);
		} else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(sv.sacscurbal, 2).set(mult(acblpf.getSacscurbal(), -1));
			} else {
				sv.sacscurbal.set(acblpf.getSacscurbal());
			}
		}
	}

	protected void lowestRerateDate1700() {
		if (isLT(covrpf.getPremCessDate(), wsaaLowRerateDate)) {
			wsaaLowRerateDate.set(covrpf.getRerateDate());
		}
		if (isLT(covrpf.getCpiDate(), wsaaNextCpiDate) && isNE(covrpf.getCpiDate(), 0)) {
			wsaaNextCpiDate.set(covrpf.getCpiDate());
		}
	}

	protected void calculateApa1800() {
		initialize(rlpdlonrec.rec);
		rlpdlonrec.function.set(Varcom.info);
		rlpdlonrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		rlpdlonrec.chdrnum.set(chdrlifIO.getChdrnum());
		rlpdlonrec.prmdepst.set(ZERO);
		rlpdlonrec.language.set(wsspcomn.language);

		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);

		rlpdlonrec.effdate.set(datcon1rec.intDate);
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if (isNE(rlpdlonrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(rlpdlonrec.statuz);
			syserrrec.params.set(rlpdlonrec.rec);
			fatalError600();
		}
		sv.prmdepst.set(rlpdlonrec.prmdepst);
	}

	protected void preScreenEdit() {
		/* PRE-EXIT */
	}

	@Override
	protected void screenEdit2000() {
		wsspcomn.edterror.set(Varcom.oK);
		validate2020();

	}

	protected void validate2020() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			return;
		}
		if (isEQ(sv.advptdat, varcom.vrcmMaxDate)) {
			sv.advptdatErr.set(errorsInner.h082);
			catchAll2030();
			return;
		}
		if (isNE(wsaaNextCpiDate, varcom.vrcmMaxDate) && isGT(sv.advptdat, wsaaNextCpiDate)) {
			sv.cpidteErr.set(errorsInner.h099);
			catchAll2030();
			return;
		}
		if (isGT(sv.advptdat, wsaaLowRerateDate)) {
			sv.advptdatErr.set(errorsInner.h156);
			catchAll2030();
			return;
		}
		/* IF Sh5mp-PAY-TO-DATE-ADVANCE NOT > CHDRLIF-BTDATE */
		/* OR NOT > CHDRLIF-PTDATE */
		/* MOVE U056 TO Sh5mp-ADVPTDAT-ERR */
		/* GO TO 2030-CATCH-ALL. */
		/* IF CHDRLIF-PTDATE NOT = CHDRLIF-BTDATE */
		if (isNE(payrpf.getPtdate(), payrpf.getBtdate())) {
			/* MOVE U059 TO Sh5mp-ADVPTDAT-ERR */
			/* MOVE H013 TO Sh5mp-ADVPTDAT-ERR <008> */
			/* MOVE 'Y' TO WSSP-EDTERROR. <002> */
			sv.ptdateErr.set(errorsInner.h013);
			catchAll2030();
			return;
		}
		if (isEQ(scrnparams.statuz, Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		datcon2rec.datcon2Rec.set(SPACES);
		
		if (isNE(sv.prorateflag, 'Y') && isbillday) {
			if (isNE(payrIO.getBillday().toString(), SPACES)) {
				getCollectiondate(wsaaToday.toString(), payrIO.getBillday().toString());
				sv.billcd.set(datcon2rec.intDate2);
			} else {
				sv.billcd.set(sv.advptdat); 
			}
		}
		else {
			sv.billcd.set(wsaaToday);  
		}
	}
	
	protected void getCollectiondate(String todaydate, String billday) {
		String dayFrom = todaydate.substring(6, 8);
		if(isLT(billday,dayFrom)) {
			todaydate = todaydate.substring(0, 6).concat(billday);
			datcon2rec.intDate1.set(todaydate);
		    datcon2rec.intDate2.set(ZERO);
		    datcon2rec.freqFactor.set("01");
		    datcon2rec.frequency.set("12");
		    callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		    if (isNE(datcon2rec.statuz, Varcom.oK)) {
		        syserrrec.statuz.set(datcon2rec.statuz);
		        fatalError600();
		    }
	}
	else {
			compute(datcon2rec.freqFactor, 0).set(sub(billday,dayFrom));
			
		    datcon2rec.intDate1.set(todaydate);
		    datcon2rec.intDate2.set(ZERO);
		    datcon2rec.freqFactor.set(datcon2rec.freqFactor);
		    datcon2rec.frequency.set("DY");
		    callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		    if (isNE(datcon2rec.statuz, Varcom.oK)) {
		        syserrrec.statuz.set(datcon2rec.statuz);
		        fatalError600();
		    }

		}
	}

	protected void catchAll2030() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void callDatcon32400() {
		/* CALL-DATCON3 */
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaFreqFactor.set(datcon3rec.freqFactor);
		/* EXIT */
	}

	@Override
	protected void update3000() {
		if (quotation.isTrue())  { 
			return;
		}
		if (isNE(sv.prorateflag, 'Y') || CollectPrortePremSeparately) { 
			update3010();
			checkAgentTerminate4100();
			billing2200();
			processLinsRecord2302();
			if (isEQ(wsaaNotEnoughCash, "Y")){
				produceBextRecord();
			}
			procesFreqChange();
			writePtrn();
			releaseSoftlock3900();
		} else {
			batchUpdate3020();
			triggerATjob();
		}
	}

	protected void update3010() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readData3010();
				case tableLoop3020:
					tableLoop3020();
				case loopCheck3040:
					loopCheck3040();
					transDescription3050();
					updateApa3060();
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void readData3010() {
		comlinkrec.currto.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(chdrlifIO.getChdrcoy().toString(),
				chdrlifIO.getChdrnum().toString());
		if (chdrpf == null) {
			syserrrec.params.set(chdrlifIO.getChdrcoy().toString().concat(chdrlifIO.getChdrnum().toString()));
			fatalError600();
		}
		Chdrpf updateChdrpf = new Chdrpf();
		updateChdrpf.setUniqueNumber(chdrpf.getUniqueNumber());
		
		//Setting values changed in billing change previous screen
		chdrpf.setGrupkey(chdrlifIO.getGrupkey().toString());
		chdrpf.setMandref(chdrlifIO.getMandref().toString());
		chdrpf.setMembsel(chdrlifIO.getMembsel().toString());
		chdrpf.setPayrnum(chdrlifIO.getPayrnum().toString());		
		chdrpf.setBillchnl(chdrlifIO.getBillchnl().toString());	
		
		//Setting values changed in billing change previous screen
		payrpf.setGrupnum(payrIO.getGrupnum().toString());
		payrpf.setGrupcoy(payrIO.getGrupcoy().toString());
		payrpf.setMembsel(payrIO.getMembsel().toString());
		payrpf.setMandref(payrIO.getMandref().toString());
		payrpf.setIncomeSeqNo(payrIO.getIncomeSeqNo().toInt());
		payrpf.setBillchnl(payrIO.getBillchnl().toString());
		
		ptrnpf = new Ptrnpf();
		ptrnpf.setPtrneff(chdrpf.getBtdate());

		payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(chdrlifIO.getChdrcoy().toString(),
				chdrlifIO.getChdrnum().toString());

		if (payrpf == null) {
			syserrrec.params.set(chdrlifIO.getChdrcoy().toString().concat(chdrlifIO.getChdrnum().toString()));
			fatalError600();
		}
		Payrpf updatePayrpf = new Payrpf();
		updatePayrpf.setUniqueNumber(payrpf.getUniqueNumber());
		wsaaCurrptdt.set(payrpf.getPtdate());
		updateChdrpf.setCurrto(wsaaCurrptdt.toInt());

		/* Read the client role file to get the payer number. */
		clrfpf = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrpf.getChdrpfx(), payrpf.getChdrcoy(),
				payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1)),
				"PY");
		if (clrfpf == null) {
			syserrrec.statuz.set(Varcom.mrnf);
			syserrrec.params.set(payrpf.getChdrcoy().concat(
					payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1))));
			fatalError600();
		}
		wsaaIndex.set(0);
		wsaaPremDiff.set(0);
		compute(wsaaTranno, 0).set(add(chdrpf.getTranno(), 1));
		/* Calculate the first date to pay-to date. */
		/* MOVE SPACES TO DTC2-FUNCTION. */
		/* MOVE CHDRLIF-BTDATE TO DTC2-INT-DATE-1. */
		/* MOVE CHDRLIF-BILLFREQ TO DTC2-FREQUENCY. */
		/* MOVE PAYR-BTDATE TO DTC2-INT-DATE-1. <018> */
		/* MOVE PAYR-BILLFREQ TO DTC2-FREQUENCY. <018> */
		/* MOVE 1 TO DTC2-FREQ-FACTOR. */
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC. */
		/* IF DTC2-STATUZ NOT = O-K */
		/* MOVE DTC2-DATCON2-REC TO SYSR-PARAMS */
		/* PERFORM XXXX-FATAL-ERROR. */
		/* MOVE DTC2-INT-DATE-2 TO WSAA-CURRENT-PAYUPTO-DATE. */
		/* Read the Premium Billing file to find first billing date. */

		linspf = linspfDAO.getLinsRecordByCoyAndNumAndflag(payrpf.getChdrcoy(), payrpf.getChdrnum(),
				payrpf.getValidflag());
		if (linspf != null) {
			wsaaLinsBtdate.set(linspf.getInstfrom());
		} else {
			wsaaLinsBtdate.set(payrpf.getBtdate());
		}
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(chdrpf.getOccdate());
		datcon3rec.intDate2.set(wsaaLinsBtdate);
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaFreqFactor.set(datcon3rec.freqFactor);
		/* If the original commence date and the first billing */
		/* date are a whole billing frequency apart, use the risk */
		/* commence dates day for future billing, otherwise use */
		/* the first billing dates day for the first renewal, and */
		/* there after the first LINS install from dates day. */
		/* The last two dates are moved previously, before calling */
		/* DATCON3. */
		if (isEQ(wsaaFreqDecimal, ZERO)) {
			wsaaLinsBtdate.set(chdrpf.getOccdate());
		}
		/* Check to see if the commence date is at the end of the month. */
		wsaaEndOfMonth = "N";
		wsaaEomDate.set(chdrpf.getOccdate());
		checkEndOfMonth9000();
		wsaaEom1stDate.set(wsaaEndOfMonth);
		/* Check to see if the 1ST bill date is at the end of the month, */
		/* and if the month is February. */
		wsaaEndOfMonth = "N";
		wsaaEomDate.set(wsaaLinsBtdate);
		if (isEQ(wsaaEomMonth, 2)) {
			checkEndOfMonth9000();
		}
		/* If the commence date was not at the end of the month, and */
		/* the 1st billing date was in February and at the end of the */
		/* month, set the billing day to the original commence day. */
		if (isEQ(wsaaEndOfMonth, "Y") && isEQ(wsaaEom1stDate, "N")) {
			wsaaLinsBtdate.set(chdrpf.getOccdate());
		}
		wsaaCurrentPayuptoDate.set(sv.advptdat);
		/* Get todays date. */
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Read T5645 to get the financial accounting rules */
		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem("Ph5mp");
		itempf = itemDao.getItempfRecord(itempf);
		if (itempf == null) {
			syserrrec.params.set(IT.concat(chdrlifIO.getChdrcoy().toString()).concat(t5645).concat("Ph5mp"));
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* Read the contract definition on T5688. */
		List<Itempf> list = itemDao.getItdmByFrmdate(chdrlifIO.getChdrcoy().toString(), t5688, chdrpf.getCnttype(),
				chdrpf.getOccdate());

		if (list == null || list.isEmpty()) {
			syserrrec.params.set(chdrlifIO.getChdrcoy().toString().concat(t5688).concat(chdrpf.getCnttype()));
			syserrrec.statuz.set(errorsInner.e308);
			fatalError600();
		} else {
			t5688rec.t5688Rec.set(StringUtil.rawToString(list.get(0).getGenarea())); // IJTI-462
		}
		wsaaRerateDate.set(varcom.vrcmMaxDate);
		/* Zeroise all the commission totals. */
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaJrnseq.set(0);
		wsaaRnwcpyDue.set(0);
		/* Read T5679 for validation of statii of future COVR's. */
		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itemDao.getItempfRecord(itempf);
		if (itempf == null) {
			syserrrec.params.set(
					chdrlifIO.getChdrcoy().toString().concat("T5679").concat(wsaaBatckey.batcBatctrcde.toString()));
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaNotEnoughCash = "N";
		/* MOVE 1 TO WSAA-FREQUENCY. */
		/* Read T5645 for accounting details. This call will read the 2nd */
		/* page of the table item(this item has more than one page of */
		/* accounting details). This first page will be read in individual */
		/* section. The details read here will be stored away in working */
		/* storage and to be used later on when calling UNITALOC(from */
		/* T5671). */
		readT5645Page34200();
		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq("01");
		itempf = itemDao.getItempfRecordBySeq(itempf);

		if (itempf != null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		} else {
			t5645rec.t5645Rec.set(SPACES);
		}

		wsaaSub1.set(1);
		List<Chdrpf> chdrUpdatList = new ArrayList<>();
		chdrUpdatList.add(updateChdrpf);
		chdrpfDAO.updateInvalidChdrRecord(chdrUpdatList);
		List<Payrpf> payrUpdatList = new ArrayList<>();
		payrUpdatList.add(updatePayrpf);
		payrpfDAO.updatePayrRecord(payrUpdatList, wsaaCurrptdt.toInt());
		readCovrpf();
		covrpfDAO.updateCovrRecord(updateCovrList, wsaaCurrptdt.toInt());
		covrpfDAO.insertCovrpfList(insertCovrList);
		racdpfDAO.updateRacdPendcsttoProrateflagBulk(updateRacdList);
	}

	protected void checkEndOfMonth9000() {
		wsaaEndOfMonth = "N";
		wsaaLeapYear = "N";
		checkLeapYear10000();
		if (isEQ(wsaaLeapYear, "Y")) {
			wsaaDaysInMonth.set(wsaaDaysInMonthLeap);
		} else {
			wsaaDaysInMonth.set(wsaaDaysInMonthNorm);
		}
		if (isGTE(wsaaEomDay, wsaaMonthDays[wsaaEomMonth.toInt()])) {
			wsaaEndOfMonth = "Y";
		}
	}

	protected void checkLeapYear10000() {
		/* CHK-LEAP-YEAR */
		/* If the year is the begining of the century */
		/* Divide the year by 400 and if result is an integer value, */
		/* this indicates that this is a Century leap year. */
		if (isEQ(wsaaCenturyYear, ZERO)) {
			compute(wsaaResult, 0).set(div(wsaaEomYear, 400));
			wsaaResult.multiply(400);
			if (isEQ(wsaaResult, wsaaEomYear)) {
				wsaaLeapYear = "Y";
			}
			return;
		}
		/* Divide year by 4 and if result is an integer value, this */
		/* indicates a yeap year. */
		compute(wsaaResult, 0).set(div(wsaaEomYear, 4));
		wsaaResult.multiply(4);
		if (isEQ(wsaaResult, wsaaEomYear)) {
			wsaaLeapYear = "Y";
		}
		/* EXIT */
	}

	protected void readT5645Page34200() {

		/* If more values are added onto the 3rd screen, then a more */
		/* method should be used. (such as looping). But for now, this */
		/* will suffice. */
		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq("02");
		itempf = itemDao.getItempfRecordBySeq(itempf);

		if (itempf != null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		} else {
			t5645rec.t5645Rec.set(SPACES);
		}

		if (isEQ(t5645rec.sacscode[2], SPACES) && isEQ(t5645rec.sacstype[2], SPACES)
				&& isEQ(t5645rec.glmap[2], SPACES)) {
			return;
		}
		wsaaT5645Cnttot32.set(t5645rec.cnttot[2]);
		wsaaT5645Glmap32.set(t5645rec.glmap[2]);
		wsaaT5645Sacscode32.set(t5645rec.sacscode[2]);
		wsaaT5645Sacstype32.set(t5645rec.sacstype[2]);
	}
	
	protected void covrRiskStatus5110(Covrpf covr) {
		wsaaValidStatus.set("N");
		if (isEQ(covr.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getStatcode(), t5679rec.covRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		} else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getStatcode(), t5679rec.ridRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
	}
	
	protected void covrPremStatus5120(Covrpf covr) {
		wsaaValidStatus.set("N");
		if (isEQ(covr.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getPstatcode(), t5679rec.covPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		} else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getPstatcode(), t5679rec.ridPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
	}

	protected void readCovrpf() {
		Covrpf covr;
		List<Covrpf> covrpflist = covrpfDAO.getCovrsurByComAndNum(chdrlifIO.getChdrcoy().toString(),
				chdrlifIO.getChdrnum().toString());
		if (!covrpflist.isEmpty()) {
			for (Covrpf c : covrpflist) {
				covrRiskStatus5110(c);
				if (statusNotValid.isTrue()) {
					continue;
				}
				/*covrPremStatus5120(c);
				if (statusNotValid.isTrue()) {
					continue;
					//For now it is not required in future if we need it we can do premium status check
				}*/
				covr = new Covrpf();
				covr.setUniqueNumber(c.getUniqueNumber());
				covr.setValidflag("2");
				covr.setCurrto(wsaaCurrptdt.toInt());
				updateCovrList.add(covr);
				c.setTranno(chdrpf.getTranno() + 1);
				c.setCurrfrom(wsaaCurrptdt.toInt());
				insertCovrList.add(c);
				readRacdData(c);
			}
		}
	}

	protected void readRacdData(Covrpf c) {
		List<Racdpf> racdList = racdpfDAO.searchRacdstRecord(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(),
				c.getRider(), c.getPlanSuffix(), "1");
		for (Racdpf racd : racdList) {
			racd.setTranno(wsaaTranno.toInt());
			racd.setPendcstto(sv.advptdat.toInt());
			if(isEQ(sv.prorateflag,"Y"))
				racd.setProrateflag("Y");
			else 
				racd.setProrateflag("N");
			updateRacdList.add(racd);
		}
	}

	protected void readAgcmData(Covrpf c) {
		List<Agcmpf> agcmList = agcmpfDAO.searchAgcmstRecord(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(),
				c.getRider(), c.getPlanSuffix(), "1");
		Agcmpf agcmpf = new Agcmpf();
		for (Agcmpf agcm : agcmList) {
			agcmpf.setUniqueNumber(agcm.getUniqueNumber());
			agcmpf.setValidflag("2");
			agcmpf.setCurrto(wsaaCurrptdt.toInt());
			updateAgcmList.add(agcmpf);
			agcm.setCurrfrom(wsaaCurrptdt.toInt());
			agcm.setTranno(chdrpf.getTranno() + 1);
			agcm.setAnnprem(mult(c.getInstprem(), wsaaNewBillfreq).getbigdata());
			insertAgcmList.add(agcm);
		}
	}

	protected void tableLoop3020() {
		if (isEQ(t5645rec.sacscode[wsaaSub1.toInt()], SPACES) && isEQ(t5645rec.sacstype[wsaaSub1.toInt()], SPACES)
				&& isEQ(t5645rec.glmap[wsaaSub1.toInt()], SPACES)) {
			wsaaSub1.set(16);
			goTo(GotoLabel.loopCheck3040);
		}
		wsaaT5645Cnttot[wsaaSub1.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
		wsaaT5645Glmap[wsaaSub1.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
		wsaaT5645Sacscode[wsaaSub1.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
		wsaaT5645Sacstype[wsaaSub1.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
		wsaaT5645Sign[wsaaSub1.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		wsaaSub1.add(1);
	}

	protected void loopCheck3040() {
		if (isLT(wsaaSub1, 16)) {
			goTo(GotoLabel.tableLoop3020);
		}
	}

	protected void transDescription3050() {
		/* <V4L001> */
		/* Get transaction description. Clone from 2300-COLLECTION section. */
		/* <V4L001> */
		Descpf descpf = descDAO.getdescData(IT, "T1688", wsaaBatckey.batcBatctrcde.toString(),
				chdrlifIO.getChdrcoy().toString(), wsspcomn.language.toString());
		if (descpf == null) {
			descpf = new Descpf();
			descpf.setLongdesc("");
		}
		wsaaTransDesc.set(descpf.getLongdesc());
	}

	protected void updateApa3060() {
		/* <V4L001> */
		/* This paragraph is required before the 2200-BILLING so that */
		/* suspense will have enough money for the amount due. If there */
		/* has extract suspense for the amount due then NEXT SENTENCE. If */
		/* Adv Prem Deposit amount (APA) is required to pay the amount due */
		/* then move DELT to withdraw money from APA and put into suspense. */
		/* Otherwise, take the extra money from suspense and try to post it */
		/* into APA. <V4L001> */
		/* <V4L001> */
		initialize(rlpdlonrec.rec);
		if (isEQ(wsaaApaRequired, ZERO)) {
			/* NEXT_SENTENCE */
		} else {
			if (isGT(wsaaApaRequired, ZERO)) {
				rlpdlonrec.function.set(Varcom.delt);
				rlpdlonrec.prmdepst.set(wsaaApaRequired);
			} else {
				rlpdlonrec.function.set(Varcom.insr);
				compute(wsaaApaRequired, 2).set(mult(wsaaApaRequired, (-1)));
				rlpdlonrec.prmdepst.set(wsaaApaRequired);
			}
			updateApa1100();
		}
	}

	protected void updateApa1100() {
		rlpdlonrec.pstw.set("PSTW");
		rlpdlonrec.chdrcoy.set(chdrpf.getChdrcoy());
		rlpdlonrec.chdrnum.set(chdrpf.getChdrnum());
		wsaaRdockey.rdocRdocpfx.set("CH");
		wsaaRdockey.rdocRdoccoy.set(chdrpf.getChdrcoy());
		wsaaRdockey.rdocRdocnum.set(chdrpf.getChdrnum());
		wsaaRdockey.rdocTranseq.set(wsaaJrnseq);
		rlpdlonrec.doctkey.set(wsaaRdockey);
		rlpdlonrec.effdate.set(chdrpf.getOccdate());
		rlpdlonrec.currency.set(payrpf.getBillcurr());
		rlpdlonrec.tranno.set(wsaaTranno);
		rlpdlonrec.transeq.set(wsaaJrnseq);
		rlpdlonrec.longdesc.set(wsaaTransDesc);
		rlpdlonrec.language.set(wsspcomn.language);
		rlpdlonrec.batchkey.set(wsspcomn.batchkey);
		rlpdlonrec.authCode.set(wsaaBatckey.batcBatctrcde);
		rlpdlonrec.time.set(varcom.vrcmTime);
		rlpdlonrec.date_var.set(varcom.vrcmDate);
		rlpdlonrec.user.set(varcom.vrcmUser);
		rlpdlonrec.termid.set(wsaaTermid);
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if (isNE(rlpdlonrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(rlpdlonrec.statuz);
			syserrrec.params.set(rlpdlonrec.rec);
			fatalError600();
		}
		wsaaJrnseq.set(rlpdlonrec.transeq);
	}

	protected void checkAgentTerminate4100() {

		/* Retrieve Today's date. */
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrpf.getAgntcoy());
		aglfIO.setAgntnum(chdrpf.getAgntnum());
		aglfIO.setFunction(Varcom.readr);
		aglfIO.setFormat(formatsInner.aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), Varcom.oK) && isNE(aglfIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			fatalError600();
		}
		wsaaAgtTerminatedFlag = "N";
		if (isLT(aglfIO.getDtetrm(), datcon1rec.intDate) || isLT(aglfIO.getDteexp(), datcon1rec.intDate)
				|| isGT(aglfIO.getDteapp(), datcon1rec.intDate)) {
			wsaaAgtTerminatedFlag = "Y";
		}
	}
	protected void billing2200() {
		/* Read T5645 for suspense balance. */
		itemIO.setParams(SPACES);
		itemIO.setItempfx(IT);
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T3695 to check the sign. */
		itemIO.setItempfx(IT);
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/* MOVE CHDRLIF-BTDATE TO WSAA-BTDATE. */
		wsaaBtdate.set(payrpf.getBtdate());
		wsaaBillcd.set(payrpf.getBillcd());
		wsaaNextdt.set(payrpf.getNextdate());
		/* Read ACBL */
		acblIO.setParams(SPACES);
		acblIO.setRldgacct(chdrpf.getChdrnum());
		acblIO.setRldgcoy(chdrpf.getChdrcoy());
		/* MOVE CHDRLIF-CNTCURR TO ACBL-ORIGCURR. <022> */
		acblIO.setOrigcurr(chdrpf.getBillcurr());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(Varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(), Varcom.oK)) && (isNE(acblIO.getStatuz(), Varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		/* Multiply the balance by the sign from T3695. */
		if (isEQ(acblIO.getStatuz(), Varcom.mrnf)) {
			wsaaSuspAvail.set(0);
		} else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspAvail, 2).set(mult(acblIO.getSacscurbal(), -1));
			} else {
				wsaaSuspAvail.set(acblIO.getSacscurbal());
			}
		}
		/* Create a LINS record for each new payment. */
		produceLinsRecord2210();
		
		if (isLT(wsaaSuspAvail, wsaaNetCbillamt)) {
			/* Read the latest premium tolerance allowed. */
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(chdrlifIO.getChdrcoy());
			itemIO.setItemtabl("T5667");
			wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde.toString());
			wsbbT5667Curr.set(payrpf.getBillcurr());
			itemIO.setItemitem(wsbbT5667Key);
			itemIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
				t5667rec.t5667Rec.set(SPACES);
			} else {
				t5667rec.t5667Rec.set(itemIO.getGenarea());
			}
			/*-Check the tolerance amount against T5667 table entry read       */
			/* above. */
			for (wsaaSub.set(1); true; wsaaSub.add(1)) {
				if (isEQ(payrpf.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
					compute(wsaaTol, 3).setRounded(div((mult(wsaaNetCbillamt, t5667rec.prmtol[wsaaSub.toInt()])), 100));
					zrdecplrec.amountIn.set(wsaaTol);
					c000CallRounding();
					wsaaTol.set(zrdecplrec.amountOut);
					if (isGT(wsaaTol, t5667rec.maxAmount[wsaaSub.toInt()])) {
						wsaaTol.set(t5667rec.maxAmount[wsaaSub.toInt()]);
					}
					wsaaTol2.set(0);
					if (isGT(t5667rec.maxamt[wsaaSub.toInt()], 0) && (isEQ(wsaaAgtTerminatedFlag, "N")
							|| (isEQ(wsaaAgtTerminatedFlag, "Y") && isEQ(t5667rec.sfind, "2")))) {
						compute(wsaaTol2, 3).setRounded(div((mult(wsaaNetCbillamt, t5667rec.prmtoln[wsaaSub.toInt()])), 100));
						zrdecplrec.amountIn.set(wsaaTol2);
						c000CallRounding();
						wsaaTol2.set(zrdecplrec.amountOut);
						if (isGT(wsaaTol2, t5667rec.maxamt[wsaaSub.toInt()])) {
							wsaaTol2.set(t5667rec.maxamt[wsaaSub.toInt()]);
						}
					}
				}
				if (isEQ(wsaaSub, 11)) {
					break;
				}
			}
			if ((setPrecision(wsaaSuspAvail, 2) && isLT(wsaaSuspAvail, (sub(wsaaNetCbillamt, wsaaTol))))) {
				if ((setPrecision(wsaaSuspAvail, 2) && isLT(wsaaSuspAvail, (sub(wsaaNetCbillamt, wsaaTol2))))) {
					wsaaNotEnoughCash = "Y";
				} else {
					setPrecision(linsrnlIO.getInstamt03(), 2);
					linsrnlIO.setInstamt03(sub(wsaaNetCbillamt, wsaaSuspAvail));
					wsaaToleranceFlag = "2";
				}
			} else {
				setPrecision(linsrnlIO.getInstamt03(), 2);
				linsrnlIO.setInstamt03(sub(wsaaNetCbillamt, wsaaSuspAvail));
				wsaaToleranceFlag = "1";
			}
		}
		
		
	}

	protected void produceLinsRecord2210() {		
					produceLinsRecord2211();
					otherFields2215();				
	}

	protected void produceLinsRecord2211() {		
	
		payrpf.setBtdate(sv.advptdat.toInt());
		chdrpf.setBtdate(sv.advptdat.toInt());
		
		if(isEQ(payrIO.getBillday(),SPACES)) {
			payrpf.setBillcd(this.sv.advptdat.toInt());
			chdrpf.setBillcd(this.sv.advptdat.toInt());
			
			if(isNE(payrpf.getBillday(),SPACES)) {
				getCollectiondate(sv.advptdat.toString(), payrpf.getBillday());
				payrpf.setOrgbillcd(datcon2rec.intDate2.toInt());
				}else {
					payrpf.setOrgbillcd(sv.advptdat.toInt());
				}
			
		}
		else {
			getCollectiondate(sv.advptdat.toString(), payrIO.getBillday().toString());
			payrpf.setBillcd(datcon2rec.intDate2.toInt());
			chdrpf.setBillcd(datcon2rec.intDate2.toInt());
			if(isNE(payrpf.getBillday(),SPACES)) {
				getCollectiondate(sv.advptdat.toString(), payrpf.getBillday());
				payrpf.setOrgbillcd(datcon2rec.intDate2.toInt());
				}else {
					payrpf.setOrgbillcd(sv.advptdat.toInt());
				}
		}
		payrpf.setBillday(payrIO.getBillday().toString());
		payrpf.setBillmonth(String.valueOf(payrpf.getBillcd()).substring(4, 6));
		
		calculateLeadDays();
		
		/* Subtract the lead days from the billed to date to calculate*/
		/* the next billing extract date for the PAYR record.*/
		compute(datcon2rec.freqFactor, 0).set((sub(ZERO, intT6654Leaddays)));
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(payrpf.getBillcd());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		
		payrpf.setNextdate(datcon2rec.intDate2.toInt()); // Need to check
		/* Store the waiver amount and the contract fee and calculate */

		/* IF CHDRLIF-BTDATE > CHDRLIF-SINSTTO */
		/* MOVE T5679-SET-CN-PREM-STAT TO CHDRLIF-PSTATCODE */
		/* MOVE WSAA-BTDATE TO CHDRLIF-BTDATE */
		/* GO TO 2219-EXIT. */
		/* <018> */
		/* IF PAYR-BTDATE > CHDRLIF-SINSTTO <018> */
		/* MOVE T5679-SET-CN-PREM-STAT TO PAYR-PSTATCODE <018> */
		/* MOVE WSAA-BTDATE TO PAYR-BTDATE <018> */
		/* GO TO 2219-EXIT. <018> */
		/* <018> */
		/* Look up the tax relief subroutine on T6687. */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(IT);
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl("T6687");
		itemIO.setItemitem(t5688rec.taxrelmth);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.oK)) {
			t6687rec.t6687Rec.set(itemIO.getGenarea());
		} else {
			t6687rec.t6687Rec.set(SPACES);
		}

		compute(wsaaNetBillamt, 2).set(sv.instPrem);

		/* Convert the net bill amount if the contract currency */
		/* is not the same as billing currency. */
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.currIn.set(payrpf.getCntcurr());
		conlinkrec.currOut.set(payrpf.getBillcurr());
		conlinkrec.amountIn.set(wsaaNetBillamt);
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		/* MOVE WSAA-TRANSACTION-DATE TO CLNK-CASHDATE. */
		conlinkrec.cashdate.set(datcon1rec.intDate);
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(chdrlifIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			conlinkrec.amountOut.set(0);
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		c000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		if (isEQ(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			wsaaNetCbillamt.set(wsaaNetBillamt);
		} else {
			wsaaNetCbillamt.set(conlinkrec.amountOut);
		}
		/* Set up LINS record.(LINS holds instalment amounts in contract */
		/* currency and the total instalment in billing currency.) */

		linsrnlIO.setParams(SPACES);
		linsrnlIO.setBillcd(ZERO);
		linsrnlIO.setInstamt01(wsaaOutstPremWithStpdty);
		linsrnlIO.setInstamt02(mgfeelrec.mgfee);
		linsrnlIO.setInstamt03(payrpf.getSinstamt03());
		linsrnlIO.setInstamt04(payrpf.getSinstamt04());
		linsrnlIO.setInstamt05(payrpf.getSinstamt05());
		linsrnlIO.setInstamt06(add(linsrnlIO.getInstamt01(), linsrnlIO.getInstamt02(), linsrnlIO.getInstamt03(),
				linsrnlIO.getInstamt04(), linsrnlIO.getInstamt05()));
		/* Convert contract amount(total) to billing amount if contract */
		/* currency is not the same as billing currency. */
		/* IF CHDRLIF-CNTCURR = CHDRLIF-BILLCURR */
		if (isEQ(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			linsrnlIO.setCbillamt(linsrnlIO.getInstamt06());
			return;
		}
		conlinkrec.clnk002Rec.set(SPACES);
		/* MOVE CHDRLIF-CNTCURR TO CLNK-CURR-IN. */
		/* MOVE CHDRLIF-BILLCURR TO CLNK-CURR-OUT. */
		conlinkrec.currIn.set(payrpf.getCntcurr());
		conlinkrec.currOut.set(payrpf.getBillcurr());
		conlinkrec.amountIn.set(linsrnlIO.getInstamt06());
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		/* MOVE WSAA-TRANSACTION-DATE TO CLNK-CASHDATE. */
		conlinkrec.cashdate.set(datcon1rec.intDate);
		/* MOVE 'CVRT' TO CLNK-FUNCTION */
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(chdrlifIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			conlinkrec.amountOut.set(0);
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		c000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		linsrnlIO.setCbillamt(conlinkrec.amountOut);
	}
	protected void calculateLeadDays() {
		FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
		FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
		FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);
		
		boolean BTPRO028Permission  = FeaConfg.isFeatureExist("2", "BTPRO028", appVars, IT);
		if(BTPRO028Permission) {
			String key= payrpf.getBillchnl().trim()+chdrpf.getCnttype().trim()+payrpf.getBillfreq().trim();
			if(!readT6654(key)) {
				key= payrpf.getBillchnl().trim().concat(chdrpf.getCnttype().trim()).concat("**");
				if(!readT6654(key)) {
					key= payrpf.getBillchnl().trim().concat("*****");
					if(!readT6654(key)) {
						syserrrec.params.set(t6654);
						syserrrec.statuz.set(Varcom.mrnf);
						fatalError600();
					}
				}
			}
		}
		else {
			wsaaT6654Item.set(SPACES);
			wsaaT6654Billchnl.set(payrpf.getBillchnl());
			wsaaT6654Cnttype.set(chdrpf.getCnttype());
			List<Itempf> itemList = itemDao.getAllitemsbyCurrency(IT,payrpf.getChdrcoy(),t6654,wsaaT6654Item.toString());
			if (itemList == null || itemList.isEmpty()) {
				syserrrec.params.set(wsaaBatckey.batcBatctrcde);
				syserrrec.statuz.set(Varcom.mrnf);
				fatalError600();
			}
		
			if (itemList != null && !itemList.isEmpty()) {
				t6654rec.t6654Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
				return ;
			}
			wsaaT6654Cnttype.set("***");
			itemList = itemDao.getAllitemsbyCurrency(IT,payrpf.getChdrcoy(),t6654,wsaaT6654Item.toString());
			if (itemList == null || itemList.isEmpty()) {
				syserrrec.params.set(t6654);
				syserrrec.statuz.set(Varcom.mrnf);
				fatalError600();
				return;
			}
			t6654rec.t6654Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		}
		intT6654Leaddays = t6654rec.leadDays.toInt();
	}

	protected boolean readT6654(String key) {
		Itempf item = new Itempf();

		item.setItempfx(IT);
		item.setItemcoy(payrpf.getChdrcoy());
		item.setItemtabl(t6654);
		item.setItemitem(key);
		item = itemDao.getItempfRecord(item);
		if (null != item) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(item.getGenarea()));
			return true;
		}
		return false;
	}
	protected void c000CallRounding() {
		/* C100-CALL */
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(chdrlifIO.getChdrcoy());
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.currency.set(chdrpf.getBillcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/* C900-EXIT */
	}

	protected void otherFields2215() {
		linsrnlIO.setChdrnum(chdrpf.getChdrnum());
		linsrnlIO.setChdrcoy(chdrpf.getChdrcoy());
		linsrnlIO.setBranch(wsaaBatckey.batcBatcbrn);
		linsrnlIO.setInstfrom(wsaaBtdate);
		linsrnlIO.setCntcurr(payrpf.getCntcurr());
		linsrnlIO.setBillcurr(payrpf.getBillcurr());
		linsrnlIO.setInstto(sv.advptdat);
		linsrnlIO.setInstfreq(payrpf.getBillfreq());
		linsrnlIO.setPayflag("O");
		linsrnlIO.setTranscode(wsaaBatckey.batcBatctrcde);
		linsrnlIO.setInstjctl("");
		linsrnlIO.setValidflag("1");
		linsrnlIO.setBillchnl(payrpf.getBillchnl());
		linsrnlIO.setPayrseqno(payrpf.getPayrseqno());
		if (isNE(prasrec.taxrelamt, 0)) {
			linsrnlIO.setTaxrelmth(t5688rec.taxrelmth);
		}
		linsrnlIO.setBillcd(sv.billcd);
		linsrnlIO.setAcctmeth(chdrpf.getAcctmeth());
		linsrnlIO.setProraterec("Y");
	}

	protected void callPmexSubroutine(Covrpf covrpf) {
		premiumrec.premiumRec.set(SPACES);
		initialize(premiumrec.premiumRec);
		premiumrec.validflag.set("Y"); //PINNACLE-3060
		premiumrec.updateRequired.set("N");
		if (proratPremCalcFlag) {
			premiumrec.proratPremCalcFlag.set("Y");
		} else {
			premiumrec.proratPremCalcFlag.set("N");
		}
		if (isFirstCovr) {
			premiumrec.firstCovr.set("Y");
		} else {
			premiumrec.firstCovr.set("N");
			premiumrec.lifeLife.set(covrpf.getLife());
			premiumrec.covrCoverage.set(covrpf.getCoverage());
			premiumrec.covrRider.set(covrpf.getRider());
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			if (isNE(premiumrec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			return;
		}
		premiumrec.batcBatctrcde.set(wsaaBatckey.batcBatctrcde);
		premiumrec.riskPrem.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		if (isProcesFreqChange) {
			premiumrec.billfreq.set(wsaaNewBillfreq);
		} else {
			premiumrec.billfreq.set(payrpf.getBillfreq());
			premiumrec.bilfrmdt.set(chdrlifIO.getPtdate());
			premiumrec.biltodt.set(sv.advptdat);
		}
		readTables(covrpf);
		if (AppVars.getInstance().getAppConfig().isVpmsEnable()) {
			premiumrec.premMethod.set(t5687rec.premmeth);
		}
		if (premiumrec.premMethod.toString().trim().equals("PMEX")) {
			premiumrec.setPmexCall.set("Y");
			premiumrec.cownnum.set(chdrlifIO.getCownnum());
			premiumrec.occdate.set(chdrlifIO.getOccdate());
		}
		premiumrec.chdrChdrcoy.set(chdrlifIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(chdrlifIO.getChdrnum());
		premiumrec.cnttype.set(chdrlifIO.getCnttype());
		premiumrec.effectdt.set(covrpf.getCrrcd());
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			traceLastRerateDate5500(covrpf);
			premiumrec.effectdt.set(wsaaLastRerateDate);
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
		} else {
			premiumrec.ratingdate.set(covrpf.getCrrcd());
			premiumrec.reRateDate.set(covrpf.getCrrcd());
		}
		if (rerateChgFlag) { // to get the rerated premiums
			premiumrec.effectdt.set(wsaaRerateEffDate);
			premiumrec.ratingdate.set(wsaaRerateEffDate);
			premiumrec.reRateDate.set(wsaaRerateEffDate);
		}
		premiumrec.mop.set(payrpf.getBillchnl());
		premiumrec.commissionPrem.set(ZERO);
		premiumrec.zstpduty01.set(ZERO);
		premiumrec.zstpduty02.set(ZERO);
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.lifeLife.set(covrpf.getLife());
		if (("PMEX".equals(t5675rec.premsubr.toString().trim())) && (lnkgFlag)) {
			if (null == covrpf.getLnkgsubrefno() || covrpf.getLnkgsubrefno().trim().isEmpty()) {
				premiumrec.lnkgSubRefNo.set(SPACE);
			} else {
				premiumrec.lnkgSubRefNo.set(covrpf.getLnkgsubrefno().trim());
			}
			if (null == covrpf.getLnkgno() || covrpf.getLnkgno().trim().isEmpty()) {
				premiumrec.linkcov.set(SPACE);
			} else {
				LinkageInfoService linkgService = new LinkageInfoService();
				FixedLengthStringData linkgCov = new FixedLengthStringData(
						linkgService.getLinkageInfo(covrpf.getLnkgno().trim()));
				premiumrec.linkcov.set(linkgCov);
			}
		}
		premiumrec.lifeJlife.set(covrpf.getJlife());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.covrRider.set(covrpf.getRider());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		Lifepf lifepf = lifepfDAO.getLifeEnqRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
		premiumrec.lsex.set(lifepf.getCltsex());
		Lifepf jlife = lifepfDAO.getLifelnbRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			calculateAnb3260(lifepf);
			premiumrec.lage.set(wsaaAnb);
			if (jlife != null) {
				calculateAnb3260(jlife);
				premiumrec.jlage.set(wsaaAnb);
				premiumrec.jlsex.set(jlife.getCltsex());
			} else {
				premiumrec.jlsex.set(SPACES);
				premiumrec.jlage.set(ZERO);
			}
		} else {
			premiumrec.lage.set(covrpf.getAnbAtCcd());
			if (jlife != null) {
				premiumrec.jlsex.set(jlife.getCltsex());
				premiumrec.jlage.set(jlife.getAnbAtCcd());
			} else {
				premiumrec.jlsex.set(SPACES);
				premiumrec.jlage.set(ZERO);
			}
		}
		Clntpf clt = clntpfDAO.searchClntRecord("CN", wsaaFsuCoy.toString(), lifepf.getLifcnum());
		if (stampDutyflag && clt.getClntStateCd() != null) {
			premiumrec.rstate01.set(clt.getClntStateCd().substring(3).trim());
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(covrpf.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.riskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(covrpf.getPremCurrency());
		if (isEQ(covrpf.getPlanSuffix(), ZERO) && isNE(chdrlifIO.getPolinc(), 1)) {
			compute(premiumrec.sumin, 3).setRounded(div(covrpf.getSumins(), chdrlifIO.getPolsum()));
		} else {
			premiumrec.sumin.set(covrpf.getSumins());
		}
		compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin, chdrlifIO.getPolinc()));
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.calcPrem.set(covrpf.getInstprem());
		premiumrec.calcBasPrem.set(covrpf.getZbinstprem());
		premiumrec.calcLoaPrem.set(covrpf.getZlinstprem());
		getAnny3280(covrpf);
		premiumrec.commTaxInd.set("Y");
		premiumrec.prevSumIns.set(ZERO);
		premiumrec.inputPrevPrem.set(ZERO);
		com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao = DAOFactory.getClntpfDAO();
		com.csc.smart400framework.dataaccess.model.Clntpf clnt = clntDao.getClientByClntnum(lifepf.getLifcnum());
		if (null != clnt && null != clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()) {
			premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
		} else {
			premiumrec.stateAtIncep.set(clt.getClntStateCd());
		}
		premiumrec.validind.set("2");
		getRcvdpf(covrpf);
		callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		if (isNE(premiumrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
	}

	protected void traceLastRerateDate5500(Covrpf covr) {
		if (billChgFlag) {
			start5510ForBilltmpChg(covr);
		} else {
			start5510(covr);
		}
	}

	protected void start5510(Covrpf covr) {
		wsaaLastRerateDate.set(covr.getRerateDate());
		datcon4rec.billday.set(payrpf.getDuedd());
		datcon4rec.billmonth.set(payrpf.getDuemm());
		compute(datcon4rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));
		datcon4rec.frequency.set(t5687rec.rtrnwfreq);
		while (!(isLTE(wsaaLastRerateDate, sv.ptdate))) {
			datcon4rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, Varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastRerateDate.set(datcon4rec.intDate2);
		}

		if (isLT(wsaaLastRerateDate, covr.getCrrcd())) {
			wsaaLastRerateDate.set(covr.getCrrcd());
		}
	}

	protected void start5510ForBilltmpChg(Covrpf covr) {
		wsaaLastRerateDate.set(covr.getRerateDate());

		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.frequency.set(t5687rec.rtrnwfreq);
		compute(datcon2rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));

		while (!(isLTE(wsaaLastRerateDate, sv.ptdate))) {
			datcon2rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, Varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			wsaaLastRerateDate.set(datcon2rec.intDate2);
		}

		if (isLT(wsaaLastRerateDate, covr.getCrrcd())) {
			wsaaLastRerateDate.set(covr.getCrrcd());
		}
	}

	protected void processLinsRecord2302() {
		linsrnlIO.setFunction(Varcom.writr);
		linsrnlIO.setFormat(formatsInner.linsrnlrec);
		SmartFileCode.execute(appVars, linsrnlIO);
		if ((isNE(linsrnlIO.getStatuz(), Varcom.oK))) {
			syserrrec.params.set(linsrnlIO.getParams());
			fatalError600();
		}
	}
	
	protected void produceBextRecord(){
		Mandpf mandpf = mandpfDAO.searchMandpfRecordData(clrfpf.getClntcoy(),clrfpf.getClntnum(),payrpf.getMandref(),"VM1DTA.MANDPF");
		if(mandpf!=null) {
			Clbapf clbapf = clbapfDAO.searchClbapfRecordData(mandpf.getBankkey(), mandpf.getBankacckey(), 
					chdrpf.getCowncoy().toString(), chdrpf.getCownnum(),chdrpf.getCownpfx());
			billreqrec.bankkey.set(mandpf.getBankkey());
			billreqrec.bankacckey.set(mandpf.getBankacckey());
			billreqrec.facthous.set(clbapf.getFacthous());
			billreqrec.mandstat.set(mandpf.getMandstat());
		} else {
			billreqrec.bankkey.set(SPACES);
			billreqrec.bankacckey.set(SPACES);
			billreqrec.facthous.set(SPACES);
			billreqrec.mandstat.set(SPACES);
		}
		callBillreq();
	}
	
	protected void callBillreq(){
		/* Search the T3629 array to obtain the required bank code.*/
		readT3629();
		billreqrec.bankcode.set(strT3629BankCode);
		billreqrec.user.set(0);
		billreqrec.contot01.set(0);
		billreqrec.contot02.set(0);
		billreqrec.instjctl.set(SPACES);
		billreqrec.payflag.set(SPACES);
		billreqrec.bilflag.set(SPACES);
		billreqrec.outflag.set(SPACES);
		billreqrec.supflag.set("N");
		billreqrec.company.set(wsspcomn.company);
		billreqrec.branch.set(wsaaBatckey.batcBatcbrn);
		billreqrec.language.set(wsspcomn.language);
		billreqrec.effdate.set(wsaaUnitEffdate);
		billreqrec.acctyear.set(wsaaBatckey.batcBatcactyr);
		billreqrec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		billreqrec.trancode.set(wsaaBatckey.batcBatctrcde);
		billreqrec.batch.set(wsaaBatckey.batcBatcbatch);
		billreqrec.fsuco.set(wsaaFsuCoy);
		billreqrec.modeInd.set("BATCH");
		billreqrec.termid.set(SPACES);
		billreqrec.time.set(ZERO);
		billreqrec.date_var.set(ZERO);
		billreqrec.tranno.set(wsaaTranno);
		billreqrec.chdrpfx.set(chdrpf.getChdrpfx());
		billreqrec.chdrcoy.set(payrpf.getChdrcoy());
		billreqrec.chdrnum.set(payrpf.getChdrnum());
		billreqrec.servunit.set(chdrpf.getServunit());
		billreqrec.cnttype.set(chdrpf.getCnttype());
		billreqrec.occdate.set(chdrpf.getOccdate());
		billreqrec.ccdate.set(chdrpf.getCcdate());
		billreqrec.instcchnl.set(chdrpf.getCollchnl());
		billreqrec.cownpfx.set(chdrpf.getCownpfx());
		billreqrec.cowncoy.set(chdrpf.getCowncoy());
		billreqrec.cownnum.set(chdrpf.getCownnum());
		billreqrec.cntbranch.set(chdrpf.getCntbranch());
		billreqrec.agntpfx.set(chdrpf.getAgntpfx());
		billreqrec.agntcoy.set(chdrpf.getAgntcoy());
		billreqrec.agntnum.set(chdrpf.getAgntnum());
		billreqrec.cntcurr.set(payrpf.getCntcurr());
		billreqrec.billcurr.set(payrpf.getBillcurr());
		billreqrec.ptdate.set(payrpf.getPtdate());
		billreqrec.instto.set(sv.advptdat);
		billreqrec.instbchnl.set(chdrpf.getBillchnl());
		billreqrec.billchnl.set(chdrpf.getBillchnl());
		billreqrec.instfreq.set(payrpf.getBillfreq());//Need to check
		billreqrec.grpscoy.set(payrpf.getGrupcoy());
		billreqrec.grpsnum.set(payrpf.getGrupnum());
		billreqrec.membsel.set(payrpf.getMembsel());
		billreqrec.mandref.set(payrpf.getMandref());
		billreqrec.nextdate.set(payrpf.getNextdate());
		billreqrec.payrpfx.set(clrrpf.getClntpfx());
		billreqrec.payrcoy.set(clrrpf.getClntcoy());
		billreqrec.payrnum.set(clrrpf.getClntnum());
		
		billreqrec.instfrom.set(sv.btdate);
		billreqrec.btdate.set(sv.btdate);
		billreqrec.duedate.set(sv.btdate);
		billreqrec.billcd.set(sv.billcd);   //PINNACLE-3019
		billreqrec.nextdate.set(wsaaNextdt);
		billreqrec.billdate.set(sv.billcd);
		
		billreqrec.sacscode01.set(t5645rec.sacscode01);
		billreqrec.sacstype01.set(t5645rec.sacstype01);
		billreqrec.glmap01.set(t5645rec.glmap01);
		billreqrec.glsign01.set(t5645rec.sign01);
		
		billreqrec.sacscode02.set(t5645rec.sacscode02);
		billreqrec.sacstype02.set(t5645rec.sacstype02);
		billreqrec.glmap02.set(t5645rec.glmap02);
		billreqrec.glsign02.set(t5645rec.sign02);
		/* If BILLREQ1 needs to know the PAYER NAME then it will be looked*/
		/* up using the PAYRPFX*/
		billreqrec.payername.set(SPACES);
		
		if (isNE(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			currNotEqualBillcurr(linsrnlIO.getInstamt01());
			billreqrec.instamt01.set(conlinkrec.amountOut);
			currNotEqualBillcurr(linsrnlIO.getInstamt02());
			billreqrec.instamt02.set(conlinkrec.amountOut);
			currNotEqualBillcurr(linsrnlIO.getInstamt03());
			billreqrec.instamt03.set(conlinkrec.amountOut);
			currNotEqualBillcurr(linsrnlIO.getInstamt04());
			billreqrec.instamt04.set(conlinkrec.amountOut);
			currNotEqualBillcurr(linsrnlIO.getInstamt05());
			billreqrec.instamt05.set(conlinkrec.amountOut);
		} else {
			billreqrec.instamt01.set(linsrnlIO.getInstamt01());
			billreqrec.instamt02.set(linsrnlIO.getInstamt02());
			billreqrec.instamt03.set(linsrnlIO.getInstamt03());
			billreqrec.instamt04.set(linsrnlIO.getInstamt04());
			billreqrec.instamt05.set(linsrnlIO.getInstamt05());
		}
		billreqrec.instamt06.set(linsrnlIO.getInstamt06());
		billreqrec.instamt07.set(ZERO);
		billreqrec.instamt08.set(ZERO);
		billreqrec.instamt09.set(ZERO);
		billreqrec.instamt10.set(ZERO);
		billreqrec.instamt11.set(ZERO);
		billreqrec.instamt12.set(ZERO);
		billreqrec.instamt13.set(ZERO);
		billreqrec.instamt14.set(ZERO);
		billreqrec.instamt15.set(ZERO);
		
		callProgram(Billreq1.class, billreqrec.billreqRec);
		if (isNE(billreqrec.statuz, Varcom.oK)) {
			syserrrec.params.set(billreqrec.billreqRec);
			syserrrec.statuz.set(billreqrec.statuz);
			fatalError600();
		}
	}
	
	protected void readT3629() {
		String keyItemitem = payrpf.getBillcurr().trim();
		boolean itemFound = false;
		Map<String, List<Itempf>> t3629ListMap = itemDao.loadSmartTable(IT, wsspcomn.company.toString(), "T3629");
		if (t3629ListMap.containsKey(keyItemitem)) {	
			List<Itempf> itempfList = t3629ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			String strEffDate = wsaaToday.toString();
			while (iterator.hasNext()) {
				Itempf item = iterator.next();
				if(Integer.parseInt(item.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(item.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(item.getItmto().toString())){
						t3629rec.t3629Rec.set(StringUtil.rawToString(item.getGenarea()));
						strT3629BankCode  = t3629rec.bankcode.toString();
						itemFound = true;
					}
				} else {
					t3629rec.t3629Rec.set(StringUtil.rawToString(item.getGenarea()));
					strT3629BankCode  = t3629rec.bankcode.toString();
					itemFound = true;					
				}				
			}		
		}	
		if (!itemFound) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("t3629");
			stringVariable1.addExpression(payrpf.getBillcurr());
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();		
		}
	}
	
	protected void currNotEqualBillcurr(PackedDecimalData instamt) {
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.currIn.set(payrpf.getCntcurr());
		conlinkrec.currOut.set(payrpf.getBillcurr());
		conlinkrec.amountIn.set(instamt);
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		conlinkrec.cashdate.set(datcon1rec.intDate);
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(chdrlifIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			conlinkrec.amountOut.set(0);
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		c000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

	protected void procesFreqChange() {
		List<Rertpf> updateRertList = new ArrayList<>();
		List<Rertpf> insertRertList = new ArrayList<>();
		List<Rertpf> insertRertList1 = new ArrayList<>();
		isProcesFreqChange = true;
		isFirstCovr = true;
		if (!updateCovrList.isEmpty()) {
			updateCovrList.clear();
		}
		List<Covrpf> covrpflist = covrpfDAO.getCovrmjaByComAndNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (!covrpflist.isEmpty()) {
			for (int i = 0; i < covrpflist.size(); i++) {
				Covrpf c = covrpflist.get(i);
				covrRiskStatus5110(c);
				if (statusNotValid.isTrue()) {
					continue;
				}
				/*covrPremStatus5120(c);
				if (statusNotValid.isTrue()) {
					continue;
					//For now it is not required in future if we need it we can do premium status check
				}*/
				callPmexSubroutine(c);
				if (stampDutyflag) {
					compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
					c.setZstpduty01(premiumrec.zstpduty01.getbigdata());
				}
				c.setInstprem(premiumrec.calcPrem.getbigdata());
				c.setZbinstprem(premiumrec.calcBasPrem.getbigdata());
				c.setZlinstprem(premiumrec.calcLoaPrem.getbigdata());
				c.setCommprem(premiumrec.commissionPrem.getbigdata());
				updateCovrList.add(c);
				readAgcmData(c);
				if (!rertList.isEmpty()) {
					for (Rertpf r : rertList) {
						if (r.getLife().equals(c.getLife()) && r.getCoverage().equals(c.getCoverage()) && r.getRider().equals(c.getRider())) {
							Rertpf rert = new Rertpf(r);
							rert.setValidflag("2");
							updateRertList.add(rert);
							if (stampDutyflag) {
								r.setLastzstpduty(premiumrec.zstpduty01.getbigdata());
							}
							r.setLastInst(premiumrec.calcPrem.getbigdata());
							r.setZblastinst(premiumrec.calcBasPrem.getbigdata());
							r.setZllastinst(premiumrec.calcBasPrem.getbigdata());
							insertRertList.add(r);
						}
					}
				}
				if (i + 1 < covrpflist.size()) {
					isFirstCovr = false;
				} else {
					isFirstCovr = true;
				}
			}
			isFirstCovr = true;
			if (!insertRertList.isEmpty()) {
				for (int i = 0; i < covrpflist.size(); i++) {
				Covrpf c = covrpflist.get(i);
					for (Rertpf r : insertRertList) {
						if (r.getLife().equals(c.getLife()) && r.getCoverage().equals(c.getCoverage()) && r.getRider().equals(c.getRider())) {
							rerateChgFlag = true;
							wsaaRerateEffDate.set(r.getEffdate());
							callPmexSubroutine(c);
							r.setTranno(wsaaTranno.toInt());
							if (stampDutyflag) {
								r.setNewzstpduty(premiumrec.zstpduty01.getbigdata());
							}
							r.setNewinst(premiumrec.calcPrem.getbigdata());
							r.setZbnewinst(premiumrec.calcBasPrem.getbigdata());
							r.setZlnewinst(premiumrec.calcLoaPrem.getbigdata());
							r.setCommprem(premiumrec.commissionPrem.getbigdata());
							insertRertList1.add(r);
							rerateChgFlag = false;
						}
					}
					if (i + 1 < covrpflist.size()) {
						isFirstCovr = false;
					} else {
						isFirstCovr = true;
					}
				}
			}
		}
		
		chdrpf.setTranno(wsaaTranno.toInt());
		chdrpf.setCurrfrom(wsaaCurrptdt.toInt());
		chdrpf.setBillfreq(wsaaNewBillfreq.toString());
		// if applicable we need to add contract fee, tolerance and others
		chdrpf.setOutstamt(wsaaOutstPremWithStpdty.getbigdata());
		chdrpf.setSinstfrom(wsaaCurrptdt.toInt());
		chdrpf.setSinstto(varcom.vrcmMaxDate.toInt());
		//Setting new frequency amount changed in billing change previous screen
		chdrpf.setSinstamt01(chdrlifIO.getSinstamt01().getbigdata());
		chdrpf.setSinstamt02(chdrlifIO.getSinstamt02().getbigdata());
		chdrpf.setSinstamt06(chdrlifIO.getSinstamt06().getbigdata());
	
		// if applicable we need to set tolerance and others
		payrpf.setEffdate(sv.advptdat.toInt());
		payrpf.setOutstamt(wsaaOutstPremWithStpdty.getbigdata());
		payrpf.setTranno(wsaaTranno.toInt());
		//Setting new frequency amount changed in billing change previous screen
		payrpf.setBillfreq(payrIO.getBillfreq().toString());
		payrpf.setValidflag("1");
		payrpf.setSinstamt01(payrIO.getSinstamt01().getbigdata());
		payrpf.setSinstamt02(payrIO.getSinstamt02().getbigdata());
		payrpf.setSinstamt06(payrIO.getSinstamt06().getbigdata());
		List<Payrpf> payrpfInsList = new ArrayList<>();
		payrpfInsList.add(payrpf);

		if (!updateRertList.isEmpty()) {
			rertpfDAO.updateRertList(updateRertList);
		}
		if (!insertRertList1.isEmpty()) {
			rertpfDAO.insertRertList(insertRertList1);
		}
		agcmpfDAO.updateInvalidRecord(updateAgcmList);
		agcmpfDAO.insertAgcmpfList(insertAgcmList);
		chdrpfDAO.insertChdrBilling(chdrpf);
		payrpfDAO.insertPayrpfList(payrpfInsList);
		covrpfDAO.updateRerateData(updateCovrList);
		List<Coprpf> insertCoprList = new ArrayList<>();
		if(!coprList.isEmpty()) {
			for(Coprpf copr:coprList) {
				copr.setTranno(wsaaTranno.toInt());
				insertCoprList.add(copr);
			}
		}
		if(!insertCoprList.isEmpty()) {
			coprpfDAO.insertCoprpfRecord(insertCoprList);
		}

		isProcesFreqChange = false;
		rlseChdr3410();
		rlsePayr3510();
	}

	protected void writePtrn() {
		/* Write PTRN record. */
		ptrnpf.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrlifIO.getChdrnum().toString());
		ptrnpf.setTranno(wsaaTranno.toInt());
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(chdrlifIO.getChdrcoy().toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setValidflag(" ");
		List<Ptrnpf> ptrnList = new ArrayList<>();
		ptrnList.add(ptrnpf);
		boolean result = ptrnpfDAO.insertPtrnPF(ptrnList);

		if (!result) {
			syserrrec.params.set(ptrnpf.getChdrcoy().concat(ptrnpf.getChdrnum()));
			fatalError600();
		}
	}
	protected void releaseSoftlock3900() {
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde.toString());
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, Varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	protected void rlseChdr3410() {
		chdrlifIO.setFunction(Varcom.rlse);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			fatalError600();
		}
	}

	protected void rlsePayr3510() {
		payrIO.setFunction(Varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}
	
	@Override
	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}

	protected void a000CallRounding() {
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.currency.set(chdrlifIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
	}
	
	protected void batchUpdate3020() {
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		batcuprec.batccoy.set(wsaaBatckey.batcBatccoy);
		batcuprec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		batcuprec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,Varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}
	
	protected void triggerATjob() {
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(Varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, Varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("PH5MPAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO  ATRT-REQ-TERM.                 */
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrlifIO.getChdrnum());
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaAdvptdat.set(sv.advptdat);
		wsaaProDaysTotPrem.set(sv.instPrem);
		wsaaChdrpfGroupkey.set(chdrlifIO.getGrupkey());
		wsaaChdrpfMandref.set(chdrlifIO.getMandref());
		wsaaChdrpfMembsel.set(chdrlifIO.getMembsel());
		wsaaChdrpfPayrnum.set(chdrlifIO.getPayrnum());
		wsaaChdrpfBillchnl.set(chdrlifIO.getBillchnl());
		wsaaChdrpfGroupkey.set(payrIO.getGrupnum());
		wsaaPayrpfGrupcoy.set(payrIO.getGrupcoy());
		wsaaChdrpfMembsel.set(payrIO.getMembsel());
		wsaaChdrpfMandref.set(payrIO.getMandref());
		wsaaPayrpfIncomeSeqNo.set(payrIO.getIncomeSeqNo());
		wsaaPayrpfOrigBillcd.set(payrIO.getOrgbillcd().toInt());
		wsaaPayrpfBillday.set(payrIO.getBillday());
		wsaaPayrpfBillmonth.set(payrIO.getBillmonth());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO WSAA-TERMID.                    */
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransArea);
		atreqrec.statuz.set(Varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, Varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

	/*
	 * Class transformed from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner {
		private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
		private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
		private FixedLengthStringData h082 = new FixedLengthStringData(4).init("H082");
		private FixedLengthStringData h013 = new FixedLengthStringData(4).init("H013");
		private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
		private FixedLengthStringData h156 = new FixedLengthStringData(4).init("H156");
		private FixedLengthStringData h099 = new FixedLengthStringData(4).init("H099");
		private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
		private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
		private FixedLengthStringData f151 = new FixedLengthStringData(4).init("F151");
	}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
		private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
		private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
		private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	}
}
