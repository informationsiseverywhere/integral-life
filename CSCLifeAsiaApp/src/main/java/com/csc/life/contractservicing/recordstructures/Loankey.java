package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:24
 * Description:
 * Copybook name: LOANKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Loankey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData loanFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData loanKey = new FixedLengthStringData(64).isAPartOf(loanFileKey, 0, REDEFINE);
  	public FixedLengthStringData loanChdrcoy = new FixedLengthStringData(1).isAPartOf(loanKey, 0);
  	public FixedLengthStringData loanChdrnum = new FixedLengthStringData(8).isAPartOf(loanKey, 1);
  	public PackedDecimalData loanLoanNumber = new PackedDecimalData(2, 0).isAPartOf(loanKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(53).isAPartOf(loanKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(loanFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		loanFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}