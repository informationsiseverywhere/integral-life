package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Lnrepayrec extends ExternalData{

	public FixedLengthStringData lnrepayRec = new FixedLengthStringData(274);
	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(lnrepayRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(lnrepayRec, 5);
  	public ZonedDecimalData trandate = new ZonedDecimalData(8, 0).isAPartOf(lnrepayRec, 9).setUnsigned();
	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(lnrepayRec, 17);
  	public FixedLengthStringData trankey = new FixedLengthStringData(20).isAPartOf(lnrepayRec, 19);
  	public FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(lnrepayRec, 39);
  	public FixedLengthStringData loanNo = new FixedLengthStringData(3).isAPartOf(lnrepayRec, 41);
	public ZonedDecimalData dissrate = new ZonedDecimalData(18, 9).isAPartOf(lnrepayRec, 44);
	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(lnrepayRec, 62);
	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(lnrepayRec, 92);
	public FixedLengthStringData errors = new FixedLengthStringData(24).isAPartOf(lnrepayRec, 95);
	public FixedLengthStringData sacscodeErr = new FixedLengthStringData(4).isAPartOf(errors, 0);
	public FixedLengthStringData trankeyErr = new FixedLengthStringData(4).isAPartOf(errors, 4);
  	public FixedLengthStringData sacstypErr = new FixedLengthStringData(4).isAPartOf(errors, 8);
  	public FixedLengthStringData dissrateErr = new FixedLengthStringData(4).isAPartOf(errors, 12);
  	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errors, 16);
  	public FixedLengthStringData loanNoErr = new FixedLengthStringData(4).isAPartOf(errors, 20);
  	public FixedLengthStringData genlkey = new FixedLengthStringData(20).isAPartOf(lnrepayRec, 119);
  	public FixedLengthStringData genlPrefix = new FixedLengthStringData(2).isAPartOf(genlkey, 0);
  	public FixedLengthStringData genlCompany = new FixedLengthStringData(1).isAPartOf(genlkey, 2);
  	public FixedLengthStringData genlCurrency = new FixedLengthStringData(3).isAPartOf(genlkey, 3);
  	public FixedLengthStringData genlAccount = new FixedLengthStringData(14).isAPartOf(genlkey, 6);
	public FixedLengthStringData sign = new FixedLengthStringData(1).isAPartOf(lnrepayRec, 139);
  	public ZonedDecimalData docamt = new ZonedDecimalData(17, 2).isAPartOf(lnrepayRec, 140);
  	public ZonedDecimalData transeq = new ZonedDecimalData(4, 0).isAPartOf(lnrepayRec, 157).setUnsigned();
	public FixedLengthStringData chdrkey = new FixedLengthStringData(12).isAPartOf(lnrepayRec, 161);
  	public FixedLengthStringData chdrpfx = new FixedLengthStringData(2).isAPartOf(chdrkey, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(chdrkey, 2);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(9).isAPartOf(chdrkey, 3);
	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(lnrepayRec, 173).setUnsigned();
	public FixedLengthStringData doctkey = new FixedLengthStringData(12).isAPartOf(lnrepayRec, 178);
  	public FixedLengthStringData doctPrefix = new FixedLengthStringData(2).isAPartOf(doctkey, 0);
  	public FixedLengthStringData doctCompany = new FixedLengthStringData(1).isAPartOf(doctkey, 2);
  	public FixedLengthStringData doctNumber = new FixedLengthStringData(9).isAPartOf(doctkey, 3);
  	
	public FixedLengthStringData batchkey = new FixedLengthStringData(22).isAPartOf(lnrepayRec, 190);
  	public FixedLengthStringData tranid = new FixedLengthStringData(22).isAPartOf(lnrepayRec, 212);
  	public ZonedDecimalData origamt = new ZonedDecimalData(17, 2).isAPartOf(lnrepayRec, 234);
  	public FixedLengthStringData origccy = new FixedLengthStringData(3).isAPartOf(lnrepayRec, 251);
 	public FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(lnrepayRec, 254);
 	public ZonedDecimalData repaidamt = new ZonedDecimalData(17, 2).isAPartOf(lnrepayRec, 257);

	public void initialize() {
		COBOLFunctions.initialize(lnrepayRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			lnrepayRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}
