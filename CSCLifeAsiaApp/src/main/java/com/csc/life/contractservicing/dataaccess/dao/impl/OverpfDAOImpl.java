package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.contractservicing.dataaccess.dao.OverpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Overpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;


public class OverpfDAOImpl extends BaseDAOImpl<Overpf> implements OverpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(OverpfDAOImpl.class);

	@Override
	public int insertOverpf(Overpf overpf) {
		 
		 StringBuilder sb = new StringBuilder("insert into OVERPF(CHDRCOY,CNTTYPE ,CHDRNUM, VALIDFLAG, OVERDUEFLAG, EFFECTIVEDATE, ENDDATE, USRPRF, JOBNM, DATIME, CREATED_AT)");
		 sb.append(" values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		  
		 PreparedStatement ps = getPrepareStatement(sb.toString());
		 ResultSet rs = null;	
	    
	    int result  = 0;
	    try {
	   	 ps.setString(1, overpf.getChdrcoy());
	   	 ps.setString(2, overpf.getCnttype());
	   	 ps.setString(3, overpf.getChdrnum());
	   	 ps.setString(4, overpf.getValidflag());
	   	 ps.setString(5, overpf.getOverdueflag());
	   	 ps.setInt(6,overpf.getEffectivedate());
	   	 ps.setInt(7,overpf.getEnddate());
	   	 ps.setString(8, getUsrprf());
	   	 ps.setString(9, getJobnm());
	   	 ps.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
	     ps.setTimestamp(11, new Timestamp(System.currentTimeMillis()));
	     
	   	 result = ps.executeUpdate();
	   	 
	    } catch (SQLException e) {
	        LOGGER.error("inserOverpf()", e);
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps, rs);
	    }
		 
	    return result;
	}
	
	 public Overpf getOverpfRecord(String chdrcoy, String chdrnum,String validflag) {
		 	Overpf overpf = null;
	        StringBuilder sql = new StringBuilder("SELECT CHDRCOY,CNTTYPE,CHDRNUM,VALIDFLAG,OVERDUEFLAG,EFFECTIVEDATE,ENDDATE,USRPRF,JOBNM,DATIME,CREATED_AT, UNIQUE_NUMBER ");
	        sql.append("FROM OVERPF WHERE CHDRCOY=? AND CHDRNUM=?  AND VALIDFLAG=?");
	        sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC");
	        PreparedStatement ps = null;
	    	ResultSet rs = null;
	      
	        try {
	        	ps = getPrepareStatement(sql.toString());
	        	ps.setString(1, chdrcoy); 
	        	ps.setString(2, chdrnum); 
	        	ps.setString(3, validflag);
	        	rs = ps.executeQuery();
	            while (rs.next()) {
	            	overpf = new Overpf();
	                overpf.setChdrcoy(rs.getString(1));
	                overpf.setCnttype(rs.getString(2));
	                overpf.setChdrnum(rs.getString(3));
	                overpf.setValidflag(rs.getString(4));
	                overpf.setOverdueflag(rs.getString(5));
	                overpf.setEffectivedate(rs.getInt(6));
	                overpf.setEnddate(rs.getInt(7));
	                overpf.setUsrprf(rs.getString(8));
	                overpf.setJobnm(rs.getString(9));
	                overpf.setDatime(rs.getDate(10));
	                overpf.setCreated_at(rs.getDate(11));
	                overpf.setUnique_number(rs.getLong(12));
	            }

	        } catch (SQLException e) {
	            LOGGER.error("getOverpfRecord()", e);
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
	        return overpf;
	 	 }
	

	 public void updateOverpfRecord(int enddate, long unique_number) {
		 	String sql = "UPDATE OVERPF SET OVERDUEFLAG='P', VALIDFLAG='2' ,ENDDATE= ?, JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=?  ";          
		 		try(PreparedStatement ps = getPrepareStatement(sql)) {
		 			ps.setInt(1, enddate);	
		 			ps.setString(2, getJobnm());
		 			ps.setString(3, getUsrprf());
		 			ps.setTimestamp(4, getDatime());
		 			ps.setLong(5, unique_number);	

            ps.executeUpdate();
		 		} catch (SQLException e) {
		 			LOGGER.error("updateOverpfRecord()", e);
		 			throw new SQLRuntimeException(e);
		 		} 
	 	}
}
