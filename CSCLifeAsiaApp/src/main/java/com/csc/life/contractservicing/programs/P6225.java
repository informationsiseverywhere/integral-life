/*
 * File: P6225.java
 * Date: 30 August 2009 0:36:52
 * Author: Quipoz Limited
 * 
 * Class transformed from P6225.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Clntrlsrec;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.contractservicing.screens.S6225ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* DESPATCH ADDRESS MAINTENANCE.
*
* Retrieves the contract header details from the contract header
* file (CHDRLNB).    Using RETRV.
*
* The client details i.e. Address are retrieved from the client
* details file (CLTS). If the client number (despnum from the
* header) is blank then the owner number is used.
*
* The client number may be changed, and will be checked against
* the client file.
*
* If 'CALC' is pressed then the screen is redisplayed.
*
* If 'KILL' is pressed then all validation is ignored and the
* despatch address in the header is left blank - ignored.
*
* Update the despatch address in the header record held in the
* buffer using KEEPS.
*
* NOTE: The retrieval of client details and movement of these
* details for both the 1000 and 2000 section is in the
* 1600-RETRIEVAL section.
*
*****************************************************************
* </pre>
*/
public class P6225 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6225");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private static final String e040 = "E040";
	private static final String f782 = "F782";
	private static final String f464 = "F464";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Clntrlsrec clntrlsrec = new Clntrlsrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6225ScreenVars sv = ScreenProgram.getScreenVars( S6225ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public P6225() {
		super();
		screenVars = sv;
		new ScreenModel("S6225", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1100();
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		/*    Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*    Retrieve client details using DESPNUM or COWNNUM if*/
		/*    DESPNUM is blank (from the contract header) as the key.*/
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		if (isEQ(chdrlnbIO.getDespnum(), SPACES)) {
			cltsIO.setClntnum(chdrlnbIO.getCownnum());
		}
		else {
			cltsIO.setClntnum(chdrlnbIO.getDespnum());
		}
		retrieve1600();
		if (isEQ(chdrlnbIO.getDespnum(), SPACES)) {
			sv.despnum.set(SPACES);
		}
		else {
			sv.despnum.set(cltsIO.getClntnum());
		}
		if (isEQ(wsspcomn.flag, "I")) {
			sv.despnumOut[varcom.pr.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	*      RETRIEVE CLIENT DETAILS AND SET SCREEN
	* </pre>
	*/
protected void retrieve1600()
	{
		retrieve1700();
	}

protected void retrieve1700()
	{
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    Set screen fields*/
		sv.despname.set(SPACES);
		sv.cltaddr01.set(SPACES);
		sv.cltaddr02.set(SPACES);
		sv.cltaddr03.set(SPACES);
		sv.cltaddr04.set(SPACES);
		sv.cltaddr05.set(SPACES);
		sv.cltpcode.set(SPACES);
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			sv.despnumErr.set(e040);
			return ;
		}
		/* Is the client dead ?*/
		if (isLTE(cltsIO.getCltdod(), chdrlnbIO.getOccdate())) {
			sv.despnumErr.set(f782);
		}
		/* Get the confirmation name.*/
		plainname();
		sv.despname.set(wsspcomn.longconfname);
		sv.cltaddr01.set(cltsIO.getCltaddr01());
		sv.cltaddr02.set(cltsIO.getCltaddr02());
		sv.cltaddr03.set(cltsIO.getCltaddr03());
		sv.cltaddr04.set(cltsIO.getCltaddr04());
		sv.cltaddr05.set(cltsIO.getCltaddr05());
		sv.cltpcode.set(cltsIO.getCltpcode());
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2100();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6225IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S6225-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*    If F11 pressed bypass validation.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
		/*    If F9 pressed then set a srceen error so the screen*/
		/*    is re-displayed.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*    If Inquiry mode then protect Bypass validation.              */
		if (isEQ(wsspcomn.flag, "I")) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2900-EXIT.                                         */
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2100()
	{
		/*    Validate fields*/
		if (isNE(sv.despnum, SPACES)) {
			cltsIO.setDataArea(SPACES);
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntnum(sv.despnum);
			retrieve1600();
		}
		if (isEQ(sv.despnum, chdrlnbIO.getCownnum())) {
			sv.despnumErr.set(f464);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.despnum, chdrlnbIO.getCownnum())) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3100();
	}

protected void updateDatabase3100()
	{
		/*  Update database files as required / WSSP*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*    If F11 pressed bypass keep of contract header detail.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/*    Update DA client role by calling CLTRELN.                    */
		if (isNE(sv.despnum, chdrlnbIO.getDespnum())) {
			removeDaRole3200();
			addDaRole3300();
		}
		/*    Update the contact header detail using KEEPS.*/
		chdrlnbIO.setDespcoy(wsspcomn.fsuco);
		chdrlnbIO.setDesppfx("CN");
		chdrlnbIO.setDespnum(sv.despnum);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void removeDaRole3200()
	{
		start3210();
	}

protected void start3210()
	{
		if (isEQ(chdrlnbIO.getDespnum(), SPACES)) {
			return ;
		}
		cltrelnrec.clntpfx.set(fsupfxcpy.clnt);
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(chdrlnbIO.getDespnum());
		cltrelnrec.clrrrole.set(clntrlsrec.desp);
		cltrelnrec.forepfx.set(fsupfxcpy.chdr);
		cltrelnrec.forecoy.set(chdrlnbIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrlnbIO.getChdrnum());
		cltrelnrec.function.set("DEL  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)
		&& isNE(cltrelnrec.statuz, "F968")) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

protected void addDaRole3300()
	{
		start3310();
	}

protected void start3310()
	{
		if (isEQ(sv.despnum, SPACES)) {
			return ;
		}
		cltrelnrec.clntpfx.set(fsupfxcpy.clnt);
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.despnum);
		cltrelnrec.clrrrole.set(clntrlsrec.desp);
		cltrelnrec.forepfx.set(fsupfxcpy.chdr);
		cltrelnrec.forecoy.set(chdrlnbIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrlnbIO.getChdrnum());
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
