package com.csc.life.contractservicing.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:35
 * Description:
 * Copybook name: TRANCHKREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tranchkrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData codeCheckRec = new FixedLengthStringData(9);
  	public FixedLengthStringData tcdeStatuz = new FixedLengthStringData(4).isAPartOf(codeCheckRec, 0);
  	public FixedLengthStringData tcdeTranCode = new FixedLengthStringData(4).isAPartOf(codeCheckRec, 4);
  	public FixedLengthStringData tcdeCompany = new FixedLengthStringData(1).isAPartOf(codeCheckRec, 8);


	public void initialize() {
		COBOLFunctions.initialize(codeCheckRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		codeCheckRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}