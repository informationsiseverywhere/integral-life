/*
 * File: P6004.java
 * Date: December 3, 2013 3:05:21 AM ICT
 * Author: CSC
 * 
 * Class transformed from P6004.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.screens.S6004ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UdivTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*              Failed Unit Sales Reversals
*              ---------------------------
*
* This program is used to display the details of the Contract
*  that is going to have a Partial Surrender or Fund Swtich
*  reversed where the unit dealing for the transcation has an
*  error and has not processed the UTRN's.
*
* Initialisation.
* ---------------
*
* Read the Contract header (CHDRLIF) using the RETRV function to g t
*  the relevant data necessary for obtaining the status descriptio 
*  and the agent details.
*
* Obtain the necessary descriptions for Contract Type, Contract
*  Risk Status, Contract Premium Status and Transaction Code (for
*  the screen title) by calling 'DESCIO'for the respective tables
*  and then display these descriptions on the screen S6004.
*
* Client name
*
*     Read the Client Details file to get the clients name
*
* Paid-to date
*
*     Read the PAYR file to get the Paid-to date
*          and the Billed-to Date
*
* Payer name
*
*     Read the Client Role file(CLRF) to get the client number of
*          the payer, then read the Client details file with
*          payer client number to get payer name
*
* Agent details
*
*     In order to read the agent file use the agent details
*          from the contract header (READR on the AGLFLNB file).
*
* Last 'Reversible Transaction'
* We must check the PTRN file to make sure that the most
* 'significant transaction' is either a Partial Surrender
* or a Fund Switch.  If there are any other PTRNS which are
* more recent that the transaction we a trying to reverse,
* then the user must reverse those transcations first before
* attempting this reversal.
*
* Read PTRN file in reverse order, using PTRNREV logical view
* Now check if the PTRN record read is 'significant' and
* check if it is either a Partial Surrender or a Fund Swtich.
* If the transaction is not 'significant' (e.g. a Minor
* Alteration) then read the next most recent PTRN and perform
* the same checks on it.
* Repeat this until a 'significant' PTRN is found. Check by
* looking in T6661, the reversals transaction table.
*
* However, whatever the last PTRN is, it should be displayed
*  on the screen S6004 so that if the transaction is not a
*  Partial Surrender or a Fund Switch, the user can then see
*  what it is and then decide to exit and take whatever
*  action is necessary.
*
* Read the UDIV file for the transcation code being reversed.
* Where a record does not exist, the normal reversal transcation
* processing must be performed. This reversal processing is
* ONLY for transcations where the fund deal has an error and did
* not process the UTRN's.
*
* Validation.
* -----------
*
* If the user pressed 'KILL', don't continue with rest of validati n
*  and exit from program.
*
* If the user pressed Refresh, validate any data input on the
*  screen by the user.
*
*
* Updating.
* ---------
*
* If the user pressed 'KILL', don't continue with rest of section
*  and exit from program.
*
* Get this far, so the last transaction is either a Partial
* Surrender or a Fund Switch which failed fund dealing.
*
* Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
* Call the AT submission module to submit REVGENAT, passing the
*  contract number as the 'primary key', this performs the
*  reversal.
*
* Tables Used.
* ------------
*
* T1688 - Transaction codes
* T1691 - Master Menu Switching
* T3588 - Contract Premium Statii
* T3623 - Contract Risk Statii
* T5688 - Contract Structure
* T6661 - Transaction Reversal Parameters
*
****************************************************************** ****
* </pre>
*/
public class P6004 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6004");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaFwdTranCode = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaContinue = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(28).isAPartOf(wsaaPrimaryKey, 8, FILLER).init(SPACES);

	private FixedLengthStringData wsaaPayrKey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaPayrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrKey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrKey, 8);
		/* ERRORS */
	private static final String f910 = "F910";
	private static final String g552 = "G552";
	private static final String h226 = "H226";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String t6661 = "T6661";
	private static final String clrfrec = "CLRFREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String payrrec = "PAYRREC   ";
	private static final String udivrec = "UDIVREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private UdivTableDAM udivIO = new UdivTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Atreqrec atreqrec = new Atreqrec();
	private T6661rec t6661rec = new T6661rec();
	private S6004ScreenVars sv = ScreenProgram.getScreenVars( S6004ScreenVars.class);
	private WsaaTransactionAreaInner wsaaTransactionAreaInner = new WsaaTransactionAreaInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		getNextPtrnRecord1850, 
		exit1890, 
		exit2090, 
		getNextPtrnRecord2250, 
		exit2290
	}

	public P6004() {
		super();
		screenVars = sv;
		new ScreenModel("S6004", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaItem.set(SPACES);
		wsaaContinue.set(SPACES);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		/*    Set Effective date to today by using DATCON1*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/* Retrieve contract fields from I/O module*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(chdrlifIO.getChdrnum());
		sv.cnttype.set(chdrlifIO.getCnttype());
		sv.cntcurr.set(chdrlifIO.getCntcurr());
		sv.cownnum.set(chdrlifIO.getCownnum());
		sv.payrnum.set(chdrlifIO.getPayrnum());
		sv.agntnum.set(chdrlifIO.getAgntnum());
		sv.occdate.set(chdrlifIO.getOccdate());
		/* Read T6661, the Reversals transaction table, using the*/
		/* Reversal transaction code as the key.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaFwdTranCode.set(t6661rec.trcode);
		/* Get rest of details to display on screen*/
		clientDetails1100();
		agentDetails1200();
		payerDetails1300();
		tableDetails1400();
		checkLastPtrns1500();
		checkUdiv5000();
		/* Check whether transaction code is a Part Surrender or Fund*/
		/* Switch which we can reverse or do we display an error message.*/
		if (isNE(wsaaContinue, "Y")) {
			if (isNE(udivIO.getStatuz(), varcom.oK)) {
				sv.batctrcdeErr.set(g552);
			}
			else {
				sv.batctrcdeErr.set(h226);
			}
			wsspcomn.edterror.set("Y");
		}
	}

protected void clientDetails1100()
	{
		/*START*/
		/* Get contract owners name*/
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(chdrlifIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		clientIo1600();
		sv.ownername.set(wsspcomn.longconfname);
		/*EXIT*/
	}

protected void agentDetails1200()
	{
		start1200();
	}

protected void start1200()
	{
		/* Get Agent details*/
		aglflnbIO.setParams(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(chdrlifIO.getAgntnum());
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)
		&& isNE(aglflnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(aglflnbIO.getStatuz(), varcom.mrnf)) {
			sv.agentname.set(SPACES);
		}
		else {
			cltsIO.setParams(SPACES);
			cltsIO.setClntnum(aglflnbIO.getClntnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			clientIo1600();
			sv.agentname.set(wsspcomn.longconfname);
		}
	}

protected void payerDetails1300()
	{
		start1300();
	}

protected void start1300()
	{
		/* Get contract Paid-to date from PAYR file*/
		payrIO.setParams(SPACES);
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setPayrseqno(ZERO);
		payrIO.setFunction(varcom.begn);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Check PAYR record read is correct for contract*/
		if (isNE(chdrlifIO.getChdrnum(), payrIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), payrIO.getChdrcoy())
		|| isEQ(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Set up Paid-to and Billed-to dates on screen*/
		sv.btdate.set(payrIO.getBtdate());
		sv.ptdate.set(payrIO.getPtdate());
		/* Get contract Payers name from client role file using the*/
		/*  CLRF logical view - Client roles by foreign key*/
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx(chdrlifIO.getChdrpfx());
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaPayrChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaPayrKey);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		clrfIO.setFormat(clrfrec);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(clrfIO.getClntnum());
		cltsIO.setClntcoy(clrfIO.getClntcoy());
		cltsIO.setClntpfx(clrfIO.getClntpfx());
		clientIo1600();
		sv.payrnum.set(clrfIO.getClntnum());
		sv.payorname.set(wsspcomn.longconfname);
	}

protected void tableDetails1400()
	{
		start1400();
	}

protected void start1400()
	{
		/* Get Contract type description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t5688);
		wsaaItem.set(chdrlifIO.getCnttype());
		getDescriptions1700();
		sv.ctypdesc.set(descIO.getLongdesc());
		/* Get Contract Risk status description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3623);
		wsaaItem.set(chdrlifIO.getStatcode());
		getDescriptions1700();
		sv.rstate.set(descIO.getShortdesc());
		/* Get Premium status description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3588);
		wsaaItem.set(chdrlifIO.getPstatcode());
		getDescriptions1700();
		sv.pstate.set(descIO.getShortdesc());
	}

protected void checkLastPtrns1500()
	{
		start1500();
		getDescription1580();
	}

protected void start1500()
	{
		/* We must check the PTRN file to make sure that the most*/
		/*  'significant transaction' is either a Partial Surrender or*/
		/*   a fund switch. If there are are other PTRNS which are more*/
		/*   recent that the transcation that we are trying to reverse,*/
		/*   then the user must reverse those transactions first before*/
		/*   attempting this reversal.*/
		/* Read PTRN file in reverse order, using PTRNREV logical view*/
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrlifIO.getChdrnum(), ptrnrevIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
		/* Now check if the PTRN record read is 'significant' and*/
		/* check if it is either a Partial Surrender or a Fund Switch.*/
		/* If the transaction is not 'significant' (eg. a Minor*/
		/* Alteration) then read the next most recent PTRN and perform*/
		/* the same checks on it.*/
		/* Repeat this until a 'significant' PTRN is found. Check by*/
		/* looking in T6661, the reversals transaction table.*/
		/* Partial Surrender or Fund Switch check.....*/
		if (isEQ(ptrnrevIO.getBatctrcde(), wsaaFwdTranCode)) {
			wsaaContinue.set("Y");
			return ;
		}
		wsaaContinue.set(SPACES);
		while ( !(isNE(wsaaContinue, SPACES))) {
			ptrnLoop1800();
		}
		
	}

protected void getDescription1580()
	{
		/* set up screen fields for last-transaction-code and the*/
		/*  description of it.*/
		sv.batctrcde.set(ptrnrevIO.getBatctrcde());
		/* Get Transaction code description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		wsaaItem.set(ptrnrevIO.getBatctrcde());
		getDescriptions1700();
		sv.longdesc.set(descIO.getLongdesc());
		/*EXIT*/
	}

protected void clientIo1600()
	{
		/*START*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			wsspcomn.longconfname.set(SPACES);
		}
		else {
			plainname();
		}
		/*EXIT*/
	}

protected void getDescriptions1700()
	{
		start1700();
	}

protected void start1700()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaItem);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setDescitem(wsaaItem);
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
	}

protected void ptrnLoop1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1800();
				case getNextPtrnRecord1850: 
					getNextPtrnRecord1850();
				case exit1890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1800()
	{
		/* Read T6661 with our PTRNREV transaction code*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ptrnrevIO.getChdrcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			itemIO.setItemitem(ptrnrevIO.getBatctrcde());
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		/* IF the 'Used by Contract Reversal' flag on the T6661 entry is*/
		/*  set to 'N', then the PTRN is non-significant so read the next*/
		/*  PTRN to see what it is.*/
		/* IF the 'Used by Contract Reversal' flag on the T6661 entry is*/
		/*  set to 'Y' or SPACES, then this Transaction must be reversed*/
		/* out before the user can go ahead with this reverssal,*/
		/* so display message on screen and do not allow the user*/
		/* to confirm Reversal by setting appropriate error flags.*/
		if (isEQ(t6661rec.contRevFlag, "N")) {
			goTo(GotoLabel.getNextPtrnRecord1850);
		}
		if (isEQ(t6661rec.contRevFlag, "Y")
		|| isEQ(t6661rec.contRevFlag, SPACES)) {
			wsaaContinue.set("N");
			goTo(GotoLabel.exit1890);
		}
	}

protected void getNextPtrnRecord1850()
	{
		/* Get next PTRNREV record*/
		ptrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrlifIO.getChdrnum(), ptrnrevIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
		/* Partial Surrender or Fund Switch check.....*/
		if (isEQ(ptrnrevIO.getBatctrcde(), wsaaFwdTranCode)) {
			wsaaContinue.set("Y");
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		/* Check if KILL pressed*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/* Check if refresh pressed*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		revalidatePtrns2100();
		checkUdiv5000();
		/* Check whether transaction code is a Part Surrender or Fund*/
		/* Switch which we can reverse or do we display an error message.*/
		if (isNE(wsaaContinue, "Y")) {
			if (isNE(udivIO.getStatuz(), varcom.oK)) {
				sv.batctrcdeErr.set(g552);
			}
			else {
				sv.batctrcdeErr.set(h226);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void revalidatePtrns2100()
	{
		start2100();
	}

protected void start2100()
	{
		/* We must check the PTRN file to make sure that the most*/
		/*  'significant transaction' is either a Partial Surrender or*/
		/*   a Fund Switch which failed unit dealing. If there*/
		/*   are any other PTRNS which are more recent than the Partial*/
		/*   Surrender or Fund Switch which failed unit dealing that*/
		/*   we are trying to reverse, then the user must reverse those*/
		/*   those transactions first before attempting this reversal.*/
		/* Read PTRN file in reverse order, using PTRNREV logical view*/
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnrevIO.setChdrnum(sv.chdrnum);
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrlifIO.getChdrnum(), ptrnrevIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
		/* Now check if the PTRN record read is 'significant' and*/
		/* check if it is either a Partial Surrender or a Fund Switch*/
		/* which failed during fund dealing. If the transaction is*/
		/* not 'significant' (eg. a Minor Alteration) then read the*/
		/* next most recent PTRN and perform the same checks on it.*/
		/* Repeat this until a 'significant' PTRN is found. Check by*/
		/* looking in T6661, the reversals transaction table.*/
		/* Partial Surrender or Fund Switch Check......*/
		if (isEQ(ptrnrevIO.getBatctrcde(), wsaaFwdTranCode)) {
			wsaaContinue.set("Y");
			return ;
		}
		wsaaContinue.set(SPACES);
		while ( !(isNE(wsaaContinue, SPACES))) {
			ptrnLoop2200();
		}
		
		/* set up screen fields for last-transaction-code and the*/
		/*  description of it.*/
		sv.batctrcde.set(ptrnrevIO.getBatctrcde());
		/* Get Transaction code description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		wsaaItem.set(ptrnrevIO.getBatctrcde());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaItem);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setDescitem(wsaaItem);
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
		sv.longdesc.set(descIO.getLongdesc());
	}

protected void ptrnLoop2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2200();
				case getNextPtrnRecord2250: 
					getNextPtrnRecord2250();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2200()
	{
		/* Read T6661 with our PTRNREV transaction code*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ptrnrevIO.getChdrcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			itemIO.setItemitem(ptrnrevIO.getBatctrcde());
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		/* IF the 'Used by Contract Reversal' flag on the T6661 entry is*/
		/*  set to 'N', then the PTRN is non-significant so read the next*/
		/*  PTRN to see what it is.*/
		/* IF the 'Used by Contract Reversal' flag on the T6661 entry is*/
		/*  set to 'Y' or SPACES, then this Transaction must be reversed*/
		/* out before the user can go ahead with this reversal,*/
		/* so display message on screen and do not allow the user*/
		/* to confirm Reversal by setting appropriate error flags.*/
		if (isEQ(t6661rec.contRevFlag, "N")) {
			goTo(GotoLabel.getNextPtrnRecord2250);
		}
		if (isEQ(t6661rec.contRevFlag, "Y")
		|| isEQ(t6661rec.contRevFlag, SPACES)) {
			wsaaContinue.set("N");
			goTo(GotoLabel.exit2290);
		}
	}

protected void getNextPtrnRecord2250()
	{
		/* Get next PTRNREV record*/
		ptrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrlifIO.getChdrnum(), ptrnrevIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
		/* Partial Surrender or Fund Switch check.....*/
		if (isEQ(ptrnrevIO.getBatctrcde(), wsaaFwdTranCode)) {
			wsaaContinue.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/* Change the contract soft lock record statuz to AT.*/
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaChdrnum.set(chdrlifIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionAreaInner.wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionAreaInner.wsaaTodate.set(wsaaToday);
		wsaaTransactionAreaInner.wsaaUser.set(varcom.vrcmUser);
		wsaaTransactionAreaInner.wsaaTranDate.set(varcom.vrcmDate);
		wsaaTransactionAreaInner.wsaaTranTime.set(varcom.vrcmTime);
		wsaaTransactionAreaInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransactionAreaInner.wsaaSupflag.set("N");
		wsaaTransactionAreaInner.wsaaSuppressTo.set(varcom.vrcmMaxDate);
		wsaaTransactionAreaInner.wsaaPlnsfx.set(ZERO);
		wsaaTransactionAreaInner.wsaaTranNum.set(ptrnrevIO.getTranno());
		wsaaTransactionAreaInner.wsaaCfiafiTranCode.set(SPACES);
		wsaaTransactionAreaInner.wsaaCfiafiTranno.set(ZERO);
		wsaaTransactionAreaInner.wsaaBbldat.set(ZERO);
		atreqrec.transArea.set(wsaaTransactionAreaInner.wsaaTransactionArea);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void checkUdiv5000()
	{
		checkUdivInit5010();
	}

protected void checkUdivInit5010()
	{
		/* If the fund deal had an error then a UDIV record will be*/
		/* written to show that the UTRN is still unprocessed.  This*/
		/* reversal processing will only allow reversal where a UDIV*/
		/* records exists.*/
		udivIO.setDataArea(SPACES);
		udivIO.setFormat(udivrec);
		udivIO.setChdrcoy(wsspcomn.company);
		udivIO.setChdrnum(ptrnrevIO.getChdrnum());
		udivIO.setTranno(ptrnrevIO.getTranno());
		udivIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, udivIO);
		if (isNE(udivIO.getStatuz(), varcom.oK)
		&& isNE(udivIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(udivIO.getParams());
			fatalError600();
		}
		if (isEQ(udivIO.getStatuz(), varcom.mrnf)) {
			wsaaContinue.set("N");
		}
	}
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-AREA--INNER
 */
private static final class WsaaTransactionAreaInner { 

	private FixedLengthStringData wsaaTransactionArea = new FixedLengthStringData(202);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransactionArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransactionArea, 45).setUnsigned();
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransactionArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransactionArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 62).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(132).isAPartOf(wsaaTransactionArea, 70, FILLER).init(SPACES);
}
}
