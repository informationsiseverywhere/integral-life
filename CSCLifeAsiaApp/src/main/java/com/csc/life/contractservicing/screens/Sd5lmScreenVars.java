package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sd5lm
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class Sd5lmScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(1080);
	public FixedLengthStringData dataFields = new FixedLengthStringData(472).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,49);
	public ZonedDecimalData datefrm = DD.datefrm.copyToZonedDecimal().isAPartOf(dataFields,89);
	public ZonedDecimalData dateto = DD.dateto.copyToZonedDecimal().isAPartOf(dataFields,97);
	
	public FixedLengthStringData contrAmounts = new FixedLengthStringData(102).isAPartOf(dataFields, 105);
	public ZonedDecimalData[] contrAmount = ZDArrayPartOfStructure(6, 17, 2, contrAmounts, 0);
	public FixedLengthStringData contrAmountFiller = new FixedLengthStringData(102).isAPartOf(contrAmounts, 0, FILLER_REDEFINE);	
	public ZonedDecimalData contrAmount01 = DD.amt.copyToZonedDecimal().isAPartOf(contrAmountFiller,0);
	public ZonedDecimalData contrAmount02 = DD.amt.copyToZonedDecimal().isAPartOf(contrAmountFiller,17);
	public ZonedDecimalData contrAmount03 = DD.amt.copyToZonedDecimal().isAPartOf(contrAmountFiller,34);
	public ZonedDecimalData contrAmount04 = DD.amt.copyToZonedDecimal().isAPartOf(contrAmountFiller,51);
	public ZonedDecimalData contrAmount05 = DD.amt.copyToZonedDecimal().isAPartOf(contrAmountFiller,68);
	public ZonedDecimalData contrAmount06 = DD.amt.copyToZonedDecimal().isAPartOf(contrAmountFiller,85);	
	
	public FixedLengthStringData contrPrcnts = new FixedLengthStringData(30).isAPartOf(dataFields, 207);
	public ZonedDecimalData[] contrPrcnt = ZDArrayPartOfStructure(6, 5, 2, contrPrcnts, 0);
	public FixedLengthStringData contrPrcntFiller = new FixedLengthStringData(30).isAPartOf(contrPrcnts, 0, FILLER_REDEFINE);
	public ZonedDecimalData contrPrcnt01 = DD.prcnt.copyToZonedDecimal().isAPartOf(contrPrcntFiller,0);
	public ZonedDecimalData contrPrcnt02 = DD.prcnt.copyToZonedDecimal().isAPartOf(contrPrcntFiller,5);
	public ZonedDecimalData contrPrcnt03 = DD.prcnt.copyToZonedDecimal().isAPartOf(contrPrcntFiller,10);
	public ZonedDecimalData contrPrcnt04 = DD.prcnt.copyToZonedDecimal().isAPartOf(contrPrcntFiller,15);
	public ZonedDecimalData contrPrcnt05 = DD.prcnt.copyToZonedDecimal().isAPartOf(contrPrcntFiller,20);
	public ZonedDecimalData contrPrcnt06 = DD.prcnt.copyToZonedDecimal().isAPartOf(contrPrcntFiller,25);
	
	public FixedLengthStringData revContrAmounts = new FixedLengthStringData(102).isAPartOf(dataFields, 237);
	public ZonedDecimalData[] revContrAmount = ZDArrayPartOfStructure(6, 17, 2, revContrAmounts, 0);
	public FixedLengthStringData revContrAmountFiller = new FixedLengthStringData(102).isAPartOf(revContrAmounts, 0, FILLER_REDEFINE);	
	public ZonedDecimalData revsgtamt = DD.revsgtamt.copyToZonedDecimal().isAPartOf(revContrAmountFiller,0);
	public ZonedDecimalData revoeramt = DD.revoeramt.copyToZonedDecimal().isAPartOf(revContrAmountFiller,17);
	public ZonedDecimalData revdedamt = DD.revdedamt.copyToZonedDecimal().isAPartOf(revContrAmountFiller,34);
	public ZonedDecimalData revundamt = DD.revundamt.copyToZonedDecimal().isAPartOf(revContrAmountFiller,51);
	public ZonedDecimalData revspsamt = DD.revspsamt.copyToZonedDecimal().isAPartOf(revContrAmountFiller,68);
	public ZonedDecimalData revslyamt = DD.revslyamt.copyToZonedDecimal().isAPartOf(revContrAmountFiller,85);
	
	public FixedLengthStringData revContrPrcnts = new FixedLengthStringData(30).isAPartOf(dataFields, 339);
	public ZonedDecimalData[] revContrPrcnt = ZDArrayPartOfStructure(6, 5, 2, revContrPrcnts, 0);
	public FixedLengthStringData revContrPrcntFiller = new FixedLengthStringData(30).isAPartOf(revContrPrcnts, 0, FILLER_REDEFINE);
	public ZonedDecimalData revsgtpct = DD.revsgtpct.copyToZonedDecimal().isAPartOf(revContrPrcntFiller,0);
	public ZonedDecimalData revoerpct = DD.revoerpct.copyToZonedDecimal().isAPartOf(revContrPrcntFiller,5);
	public ZonedDecimalData revdedpct = DD.revdedpct.copyToZonedDecimal().isAPartOf(revContrPrcntFiller,10);
	public ZonedDecimalData revundpct = DD.revundpct.copyToZonedDecimal().isAPartOf(revContrPrcntFiller,15);
	public ZonedDecimalData revspspct = DD.revspspct.copyToZonedDecimal().isAPartOf(revContrPrcntFiller,20);
	public ZonedDecimalData revslypct = DD.revslypct.copyToZonedDecimal().isAPartOf(revContrPrcntFiller,25);
	
	public ZonedDecimalData coCopPayment = DD.payment.copyToZonedDecimal().isAPartOf(dataFields,369);
	public ZonedDecimalData liscPayment = DD.payment.copyToZonedDecimal().isAPartOf(dataFields,386);
	
	public ZonedDecimalData totContrAmount = DD.amt.copyToZonedDecimal().isAPartOf(dataFields,403);
	public ZonedDecimalData revTotContrAmount = DD.amt.copyToZonedDecimal().isAPartOf(dataFields,420);
	
	public FixedLengthStringData notRecvd = DD.recind.copy().isAPartOf(dataFields,437);
	public ZonedDecimalData riskPremium = DD.premium.copyToZonedDecimal().isAPartOf(dataFields,438);
		
	public ZonedDecimalData contrTax = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,455);
	
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(152).isAPartOf(dataArea,472);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData datefrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData datetoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData contrAmount01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData contrAmount02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData contrAmount03Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData contrAmount04Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData contrAmount05Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData contrAmount06Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData contrPrcnt01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData contrPrcnt02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData contrPrcnt03Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData contrPrcnt04Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData contrPrcnt05Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData contrPrcnt06Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData revContrAmountsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData[] revContrAmountErr = FLSArrayPartOfStructure(6, 4, revContrAmountsErr, 0);
	public FixedLengthStringData revContrAmountErrFiller = new FixedLengthStringData(24).isAPartOf(revContrAmountsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData revsgtamtErr = new FixedLengthStringData(4).isAPartOf(revContrAmountErrFiller, 0);
	public FixedLengthStringData revoeramtErr = new FixedLengthStringData(4).isAPartOf(revContrAmountErrFiller, 4);
	public FixedLengthStringData revdedamtErr = new FixedLengthStringData(4).isAPartOf(revContrAmountErrFiller, 8);
	public FixedLengthStringData revundamtErr = new FixedLengthStringData(4).isAPartOf(revContrAmountErrFiller, 12);
	public FixedLengthStringData revspsamtErr = new FixedLengthStringData(4).isAPartOf(revContrAmountErrFiller, 16);
	public FixedLengthStringData revslyamtErr = new FixedLengthStringData(4).isAPartOf(revContrAmountErrFiller, 20);
	public FixedLengthStringData revContrPrcntsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData[] revContrPrcntErr = FLSArrayPartOfStructure(6, 4, revContrPrcntsErr, 0);
	public FixedLengthStringData revContrPrcntFillerErr = new FixedLengthStringData(24).isAPartOf(revContrPrcntsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData revsgtpctErr = new FixedLengthStringData(4).isAPartOf(revContrPrcntFillerErr, 0);
	public FixedLengthStringData revoerpctErr = new FixedLengthStringData(4).isAPartOf(revContrPrcntFillerErr, 4);
	public FixedLengthStringData revdedpctErr = new FixedLengthStringData(4).isAPartOf(revContrPrcntFillerErr, 8);
	public FixedLengthStringData revundpctErr = new FixedLengthStringData(4).isAPartOf(revContrPrcntFillerErr, 12);
	public FixedLengthStringData revspspctErr = new FixedLengthStringData(4).isAPartOf(revContrPrcntFillerErr, 16);
	public FixedLengthStringData revslypctErr = new FixedLengthStringData(4).isAPartOf(revContrPrcntFillerErr, 20);
	public FixedLengthStringData coCopPaymentErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData liscPaymentErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData totContrAmountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData revTotContrAmountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData notRecvdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData riskPremiumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData contrTaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(456).isAPartOf(dataArea,624);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] datefrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] datetoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] contrAmount01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] contrAmount02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] contrAmount03Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] contrAmount04Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] contrAmount05Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] contrAmount06Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] contrPrcnt01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] contrPrcnt02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] contrPrcnt03Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] contrPrcnt04Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] contrPrcnt05Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] contrPrcnt06Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] revsgtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] revoeramtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] revdedamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] revundamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] revspsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] revslyamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] revsgtpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] revoerpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] revdedpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] revundpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] revspspctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] revslypctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] coCopPaymentOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] liscPaymentOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] totContrAmountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] revTotContrAmountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] notRecvdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] riskPremiumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] contrTaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData datefrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datetoDisp = new FixedLengthStringData(10);
	
	public LongData Sd5lmscreenWritten = new LongData(0);
	public LongData Sd5lmprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sd5lmScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
	fieldIndMap.put(liscPaymentOut,new String[] {null,null, "02",null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(coCopPaymentOut,new String[] {null,null,"03",null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(revsgtpctOut,new String[] {"04",null,null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(revoerpctOut,new String[] {"05",null,null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(revdedpctOut,new String[] {"06",null,null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(revundpctOut,new String[] {"07",null,null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(revspspctOut,new String[] {"08",null,null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(revslypctOut,new String[] {"09",null,null,null, null, null, null, null, null, null, null, null});
	
	screenFields = new BaseData[] {chdrnum,cnttype,cntypdesc,lifenum,lifedesc,datefrm,dateto,contrAmount01,contrAmount02,contrAmount03,contrAmount04,contrAmount05,contrAmount06,contrPrcnt01,contrPrcnt02,contrPrcnt03,contrPrcnt04,contrPrcnt05,contrPrcnt06,	revsgtamt,revoeramt,revdedamt,revundamt,revspsamt,revslyamt,	revsgtpct,revoerpct,revdedpct,revundpct,revspspct,revslypct,coCopPayment,liscPayment,totContrAmount,revTotContrAmount,notRecvd,riskPremium,contrTax};
	screenOutFields = new BaseData[][] {chdrnumOut,cnttypeOut,cntypdescOut,lifenumOut,lifedescOut,datefrmOut,datetoOut,contrAmount01Out,contrAmount02Out,contrAmount03Out,contrAmount04Out,contrAmount05Out,contrAmount06Out,contrPrcnt01Out,contrPrcnt02Out,contrPrcnt03Out,contrPrcnt04Out,contrPrcnt05Out,contrPrcnt06Out,	revsgtamtOut,revoeramtOut,revdedamtOut,revundamtOut,revspsamtOut,revslyamtOut,	revsgtpctOut,revoerpctOut,revdedpctOut,revundpctOut,revspspctOut,revslypctOut,coCopPaymentOut,liscPaymentOut,totContrAmountOut,revTotContrAmountOut,notRecvdOut,riskPremiumOut,contrTaxOut};
	screenErrFields = new BaseData[] {chdrnumErr,cnttypeErr,cntypdescErr,lifenumErr,lifedescErr,datefrmErr,datetoErr,contrAmount01Err,contrAmount02Err,contrAmount03Err,contrAmount04Err,contrAmount05Err,contrAmount06Err,contrPrcnt01Err,contrPrcnt02Err,contrPrcnt03Err,contrPrcnt04Err,contrPrcnt05Err,contrPrcnt06Err,	revsgtamtErr,revoeramtErr,revdedamtErr,revundamtErr,revspsamtErr,revslyamtErr,	revsgtpctErr,revoerpctErr,revdedpctErr,revundpctErr,revspspctErr,revslypctErr,coCopPaymentErr,liscPaymentErr,totContrAmountErr,revTotContrAmountErr,notRecvdErr,riskPremiumErr,contrTaxErr};
	
	screenDateFields = new BaseData[] {datefrm, dateto};
	screenDateErrFields = new BaseData[] {datefrmErr, datetoErr};
	screenDateDispFields = new BaseData[] {datefrmDisp,datetoDisp};

	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sd5lmscreen.class;
	protectRecord = Sd5lmprotect.class;  

	}
	
}
