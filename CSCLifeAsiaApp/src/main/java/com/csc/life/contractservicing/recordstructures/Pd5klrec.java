package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Pd5klrec extends ExternalData{
	public FixedLengthStringData parmRecord = new FixedLengthStringData(24);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData chdrnum1 = new FixedLengthStringData(8).isAPartOf(parmRecord, 8);
  	public FixedLengthStringData taxFrmYear = new FixedLengthStringData(4).isAPartOf(parmRecord,16);
  	public FixedLengthStringData taxToYear = new FixedLengthStringData(4).isAPartOf(parmRecord,20);

	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}
