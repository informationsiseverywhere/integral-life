package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh593screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sh593screensfl";
		lrec.subfileClass = Sh593screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 17;
		lrec.pageSubfile = 8;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 14, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh593ScreenVars sv = (Sh593ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh593screenctlWritten, sv.Sh593screensflWritten, av, sv.sh593screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh593ScreenVars screenVars = (Sh593ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownerdesc.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifedesc.setClassString("");
		screenVars.numberOfLoans.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifedesc.setClassString("");
		screenVars.hpleamt.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.intanny.setClassString("");
		screenVars.hpleint.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.hpletot.setClassString("");
	}

/**
 * Clear all the variables in Sh593screenctl
 */
	public static void clear(VarModel pv) {
		Sh593ScreenVars screenVars = (Sh593ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.currcd.clear();
		screenVars.cownnum.clear();
		screenVars.ownerdesc.clear();
		screenVars.lifenum.clear();
		screenVars.lifedesc.clear();
		screenVars.numberOfLoans.clear();
		screenVars.jlife.clear();
		screenVars.jlifedesc.clear();
		screenVars.hpleamt.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.intanny.clear();
		screenVars.hpleint.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.hpletot.clear();
	}
}
