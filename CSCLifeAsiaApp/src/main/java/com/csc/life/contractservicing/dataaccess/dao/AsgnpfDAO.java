package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;

import com.csc.life.contractservicing.dataaccess.model.Asgnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AsgnpfDAO extends BaseDAO<Asgnpf> {

	public List<Asgnpf> getAsgnpfByChdrNum(String chdrcoy, String chdrnum, String seqno);
}
