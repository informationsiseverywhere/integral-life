/*
 * File: Grvregis.java
 * Date: 29 August 2009 22:51:20
 * Author: Quipoz Limited
 * 
 * Class transformed from GRVREGIS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.terminationclaims.dataaccess.RegplnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegtTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
* REMARKS
*
* GRVREGIS: Delete REGP and write REGT routine.
* =============================================
*
* This subroutine will be called upon via T5671 generic
* processing. It is called using the standard generic reversal
* subroutine linkage area, GREVERSREC.
*
* The purpose of the subroutine is to delete all the REGP records
* up until the REGP records created at issue which will be
* coverted into REGT records and then deleted. This will be called
* via Alter from Inception processing.
*
*
*****************************************************************
* </pre>
*/
public class Grvregis extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaSameTrannoFound = "";
	private PackedDecimalData wsaaRgpynum = new PackedDecimalData(5, 0);
		/* FORMATS */
	private String regtrec = "REGTREC";
	private String regplnbrec = "REGPLNBREC";
	private Greversrec greversrec = new Greversrec();
		/*Regular Payments File*/
	private RegplnbTableDAM regplnbIO = new RegplnbTableDAM();
		/*Regular Payment Temporary Record*/
	private RegtTableDAM regtIO = new RegtTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Grvregis() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*STARTS*/
		initialize1000();
		while ( !(isEQ(regplnbIO.getStatuz(),varcom.endp))) {
			processRegp2000();
		}
		
		exitProgram();
	}

protected void initialize1000()
	{
		start1000();
	}

protected void start1000()
	{
		wsaaSameTrannoFound = "N";
		regplnbIO.setChdrcoy(greversrec.chdrcoy);
		regplnbIO.setChdrnum(greversrec.chdrnum);
		regplnbIO.setLife(greversrec.life);
		regplnbIO.setCoverage(greversrec.coverage);
		regplnbIO.setRider(greversrec.rider);
		regplnbIO.setPlanSuffix(greversrec.planSuffix);
		regplnbIO.setRgpynum(ZERO);
		regplnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regplnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		regplnbIO.setFormat(regplnbrec);
		SmartFileCode.execute(appVars, regplnbIO);
		if (isNE(regplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regplnbIO.getParams());
			syserrrec.statuz.set(regplnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(greversrec.chdrcoy,regplnbIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,regplnbIO.getChdrnum())
		|| isNE(greversrec.life,regplnbIO.getLife())
		|| isNE(greversrec.coverage,regplnbIO.getCoverage())
		|| isNE(greversrec.rider,regplnbIO.getRider())
		|| isNE(greversrec.planSuffix,regplnbIO.getPlanSuffix())) {
			syserrrec.params.set(regplnbIO.getParams());
			syserrrec.statuz.set(regplnbIO.getStatuz());
			fatalError600();
		}
		wsaaRgpynum.set(regplnbIO.getRgpynum());
	}

protected void processRegp2000()
	{
		/*STARTS*/
		if (isNE(regplnbIO.getTranno(),greversrec.tranno)) {
			differentTranno2020();
		}
		else {
			sameTranno2040();
		}
		/*EXIT*/
	}

protected void differentTranno2020()
	{
		/*STARTS*/
		if (isEQ(wsaaSameTrannoFound,"N")) {
			deleteRegp2400();
			getNextRegp2300();
		}
		else {
			if (isEQ(wsaaRgpynum,regplnbIO.getRgpynum())) {
				getNextRegp2300();
			}
			else {
				wsaaRgpynum.set(regplnbIO.getRgpynum());
				wsaaSameTrannoFound = "N";
				deleteRegp2400();
				getNextRegp2300();
			}
		}
		/*EXIT*/
	}

protected void sameTranno2040()
	{
		/*STARTS*/
		convertRegpRegt2100();
		deleteRegp2400();
		rewriteRegt2200();
		getNextRegp2300();
		wsaaSameTrannoFound = "Y";
		/*EXIT*/
	}

protected void convertRegpRegt2100()
	{
		starts2109();
	}

protected void starts2109()
	{
		regtIO.setChdrcoy(regplnbIO.getChdrcoy());
		regtIO.setChdrnum(regplnbIO.getChdrnum());
		regtIO.setLife(regplnbIO.getLife());
		regtIO.setCoverage(regplnbIO.getCoverage());
		regtIO.setRider(regplnbIO.getRider());
		regtIO.setRgpynum(regplnbIO.getRgpynum());
		if (isNE(regplnbIO.getPlanSuffix(),ZERO)) {
			regtIO.setSeqnbr(regplnbIO.getPlanSuffix());
		}
		else {
			regtIO.setSeqnbr(1);
		}
		regtIO.setSacscode(regplnbIO.getSacscode());
		regtIO.setSacstype(regplnbIO.getSacstype());
		regtIO.setGlact(regplnbIO.getGlact());
		regtIO.setDebcred(regplnbIO.getDebcred());
		regtIO.setDestkey(regplnbIO.getDestkey());
		regtIO.setPaycoy(regplnbIO.getPaycoy());
		regtIO.setPayclt(regplnbIO.getPayclt());
		regtIO.setRgpymop(regplnbIO.getRgpymop());
		regtIO.setRegpayfreq(regplnbIO.getRegpayfreq());
		regtIO.setCurrcd(regplnbIO.getCurrcd());
		regtIO.setPymt(regplnbIO.getPymt());
		regtIO.setPrcnt(regplnbIO.getPrcnt());
		regtIO.setTotamnt(regplnbIO.getTotamnt());
		regtIO.setPayreason(regplnbIO.getPayreason());
		regtIO.setClaimevd(regplnbIO.getClaimevd());
		regtIO.setBankkey(regplnbIO.getBankkey());
		regtIO.setBankacckey(regplnbIO.getBankacckey());
		regtIO.setCrtdate(regplnbIO.getCrtdate());
		regtIO.setFirstPaydate(regplnbIO.getFirstPaydate());
		regtIO.setRevdte(regplnbIO.getRevdte());
		regtIO.setFinalPaydate(regplnbIO.getFinalPaydate());
		regtIO.setAnvdate(regplnbIO.getAnvdate());
		regtIO.setRgpytype(regplnbIO.getRgpytype());
		regtIO.setCrtable(regplnbIO.getCrtable());
		regtIO.setPlanSuffix(regplnbIO.getPlanSuffix());
		regtIO.setUserProfile(regplnbIO.getUserProfile());
		regtIO.setJobName(regplnbIO.getJobName());
		regtIO.setDatime(regplnbIO.getDatime());
	}

protected void rewriteRegt2200()
	{
		/*STARTS*/
		regtIO.setFunction(varcom.writr);
		regtIO.setFormat(regtrec);
		SmartFileCode.execute(appVars, regtIO);
		if (isNE(regtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtIO.getParams());
			syserrrec.statuz.set(regtIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getNextRegp2300()
	{
		/*STARTS*/
		regplnbIO.setFunction(varcom.nextr);
		regplnbIO.setFormat(regplnbrec);
		SmartFileCode.execute(appVars, regplnbIO);
		if (isNE(regplnbIO.getStatuz(),varcom.oK)
		&& isNE(regplnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regplnbIO.getParams());
			syserrrec.statuz.set(regplnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(greversrec.chdrcoy,regplnbIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,regplnbIO.getChdrnum())
		|| isNE(greversrec.life,regplnbIO.getLife())
		|| isNE(greversrec.coverage,regplnbIO.getCoverage())
		|| isNE(greversrec.rider,regplnbIO.getRider())
		|| isNE(greversrec.planSuffix,regplnbIO.getPlanSuffix())) {
			regplnbIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void deleteRegp2400()
	{
		/*STARTS*/
		regplnbIO.setFunction(varcom.deltd);
		regplnbIO.setFormat(regplnbrec);
		SmartFileCode.execute(appVars, regplnbIO);
		if (isNE(regplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regplnbIO.getParams());
			syserrrec.statuz.set(regplnbIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void rewriteRegp2500()
	{
		/*STARTS*/
		/*EXIT*/
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
