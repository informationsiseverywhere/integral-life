package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:45
 * Description:
 * Copybook name: ACMVREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvrevKey = new FixedLengthStringData(64).isAPartOf(acmvrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvrevRldgcoy = new FixedLengthStringData(1).isAPartOf(acmvrevKey, 0);
  	public FixedLengthStringData acmvrevRdocnum = new FixedLengthStringData(9).isAPartOf(acmvrevKey, 1);
  	public PackedDecimalData acmvrevTranno = new PackedDecimalData(5, 0).isAPartOf(acmvrevKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(acmvrevKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}