package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6633
 * @version 1.0 generated on 30/08/09 06:54
 * @author Quipoz
 */
public class S6633ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(375);
	public FixedLengthStringData dataFields = new FixedLengthStringData(87).isAPartOf(dataArea, 0);
	public FixedLengthStringData loanAnnivInterest = DD.annintloan.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData policyAnnivInterest = DD.annintpoly.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData annloan = DD.annloan.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData annpoly = DD.annpoly.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData compfreq = DD.compfreq.copy().isAPartOf(dataFields,5);
	public ZonedDecimalData day = DD.day.copyToZonedDecimal().isAPartOf(dataFields,7);
	public ZonedDecimalData interestDay = DD.interday.copyToZonedDecimal().isAPartOf(dataFields,9);
	public FixedLengthStringData interestFrequency = DD.intfreq.copy().isAPartOf(dataFields,11);
	public ZonedDecimalData intRate = DD.intrat.copyToZonedDecimal().isAPartOf(dataFields,13);
	public FixedLengthStringData inttype = DD.inttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,22);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,30);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,38);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,46);
	public ZonedDecimalData mperiod = DD.mperiod.copyToZonedDecimal().isAPartOf(dataFields,76);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,80);
	public ZonedDecimalData nofDay = DD.nofdigits.copyToZonedDecimal().isAPartOf(dataFields,85);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 87);
	public FixedLengthStringData annintloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData annintpolyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData annloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData annpolyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData compfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData dayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData interdayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData intfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData intratErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData inttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mperiodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData nofDayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 159);
	public FixedLengthStringData[] annintloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] annintpolyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] annloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] annpolyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] compfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] dayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] interdayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] intfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] intratOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] inttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mperiodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] nofDayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6633screenWritten = new LongData(0);
	public LongData S6633protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6633ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(intratOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(inttypeOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mperiodOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dayOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(compfreqOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annpolyOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annloanOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annintpolyOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annintloanOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(intfreqOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(interdayOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nofDayOut,new String[] {null ,null, null ,"14", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, intRate, nofDay, inttype, mperiod, day, compfreq, annpoly, annloan, policyAnnivInterest, loanAnnivInterest, interestFrequency, interestDay};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, intratOut, nofDayOut, inttypeOut, mperiodOut, dayOut, compfreqOut, annpolyOut, annloanOut, annintpolyOut, annintloanOut, intfreqOut, interdayOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, intratErr, nofDayErr, inttypeErr, mperiodErr, dayErr, compfreqErr, annpolyErr, annloanErr, annintpolyErr, annintloanErr, intfreqErr, interdayErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6633screen.class;
		protectRecord = S6633protect.class;
	}

}
