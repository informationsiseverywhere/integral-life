/*
 * File: P6350.java
 * Date: 30 August 2009 0:44:17
 * Author: Quipoz Limited
 * 
 * Class transformed from P6350.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;


import java.util.List; //ILB-456

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;//ILIFE-7968
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.screens.S6350ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILIFE-7968
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

//ILB-456 start 
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                   P6350 - POLICY SELECTION.
*                   ~~~~~~~~~~~~~~~~~~~~~~~~~
*Initialise
*----------
*
*  Skip this  section  if  returning  from an optional selection
*  (current stack position action flag = '*').
*
*  Clear the subfile ready for loading.
*
*  The  details  of  the  contract  being  enquired upon will be
*  stored in the CHDRMJA  I/O  module.  Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       The joint  owner's  client (CLTS) details if they exist.
*
*  This  program  will  display  all  of the Plan Suffixes for a
*  given  Plan. This will entail 'breaking out' from the summary
*  record  in  the  case  of Plan Processing and generating Plan
*  Suffixes for display in descending order.
*
*  The  Risk Status code and description and Premium Status Code
*  and  description  from  the  first Coverage on the first Life
*  will be displayed for each Plan Suffix.
*
*  Load the subfile as follows:
*
*       The first record will have Plan suffix '0000', this will
*       be  for a selection  of the whole  Plan.  Statuses  will
*       relate to the summarised COVRMJA record read  with  plan
*       suffix = '0000'. Do NOT load  if  WSSP-Flag = 'N' or 'J'
*       and POLSUM not equal to POLINC (CHDRMJA).
*
*       Next subtract  the  header  policies summarised (POLSUM)
*       from the policies  within  the  plan  (POLINC)  giving a
*       factor which will be  the  total  number of reads on the
*       cover file (COVRMJA).
*
*       Read the  first COVRMJA record on  the  contract,  using
*       CHDRMJA-POLINC in Plan Suffix, '01'in the life field and
*       a function of BEGN. Check for change of key.
*
*       Obtain the long description for the coverage risk status
*       STATCODE,  from  T5682  and  the  premium  status  code,
*       PSTATCODE, from T5681 and display them.
*
*       Repeat  this process  subtracting 1 from the plan suffix
*       until the number of  times  specified has  been  reached
*       according to the factor calculated  above. At this point
*       read the  file once more with a plan suffix of '0000' to
*       obtain the details  associated  with  all the summarised
*       records.
*
*       On each time through store the life number and write out
*       the details.  When reaching  the  summarised record, the
*       details  read (plan suffix = '0000') will pertain to all
*       remaining policies.
*
*  Load all pages  required  in  the subfile and set the subfile
*  more indicator to no.
*
*Validation
*----------
*
*  There  is no validation required as any character may be used
*  to select a policy.
*
*Updating
*--------
*
*  There is no updating in this program.
*
*Next Program
*------------
*
*  If KILL   was requested move spaces to  the  current  program
*  position and action field, add 2 to the  program  pointer and
*  exit.
*
*  At  this  point  the program will be either searching for the
*  FIRST  selected  record  in  order  to  pass  control  to the
*  appropriate   generic   enquiry   program  for  the  selected
*  Plan or it will be returning from the Component Select screen
*  after  selecting  some  details  and  searching  for the NEXT
*  selected Plan record (Stack action flag = '*').
*
*  It will be able  to determine which of these two states it is
*  in by examining the Stack Action Flag.
*
*  If  returning  from  an  enquiry, move  spaces to the current
*  WSSP-SEC-ACTN field and the Select field.
*
*  Continue  reading  the  subfile  records  using the Read Next
*  Changed  Subfile  function,  (SRNCH) until end of file or the
*  Select field is non-blank.
*
*  If nothing was selected or there are no  more  selections  to
*  process,  continue  by  moving blanks to  the  current  Stack
*  Action field and program position then exit.
*
*  If  a  selection has been found  perform a RLSE on COVRMJA to
*  ensure that any previously held record is freed up.
*
*  It  is  essential that the plan suffix of the selected record
*  is  passed  across  to  the Component Enquiry program even if
*  this is a  'generated'  Plan  Suffix from the summary record.
*  Use the key of  the  selected  record  to  perform a READR on
*  COVRMJA. If the selected Plan  Suffix  is  not  greater  than
*  CHDR-POLSUM then use a value of zero in the Plan Suffix field
*  for the READR.
*
*  Then move in to COVRMJA-PLAN-SUFFIX the actual value of the
*  Suffix  that  was  selected  by the user and perform a KEEPS.
*  This  will then store the chosen Plan Suffix in the COVRMJAIO
*  module.
*
*  Add 1 to the program pointer and exit.
*
*Notes.
*------
*
*  Tables Used:
*
*T3588 - Contract Premium Status        Key: PSTATCODE  (CHDR)
*T3623 - Contract Risk Status           Key: STATCODE   (CHDRMJA)
*T5681 - Coverage Premium Status        Key: PSTATCODE  (COVRMJA)
*T5682 - Coverage Risk Status           Key: STATCODE   (COVRMJA)
*T5688 - Contract Structure             Key: CNTTYPE
*
*
*****************************************************************
* </pre>
*/
public class P6350 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6350");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaPoliciesProtected = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaSelected = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaActualWritten = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0);
	private String redirection = "(Redirection)";
	private String switch_var = "(Switch)     ";

	private FixedLengthStringData wsaaPlan = new FixedLengthStringData(1);
	private Validator planNotSelected = new Validator(wsaaPlan, " ");
	private Validator planSelected = new Validator(wsaaPlan, "Y");

	private FixedLengthStringData wsaaEofFlag = new FixedLengthStringData(1);
	private Validator wsaaEofExit = new Validator(wsaaEofFlag, "Y");
	private String wsaaCovStatExit = "";
	private String wsaaPremStatExit = "";
	private String wsaaCovStatus = "";
	private String wsaaPremStatus = "";

	private FixedLengthStringData wsaaTable = new FixedLengthStringData(300);
	private PackedDecimalData[] wsaaPlnSuff = PDArrayPartOfStructure(100, 4, 0, wsaaTable, 0);
	private PackedDecimalData suffSub1 = new PackedDecimalData(4, 0);
	private PackedDecimalData suffSub2 = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaValidSuff = new FixedLengthStringData(1).init("Y");
	private Validator validSuffix = new Validator(wsaaValidSuff, "Y");
	private FixedLengthStringData wsaaSecProg1 = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaSecProg2 = new FixedLengthStringData(5);
		/* ERRORS */
	private String g633 = "G633";
	private String g634 = "G634";
	private String h080 = "H080";
	private String e304 = "E304";
		/* TABLES */
	private String t5681 = "T5681";
	private String t5682 = "T5682";
	private String t5679 = "T5679";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t5688 = "T5688";
		/* FORMATS */
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatckey = new Batckey();
	private S6350ScreenVars sv = ScreenProgram.getScreenVars( S6350ScreenVars.class);
	
	//ILIFE-7968 start
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	//ILIFE-7968 end
	
	//ILB-456 start 
	 private Itempf itempf = new Itempf();
	 private ItemDAO itemDAO= getApplicationContext().getBean("itemDao",ItemDAO.class);
	 private Covrpf covrpf = new Covrpf();
	 private List<Covrpf> covrpfList = null;
	 private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	 private Lifepf lifepf = new Lifepf();
	 private List<Lifepf> LifepfList;
	 private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	 private Clntpf clntpf= new Clntpf();
	 private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	 private List<Clntpf> clntpfList;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		readPlanRecs1110, 
		exit1190, 
		addToSubfile1230, 
		exit1290, 
		suffix1295, 
		exit1295, 
		exit1298, 
		exit1390, 
		exit1490, 
		exit1590, 
		preExit, 
		exit2090, 
		exit2690, 
		exit3090, 
		exit4090, 
		exit4190, 
		exit4490
	}

	public P6350() {
		super();
		screenVars = sv;
		new ScreenModel("S6350", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntpf.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(clntpf.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(clntpf.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(clntpf.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntpf.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			retrvContract1020();
			loadStatii1030();
			subfileLoad1040();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			sv.textfield.set(SPACES);
			sv.textfieldOut[varcom.nd.toInt()].set("Y");
			sv.textfieldOut[varcom.bl.toInt()].set("N");
			goTo(GotoLabel.exit1090);
		}
		wsaaPlanSuffix.set(ZERO);
		wsaaPoliciesProtected.set(ZERO);
		wsaaSelected.set(ZERO);
		wsaaActualWritten.set(ZERO);
		wsaaSub.set(ZERO);
		initialize(wsaaTable);
		suffSub1.set(ZERO);
		suffSub2.set(ZERO);
		wsaaPlan.set(SPACES);
		wsspcomn.lastActn.set(SPACES);
		wsaaEofFlag.set(SPACES);
		wsaaCovStatExit = "N";
		wsaaPremStatExit = "N";
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.currfrom.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.numpols.set(ZERO);
	}

protected void retrvContract1020()
	{
	//ILIFE-7968 start
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction("RETRV");
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		//ILIFE-7968 end
		if (isEQ(wsaaBatckey.batcBatctrcde,"T551")) {
			sv.textfield.set(redirection);
			sv.textfieldOut[varcom.bl.toInt()].set("Y");
			sv.textfieldOut[varcom.nd.toInt()].set("N");
		}
		else {
			if (isEQ(wsaaBatckey.batcBatctrcde,"T676")) {
				sv.textfield.set(switch_var);
				sv.textfieldOut[varcom.bl.toInt()].set("Y");
				sv.textfieldOut[varcom.nd.toInt()].set("N");
			}
			else {
				sv.textfield.set(SPACES);
				sv.textfieldOut[varcom.bl.toInt()].set("N");
				sv.textfieldOut[varcom.nd.toInt()].set("Y");
			}
		}
		//ILIFE-7979
		if (isEQ(chdrpf.getPolinc(),1)) {
			goTo(GotoLabel.exit1090);
		}
	}

protected void loadStatii1030()
	{
		//ILB-456
		//itemIO.setDataKey(SPACES);
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			fatalError600();
		}
		/*itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}*/
		t5679rec.t5679Rec.set(itempf.getGenarea());
	}

protected void subfileLoad1040()
	{
		scrnparams.subfileRrn.set(1);
		sv.planSuffix.set(ZERO);
		sv.rstatdesc.set("Select Whole Plan  ");
		if (isEQ(wsspcomn.flag,"N")
		|| isEQ(wsspcomn.flag,"J")
		|| isEQ(wsspcomn.flag,"M")
		|| isEQ(wsspcomn.flag,"L")) {
			if (isNE(chdrpf.getPolsum(),chdrpf.getPolinc())) {
				sv.selectOut[varcom.pr.toInt()].set("Y");
			}
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.selectOut[varcom.pr.toInt()].set("N");
		wsaaEofFlag.set("N");
		//ILB-456
		//covrmjaIO.setDataArea(SPACES);
		covrpf.setChdrcoy(wsspcomn.company.toString());
		covrpf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
		covrpf.setLife("01");
		covrpf.setCoverage("01");
		covrpf.setRider("00");
		covrpf.setPlanSuffix(9999);
		covrpf=covrpfDAO.getCovrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),"1");
		if (covrpf == null) {
			fatalError600();
		}
		/*covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("LIFE", "COVERAGE", "RIDER");*/
		while ( !(wsaaEofExit.isTrue())) {
			loadCovrSubfile1100();
		}
		
		loadHeader1500();
	}

protected void loadCovrSubfile1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case readPlanRecs1110: {
					readPlanRecs1110();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readPlanRecs1110()
	{
		int covrpfCount;
		/*SmartFileCode.execute(appVars, covrmjaIO);
		if ((isNE(covrmjaIO.getStatuz(),varcom.oK))
		&& (isNE(covrmjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		for(covrpfCount=0;covrpfCount<covrpfList.size();covrpfCount++)
		{

			if(covrpfList.isEmpty()) {
				fatalError600();
			}
			if (isNE(covrpf.getLife(),"01")
					&& isNE(covrpf.getCoverage(),"01")
					&& isNE(covrpf.getRider(),"00")) {
						//covrmjaIO.setFunction(varcom.nextr);
						goTo(GotoLabel.readPlanRecs1110);
					}
		}
		//ILIFE-7979 start
		/*if (isNE(covrpf.getLife(),"01")
		&& isNE(covrpf.getCoverage(),"01")
		&& isNE(covrpf.getRider(),"00")) {
			covrmjaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readPlanRecs1110);
		}*/
		if (isNE(wsspcomn.company,covrpf.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),covrpf.getChdrnum())
		|| (covrpfList.size() ==0)) {
			wsaaEofFlag.set("Y");
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			wsaaPlanSuffix.set(chdrpf.getPolsum());
			for (wsaaPlanSuffix.set(chdrpf.getPolsum()); !(isEQ(wsaaPlanSuffix,ZERO)); wsaaPlanSuffix.add(-1)){
				statiiToSubfile1200();
			}
		}
		//ILIFE-7979 end
		else {
			wsaaPlanSuffix.set(covrpf.getPlanSuffix());
			statiiToSubfile1200();
		}
		covrpfCount++;
		//covrmjaIO.setFunction(varcom.nextr);
	}

protected void statiiToSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					covrStatusDesc1210();
					premStatusDesc1220();
				}
				case addToSubfile1230: {
					addToSubfile1230();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covrStatusDesc1210()
	{
		wsaaCovStatExit = "N";
		wsaaPremStatExit = "N";
		wsaaCovStatus = "N";
		wsaaPremStatus = "N";
		sv.planSuffix.set(wsaaPlanSuffix);
		wsaaSub.set(ZERO);
		while ( !(isEQ(wsaaCovStatExit,"Y"))) {
			checkStatusCoverage1300();
		}
		
		descIO.setDescitem(covrpf.getStatcode());
		descIO.setDesctabl(t5682);
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.rstatdesc.fill("?");
		}
	}

protected void premStatusDesc1220()
	{
		wsaaSub.set(ZERO);
		while ( !(isEQ(wsaaPremStatExit,"Y"))) {
			checkStatusPremium1400();
		}
		
		descIO.setDescitem(covrpf.getPstatcode());
		descIO.setDesctabl(t5681);
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.pstatdesc.fill("?");
		}
		suffSub2.set(ZERO);
		wsaaValidSuff.set("N");
		checkSuffix1295();
		if (isEQ(wsaaCovStatus,"Y")
		&& isEQ(wsaaPremStatus,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set(SPACES);
			if (validSuffix.isTrue()) {
				subfileUpdate1297();
				goTo(GotoLabel.exit1290);
			}
			else {
				goTo(GotoLabel.addToSubfile1230);
			}
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (validSuffix.isTrue()) {
			goTo(GotoLabel.exit1290);
		}
	}

protected void addToSubfile1230()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaActualWritten.add(1);
		}
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		suffSub1.add(1);
		wsaaPlnSuff[suffSub1.toInt()].set(wsaaPlanSuffix);
	}

protected void checkSuffix1295()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case suffix1295: {
					suffix1295();
				}
				case exit1295: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void suffix1295()
	{
		suffSub2.add(1);
		if (isEQ(wsaaPlnSuff[suffSub2.toInt()],ZERO)) {
			goTo(GotoLabel.exit1295);
		}
		if (isEQ(wsaaPlanSuffix,wsaaPlnSuff[suffSub2.toInt()])) {
			wsaaValidSuff.set("Y");
			goTo(GotoLabel.exit1295);
		}
		if (isLT(suffSub2,100)) {
			goTo(GotoLabel.suffix1295);
		}
	}

protected void subfileUpdate1297()
	{
		subfileBegin1297();
	}

protected void subfileBegin1297()
	{
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			readSubfile1298();
		}
		
		scrnparams.function.set(varcom.supd);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readSubfile1298()
	{
		try {
			subfileRead1298();
		}
		catch (GOTOException e){
		}
	}

protected void subfileRead1298()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.planSuffix,wsaaPlanSuffix)) {
			goTo(GotoLabel.exit1298);
		}
		scrnparams.statuz.set(varcom.endp);
		sv.covRiskStat.set(covrpf.getStatcode());
		sv.covPremStat.set(covrpf.getPstatcode());
		descIO.setDescitem(covrpf.getStatcode());
		descIO.setDesctabl(t5682);
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.rstatdesc.fill("?");
		}
		descIO.setDescitem(covrpf.getPstatcode());
		descIO.setDesctabl(t5681);
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.pstatdesc.fill("?");
		}
	}

protected void checkStatusCoverage1300()
	{
		try {
			search1310();
		}
		catch (GOTOException e){
		}
	}

protected void search1310()
	{
		wsaaSub.add(1);
		sv.covRiskStat.set(covrpf.getStatcode());
		if (isGT(wsaaSub,12)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaCovStatExit = "Y";
		}
		else {
			if (isNE(covrpf.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.exit1390);
			}
			else {
				sv.covRiskStat.set(t5679rec.covRiskStat[wsaaSub.toInt()]);
				wsaaCovStatus = "Y";
				wsaaCovStatExit = "Y";
			}
		}
	}

protected void checkStatusPremium1400()
	{
		try {
			search1410();
		}
		catch (GOTOException e){
		}
	}

protected void search1410()
	{
		wsaaSub.add(1);
		sv.covPremStat.set(covrpf.getPstatcode());
		if (isGT(wsaaSub,12)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaPremStatExit = "Y";
		}
		else {
			if (isNE(covrpf.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.exit1490);
			}
			else {
				sv.covPremStat.set(t5679rec.covPremStat[wsaaSub.toInt()]);
				wsaaPremStatus = "Y";
				wsaaPremStatExit = "Y";
			}
		}
	}

protected void loadHeader1500()
	{
		try {
			headings1510();
			headingsContd1520();
			contractStatus1530();
			premiumStatus1540();
			lifeDetails1550();
			readJlife1560();
		}
		catch (GOTOException e){
		}
	}

protected void headings1510()
	{
	//ILIFE-7979 start
		sv.chdrnum.set(chdrpf.getChdrnum()); /* IJTI-1479 */
		sv.cnttype.set(chdrpf.getCnttype()); /* IJTI-1479 */
		descIO.setDescitem(chdrpf.getCnttype()); /* IJTI-1479 */
		descIO.setDesctabl(t5688);
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
	}

protected void headingsContd1520()
	{
		sv.numpols.set(chdrpf.getPolinc());
		sv.payfreq.set(chdrpf.getBillfreq());
		sv.mop.set(chdrpf.getBillchnl());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.currfrom.set(chdrpf.getCcdate());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());
	}
//ILIFE-7979 end
protected void contractStatus1530()
	{
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());//ILIFE-7979
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		else {
			sv.chdrstatus.fill("?");
		}
	}

protected void premiumStatus1540()
	{
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());//ILIFE-7979
		callDescio1700();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.premstatus.set(descIO.getShortdesc());
		}
		else {
			sv.premstatus.fill("?");
		}
	}

protected void lifeDetails1550()
	{
		//ILB-456
		//lifemjaIO.setDataArea(SPACES);
		lifepf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
		lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7979
		lifepf.setLife("01");
		lifepf.setJlife("00");
		LifepfList = lifepfDAO.selectLifepfRecord(lifepf);
		if(LifepfList == null) {
			readJlife1560();
			goTo(GotoLabel.exit1590);
		}
		/*lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			readJlife1560();
			goTo(GotoLabel.exit1590);
		}*/
		sv.lifenum.set(lifepf.getLifcnum());
		clntpf.setClntnum(lifepf.getLifcnum()); /* IJTI-1479 */
		callCltsio1600();
		//ILB-456
		if(clntpfList==null || (isNE(clntpf.getValidflag(),1)))
		{
			sv.jlifenameErr.set(e304);
			sv.jlifename.set(SPACES);
		}
		/*if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.lifenameErr.set(e304);
			sv.lifename.set(SPACES);
		}*/
		else {
			plainname();
			sv.lifename.set(wsspcomn.longconfname);
		}
	}

protected void readJlife1560()
	{
		//ILB-456
		lifepf.setJlife("01");
		LifepfList = lifepfDAO.selectLifepfRecord(lifepf);
		if(LifepfList==null){
			sv.jlife.set("NONE");
			sv.jlifename.set("NONE");
			goTo(GotoLabel.exit1590);
		}
		/*lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if ((isNE(lifemjaIO.getStatuz(),varcom.oK))
		&& (isNE(lifemjaIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlife.set("NONE");
			sv.jlifename.set("NONE");
			goTo(GotoLabel.exit1590);
		}*/
		sv.jlife.set(lifepf.getLifcnum());
		clntpf.setClntnum(lifepf.getLifcnum()); /* IJTI-1479 */
		callCltsio1600();
		//ILB-456
		if(clntpfList==null || (isNE(clntpf.getValidflag(),1)))
		{
			sv.jlifenameErr.set(e304);
			sv.jlifename.set(SPACES);
		}
		/*if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.jlifenameErr.set(e304);
			sv.jlifename.set(SPACES);
		}*/
		else {
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
		}
	}

protected void callCltsio1600()
	{
		/*READ*/
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		/*cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		/*EXIT*/
	}

protected void callDescio1700()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsaaSelected.set(0);
		wsaaPlan.set(SPACES);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		//ILIFE-7979
		if (isEQ(chdrpf.getPolinc(),1)) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2010();
			checkForErrors2050();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		scrnparams.subfileRrn.set(1);
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSubfile2010()
	{
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (planSelected.isTrue()
		&& isGT(wsaaSelected,0)) {
			scrnparams.errorCode.set(g633);
			wsspcomn.edterror.set("Y");
		}
		if (planNotSelected.isTrue()
		&& isEQ(wsaaSelected,0)) {
			scrnparams.errorCode.set(g634);
			wsspcomn.edterror.set("Y");
		}
		//ILIFE-7979
		compute(wsaaPoliciesProtected, 0).set(sub(chdrpf.getPolinc(),wsaaActualWritten));
		if (isGT(chdrpf.getPolinc(),1)) {
			if (isEQ(wsaaPoliciesProtected,wsaaActualWritten)) {
				wsaaPlan.set("Y");
			}
		}
		if (planSelected.isTrue()) {
			wsspcomn.lastActn.set("L");
		}
		else {
			wsspcomn.lastActn.set("O");
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2600()
	{
		try {
			readNextRecord2610();
			validation2620();
			updateErrorIndicators2630();
		}
		catch (GOTOException e){
		}
	}

protected void readNextRecord2610()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void validation2620()
	{
		if (isNE(sv.select,SPACES)) {
			if (isEQ(scrnparams.subfileRrn,1)) {
				wsaaPlan.set("Y");
			}
			else {
				wsaaSelected.add(1);
			}
		}
		//ILIFE-7979
		if (isNE(sv.select,SPACES)
		&& isLTE(chdrpf.getPolinc(),1)) {
			scrnparams.errorCode.set(h080);
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateErrorIndicators2630()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2690);
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		goTo(GotoLabel.exit3090);
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
			allSelects4020();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			releaseSftlck4400();
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			initialEntry4100();
			if (isNE(sv.select,SPACES)) {
				sv.select.set(SPACES);
				goTo(GotoLabel.exit4090);
			}
		}
		//ILIFE-7979
		if (isEQ(chdrpf.getPolinc(),1)) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
				goTo(GotoLabel.exit4090);
			}
			else {
				wsspcomn.secProg[2].set(wsaaSecProg1);
				wsspcomn.secProg[3].set(wsaaSecProg2);
				wsaaSecProg1.set(SPACES);
				wsaaSecProg2.set(SPACES);
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wsspcomn.programPtr.add(1);
				goTo(GotoLabel.exit4090);
			}
		}
	}

protected void allSelects4020()
	{
		if (isEQ(sv.select,SPACES)) {
			while ( !(isNE(sv.select,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4300();
			}
			
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			wsspcomn.secProg[2].set(wsaaSecProg1);
			wsspcomn.secProg[3].set(wsaaSecProg2);
			wsaaSecProg1.set(SPACES);
			wsaaSecProg2.set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		keepsCovrmja4200();
		sv.select.set(SPACES);
	}

protected void initialEntry4100()
	{
		try {
			saveNextProgs4110();
			startOfSubfile4120();
		}
		catch (GOTOException e){
		}
	}

protected void saveNextProgs4110()
	{
		wsaaSecProg1.set(wsspcomn.secProg[3]);
		wsaaSecProg2.set(wsspcomn.secProg[4]);
		wsspcomn.secProg[3].set(SPACES);
		wsspcomn.secProg[4].set(SPACES);
		/*ONE-POLICY*/
		//ILIFE-7979
		if (isEQ(chdrpf.getPolinc(),1)) {
			sv.planSuffix.set(ZERO);
			keepsCovrmja4200();
			goTo(GotoLabel.exit4190);
		}
	}

protected void startOfSubfile4120()
	{
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.select,SPACES)) {
			keepsCovrmja4200();
		}
	}

protected void keepsCovrmja4200()
	{
		go4210();
	}

protected void go4210()
	{
		//ILB-456
		//covrmjaIO.setDataArea(SPACES);
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())) {
			covrpf.setPlanSuffix(0);
		}
		else {
			covrpf.setPlanSuffix(sv.planSuffix.toInt());
		}
		covrpf.setChdrcoy(wsspcomn.company.toString());
		covrpf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
		covrpf.setLife("01");
		covrpf.setCoverage("00");
		covrpf.setRider("00");
		covrpfList=covrpfDAO.getCovrsurByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		if(covrpfList != null && covrpfList.size() > 0)
		{
			for(Covrpf covr : covrpfList)
			{
				if (covr == null) {
					fatalError600();
				}
				if (isNE(wsspcomn.company,covr.getChdrcoy())
						|| isNE(chdrpf.getChdrnum(),covr.getChdrnum())
						|| (covrpf == null)) {
							fatalError600();
						}
				covr.setPlanSuffix(sv.planSuffix.toInt());
				covrpfDAO.setCacheObject(covr);
			}
		}
		/*covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrmjaIO);
		if ((isNE(covrmjaIO.getStatuz(),varcom.oK))
		&& (isNE(covrmjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		//ILIFE-7979
		if (isNE(wsspcomn.company,covrpf.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),covrpf.getChdrnum())
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		
		/*covrpf.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		

		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void readSubfile4300()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S6350", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void releaseSftlck4400()
	{
		try {
			unlockContract4410();
		}
		catch (GOTOException e){
		}
	}

protected void unlockContract4410()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit4490);
		}
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum()); /* IJTI-1479 */
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}
}