package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PrmhpfDAO extends BaseDAO<Prmhpf>{
	public Map<String, List<Prmhpf>> getPrmhRecordByTodate(String chdrcoy, List<String> chdrnumList, int todate);
	public Prmhpf getPrmhRecord(String chdrcoy, String chdrnum);
	public Prmhpf getPrmhenqRecord(String chdrcoy, String chdrnum);
	public void insertPrmhenqRecord(Prmhpf prmhpf);
}