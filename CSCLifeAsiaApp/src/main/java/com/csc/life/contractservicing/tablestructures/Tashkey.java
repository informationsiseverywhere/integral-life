package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:36
 * Description:
 * Copybook name: TASHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tashkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData tashFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData tashKey = new FixedLengthStringData(64).isAPartOf(tashFileKey, 0, REDEFINE);
  	public FixedLengthStringData tashChdrcoy = new FixedLengthStringData(1).isAPartOf(tashKey, 0);
  	public FixedLengthStringData tashChdrnum = new FixedLengthStringData(8).isAPartOf(tashKey, 1);
  	public PackedDecimalData tashTranno = new PackedDecimalData(5, 0).isAPartOf(tashKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(tashKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tashFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tashFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}