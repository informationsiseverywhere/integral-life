package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for ST522
 * @version 1.0 generated on 30/08/09 07:27
 * @author Quipoz
 */
public class St522ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(88);
	public FixedLengthStringData dataFields = new FixedLengthStringData(24).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,11);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 24);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 40);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData St522screenWritten = new LongData(0);
	public LongData St522protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public St522ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trannoOut,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrsel, tranno, effdate, action};
		screenOutFields = new BaseData[][] {chdrselOut, trannoOut, effdateOut, actionOut};
		screenErrFields = new BaseData[] {chdrselErr, trannoErr, effdateErr, actionErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = St522screen.class;
		protectRecord = St522protect.class;
	}

}
