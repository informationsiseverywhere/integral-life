package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr51tscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 13;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 21, 2, 73}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51tScreenVars sv = (Sr51tScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr51tscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr51tscreensfl, 
			sv.Sr51tscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr51tScreenVars sv = (Sr51tScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr51tscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr51tScreenVars sv = (Sr51tScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr51tscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr51tscreensflWritten.gt(0))
		{
			sv.sr51tscreensfl.setCurrentIndex(0);
			sv.Sr51tscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr51tScreenVars sv = (Sr51tScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr51tscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr51tScreenVars screenVars = (Sr51tScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqnbr.setFieldName("seqnbr");
				screenVars.frmdateDisp.setFieldName("frmdateDisp");
				screenVars.todateDisp.setFieldName("todateDisp");
				screenVars.logtype.setFieldName("logtype");
				screenVars.activeInd.setFieldName("activeInd");
				screenVars.apind.setFieldName("apind");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.validflag.setFieldName("validflag");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.seqnbr.set(dm.getField("seqnbr"));
			screenVars.frmdateDisp.set(dm.getField("frmdateDisp"));
			screenVars.todateDisp.set(dm.getField("todateDisp"));
			screenVars.logtype.set(dm.getField("logtype"));
			screenVars.activeInd.set(dm.getField("activeInd"));
			screenVars.apind.set(dm.getField("apind"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.crtuser.set(dm.getField("crtuser"));
			screenVars.validflag.set(dm.getField("validflag"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr51tScreenVars screenVars = (Sr51tScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqnbr.setFieldName("seqnbr");
				screenVars.frmdateDisp.setFieldName("frmdateDisp");
				screenVars.todateDisp.setFieldName("todateDisp");
				screenVars.logtype.setFieldName("logtype");
				screenVars.activeInd.setFieldName("activeInd");
				screenVars.apind.setFieldName("apind");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.validflag.setFieldName("validflag");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("seqnbr").set(screenVars.seqnbr);
			dm.getField("frmdateDisp").set(screenVars.frmdateDisp);
			dm.getField("todateDisp").set(screenVars.todateDisp);
			dm.getField("logtype").set(screenVars.logtype);
			dm.getField("activeInd").set(screenVars.activeInd);
			dm.getField("apind").set(screenVars.apind);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("crtuser").set(screenVars.crtuser);
			dm.getField("validflag").set(screenVars.validflag);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr51tscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr51tScreenVars screenVars = (Sr51tScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.seqnbr.clearFormatting();
		screenVars.frmdateDisp.clearFormatting();
		screenVars.todateDisp.clearFormatting();
		screenVars.logtype.clearFormatting();
		screenVars.activeInd.clearFormatting();
		screenVars.apind.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.crtuser.clearFormatting();
		screenVars.validflag.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr51tScreenVars screenVars = (Sr51tScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.seqnbr.setClassString("");
		screenVars.frmdateDisp.setClassString("");
		screenVars.todateDisp.setClassString("");
		screenVars.logtype.setClassString("");
		screenVars.activeInd.setClassString("");
		screenVars.apind.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.crtuser.setClassString("");
		screenVars.validflag.setClassString("");
	}

/**
 * Clear all the variables in Sr51tscreensfl
 */
	public static void clear(VarModel pv) {
		Sr51tScreenVars screenVars = (Sr51tScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.seqnbr.clear();
		screenVars.frmdateDisp.clear();
		screenVars.frmdate.clear();
		screenVars.todateDisp.clear();
		screenVars.todate.clear();
		screenVars.logtype.clear();
		screenVars.activeInd.clear();
		screenVars.apind.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.crtuser.clear();
		screenVars.validflag.clear();
	}
}
