package com.csc.life.contractservicing.procedures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.dip.jvpms.web.VPMScaller;
import com.csc.fsu.general.dataaccess.model.Chdrpf;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.dataaccess.dao.impl.DescpfDAOImpl;
import com.csc.smart.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.AppVars;

//IBPLIFE-1488
public class UwoccupationUtilImpl implements UwoccupationUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(UwoccupationUtilImpl.class);
	private static final String UWOCCUPATION = "UWOCCUPATION";
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescpfDAO descpfDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAOImpl.class);
	private Map<String, Itempf> t5448Map;
	private Map<String, Itempf> th618Map;
	private Map<String, Descpf> t3628descMap;
	private Map<String, String> vpmsCacheMap = new HashMap<>();
	private static final String T5448 = "T5448";
	private static final String TH618 = "TH618";
	private static final String T3628 = "T3628";
	
	public List<String> getUnacceptableContracts(List<Chdrpf> contractList, String company,
			UWOccupationRec uwOccupationRec, String fsuco, UWOccupationRec origUWOccupationRec) {

		List<String> contractNoList = new ArrayList<>();
		if (contractList != null && !contractList.isEmpty()) {
			t5448Map = itempfDAO.getItemMap("IT", company, T5448);
			th618Map = itempfDAO.getItemMap("IT", company, TH618);
			t3628descMap = descpfDAO.getMapDescItems("IT", fsuco, T3628, "E");
			ExternalisedRules er = new ExternalisedRules();
			if (AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(UWOCCUPATION)) {
				for (Chdrpf chdrmjaIO : contractList) {
					checkEachChdrpf(chdrmjaIO, contractNoList, uwOccupationRec, origUWOccupationRec);
				}
			}
		}
		if(vpmsCacheMap != null) {
			vpmsCacheMap.clear();
		}
		if(t5448Map != null) {
			t5448Map.clear();
		}
		if(th618Map != null) {
			th618Map.clear();
		}
		return contractNoList;
	}
	
	private void checkEachChdrpf(Chdrpf chdrmjaIO, 
			List<String> contractNoList, UWOccupationRec uwOccupationRec, 
			UWOccupationRec origUWOccupationRec) {
		List<Covrpf> covrList = getCovrpfList(chdrmjaIO);
		if (covrList != null && !covrList.isEmpty()) {
			for (Covrpf covr : covrList) {
				String uwResult = checkAcceptCovrpf(chdrmjaIO.getCnttype(), covr.getCrtable(), uwOccupationRec);
				String origUWResult = checkAcceptCovrpf(chdrmjaIO.getCnttype(), covr.getCrtable(), origUWOccupationRec);
				if (checkHigherRisk(origUWResult, uwResult)) {
					contractNoList.add(chdrmjaIO.getChdrnum());
					break;
				}
			}
		}
	}
	private boolean checkHigherRisk(String origUWResult,String uwResult) {
		int origUWResultInt = getIntUWResult(origUWResult);
		int uwResultInt = getIntUWResult(uwResult);
		if(uwResultInt > origUWResultInt) {
			return true;
		}
		return false;
	}
	private int getIntUWResult(String vpmsResult) {
		if("Decline".equals(vpmsResult)) {
			return 2;
		}
		if("Conditionally Accepted".equals(vpmsResult)) {
				return 1;
			}
		return 0;
	}
	/**
	 * 2:Decline,1:Conditionally Accepted,0:Accepted
	 */
	public int getUWResult(UWOccupationRec uwOccupationRec, String company, String fsuco,
			String cnttype, String crtable) {

		String vpmsResult = getUWMessage(uwOccupationRec, company, fsuco,
				 cnttype, crtable);
		
		if("Decline".equals(vpmsResult)) {
			return 2;
		}
		if("Conditionally Accepted".equals(vpmsResult)) {
				return 1;
			}
		return 0;
	}
	
	private String getUWMessage(UWOccupationRec uwOccupationRec, String company, String fsuco,
			String cnttype, String crtable) {

		t5448Map = itempfDAO.getItemMap("IT", company, T5448);
		th618Map = itempfDAO.getItemMap("IT", company, TH618);
		t3628descMap = descpfDAO.getMapDescItems("IT", fsuco, T3628, "E");
		ExternalisedRules er = new ExternalisedRules();
		if (AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(UWOCCUPATION)) {
			return checkAcceptCovrpf(cnttype, crtable, uwOccupationRec);
		}
		return null;
	}

	private List<Covrpf> getCovrpfList(Chdrpf chdrmjaIO) {
		String chdrnum = chdrmjaIO.getChdrnum();
		Covrpf covrpfModel = new Covrpf();
		covrpfModel.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrpfModel.setChdrnum(chdrnum);
		return covrpfDAO.searchCovrRecordForContract(covrpfModel);
	}

	private FixedLengthStringData[] getRiskClass(String cnttype, String crtable) {
		String t5488item = cnttype + crtable;
		if (t5448Map != null && t5448Map.containsKey(t5488item)) {
			Itempf t5488 = t5448Map.get(t5488item);
			T5448rec t5448Rec = new T5448rec();
			t5448Rec.t5448Rec.set(StringUtil.rawToString(t5488.getGenarea()));
			String rsktyp = t5448Rec.rrsktyp.toString();
			if (th618Map != null && th618Map.containsKey(rsktyp)) {
				Itempf th618 = th618Map.get(rsktyp);
				Th618rec th618rec = new Th618rec();
				th618rec.th618Rec.set(StringUtil.rawToString(th618.getGenarea()));
				return th618rec.lrkcls;
			}
		}
		return new FixedLengthStringData[0];
	}

	private String checkAcceptCovrpf(String cnttype, String crtable, UWOccupationRec uwOccupationRec) {
		FixedLengthStringData[] lrkcls = getRiskClass(cnttype, crtable);
		for (FixedLengthStringData rkcls : lrkcls) {
			if (rkcls != null && !rkcls.toString().trim().isEmpty()) {
				String industryType = getIndustryDesc(uwOccupationRec.indusType.toString().trim());
				if (industryType != null && !industryType.isEmpty()) {
					StringBuilder sb = new StringBuilder();
					sb.append(cnttype);
					sb.append(uwOccupationRec.transEffdate.toString());
					sb.append(crtable);
					sb.append(uwOccupationRec.occupCode.toString());
					sb.append(industryType);
					sb.append(rkcls);
					return getUWOccupationRec(sb.toString(), cnttype, crtable, uwOccupationRec, industryType, rkcls);
				}
			}
		}
		return null;
	}

	private String getUWOccupationRec(String uwKey, String cnttype, String crtable, UWOccupationRec uwOccupationRec,
			String industryType, FixedLengthStringData rkcls) {
		String outputUWDec = null;
		if (vpmsCacheMap.containsKey(uwKey)) {
			outputUWDec = vpmsCacheMap.get(uwKey);
			return outputUWDec;
		} else {
			UWOccupationRec calcUWrec = new UWOccupationRec();
			calcUWrec.cnttype.set(cnttype);
			calcUWrec.transEffdate.set(uwOccupationRec.transEffdate);
			calcUWrec.crtable.set(crtable);
			calcUWrec.occupCode.set(uwOccupationRec.occupCode);
			calcUWrec.indusType.set(industryType);
			calcUWrec.riskClass.set(rkcls);
			vpmsCall(UWOCCUPATION, calcUWrec.calcuwRec);
			outputUWDec = calcUWrec.outputUWDec.toString().trim();
			vpmsCacheMap.put(uwKey, outputUWDec);
			return outputUWDec;
		}
	}
	private String getIndustryDesc(String code) {
		if (t3628descMap.containsKey(code)) {
			return t3628descMap.get(code).getLongdesc().trim();
		}
		return null;
	}

	private int vpmsCall(String whatToCallName, FixedLengthStringData calcUWrec) {

		LOGGER.info("CALLING VPMS FOR ={}", whatToCallName);
		VPMScaller caller = new VPMScaller(0, whatToCallName, calcUWrec);
		caller.mapRequestToBean(calcUWrec, 0);
		int status = caller.callVpms();
		LOGGER.info("VPMS RETURN STATUS={}", status);
		if (status == 0) {
			caller.mapResponseToBean(calcUWrec, 0);
		}
		if (status == 1) {
			caller.getErrorCode(calcUWrec);
		}
		return (status);
	}

	private ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}

	@Override
	public UWOccupationRec getUWOccupationRec(UWOccupationRec uwOccupationRec,String company, String fsuco, String cnttype, String crtable) {
		t5448Map = itempfDAO.getItemMap("IT", company, T5448);
		th618Map = itempfDAO.getItemMap("IT", company, TH618);
		t3628descMap = descpfDAO.getMapDescItems("IT", fsuco, T3628, "E");
		FixedLengthStringData[] lrkcls = getRiskClass(cnttype, crtable);
		for (FixedLengthStringData rkcls : lrkcls) {
			if (rkcls != null && !rkcls.toString().trim().isEmpty()) {
				String industryType = getIndustryDesc(uwOccupationRec.indusType.toString().trim());
				if (industryType != null && !industryType.isEmpty()) {
					StringBuilder sb = new StringBuilder();
					sb.append(cnttype);
					sb.append(uwOccupationRec.transEffdate.toString());
					sb.append(crtable);
					sb.append(uwOccupationRec.occupCode.toString());
					sb.append(industryType);
					sb.append(rkcls);
					
					UWOccupationRec calcUWrec = new UWOccupationRec();
					calcUWrec.cnttype.set(cnttype);
					calcUWrec.transEffdate.set(uwOccupationRec.transEffdate);
					calcUWrec.crtable.set(crtable);
					calcUWrec.occupCode.set(uwOccupationRec.occupCode);
					calcUWrec.indusType.set(industryType);
					calcUWrec.riskClass.set(rkcls);
					vpmsCall(UWOCCUPATION, calcUWrec.calcuwRec);
					return calcUWrec;
				}
			}
		}
		return null;
	}


}
