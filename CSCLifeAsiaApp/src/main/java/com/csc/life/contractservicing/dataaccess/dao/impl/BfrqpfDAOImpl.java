/******************************************************************************
 * File Name 		: BfrqpfDAOImpl.java
 * Author			: smalchi2
 * Creation Date	: 04 January 2017
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for BFRQPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.BfrqpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Bfrqpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class BfrqpfDAOImpl extends BaseDAOImpl<Bfrqpf> implements BfrqpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(BfrqpfDAOImpl.class);

	public List<Bfrqpf> searchBfrqpfRecord(Bfrqpf bfrqpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, CNTTYPE, CURRENCY, FREQ, ");
		sqlSelect.append("BILLFREQ, INSTPRAMT, INSTPREM, EFFDATE, VALIDFLAG, ");
		sqlSelect.append("USRPRF, JOBNM, DATIME ");
		sqlSelect.append("FROM BFRQPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND EFFDATE = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, EFFDATE ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psBfrqpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlBfrqpfRs = null;
		List<Bfrqpf> outputList = new ArrayList<Bfrqpf>();

		try {

			psBfrqpfSelect.setString(1, bfrqpf.getChdrcoy());
			psBfrqpfSelect.setString(2, bfrqpf.getChdrnum());
			psBfrqpfSelect.setInt(3, bfrqpf.getEffdate());

			sqlBfrqpfRs = executeQuery(psBfrqpfSelect);

			while (sqlBfrqpfRs.next()) {

				Bfrqpf bfrqpfnew = new Bfrqpf();

				bfrqpfnew.setUniqueNumber(sqlBfrqpfRs.getInt("UNIQUE_NUMBER"));
				bfrqpfnew.setChdrcoy(sqlBfrqpfRs.getString("CHDRCOY"));
				bfrqpfnew.setChdrnum(sqlBfrqpfRs.getString("CHDRNUM"));
				bfrqpfnew.setCnttype(sqlBfrqpfRs.getString("CNTTYPE"));
				bfrqpfnew.setCurrency(sqlBfrqpfRs.getString("CURRENCY"));
				bfrqpfnew.setFreq(sqlBfrqpfRs.getString("FREQ"));
				bfrqpfnew.setBillfreq(sqlBfrqpfRs.getString("BILLFREQ"));
				bfrqpfnew.setInstpramt(sqlBfrqpfRs.getBigDecimal("INSTPRAMT"));
				bfrqpfnew.setInstprem(sqlBfrqpfRs.getBigDecimal("INSTPREM"));
				bfrqpfnew.setEffdate(sqlBfrqpfRs.getInt("EFFDATE"));
				bfrqpfnew.setValidflag(sqlBfrqpfRs.getString("VALIDFLAG"));
				bfrqpfnew.setUsrprf(sqlBfrqpfRs.getString("USRPRF"));
				bfrqpfnew.setJobnm(sqlBfrqpfRs.getString("JOBNM"));
				bfrqpfnew.setDatime(sqlBfrqpfRs.getDate("DATIME"));

				outputList.add(bfrqpfnew);
			}

		} catch (SQLException e) {
			LOGGER.error("searchBfrqpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psBfrqpfSelect, sqlBfrqpfRs);
		}

		return outputList;
	}
	
	public List<Bfrqpf> getAll(int batchExtractSize, int batchID ) {

        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.UNIQUE_NUMBER)-1)/?) ROWNM, TE.* FROM BFRQ");
        sqlStr.append(" TE");
        sqlStr.append(")cc WHERE cc.ROWNM = ?");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		List<Bfrqpf> pfList = new ArrayList<Bfrqpf>();
		ResultSet rs = null;
        try {
        	ps.setInt(1, batchExtractSize);
        	ps.setInt(2, batchID);
        	rs = ps.executeQuery();
            while (rs.next()) {
            	Bfrqpf pf = new Bfrqpf();
            	pf.setChdrcoy(rs.getString("CHDRCOY"));
            	pf.setChdrnum(rs.getString("CHDRNUM"));
            	pf.setCnttype(rs.getString("CNTTYPE"));
            	pf.setCurrency(rs.getString("CURRENCY"));
            	pf.setFreq(rs.getString("FREQ"));
            	pf.setBillfreq(rs.getString("BILLFREQ"));
            	pf.setInstpramt(rs.getBigDecimal("INSTPRAMT"));
            	pf.setInstprem(rs.getBigDecimal("INSTPREM"));
            	pf.setEffdate(rs.getInt("EFFDATE"));
            	pf.setValidflag(rs.getString("VALIDFLAG"));
            	pf.setUsrprf(rs.getString("USRPRF"));
            	pf.setJobnm(rs.getString("JOBNM"));
            	pf.setDatime(rs.getDate("DATIME"));
            	pf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
            	pfList.add(pf);
            }

        } catch (SQLException e) {
            LOGGER.error("getAll()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return pfList;
	}

	public void updateBfrqpfInvalid(List<Bfrqpf> bfrqpfList) {
		String sqlStr = "UPDATE BFRQPF SET VALIDFLAG='2', JOBNM=?, USRPRF=?, DATIME=? WHERE UNIQUE_NUMBER=? ";
		PreparedStatement ps = getPrepareStatement(sqlStr);
		try {
			for (Bfrqpf c : bfrqpfList) {
				ps.setString(1, getJobnm());
				ps.setString(2, getUsrprf());
				ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
				ps.setLong(4, c.getUniqueNumber());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateBfrqpfInvalid()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
}
