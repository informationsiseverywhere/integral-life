/*
 * File: Grevanny.java
 * Date: 29 August 2009 22:50:44
 * Author: Quipoz Limited
 * 
 * Class transformed from GREVANNY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.AnnyrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This  is  a  new  Subroutine & forms part of the 9405 Annuities
* Development. It is  called  upon  via T5671 Generic Processing.
*
* It  will  be  called  using  the  standard   generic   reversal
* subroutine  linkage GREVERSREC. ANNY annuity record maintenance
* is  performed  by  the  standard Valid-Flag '1'/'2' processing.
* The  purpose  of  this  Subroutine  is  to  delete  the current
* Valid-Flag  '1'  ANNY  and reinstate the earlier Valid-Flag '2'
* records, based on a match of TRANNO.
*
* A  new  logical,  ANNYREV,  is used  in  this  subroutine. This
* Logical  contains  both  a  Descending  Key  and  a Transaction
* Number, which cuts down on the logic processing involved.
*
*****************************************************************
* </pre>
*/
public class Grevanny extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "GREVANNY";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private static final String annyrec = "ANNYREC";
	private static final String annyrevrec = "ANNYREVREC";
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private AnnyrevTableDAM annyrevIO = new AnnyrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Greversrec greversrec = new Greversrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		error690
	}

	public Grevanny() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		greversrec.statuz.set(varcom.oK);
		processAnny2000();
		/*EXIT*/
		exitProgram();
	}

protected void processAnny2000()
	{
		start2010();
	}

protected void start2010()
	{
		annyIO.setParams(SPACES);
		annyIO.setChdrcoy(greversrec.chdrcoy);
		annyIO.setChdrnum(greversrec.chdrnum);
		annyIO.setLife(greversrec.life);
		annyIO.setCoverage(greversrec.coverage);
		annyIO.setRider(greversrec.rider);
		annyIO.setPlanSuffix(greversrec.planSuffix);
		annyIO.setFormat(annyrec);
		annyIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		annyIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isNE(greversrec.chdrcoy,annyIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,annyIO.getChdrnum())
		|| isNE(greversrec.life,annyIO.getLife())
		|| isNE(greversrec.coverage,annyIO.getCoverage())
		|| isNE(greversrec.rider,annyIO.getRider())
		|| isNE(greversrec.planSuffix,annyIO.getPlanSuffix())
		|| isEQ(annyIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		while ( !(isEQ(annyIO.getStatuz(),varcom.endp)
		|| isNE(annyIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(annyIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(greversrec.life,annyIO.getLife())
		|| isNE(greversrec.coverage,annyIO.getCoverage())
		|| isNE(greversrec.rider,annyIO.getRider())
		|| isNE(greversrec.planSuffix,annyIO.getPlanSuffix()))) {
			reverseAnnys3100();
		}
		
	}

protected void reverseAnnys3100()
	{
		/*START*/
		if (isEQ(annyIO.getTranno(),greversrec.tranno)
		&& isEQ(annyIO.getValidflag(),"1")) {
			deletRewrtAnnys3200();
		}
		else {
			annyIO.setFunction(varcom.nextr);
			annyIO.setFormat(annyrec);
			SmartFileCode.execute(appVars, annyIO);
			if (isNE(annyIO.getStatuz(),varcom.oK)
			&& isNE(annyIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(annyIO.getParams());
				syserrrec.statuz.set(annyIO.getStatuz());
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void deletRewrtAnnys3200()
	{
		start3200();
	}

protected void start3200()
	{
		annyIO.setFunction(varcom.deltd);
		annyIO.setFormat(annyrec);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		annyrevIO.setParams(SPACES);
		annyrevIO.setChdrcoy(greversrec.chdrcoy);
		annyrevIO.setChdrnum(greversrec.chdrnum);
		annyrevIO.setLife(greversrec.life);
		annyrevIO.setCoverage(greversrec.coverage);
		annyrevIO.setRider(greversrec.rider);
		annyrevIO.setPlanSuffix(greversrec.planSuffix);
		annyrevIO.setTranno(greversrec.tranno);
		annyrevIO.setFormat(annyrevrec);
		annyrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		annyrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		annyrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, annyrevIO);
		if (isNE(annyrevIO.getStatuz(),varcom.oK)
		&& isNE(annyrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(annyrevIO.getParams());
			syserrrec.statuz.set(annyrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(greversrec.chdrcoy,annyrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,annyrevIO.getChdrnum())
		|| isNE(greversrec.life,annyrevIO.getLife())
		|| isNE(greversrec.coverage,annyrevIO.getCoverage())
		|| isNE(greversrec.rider,annyrevIO.getRider())
		|| isNE(greversrec.planSuffix,annyrevIO.getPlanSuffix())
		|| isEQ(annyrevIO.getStatuz(),varcom.endp)) {
			annyrevIO.setStatuz(varcom.endp);
		}
		annyIO.setStatuz(varcom.endp);
		if (isEQ(annyrevIO.getStatuz(),varcom.oK)) {
			annyrevIO.setValidflag("1");
			annyrevIO.setFunction(varcom.writd);
			annyrevIO.setFormat(annyrevrec);
			SmartFileCode.execute(appVars, annyrevIO);
			if (isNE(annyrevIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(annyrevIO.getParams());
				syserrrec.statuz.set(annyrevIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fatalError601();
				case error690: 
					error690();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.error690);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void error690()
	{
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
