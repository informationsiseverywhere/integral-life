package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr51mscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr51mscreensfl";
		lrec.subfileClass = Sr51mscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 4;
		lrec.pageSubfile = 3;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51mScreenVars sv = (Sr51mScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr51mscreenctlWritten, sv.Sr51mscreensflWritten, av, sv.sr51mscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr51mScreenVars screenVars = (Sr51mScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.register.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.susamt.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.acdes.setClassString("");
		screenVars.osbal.setClassString("");
		screenVars.apind.setClassString("");
		screenVars.tolerance.setClassString("");
	}

/**
 * Clear all the variables in Sr51mscreenctl
 */
	public static void clear(VarModel pv) {
		Sr51mScreenVars screenVars = (Sr51mScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.register.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.billfreq.clear();
		screenVars.mop.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.susamt.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.acdes.clear();
		screenVars.osbal.clear();
		screenVars.apind.clear();
		screenVars.tolerance.clear();
	}
}
