/*
 * File: P5066.java
 * Date: 30 August 2009 0:01:27
 * Author: Quipoz Limited
 * 
 * Class transformed from P5066.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.financials.dataaccess.Rmrkpf;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.model.Agslpf;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.screens.S5066ScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifesurTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;



/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  P5066 - Surrender Reversal AT Submission.
*  -----------------------------------------
*
*  This  transaction,  Full    Surrender  Reversal  is  selected
*  from the Full Surrender sub-menu S5245/P5245.
*
* Initialise
* ----------
*
*  Read  the    Contract    header  (function  RETRV)  and  read
*  the  relevant  data  necessary  for  obtaining   the   status
*  description,   the   Owner,   CCD,   Paid-to-date   and   the
*  Bill-to-date.
*
*  Read  the  first/only  Surrender    Header  record  (SURHCLM)
*  for this contract.
*
*  LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To  obtain  the  life assured and joint-life details (if any)
*  do the following;-
*
*       - Obtain  the  life  details  using LIFESUR,(READR) (for
*            this  contract  number, using the life number/joint
*            life  number  from the SURHCLM record).  Format the
*            name accordingly.
*
*       - Obtain  the  joint-life  details using LIFESUR (READR)
*            (for   this   contract   number,   using  the  life
*            number/joint  life number from the SURHCLM record).
*            Format the name accordingly.
*
*  Obtain the  necessary  descriptions   by    calling  'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Format Name
*
*       Read the  client  details  record  and  use the relevant
*            copybook in order to format the required names.
*
*  Build the Coverage/Rider review details
*
*       Read the  Surrender  Claim  details record (SURDCLM) and
*            output  a  detail  line,  for  each record for this
*            contract, to the subfile and display the entries.
*
*       Sum the  Estimated  values  and  the  Actual amounts and
*            display them on the screen.
*
*       From the  Surrender  Claim  header display the following
*            details on the screen:
*
*            - effective date
*            - policy loan amount
*            - other adjustments amount
*            - currency
*            - surrender reason code
*            - Surrender reason description
*
*  The above details are returned  to  the user and if he wishes
*  to continue  and  perform  the  Reversal  he  presses  Enter,
*  otherwise  he    opts    for  KILL  to  kill  and  exit  from
*  this transaction.
*
* Validation
* ----------
*
*  If  KILL  was entered,  then  skip  the   remainder   of  the
*  validation and exit from the program.
*
*  No validation occurs, skip this section.
*
* Updating
* --------
*
*  If the KILL function key was pressed, skip the updating  and
*  exit from the program.
*
*  Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
*  Call    the  AT  submission module to submit P5066AT, passing
*  the Contract number, as the 'primary key', this performs  the
*  Full Surrender Reversal transaction.
*
*   Next Program
*   ------------
*
*  For KILL or 'Enter', add 1 to the program pointer and exit.
*
*
*  Modifications
*  -------------
*
*  Rewrite P5066 with the above processing included.
*
*  Include the AT module P5066AT.
*
*  Include  the  following logical views (access only the fields
*  required).
*
*        - CHDRSUR
*        - LIFESUR
*        - COVRSUR
*        - SURHCLM
*        - SURDCLM
*****************************************************************
* </pre>
*/
public class P5066 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5066");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
		/* ERRORS */
	private static final String e304 = "E304";
	private static final String e350 = "E350";
	private static final String f910 = "F910";
	private static final String h143 = "H143";
	private static final String h093 = "H093";
	private static final String e751 = "E751";
	private PackedDecimalData wsaaEstMatValue = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaSacscurbal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaclaimamt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(17, 2).init(0);
	private String wsaaTaxdFound = "";

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaSurdCurrcd = new FixedLengthStringData(3);
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5645 = "T5645";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifesurTableDAM lifesurIO = new LifesurTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Gensswrec gensswrec = new Gensswrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5645rec t5645rec = new T5645rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Wssplife wssplife = new Wssplife();
	private S5066ScreenVars sv = ScreenProgram.getScreenVars( S5066ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
	//ILIFE-6513 starts
	private List<Acmvpf> acmvpf = null;
	private List<Acmvpf> totalAcmvpfList= new ArrayList<Acmvpf>();
	private AcmvpfDAO acmvpfDAO =  getApplicationContext().getBean("acmvDAO", AcmvpfDAO.class);
	private Acmvpf acmvIO = new Acmvpf();
	private PackedDecimalData wsaaservamt = new PackedDecimalData(17,2);
	private PackedDecimalData wsaaedamt = new PackedDecimalData(17,2);
	private PackedDecimalData wsaaloanamt = new PackedDecimalData(17,2);
	private PackedDecimalData wsaatotalamt = new PackedDecimalData(17,2);
	private FixedLengthStringData wsaaConstants = new FixedLengthStringData(14);
	private FixedLengthStringData wsccLe = new FixedLengthStringData(2).isAPartOf(wsaaConstants, 0).init("LE");
	private FixedLengthStringData wscct = new FixedLengthStringData(2).isAPartOf(wsaaConstants, 2).init("T1");
	private FixedLengthStringData wscctt = new FixedLengthStringData(2).isAPartOf(wsaaConstants, 4).init("T2");
	private FixedLengthStringData wsccLn = new FixedLengthStringData(2).isAPartOf(wsaaConstants, 6).init("LN");
	private FixedLengthStringData wsccLi = new FixedLengthStringData(2).isAPartOf(wsaaConstants, 8).init("LI");
	private FixedLengthStringData wsccbt = new FixedLengthStringData(4).isAPartOf(wsaaConstants, 10).init("T512");
	//ILIFE-6513 ends
	private Pyoupf pyoupf = new Pyoupf();
	private PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO",PyoupfDAO.class);
	private List<Pyoupf> pyoupfList;
	private List<String> deleteList;
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	private List<Itempf> itdmpfList = null;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private T5687rec t5687rec = new T5687rec();
	private FixedLengthStringData wsaaSurrenderMethod = new FixedLengthStringData(4).init("SC14");

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1030, 
		exit1090, 
		exit5100
	}

	public P5066() {
		super();
		screenVars = sv;
		new ScreenModel("S5066", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case continue1030: {
					continue1030();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		 
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaEstMatValue.set(ZERO);
		wsaaActvalue.set(ZERO);
		wsaaSacscurbal.set(ZERO);
		wsaaclaimamt.set(ZERO);
		wsaaX.set(ZERO);
		wsaaY.set(ZERO);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("S5066", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.clamant.set(ZERO);
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.actvalue.set(ZERO);
		sv.estMatValue.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.tdbtamt.set(ZERO);
		sv.zrcshamt.set(ZERO);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		chdrsurIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		chdrsurIO.setFunction("READR");
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		sv.occdate.set(chdrsurIO.getOccdate());
		sv.chdrnum.set(chdrsurIO.getChdrnum());
		sv.cnttype.set(chdrsurIO.getCnttype());
		//ILIFE-6513 starts
		wsaaservamt.set(ZERO);
		wsaaedamt.set(ZERO);
			acmv100();
		//ILIFE-6513 ends	
			 
		descIO.setDescitem(chdrsurIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrsurIO.getCownnum());
		cltsIO.setClntnum(chdrsurIO.getCownnum());
		getClientDetails1400();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrsurIO.getBtdate());
		sv.ptdate.set(chdrsurIO.getPtdate());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrsurIO.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrsurIO.getPstatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		covrsurIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		
		surhclmIO.setParams(SPACES);
		surhclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
		surhclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surhclmIO.setTranno(ZERO);
		surhclmIO.setPlanSuffix(covrsurIO.getPlanSuffix());
		surhclmIO.setFunction("BEGN");
		while ( !(isEQ(surhclmIO.getStatuz(),varcom.endp))) {
			processSurhclm1600();
		}
		
		sv.effdate.set(surhclmIO.getEffdate());
		sv.reasoncd.set(surhclmIO.getReasoncd());
		sv.resndesc.set(surhclmIO.getResndesc());
		sv.policyloan.set(surhclmIO.getPolicyloan());
		sv.tdbtamt.set(surhclmIO.getTdbtamt());
		sv.otheradjst.set(surhclmIO.getOtheradjst());
		sv.currcd.set(surhclmIO.getCurrcd());
		sv.planSuffix.set(surhclmIO.getPlanSuffix());
		sv.taxamt.set(surhclmIO.getTaxamt());
		lifesurIO.setDataArea(SPACES);
		lifesurIO.setChdrcoy(surhclmIO.getChdrcoy());
		lifesurIO.setChdrnum(surhclmIO.getChdrnum());
		lifesurIO.setLife(surhclmIO.getLife());
		lifesurIO.setJlife("00");
		lifesurIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifesurIO);
		if (isNE(lifesurIO.getStatuz(),varcom.oK)
		&& isNE(lifesurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifesurIO.getChdrcoy())
		|| isNE(chdrsurIO.getChdrnum(),lifesurIO.getChdrnum())
		|| isNE(lifesurIO.getJlife(),"00")
		&& isNE(lifesurIO.getJlife(),SPACES)
		|| isEQ(lifesurIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1400();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.linsnameErr.set(h143);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		lifesurIO.setJlife("01");
		lifesurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifesurIO);
		if ((isNE(lifesurIO.getStatuz(),varcom.mrnf))
		&& (isNE(lifesurIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isEQ(lifesurIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
		}
		sv.jlifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1400();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.jlinsnameErr.set(e350);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
		
		
		
	}

protected void continue1030()
	{
		surdclmIO.setDataArea(SPACES);
		surdclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surdclmIO.setTranno(surhclmIO.getTranno());
		surdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		
		surdclmIO.setStatuz(SPACES);
		while ( !(isEQ(surdclmIO.getStatuz(),varcom.endp))) {
			processSurdclm1500();
		}
		
		sv.estimateTotalValue.set(wsaaEstMatValue);
		compute(sv.clamant, 2).set(sub(sub(sub(add(add(wsaaActvalue,surhclmIO.getOtheradjst()),surhclmIO.getZrcshamt()),surhclmIO.getPolicyloan()),surhclmIO.getTaxamt()),surhclmIO.getTdbtamt()));
		ptrnrevIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.estimateTotalValue,ZERO)
		&& isNE(sv.clamant,ZERO)) {
			checkSurrenderValue1700();
		}
		checkCalcTax5100();
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1400()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void processSurdclm1500()
	{
		read1510();
	}

protected void read1510()
	{
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.endp)
		&& isNE(surdclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(surdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrsurIO.getChdrcoy(),surdclmIO.getChdrcoy())
		|| isNE(chdrsurIO.getChdrnum(),surdclmIO.getChdrnum())
		|| isNE(surhclmIO.getTranno(),surdclmIO.getTranno())
		|| isEQ(surdclmIO.getStatuz(),varcom.endp)) {
			surdclmIO.setStatuz(varcom.endp);
		}
		else {
			wsaaSurdCurrcd.set(surdclmIO.getCurrcd());
			sv.life.set(surdclmIO.getLife());
			sv.coverage.set(surdclmIO.getCoverage());
			sv.rider.set(surdclmIO.getRider());
			sv.hcrtable.set(surdclmIO.getCrtable());
			sv.shortds.set(surdclmIO.getShortds());
			itdmpfList = itemDAO.getItdmByFrmdate(surdclmIO.getChdrcoy().toString(), "T5687", surdclmIO.getCrtable().toString(), wsaaToday.toInt());
			for(Itempf item : itdmpfList) {
				t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
			}
			if(t5687rec.svMethod.equals(wsaaSurrenderMethod)) {
				if (isEQ(surdclmIO.getRider(), "00")) {
					sv.rider.set(SPACES);
				}
				else {
					sv.rider.set(surdclmIO.getRider());
				}
				if (isNE(surdclmIO.getRider(), "00")
				&& isNE(surdclmIO.getRider(), "  ")) {
					sv.coverage.set(SPACES);
				}
			}
			if(surdclmIO.getFieldType().equals("P")) {
				sv.coverage.set(SPACES);
				sv.life.set(SPACES);
				sv.rider.set(SPACES);
			}
			sv.cnstcur.set(surdclmIO.getCurrcd());
			sv.estMatValue.set(surdclmIO.getEstMatValue());
			sv.actvalue.set(surdclmIO.getActvalue());
			wsaaEstMatValue.add(surdclmIO.getEstMatValue());
			if (isEQ(surdclmIO.getFieldType(),"C")) {
				wsaaActvalue.subtract(surdclmIO.getActvalue());
			}
			else {
				wsaaActvalue.add(surdclmIO.getActvalue());
			}
			sv.fund.set(surdclmIO.getVirtualFund());
			sv.fundtype.set(surdclmIO.getFieldType());
			scrnparams.function.set(varcom.sadd);
			processScreen("S5066", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			else {
				surdclmIO.setFunction(varcom.nextr);
			}
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void processSurhclm1600()
	{
			para1600();
		}

protected void para1600()
	{
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)
		&& isNE(surhclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		if ((isEQ(chdrsurIO.getChdrcoy(),surhclmIO.getChdrcoy()))
		&& (isEQ(chdrsurIO.getChdrnum(),surhclmIO.getChdrnum()))
		&& (isEQ(covrsurIO.getPlanSuffix(),surhclmIO.getPlanSuffix()))
		&& (isNE(surhclmIO.getStatuz(),varcom.endp))) {
			surhclmIO.setStatuz(varcom.endp);
			return ;
		}
		if ((isNE(chdrsurIO.getChdrcoy(),surhclmIO.getChdrcoy()))
		|| (isNE(chdrsurIO.getChdrnum(),surhclmIO.getChdrnum()))
		|| (isEQ(surhclmIO.getStatuz(),varcom.endp))) {
			surhclmIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		surhclmIO.setFunction(varcom.nextr);
	}

protected void checkSurrenderValue1700()
	{
		readT56451710();
		readAcbl1720();
	}

protected void readT56451710()
	{
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readAcbl1720()
	{
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(wsspcomn.company);
		acblIO.setRldgacct(chdrsurIO.getChdrnum());
		acblIO.setSacscode(t5645rec.sacscode[1]);
		acblIO.setSacstyp(t5645rec.sacstype[1]);
		acblIO.setOrigcurr(wsaaSurdCurrcd);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			wsaaSacscurbal.set(ZERO);
		}
		else {
		if (isEQ(t5645rec.sign[1],"-")) {
			compute(wsaaSacscurbal, 2).set(mult(acblIO.getSacscurbal(),-1));
		}
		else {
			wsaaSacscurbal.set(acblIO.getSacscurbal());
		}
		
		if (isLT(acblIO.getSacscurbal(),ZERO))
		{
			compute(wsaaSacscurbal, 2).set(mult(acblIO.getSacscurbal(),-1));
		}
	}
		
		
	}

//ILIFE-6513 starts
protected void acmv100(){
	
	
	
	acmvIO = new Acmvpf();
	acmvIO.setRldgcoy(chdrsurIO.getChdrcoy().charat(0));
	acmvIO.setRdocnum(chdrsurIO.getChdrnum().toString());
	acmvIO.setRldgacct(chdrsurIO.getChdrnum().toString());
	acmvIO.setSacscode(wsccLe.toString());
	acmvIO.setSacstyp(wscct.toString());
	acmvIO.setBatctrcde(wsccbt.toString());
	List<Acmvpf> acmvpfList1 =this.acmvpfDAO.ReadSurrender100(acmvIO);
    
	for (Acmvpf pf : acmvpfList1) {
		if (isEQ(pf.getBatctrcde(),wsccbt.toString()))
				{
					compute(wsaaservamt, 2).set(add(pf.getOrigamt()));
				}
		
	}
	 
	
	acmvIO = new Acmvpf();
	acmvIO.setRldgcoy(chdrsurIO.getChdrcoy().charat(0));
	acmvIO.setRdocnum(chdrsurIO.getChdrnum().toString());
	acmvIO.setRldgacct(chdrsurIO.getChdrnum().toString());
	acmvIO.setSacscode(wsccLe.toString());
	acmvIO.setSacstyp(wscctt.toString());
	acmvIO.setBatctrcde(wsccbt.toString());
	List<Acmvpf> acmvpfList2 =this.acmvpfDAO.ReadSurrender100(acmvIO);
    
	for (Acmvpf pf : acmvpfList2) {
		if (isEQ(pf.getBatctrcde(),wsccbt.toString()))
				{
					compute(wsaaedamt, 2).set(add(pf.getOrigamt()));
				}
		
	}
	
	acmvIO = new Acmvpf();
	acmvIO.setRldgcoy(chdrsurIO.getChdrcoy().charat(0));
	acmvIO.setRdocnum(chdrsurIO.getChdrnum().toString());
	acmvIO.setRldgacct(chdrsurIO.getChdrnum().toString());
	acmvIO.setSacscode(wsccLn.toString());
	acmvIO.setSacstyp(wsccLi.toString());
	acmvIO.setBatctrcde(wsccbt.toString());
	List<Acmvpf> acmvpfList3 =this.acmvpfDAO.ReadSurrender100(acmvIO);
    
	for (Acmvpf pf : acmvpfList3) {
		if (isEQ(pf.getBatctrcde(),wsccbt.toString()))
				{
					compute(wsaaloanamt, 2).set(add(pf.getOrigamt()));
				}
		
	}
	
	compute(wsaatotalamt, 2).set(add(wsaaedamt, wsaaservamt,wsaaloanamt));
	
}
//ILIFE-6513 ENDS

protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if ((isNE(wsaaSacscurbal,ZERO))) {
			compute(wsaaclaimamt, 2).set(sub(sv.clamant, wsaatotalamt));//ILIFE-6513 
			if ((isLT(wsaaSacscurbal,wsaaclaimamt))) {
				sv.clamantErr.set(e751);
				wsspcomn.edterror.set("Y");
			}
		}
		return ;
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		if ((isNE(wsaaSacscurbal,ZERO))) {
			compute(wsaaclaimamt, 2).set(sub(sv.clamant, wsaatotalamt)); //ILIFE-6513 
			if ((isLT(wsaaSacscurbal,wsaaclaimamt))) {
				sv.clamantErr.set(e751);
				wsspcomn.edterror.set("Y");
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			return ;
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrsurIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrsurIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransAreaInner.wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransAreaInner.wsaaTranDate.set(varcom.vrcmDate);
		wsaaTransAreaInner.wsaaTranTime.set(varcom.vrcmTime);
		wsaaTransAreaInner.wsaaUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO WSAA-TERMID.                    */
		wsaaTransAreaInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransAreaInner.wsaaTranCode.set(ptrnrevIO.getBatctrcde());
		wsaaTransAreaInner.wsaaTodate.set(ptrnrevIO.getPtrneff());
		wsaaTransAreaInner.wsaaSupflag.set("N");
		wsaaTransAreaInner.wsaaSuppressTo.set(ZERO);
		wsaaTransAreaInner.wsaaPlnsfx.set(surhclmIO.getPlanSuffix());
		wsaaTransAreaInner.wsaaTranNum.set(surhclmIO.getTranno());
		wsaaTransAreaInner.wsaaCfiafiTranno.set(ZERO);
		wsaaTransAreaInner.wsaaCfiafiTranCode.set(SPACES);
		wsaaTransAreaInner.wsaaBbldat.set(ZERO);
		atreqrec.transArea.set(wsaaTransAreaInner.wsaaTransArea);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
		updateSurr();
	}



protected void updateSurr()
	{
	
	pyoupfList=pyoupfDAO.getAllItemitem(chdrsurIO.getChdrnum().toString()); 
	deleteList=new ArrayList<String>();
			
	 if (pyoupfList.size() >= 1 )
	 {
			for(Pyoupf pyoupf :pyoupfList)
				{
					deleteList.add(pyoupf.getChdrnum());
				}
			pyoupfDAO.deletePyoupfRecord(deleteList);
	 }
			
	 
	}
	
  
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
			callSubroutine4310();
		}

protected void callSubroutine4310()
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		compute(wsaaX, 0).set(add(1,wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void checkCalcTax5100()
	{
		try {
			start5100();
			readTr52d5100();
			readSubFile5100();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start5100()
	{
		wsaaFeeTax.set(ZERO);
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(chdrsurIO.getChdrcoy());
		taxdrevIO.setChdrnum(chdrsurIO.getChdrnum());
		taxdrevIO.setEffdate(sv.effdate);
		taxdrevIO.setFunction(varcom.begn);
		taxdrevIO.setFormat(formatsInner.taxdrevrec);
		wsaaTaxdFound = "N";
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)
			&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(taxdrevIO.getParams());
				syserrrec.statuz.set(taxdrevIO.getStatuz());
				fatalError600();
			}
			if (isEQ(taxdrevIO.getStatuz(), varcom.oK)
			&& isEQ(taxdrevIO.getChdrcoy(), chdrsurIO.getChdrcoy())
			&& isEQ(taxdrevIO.getChdrnum(), chdrsurIO.getChdrnum())
			&& isEQ(taxdrevIO.getEffdate(), sv.effdate)) {
				if (isEQ(taxdrevIO.getTranno(), surhclmIO.getTranno())) {
					wsaaTaxdFound = "Y";
					if (isNE(taxdrevIO.getTxabsind01(), "Y")) {
						wsaaFeeTax.add(taxdrevIO.getTaxamt01());
					}
					if (isNE(taxdrevIO.getTxabsind02(), "Y")) {
						wsaaFeeTax.add(taxdrevIO.getTaxamt02());
					}
				}
			}
			else {
				taxdrevIO.setStatuz(varcom.endp);
			}
			taxdrevIO.setFunction(varcom.nextr);
		}
		
		if (isEQ(wsaaTaxdFound, "Y")) {
			sv.taxamt.add(wsaaFeeTax);
			goTo(GotoLabel.exit5100);
		}
		/* for unit linked contracts, taxes will only be calculated        */
		/* during unit dealing. It is possible to reverse the surrender    */
		/* before the unit dealing takes place, hence we need to           */
		/* calculate the taxes if it hasn't been calculated yet.           */
		/* Read CHDRENQ                                                    */
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setChdrcoy(chdrsurIO.getChdrcoy());
		chdrenqIO.setChdrnum(chdrsurIO.getChdrnum());
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void readTr52d5100()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrenqIO.getChdrcoy());
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrenqIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrenqIO.getChdrcoy());
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit5100);
		}
	}

protected void readSubFile5100()
	{
		/* Accumulate TOTAL FEE                                            */
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5066", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			if (isEQ(sv.fundtype, "C")
			|| isEQ(sv.fundtype, "J")) {
				callTaxsubr5200();
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("S5066", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		sv.taxamt.add(wsaaFeeTax);
	}

protected void callTaxsubr5200()
	{
		start5200();
	}

protected void start5200()
	{
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
		wsaaTr52eCrtable.set(sv.hcrtable);
		readTr52e5300();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e5300();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e5300();
		}
		/* IF TR52E-TAXIND-05          NOT = 'Y'                        */
		if (isNE(tr52erec.taxind03, "Y")) {
			/*     GO TO 5100-EXIT                                   <S19FIX>*/
			return ;
		}
		/* Call tax subroutine                                             */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(sv.life);
		txcalcrec.coverage.set(sv.coverage);
		txcalcrec.rider.set(sv.rider);
		txcalcrec.crtable.set(sv.hcrtable);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.register.set(chdrenqIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(sv.currcd);
		wsaaCntCurr.set(sv.currcd);
		txcalcrec.rateItem.set(sv.currcd);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.cntTaxInd.set(SPACES);
		/*   MOVE WSAA-TOTAL-FEE         TO TXCL-AMOUNT-IN.               */
		txcalcrec.amountIn.set(sv.actvalue);
		txcalcrec.transType.set("SURF");
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void readTr52e5300()
	{
		start5300();
	}

protected void start5300()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(wsaaTr52eKey);
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy()))
		&& (isEQ(itdmIO.getItemtabl(), tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}
/*
 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
 */
private static final class WsaaTransAreaInner { 

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData taxdrevrec = new FixedLengthStringData(10).init("TAXDREVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
}