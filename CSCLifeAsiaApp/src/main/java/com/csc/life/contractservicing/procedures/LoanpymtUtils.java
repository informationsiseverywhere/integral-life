/*
 * File: LoanpymtUtils.java
 * Date: 13 Oct 2018 22:58:40
 * Author: sbatra9
 * 
 * Purpose :curd data for Loanpymt
 * 
 * Copyright (2007) DXC, all rights reserved.
 */ 
package com.csc.life.contractservicing.procedures;

import com.csc.fsu.general.recordstructures.CashedPojo;


public interface LoanpymtUtils {
	public void callLoanpymt(CashedPojo cashedPojo);

}