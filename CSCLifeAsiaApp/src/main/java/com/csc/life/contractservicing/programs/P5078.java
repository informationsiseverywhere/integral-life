/*
 * File: P5078.java
 * Date: 30 August 2009 0:04:03
 * Author: Quipoz Limited
 *
 * Class transformed from P5078.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTallyAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.CrelpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Crelpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.recordstructures.Pd5h5rec;
import com.csc.life.contractservicing.screens.S5078ScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.BnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.life.newbusiness.procedures.Antisocl;
import com.csc.life.newbusiness.recordstructures.Antisoclkey;
import com.csc.life.newbusiness.tablestructures.Tr52yrec;
import com.csc.life.newbusiness.tablestructures.Tr52zrec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5662rec;
import com.csc.smart.dataaccess.dao.CcfkpfDAO;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*  Retrieve the contract header information from the CHDRMNA I/O
* module.
*
*  Look up the following :
*
*       - the contract type description from T5688.
*
*  Up  to  ten  beneficiaries  are  allowed per contract, and no
* more,  so  a  fixed  subfile size can be used. Initialise the
* subfile  with  blank records (INIT function), and then update
* any  records  with  existing details as required. Details are
* loaded from the beneficiaries file (BNFYMNA). For each record
* read:
*
*       - use the  next  available  relative  record  number  to
*       retrieve  a  blank subfile record (i.e. starting at
*       one).
*
*       - look up the  client name (CLTS), formatting the client
*       name for confirmation.
*
*       - hold the client number in a "hidden" field to indicate
*       that the record was  loaded  from the database (and
*       so is not new  -  required  during  validation  and
*       updating).
*
*       - hold  the  original percentage in a "hidden" field for
*       later validation.
*
*  Accumulate the current percentage allocated.
*
* Validation
* ----------
*
*  If in enquiry  mode  (WSSP-FLAG  =  'I')  protect  the entire
* Screen prior to output.
*
*  If  the  'KILL'  function  key  was  pressed,  skip  all  the
* validation.
*
*  If in enquiry mode, skip all the validation.
*
*  Validate each changed  subfile  record according to the rules
* defined in the  screen and field help.
*
* Check that the accumulated percentages is still exactly 100%.
*
* Updating
* --------
*
*  If the 'KILL' function  key  was  pressed,  or  if in enquiry
* mode, skip the updating only.
*
*  Add, modify or  delete  beneficiary  record  depending  on if
* details  have  been  keyed in, blanked out or changed. Ignore
* any  subfile  records  which were not changed.
* Note, that if a  client  number  is  changed, the original
* the original record must be deleted and a new one added.
*
*  When adding a new beneficiary record, initialise fields which
* are not from the screen as follows:
*
*             Valid flag = '1',
*             Transaction no. = contract trans. no.,
*             Company = contract company.
*
*  If deleting records then we should set the validflag = 2
*  But do not actually delete the record as we may use them
*  at a later date.
*
*  Update CLRR-records (Client Roles) :
*
*  If update-flag = add - Add the record to CLRR.
*  If update-flag = Mod - Delete the old record from
*                         CLRR and Create the new record.
*  If update-flag = Add - Create the new record in CLRR.
*
*  The Key to CLRR is as following :
*
*  CLNTPFX = 'CN'
*  CLNTCOY = FSU COMPANY
*  CLNTNUM = NEW/OLD CLIENT NO.
*  CLRRROLE = CLIENT ROLE 'BN'
*  FOREPFX  = 'CH'
*  FORECOY  = CLIENT/CONTRACT COMPANY.
*  FORENUM  = CONTRACT NO.
*
*****************************************************************
* </pre>
*/
public class P5078 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5078");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private int ix = 0;
	private int iy = 0;
	private int iz = 0;
	private int wsaaCount = 0;

	private FixedLengthStringData wsaaMultBnfy = new FixedLengthStringData(20);
	protected ZonedDecimalData[] wsaaBnfyCount = ZDArrayPartOfStructure(10, 2, 0, wsaaMultBnfy, 0, UNSIGNED_TRUE);
	protected int ib = 0;
	protected String wsaaCrtBnfyreln = " ";
	protected int wsaaPctSub = 0;
	protected FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8).init(SPACES);
	private static final int wsaaSubfileSize = 30;
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(5, 0).setUnsigned();
	private int wsaaNofRead;
	private int wsaaLastTranno = 0;

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private FixedLengthStringData wsaaOkeyBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaLetokeys, 1);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 5).setUnsigned();

		/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);

	private FixedLengthStringData wsaaFound = new FixedLengthStringData(1).init(SPACES);
	protected Validator found = new Validator(wsaaFound, "Y");
	private Validator notFound = new Validator(wsaaFound, "N");

	protected FixedLengthStringData wsaaBeneTypeFound = new FixedLengthStringData(1).init("N");
	protected Validator beneTypeFound = new Validator(wsaaBeneTypeFound, "Y");

	protected FixedLengthStringData wsaaValidRelation = new FixedLengthStringData(1).init(SPACES);
	protected Validator validRelation = new Validator(wsaaValidRelation, "Y");

	private FixedLengthStringData wsaaSelfRelation = new FixedLengthStringData(1).init(SPACES);
	private Validator selfRelation = new Validator(wsaaSelfRelation, "Y");
	protected Validator noSelfRelation = new Validator(wsaaSelfRelation, "N");

	private FixedLengthStringData wsaaBatcKey = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaBatcBatctrcde = new FixedLengthStringData(5).isAPartOf(wsaaBatcKey, 17);

	private FixedLengthStringData wsaaMandatorys = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaMandatory = FLSArrayPartOfStructure(30, 1, wsaaMandatorys, 0);

	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(3);
	private Validator wsaaAdd = new Validator(wsaaUpdateFlag, "ADD");
	private Validator wsaaDelete = new Validator(wsaaUpdateFlag, "DEL");
	private Validator wsaaModify = new Validator(wsaaUpdateFlag, "MOD");
	protected ZonedDecimalData wsbbTableSub = new ZonedDecimalData(3, 0).setUnsigned();
	protected ZonedDecimalData wsbbTableRsub = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaClntArray = new FixedLengthStringData(420);
		/*                             OCCURS 10.                       */
	protected FixedLengthStringData[] wsaaClient = FLSArrayPartOfStructure(30, 10, wsaaClntArray, 0);
	protected FixedLengthStringData[] wsaaType = FLSArrayPartOfStructure(30, 2, wsaaClntArray, 300);
	protected FixedLengthStringData[] wsaaSequence = FLSArrayPartOfStructure(30, 2, wsaaClntArray, 360);

	private FixedLengthStringData wsaaPctTypes = new FixedLengthStringData(90);
	protected FixedLengthStringData[] wsaaPctType = FLSArrayPartOfStructure(10, 2, wsaaPctTypes, 0);
	protected FixedLengthStringData[] wsaaPctSeq = FLSArrayPartOfStructure(10, 2, wsaaPctTypes, 20);
	protected ZonedDecimalData[] wsaaPctTot = ZDArrayPartOfStructure(10, 5, 2, wsaaPctTypes, 40, UNSIGNED_TRUE);
	protected int wsaaDuprSub = 0;
	protected String wsaaPtrnFlag = "";
	protected Datcon2rec datcon2rec = new Datcon2rec();
	private int wsaaRrn = 0;
		/* TABLES */
	protected static final String t3584 = "T3584";
	protected static final String t5662 = "T5662";
	private static final String tr384 = "TR384";
	protected static final String tr52y = "TR52Y";
	private static final String tr52z = "TR52Z";
	private static final String chdrmnarec = "CHDRMNAREC";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	protected ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	protected T5662rec t5662rec = new T5662rec();
	private Tr384rec tr384rec = new Tr384rec();
	protected Tr52yrec tr52yrec = new Tr52yrec();
	protected Tr52zrec tr52zrec = new Tr52zrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	protected S5078ScreenVars sv = getLScreenVars();//ScreenProgram.getScreenVars( S5078ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	protected WsbbStackArrayInner wsbbStackArrayInner = new WsbbStackArrayInner();	
	
	protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	protected ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private CrelpfDAO crelpfDAO = getApplicationContext().getBean("crelpfDAO", CrelpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	private BnfypfDAO bnfypfDAO = getApplicationContext().getBean("bnfypfDAO", BnfypfDAO.class);
	private BabrpfDAO babrpfDAO = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	protected ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	protected CcfkpfDAO ccfkpfDAO = getApplicationContext().getBean("ccfkpfDAO", CcfkpfDAO.class);
	protected Itempf itempf = null;
	protected Descpf descpf = null;
    protected Clntpf clntpf = null;
    private Crelpf crelpf = null;	
    private Ptrnpf ptrnpf = null;
    protected Lifepf lifepf = null;
    private Clrrpf clrrpf = null;
    private Bnfypf bnfypf = null;
    private Bnfypf instBnfypf = null;
    private Babrpf babrpf = null;
    protected Clbapf clbapf = null;
    protected List<Itempf> itdmpfList = null;
    private List<Bnfypf> bnfypfList = null;
    
    private Batckey wsaaBatckey1 = new Batckey();
    private Pd5h5rec ptemprec = new Pd5h5rec();
    int wsaaX = 0;
    int wsaaY = 0;
    protected int len = 0;
    protected int lenRel = 0;
    private StringBuffer bankAccKeybuffer = null;
    protected boolean benesequence = false;
    //ILIFE-7646
    protected String benValue;
    protected static final String  BEN_FEATURE_ID="NBPRP091";
    protected boolean benausFlag=false;
    boolean bentypeflag = false;
   //ILJ-ANSO
    private Antisoclkey antisoclkey = new Antisoclkey();
    int ansoErrStat=0;
    List<String> beneficiary = new ArrayList<String>();
    private boolean NBPRP117permission = false;
    
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2190,
		b790Exit
	}

	public P5078() {
		super();
		screenVars = sv;
		new ScreenModel("S5078", AppVars.getInstance(), sv);
	}
	protected S5078ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5078ScreenVars.class);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);		
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().trim().equals("C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().trim().equals("C")) {
			corpname();
			return ;
		}
		if (clntpf.getGivname()!=null & !clntpf.getGivname().trim().equals("")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().trim().equals("C")) {
			corpname();
			return ;
		}
		if (clntpf.getEthorig().trim().equals("1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		retrieveHeader1030();
		readContractLongdesc1040();
		loadSubfileFields1060();
		
	}


protected void initialise1010()
	{
		NBPRP117permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP117", appVars, "IT");
		wsaaBatckey.set(wsspcomn.batchkey);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaMandatorys.set(SPACES);
		ix = 0;
		iy = 0;
		iz = 0;
		wsaaCount = 0;
		wsaaMultBnfy.set(ZERO);
		ib = 0;
		/* Get today's date.*/
		benesequence = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP087", appVars, "IT");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		/* Initialise screen fields.*/
		/* MOVE SPACES                 TO S5078-BNYRLN                  */
		sv.cltreln.set(SPACES);
		sv.bnycd.set(SPACES);
		sv.bnysel.set(SPACES);
		sv.relto.set(SPACES);
		sv.revcflg.set(SPACES);
		sv.clntsname.set(SPACES);
		sv.bnypc.set(ZERO);
		if(benesequence){
		sv.sequence.set(SPACES);
		sv.actionflag.set("Y");
		}
		else{
		sv.actionflag.set("N");
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("S5078", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrieveHeader1030()
	{
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmnaIO.getStatuz());
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		/* Release the contract header record for subsequent updating.*/
		chdrmnaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmnaIO.getStatuz());
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
	}

protected void readContractLongdesc1040()
	{
		/* Get the Contract Type description.*/
	    descpf = descDAO.getdescData("IT", "T5688", chdrmnaIO.getCnttype().toString(), chdrmnaIO.getChdrcoy().toString(), wsspcomn.language.toString());
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		if (descpf == null) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		/*  Read CLTS to get contract owner long name                      */
		clntpf = new Clntpf();	
		clntpf.setClntpfx(fsupfxcpy.clnt.toString());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(chdrmnaIO.getCownnum().toString());
		sv.cownnum.set(chdrmnaIO.getCownnum());
	    clntpf = clntpfDAO.selectClient(clntpf);
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		/*  Read LIFE to get life insured                                  */
		lifepf = lifepfDAO.getLifeEnqRecord(wsspcomn.company.toString(), chdrmnaIO.getChdrnum().toString(), "01", "00");
		if (lifepf == null) {
			syserrrec.params.set(wsspcomn.company.toString().concat(chdrmnaIO.getChdrnum().toString()).concat("0100"));
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
		/*  Read CLTS to get life insured long name                        */
		clntpf = new Clntpf();	
		clntpf.setClntpfx(fsupfxcpy.clnt.toString());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(lifepf.getLifcnum());
		sv.lifcnum.set(lifepf.getLifcnum());
		clntpf = clntpfDAO.selectClient(clntpf);
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/*  Read Contract Beneficiary Mandatory Table - TR52Z.             */
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tr52z);
		itempf.setItemitem(chdrmnaIO.getCnttype().toString());
        itempf = itemDAO.getItempfRecord(itempf);
		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tr52z);
			itempf.setItemitem("***");

		}
		if (itempf == null) {
			tr52zrec.tr52zRec.set(SPACES);
		}else{
		tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		/* Load any beneficiary records into the working storage array*/
		/* prior to loading them into the subfile.*/
	
		loadStack1100();
	}

protected void loadSubfileFields1060()
	{
		moveStackToScreen1400();
		/*EXIT*/
	}

protected void loadStack1100()
	{
		/*PARA*/
		wsbbStackArrayInner.wsbbStackArray.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, wsaaSubfileSize)); wsaaIndex.add(1)){
			initialZero1300();
		}
	
		bnfypfList = bnfypfDAO.getBnfymnaByCoyAndNum(chdrmnaIO.getChdrcoy().toString(), chdrmnaIO.getChdrnum().toString());
		
		if(bnfypfList !=null && bnfypfList.size()>0){
		 for (wsaaNofRead=1; wsaaNofRead <= bnfypfList.size() ; wsaaNofRead++){
			bnfypf = bnfypfList.get(wsaaNofRead-1);
			loadBeneficiaries1200();
		  }
		}
		/*EXIT*/
	}

protected void loadBeneficiaries1200()
	{
	
		clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(bnfypf.getBnyclt());/* IJTI-1523 */
		clntpf = clntpfDAO.selectClient(clntpf);
	
		plainname();
		wsbbStackArrayInner.wsbbBnysel[wsaaNofRead].set(bnfypf.getBnyclt());
		wsbbStackArrayInner.wsbbClntnm[wsaaNofRead].set(wsspcomn.longconfname);
		/* MOVE BNFYMNA-BNYRLN         TO WSBB-BNYRLN(WSAA-NOF-READ).   */
		wsbbStackArrayInner.wsbbBnyrln[wsaaNofRead].set(bnfypf.getCltreln());
		wsbbStackArrayInner.wsbbBnytype[wsaaNofRead].set(bnfypf.getBnytype());
		wsbbStackArrayInner.wsbbBnycd[wsaaNofRead].set(bnfypf.getBnycd());
		wsbbStackArrayInner.wsbbBnypc[wsaaNofRead].set(bnfypf.getBnypc());
		wsbbStackArrayInner.wsbbEffdate[wsaaNofRead].set(bnfypf.getEffdate());
		wsbbStackArrayInner.wsbbEnddate[wsaaNofRead].set(bnfypf.getEnddate());
		wsbbStackArrayInner.wsbbRelto[wsaaNofRead].set(bnfypf.getRelto());
		wsbbStackArrayInner.wsbbRevcflg[wsaaNofRead].set(bnfypf.getRevcflg());
		wsbbStackArrayInner.wsbbSelfind[wsaaNofRead].set(bnfypf.getSelfind());
		if(benesequence){
		wsbbStackArrayInner.wsbbpaymthbf[wsaaNofRead].set(bnfypf.getPaymmeth());
		wsbbStackArrayInner.wsbbSequence[wsaaNofRead].set(bnfypf.getSequence());//ICIL-372
		wsbbStackArrayInner.wsbbBankkey[wsaaNofRead].set(bnfypf.getBankkey());
		wsbbStackArrayInner.wsbbBankacckey[wsaaNofRead].set(bnfypf.getBankacckey());
		if(bnfypf.getBankkey()!=null && !bnfypf.getBankkey().trim().equals("")) {
			babrpf = babrpfDAO.searchBankkey(bnfypf.getBankkey());
			wsbbStackArrayInner.wsbbBankdesc[wsaaNofRead].set(babrpf.getBankdesc());
			}
		}
	}

protected void initialZero1300()
	{
		wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()].set(ZERO);
		wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbRelto[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBnyrln[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBnycd[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbClntnm[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()].set(varcom.vrcmMaxDate);
		wsbbStackArrayInner.wsbbEnddate[wsaaIndex.toInt()].set(varcom.vrcmMaxDate);
		wsbbStackArrayInner.wsbbSelfind[wsaaIndex.toInt()].set(SPACES);
		if(benesequence){
		wsbbStackArrayInner.wsbbSequence[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbpaymthbf[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBankkey[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBankacckey[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBankdesc[wsaaIndex.toInt()].set(SPACES);
		}
	}

protected void moveStackToScreen1400()
	{
		/*PARA*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S5078", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, wsaaSubfileSize)); wsaaIndex.add(1)){
			moveToScreen1500();
		}
		/*EXIT*/
	}

protected void moveToScreen1500()
	{
		sv.bnyselOut[varcom.pr.toInt()].set(SPACES);
		sv.bnytypeOut[varcom.pr.toInt()].set(SPACES);
		sv.cltrelnOut[varcom.pr.toInt()].set(SPACES);
		sv.bnycdOut[varcom.pr.toInt()].set(SPACES);
		sv.bnypcOut[varcom.pr.toInt()].set(SPACES);
		sv.effdateOut[varcom.pr.toInt()].set(SPACES);
		sv.enddateOut[varcom.pr.toInt()].set(SPACES);
		if(benesequence){
		sv.sequenceOut[varcom.pr.toInt()].set(SPACES);
		sv.bankacckeyOut[varcom.pr.toInt()].set(SPACES);
		sv.bankkeyOut[varcom.pr.toInt()].set(SPACES);
		}
		/* MOVE WSBB-BNYRLN(WSAA-INDEX) TO S5078-BNYRLN.                */
		sv.cltreln.set(wsbbStackArrayInner.wsbbBnyrln[wsaaIndex.toInt()]);
		sv.bnycd.set(wsbbStackArrayInner.wsbbBnycd[wsaaIndex.toInt()]);
		sv.bnypc.set(wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()]);
		sv.bnytype.set(wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()]);
		sv.relto.set(wsbbStackArrayInner.wsbbRelto[wsaaIndex.toInt()]);
		sv.revcflg.set(wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()]);
		sv.bnysel.set(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]);
		/* MOVE WSBB-CLNTNM(WSAA-INDEX) TO S5078-CLNTNM.                */
		sv.clntsname.set(wsbbStackArrayInner.wsbbClntnm[wsaaIndex.toInt()]);
		sv.effdate.set(wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()]);
		sv.enddate.set(wsbbStackArrayInner.wsbbEnddate[wsaaIndex.toInt()]);
		if(benesequence){
		sv.paymthbf.set(wsbbStackArrayInner.wsbbpaymthbf[wsaaIndex.toInt()]);
		sv.sequence.set(wsbbStackArrayInner.wsbbSequence[wsaaIndex.toInt()]);
		sv.bankkey.set(wsbbStackArrayInner.wsbbBankkey[wsaaIndex.toInt()]);
		sv.bkrelackey.set(wsbbStackArrayInner.wsbbBankacckey[wsaaIndex.toInt()]);
		sv.bnkcdedsc.set(wsbbStackArrayInner.wsbbBankdesc[wsaaIndex.toInt()]);
		if(sv.bkrelackey != null && !sv.bkrelackey.toString().trim().equals("")) {
			len = sv.bkrelackey.toString().trim().length();
		    if(len >4) {
		    	bankAccKeybuffer = new StringBuffer("");
		    	for(int i=0; i<len-4;i++) {
		    		bankAccKeybuffer.append("*");
		    	}
		    	bankAccKeybuffer.append(sv.bkrelackey.toString().trim().substring(len-4, len));
		    	sv.bankacckey.set(bankAccKeybuffer.toString());
		    }else {
		    	sv.bankacckey.set(sv.bkrelackey);
		    }
		    
		}else {
			sv.bankacckey.set(SPACES);
		}
		}
		scrnparams.subfileRrn.set(wsaaIndex);
		if (isEQ(wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()], "N")) {
			sv.bnyselOut[varcom.pr.toInt()].set("Y");
			sv.bnytypeOut[varcom.pr.toInt()].set("Y");
			sv.cltrelnOut[varcom.pr.toInt()].set("Y");
			sv.bnycdOut[varcom.pr.toInt()].set("Y");
			sv.bnypcOut[varcom.pr.toInt()].set("Y");
			sv.effdateOut[varcom.pr.toInt()].set("Y");
			sv.enddateOut[varcom.pr.toInt()].set("Y");
			if(benesequence){
			sv.sequenceOut[varcom.pr.toInt()].set("Y");
			}
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5078", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		scrnparams.subfileRrn.set(1);
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateScreen2010();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5078IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5078-DATA-AREA            */
		/*                                      S5078-SUBFILE-AREA.        */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/* Validate subfile fields.*/
		wsaaClntArray.set(SPACES);
		wsaaMultBnfy.set(ZERO);
		initialize(wsaaPctTypes);
		wsaaPctSub = 1;
		scrnparams.function.set(varcom.sstrt);
		scrnparams.statuz.set(varcom.oK);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2100();
		}

		/* Calculate default share percentages if none are entered.        */
		/* IF WSAA-BNY-CNT          NOT = ZERO    AND           <V76L01>*/
		/*    WSAA-BNYPC-TOTAL          = ZERO    AND           <V76L01>*/
		/*    S5078-ERROR-SUBFILE       = SPACE                 <V76L01>*/
		/*    COMPUTE WSAA-QUOTA        = 100 / WSAA-BNY-CNT    <V76L01>*/
		/*    MOVE O-K                 TO SCRN-STATUZ           <V76L01>*/
		/*    MOVE SSTRT               TO SCRN-FUNCTION         <V76L01>*/
		/*    PERFORM 2400-DEFAULT-SHARE                        <V76L01>*/
		/*       UNTIL SCRN-STATUZ      = ENDP                  <V76L01>*/
		/* END-IF.                                                      */
		/* If entered, share percentages must be entered against           */
		/* all beneficiaries.                                              */
		/*  IF WSAA-NO-DETS         NOT = SPACE  AND            <V76L01>*/
		/*     WSAA-BNYPC-TOTAL         > ZERO                  <V76L01>*/
		/*     MOVE E518               TO SCRN-ERROR-CODE       <V76L01>*/
		/*     MOVE 'Y'                TO WSSP-EDTERROR         <V76L01>*/
		/* END-IF.                                              <V76L01>*/
		/* If the percentage share does not total 100%, display an*/
		/* error.*/
		/*  IF WSAA-BNYPC-TOTAL     NOT = 100                           */
		/*     AND BENEFICIARY-EXISTS                                   */
		/*      MOVE E631              TO SCRN-ERROR-CODE               */
		/*      MOVE 'Y'               TO WSSP-EDTERROR                 */
		/* END-IF.                                                      */
		/*   Check for mandatory beneficiary type in TR52Z.                */
		a100MandatoryBnfy();
		/*   Check for percentage under each beneficiary type.             */
		for (ix = 1; !(isGT(ix, 10)); ix++){
			if (isNE(wsaaPctTot[ix], 100)
			&& isNE(wsaaPctTot[ix], ZERO)) {
				scrnparams.errorCode.set(errorsInner.e631);
				wsspcomn.edterror.set("Y");
			}
		}
	}

protected void validateSubfile2100()
	{
		try {
			validateSubfile2120();
			updateErrorIndicators2170();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}
protected void callDatcon21600()
{
	/*PARA*/
	datcon2rec.frequency.set("01");
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	/*EXIT*/
}

protected void validateSubfile2120()
	{
		processScreen("S5078", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		
		if (isEQ(sv.bnysel, SPACES)) {
			sv.clntsname.set(SPACES);			
		}
		
		/* If details are blanked out, go on to the next record.*/
		/* IF S5078-BNYRLN              = SPACES                        */
		if (isEQ(sv.cltreln, SPACES)
		&& isEQ(sv.bnycd, SPACES)
		&& isEQ(sv.bnypc, ZERO)
		&& isEQ(sv.bnysel, SPACES)
		&& isEQ(sv.effdate, varcom.vrcmMaxDate)
		&& isEQ(sv.bnytype, SPACES)) {
		if(benesequence){
				if(isEQ(sv.sequence, SPACES)
						&& isEQ(sv.paymthbf, SPACES)
						&& isEQ(sv.bankacckey, SPACES)){
				sv.bankkey.set(SPACES);
				sv.bkrelackey.set(SPACES);
				sv.bnkcdedsc.set(SPACES);			
				scrnparams.function.set(varcom.srdn);
				goTo(GotoLabel.exit2190);
				}
		}
		else{		
			scrnparams.function.set(varcom.srdn);
			goTo(GotoLabel.exit2190);
		}
	}
		/* Check relationship. Validation check is replaced by T3584    */
		/* MOVE SPACES                 TO DESC-DATA-KEY.                */
		/* MOVE 'IT'                   TO DESC-DESCPFX.                 */
		/* MOVE WSSP-COMPANY           TO DESC-DESCCOY.                 */
		/* MOVE T5663                  TO DESC-DESCTABL.                */
		/* MOVE S5078-BNYRLN           TO DESC-DESCITEM.                */
		/* MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.                */
		/* MOVE READR                  TO DESC-FUNCTION.                */
		/* CALL 'DESCIO'               USING DESC-PARAMS.               */
		/* IF DESC-STATUZ           NOT = O-K                           */
		/*    AND                   NOT = MRNF                          */
		/*     MOVE DESC-STATUZ        TO SYSR-STATUZ                   */
		/*     PERFORM 600-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		/* IF DESC-STATUZ               = MRNF                          */
		/*     MOVE E468               TO S5078-BNYRLN-ERR              */
		/* END-IF.                                                      */
		/* Check beneficiary.*/
		checkBeneficiary2200();
		/* Check the percentage/code fields.*/
		checkShare2300();
		/* IF S5078-BNYRLN         NOT =  SPACES AND            <V76L01>*/
		setEffectiveDate();
		
		if (isEQ(sv.bnytype, SPACES)) {
			sv.bnytypeErr.set(errorsInner.e186);
		}
		if (isEQ(sv.cltreln, SPACES)) {
			sv.cltrelnErr.set(errorsInner.e186);
		}
		if (isEQ(sv.bnypc, ZERO)) {
			sv.bnypcErr.set(errorsInner.e186);
		}
		/*ICIL-11 start*/
		if(benesequence){
		if (isEQ(sv.sequence, SPACES)) {
			sv.sequenceErr.set(errorsInner.e186);
		}else if(sv.sequence.trim().length()<2){//ICIL-372
			sv.sequence.set("0".concat(sv.sequence.trim()));
		}
		}
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)
		|| isEQ(sv.effdate, ZERO)) {
			sv.effdateErr.set(errorsInner.h046);
		}
		if(benesequence){
		if(isEQ(sv.bankacckey, "EROR")) {//ICIL-384
			sv.paymthbfErr.set(errorsInner.rrbs);
			sv.bankacckey.set(SPACES);
		}
		 if(isEQ(sv.paymthbf,"C") || isEQ(sv.paymthbf,"4")) {
			if(isEQ(sv.bankacckey, SPACES)) {
				sv.bankacckeyErr.set(errorsInner.rrbr);
			}
		}
		 if(isNE(sv.paymthbf, SPACES) && isNE(sv.paymthbf,"C") && isNE(sv.paymthbf,"4") ) {
			if(isNE(sv.bankacckey, SPACES)&&isNE(sv.bankacckey, "EROR")) {
				sv.bankacckeyErr.set(errorsInner.e570);
			}
		}
		if(isNE(sv.bankacckey, SPACES)&&isNE(sv.bankacckey, "EROR")) {
			if(isEQ(sv.paymthbf, SPACES)) {
				sv.paymthbfErr.set(errorsInner.rrbs);
			}
		}
		}
		/*ICIL-11  end*/
		/* The Beneficiary effective date cannot be less than the RCD      */
		if (isLT(sv.effdate, chdrmnaIO.getOccdate())) {
			sv.effdateErr.set(errorsInner.h359);
		}
		if(isNE(sv.enddate, SPACES)&& isNE(sv.enddate, varcom.vrcmMaxDate)&&isLT(sv.enddate,sv.effdate)){
			sv.enddateErr.set(errorsInner.rfwr);
		}
	else{
	if(isEQ(sv.bnytype,"BD")){
	datcon2rec.intDate1.set(sv.effdate);
	datcon2rec.freqFactor.set(3);
	callDatcon21600();
	if(isEQ(sv.enddate, SPACES)||isEQ(sv.enddate, varcom.vrcmMaxDate)){
		sv.enddate.set(datcon2rec.intDate2);
	}
	else if(isNE(sv.enddate, SPACES)&& isNE(sv.enddate, varcom.vrcmMaxDate)&&isLT(sv.enddate,datcon2rec.intDate2)){
				sv.enddateErr.set(errorsInner.rfwq);
			}
		}
	}
		if(benesequence){
		validBankExist();
		}
	
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validBankExist() {
	if(isNE(sv.paymthbf,"C") && isNE(sv.paymthbf,"4")) {
		return ;
	}
	
	if(sv.bankacckey == null || isEQ(sv.bankacckey, SPACES)) {
		return ;
	}
	len = sv.bankacckey.toString().trim().length();
	lenRel = sv.bkrelackey.toString().trim().length();
	if(sv.bankacckey.toString().indexOf("*")==-1 && isNE(sv.bankacckey, sv.bkrelackey)) {
		sv.bkrelackey.set(sv.bankacckey);
	}else if(sv.bankacckey.toString().indexOf("*")!=-1 
		&& isNE(sv.bankacckey.toString().trim().substring(len-4, len), sv.bkrelackey.toString().trim().substring(lenRel-4, lenRel))){
		if(isEQ(sv.paymthbf, "C")) {
			sv.bankacckeyErr.set(errorsInner.rfh7);
		}else if(isEQ(sv.paymthbf, "4")) {
			sv.bankacckeyErr.set(errorsInner.f826);
		}
		return ;
	}else if(sv.bankacckey.toString().indexOf("*")!=-1 && len!=lenRel){
		if(isEQ(sv.paymthbf, "C")) {
			sv.bankacckeyErr.set(errorsInner.rfh7);
		}else if(isEQ(sv.paymthbf, "4")) {
			sv.bankacckeyErr.set(errorsInner.f826);
		}
		return ;
	}
	
	clbapf = new Clbapf();
	clbapf.setClntcoy(wsspcomn.fsuco.toString());
	clbapf.setClntpfx("CN");
	clbapf.setClntnum(sv.bnysel.toString().trim());
	
	if(isEQ(sv.paymthbf,"C")) {
		clbapf.setClntrel("CC");
		clbapf.setBsortcde(sv.bankkey.toString());
		clbapf.setBankacckey(ccfkpfDAO.retreiveReferenceNumber(sv.bkrelackey.toString().trim()));	
		if(clbapf.getBankacckey().equals("")) {
			sv.bankacckeyErr.set(errorsInner.rfh7);
			return ;
		}
	}else {
		clbapf.setBankkey(sv.bankkey.toString());
		clbapf.setClntrel("CB");
		clbapf.setBankacckey(sv.bkrelackey.toString());
	}
	
	if(!clbapfDAO.isExistClbapf(clbapf)) {
		if(isEQ(sv.paymthbf, "C")) {
			sv.bankacckeyErr.set(errorsInner.rfh7);
		}else if(isEQ(sv.paymthbf, "4")) {
			sv.bankacckeyErr.set(errorsInner.f826);
		}
	}
	
	
}
protected void updateErrorIndicators2170()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("S5078", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
	}

protected void checkBeneficiary2200()
	{
		
		if (isEQ(sv.bnysel, SPACES)) {
			sv.clntsname.set(SPACES);
			sv.bnyselErr.set(errorsInner.f691);
			return ;
		}
		if (isEQ(sv.bnytype, SPACES)) {
			sv.bnytypeErr.set(errorsInner.e186);
			return ;
		}
		if(benesequence){
		if (isEQ(sv.sequence, SPACES)) {
			sv.sequenceErr.set(errorsInner.e186);
			sv.sequenceOut[Varcom.pr.toInt()].set("Y");
			return ;
		}
		}

		clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.bnysel.toString().trim());  //Code change for ILIFE-6180 deployed with ILIFE-6304
		clntpf = clntpfDAO.selectClient(clntpf);
		if (clntpf == null) {
			/*     MOVE E339               TO S5078-BNYSEL-ERR              */
			sv.bnyselErr.set(errorsInner.e058);
			return ;
		}
		else {
			plainname();
		}
		/* Do not allow selection of a dead client                         */
		if (isNE(clntpf.getCltdod(), varcom.vrcmMaxDate)) {
			sv.bnyselErr.set(errorsInner.f782);
			return ;
		}
		/* MOVE WSSP-LONGCONFNAME      TO S5078-CLNTNM.                 */
		
		sv.clntsname.set(wsspcomn.longconfname);
		
		
		if (isNE(sv.cltreln, SPACES)) {
			a200ReadT3584();
		}
		b100CallTr52y();
		sv.relto.set(tr52yrec.relto);
		getOwnerNumber();
		
		if (isEQ(sv.relto, "L")) {
			wsaaClntnum.set(lifepf.getLifcnum());
		}
		chkAlrdyRlnshp2600();
		/* Check for a duplicate client number.*/
		/* PERFORM VARYING WSAA-DUPR-SUB FROM 1 BY 1                    */
		/*    UNTIL WSAA-DUPR-SUB      > SCRN-SUBFILE-RRN               */
		/*     IF S5078-BNYSEL         = WSAA-CLIENT(WSAA-DUPR-SUB)     */
		/*         MOVE E048          TO S5078-BNYSEL-ERR               */
		/*     END-IF                                                   */
		/* END-PERFORM.                                                 */
		//ILIFE-7646
		benausFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),BEN_FEATURE_ID, appVars, "IT");
		if(benausFlag) {
		if(null == benValue) {
			benValue = SPACE;
			}
		
		if(isEQ(benValue, SPACE)&& isNE(sv.bnytype, SPACES)) {
			benValue = sv.bnytype.trim();
			}
		
		if(isNE(sv.bnytype, SPACE) && !benValue.equals(sv.bnytype.toString())) {
			sv.bnytypeErr.set(errorsInner.rrlu);
			wsspcomn.edterror.set("Y");
			}
		}
		//ILIFE-7646
		for (wsaaDuprSub=1; !(isGT(wsaaDuprSub, scrnparams.subfileRrn)); wsaaDuprSub++){
			if (isEQ(sv.bnysel, wsaaClient[wsaaDuprSub])
			&& isEQ(sv.bnytype, wsaaType[wsaaDuprSub])
			&& isNE(sv.bnysel, SPACES)) {
				sv.bnyselErr.set(errorsInner.e048);
				wsspcomn.edterror.set("Y");
				return ;
			}
		}
		/* Check the relationship against blank and allow self flag        */
		/* in TR52Z.                                                       */
		if (isEQ(sv.cltreln, SPACES)) {
			b300ReadTr52z();
			if (found.isTrue()
			&& isNE(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
				sv.cltrelnErr.set(errorsInner.e186);
			}
		}
		wsaaClient[scrnparams.subfileRrn.toInt()].set(sv.bnysel);
		if (isNE(sv.bnytype, SPACES)
		&& isNE(sv.bnysel, SPACES)) {
			wsaaType[scrnparams.subfileRrn.toInt()].set(sv.bnytype);
			if(benesequence){
			wsaaSequence[scrnparams.subfileRrn.toInt()].set(sv.sequence);
			}
		}
		ib = 0;
		for (wsaaPctSub = 1; !(isGT(wsaaPctSub, 10)); wsaaPctSub++){
			if (isEQ(sv.bnytype, wsaaPctType[wsaaPctSub])) {
				if(benesequence){
					if(isEQ(sv.sequence, this.wsaaPctSeq[wsaaPctSub])){
				wsaaPctTot[wsaaPctSub].add(sv.bnypc);
				wsaaBnfyCount[wsaaPctSub].add(1);
				ib = wsaaPctSub;
				wsaaPctSub = 11;
					}
				}
				else{
					wsaaPctTot[wsaaPctSub].add(sv.bnypc);
					wsaaBnfyCount[wsaaPctSub].add(1);
					ib = wsaaPctSub;
					wsaaPctSub = 11;
				}
			}
			else {
				if (isEQ(wsaaPctType[wsaaPctSub], SPACES)) {
					wsaaPctType[wsaaPctSub].set(sv.bnytype);
					if(benesequence){
					wsaaPctSeq[wsaaPctSub].set(sv.sequence);
					}
					wsaaPctTot[wsaaPctSub].set(sv.bnypc);
					wsaaBnfyCount[wsaaPctSub].set(1);
					ib = wsaaPctSub;
					wsaaPctSub = 11;
				}
			}
		}
		if (isGT(wsaaBnfyCount[ib], 1)
		&& isEQ(tr52yrec.multind, "N")) {
			sv.bnyselErr.set(errorsInner.rfk6);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.relto, "N")) {
			b200MandRlsp();
		}
		
		//ILJ-ANSO
		if(NBPRP117permission) {
		initialize(antisoclkey.antisocialKey);
		antisoclkey.kjSName.set(clntpf.getLsurname());
		antisoclkey.kjGName.set(clntpf.getLgivname());
		antisoclkey.clntype.set(clntpf.getClttype());

		
		callProgram(Antisocl.class, antisoclkey.antisocialKey);
		
		if (isNE(antisoclkey.statuz, varcom.oK)){ 
			if(!beneficiary.contains(sv.bnysel.toString())) {
				sv.bnyselErr.set(antisoclkey.statuz);
				ansoErrStat=1;
				beneficiary.add(sv.bnysel.toString());
			}
			else if((ansoErrStat==0)) {
				sv.bnyselErr.set(antisoclkey.statuz);
				ansoErrStat=1;
			}
		
		}
		}
		//end
	}

protected void checkShare2300()
	{
		/*READ-T5662*/
		/* IF S5078-BNYCD               = SPACES                        */
		/*    AND S5078-BNYPC           = 0                             */
		/*     MOVE E518              TO S5078-BNYCD-ERR                */
		/*                               S5078-BNYPC-ERR                */
		/*     GO TO 2349-EXIT                                          */
		/* END-IF.                                                      */
		/* Read Beneficiary Codes table.*/
		if (isNE(sv.bnycd, SPACES)) {
			readT56622350();
		}
		if (isGT(sv.bnypc, 100)) {
			sv.bnypcErr.set(errorsInner.f348);
		}
		/*EXIT*/
	}

protected void readT56622350()
	{
	   itdmpfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), t5662, sv.bnycd.toString(), chdrmnaIO.getOccdate().toInt());
		
		if (itdmpfList == null || itdmpfList.size() == 0) {
			sv.bnycdErr.set(errorsInner.e470);
			return ;
		}
		t5662rec.t5662Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		sv.bnypc.set(t5662rec.bnypc);
	}

	/**
	* <pre>
	*2400-DEFAULT-SHARE SECTION.                              <V76L01>
	*2400-START.                                              <V76L01>
	*                                                         <V76L01>
	*    CALL 'S5078IO'           USING SCRN-SCREEN-PARAMS    <V76L01>
	*                                   S5078-DATA-AREA       <V76L01>
	*                                   S5078-SUBFILE-AREA.   <V76L01>
	*                                                         <V76L01>
	*    IF SCRN-STATUZ           NOT = O-K   AND             <V76L01>
	*       SCRN-STATUZ           NOT = ENDP                  <V76L01>
	*       MOVE SCRN-STATUZ         TO SYSR-STATUZ           <V76L01>
	*       PERFORM 600-FATAL-ERROR                           <V76L01>
	*    END-IF.                                              <V76L01>
	*                                                         <V76L01>
	*    IF SCRN-STATUZ               = ENDP                  <V76L01>
	*  The exit paragraph name will be standardised to 2090 -EXIT.
	*       GO TO 2400-EXIT
	*       GO TO 2090-EXIT                                   <V76L01>
	*    END-IF.                                              <V76L01>
	*                                                         <V76L01>
	*    IF S5078-BNYSEL          NOT = SPACE                 <V76L01>
	*       MOVE WSAA-QUOTA          TO S5078-BNYPC           <V76L01>
	*       ADD  WSAA-QUOTA          TO WSAA-BNYPC-TOTAL      <V76L01>
	*       MOVE SUPD                TO SCRN-FUNCTION         <V76L01>
	*                                                         <V76L01>
	*       CALL 'S5078IO'        USING SCRN-SCREEN-PARAMS    <V76L01>
	*                                   S5078-DATA-AREA       <V76L01>
	*                                   S5078-SUBFILE-AREA    <V76L01>
	*                                                         <V76L01>
	*       IF SCRN-STATUZ        NOT = O-K                   <V76L01>
	*          MOVE SCRN-STATUZ      TO SYSR-STATUZ           <V76L01>
	*          PERFORM 600-FATAL-ERROR                        <V76L01>
	*       END-IF                                            <V76L01>
	*    END-IF.                                              <V76L01>
	*                                                         <V76L01>
	*    MOVE SRDN                   TO SCRN-FUNCTION.        <V76L01>
	*                                                         <V76L01>
	*  The exit paragraph name will be standardised to 2090 -EXIT.
	*2400-EXIT.
	*2090-EXIT.                                               <V76L01>
	*    EXIT.                                                <V76L01>
	* </pre>
	*/
protected void chkAlrdyRlnshp2600()
	{
		
		crelpf = crelpfDAO.getMcrlByCoyAndNum(wsspcomn.fsuco.toString(), wsaaClntnum.toString(), wsspcomn.fsuco.toString(), sv.bnysel.toString());
		if (crelpf!=null && isEQ(sv.cltreln, SPACES)) {
			sv.cltreln.set(crelpf.getCltreln());
			return ;
		}
		if (crelpf!=null && isNE(sv.cltreln, crelpf.getCltreln()) && isNE(sv.cltreln, SPACES)) {
			sv.cltrelnErr.set(errorsInner.rfk0);
			wsspcomn.edterror.set("Y");
		}
	}

protected void a100MandatoryBnfy()
	{
		/*  Check for Mandatory Beneficiary's relation using TR52Z.        */
		wsaaMandatorys.set(SPACES);
		wsaaFound.set(SPACES);
		wsaaCount = 0;
		iz = 0;
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tr52z);
		itempf.setItemitem(chdrmnaIO.getCnttype().toString());
		itempf = itemDAO.getItempfRecord(itempf);
		
		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tr52z);
			itempf.setItemitem("***");
			itempf = itemDAO.getItempfRecord(itempf);
		}
		if (itempf != null) {
		tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}else{
		tr52zrec.tr52zRec.set(SPACES);	
		}
		for (ix=1; !(isGT(ix, 10)); ix++){
			if (isNE(tr52zrec.bnytype[ix], SPACES)
			&& isEQ(tr52zrec.manopt[ix], "Y")) {
				iz++;
				for (iy=1; !(isGT(iy, 30)
				|| isEQ(wsaaMandatory[iz], "Y")); iy++){
					wsaaMandatory[iz].set("N");
					if (isEQ(tr52zrec.bnytype[ix], wsaaType[iy])) {
						wsaaMandatory[iz].set("Y");
					}
				}
			}
		}
		if (ix>10) {
			ix=1;
			while ( !(notFound.isTrue())) {
				a300ContinueCheck();
			}

		}
		wsaaCount = wsaaCount + inspectTallyAll(wsaaMandatorys, "N");
		if (isGT(wsaaCount, 0)) {
			sv.chdrnumErr.set(errorsInner.rfk2);
			wsspcomn.edterror.set("Y");
		}
	}

protected void a200ReadT3584()
	{
		
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), t3584, sv.cltreln.toString(), wsspcomn.fsuco.toString(), wsspcomn.language.toString());
	
		if (descpf == null) {
			sv.cltrelnErr.set(errorsInner.w038);
		}
	}

protected void a300ContinueCheck()
	{
		if (isEQ(tr52zrec.gitem, SPACES)) {
			wsaaFound.set("N");
			return ;
		}
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tr52z);
		itempf.setItemitem(tr52zrec.gitem.toString());
		itempf = itemDAO.getItempfRecord(itempf);
	    if(itempf != null){
		tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
	    }else{
	    	tr52zrec.tr52zRec.set(SPACES);	
	    }
		for (ix=1; ix<=10; ix++){
			if (isNE(tr52zrec.bnytype[ix], SPACES)
			&& isEQ(tr52zrec.manopt[ix], "Y")) {
				iz++;
				for (iy=1; !(iy>30|| isEQ(wsaaMandatory[iz], "Y")); iy++){
					wsaaMandatory[iz].set("N");
					if (isEQ(tr52zrec.bnytype[ix], wsaaType[iy])) {
						wsaaMandatory[iz].set("Y");
					}
				}
			}
		}
	}

protected void b100CallTr52y()
	{
		/* Read Beneficiary Type Table - TR52Y.                            */
	    itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tr52y);
		itempf.setItemitem(sv.bnytype.toString());
		itempf = itemDAO.getItempfRecord(itempf);         //MLIL-230(Base Issue)
	    if(itempf != null){
		 tr52yrec.tr52yRec.set(StringUtil.rawToString(itempf.getGenarea()));
	    }else{
	     tr52yrec.tr52yRec.set(SPACES);
	    }
	}

protected void b200MandRlsp()
	{
		/*  Check for Mandatory Beneficiary's relation using TR52Z.        */
		wsaaCrtBnfyreln="N";
		wsaaBeneTypeFound.set("N");
		wsaaValidRelation.set("N");
		if (isNE(sv.bnytype, SPACES)) {
			b300ReadTr52z();
		}
		if (isEQ(wsaaCrtBnfyreln, "N")
		&& validRelation.isTrue()
		&& isNE(sv.bnysel, wsaaClntnum)) {
			sv.cltrelnErr.set(errorsInner.rfk1);
		}
		if (beneTypeFound.isTrue()
		&& isEQ(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
			if (isEQ(sv.bnysel, wsaaClntnum)) {
				sv.cltreln.set("SELF");
			}
			else {
				if (isEQ(sv.cltreln, "SELF")) {
					sv.bnyselErr.set(errorsInner.rfk1);
				}
			}
		}
		if (beneTypeFound.isTrue()
		&& isNE(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
			if (isEQ(sv.bnysel, wsaaClntnum)
			&& noSelfRelation.isTrue()) {
				sv.bnyselErr.set(errorsInner.rfk3);
			}
		}
		if (beneTypeFound.isTrue()) {
			wsbbStackArrayInner.wsbbSelfind[scrnparams.subfileRrn.toInt()].set(tr52zrec.selfind[wsbbTableSub.toInt()]);
		}
		else {
			wsbbStackArrayInner.wsbbSelfind[scrnparams.subfileRrn.toInt()].set(SPACES);
		}
		if (isEQ(wsaaCrtBnfyreln, "Y")) {
			wsbbStackArrayInner.wsbbRevcflg[scrnparams.subfileRrn.toInt()].set(tr52zrec.revcflg[wsbbTableRsub.toInt()]);
		}
		else {
			wsbbStackArrayInner.wsbbRevcflg[scrnparams.subfileRrn.toInt()].set(SPACES);
		}
	}

protected void b300ReadTr52z()
	{
		wsaaFound.set("Y");
		bentypeflag = false;
		/* Read the beneficiary type details from TR52Z for the            */
		/* contract type held on CHDRMNA.                                  */
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tr52z);
		itempf.setItemitem(chdrmnaIO.getCnttype().toString());
		itempf = itemDAO.getItempfRecord(itempf);
		
		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tr52z);
			itempf.setItemitem("***");
			itempf = itemDAO.getItempfRecord(itempf);
		}
		
		if(itempf != null){
			tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}else{
			tr52zrec.tr52zRec.set(SPACES);
		}
		
		/* Find the benef type in the table by checking the current table  */
		/* and if not found checking subsequent tables if they exist.      */
		wsbbTableSub.set(1);
		wsbbTableRsub.set(1);
		while ( !(isEQ(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)
		|| isNE(wsaaFound, "Y"))) {
			b400FindBnytype();
			if(!bentypeflag) {
				sv.bnytypeErr.set(errorsInner.rfl8);
			}
		}

		if (isEQ(sv.bnytype, tr52zrec.bnytype[wsbbTableSub.toInt()])) {
			beneTypeFound.setTrue();
		}
		/* Compute the subscript for the first relat and find the relat.   */
		/* Note the position of the bene type(found already above)         */
		/* determines the start position of the associated relationship.   */
		if (isNE(sv.cltreln, SPACES)
		&& beneTypeFound.isTrue()) {
			compute(wsbbTableRsub, 0).set(add(mult((sub(wsbbTableSub, 1)), 5), 1));
			while ( !(isEQ(tr52zrec.cltreln[wsbbTableRsub.toInt()], sv.cltreln)
			|| notFound.isTrue())) {
				b700FindRelation();
			}

		}
		if (isNE(sv.cltreln, SPACES)) {
			if (isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
				validRelation.setTrue();
				wsaaCrtBnfyreln="Y";
			}
		}
		if (isNE(sv.cltreln, SPACES)) {
			wsaaSelfRelation.set("N");
			if (isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
			&& isEQ(tr52zrec.cltreln[wsbbTableRsub.toInt()], "SELF")) {
				selfRelation.setTrue();
			}
		}
	}

protected void b400FindBnytype()
	{
		/*B410-BEGIN*/
		/* Actually find the BNYTYPE.                                      */
		for (wsbbTableSub.set(1); !(isGT(wsbbTableSub, 10)); wsbbTableSub.add(1)){
			b500Nothing();
		}
		/* If the type has not been found look in the continuation table   */
		if (isGT(wsbbTableSub, 10)
		|| isNE(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)) {
			wsbbTableSub.set(1);
			b600ContinuationTable();
		}
		/*B490-EXIT*/
	}

protected void b500Nothing()
	{
		/*B510-ABSOLUTLY-NOTHING*/
		if(isEQ(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)) {
			bentypeflag = true;
		}
		/*B590-EXIT*/
	}

protected void b600ContinuationTable()
	{
		/* Read the beneficiary type details from continuation table       */
		/* Last entry on the table is found and the beneficiary type       */
		/* looked for has not been found.                                  */
		if (isEQ(tr52zrec.gitem, SPACES)) {
			wsaaFound.set("N");
			return ;
		}
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tr52z);
		itempf.setItemitem(tr52zrec.gitem.toString());
        if(itempf != null){
		    tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
        }else{
        	tr52zrec.tr52zRec.set(SPACES);	
        }
	}

protected void b700FindRelation()
	{
		try {
			while ( !(isEQ(wsbbTableRsub, 50)
					|| isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
					|| notFound.isTrue())) {
						b770SearchLine();
					}

					/*  If not found on this table entry, try the next one.            */
					if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
						wsbbTableSub.set(1);
						wsbbTableRsub.set(1);
						b600ContinuationTable();
					}
					goTo(GotoLabel.b790Exit);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}


	/**
	* <pre>
	*  Check a single line of riders (performed paragraphs).
	* </pre>
	*/
protected void b770SearchLine()
	{
		for (int loopVar1 = 0; !(loopVar1 == 5); loopVar1 += 1){
			b780CheckElement();
		}
		if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
			if (isNE(wsbbTableSub, 10)) {
				wsbbTableSub.add(1);
				if (isNE(tr52zrec.bnytype[wsbbTableSub.toInt()], SPACES)) {
					wsaaFound.set("N");
				}
			}
		}
	}

protected void b780CheckElement()
	{
		if (isNE(tr52zrec.cltreln[wsbbTableRsub.toInt()], SPACES)) {
			validRelation.setTrue();
		}
		if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
		&& isNE(wsbbTableRsub, 50)) {
			wsbbTableRsub.add(1);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
	    bnfypf = new Bnfypf();
		bnfypf.setChdrcoy(chdrmnaIO.getChdrcoy().toString().charAt(0));
		bnfypf.setChdrnum(chdrmnaIO.getChdrnum().toString());
		wsaaPtrnFlag = "N";
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			update3100();
		}

		/* If the beneficiary records have been updated, write a*/
		/* transaction record and update the contract header record.*/
		if (isEQ(wsaaPtrnFlag, "Y")) {
			updateChdr3800();
			writePtrn3900();
			callBldenrl3a00();
			writeLetter6000();
		}
		
	}


protected void update3100()
	{
		processScreen("S5078", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			return ;
		}
		checkChange3200();
		/* IF WSAA-ADD                                                  */
		/*     PERFORM 3300-ADD-RECORD                                  */
		/* END-IF.                                                      */
		/* IF WSAA-DELETE                                               */
		/*     PERFORM 3400-DELETE-RECORD                               */
		/* END-IF.                                                      */
		/* IF WSAA-MODIFY                                               */
		/*     PERFORM 3500-MODIFY-RECORD                               */
		/* END-IF.                                                      */
		if (wsaaAdd.isTrue()) {
			addRecord3300();
		}
		if (wsaaDelete.isTrue()) {
			deleteRecord3400();
		}
		if (wsaaModify.isTrue()) {
			modifyRecord3500();
		}
		scrnparams.function.set(varcom.srdn);
	}

protected void checkChange3200()
	{
		
		/* MOVE SPACES                TO WSAA-UPDATE-FLAG.             */
		wsaaUpdateFlag.set(SPACES);
		wsaaRrn = scrnparams.subfileRrn.toInt();
		/* Check if nothing has actually changed.*/
		if (isEQ(sv.bnysel, wsbbStackArrayInner.wsbbBnysel[wsaaRrn])
		&& isEQ(sv.cltreln, wsbbStackArrayInner.wsbbBnyrln[wsaaRrn])
		&& isEQ(sv.bnycd, wsbbStackArrayInner.wsbbBnycd[wsaaRrn])
		&& isEQ(sv.bnypc, wsbbStackArrayInner.wsbbBnypc[wsaaRrn])
		&& isEQ(sv.effdate, wsbbStackArrayInner.wsbbEffdate[wsaaRrn])
		&& isEQ(sv.bnytype, wsbbStackArrayInner.wsbbBnytype[wsaaRrn])
		&& isEQ(sv.enddate, wsbbStackArrayInner.wsbbEnddate[wsaaRrn])) {
			if(benesequence){
				if(isEQ(sv.sequence, wsbbStackArrayInner.wsbbSequence[wsaaRrn])
						&& isEQ(sv.paymthbf, wsbbStackArrayInner.wsbbpaymthbf[wsaaRrn])
						&& isEQ(sv.bankkey, wsbbStackArrayInner.wsbbBankkey[wsaaRrn])
						&& isEQ(sv.bkrelackey, wsbbStackArrayInner.wsbbBankacckey[wsaaRrn])){
					return ;
				}
			}
			else
				return ;
		}
		/* Check if this is a new record.*/
		if (isNE(sv.bnysel, SPACES)
		&& isEQ(wsbbStackArrayInner.wsbbBnysel[wsaaRrn], SPACES)) {
			/*     MOVE 'ADD'              TO WSAA-UPDATE-FLAG              */
			wsaaUpdateFlag.set("ADD");
			return ;
		}
		/* Check if this is a record which has been deleted.*/
		if (isNE(wsbbStackArrayInner.wsbbBnysel[wsaaRrn], SPACES)
		&& isEQ(sv.bnysel, SPACES)) {
			/*     MOVE 'DEL'              TO WSAA-UPDATE-FLAG              */
			wsaaUpdateFlag.set("DEL");
			return ;
		}
		if (isEQ(sv.cltreln, SPACES)
		&& isEQ(sv.bnycd, SPACES)
		&& isEQ(sv.bnypc, ZERO)
		&& isEQ(sv.bnysel, SPACES)
		&& isEQ(sv.effdate, varcom.vrcmMaxDate)
		&& isEQ(sv.bnytype, SPACES)) {
			if(benesequence){
				if(isEQ(sv.sequence, SPACES))
					return ;
			}
			else{
				return ;
			}
		}
		/* If it is none of the above, it must be a modification.*/
		/* MOVE 'MOD'                  TO WSAA-UPDATE-FLAG.             */
		wsaaUpdateFlag.set("MOD");
	}

protected void addRecord3300()
	{
			/* Write a new beneficiary record.*/
		bnfypf.setChdrcoy(chdrmnaIO.getChdrcoy().toString().charAt(0));
		bnfypf.setChdrnum(chdrmnaIO.getChdrnum().toString());
		bnfypf.setBnyclt(sv.bnysel.toString());
		/* MOVE S5078-BNYRLN           TO BNFYMNA-BNYRLN.               */
		bnfypf.setCltreln(sv.cltreln.toString());
		bnfypf.setBnycd(sv.bnycd.toString().charAt(0));
		bnfypf.setBnytype(sv.bnytype.toString());
		bnfypf.setRelto(sv.relto.toString().charAt(0));
		bnfypf.setBnypc(BigDecimal.valueOf(sv.bnypc.toDouble()));
		bnfypf.setSelfind(wsbbStackArrayInner.wsbbSelfind[wsaaRrn].toString().charAt(0));
		bnfypf.setRevcflg(wsbbStackArrayInner.wsbbRevcflg[wsaaRrn].toString().charAt(0));
		bnfypf.setTranno(chdrmnaIO.getTranno().toInt());
		bnfypf.setValidflag("1".charAt(0));
		bnfypf.setCurrfr(datcon1rec.intDate.toInt());
		/*                                BNFYMNA-EFFDATE.              */
		bnfypf.setEffdate(sv.effdate.toInt());
		bnfypf.setEnddate(sv.enddate.toInt());
		bnfypf.setCurrto(varcom.vrcmMaxDate.toInt());
		bnfypf.setTermid(varcom.vrcmTermid.toString());
		bnfypf.setUserT(varcom.vrcmUser.toInt());
		bnfypf.setTrtm(varcom.vrcmTime.toInt());
		bnfypf.setTrdt(varcom.vrcmDate.toInt());
		if(benesequence){
		if(sv.sequence.length()<2) {
		bnfypf.setSequence("0".concat(sv.sequence.toString()));
		}else {
		bnfypf.setSequence(sv.sequence.toString());
		}
		bnfypf.setPaymmeth(sv.paymthbf.toString());
		bnfypf.setBankkey(sv.bankkey.toString());
		bnfypf.setBankacckey(sv.bkrelackey.toString());
		}
		if(bnfypfDAO.insertBnfypf(bnfypf)<=0){
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(chdrmnaIO.getChdrcoy().toString().concat(chdrmnaIO.getChdrnum().toString()));
			fatalError600();
		}
		/*    Check a client role for the beneficiary already existing     */
		/*    or not .                                                     */
		clrrpf = new Clrrpf();
		clrrpf.setClntpfx("CN");
		clrrpf.setClntcoy(wsspcomn.fsuco.toString());
		clrrpf.setClntnum(sv.bnysel.toString());
		clrrpf.setClrrrole("BN");
		clrrpf.setForepfx("CH");
		clrrpf.setForecoy(chdrmnaIO.getChdrcoy().toString());
		clrrpf.setForenum(sv.chdrnum.toString());
		clrrpf.setUsed2b("U");
	    clrrpf = clrrpfDAO.getClrrpfByClrrkeyAndUsed2b(clrrpf);
		if (clrrpf!=null) {
			skipClrn3380();
		}else{
		/*    Write a client role for the beneficiary.                     */
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.bnysel);
		cltrelnrec.clrrrole.set("BN");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(sv.chdrnum);
		cltrelnrec.function.set("ADD  ");
		callCltreln3700();
		skipClrn3380();
		}
	}

protected void skipClrn3380()
	{
		wsaaPtrnFlag = "Y";
		/* If record doesn't exists in MCRL file , write a fresh record.   */
		if (isEQ(sv.bnysel, wsaaClntnum)) {
			return ;
		}
		
		crelpf = crelpfDAO.getMcrlByCoyAndNum(wsspcomn.fsuco.toString(), wsaaClntnum.toString(), wsspcomn.fsuco.toString(), sv.bnysel.toString());
		
		if (crelpf == null	&& isNE(sv.cltreln, SPACES)) {
			crelpf = new Crelpf();
			crelpf.setClntcoy(wsspcomn.fsuco.toString());
			crelpf.setMcltcoy(wsspcomn.fsuco.toString());
			crelpf.setMcltnum(wsaaClntnum.toString());
			crelpf.setClntnum(sv.bnysel.toString());
			crelpf.setCltreln(sv.cltreln.toString());
			crelpf.setValidflag("1");
			varcom.vrcmTranid.set(wsspcomn.tranid);
			crelpf.setTermid(varcom.vrcmTermid.toString());
			varcom.vrcmTimex.set(getCobolTime());
			varcom.vrcmTime.set(varcom.vrcmTimen);
			varcom.vrcmDate.set(getCobolDate());
			crelpf.setTermid(varcom.vrcmTermid.toString());
			crelpf.setUser_t(varcom.vrcmUser.toInt());
			crelpf.setTrdt(varcom.vrcmDate.toInt());
			crelpf.setTrtm(varcom.vrcmTime.toInt());
			
			if (crelpfDAO.inertMcrl(crelpf)==0) {
				syserrrec.params.set(wsspcomn.fsuco.toString().concat(wsaaClntnum.toString()).concat(sv.bnysel.toString()));
				syserrrec.statuz.set("MRNF");
				fatalError600();
			}
		}
	}

protected void deleteRecord3400()
	{
		
	    bnfypf = new Bnfypf();
		bnfypf.setChdrcoy(chdrmnaIO.getChdrcoy().toString().charAt(0));
		bnfypf.setChdrnum(chdrmnaIO.getChdrnum().toString());
		bnfypf.setBnytype(wsbbStackArrayInner.wsbbBnytype[wsaaRrn].toString());
	//	bnfypf.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaRrn].toString());   /* MLIL-627 */
	    bnfypf.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaRrn].toString().trim());  /* MLIL-627 */
		bnfypf.setTranno(chdrmnaIO.getTranno().toInt());
		List<Bnfypf> list = bnfypfDAO.getBnfymnaByKeyAndTranno(bnfypf);
		if(list != null && list.size() > 0){
			for(int i=0; i<list.size();i++){
				bnfypf = list.get(i);
				callBnfymna3420();
			}
		}
		
	
	}

	/**
	* <pre>
	**** PERFORM 3600-CALL-BNFYMNAIO.
	* </pre>
	*/
protected void callBnfymna3420()
	{
	    bnfypf.setValidflag("2".charAt(0));
	    bnfypf.setCurrto(datcon1rec.intDate.toInt());
		if(bnfypfDAO.updateBnfymnaByUnqueNum(bnfypf)<=0){
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(chdrmnaIO.getChdrcoy().toString().concat(chdrmnaIO.getChdrnum().toString()));
			fatalError600();
		}
		/* MOVE REWRT                  TO BNFYMNA-FUNCTION.             */
		wsaaLastTranno = bnfypf.getTranno();
		
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(bnfypf.getBnyclt());
		cltrelnrec.clrrrole.set("BN");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(sv.chdrnum);
		cltrelnrec.function.set("REM  ");
		callCltreln3700();
		wsaaPtrnFlag = "Y";
	}



protected void modifyRecord3500()
	{

		/* Validflag 2 old beneficiary record.*/
	    bnfypf = new Bnfypf();
	    bnfypf.setChdrcoy(chdrmnaIO.getChdrcoy().charat(0));
	    bnfypf.setChdrnum(chdrmnaIO.getChdrnum().toString());
	    bnfypf.setBnytype(wsbbStackArrayInner.wsbbBnytype[wsaaRrn].toString());
		bnfypf.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaRrn].toString().trim());
		bnfypf.setTranno(chdrmnaIO.getTranno().toInt());
	    List<Bnfypf> list = bnfypfDAO.getBnfymnaByKeyAndTranno(bnfypf);
	    if(list != null && list.size() > 0){
	    	for(int i=0;i<list.size();i++){
	    		bnfypf = list.get(i);
	    		callBnfymna3520();
	    	}
	    }
		
		/* MOVE READH                  TO BNFYMNA-FUNCTION.             */
	
	}

	/**
	* <pre>
	**** PERFORM 3600-CALL-BNFYMNAIO.
	* </pre>
	*/
protected void callBnfymna3520()
	{
		
		
	    bnfypf.setValidflag("2".charAt(0));
	    bnfypf.setCurrto(datcon1rec.intDate.toInt());
	    bnfypfDAO.updateBnfymnaByUnqueNum(bnfypf);
		/* MOVE REWRT                  TO BNFYMNA-FUNCTION.             */

		wsaaLastTranno = bnfypf.getTranno();
		/* Write new beneficiary record.*/
		instBnfypf = new Bnfypf();
		instBnfypf.setChdrcoy(chdrmnaIO.getChdrcoy().charat(0));
		instBnfypf.setChdrnum(chdrmnaIO.getChdrnum().toString());
		instBnfypf.setBnytype(sv.bnytype.toString());
		instBnfypf.setBnyclt(sv.bnysel.toString());
		/* MOVE S5078-BNYRLN           TO BNFYMNA-BNYRLN.               */
		instBnfypf.setCltreln(sv.cltreln.toString());
		instBnfypf.setRelto(sv.relto.toString().charAt(0));
		instBnfypf.setBnycd(sv.bnycd.charat(0));
		instBnfypf.setBnypc(sv.bnypc.getbigdata());
		instBnfypf.setSelfind(wsbbStackArrayInner.wsbbSelfind[wsaaRrn].charat(0));
		instBnfypf.setRevcflg(wsbbStackArrayInner.wsbbRevcflg[wsaaRrn].charat(0));
		instBnfypf.setTranno(chdrmnaIO.getTranno().toInt());
		instBnfypf.setCurrfr(datcon1rec.intDate.toInt());
		instBnfypf.setCurrto(varcom.vrcmMaxDate.toInt());
		/* If the beneficiary has changed, set the effective from date*/
		/* to the business date.  if not, do not alter the effective*/
		/* from date.*/
		/* IF S5078-BNYSEL          NOT = WSBB-BNYSEL(WSAA-RRN)         */
		/*     MOVE DTC1-INT-DATE      TO BNFYMNA-EFFDATE               */
		/* END-IF.                                                      */
		instBnfypf.setEffdate(sv.effdate.toInt());
		instBnfypf.setEnddate(sv.enddate.toInt());
		instBnfypf.setValidflag("1".charAt(0));
		instBnfypf.setTermid(varcom.vrcmTermid.toString());
		instBnfypf.setUserT(varcom.vrcmUser.toInt());
		instBnfypf.setTrtm(varcom.vrcmTime.toInt());
		instBnfypf.setTrdt(varcom.vrcmDate.toInt());
		if(benesequence){
		if(sv.sequence.length()<2) {
			instBnfypf.setSequence("0".concat(sv.sequence.toString()));
		}else {
		instBnfypf.setSequence(sv.sequence.toString());
		}
		instBnfypf.setPaymmeth(sv.paymthbf.toString());
		instBnfypf.setBankkey(sv.bankkey.toString());
		instBnfypf.setBankacckey(sv.bkrelackey.toString());
		}
        if(bnfypfDAO.insertBnfypf(instBnfypf)<=0){
        	syserrrec.statuz.set("MRNF");
			syserrrec.params.set(chdrmnaIO.getChdrcoy().concat(chdrmnaIO.getChdrnum().toString()));
			fatalError600();
        }
		/* Remove old client relation record and write new record.*/
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(wsbbStackArrayInner.wsbbBnysel[wsaaRrn]);
		cltrelnrec.clrrrole.set("BN");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(sv.chdrnum);
		cltrelnrec.function.set("REM  ");
		callCltreln3700();
		/* Check a client role for the beneficiary already existing     */
		/* or not .                                                     */
		clrrpf = new Clrrpf();
		clrrpf.setClntpfx("CN");
		clrrpf.setClntcoy(wsspcomn.fsuco.toString());
		clrrpf.setClntnum(sv.bnysel.toString());
		clrrpf.setClrrrole("BN");
		clrrpf.setForepfx("CH");
		clrrpf.setForecoy(chdrmnaIO.getChdrcoy().toString());
		clrrpf.setForenum(sv.chdrnum.toString());
		clrrpf.setUsed2b("U");
	    clrrpf = clrrpfDAO.getClrrpfByClrrkeyAndUsed2b(clrrpf);
		if (clrrpf != null) {
			skipClrn3570();
		}else{
		cltrelnrec.clntnum.set(sv.bnysel);
		cltrelnrec.function.set("ADD  ");
		callCltreln3700();
		skipClrn3570();
		}
	}

protected void skipClrn3570()
	{
		wsaaPtrnFlag = "Y";
		/* If record doesn't exists in MCRL file , write a fresh record.   */
		if (isEQ(sv.bnysel, wsaaClntnum)) {
			return ;
		}

		crelpf = crelpfDAO.getMcrlByCoyAndNum(wsspcomn.fsuco.toString(), wsaaClntnum.toString(), wsspcomn.fsuco.toString(), sv.bnysel.toString());
	
	
		if (crelpf == null && isNE(sv.cltreln, SPACES)) {
			crelpf = new Crelpf();
			crelpf.setClntcoy(wsspcomn.fsuco.toString());
			crelpf.setMcltcoy(wsspcomn.fsuco.toString());
			crelpf.setMcltnum(wsaaClntnum.toString());
			crelpf.setClntnum(sv.bnysel.toString());
			crelpf.setCltreln(sv.cltreln.toString());
			crelpf.setValidflag("1");		
			varcom.vrcmTranid.set(wsspcomn.tranid);
			crelpf.setTermid(varcom.vrcmTermid.toString());
			varcom.vrcmTimex.set(getCobolTime());
			varcom.vrcmTime.set(varcom.vrcmTimen);
			varcom.vrcmDate.set(getCobolDate());
			crelpf.setTermid(varcom.vrcmTermid.toString());
			crelpf.setUser_t(varcom.vrcmUser.toInt());
			crelpf.setTrdt(varcom.vrcmDate.toInt());
			crelpf.setTrtm(varcom.vrcmTime.toInt());
			
			if (crelpfDAO.inertMcrl(crelpf)==0) {
				syserrrec.params.set(wsspcomn.fsuco.toString().concat(wsaaClntnum.toString()).concat(sv.bnysel.toString()));
				syserrrec.statuz.set("MRNF");
				fatalError600();
			}
		}
	}


protected void callCltreln3700()
	{
		/*CALL*/
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateChdr3800()
	{
		chdrmnaIO.setFunction(varcom.readh);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmnaIO.getStatuz());
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		setPrecision(chdrmnaIO.getTranno(), 0);
		chdrmnaIO.setTranno(add(chdrmnaIO.getTranno(), 1));
		chdrmnaIO.setFunction(varcom.rewrt);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmnaIO.getStatuz());
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
	}

protected void writePtrn3900()
	{
		
	    ptrnpf = new Ptrnpf();
	    ptrnpf.setChdrpfx("CH");
		ptrnpf.setChdrcoy(chdrmnaIO.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrmnaIO.getChdrnum().toString());
		ptrnpf.setTranno(chdrmnaIO.getTranno().toInt());		
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		wsaaBatckey1.set(wsspcomn.batchkey);
		ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey1.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey1.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
		
		List<Ptrnpf> list = new ArrayList<Ptrnpf>();
		list.add(ptrnpf);
		
		
		if (!ptrnpfDAO.insertPtrnPF(list)) {
			syserrrec.params.set(chdrmnaIO.getChdrcoy().toString().concat(chdrmnaIO.getChdrnum().toString()));
			fatalError600();
		}
	}

protected void callBldenrl3a00()
	{
		/*A10-START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrmnaIO.getChdrpfx());
		bldenrlrec.company.set(chdrmnaIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrmnaIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
		/*A90-EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/

		unlockSftlock5000();
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}


protected void loadProgram4400()
{
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX].set(ptemprec.progOut[wsaaY]);
	wsaaX++;
	wsaaY++;
	/*EXIT1*/
}

protected void unlockSftlock5000()
	{
	
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void writeLetter6000()
	{
					start6010();
					readTr3846020();
				}

protected void start6010()
	{
		/* Read Table T6634 for get letter-type.                           */
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
	}

	/**
	* <pre>
	*6020-READ-T6634.                                         <PCPPRT>
	* </pre>
	*/
protected void readTr3846020()
	{
	    itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634        TO ITEM-ITEMITEM.        <PCPPRT>*/
		itempf.setItemtabl(tr384);
		itempf.setItemitem(wsaaItemTr384.toString());
		itempf = itemDAO.getItempfRecord(itempf);

		/* If not record not found then read again using generic key.      */
		if (itempf == null) {
		
			if (itempf == null 	&& isNE(wsaaItemCnttype, "***")) {
				wsaaItemCnttype.set("***");
				/*     GO TO 6020-READ-T6634                            <PCPPRT>*/
				readTr3846020();
				return ;
			}
			else {
				syserrrec.statuz.set("MRNF");
				syserrrec.params.set(tr384.concat(wsaaItemTr384.toString()));
				fatalError600();
			}
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* IF T6634-LETTER-TYPE        = SPACES                 <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*   Get Endorsement no.                                       */
		/* MOVE 'NEXT '                TO ALNO-FUNCTION.        <PCPPRT>*/
		/* MOVE 'EN'                   TO ALNO-PREFIX.          <PCPPRT>*/
		/* MOVE WSKY-BATC-BATCBRN      TO ALNO-GENKEY.          <PCPPRT>*/
		/* MOVE WSSP-COMPANY           TO ALNO-COMPANY.         <PCPPRT>*/
		/* CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                 <PCPPRT>*/
		/* IF ALNO-STATUZ              NOT = O-K                <PCPPRT>*/
		/*    MOVE ALNO-STATUZ         TO SYSR-STATUZ           <PCPPRT>*/
		/*    MOVE 'ALOCNO'            TO SYSR-PARAMS           <PCPPRT>*/
		/*    PERFORM 600-FATAL-ERROR.                          <PCPPRT>*/
		/*   Get set-up parameter for call 'HLETRQS'                   */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		/* MOVE 'EN'                   TO LETRQST-RDOCPFX.      <PCPPRT>*/
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		/* MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.      <PCPPRT>*/
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.branch.set(wsspcomn.branch);
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(wsspcomn.language);
		wsaaOkeyBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaOkeyTranno.set(wsaaLastTranno);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure WSBB-STACK-ARRAY--INNER
 */
public static final class WsbbStackArrayInner {

	private FixedLengthStringData wsbbStackArray = new FixedLengthStringData(4050);
	private FixedLengthStringData wsbbBnytypeArray = new FixedLengthStringData(60).isAPartOf(wsbbStackArray, 0);
	private FixedLengthStringData[] wsbbBnytype = FLSArrayPartOfStructure(30, 2, wsbbBnytypeArray, 0);
	private FixedLengthStringData wsbbBnyrlnArray = new FixedLengthStringData(120).isAPartOf(wsbbStackArray, 60);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbBnyrln = FLSArrayPartOfStructure(30, 4, wsbbBnyrlnArray, 0);
	private FixedLengthStringData wsbbReltoArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 180);
	private FixedLengthStringData[] wsbbRelto = FLSArrayPartOfStructure(30, 1, wsbbReltoArray, 0);
	private FixedLengthStringData wsbbRevcflgArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 210);
	public FixedLengthStringData[] wsbbRevcflg = FLSArrayPartOfStructure(30, 1, wsbbRevcflgArray, 0);
	private FixedLengthStringData wsbbBnycdArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 240);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbBnycd = FLSArrayPartOfStructure(30, 1, wsbbBnycdArray, 0);
	private FixedLengthStringData wsbbBnypcArray = new FixedLengthStringData(150).isAPartOf(wsbbStackArray, 270);
		/*                             OCCURS 10.                       */
	private ZonedDecimalData[] wsbbBnypc = ZDArrayPartOfStructure(30, 5, 2, wsbbBnypcArray, 0, UNSIGNED_TRUE);
	private FixedLengthStringData wsbbBnyselArray = new FixedLengthStringData(300).isAPartOf(wsbbStackArray, 420);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbBnysel = FLSArrayPartOfStructure(30, 10, wsbbBnyselArray, 0);
	private FixedLengthStringData wsbbClntnmArray = new FixedLengthStringData(900).isAPartOf(wsbbStackArray, 720);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbClntnm = FLSArrayPartOfStructure(30, 30, wsbbClntnmArray, 0);
	private FixedLengthStringData wsbbEffdateArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 1620);
		/*                             OCCURS 10.                       */
	private PackedDecimalData[] wsbbEffdate = PDArrayPartOfStructure(30, 8, 0, wsbbEffdateArray, 0);
	private FixedLengthStringData wsbbEnddateArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 1860);
	/*                             OCCURS 10.               <V76L01>*/
private PackedDecimalData[] wsbbEnddate = PDArrayPartOfStructure(30, 8, 0, wsbbEnddateArray, 0);

	private FixedLengthStringData wsbbChangeArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2100);
		/*                             OCCURS 30.                       */
	private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(30, 1, wsbbChangeArray, 0);
	private FixedLengthStringData wsbbSelfindArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2130);
	public FixedLengthStringData[] wsbbSelfind = FLSArrayPartOfStructure(30, 1, wsbbSelfindArray, 0);
	/*ICIL-11*/
	private FixedLengthStringData wsbbSequenceArray = new FixedLengthStringData(60).isAPartOf(wsbbStackArray, 2160);
	private FixedLengthStringData[] wsbbSequence = FLSArrayPartOfStructure(30, 2, wsbbSequenceArray, 0);
	private FixedLengthStringData wsbbpaymthbfArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2220); 
	private FixedLengthStringData[] wsbbpaymthbf = FLSArrayPartOfStructure(30, 1, wsbbpaymthbfArray, 0);
	private FixedLengthStringData wsbbBankkeyArray = new FixedLengthStringData(300).isAPartOf(wsbbStackArray, 2250);
	private FixedLengthStringData[] wsbbBankkey = FLSArrayPartOfStructure(30, 10, wsbbBankkeyArray, 0);
	private FixedLengthStringData wsbbBankacckeyArray = new FixedLengthStringData(600).isAPartOf(wsbbStackArray, 2550);
	private FixedLengthStringData[] wsbbBankacckey = FLSArrayPartOfStructure(30, 20, wsbbBankacckeyArray, 0);
	private FixedLengthStringData wsbbBankdescArray = new FixedLengthStringData(900).isAPartOf(wsbbStackArray, 3150);
	private FixedLengthStringData[] wsbbBankdesc = FLSArrayPartOfStructure(30, 30, wsbbBankdescArray, 0);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private String e048 = "E048";
	private String e186 = "E186";
	private String e631 = "E631";
	private String e058 = "E058";
	private String e470 = "E470";
	private String f348 = "F348";
	private String f691 = "F691";
	private String h046 = "H046";
	private String h359 = "H359";
	private String f782 = "F782";
	private String rfk0 = "RFK0";
	private String rfk1 = "RFK1";
	private String rfk2 = "RFK2";
	private String rfk3 = "RFK3";
	private String rfk6 = "RFK6";
	private String w038 = "W038";
	private String rfwq = "RFWQ";
	private String rfwr = "RFWR";
	private String h093 = "H093";
	private String rrbr = "RRBR";//ICIL-11	
	private String rrbs = "RRBS";//ICIL-11	
	private String e570 = "E570";//ICIL-11	
	private String rfh7 = "RFH7";//ICIL-11
	private String f826 = "F826";//ICIL-11
	private String rrlu = "RRLU";//ILIFE-7646
	private String rfl8 = "RFL8";
	
}

protected void setEffectiveDate()	{
	
		if (isNE(sv.cltreln, SPACES) 
		&& isNE(sv.bnytype, SPACES) 
		&& isNE(sv.bnycd, SPACES) 
		&& isNE(sv.bnypc, ZERO)
		&& isNE(sv.bnysel, SPACES) 
		&& (isEQ(sv.effdate, varcom.vrcmMaxDate) 
		|| isEQ(sv.effdate, ZERO))) {
			sv.effdate.set(datcon1rec.intDate);
		}
	}

protected void getOwnerNumber() {
	if (isEQ(sv.relto, "O")
	|| isEQ(sv.relto, "B")) {
		wsaaClntnum.set(chdrmnaIO.getCownnum());
	}
}



}
