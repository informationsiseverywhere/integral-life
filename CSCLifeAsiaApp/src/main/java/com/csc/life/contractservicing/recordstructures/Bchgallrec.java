package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:32
 * Description:
 * Copybook name: BCHGALLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bchgallrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bchgallRec = new FixedLengthStringData(63);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(bchgallRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(bchgallRec, 4);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(bchgallRec, 8);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(bchgallRec, 9);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(bchgallRec, 17);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(bchgallRec, 19);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(bchgallRec, 21);
  	public ZonedDecimalData planSuffix = new ZonedDecimalData(4, 0).isAPartOf(bchgallRec, 23);
  	public FixedLengthStringData oldBillfreq = new FixedLengthStringData(2).isAPartOf(bchgallRec, 27);
  	public FixedLengthStringData newBillfreq = new FixedLengthStringData(2).isAPartOf(bchgallRec, 29);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(bchgallRec, 31).setUnsigned();
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(bchgallRec, 39).setUnsigned();
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(bchgallRec, 44);
  	public PackedDecimalData occdate = new PackedDecimalData(8, 0).isAPartOf(bchgallRec, 48).setUnsigned();
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(bchgallRec, 53);
  	public FixedLengthStringData trancode = new FixedLengthStringData(4).isAPartOf(bchgallRec, 56);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(bchgallRec, 60);


	public void initialize() {
		COBOLFunctions.initialize(bchgallRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bchgallRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}