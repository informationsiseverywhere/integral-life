/*
 * File: LoanpymtPojo.java
 * Date: 13 Oct 2018 22:58:40
 * Author: sbatra9
 * 
 * Purpose :curd data for Loanpymt
 * 
 * Copyright (2007) DXC, all rights reserved.
 */ 
package com.csc.life.contractservicing.procedures;


public class LoanpymtPojo {
	
	private String function;
  	private String statuz;
	private String chdrnum;
	private String chdrcoy;
	private String loanNumber;
	private String loanType;
	private String lastTranno;
	
	public LoanpymtPojo() {
		function = "";
		statuz = "";
		chdrnum = "";
		chdrcoy = "";
		loanNumber = "";
		loanType = "";
		lastTranno = "";
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public String getLastTranno() {
		return lastTranno;
	}
	public void setLastTranno(String lastTranno) {
		this.lastTranno = lastTranno;
	}
	
	
}