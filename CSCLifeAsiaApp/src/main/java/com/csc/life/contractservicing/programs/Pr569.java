/*
 * File: Pr569.java
 * Date: 30 August 2009 1:42:11
 * Author: Quipoz Limited
 *
 * Class transformed from PR569.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrincTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CvuwTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CprppfDAO;
import com.csc.life.contractservicing.procedures.LincCSService;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.contractservicing.screens.Sr569ScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
//import com.csc.life.productdefinition.dataaccess.model.Covtpf;
//import com.csc.life.productdefinition.dataaccess.dao.impl.CovtpfDAOImpl;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.procedures.LincCommonsUtil;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*             PR569 - COMPONENT SELECTION MAJOR ALTS
*             ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Initialise
*----------
*
*  Skip  this  section  if  returning from an optional selection
*  (current stack position action flag = '*').
*
*  Clear the subfile ready for loading.l
*
*  The  details  of  the  contract  being  enquired upon will be
*  stored  in  the  CHDRMJA I/O module. Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       The joint  owner's  client (CLTS) details if they exist.
*
*  Load the subfile as follows:
*
*  Retrieve the COVRMJA file (RETRV), if the selected plan has a
*  plan suffix  which  is  contained  within the summarised plan
*  record:  less than  or  equal  to  the  number  of summarised
*  policies (CHDR-POLSUM):  then the policy must be 'broken out'
*  with each life,  coverage  and rider for an associated policy
*  written to the  subfile  along  with  the  description of the
*  cover/rider and their short status code descriptions.
*
*  COVR-STATCODE will be decoded against T5682 - Risk Status and
*  COVR-PSTATCODE  will  be  decoded  against  T5681  -  Premium
*  Status.
*
*       The first record of the subfile will have the first life
*       details for a contract. There will be one or many lives,
*       for each  life, there will be one or many coverages. For
*       each coverage, there will be none, one or many riders.
*
*       Use a key of CHDRCOY  and CHDRNUM, life  number 01 and a
*       joint life  number 00 to perform a BEGN on the life file
*       LIFEALT.
*
*       Display the  Life  number from the LIFEALT record in the
*       Component   Number   field.  Place  the  Client  Number,
*       (LIFE-LIFCNUM),   in   the   Component   field  and  the
*       "confirmation name"  (from  CLTS)  for  the  life as the
*       Component   Description.   Use   the   Contract  Company
*       (CHDRCOY),   Contract   Number  (CHDRNUM),  Life  Number
*       (LIFE),Plan suffix from COVRMJA (If contained within the
*       summarised records,  store  the  plan  suffix number and
*       move '0000' to Plan suffix.) with Coverage and Rider set
*       to '00',then perform a BEGN on COVRMJA. This will return
*       the first coverage for the Life.
*
*       If  the stored  Plan  Suffix  equal  to  '0000'  then  a
*       selection is  required across  the whole plan, therefore
*       load all  records  with  broken out Policies placing the
*       Plan suffix in  the Hidden Field. i.e. ignore changes in
*       Plan suffix,  exit  when change of company, and contract
*       number.
*
*       Coverage details  -  write  the  coverage  record to the
*       subfile with the coverage number in the Component Number
*       field.  Obtain the Coverage Code from T5673 and place it
*       in the Component field indented by 2 spaces. Look up its
*       description from  T5687  for the element description and
*       place   this   in   the   Component  Description  field.
*       Sequentially read the next coverage/rider record.
*
*       NOTE:   read  T5673  the  Contract  Structure  table  by
*       contract  type  (from  CHDRMJA)  and  effective  on  the
*       original   inception   date   of   the   contract.  When
*       processing this table item remember that it can continue
*       onto  additional  (unlimited) table items.  These should
*       be read in turn during the processing.
*
*       Rider details -  if  the  coverage number is the same as
*       the one  put  on  the coverage subfile record above, the
*       rider number will  not  be  zero  (if  it  is, this is a
*       database error).  This is a rider on the above coverage.
*       Write the  rider  record  to  the subfile with the rider
*       number in the Component Number field and its description
*       from   T5687   in   the   Component  Description  field.
*       Sequentially  read  the  next coverage/rider record.  If
*       this  is   another   rider   record   for   the  current
*       transaction, repeat  this  rider  processing.  If  it is
*       another  coverage   for   the   same  life,  repeat  the
*       processing from  the coverage section above.  If it is a
*       coverage  for   another   life,  repeat  all  the  above
*       processing for the  next  life.  If it is a coverage for
*       another proposal (or the end of the file) all components
*       have been  loaded. On each subfile load, store in hidden
*       fields the Plan Suffix, life, cover and rider numbers.
*
*  Load all pages  required  in  the subfile and set the subfile
*  more indicator to no.
*
*Validation
*----------
*
*  There is no validation in this program.
*
*Updating
*--------
*
*  If any  Increase records exist for this contract (INCR), then
*  Warning  Message 'WARNING: Incr Pend'g del' will be displayed.
*  If the  user  presses enter then  these INCR records will  be
*  deleted  and the  increase  recalculated  as  normal  through
*  batch processing.  Additionally, any  COVR  records that have
*  the COVR-INDEXATION-IND set to 'P' (Pending)  will be updated
*  to have spaces in this field.
*
*Next Program
*------------
*
*  If "KILL" was requested move spaces to  the  current  program
*  position and action field, add 1 to the  program  pointer and
*  exit.
*
*  At this point the program will be either  searching  for  the
*  FIRST  selected  record  in  order  to pass  control  to  the
*  appropriate  generic  enquiry  program   for   the   selected
*  component  or it will be returning from one  of  the  Enquiry
*  screens after displaying some details and  searching  for the
*  NEXT selected record.
*
*  It will be able  to determine which of these two states it is
*  in by examining the Stack Action Flag.
*
*  If not returning  from  a  component (stack action is blank),
*  save the next four  programs currently on the stack. Read the
*  first record from the subfile. If this is not  selected, read
*  the next one and so on, until a selected  record is found, or
*  the end of the subfile is reached.
*
*  If a subfile  record  has been selected, look up the programs
*  required to  be  processed  from  the coverage/rider programs
*  table (T5671  -  accessed  by transaction number concatenated
*  with coverage/rider code from the subfile record). Move these
*  four programs into  the  program  stack  and  set the current
*  stack action to '*'  (so  that the system will return to this
*  program to process the next one).
*
*  RLSE the COVRMJA record, then read with the key below. If O-K
*  set up the  key details of the coverage/rider to be processed
*  (in COVRMJA using  the KEEPS func) by the  called programs as
*  follows:
*
*            Company - WSSP company
*            Contract no - from CHDRMJA
*            Plan Suffix - from hidden Plan suffix field
*            Life number - from hidden life field
*            Coverage number -  from hidden coverage field
*            Rider number - from hidden rider field
*
*  Add one to  the  program  pointer  and  exit  to  process the
*  required generic component.
*
*  If nothing was selected or there are no  more  selections  to
*  process,  continue  by  moving blanks to  the  current  Stack
*  Action  field  reload  the  saved  four programs and exit.
*
*Notes.
*------
*
*  Tables Used:
*
*T5671 - Cover/Rider Secondary Switching Key: Transaction Code
*                                           + Program Number
*T3588 - Contract Premium Status         Key: PSTATCODE
*T3623 - Contract Risk Status            Key: STATCODE
*T3673 - Contract Structure              Key: CNTTYPE
*T5688 - Contract Structure              Key: CNTTYPE
*
*Examples.
*---------
*
*  In this  example  the Contract Header record, (CHDR), has the
*  following values:
*
*CHDR-POLSUM = 3
*CHDR-POLINC = 5
*CHDR-NXTSFX = 0004
*
*  This indicates that  the number of policies in plan is 5, the
*  number summarised  is  3  and  therefore the next plan suffix
*  number is 0004.
*
*<-- File Details -->          <--------  Screen Details -------->
*
* Plan  Life Cov Rid           Component     Component Description
*Suffix                        OO OOOOOOOO   OOOOOOOOOOOOOOOO
*
* 0000   01   01  00              0001
*                                 0002
*                                 0003
*                              01 12345678   Life Name #1
*                              01   COV1     Coverage #1
* 0000   01   01  01           01     RID1   Rider #1 of Coverage #1
* 0000   01   01  02           02     RID2   Rider #2 of Coverage #1
* 0000   01   02  00           01   COV2     Coverage #2
* 0000   01   02  01           01     RID1   Rider #1 of Coverage #2
* 0000   02   01  00           02 87654321   Life Name #2
*                              01   COV1     Coverage #1
* 0000   02   01  01           01     RID1   Rider #1 of Coverage #1
* 0000   02   01  02           02     RID2   Rider #2 of Coverage #1
*
*  The  above  records  show the lives and  components  for  the
*  summary records. In this example the first  three  lines show
*  which  policies  with  the  plan  are   summarised  by  these
*  components.
*
*  At this point  the program will come upon the first component
*  for the next  policy  within  the  plan  -  one that does not
*  summarise  anything   and   so   its  display  will  be  more
*  straightforward.
*
* Plan  Life Cov Rid           Component     Component Description
*Suffix                        OO OOOOOOOO   OOOOOOOOOOOOOOOO
*
* 0004   01   01  00              0004
*                             01 12345678   Life Name #1
*                             01   COV1     Coverage #1
* 0004   01   01  01          01     RID1   Rider #1 of Coverage #1
* 0004   01   01  02          02     RID2   Rider #2 of Coverage #1
* 0004   01   01  03          03     RID3   Rider #3 of Coverage #1
* 0004   01   02  00          01   COV2     Coverage #2
* 0004   01   02  01          01     RID1   Rider #1 of Coverage #2
* 0004   01   03  00          03   COV3     Coverage #3
* 0004   02   01  00          02 87654321   Life Name #2
*                             01   COV1     Coverage #1
* 0004   02   01  01          01     RID1   Rider #1 of Coverage #1
* 0004   02   01  02          02     RID2   Rider #2 of Coverage #1
* 0005   01   01  00             0005
*                             01 12345678   Life Name #1
*                             01   COV1     Coverage #1
* 0005   01   01  01          01     RID1   Rider #1 of Coverage #1
* 0005   01   01  02          02     RID2   Rider #2 of Coverage #1
* 0005   01   01  03          03     RID3   Rider #3 of Coverage #1
* 0005   01   02  00          01   COV2     Coverage #2
* 0005   01   02  01          01     RID1   Rider #1 of Coverage #2
* 0005   01   03  00          03   COV3     Coverage #3
* 0005   02   01  00          02 87654321   Life Name #2
*                             01   COV1     Coverage #1
* 0005   02   01  01          01     RID1   Rider #1 of Coverage #1
* 0005   02   01  02          02     RID2   Rider #2 of Coverage #1
*
*
*****************************************************************
* </pre>
*/
public class Pr569 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR569");
	private PackedDecimalData wsaaPrsub = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatus = "";
	private String wsaaFirstTime = "";

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1);
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-MISCELLANEOUS-FLAGS */
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");

	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");

	private FixedLengthStringData wsaaLifeExit = new FixedLengthStringData(1);
	private Validator wsaaLifeEof = new Validator(wsaaLifeExit, "Y");

	private FixedLengthStringData wsaaCoverExit = new FixedLengthStringData(1);
	private Validator wsaaCoverEof = new Validator(wsaaCoverExit, "Y");

	private FixedLengthStringData wsaaLifeFound = new FixedLengthStringData(1);
	private Validator wsaaLifeFoundNow = new Validator(wsaaLifeFound, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaLifemjaKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaLastLifeno = new FixedLengthStringData(2).init(SPACES);
	private ZonedDecimalData wsaaLastSuffix = new ZonedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaLastCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaLastRider = new FixedLengthStringData(2).init(SPACES);
	private static final String wsaaLapse = "T514";
	private static final String wsaaPup = "T575";
	private FixedLengthStringData wsaaSaveLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaSaveCoverage = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaSavePlansufx = new ZonedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
		/* WSAA-PROGRAM-SAVE */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(4, 5);

		/*    '(Redirection)'.
		    '(Switch)     '.                                             */
	private FixedLengthStringData wsaaInvalidCtdate = new FixedLengthStringData(1);
	private Validator invalidCtdate = new Validator(wsaaInvalidCtdate, "Y");

	private FixedLengthStringData wsaaProcess = new FixedLengthStringData(1);
	private Validator addComp = new Validator(wsaaProcess, "A");
	private Validator modComp = new Validator(wsaaProcess, "M");
	protected Validator appAdd = new Validator(wsaaProcess, "P");
	private Validator appMod = new Validator(wsaaProcess, "S");
	private Validator revComp = new Validator(wsaaProcess, "R");
	private Validator addLife = new Validator(wsaaProcess, "D");
	private Validator enqComp = new Validator(wsaaProcess, "I");
	private FixedLengthStringData wsaaComponent = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaLife = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCovtCoverage = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaIndicStatus = new FixedLengthStringData(1);
	private Validator indicFound = new Validator(wsaaIndicStatus, "Y");

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private String wsaaChange = "";

	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrincTableDAM covrincIO = new CovrincTableDAM();
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private CvuwTableDAM cvuwIO = new CvuwTableDAM();
	private IncrmjaTableDAM incrmjaIO = new IncrmjaTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T7508rec t7508rec = new T7508rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Th506rec th506rec = new Th506rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	protected Sr569ScreenVars sv = getLScreenVars();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	protected WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private TablesInner tablesInner = new TablesInner();
	
	//TSD-266
	private ZonedDecimalData wsaaLDob = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaJlDob = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCDob = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaJcDob = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaLSex = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaCSex = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaJlSex = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaJcSex = new FixedLengthStringData(1).init(SPACES);
	private boolean ispermission = false;
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private List<Covrpf> covrpfList;
	//private Covrpf covrpfIO;
	private int covrpfCount = 0;
	private Lifepf lifepfIO;
	private int lifepfCount = 0;
	private List<Lifepf> lifepfList;
	private Covtpf covtpfIO;
	private int covtpfCount = 0;
	private List<Covtpf> covtpfList;
	
	//TSD-266
	//ILIFE-6586
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Ptrnpf ptrnpf;
	private Clntpf clts;
	private Itempf itempf = null;
	private boolean statusFlag = false;
	//ILIFE-6809
	private RacdpfDAO racdpfDAO = this.getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private CprppfDAO cprppfDAO = getApplicationContext().getBean("cprppfDAO",CprppfDAO.class);
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private Descpf descpf;
	private Batckey wsaaBatckey1 = new Batckey();

	private static final String h017 = "H017";
    private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
    private T5688rec t5688rec = new T5688rec();
    private T5677rec t5677rec = new T5677rec();
    private FixedLengthStringData wsaaT5677Key = new FixedLengthStringData(8);
    private FixedLengthStringData wsaaT5677Tranno = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 0);
    private FixedLengthStringData wsaaT5677FollowUp = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 4);
	boolean CSMIN003Permission  = false;
	boolean NBPRP056Permission  = false;
	private static final String t555 = "T555";
	private static final String t557 = "T557";
	private static final String NBPRP056 = "NBPRP056";
	private static final String t5661 = "T5661";
	private static final String f665 = "F665"; //ILIFE-8486
	private static final String t6a8 = "T6A8";
	private static final String t556 = "T556";
	private boolean lincFlag = false;
	private static final String lincFeature = "NBPRP116";

			
	protected Sr569ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars( Sr569ScreenVars.class);
	}
	
	//ILB-456 start 
    private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	//ILIFe-8075
	private List<Chdrpf> chdrpfList;
	List<Fluppf> fluprevIOList;
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private T5661rec t5661rec = new T5661rec();
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private Map<String,Descpf> descMap = new HashMap<>();
	private Map<String,String> crtableMap = new HashMap<>();
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090,
		listCoverages1150,
		exit1190,
		a1480Return,
		a1490Exit,
		a1580Return,
		a1590Exit,
		covPrem1850,
		ridPrem1860,
		valid1880,
		exit1890,
		exit2090,
		continue4080,
		updateScreen4085,
		exit4090,
		exit4190,
		exit4290,
		readProgramTable4410,
		loadProgsToWssp4420,
		exit4490,
		exit4590,
		exit4890,
		readCovt4910,
		callCprp5450,
		exit5400
	}

	public Pr569() {
		super();
		screenVars = sv;
		new ScreenModel("Sr569", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clts.getClttype())) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clts.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clts.getClttype())) {
			corpname();
			return ;
		}
		if (isNE(clts.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clts.getClttype())) {
			corpname();
			return ;
		}
		if ("1".equals(clts.getEthorig())) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clts.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clts.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clts.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clts.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clts.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			retrvContract1020();
			retreiveUwdate1035();
			retrvCoverage1040();
			readLifeDetails1050();
			jointLifeDetails1060();
			contractTypeStatusDesc();
			subfileLoad1080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise1010()
	{
		/* If returning from a multiple selection then go to exit.*/
	if(isNE(wsspcomn.flag,"X"))
    {
          wsaaProcess.set(wsspcomn.flag);
   }

		//wsaaProcess.set(wsspcomn.flag);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			/*       MOVE SPACES TO SR569-TEXTFIELD*/
			/*       MOVE 'Y' TO SR569-TEXTFIELD-OUT(ND)*/
			/*       MOVE 'N' TO SR569-TEXTFIELD-OUT(BL)*/
			if (isEQ(wsaaBatckey.batcBatctrcde, "T551")) {
				/*         MOVE REDIRECTION TO SR569-TEXTFIELD                    */
				/*         MOVE TR386-PROGDESC-3  TO SR569-TEXTFIELD              */
				sv.textfield.set(tr386rec.progdesc03);
				sv.textfieldOut[varcom.bl.toInt()].set("Y");
				sv.textfieldOut[varcom.nd.toInt()].set("N");
				goTo(GotoLabel.exit1090);
			}
			else {
				if (isEQ(wsaaBatckey.batcBatctrcde, "T676")) {
					/*            MOVE SWITCH TO SR569-TEXTFIELD                      */
					/*            MOVE TR386-PROGDESC-4  TO SR569-TEXTFIELD           */
					sv.textfield.set(tr386rec.progdesc04);
					sv.textfieldOut[varcom.bl.toInt()].set("Y");
					sv.textfieldOut[varcom.nd.toInt()].set("N");
					goTo(GotoLabel.exit1090);
				}
				else {
					sv.textfield.set(SPACES);
					sv.textfieldOut[varcom.bl.toInt()].set("N");
					sv.textfieldOut[varcom.nd.toInt()].set("Y");
					goTo(GotoLabel.exit1090);
				}
			}
		}
		/* Clear the WORKING STORAGE.*/
		wsaaImmexitFlag.set(SPACES);
		wsaaSelectFlag.set(SPACES);
		wsaaLifeExit.set(SPACES);
		wsaaCoverExit.set(SPACES);
		wsaaLastLifeno.set(SPACES);
		wsaaLastCoverage.set(SPACES);
		wsaaLastRider.set(SPACES);
		wsaaLastSuffix.set(ZERO);
		wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
		wsaaFirstTime = "Y";
		/* Clear the Subfile.*/
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		ispermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "MTL26601", appVars,smtpfxcpy.item.toString()); //ILIFE-2345-2347
		CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
		NBPRP056Permission  = FeaConfg.isFeatureExist("2", NBPRP056, appVars, "IT");
		lincFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), lincFeature, appVars, "IT");
		
	}

protected void retrvContract1020()
	{
		/* Read CHDRMJA (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/*HEADER-DETAIL*/
		/* Move Contract details to the screen header.*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrpf.getPolinc());
	}

protected void retreiveUwdate1035()
	{
		/* Read CVUW to get the underwriting decision date.*/
		cvuwIO.setRecKeyData(SPACES);
		cvuwIO.setRecNonKeyData(SPACES);
		cvuwIO.setChdrcoy(chdrpf.getChdrcoy());
		cvuwIO.setChdrnum(chdrpf.getChdrnum());
		cvuwIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(), varcom.oK)
		&& isNE(cvuwIO.getStatuz(), varcom.mrnf)
		&& isNE(cvuwIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(cvuwIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), cvuwIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), cvuwIO.getChdrnum())
		|| isNE(cvuwIO.getStatuz(), varcom.oK)) {
			sv.huwdcdte.set(varcom.vrcmMaxDate);
		}
		else {
			sv.huwdcdte.set(cvuwIO.getHuwdcdte());
		}
		if (!modComp.isTrue()) {
			sv.huwdcdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void retrvCoverage1040()
	{
		/* Retrieve the coverage in order to obtain policy selected.*/
			//ILB-456 
			covrpf = covrpfDAO.getCacheObject(covrpf);
			if(null==covrpf) {
				covrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, covrmjaIO);
				if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(covrmjaIO.getParams());
					fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
				
		selectRecordCustomerSpecific();

		sv.planSuffix.set(covrpf.getPlanSuffix());
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		/*--- Read TR386 table to get screen literals                      */
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("TR386");
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itempf.setItemitem(wsaaTr386Key.toString().trim());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsspcomn.company.toString().concat("TR386").concat(wsaaTr386Key.toString()));
			fatalError600();
		}
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* If Covr plan suffix is zero then we have selected accross the*/
		/*    whole plan.*/
		/* If Covr plan suffix is NOT zero then we have selected a partic-*/
		/*    ular Policy.*/
		if (isEQ(sv.planSuffix, ZERO)) {
			wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("L");
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			/*      MOVE 'Whole Plan      '  TO SR569-ENTITY                  */
			/*      MOVE TR386-PROGDESC-1    TO SR569-ENTITY                  */
			sv.entity.set(tr386rec.progdesc01);
			sv.entityOut[varcom.hi.toInt()].set("Y");
		}
		else {
			wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("O");
			sv.plnsfxOut[varcom.nd.toInt()].set(" ");
			/*      MOVE 'Policy Number  :'  TO SR569-ENTITY                  */
			/*      MOVE TR386-PROGDESC-2    TO SR569-ENTITY                  */
			sv.entity.set(tr386rec.progdesc02);
			sv.entityOut[varcom.hi.toInt()].set(" ");
		}
		/* If the policy selected is a summarised policy then to read the*/
		/* Coverage/Rider records we must zeroise the Plan Suffix.*/
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())
		&& isNE(covrpf.getPlanSuffix(), ZERO)
		&& isNE(chdrpf.getPolsum(), 1)) {
			wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		}
		/* If a COVR record has been retrieved then the program must have*/
		/* been invoked from the Policy Selection screen. The record is*/
		/* then released and processing should continue as for a Plan. The*/
		/* following BEGN will position this program at the first COVR*/
		/* record for the selected policy.*/
		/*covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
//ILIFE-8075 start
		covrpfDAO.deleteCacheObject(covrpf);
		/*covrpfList = covrpfDAO.searchCovrRecordForContract(covrpf);
		if(covrpfList.isEmpty()) {
			fatalError600();
		}*/
//ILIFE-8075 end
		if (isEQ(wsaaBatckey.batcBatctrcde, "T551")) {
			/*     MOVE REDIRECTION TO SR569-TEXTFIELD                       */
			/*      MOVE TR386-PROGDESC-3  TO SR569-TEXTFIELD                 */
			sv.textfield.set(tr386rec.progdesc03);
			sv.textfieldOut[varcom.bl.toInt()].set("Y");
			sv.textfieldOut[varcom.nd.toInt()].set("N");
		}
		else {
			if (isEQ(wsaaBatckey.batcBatctrcde, "T676")) {
				/*        MOVE SWITCH TO SR569-TEXTFIELD                         */
				/*        MOVE TR386-PROGDESC-4  TO SR569-TEXTFIELD              */
				sv.textfield.set(tr386rec.progdesc04);
				sv.textfieldOut[varcom.bl.toInt()].set("Y");
				sv.textfieldOut[varcom.nd.toInt()].set("N");
			}
			else {
				sv.textfield.set(SPACES);
				sv.textfieldOut[varcom.bl.toInt()].set("N");
				sv.textfieldOut[varcom.nd.toInt()].set("Y");
			}
		}
	}

protected void readLifeDetails1050()
	{
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
	/*
	 	lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
	 	lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
	 
	 sv.lifenum.set(lifemjaIO.getLifcnum());
	 cltsIO.setClntnum(lifemjaIO.getLifcnum());
	 cltsIO.setClntcoy(wsspcomn.fsuco);
	 cltsIO.setClntpfx("CN");
     cltsIO.setFunction(varcom.readr);
	 SmartFileCode.execute(appVars, cltsIO);
	 if (isNE(cltsIO.getStatuz(), varcom.oK)) {
	 syserrrec.params.set(cltsIO.getParams());
	 fatalError600();
	 }
		/* Format the Name in Plain format.
	 plainname();
	 sv.lifename.set(wsspcomn.longconfname);
		//TSD-266 Starts
	 wsaaLDob.set(lifemjaIO.getCltdob());
	 wsaaLSex.set(lifemjaIO.getCltsex());
	 wsaaCDob.set(cltsIO.getCltdob());
	 wsaaCSex.set(cltsIO.getCltsex());
			  
		//TSD-266 Ends
	 */
		
		/*ILIFE-3395 STARTS */
		Lifepf lifeData = new Lifepf();		
		LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);		
		lifeData.setChdrcoy(chdrpf.getChdrcoy().toString());
		lifeData.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
		lifeData.setValidflag("1");
		
		lifepfList = lifepfDAO.searchLifeRecordByChdrNum(lifeData.getChdrcoy(), lifeData.getChdrnum());
		
		if(lifepfList.size() == 0)
		{
			syserrrec.params.set(clts.getClntcoy().concat(clts.getClntnum()));
			fatalError600();
		}
		
		for(lifepfCount=0;lifepfCount<lifepfList.size();lifepfCount++)
		{   
			if(isEQ(lifepfList.get(lifepfCount).getJlife(), "00"))
			{
			  lifepfIO=lifepfList.get(lifepfCount);
		
		/* Get the client name.*/
			sv.lifenum.set(lifepfIO.getLifcnum());
			clts = new Clntpf();
			clts.setClntnum(lifepfIO.getLifcnum());
			clts.setClntcoy(wsspcomn.fsuco.toString());
			clts.setClntpfx("CN");
			clts = clntpfDAO.getCltsRecordByKey(clts);
			if (clts == null) {
				syserrrec.params.set(lifepfIO.getLifcnum().concat(wsspcomn.fsuco.toString()));
				fatalError600();
			}
		/* Format the Name in Plain format.*/
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		//TSD-266 Starts
		wsaaLDob.set(lifepfIO.getCltdob());
		wsaaLSex.set(lifepfIO.getCltsex());
		wsaaCDob.set(clts.getCltdob());
		wsaaCSex.set(clts.getCltsex());
		break;
		}
		}
		/*ILIFE-3395 Ends */
	}	

		

protected void jointLifeDetails1060()
	{
		/* Check for the existence of Joint Life details.
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
	    SmartFileCode.execute(appVars, lifemjaIO);
		/*  IF LIFEMJA-STATUZ           NOT = O-K*/
		/*     MOVE 'NONE'              TO SR569-JLIFE*/
		/*                                 SR569-JLIFENAME*/
		/*  ELSE
	
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
		sv.jlife.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(cltsIO.getParams());
		fatalError600();
		}
		else {
		plainname();
		sv.jlifename.set(wsspcomn.longconfname);
		}
		//TSD-266 Ends
		*/
	   
	    /*ILIFE-3395 STARTS */
	      
			if(lifepfList.size() >0)
			{  for(lifepfCount=0;lifepfCount<lifepfList.size();lifepfCount++)
				
			{
				if(isEQ(lifepfList.get(lifepfCount).getJlife(), "01"))
				{
				lifepfIO=lifepfList.get(lifepfCount);
			
			
				sv.jlife.set(lifepfIO.getLifcnum());
				clts.setClntnum(lifepfIO.getLifcnum());
				clts.setClntcoy(wsspcomn.fsuco.toString());
				clts.setClntpfx("CN");
				clts = clntpfDAO.getCltsRecordByKey(clts);
				if (clts==null) {
					syserrrec.params.set(clts.getClntcoy().concat(clts.getClntnum()));
					fatalError600();
				}
				else {
					plainname();
					sv.jlifename.set(wsspcomn.longconfname);
				}
			
			wsaaJlDob.set(lifepfIO.getCltdob());
			wsaaJlSex.set(lifepfIO.getCltsex());
			wsaaJcDob.set(clts.getCltdob());
	      	wsaaJcSex.set(clts.getCltsex());
	      	
	      	break;
				}
	      	
			}
			}
			/*ILIFE-3395 ENDS */	
			
	}
protected void contractTypeStatusDesc()
{
	Map<String,String> statusDescMap = new HashMap<>();
	statusDescMap.put("T5688", chdrpf.getCnttype());//IJTI-1410
	statusDescMap.put("T3623", chdrpf.getStatcode());//IJTI-1410
	statusDescMap.put("T3588", chdrpf.getPstcde());//IJTI-1410
	    
	descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), statusDescMap);
	contractTypeStatus1070();
}
protected void contractTypeStatus1070()
	{
		/* Obtain the Contract Type description from T5688.*/
		//ILIFE-6809
		//descpf=descDAO.getdescData("IT", "T5688" ,chdrpf.getCnttype().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descMap == null || descMap.isEmpty() || descMap.get("T5688")==null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descMap.get("T5688").getLongdesc());
		}
		/* Obtain the Contract Status description from T3623.*/

		//descpf=descDAO.getdescData("IT", "T3623" ,chdrpf.getStatcode().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descMap == null || descMap.isEmpty() || descMap.get("T3623")==null) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descMap.get("T3623").getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588.*/
		//descpf=descDAO.getdescData("IT", "T3588" ,chdrpf.getPstcde().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descMap == null || descMap.isEmpty() || descMap.get("T3588")==null) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descMap.get("T3588").getShortdesc());
		}
	}

protected void subfileLoad1080()
	{
		wsaaLifeFound.set("N");
		/* MOVE  1                     TO WSAA-LIFE-NUM.*/
		wsaaLifeExit.set("N");
		for(lifepfCount=0;lifepfCount<lifepfList.size();lifepfCount++)
		{   
			if(isEQ(lifepfList.get(lifepfCount).getJlife(), "00"))
			{
			lifepfIO=lifepfList.get(lifepfCount);
			processLife1100();
		    } 
		}
		scrnparams.subfileRrn.set(1);
		updateScreenIndic1900a();
	}

protected void updateScreenIndic1900a()
	{
		/*A*/
		scrnparams.function.set(varcom.sstrt);
		scrnparams.statuz.set(varcom.oK);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			obtainIndic1910a();
		}

		scrnparams.statuz.set(varcom.oK);
		/*A-EXIT*/
	}

	/**
	* <pre>
	*1910A-OBTAIN-INDIC.
	* </pre>
	*/
protected void obtainIndic1910a()
	{
		obtain1910a();
	}

protected void obtain1910a()
	{
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.oK)) {
			findIndic1920a();
			if (indicFound.isTrue()) {
				sv.asterisk.set("*");
				scrnparams.function.set(varcom.supd);
				processScreen("SR569", sv);
				if (isNE(scrnparams.statuz, varcom.oK)
				&& isNE(scrnparams.statuz, varcom.endp)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
			else if (isNE(wsaaBatckey.batcBatctrcde.toString(), t6a8)
					&& isNE(wsaaBatckey.batcBatctrcde.toString(), t556)) {
				sv.selectOut[varcom.pr.toInt()].set("Y");
				sv.selectOut[varcom.nd.toInt()].set("Y");
				scrnparams.function.set(varcom.supd);
				processScreen("SR569", sv);
				if (isNE(scrnparams.statuz, varcom.oK)
				&& isNE(scrnparams.statuz, varcom.endp)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
		scrnparams.function.set(varcom.srdn);
	}

	/**
	* <pre>
	*1920A-FIND-INDIC.
	* </pre>
	*/
protected void findIndic1920a()
	{
		find1920a();
	}

protected void find1920a()
	{
		wsaaIndicStatus.set("N");
		if (isEQ(sv.cmpntnum, " +")) {
			return ;
		}
		if (isNE(subString(sv.component, 1, 1), SPACES)) {
			wsaaLife.set(sv.cmpntnum);
		}
		else {
			if (isNE(subString(sv.component, 3, 1), SPACES)) {
				wsaaCovtCoverage.set(sv.cmpntnum);
				wsaaComponent.set(subString(sv.component, 3, 4));
				findFromCovt1930a();
			}
			else {
				if (isNE(subString(sv.component, 5, 1), SPACES)) {
					wsaaComponent.set(subString(sv.component, 5, 4));
					findFromCovt1930a();
				}
			}
		}
	}

	/**
	* <pre>
	*1930A-FIND-FROM-COVT.
	* </pre>
	*/
protected void findFromCovt1930a(){
	//ILIFE-6809
//	List<Covtpf> covtList = covtpfDAO.getCovtpfData(wsspcomn.company.toString(),chdrmjaIO.getChdrnum().toString(), wsaaLife.toString(), wsaaCovtCoverage.toString(),"00", 0); commented for ILIFE-7372
	//Modified for ILIFE-8178
	List<Covtpf> covtList = covtpfDAO.getCovtpfData(wsspcomn.company.toString(),chdrpf.getChdrnum(), wsaaLife.toString(), wsaaCovtCoverage.toString(),sv.hrider.toString());// modified for ILIFE-7372 IJTI-1410
	for(Covtpf covtpf : covtList){
		if (covtpf.getCrtable().equals(wsaaComponent.toString())) {
			wsaaIndicStatus.set("Y");
			break;
		}
	}
}


protected void processLife1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					mainLife1110();
					screenFields1120();
					subfileAdd1130();
					jlifeCheck1140();
				case listCoverages1150:
					listCoverages1150();
				case exit1190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void mainLife1110()
	{
		/* Set up the LIFE details from the previously read LIFEMJA record*/
		/*IF WSAA-LIFE-FOUND              = 'Y'                    <007*/
		/*     GO TO  1120-SCREEN-FIELDS.                          <007*/
		/*  SKIP READ IF RECORD ALREADY IN W-S
		if (isEQ(lifemjaIO.getFunction(), varcom.nextr)) {
			/*NEXT_SENTENCE
		}
		else {
			getLifemja1900();
		}
		/* CALL 'LIFEMJAIO'            USING LIFEMJA-PARAMS.*/
		/* IF  LIFEMJA-STATUZ          NOT = O-K*/
		/* AND LIFEMJA-STATUZ          NOT = ENDP*/
		/*      MOVE LIFEMJA-PARAMS    TO SYSR-PARAMS*/
		/*      PERFORM 600-FATAL-ERROR.*/
		/* If change of Contract or Branch then exit.
		if (isNE(wsspcomn.company, lifemjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), lifemjaIO.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(), varcom.endp)) {
			wsaaLifeExit.set("Y");
			goTo(GotoLabel.exit1190);
		}
		*/
		
	/*ILIFE-3395 STARTS */
		if (isNE(wsspcomn.company, lifepfIO.getChdrcoy())
				|| isNE(chdrpf.getChdrnum(), lifepfIO.getChdrnum())
				||lifepfCount>lifepfList.size()) {
					wsaaLifeExit.set("Y");
					goTo(GotoLabel.exit1190);
				}
		/*ILIFE-3395 ENDS */
	}


protected void screenFields1120()
	{
	/*ILIFE-3395 STARTS */
		
		sv.subfileFields.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.selectOut[varcom.nd.toInt()].set("Y");
		sv.cmpntnum.set(lifepfIO.getLife());
		wsaaMiscellaneousInner.wsaaLifeNo.set(lifepfIO.getLife());
		sv.hlifeno.set(lifepfIO.getLife());
		sv.component.set(lifepfIO.getLifcnum());
		sv.hlifcnum.set(lifepfIO.getLifcnum());
		clts.setClntnum(lifepfIO.getLifcnum());
		sv.hsuffix.set(ZERO);
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts.setClntpfx("CN");
		clts = clntpfDAO.getCltsRecordByKey(clts);
		if (clts == null) {
			syserrrec.params.set(wsspcomn.fsuco.toString().concat(lifepfIO.getLifcnum()));
			fatalError600();
		}
		plainname();
		sv.deit.set(wsspcomn.longconfname);
		//ILIFE-2345-2347 Start
		if(ispermission){
			if (isNE(clts.getCltdob(), lifepfIO.getCltdob())) {
				sv.cmpntnumErr.set(formatsInner.rfpk);
				wsspcomn.edterror.set("Y");
			} 
			if (isNE(clts.getCltsex(), lifepfIO.getCltsex())) {
				sv.hlifenoErr.set(formatsInner.pfpl);
				wsspcomn.edterror.set("Y");
			}
		}
		/*ILIFE-3395 ENDS */
	}

protected void subfileAdd1130()
	{
		/* Add a record to the subfile.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void jlifeCheck1140()
	{
		/* Check for the existence of Joint Life details.*/
		/*MOVE NEXTR                  TO LIFEMJA-FUNCTION.*/
		/*CALL 'LIFEMJAIO' USING LIFEMJA-PARAMS.*/
		/*IF LIFEMJA-STATUZ           NOT = O-K*/
		/*AND LIFEMJA-STATUZ          NOT = ENDP*/
		/*    MOVE LIFEMJA-PARAMS     TO SYSR-PARAMS*/
		/*    PERFORM 600-FATAL-ERROR.*/
		/*IF LIFEMJA-CHDRCOY          NOT = CHDRMJA-CHDRCOY*/
		/*OR LIFEMJA-CHDRNUM          NOT = CHDRMJA-CHDRNUM*/
		/*OR LIFEMJA-STATUZ           = ENDP*/
		/*   MOVE 'Y'                 TO WSAA-LIFE-EXIT.*/
		/*IF LIFEMJA-JLIFE            NOT = ZERO                   <007*/
		/*   NEXT SENTENCE                                         <007*/
		/*ELSE                                                     <007*/
		/*   ADD 1                    TO WSAA-LIFE-NUM             <007*/
		/*   IF WSAA-LIFE-NUM         =  LIFEMJA-LIFE              <007*/
		/*   MOVE 'Y'                 TO WSAA-LIFE-FOUND.          <007*/
		/* Check for the existence of Joint Life details.
		lifemjaIO.setFunction(varcom.nextr);
	 
		getLifemja1900();
		if (isNE(lifemjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(lifemjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(), varcom.endp)) {
			wsaaLifeExit.set("Y");
		}
		if (isNE(lifemjaIO.getLife(), wsaaMiscellaneousInner.wsaaLifeNo)) {
			goTo(GotoLabel.listCoverages1150);
		}
		/* If a Joint life exists then add it to the subfile otherwise*/
		/* move the stored key of the main life to the LIFE I/O module.
		if (isNE(lifemjaIO.getJlife(), ZERO)) {
			jlifeToScreen1200();
		}
		/*ELSE*/
		/* MOVE WSAA-LIFEMJA-KEY    TO LIFEMJA-DATA-KEY.*/
		/*  ENSURE LIFEMJA CORRECTLY POSITIONED BY READING NEXT!!
		lifemjaIO.setFunction(varcom.nextr);
		getLifemja1900();
		*/
		
	   if(lifepfCount>=lifepfList.size() ) {
	    	wsaaLifeExit.set("Y");
	    	return;
	   }
	   lifepfIO=lifepfList.get(lifepfCount);
	   
	  /*  if (isNE(lifepfIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
	    			|| isNE(lifepfIO.getChdrnum(), chdrmjaIO.getChdrnum())
	    			||lifepfCount>lifepfList.size() ) {
	    				wsaaLifeExit.set("Y");
	    			}
	   */ 
		
		if (isNE(lifepfIO.getLife(), wsaaMiscellaneousInner.wsaaLifeNo)) {
			goTo(GotoLabel.listCoverages1150);
		}
		
		if (isNE(lifepfIO.getJlife(), ZERO)) {
			jlifeToScreen1200();
		}
	}

protected void listCoverages1150()
	{
		/* Add to the subfile all associated coverages and riders.
		covrmjaIO.setChdrcoy(wsspcomn.company);
	    covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		/* MOVE LIFEMJA-LIFE           TO COVRMJA-LIFE.
		covrmjaIO.setLife(wsaaMiscellaneousInner.wsaaLifeNo);
		covrmjaIO.setCoverage("00");
		covrmjaIO.setRider("00");
		/*   IF CHDRMJA-POLSUM           NOT > ZERO*/
		/*   AND CHDRMJA-POLINC          NOT = 1*/
		/*      MOVE '9999'              TO COVRMJA-PLAN-SUFFIX*/
		/*      MOVE CHDRMJA-POLINC      TO WSAA-PLAN-SUFFIX*/
		/*   ELSE*/
		/*      MOVE WSAA-PLAN-SUFFIX    TO COVRMJA-PLAN-SUFFIX.
		covrmjaIO.setPlanSuffix(0);
		covrmjaIO.setFunction(varcom.begn);
		/* List all the coverages for that Life.
		wsaaCoverExit.set("N");
		while ( !(wsaaCoverEof.isTrue())) {
			listCoverages1300();
		}
		if (appAdd.isTrue()
		|| revComp.isTrue()) {
		a1500CheckCovtCoverage();
		}
		lifemjaIO.setFunction(varcom.nextr);
		*/
		
	    /*ILIFE-3395 STARTS */   
//ILIFE-8075 start
	//	Covrpf covrData = new Covrpf();				
		covrpf.setChdrcoy(wsspcomn.company.toString());
		covrpf.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
		covrpf.setLife(wsaaMiscellaneousInner.wsaaLifeNo.toString());
	//	covrData.setCoverage("00");
	//	covrData.setRider("00");
	//   covrData.setPlanSuffix(0);
		wsaaCoverExit.set("N");
		covrpfList = covrpfDAO.selectCoverage(covrpf);//IJTI-1410
//ILIFE-8075 end
		for(Covrpf covr: covrpfList) {
			crtableMap.put(covr.getCrtable(), "T5687");
			if(covr.getPstatcode()!=null) {
				crtableMap.put(covr.getPstatcode(),"T5681");
			}
			if(covr.getStatcode()!=null)
			{
				crtableMap.put(covr.getStatcode(), "T5682");
			}
		}
		getAllItemsDescription(crtableMap);
		
		for(covrpfCount=0;covrpfCount<covrpfList.size();covrpfCount++)
		{
			covrpf=covrpfList.get(covrpfCount);
			listCoverages1300();
		}
		if (appAdd.isTrue()
					|| revComp.isTrue()) {
					a1500CheckCovtCoverage();
					}
		//lifemjaIO.setFunction(varcom.nextr);
		//lifepfCount++;
		}
	    
        /*ILIFE-3395 ENDS */
protected void getAllItemsDescription(Map<String,String> itemMap) {
	descMap= descDAO.searchMultiDescpfRecords(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);
}
protected void jlifeToScreen1200()
	{
		jlifeToSubfile1210();
		subfileAdd1220();
	}

protected void jlifeToSubfile1210()
	{
		/* If the LIFEMJA record just read was for Joint Life details*/
		/* on the current LIFE then display the details.*/
		sv.cmpntnum.set(" +");
		sv.component.set(lifepfIO.getLifcnum());
		clts.setClntnum(lifepfIO.getLifcnum());
		sv.hsuffix.set(ZERO);
		/* Protect the action field as a joint life has been added.*/
		sv.selectOut[varcom.pr.toInt()].set("Y");
		clts.setClntpfx("CN");
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts = clntpfDAO.getCltsRecordByKey(clts);
		if (clts == null) {
			syserrrec.params.set(wsspcomn.fsuco.toString().concat(lifepfIO.getLifcnum()));
			fatalError600();
		}
		plainname();
		sv.deit.set(wsspcomn.longconfname);
		//ILIFE-2345-2347 Start
		if(ispermission){
			if (isNE(clts.getCltdob(), lifepfIO.getCltdob())) {
				sv.cmpntnumErr.set(formatsInner.rfpk);
				wsspcomn.edterror.set("Y");
			}
			if (isNE(clts.getCltsex(), lifepfIO.getCltsex())) {
				sv.hlifenoErr.set(formatsInner.pfpl);
				wsspcomn.edterror.set("Y");
			}
		}
		//ILIFE-2345-2347 End
	}

protected void subfileAdd1220()
	{
		/* Add a record to the subfile.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void listCoverages1300(){
		
	    /*ILIFE-3395 STARTS */
	
		if(covrpfList.size() == 0)
		{
			//syserrrec.params.set(covrpf.getParams());
			fatalError600();
	
		}
		
		if (isNE(covrpf.getChdrcoy(), wsspcomn.company)
				|| isNE(covrpf.getChdrnum(), chdrpf.getChdrnum())
				|| isNE(covrpf.getLife(), wsaaMiscellaneousInner.wsaaLifeNo)) {
					wsaaCoverExit.set("Y");
					return ;
				}
		
		if (isEQ(wsaaMiscellaneousInner.wsaaPlanSuffix, 0)
				|| isLTE(wsaaMiscellaneousInner.wsaaPlanSuffix, chdrpf.getPolsum())) {
					if (isNE(covrpf.getPlanSuffix(), 0)) {
						//covrmjaIO.setFunction(varcom.nextr);
						covrpfCount++;
						return ;
					}
				}
		
		if (isNE(covrpf.getPlanSuffix(), wsaaMiscellaneousInner.wsaaPlanSuffix)) {
			covrpfCount++;
			return ;
		}
		
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.selectOut[varcom.nd.toInt()].set(SPACES);
		if (isEQ(covrpf.getRider(), ZERO)) {
			coverageToScreen1400();
			wsaaSaveLife.set(covrpf.getLife());
			wsaaSaveCoverage.set(covrpf.getCoverage());
			wsaaSavePlansufx.set(covrpf.getPlanSuffix());
			if (appAdd.isTrue()
			|| revComp.isTrue()) {
				a1400CheckCovtRider();
			}
		}
		else {
			riderToScreen1500();
		}
		
	}

protected void coverageToScreen1400()
	{
		covrmjaSubfile1410();
	}

protected void covrmjaSubfile1410()
	{
		/* Set up and write the current Coverage details.*/
		sv.cmpntnum.set(covrpf.getCoverage());
		wsaaMiscellaneousInner.wsaaCoverage.set(covrpf.getCrtable());
		sv.hcrtable.set(covrpf.getCrtable());
		sv.component.set(wsaaMiscellaneousInner.wsaaCovrComponent);
		getDescription1600(descMap,covrpf.getCrtable());
		covrStatusDescs1700(covrpf);
		validateComponent1600();
		/* Store the COVRMJA key for later use in screen Hidden Fields.*/
		sv.hlifeno.set(covrpf.getLife());
		sv.hsuffix.set(covrpf.getPlanSuffix());
		sv.hcoverage.set(covrpf.getCoverage());
		sv.hrider.set(covrpf.getRider());
		if (appAdd.isTrue()) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		selectRecordCustomerSpecific();
		
		scrnparams.function.set(varcom.sadd);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
	}

protected void a1400CheckCovtRider(){
	//ILIFE-6809
	List<Covtpf> covtridList = covtpfDAO.getCovtridData(wsspcomn.company.toString(),chdrpf.getChdrnum(),wsaaSaveLife.toString(),wsaaSaveCoverage.toString());//IJTI-1410
	for(Covtpf covt: covtridList)
	{
		crtableMap.put(covt.getCrtable(), "T5687");
	}
	getAllItemsDescription(crtableMap);
	for(Covtpf covtpf : covtridList ){
		a1440aCallCovt(covtpf);
	}
}

protected void a1440aCallCovt(Covtpf covtpf)
	{
		/* Check if existing in COVR - and if yes need not load*/
		/* again for Reverse - component add/mod proposal .*/
		if (revComp.isTrue()) {
			statusFlag = covrpfDAO.isExistCovrRecord(covtpf.getChdrcoy(), covtpf.getChdrnum(), covtpf.getLife(),covtpf.getCoverage(), covtpf.getRider());
			//ILIFE-7369 start
			if (statusFlag) {
				return;
			}
			//ILIFE-7369 end
		}
		sv.hlifeno.set(covtpf.getLife());
		sv.hsuffix.set(wsaaSavePlansufx);
		sv.hcoverage.set(covtpf.getCoverage());
		sv.hrider.set(covtpf.getRider());
		sv.cmpntnum.set(covtpf.getCoverage());
		wsaaMiscellaneousInner.wsaaRider.set(covtpf.getCrtable());
		wsaaMiscellaneousInner.wsaaCoverage.set(covtpf.getCrtable());
		sv.hcrtable.set(covtpf.getCrtable());
		sv.component.set(wsaaMiscellaneousInner.wsaaRidrComponent);
		getDescription1600(descMap,covtpf.getCrtable());
		scrnparams.function.set(varcom.sadd);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}


protected void a1500CheckCovtCoverage()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a1500Begin();
					a1540aCallCovt();
				case a1580Return:
				case a1590Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a1500Begin()
	{
	    Covtpf covtData = new Covtpf();		
		covtData.setChdrcoy(wsspcomn.company.toString());
		covtData.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
		covtData.setLife(wsaaMiscellaneousInner.wsaaLifeNo.toString());
		covtpfList = covtpfDAO.getCovtCovDetails(covtData);
		
		if(isEQ(covtpfList.size(),0))
				{goTo(GotoLabel.a1590Exit);}
		
		for(Covtpf covt: covtpfList)
		{
			crtableMap.put(covt.getCrtable(), "T5687");
		}
		getAllItemsDescription(crtableMap);
		
		
		for(covtpfCount=0;covtpfCount<covtpfList.size();covtpfCount++)
		{
			covtpfIO=covtpfList.get(covtpfCount);
			a1540aCallCovt();
			
		}
		
		/*ILIFE-3395 ends */
		
		
	}

protected void a1540aCallCovt()
	{
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.selectOut[varcom.nd.toInt()].set(SPACES);
		if(isNE(covtpfIO.getChdrcoy(), wsspcomn.company)
				|| isNE(covtpfIO.getChdrnum(), chdrpf.getChdrnum())
				|| isNE(covtpfIO.getLife(), wsaaMiscellaneousInner.wsaaLifeNo)
		        || isLTE(covtpfList.size(),covtpfCount))
		        {
					covtpfCount = covtpfList.size();
					goTo(GotoLabel.a1590Exit);
				}
		
		if (revComp.isTrue()) {
			b1500CheckCovr();
			if (statusFlag) {
				goTo(GotoLabel.a1580Return);
			}
		}
		if (appAdd.isTrue()) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		sv.hlifeno.set(covtpfIO.getLife());
		sv.hsuffix.set(ZERO);
		sv.hcoverage.set(covtpfIO.getCoverage());
		sv.hrider.set(covtpfIO.getRider());
		sv.cmpntnum.set(covtpfIO.getCoverage());
		wsaaMiscellaneousInner.wsaaCoverage.set(covtpfIO.getCrtable());
		sv.hcrtable.set(covtpfIO.getCrtable());
		sv.component.set(wsaaMiscellaneousInner.wsaaCovrComponent);
		getDescription1600(descMap,covtpfIO.getCrtable());
		scrnparams.function.set(varcom.sadd);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		wsaaSaveLife.set(covtpfIO.getLife());
		wsaaSaveCoverage.set(covtpfIO.getCoverage());
		wsaaSavePlansufx.set(ZERO);
		a1400CheckCovtRider();
		
		/*ILIFE-3395 ENDS */
	
		
	}

protected void b1500CheckCovr()
	{
		statusFlag = covrpfDAO.isExistCovrRecord(covtpfIO.getChdrcoy(), covtpfIO.getChdrnum(), covtpfIO.getLife(), covtpfIO.getCoverage(), covtpfIO.getRider());
	}

protected void riderToScreen1500()
	{
		/* Set up and write the current Rider details.*/
		sv.cmpntnum.set(covrpf.getRider());
		wsaaMiscellaneousInner.wsaaRider.set(covrpf.getCrtable());
		wsaaMiscellaneousInner.wsaaCoverage.set(covrpf.getCrtable());
		sv.hcrtable.set(covrpf.getCrtable());
		sv.component.set(wsaaMiscellaneousInner.wsaaRidrComponent);
		getDescription1600(descMap,covrpf.getCrtable());
		covrStatusDescs1700(covrpf);
		validateComponent1600();
		/* Store the COVRMJA key for later use in screen Hidden Fields.*/
		sv.hlifeno.set(covrpf.getLife());
		sv.hsuffix.set(covrpf.getPlanSuffix());
		sv.hcoverage.set(covrpf.getCoverage());
		sv.hrider.set(covrpf.getRider());
		if (appAdd.isTrue()) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		selectRecordCustomerSpecific();
		scrnparams.function.set(varcom.sadd);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
	}

protected void validateComponent1600()
	{
		validateComponentPara1600();
	}

protected void validateComponentPara1600()
	{
		/* Read the valid statii from table T5679.*/
	    itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
        itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5679").concat(wsaaBatckey.batcBatctrcde.toString()));
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			headerStatuzCheck1800();
		}
		if (isNE(wsaaValidStatus, "Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void getDescription1600(Map<String, Descpf> descriptionMap,String item){
	/* Read the contract definition description from table T5687 for*/
	/* the contract held on the client header record.*/
	//ILIFE-6809
	/*descpf=descDAO.getdescData("IT", "T5687", wsaaMiscellaneousInner.wsaaCoverage.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	if (descpf == null) {
		sv.deit.fill("?");
	}
	else {
		sv.deit.set(descpf.getLongdesc());
	}*/
	
	if(descMap==null || descMap.isEmpty() || descMap.get(item)==null ){
		sv.deit.fill("?");
	}
	else {
		sv.deit.set(descMap.get(item).getLongdesc());
	}
}

	protected void covrStatusDescs1700(Covrpf covr) {
		/* Use the Risk Status and Premium Status codes to read T5681 and */
		/* T5682 and obtain the short descriptions. */
		// ILIFE-6809
		//descpf = descDAO.getdescData("IT", "T5681", covrpf.getPstatcode().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descMap == null || descMap.isEmpty() || descMap.get(covr.getPstatcode())==null) {
			sv.pstatcode.fill("?");
		} else {
			sv.pstatcode.set(descMap.get(covr.getPstatcode()).getShortdesc());
		}
		//descpf = descDAO.getdescData("IT", "T5682", covrpf.getStatcode().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descMap == null || descMap.isEmpty() || descMap.get(covr.getStatcode())==null) {
			sv.statcode.fill("?");
		} else {
			sv.statcode.set(descMap.get(covr.getStatcode()).getShortdesc());
		}
	}

	/**
	* <pre>
	*     CHECK THE COMPONENT STATUS AGAINST T5679.
	* </pre>
	*/
protected void headerStatuzCheck1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					headerStatuzCheck1810();
				case covPrem1850:
					covPrem1850();
				case ridPrem1860:
					ridPrem1860();
				case valid1880:
					valid1880();
				case exit1890:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void headerStatuzCheck1810()
	{
		/*    IF COVRMJA-RIDER = ZERO*/
		/*     IF T5679-COV-RISK-STAT(WSAA-INDEX) NOT = SPACE*/
		/*       IF (T5679-COV-RISK-STAT(WSAA-INDEX) = COVRMJA-STATCODE)*/
		/*       AND (T5679-COV-PREM-STAT(WSAA-INDEX) = COVRMJA-PSTATCODE)*/
		/*            MOVE 'Y'            TO WSAA-VALID-STATUS*/
		/*            GO TO 1890-EXIT.*/
		/*    IF COVRMJA-RIDER > ZERO*/
		/*     IF T5679-RID-RISK-STAT(WSAA-INDEX) NOT = SPACE*/
		/*       IF (T5679-RID-RISK-STAT(WSAA-INDEX) = COVRMJA-STATCODE)*/
		/*       AND (T5679-RID-PREM-STAT(WSAA-INDEX) = COVRMJA-PSTATCODE)*/
		/*            MOVE 'Y'            TO WSAA-VALID-STATUS*/
		/*            GO TO 1890-EXIT.*/
		wsaaPrsub.set(ZERO);
		if (isEQ(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
					goTo(GotoLabel.covPrem1850);
				}
			}
		}
		if (isGT(covrpf.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode()))) {
					goTo(GotoLabel.ridPrem1860);
				}
			}
		}
		goTo(GotoLabel.exit1890);
	}

protected void covPrem1850()
	{
		wsaaPrsub.add(1);
		if (isEQ(t5679rec.covPremStat[wsaaPrsub.toInt()], covrpf.getPstatcode())) {
			goTo(GotoLabel.valid1880);
		}
		if (isLT(wsaaPrsub, 12)) {
			goTo(GotoLabel.covPrem1850);
		}
		else {
			goTo(GotoLabel.exit1890);
		}
	}

protected void ridPrem1860()
	{
		wsaaPrsub.add(1);
		if (isEQ(t5679rec.ridPremStat[wsaaPrsub.toInt()], covrpf.getPstatcode())) {
			goTo(GotoLabel.valid1880);
		}
		if (isLT(wsaaPrsub, 12)) {
			goTo(GotoLabel.ridPrem1860);
		}
		else {
			goTo(GotoLabel.exit1890);
		}
	}

protected void valid1880()
	{
		wsaaValidStatus = "Y";
	}


/*ILIFE-3395 STARTS */

/*
protected void getLifemja1900()
	{
		/*START
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		
	}
	/*ILIFE-3395 ENDS */

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		ispermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "MTL26601", appVars,smtpfxcpy.item.toString());//TSD-266
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		scrnparams.subfileRrn.set(1);
		//TSD-266-Starts
		if(ispermission){
			if (isNE(sv.lifenum,SPACES)) {
				if(isNE(wsaaLDob,wsaaCDob)) {
					sv.lifenumErr.set(formatsInner.rfpk);
					wsspcomn.edterror.set("Y");
				}
				else {
//					if(ispermission){
						if(isNE(wsaaLSex,wsaaCSex)){
							sv.lifenumErr.set(formatsInner.pfpl);
							wsspcomn.edterror.set("Y");
						}
//					}
				}
			}
	
			if(isNE(sv.jlife,SPACES)) {
				if(isNE(wsaaJlDob,wsaaJcDob)){
					sv.jlifenumErr.set(formatsInner.rfpk);
					wsspcomn.edterror.set("Y");
				}
				else {
//					if(ispermission){
						if(isNE(wsaaJlSex,wsaaJcSex)) {
							sv.jlifenumErr.set(formatsInner.pfpl);
							wsspcomn.edterror.set("Y");
						}
//					}
				}
			}
		}
		//TSD-266 Ends
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*2010-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'SR569IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         SR569-DATA-AREA                         */
		/*                         SR569-SUBFILE-AREA.                     */
		/* If user has abandoned the transaction, then prevent **/
		/* program P5133 from continuing to  Fund Redirection. **/
		if (isEQ(scrnparams.statuz, "SUBM ")) {
			wsspcomn.flag.set("K");
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If termination of processing then go to exit. P6350 will*/
		/*  release the soft lock.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
		
		//fluprevIOList = fluppfDAO.searchFlupRecordByChdrNum(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString());  commented for ILIFE-9214
		fluprevIOList = fluppfDAO.searchFlupRecordByChdrNum(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());   //modified for ILIFE-9214
		if (CSMIN003Permission){
            //validate follow up status if R/W then continue
			if (isEQ(wsaaBatckey.batcBatctrcde, t555)
					||isEQ(wsaaBatckey.batcBatctrcde, t557)) {
				chekIfRequired3410();
				readDefaultsTable3420();
				validateFollowUpStatus2a00();
			}
		}
		if(NBPRP056Permission) {
			if (isEQ(wsaaBatckey.batcBatctrcde, t555)
					||isEQ(wsaaBatckey.batcBatctrcde, t557)) {
				validateFollowUpStatus2a10();
			}
		}
		
		if(lincFlag && (appAdd.isTrue() || appMod.isTrue())) {
			validateFollowUpStatus2a10();
			performLincValidations();
			if(isEQ(wsspcomn.edterror, "Y"))
				goTo(GotoLabel.exit2090);
		}
		
		scrnparams.subfileRrn.set(1);
	}

	private void validateFollowUpStatus2a10() {
	
		boolean showOutstandingErr = true;
		List<String> flupList = new ArrayList<>();
		if(fluprevIOList!=null&&!fluprevIOList.isEmpty()){
			for (Fluppf fluppf: fluprevIOList) {
				if(lincFlag)
					showOutstandingErr = true;
				readT5661(fluppf.getFupCde());
				flupList.add(fluppf.getFupCde());
				for (wsaaX.set(1); !(isGT(wsaaX, 10))
						&& isNE(t5661rec.fuposs[wsaaX.toInt()], SPACE); wsaaX.add(1)){
					if (isEQ(fluppf.getFupSts(), t5661rec.fuposs[wsaaX.toInt()])) {
						showOutstandingErr = false;
					}
				}
			}
			if(flupList.isEmpty()){
				return;
			}
			if(showOutstandingErr){
				scrnparams.errorCode.set(h017);
				wsspcomn.edterror.set("Y");
				if(!lincFlag)
					goTo(GotoLabel.exit2090);
			}
		}
	}

	protected void validateFollowUpStatus2a00() {
		//In T5688, default is none, then break
		if (isEQ(t5688rec.defFupMeth, SPACES)) {
			return;
		}
		if (isEQ(t5677rec.fupcdess, SPACES)) {
			return;
		}
		boolean showOutstandingErr = false;
		List<String> flupList = new ArrayList<>();
		if(fluprevIOList!=null&&!fluprevIOList.isEmpty()){
			for (Fluppf fluppf: fluprevIOList) {
				for (FixedLengthStringData fixedLengthStringData : t5677rec.fupcdes) {
					if(fixedLengthStringData !=null && !fixedLengthStringData.trim().equals("")) {
						if (isEQ(fluppf.getFupCde(),fixedLengthStringData.toString().trim())) {
							flupList.add(fluppf.getFupCde());
							if(isEQ(fluppf.getFupSts(),"R") || isEQ(fluppf.getFupSts(),"W")){
								showOutstandingErr = false;
							}else{
								showOutstandingErr = true;
								break;
							}
						}
					}
				}
				if(showOutstandingErr){
				    break;
                }
			}
			if(flupList.isEmpty()){
				return;
			}
			if(showOutstandingErr){
				scrnparams.errorCode.set(h017);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
	}

	protected void chekIfRequired3410()
    {
        itempf = new Itempf();
        itempf.setItempfx("IT");
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tablesInner.t5688.toString());
        itempf.setItemitem(chdrpf.getCnttype().trim());//IJTI-1410
        itempf = itemDAO.getItemRecordByItemkey(itempf);
		if(itempf != null){
			t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
    }

    protected void readDefaultsTable3420()
    {
        if (isEQ(t5688rec.defFupMeth, SPACES)) {
            return;
        }
        wsaaT5677Tranno.set(wsaaBatckey.batcBatctrcde);
        wsaaT5677FollowUp.set(t5688rec.defFupMeth);
        itempf = new Itempf();
        itempf.setItempfx("IT");
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tablesInner.t5677.toString());
        itempf.setItemitem(wsaaT5677Key.toString().trim());
        itempf = itemDAO.getItemRecordByItemkey(itempf);
        if(itempf != null){
			t5677rec.t5677Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
    }
    
    protected void readT5661(String fupCode)
    {
    	wsaaT5661Lang.set(wsspcomn.language);
    	wsaaT5661Fupcode.set(fupCode);
    	itempf = new Itempf();
    	itempf.setItempfx("IT");
    	itempf.setItemcoy(wsspcomn.company.toString());
    	itempf.setItemtabl(t5661);
    	itempf.setItemitem(wsaaT5661Key.toString());
    	itempf = itemDAO.getItempfRecord(itempf);

    	if (null == itempf) {
    		return;
    	}
    	t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
    }

protected void validateSubfile2030()
	{
		/* Validate the selections within the subfile.*/
		/* PERFORM 2600-VALIDATE-SUBFILE*/
		/*   UNTIL SCRN-STATUZ         = ENDP.*/
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}

		/* If no selections.*/
		if (!revComp.isTrue()) {
			if (isEQ(wsaaMiscellaneousInner.wsaaSelected, 0)) {
				scrnparams.errorCode.set("G931");
				wsspcomn.edterror.set("Y");
			}
		}
		if (isNE(sv.huwdcdte, varcom.vrcmMaxDate)) {
			if (isEQ(sv.huwdcdte, 0)) {
				sv.huwdcdteErr.set("E032");
				wsspcomn.edterror.set("Y");
			}
			else {
				if (isGT(sv.huwdcdte, wsaaToday)) {
					sv.huwdcdteErr.set("HL01");
					wsspcomn.edterror.set("Y");
				}
			}
			//ILIFE-8486---Start
		}else{
			if(!enqComp.isTrue()) {
				scrnparams.errorCode.set(f665);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
			//ILIFE-8486---End
	}

	/**
	* <pre>
	*2600-VALIDATE-SUBFILE SECTION.
	*2610-READ-NEXT-MODIFIED-RECORD.
	* sequentially read down the subfile to find a selected record.
	*    MOVE SRNCH                  TO SCRN-FUNCTION.
	*    CALL 'SR569IO'           USING SCRN-SCREEN-PARAMS
	*                                   SR569-DATA-AREA
	*                                   SR569-SUBFILE-AREA.
	*    IF SCRN-STATUZ              NOT = O-K
	*                            AND ENDP
	*       MOVE SCRN-STATUZ         TO SYSR-STATUZ
	*       PERFORM 600-FATAL-ERROR.
	*2620-VALIDATION.
	*    IF SR569-ERROR-SUBFILE      NOT = SPACES
	*       MOVE 'Y'                 TO WSSP-EDTERROR
	*    ELSE
	*       MOVE O-K                 TO WSSP-EDTERROR
	*       GO TO 2690-EXIT.
	*2630-UPDATE-SCREEN.
	*    MOVE SUPD                   TO SCRN-FUNCTION.
	*    CALL 'SR569IO' USING SCRN-SCREEN-PARAMS
	*                         SR569-DATA-AREA
	*                         SR569-SUBFILE-AREA.
	*    IF SCRN-STATUZ              NOT = O-K
	*       MOVE SCRN-STATUZ         TO SYSR-STATUZ
	*       PERFORM 600-FATAL-ERROR.
	*    IF SR569-ERROR-SUBFILE      NOT = SPACES
	*       MOVE 'Y'                 TO WSSP-EDTERROR.
	*2690-EXIT.
	*     EXIT.
	* </pre>
	*/
protected void validateSubfile2600()
	{
		readNextRecord2610();
		updateErrorIndicators2630();
	}

protected void readNextRecord2610()
	{
		/* Read down the subfile for records selected.*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*VALIDATION*/
		/* If record selected increment the selection counter.*/
		if (isNE(sv.select, SPACES)) {
			wsaaMiscellaneousInner.wsaaSelected.add(1);
		}
	}

protected void updateErrorIndicators2630()
	{
		/* Redisplay the screen if any errors exist.*/
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/* Update database files as required*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*  GO TO 3090-EXIT.*/
		if (!appAdd.isTrue()
		|| !appMod.isTrue()) {
			return ;
		}
		incrmjaIO.setDataArea(SPACES);
		incrmjaIO.setChdrcoy(wsspcomn.company);
		incrmjaIO.setChdrnum(chdrpf.getChdrnum());
		incrmjaIO.setLife(ZERO);
		incrmjaIO.setRider(ZERO);
		incrmjaIO.setPlanSuffix(ZERO);
		incrmjaIO.setFunction(varcom.begn);
		incrmjaIO.setFormat(formatsInner.incrmjarec);
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)
		&& isNE(incrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(incrmjaIO.getChdrcoy(), wsspcomn.company)
		|| isNE(incrmjaIO.getChdrnum(), chdrpf.getChdrnum())
		|| isEQ(incrmjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(wsaaFirstTime, "Y")) {
			sv.chdrnumErr.set("J008");
			wsspcomn.edterror.set("Y");
			wsaaFirstTime = "N";
			return ;
		}
		incrmjaIO.setDataArea(SPACES);
		incrmjaIO.setChdrcoy(wsspcomn.company);
		incrmjaIO.setChdrnum(chdrpf.getChdrnum());
		incrmjaIO.setLife(ZERO);
		incrmjaIO.setRider(ZERO);
		incrmjaIO.setPlanSuffix(ZERO);
		incrmjaIO.setFunction(varcom.begnh);
		incrmjaIO.setFormat(formatsInner.incrmjarec);
		while ( !(endOfFile.isTrue())) {
			updateIncr3100();
		}

		covrincIO.setParams(SPACES);
		covrincIO.setChdrcoy(wsspcomn.company);
		covrincIO.setChdrnum(chdrpf.getChdrnum());
		covrincIO.setLife(ZERO);
		covrincIO.setJlife(ZERO);
		covrincIO.setCoverage(ZERO);
		covrincIO.setRider(ZERO);
		covrincIO.setPlanSuffix(ZERO);
		covrincIO.setFunction(varcom.begnh);
		while ( !(isEQ(covrincIO.getStatuz(), varcom.endp))) {
			updateCovr3200();
		}

	}

protected void updateIncr3100()
	{
		nextIncr3120();
	}

protected void nextIncr3120()
	{
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)
		&& isNE(incrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(incrmjaIO.getStatuz(), varcom.endp)) {
			wsaaEof.set("Y");
			return ;
		}
		if (isNE(incrmjaIO.getChdrcoy(), wsspcomn.company)
		|| isNE(incrmjaIO.getChdrnum(), chdrpf.getChdrnum())) {
			wsaaEof.set("Y");
			/*       RELEASE HELD RECORD*/
			incrmjaIO.setFunction(varcom.rewrt);
		}
		else {
			incrmjaIO.setFunction(varcom.delet);
		}
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incrmjaIO.getParams());
			fatalError600();
		}
		incrmjaIO.setFunction(varcom.nextr);
	}

protected void updateCovr3200()
	{
		nextCovr3220();
	}

protected void nextCovr3220()
	{
		SmartFileCode.execute(appVars, covrincIO);
		if (isNE(covrincIO.getStatuz(), varcom.oK)
		&& isNE(covrincIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrincIO.getParams());
			syserrrec.statuz.set(covrincIO.getParams());
			fatalError600();
		}
		if (isNE(covrincIO.getChdrcoy(), wsspcomn.company)
		|| isNE(covrincIO.getChdrnum(), chdrpf.getChdrnum())
		|| isEQ(covrincIO.getStatuz(), varcom.endp)) {
			covrincIO.setStatuz(varcom.endp);
			return ;
		}
		covrincIO.setIndexationInd(SPACES);
		covrincIO.setFunction(varcom.rewrt);
		covrincIO.setFormat(formatsInner.covrincrec);
		SmartFileCode.execute(appVars, covrincIO);
		if (isNE(covrincIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrincIO.getParams());
			syserrrec.statuz.set(covrincIO.getStatuz());
			fatalError600();
		}
		covrincIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4010();
				case continue4080:
					continue4080();
				case updateScreen4085:
					updateScreen4085();
				case exit4090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		/* If selection terminated clear program and action stacks.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			releaseSftlck4400();
			wsaaMiscellaneousInner.wsaaSub.set(1);
			wsspcomn.secActn[wsaaMiscellaneousInner.wsaaSub.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/* If not returning from a selection ACTION = ' ' then we must*/
		/*   store the next four Programs in the stack and load in the*/
		/*   generic programs according to Table T5671.*/
		/* If returning from a selection ACTION = '*' then sequentially*/
		/*   read down the subfile to find the next selected record.*/
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			initSelection4100();
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		/* Read the Subfile until a Selection has been made.*/
		wsaaSelectFlag.set("N");
		wsaaImmexitFlag.set("N");
		while ( !(wsaaSelection.isTrue()
		|| wsaaImmExit.isTrue())) {
			next4200();
		}

		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			if (wsaaImmExit.isTrue()) {
				/*       IF ADD-COMP OR MOD-COMP OR APP-ADD OR APP-MOD*/
				/*       OR REV-COMP*/
				/*          PERFORM 4800-UPDATE-HISTORY*/
				/*       END-IF*/
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
				wsspcomn.nextprog.set(scrnparams.scrname);
				goTo(GotoLabel.exit4090);
			}
		}
		/* If the end of the subfile, exit to next initial program.*/
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		if (wsaaImmExit.isTrue()) {
			if (addComp.isTrue()
			|| modComp.isTrue()
			|| revComp.isTrue()) {
				/*       PERFORM 4800-UPDATE-HISTORY                            */
				/*       PERFORM 8000-DRY-PROCESSING                    <LA3995>*/
				/*       PERFORM 4400-RELEASE-SFTLCK                            */
				checkChanges9000();
				if (isEQ(wsaaChange, "Y")) {
					updateHistory4800();
					dryProcessing8000();
				}
				releaseSftlck4400();
				if (addComp.isTrue()
				|| modComp.isTrue()) {
					if (isEQ(wsaaChange, "Y")) {
						updateUwdate5200();
					}
				}
				if(lincFlag)
					performLincProcessing();
			}
			if (appAdd.isTrue()
			|| appMod.isTrue()) {
				uwdateMandatory5300();
				if (isEQ(sv.huwdcdte, varcom.vrcmMaxDate)
				|| isEQ(sv.huwdcdte, ZERO)
				&& isEQ(th506rec.mandatory02, "Y")) {
					sv.huwdcdteErr.set("HL05");
					wsspcomn.edterror.set("Y");
					wsspcomn.nextprog.set(scrnparams.scrname);
					planReload4500();
					wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
					goTo(GotoLabel.updateScreen4085);
				}
			}
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(1);
			wsspcomn.nextprog.set(wsaaProg);
			goTo(GotoLabel.exit4090);
		}
		/* The following check only applies if it is a Lapse transaction.*/
		if (isNE(wsaaBatckey.batcBatctrcde, wsaaLapse)
		&& isNE(wsaaBatckey.batcBatctrcde, wsaaPup)) {
			goTo(GotoLabel.continue4080);
		}
		/*  Check if the whole cover has been lapsed. If it is,*/
		/*  there is no need to lapse individual rider again.*/
		if (isEQ(sv.hlifeno, wsaaLastLifeno)
		&& isEQ(sv.hsuffix, wsaaLastSuffix)
		&& isEQ(sv.hcoverage, wsaaLastCoverage)
		&& isEQ(wsaaLastRider, ZERO)) {
			sv.asterisk.set(SPACES);
			goTo(GotoLabel.updateScreen4085);
		}
	}

protected void continue4080()
	{
		/* Read  Program  table to  find  Cover / Rider programs to be*/
		/* executed.*/
		coverRiderStore4300();
		/* Read the  Program  table T5671 for  the components programs*/
		/* to be executed next.*/
		programTables4400();
		/* Remember what the current coverage/rider is.*/
		wsaaLastLifeno.set(sv.hlifeno);
		wsaaLastSuffix.set(sv.hsuffix);
		wsaaLastCoverage.set(sv.hcoverage);
		wsaaLastRider.set(sv.hrider);
	}

protected void updateScreen4085()
	{
		screenUpdate4600();
	}

protected void initSelection4100()
	{
		try {
			saveNextProgs4110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void saveNextProgs4110()
	{
		/*    IF WSAA-IMM-EXIT*/
		/*       GO TO 4190-EXIT.*/
		/* Save the  next  four  programs  after  PR569  into  Working*/
		/* Storage for re-instatement at the end of the Subfile.*/
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
			loop14120();
		}
		/* First Read of the Subfile for a Selection.*/
		scrnparams.function.set(varcom.sstrt);
		/* Go to the first  record  in  the Subfile to  find the First*/
		/* Selection.*/
		goTo(GotoLabel.exit4190);
	}

protected void loop14120()
	{
		/* This loop will load the next four  programs from WSSP Stack*/
		/* into a Working Storage Save area.*/
		wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void next4200()
	{
		try {
			nextRec4210();
			ifSubfileEndp4220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void nextRec4210()
	{
		/* Read next subfile record sequentially.*/
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4220()
	{
		/* Check for the end of the Subfile.*/
		/* If end of Subfile re-load the Saved four programs to Return*/
		/*   to initial stack order.*/
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			reloadProgsWssp4230();
			goTo(GotoLabel.exit4290);
		}
		/* Exit if a selection has been found.*/
		if (isNE(sv.select, SPACES)) {
			sv.asterisk.set("*");
			sv.select.set(SPACES);
			wsaaSelectFlag.set("Y");
		}
		/*   MOVE 'Y'                 TO WSAA-SELECT-FLAG*/
		/*   Following code commented out as later validation may*/
		/*   result in an error being detected.*/
		/*   MOVE SUPD                TO SCRN-FUNCTION*/
		/*   CALL 'SR569IO' USING SCRN-SCREEN-PARAMS*/
		/*                            SR569-DATA-AREA*/
		/*                            SR569-SUBFILE-AREA*/
		/*   IF SCRN-STATUZ           NOT = O-K*/
		/*                        AND NOT = ENDP*/
		/*      MOVE SCRN-STATUZ      TO SYSR-STATUZ*/
		/*      PERFORM 600-FATAL-ERROR.*/
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.exit4290);
	}

protected void reloadProgsWssp4230()
	{
		/* Set Flag for immediate exit as the next action is to go to the*/
		/* next program in the stack.*/
		/* Set Subscripts and call Loop.*/
		wsaaImmexitFlag.set("Y");
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loop34240();
		}
		goTo(GotoLabel.exit4290);
	}

protected void loop34240()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void coverRiderStore4300()
	{
		coverRiderRead4300();
	}

protected void coverRiderRead4300()
	{
		/* First perform a READR on the selected Coverage/Rider record.*/
		/* Use the Hidden fields against each subfile record as the Key.*/
		//covrmjaIO.setDataArea(SPACES);
		covrpf.setChdrcoy(wsspcomn.company.toString());
		covrpf.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
		covrpf.setLife(sv.hlifeno.toString());
		covrpf.setPlanSuffix(sv.hsuffix.toInt());
		covrpf.setCoverage(sv.hcoverage.toString());
		covrpf.setRider(sv.hrider.toString());
		wsaaMiscellaneousInner.wsaaCrtable.set(sv.hcrtable);
		Covrpf covrpfIO = covrpfDAO.getcovrincrRecord(wsspcomn.company.toString(),chdrpf.getChdrnum(),sv.hlifeno.toString(),
				sv.hcoverage.toString(),sv.hrider.toString(),sv.hsuffix.toInt());
		
		if ((covrpfIO == null)
				&& (appAdd.isTrue()
				|| appMod.isTrue()
				|| modComp.isTrue()
				|| revComp.isTrue())) {
					//covrpfIO.setRecNonKeyData(SPACES);
					covrpf.setChdrcoy(wsspcomn.company.toString());
					covrpf.setChdrnum(chdrpf.getChdrnum());
					covrpf.setLife(sv.hlifeno.toString());
					covrpf.setPlanSuffix(sv.hsuffix.toInt());
					covrpf.setCoverage(sv.hcoverage.toString());
					covrpf.setRider(sv.hrider.toString());
					wsaaMiscellaneousInner.wsaaCrtable.set(sv.hcrtable);
					covrpf.setCrtable(sv.hcrtable.toString());
					covrpf.setPayrseqno(1);
					covrpf.setCrrcd(chdrpf.getCcdate()); /*ILIFE-3839*/
					covrpf.setPlanSuffix(sv.planSuffix.toInt());
					covrpfDAO.setCacheObject(covrpf);
				}
		else {
			covrpf.setPlanSuffix(sv.planSuffix.toInt());
			covrpfDAO.setCacheObject(covrpfIO);
		}
		
	}

protected void releaseSftlck4400()
	{
		unlockContract4410();
	}

protected void unlockContract4410()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void programTables4400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case readProgramTable4410:
					readProgramTable4410();
				case loadProgsToWssp4420:
					loadProgsToWssp4420();
				case exit4490:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readProgramTable4410()
	{
		/* Read the  Program  table T5671 for  the components programs to*/
		/* be executed next.*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5671");
		itempf.setItemitem((wsaaBatckey.batcBatctrcde.toString()).concat(wsaaMiscellaneousInner.wsaaCrtable.toString()));
		itempf = itemDAO.getItemRecordByItemkey(itempf);
	
		if (itempf == null) {
			if (isEQ(wsaaMiscellaneousInner.wsaaCrtable, "****")) {
				sv.selectErr.set("G433");
				wsspcomn.edterror.set("Y");
				wsspcomn.nextprog.set(scrnparams.scrname);
				planReload4500();
				wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
				goTo(GotoLabel.exit4490);
			}
			else {
				wsaaMiscellaneousInner.wsaaCrtable.set("****");
				goTo(GotoLabel.readProgramTable4410);
			}
		}
		else {
			t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(chdrpf.getBillfreq(), "00")
		|| appAdd.isTrue()
		|| appMod.isTrue()) {
			goTo(GotoLabel.loadProgsToWssp4420);
		}
		//ILIFE-6809
		List<Racdpf> racdpfs = racdpfDAO.searchRacdmjaResult(chdrpf.getChdrcoy().toString().trim(), chdrpf.getChdrnum().trim(), //IJTI-1410
				sv.hlifeno.toString().trim(), sv.hcoverage.toString().trim(), sv.hrider.toString().trim(), 
				sv.hsuffix.toString().trim());
		wsaaInvalidCtdate.set("N");
		for (Racdpf racdpf : racdpfs) {
			if (isLT(PackedDecimalData.parseLong(racdpf.getCtdate()),chdrpf.getPtdate())) {
				wsaaInvalidCtdate.set("Y");
				break;
			}
		}
		if (invalidCtdate.isTrue()) {
			sv.selectErr.set("R078");
			wsspcomn.edterror.set("Y");
			wsspcomn.nextprog.set(scrnparams.scrname);
			planReload4500();
			wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
			goTo(GotoLabel.exit4490);
		}
	}

protected void loadProgsToWssp4420()
	{
		/* Move the component programs to the WSSP stack.*/
		wsaaMiscellaneousInner.wsaaSub1.set(1);
		compute(wsaaMiscellaneousInner.wsaaSub2, 0).set(add(1, wsspcomn.programPtr));
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
			loop24430();
		}
		/* Reset the Action to '*' signifying action desired to the next*/
		/* program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		wsspcomn.nextprog.set(wsaaProg);
		goTo(GotoLabel.exit4490);
	}

protected void loop24430()
	{
		/* This loop will load four programs from table T5671 to the WSSP*/
		/* stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(t5671rec.pgm[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void planReload4500()
	{
		try {
			reloadProgsWssp4510();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void reloadProgsWssp4510()
	{
		/* Set Flag for immediate exit as the next action is to go to the*/
		/* next program in the stack.*/
		/* Set Subscripts and call Loop.*/
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
			loop34520();
		}
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		goTo(GotoLabel.exit4590);
	}

protected void loop34520()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void screenUpdate4600()
	{
		/*PARA*/
		/*2630-UPDATE-SCREEN.*/
		scrnparams.function.set(varcom.supd);
		processScreen("SR569", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateHistory4800()
	{
		try {
			readPtrnrev4810();
			writePtrn4820();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readPtrnrev4810()
	{
		/* READ PTRNREV LAST TRANSACTION CODE*/
		wsaaTranno.set(chdrpf.getTranno());
		compute(wsaaNewTranno, 0).set(add(1, chdrpf.getTranno()));
		ptrnrevIO.setDataArea(SPACES);
		ptrnrevIO.setChdrcoy(chdrpf.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrpf.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(ptrnrevIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(ptrnrevIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isEQ(ptrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (revComp.isTrue()) {
			ptrnrevIO.setFunction(varcom.readh);
		}
		else {
			ptrnrevIO.setFunction(varcom.readr);
		}
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (revComp.isTrue()) {
			b4900ReversePtrn();
			deleteCovt4900();
			a4900ReverseChdr();
			deleteCvuw5100();
			deleteCprp5400();
			/*goTo(GotoLabel.exit4890);*/
		}
		if (isEQ(wsaaBatckey.batcBatctrcde, ptrnrevIO.getBatctrcde())) {
			goTo(GotoLabel.exit4890);
		}
	}

protected void writePtrn4820()
	{
		/* Write a PTRN record.*/
	
		ptrnpf = new Ptrnpf();
		ptrnpf.setChdrpfx("CH");
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());	
		ptrnpf.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
		ptrnpf.setValidflag("1");
		ptrnpf.setTranno(wsaaNewTranno.toInt());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());	
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setCrtuser(" ");
		ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		/*ilife-6890 */
		wsaaBatckey1.set(wsspcomn.batchkey);
		ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey1.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey1.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
		
		
		List<Ptrnpf> list = new ArrayList<Ptrnpf>();
		list.add(ptrnpf);
		ptrnpfDAO.insertPtrnPF(list);
		
		updateContractStatus5000();
	}

protected void deleteCovt4900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					deleteCovtmja4900();
				case readCovt4910:
					readCovt4910();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void deleteCovtmja4900()
	{
		covtmjaIO.setStatuz(varcom.oK);
		covtmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		covtmjaIO.setChdrcoy(wsspcomn.company);
		covtmjaIO.setChdrnum(chdrpf.getChdrnum());
		covtmjaIO.setLife("00");
		covtmjaIO.setPlanSuffix(0);
		covtmjaIO.setCoverage("00");
		covtmjaIO.setRider("00");
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		covtmjaIO.setFunction(varcom.begnh);
	}

protected void readCovt4910()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.company, covtmjaIO.getChdrcoy())
		&& isEQ(chdrpf.getChdrnum(), covtmjaIO.getChdrnum())) {
			/*NEXT_SENTENCE*/
		}
		else {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		covtmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		covtmjaIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readCovt4910);
	}

protected void a4900ReverseChdr()
	{
		a4910RlseCurrentChdr();
		a4930KeepsChdrmja();
	}

protected void a4910RlseCurrentChdr()
	{
	//ILB-456 starts
		/*chdrmjaIO.setStatuz(varcom.oK);
		wsaaChdrnum.set(SPACES);
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		wsaaChdrnum.set(SPACES);
		chdrpfDAO.deleteCacheObject(chdrpf);
		// DO A READH ON CHDRMJA RECORD BEFORE REWRT
		chdrpf = chdrpfDAO.getchdrRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (chdrpf == null) {
			fatalError600();
		}
		if (isNE(chdrpf.getValidflag(), 1)) {
			fatalError600();
		}
		wsaaChdrnum.set(chdrpf.getChdrnum());
		chdrpfDAO.deleteRcdByUnique(chdrpf);
		/*    Select the next record which will be the valid flag 2 record*/

		chdrpf.setChdrcoy(wsspcomn.company.charat(0));
		chdrpf.setChdrnum(wsaaChdrnum.toString());
		chdrpf = chdrpfDAO.getchdrRecordData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum()); //ILIFE-8079
		
		if (chdrpf == null) {
			fatalError600();
		}
		
		if (isNE(chdrpf.getChdrcoy(), wsspcomn.company)
		|| isNE(chdrpf.getChdrnum(), wsaaChdrnum)
		|| isNE(chdrpf.getValidflag(), 2)) {
			fatalError600();
		}
		chdrpf.setValidflag("1".charAt(0));
		chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpf.setTranno(wsaaTranno.toInt());
		setPrecision(chdrpf.getTranlused(), 0);
		chdrpf.setTranlused(sub(wsaaNewTranno, 1).toInt());

		chdrpfDAO.updateChdrUniqueno(chdrpf);
		
		//ILB-456 ends
	}

protected void a4930KeepsChdrmja()
	{
		/* DO A KEEPS ON CHDRMJA RECORD FOR THE NEXT PROGRAM*/
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		/*A4990-EXIT*/
	}

protected void b4900ReversePtrn()
	{
		/*B4900-REWRT-PTRN*/
		ptrnrevIO.setUserProfile(wsspcomn.userid); //PINNACLE-2931
		ptrnrevIO.setValidflag("2");
		ptrnrevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		/*B4900-EXIT*/
	}

protected void updateContractStatus5000()
	{
		readT56795010();
		updateCurrent5015();
		createNew5020();
		keepsChdrmja5030();
	}

	/**
	* <pre>
	* READ T5679 TO GET THE RISK STATUS USING WSKY-BATC-BATCTRCDE.
	* </pre>
	*/
protected void readT56795010()
	{
		/* GET THE CONTRACT RISK STATUS FROM T5679*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);

		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5679").concat(wsaaBatckey.batcBatctrcde.toString()));
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void updateCurrent5015()
	{
		/* UPDATE CHDRMJA RECORD VALID FLAG TO '2' AND CURRTO TO TODAY'S DT*/
		//ILB-456 starts

		chdrpfDAO.deleteCacheObject(chdrpf);//ILIFE-8079
		// DO A READH ON CHDRMJA RECORD BEFORE REWRT
//ILIFE-8075 start
	//	chdrpf = chdrpfDAO.getchdrRecordData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());//ILIFE-8079
		chdrpf = chdrpfDAO.getchdrRecordservunit(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
//ILIFE-8075 end	
	if (chdrpf == null) {
			fatalError600();
		}
		/* SET THE CHDRMJA RECORD VALID FLAG TO '2'*/
		chdrpf.setCurrto(wsaaToday.toInt());
		boolean flag = chdrpfDAO.updateChdrValidflag(chdrpf, "2");
		if (flag == false) {
			fatalError600();
		}	


		//ILB-456 ends
	}

protected void createNew5020()
	{
		/* CREATE NEW RECORD IN CHDRMJA WITH STATCODE FROM T5679*/
		chdrpf.setStatcode(t5679rec.setCnRiskStat.toString());
		chdrpf.setCurrfrom(wsaaToday.toInt());
		chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpf.setValidflag("1".charAt(0));
		chdrpf.setTranlused(chdrpf.getTranno());
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(add(chdrpf.getTranno(), 1).toInt());
		chdrpfDAO.insertChdrRecords(chdrpf); //ILIFE-8076
	}

protected void keepsChdrmja5030()
	{
		/* DO A KEEPS ON CHDRMJA RECORD FOR THE NEXT PROGRAM*/
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		/*EXIT*/
	}

protected void deleteCvuw5100()
	{
		begin5100();
	}

protected void begin5100()
	{
		cvuwIO.setRecKeyData(SPACES);
		cvuwIO.setChdrcoy(chdrpf.getChdrcoy());
		cvuwIO.setChdrnum(chdrpf.getChdrnum());
		cvuwIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(), varcom.oK)
		&& isNE(cvuwIO.getStatuz(), varcom.mrnf)
		&& isNE(cvuwIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(cvuwIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), cvuwIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), cvuwIO.getChdrnum())
		|| isNE(cvuwIO.getStatuz(), varcom.oK)) {
			return ;
		}
		cvuwIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cvuwIO.getStatuz());
			syserrrec.params.set(cvuwIO.getParams());
			fatalError600();
		}
	}

protected void updateUwdate5200()
	{
		/*BEGIN*/
		cvuwIO.setRecKeyData(SPACES);
		cvuwIO.setChdrcoy(chdrpf.getChdrcoy());
		cvuwIO.setChdrnum(chdrpf.getChdrnum());
		cvuwIO.setHuwdcdte(sv.huwdcdte);
		cvuwIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cvuwIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void uwdateMandatory5300()
	{
		/* Read table TH506 to see if underwriting decision date is*/
		/* mandatory and therefore check HPAD-HUWDCDATE is set*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("TH506");
		itempf.setItemitem(chdrpf.getCnttype());//IJTI-1410
		
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("TH506");
			itempf.setItemitem("***");
			itempf = itemDAO.getItemRecordByItemkey(itempf);

		}
		if (itempf != null) {
			th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}
//ILIFE-6809
protected void deleteCprp5400(){
	cprppfDAO.delCprpRecord(chdrpf.getChdrpfx(), chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());//IJTI-1410
}


protected void dryProcessing8000()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrpf.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (itempf == null) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (itempf == null) {
			return ;
		}
		t7508rec.t7508Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrpf.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrpf.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
		drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
		drypDryprcRecInner.drypStmdte.set(chdrpf.getStatdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(varcom.vrcmMaxDate);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75088100()
	{
	    itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T7508");
		itempf.setItemitem(wsaaT7508Key.toString());
        itempf = itemDAO.getItemRecordByItemkey(itempf);
	}

protected void checkChanges9000()
	{
		start9000();
	}

protected void start9000()
	{
		wsaaChange = "N";
		covtmjaIO.setRecKeyData(SPACES);
		covtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		covtmjaIO.setChdrnum(chdrpf.getChdrnum());
		covtmjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covtmjaIO.getStatuz(), varcom.oK)
		&& isEQ(covtmjaIO.getChdrcoy(), chdrpf.getChdrcoy())
		&& isEQ(covtmjaIO.getChdrnum(), chdrpf.getChdrnum())) {
			wsaaChange = "Y";
			return ;
		}
		if (isNE(sv.huwdcdte, ZERO)
		&& isNE(sv.huwdcdte, varcom.vrcmMaxDate)) {
			wsaaChange = "Y";
			return ;
		}
		cvuwIO.setRecKeyData(SPACES);
		cvuwIO.setChdrcoy(chdrpf.getChdrcoy());
		cvuwIO.setChdrnum(chdrpf.getChdrnum());
		cvuwIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(), varcom.oK)
		&& isNE(cvuwIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(cvuwIO.getParams());
			fatalError600();
		}
		if (isEQ(cvuwIO.getStatuz(), varcom.oK)
		&& isEQ(cvuwIO.getChdrcoy(), chdrpf.getChdrcoy())
		&& isEQ(cvuwIO.getChdrnum(), chdrpf.getChdrnum())) {
			wsaaChange = "Y";
			return ;
		}
	}

	protected void performLincProcessing() {
		Tjl47rec tjl47rec = new Tjl47rec(); 
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf.setItemtabl(tablesInner.tjl47.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if(itempf != null) {
			tjl47rec.tjl47Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			LincCSService lincService = (LincCSService) getApplicationContext().getBean(tjl47rec.lincsubr.toString().toLowerCase());
			if(modComp.isTrue())
				lincService.modifyComponentProposal(setLincpfHelper(), tjl47rec);
		}
	}
	
	protected LincpfHelper setLincpfHelper() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		LincpfHelper lincpfHelper = new LincpfHelper();
		lincpfHelper.setChdrcoy(wsspcomn.company.toString());
		lincpfHelper.setChdrnum(chdrpf.getChdrnum());
		lincpfHelper.setOwncoy(wsspcomn.fsuco.toString());
		lincpfHelper.setClntnum(chdrpf.getCownnum().trim());
		lincpfHelper.setCownnum(chdrpf.getCownnum().trim());
		lincpfHelper.setLifcnum(lifepfIO.getLifcnum());
		lincpfHelper.setLcltdob(lifepfIO.getCltdob());
		lincpfHelper.setLcltsex(lifepfIO.getCltsex().trim());
		lincpfHelper.setLzkanasnm(clts.getZkanasnm());
		lincpfHelper.setLzkanagnm(clts.getZkanagnm());
		lincpfHelper.setLzkanaddr01(clts.getZkanaddr01());
		lincpfHelper.setLzkanaddr02(clts.getZkanaddr02());
		lincpfHelper.setLzkanaddr03(clts.getZkanaddr03());
		lincpfHelper.setLzkanaddr04(clts.getZkanaddr04());
		lincpfHelper.setLzkanaddr05(clts.getZkanaddr05());
		lincpfHelper.setLgivname(clts.getGivname());
		lincpfHelper.setLsurname(clts.getSurname());
		lincpfHelper.setCnttype(chdrpf.getCnttype().trim());
		lincpfHelper.setOccdate(chdrpf.getOccdate());	
		lincpfHelper.setTransactionDate(wsaaToday.toInt());
		lincpfHelper.setTranscd(wsaaBatckey.batcBatctrcde.toString());
		lincpfHelper.setLanguage(wsspcomn.language.toString());
		lincpfHelper.setTranno(chdrpf.getTranno());
		lincpfHelper.setPtrnEff(datcon1rec.intDate.toInt());
		lincpfHelper.setTermId(varcom.vrcmTermid.toString());
		lincpfHelper.setTrdt(varcom.vrcmDate.toInt());
		lincpfHelper.setTrtm(varcom.vrcmTime.toInt());
		lincpfHelper.setUsrprf(varcom.vrcmUser.toString());
		lincpfHelper.setJobnm(wsspcomn.userid.toString());
		lincpfHelper = setClientDetails(lincpfHelper);
		return lincpfHelper;
	}
	
	protected LincpfHelper setClientDetails(LincpfHelper lincpfHelper) {
		Clntpf clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(lincpfHelper.getOwncoy());
		clntpf.setClntnum(lincpfHelper.getClntnum());
		clntpf = clntpfDAO.selectClient(clntpf);
		lincpfHelper.setZkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setZkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setZkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setZkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setZkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setZkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setZkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setGivname(clntpf.getGivname());
		lincpfHelper.setSurname(clntpf.getSurname());
		lincpfHelper.setCltsex(clntpf.getCltsex());
		lincpfHelper.setCltdob(clntpf.getCltdob());
		lincpfHelper.setClttype(clts.getClttype());//check this
		return lincpfHelper;
	}
	
	protected void performLincValidations() {
		String lincError;
		LincCommonsUtil lincUtil = (LincCommonsUtil) getApplicationContext().getBean("lincCommonsUtil");
		lincError = lincUtil.validateLincRecord(chdrpf.getChdrnum().trim());
		if(lincError != null) {
			sv.chdrnumErr.set(lincError);
			wsspcomn.edterror.set("Y");
		}
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
public static final class WsaaMiscellaneousInner {

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");

	private FixedLengthStringData wsaaPlanComponent = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaPlanComponent, 0).setUnsigned();

	private FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).init(1);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaLifeNo = new FixedLengthStringData(2);
	public ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5681 = new FixedLengthStringData(5).init("T5681");
	private FixedLengthStringData t5682 = new FixedLengthStringData(5).init("T5682");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData th506 = new FixedLengthStringData(5).init("TH506");
	private FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData t5677 = new FixedLengthStringData(5).init("T5677");
	private FixedLengthStringData tjl47 = new FixedLengthStringData(5).init("TJL47");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
	private FixedLengthStringData covrincrec = new FixedLengthStringData(10).init("COVRINCREC");
	private FixedLengthStringData racdmjarec = new FixedLengthStringData(10).init("RACDMJAREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	//TSD-266 Start
	private FixedLengthStringData rfpj = new FixedLengthStringData(4).init("RFPJ");
	private FixedLengthStringData rfpk = new FixedLengthStringData(4).init("RFPK");
	private FixedLengthStringData pfpl = new FixedLengthStringData(4).init("RFPL");
//	private FixedLengthStringData zm08 = new FixedLengthStringData(4).init("ZM08"); not need
	//TSD-266 Ends
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

		/*SCREEN FORMATS**********************************************
		  COPY SCRNPARAMS.
		  COPY SR569SCR.                                               */
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}

protected void selectRecordCustomerSpecific(){
	
}

}