package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:04
 * Description:
 * Copybook name: LEXTREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lextrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lextrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lextrevKey = new FixedLengthStringData(64).isAPartOf(lextrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData lextrevChdrcoy = new FixedLengthStringData(1).isAPartOf(lextrevKey, 0);
  	public FixedLengthStringData lextrevChdrnum = new FixedLengthStringData(8).isAPartOf(lextrevKey, 1);
  	public FixedLengthStringData lextrevLife = new FixedLengthStringData(2).isAPartOf(lextrevKey, 9);
  	public FixedLengthStringData lextrevCoverage = new FixedLengthStringData(2).isAPartOf(lextrevKey, 11);
  	public FixedLengthStringData lextrevRider = new FixedLengthStringData(2).isAPartOf(lextrevKey, 13);
  	public PackedDecimalData lextrevSeqnbr = new PackedDecimalData(3, 0).isAPartOf(lextrevKey, 15);
  	public PackedDecimalData lextrevTranno = new PackedDecimalData(5, 0).isAPartOf(lextrevKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(lextrevKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lextrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lextrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}