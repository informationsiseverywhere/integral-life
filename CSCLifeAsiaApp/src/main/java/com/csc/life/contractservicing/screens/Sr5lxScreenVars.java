package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sr5lxScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(197);
	public FixedLengthStringData dataFields = new FixedLengthStringData(68).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,6);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,14);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,22);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,30);
	public ZonedDecimalData taxPeriodFrm = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields, 60);
	public ZonedDecimalData taxPeriodTo = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields, 64);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 69);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData taxPeriodFrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData taxPeriodToErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 101);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] taxPeriodFrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] taxPeriodToOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	
	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr5lxscreenWritten = new LongData(0);
	public LongData Sr5lxprotectWritten = new LongData(0);
	
	@Override
	public boolean hasSubfile() {
		return false;
	}
	
	public Sr5lxScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {
		fieldIndMap.put(taxPeriodFrmOut, new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxPeriodToOut, new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, taxPeriodFrm, taxPeriodTo};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, taxPeriodFrmOut, taxPeriodToOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, taxPeriodFrmErr, taxPeriodToErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr5lxscreen.class;
		protectRecord = Sr5lxprotect.class;
	}
}
