package com.csc.life.contractservicing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: TpoldbtTableDAM.java
 * Date: Sun, 30 Aug 2009 03:49:51
 * Class transformed from TPOLDBT.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class TpoldbtTableDAM extends TpdbpfTableDAM {

	public TpoldbtTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("TPOLDBT");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "CNTTYPE, " +
		            "EFFDATE, " +
		            "ORGAMNT, " +
		            "FROMDATE, " +
		            "TODATE, " +
		            "TDBTDESC, " +
		            "TDBTRATE, " +
		            "TDBTAMT, " +
		            "VALIDFLAG, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               tranno,
                               cnttype,
                               effdate,
                               orgamnt,
                               fromdate,
                               todate,
                               tdbtdesc,
                               tdbtrate,
                               tdbtamt,
                               validflag,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(52);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(128);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getCnttype().toInternal()
					+ getEffdate().toInternal()
					+ getOrgamnt().toInternal()
					+ getFromdate().toInternal()
					+ getTodate().toInternal()
					+ getTdbtdesc().toInternal()
					+ getTdbtrate().toInternal()
					+ getTdbtamt().toInternal()
					+ getValidflag().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, orgamnt);
			what = ExternalData.chop(what, fromdate);
			what = ExternalData.chop(what, todate);
			what = ExternalData.chop(what, tdbtdesc);
			what = ExternalData.chop(what, tdbtrate);
			what = ExternalData.chop(what, tdbtamt);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getOrgamnt() {
		return orgamnt;
	}
	public void setOrgamnt(Object what) {
		setOrgamnt(what, false);
	}
	public void setOrgamnt(Object what, boolean rounded) {
		if (rounded)
			orgamnt.setRounded(what);
		else
			orgamnt.set(what);
	}	
	public PackedDecimalData getFromdate() {
		return fromdate;
	}
	public void setFromdate(Object what) {
		setFromdate(what, false);
	}
	public void setFromdate(Object what, boolean rounded) {
		if (rounded)
			fromdate.setRounded(what);
		else
			fromdate.set(what);
	}	
	public PackedDecimalData getTodate() {
		return todate;
	}
	public void setTodate(Object what) {
		setTodate(what, false);
	}
	public void setTodate(Object what, boolean rounded) {
		if (rounded)
			todate.setRounded(what);
		else
			todate.set(what);
	}	
	public FixedLengthStringData getTdbtdesc() {
		return tdbtdesc;
	}
	public void setTdbtdesc(Object what) {
		tdbtdesc.set(what);
	}	
	public PackedDecimalData getTdbtrate() {
		return tdbtrate;
	}
	public void setTdbtrate(Object what) {
		setTdbtrate(what, false);
	}
	public void setTdbtrate(Object what, boolean rounded) {
		if (rounded)
			tdbtrate.setRounded(what);
		else
			tdbtrate.set(what);
	}	
	public PackedDecimalData getTdbtamt() {
		return tdbtamt;
	}
	public void setTdbtamt(Object what) {
		setTdbtamt(what, false);
	}
	public void setTdbtamt(Object what, boolean rounded) {
		if (rounded)
			tdbtamt.setRounded(what);
		else
			tdbtamt.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		cnttype.clear();
		effdate.clear();
		orgamnt.clear();
		fromdate.clear();
		todate.clear();
		tdbtdesc.clear();
		tdbtrate.clear();
		tdbtamt.clear();
		validflag.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}