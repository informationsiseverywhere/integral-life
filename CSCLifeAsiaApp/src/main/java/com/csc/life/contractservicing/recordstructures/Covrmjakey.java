package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:24
 * Description:
 * Copybook name: COVRMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrmjaKey = new FixedLengthStringData(64).isAPartOf(covrmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(covrmjaKey, 0);
  	public FixedLengthStringData covrmjaChdrnum = new FixedLengthStringData(8).isAPartOf(covrmjaKey, 1);
  	public FixedLengthStringData covrmjaLife = new FixedLengthStringData(2).isAPartOf(covrmjaKey, 9);
  	public FixedLengthStringData covrmjaCoverage = new FixedLengthStringData(2).isAPartOf(covrmjaKey, 11);
  	public FixedLengthStringData covrmjaRider = new FixedLengthStringData(2).isAPartOf(covrmjaKey, 13);
  	public PackedDecimalData covrmjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrmjaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrmjaKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}