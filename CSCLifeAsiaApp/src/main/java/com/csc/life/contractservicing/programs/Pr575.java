/*
 * File: Pr575.java
 * Date: 30 August 2009 1:45:12
 * Author: Quipoz Limited
 * 
 * Class transformed from PR575.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Undactncpy;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PrmhactTableDAM;
import com.csc.life.contractservicing.screens.Sr575ScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.productdefinition.tablestructures.Tr51prec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgwTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Centre;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*
* COMPONENT CHANGE - UNIT-LINKED GENERIC.
*
* This program will be invoked for both Component Change and for
* Component Add-ons.
*
* This program will not create nor maintain Coverage/Rider
* records directly on to the COVR data set. Instead it will
* create Transaction record(s) which will have a similar format
* to the COVR records and these will be converted to COVR records
* by the AT module which will follow.
*
*  IF no Coverage/rider details have been changed, but
*    Reassurance or Options/Extras details have been amended,
*    we force the program to produce a COVT record so that the
*    Component change AT module is executed, thus writing a PTRN
*    indicating that something has happened to the Coverage/Rider
*    details.
*
*
*****************************************************************
* </pre>
*/
public class Pr575 extends ScreenProgCS {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR575");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private final int wsaaMaxOcc = 8;

	private FixedLengthStringData wsaaCtrltime = new FixedLengthStringData(1);
	private Validator firsttime = new Validator(wsaaCtrltime, "Y");
	private Validator nonfirst = new Validator(wsaaCtrltime, "N");

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private Validator premNotReqd = new Validator(wsaaPremStatuz, "N");
	private Validator userPremEntered = new Validator(wsaaPremStatuz, "U");
	private PackedDecimalData wsaaCalcPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreZbinstprem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreSingp = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreInstprem = new ZonedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator addComp = new Validator(wsaaFlag, "A");
	private Validator modifyComp = new Validator(wsaaFlag, "M");
	private Validator compEnquiry = new Validator(wsaaFlag, "I");
	private Validator compApp = new Validator(wsaaFlag, "S","P");
	private Validator revComp = new Validator(wsaaFlag, "R");
	private Validator addLife = new Validator(wsaaFlag, "D");

	private FixedLengthStringData wsaaIfEndOfPlan = new FixedLengthStringData(1);
	private Validator wsaaEndOfPlan = new Validator(wsaaIfEndOfPlan, "Y");

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator nonplan = new Validator(wsaaPlanproc, "N");
	private Validator plan = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaIfSuminsChanged = new FixedLengthStringData(1);
	private Validator wsaaSuminsChanged = new Validator(wsaaIfSuminsChanged, "I","D");
	private Validator wsaaSuminsIncreased = new Validator(wsaaIfSuminsChanged, "I");
	private Validator wsaaSuminsDecreased = new Validator(wsaaIfSuminsChanged, "D");

	private FixedLengthStringData wsaaIfPremChanged = new FixedLengthStringData(1);
	private Validator wsaaPremChanged = new Validator(wsaaIfPremChanged, "I","D");
	private Validator wsaaPremIncreased = new Validator(wsaaIfPremChanged, "I");
	private Validator wsaaPremDecreased = new Validator(wsaaIfPremChanged, "D");

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(" ");
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");
	private PackedDecimalData wsaaTargFrom = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaBillfreqNum = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, REDEFINE);
	private ZonedDecimalData wsaaBillfreqN = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreqNum, 0).setUnsigned();
	private ZonedDecimalData wsaaNewTarget = new ZonedDecimalData(17, 2);

	private FixedLengthStringData wsaaAnotherFpco = new FixedLengthStringData(1).init("N");
	private Validator anotherFpco = new Validator(wsaaAnotherFpco, "Y");

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private PackedDecimalData wsaaCovrPlnsfx = new PackedDecimalData(4, 0).isAPartOf(wsaaCovrKey, 15);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaCovrKey, 18, FILLER).init(SPACES);
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaStoreSumin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaIfCovt = new FixedLengthStringData(1);
	private Validator wsaaNoCovt = new Validator(wsaaIfCovt, "N");
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private PackedDecimalData wsaaSumin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaLumpSum = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaOldPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSuminsDiff = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaOldMortcls = new FixedLengthStringData(1);
	private PackedDecimalData wsaaNextSeqnbr = new PackedDecimalData(3, 0).init(999);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaOptext = new FixedLengthStringData(1);
	private Validator optextYes = new Validator(wsaaOptext, "Y");
	private Validator optextNo = new Validator(wsaaOptext, "N");
	private PackedDecimalData wsaaDate = new PackedDecimalData(8, 0);
		/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaAnbAtCcd2 = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaSex2 = new FixedLengthStringData(1).init(SPACES);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzMatage = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzMattrm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private FixedLengthStringData subname = new FixedLengthStringData(10).init(SPACES);
	private ZonedDecimalData premiumCalcInd = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private FixedLengthStringData totPolsInPlan = new FixedLengthStringData(4);
	private ZonedDecimalData singPremInd = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private ZonedDecimalData regPremInd = new ZonedDecimalData(1, 0).init(0).setUnsigned();
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(22, 5);

	private FixedLengthStringData wsaaCentreString = new FixedLengthStringData(80);
	private FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(wsaaCentreString, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCentreHeading = new FixedLengthStringData(32).isAPartOf(wsaaCentreString, 24);
	private FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(wsaaCentreString, 56, FILLER).init(SPACES);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler7 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler7, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler9 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler9, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private ZonedDecimalData sub4 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData billFrequency = new FixedLengthStringData(2);
	private static final int wsaaMaxMort = 6;
	/* BRD-306 START */
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTempPrev = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaPrevDue = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNextDue = new ZonedDecimalData(8, 0).setUnsigned();
	/* BRD-306 END */
	private FixedLengthStringData wsaaError1Flag = new FixedLengthStringData(1);
	private Validator wsaaError1 = new Validator(wsaaError1Flag, "Y");

	private FixedLengthStringData wsaaError2Flag = new FixedLengthStringData(1);
	private Validator wsaaError2 = new Validator(wsaaError2Flag, "Y");

	private FixedLengthStringData wsaaError3Flag = new FixedLengthStringData(1);
	private Validator wsaaError3 = new Validator(wsaaError3Flag, "Y");

	private FixedLengthStringData wsaaFoundFlag = new FixedLengthStringData(1);
	private Validator wsaaFound = new Validator(wsaaFoundFlag, "Y");

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);
	private FixedLengthStringData wsaaLextUpdates = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsccSingPremFlag = new FixedLengthStringData(1);
	private Validator singlePremium = new Validator(wsccSingPremFlag, "Y");

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private String wsaaInPh = "";
	private PackedDecimalData wsaaLinstamt = new PackedDecimalData(17, 2);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovrrgwTableDAM covrrgwIO = new CovrrgwTableDAM();
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private FprmTableDAM fprmIO = new FprmTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private MandTableDAM mandIO = new MandTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private PrmhactTableDAM prmhactIO = new PrmhactTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T5551rec t5551rec = new T5551rec();
	private T2240rec t2240rec = new T2240rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T5729rec t5729rec = new T5729rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Gensswrec gensswrec = new Gensswrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Undactncpy undactncpy = new Undactncpy();
	private T5667rec t5667rec = new T5667rec();
	private Tr51prec tr51prec = new Tr51prec();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private Sr575ScreenVars sv = ScreenProgram.getScreenVars( Sr575ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();

	/*TSD-308 START*/
    private Datcon4rec datcon4rec = new Datcon4rec();	// TSD-308
	private ZonedDecimalData wsaaMnthVers = new ZonedDecimalData(8, 0);	// TSD-308
	private ZonedDecimalData wsaaLastAnnv = new ZonedDecimalData(8, 0);	// TSD-308
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	boolean isFeaAllowed = false; 
	/*TSD-308 END*/
	private boolean loadingFlag = false;//ILIFE-3399
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();

	private Itempf itempf = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private T3644rec t3644rec = new T3644rec();
	boolean NBPRP056Permission  = false;
	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	private FixedLengthStringData hroWarnMsg = new FixedLengthStringData(20).init("High Risk Occupation");
	private Ta610rec ta610rec = new Ta610rec();
	private T5661rec t5661rec = new T5661rec();
	boolean isFollowUpRequired  = false;
	private Fluppf fluppf;
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();	//ICIL-1494
	private int fupno = 0;
	private List<Fluppf> fluppfList = new ArrayList<Fluppf>();
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private Descpf fupDescpf;
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private static final String NBPRP056 = "NBPRP056";
	private static final String t5661 = "T5661";
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> itempfList = new ArrayList<Itempf>();	//ICIL-1494
	
		//ILB-456 starts
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private Covrpf covrpf = new Covrpf();
	private int covrpfCount = 0;
	private List<Covrpf> covrpfList;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrList = new ArrayList();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		loadHeading1200, 
		loop11250, 
		loop21300, 
		setScreen1060, 
		exit1900, 
		beforeExit2080, 
		redisplay2080, 
		exit2090, 
		checkSuminCont2526, 
		checkLumpSum2527, 
		checkMaturityFields2530, 
		checkMatAgeTerm2540, 
		checkPremAgeTerm2540, 
		cont, 
		validDate2541, 
		datesCont2541, 
		ageAnniversary2541, 
		check2542, 
		check2543, 
		checkOccurance2545, 
		checkTermFields2550, 
		checkComplete2555, 
		checkMortcls2560, 
		loop2565, 
		checkLiencd2570, 
		loop2575, 
		checkMore2580, 
		adjustPrem2725, 
		exit2790, 
		checkPcdt3020, 
		exit3490, 
		cont3555, 
		exit5090, 
		skipTr52e5800, 
		covtExist6025, 
		restOfEnquiry6060, 
		matchProgName
	}

	public Pr575() {
		super();
		screenVars = sv;
		new ScreenModel("Sr575", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1100();
					premmeth1020();
					premiumTolerance1100();
				case loadHeading1200: 
					loadHeading1200();
				case loop11250: 
					loop11250();
				case loop21300: 
					loop21300();
				case setScreen1060: 
					setScreen1060();
				case exit1900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1100()
	{
		if (isEQ(wsspcomn.flag, "J")) {
			wsspcomn.flag.set("I");
		}
		wsaaFlag.set(wsspcomn.flag);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1900);
		}
		NBPRP056Permission  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), NBPRP056, appVars, "IT");
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		undactncpy.undActions.set(wsspcomn.undAction);
		wsaaFlag.set(wsspcomn.flag);
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);	//ICIL-1494
		}
		/* INITIALISE NUMERIC AND VARIOUS FIELDS ON THE SCREEN*/
		sv.comind.set(SPACES);
		wsccSingPremFlag.set(SPACES);
		sv.optextind.set(SPACES);
		singPremInd.set(ZERO);
		sv.singlePremium.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		sv.sumin.set(ZERO);
		sv.polinc.set(ZERO);
		sv.planSuffix.set(ZERO);
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		sv.adjustageamt.set(0);
		/*BRD-306 END */
		sv.zbinstprem.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.matage.set(ZERO);
		sv.mattrm.set(ZERO);
		sv.taxamt.set(ZERO);
		/*                               SR575-LUMP-CONTRIB.           */
		/*MOVE 'N'                    TO SR575-RESERVE-UNITS-IND.      */
		/*MOVE VRCM-MAX-DATE          TO SR575-RESERVE-UNITS-DATE.     */
		sv.premcess.set(varcom.vrcmMaxDate);
		sv.mattcess.set(varcom.vrcmMaxDate);
		sv.currcd.set(wsspcomn.trancurr);
		wsaaCtrltime.set("N");
		wsaaSumin.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaSumin.set(ZERO);
		wsaaOldPrem.set(ZERO);
		wsaaOldSumins.set(ZERO);
		wsaaPremDiff.set(ZERO);
		wsaaTaxamt.set(ZERO);
		sv.taxamt.set(ZERO);
		wsaaSuminsDiff.set(ZERO);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		covtmjaIO.setReserveUnitsDate(ZERO);
		wsaaFirstTaxCalc.set("Y");
		/* Read CHDRMJA (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		totPolsInPlan.set(chdrpf.getPolinc());
		sv.polinc.set(chdrpf.getPolinc());
		/*BRD-306 START */
		sv.cnttype.set(chdrpf.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
	
        /*Calculating Effective date */
		datcon2rec.intDate1.set(chdrpf.getOccdate());
		datcon2rec.intDate2.set(chdrpf.getOccdate());
		if (addComp.isTrue()) {
	//	|| (modifyComp.isTrue())) {//ILIFE-2090
			while ( !(isGT(datcon2rec.intDate2,wsspcomn.currfrom))) {
				getNextEffdate1400();
			}
			
			wsaaPrevDue.set(wsaaTempPrev);
			wsaaNextDue.set(datcon2rec.intDate2);
		}
		if (addComp.isTrue()) {
		//||(modifyComp.isTrue())) {   //ILIFE-2090
			sv.effdate.set(wsspcomn.currfrom);
		}
		else {
			sv.effdate.set(chdrpf.getOccdate());
		}
		/*BRD-306 END */
				
		/*MOVE CHDRMJA-CNTCURR          TO SR575-CURRCD.               */
		/*If the billing frequency on the Contract Header is greater   */
		/*than zero then this is a regular premium component.          */
		/*Otherwise the component is a single premium one.             */
		/*IF CHDRMJA-BILLFREQ         = ZEROS                          */
		/*   MOVE 1                     TO SING-PREM-IND               */
		/*ELSE                                                         */
		/*   MOVE 1                     TO REG-PREM-IND.               */
		/*MOVE CHDRMJA-BILLFREQ       TO BILL-FREQUENCY.               */
		//ILB-456
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isEQ(covrmjaIO.getStatuz(), varcom.mrnf)) {
				covrmjaIO.setChdrnum(SPACES);
				goTo(GotoLabel.exit1900);
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isEQ(covrmjaIO.getStatuz(), varcom.mrnf)) {
			covrmjaIO.setChdrnum(SPACES);
			goTo(GotoLabel.exit1900);
		}
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		if (isEQ(covrpf.getInstprem(), ZERO)) {
			wsaaLinstamt.set(covrpf.getSingp());
		}
		else {
			wsaaLinstamt.set(covrpf.getInstprem());
		}
		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRMJA-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}//MTL002
			}
		}
		/* Read TR52D for Taxcode.                                         */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		/* Read the PAYR record to get the Billing Details.                */
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setPayrseqno(covrpf.getPayrseqno());
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		sv.currcd.set(payrIO.getCntcurr());
		/*    If the billing frequency on the Contract Header is greater   */
		/*    than zero then this is a regular premium component.          */
		/*    Otherwise the component is a single premium one.             */
		if (isEQ(payrIO.getBillfreq(), ZERO)) {
			singPremInd.set(1);
		}
		billFrequency.set(payrIO.getBillfreq());
		if (isGT(covrpf.getSingp(), 0)
		&& isGT(covrpf.getInstprem(), 0)) {
			sv.singprmOut[varcom.nd.toInt()].set("N");
		}
		else {
			sv.singlePremium.set(ZERO);
			sv.singprmOut[varcom.nd.toInt()].set("Y");
		}
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		wsaaCrtable.set(covrpf.getCrtable());
		/* Release the coverage details for processing of Whole Plan.*/
		/*   i.e. If entering on an action of Component ADD then the*/
		/*        Component to be added must exist for all policies*/
		/*        within the plan.*/
		/*   i.e. If entering on an action of Component Modify/Enquiry*/
		/*        and Whole Plan selected then we will process the*/
		/*        component for all policies within the plan.*/
		if (isEQ(wsaaPlanSuffix, ZERO)) {

			/*covrmjaIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				fatalError600();
			}*/
			covrpfDAO.deleteCacheObject(covrpf);
		}
		/*    If modify of Component requested and the component           */
		/*    in question is a Single premium Component then we move 'y'   */
		/*    to comp enquiry and spaces to comp modify and an error       */
		/*    informing the user that single premium component cannot      */
		/*    be modified.                                                 */
		if (modifyComp.isTrue()
		&& isEQ(payrIO.getBillfreq(), ZERO)) {
			wsaaFlag.set("I");
			wsccSingPremFlag.set("Y");
		}
		/*    Obtain the long description from DESC using DESCIO.*/
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5687");
		descIO.setDescitem(wsaaCrtable);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		wsaaHedline.set(descIO.getLongdesc());
		/*                                                         <D9604> */
		/*  Read T5729 to determine if it is a flexible premium    <D9604> */
		/*  contract. If so, use the target start date instead of the      */
		/*  PTDATE as the effective date when performing a modify.         */
		wsaaFlexiblePremium.set("N");
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5729);
		itemIO.setItemitem(chdrpf.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			t5729rec.t5729Rec.set(itemIO.getGenarea());
			wsaaFlexiblePremium.set("Y");
			if (modifyComp.isTrue()) {
				readFpcoFprm1300();
			}
		}
		//ILIFE-3399-STRATS
		loadingFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP04", appVars, "IT");/* IJTI-1523 */
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-3399-ENDS
	}
protected void getNextEffdate1400()
{
	/*GET-EFFDATE*/
	datcon2rec.freqFactor.set(1);
	datcon2rec.frequency.set(chdrpf.getBillfreq());
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz,varcom.bomb)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	wsaaTempPrev.set(datcon2rec.intDate1);
	datcon2rec.intDate1.set(datcon2rec.intDate2);
	wsaaFreq.add(1);
	/*EXIT*/
}

protected void premmeth1020()
	{
		/*  - use the  premium-method  selected  from  T5687, if not*/
		/*       blank,  to access T5675.  This gives the subroutine*/
		/*       to use for the calculation.*/
		/*    Obtain the general Coverage/Rider details (including Premium*/
		/*    Calculation Method details from T5687 using the coverage/*/
		/*    rider code as part of the key (COVTLNB-CRTABLE).*/
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5687");
		itdmIO.setItemitem(covrpf.getCrtable());
		/*MOVE CHDRMJA-PTDATE         TO ITDM-ITMFRM.                  */
		itdmIO.setItmfrm(payrIO.getPtdate());
		if (flexiblePremium.isTrue()) {
			if (modifyComp.isTrue()) {
				itdmIO.setItmfrm(wsaaTargFrom);
			}
			else {
				itdmIO.setItmfrm(payrIO.getBtdate());
			}
		}
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrpf.getCrtable())
		|| isNE(itdmIO.getValidflag(), "1")) {
			syserrrec.statuz.set(errorsInner.e366);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/* IF T5687-RIIND              = 'N' OR SPACES          <R96REA>*/
		/*    MOVE SPACES              TO SR575-RATYPIND        <R96REA>*/
		/*    MOVE 'Y'                 TO SR575-RATYPIND-OUT(ND)<R96REA>*/
		/*    MOVE 'N'                    TO PREM-REQD.                    */
		wsaaPremStatuz.set("N");
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t5687rec.premmeth);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/* MOVE 'Y'                 TO PREM-REQD                     */
			wsaaPremStatuz.set("Y");
			goTo(GotoLabel.loadHeading1200);
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void premiumTolerance1100()
	{
		/* Read the latest premium tollerance allowed.                     */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(payrIO.getCntcurr());
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
	}

protected void loadHeading1200()
	{
		wsaaCentreHeading.set(wsaaHedline);
		callProgram(Centre.class, wsaaCentreString);
		wsaaHeading.set(wsaaCentreHeading);
		wsaaHeadingChar[1].set(SPACES);
		/*MOVE SPACE                  TO WSAA-HEADING-CHAR (26).       */
		wsaaHeadingChar[32].set(SPACES);
		sub4.set(1);
	}

protected void loop11250()
	{
		if (isEQ(wsaaHeadingChar[sub4.toInt()], SPACES)) {
			sub4.add(1);
			goTo(GotoLabel.loop11250);
		}
		sub4.subtract(1);
		wsaaHeadingChar[sub4.toInt()].set(wsaaStartUnderline);
		/*MOVE 26                     TO SUB4.                         */
		sub4.set(32);
	}

protected void loop21300()
	{
		if (isEQ(wsaaHeadingChar[sub4.toInt()], SPACES)) {
			sub4.subtract(1);
			goTo(GotoLabel.loop21300);
		}
		sub4.add(1);
		wsaaHeadingChar[sub4.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		/* GET LIFE INSURED AND JOINT LIFE DETAILS NEEDED FOR PREMIUM*/
		/* CALCULATION AND FOR SHOWING THE LIFE NAME ON THE SCREEN.*/
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/*   If component add for a single premium then we require    <005>*/
		/*   todays date to be used to calculate age next birthday    <005>*/
		/*   instead of CHDRMJA-BTDATE.                               <005>*/
		/*                                                            <005>*/
		/*   Calculation of age of life assured is incorrect...            */
		/*    IF  ADD-COMP                                                 */
		/*        MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1           */
		/*        MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2           */
		/*        MOVE '01'                   TO DTC3-FREQUENCY            */
		/*        IF CHDRMJA-BILLFREQ         = 00                    <005>*/
		/*           MOVE TDAY                   TO DTC1-FUNCTION     <005>*/
		/*                                                            <005>*/
		/*           CALL 'DATCON1'              USING DTC1-DATCON1-REC005>*/
		/*                                                            <005>*/
		/*           MOVE DTC1-INT-DATE          TO DTC3-INT-DATE-2   <005>*/
		/*                                                            <005>*/
		/*           CALL 'DATCON3' USING DTC3-DATCON3-REC            <005>*/
		/*                                                            <005>*/
		/*           ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD005>*/
		/*        ELSE                                                <005>*/
		/*           CALL 'DATCON3' USING DTC3-DATCON3-REC                 */
		/*           ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD    */
		/*    ELSE                                                         */
		/*        MOVE LIFEMJA-ANB-AT-CCD                                  */
		/*          TO WSAA-ANB-AT-CCD.                                    */
		/*    Chdrmja-btdate is always used to calculate the age of life   */
		/*    assured.                                                     */
		/* MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1          <023>*/
		/*MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2          <023>*/
		/* MOVE PAYR-BTDATE            TO DTC3-INT-DATE-2          <046>*/
		/* MOVE '01'                   TO DTC3-FREQUENCY           <023>*/
		/* CALL 'DATCON3' USING DTC3-DATCON3-REC                   <023>*/
		/* ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD      <023>*/
		calcAge1700();
		if (!addComp.isTrue()) {
			wsaaAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		/*MOVE WSAA-ANB-AT-CCD        TO SR575-ANB-AT-CCD.             */
		sv.anbAtCcd.set(lifemjaIO.getAnbAtCcd());
		wsaaCltdob.set(lifemjaIO.getCltdob());
		wsaaSex.set(lifemjaIO.getCltsex());
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		/* MOVE LIFEMJA-CHDRCOY        TO CLTS-CLNTCOY.                 */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/* CHECK FOR EXISTENCE OF JOINT LIFE DETAILS.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaAnbAtCcd2.set(0);
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.setScreen1060);
		}
		/* IF  ADD-COMP                                                 */
		/*     MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1           */
		/*   MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2           */
		/*     MOVE PAYR-BTDATE            TO DTC3-INT-DATE-2      <046>*/
		/*     MOVE '01'                   TO DTC3-FREQUENCY            */
		/*IF CHDRMJA-BILLFREQ         = 00                    <005>*/
		/*     IF   PAYR-BILLFREQ          = 00                    <046>*/
		/*        MOVE TDAY                   TO DTC1-FUNCTION     <005>*/
		/*        CALL 'DATCON1'              USING DTC1-DATCON1-REC<005*/
		/*        MOVE DTC1-INT-DATE          TO DTC3-INT-DATE-2   <005>*/
		/*        CALL 'DATCON3' USING DTC3-DATCON3-REC            <005>*/
		/*        ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD<005*/
		/*     ELSE                                                <005>*/
		/*        CALL 'DATCON3' USING DTC3-DATCON3-REC                 */
		/*        ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD    */
		/* ELSE                                                         */
		/*     MOVE LIFEMJA-ANB-AT-CCD                                  */
		/*       TO WSAA-ANB-AT-CCD2.                                   */
		if (addComp.isTrue()) {
			initialize(agecalcrec.agecalcRec);
			agecalcrec.function.set("CALCP");
			agecalcrec.language.set(wsspcomn.language);
			agecalcrec.cnttype.set(chdrpf.getCnttype());
			agecalcrec.intDate1.set(lifemjaIO.getCltdob());
			agecalcrec.intDate2.set(payrIO.getBtdate());
			agecalcrec.company.set(wsspcomn.fsuco);
			if (isEQ(payrIO.getBillfreq(), 0)) {
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				agecalcrec.intDate2.set(datcon1rec.intDate);
				callProgram(Agecalc.class, agecalcrec.agecalcRec);
				if (isNE(agecalcrec.statuz, varcom.oK)) {
					syserrrec.statuz.set(agecalcrec.statuz);
					syserrrec.params.set(agecalcrec.agecalcRec);
					fatalError600();
				}
				/*           MOVE AGEC-AGERATING         TO WSAA-ANB-AT-CCD<LIF2.1>*/
				wsaaAnbAtCcd2.set(agecalcrec.agerating);
			}
			else {
				callProgram(Agecalc.class, agecalcrec.agecalcRec);
				if (isNE(agecalcrec.statuz, varcom.oK)) {
					syserrrec.statuz.set(agecalcrec.statuz);
					syserrrec.params.set(agecalcrec.agecalcRec);
					fatalError600();
				}
				/*           MOVE AGEC-AGERATING         TO WSAA-ANB-AT-CCD<LIF2.1>*/
				wsaaAnbAtCcd2.set(agecalcrec.agerating);
			}
		}
		else {
			wsaaAnbAtCcd2.set(lifemjaIO.getAnbAtCcd());
		}
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		wsaaSex2.set(lifemjaIO.getCltsex());
		/* MOVE LIFEMJA-CHDRCOY  TO CLTS-CLNTCOY.                       */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction("READR");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void setScreen1060()
	{
		/* Set up the screen header details which are constant for all*/
		/*   policies within the plan.*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.polinc.set(chdrpf.getPolinc());
		sv.life.set(lifemjaIO.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		/*MOVE WSAA-ANB-AT-CCD        TO SR575-ANB-AT-CCD.             */
		sv.anbAtCcd.set(lifemjaIO.getAnbAtCcd());
		/*MOVE CHDRMJA-CNTCURR        TO SR575-CURRCD.                 */
		sv.currcd.set(payrIO.getCntcurr());
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		if (isEQ(wsaaPlanSuffix, ZERO)
		&& !addComp.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			covrpf.setPlanSuffix(9999);
			covtmjaIO.setPlanSuffix(9999);
			//covrmjaIO.setStatuz(SPACES);
			//covrmjaIO.setFunction(varcom.begn);
			covrpfList=covrpfDAO.searchCovrRecordForContract(covrpf);
			covtmjaIO.setFunction(varcom.begn);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanproc.set("Y");
			/**       IF ADD-COMP                                               */
			/**          MOVE ZEROES           TO COVRMJA-COVERAGE*/
			/**                                   COVRMJA-RIDER*/
			/**          NEXT SENTENCE                                          */
			/**       ELSE                                                      */
			/**          NEXT SENTENCE                                          */
		}
		else {
			wsaaPlanproc.set("N");
			covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
			covtmjaIO.setFunction(varcom.readr);
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		/*1899-SETUP-CURRENT-PAYER.                                   <048>*/
		/* Set up the payer information ....                               */
		/* PERFORM 1970-PAYR-LOAD.                                 <048>*/
		/*MOVE COVRMJA-PAYRSEQNO      TO SR575-PAYRSEQNUM.        <048>*/
		/*MOVE CLRF-CLNTNUM           TO SR575-CLNTNUM.           <048>*/
		/* Format the CLIENT name ...                                      */
		/*PERFORM PLAINNAME.                                      <048>*/
		/*MOVE WSSP-LONGCONFNAME      TO SR575-PAYERNAME.         <048>*/
		/* If this is a component modify, check whether this component  */
		/* is subject to Automatic Increases (CPI-DATE holds a valid    */
		/* date).  If so, display a message stating that future         */
		/* increases will not be offered.                               */
		if (modifyComp.isTrue()
		&& isNE(covrpf.getCpiDate(), ZERO)
		&& isNE(covrpf.getCpiDate(), varcom.vrcmMaxDate)) {
			scrnparams.errorCode.set(errorsInner.f862);
		}
		/* The following 5000-section relates to the main coverage details*/
		/*   for each individual policy and as such will initially be load*/
		/*   -ed from here. But, if the Whole Plan has been selected or*/
		/*   Adding the Component then the 5000-section will be repeated*/
		/*   in the 4000-section to load each policy.*/
		policyLoad5000();
		if (isNE(tr52drec.txcode, SPACES)) {
			/*  AND T5687-BBMETH                = SPACES                     */
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("N");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	*1500-MATTCESS SECTION.                                           
	*1500-PARA.                                                       
	*    IF SR575-MATAGE      = 0                                     
	*        GO TO 1510-MATTRM.                                       
	*    IF T5551-EAAGE              = 'A'                            
	*    IF NOT ADD-COMP                                              
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR575-MATAGE                
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5551-EAAGE              = 'A'                            
	*    IF ADD-COMP                                                  
	*       MOVE CHDRMJA-PTDATE     TO DTC2-INT-DATE-1                
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR575-MATAGE                
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5551-EAAGE              = 'E' OR SPACE                   
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       MOVE SR575-MATAGE TO DTC2-FREQ-FACTOR.                    
	*    PERFORM 1600-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ        TO SR575-MATTCESS-ERR             
	*       GO TO 1540-EXIT.                                          
	*    MOVE DTC2-INT-DATE-2        TO SR575-MATTCESS.               
	*    GO TO 1540-EXIT.                                             
	*1510-MATTRM.                                                     
	*    IF SR575-MATTRM     = 0                                      
	*        GO TO 1540-EXIT.                                         
	*    IF T5551-EAAGE              = 'A' OR SPACE                   
	*       MOVE SR575-MATTRM TO DTC2-FREQ-FACTOR                     
	*    IF NOT ADD-COMP                                              
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*    ELSE                                                         
	*       MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1.              
	*    IF T5551-EAAGE              = 'E'                            
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       ADD SR575-MATTRM, WSZZ-ANB-AT-CCD                         
	*                                GIVING DTC2-FREQ-FACTOR          
	*    IF NOT ADD-COMP                                              
	*       IF CHDRMJA-OCCDATE NOT = WSZZ-CLTDOB                      
	*          SUBTRACT 1          FROM DTC2-FREQ-FACTOR.             
	*    IF T5551-EAAGE              = 'E'                            
	*    IF ADD-COMP                                                  
	*       IF CHDRMJA-BTDATE NOT = WSZZ-CLTDOB                       
	*          SUBTRACT 1          FROM DTC2-FREQ-FACTOR.             
	*    PERFORM 1600-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*        MOVE DTC2-STATUZ        TO SR575-MATTCESS-ERR            
	*        GO TO 1540-EXIT.                                         
	*    MOVE DTC2-INT-DATE-2        TO SR575-MATTCESS.               
	*1540-EXIT.                                                       
	*    EXIT.                                                        
	* 1550-PREMCESS  SECTION.                                         
	* 1550-PARA.                                                      
	*     IF SR575-PREM-CESS-AGE      = 0                             
	*         GO TO 1560-PREM-CESS-TERM.                              
	*     IF T5551-EAAGE              = 'A'                           
	*     IF NOT ADD-COMP                                             
	*        MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1              
	*        SUBTRACT WSZZ-ANB-AT-CCD FROM SR575-PREM-CESS-AGE        
	*                          GIVING DTC2-FREQ-FACTOR.               
	*     IF T5551-EAAGE              = 'A'                           
	*     IF ADD-COMP                                                 
	*        MOVE CHDRMJA-BTDATE     TO DTC2-INT-DATE-1               
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR575-PREM-CESS-AGE         
	*                          GIVING DTC2-FREQ-FACTOR.               
	*     IF T5551-EAAGE              = 'E' OR SPACE                  
	*        MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1              
	*        MOVE SR575-PREM-CESS-AGE TO DTC2-FREQ-FACTOR.            
	*     PERFORM 1600-CALL-DATCON2.                                  
	*     IF DTC2-STATUZ              NOT = O-K                       
	*        MOVE DTC2-STATUZ        TO SR575-PREMCESS-ERR            
	*         GO TO 1590-EXIT.                                        
	*     MOVE DTC2-INT-DATE-2        TO SR575-PREMCESS.              
	*     GO TO 1590-EXIT.                                            
	* 1560-PREM-CESS-TERM.                                            
	*     IF SR575-PREM-CESS-TERM     = 0                             
	*         GO TO 1590-EXIT.                                        
	*     IF T5551-EAAGE              = 'A' OR SPACE                  
	*        MOVE SR575-PREM-CESS-TERM TO DTC2-FREQ-FACTOR            
	*     IF NOT ADD-COMP                                             
	*        MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1              
	*     ELSE                                                        
	*        MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1.             
	*     IF T5551-EAAGE              = 'E'                           
	*        MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1              
	*        ADD SR575-PREM-CESS-TERM, WSZZ-ANB-AT-CCD                
	*                                 GIVING DTC2-FREQ-FACTOR         
	*     IF NOT ADD-COMP                                             
	*        IF CHDRMJA-OCCDATE NOT = WSZZ-CLTDOB                     
	*           SUBTRACT 1          FROM DTC2-FREQ-FACTOR.            
	*     IF T5551-EAAGE              = 'E'                           
	*     IF ADD-COMP                                                 
	*        IF CHDRMJA-BTDATE NOT = WSZZ-CLTDOB                      
	*           SUBTRACT 1          FROM DTC2-FREQ-FACTOR.            
	*     PERFORM 1600-CALL-DATCON2.                                  
	*     IF DTC2-STATUZ              NOT = O-K                       
	*         MOVE DTC2-STATUZ        TO SR575-PREMCESS-ERR           
	*         GO TO 1590-EXIT.                                        
	*     MOVE DTC2-INT-DATE-2        TO SR575-PREMCESS.              
	* 1590-EXIT.                                                      
	*      EXIT.                                                      
	* </pre>
	*/
protected void readFpcoFprm1300()
	{
		start1310();
	}

	/**
	* <pre>
	****************************                              <D9604>
	* </pre>
	*/
protected void start1310()
	{
		/*<D9604>*/
		/* Read the first active FPCO.                             <D9604>*/
		/* Then attempt to read another. As we only permit a modify<D9604>*/
		/* there is only one active FPCO, set an error message if f<D9604>*/
		/* If not an error, read the FPRM.                         <D9604>*/
		/*<D9604>*/
		wsaaAnotherFpco.set("N");
		fpcoIO.setDataArea(SPACES);
		fpcoIO.setChdrcoy(covrpf.getChdrcoy());
		fpcoIO.setChdrnum(covrpf.getChdrnum());
		fpcoIO.setLife(covrpf.getLife());
		fpcoIO.setCoverage(covrpf.getCoverage());
		fpcoIO.setRider(covrpf.getRider());
		fpcoIO.setPlanSuffix(covrpf.getPlanSuffix());
		fpcoIO.setTargfrom(0);
		fpcoIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcoIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcoIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrpf.getChdrcoy(), fpcoIO.getChdrcoy())
		&& isNE(covrpf.getChdrnum(), fpcoIO.getChdrnum())
		&& isNE(covrpf.getLife(), fpcoIO.getLife())
		&& isNE(covrpf.getCoverage(), fpcoIO.getCoverage())
		&& isNE(covrpf.getRider(), fpcoIO.getRider())
		&& isNE(covrpf.getPlanSuffix(), fpcoIO.getPlanSuffix())) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			fatalError600();
		}
		if (isGT(fpcoIO.getPremRecPer(), fpcoIO.getTargetPremium())) {
			sv.singprmOut[varcom.pr.toInt()].set("Y");
			scrnparams.errorCode.set(errorsInner.d040);
		}
		wsaaTargFrom.set(fpcoIO.getTargfrom());
		/*<D9604>*/
		/* Now attempt to read another FPCO.                       <D9604>*/
		/* If found set an error as we will only allow a modify    <D9604>*/
		/* when there is one active FPCO                           <D9604>*/
		/*<D9604>*/
		fpcoIO.setTargfrom(fpcoIO.getTargto());
		fpcoIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)
		&& isNE(fpcoIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			fatalError600();
		}
		if (isEQ(fpcoIO.getStatuz(), varcom.oK)) {
			sv.singprmOut[varcom.pr.toInt()].set("Y");
			wsaaAnotherFpco.set("Y");
			scrnparams.errorCode.set(errorsInner.d041);
			return ;
		}
		fprmIO.setDataArea(SPACES);
		fprmIO.setChdrcoy(chdrpf.getChdrcoy());
		fprmIO.setChdrnum(chdrpf.getChdrnum());
		fprmIO.setPayrseqno(payrIO.getPayrseqno());
		fprmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			fatalError600();
		}
	}

protected void readTr51p1400()
	{
		begn1410();
	}

protected void begn1410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr51p);
		itemIO.setItemitem(chdrpf.getCnttype());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			tr51prec.tr51pRec.set(itemIO.getGenarea());
		}
		else {
			initialize(tr51prec.tr51pRec);
		}
	}

protected void mattcess1500()
	{
		para1500();
	}

protected void para1500()
	{
		if (isEQ(sv.matage, ZERO)
		&& isEQ(sv.mattrm, ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.matage, 0), true)
		&& isEQ(isGT(sv.mattrm, 0), false)){
			if (isEQ(t5551rec.eaage, "A")) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <033>*/
					datcon2rec.intDate1.set(payrIO.getBtdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.matage, wszzAnbAtCcd));
			}
			if (isEQ(t5551rec.eaage, "E")
			|| isEQ(t5551rec.eaage, SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.matage);
			}
		}
		else if (isEQ(isGT(sv.matage, 0), false)
		&& isEQ(isGT(sv.mattrm, 0), true)){
			if (isEQ(t5551rec.eaage, "A")
			|| isEQ(t5551rec.eaage, SPACES)) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <033>*/
					datcon2rec.intDate1.set(payrIO.getBtdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.mattrm);
			}
			if (isEQ(t5551rec.eaage, "E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.mattrm, wszzAnbAtCcd), 1));
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.mattcessErr.set(datcon2rec.statuz);
		}
		else {
			sv.mattcess.set(datcon2rec.intDate2);
		}
	}

protected void premcess1550()
	{
		para1550();
	}

protected void para1550()
	{
		if (isEQ(sv.premCessAge, ZERO)
		&& isEQ(sv.premCessTerm, ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.premCessAge, 0), true)
		&& isEQ(isGT(sv.premCessTerm, 0), false)){
			if (isEQ(t5551rec.eaage, "A")) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <033>*/
					datcon2rec.intDate1.set(payrIO.getBtdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge, wszzAnbAtCcd));
			}
			if (isEQ(t5551rec.eaage, "E")
			|| isEQ(t5551rec.eaage, SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.premCessAge);
			}
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), true)){
			if (isEQ(t5551rec.eaage, "A")
			|| isEQ(t5551rec.eaage, SPACES)) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <033>*/
					datcon2rec.intDate1.set(payrIO.getBtdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.premCessTerm);
			}
			if (isEQ(t5551rec.eaage, "E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.premCessTerm, wszzAnbAtCcd), 1));
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.premcessErr.set(datcon2rec.statuz);
		}
		else {
			sv.premcess.set(datcon2rec.intDate2);
		}
	}

protected void callDatcon21600()
	{
		/*PARA*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcAge1700()
	{
		init1710();
	}

protected void init1710()
	{
		/* Routine to calculate Age next/nearest/last birthday             */
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(lifemjaIO.getCltdob());
		agecalcrec.intDate2.set(payrIO.getBtdate());
		/* TSD-308 START */
		//TODO:
		isFeaAllowed = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "MTL30801", appVars,smtpfxcpy.item.toString());
		if (isFeaAllowed) {
		caluclateTransactionDate();	// TSD-308
		calculateMonthiversary(); // TSD-308
		caluculateAnniversarydate(); // TSD-308
		agecalcrec.intDate2.set(wsaaLastAnnv);	//  TSD-308
		
		}
		/* TSD-308 END */
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			syserrrec.params.set(agecalcrec.agecalcRec);
			fatalError600();
		}
		wsaaAnbAtCcd.set(agecalcrec.agerating);
		
		
	
	}
/* TSD-308 START */
protected void caluclateTransactionDate(){
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	if (isNE(datcon1rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon1rec.datcon1Rec);
		syserrrec.statuz.set(datcon1rec.statuz);
		fatalError600();
	}
	
}

protected void calculateMonthiversary(){
	datcon3rec.intDate1.set(chdrpf.getOccdate());
	datcon3rec.intDate2.set(datcon1rec.intDate);
	datcon3rec.frequency.set("12");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	ZonedDecimalData var = new ZonedDecimalData(11, 5).init(0);
	compute(var).set(add(datcon3rec.freqFactor, .99999));
	
	datcon2rec.intDate1.set(covrpf.getCrrcd());
	datcon2rec.frequency.set("12");
	datcon2rec.freqFactor.set(var);
	
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	wsaaMnthVers.set(datcon2rec.intDate2);
	
}

protected void caluculateAnniversarydate(){
	datcon4rec.datcon4Rec.set(SPACES);
	datcon4rec.frequency.set("01");
	datcon4rec.freqFactor.set(001);                               
	datcon4rec.intDate1.set(covrpf.getCurrfrom());                       
	datcon4rec.billmonth.set(String.valueOf(covrpf.getCurrfrom()).substring(4, 6));
	datcon4rec.billday.set(String.valueOf(covrpf.getCurrfrom()).substring(6, 8));
	
	while ( !(isGT(datcon4rec.intDate2,wsaaMnthVers))) {
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		wsaaLastAnnv.set(datcon4rec.intDate1);
		
		datcon4rec.intDate1.set(datcon4rec.intDate2);
	}
}
/* TSD-308 END */
protected void checkDefaults1800()
	{
		para1800();
	}

protected void para1800()
	{
		/* Check the Maximum and Minimum values for the Sum Insured on*/
		/* T5551. If both maximum and minimum values are zero then no*/
		/* entry is allowed so protect and non-display the field and its*/
		/* prompt on the screen.*/
		/*    IF  T5551-SUM-INS-MAX = ZERO                                 */
		if (isEQ(t5551rec.sumInsMin, ZERO)
		&& isEQ(t5551rec.sumInsMax, ZERO)) {
			sv.sumin.set(ZERO);
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		/* Check the first entry on T5551 fot the 'From' and 'To' values*/
		/* for both Premium Term and Maturity Term. If the 'From' and*/
		/* 'To' values are the same for either Premium Term or Maturity*/
		/* term then protect the corresponding 'Age' and Date fields on*/
		/* the screen, display the value of the term in the Term field*/
		/* and calculate and display the corresponding Cessation Date.*/
		if (isEQ(t5551rec.maturityTermFrom01, t5551rec.maturityTermTo01)) {
			sv.matage.set(ZERO);
			sv.matageOut[varcom.pr.toInt()].set("Y");
			sv.mattrm.set(t5551rec.maturityTermTo01);
			sv.mattrmOut[varcom.pr.toInt()].set("Y");
			datcon2rec.freqFactor.set(t5551rec.maturityTermTo01);
			datcon2rec.intDate1.set(chdrpf.getOccdate());
			callDatcon21600();
			sv.mattcess.set(datcon2rec.intDate2);
			sv.mattcessOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5551rec.premCesstermFrom01, t5551rec.premCesstermTo01)) {
			sv.premCessAge.set(ZERO);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.premCessTerm.set(t5551rec.premCesstermTo01);
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			datcon2rec.freqFactor.set(t5551rec.premCesstermTo01);
			datcon2rec.intDate1.set(chdrpf.getOccdate());
			callDatcon21600();
			sv.premcess.set(datcon2rec.intDate2);
			sv.premcessOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkLext1900()
	{
		readLext1910();
	}

protected void readLext1910()
	{
		/* MOVE CHDRMJA-CHDRCOY        TO LEXT-CHDRCOY.                 */
		/* MOVE CHDRMJA-CHDRNUM        TO LEXT-CHDRNUM.                 */
		/* MOVE SR575-LIFE             TO LEXT-LIFE.                    */
		/* MOVE SR575-COVERAGE         TO LEXT-COVERAGE.                */
		/* MOVE SR575-RIDER            TO LEXT-RIDER.                   */
		/* MOVE 0                      TO LEXT-SEQNBR.                  */
		/* MOVE LEXTREC                TO LEXT-FORMAT.                  */
		/* MOVE BEGN                   TO LEXT-FUNCTION.                */
		/* CALL 'LEXTIO' USING LEXT-PARAMS.                             */
		/* IF LEXT-STATUZ              NOT = O-K AND                    */
		/*                             NOT = ENDP                       */
		/*    MOVE LEXT-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* IF CHDRMJA-CHDRCOY   NOT = LEXT-CHDRCOY                      */
		/*  OR CHDRMJA-CHDRNUM  NOT = LEXT-CHDRNUM                      */
		/*  OR SR575-LIFE       NOT = LEXT-LIFE                         */
		/*  OR SR575-COVERAGE   NOT = LEXT-COVERAGE                     */
		/*  OR SR575-RIDER      NOT = LEXT-RIDER                        */
		/*    MOVE ENDP                TO LEXT-STATUZ.                  */
		/* IF LEXT-STATUZ              = ENDP                           */
		/*    MOVE ' '                 TO SR575-OPTEXTIND               */
		/* ELSE                                                         */
		/*    MOVE '+'                 TO SR575-OPTEXTIND.              */
		wsaaLextUpdates.set("N");
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrcoy(chdrpf.getChdrcoy());
		lextrevIO.setChdrnum(chdrpf.getChdrnum());
		lextrevIO.setLife(sv.life);
		lextrevIO.setCoverage(sv.coverage);
		lextrevIO.setRider(sv.rider);
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.oK)
		&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), lextrevIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), lextrevIO.getChdrnum())
		|| isNE(sv.life, lextrevIO.getLife())
		|| isNE(sv.coverage, lextrevIO.getCoverage())
		|| isNE(sv.rider, lextrevIO.getRider())) {
			lextrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextrevIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
		/* Extra check in here to see if any LEXT records associated with  */
		/*  this component have been amended whilst we have been here in   */
		/*  component Add/Modify - check TRANNOs on LEXTREV records        */
		/*  against the current TRANNO held on the Contract Header we      */
		/*  are currently using in the CHDRMJA logical.                    */
		if (isNE(lextrevIO.getStatuz(), varcom.endp)) {
			if (isGT(lextrevIO.getTranno(), chdrpf.getTranno())) {
				wsaaLextUpdates.set("Y");
				lextrevIO.setStatuz(varcom.endp);
			}
		}
		while ( !(isEQ(lextrevIO.getStatuz(), varcom.endp))) {
			lextrevIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, lextrevIO);
			if (isNE(lextrevIO.getStatuz(), varcom.oK)
			&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(lextrevIO.getParams());
				syserrrec.statuz.set(lextrevIO.getStatuz());
				fatalError600();
			}
			if (isNE(chdrpf.getChdrcoy(), lextrevIO.getChdrcoy())
			|| isNE(chdrpf.getChdrnum(), lextrevIO.getChdrnum())
			|| isNE(sv.life, lextrevIO.getLife())
			|| isNE(sv.coverage, lextrevIO.getCoverage())
			|| isNE(sv.rider, lextrevIO.getRider())) {
				lextrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isGT(lextrevIO.getTranno(), chdrpf.getTranno())) {
					wsaaLextUpdates.set("Y");
					lextrevIO.setStatuz(varcom.endp);
				}
			}
		}
		
	}

	/**
	* <pre>
	*1950-CHECK-RACT SECTION.                                 <R96REA>
	*1950-READ-RACT.                                          <R96REA>
	****                                                      <R96REA>
	**** MOVE 'N'                    TO WSAA-RACT-UPDATES.    <R96REA>
	**** MOVE SPACES                 TO RACT-PARAMS.          <R96REA>
	**** MOVE CHDRMJA-CHDRCOY        TO RACT-CHDRCOY.         <R96REA>
	**** MOVE CHDRMJA-CHDRNUM        TO RACT-CHDRNUM.         <R96REA>
	**** MOVE SR575-LIFE             TO RACT-LIFE.            <R96REA>
	**** MOVE SR575-COVERAGE         TO RACT-COVERAGE.        <R96REA>
	**** MOVE SR575-RIDER            TO RACT-RIDER.           <R96REA>
	**** MOVE RACTREC                TO RACT-FORMAT.          <R96REA>
	**** MOVE BEGN                   TO RACT-FUNCTION.        <R96REA>
	**** CALL 'RACTIO' USING RACT-PARAMS.                     <R96REA>
	**** IF RACT-STATUZ              NOT = O-K AND            <R96REA>
	****                             NOT = ENDP               <R96REA>
	****    MOVE RACT-PARAMS         TO SYSR-PARAMS           <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	****                                                      <R96REA>
	**** IF CHDRMJA-CHDRCOY   NOT = RACT-CHDRCOY              <R96REA>
	****  OR CHDRMJA-CHDRNUM  NOT = RACT-CHDRNUM              <R96REA>
	****  OR SR575-LIFE       NOT = RACT-LIFE                 <R96REA>
	****  OR SR575-COVERAGE   NOT = RACT-COVERAGE             <R96REA>
	****  OR SR575-RIDER      NOT = RACT-RIDER                <R96REA>
	****    MOVE ENDP                TO RACT-STATUZ.          <R96REA>
	**** IF RACT-STATUZ              = ENDP                   <R96REA>
	****    MOVE ' '                 TO SR575-RATYPIND        <R96REA>
	**** ELSE                                                 <R96REA>
	****    MOVE '+'                 TO SR575-RATYPIND.       <R96REA>
	* Extra check in here to see if any RACT records associated with  
	*  this component have been amended whilst we have been here in   
	*  component Add/Modify - check TRANNOs on RACTRCC records        
	*  against the current TRANNO held on the Contract Header we      
	*  are currently using in the CHDRMJA logical.                    
	****                                                      <R96REA>
	**** IF RACT-STATUZ              NOT = ENDP               <R96REA>
	****     IF RACT-TRANNO          > CHDRMJA-TRANNO         <R96REA>
	****         MOVE 'Y'            TO WSAA-RACT-UPDATES     <R96REA>
	****         MOVE ENDP           TO RACT-STATUZ           <R96REA>
	****         GO TO 1959-EXIT                              <R96REA>
	****     END-IF                                           <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	**** MOVE SPACES                 TO RACTRCC-PARAMS.       <R96REA>
	**** MOVE CHDRMJA-CHDRCOY        TO RACTRCC-CHDRCOY.      <R96REA>
	**** MOVE CHDRMJA-CHDRNUM        TO RACTRCC-CHDRNUM.      <R96REA>
	**** MOVE SR575-LIFE             TO RACTRCC-LIFE.         <R96REA>
	**** MOVE SR575-COVERAGE         TO RACTRCC-COVERAGE.     <R96REA>
	**** MOVE SR575-RIDER            TO RACTRCC-RIDER.        <R96REA>
	**** MOVE SPACES                 TO RACTRCC-RASNUM.       <R96REA>
	**** MOVE SPACES                 TO RACTRCC-RATYPE.       <R96REA>
	**** MOVE SPACES                 TO RACTRCC-VALIDFLAG.    <R96REA>
	**** MOVE RACTRCCREC             TO RACTRCC-FORMAT.       <R96REA>
	**** MOVE BEGN                   TO RACTRCC-FUNCTION.     <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTRCC-STATUZ           NOT = O-K                <R96REA>
	****    AND RACTRCC-STATUZ       NOT = ENDP               <R96REA>
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS           <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	****                                                      <R96REA>
	**** IF CHDRMJA-CHDRCOY          NOT = RACTRCC-CHDRCOY    <R96REA>
	****  OR CHDRMJA-CHDRNUM         NOT = RACTRCC-CHDRNUM    <R96REA>
	****  OR SR575-LIFE              NOT = RACTRCC-LIFE       <R96REA>
	****  OR SR575-COVERAGE          NOT = RACTRCC-COVERAGE   <R96REA>
	****  OR SR575-RIDER             NOT = RACTRCC-RIDER      <R96REA>
	****     MOVE ENDP               TO RACTRCC-STATUZ        <R96REA>
	****  END-IF.                                             <R96REA>
	****                                                      <R96REA>
	**** PERFORM                     UNTIL RACTRCC-STATUZ = EN<R96REA>
	****                                                      <R96REA>
	****     MOVE NEXTR              TO RACTRCC-FUNCTION      <R96REA>
	****                                                      <R96REA>
	****     CALL 'RACTRCCIO'        USING RACTRCC-PARAMS     <R96REA>
	****                                                      <R96REA>
	****     IF RACTRCC-STATUZ       NOT = O-K                <R96REA>
	****        AND RACTRCC-STATUZ   NOT = ENDP               <R96REA>
	****         MOVE RACTRCC-PARAMS TO SYSR-PARAMS           <R96REA>
	****         MOVE RACTRCC-STATUZ TO SYSR-STATUZ           <R96REA>
	****         PERFORM 600-FATAL-ERROR                      <R96REA>
	****     END-IF                                           <R96REA>
	****                                                      <R96REA>
	****     IF CHDRMJA-CHDRCOY      NOT = RACTRCC-CHDRCOY    <R96REA>
	****      OR CHDRMJA-CHDRNUM     NOT = RACTRCC-CHDRNUM    <R96REA>
	****      OR SR575-LIFE          NOT = RACTRCC-LIFE       <R96REA>
	****      OR SR575-COVERAGE      NOT = RACTRCC-COVERAGE   <R96REA>
	****      OR SR575-RIDER         NOT = RACTRCC-RIDER      <R96REA>
	****        MOVE ENDP            TO RACTRCC-STATUZ        <R96REA>
	****     ELSE                                             <R96REA>
	****         IF RACTRCC-TRANNO   > CHDRMJA-TRANNO         <R96REA>
	****             MOVE 'Y'        TO WSAA-RACT-UPDATES     <R96REA>
	****             MOVE ENDP       TO RACTRCC-STATUZ        <R96REA>
	****         END-IF                                       <R96REA>
	****     END-IF                                           <R96REA>
	****                                                      <R96REA>
	**** END-PERFORM.                                         <R96REA>
	****                                                      <R96REA>
	*1959-EXIT.                                               <R96REA>
	**** EXIT.                                                <R96REA>
	* </pre>
	*/
protected void checkAgnt1960()
	{
		readPcdtmja1960();
	}

protected void readPcdtmja1960()
	{
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(covrpf.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			sv.comind.set(" ");
		}
		else {
			sv.comind.set("+");
		}
	}

	/**
	* <pre>
	*1970-PAYR-LOAD SECTION.                                     <033>
	************************                                     <033>
	*                                                            <033>
	*1972-OBTAIN-CLNTNUM.                                        <033>
	*                                                            <033>
	*****MOVE SPACES                 TO CLRF-DATA-AREA.          <033>
	*****MOVE CLRFREC                TO CLRF-FORMAT.             <033>
	*****MOVE READR                  TO CLRF-FUNCTION.           <033>
	*****                                                        <033>
	*****MOVE 'CH'                   TO CLRF-FOREPFX.            <033>
	*****MOVE CHDRMJA-CHDRCOY        TO CLRF-FORECOY.            <033>
	*****MOVE CHDRMJA-CHDRNUM        TO WSAA-FORENUM.            <033>
	*****MOVE COVRMJA-PAYRSEQNO      TO WSAA-SEQNUM.             <033>
	*****MOVE WSAA-CLRF-FORENUM      TO CLRF-FORENUM.            <033>
	*****MOVE 'PY'                   TO CLRF-CLRRROLE.           <033>
	*                                                            <033>
	*****CALL 'CLRFIO'            USING CLRF-PARAMS.             <033>
	*                                                            <033>
	*****IF CLRF-STATUZ             NOT = O-K                    <033>
	*****   MOVE CLRF-STATUZ         TO SYSR-STATUZ              <033>
	*****   MOVE CLRF-PARAMS         TO SYSR-PARAMS              <033>
	*****   PERFORM 600-FATAL-ERROR                              <033>
	**** END-IF.                                                 <033>
	*                                                            <033>
	*1973-OBTAIN-CLNTNAME.                                       <033>
	*                                                            <033>
	*****MOVE SPACES                 TO CLTS-DATA-AREA.          <033>
	*****MOVE CLTSREC                TO CLTS-FORMAT.             <033>
	*****MOVE READR                  TO CLTS-FUNCTION.           <033>
	*                                                            <033>
	*****MOVE CLRF-CLNTPFX           TO CLTS-CLNTPFX.            <033>
	*****MOVE CLRF-CLNTCOY           TO CLTS-CLNTCOY.            <033>
	*****MOVE CLRF-CLNTNUM           TO CLTS-CLNTNUM.            <033>
	*                                                            <033>
	*****CALL 'CLTSIO'            USING CLTS-PARAMS.             <033>
	*                                                            <033>
	*****IF CLTS-STATUZ             NOT = O-K                    <033>
	*****   MOVE CLTS-STATUZ         TO SYSR-STATUZ              <033>
	*****   MOVE CLTS-PARAMS         TO SYSR-PARAMS              <033>
	*****   PERFORM 600-FATAL-ERROR                              <033>
	*****END-IF.                                                 <033>
	*                                                            <033>
	*1979-EXIT.                                                  <033>
	**** EXIT.                                                   <033>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax5800();
		}
		/*    If single premium flag set then output message.              */
		/* IF SINGLE-PREMIUM                                    <CAS1.0>*/
		/*    MOVE F008                TO SCRN-ERROR-CODE.      <CAS1.0>*/
		if (singlePremium.isTrue()) {
			sv.comindOut[varcom.pr.toInt()].set("Y");
			/*                                SR575-OPTEXTIND-OUT (PR)      */
			sv.matageOut[varcom.pr.toInt()].set("N");
			sv.mattrmOut[varcom.pr.toInt()].set("N");
			sv.pcessageOut[varcom.pr.toInt()].set("N");
			sv.pcesstrmOut[varcom.pr.toInt()].set("N");
			sv.mortclsOut[varcom.pr.toInt()].set("N");
			wszzAnbAtCcd.set(sv.anbAtCcd);
		}
		wsaaOldSumins.set(sv.sumin);
		wsaaOldPrem.set(sv.singlePremium);
		/*MOVE 'SUMIN     '           TO SCRN-POSITION-CURSOR.         */
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
				case beforeExit2080: 
					beforeExit2080();
				case redisplay2080: 
					redisplay2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'SR575IO' USING SCRN-SCREEN-PARAMS                      */
		/*                                SR575-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If CF11 (KILL) is requested skip all validation.*/
		wsaaIfSuminsChanged.set("N");
		wsaaIfPremChanged.set("N");
		if (anotherFpco.isTrue()) {
			wsspcomn.edterror.set("Y");
			sv.singprmErr.set(errorsInner.d041);
			goTo(GotoLabel.redisplay2080);
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(payrIO.getBtdate(), wsspcomn.currfrom)) {
			sv.chdrnumErr.set(errorsInner.f990);
			goTo(GotoLabel.beforeExit2080);
		}
		if (singlePremium.isTrue()) {
			wsaaFlag.set("M");
		}
		/*    IF  NOT COMP-ENQUIRY                                         */
		/*    PERFORM 2900-REVIEW-PAYER                           <048>*/
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			validateScreen2100();
		}
		/* If  options/extras already exist, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed. If*/
		/* options  and  extras  are  requested,  DO  NOT  CALCULATE THE*/
		/* PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		if (isNE(sv.comind, " ")
		&& isNE(sv.comind, "+")
		&& isNE(sv.comind, "X")) {
			sv.comindErr.set(errorsInner.g620);
		}
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		/*If  reassurance   already exists, there will be a '+' in<R96REA>*/
		/*field.  A  request  to access the details is made by ent<R96REA>*/
		/*'X'.  No  other  values  (other  than  blank) are allowe<R96REA>*/
		/* IF SR575-RATYPIND           NOT = ' ' AND            <R96REA>*/
		/*                             NOT = '+' AND            <R96REA>*/
		/*                             NOT = 'X'                <R96REA>*/
		/*    MOVE G620                TO SR575-RATYPIND-ERR.   <R96REA>*/
		/* If Component Enquiry then skip validation.                      */
		if (compEnquiry.isTrue()) {
			if (compEnquiry.isTrue()
			|| compApp.isTrue()
			|| revComp.isTrue()) {
				goTo(GotoLabel.beforeExit2080);
			}
		}
		/* If sum assured is changed and reassurance details exist force   */
		/* to go onto the reassuranc screen,to check if sum assured is     */
		/* still greater then the amount to be reassured ..                */
		/* IF MODIFY-COMP                                       <R96REA>*/
		/*  IF SR575-RATYPIND               = '+'               <R96REA>*/
		/*    IF SR575-SUMIN           NOT = WSAA-OLD-SUMINS    <R96REA>*/
		/*       MOVE 'X'              TO SR575-RATYPIND.       <R96REA>*/
		if (isNE(sv.sumin, wsaaOldSumins)) {
			compute(wsaaSuminsDiff, 2).set(sub(sv.sumin, wsaaOldSumins));
			if (isLT(wsaaSuminsDiff, 0)) {
				compute(wsaaSuminsDiff, 2).set(mult(wsaaSuminsDiff, -1));
				wsaaIfSuminsChanged.set("D");
			}
			else {
				wsaaIfSuminsChanged.set("I");
			}
		}
		if (isNE(sv.singlePremium, wsaaOldPrem)) {
			compute(wsaaPremDiff, 2).set(sub(sv.singlePremium, wsaaOldPrem));
			if (isLT(wsaaPremDiff, 0)) {
				compute(wsaaPremDiff, 2).set(mult(wsaaPremDiff, -1));
				wsaaIfPremChanged.set("D");
			}
			else {
				wsaaIfPremChanged.set("I");
			}
		}
		/* If everything else is O-K calculate the Instalment Premium.     */
		if (isNE(sv.optextind, "X")
		&& isNE(sv.comind, "X")
		&& isEQ(sv.errorIndicators, SPACES)
		&& addComp.isTrue()) {
			calcPremium2700();
		}
		if (isNE(sv.optextind, "X")
		&& isNE(sv.comind, "X")
		&& isEQ(sv.errorIndicators, SPACES)
		&& modifyComp.isTrue()) {
			calcPremium2700();
		}
		getPrmh5700();
		if (isNE(sv.singlePremium, wsaaLinstamt)
		&& isEQ(wsaaInPh, "Y")
		&& isEQ(tr51prec.premind, "N")) {
			sv.singprmErr.set(errorsInner.rlbq);
		}
	}

protected void beforeExit2080()
	{
		/*    IF  SR575-OPTEXTIND         NOT = 'X' AND                    */
		/*        SR575-COMIND            NOT = 'X'                        */
		/*       IF TR52D-TXCODE             NOT = SPACE                   */
		/*          PERFORM 5800-CHECK-CALC-TAX                            */
		/*       END-IF                                                    */
		/*    END-IF.                                                      */
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.redisplay2080);
		}
		/* If 'CALC' was entered then re-display the screen.*/
		if (isNE(scrnparams.statuz, varcom.calc)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void redisplay2080()
	{
		wsspcomn.edterror.set("Y");
	}

protected void validateScreen2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkSumin2525();
					if(NBPRP056Permission) {
						validateOccupationOrOccupationClass();
					}
				case checkSuminCont2526: 
					checkSuminCont2526();
				case checkLumpSum2527: 
					checkLumpSum2527();
				case checkMaturityFields2530: 
					checkMaturityFields2530();
				case checkMatAgeTerm2540: 
					checkMatAgeTerm2540();
				case checkPremAgeTerm2540: 
					checkPremAgeTerm2540();
				case cont: 
					cont();
				case validDate2541: 
					validDate2541();
				case datesCont2541: 
					datesCont2541();
				case ageAnniversary2541: 
					ageAnniversary2541();
				case check2542: 
					check2542();
				case check2543: 
					check2543();
				case checkOccurance2545: 
					checkOccurance2545();
				case checkTermFields2550: 
					checkTermFields2550();
				case checkComplete2555: 
					checkComplete2555();
				case checkMortcls2560: 
					checkMortcls2560();
				case loop2565: 
					loop2565();
				case checkLiencd2570: 
					checkLiencd2570();
				case loop2575: 
					loop2575();
				case checkMore2580: 
					checkMore2580();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*2110-LOAD-POLICY.                                                
	* Before  the  premium amount is calculated, the screen must be
	* valid.  So  all  editing  is  completed before the premium is
	* calculated.
	*  1) Check  the  sum-assured,  if  applicable, against the
	*       limits (NB. these apply to the plan, so adjust first).
	*    - if only one sum insured is allowed, re-calculate plan
	*      level sum insured (if applicable).
	**** IF T5551-SUM-INS-MAX        = T5551-SUM-INS-MIN              
	****                        AND  NOT = 0                          
	****    IF PLAN                                                   
	****       COMPUTE SR575-SUMIN ROUNDED = (T5551-SUM-INS-MIN       
	****                             * CHDRMJA-POLSUM                 
	****                             / SR575-POLINC).                 
	* </pre>
	*/
protected void checkSumin2525()
	{
		a100CheckLimit();
		if (isEQ(sv.sumin, covrpf.getSumins())) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkSuminCont2526);
		}
		if (plan.isTrue()
		&& isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(wsaaSumin, 3).setRounded((div(mult(sv.sumin, sv.polinc), chdrpf.getPolsum())));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
	}

protected void checkSuminCont2526()
	{
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)) {
			goTo(GotoLabel.checkLumpSum2527);
		}
		if (isLT(wsaaSumin, t5551rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin, t5551rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

protected void checkLumpSum2527()
	{
		/*  1a) Check  the  Lump-Sum,  if  applicable, against the*/
		/*       limits (NB. these apply to the plan, so adjust first).*/
		/*    - if only one Lump Sum is allowed, re-calculate plan*/
		/*      level Lump Sum (if applicable).*/
		/* IF T5551-MAX-LUMP-SUM       = T5551-MIN-LUMP-SUM             */
		/*                        AND  NOT = 0                          */
		/*    IF PLAN                                                   */
		/*      AND                                                     */
		/*    SR575-PLAN-SUFFIX  NOT > CHDRMJA-POLSUM                   */
		/*       COMPUTE SR575-LUMP-CONTRIB ROUNDED                     */
		/*                             = (T5551-MIN-LUMP-SUM            */
		/*                             * CHDRMJA-POLSUM                 */
		/*                             / SR575-POLINC).                 */
		/* IF PLAN                                                      */
		/*      AND                                                     */
		/*    SR575-PLAN-SUFFIX  NOT > CHDRMJA-POLSUM                   */
		/*    COMPUTE WSAA-LUMP-SUM  ROUNDED                            */
		/*                               = (SR575-LUMP-CONTRIB          */
		/*                                  * SR575-POLINC              */
		/*                                  / CHDRMJA-POLSUM)           */
		/* ELSE                                                         */
		/*    MOVE SR575-LUMP-CONTRIB  TO WSAA-LUMP-SUM.                */
		if (isEQ(t5551rec.maxLumpSum, t5551rec.minLumpSum)) {
			goTo(GotoLabel.checkMaturityFields2530);
		}
	}

	/**
	* <pre>
	**** IF WSAA-LUMP-SUM            < T5551-MIN-LUMP-SUM             
	****     MOVE H369               TO SR575-LMPCNT-ERR.        <004>
	**** IF WSAA-LUMP-SUM            > T5551-MAX-LUMP-SUM             
	****     MOVE H368               TO SR575-LMPCNT-ERR.        <004>
	*  2) Check the consistency of the risk age and term fields
	*       and  premium  age and term fields. Either, risk age
	*       and  premium age must be used or risk term and
	*       premium  term  must  be  used.  They  must  not  be
	*       combined. Note that  these  only need validating if
	*       they were not defaulted.
	*     NOTE: Age and Term fields may now be mixed.                 
	*2530-CHECK-MATURITY-FIELDS.                                      
	*    IF SR575-MATAGE      > 0 AND                                 
	*       SR575-MATTRM     > 0                                      
	*        MOVE F220               TO SR575-MATAGE-ERR              
	*                                   SR575-MATTRM-ERR.             
	*    IF T5551-EAAGE              NOT = SPACE AND                  
	*       SR575-MATAGE      = 0 AND                                 
	*       SR575-MATTRM     = 0                                      
	*        MOVE E560               TO SR575-MATAGE-ERR              
	*                                   SR575-MATTRM-ERR.             
	*2535-CHECK-PCESS-FIELDS.                                         
	*    IF SR575-PREM-CESS-AGE      > 0 AND                          
	*       SR575-PREM-CESS-TERM     > 0                              
	*        MOVE F220               TO SR575-PCESSAGE-ERR            
	*                                   SR575-PCESSTRM-ERR.           
	*    IF SR575-PREM-CESS-AGE     = 0 AND                           
	*       SR575-PREM-CESS-TERM    = 0 AND                           
	*       SR575-MATAGE-ERR      = SPACES AND                        
	*       SR575-MATTRM-ERR      = SPACES                            
	*        MOVE SR575-MATAGE TO SR575-PREM-CESS-AGE                 
	*        MOVE SR575-MATTRM TO SR575-PREM-CESS-TERM.               
	*    IF T5551-EAAGE              NOT = SPACE AND                  
	*       SR575-PREM-CESS-AGE      = 0 AND                          
	*       SR575-PREM-CESS-TERM     = 0                              
	*        MOVE E560               TO SR575-PCESSAGE-ERR            
	*                                   SR575-PCESSTRM-ERR.           
	*2540-CHECK-AGE-TERM.                                             
	*    IF (SR575-MATAGE-ERR      NOT = SPACES) OR                   
	*       (SR575-MATTRM-ERR      NOT = SPACES) OR                   
	*       (SR575-PCESSAGE-ERR      NOT = SPACES) OR                 
	*       (SR575-PCESSTRM-ERR      NOT = SPACES)                    
	*        GO TO 2560-CHECK-MORTCLS.                                
	*    IF SR575-MATAGE      > 0 AND                                 
	*       SR575-PREM-CESS-AGE      = 0                              
	*        MOVE F224               TO SR575-PCESSAGE-ERR.           
	*    IF SR575-MATTRM     > 0 AND                                  
	*       SR575-PREM-CESS-TERM     = 0                              
	*        MOVE F225               TO SR575-PCESSTRM-ERR.           
	* </pre>
	*/
protected void checkMaturityFields2530()
	{
		if (isNE(t5551rec.eaage, SPACES)
		&& isEQ(sv.matage, 0)
		&& isEQ(sv.mattrm, 0)) {
			sv.matageErr.set(errorsInner.e560);
			sv.mattrmErr.set(errorsInner.e560);
		}
		if (isEQ(isGT(sv.matage, 0), true)
		&& isEQ(isGT(sv.mattrm, 0), false)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), false)){
			goTo(GotoLabel.checkMatAgeTerm2540);
		}
		else if (isEQ(isGT(sv.matage, 0), false)
		&& isEQ(isGT(sv.mattrm, 0), true)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), false)){
			goTo(GotoLabel.checkMatAgeTerm2540);
		}
		else if (isEQ(isGT(sv.matage, 0), false)
		&& isEQ(isGT(sv.mattrm, 0), false)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), true)){
			goTo(GotoLabel.checkMatAgeTerm2540);
		}
		else if (isEQ(isGT(sv.matage, 0), true)
		&& isEQ(isGT(sv.mattrm, 0), false)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), true)){
			mattcess1500();
			/***         IF WSAA-DATE        NOT = SR575-MATTCESS               */
			/***            MOVE WSAA-DATE          TO SR575-MATTCESS   <CAS1.0>*/
			/***            MOVE F220               TO SR575-MATAGE-ERR <CAS1.0>*/
			/***                                       SR575-MATTRM-ERR <CAS1.0>*/
			/***                                       SR575-MATTCESS-ER<CAS1.0>*/
			/***         END-IF                                                 */
		}
		else if (isEQ(isGT(sv.matage, 0), false)
		&& isEQ(isGT(sv.mattrm, 0), true)
		&& isEQ(isNE(sv.mattcess, varcom.vrcmMaxDate), true)){
			mattcess1500();
			/***         IF WSAA-DATE        NOT = SR575-MATTCESS       <CAS1.0>*/
			/***            MOVE WSAA-DATE          TO SR575-MATTCESS   <CAS1.0>*/
			/***            MOVE F220               TO SR575-MATAGE-ERR <CAS1.0>*/
			/***                                       SR575-MATTRM-ERR <CAS1.0>*/
			/***                                       SR575-MATTCESS-ER<CAS1.0>*/
			/***         END-IF                                                 */
		}
		else{
			/* More than one field is entered.                                 */
			sv.matageErr.set(errorsInner.f220);
			sv.mattrmErr.set(errorsInner.f220);
			sv.mattcessErr.set(errorsInner.f220);
		}
	}

protected void checkMatAgeTerm2540()
	{
		if (isNE(t5551rec.eaage, SPACES)
		&& isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
		if (isEQ(isGT(sv.premCessAge, 0), true)
		&& isEQ(isGT(sv.premCessTerm, 0), false)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), false)){
			goTo(GotoLabel.checkPremAgeTerm2540);
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), true)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), false)){
			goTo(GotoLabel.checkPremAgeTerm2540);
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), false)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), true)){
			goTo(GotoLabel.checkPremAgeTerm2540);
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), false)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), false)){
			sv.premCessAge.set(sv.matage);
			sv.premCessTerm.set(sv.mattrm);
			sv.premcess.set(sv.mattcess);
		}
		else if (isEQ(isGT(sv.premCessAge, 0), true)
		&& isEQ(isGT(sv.premCessTerm, 0), false)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), true)){
			premcess1550();
			/***         IF WSAA-DATE        NOT = SR575-PREMCESS               */
			/***            MOVE WSAA-DATE          TO SR575-PREMCESS   <CAS1.0>*/
			/***            MOVE F220               TO SR575-PCESSAGE-ER<CAS1.0>*/
			/***                                       SR575-PCESSTRM-ER<CAS1.0>*/
			/***                                       SR575-PREMCESS-ER<CAS1.0>*/
			/***         END-IF                                                 */
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), true)
		&& isEQ(isNE(sv.premcess, varcom.vrcmMaxDate), true)){
			/*       MOVE SR575-MATTCESS TO WSAA-DATE               <LA1208>*/
			/*       PERFORM 1500-MATTCESS                          <LA1208>*/
			premcess1550();
			/***         IF WSAA-DATE        NOT = SR575-MATTCESS               */
			/***            MOVE WSAA-DATE          TO SR575-PREMCESS   <CAS1.0>*/
			/***            MOVE F220               TO SR575-PCESSAGE-ER<CAS1.0>*/
			/***                                       SR575-PCESSTRM-ER<CAS1.0>*/
			/***                                       SR575-PREMCESS-ER<CAS1.0>*/
			/***         END-IF                                                 */
		}
		else{
			/* More than one field is entered.                                 */
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
			sv.premcessErr.set(errorsInner.f220);
		}
	}

protected void checkPremAgeTerm2540()
	{
		if ((isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.matageErr, SPACES))
		|| (isNE(sv.mattrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*    THIS SECTION CALCULATES THE NEW PREMIUM PAYMENT AND*/
		/*    COMPARES THE MANDATE AMOUT WITH IT.*/
		/*    THE FORMULA USED IS AS FOLLOWS.*/
		/*    NP = OP - (CP - SP).*/
		/*  WHERE :*/
		/*    NP = NEW PREMIUM (TOTAL)*/
		/*    OP = OLD PREMIUM (TOTAL)*/
		/*    CP = OLD SINGLE PREMIUM*/
		/*    SP = NEW SINGLE PREMIUM*/
		/*IF CHDRMJA-BILLCHNL         NOT = 'B'                   <025>*/
		/*IF PAYR-BILLCHNL            NOT = 'B'                   <052>*/
		/*  GO TO CONT.                                          <052>*/
		if (isLTE(payrIO.getMandref(), SPACES)) {
			goTo(GotoLabel.cont);
		}
		if (isNE(chdrpf.getPayrnum(), SPACES)) {
			mandIO.setPayrnum(chdrpf.getPayrnum());
			mandIO.setPayrcoy(chdrpf.getPayrcoy());
			/*********MOVE CHDRMJA-MANDREF     TO MAND-MANDREF             <025>*/
		}
		else {
			mandIO.setPayrnum(chdrpf.getCownnum());
			mandIO.setPayrcoy(chdrpf.getCowncoy());
			mandIO.setPayrcoy(chdrpf.getCowncoy());
		}
		/*    MOVE CHDRMJA-COWNCOY     TO MAND-PAYRCOY             <025>*/
		/*MOVE CHDRMJA-MANDREF     TO MAND-MANDREF.            <025>*/
		/* MOVE PAYR-MANDREF        TO MAND-MANDREF                <046>*/
		mandIO.setMandref(payrIO.getMandref());
		mandIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			syserrrec.statuz.set(mandIO.getStatuz());
			fatalError600();
		}
		if (isEQ(mandIO.getMandAmt(), ZERO)) {
			goTo(GotoLabel.cont);
		}
		/*IF   CHDRMJA-BILLFREQ > 0                               <025>*/
		if (isGT(payrIO.getBillfreq(), 0)) {
			compute(wsaaCalcPrem, 2).set(sub(covrpf.getInstprem(), (sub(payrIO.getSinstamt06(), sv.singlePremium))));
		}
		if (isNE(mandIO.getMandAmt(), wsaaCalcPrem)) {
			sv.singprmErr.set(errorsInner.ev01);
			wsspcomn.edterror.set("Y");
		}
	}

protected void cont()
	{
		/*  To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (T5551).*/
		/*IF SR575-RESERVE-UNITS-DATE NOT = 0                          */
		/*   IF  SR575-RESERVE-UNITS-DATE < CHDRMJA-OCCDATE            */
		/*       MOVE H359 TO SR575-RUNDTE-ERR.                        */
		/*    IF T5551-EAAGE NOT = SPACES                                  */
		/*      OR SR575-MATTCESS = VRCM-MAX-DATE                          */
		/*       PERFORM 1500-MATTCESS                                     */
		/*    ELSE                                                         */
		/*       IF SR575-MATTCESS-ERR = SPACES                            */
		/*          IF SR575-MATAGE NOT = 0                                */
		/*             MOVE WSZZ-CLTDOB   TO DTC2-INT-DATE-1               */
		/*             MOVE SR575-MATAGE                                   */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*             PERFORM 1600-CALL-DATCON2                           */
		/*                IF DTC2-INT-DATE-2 < SR575-MATTCESS              */
		/*                   MOVE U029    TO SR575-MATAGE-ERR              */
		/*                                   SR575-MATTCESS-ERR            */
		/*                ELSE                                             */
		/*                   SUBTRACT 1   FROM DTC2-FREQ-FACTOR            */
		/*                   PERFORM 1600-CALL-DATCON2                     */
		/*                   IF DTC2-INT-DATE-2 NOT < SR575-MATTCESS       */
		/*                      MOVE U029 TO SR575-MATAGE-ERR              */
		/*                                   SR575-MATTCESS-ERR            */
		/*                   ELSE                                          */
		/*                      NEXT SENTENCE                              */
		/*          ELSE                                                   */
		/*             IF SR575-MATTRM NOT = 0                             */
		/*                IF NOT ADD-COMP                                  */
		/*                   MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1   */
		/*                ELSE                                             */
		/*                   MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1   */
		/*                MOVE SR575-MATTRM                                */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*                PERFORM 1600-CALL-DATCON2                        */
		/*                   IF DTC2-INT-DATE-2 < SR575-MATTCESS           */
		/*                      MOVE U029 TO SR575-MATTRM-ERR              */
		/*                                   SR575-MATTCESS-ERR            */
		/*                   ELSE                                          */
		/*                      SUBTRACT 1 FROM DTC2-FREQ-FACTOR           */
		/*                      PERFORM 1600-CALL-DATCON2                  */
		/*                      IF DTC2-INT-DATE-2                         */
		/*                                   NOT < SR575-MATTCESS          */
		/*                         MOVE U029 TO SR575-MATTRM-ERR           */
		/*                                      SR575-MATTCESS-ERR.        */
		/*    IF T5551-EAAGE NOT = SPACES                                  */
		/*      OR SR575-PREMCESS = VRCM-MAX-DATE                          */
		/*       PERFORM 1550-PREMCESS                                     */
		/*    ELSE                                                         */
		/*       IF SR575-PREMCESS-ERR = SPACES                            */
		/*          IF SR575-PREM-CESS-AGE NOT = 0                         */
		/*             MOVE WSZZ-CLTDOB   TO DTC2-INT-DATE-1               */
		/*             MOVE SR575-PREM-CESS-AGE                            */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*             PERFORM 1600-CALL-DATCON2                           */
		/*                IF DTC2-INT-DATE-2 < SR575-PREMCESS              */
		/*                   MOVE U029    TO SR575-PCESSAGE-ERR            */
		/*                                   SR575-PREMCESS-ERR            */
		/*                ELSE                                             */
		/*                   SUBTRACT 1   FROM DTC2-FREQ-FACTOR            */
		/*                   PERFORM 1600-CALL-DATCON2                     */
		/*                   IF DTC2-INT-DATE-2 NOT < SR575-PREMCESS       */
		/*                      MOVE U029 TO SR575-PCESSAGE-ERR            */
		/*                                   SR575-PREMCESS-ERR            */
		/*                   ELSE                                          */
		/*                      NEXT SENTENCE                              */
		/*          ELSE                                                   */
		/*             IF SR575-PREM-CESS-TERM NOT = 0                     */
		/*                IF NOT ADD-COMP                                  */
		/*                   MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1   */
		/*                ELSE                                             */
		/*                   MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1   */
		/*                MOVE SR575-PREM-CESS-TERM                        */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*                PERFORM 1600-CALL-DATCON2                        */
		/*                   IF DTC2-INT-DATE-2 < SR575-PREMCESS           */
		/*                      MOVE U029 TO SR575-PCESSTRM-ERR            */
		/*                                   SR575-PREMCESS-ERR            */
		/*                   ELSE                                          */
		/*                      SUBTRACT 1   FROM DTC2-FREQ-FACTOR         */
		/*                      PERFORM 1600-CALL-DATCON2                  */
		/*                      IF DTC2-INT-DATE-2                         */
		/*                                NOT < SR575-PREMCESS             */
		/*                         MOVE U029 TO SR575-PCESSTRM-ERR         */
		/*                                      SR575-PREMCESS-ERR.        */
		if (isEQ(sv.mattcess, varcom.vrcmMaxDate)) {
			mattcess1500();
		}
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			premcess1550();
		}
		if (isNE(sv.mattcessErr, SPACES)
		|| isNE(sv.premcessErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2560);
		}
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			sv.premcess.set(sv.mattcess);
		}
		if (isGT(sv.premcess, sv.mattcess)) {
			sv.premcessErr.set(errorsInner.e566);
			sv.mattcessErr.set(errorsInner.e566);
		}
		if (isEQ(sv.rider, "00")) {
			goTo(GotoLabel.datesCont2541);
		}
		/* Read the Temporary COVT file for the Coverage record which must */
		/* have been written before a rider can be selected.               */
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setCoverage(covrpf.getCoverage());
		covtmjaIO.setRider("00");
		covtmjaIO.setSeqnbr(ZERO);
		covtmjaIO.setPlanSuffix(wsaaPlanSuffix);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(covtmjaIO.getLife(), covrpf.getLife())
		|| isNE(covtmjaIO.getCoverage(), covrpf.getCoverage())
		|| isNE(covtmjaIO.getRider(), "00")) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtmjaIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.validDate2541);
		}
		/* Read the Temporary COVT file for the Coverage record which must */
		/* have been written before a rider can be selected.               */
		/* if a policy that was summarised was selected a                  */
		/* the plan-suffix was in correct (fell over because               */
		/* could not find a record with that plan-suffix)                  */
		/* need to access the summaried reccord (plan-suffix of 0)         */
		if (modifyComp.isTrue()) {
			if (isEQ(chdrpf.getPolsum(), ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isGT(chdrpf.getPolsum(), covrpf.getPlanSuffix())
				|| isEQ(chdrpf.getPolsum(), covrpf.getPlanSuffix())) {
					covrpf.setPlanSuffix(0);
				}
			}
		}
		/*      MOVE  '0'                TO COVRMJA-PLAN-SUFFIX<032>*/
		if (addComp.isTrue()) {
			if (isEQ(chdrpf.getPolsum(), ZERO)) {
				/*   MOVE '1'                    TO COVRMJA-PLAN-SUFFIX<032>*/
				covrpf.setPlanSuffix(1);
			}
			else {
				/*   MOVE '0'                    TO COVRMJA-PLAN-SUFFIX<032>*/
				covrpf.setPlanSuffix(0);
			}
		}
		if (isGT(chdrpf.getPolsum(), covrpf.getPlanSuffix())
		|| isEQ(chdrpf.getPolsum(), covrpf.getPlanSuffix())) {
			/*   MOVE '0'                    TO COVRMJA-PLAN-SUFFIX<032>*/
			covrpf.setPlanSuffix(0);
		}
		covrpf.setRider("00");
		List<String> tempList = new ArrayList();
		tempList.add(covrpf.getChdrnum());
		covrList = covrpfDAO.searchCovrRecord(covrpf.getChdrcoy(),tempList);
		tempList.clear();
		for(Covrpf c : covrList) {
			if(c.getCoverage().equals("01") && c.getRider().equals("00")) {
				covrpf.setPremCessDate(c.getPremCessDate());
				covrpf.setRiskCessDate(c.getRiskCessDate());
			}
		}
		if (covrpf == null) {
			fatalError600();
		}
		/*covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
		covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
	}

protected void validDate2541()
	{
		/* Validate that the riders Risk and Premium Cessation dates are   */
		/* not greater than the Coverages to which it is attached. A rider */
		/* can not mature after its driving coverage.                      */
		if (isGT(sv.mattcess, covtmjaIO.getRiskCessDate())) {
			sv.mattcessErr.set(errorsInner.h033);
		}
		if (isGT(sv.premcess, covtmjaIO.getPremCessDate())) {
			sv.premcessErr.set(errorsInner.h044);
		}
		/* Re-Read the Rider record in order to position the file and data */
		/* at the correct record information for processing.               */
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		/*    MOVE COVRMJA-LIFE           TO COVTMJA-LIFE.                 */
		/*    MOVE COVRMJA-COVERAGE       TO COVTMJA-COVERAGE.             */
		/*    MOVE COVRMJA-RIDER          TO COVTMJA-RIDER.                */
		covtmjaIO.setLife(sv.life);
		covtmjaIO.setCoverage(sv.coverage);
		covtmjaIO.setRider(sv.rider);
		/*    MOVE COVRMJA-SEQNBR         TO COVTMJA-SEQNBR.               */
		covtmjaIO.setPlanSuffix(wsaaPlanSuffix);
		covtmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		/* Re-Read the Rider record in order to position the file and data */
		/* in the correct position for the information to be processed.    */
		/* (in regard to the  covr file).....                              */
		covrpf.setChdrcoy(covrpf.getChdrcoy());
		covrpf.setChdrnum(covrpf.getChdrnum());
		covrpf.setLife(sv.life.toString());
		covrpf.setCoverage(sv.coverage.toString());
		covrpf.setRider(sv.rider.toString());
		covrpfList = covrpfDAO.searchCovrpfRecord(covrpf);
		if (covrpfList.isEmpty()) {
			goTo(GotoLabel.datesCont2541);
		}
		/*covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
	}

protected void datesCont2541()
	{
		/* If modify mode and the record being modified is the coverage    */
		/* check that the cessation dates are not less than those on the   */
		/* riders.                                                         */
		/* Once the check has been completed re read the COVR to ensure    */
		/* the correct record details are retained                         */
		if (isEQ(sv.rider, "00")
		&& modifyComp.isTrue()) {
			covrrgwIO.setParams(SPACES);
			covrrgwIO.setChdrcoy(covrpf.getChdrcoy());
			covrrgwIO.setChdrnum(covrpf.getChdrnum());
			covrrgwIO.setLife(covrpf.getLife());
			covrrgwIO.setCoverage(covrpf.getCoverage());
			covrrgwIO.setRider(covrpf.getRider());
			covrrgwIO.setPlanSuffix(covrpf.getPlanSuffix());
			covrrgwIO.setFunction(varcom.begn);
			covrrgwIO.setFormat(formatsInner.covrrgwrec);
			SmartFileCode.execute(appVars, covrrgwIO);
			if (isNE(covrrgwIO.getStatuz(), varcom.oK)
			&& isNE(covrrgwIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(covrrgwIO.getParams());
				syserrrec.statuz.set(covrrgwIO.getStatuz());
				fatalError600();
			}
			while ( !(isEQ(covrrgwIO.getStatuz(), varcom.endp))) {
				riderCessation2541();
			}
			
		}
		if (isNE(sv.mattcessErr, SPACES)
		|| isNE(sv.premcessErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*  Calculate cessasion age and term.*/
		if (isEQ(sv.mattcess, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.check2542);
		}
		/* MOVE WSZZ-CLTDOB            TO DTC3-INT-DATE-1.              */
		/* MOVE SR575-MATTCESS         TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-MATAGE.                  */
		/* MOVE SR575-PREMCESS         TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-AGE.           */
		if (isEQ(t5551rec.eaage, "A")) {
			goTo(GotoLabel.ageAnniversary2541);
		}
		if (isEQ(sv.matage, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.mattcess);
			callDatcon32600();
			wszzMatage.set(datcon3rec.freqFactor);
		}
		else {
			wszzMatage.set(sv.matage);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premcess);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		goTo(GotoLabel.check2542);
	}

protected void ageAnniversary2541()
	{
		if (isEQ(sv.matage, ZERO)) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
			datcon3rec.intDate2.set(sv.mattcess);
			callDatcon32600();
			compute(wszzMatage, 5).set(add(datcon3rec.freqFactor, wszzAnbAtCcd));
		}
		else {
			wszzMatage.set(sv.matage);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
			datcon3rec.intDate2.set(sv.premcess);
			callDatcon32600();
			compute(wszzPremCessAge, 5).set(add(datcon3rec.freqFactor, wszzAnbAtCcd));
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
	}

protected void check2542()
	{
		if (isEQ(sv.premcess, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.check2543);
		}
		/* MOVE CHDRMJA-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* MOVE SR575-MATTCESS         TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-MATTRM.                  */
		/* MOVE SR575-PREMCESS         TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-TERM.          */
		if (addComp.isTrue()) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
		}
		else {
			datcon3rec.intDate1.set(covrpf.getCrrcd());
		}
		if (isEQ(sv.mattrm, ZERO)) {
			datcon3rec.intDate2.set(sv.mattcess);
			callDatcon32600();
			wszzMattrm.set(datcon3rec.freqFactor);
		}
		else {
			wszzMattrm.set(sv.mattrm);
		}
		if (isEQ(sv.premCessTerm, ZERO)) {
			datcon3rec.intDate2.set(sv.premcess);
			callDatcon32600();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
	}

protected void check2543()
	{
		/*  Assume the dates are invalid until proved otherwise.*/
		sv.matageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.mattrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		x.set(0);
	}

	/**
	* <pre>
	* Check each possible option.
	* </pre>
	*/
protected void checkOccurance2545()
	{
		x.add(1);
		if (isGT(x, wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2555);
		}
		if ((isEQ(t5551rec.ageIssageFrm[x.toInt()], 0)
		&& isEQ(t5551rec.ageIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5551rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5551rec.ageIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkTermFields2550);
		}
		if (isGTE(wszzMatage, t5551rec.maturityAgeFrom[x.toInt()])
		&& isLTE(wszzMatage, t5551rec.maturityAgeTo[x.toInt()])) {
			sv.matageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge, t5551rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge, t5551rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2550()
	{
		if ((isEQ(t5551rec.termIssageFrm[x.toInt()], 0)
		&& isEQ(t5551rec.termIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5551rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5551rec.termIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkOccurance2545);
		}
		if (isGTE(wszzMattrm, t5551rec.maturityTermFrom[x.toInt()])
		&& isLTE(wszzMattrm, t5551rec.maturityTermTo[x.toInt()])) {
			sv.mattrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm, t5551rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm, t5551rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2545);
	}

protected void checkComplete2555()
	{
		if (isNE(sv.mattrmErr, SPACES)
		&& isEQ(sv.mattrm, ZERO)) {
			sv.mattcessErr.set(sv.mattrmErr);
			sv.mattrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr, SPACES)
		&& isEQ(sv.premCessTerm, ZERO)) {
			sv.premcessErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.matageErr, SPACES)
		&& isEQ(sv.matage, ZERO)) {
			sv.mattcessErr.set(sv.matageErr);
			sv.matageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr, SPACES)
		&& isEQ(sv.premCessAge, ZERO)) {
			sv.premcessErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
	}

	/**
	* <pre>
	*2557-CHECK-PREM.                                            <067>
	*  Check the premium amount against T5533 to see if its           
	*  within the limits for that frequency.                          
	**                                                           <054>
	**    MOVE 1                     TO WSCC-SUB.                <054>
	**    PERFORM UNTIL                                          <054>
	**          BILL-FREQ-NUMERIC = T5533-FREQUENCY(WSCC-SUB)    <054>
	**       OR WSCC-SUB             >  8                        <054>
	**             ADD 1                 TO WSCC-SUB             <054>
	**    END-PERFORM.                                           <054>
	**    IF  SR575-SINGLE-PREMIUM   >  T5533-CMAX(WSCC-SUB)     <054>
	**     OR SR575-SINGLE-PREMIUM   <  T5533-CMIN(WSCC-SUB)     <054>
	**        MOVE G070              TO SR575-SINGPRM-ERR.       <054>
	* </pre>
	*/
protected void checkMortcls2560()
	{
		/*  3) Mortality-Class,  if the mortality class appears on a*/
		/*       coverage/rider  screen  it  is  a  compulsory field*/
		/*       because it will  be used in calculating the premium*/
		/*       amount. The mortality class entered must one of the*/
		/*       ones in the edit rules table.*/
		if (isEQ(sv.mortclsOut[varcom.pr.toInt()], "Y")) {
			goTo(GotoLabel.checkLiencd2570);
		}
		x.set(0);
	}

protected void loop2565()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2570);
		}
		if (isNE(t5551rec.mortcls[x.toInt()], sv.mortcls)) {
			goTo(GotoLabel.loop2565);
		}
	}

protected void checkLiencd2570()
	{
		if (isEQ(sv.liencdOut[varcom.pr.toInt()], "Y")
		|| isEQ(sv.liencd, SPACES)) {
			goTo(GotoLabel.checkMore2580);
		}
		x.set(0);
	}

protected void loop2575()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2580);
		}
		if (isEQ(t5551rec.liencd[x.toInt()], SPACES)
		|| isNE(t5551rec.liencd[x.toInt()], sv.liencd)) {
			goTo(GotoLabel.loop2575);
		}
	}

protected void checkMore2580()
	{
		/*IF  SR575-RESERVE-UNITS-IND  = 'N' AND                       */
		/*    SR575-RESERVE-UNITS-DATE NOT = VRCM-MAX-DATE             */
		/*       MOVE G099              TO SR575-RUNDTE-ERR            */
		/*       MOVE G099              TO SR575-RSUNIN-ERR            */
		/*  ELSE                                                       */
		/*     IF SR575-RESERVE-UNITS-IND  = 'Y' AND                   */
		/*        SR575-RESERVE-UNITS-DATE = VRCM-MAX-DATE             */
		/*        MOVE E186              TO SR575-RUNDTE-ERR.          */
		if (isNE(sv.singlePremium, ZERO)) {
			zrdecplrec.amountIn.set(sv.singlePremium);
			zrdecplrec.currency.set(sv.currcd);
			a200CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.singlePremium)) {
				sv.singprmErr.set(errorsInner.rfik);
				wsspcomn.edterror.set("Y");
			}
		}
		if (isEQ(wsspcomn.edterror, "Y")) {
			return ;
		}
		if (isEQ(singPremInd, 1)) {
			return ;
		}
	}

protected void riderCessation2541()
	{
		para2541();
	}

protected void para2541()
	{
		/*  Check the riders for cessation dates greater than the          */
		/*  coverage cessation date. As soon as an invalid date is         */
		/*  found display error. On finding an error the user will         */
		/*  have to exit th coverage screen and change the riders          */
		/*  cessation dates before changing the coverage cessation dates   */
		if (isNE(covrrgwIO.getChdrcoy(), wsaaCovrChdrcoy)
		|| isNE(covrrgwIO.getChdrnum(), wsaaCovrChdrnum)
		|| isNE(covrrgwIO.getLife(), wsaaCovrLife)
		|| isNE(covrrgwIO.getCoverage(), wsaaCovrCoverage)) {
			covrrgwIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(covrrgwIO.getRider(), "00")) {
			/*CONTINUE_STMT*/
		}
		else {
			if (isGT(covrrgwIO.getRiskCessDate(), sv.mattcess)) {
				sv.mattcessErr.set(errorsInner.h033);
				covrrgwIO.setStatuz(varcom.endp);
			}
			if (isGT(covrrgwIO.getPremCessDate(), sv.premcess)) {
				sv.premcessErr.set(errorsInner.h044);
				covrrgwIO.setStatuz(varcom.endp);
			}
		}
		/*                                                      <CAS1.0>*/
		/* IF COVRRGW-RISK-CESS-DATE  > SR575-MATTCESS          <CAS1.0>*/
		/*    MOVE H033               TO SR575-MATTCESS-ERR     <CAS1.0>*/
		/*    MOVE ENDP               TO COVRRGW-STATUZ.        <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		/* IF COVRRGW-PREM-CESS-DATE  > SR575-PREMCESS          <CAS1.0>*/
		/*    MOVE H044               TO SR575-PREMCESS-ERR     <CAS1.0>*/
		/*    MOVE ENDP               TO COVRRGW-STATUZ.        <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		/* IF COVRRGW-STATUZ          = ENDP                    <CAS1.0>*/
		/*    GO TO 2541-EXIT.                                  <CAS1.0>*/
		covrrgwIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrrgwIO);
		if (isNE(covrrgwIO.getStatuz(), varcom.oK)
		&& isNE(covrrgwIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrrgwIO.getParams());
			syserrrec.statuz.set(covrrgwIO.getStatuz());
			fatalError600();
		}
	}

protected void callDatcon32600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcPremium2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2700();
					calc2710();
				case adjustPrem2725: 
					adjustPrem2725();
				case exit2790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2700()
	{
		/* If plan processing, and no policies applicable,*/
		/*   skip the validation as this COVTMJA is to be deleted.*/
		/* PREMIUM CALCULATION*/
		/* The  premium amount is  required  on  all  products  and  all*/
		/* validating  must  be  successfully  completed  before  it  is*/
		/* calculated. If there is  no  premium  method defined (i.e the*/
		/* relevant code was blank), the premium amount must be entered.*/
		/* Otherwise, it is optional and always calculated.*/
		/* Note that a premium calculation subroutine may calculate the*/
		/* premium from the sum insured OR the sum insured from the*/
		/* premium.*/
		/* To calculate  it,  call  the  relevant calculation subroutine*/
		/* worked out above passing:*/
		/*    IF PREM-REQD           = 'N'                                 */
		/*       GO TO 2710-CALC.                                          */
		/*    IF SR575-SINGLE-PREMIUM          = 0                         */
		/*       MOVE U019                TO SR575-SINGPRM-ERR.            */
		/*    GO TO 2790-EXIT.                                             */
		/* If there is no premium calculation method, set premium          */
		/* required field to YES.                                          */
		if (isEQ(t5675rec.premsubr, SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			wsaaPremStatuz.set("N");
		}
		/* If a premium is required check if this premium > 0              */
		if (premReqd.isTrue()) {
			if (isEQ(sv.singlePremium, 0)) {
				/*        MOVE U019             TO SR575-SINGPRM-ERR             */
				sv.singprmErr.set(errorsInner.g818);
			}
			goTo(GotoLabel.exit2790);
		}
		/* If the premium is greater than zero and the user has modified   */
		/* a premium then skip premium calculation.                        */
		if (modifyComp.isTrue()
		&& userPremEntered.isTrue()
		&& premReqd.isTrue()) {
			/*   MOVE 'N'                 TO WSAA-PREM-STATUZ         <031>*/
			if (isGT(sv.singlePremium, 0)) {
				goTo(GotoLabel.exit2790);
			}
		}
		/* If the premium is greater than zero and the user is modifying   */
		/* a component premium calculation is skipped.                     */
		if (isGT(sv.singlePremium, 0)
		&& premReqd.isTrue()) {
			if (wsaaPremChanged.isTrue()
			&& modifyComp.isTrue()) {
				wsaaPremStatuz.set("U");
				sv.singprmErr.set(errorsInner.f404);
				goTo(GotoLabel.exit2790);
			}
		}
		if (isGT(sv.singlePremium, 0)
		&& premReqd.isTrue()) {
			if (!wsaaPremChanged.isTrue()) {
				goTo(GotoLabel.exit2790);
			}
		}
	}

protected void calc2710()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		/*  Effective date to be current billed to date, as increase will*/
		/*  be paid as part of next billing*/
		/*MOVE CHDRMJA-BTDATE         TO CPRM-EFFECTDT.                */
		premiumrec.effectdt.set(payrIO.getBtdate());
		if (flexiblePremium.isTrue()
		&& modifyComp.isTrue()) {
			premiumrec.effectdt.set(wsaaTargFrom);
		}
		premiumrec.termdate.set(sv.premcess);
		/*MOVE CHDRMJA-CNTCURR        TO CPRM-CURRCODE.                */
		premiumrec.currcode.set(payrIO.getCntcurr());
		/*  (wsaa-sumin already adjusted for plan processing)*/
		wsaaStoreSumin.set(wsaaSumin);
		if (addComp.isTrue()) {
			premiumrec.sumin.set(wsaaSumin);
		}
		else {
			premiumrec.function.set("COMPM");
			if (plan.isTrue()
			&& isLTE(sv.planSuffix, chdrpf.getPolsum())
			&& isGT(chdrpf.getPolsum(), ZERO)) {
				compute(wsaaSumin, 3).setRounded(div(mult(sv.sumin, sv.polinc), chdrpf.getPolsum()));
				premiumrec.sumin.set(wsaaSumin);
			}
			else {
				premiumrec.sumin.set(wsaaSumin);
			}
		}
		/*            MOVE WSAA-SUMINS-DIFF       TO CPRM-SUMIN.          */
		if (isEQ(premiumrec.sumin, ZERO)) {
			if (optextYes.isTrue()) {
				premiumrec.sumin.set(wsaaStoreSumin);
			}
		}
		premiumrec.mortcls.set(sv.mortcls);
		/*MOVE CHDRMJA-BILLFREQ       TO CPRM-BILLFREQ.                */
		/*MOVE CHDRMJA-BILLCHNL       TO CPRM-MOP                      */
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrpf.getOccdate());
		premiumrec.reRateDate.set(chdrpf.getOccdate());
		premiumrec.calcPrem.set(sv.singlePremium);
		premiumrec.calcBasPrem.set(sv.singlePremium);
		premiumrec.calcLoaPrem.set(ZERO);
		/*   IF   ADD-COMP*/
		/*        MOVE SR575-SINGLE-PREMIUM   TO CPRM-CALC-PREM*/
		/*   ELSE*/
		/*        MOVE WSAA-PREM-DIFF         TO CPRM-CALC-PREM.          */
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsaaSex2);
		premiumrec.jlage.set(wsaaAnbAtCcd2);
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		callDatcon32600();
		compute(premiumrec.duration, 5).set(add(datcon3rec.freqFactor, .99999));
		if (plan.isTrue()
		&& isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.polinc), chdrpf.getPolsum())));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.polinc), chdrpf.getPolsum())));
		}
		/* As this is a unit-linked program, there is no need for the   */
		/* linkage to be set up with ANNY details.  Therefore, all      */
		/* fields are initialised before the call to the subroutine.    */
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			premiumrec.cnttype.set(chdrpf.getCnttype());
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		//****Ticket #ILIFE-2005 end
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			syserrrec.params.set(premiumrec.premiumRec);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.singprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2790);
		}
		if (!addComp.isTrue()) {
			if (isEQ(wsaaSuminsDiff, ZERO)) {
				if (optextYes.isTrue()) {
					goTo(GotoLabel.adjustPrem2725);
				}
			}
		}
	}

	/**
	* <pre>
	*****IF  MODIFY-COMP                                              
	*****    PERFORM 2800-ADJUST-SUMINS-PREM                          
	*****    GO TO 2790-EXIT.                                         
	* </pre>
	*/
protected void adjustPrem2725()
	{
		/* Adjust premium calculated for plan processing.*/
		if (plan.isTrue()
		&& isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& isGT(sv.polinc, ZERO)
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, chdrpf.getPolsum()), sv.polinc)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, chdrpf.getPolsum()), sv.polinc)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, chdrpf.getPolsum()), sv.polinc)));
		}
		/* Put possibly calculated sum insured back on the screen.*/
		if (plan.isTrue()
		&& isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& isGT(sv.polinc, ZERO)
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin, chdrpf.getPolsum()), sv.polinc)));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		/*BRD-306 END */
		/* Having calculated it, the  entered value, if any, is compared*/
		/* with it to check that  it  is within acceptable limits of the*/
		/* automatically calculated figure.  If  it  is  less  than  the*/
		/* amount calculated and  within  tolerance  then  the  manually*/
		/* entered amount is allowed.  If  the entered value exceeds the*/
		/* calculated one, the calculated value is used.*/
		if (isEQ(sv.singlePremium, 0)) {
			sv.singlePremium.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		wsaaBillfreq.set(chdrpf.getBillfreq());
		/*  If in Component Modify, re-calculate the new target    <D9604>*/
		/*  using the premium calculated and check if it is still  <D9604>*/
		/*  within the target for the component.                   <D9604>*/
		if (flexiblePremium.isTrue()
		&& !addComp.isTrue()) {
			compute(wsaaNewTarget, 3).setRounded(mult(premiumrec.calcPrem, wsaaBillfreqN));
			if (isGT(fpcoIO.getPremRecPer(), wsaaNewTarget)) {
				sv.singprmErr.set(errorsInner.d040);
				goTo(GotoLabel.exit2790);
			}
		}
		if (isGTE(sv.singlePremium, premiumrec.calcPrem)) {
			sv.singlePremium.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, sv.singlePremium));
		sv.singprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)
		|| isEQ(sv.singprmErr, SPACES)); wsaaSub.add(1)){
			searchForTolerance2740();
		}
		/* If the premium is within the tolerance limits, then we have to  */
		/* adjust the basic premium (the loaded premium will not change).  */
		if (isEQ(sv.singprmErr, SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.singlePremium, sv.zlinstprem));
		}
		goTo(GotoLabel.exit2790);
	}

protected void searchForTolerance2740()
	{
		/* Calculate tolerance Limit and Check whether it is greater       */
		/*   than the maximum tolerance amount in table T5667.             */
		if (isEQ(payrIO.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtol[wsaaSub.toInt()])), 100));
			if (isLTE(wsaaDiff, wsaaTol)
			&& isLTE(wsaaDiff, t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.singprmErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()], ZERO)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtoln[wsaaSub.toInt()])), 100));
				if (isLTE(wsaaDiff, wsaaTol)
				&& isLTE(wsaaDiff, t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.singprmErr.set(SPACES);
				}
			}
		}
	}

	/**
	* <pre>
	**2800-ADJUST-SUMINS-PREM SECTION.                                
	**2800-START.                                                     
	* Adjust premium calculated for plan processing.                  
	*****IF PLAN                                                      
	*****    AND                                                      
	*****   SR575-PLAN-SUFFIX  NOT > CHDRMJA-POLSUM                   
	*****   COMPUTE CPRM-CALC-PREM ROUNDED = (CPRM-CALC-PREM          
	*****                                    * CHDRMJA-POLSUM         
	*****                                    / SR575-POLINC).         
	* Put possibly calculated sum insured back on the screen.         
	*****IF PLAN                                                      
	*****    AND                                                      
	*****   SR575-PLAN-SUFFIX  NOT > CHDRMJA-POLSUM                   
	*****   COMPUTE CPRM-SUMIN    ROUNDED = (CPRM-SUMIN               
	*****                                    * CHDRMJA-POLSUM         
	*****                                    / SR575-POLINC).         
	*****IF  WSAA-PREM-INCREASED                                      
	*****    COMPUTE SR575-SINGLE-PREMIUM                             
	*****            = WSAA-OLD-PREM + CPRM-CALC-PREM                 
	*****    COMPUTE SR575-SUMIN                                      
	*****            = WSAA-OLD-SUMINS      + CPRM-SUMIN              
	*****ELSE                                                         
	*****    COMPUTE SR575-SINGLE-PREMIUM                             
	*****            = WSAA-OLD-PREM  - CPRM-CALC-PREM                
	*****    COMPUTE SR575-SUMIN                                      
	*****            = WSAA-OLD-SUMINS  - CPRM-SUMIN.                 
	**2890-EXIT.                                                      
	*****  EXIT.                                                      
	*****                                                        <048>
	*****                                                        <048>
	*2900-REVIEW-PAYER SECTION.                                  <048>
	***************************                                  <048>
	*                                                            <048>
	*2920-READ-PAYER.                                            <048>
	* Start reading the PAYR file at the beginning ....
	*                                                            <048>
	*****MOVE SPACES                 TO PAYR-DATA-AREA.          <048>
	*****MOVE PAYRREC                TO PAYR-FORMAT.             <048>
	*****MOVE READR                  TO PAYR-FUNCTION.           <048>
	*                                                            <048>
	*****MOVE CHDRMJA-CHDRCOY        TO PAYR-CHDRCOY.            <048>
	*****MOVE CHDRMJA-CHDRNUM        TO PAYR-CHDRNUM.            <048>
	*****MOVE SR575-PAYRSEQNUM       TO PAYR-PAYRSEQNO.          <048>
	*                                                            <048>
	*****CALL 'PAYRIO'            USING PAYR-PARAMS.             <048>
	*                                                            <048>
	*****IF PAYR-STATUZ             NOT = O-K AND                <048>
	*****   PAYR-STATUZ             NOT = MRNF                   <048>
	*****   MOVE PAYR-STATUZ         TO SYSR-STATUZ              <048>
	*****   MOVE PAYR-PARAMS         TO SYSR-PARAMS              <048>
	*****   PERFORM 600-FATAL-ERROR                              <048>
	*****ELSE                                                    <048>
	*****   IF PAYR-STATUZ           =  MRNF                     <048>
	*****      MOVE I074             TO SR575-PAYRSEQNUM-ERR     <048>
	*****      GO TO 2950-EXIT                                   <048>
	*****   END-IF                                               <048>
	*****END-IF.                                                 <048>
	*                                                            <048>
	*2930-OBTAIN-CLNTNUM.                                        <048>
	*                                                            <048>
	*****MOVE SPACES                 TO CLRF-DATA-AREA.          <048>
	*****MOVE CLRFREC                TO CLRF-FORMAT.             <048>
	*****MOVE READR                  TO CLRF-FUNCTION.           <048>
	*                                                            <048>
	*****MOVE 'CH'                   TO CLRF-FOREPFX.            <048>
	*****MOVE CHDRMJA-CHDRCOY        TO CLRF-FORECOY.            <048>
	*****MOVE CHDRMJA-CHDRNUM        TO WSAA-FORENUM.            <048>
	*****MOVE SR575-PAYRSEQNUM       TO WSAA-SEQNUM.             <048>
	*****MOVE WSAA-CLRF-FORENUM      TO CLRF-FORENUM.            <048>
	*****MOVE 'PY'                   TO CLRF-CLRRROLE.           <048>
	*                                                            <048>
	*****CALL 'CLRFIO'            USING CLRF-PARAMS.             <048>
	*                                                            <048>
	*****IF CLRF-STATUZ             NOT = O-K                    <048>
	*****   MOVE CLRF-STATUZ         TO SYSR-STATUZ              <048>
	*****   MOVE CLRF-PARAMS         TO SYSR-PARAMS              <048>
	*****   PERFORM 600-FATAL-ERROR                              <048>
	*****END-IF.                                                 <048>
	*                                                            <048>
	*2940-OBTAIN-CLNTNAME.                                       <048>
	*                                                            <048>
	*****MOVE SPACES                 TO CLTS-DATA-AREA.          <048>
	*****MOVE CLTSREC                TO CLTS-FORMAT.             <048>
	*****MOVE READR                  TO CLTS-FUNCTION.           <048>
	*                                                            <048>
	*****MOVE CLRF-CLNTPFX           TO CLTS-CLNTPFX.            <048>
	*****MOVE CLRF-CLNTCOY           TO CLTS-CLNTCOY.            <048>
	*****MOVE CLRF-CLNTNUM           TO CLTS-CLNTNUM.            <048>
	*                                                            <048>
	*****CALL 'CLTSIO'            USING CLTS-PARAMS.             <048>
	*                                                            <048>
	*****IF CLTS-STATUZ             NOT = O-K                    <048>
	*****   MOVE CLTS-STATUZ         TO SYSR-STATUZ              <048>
	*****   MOVE CLTS-PARAMS         TO SYSR-PARAMS              <048>
	*****   PERFORM 600-FATAL-ERROR                              <048>
	*****END-IF.                                                 <048>
	*                                                            <048>
	**** MOVE CLRF-CLNTNUM           TO SR575-CLNTNUM.           <048>
	*                                                            <048>
	* Format the (NEW) CLIENT name ...                                
	**** PERFORM PLAINNAME.                                      <048>
	*                                                            <048>
	*****MOVE WSSP-LONGCONFNAME      TO SR575-PAYERNAME.         <048>
	*                                                            <048>
	*2950-EXIT.                                                  <048>
	**** EXIT.                                                   <048>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loadWsspFields3010();
				case checkPcdt3020: 
					checkPcdt3020();
				case exit3490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loadWsspFields3010()
	{
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3490);
		}
		/* If the 'KILL' function key was pressed or if in enquiry mode,*/
		/* skip the updating.*/
		if (isEQ(scrnparams.statuz, "KILL")
		|| isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3490);
		}
		/*    IF COMP-ENQUIRY                                              */
		keepCovr3600();
		/*    IF COMP-ENQUIRY                                              */
		/* OR SR575-OPTEXTIND          = 'X'                            */
		/*    OR SR575-COMIND             = 'X'                            */
		if (compEnquiry.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.exit3490);
		}
		if (compApp.isTrue()) {
			goTo(GotoLabel.checkPcdt3020);
		}
		/* If the Options/Extras check box has been selected,              */
		/* do not leave the section if it has been selected in             */
		/* conjunction with either the Commission or the                   */
		/* Reassurance check box.                                          */
		if (isEQ(sv.optextind, "X")
		&& isNE(sv.comind, "X")) {
			goTo(GotoLabel.exit3490);
		}
		/*    IF SR575-COMIND             = 'X'                    <V4L004>*/
		/*         GO TO 3020-CHECK-PCDT.                          <V4L004>*/
		/* Release the COVTMJA record.*/
		covtmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		if (addComp.isTrue()) {
			covtmjaIO.setPlanSuffix(0);
			initcovt3500();
			setupcovtmja3550();
			/* MOVE WRITR              TO COVTMJA-FUNCTION              */
			covtmjaIO.setFunction(varcom.updat);
			updateCovtmja3700();
		}
		if (modifyComp.isTrue()) {
			if (wsaaNoCovt.isTrue()) {
				covtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
				/*          PERFORM 3500-INITCOVT*/
				setupcovtmja3550();
				/*            Extra check in here to see if RACT or                */
				/*            or LEXT updates have occurred - if so, we want to    */
				/*            set update flag to 'Y'.                              */
				if (isEQ(wsaaLextUpdates, "Y")) {
					/*           OR WSAA-RACT-UPDATES     = 'Y'             <R96REA>*/
					wsaaUpdateFlag.set("Y");
				}
				/* MOVE WRITR              TO COVTMJA-FUNCTION          */
				covtmjaIO.setFunction(varcom.updat);
				updateCovtmja3700();
			}
			else {
				covtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
				setupcovtmja3550();
				/*            Extra check in here to see if RACT or                */
				/*            or LEXT updates have occurred - if so, we want to    */
				/*            set update flag to 'Y'.                              */
				if (isEQ(wsaaLextUpdates, "Y")) {
					/*           OR WSAA-RACT-UPDATES     = 'Y'             <R96REA>*/
					wsaaUpdateFlag.set("Y");
				}
				/*          IF WSAA-UPDATE-FLAG = 'Y'*/
				covtmjaIO.setFunction(varcom.updat);
				updateCovtmja3700();
			}
		}
		/*    IF  WSAA-UPDATE-FLAG = 'N'                                   */
		/*        GO TO 3490-EXIT.                                         */
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		covtmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		/*    IF  SR575-COMIND            =  'X'                   <V4L004>*/
		/*        GO TO 3020-CHECK-PCDT                            <V4L004>*/
		/*    END-IF.                                              <V4L004>*/
		if (isEQ(wsaaUpdateFlag, "N")) {
			goTo(GotoLabel.exit3490);
		}
		/*    If the whole plan selected then do a KEEPS on the COVR       */
		/*    file so that if there is no COVT record the COVR record      */
		/*    can be retrieved to show FUND DIRECTION.                     */
		keepCovr3600();
		goTo(GotoLabel.exit3490);
	}

protected void checkPcdt3020()
	{
		/* If no PCDT record is to be created then the existing agent is*/
		/* to receive commission, therefore set up current details in*/
		/* PCDT to make AT processing uniform throughout.*/
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrpf.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			defaultPcdt3800();
		}
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void initcovt3500()
	{
		para3500();
	}

protected void para3500()
	{
		/* Before creating a  new COVTMJA record initialise the coverage*/
		/* fields.*/
		covtmjaIO.setCrtable(wsaaCrtable);
		/*    MOVE COVTMJA-CRTABLE        TO COVTMJA-CRTABLE.              */
		/*MOVE SPACES                 TO COVTMJA-RESERVE-UNITS-IND.    */
		covtmjaIO.setRiskCessDate(0);
		covtmjaIO.setBenCessAge(0);
		covtmjaIO.setPremCessAge(0);
		covtmjaIO.setRiskCessTerm(0);
		covtmjaIO.setBenCessTerm(0);
		covtmjaIO.setPremCessTerm(0);
		covtmjaIO.setPolinc(0);
		covtmjaIO.setInstprem(0);
		covtmjaIO.setZbinstprem(0);
		covtmjaIO.setZbinstprem(0);
		covtmjaIO.setSingp(0);
		covtmjaIO.setSumins(0);
		/* MOVE SR575-PLAN-SUFFIX      TO COVTMJA-SEQNBR.               */
		covtmjaIO.setSeqnbr(wsaaNextSeqnbr);
		wsaaNextSeqnbr.subtract(1);
		covtmjaIO.setRiskCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setPremCessDate(varcom.vrcmMaxDate);
		/*MOVE CHDRMJA-BTDATE         TO COVTMJA-EFFDATE.              */
		covtmjaIO.setEffdate(payrIO.getBtdate());
		if (flexiblePremium.isTrue()
		&& modifyComp.isTrue()) {
			covtmjaIO.setEffdate(wsaaTargFrom);
		}
		covtmjaIO.setAnbccd(1, ZERO);
		covtmjaIO.setAnbccd(2, ZERO);
		covtmjaIO.setSex(1, SPACES);
		covtmjaIO.setSex(2, SPACES);
		covtmjaIO.setPayrseqno(1);
	}

protected void setupcovtmja3550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3550();
				case cont3555: 
					cont3555();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3550()
	{
		/* Check for changes in the COVTMJA Record.*/
		covtmjaIO.setCrtable(wsaaCrtable);
		wsaaUpdateFlag.set("N");
		if (addComp.isTrue()) {
			covtmjaIO.setPlanSuffix(0);
		}
		else {
			covtmjaIO.setPlanSuffix(wsaaPlanSuffix);
		}
		/* MOVE SR575-PLAN-SUFFIX      TO COVTMJA-SEQNBR.               */
		covtmjaIO.setSeqnbr(wsaaNextSeqnbr);
		wsaaNextSeqnbr.subtract(1);
		if (isNE(sv.mattcess, covtmjaIO.getRiskCessDate())) {
			covtmjaIO.setRiskCessDate(sv.mattcess);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premcess, covtmjaIO.getPremCessDate())) {
			covtmjaIO.setPremCessDate(sv.premcess);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.matage, covtmjaIO.getRiskCessAge())) {
			covtmjaIO.setRiskCessAge(sv.matage);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge, covtmjaIO.getPremCessAge())) {
			covtmjaIO.setPremCessAge(sv.premCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.mattrm, covtmjaIO.getRiskCessTerm())) {
			covtmjaIO.setRiskCessTerm(sv.mattrm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm, covtmjaIO.getPremCessTerm())) {
			covtmjaIO.setPremCessTerm(sv.premCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin, covtmjaIO.getSumins())) {
			covtmjaIO.setSumins(sv.sumin);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zbinstprem, covtmjaIO.getZbinstprem())) {
			covtmjaIO.setZbinstprem(sv.zbinstprem);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem, covtmjaIO.getZlinstprem())) {
			covtmjaIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covtmjaIO.setLoadper(sv.loadper);
			covtmjaIO.setRateadj(sv.rateadj);
			covtmjaIO.setFltmort(sv.fltmort);
			covtmjaIO.setPremadj(sv.premadj);
			covtmjaIO.setAgeadj(sv.adjustageamt);
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		/*    Single Premium Component*/
		/*IF CHDRMJA-BILLFREQ         = '00'                           */
		if (isEQ(payrIO.getBillfreq(), "00")) {
			if (isNE(sv.singlePremium, covtmjaIO.getSingp())) {
				covtmjaIO.setSingp(sv.singlePremium);
			}
			goTo(GotoLabel.cont3555);
		}
		/*       GO TO 3555-CONT                                        */
		/*    ELSE                                                      */
		/*       GO TO 3555-CONT.                                       */
		/*    Regular Premium Component*/
		if (isNE(sv.singlePremium, covtmjaIO.getInstprem())) {
			covtmjaIO.setInstprem(sv.singlePremium);
			wsaaUpdateFlag.set("Y");
		}
	}

	/**
	* <pre>
	**** Possible Lump Sum!                                           
	**** IF SR575-LUMP-CONTRIB       NOT = COVTMJA-SINGP              
	****    MOVE SR575-LUMP-CONTRIB  TO COVTMJA-SINGP                 
	****    MOVE 'Y'                 TO WSAA-UPDATE-FLAG.             
	* </pre>
	*/
protected void cont3555()
	{
		if (isNE(sv.mortcls, covtmjaIO.getMortcls())) {
			covtmjaIO.setMortcls(sv.mortcls);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd, covtmjaIO.getLiencd())) {
			covtmjaIO.setLiencd(sv.liencd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.polinc, covtmjaIO.getPolinc())) {
			covtmjaIO.setPolinc(sv.polinc);
			wsaaUpdateFlag.set("Y");
		}
		/*IF SR575-PAYRSEQNUM         NOT = COVTMJA-PAYRSEQNO     <048>*/
		/*   MOVE SR575-PAYRSEQNUM    TO COVTMJA-PAYRSEQNO        <048>*/
		/*   MOVE 'Y'                 TO WSAA-UPDATE-FLAG         <048>*/
		/*END-IF.                                                 <048>*/
		/*IF SR575-RESERVE-UNITS-IND  NOT = COVTMJA-RESERVE-UNITS-IND  */
		/*   MOVE SR575-RESERVE-UNITS-IND TO COVTMJA-RESERVE-UNITS-IND */
		/*   MOVE 'Y'                 TO WSAA-UPDATE-FLAG.             */
		/*IF SR575-RESERVE-UNITS-DATE NOT = COVTMJA-RESERVE-UNITS-DATE */
		/*   MOVE SR575-RESERVE-UNITS-DATE                             */
		/*                            TO COVTMJA-RESERVE-UNITS-DATE    */
		/*   MOVE 'Y'                 TO WSAA-UPDATE-FLAG.             */
		/*IF CHDRMJA-BILLFREQ         NOT = COVTMJA-BILLFREQ           */
		/*   MOVE CHDRMJA-BILLFREQ    TO COVTMJA-BILLFREQ              */
		if (isNE(payrIO.getBillfreq(), covtmjaIO.getBillfreq())) {
			covtmjaIO.setBillfreq(payrIO.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		/*IF CHDRMJA-BILLCHNL         NOT = COVTMJA-BILLCHNL           */
		/*   MOVE CHDRMJA-BILLCHNL    TO COVTMJA-BILLCHNL              */
		if (isNE(payrIO.getBillchnl(), covtmjaIO.getBillchnl())) {
			covtmjaIO.setBillchnl(payrIO.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaAnbAtCcd, covtmjaIO.getAnbccd(1))) {
			covtmjaIO.setAnbccd(1, wsaaAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex, covtmjaIO.getSex(1))) {
			covtmjaIO.setSex(1, wsaaSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isEQ(sv.jlifcnum, "NONE")) {
			covtmjaIO.setAnbccd(2, ZERO);
			covtmjaIO.setSex(2, SPACES);
			return ;
		}
		if (isNE(wsaaAnbAtCcd2, covtmjaIO.getAnbccd(2))) {
			covtmjaIO.setAnbccd(2, wsaaAnbAtCcd2);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex2, covtmjaIO.getSex(2))) {
			covtmjaIO.setSex(2, wsaaSex2);
			wsaaUpdateFlag.set("Y");
		}
	}

protected void keepCovr3600()
	{
		/*PARA*/
		/*    IF WSAA-PLAN-SUFFIX         = ZEROS                     <064>*/
		/*       MOVE WSAA-COVR-KEY       TO COVRMJA-DATA-KEY         <064>*/
		/*       IF SR575-PLAN-SUFFIX     >  CHDRMJA-POLSUM           <064>*/
		/*          MOVE SR575-PLAN-SUFFIX  TO COVRMJA-PLAN-SUFFIX    <064>*/
		/*       ELSE                                                 <064>*/
		/*          MOVE ZEROES           TO COVRMJA-PLAN-SUFFIX      <064>*/
		/*       END-IF                                               <064>*/
		if (isEQ(wsaaPlanSuffix, ZERO)) {
			//covrmjaIO.setDataKey(wsaaCovrKey);
			covrpf.setPlanSuffix(wsaaPlanSuffix.toInt());
			covrpfDAO.setCacheObject(covrpf);
			/*covrmjaIO.setFunction("KEEPS");
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				fatalError600();
			}*/
		}
		/*EXIT*/
	}

protected void updateCovtmja3700()
	{
		para3700();
	}

protected void para3700()
	{
		/*  IF  WSAA-UPDATE-FLAG = 'N'                                   */
		/*      GO TO 3790-EXIT.                                         */
		covtmjaIO.setBenCessAge(ZERO);
		covtmjaIO.setBenCessTerm(ZERO);
		covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setReserveUnitsDate(ZERO);
		covtmjaIO.setTermid(varcom.vrcmTermid);
		covtmjaIO.setUser(varcom.vrcmUser);
		covtmjaIO.setTransactionDate(varcom.vrcmDate);
		covtmjaIO.setTransactionTime(varcom.vrcmTime);
		if (isEQ(wsaaUpdateFlag, "N")) {
			return ;
		}
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void defaultPcdt3800()
	{
		moveValues3800();
		call3830();
	}

protected void moveValues3800()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setInstprem(ZERO);
		/*COMPUTE PCDTMJA-TRANNO = CHDRMJA-TRANNO + 1.                 */
		wsaaIndex.set(1);
	}

	/**
	* <pre>
	* instead of reading the pcdd file (which holds the oridinal      
	* agent split) when no pcdt records have been created, default    
	* the servicing agent from the contract header file.              
	*    MOVE CHDRMJA-CHDRCOY        TO PCDDMJA-CHDRCOY.              
	*    MOVE CHDRMJA-CHDRNUM        TO PCDDMJA-CHDRNUM.              
	*    MOVE BEGN                   TO PCDDMJA-FUNCTION.             
	*    MOVE ZEROES                 TO WSAA-INDEX.                   
	*3810-PCDD-LOOP.                                                  
	*    ADD 1                       TO WSAA-INDEX.                   
	*    IF WSAA-INDEX > 10                                           
	*       GO TO 3830-CALL.                                          
	*    MOVE PCDDMJAREC             TO PCDDMJA-FORMAT.               
	*    CALL 'PCDDMJAIO'            USING PCDDMJA-PARAMS.            
	*    IF PCDDMJA-CHDRNUM          NOT = CHDRMJA-CHDRNUM OR         
	*       PCDDMJA-CHDRCOY          NOT = CHDRMJA-CHDRCOY            
	*       MOVE ENDP                TO PCDDMJA-STATUZ.               
	*    IF PCDDMJA-STATUZ           NOT = O-K                        
	*    AND PCDDMJA-STATUZ          NOT = ENDP                       
	*       MOVE PCDDMJA-PARAMS      TO SYSR-PARAMS                   
	*       PERFORM 600-FATAL-ERROR.                                  
	*    IF PCDDMJA-STATUZ              = ENDP                        
	*        GO TO 3820-ADD-PCDT.                                     
	*    MOVE PCDDMJA-AGNTNUM      TO PCDTMJA-AGNTNUM (WSAA-INDEX).   
	*    MOVE PCDDMJA-SPLIT-BCOMM  TO PCDTMJA-SPLITC (WSAA-INDEX).    
	*    MOVE NEXTR                  TO PCDDMJA-FUNCTION.             
	*    GO TO 3810-PCDD-LOOP.                                        
	*3820-ADD-PCDT.                                                   
	*    IF WSAA-INDEX > 10                                           
	*       GO TO 3830-CALL.                                          
	*    MOVE ZEROES               TO PCDTMJA-SPLITC (WSAA-INDEX).    
	*    ADD  1                    TO WSAA-INDEX.                     
	*    GO TO 3820-ADD-PCDT.                                         
	* </pre>
	*/
protected void call3830()
	{
		pcdtmjaIO.setAgntnum(wsaaIndex, chdrpf.getAgntnum());
		pcdtmjaIO.setSplitc(wsaaIndex, 100);
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move        */
		/*    spaces to  the current program entry in the program          */
		/*    stack and exit.                                              */
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/* Determine processing according to whether we are selectin*/
		/*   or returning from the Options and Extras data screen.*/
		if (isEQ(sv.optextind, "X")) {
			optionsExe4500();
			return ;
		}
		else {
			if (isEQ(sv.optextind, "?")) {
				optionsRet4600();
				return ;
			}
		}
		if (isEQ(sv.comind, "X")) {
			commExe4900();
			return ;
		}
		else {
			if (isEQ(sv.comind, "?")) {
				commRet4a00();
				return ;
			}
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe4b00();
			return ;
		}
		else {
			if (isEQ(sv.taxind, "?")) {
				taxRet4b50();
				return ;
			}
		}
		/* IF SR575-RATYPIND           = 'X'                    <R96REA>*/
		/*    PERFORM 4200-REASSURANCE-EXE                      <R96REA>*/
		/*    GO TO 4090-EXIT                                   <R96REA>*/
		/* ELSE                                                 <R96REA>*/
		/* IF SR575-RATYPIND           = '?'                    <R96REA>*/
		/*    PERFORM 4300-REASSURANCE-RET                      <R96REA>*/
		/*       GO TO 4090-EXIT.                               <R96REA>*/
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		wsaaIfEndOfPlan.set("N");
		if (plan.isTrue()) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				//covrmjaIO.setFunction(varcom.nextr);
				covrpfCount++;
				clearScreen4800();
				policyLoad5000();
				if (!covrpf.equals(null)) { //ILB-456
					wsspcomn.nextprog.set(wsaaProg);
					wsaaIfEndOfPlan.set("Y");
					wayout4700();
				}
				/*if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
					wsspcomn.nextprog.set(wsaaProg);
					wsaaIfEndOfPlan.set("Y");
					wayout4700();
				}*/
				else {
					wsspcomn.nextprog.set(scrnparams.scrname);
				}
			}
			else {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
				wayout4700();
			}
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4700();
		}
	}

	/**
	* <pre>
	*4200-REASSURANCE-EXE SECTION.                            <R96REA>
	*4200-PARA.                                               <R96REA>
	**Keep the COVRMJA record if Whole Plan selected or Add co<R96REA>
	****as this will have been released within the 1000-sectio<R96REA>
	****enable all policies to be processed.                  <R96REA>
	**** MOVE KEEPS               TO COVRMJA-FUNCTION.        <R96REA>
	****                                                      <R96REA>
	**** CALL 'COVRMJAIO' USING COVRMJA-PARAMS.               <R96REA>
	****                                                      <R96REA>
	**** IF COVRMJA-STATUZ        NOT = O-K                   <R96REA>
	****    MOVE COVRMJA-PARAMS   TO SYSR-PARAMS              <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	**** If the reassurance details have been selected,(value <R96REA>
	**** then set an asterisk in the program stack action fiel<R96REA>
	**** ensure that control returns here, set the parameters <R96REA>
	**** generalised secondary switching and save the original<R96REA>
	**** programs from the program stack.                     <R96REA>
	**** MOVE '?'               TO SR575-RATYPIND.            <R96REA>
	**** COMPUTE SUB1           =  WSSP-PROGRAM-PTR + 1.      <R96REA>
	**** MOVE 1                 TO SUB2.                      <R96REA>
	**** PERFORM 4510-SAVE      8 TIMES.                      <R96REA>
	****                                                      <R96REA>
	**** MOVE 'C'               TO GENS-FUNCTION.             <R96REA>
	**** PERFORM                4210-GENSWW.                  <R96REA>
	****                                                      <R96REA>
	**** COMPUTE SUB1           =  WSSP-PROGRAM-PTR + 1.      <R96REA>
	**** MOVE 1                 TO SUB2.                      <R96REA>
	**** PERFORM 4530-LOAD               8 TIMES.             <R96REA>
	**** MOVE '*'                 TO WSSP-SEC-ACTN(WSSP-PROGRA<R96REA>
	**** ADD 1                    TO WSSP-PROGRAM-PTR.        <R96REA>
	*4209-EXIT.                                               <R96REA>
	****  EXIT.                                               <R96REA>
	*   If a value has been placed in the GENS-FUNCTION then call     
	*   the generalised secondary switching module to obtain the      
	*   next 8 programs and load them into the program stack.         
	* </pre>
	*/
protected void gensww4210()
	{
		para4211();
	}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde); //
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			syserrrec.params.set(gensswrec.gensswRec);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			/*    IF LEXT-STATUZ            = ENDP                     <017>*/
			if (isEQ(lextrevIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

	/**
	* <pre>
	*4300-REASSURANCE-RET SECTION.                            <R96REA>
	*4310-PARA.                                               <R96REA>
	**** COMPUTE SUB1        = WSSP-PROGRAM-PTR + 1.          <R96REA>
	**** MOVE 1              TO SUB2.                         <R96REA>
	**** PERFORM 4610-RESTORE 8 TIMES.                        <R96REA>
	**** MOVE ' '            TO WSSP-SEC-ACTN(WSSP-PROGRAM-PTR<R96REA>
	**** PERFORM             1950-CHECK-RACT.                 <R96REA>
	**** ADD 1               TO WSSP-PROGRAM-PTR.                <010>
	* Set WSSP-NEXTPROG to the current screen name (thus returning    
	*   returning to re-display the screen).                          
	**** MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.           <R96REA>
	*4319-EXIT.                                               <R96REA>
	****  EXIT.                                               <R96REA>
	* </pre>
	*/
protected void optionsExe4500()
	{
		keepCovr4510();
	}

protected void keepCovr4510()
	{
		/* Keep the COVRMJA record if Whole Plan selected or Add component*/
		/*   as this will have been released within the 1000-section to*/
		/*   enable all policies to be processed.*/
		//ILB-456
		covrpfDAO.setCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		/* The first thing to consider is the handling of an Options/*/
		/*   Extras request. If the indicator is 'X', a request to visit*/
		/*   options and extras has been made. In this case:*/
		/*    - change the options/extras request indicator to '?',*/
		sv.optextind.set("?");
		/* Save the next 8 programs from the program stack.*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'A' to retrieve the       */
		/*       program switching required,  and  move  them to the       */
		/*       stack,                                                    */
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*'.*/
		/* Add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		para4600();
	}

protected void para4600()
	{
		/* On return from this  request, the current stack "action" will*/
		/*   be '*' and the options/extras indicator  will  be  '?'.  To*/
		/*   handle the return from options and extras:*/
		/* - calculate the  premium  as  described  above  in  the         */
		/*   'Validation' section, and check that it is within the         */
		/*   tolerance limit,                                              */
		/*    PERFORM 5200-CALC-PREMIUM.                                   */
		/* MOVE 'Y'                    TO WSAA-OPTEXT.          <RA9606>*/
		/* PERFORM 2700-CALC-PREMIUM.                           <RA9606>*/
		/* If enquiry on a component then do not recalculate the Premium.  */
		/*    IF  NOT COMP-ENQUIRY                                 <V4L004>*/
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			wsaaOptext.set("Y");
			calcPremium2700();
		}
		wsaaOptext.set("N");
		/* Restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			restore4610();
		}
		/* Blank out the stack "action".*/
		/* MOVE ' '                 TO WSSP-SEC-ACTN (WSSP-PROGRAM-PTR).*/
		if (isEQ(sv.optextind, "?")) {
			sv.optextind.set("+");
		}
		/* Set WSSP-NEXTPROG to the current screen name (thus returning*/
		/*   returning to re-display the screen).*/
		/* MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.                   */
		if (isNE(sv.comind, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4700()
	{
		/*PARA*/
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (wsaaEndOfPlan.isTrue()) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void clearScreen4800()
	{
		/*PARA*/
		sv.comind.set(SPACES);
		sv.optextind.set(SPACES);
		singPremInd.set(ZERO);
		sv.singlePremium.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		sv.sumin.set(ZERO);
		sv.polinc.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.matage.set(ZERO);
		sv.mattrm.set(ZERO);
		/*                                SR575-LUMP-CONTRIB.           */
		/*MOVE 'N'                    TO SR575-RESERVE-UNITS-IND.      */
		/*MOVE VRCM-MAX-DATE          TO SR575-RESERVE-UNITS-DATE.     */
		sv.premcess.set(varcom.vrcmMaxDate);
		sv.mattcess.set(varcom.vrcmMaxDate);
		/*EXIT*/
	}

protected void commExe4900()
	{
		para4900();
	}

protected void para4900()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an*/
		/* options/extras request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		wsspcomn.tranrate.set(sv.singlePremium);
		/*MOVE CHDRMJA-BTDATE         TO WSSP-EFFDATE.                 */
		wssplife.effdate.set(payrIO.getBtdate());
		if (flexiblePremium.isTrue()
		&& modifyComp.isTrue()) {
			wsspcomn.currfrom.set(wsaaTargFrom);
		}
		else {
			wsspcomn.currfrom.set(payrIO.getBtdate());
		}
		//ILB-456
		covrpfDAO.setCacheObject(covrpf);
		/*covrmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		sv.comind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'B' to retrieve the*/
		/*       program switching required,  and  move  them to the*/
		/*       stack,*/
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void commRet4a00()
	{
		/*A00-PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		if (isEQ(sv.comind, "?")) {
			sv.comind.set("+");
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*A90-EXIT*/
	}

protected void taxExe4b00()
	{
		start4b00();
	}

protected void start4b00()
	{
		/*  - Keep the CHDR/COVT record                                    */
		//ILB-456
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		wsaaStoreZbinstprem.set(covtmjaIO.getZbinstprem());
		wsaaStoreSingp.set(covtmjaIO.getSingp());
		wsaaStoreInstprem.set(covtmjaIO.getInstprem());
		covtmjaIO.setZbinstprem(sv.zbinstprem);
		if (isEQ(payrIO.getBillfreq(), "00")) {
			covtmjaIO.setSingp(sv.singlePremium);
			covtmjaIO.setInstprem(0);
		}
		else {
			covtmjaIO.setInstprem(sv.singlePremium);
			covtmjaIO.setSingp(0);
		}
		covtmjaIO.setFunction(varcom.keeps);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		wssplife.effdate.set(chdrpf.getBtdate());
		sv.taxind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			save4510();
		}
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet4b50()
	{
		/*B50-PARA*/
		sv.taxind.set("+");
		covtmjaIO.setZbinstprem(wsaaStoreZbinstprem);
		covtmjaIO.setSingp(wsaaStoreSingp);
		covtmjaIO.setInstprem(wsaaStoreInstprem);
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*B50-EXIT*/
	}

protected void policyLoad5000()
	{
		try {
			para5000();
			readCovt5020();
			fieldsToScreen5080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para5000()
	{
		/* If Whole plan selected or component ADD, then we need to read*/
		/*   the COVRMJA file to find either the record to be changed or*/
		/*   the policies to which the component must be added.*/
		/* If COVRMJA statuz = ENDP then all policies have been processed*/
		/*   so exit this section.*/
		/* Else attempt a search of the COVTMJA temporary file for detail*/
		/*   changes.*/
		wsaaOldPrem.set(0);
		wsaaOldSumins.set(0);
		if (plan.isTrue()) {
			readCovr5100();

			for (Covrpf covr : covrpfList) {
					if (covr == null) {
						goTo(GotoLabel.exit5090);
					}
					else {
						covtmjaIO.setFunction(varcom.readr);
						if (addComp.isTrue()) {
							covtmjaIO.setCoverage(wsaaCovrCoverage);
							covtmjaIO.setRider(wsaaCovrRider);
							covtmjaIO.setPlanSuffix(covr.getPlanSuffix());
						}
						else {
							/*             MOVE COVRMJA-DATA-KEY    TO COVTMJA-DATA-KEY*/
							covtmjaIO.setDataKey(wsaaCovrKey);
							if (isNE(covr.getPlanSuffix(),ZERO)) {
								covtmjaIO.setPlanSuffix(covr.getPlanSuffix());
							}
					}
				}
					covrpf.setPlanSuffix(covr.getPlanSuffix());
				}
			/*if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
				goTo(GotoLabel.exit5090);
			}
			else {
				covtmjaIO.setFunction(varcom.readr);
				if (addComp.isTrue()) {
					covtmjaIO.setCoverage(wsaaCovrCoverage);
					covtmjaIO.setRider(wsaaCovrRider);
					covtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
				}
				else {
					          MOVE COVRMJA-DATA-KEY    TO COVTMJA-DATA-KEY
					covtmjaIO.setDataKey(wsaaCovrKey);
					if (isNE(covrmjaIO.getPlanSuffix(), ZERO)) {
						covtmjaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
					}
				}
			}*/
		}
		wsaaOldSumins.set(covrpf.getSumins());
		wsaaOldPrem.set(covrpf.getInstprem());
		/* The header is set to show which policy is being actioned.*/
		/* If all policies are within the summarised record then display*/
		/*   the number of policies summarised and set the Hi-light on*/
		/*   Plan suffix to display the literal -*/
		/*                  'Policy Number : 10 to 1'*/
		if (plan.isTrue()) {
			if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
				sv.planSuffix.set(chdrpf.getPolsum());
				sv.plnsfxOut[varcom.hi.toInt()].set("Y");
			}
			else {
				sv.planSuffix.set(covrpf.getPlanSuffix());
			}
		}
	}

protected void readCovt5020()
	{
		/* Read the Temporary COVT record which will contain any previous*/
		/*   changes before AT submission creates the COVRMJA record.*/
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.mrnf)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(covtmjaIO.getLife(), covrpf.getLife())
		|| isNE(covtmjaIO.getCoverage(), covrpf.getCoverage())
		|| isNE(covtmjaIO.getRider(), covrpf.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		/* If entering on Component Change then just Display the details*/
		/*   as no defaults are necessary.*/
		/* Else we must load the Tables required for defaults.*/
		/*   note - Both statuz's are checked as we may previously have*/
		/*          read the file directly or sequentially.*/
		wsaaIfCovt.set("Y");
		if (!addComp.isTrue()) {
			recordToScreen6000();
			goTo(GotoLabel.exit5090);
		}
		else {
			if (isEQ(covtmjaIO.getStatuz(), varcom.mrnf)
			|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
				covtmjaIO.setNonKey(SPACES);
				/*COVTMJA-RESERVE-UNITS-IND     */
				covtmjaIO.setAnbccd(1, ZERO);
				covtmjaIO.setAnbccd(2, ZERO);
				covtmjaIO.setSingp(ZERO);
				covtmjaIO.setPlanSuffix(ZERO);
				covtmjaIO.setInstprem(ZERO);
				covtmjaIO.setZbinstprem(ZERO);
				covtmjaIO.setZlinstprem(ZERO);
				covtmjaIO.setPremCessAge(ZERO);
				covtmjaIO.setPremCessTerm(ZERO);
				covtmjaIO.setRiskCessAge(ZERO);
				covtmjaIO.setRiskCessTerm(ZERO);
				covtmjaIO.setBenCessAge(ZERO);
				covtmjaIO.setBenCessTerm(ZERO);
				covtmjaIO.setEffdate(ZERO);
				covtmjaIO.setSumins(ZERO);
				covtmjaIO.setPremCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setRiskCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
				/*COVTMJA-RESERVE-UNITS-DATE    */
				covtmjaIO.setPolinc(chdrpf.getPolinc());
				covtmjaIO.setCrtable(wsaaCrtable);
				tableLoads7000();
			}
			else {
				tableLoads7000();
			}
		}
	}

protected void fieldsToScreen5080()
	{
		/* Move fields to screen.*/
		sv.planSuffix.set(covtmjaIO.getPlanSuffix());
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.premcess.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.mattcess.set(covtmjaIO.getRiskCessDate());
		sv.matage.set(covtmjaIO.getRiskCessAge());
		sv.mattrm.set(covtmjaIO.getRiskCessTerm());
		/*MOVE COVTMJA-RESERVE-UNITS-DATE TO SR575-RESERVE-UNITS-DATE. */
		/*MOVE COVTMJA-RESERVE-UNITS-IND  TO SR575-RESERVE-UNITS-IND.  */
		sv.polinc.set(covtmjaIO.getPolinc());
		sv.planSuffix.set(covtmjaIO.getPolinc());
		sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		if (isEQ(covtmjaIO.getInstprem(), 0)) {
			sv.singlePremium.set(covtmjaIO.getSingp());
			/******** MOVE ZERO                TO SR575-LUMP-CONTRIB            */
		}
		else {
			sv.singlePremium.set(covtmjaIO.getInstprem());
		}
		/* MOVE COVTMJA-SINGP       TO SR575-LUMP-CONTRIB.           */
		sv.sumin.set(covtmjaIO.getSumins());
		sv.zbinstprem.set(covtmjaIO.getZbinstprem());
		sv.zlinstprem.set(covtmjaIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtmjaIO.getLoadper());
		sv.rateadj.set(covtmjaIO.getRateadj());
		sv.fltmort.set(covtmjaIO.fltmort);
		sv.premadj.set(covtmjaIO.getPremadj());
		sv.adjustageamt.set(covtmjaIO.getAgeadj());
		/*BRD-306 END */
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		if (isEQ(t5687rec.premmeth, SPACES)) {
			/* Nothing to do. */
		}
		/*    If both the maximum and minimum values for the Sum*/
		/*    Insured on T5551 are zeros then no entry is allowed in*/
		/*    that field - protect and non-display it*/
		if ((isEQ(t5551rec.sumInsMax, ZERO)
		&& isEQ(t5551rec.sumInsMin, ZERO))) {
			sv.sumin.set(ZERO);
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)
		&& isNE(t5551rec.sumInsMax, 0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5551rec.sumInsMax);
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(sv.sumin, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())));
			}
		}
		/*  - if all   the   valid   mortality  classes  are  blank,*/
		/*       non-display  and  protect  this  field. If there is*/
		/*       only  one  mortality  class, display and protect it*/
		/*       (no validation will be required).*/
		if (isEQ(t5551rec.mortclss, SPACES)) {
			sv.mortcls.set(SPACES);
			sv.mortclsOut[varcom.nd.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5551rec.mortcls01, SPACES)
		&& isEQ(t5551rec.mortcls02, SPACES)
		&& isEQ(t5551rec.mortcls03, SPACES)
		&& isEQ(t5551rec.mortcls04, SPACES)
		&& isEQ(t5551rec.mortcls05, SPACES)
		&& isEQ(t5551rec.mortcls06, SPACES)) {
			sv.mortcls.set(t5551rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*  - if all the valid lien codes are blank, non-display and*/
		/*       protect  this field. Otherwise, if there is only one      */
		/*       lien code then move this to the screen, protect it        */
		/*       and  display it. if there are more than two lien          */
		/*       codes then leave unprotected for the user to enter        */
		/*       their choice.                                             */
		if (isEQ(t5551rec.liencds, SPACES)) {
			sv.liencd.set(SPACES);
			/*sv.liencdOut[varcom.nd.toInt()].set("Y");*/
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5551rec.liencd01, SPACES)
		&& isEQ(t5551rec.liencd02, SPACES)
		&& isEQ(t5551rec.liencd03, SPACES)
		&& isEQ(t5551rec.liencd04, SPACES)
		&& isEQ(t5551rec.liencd05, SPACES)
		&& isEQ(t5551rec.liencd06, SPACES)) {
			sv.liencd.set(t5551rec.liencd01);
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		/* If both the maximum and minimum values for the Lump Sum      */
		/* Payment on T5551 are zeros then no entry is allowed in       */
		/* that field - protect and non-display it                      */
		/* IF (T5551-MAX-LUMP-SUM      = ZERO AND                       */
		/*     T5551-MIN-LUMP-SUM      = ZERO)                          */
		/*     MOVE ZEROS              TO SR575-LUMP-CONTRIB            */
		/*     MOVE 'Y'                TO SR575-LMPCNT-OUT(PR)          */
		/*                                SR575-LMPCNT-OUT(ND).         */
		/* IF T5551-MAX-LUMP-SUM       = T5551-MIN-LUMP-SUM AND         */
		/*                             NOT = 0                          */
		/*    MOVE 'Y'                 TO SR575-LMPCNT-OUT(PR)          */
		/*    MOVE T5551-MAX-LUMP-SUM  TO SR575-LUMP-CONTRIB            */
		/*    IF PLAN                                                   */
		/*       COMPUTE SR575-LUMP-CONTRIB ROUNDED =                   */
		/*                             ( SR575-LUMP-CONTRIB             */
		/*                             * ( CHDRMJA-POLSUM - 1 )         */
		/*                             / CHDRMJA-POLSUM).               */
		/*  - using  the  age  next  birthday  (ANB at RCD) from the*/
		/*       applicable  life  (see above), look up Issue Age on*/
		/*       the AGE and TERM  sections.  If  the  age fits into*/
		/*       a "slot" in  one  of  these sections,  and the risk*/
		/*       cessation  limits   are  the   same,   default  and*/
		/*       protect the risk cessation fields. Also do the same*/
		/*       for the premium  cessation  details.  In this case,*/
		/*       also  calculate  the  risk  and  premium  cessation*/
		/*       dates.*/
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		checkDefaults1800();
		mattcess1500();
		premcess1550();
		if (isNE(t5551rec.eaage, SPACES)) {
			sv.premcessOut[varcom.pr.toInt()].set("Y");
			sv.mattcessOut[varcom.pr.toInt()].set("Y");
		}
		optionsAndExtras5060();
		/* PERFORM 5070-REASSURANCE.                            <R96REA>*/
		checkAgnt1960();
	}

protected void optionsAndExtras5060()
	{
		/*PARA*/
		/* OPTIONS AND EXTRAS*/
		/* If options and extras are  not  allowed*/
		/* non-display and protect the fields.*/
		/* Otherwise,  read the  options  and  extras  details  for  the*/
		/* current coverage/rider.  If any  records  exist, put a '+' in*/
		/* the Options/Extras indicator (to show that there are some).*/
		if (isEQ(t5551rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext1900();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*5070-REASSURANCE SECTION.                                <R96REA>
	*5071-PARA.                                               <R96REA>
	**REASSURANCE                                             <R96REA>
	**If reassurance is not allowed                           <R96REA>
	**non-display and protect the field.                      <R96REA>
	****                                                      <R96REA>
	**Otherwise,  read the  reassurance details  for  the  cur<R96REA>
	**coverage/rider.  If any  records  exist, put a '+' in  t<R96REA>
	**reassurance indicator (to show that there is one).      <R96REA>
	**** IF T5687-RIIND              = 'N' OR SPACES          <R96REA>
	****    MOVE 'Y'                 TO SR575-RATYPIND-OUT (ND<R96REA>
	**** ELSE                                                 <R96REA>
	****    PERFORM 1950-CHECK-RACT.                          <R96REA>
	*5070-EXIT.                                               <R96REA>
	****  EXIT.                                               <R96REA>
	* This code has been commented out since it is never performed.
	*    If this is an Enquiry, protect all the fields except the
	*    Charge Options indicator.
	*    IF  COMP-ENQUIRY                                             
	*        MOVE 'Y'                TO SR575-MATAGE-OUT   (PR)       
	*                                   SR575-MATTCESS-OUT (PR)       
	*                                   SR575-MATTRM-OUT   (PR)       
	*                                   SR575-SUMIN-OUT    (PR)       
	*                                   SR575-OPTEXTIND-OUT (PR)      
	*                                   SR575-PCESSAGE-OUT (PR)       
	*                                   SR575-PCESSTRM-OUT (PR)       
	*                                   SR575-PREMCESS-OUT (PR)       
	*                                   SR575-MORTCLS-OUT  (PR)       
	*                                   SR575-LIENCD-OUT   (PR)       
	*                                   SR575-SINGPRM-OUT  (PR)       
	*                                   SR575-LMPCNT-OUT  (PR)        
	*                                   SR575-RUNDTE-OUT   (PR)       
	*                                   SR575-RSUNIN-OUT   (PR)       
	*                                   SR575-PLNSFX-OUT   (PR)       
	*                                   SR575-STFUND-OUT   (PR)       
	*                                   SR575-STSECT-OUT   (PR)       
	*                                   SR575-STSSECT-OUT   (PR).     
	*5090-EXIT.                                                       
	*     EXIT.                                                       
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void readCovr5100()
	{
		/*READ-COVRMJA*/
		/* Read the Coverage file.*/
	covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
	for (Covrpf covr:covrpfList) {
		if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
				|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
				|| isNE(covr.getLife(),wsaaCovrLife)
				|| isNE(covr.getCoverage(),wsaaCovrCoverage)
				|| isNE(covr.getRider(),wsaaCovrRider)
				|| (!covrpfList.isEmpty())) {
					return;
				}
	}

		/*EXIT*/
	}

protected void calcPremium5200()
	{
		/*CALC-PREMIUM*/
		/* Use the premium-method selected from T5687, if not blank THEN*/
		/*   access T5675. This gives the subroutine for the calculation.*/
		/* If the benefit billing method is not blank, non-display and*/
		/*   protect the premium field (so skip the following).*/
		/* MOVE 'N'                    TO PREM-REQD.                    */
		wsaaPremStatuz.set("N");
		/*METHOD-TABLE*/
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/* MOVE 'Y'                 TO PREM-REQD                     */
			wsaaPremStatuz.set("Y");
		}
		else {
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
			t5675rec.t5675Rec.set(itemIO.getGenarea());
		}
		/*EXIT*/
	}

protected void getPrmh5700()
	{
		start5710();
		read5720();
	}

protected void start5710()
	{
		prmhactIO.setRecKeyData(SPACES);
		wsaaInPh = "N";
		prmhactIO.setChdrcoy(chdrpf.getChdrcoy());
		prmhactIO.setChdrnum(chdrpf.getChdrnum());
		prmhactIO.setTranno(99999);
		prmhactIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		prmhactIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		prmhactIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void read5720()
	{
		SmartFileCode.execute(appVars, prmhactIO);
		if (isNE(prmhactIO.getStatuz(), varcom.oK)
		|| isNE(prmhactIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(prmhactIO.getChdrnum(), chdrpf.getChdrnum())) {
			return ;
		}
		wsaaInPh = "Y";
		readTr51p1400();
		/*EXIT*/
	}

protected void checkCalcTax5800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5800();
				case skipTr52e5800: 
					skipTr52e5800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5800()
	{
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.skipTr52e5800);
		}
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
	}

protected void skipTr52e5800()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			txcalcrec.chdrnum.set(chdrpf.getChdrnum());
			txcalcrec.life.set(covrpf.getLife());
			txcalcrec.coverage.set(covrpf.getCoverage());
			txcalcrec.rider.set(covrpf.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covrpf.getCrtable());
			txcalcrec.cnttype.set(chdrpf.getCnttype());
			txcalcrec.register.set(chdrpf.getReg());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrpf.getCntcurr());
			wsaaCntCurr.set(chdrpf.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
				compute(txcalcrec.amountIn, 2).set(add(add(sv.singlePremium, sv.zlinstprem), sv.zbinstprem));
			}
			else {
				txcalcrec.amountIn.set(sv.singlePremium);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrpf.getBtdate());
			txcalcrec.tranno.set(chdrpf.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(txcalcrec.statuz);
				syserrrec.params.set(txcalcrec.linkRec);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(sv.singlePremium);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				if (isEQ(sv.taxind, SPACES)) {
					sv.taxind.set("+");
				}
			}
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void recordToScreen6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covrRecord6010();
					fieldsToScreen6020();
				case covtExist6025: 
					covtExist6025();
					enquiryProtect6030();
				case restOfEnquiry6060: 
					restOfEnquiry6060();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covrRecord6010()
	{
		/* Move the COVR Details to the COVT record as the Coding moves*/
		/*   the COVT fields to the screen.*/
		if (isEQ(covtmjaIO.getStatuz(), varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			wsaaIfCovt.set("N");
			covtmjaIO.setSex01(wsaaSex);
			covtmjaIO.setSex02(wsaaSex2);
			covtmjaIO.setAnbAtCcd01(wsaaAnbAtCcd);
			covtmjaIO.setAnbAtCcd02(wsaaAnbAtCcd2);
			covtmjaIO.setCrtable(covrpf.getCrtable());
			covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
			covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
			covtmjaIO.setRiskCessAge(covrpf.getRiskCessAge());
			covtmjaIO.setPremCessAge(covrpf.getPremCessAge());
			covtmjaIO.setRiskCessTerm(covrpf.getRiskCessTerm());
			covtmjaIO.setPremCessTerm(covrpf.getPremCessTerm());
			/*       MOVE ZERO                   TO COVTMJA-RISK-CESS-AGE <024>*/
			/*       MOVE ZERO                   TO COVTMJA-PREM-CESS-AGE <024>*/
			/*       MOVE ZERO                   TO COVTMJA-RISK-CESS-TERM<024>*/
			/*       MOVE ZERO                   TO COVTMJA-PREM-CESS-TERM<024>*/
			/*       MOVE COVRMJA-RISK-CESS-AGE  TO COVTMJA-RISK-CESS-AGE      */
			/*       MOVE COVRMJA-PREM-CESS-AGE  TO COVTMJA-PREM-CESS-AGE      */
			/*       MOVE COVRMJA-RISK-CESS-TERM TO COVTMJA-RISK-CESS-TERM     */
			/*       MOVE COVRMJA-PREM-CESS-TERM TO COVTMJA-PREM-CESS-TERM     */
			/*     MOVE ZERO                   TO COVTMJA-RISK-CESS-AGE <024>*/
			/*     MOVE ZERO                   TO COVTMJA-PREM-CESS-AGE <024>*/
			/*     MOVE ZERO                   TO COVTMJA-RISK-CESS-TERM<024>*/
			/*     MOVE ZERO                   TO COVTMJA-PREM-CESS-TERM<024>*/
			/*MOVE COVRMJA-RESERVE-UNITS-DATE TO                        */
			/*                          COVTMJA-RESERVE-UNITS-DATE      */
			/*MOVE COVRMJA-RESERVE-UNITS-IND TO                         */
			/*                          COVTMJA-RESERVE-UNITS-IND       */
			covtmjaIO.setSumins(covrpf.getSumins());
			covtmjaIO.setInstprem(covrpf.getInstprem());
			covtmjaIO.setZbinstprem(covrpf.getZbinstprem());
			covtmjaIO.setZlinstprem(covrpf.getZlinstprem());
			covtmjaIO.setMortcls(covrpf.getMortcls());
			covtmjaIO.setLiencd(covrpf.getLiencd());
			covtmjaIO.setJlife(covrpf.getJlife());
			/*  MOVE CHDRMJA-BTDATE         TO COVTMJA-EFFDATE            */
			/*  MOVE CHDRMJA-BILLFREQ       TO COVTMJA-BILLFREQ           */
			/*  MOVE CHDRMJA-BILLCHNL       TO COVTMJA-BILLCHNL           */
			covtmjaIO.setEffdate(payrIO.getBtdate());
			if (flexiblePremium.isTrue()) {
				covtmjaIO.setEffdate(wsaaTargFrom);
			}
			/*    END-IF.                                           <Q11>   */
			covtmjaIO.setBillfreq(payrIO.getBillfreq());
			covtmjaIO.setBillchnl(payrIO.getBillchnl());
			covtmjaIO.setSingp(covrpf.getSingp());
			sv.statFund.set(covrpf.getStatFund());
			sv.statSect.set(covrpf.getStatSect());
			sv.statSubsect.set(covrpf.getStatSubsect());
			covtmjaIO.setPayrseqno(covrpf.getPayrseqno());
			covtmjaIO.setPolinc(chdrpf.getPolinc());
		}
	}

protected void fieldsToScreen6020()
	{
		/* IF COVTMJA-INSTPREM         = 0                              */
		/*    MOVE ZERO                TO SR575-LUMP-CONTRIB            */
		/* ELSE                                                         */
		/*    MOVE COVTMJA-SINGP       TO SR575-LUMP-CONTRIB.           */
		if (isEQ(covtmjaIO.getStatuz(), varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.covtExist6025);
		}
		/* If a summarised record then breakout the Inst.Premium & Sumin*/
		/*   for the number of summarised policies within the Plan, this*/
		/*   is only applicable if a COVT does not already exist in which*/
		/*   case it has already been broken down before.*/
		/* Note - the 1st policy in the summarised record will contain*/
		/*        any division remainder.*/
		if (isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& !plan.isTrue()) {
			if (isEQ(sv.planSuffix, 1)) {
				compute(sv.sumin, 2).set((sub(covtmjaIO.getSumins(), (div(mult(covtmjaIO.getSumins(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
				compute(sv.singlePremium, 2).set((sub(covtmjaIO.getInstprem(), (div(mult(covtmjaIO.getInstprem(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
				compute(sv.zbinstprem, 2).set((sub(covtmjaIO.getZbinstprem(), (div(mult(covtmjaIO.getZbinstprem(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
				compute(sv.zlinstprem, 2).set((sub(covtmjaIO.getZlinstprem(), (div(mult(covtmjaIO.getZlinstprem(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
			}
			else {
				compute(sv.sumin, 3).setRounded(div(covtmjaIO.getSumins(), chdrpf.getPolsum()));
				compute(sv.singlePremium, 3).setRounded(div(covtmjaIO.getInstprem(), chdrpf.getPolsum()));
				compute(sv.zbinstprem, 3).setRounded(div(covtmjaIO.getZbinstprem(), chdrpf.getPolsum()));
				compute(sv.zlinstprem, 3).setRounded(div(covtmjaIO.getZlinstprem(), chdrpf.getPolsum()));
			}
		}
		else {
			sv.sumin.set(covtmjaIO.getSumins());
			sv.zbinstprem.set(covtmjaIO.getZbinstprem());
			sv.zlinstprem.set(covtmjaIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtmjaIO.getLoadper());
			sv.rateadj.set(covtmjaIO.getRateadj());
			sv.fltmort.set(covtmjaIO.fltmort);
			sv.premadj.set(covtmjaIO.getPremadj());
			sv.adjustageamt.set(covtmjaIO.getAgeadj());
			/*BRD-306 END */
			/*IF CHDRMJA-BILLFREQ      = ZEROS                     <005>*/
			if (isEQ(payrIO.getBillfreq(), ZERO)) {
				sv.singlePremium.set(covtmjaIO.getSingp());
			}
			else {
				sv.singlePremium.set(covtmjaIO.getInstprem());
			}
		}
		if (isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& !plan.isTrue()) {
			if (isEQ(sv.planSuffix, 1)) {
				compute(wsaaOldSumins, 2).set((sub(wsaaOldSumins, (div(mult(wsaaOldSumins, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
				compute(wsaaOldPrem, 2).set((sub(wsaaOldPrem, (div(mult(wsaaOldPrem, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
			}
			else {
				compute(wsaaOldSumins, 3).setRounded(div(wsaaOldSumins, chdrpf.getPolsum()));
				compute(wsaaOldPrem, 3).setRounded(div(wsaaOldPrem, chdrpf.getPolsum()));
			}
		}
	}

protected void covtExist6025()
	{
		if (isEQ(covtmjaIO.getStatuz(), varcom.oK)) {
			sv.sumin.set(covtmjaIO.getSumins());
			sv.zbinstprem.set(covtmjaIO.getZbinstprem());
			sv.zlinstprem.set(covtmjaIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtmjaIO.getLoadper());
			sv.rateadj.set(covtmjaIO.getRateadj());
			sv.fltmort.set(covtmjaIO.fltmort);
			sv.premadj.set(covtmjaIO.getPremadj());
			sv.adjustageamt.set(covtmjaIO.getAgeadj());
			/*BRD-306 END */
			if (isEQ(covtmjaIO.getInstprem(), ZERO)) {
				sv.singlePremium.set(covtmjaIO.getSingp());
			}
			else {
				sv.singlePremium.set(covtmjaIO.getInstprem());
			}
		}
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.premcess.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.mattcess.set(covtmjaIO.getRiskCessDate());
		sv.matage.set(covtmjaIO.getRiskCessAge());
		sv.mattrm.set(covtmjaIO.getRiskCessTerm());
		/*MOVE COVTMJA-RESERVE-UNITS-DATE TO SR575-RESERVE-UNITS-DATE. */
		/*MOVE COVTMJA-RESERVE-UNITS-IND  TO SR575-RESERVE-UNITS-IND.  */
		sv.polinc.set(covtmjaIO.getPolinc());
	}

protected void enquiryProtect6030()
	{
		/* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to*/
		/*   protect all input  capable  fields  except  the  indicators*/
		/*   controlling where to  switch  to  next  (options and extras*/
		/*   indicator).*/
		/*    IF  COMP-ENQUIRY                                             */
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.matageOut[varcom.pr.toInt()].set("Y");
			sv.mattcessOut[varcom.pr.toInt()].set("Y");
			sv.mattrmOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.premcessOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.singprmOut[varcom.pr.toInt()].set("Y");
			sv.plnsfxOut[varcom.pr.toInt()].set("Y");
			sv.stfundOut[varcom.pr.toInt()].set("Y");
			sv.stsectOut[varcom.pr.toInt()].set("Y");
			sv.stssectOut[varcom.pr.toInt()].set("Y");
		}
		/* If we have a record to display then we must load all the tables*/
		/*   required for Validation in the 2000-section.*/
		if (!addComp.isTrue()) {
			tableLoads7000();
		}
		optionsAndExtras5060();
		/* PERFORM 5070-REASSURANCE.                            <R96REA>*/
		checkAgnt1960();
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		/* If enquiry then no validation required so skip following        */
		/* paragraph.                                                      */
		/*    IF COMP-ENQUIRY                                      <V4L004>*/
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.restOfEnquiry6060);
		}
		/* This code between the ------------- lines                       */
		/*   was added so Sum Insured is protected and nondisplay          */
		/*   for both add and modify whereas before it was only performed  */
		/*   for add.                                                      */
		/*    If both the maximum and minimum values for the Sum           */
		/*    Insured on T5551 are zeros then no entry is allowed in       */
		/*    that field - protect and non-display it                      */
		if ((isEQ(t5551rec.sumInsMax, ZERO)
		&& isEQ(t5551rec.sumInsMin, ZERO))) {
			sv.sumin.set(ZERO);
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t5551rec.sumInsMax, t5551rec.sumInsMin)
		&& isNE(t5551rec.sumInsMax, 0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5551rec.sumInsMax);
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(sv.sumin, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())));
			}
		}
		/*  - if all   the   valid   mortality  classes  are  blank,       */
		/*       non-display  and  protect  this  field. If there is       */
		/*       only  one  mortality  class, display and protect it       */
		/*       (no validation will be required).                         */
		if (isEQ(t5551rec.mortclss, SPACES)) {
			sv.mortcls.set(SPACES);
			sv.mortclsOut[varcom.nd.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5551rec.mortcls01, SPACES)
		&& isEQ(t5551rec.mortcls02, SPACES)
		&& isEQ(t5551rec.mortcls03, SPACES)
		&& isEQ(t5551rec.mortcls04, SPACES)
		&& isEQ(t5551rec.mortcls05, SPACES)
		&& isEQ(t5551rec.mortcls06, SPACES)) {
			sv.mortcls.set(t5551rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*  - if all the valid lien codes are blank, non-display and       */
		/*       protect  this field. Otherwise, this is an optional       */
		/*       field.                                                    */
		if (isEQ(t5551rec.liencds, SPACES)) {
			sv.liencd.set(SPACES);
			sv.liencdOut[varcom.nd.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5551rec.liencd01, SPACES)
		&& isEQ(t5551rec.liencd02, SPACES)
		&& isEQ(t5551rec.liencd03, SPACES)
		&& isEQ(t5551rec.liencd04, SPACES)
		&& isEQ(t5551rec.liencd05, SPACES)
		&& isEQ(t5551rec.liencd06, SPACES)) {
			sv.liencd.set(t5551rec.liencd01);
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		/*    If both the maximum and minimum values for the Lump Sum      */
		/*    Payment on T5551 are zeros then no entry is allowed in       */
		/*    that field - protect and non-display it                      */
		/*                                                            <0*/
		/* IF (T5551-MAX-LUMP-SUM      = ZERO AND                     <0*/
		/*     T5551-MIN-LUMP-SUM      = ZERO)                        <0*/
		/*     MOVE ZEROS              TO SR575-LUMP-CONTRIB          <0*/
		/*     MOVE 'Y'                TO SR575-LMPCNT-OUT(PR)        <0*/
		/*                                SR575-LMPCNT-OUT(ND).       <0*/
		/*                                                            <0*/
		/* IF T5551-MAX-LUMP-SUM       = T5551-MIN-LUMP-SUM AND       <0*/
		/*                             NOT = 0                        <0*/
		/*    MOVE 'Y'                 TO SR575-LMPCNT-OUT (PR)       <0*/
		/*    MOVE T5551-MAX-LUMP-SUM  TO SR575-LUMP-CONTRIB          <0*/
		/*    IF PLAN                                                 <0*/
		/*       COMPUTE SR575-LUMP-CONTRIB ROUNDED =                 <0*/
		/*                             ( SR575-LUMP-CONTRIB           <0*/
		/*                             * ( CHDRMJA-POLSUM - 1 )       <0*/
		/*                             / CHDRMJA-POLSUM).             <0*/
		/*                                                            <0*/
		/*                                                            <0*/
		/* Lump sum must always be protected in modify mode.          <0*/
		/*                                                            <0*/
		/* IF MODIFY-COMP                                             <0*/
		/*    MOVE 'Y'                 TO SR575-LMPCNT-OUT (PR).      <0*/
		/*                                                            <0*/
		/*  - using  the  age  next  birthday  (ANB at RCD) from the       */
		/*       applicable  life  (see above), look up Issue Age on       */
		/*       the AGE and TERM  sections.  If  the  age fits into       */
		/*       a "slot" in  one  of  these sections,  and the risk       */
		/*       cessation  limits   are  the   same,   default  and       */
		/*       protect the risk cessation fields. Also do the same       */
		/*       for the premium  cessation  details.  In this case,       */
		/*       also  calculate  the  risk  and  premium  cessation       */
		/*       dates.                                                    */
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		checkDefaults1800();
	}

protected void restOfEnquiry6060()
	{
		/*    IF COMP-ENQUIRY                                      <V4L004>*/
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			enquiryDisplay6080();
		}
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/*    If modify was requested then we must check to see if         */
		/*    Options And Extras are allowed by the table.                 */
		optionsAndExtras5060();
		/*EXIT*/
	}

protected void enquiryDisplay6080()
	{
		para6081();
	}

protected void para6081()
	{
		if (isEQ(sv.sumin, ZERO)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.suminOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(sv.mortcls, SPACES)) {
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(sv.liencd, SPACES)) {
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(sv.singlePremium, ZERO)) {
			sv.singprmOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void tableLoads7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t5687Load7010();
					t5671Load7020();
				case matchProgName: 
					matchProgName();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t5687Load7010()
	{
		/* General Coverage/Rider details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtmjaIO.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covtmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void t5671Load7020()
	{
		/* Coverage/Rider Switching.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(covtmjaIO.getCrtable());
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		/*MATCH PROG WITH PROG ON 5551***********/
		x.set(0);
	}

protected void matchProgName()
	{
		x.add(1);
		if (isGT(x, 4)) {
			scrnparams.errorCode.set(errorsInner.e302);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(t5671rec.pgm[x.toInt()], wsaaProg)) {
			wsbbTran.set(t5671rec.edtitm[x.toInt()]);
		}
		else {
			goTo(GotoLabel.matchProgName);
		}
		/*    Read T5551 to obtain the Unit Linked Edit Rules.*/
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setItemtabl(tablesInner.t5551);
		/*MOVE CHDRMJA-CNTCURR        TO WSBB-CURRENCY.                */
		wsbbCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isNE(itdmIO.getValidflag(), "1")) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
		}
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5551)
		|| isNE(itdmIO.getItemitem(), wsbbTranCurrency)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			fatalError600();
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void a100CheckLimit()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		chkrlrec.clntnum.set(sv.lifenum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.currcd);
		chkrlrec.cnttype.set(chdrpf.getCnttype());
		chkrlrec.crtable.set(covtmjaIO.getCrtable());
		chkrlrec.sumins.set(sv.sumin);
		chkrlrec.life.set(covtmjaIO.getLife());
		chkrlrec.chdrnum.set(chdrpf.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz, varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.suminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}

protected void a200CallRounding()
	{
		/*A200-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A290-EXIT*/
	}

protected void validateOccupationOrOccupationClass() 
	{
		readTa610();
		String occupation = lifemjaIO.getOccup().toString();
		if( occupation != null && !occupation.trim().isEmpty()) {
			for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
					&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
				if(isEQ(occupation,ta610rec.occclassdes[wsaaCount.toInt()])){
					isFollowUpRequired = true;
					scrnparams.errorCode.set(errorsInner.rrsu);
					break;
				}
			}
			
			if(!isFollowUpRequired){
				getOccupationClass2900(occupation);
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu);
						break;
					}
				}	
			}
		}
	}

protected void getOccupationClass2900(String occupation) 
	{
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
	}

protected void readTa610() 
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tA610);
		itempf.setItemitem(covtmjaIO.getCrtable().toString());
		itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
		itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
		itempfList  = itempfDAO.findByItemDates(itempf);	//ICIL-1494
		if (itempfList.size()>0 && itempfList.get(0).getGenarea()!=null) {
			ta610rec.tA610Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}

protected void createFollowUps3400()
	{
		try {
			fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1523 */
			fupno = fluppfAvaList.size();	//ICIL-1494
			writeFollowUps3410();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void writeFollowUps3410()
	{
		fupno = 0;
		for (wsaaCount.set(1); !(isGT(wsaaCount, 5))
				&& isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
			writeFollowUp3500();
		}
		fluppfDAO.insertFlupRecord(fluppfList);
	}

protected void writeFollowUp3500()
	{
		try {
			fluppf = new Fluppf();
			lookUpStatus3510();
			lookUpDescription3520();
			writeRecord3530();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void lookUpStatus3510()
	{
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
	
		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520()
	{
		fupDescpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	}

private void writeRecord3530() 
	{
		fupno++;
		fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
		fluppf.setChdrnum(chdrpf.getChdrnum());
		setPrecision(fupno, 0);
		fluppf.setFupNo(fupno);
		fluppf.setLife("01");
		fluppf.setTranNo(chdrpf.getTranno());
		fluppf.setFupDt(wsaaToday.toInt());
		fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString().trim());
		fluppf.setFupTyp('P');
		fluppf.setFupRmk(fupDescpf.getLongdesc());
		fluppf.setjLife("00");
		fluppf.setTrdt(varcom.vrcmDate.toInt());
		fluppf.setTrtm(varcom.vrcmTime.toInt());
		fluppf.setUserT(varcom.vrcmUser.toInt());
		fluppf.setEffDate(wsaaToday.toInt());
		fluppf.setCrtUser(varcom.vrcmUser.toString());
		fluppf.setCrtDate(wsaaToday.toInt());
		fluppf.setLstUpUser(varcom.vrcmUser.toString());
		fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
		fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		fluppfList.add(fluppf);
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e302 = new FixedLengthStringData(4).init("E302");
	private FixedLengthStringData e366 = new FixedLengthStringData(4).init("E366");
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData ev01 = new FixedLengthStringData(4).init("EV01");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f404 = new FixedLengthStringData(4).init("F404");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h033 = new FixedLengthStringData(4).init("H033");
	private FixedLengthStringData h044 = new FixedLengthStringData(4).init("H044");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f862 = new FixedLengthStringData(4).init("F862");
	private FixedLengthStringData d040 = new FixedLengthStringData(4).init("D040");
	private FixedLengthStringData d041 = new FixedLengthStringData(4).init("D041");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData f990 = new FixedLengthStringData(4).init("F990");
	private FixedLengthStringData rlbq = new FixedLengthStringData(4).init("RLBQ");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t5551 = new FixedLengthStringData(5).init("T5551");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData tr51p = new FixedLengthStringData(5).init("TR51P");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");//BRD-306
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrrgwrec = new FixedLengthStringData(10).init("COVRRGWREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
}
}