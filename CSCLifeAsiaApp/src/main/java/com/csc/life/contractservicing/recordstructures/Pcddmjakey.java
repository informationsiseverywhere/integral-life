package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:37
 * Description:
 * Copybook name: PCDDMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pcddmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData pcddmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData pcddmjaKey = new FixedLengthStringData(64).isAPartOf(pcddmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData pcddmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(pcddmjaKey, 0);
  	public FixedLengthStringData pcddmjaChdrnum = new FixedLengthStringData(8).isAPartOf(pcddmjaKey, 1);
  	public FixedLengthStringData pcddmjaAgntnum = new FixedLengthStringData(8).isAPartOf(pcddmjaKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(pcddmjaKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(pcddmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		pcddmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}