package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:37
 * Description:
 * Copybook name: CWFDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cwfdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData cwfdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData cwfdKey = new FixedLengthStringData(64).isAPartOf(cwfdFileKey, 0, REDEFINE);
  	public FixedLengthStringData cwfdChdrcoy = new FixedLengthStringData(1).isAPartOf(cwfdKey, 0);
  	public FixedLengthStringData cwfdChdrnum = new FixedLengthStringData(8).isAPartOf(cwfdKey, 1);
  	public PackedDecimalData cwfdPrcSeqNbr = new PackedDecimalData(5, 0).isAPartOf(cwfdKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(cwfdKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(cwfdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		cwfdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}