package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5077
 * @version 1.0 generated on 30/08/09 06:33
 * @author Quipoz
 */
public class S5077ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(947);
	public FixedLengthStringData dataFields = new FixedLengthStringData(547).isAPartOf(dataArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,86);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,116);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,117);
	public FixedLengthStringData oragntnam = DD.oragntnam.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,219);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,266);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,274);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,284);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,292);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,296);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,346);
	//ICIL-149 start
		public FixedLengthStringData bnkout = DD.bnkout.copy().isAPartOf(dataFields,356);
		public FixedLengthStringData bnksm = DD.bnksm.copy().isAPartOf(dataFields,376);
		public FixedLengthStringData bnktel = DD.bnktel.copy().isAPartOf(dataFields,386);
		public FixedLengthStringData bnkoutname = DD.bnkoutname.copy().isAPartOf(dataFields,406);
		public FixedLengthStringData bnksmname = DD.bnksmname.copy().isAPartOf(dataFields,453);
		public FixedLengthStringData bnktelname = DD.bnktelname.copy().isAPartOf(dataFields,500);//ICIL-149 end
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(100).isAPartOf(dataArea, 547);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData oragntnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData bnkoutErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);//ICIL-149 start
	public FixedLengthStringData bnksmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData bnktelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData bnkoutnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData bnksmnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData bnktelnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);//ICIL-149 end
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(300).isAPartOf(dataArea, 647);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] oragntnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] bnkoutOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);//ICIL-149 start
	public FixedLengthStringData[] bnksmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] bnktelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] bnkoutnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] bnksmnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] bnktelnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);//ICIL-149 end
	
	
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5077screenWritten = new LongData(0);
	public LongData S5077protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5077ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payrnumOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payornameOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnkoutOut,new String[] {"38","106","-38","78", null, null, null, null, null, null, null, null});//ICIL-149 
		fieldIndMap.put(bnktelOut,new String[] {"33","34","-33","79", null, null, null, null, null, null, null, null});//ICIL-149
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, rstate, pstate, cownnum, ownername, payrnum, payorname, agntnum, agentname, occdate, ptdate, billfreq, mop, agntsel, oragntnam, reasoncd, resndesc,bnkout,bnksm,bnktel,bnkoutname,bnksmname,bnktelname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, rstateOut, pstateOut, cownnumOut, ownernameOut, payrnumOut, payornameOut, agntnumOut, agentnameOut, occdateOut, ptdateOut, billfreqOut, mopOut, agntselOut, oragntnamOut, reasoncdOut, resndescOut,bnkoutOut,bnksmOut,bnktelOut,bnkoutnameOut,bnksmnameOut,bnktelnameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, rstateErr, pstateErr, cownnumErr, ownernameErr, payrnumErr, payornameErr, agntnumErr, agentnameErr, occdateErr, ptdateErr, billfreqErr, mopErr, agntselErr, oragntnamErr, reasoncdErr, resndescErr,bnkoutErr,bnksmErr,bnktelErr,bnkoutnameErr,bnksmnameErr,bnktelnameErr};
		screenDateFields = new BaseData[] {occdate, ptdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5077screen.class;
		protectRecord = S5077protect.class;
	}

}
