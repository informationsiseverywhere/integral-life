package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:40
 * Description:
 * Copybook name: FLUPREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fluprevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fluprevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData fluprevKey = new FixedLengthStringData(256).isAPartOf(fluprevFileKey, 0, REDEFINE);
  	public FixedLengthStringData fluprevChdrcoy = new FixedLengthStringData(1).isAPartOf(fluprevKey, 0);
  	public FixedLengthStringData fluprevChdrnum = new FixedLengthStringData(8).isAPartOf(fluprevKey, 1);
  	public PackedDecimalData fluprevTranno = new PackedDecimalData(5, 0).isAPartOf(fluprevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(fluprevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fluprevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fluprevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}