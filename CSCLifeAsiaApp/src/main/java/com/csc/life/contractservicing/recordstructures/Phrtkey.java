package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:48
 * Description:
 * Copybook name: PHRTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Phrtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData phrtFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData phrtKey = new FixedLengthStringData(256).isAPartOf(phrtFileKey, 0, REDEFINE);
  	public FixedLengthStringData phrtChdrcoy = new FixedLengthStringData(1).isAPartOf(phrtKey, 0);
  	public FixedLengthStringData phrtChdrnum = new FixedLengthStringData(8).isAPartOf(phrtKey, 1);
  	public PackedDecimalData phrtTranno = new PackedDecimalData(5, 0).isAPartOf(phrtKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(phrtKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(phrtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		phrtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}