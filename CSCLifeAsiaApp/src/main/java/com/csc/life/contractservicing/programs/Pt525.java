/*
 * File: Pt525.java
 * Date: 30 August 2009 2:01:25
 * Author: Quipoz Limited
 * 
 * Class transformed from PT525.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.screens.St525ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pt525 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PT525");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSflcnt = new PackedDecimalData(3, 0);
		/* TABLES */
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t5688 = "T5688";
	private String t3629 = "T3629";
	private String lifelnbrec = "LIFELNBREC";
	private String tpoldbtrec = "TPOLDBTREC";
	private String cltsrec = "CLTSREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Policy Debt*/
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	private St525ScreenVars sv = ScreenProgram.getScreenVars( St525ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		read1310, 
		exit5009, 
		preExit
	}

	public Pt525() {
		super();
		screenVars = sv;
		new ScreenModel("St525", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{	
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("ST525", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		descIO.setRecKeyData(SPACES);
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setDesctabl(t5688);
		getDesc1100();
		sv.ctypedes.set(descIO.getLongdesc());
		getLife1300();
		cltsIO.setRecKeyData(SPACES);
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		getClientDetails1200();
		sv.linsname.set(wsspcomn.longconfname);
		cltsIO.setRecKeyData(SPACES);
		sv.cownnum.set(chdrlnbIO.getCownnum());
		cltsIO.setClntnum(chdrlnbIO.getCownnum());
		getClientDetails1200();
		sv.ownername.set(wsspcomn.longconfname);
		sv.occdate.set(chdrlnbIO.getOccdate());
		descIO.setRecKeyData(SPACES);
		descIO.setDescitem(chdrlnbIO.getStatcode());
		descIO.setDesctabl(t3623);
		getDesc1100();
		sv.rstate.set(descIO.getShortdesc());
		descIO.setRecKeyData(SPACES);
		descIO.setDescitem(chdrlnbIO.getPstatcode());
		descIO.setDesctabl(t3588);
		getDesc1100();
		sv.pstate.set(descIO.getShortdesc());
		sv.ptdate.set(chdrlnbIO.getPtdate());
		sv.btdate.set(chdrlnbIO.getBtdate());
		descIO.setRecKeyData(SPACES);
		sv.currcd.set(chdrlnbIO.getCntcurr());
		descIO.setDescitem(chdrlnbIO.getCntcurr());
		descIO.setDesctabl(t3629);
		getDesc1100();
		sv.currds.set(descIO.getShortdesc());
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setStatuz(varcom.oK);
		tpoldbtIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrlnbIO.getChdrnum());
		tpoldbtIO.setFormat(tpoldbtrec);
		tpoldbtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		tpoldbtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		tpoldbtIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)
		&& isNE(tpoldbtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			fatalError600();
		}
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)
		|| isNE(tpoldbtIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(tpoldbtIO.getChdrnum(),chdrlnbIO.getChdrnum())) {
			tpoldbtIO.setStatuz(varcom.endp);
		}
		wsaaSflcnt.set(ZERO);
		while ( !(isEQ(tpoldbtIO.getStatuz(),varcom.endp)
		|| isGTE(wsaaSflcnt,sv.subfilePage))) {
			loadSubfile5000();
		}
		
	}

protected void getDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		read1210();
	}

protected void read1210()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			wsspcomn.longconfname.fill("?");
		}
		else {
			plainname();
		}
	}

protected void getLife1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					getLifePara1300();
				}
				case read1310: {
					read1310();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getLifePara1300()
	{
		lifelnbIO.setRecKeyData(SPACES);
		lifelnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void read1310()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrcoy(),lifelnbIO.getChdrcoy())
		|| isNE(chdrlnbIO.getChdrnum(),lifelnbIO.getChdrnum())
		|| isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isNE(lifelnbIO.getValidflag(),"1")) {
			lifelnbIO.setFunction(varcom.nextr);
			goTo(GotoLabel.read1310);
		}
		/*EXIT*/
	}

protected void loadSubfile5000()
	{
		try {
			loadSubfilePara5000();
		}
		catch (GOTOException e){
		}
	}

protected void loadSubfilePara5000()
	{
		sv.tranno.set(tpoldbtIO.getTranno());
		sv.zbamt.set(tpoldbtIO.getOrgamnt());
		sv.tdbtrate.set(tpoldbtIO.getTdbtrate());
		sv.fromdate.set(tpoldbtIO.getFromdate());
		sv.todate.set(tpoldbtIO.getTodate());
		sv.payable.set(tpoldbtIO.getTdbtamt());
		sv.lngdes.set(tpoldbtIO.getTdbtdesc());
		scrnparams.function.set(varcom.sadd);
		processScreen("ST525", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaSflcnt.add(1);
		tpoldbtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)
		&& isNE(tpoldbtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			fatalError600();
		}
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)
		|| isNE(tpoldbtIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(tpoldbtIO.getChdrnum(),chdrlnbIO.getChdrnum())) {
			tpoldbtIO.setStatuz(varcom.endp);
			scrnparams.subfileMore.set(SPACES);
			goTo(GotoLabel.exit5009);
		}
		scrnparams.subfileMore.set("Y");
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaSflcnt.set(ZERO);
			while ( !(isEQ(tpoldbtIO.getStatuz(),varcom.endp)
			|| isGTE(wsaaSflcnt,sv.subfilePage))) {
				loadSubfile5000();
			}
			
			wsspcomn.edterror.set("Y");
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
