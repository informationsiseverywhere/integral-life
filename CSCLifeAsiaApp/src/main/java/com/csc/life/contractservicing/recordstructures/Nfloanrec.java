package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:49
 * Description:
 * Copybook name: NFLOANREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Nfloanrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData nfloanRec = new FixedLengthStringData(58);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(nfloanRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(nfloanRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(nfloanRec, 5);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(nfloanRec, 13);
  	public FixedLengthStringData openPrintFlag = new FixedLengthStringData(1).isAPartOf(nfloanRec, 18);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(nfloanRec, 19);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(nfloanRec, 21);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(nfloanRec, 22);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(nfloanRec, 23);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(nfloanRec, 26);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(nfloanRec, 29);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(nfloanRec, 31);
  	public PackedDecimalData polinc = new PackedDecimalData(4, 0).isAPartOf(nfloanRec, 34);
  	public ZonedDecimalData ptdate = new ZonedDecimalData(8, 0).isAPartOf(nfloanRec, 37).setUnsigned();
  	public FixedLengthStringData batcBatctrcde = new FixedLengthStringData(4).isAPartOf(nfloanRec, 45);
  	public PackedDecimalData outstamt = new PackedDecimalData(17, 2).isAPartOf(nfloanRec, 49);


	public void initialize() {
		COBOLFunctions.initialize(nfloanRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		nfloanRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}