package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5047
 * @version 1.0 generated on 30/08/09 06:32
 * @author Quipoz
 */
public class S5047ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(578);
	public FixedLengthStringData dataFields = new FixedLengthStringData(290).isAPartOf(dataArea, 0);
	public FixedLengthStringData ceaseInd = DD.ceaseind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData reversalInd = DD.revind.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,21);
	public ZonedDecimalData cpiDate = DD.cpidte.copyToZonedDecimal().isAPartOf(dataFields,29);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,37);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,97);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,105);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,152);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,160);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,207);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,215);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,262);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,272);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,280);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 290);
	public FixedLengthStringData ceaseindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData revindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cpidteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 362);
	public FixedLengthStringData[] ceaseindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] revindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cpidteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(286);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(92).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(subfileFields,2);
	public FixedLengthStringData hiddenAnnvryMethod = DD.hannvry.copy().isAPartOf(subfileFields,10);
	public ZonedDecimalData hrrn = DD.hrrn.copyToZonedDecimal().isAPartOf(subfileFields,14);
	public FixedLengthStringData hselect = DD.hselect.copy().isAPartOf(subfileFields,23);
	public ZonedDecimalData lastInst = DD.lastinst.copyToZonedDecimal().isAPartOf(subfileFields,24);
	public ZonedDecimalData lastSumi = DD.lastsumi.copyToZonedDecimal().isAPartOf(subfileFields,41);
	public ZonedDecimalData newinst = DD.newinst.copyToZonedDecimal().isAPartOf(subfileFields,56);
	public ZonedDecimalData newsumi = DD.newsumi.copyToZonedDecimal().isAPartOf(subfileFields,73);
	public FixedLengthStringData refusalFlag = DD.refflag.copy().isAPartOf(subfileFields,88);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,89);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,91);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 92);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData hannvryErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hrrnErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hselectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData lastinstErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData lastsumiErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData newinstErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData newsumiErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData refflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(144).isAPartOf(subfileArea, 140);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] hannvryOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hrrnOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hselectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] lastinstOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] lastsumiOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] newinstOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] newsumiOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] refflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 284);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cpiDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);

	public LongData S5047screensflWritten = new LongData(0);
	public LongData S5047screenctlWritten = new LongData(0);
	public LongData S5047screenWritten = new LongData(0);
	public LongData S5047protectWritten = new LongData(0);
	public GeneralTable s5047screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5047screensfl;
	}

	public S5047ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01","04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ptdateOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btdateOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(revindOut,new String[] {"05","02","-05","04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ceaseindOut,new String[] {"06","02","-06","04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenSflFields = new BaseData[] {coverage, rider, newinst, lastInst, lastSumi, newsumi, crrcd, select, refusalFlag, hrrn, hselect, hiddenAnnvryMethod};
		screenSflOutFields = new BaseData[][] {coverageOut, riderOut, newinstOut, lastinstOut, lastsumiOut, newsumiOut, crrcdOut, selectOut, refflagOut, hrrnOut, hselectOut, hannvryOut};
		screenSflErrFields = new BaseData[] {coverageErr, riderErr, newinstErr, lastinstErr, lastsumiErr, newsumiErr, crrcdErr, selectErr, refflagErr, hrrnErr, hselectErr, hannvryErr};
		screenSflDateFields = new BaseData[] {crrcd};
		screenSflDateErrFields = new BaseData[] {crrcdErr};
		screenSflDateDispFields = new BaseData[] {crrcdDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, descrip, cpiDate, reversalInd, ceaseInd};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, descripOut, cpidteOut, revindOut, ceaseindOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, descripErr, cpidteErr, revindErr, ceaseindErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, cpiDate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, cpidteErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, cpiDateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5047screen.class;
		screenSflRecord = S5047screensfl.class;
		screenCtlRecord = S5047screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5047protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5047screenctl.lrec.pageSubfile);
	}
}
