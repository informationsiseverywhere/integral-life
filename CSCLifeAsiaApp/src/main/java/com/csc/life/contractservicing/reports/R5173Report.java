package com.csc.life.contractservicing.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5173.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5173Report extends SMARTReportLayout { 

	private FixedLengthStringData bschednam = new FixedLengthStringData(10);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData desc = new FixedLengthStringData(50);
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private FixedLengthStringData occdate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData tranno = new ZonedDecimalData(5, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5173Report() {
		super();
	}


	/**
	 * Print the XML for R5173d01
	 */
	public void printR5173d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		bschednam.setFieldName("bschednam");
		bschednam.setInternal(subString(recordData, 1, 10));
		desc.setFieldName("desc");
		desc.setInternal(subString(recordData, 11, 50));
		tranno.setFieldName("tranno");
		tranno.setInternal(subString(recordData, 61, 5));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 66, 10));
		printLayout("R5173d01",			// Record name
			new BaseData[]{			// Fields:
				bschednam,
				desc,
				tranno,
				effdate
			}
		);

	}

	/**
	 * Print the XML for R5173h01
	 */
	public void printR5173h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 32, 8));
		occdate.setFieldName("occdate");
		occdate.setInternal(subString(recordData, 40, 10));
		printLayout("R5173h01",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr,
				chdrnum,
				occdate
			}
		);

		currentPrintLine.set(13);
	}


}
