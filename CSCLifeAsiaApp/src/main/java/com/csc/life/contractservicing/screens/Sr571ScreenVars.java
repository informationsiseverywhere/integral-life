package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR571
 * @version 1.0 generated on 30/08/09 07:20
 * @author Quipoz
 */
public class Sr571ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,48);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,51);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,59);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,141);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,143);
	public ZonedDecimalData linstamt = DD.linstamt.copyToZonedDecimal().isAPartOf(dataFields,190);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,201);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,202);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,206);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,207);
	public ZonedDecimalData premCessDate = DD.pcesdte.copyToZonedDecimal().isAPartOf(dataFields,208);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,216);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,219);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,222);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,226);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,234);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,237);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,240);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,242);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,243);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,244);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,246);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,250);
	public FixedLengthStringData taxamts = new FixedLengthStringData(34).isAPartOf(dataFields, 265);
	public ZonedDecimalData[] taxamt = ZDArrayPartOfStructure(2, 17, 2, taxamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(34).isAPartOf(taxamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData taxamt01 = DD.taxamt.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData taxamt02 = DD.taxamt.copyToZonedDecimal().isAPartOf(filler,17);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,299);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,300);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,313);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,330);
	/*BRD-306 START */
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 347);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 350);
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 380);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 397);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 414);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 431);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 448);
	/*BRD-306 END */
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,465);//ILIFE-3421
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,466);//BRD-NBP-011
	public FixedLengthStringData exclind = DD.optextind.copy().isAPartOf(dataFields,469);
	public FixedLengthStringData fuind = DD.fuind.copy().isAPartOf(dataFields,470);
	public ZonedDecimalData cashvalarer = DD.cashvalare.copyToZonedDecimal().isAPartOf(dataFields,471);//ICIL-299
	//IBPLIFE-2134
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(dataFields,488);
	public FixedLengthStringData trcdedesc = DD.trandesc.copy().isAPartOf(dataFields,492);
	public FixedLengthStringData dateeff = DD.dateeff.copy().isAPartOf(dataFields,522);
	public FixedLengthStringData covrprpse = DD.covrprpse.copy().isAPartOf(dataFields,530);
			

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData linstamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData pcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData taxamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData[] taxamtErr = FLSArrayPartOfStructure(2, 4, taxamtsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(taxamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData taxamt01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData taxamt02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	/*BRD-306 START */
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	/*BRD-306 END */
	
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);//ILIFE-3421
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);//BRD-NBP-011
	public FixedLengthStringData exclindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData fuindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData cashvalarerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);//ICIL-299
	//IBPLIFE-2134
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData trcdedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData dateeffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData covrprpseErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
		
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] linstamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] pcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData taxamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 396);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(2, 12, taxamtsOut, 0);
	public FixedLengthStringData[][] taxamtO = FLSDArrayPartOfArrayStructure(12, 1, taxamtOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(taxamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] taxamt01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] taxamt02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	/*BRD-306 START */
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	/*BRD-306 END */
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);//ILIFE-3421
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);//BRD-NBP-011
	public FixedLengthStringData[] exclindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] fuindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] cashvalarerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);//ICIL-299
	public FixedLengthStringData[] riskCessAgeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);//ILJ-46
	//public FixedLengthStringData[] riskCessDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);//ILJ-46
	//IBPLIFE-2132
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
    public FixedLengthStringData[] trcdedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] dateeffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] covrprpseOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
		
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dateeffDisp = new FixedLengthStringData(10);//IBPLIFE-2134

	public LongData Sr571screenWritten = new LongData(0);
	public LongData Sr571protectWritten = new LongData(0);
	public FixedLengthStringData contnewBScreenflag = new FixedLengthStringData(1); 
	public boolean hasSubfile() {
		return false;
	}


	public Sr571ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminOut,new String[] {"14","02","-14","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcessageOut,new String[] {"15","03","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesstrmOut,new String[] {"16","04","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesdteOut,new String[] {"29","28","-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"19","06","-19","05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"20","08","-20","07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"21","09","-21","09",null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"22","31","-22","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"23","26","-23","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"33","34","-33","34",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {"36","37","-36","38",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zlinstpremOut,new String[] {null, null, null, "32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamtOut,new String[] {null, null, null, "32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamt01Out,new String[] {null, "74", null, "42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"43","42","-43","42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamt02Out,new String[] {"22","75", "-22","42",null, null, null, null, null, null, null, null});
		/*BRD-306 START */
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmbasisOut,new String[] {"56", "57", null, "58", null, null, null, null, null, null, null, null});
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(exclindOut,new String[] {null, null, null, "73", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuindOut,new String[] {"90","61", "-90","61", null, null, null, null, null, null, null, null});
		// fwang3 ICIL-560
		fieldIndMap.put(cashvalarerOut,new String[] {null, null, null, "76", null, null, null, null, null, null, null, null});
		fieldIndMap.put(riskCessAgeOut,new String[] {null, null, null, "123", null, null, null, null, null, null, null, null});//ILJ-46
		//fieldIndMap.put(riskCessDateOut,new String[] {null, null, null, "124", null, null, null, null, null, null, null, null});//ILJ-46

		/*screenFields = new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifcnum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, numpols, planSuffix, sumin, currcd, riskCessAge, riskCessTerm, riskCessDate, premCessAge, premCessTerm, premCessDate, mortcls, liencd, optextind, instPrem, comind, select, pbind, bappmeth, zlinstprem, effdate, linstamt, taxamt01, taxind, taxamt02, loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt, prmbasis, dialdownoption,exclind,fuind,cashvalarer};
		screenOutFields = new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, numpolsOut, plnsfxOut, suminOut, currcdOut, rcessageOut, rcesstrmOut, rcesdteOut, pcessageOut, pcesstrmOut, pcesdteOut, mortclsOut, liencdOut, optextindOut, instprmOut, comindOut, selectOut, pbindOut, bappmethOut, zlinstpremOut, effdateOut, linstamtOut, taxamt01Out, taxindOut, taxamt02Out,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut, prmbasisOut, dialdownoptionOut,exclindOut,fuindOut,cashvalarerOut};
		screenErrFields = new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, numpolsErr, plnsfxErr, suminErr, currcdErr, rcessageErr, rcesstrmErr, rcesdteErr, pcessageErr, pcesstrmErr, pcesdteErr, mortclsErr, liencdErr, optextindErr, instprmErr, comindErr, selectErr, pbindErr, bappmethErr, zlinstpremErr, effdateErr, linstamtErr, taxamt01Err, taxindErr, taxamt02Err,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr, prmbasisErr, dialdownoptionErr,exclindErr,fuindErr,cashvalarerErr};
		BRD-306 END 
		screenDateFields = new BaseData[] {riskCessDate, premCessDate, effdate};
		screenDateErrFields = new BaseData[] {rcesdteErr, pcesdteErr, effdateErr};
		screenDateDispFields = new BaseData[] {riskCessDateDisp, premCessDateDisp, effdateDisp};
		*/
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields =getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr571screen.class;
		protectRecord = Sr571protect.class;
	}
	
	public int getDataAreaSize()
	{
		return 1522;
	}
	public int getDataFieldsSize()
	{
		return 630;
	}
	public int getErrorIndicatorSize()
	{
		return 220;
	}
	public int getOutputFieldSize()
	{
		return 672;
	}
	public BaseData[] getscreenFields(){
		return new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifcnum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, numpols, planSuffix, sumin, currcd, riskCessAge, riskCessTerm, riskCessDate, premCessAge, premCessTerm, premCessDate, mortcls, liencd, optextind, instPrem, comind, select, pbind, bappmeth, zlinstprem, effdate, linstamt, taxamt01, taxind, taxamt02, loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt, prmbasis, dialdownoption,exclind,fuind,cashvalarer,trcode,trcdedesc,dateeff,covrprpse};
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, numpolsOut, plnsfxOut, suminOut, currcdOut, rcessageOut, rcesstrmOut, rcesdteOut, pcessageOut, pcesstrmOut, pcesdteOut, mortclsOut, liencdOut, optextindOut, instprmOut, comindOut, selectOut, pbindOut, bappmethOut, zlinstpremOut, effdateOut, linstamtOut, taxamt01Out, taxindOut, taxamt02Out,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut, prmbasisOut, dialdownoptionOut,exclindOut,fuindOut,cashvalarerOut,riskCessAgeOut,trcodeOut,trcdedescOut,dateeffOut,covrprpseOut};
	}
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, numpolsErr, plnsfxErr, suminErr, currcdErr, rcessageErr, rcesstrmErr, rcesdteErr, pcessageErr, pcesstrmErr, pcesdteErr, mortclsErr, liencdErr, optextindErr, instprmErr, comindErr, selectErr, pbindErr, bappmethErr, zlinstpremErr, effdateErr, linstamtErr, taxamt01Err, taxindErr, taxamt02Err,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr, prmbasisErr, dialdownoptionErr,exclindErr,fuindErr,cashvalarerErr,trcodeErr,trcdedescErr,dateeffErr,covrprpseErr};
	}
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {riskCessDate, premCessDate, effdate, dateeff};
	}
	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {riskCessDateDisp, premCessDateDisp, effdateDisp, dateeffDisp};
	}
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {rcesdteErr, pcesdteErr, effdateErr, dateeffErr};
	}

}
