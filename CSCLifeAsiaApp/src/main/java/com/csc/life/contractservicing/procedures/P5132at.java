/*
 * File: P5132at.java
 * Date: 30 August 2009 0:12:07
 * Author: Quipoz Limited
 *
 * Class transformed from P5132AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.tablestructures.Tr695rec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.tablestructures.Th502rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.recordstructures.Acomcalrec;
import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.flexiblepremium.dataaccess.FpcorevTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.procedures.Rlliadb;
import com.csc.life.productdefinition.procedures.Vpuubbl;
import com.csc.life.productdefinition.procedures.Vpxubbl;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Rlliadbrec;
import com.csc.life.productdefinition.recordstructures.Stdtallrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5676rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltmjaTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;

/**
* <pre>
*REMARKS.
*
*
*
*  P5132AT - Component Change AT Processing.
*  -----------------------------------------
*
*  This program will be run  under  AT  and  will  perform  the
*  general  functions for the finalisation of Component Change.
*  It will then ascertain whether or not  there  is  a  generic
*  component  specific  subroutine  to complete the change and,
*  if so, it will call it.
*
*  The  general  processing  will  carry  out   the   following
*  functions:
*
*  1.  Increment the TRANNO field on the Contract Header.
*
*  2.  Write a PTRN record to mark the transaction.
*
*  3.  Call the Breakout routine if required.
*
*  4.  Create a new COVR record from the COVT transaction
*      records.
*
*  5.  Rewrite the old COVR record with its VALIDFLAG set
*      to '2'.
*
*  6.  Delete the COVT records.
*
*  7.  Create new ULNK records from the UNLT transaction
*      records if they exist.
*
*  8.  Rewrite the old corresponding ULNK records with their
*      VALIDFLAG's set to '2'.
*
*  9.  Delete the UNLT records.
*
*  10. If an increase or decrease has occurred it will create
*      or update the necessary commission records, (on AGCM),
*      by calling the ACOMCALC subroutine.
*
*  11. If commission clawback is required it will create the
*      necessary G/L postings, (ACMV records), again in the
*      ACOMCALC subroutine.
*
*  12. Call the generic subroutine if one exists.
*
*  Processing:
*  -----------
*
*
*  Contract Header.
*  ----------------
*
*  The  ATMOD-PRIMARY-KEY will contain the Company and Contract
*  number  of  the  contract  being  processed.  Use  these  to
*  perform  a READH on CHDRMJA. Increment the TRANNO field by 1
*  and rewrite the Contract Header.
*
*
*  PTRN Record.
*  ------------
*
*  Write a PTRN record with the new TRANNO.
*
*  See the Notes section at the end of this  specification  for
*  a description of the field contents.
*
*  Breakout Processing.
*  --------------------
*
*
*  A  physical  breakout  will  be  required if any COVT record
*  exists for the transaction that has a Plan  Suffix  that  is
*  less  than  or equal to CHDRMJA-POLSUM. All the COVT records
*  will have to be read first just to determine whether or  not
*  this is necessary.
*
*  Use  the  Contract  Company  and Contract Number to read all
*  the COVT records that match on  this  portion  of  the  key.
*  Make  a  note of the lowest COVT-PLAN-SUFFIX. At the end, if
*  this is not greater than CHDRMJA-POLSUM then the  subroutine
*  BRKOUT  must  be called. Set the OLD-SUMMARY to the value of
*  CHDRMJA-POLSUM and the  NEW-SUMMARY  to  the  value  of  the
*  lowest COVT-PLAN-SUFFIX, then call 'BRKOUT'.
*
*
*  Coverage Record Processing.
*  ---------------------------
*
*  The  COVT  records  will  drive the processing for Component
*  Change  records,  Fund  Direction  records,   Options/Extras
*  records  and  Commission and Clawback records. For each COVT
*  record found the program should perform the  processing  for
*  COVT,   UNLT,   LEXT   and,   if  necessary  Commission  and
*  Commission Clawback.
*
*  Use the Contract Company and Contract  Number  to  read  all
*  the  COVT records that match on this portion of the key. For
*  each one read the  corresponding  COVR  record,  change  its
*  VALIDFLAG  to  '2'  and  rewrite  it.  Then write a new COVR
*  record from the COVT record  and  delete  the  COVT  record.
*  Perform  the  UNLT, LEXT, Commission and Commission Clawback
*  processing for all records associated with the COVT record.
*
*  The program must determine whether or  not  an  increase  or
*  decrease  has  occurred.  The  increase  or decrease will be
*  relevant to individual components. An increase  or  decrease
*  will  be  deemed  to have occurred if the new premium varies
*  from the old.
*
*  If an increase  or  decrease  has  occurred  the  commission
*  records  will  have  to be processed. If there is a decrease
*  then Commission Clawback may also have to be  processed.  If
*  there  is  no difference then there will be no Commission or
*  Clawback processing.
*
*
*  Fund Direction Processing.
*  --------------------------
*
*  Use the COVT key to read UNLT. If no record  is  found  then
*  there  is no Fund Direction processing. If one is found then
*  use the key  to  read  the  corresponding  ULNK  record  and
*  rewrite  it with its VALIDFLAG set to '2'. Create a new ULNK
*  record from the UNLT record and delete the UNLT record.
*
*
*  Commission Processing.
*  ----------------------
*
*  See the remarks in the ACOMCALC subroutine for details on
*   how the AGCM records are processed.
*
*  Generic Processing.
*  -------------------
*
*  The  program  will  now read T5671 with a key of Transaction
*  Code concatenated with the Component Code, (CRTABLE). If  no
*  entry  is found then no generic processing will be required.
*  If a record is found then  call  the  program  held  therein
*  with   a   linkage   section  containing  Contract  Company,
*  Contract Number and TRANNO.
*
*
*  Policy Suspense will need to be reduced by the premium of the
*  component. To do this call LIFRTRN which will create an RTRN;
*  Notes.
*  ------
*
*  Tables:
*  -------
*
*   T5671 - Coverage/Rider Switching           Key: Tran Code||CRTABLE
*   T5687 - General Coverage/Rider Details     Key: CRTABLE
*   T5729 - Flexible Premium details           Key: CRTABLE
*
*
*  PTRN Record:
*  ------------
*
*  . PTRN-BATCTRCDE         -  Transaction Code
*  . PTRN-CHDRCOY           -  CHDRMJA-CHDRCOY
*  . PTRN-CHDRNUM           -  CHDRMJA-CHDRNUM
*  . PTRN-TRANNO            -  CHDRMJA-TRANNO
*  . PTRN-TRANSACTION-DATE  -  Current Date
*  . PTRN-TRANSACTION-TIME  -  Current Time
*  . PTRN-PTRNEFF           -  Current Date
*  . PTRN-TERMID            -  Passed in Linkage
*  . PTRN-USER              -  Passed in Linkage.
*
*
* The general processing is:
* 1. Increment the TRANNO field on the Contract Header.
*
* 2. Write a PTRN record to mark the transaction.
*
* 3. Call the Breakout routine if required, if it is called keep
*    the new updated CHDR which will have the latest details.
*
* 4. Create a new COVR record from the COVT transaction records.
*
* 5. Rewrite the old COVR record with its VALIDFLAG set
*    to '2'.
*
* 6  For Flexible Premiums, rewrite old FPCOs with validflag '2'.
*    Re-calculate FPCO details according to adjusted premium
*    from Component Modify.  Write new FPCO.
*
*    For a Component Add, calculate FPCO values,zeroise Premium
*    Rec'd, Premium Billed and Min Overdue fields and write new
*    record.
*
* 7. Delete the COVT records.
*
* 8. Create new ULNK records from the UNLT transaction
*    records if they exist.
*
* 9. Rewrite the old corresponding ULNK records with their
*    VALIDFLAG's set to '2'.
*
* 10.Delete the UNLT records.
*
* 11. If an increase or decrease has occurred it will create
*     or update the necessary commission records, (on AGCM),
*     by calling the ACOMCALC subroutine.
*
* 12. If commission clawback is required it will create the
*     necessary G/L Postings, (ACMV records), in the
*     ACOMCALC subroutine.
*
* 13. Call the generic subroutine if one exists.
*
* 14. Update the CHDR record with new instalment fields.
*
* X400-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through its life and report on any
* changes to it. The statistical records produced form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as its item name, the transaction code and
* the contract status of a contract, e.g. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, e.g. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, e.g. AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to COVRSTA records, and
* current categories refer to COVRSTS records. AGCMSTS
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the AGCMSTS, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*
*
*****************************************************************
* </pre>
*/

public class P5132at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5132AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
		/* WSAA-WORKING-STORAGE */
	private PackedDecimalData wsaaBrkoutSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaLastSuffix = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaBillfreqX = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreqX, 0).setUnsigned();

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransArea, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 24);
	private FixedLengthStringData filler1 = new FixedLengthStringData(175).isAPartOf(wsaaTransArea, 25, FILLER).init(SPACES);

	private FixedLengthStringData wsaaIfOvrdCommEnd = new FixedLengthStringData(1);
	private Validator wsaaEndOvrdComm = new Validator(wsaaIfOvrdCommEnd, "Y");
		/* STORE-OLDCOVR-DATES */
	private PackedDecimalData wsaaUnitstmtDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaRerateDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaRerateFromDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaAnivprocDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBenbillDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaIucancDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(13, 2).init(ZERO);
	private FixedLengthStringData wsaaT5448Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBonusInd = new FixedLengthStringData(1);
	private PackedDecimalData wsaaLextExtDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaFreqFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaStampDutyAcc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtAnnprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovrAnnprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinstamt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveZbinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSaveCommPrem = new PackedDecimalData(17, 2);      //IBPLIFE-5237
	private PackedDecimalData wsaaCovtSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtZbinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovtZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSuspPosting = new PackedDecimalData(17, 2);
	private static final int wsaaJrnseq = 0;
	private FixedLengthStringData wsaaBasicCommMeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBascpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSrvcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRnwcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBasscmth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBasscpy = new FixedLengthStringData(4);
	private PackedDecimalData wsaaCovtSuffix = new PackedDecimalData(4, 0);
	private String wsaaFirstUnltmja = "Y";
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaA = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaLextDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaUnltSeq = new PackedDecimalData(3, 0);
		/* WSAA-UALPRCS */
	private ZonedDecimalData[] wsaaUalprc = ZDInittedArray(10, 17, 2);

	private FixedLengthStringData wsaaSaveUalprcs = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaSaveUalprc = PDArrayPartOfStructure(10, 17, 2, wsaaSaveUalprcs, 0);

	private FixedLengthStringData wsaaUalprcAmounts = new FixedLengthStringData(90);
	private PackedDecimalData[] wsaaUalprcAmt = PDArrayPartOfStructure(10, 17, 2, wsaaUalprcAmounts, 0);
	private FixedLengthStringData wsaaStoreCovtKey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaCompkey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompkeyTranno = new FixedLengthStringData(4).isAPartOf(wsaaCompkey, 0);
	private FixedLengthStringData wsaaCompkeyCrtable = new FixedLengthStringData(4).isAPartOf(wsaaCompkey, 4);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private PackedDecimalData wsaaTotalTax = new PackedDecimalData(9, 5).init(0);

	private FixedLengthStringData wsaaProcessCompleteFlag = new FixedLengthStringData(1);
	private Validator processComplete = new Validator(wsaaProcessCompleteFlag, "Y");

	private FixedLengthStringData wsaaUlnkUpdate = new FixedLengthStringData(1);
	private Validator wsaaNoUlnkUpdate = new Validator(wsaaUlnkUpdate, "N");

	private FixedLengthStringData wsaaIfSplitReqd = new FixedLengthStringData(1);
	private Validator wsaaSplitNotReqd = new Validator(wsaaIfSplitReqd, "N");
	private Validator wsaaSplitReqd = new Validator(wsaaIfSplitReqd, "Y");

	private FixedLengthStringData wsaaEndProcessFlag = new FixedLengthStringData(1);
	private Validator processExit = new Validator(wsaaEndProcessFlag, "Y");

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");
	private PackedDecimalData wsaaOrigPtdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrPtdate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaDayTemp = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaDayOld = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYyOld = new ZonedDecimalData(4, 0).isAPartOf(wsaaDayOld, 0).setUnsigned();
	private ZonedDecimalData wsaaMmOld = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayOld, 4).setUnsigned();
	private ZonedDecimalData wsaaDdOld = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayOld, 6).setUnsigned();

	private FixedLengthStringData wsaaDayNew = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYyNew = new ZonedDecimalData(4, 0).isAPartOf(wsaaDayNew, 0).setUnsigned();
	private ZonedDecimalData wsaaMmNew = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayNew, 4).setUnsigned();
	private ZonedDecimalData wsaaDdNew = new ZonedDecimalData(2, 0).isAPartOf(wsaaDayNew, 6).setUnsigned();
	private ZonedDecimalData wsaaT5729Sub = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaNewTarget = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaNewBilled = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaNewBilledDecimal = new ZonedDecimalData(18, 7);
	private ZonedDecimalData wsaaNewOverdueMin = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOldBilled = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOldOverdueMin = new ZonedDecimalData(17, 2);

	private FixedLengthStringData wsaaCompChanged = new FixedLengthStringData(1).init(" ");
	private Validator compChanged = new Validator(wsaaCompChanged, "Y");

	private FixedLengthStringData wsaaCompAction = new FixedLengthStringData(1);
	private Validator wsaaCompAdd = new Validator(wsaaCompAction, "A");
	private Validator wsaaCompChg = new Validator(wsaaCompAction, "C");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaEarliestRerateDate = new PackedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaCpiValid = new FixedLengthStringData(1);
	private Validator cpiValid = new Validator(wsaaCpiValid, "Y");

	private FixedLengthStringData wsaaWopMatch = new FixedLengthStringData(1);
	private Validator wopMatch = new Validator(wsaaWopMatch, "Y");
	private Validator wopNotMatch = new Validator(wsaaWopMatch, "N");

	private FixedLengthStringData wsaaCrtableMatch = new FixedLengthStringData(1).init("N");
	private Validator crtableMatch = new Validator(wsaaCrtableMatch, "Y");
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(28);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 2);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 4);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 6);

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItem, 3);
	private String wsaaWaiveCont = "";

	private FixedLengthStringData wsaaZrwvflgs = new FixedLengthStringData(3);
	private FixedLengthStringData[] wsaaZrwvflg = FLSArrayPartOfStructure(3, 1, wsaaZrwvflgs, 0);
	private FixedLengthStringData wsaaTr517Rec = new FixedLengthStringData(250);

	private FixedLengthStringData wsaaTr695Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr695Coverage = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 0);
	private FixedLengthStringData wsaaTr695Rider = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 4);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler7, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler7, 6).setUnsigned();
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String f294 = "F294";
	private static final String g586 = "G586";
	private static final String hl49 = "HL49";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage/rider - new business*/
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Flexible Premium Reversals 9604*/
	private FpcorevTableDAM fpcorevIO = new FpcorevTableDAM();
		/*Flexible Premium Logical File*/
	private FprmTableDAM fprmIO = new FprmTableDAM();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
		/*Interestr Bearing Fund Interest Details*/
	private HitdTableDAM hitdIO = new HitdTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Temporary Commission Split*/
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private UnltmjaTableDAM unltmjaIO = new UnltmjaTableDAM();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Batckey wsaaBatckey = new Batckey();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Acomcalrec acomcalrec = new Acomcalrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Isuallrec isuallrec = new Isuallrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Stdtallrec stdtallrec = new Stdtallrec();
	private Actvresrec actvresrec = new Actvresrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Ubblallpar ubblallpar = new Ubblallpar();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private T5534rec t5534rec = new T5534rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5676rec t5676rec = new T5676rec();
	private T5671rec t5671rec = new T5671rec();
	private T5688rec t5688rec = new T5688rec();
	private T5729rec t5729rec = new T5729rec();
	private T5448rec t5448rec = new T5448rec();
	private T5515rec t5515rec = new T5515rec();
	private Th510rec th510rec = new Th510rec();
	private Tr517rec tr517rec = new Tr517rec();
	private Th502rec th502rec = new Th502rec();
	private T6640rec t6640rec = new T6640rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Tr695rec tr695rec = new Tr695rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Rlliadbrec rlliadbrec = new Rlliadbrec();
	private Tr384rec tr384rec = new Tr384rec();
	private T7508rec t7508rec = new T7508rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private ExternalisedRules er = new ExternalisedRules();
	private Exclpf exclpf=null; 
	private boolean stampDutyflag = false;
	private boolean susur002Permission = false;//ICIL-537
	private FixedLengthStringData wssasurrender = new FixedLengthStringData(4);
	private Validator surrenderValidator = new Validator(wssasurrender, "TA83");
	private boolean chnSurrend = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class); 
	private Covrpf covrpup = new Covrpf();
	private List<Covrpf> covrpupList = new ArrayList<Covrpf>();
	private List<Covrpf> covrlnbList = new ArrayList<Covrpf>();
	private CovrpfDAO covrpupDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private boolean autoIncrflag = false;
	private T6658rec t6658rec = new T6658rec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private String itemPFX = "IT";
	private PackedDecimalData wsaaCovtCommPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCovrCommPrem = new PackedDecimalData(17, 2);
	private boolean effdateFlag;
	private static final String EFFDATE_FEATURE = "CSCOM012";
	private int wsaaAnivDate ; //PINNACLE-2059
	//PINNACLE-2323 START
	private boolean adjuReasrFlag;
	private static final String ADJU_REASR_FEATURE = "CSLRI008";
	private List<Covrpf> covrpfList = null;
	private Covrpf covrpf = null;
	private boolean suminsChange;
	//PINNACLE-2323 END 
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		pass1030,
		deleteCovt2650,
		nextCovt2670,
		exit2690,
		exit3790,
		readNextLext4064,
		rerateDates4065,
		break8500,
		nextRecord20100,
		read20200,
		addUlnk21050,
		exit21090,
		e090Exit,
		a230Exit,
		a420Check,
		a480Next,
		a110Fund,
		a180NextFund
	}

	public P5132at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		mainline0001();
		exit0009();
	}

protected void mainline0001()
	{
		contractHeader1000();
		brkoutProcessing2000();
		brkoutCovtUnlt2500();
		coverageProcessing3000();
		updateHeader3500();
		callLifrtrn3600();
		writeLetter3700();
		x100WritePtrn();
		m800UpdateMlia();
		x200UpdateBatchHeader();
		a500AddUnderwritting();
		dryProcessing8000();
		x300ReleaseSftlck();
		x400Statistics();
	}

protected void exit0009()
	{
		exitProgram();
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void contractHeader1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initial1010();
					contractHeader1020();
				case pass1030:
					pass1030();
					readStatusCodes1030();
					callDatcon31040();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initial1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		wsaaToday.set(datcon1rec.intDate);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		
		susur002Permission = FeaConfg.isFeatureExist("2", "SUSUR002", appVars, "IT"); //ICIL-537
		wsaaBatckey.set(atmodrec.batchKey);
		wssasurrender.set(wsaaBatckey.batcBatctrcde);
		if( susur002Permission && surrenderValidator.isTrue()) {
			chnSurrend = true;
	}
		autoIncrflag = FeaConfg.isFeatureExist("2", "CSCOM011", appVars, "IT");
		effdateFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), EFFDATE_FEATURE, appVars, itemPFX);
		adjuReasrFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), ADJU_REASR_FEATURE, appVars, itemPFX);
	}

protected void contractHeader1020()
	{
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		wsaaFlexiblePremium.set("N");
		itemIO.setStatuz(SPACES);
		itemIO.setDataKey(SPACES);
		itemIO.setItemtabl(tablesInner.t5729);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
			t5729rec.t5729Rec.set(itemIO.getGenarea());
			wsaaFlexiblePremium.set("Y");
		}
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction("READH");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		chdrmjaIO.setValidflag("2");
		payrIO.setValidflag("2");
		if (flexiblePremium.isTrue()) {
			chdrmjaIO.setCurrto(wsaaEffdate);
			wsaaOrigPtdate.set(wsaaEffdate);
			wsaaPayrPtdate.set(payrIO.getPtdate());
			goTo(GotoLabel.pass1030);
		}
		if(chnSurrend) {//ICIL-537
			wsaaEffdate.set(wsaaToday);
			chdrmjaIO.setCurrto(wsaaEffdate);
			wsaaOrigPtdate.set(wsaaEffdate);
			wsaaPayrPtdate.set(payrIO.getPtdate());
		}else {
		chdrmjaIO.setCurrto(payrIO.getPtdate());
		wsaaOrigPtdate.set(payrIO.getPtdate());
		}
		wsaaItemCnttype.set(chdrmjaIO.getCnttype());
		}

protected void pass1030()
	{
		/* Update CHDRMJA header with TRANNO + 1 AFTER REWRT OF OLD RECORD*/
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setCurrto(99999999);
		chdrmjaIO.setCurrfrom(wsaaOrigPtdate);
		chdrmjaIO.setFunction(varcom.writr);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/* Update PAYR record with TRANNO + 1 AFTER REWRT OF OLD RECORD*/
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setValidflag("1");
		payrIO.setPtdate(wsaaOrigPtdate);
		payrIO.setEffdate(wsaaOrigPtdate);
		if (flexiblePremium.isTrue() || chnSurrend) {//ICIL-537
			payrIO.setPtdate(wsaaPayrPtdate);
		}
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readStatusCodes1030()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isNE(itdmIO.getItemcoy(),atmodrec.company)) {
			itdmIO.setItemitem(chdrmjaIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			xxxxFatalError();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void callDatcon31040()
	{
		if (isEQ(payrIO.getBillfreq(),"00")) {
			wsaaFreqFactor.set(1);
			return ;
		}
		wsaaBillfreqX.set(payrIO.getBillfreq());
		wsaaFreqFactor.set(wsaaBillfreq9);
	}

protected void brkoutProcessing2000()
	{
		checkBreakout2010();
		callBreakout2020();
	}

protected void checkBreakout2010()
	{
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setStatuz(varcom.oK);
		covtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife("01");
		covtmjaIO.setJlife("00");
		covtmjaIO.setCoverage("01");
		covtmjaIO.setRider("00");
		covtmjaIO.setPlanSuffix(9999);
		wsaaBrkoutSuffix.set(ZERO);
		wsaaLastSuffix.set(chdrmjaIO.getPolsum());
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covtmjaIO.getStatuz(),varcom.endp))) {
			readAllCovts2100();
		}

	}

protected void callBreakout2020()
	{
		if (isGT(wsaaBrkoutSuffix,ZERO)) {
			brkoutrec.outRec.set(SPACES);
			brkoutrec.brkChdrcoy.set(chdrmjaIO.getChdrcoy());
			brkoutrec.brkChdrnum.set(chdrmjaIO.getChdrnum());
			brkoutrec.brkOldSummary.set(chdrmjaIO.getPolsum());
			brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
			compute(brkoutrec.brkNewSummary, 0).set(sub(wsaaBrkoutSuffix,1));
			callProgram(Brkout.class, brkoutrec.outRec);
			if (isNE(brkoutrec.brkStatuz,varcom.oK)) {
				syserrrec.params.set(brkoutrec.brkStatuz);
				xxxxFatalError();
			}
		}
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readAllCovts2100()
	{
			callCovt2110();
		}

protected void callCovt2110()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.oK)
		&& isEQ(covtmjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		&& isEQ(covtmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		&& isEQ(wsaaLife,SPACES)) {
			wsaaLife.set(covtmjaIO.getLife());
			wsaaJlife.set(covtmjaIO.getJlife());
			wsaaCoverage.set(covtmjaIO.getCoverage());
		}
		if (isNE(covtmjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			covtmjaIO.setStatuz(varcom.endp);
			return ;
		}
		if (isLTE(covtmjaIO.getPlanSuffix(),chdrmjaIO.getPolsum())
		&& isLTE(covtmjaIO.getPlanSuffix(),wsaaLastSuffix)) {
			wsaaLastSuffix.set(covtmjaIO.getPlanSuffix());
			wsaaBrkoutSuffix.set(covtmjaIO.getPlanSuffix());
		}
		covtmjaIO.setFunction(varcom.nextr);
	}

protected void brkoutCovtUnlt2500()
	{
			start2510();
		}

protected void start2510()
	{
		if (isEQ(chdrmjaIO.getPolinc(),chdrmjaIO.getPolsum())
		|| isEQ(chdrmjaIO.getPolinc(),1)) {
			return ;
		}
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setStatuz(SPACES);
		covtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife("01");
		covtmjaIO.setJlife("00");
		covtmjaIO.setCoverage("01");
		covtmjaIO.setRider("00");
		covtmjaIO.setPlanSuffix(9999);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		while ( !(isEQ(covtmjaIO.getStatuz(),varcom.endp))) {
			processCovt2600();
		}

	}

protected void processCovt2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					callCovt2600();
				case deleteCovt2650:
					deleteCovt2650();
				case nextCovt2670:
					nextCovt2670();
				case exit2690:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callCovt2600()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covtmjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			covtmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2690);
		}
		wsaaStoreCovtKey.set(covtmjaIO.getDataKey());
		covtmjaIO.setFunction(varcom.nextr);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(covtmjaIO.getLife());
		covrmjaIO.setJlife(covtmjaIO.getJlife());
		covrmjaIO.setCoverage(covtmjaIO.getCoverage());
		covrmjaIO.setRider(covtmjaIO.getRider());
		covrmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit2690);
		}
		brkoutUnltRec2800();
		a400ReadPcdtRec();
		wsaaSaveSumins.set(covtmjaIO.getSumins());
		wsaaSaveSingp.set(covtmjaIO.getSingp());
		wsaaSaveInstprem.set(covtmjaIO.getInstprem());
		wsaaSaveZbinstprem.set(covtmjaIO.getZbinstprem());
		wsaaSaveZlinstprem.set(covtmjaIO.getZlinstprem());
		wsaaSaveCommPrem.set(covtmjaIO.getCommPrem());     //IBPLIFE-5237
		if (isNE(wsaaSaveSumins,0)) {
			compute(wsaaCovtSumins, 3).setRounded(div(wsaaSaveSumins,chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtSumins.set(0);
		}
		zrdecplrec.amountIn.set(wsaaCovtSumins);
		callRounding9000();
		wsaaCovtSumins.set(zrdecplrec.amountOut);
		if (isNE(wsaaSaveSingp,0)) {
			compute(wsaaCovtSingp, 3).setRounded(div(wsaaSaveSingp,chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtSingp.set(0);
		}
		zrdecplrec.amountIn.set(wsaaCovtSingp);
		callRounding9000();
		wsaaCovtSingp.set(zrdecplrec.amountOut);
		if (isNE(wsaaSaveInstprem,0)) {
			compute(wsaaCovtInstprem, 3).setRounded(div(wsaaSaveInstprem,chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtInstprem.set(0);
		}
		zrdecplrec.amountIn.set(wsaaCovtInstprem);
		callRounding9000();
		wsaaCovtInstprem.set(zrdecplrec.amountOut);
		if (isNE(wsaaSaveZbinstprem,0)) {
			compute(wsaaCovtZbinstprem, 3).setRounded(div(wsaaSaveZbinstprem,chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtZbinstprem.set(0);
		}
		zrdecplrec.amountIn.set(wsaaCovtZbinstprem);
		callRounding9000();
		wsaaCovtZbinstprem.set(zrdecplrec.amountOut);
		if (isNE(wsaaSaveZlinstprem,0)) {
			compute(wsaaCovtZlinstprem, 3).setRounded(div(wsaaSaveZlinstprem,chdrmjaIO.getPolinc()));
		}
		else {
			wsaaCovtZlinstprem.set(0);
		}
		zrdecplrec.amountIn.set(wsaaCovtZlinstprem);
		callRounding9000();
		wsaaCovtZlinstprem.set(zrdecplrec.amountOut);
		wsaaCovtSuffix.set(chdrmjaIO.getPolsum());
		while ( !(isEQ(chdrmjaIO.getPolinc(),wsaaCovtSuffix)
		|| isLT(chdrmjaIO.getPolinc(),wsaaCovtSuffix))) {
			addCovtPcdt2700();
		}

		covtmjaIO.setDataKey(wsaaStoreCovtKey);
		covtmjaIO.setFunction(varcom.readh);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(chdrmjaIO.getPolsum(),ZERO)) {
			goTo(GotoLabel.deleteCovt2650);
		}
		covtmjaIO.setSumins(wsaaSaveSumins);
		covtmjaIO.setSingp(wsaaSaveSingp);
		covtmjaIO.setInstprem(wsaaSaveInstprem);
		covtmjaIO.setZbinstprem(wsaaSaveZbinstprem);
		covtmjaIO.setZlinstprem(wsaaSaveZlinstprem);
		covtmjaIO.setCommPrem(wsaaSaveCommPrem);            //IBPLIFE-5237
		covtmjaIO.setFunction(varcom.rewrt);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		goTo(GotoLabel.nextCovt2670);
	}

protected void deleteCovt2650()
	{
		covtmjaIO.setFunction(varcom.delet);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void nextCovt2670()
	{
		covtmjaIO.setFunction(varcom.nextr);
	}

protected void addCovtPcdt2700()
	{
		start2700();
	}

protected void start2700()
	{
		wsaaCovtSuffix.add(1);
		covtmjaIO.setPlanSuffix(wsaaCovtSuffix);
		covtmjaIO.setSeqnbr(wsaaCovtSuffix);
		covtmjaIO.setSumins(wsaaCovtSumins);
		covtmjaIO.setSingp(wsaaCovtSingp);
		covtmjaIO.setInstprem(wsaaCovtInstprem);
		covtmjaIO.setZbinstprem(wsaaCovtZbinstprem);
		covtmjaIO.setZlinstprem(wsaaCovtZlinstprem);
		covtmjaIO.setCommPrem(wsaaSaveCommPrem);
		compute(wsaaSaveSumins, 2).set(sub(wsaaSaveSumins,wsaaCovtSumins));
		compute(wsaaSaveSingp, 2).set(sub(wsaaSaveSingp,wsaaCovtSingp));
		compute(wsaaSaveInstprem, 2).set(sub(wsaaSaveInstprem,wsaaCovtInstprem));
		compute(wsaaSaveZbinstprem, 2).set(sub(wsaaSaveZbinstprem,wsaaCovtZbinstprem));
		compute(wsaaSaveZlinstprem, 2).set(sub(wsaaSaveZlinstprem,wsaaCovtZlinstprem));
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		covtmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		a500AddPcdtRec();
	}

protected void brkoutUnltRec2800()
	{
			start2800();
		}

protected void start2800()
	{
		unltmjaIO.setDataArea(SPACES);
		unltmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		unltmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		unltmjaIO.setLife(covtmjaIO.getLife());
		unltmjaIO.setCoverage(covtmjaIO.getCoverage());
		unltmjaIO.setRider(covtmjaIO.getRider());
		unltmjaIO.setSeqnbr(covtmjaIO.getPlanSuffix());
		unltmjaIO.setFunction(varcom.readr);
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(),varcom.oK)) {
			if (isNE(unltmjaIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(unltmjaIO.getParams());
				syserrrec.statuz.set(unltmjaIO.getStatuz());
				xxxxFatalError();
			}
		}
		if (isEQ(unltmjaIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		wsaaSaveUalprcs.set(unltmjaIO.getUalprcs());
		if (isEQ(unltmjaIO.getPercOrAmntInd(),"A")) {
			for (sub1.set(1); !(isGT(sub1,10)); sub1.add(1)){
				calcUalprc2850();
			}
		}
		wsaaUnltSeq.set(chdrmjaIO.getPolsum());
		while ( !(isEQ(chdrmjaIO.getPolinc(),wsaaUnltSeq)
		|| isLT(chdrmjaIO.getPolinc(),wsaaUnltSeq))) {
			addUnlt2900();
		}

		unltmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		unltmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		unltmjaIO.setLife(covtmjaIO.getLife());
		unltmjaIO.setCoverage(covtmjaIO.getCoverage());
		unltmjaIO.setRider(covtmjaIO.getRider());
		unltmjaIO.setSeqnbr(covtmjaIO.getPlanSuffix());
		unltmjaIO.setFunction(varcom.readh);
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(),varcom.oK)) {
			if (isNE(unltmjaIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(unltmjaIO.getParams());
				syserrrec.statuz.set(unltmjaIO.getStatuz());
				xxxxFatalError();
			}
		}
		unltmjaIO.setUalprcs(wsaaSaveUalprcs);
		unltmjaIO.setFunction(varcom.rewrt);
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(unltmjaIO.getParams());
			syserrrec.statuz.set(unltmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void calcUalprc2850()
	{
		/*START*/
		if (isNE(wsaaSaveUalprc[sub1.toInt()],0)) {
			compute(wsaaUalprcAmt[sub1.toInt()], 2).set(div(wsaaSaveUalprc[sub1.toInt()],chdrmjaIO.getPolinc()));
		}
		else {
			wsaaUalprcAmt[sub1.toInt()].set(0);
		}
		zrdecplrec.amountIn.set(wsaaUalprcAmt[sub1.toInt()]);
		callRounding9000();
		wsaaUalprcAmt[sub1.toInt()].set(zrdecplrec.amountOut);
		/*EXIT*/
	}

protected void addUnlt2900()
	{
					start2900();
					writeUnlt2910();
				}

protected void start2900()
	{
		wsaaUnltSeq.add(1);
		unltmjaIO.setSeqnbr(wsaaUnltSeq);
		if (isEQ(unltmjaIO.getPercOrAmntInd(),"P")) {
			return ;
		}
		unltmjaIO.setUalprcs(wsaaUalprcAmounts);
		for (sub1.set(1); !(isGT(sub1,10)); sub1.add(1)){
			calcSummAmt2950();
		}
	}

protected void writeUnlt2910()
	{
		unltmjaIO.setFunction(varcom.writr);
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(unltmjaIO.getParams());
			syserrrec.statuz.set(unltmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void calcSummAmt2950()
	{
		/*START*/
		if (isNE(wsaaSaveUalprc[sub1.toInt()],0)) {
			compute(wsaaSaveUalprc[sub1.toInt()], 2).set(sub(wsaaSaveUalprc[sub1.toInt()],wsaaUalprcAmt[sub1.toInt()]));
		}
		/*EXIT*/
	}

protected void coverageProcessing3000()
	{
		begnCovt3010();
	}

protected void begnCovt3010()
	{
		wsaaSinstamt.set(0);
		wsaaSuspPosting.set(ZERO);
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setStatuz(SPACES);
		covtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covtmjaIO.setLife("01");
		covtmjaIO.setJlife("00");
		covtmjaIO.setCoverage("01");
		covtmjaIO.setRider("00");
		covtmjaIO.setPlanSuffix(9999);
		covtmjaIO.setFunction(varcom.begnh);
		Covtpf covtpfObj = null;
		while ( !(isEQ(covtmjaIO.getStatuz(),varcom.endp))) {
			processCovt3100();
			if(adjuReasrFlag && null == covtpfObj && suminsChange) { //PINNACLE-2323 store the first covt
				covtpfObj = new Covtpf();
				covtpfObj.setChdrcoy(covtmjaIO.getChdrcoy().toString());
				covtpfObj.setChdrnum(covtmjaIO.getChdrnum().toString());
				covtpfObj.setLife(covtmjaIO.getLife().toString());
				covtpfObj.setCoverage(covtmjaIO.getCoverage().toString());
				covtpfObj.setRider(covtmjaIO.getRider().toString());
			}
		}
		if(adjuReasrFlag && null != covtpfObj) {
			//get all the covrpf greater than or equal to covt coverage
			Covrpf covrpfObj = new Covrpf();
			covrpfObj.setChdrcoy(covtpfObj.getChdrcoy());
			covrpfObj.setChdrnum(covtpfObj.getChdrnum());
			covrpfObj.setLife(covtpfObj.getLife());
			covrpfObj.setCoverage(covtpfObj.getCoverage());
			covrpfList = covrpupDAO.searchCovrpfRecordByCoverage(covrpfObj);
			for(int i = 0;i< covrpfList.size();i++) {
				covrpf = covrpfList.get(i);
				activateReassurance8000();
			}
		}
		if (flexiblePremium.isTrue()
		&& compChanged.isTrue()) {
			writeFprm3100();
		}
		a100WopDates();
	}

protected void writeFprm3100()
	{
		start3110();
	}

protected void start3110()
	{
		fprmIO.setDataArea(SPACES);
		fprmIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		fprmIO.setChdrnum(chdrmjaIO.getChdrnum());
		fprmIO.setPayrseqno(payrIO.getPayrseqno());
		fprmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			xxxxFatalError();
		}
		fprmIO.setCurrto(wsaaOrigPtdate);
		fprmIO.setValidflag("2");
		fprmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			xxxxFatalError();
		}
		fprmIO.setCurrfrom(wsaaOrigPtdate);
		fprmIO.setCurrto(99999999);
		setPrecision(fprmIO.getTotalBilled(), 3);
		fprmIO.setTotalBilled(add(sub(fprmIO.getTotalBilled(),wsaaOldBilled),wsaaNewBilled), true);
		setPrecision(fprmIO.getMinPrmReqd(), 3);
		fprmIO.setMinPrmReqd(add(sub(fprmIO.getMinPrmReqd(),wsaaOldOverdueMin),wsaaNewOverdueMin), true);
		fprmIO.setTranno(chdrmjaIO.getTranno());
		fprmIO.setValidflag("1");
		fprmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			xxxxFatalError();
		}
	}

protected void processCovt3100()
	{
			callCovt3110();
		}

protected void callCovt3110()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covtmjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			covtmjaIO.setStatuz(varcom.endp);
			return ;
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtmjaIO.getCrtable());
		itdmIO.setItmfrm(covtmjaIO.getEffdate());
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covtmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covtmjaIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
			wsaaBasicCommMeth.set(t5687rec.basicCommMeth);
			wsaaBascpy.set(t5687rec.bascpy);
			wsaaSrvcpy.set(t5687rec.srvcpy);
			wsaaRnwcpy.set(t5687rec.rnwcpy);
			wsaaBasscmth.set(t5687rec.basscmth);
			wsaaBasscpy.set(t5687rec.basscpy);
		}
		itempf = new Itempf();
		itempf.setItempfx(itemPFX);
		itempf.setItemcoy(atmodrec.company.toString());
		itempf.setItemtabl(tablesInner.t6658.toString());
		itempf.setItemitem(t5687rec.anniversaryMethod.toString());
		itempf = itemDAO.getItempfRecord(itempf);
		if (itempf != null) {
			t6658rec.t6658Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		
		if (isEQ(covtmjaIO.getInstprem(),ZERO)) {
			if (isNE(t5687rec.zrrcombas,SPACES)) {
				wsaaCovtAnnprem.set(covtmjaIO.getZbinstprem());
			}
			else {
				wsaaCovtAnnprem.set(covtmjaIO.getSingp());
			}
		}
		else {
			if (isNE(t5687rec.zrrcombas,SPACES)) {
				compute(wsaaCovtAnnprem, 5).set(mult(covtmjaIO.getZbinstprem(),wsaaFreqFactor));
			}
			else {
				compute(wsaaCovtAnnprem, 5).set(mult(covtmjaIO.getInstprem(),wsaaFreqFactor));
			}
		}
		
		if(isNE(covtmjaIO.getCommPrem(),ZERO)){
			wsaaCovtCommPrem.set(covtmjaIO.getCommPrem());  //IBPLIFE-5237
		}
		
		covtmjaIO.setFunction(varcom.nextr);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(covtmjaIO.getLife());
		covrmjaIO.setJlife(covtmjaIO.getJlife());
		covrmjaIO.setCoverage(covtmjaIO.getCoverage());
		covrmjaIO.setRider(covtmjaIO.getRider());
		covrmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.oK)) {
			if (isEQ(covrmjaIO.getInstprem(),ZERO)) {
				if (isNE(t5687rec.zrrcombas,SPACES)) {
					wsaaCovrAnnprem.set(covrmjaIO.getZbinstprem());
				}
				else {
					wsaaCovrAnnprem.set(covrmjaIO.getSingp());
				}
			}
			else {
				if (isNE(t5687rec.zrrcombas,SPACES)) {
					compute(wsaaCovrAnnprem, 5).set(mult(covrmjaIO.getZbinstprem(),wsaaFreqFactor));
				}
				else {
					compute(wsaaCovrAnnprem, 5).set(mult(covrmjaIO.getInstprem(),wsaaFreqFactor));
				}
			}
			if(isNE(covtmjaIO.sumins,covrmjaIO.sumins)) {
				suminsChange = true;
			}
		}
		else {
			wsaaCovrAnnprem.set(ZERO);
			suminsChange = true;
		}
		if(isGT(covrmjaIO.getCommPrem(),ZERO)) {
			wsaaCovrCommPrem.set(covrmjaIO.getCommPrem());   //IBPLIFE-5237
		}
		
		if (isEQ(covrmjaIO.getStatuz(),varcom.oK)) {
			compute(wsaaSinstamt, 2).set(sub(add(wsaaSinstamt,covtmjaIO.getInstprem()),covrmjaIO.getInstprem()));
		}
		else {
			compute(wsaaSinstamt, 2).set(add(wsaaSinstamt,covtmjaIO.getInstprem()));
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.oK)) {
			wsaaCompAction.set("C");
			storeDates4000();
			compChange4000();
		}
		else {
			wsaaCompAction.set("A");
			compAdd5000();
		}
		if (isNE(covtmjaIO.getRider(),"00")) {
			readTr6953200();
		}
		if (isNE(wsaaCovrAnnprem,wsaaCovtAnnprem)) {
			commissionProcess7000();
			if(!adjuReasrFlag) {
				activateReassurance8000();
			}
		}
		a300DeletePcdtRec();
		convertUntlToUlnk20000();
		d000StampDuty();
		e000T5671CompProcess();
		deleteCovt5500();
	}

protected void readTr6953200()
	{
			read3210();
		}

protected void read3210()
	{
		covrlnbIO.setDataArea(SPACES);
		covrlnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrlnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrlnbIO.setLife(covtmjaIO.getLife());
		covrlnbIO.setJlife(covtmjaIO.getJlife());
		covrlnbIO.setCoverage(covtmjaIO.getCoverage());
		covrlnbIO.setRider("00");
		covrlnbIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		covrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			xxxxFatalError();
		}
		wsaaTr695Coverage.set(covrlnbIO.getCrtable());
		wsaaTr695Rider.set(covtmjaIO.getCrtable());
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.tr695);
		itdmIO.setItemitem(wsaaTr695Key);
		itdmIO.setItmfrm(covtmjaIO.getEffdate());
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr695)
		|| isNE(itdmIO.getItemitem(),wsaaTr695Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaTr695Rider.set("****");
			itdmIO.setDataArea(SPACES);
			itdmIO.setItemitem(wsaaTr695Key);
			itdmIO.setItemtabl(tablesInner.tr695);
			itdmIO.setItemcoy(atmodrec.company);
			itdmIO.setItmfrm(covtmjaIO.getEffdate());
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				xxxxFatalError();
			}
			if (isNE(itdmIO.getItemcoy(),atmodrec.company)
			|| isNE(itdmIO.getItemtabl(), tablesInner.tr695)
			|| isNE(itdmIO.getItemitem(),wsaaTr695Key)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				return ;
			}
		}
		tr695rec.tr695Rec.set(itdmIO.getGenarea());
		wsaaBasicCommMeth.set(tr695rec.basicCommMeth);
		wsaaBascpy.set(tr695rec.bascpy);
		wsaaSrvcpy.set(tr695rec.srvcpy);
		wsaaRnwcpy.set(tr695rec.rnwcpy);
		wsaaBasscmth.set(tr695rec.basscmth);
		wsaaBasscpy.set(tr695rec.basscpy);
	}

protected void updateHeader3500()
	{
		chdr3510();
	}

protected void chdr3510()
	{
		setPrecision(chdrmjaIO.getSinstamt01(), 2);
		chdrmjaIO.setSinstamt01(add(chdrmjaIO.getSinstamt01(),wsaaSinstamt));
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(chdrmjaIO.getSinstamt06(),wsaaSinstamt));
		setPrecision(payrIO.getSinstamt01(), 2);
		payrIO.setSinstamt01(add(payrIO.getSinstamt01(),wsaaSinstamt));
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(payrIO.getSinstamt06(),wsaaSinstamt));
		chdrmjaIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void callLifrtrn3600()
	{
		readT56453610();
		lifrtrn3620();
	}

protected void readT56453610()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("P5132");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		else {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("P5132");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void lifrtrn3620()
	{
		/*  Post the Single Premium Amount.                             */
		/*    IF  WSAA-SUSP-POSTING      NOT  =  ZERO              <V74L01>*/
		if ((setPrecision(ZERO, 5)
		&& isNE(add(wsaaSuspPosting, wsaaTotalTax), ZERO))) {
			lifrtrnrec.function.set("PSTW");
			lifrtrnrec.batckey.set(atmodrec.batchKey);
			lifrtrnrec.rdocnum.set(chdrmjaIO.getChdrnum());
			lifrtrnrec.rldgacct.set(chdrmjaIO.getChdrnum());
			lifrtrnrec.tranno.set(chdrmjaIO.getTranno());
			lifrtrnrec.jrnseq.set(0);
			lifrtrnrec.rldgcoy.set(chdrmjaIO.getChdrcoy());
			lifrtrnrec.origcurr.set(chdrmjaIO.getCntcurr());
			lifrtrnrec.sacscode.set(t5645rec.sacscode01);
			lifrtrnrec.sacstyp.set(t5645rec.sacstype01);
			lifrtrnrec.glcode.set(t5645rec.glmap01);
			lifrtrnrec.glsign.set(t5645rec.sign01);
			lifrtrnrec.contot.set(t5645rec.cnttot01);
			lifrtrnrec.tranref.set(chdrmjaIO.getChdrnum());
			lifrtrnrec.trandesc.set(descIO.getLongdesc());
			lifrtrnrec.crate.set(0);
			lifrtrnrec.acctamt.set(0);
			lifrtrnrec.rcamt.set(0);
			lifrtrnrec.user.set(wsaaUser);
			lifrtrnrec.termid.set(wsaaTermid);
			lifrtrnrec.transactionDate.set(wsaaTransactionDate);
			lifrtrnrec.transactionTime.set(wsaaTransactionTime);
			lifrtrnrec.origamt.set(wsaaSuspPosting);
			lifrtrnrec.origamt.add(wsaaTotalTax);
			lifrtrnrec.genlcur.set(SPACES);
			lifrtrnrec.genlcoy.set(chdrmjaIO.getChdrcoy());
			lifrtrnrec.postyear.set(SPACES);
			lifrtrnrec.postmonth.set(SPACES);
			lifrtrnrec.effdate.set(wsaaEffdate);
			lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
			lifrtrnrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
			if (isNE(lifrtrnrec.statuz,varcom.oK)) {
				syserrrec.params.set(lifrtrnrec.lifrtrnRec);
				xxxxFatalError();
			}
		}
	}
protected void updateExcl(){
	List<Exclpf> exclpflist = new LinkedList<Exclpf>(); 
	exclpflist=exclpfDAO.getExclpfRecord(chdrmjaIO.getChdrnum().toString(),covrmjaIO.getCrtable().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),covrmjaIO.getRider().toString()); 
	 for (int i = 0; i < exclpflist.size(); i++) 
		{
		 	exclpf = exclpflist.get(i);
		 	if(isEQ(exclpf.getValidflag(),"3")) { //PINNACLE-2855
			exclpf.setTranno(chdrmjaIO.getTranno().toInt());
			exclpfDAO.updateStatus(exclpf);	
		 	}
			
		} 
	
}
protected void writeLetter3700()
	{
		try {
			readT66343710();
			setUpParm3730();
		}
		catch (GOTOException e){
		}
	}

protected void readT66343710()
	{
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			readTr384Again3800();
		}
		else {
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		if (isEQ(tr384rec.letterType,SPACES)) {
			goTo(GotoLabel.exit3790);
		}
	}

protected void setUpParm3730()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(atmodrec.company);
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			xxxxFatalError();
		}
	}

protected void readTr384Again3800()
	{
		start3810();
	}

protected void start3810()
	{
		wsaaItemCnttype.set("***");
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		else {
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
	}

protected void storeDates4000()
	{
		/*STORE-DATES-PARA*/
		wsaaUnitstmtDate.set(varcom.vrcmMaxDate);
		wsaaRerateDate.set(varcom.vrcmMaxDate);
		wsaaAnivprocDate.set(varcom.vrcmMaxDate);
		wsaaIucancDate.set(varcom.vrcmMaxDate);
		wsaaBenbillDate.set(varcom.vrcmMaxDate);
		wsaaBonusInd.set(SPACES);
		wsaaBonusInd.set(covrmjaIO.getBonusInd());
		wsaaBenbillDate.set(covrmjaIO.getBenBillDate());
		wsaaAnivprocDate.set(covrmjaIO.getAnnivProcDate());
		wsaaRerateDate.set(covrmjaIO.getRerateDate());
		wsaaUnitstmtDate.set(covrmjaIO.getUnitStatementDate());
		wsaaIucancDate.set(covrmjaIO.getInitUnitCancDate());
		wsaaCpiDate.set(covrmjaIO.getCpiDate());
		/*STORE-DATES-EXIT*/
	}

protected void compChange4000()
	{
		t5687Table4010();
		validflag14030();
	}

protected void t5687Table4010()
	{
		/*VALIDFLAG-2*/
		covrmjaIO.setCurrto(covtmjaIO.getEffdate());
		covrmjaIO.setValidflag("2");
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		updateExcl();
	}

protected void validflag14030()
	{
		tableT5448Read4100();
		if (isNE(t5448rec.proppr,SPACES)) {
			wsaaOldSumins.set(ZERO);
		}
		else {
			wsaaOldSumins.set(covrmjaIO.getSumins());
		}
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setCurrfrom(covtmjaIO.getEffdate());
		covrmjaIO.setCurrto(chdrmjaIO.getCurrto());
		if (isEQ(covtmjaIO.getJlife(),SPACES)
		|| isEQ(covtmjaIO.getJlife(),"00")) {
			covrmjaIO.setSex(covtmjaIO.getSex01());
		}
		else {
			covrmjaIO.setSex(covtmjaIO.getSex02());
		}
		covrmjaIO.setPremCurrency(payrIO.getCntcurr());
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setTransactionDate(wsaaTransactionDate);
		covrmjaIO.setTransactionTime(wsaaTransactionTime);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setPayrseqno(covtmjaIO.getPayrseqno());
		covrmjaIO.setRiskCessDate(covtmjaIO.getRiskCessDate());
		covrmjaIO.setPremCessDate(covtmjaIO.getPremCessDate());
		covrmjaIO.setBenCessDate(covtmjaIO.getBenCessDate());
		covrmjaIO.setRiskCessAge(covtmjaIO.getRiskCessAge());
		covrmjaIO.setPremCessAge(covtmjaIO.getPremCessAge());
		covrmjaIO.setBenCessAge(covtmjaIO.getBenCessAge());
		covrmjaIO.setRiskCessTerm(covtmjaIO.getRiskCessTerm());
		covrmjaIO.setPremCessTerm(covtmjaIO.getPremCessTerm());
		covrmjaIO.setBenCessTerm(covtmjaIO.getBenCessTerm());
		covrmjaIO.setAnnivProcDate(ZERO);
		covrmjaIO.setBenBillDate(ZERO);
		covrmjaIO.setConvertInitialUnits(ZERO);
		covrmjaIO.setCpiDate(ZERO);
		covrmjaIO.setExtraAllocDate(ZERO);
		covrmjaIO.setInitUnitCancDate(ZERO);
		covrmjaIO.setInitUnitIncrsDate(ZERO);
		covrmjaIO.setRerateDate(ZERO);
		covrmjaIO.setRerateFromDate(ZERO);
		covrmjaIO.setReviewProcessing(ZERO);
		covrmjaIO.setUnitStatementDate(ZERO);
		covrmjaIO.setBenBillDate(wsaaBenbillDate);
		covrmjaIO.setUnitStatementDate(wsaaUnitstmtDate);
		covrmjaIO.setAnnivProcDate(wsaaAnivprocDate);
		covrmjaIO.setInitUnitCancDate(wsaaIucancDate);
		if(autoIncrflag)
		{	
			if(isEQ(t6658rec.incrFlg,"Y"))
			{
			 covrmjaIO.setCpiDate(wsaaCpiDate);//incr
			}
			else
			{
				covrmjaIO.setCpiDate(varcom.maxdate);//incr
			}
		}
		else
		{
			 covrmjaIO.setCpiDate(varcom.maxdate);//incr
		}
		covrmjaIO.setSumins(covtmjaIO.getSumins());
		covrmjaIO.setMortcls(covtmjaIO.getMortcls());
		covrmjaIO.setLiencd(covtmjaIO.getLiencd());
		covrmjaIO.setSingp(covtmjaIO.getSingp());
		covrmjaIO.setZbinstprem(covtmjaIO.getZbinstprem());
		covrmjaIO.setZlinstprem(covtmjaIO.getZlinstprem());
		covrmjaIO.setCommPrem(covtmjaIO.getCommPrem());                //IBPLIFE-5237
		covrmjaIO.setInstprem(covtmjaIO.getInstprem());
		covrmjaIO.setJlife(covtmjaIO.getJlife());
		covrmjaIO.setBonusInd(wsaaBonusInd);
		covrmjaIO.setBappmeth(covtmjaIO.getBappmeth());
		covrmjaIO.setTpdtype(covtmjaIO.getTpdtype());//ILIFE-7118
		covrmjaIO.setLnkgind(covtmjaIO.getLnkgind());
		computeRerateDate4060();
		stampDutyflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if (stampDutyflag) {
			covrmjaIO.setZstpduty01(covtmjaIO.getZstpduty01());
			covrmjaIO.setZclstate(covtmjaIO.getZclstate());
		}
		covrmjaIO.setRiskprem(covtmjaIO.getRiskprem()); //ILIFE-7845 
		/*ILIFE-8248 start*/
		covrmjaIO.setLnkgno(covtmjaIO.getLnkgno());
		covrmjaIO.setLnkgsubrefno(covtmjaIO.getLnkgsubrefno());
		/*ILIFE-8248 end*/
		covrmjaIO.setValidflag("1");
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (flexiblePremium.isTrue()) {
			readFpco4200();
			calcFpcoAmounts4300();
			rewrtFpco4400();
			writeFpco4500();
		}
		cashDividendModify4600();
		wsaaCompChanged.set("Y");
	}

protected void readFpco4200()
	{
		start4210();
	}

protected void start4210()
	{
		fpcorevIO.setDataArea(SPACES);
		fpcorevIO.setChdrcoy(covtmjaIO.getChdrcoy());
		fpcorevIO.setChdrnum(covtmjaIO.getChdrnum());
		fpcorevIO.setLife(covtmjaIO.getLife());
		fpcorevIO.setCoverage(covtmjaIO.getCoverage());
		fpcorevIO.setRider(covtmjaIO.getRider());
		fpcorevIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		fpcorevIO.setTargfrom(99999999);
		fpcorevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcorevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(covrmjaIO.getChdrcoy(),fpcorevIO.getChdrcoy())
		&& isNE(covrmjaIO.getChdrnum(),fpcorevIO.getChdrnum())
		&& isNE(covrmjaIO.getLife(),fpcorevIO.getLife())
		&& isNE(covrmjaIO.getCoverage(),fpcorevIO.getCoverage())
		&& isNE(covrmjaIO.getRider(),fpcorevIO.getRider())
		&& isNE(covrmjaIO.getPlanSuffix(),fpcorevIO.getPlanSuffix())) {
			syserrrec.params.set(fpcorevIO.getParams());
			xxxxFatalError();
		}
	}

protected void calcFpcoAmounts4300()
	{
		start4310();
	}

	/**
	* <pre>
	*******************************                           <D9604>
	* </pre>
	*/
protected void start4310()
	{
		/*  Calculate new Target for the period.                   <D9604>*/
		compute(wsaaNewTarget, 3).setRounded(mult(covrmjaIO.getInstprem(),wsaaBillfreq9));
		zrdecplrec.amountIn.set(wsaaNewTarget);
		callRounding9000();
		wsaaNewTarget.set(zrdecplrec.amountOut);
		/*  Calculate new Billed amount.                           <D9604>*/
		compute(wsaaNewBilledDecimal, 7).set(div(fpcorevIO.getBilledInPeriod(),fpcorevIO.getTargetPremium()));
		zrdecplrec.amountIn.set(wsaaNewBilledDecimal);
		callRounding9000();
		wsaaNewBilledDecimal.set(zrdecplrec.amountOut);
		/*  Calculate the new billed amount                        <D9604>*/
		compute(wsaaNewBilled, 8).setRounded(mult(wsaaNewBilledDecimal,wsaaNewTarget));
		zrdecplrec.amountIn.set(wsaaNewBilled);
		callRounding9000();
		wsaaNewBilled.set(zrdecplrec.amountOut);
		/*  Calculate the new minimum overdue amount               <D9604>*/
		compute(wsaaNewOverdueMin, 3).setRounded(mult(wsaaNewBilled,(div(fpcorevIO.getMinOverduePer(),100))));
		zrdecplrec.amountIn.set(wsaaNewOverdueMin);
		callRounding9000();
		wsaaNewOverdueMin.set(zrdecplrec.amountOut);
	}

protected void rewrtFpco4400()
	{
		start4410();
	}

protected void start4410()
	{
		fpcorevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			xxxxFatalError();
		}
		wsaaOldBilled.set(fpcorevIO.getBilledInPeriod());
		wsaaOldOverdueMin.set(fpcorevIO.getOverdueMin());
		fpcorevIO.setValidflag("2");
		fpcorevIO.setCurrto(wsaaEffdate);
		fpcorevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			xxxxFatalError();
		}
	}

protected void writeFpco4500()
	{
		start4510();
	}

protected void start4510()
	{
		fpcorevIO.setChdrcoy(covtmjaIO.getChdrcoy());
		fpcorevIO.setChdrnum(covtmjaIO.getChdrnum());
		fpcorevIO.setLife(covtmjaIO.getLife());
		fpcorevIO.setCoverage(covtmjaIO.getCoverage());
		fpcorevIO.setRider(covtmjaIO.getRider());
		fpcorevIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		fpcorevIO.setTranno(chdrmjaIO.getTranno());
		fpcorevIO.setTargfrom(wsaaEffdate);
		datcon2rec.intDate1.set(wsaaEffdate);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		fpcorevIO.setTargto(datcon2rec.intDate2);
		fpcorevIO.setAnnivProcDate(datcon2rec.intDate2);
		fpcorevIO.setCurrfrom(wsaaEffdate);
		fpcorevIO.setEffdate(wsaaEffdate);
		fpcorevIO.setCurrto(99999999);
		fpcorevIO.setBilledInPeriod(wsaaNewBilled);
		fpcorevIO.setTargetPremium(wsaaNewTarget);
		fpcorevIO.setOverdueMin(wsaaNewOverdueMin);
		fpcorevIO.setValidflag("1");
		fpcorevIO.setActiveInd("Y");
		fpcorevIO.setAnnProcessInd("N");
		fpcorevIO.setFormat(formatsInner.fpcorevrec);
		fpcorevIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			xxxxFatalError();
		}
	}

protected void computeRerateDate4060()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					optionalExtras4062();
				case readNextLext4064:
					readNextLext4064();
				case rerateDates4065:
					rerateDates4065();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void optionalExtras4062()
	{
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lextIO.setChdrnum(chdrmjaIO.getChdrnum());
		lextIO.setLife(covtmjaIO.getLife());
		lextIO.setCoverage(covtmjaIO.getCoverage());
		lextIO.setRider(covtmjaIO.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
		wsaaLextDate.set(varcom.vrcmMaxDate);
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp)
		|| isNE(lextIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(lextIO.getCoverage(),covtmjaIO.getCoverage())
		|| isNE(lextIO.getRider(),covtmjaIO.getRider()))) {
			optionalExtraProcessingThru4063();
		}

		goTo(GotoLabel.rerateDates4065);
	}

protected void optionalExtraProcessing4063()
	{
		if (isEQ(lextIO.getExtCessTerm(),0)) {
			goTo(GotoLabel.readNextLext4064);
		}
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon2rec.freqFactor.set(lextIO.getExtCessTerm());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		wsaaLextExtDate.set(lextIO.getExtCessDate());
		if (isEQ(datcon2rec.intDate2,lextIO.getExtCessDate())) {
			if (isGT(covtmjaIO.getPremCessDate(),lextIO.getExtCessDate())) {
				goTo(GotoLabel.readNextLext4064);
			}
		}
		if (isLT(covtmjaIO.getPremCessDate(),lextIO.getExtCessDate())) {
			lextIO.setExtCessDate(covtmjaIO.getPremCessDate());
		}
		else {
			lextIO.setExtCessDate(datcon2rec.intDate2);
		}
		if (isNE(wsaaLextExtDate,lextIO.getExtCessDate())) {
			datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
			datcon3rec.intDate2.set(lextIO.getExtCessDate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				syserrrec.statuz.set(datcon3rec.statuz);
				xxxxFatalError();
			}
			else {
				lextIO.setExtCessTerm(datcon3rec.freqFactor);
			}
		}
		lextIO.setTermid(wsaaTermid);
		lextIO.setTransactionDate(wsaaTransactionDate);
		lextIO.setTransactionTime(wsaaTransactionTime);
		lextIO.setUser(wsaaUser);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readNextLext4064()
	{
		if (isLT(lextIO.getExtCessDate(),wsaaLextDate)
		&& isNE(lextIO.getExtCessDate(),0)) {
			wsaaLextDate.set(lextIO.getExtCessDate());
		}
		lextIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rerateDates4065()
	{
		if(effdateFlag && wsaaCompAdd.isTrue()) {
			List<Covrpf> covrpfList=covrpupDAO.searchCovrmjaByChdrnumCoy(wsaaPrimaryChdrnum.toString(),atmodrec.company.toString());
			if(!covrpfList.isEmpty()) {
				for(Covrpf c:covrpfList) {
					if(c.getValidflag().equals("1") && c.getStatcode().equals("IF")) {
						
						covrmjaIO.setRerateDate(c.getRerateDate());
						covrmjaIO.setRerateFromDate(c.getRerateFromDate());
						wsaaAnivDate =  c.getAnnivProcDate(); //PINNACLE-2059
						break;
					}
				}
			}
			return;
		}
		wsaaRerateDate.set(wsaaLextDate);
		if (isLT(covtmjaIO.getPremCessDate(),wsaaRerateDate)) {
			wsaaRerateDate.set(covtmjaIO.getPremCessDate());
		}
		covrmjaIO.setRerateFromDate(wsaaRerateDate);
		if (isNE(t5687rec.rtrnwfreq,0)) {
			if (wsaaCompAdd.isTrue()) {
				datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
			}
			else {
				datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
			}
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(t5687rec.rtrnwfreq);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				xxxxFatalError();
			}
			else {
				if (isLT(datcon2rec.intDate2,wsaaRerateDate)) {
					wsaaRerateDate.set(datcon2rec.intDate2);
					covrmjaIO.setRerateFromDate(datcon2rec.intDate2);
				}
			}
		}
		if (isNE(t5687rec.premGuarPeriod,0)) {
			datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(t5687rec.premGuarPeriod);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				xxxxFatalError();
			}
			else {
				if (isGT(datcon2rec.intDate2,covrmjaIO.getRerateFromDate())) {
					covrmjaIO.setRerateFromDate(datcon2rec.intDate2);
				}
			}
		}
		covrmjaIO.setRerateDate(wsaaRerateDate);
	}

protected void optionalExtraProcessingThru4063()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					optionalExtraProcessing4063();
				case readNextLext4064:
					readNextLext4064();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void tableT5448Read4100()
	{
		t54484101();
	}

protected void t54484101()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5448);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(covrmjaIO.getCrtable());
		stringVariable1.setStringInto(wsaaT5448Item);
		itdmIO.setItemitem(wsaaT5448Item);
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(wsaaT5448Item,itdmIO.getItemitem())
		|| isNE(chdrmjaIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5448)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		else {
			t5448rec.t5448Rec.set(itdmIO.getGenarea());
		}
	}

protected void cashDividendModify4600()
	{
			divMod4600();
		}

protected void divMod4600()
	{
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(covtmjaIO.getChdrcoy());
		hcsdIO.setChdrnum(covtmjaIO.getChdrnum());
		hcsdIO.setLife(covtmjaIO.getLife());
		hcsdIO.setCoverage(covtmjaIO.getCoverage());
		hcsdIO.setRider(covtmjaIO.getRider());
		hcsdIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)
		&& isNE(hcsdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(hcsdIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		hcsdIO.setValidflag("2");
		hcsdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			xxxxFatalError();
		}
		hcsdIO.setValidflag("1");
		hcsdIO.setTranno(covrmjaIO.getTranno());
		hcsdIO.setEffdate(payrIO.getPtdate());
		hcsdIO.setZdivopt(covtmjaIO.getZdivopt());
		hcsdIO.setPayclt(covtmjaIO.getPayclt());
		hcsdIO.setPaymth(covtmjaIO.getPaymth());
		hcsdIO.setPaycurr(covtmjaIO.getPaycurr());
		hcsdIO.setFacthous(covtmjaIO.getFacthous());
		hcsdIO.setBankkey(covtmjaIO.getBankkey());
		hcsdIO.setBankacckey(covtmjaIO.getBankacckey());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void compAdd5000()
	{
		t5687Table5010();
		setupCovr5020();
	}

protected void t5687Table5010()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtmjaIO.getCrtable());
		itdmIO.setItmfrm(covtmjaIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covtmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covtmjaIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void setupCovr5020()
	{
		covrmjaIO.setSumins(covtmjaIO.getSumins());
		covrmjaIO.setInstprem(covtmjaIO.getInstprem());
		covrmjaIO.setZbinstprem(covtmjaIO.getZbinstprem());
		covrmjaIO.setZlinstprem(covtmjaIO.getZlinstprem());
		covrmjaIO.setCommPrem(covtmjaIO.getCommPrem());     //IBPLIFE-5237
		covrmjaIO.setSingp(covtmjaIO.getSingp());
		covrmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		covrmjaIO.setCurrto(chdrmjaIO.getCurrto());
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			covrmjaIO.setCrrcd(covtmjaIO.getEffdate());
			covrmjaIO.setCurrfrom(covtmjaIO.getEffdate());
			wsaaSuspPosting.add(covrmjaIO.getSingp());
			covrmjaIO.setCpiDate(varcom.vrcmMaxDate);
		}
		else {
			covrmjaIO.setCurrfrom(payrIO.getBtdate());
			covrmjaIO.setCrrcd(payrIO.getBtdate());
			covrmjaIO.setCpiDate(0);
		}
		if (isEQ(covtmjaIO.getRider(),"00")
		|| isEQ(covtmjaIO.getRider(),SPACES)) {
			covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
		}
		else {
			covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
		}
		if (isNE(payrIO.getBillfreq(),"00")
		&& isNE(t5687rec.singlePremInd,"Y")) {
			if (isEQ(covtmjaIO.getRider(),SPACES)
			|| isEQ(covtmjaIO.getRider(),"00")) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
			else {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		else {
			if (isEQ(covtmjaIO.getRider(),SPACES)
			|| isEQ(covtmjaIO.getRider(),"00")) {
				covrmjaIO.setPstatcode(t5679rec.setSngpCovStat);
			}
			else {
				covrmjaIO.setPstatcode(t5679rec.setSngpCovStat);
			}
		}
		covrmjaIO.setReptcds(t5687rec.reptcds);
		covrmjaIO.setStatFund(t5687rec.statFund);
		covrmjaIO.setStatSect(t5687rec.statSect);
		covrmjaIO.setStatSubsect(t5687rec.statSubSect);
		covrmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		covrmjaIO.setLife(covtmjaIO.getLife());
		covrmjaIO.setCoverage(covtmjaIO.getCoverage());
		covrmjaIO.setRider(covtmjaIO.getRider());
		covrmjaIO.setCrInstamt01(0);
		covrmjaIO.setCrInstamt02(0);
		covrmjaIO.setCrInstamt03(0);
		covrmjaIO.setCrInstamt04(0);
		covrmjaIO.setCrInstamt05(0);
		covrmjaIO.setEstMatValue01(0);
		covrmjaIO.setEstMatValue02(0);
		covrmjaIO.setEstMatDate01(0);
		covrmjaIO.setEstMatDate02(0);
		covrmjaIO.setEstMatInt01(0);
		covrmjaIO.setAnnivProcDate(0);
		covrmjaIO.setBenBillDate(0);
		covrmjaIO.setConvertInitialUnits(0);
		covrmjaIO.setExtraAllocDate(0);
		covrmjaIO.setInitUnitCancDate(0);
		covrmjaIO.setInitUnitIncrsDate(0);
		covrmjaIO.setRerateDate(0);
		covrmjaIO.setRerateFromDate(0);
		covrmjaIO.setReviewProcessing(0);
		covrmjaIO.setUnitStatementDate(0);
		covrmjaIO.setEstMatInt02(0);
		covrmjaIO.setPremCurrency(payrIO.getCntcurr());
		covrmjaIO.setJlife(covtmjaIO.getJlife());
		if (isEQ(covtmjaIO.getJlife(),SPACES)
		|| isEQ(covtmjaIO.getJlife(),"00")) {
			covrmjaIO.setAnbAtCcd(covtmjaIO.getAnbAtCcd01());
			covrmjaIO.setSex(covtmjaIO.getSex01());
		}
		else {
			covrmjaIO.setAnbAtCcd(covtmjaIO.getAnbAtCcd02());
			covrmjaIO.setSex(covtmjaIO.getSex02());
		}
		covrmjaIO.setCrtable(covtmjaIO.getCrtable());
		covrmjaIO.setRiskCessDate(covtmjaIO.getRiskCessDate());
		covrmjaIO.setPremCessDate(covtmjaIO.getPremCessDate());
		covrmjaIO.setBenCessDate(covtmjaIO.getBenCessDate());
		covrmjaIO.setNextActDate(0);
		covrmjaIO.setRiskCessAge(covtmjaIO.getRiskCessAge());
		covrmjaIO.setPremCessAge(covtmjaIO.getPremCessAge());
		covrmjaIO.setBenCessAge(covtmjaIO.getBenCessAge());
		covrmjaIO.setRiskCessTerm(covtmjaIO.getRiskCessTerm());
		covrmjaIO.setPremCessTerm(covtmjaIO.getPremCessTerm());
		covrmjaIO.setBenCessTerm(covtmjaIO.getBenCessTerm());
		covrmjaIO.setPayrseqno(covtmjaIO.getPayrseqno());
		covrmjaIO.setVarSumInsured(0);
		covrmjaIO.setMortcls(covtmjaIO.getMortcls());
		covrmjaIO.setLiencd(covtmjaIO.getLiencd());
		covrmjaIO.setBappmeth(covtmjaIO.getBappmeth());
		covrmjaIO.setDeferPerdAmt(0);
		covrmjaIO.setTotMthlyBenefit(0);
		covrmjaIO.setCoverageDebt(0);
		covrmjaIO.setStatSumins(0);
		covrmjaIO.setRtrnyrs(0);
		covrmjaIO.setPremCessAgeMth(0);
		covrmjaIO.setPremCessAgeDay(0);
		covrmjaIO.setPremCessTermMth(0);
		covrmjaIO.setPremCessTermDay(0);
		covrmjaIO.setRiskCessAgeMth(0);
		covrmjaIO.setRiskCessAgeDay(0);
		covrmjaIO.setRiskCessTermMth(0);
		covrmjaIO.setRiskCessTermDay(0);
		covrmjaIO.setTransactionDate(wsaaTransactionDate);
		covrmjaIO.setTransactionTime(wsaaTransactionTime);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setReserveUnitsInd(covtmjaIO.getReserveUnitsInd());
		covrmjaIO.setReserveUnitsDate(covtmjaIO.getReserveUnitsDate());
		covrmjaIO.setTpdtype(covtmjaIO.getTpdtype());//ILIFE-7118
		covrmjaIO.setLnkgind(covtmjaIO.getLnkgind());
		covrmjaIO.setSingpremtype(covtmjaIO.getSingpremtype());//ILIFE-7805
		if (isEQ(t5687rec.bbmeth,SPACES)) {
			covrmjaIO.setBenBillDate(ZERO);
		}
		else {
			covrmjaIO.setBenBillDate(covrmjaIO.getCrrcd());
		}
		computeRerateDate4060();
		if (isEQ(chdrmjaIO.getBillfreq(),"00 ")) {
			y100SpDate();
		}
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			f000BenefitBilling();
		}
		covrmjaIO.setValidflag("1");
		stampDutyflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if (stampDutyflag) {
			covrmjaIO.setZstpduty01(covtmjaIO.getZstpduty01());
			covrmjaIO.setZclstate(covtmjaIO.getZclstate());
		}
		covrmjaIO.setRiskprem(covtmjaIO.getRiskprem()); //ILIFE-7845
		/*ILIFE-8248 start*/
		covrmjaIO.setLnkgno(covtmjaIO.getLnkgno());
		covrmjaIO.setLnkgsubrefno(covtmjaIO.getLnkgsubrefno());
		/*ILIFE-8248 end*/
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		updateExcl();
		/* Calculate tax                                                   */
		b100CalcTax();
		/* For a Single Premium component After each COVR is written       */
		/* do benefit billing. Call the benefit billing routine.           */
		if (isEQ(t5687rec.singlePremInd,"Y")
		&& isNE(t5534rec.subprog,SPACES)) {
			
		/*IVE-795 RUL Product - Benefit Billing Calculation started*/
		//callProgram(t5534rec.subprog, ubblallpar.ubblallRec);
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5534rec.subprog.toString()) && er.isExternalized(ubblallpar.cnttype.toString(), ubblallpar.crtable.toString())))
			{
				callProgramX(t5534rec.subprog, ubblallpar.ubblallRec); //ILIFE-8320
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxubblrec vpxubblrec = new Vpxubblrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

				vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);		
				//ubblallpar.sumins.format(ubblallpar.sumins.toString());
				vpxubblrec.function.set("INIT");
				callProgram(Vpxubbl.class, vpmcalcrec.vpmcalcRec,vpxubblrec);	
				ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
				vpmfmtrec.initialize();
				vpmfmtrec.date01.set(ubblallpar.effdate);
				
				//callProgram(t5534rec.subprog, ubblallpar.ubblallRec);
				callProgram(t5534rec.subprog, vpmfmtrec,ubblallpar.ubblallRec,vpxubblrec);
				
				callProgram(Vpuubbl.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				
				if(isEQ(ubblallpar.statuz,SPACES))
					ubblallpar.statuz.set(Varcom.oK);
			}
			/*IVE-795 RUL Product - Benefit Billing Calculation end*/
			t5534rec.t5534Rec.set(SPACES);
			if (isNE(ubblallpar.statuz,varcom.oK)) {
				syserrrec.params.set(ubblallpar.ubblallRec);
				syserrrec.statuz.set(ubblallpar.statuz);
				xxxxFatalError();
			}
		}
		if (flexiblePremium.isTrue()) {
			writeNewFpco5300();
		}
	}

protected void cashDividendAdd5100()
	{
			divAdd5100();
		}

protected void divAdd5100()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t6640);
		itdmIO.setItemitem(covtmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6640)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())) {
			t6640rec.t6640Rec.set(SPACES);
			return ;
		}
		else {
			t6640rec.t6640Rec.set(itdmIO.getGenarea());
		}
		if (isNE(t6640rec.zbondivalc,SPACES)) {
			readTh5025200();
		}
		else {
			th502rec.th502Rec.set(SPACES);
			return ;
		}
		//if (isEQ(th502rec.zrevbonalc,"X")) { 
		if (isEQ(th502rec.zrevbonalc,"Y")) { //ILIFE-6921 :Impact of ILIFE-4085 added this condition
			if (isEQ(t6640rec.revBonusMeth,SPACES)) {
				syserrrec.statuz.set(g586);
				xxxxFatalError();
			}
		}
		//if(isNE(th502rec.zcshdivalc,"X")){ 
		if(isNE(th502rec.zcshdivalc,"Y")) {//ILIFE-6921 :Impact of ILIFE-4085 added this condition
			return ;
		}
		if (isEQ(t6640rec.zcshdivmth,SPACES)) {
			syserrrec.statuz.set(g586);
			xxxxFatalError();
		}
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(atmodrec.company);
		hcsdIO.setChdrnum(covtmjaIO.getChdrnum());
		hcsdIO.setLife(covtmjaIO.getLife());
		hcsdIO.setCoverage(covtmjaIO.getCoverage());
		hcsdIO.setRider(covtmjaIO.getRider());
		hcsdIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		hcsdIO.setValidflag("1");
		hcsdIO.setTranno(covrmjaIO.getTranno());
		hcsdIO.setEffdate(covrmjaIO.getCrrcd());
		hcsdIO.setZdivopt(covtmjaIO.getZdivopt());
		hcsdIO.setZcshdivmth(t6640rec.zcshdivmth);
		hcsdIO.setPaycoy(covtmjaIO.getPaycoy());
		hcsdIO.setPayclt(covtmjaIO.getPayclt());
		hcsdIO.setPaymth(covtmjaIO.getPaymth());
		hcsdIO.setFacthous(covtmjaIO.getFacthous());
		hcsdIO.setBankkey(covtmjaIO.getBankkey());
		hcsdIO.setBankacckey(covtmjaIO.getBankacckey());
		hcsdIO.setPaycurr(covtmjaIO.getPaycurr());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readTh5025200()
	{
		start5200();
	}

protected void start5200()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.th502);
		itemIO.setItemitem(t6640rec.zbondivalc);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hl49);
			itemIO.setStatuz(hl49);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		th502rec.th502Rec.set(itemIO.getGenarea());
		if (isEQ(th502rec.zrevbonalc,SPACES)
		&& isEQ(th502rec.zcshdivalc,SPACES)) {
			syserrrec.statuz.set(hl49);
			itemIO.setStatuz(hl49);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
	}

protected void writeNewFpco5300()
	{
		start5310();
	}

protected void start5310()
	{
		fpcorevIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		fpcorevIO.setChdrnum(chdrmjaIO.getChdrnum());
		fpcorevIO.setLife(covrmjaIO.getLife());
		fpcorevIO.setJlife(covrmjaIO.getJlife());
		fpcorevIO.setCoverage(covrmjaIO.getCoverage());
		fpcorevIO.setRider(covrmjaIO.getRider());
		fpcorevIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		fpcorevIO.setValidflag("1");
		fpcorevIO.setTargfrom(payrIO.getBtdate());
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(payrIO.getBtdate());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		fpcorevIO.setTargto(datcon2rec.intDate2);
		fpcorevIO.setAnnivProcDate(datcon2rec.intDate2);
		fpcorevIO.setCurrfrom(wsaaToday);
		fpcorevIO.setEffdate(wsaaToday);
		fpcorevIO.setCurrto(varcom.vrcmMaxDate);
		fpcorevIO.setActiveInd("Y");
		fpcorevIO.setAnnProcessInd("N");
		setPrecision(fpcorevIO.getTargetPremium(), 2);
		fpcorevIO.setTargetPremium(mult(covrmjaIO.getInstprem(),wsaaBillfreq9));
		fpcorevIO.setPremRecPer(ZERO);
		fpcorevIO.setBilledInPeriod(ZERO);
		fpcorevIO.setOverdueMin(ZERO);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub,6)
		|| isEQ(t5729rec.frqcy[wsaaT5729Sub.toInt()],wsaaBillfreqX)); wsaaT5729Sub.add(1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT5729Sub,1)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMina01);
		}
		else if (isEQ(wsaaT5729Sub,2)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMinb01);
		}
		else if (isEQ(wsaaT5729Sub,3)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMinc01);
		}
		else if (isEQ(wsaaT5729Sub,4)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMind01);
		}
		else if (isEQ(wsaaT5729Sub,5)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMine01);
		}
		else if (isEQ(wsaaT5729Sub,6)){
			fpcorevIO.setMinOverduePer(t5729rec.overdueMinf01);
		}
		else{
			syserrrec.params.set(tablesInner.t5729);
			xxxxFatalError();
		}
		fpcorevIO.setTranno(chdrmjaIO.getTranno());
		fpcorevIO.setFormat(formatsInner.fpcorevrec);
		fpcorevIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void deleteCovt5500()
	{
		/*COVT*/
		covtmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			xxxxFatalError();
		}
		covtmjaIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void commissionProcess7000()
	{
		start7010();
	}

protected void start7010()
	{
		acomcalrec.acomcalRec.set(SPACES);
		acomcalrec.statuz.set(varcom.oK);
		acomcalrec.company.set(chdrmjaIO.getChdrcoy());
		acomcalrec.chdrnum.set(chdrmjaIO.getChdrnum());
		acomcalrec.covrLife.set(covrmjaIO.getLife());
		acomcalrec.covrCoverage.set(covrmjaIO.getCoverage());
		acomcalrec.covrRider.set(covrmjaIO.getRider());
		acomcalrec.covrPlanSuffix.set(covrmjaIO.getPlanSuffix());
		acomcalrec.covtLife.set(covtmjaIO.getLife());
		acomcalrec.covtCoverage.set(covtmjaIO.getCoverage());
		acomcalrec.covtRider.set(covtmjaIO.getRider());
		acomcalrec.covtPlanSuffix.set(covtmjaIO.getPlanSuffix());
		acomcalrec.tranno.set(chdrmjaIO.getTranno());
		/*acomcalrec.tranno.set(add(chdrmjaIO.getTranno(),1));*/
		acomcalrec.transactionDate.set(wsaaTransactionDate);
		acomcalrec.transactionTime.set(wsaaTransactionTime);
		acomcalrec.user.set(wsaaUser);
		acomcalrec.termid.set(wsaaTermid);
		acomcalrec.payrBtdate.set(payrIO.getBtdate());
		acomcalrec.payrPtdate.set(payrIO.getPtdate());
		acomcalrec.payrCntcurr.set(payrIO.getCntcurr());
		acomcalrec.cnttype.set(chdrmjaIO.getCnttype());
		acomcalrec.chdrOccdate.set(chdrmjaIO.getOccdate());
		acomcalrec.chdrAgntcoy.set(chdrmjaIO.getAgntcoy());
		acomcalrec.singlePremInd.set(t5687rec.singlePremInd);
		acomcalrec.covrJlife.set(covrmjaIO.getJlife());
		acomcalrec.covrCrtable.set(covrmjaIO.getCrtable());
		acomcalrec.comlvlacc.set(t5688rec.comlvlacc);
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			acomcalrec.bascpy.set(wsaaBasscpy);
			acomcalrec.basicCommMeth.set(wsaaBasscmth);
			acomcalrec.payrBillfreq.set("00");
			acomcalrec.chdrCurrfrom.set(covrmjaIO.getCurrfrom());
		}
		else {
			acomcalrec.basicCommMeth.set(wsaaBasicCommMeth);
			acomcalrec.bascpy.set(wsaaBascpy);
			acomcalrec.payrBillfreq.set(payrIO.getBillfreq());
			acomcalrec.chdrCurrfrom.set(chdrmjaIO.getCurrfrom());
		}
		if (flexiblePremium.isTrue()) {
			acomcalrec.fpcoTargPrem.set(fpcorevIO.getTargetPremium());
			acomcalrec.fpcoCurrto.set(fpcorevIO.getTargto());
		}
		else {
			acomcalrec.fpcoTargPrem.set(0);
			acomcalrec.fpcoCurrto.set(0);
		}
		acomcalrec.rnwcpy.set(wsaaRnwcpy);
		acomcalrec.srvcpy.set(wsaaSrvcpy);
		acomcalrec.covtAnnprem.set(wsaaCovtAnnprem);
		acomcalrec.covrAnnprem.set(wsaaCovrAnnprem);
		/*IBPLIFE-5237 starts */
		acomcalrec.covrCommPrem.set(wsaaCovrCommPrem);
		acomcalrec.covtCommPrem.set(wsaaCovtCommPrem);
		/*IBPLIFE-5237 ends */
		acomcalrec.atmdLanguage.set(atmodrec.language);
		acomcalrec.atmdBatchKey.set(atmodrec.batchKey);
		acomcalrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Acomcalc.class, acomcalrec.acomcalRec);
		if (isNE(acomcalrec.statuz,varcom.oK)) {
			syserrrec.params.set(acomcalrec.acomcalRec);
			syserrrec.statuz.set(acomcalrec.statuz);
			xxxxFatalError();
		}
	}

protected void activateReassurance8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start8001();
				case break8500:
					break8500();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start8001()
	{
		//PINNACLE-2323 START
		String jLife = "";
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		if(null != covrpfList && !covrpfList.isEmpty() && null != covrpf) {
			lifeenqIO.setLife(covrpf.getLife());
			jLife = covrpf.getJlife();
		}else {
			lifeenqIO.setLife(covrmjaIO.getLife());
			jLife = covrmjaIO.getJlife().toString();
		}
		//PINNACLE-2323 END
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			xxxxFatalError();
		}
		wsaaL1Clntnum.set(lifeenqIO.getLifcnum());
		//PINNACLE-2323
		if (isNE(jLife,"01")) {
			goTo(GotoLabel.break8500);
		}
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			wsaaL2Clntnum.set(lifeenqIO.getLifcnum());
		}
		else {
			wsaaL2Clntnum.set(SPACES);
		}
	}

protected void break8500()
	{
		actvresrec.actvresRec.set(SPACES);
		actvresrec.function.set("ACT8");
		actvresrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		actvresrec.chdrnum.set(chdrmjaIO.getChdrnum());
		if(null != covrpfList && !covrpfList.isEmpty() && null != covrpf) {
			actvresrec.life.set(covrpf.getLife());
			actvresrec.coverage.set(covrpf.getCoverage());
			actvresrec.rider.set(covrpf.getRider());
			actvresrec.planSuffix.set(covrpf.getPlanSuffix());
			actvresrec.crtable.set(covrpf.getCrtable());
			actvresrec.jlife.set(covrpf.getJlife());
			actvresrec.effdate.set(covrpf.getCrrcd());
			actvresrec.crrcd.set(covrpf.getCrrcd());
			actvresrec.newSumins.set(covrpf.getSumins());
			actvresrec.oldSumins.set(ZERO);
		}else {
			actvresrec.life.set(covrmjaIO.getLife());
			actvresrec.coverage.set(covrmjaIO.getCoverage());
			actvresrec.rider.set(covrmjaIO.getRider());
			actvresrec.planSuffix.set(covrmjaIO.getPlanSuffix());
			actvresrec.crtable.set(covrmjaIO.getCrtable());
			actvresrec.jlife.set(covrmjaIO.getJlife());
			actvresrec.effdate.set(covrmjaIO.getCrrcd());
			actvresrec.crrcd.set(covrmjaIO.getCrrcd());
			actvresrec.newSumins.set(covtmjaIO.getSumins());
			actvresrec.oldSumins.set(wsaaOldSumins);
		}
		actvresrec.clntcoy.set(wsaaFsuCoy);
		actvresrec.l1Clntnum.set(wsaaL1Clntnum);
		actvresrec.l2Clntnum.set(wsaaL2Clntnum);
		actvresrec.currency.set(chdrmjaIO.getCntcurr());
		actvresrec.cnttype.set(chdrmjaIO.getCnttype());
		actvresrec.tranno.set(chdrmjaIO.getTranno());
		actvresrec.language.set(atmodrec.language);
		actvresrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			xxxxFatalError();
		}
	}

protected void convertUntlToUlnk20000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para20000();
				case nextRecord20100:
					nextRecord20100();
				case read20200:
					read20200();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para20000()
	{
		unltmjaIO.setDataArea(SPACES);
		unltmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		unltmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		unltmjaIO.setLife(covtmjaIO.getLife());
		unltmjaIO.setCoverage(covtmjaIO.getCoverage());
		unltmjaIO.setRider(covtmjaIO.getRider());
		unltmjaIO.setSeqnbr(covtmjaIO.getPlanSuffix());
		unltmjaIO.setFunction(varcom.begnh);
		goTo(GotoLabel.read20200);
	}

protected void nextRecord20100()
	{
		unltmjaIO.setFunction(varcom.nextr);
	}

protected void read20200()
	{
		unltmjaio24000();
		if ((isEQ(unltmjaIO.getStatuz(),varcom.endp))) {
			return ;
		}
		if ((isNE(unltmjaIO.getChdrcoy(),covtmjaIO.getChdrcoy()))
		|| (isNE(unltmjaIO.getChdrnum(),covtmjaIO.getChdrnum()))
		|| (isNE(unltmjaIO.getLife(),covtmjaIO.getLife()))
		|| (isNE(unltmjaIO.getCoverage(),covtmjaIO.getCoverage()))
		|| (isNE(unltmjaIO.getRider(),covtmjaIO.getRider()))
		|| (isNE(unltmjaIO.getSeqnbr(),covtmjaIO.getPlanSuffix()))) {
			return ;
		}
		convertUnltmja21000();
		unltmjaIO.setFunction(varcom.delet);
		unltmjaio24000();
		goTo(GotoLabel.nextRecord20100);
	}

protected void convertUnltmja21000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					convert21000();
				case addUlnk21050:
					addUlnk21050();
				case exit21090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void convert21000()
	{
		storePrc22000();
		ulnkIO.setDataKey(SPACES);
		ulnkIO.setChdrcoy(unltmjaIO.getChdrcoy());
		ulnkIO.setChdrnum(unltmjaIO.getChdrnum());
		ulnkIO.setLife(unltmjaIO.getLife());
		ulnkIO.setJlife("00");
		ulnkIO.setCoverage(unltmjaIO.getCoverage());
		ulnkIO.setRider(unltmjaIO.getRider());
		ulnkIO.setPlanSuffix(unltmjaIO.getSeqnbr());
		ulnkIO.setFunction(varcom.readh);
		ulnkIO.setFormat(formatsInner.ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(),varcom.oK)
		&& isNE(ulnkIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(ulnkIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.addUlnk21050);
		}
		wsaaUlnkUpdate.set("N");
		for (wsaaA.set(1); !(isGT(wsaaA,10)); wsaaA.add(1)){
			updateUlnk27000();
		}
		if (wsaaNoUlnkUpdate.isTrue()) {
			goTo(GotoLabel.exit21090);
		}
		ulnkIO.setValidflag("2");
		ulnkIO.setFunction(varcom.rewrt);
		ulnkio26000();
	}

protected void addUlnk21050()
	{
		setupUlnk25000();
		ulnkIO.setFunction(varcom.writr);
		ulnkio26000();
		wsaaSub.set(1);
		a100CheckHitd();
	}

protected void storePrc22000()
	{
		/*PARA*/
		wsaaUalprc[1].set(unltmjaIO.getUalprc(1));
		wsaaUalprc[2].set(unltmjaIO.getUalprc(2));
		wsaaUalprc[3].set(unltmjaIO.getUalprc(3));
		wsaaUalprc[4].set(unltmjaIO.getUalprc(4));
		wsaaUalprc[5].set(unltmjaIO.getUalprc(5));
		wsaaUalprc[6].set(unltmjaIO.getUalprc(6));
		wsaaUalprc[7].set(unltmjaIO.getUalprc(7));
		wsaaUalprc[8].set(unltmjaIO.getUalprc(8));
		wsaaUalprc[9].set(unltmjaIO.getUalprc(9));
		wsaaUalprc[10].set(unltmjaIO.getUalprc(10));
		/*EXIT*/
	}

protected void unltmjaio24000()
	{
		/*READ*/
		unltmjaIO.setFormat(formatsInner.unltmjarec);
		SmartFileCode.execute(appVars, unltmjaIO);
		if (isNE(unltmjaIO.getStatuz(),varcom.oK)) {
			if (isNE(unltmjaIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(unltmjaIO.getParams());
				syserrrec.statuz.set(unltmjaIO.getStatuz());
				xxxxFatalError();
			}
		}
		/*EXIT*/
	}

protected void setupUlnk25000()
	{
		setup25010();
	}

protected void setup25010()
	{
		ulnkIO.setChdrcoy(unltmjaIO.getChdrcoy());
		ulnkIO.setChdrnum(unltmjaIO.getChdrnum());
		ulnkIO.setLife(unltmjaIO.getLife());
		ulnkIO.setJlife("00");
		ulnkIO.setCoverage(unltmjaIO.getCoverage());
		ulnkIO.setRider(unltmjaIO.getRider());
		ulnkIO.setPlanSuffix(unltmjaIO.getSeqnbr());
		ulnkIO.setUalfnds(unltmjaIO.getUalfnds());
		ulnkIO.setUalprc(1, wsaaUalprc[1]);
		ulnkIO.setUalprc(2, wsaaUalprc[2]);
		ulnkIO.setUalprc(3, wsaaUalprc[3]);
		ulnkIO.setUalprc(4, wsaaUalprc[4]);
		ulnkIO.setUalprc(5, wsaaUalprc[5]);
		ulnkIO.setUalprc(6, wsaaUalprc[6]);
		ulnkIO.setUalprc(7, wsaaUalprc[7]);
		ulnkIO.setUalprc(8, wsaaUalprc[8]);
		ulnkIO.setUalprc(9, wsaaUalprc[9]);
		ulnkIO.setUalprc(10, wsaaUalprc[10]);
		ulnkIO.setUspcprs(unltmjaIO.getUspcprs());
		ulnkIO.setTranno(unltmjaIO.getTranno());
		ulnkIO.setPercOrAmntInd(unltmjaIO.getPercOrAmntInd());
		ulnkIO.setCurrto(unltmjaIO.getCurrto());
		ulnkIO.setPremTopupInd(SPACES);
		ulnkIO.setValidflag("1");
		ulnkIO.setCurrfrom(0);
	}

protected void ulnkio26000()
	{
		/*CALL*/
		ulnkIO.setFormat(formatsInner.ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(ulnkIO.getChdrcoy(),covtmjaIO.getChdrcoy()))
		|| (isNE(ulnkIO.getChdrnum(),covtmjaIO.getChdrnum()))
		|| (isNE(ulnkIO.getLife(),covtmjaIO.getLife()))
		|| (isNE(ulnkIO.getCoverage(),covtmjaIO.getCoverage()))
		|| (isNE(ulnkIO.getRider(),covtmjaIO.getRider()))
		|| (isNE(ulnkIO.getPlanSuffix(),covtmjaIO.getPlanSuffix()))) {
			ulnkIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void updateUlnk27000()
	{
		/*ULNK*/
		if (isNE(ulnkIO.getPercOrAmntInd(),unltmjaIO.getPercOrAmntInd())) {
			wsaaUlnkUpdate.set("Y");
		}
		if (isNE(ulnkIO.getUalfnd(wsaaA),unltmjaIO.getUalfnd(wsaaA))) {
			wsaaUlnkUpdate.set("Y");
		}
		if (isNE(ulnkIO.getUalprc(wsaaA),unltmjaIO.getUalprc(wsaaA))) {
			wsaaUlnkUpdate.set("Y");
		}
		if (isNE(ulnkIO.getUspcpr(wsaaA),unltmjaIO.getUspcpr(wsaaA))) {
			wsaaUlnkUpdate.set("Y");
		}
		/*EXIT*/
	}

protected void a300DeletePcdtRec()
	{
		a300Start();
	}

protected void a300Start()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setLife(covtmjaIO.getLife());
		pcdtmjaIO.setCoverage(covtmjaIO.getCoverage());
		pcdtmjaIO.setRider(covtmjaIO.getRider());
		pcdtmjaIO.setTranno(chdrmjaIO.getTranno());
		pcdtmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			xxxxFatalError();
		}
		pcdtmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a400ReadPcdtRec()
	{
		a400SetupKey();
	}

protected void a400SetupKey()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setLife(covtmjaIO.getLife());
		pcdtmjaIO.setCoverage(covtmjaIO.getCoverage());
		pcdtmjaIO.setRider(covtmjaIO.getRider());
		pcdtmjaIO.setTranno(chdrmjaIO.getTranno());
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		pcdtmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a500AddPcdtRec()
	{
		/*A500-SETUP-KEY*/
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		pcdtmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*A500-EXIT*/
	}

protected void d000StampDuty()
	{
			d010Duty();
		}

protected void d010Duty()
	{
		if (isEQ(t5687rec.stampDutyMeth,SPACES)) {
			return ;
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5676);
		itemIO.setItemitem(t5687rec.stampDutyMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5676rec.t5676Rec.set(itemIO.getGenarea());
		if (isEQ(t5676rec.subprog,SPACES)) {
			return ;
		}
		stdtallrec.company.set(chdrmjaIO.getChdrcoy());
		stdtallrec.chdrnum.set(chdrmjaIO.getChdrnum());
		stdtallrec.life.set(covrmjaIO.getLife());
		stdtallrec.coverage.set(covrmjaIO.getCoverage());
		stdtallrec.rider.set(covrmjaIO.getRider());
		stdtallrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		stdtallrec.cntcurr.set(payrIO.getCntcurr());
		stdtallrec.effdate.set(covrmjaIO.getCrrcd());
		stdtallrec.stampDuty.set(0);
		callProgram(t5676rec.subprog, stdtallrec.stdt001Rec);
		zrdecplrec.amountIn.set(stdtallrec.stampDuty);
		callRounding9000();
		stdtallrec.stampDuty.set(zrdecplrec.amountOut);
	}

protected void e000T5671CompProcess()
	{
		try {
			e010ReadT5671();
		}
		catch (GOTOException e){
		}
	}

protected void e010ReadT5671()
	{
		wsaaCompkeyTranno.set(wsaaBatckey.batcBatctrcde);
		wsaaCompkeyCrtable.set(covrmjaIO.getCrtable());
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItemitem(wsaaCompkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.e090Exit);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		chdrlnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		chdrlnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		chdrlnbIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			xxxxFatalError();
		}
		isuallrec.company.set(chdrmjaIO.getChdrcoy());
		isuallrec.chdrnum.set(chdrmjaIO.getChdrnum());
		isuallrec.life.set(covrmjaIO.getLife());
		isuallrec.coverage.set(covrmjaIO.getCoverage());
		isuallrec.rider.set(covrmjaIO.getRider());
		isuallrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		isuallrec.covrSingp.set(covrmjaIO.getSingp());
		isuallrec.freqFactor.set(1);
		isuallrec.batchkey.set(atmodrec.batchKey);
		isuallrec.transactionDate.set(wsaaTransactionDate);
		isuallrec.transactionTime.set(wsaaTransactionTime);
		isuallrec.user.set(wsaaUser);
		isuallrec.termid.set(wsaaTermid);
		isuallrec.newTranno.set(ZERO);
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			isuallrec.effdate.set(covrmjaIO.getCurrfrom());
			isuallrec.runDate.set(covrmjaIO.getCurrfrom());
		}
		else {
			isuallrec.effdate.set(payrIO.getPtdate());
			isuallrec.runDate.set(payrIO.getPtdate());
		}
		if (flexiblePremium.isTrue()
		&& isNE(t5687rec.singlePremInd,"Y")) {
			isuallrec.effdate.set(wsaaOrigPtdate);
			isuallrec.runDate.set(wsaaOrigPtdate);
		}
		if(chnSurrend) {//ICIL-537
			isuallrec.effdate.set(wsaaOrigPtdate);
			isuallrec.runDate.set(wsaaOrigPtdate);
		}
		
		isuallrec.language.set(atmodrec.language);
		isuallrec.function.set(SPACES);
		for (sub1.set(1); !(isGT(sub1,3)); sub1.add(1)){
			e020SubCall();
		}
		//PINNACLE-2059
		if(effdateFlag && wsaaCompAdd.isTrue()) {
			updateCovrAnvProcDate();
		}
		goTo(GotoLabel.e090Exit);
	}

	protected void updateCovrAnvProcDate() {
		covrmjaIO.setAnnivProcDate(wsaaAnivDate);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void e020SubCall()
	{
		isuallrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.subprog[sub1.toInt()],SPACES)) {
			callProgram(t5671rec.subprog[sub1.toInt()], isuallrec.isuallRec);
		}
		if (isNE(isuallrec.statuz,varcom.oK)) {
			syserrrec.params.set(isuallrec.isuallRec);
			syserrrec.statuz.set(isuallrec.statuz);
			xxxxFatalError();
		}
	}

protected void f000BenefitBilling()
	{
			f010BenefitBilling();
		}

protected void f010BenefitBilling()
	{
		if (isEQ(t5687rec.bbmeth,SPACES)) {
			return ;
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5534);
		itemIO.setItemitem(t5687rec.bbmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5534rec.t5534Rec.set(itemIO.getGenarea());
		if (isEQ(t5534rec.subprog,SPACES)) {
			return ;
		}
		ubblallpar.ubblallRec.set(SPACES);
		ubblallpar.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
		ubblallpar.chdrChdrnum.set(chdrmjaIO.getChdrnum());
		ubblallpar.lifeLife.set(covrmjaIO.getLife());
		ubblallpar.lifeJlife.set(covrmjaIO.getJlife());
		ubblallpar.covrCoverage.set(covrmjaIO.getCoverage());
		ubblallpar.covrRider.set(covrmjaIO.getRider());
		ubblallpar.planSuffix.set(covrmjaIO.getPlanSuffix());
		ubblallpar.billfreq.set(t5534rec.unitFreq);
		ubblallpar.cntcurr.set(chdrmjaIO.getCntcurr());
		ubblallpar.cnttype.set(chdrmjaIO.getCnttype());
		ubblallpar.tranno.set(chdrmjaIO.getTranno());
		ubblallpar.effdate.set(covrmjaIO.getCrrcd());
		ubblallpar.premMeth.set(t5534rec.premmeth);
		ubblallpar.jlifePremMeth.set(t5534rec.jlPremMeth);
		ubblallpar.sumins.set(covrmjaIO.getSumins());
		ubblallpar.premCessDate.set(covrmjaIO.getPremCessDate());
		ubblallpar.crtable.set(covrmjaIO.getCrtable());
		ubblallpar.billchnl.set(covtmjaIO.getBillchnl());
		ubblallpar.mortcls.set(covtmjaIO.getMortcls());
		ubblallpar.svMethod.set(t5534rec.svMethod);
		ubblallpar.language.set(atmodrec.language);
		ubblallpar.user.set(wsaaUser);
		ubblallpar.batccoy.set(wsaaBatckey.batcBatccoy);
		ubblallpar.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ubblallpar.batcactyr.set(wsaaBatckey.batcBatcactyr);
		ubblallpar.batcactmn.set(wsaaBatckey.batcBatcactmn);
		ubblallpar.batctrcde.set(wsaaBatckey.batcBatctrcde);
		ubblallpar.batch.set(wsaaBatckey.batcBatcbatch);
		ubblallpar.tranno.set(chdrmjaIO.getTranno());
		ubblallpar.adfeemth.set(t5534rec.adfeemth);
		ubblallpar.function.set("ISSUE");
		ubblallpar.polsum.set(chdrmjaIO.getPolsum());
		ubblallpar.ptdate.set(chdrmjaIO.getPtdate());
		ubblallpar.polinc.set(chdrmjaIO.getPolinc());
		ubblallpar.occdate.set(chdrmjaIO.getOccdate());
		ubblallpar.chdrRegister.set(chdrmjaIO.getRegister());
		/* Get the next benefit billing date.                              */
		/*    MOVE SPACE                  TO DTC2-DATCON2-REC.     <LA4351>*/
		/*    MOVE T5534-UNIT-FREQ        TO DTC2-FREQUENCY.       <LA4351>*/
		/*    MOVE 1                      TO DTC2-FREQ-FACTOR.     <LA4351>*/
		/*    MOVE COVRMJA-CRRCD          TO DTC2-INT-DATE-1.      <LA4351>*/
		/*    MOVE 0                      TO DTC2-INT-DATE-2.      <LA4351>*/
		/*    CALL 'DATCON2'           USING DTC2-DATCON2-REC.     <LA4351>*/
		/*    IF  DTC2-STATUZ        NOT  = O-K                    <LA4351>*/
		/*        MOVE DTC2-DATCON2-REC   TO SYSR-PARAMS           <LA4351>*/
		/*        MOVE DTC2-STATUZ        TO SYSR-STATUZ           <LA4351>*/
		/*        PERFORM XXXX-FATAL-ERROR                         <LA4351>*/
		/*    END-IF.                                              <LA4351>*/
		/*    MOVE DTC2-INT-DATE-2        TO COVRMJA-BEN-BILL-DATE.<LA4351>*/
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.frequency.set(t5534rec.unitFreq);
		datcon4rec.freqFactor.set(1);
		datcon4rec.intDate1.set(covrmjaIO.getCrrcd());
		wsaaOccdate.set(covrmjaIO.getCrrcd());
		datcon4rec.billdayNum.set(wsaaOccDd);
		datcon4rec.billmonthNum.set(wsaaOccMm);
		datcon4rec.intDate2.set(0);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		covrmjaIO.setBenBillDate(datcon4rec.intDate2);
	}

protected void a100WopDates()
{
	covrlnbList = covrpupDAO.getCovrsurByComAndNum(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
	for (Covrpf covrmjaIO : covrlnbList) {
		a210Covrmja(covrmjaIO);
	}
}
protected void a210Covrmja(Covrpf covrmjaIO)
	{
		a300ReadTr517(covrmjaIO);
		wsaaCpiValid.set("N");
		if (wopMatch.isTrue()) {
			wsaaTr517Rec.set(tr517rec.tr517Rec);
			if (isGT(covrmjaIO.getCpiDate(),covrmjaIO.getRerateDate())
			|| isEQ(covrmjaIO.getCpiDate(),0)) {
				wsaaEarliestRerateDate.set(covrmjaIO.getRerateDate());
			}
			else {
				wsaaEarliestRerateDate.set(covrmjaIO.getCpiDate());
			}
			if (isNE(tr517rec.zrwvflg02,"Y")) {
				covrlnbIO.setParams(SPACES);
				covrlnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
				covrlnbIO.setChdrnum(covrmjaIO.getChdrnum());
				covrlnbIO.setLife(covrmjaIO.getLife());
				covrlnbIO.setPlanSuffix(0);
				covrlnbIO.setFormat(formatsInner.covrlnbrec);
				covrlnbIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
				while ( !(isNE(covrlnbIO.getChdrcoy(),covrmjaIO.getChdrcoy())
				|| isNE(covrlnbIO.getChdrnum(),covrmjaIO.getChdrnum())
				|| isNE(covrlnbIO.getLife(),covrmjaIO.getLife())
				|| isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
					a400SetWopDate();
				}

				if (isNE(wsaaEarliestRerateDate, 0)) {
					a240RewriteCovrmja(covrmjaIO);
				}
			}
			else {
				covrlnbIO.setParams(SPACES);
				covrlnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
				covrlnbIO.setChdrnum(covrmjaIO.getChdrnum());
				covrlnbIO.setPlanSuffix(0);
				covrlnbIO.setFormat(formatsInner.covrlnbrec);
				covrlnbIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
				while ( !(isNE(covrlnbIO.getChdrcoy(),covrmjaIO.getChdrcoy())
				|| isNE(covrlnbIO.getChdrnum(),covrmjaIO.getChdrnum())
				|| isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
					a400SetWopDate();
				}

				if (isNE(wsaaEarliestRerateDate, 0)) {
					a240RewriteCovrmja(covrmjaIO);
				}
			}
		}
	}


protected void a240RewriteCovrmja(Covrpf covrmjaIO)
	{
	covrmjaIO.setRerateDate(wsaaEarliestRerateDate.toInt());
	covrpupDAO.updateCovrpf(covrmjaIO);
	if (cpiValid.isTrue()) {
	covrmjaIO.setCpiDate(wsaaEarliestRerateDate.toInt());
	covrpupDAO.updateCovrpfRecord(covrmjaIO);
	}
	}

protected void a270Covrmjaio()
	{
		/*A280-READ-COVRMJA*/
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			xxxxFatalError();
		}
		/*A290-EXIT*/
	}

protected void a300ReadTr517(Covrpf covrmjaIO)
	{
		a310Read(covrmjaIO);
	}

protected void a310Read(Covrpf covrmjaIO)
	{
		/* Read TR517 for Wavier of Premium Component.                     */
		wopNotMatch.setTrue();
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(subString(itdmIO.getItemitem(), 1, 4),covrmjaIO.getCrtable())) {
			return ;
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			wopMatch.setTrue();
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
		}
	}

protected void a30cReadTr517()
	{
			a31cRead();
		}

protected void a31cRead()
	{
		wsaaWaiveCont = "N";
		wsaaZrwvflgs.set(tr517rec.zrwvflgs);
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(tr517rec.contitem);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(subString(itdmIO.getItemitem(), 1, 4),tr517rec.contitem)) {
			return ;
		}
		wsaaWaiveCont = "Y";
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		tr517rec.zrwvflgs.set(wsaaZrwvflgs);
	}

protected void a400SetWopDate()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a410SetWop();
				case a420Check:
					a420Check();
				case a480Next:
					a480Next();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410SetWop()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		tr517rec.tr517Rec.set(wsaaTr517Rec);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			xxxxFatalError();
		}
		if (isNE(covrlnbIO.getValidflag(),"1")) {
			goTo(GotoLabel.a480Next);
		}
		wsaaCrtableMatch.set("N");
	}

protected void a420Check()
	{
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,50)
		|| crtableMatch.isTrue()); wsaaCnt.add(1)){
			if (isEQ(covrlnbIO.getCrtable(),tr517rec.ctable[wsaaCnt.toInt()])) {
				crtableMatch.setTrue();
			}
		}
		if (!crtableMatch.isTrue()
		&& isNE(tr517rec.contitem,SPACES)) {
			a30cReadTr517();
			if (isEQ(wsaaWaiveCont,"Y")) {
				goTo(GotoLabel.a420Check);
			}
		}
		if (crtableMatch.isTrue()
		&& (isEQ(tr517rec.zrwvflg02,"Y")
		|| isEQ(covrlnbIO.getLife(),covrmjaIO.getLife()))) {
			if (isLT(covrlnbIO.getRerateDate(),wsaaEarliestRerateDate)
			&& isGT(covrlnbIO.getRerateDate(),0)) {
				wsaaEarliestRerateDate.set(covrlnbIO.getRerateDate());
			}
			if (isNE(covrlnbIO.getCpiDate(),varcom.vrcmMaxDate)) {
				cpiValid.setTrue();
			}
			if (isLT(covrlnbIO.getCpiDate(),wsaaEarliestRerateDate)
			&& isGT(covrlnbIO.getCpiDate(),0)) {
				wsaaEarliestRerateDate.set(covrlnbIO.getCpiDate());
			}
		}
	}

protected void a480Next()
	{
		covrlnbIO.setFunction(varcom.nextr);
		/*A490-EXIT*/
	}

protected void x100WritePtrn()
	{
		x110PtrnRecord();
	}

protected void x110PtrnRecord()
	{
		/* Write a transaction record to PTRN file*/
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcpfx(wsaaBatckey.batcBatcpfx);
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setCrtuser(username); //PINNACLE-2931
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(payrIO.getPtdate());
		/* MOVE WSAA-TODAY             TO PTRN-PTRNEFF.                 */
		/* MOVE PAYR-PTDATE            TO PTRN-DATESUB.         <LA5184>*/
		if (flexiblePremium.isTrue() || chnSurrend) { //ICIL-537
			ptrnIO.setPtrneff(wsaaEffdate);
			/*****                                PTRN-DATESUB          <LA5184>*/
		}
		
		/* IF CHDRMJA-BILLFREQ          = '00 '                 <LA5184>*/
		/*    MOVE WSAA-TODAY          TO PTRN-DATESUB          <LA5184>*/
		/* END-IF.                                              <LA5184>*/
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			xxxxFatalError();
		}
	}

protected void x200UpdateBatchHeader()
	{
		/*X210-UPDATE-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/*X290-EXIT*/
	}

protected void x300ReleaseSftlck()
	{
		x310ReleaseSoftlock();
	}

protected void x310ReleaseSoftlock()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(atmodrec.primaryKey);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void x400Statistics()
	{
		x410Start();
	}

protected void x410Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void y100SpDate()
	{
		y110Start();
	}

protected void y110Start()
	{
		wsaaDayTemp.set(payrIO.getBtdate());
		wsaaDayOld.set(wsaaDayTemp);
		wsaaDayTemp.set(wsaaToday);
		wsaaDayNew.set(wsaaDayTemp);
		if (isLT(wsaaDdNew,wsaaDdOld)) {
			compute(wsaaMmNew, 0).set(sub(wsaaMmNew,1));
			if (isEQ(wsaaMmNew,0)) {
				wsaaMmNew.set(12);
				compute(wsaaYyNew, 0).set(sub(wsaaYyNew,1));
			}
		}
		wsaaDdNew.set(wsaaDdOld);
		wsaaDayTemp.set(wsaaDayNew);
		/*    MOVE WSAA-DAY-TEMP          TO COVRMJA-BEN-BILL-DATE <LA4477>*/
		/*                                   COVRMJA-CURRFROM      <LA4477>*/
		/*                                   COVRMJA-CRRCD.        <LA4477>*/
		covrmjaIO.setCurrfrom(wsaaDayTemp);
		covrmjaIO.setCrrcd(wsaaDayTemp);
		if (isEQ(t5687rec.bbmeth, SPACES)) {
			covrmjaIO.setBenBillDate(ZERO);
		}
		else {
			covrmjaIO.setBenBillDate(wsaaDayTemp);
		}
	}

protected void m800UpdateMlia()
	{
		/*M800-START*/
		rlliadbrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		rlliadbrec.chdrnum.set(chdrmjaIO.getChdrnum());
		rlliadbrec.fsucoy.set(wsaaFsuCoy);
		rlliadbrec.batctrcde.set(ptrnIO.getBatctrcde());
		rlliadbrec.language.set(atmodrec.language);
		callProgram(Rlliadb.class, rlliadbrec.rlliaRec);
		if (isNE(rlliadbrec.statuz,varcom.oK)) {
			syserrrec.params.set(rlliadbrec.rlliaRec);
			syserrrec.statuz.set(rlliadbrec.statuz);
			xxxxFatalError();
		}
		/*M800-EXIT*/
	}

protected void a100CheckHitd()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case a110Fund:
					a110Fund();
				case a180NextFund:
					a180NextFund();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Fund()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(chdrmjaIO.getCurrfrom());
		itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub));
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.endp)
		&& isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(),ulnkIO.getUalfnd(wsaaSub))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isNE(t5515rec.zfundtyp,"D")) {
			goTo(GotoLabel.a180NextFund);
		}
		hitdIO.setParams(SPACES);
		hitdIO.setChdrcoy(covrmjaIO.getChdrcoy());
		hitdIO.setChdrnum(covrmjaIO.getChdrnum());
		hitdIO.setLife(covrmjaIO.getLife());
		hitdIO.setCoverage(covrmjaIO.getCoverage());
		hitdIO.setRider(covrmjaIO.getRider());
		hitdIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		hitdIO.setZintbfnd(ulnkIO.getUalfnd(wsaaSub));
		hitdIO.setFormat(formatsInner.hitdrec);
		hitdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)
		&& isNE(hitdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hitdIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(hitdIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.a180NextFund);
		}
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			hitdIO.setEffdate(covrmjaIO.getCurrfrom());
		}
		else {
			if (flexiblePremium.isTrue() || chnSurrend) {
				hitdIO.setEffdate(wsaaOrigPtdate);
			}
			else {
				hitdIO.setEffdate(payrIO.getPtdate());
			}
		}
		hitdIO.setZlstintdte(varcom.vrcmMaxDate);
		hitdIO.setZlstsmtdt(varcom.vrcmMaxDate);
		hitdIO.setZlstfndval(ZERO);
		hitdIO.setZlstsmtno(ZERO);
		hitdIO.setZlstsmtbal(ZERO);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.th510);
		itdmIO.setItmfrm(hitdIO.getEffdate());
		itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub));
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.th510)
		|| isNE(itdmIO.getItemitem(),ulnkIO.getUalfnd(wsaaSub))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemtabl(tablesInner.th510);
			itdmIO.setItemitem(ulnkIO.getUalfnd(wsaaSub));
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		th510rec.th510Rec.set(itdmIO.getGenarea());
		a300GetNextFreq();
		hitdIO.setZnxtintdte(datcon2rec.intDate2);
		hitdIO.setTranno(ulnkIO.getTranno());
		hitdIO.setValidflag("1");
		hitdIO.setEffdate(hitdIO.getEffdate());
		hitdIO.setFormat(formatsInner.hitdrec);
		hitdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			xxxxFatalError();
		}
	}

protected void a180NextFund()
	{
		wsaaSub.add(1);
		if (isLTE(wsaaSub,10)) {
			goTo(GotoLabel.a110Fund);
		}
		/*A190-EXIT*/
	}

protected void a300GetNextFreq()
	{
		a310Next();
	}

protected void a310Next()
	{
		/*    MOVE SPACES                 TO DTC2-DATCON2-REC.     <LA4351>*/
		/*    MOVE HITD-EFFDATE           TO DTC2-INT-DATE-1.      <LA4351>*/
		/*    MOVE 1                      TO DTC2-FREQ-FACTOR.     <LA4351>*/
		/*    MOVE TH510-ZINTALOFRQ       TO DTC2-FREQUENCY.       <LA4351>*/
		/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC.  <LA4351>*/
		/*    IF DTC2-STATUZ              NOT = O-K                <LA4351>*/
		/*        MOVE DTC2-DATCON2-REC   TO SYSR-PARAMS           <LA4351>*/
		/*        MOVE DTC2-STATUZ        TO SYSR-STATUZ           <LA4351>*/
		/*        PERFORM XXXX-FATAL-ERROR                         <LA4351>*/
		/*    END-IF.                                              <LA4351>*/
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(hitdIO.getEffdate());
		wsaaOccdate.set(hitdIO.getEffdate());
		datcon4rec.intDate2.set(0);
		datcon4rec.billdayNum.set(wsaaOccDd);
		datcon4rec.billmonthNum.set(wsaaOccMm);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(th510rec.zintalofrq);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate2.set(datcon4rec.intDate2);
		if (isNE(th510rec.zintfixdd,ZERO)) {
			/*     MOVE TH510-ZINTFIXDD    TO DTC2-INT-DATE-2 (5:2) <LA4737>*/
			datcon2rec.intDate2.setSub1String(7, 2, th510rec.zintfixdd);
		}
	}

protected void a500AddUnderwritting()
	{
		a500Ctrl();
	}

protected void a500Ctrl()
	{
		/* PERFORM A600-DEL-UNDERWRITTING.                     <LFA1062>*/
		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrmjaIO.setStatuz(varcom.oK);
		while ( !(isNE(covrmjaIO.getStatuz(),varcom.oK))) {
			a600DelUnderwritting();
		}

		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrmjaIO.setStatuz(varcom.oK);
		while ( !(isNE(covrmjaIO.getStatuz(),varcom.oK))) {
			a800NextrCovrmjaio();
		}

	}

protected void a600DelUnderwritting()
	{
			a600Ctrl();
		}

protected void a600Ctrl()
	{
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (!(isEQ(covrmjaIO.getStatuz(),varcom.oK)
		&& isEQ(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		&& isEQ(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum()))) {
			covrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		covrmjaIO.setFunction(varcom.nextr);
		initialize(crtundwrec.parmRec);
		a900ReadLife();
		crtundwrec.clntnum.set(lifeenqIO.getLifcnum());
		crtundwrec.coy.set(chdrmjaIO.getChdrcoy());
		crtundwrec.currcode.set(chdrmjaIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrmjaIO.getChdrnum());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
	}

protected void a800NextrCovrmjaio()
	{
			a800Ctrl();
		}

protected void a800Ctrl()
	{
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (!(isEQ(covrmjaIO.getStatuz(),varcom.oK)
		&& isEQ(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		&& isEQ(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum()))) {
			covrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		covrmjaIO.setFunction(varcom.nextr);
		initialize(crtundwrec.parmRec);
		a900ReadLife();
		crtundwrec.clntnum.set(lifeenqIO.getLifcnum());
		crtundwrec.coy.set(covrmjaIO.getChdrcoy());
		crtundwrec.chdrnum.set(covrmjaIO.getChdrnum());
		crtundwrec.life.set(covrmjaIO.getLife());
		crtundwrec.crtable.set(covrmjaIO.getCrtable());
		crtundwrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		crtundwrec.sumins.set(covrmjaIO.getSumins());
		crtundwrec.cnttyp.set(chdrmjaIO.getCnttype());
		crtundwrec.currcode.set(chdrmjaIO.getCntcurr());
		crtundwrec.function.set("ADD");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
	}

protected void a900ReadLife()
	{
		a900Ctrl();
	}

protected void a900Ctrl()
	{
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void b100CalcTax()
	{
		b110Start();
	}

protected void b110Start()
	{
		wsaaTotalTax.set(ZERO);
		/* Read table TR52D using CHDRMJA-REGISTER as key                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrmjaIO.getRegister());
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItemcoy(atmodrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setItempfx("IT");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)
		|| isEQ(covrmjaIO.getSingp(), ZERO)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		wsaaTr52eCrtable.set(covrmjaIO.getCrtable());
		b200ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			b200ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b200ReadTr52e();
		}
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		/* Call Tax subroutine                                             */
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		txcalcrec.life.set(covrmjaIO.getLife());
		txcalcrec.coverage.set(covrmjaIO.getCoverage());
		txcalcrec.rider.set(covrmjaIO.getRider());
		txcalcrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		txcalcrec.crtable.set(covrmjaIO.getCrtable());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		wsaaCntCurr.set(chdrmjaIO.getCntcurr());
		txcalcrec.tranno.set(chdrmjaIO.getTranno());
		txcalcrec.language.set(atmodrec.language);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.transType.set("PREM");
		txcalcrec.effdate.set(wsaaEffdate);
		txcalcrec.jrnseq.set(wsaaJrnseq);
		txcalcrec.batckey.set(atmodrec.batchKey);
		txcalcrec.amountIn.set(covrmjaIO.getSingp());
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			xxxxFatalError();
		}
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaTotalTax.add(txcalcrec.taxAmt[1]);
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaTotalTax.add(txcalcrec.taxAmt[2]);
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		taxdIO.setChdrnum(chdrmjaIO.getChdrnum());
		taxdIO.setLife(covrmjaIO.getLife());
		taxdIO.setCoverage(covrmjaIO.getCoverage());
		taxdIO.setRider(covrmjaIO.getRider());
		taxdIO.setPlansfx(covrmjaIO.getPlanSuffix());
		taxdIO.setEffdate(txcalcrec.effdate);
		taxdIO.setInstfrom(txcalcrec.effdate);
		taxdIO.setBillcd(wsaaEffdate);
		taxdIO.setInstto(payrIO.getPtdate());
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setTranno(chdrmjaIO.getTranno());
		taxdIO.setTrantype("PREM");
		taxdIO.setBaseamt(covrmjaIO.getSingp());
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdIO.getParams());
			syserrrec.statuz.set(taxdIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void b200ReadTr52e()
	{
		b210Start();
	}

protected void b210Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(wsaaEffdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (((isNE(itdmIO.getItemcoy(), atmodrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (((isEQ(itdmIO.getItemcoy(), atmodrec.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuCoy);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrmjaIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypRcesdte.set(covrmjaIO.getRiskCessDate());
		if (isEQ(covrmjaIO.getUnitStatementDate(), 0)) {
			drypDryprcRecInner.drypCbunst.set(covrmjaIO.getCrrcd());
		}
		else {
			drypDryprcRecInner.drypCbunst.set(covrmjaIO.getUnitStatementDate());
		}
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void callRounding9000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(atmodrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t5534 = new FixedLengthStringData(5).init("T5534");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5676 = new FixedLengthStringData(5).init("T5676");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData t5448 = new FixedLengthStringData(5).init("T5448");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData th510 = new FixedLengthStringData(5).init("TH510");
	private FixedLengthStringData th502 = new FixedLengthStringData(5).init("TH502");
	private FixedLengthStringData t6640 = new FixedLengthStringData(5).init("T6640");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData tr695 = new FixedLengthStringData(5).init("TR695");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t6658 = new FixedLengthStringData(5).init("T6658");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC   ");
	private FixedLengthStringData unltmjarec = new FixedLengthStringData(10).init("UNLTMJAREC");
	private FixedLengthStringData ulnkrec = new FixedLengthStringData(10).init("ULNKREC   ");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC   ");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData fpcorevrec = new FixedLengthStringData(10).init("FPCOREVREC");
	private FixedLengthStringData lifeenqrec = new FixedLengthStringData(10).init("LIFEENQREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hitdrec = new FixedLengthStringData(10).init("HITDREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	private PackedDecimalData drypRcesdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 61);
	private PackedDecimalData drypCbunst = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 66);
}
}
