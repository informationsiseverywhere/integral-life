package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:26
 * Description:
 * Copybook name: TR52DREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr52drec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr52dRec = new FixedLengthStringData(500);
  	public FixedLengthStringData txcode = new FixedLengthStringData(1).isAPartOf(tr52dRec, 0);
  	public FixedLengthStringData txsubr = new FixedLengthStringData(10).isAPartOf(tr52dRec, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(489).isAPartOf(tr52dRec, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr52dRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr52dRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}