package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PrmhpfDAOImpl extends BaseDAOImpl<Prmhpf> implements PrmhpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(PrmhpfDAOImpl.class);
	
	public Map<String, List<Prmhpf>> getPrmhRecordByTodate(String chdrcoy, List<String> chdrnumList, int effdate) {
		Prmhpf prmhpf = null;
		Map<String, List<Prmhpf>> prmhListMap = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM PRMHPF WHERE CHDRCOY=? AND ");
		if(chdrnumList != null && !chdrnumList.isEmpty())
			sb.append(getSqlInStr("CHDRNUM", chdrnumList)).append(" AND ");
		sb.append("TODATE<=? AND VALIDFLAG='1' ORDER BY TRANNO DESC");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setInt(2, effdate);
			rs = ps.executeQuery();
			while(rs.next()) {
				prmhpf = new Prmhpf();
				prmhpf.setChdrcoy(rs.getString("CHDRCOY"));
				prmhpf.setChdrnum(rs.getString("CHDRNUM"));
				prmhpf.setValidflag(rs.getString("VALIDFLAG"));
				prmhpf.setActind(rs.getString("ACTIND"));
				prmhpf.setFrmdate(rs.getInt("FRMDATE"));
				prmhpf.setTodate(rs.getInt("TODATE"));
				prmhpf.setTranno(rs.getInt("TRANNO"));
				prmhpf.setEffdate(rs.getInt("EFFDATE"));
				prmhpf.setApind(rs.getString("APIND"));
				prmhpf.setLogtype(rs.getString("LOGTYPE"));
				prmhpf.setCrtuser(rs.getString("CRTUSER"));
				prmhpf.setLtranno(rs.getInt("LTRANNO"));
				prmhpf.setBillfreq(rs.getString("BILLFREQ"));
				prmhpf.setZmthos(rs.getInt("ZMTHOS"));
				prmhpf.setUsrprf(rs.getString("USRPRF"));
				prmhpf.setJobnm(rs.getString("JOBNM"));
				if (prmhListMap.containsKey(prmhpf.getChdrnum())) {
					prmhListMap.get(prmhpf.getChdrnum()).add(prmhpf);
                } else {
                    List<Prmhpf> prmhpfList = new ArrayList<Prmhpf>();
                    prmhpfList.add(prmhpf);
                    prmhListMap.put(prmhpf.getChdrnum(), prmhpfList);
                }
			}
		}
		catch(SQLException e) {
			LOGGER.error("getPrmhRecordByTodate()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return prmhListMap;
	}
	
	public Prmhpf getPrmhRecord(String chdrcoy, String chdrnum) {
		Prmhpf prmhpf = null;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM PRMHPF WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG='1'");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			rs = ps.executeQuery();
			while(rs.next()) {
				prmhpf = new Prmhpf();
				prmhpf.setChdrcoy(rs.getString("CHDRCOY"));
				prmhpf.setChdrnum(rs.getString("CHDRNUM"));
				prmhpf.setValidflag(rs.getString("VALIDFLAG"));
				prmhpf.setActind(rs.getString("ACTIND"));
				prmhpf.setFrmdate(rs.getInt("FRMDATE"));
				prmhpf.setTodate(rs.getInt("TODATE"));
				prmhpf.setTranno(rs.getInt("TRANNO"));
				prmhpf.setEffdate(rs.getInt("EFFDATE"));
				prmhpf.setApind(rs.getString("APIND"));
				prmhpf.setLogtype(rs.getString("LOGTYPE"));
				prmhpf.setCrtuser(rs.getString("CRTUSER"));
				prmhpf.setLtranno(rs.getInt("LTRANNO"));
				prmhpf.setBillfreq(rs.getString("BILLFREQ"));
				prmhpf.setZmthos(rs.getInt("ZMTHOS"));
				prmhpf.setUsrprf(rs.getString("USRPRF"));
				prmhpf.setJobnm(rs.getString("JOBNM"));
			}
		}
		catch(SQLException e) {
			LOGGER.error("getPrmhRecord()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return prmhpf;
	}
	
	public Prmhpf getPrmhenqRecord(String chdrcoy, String chdrnum) {
		Prmhpf prmhpf = null;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM PRMHENQ WHERE CHDRCOY=? AND CHDRNUM=? ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			rs = ps.executeQuery();
			while(rs.next()) {
				prmhpf = new Prmhpf();
				prmhpf.setChdrcoy(rs.getString("CHDRCOY"));
				prmhpf.setChdrnum(rs.getString("CHDRNUM"));
				prmhpf.setValidflag(rs.getString("VALIDFLAG"));
				prmhpf.setActind(rs.getString("ACTIND"));
				prmhpf.setFrmdate(rs.getInt("FRMDATE"));
				prmhpf.setTodate(rs.getInt("TODATE"));
				prmhpf.setTranno(rs.getInt("TRANNO"));
				prmhpf.setEffdate(rs.getInt("EFFDATE"));
				prmhpf.setApind(rs.getString("APIND"));
				prmhpf.setLogtype(rs.getString("LOGTYPE"));
				prmhpf.setCrtuser(rs.getString("CRTUSER"));
				prmhpf.setLtranno(rs.getInt("LTRANNO"));
				prmhpf.setBillfreq(rs.getString("BILLFREQ"));
				prmhpf.setZmthos(rs.getInt("ZMTHOS"));
				prmhpf.setUsrprf(rs.getString("USRPRF"));
				prmhpf.setJobnm(rs.getString("JOBNM"));
			}
		}
		catch(SQLException e) {
			LOGGER.error("getPrmhenqRecord()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return prmhpf;
	}
	
	public void insertPrmhenqRecord(Prmhpf prmhpf) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO PRMHENQ (CHDRCOY, CHDRNUM, VALIDFLAG, ACTIND, FRMDATE, TODATE, TRANNO, EFFDATE, APIND, LOGTYPE, CRTUSER, LTRANNO, BILLFREQ, ZMTHOS, JOBNM, USRPRF, DATIME)");
		sb.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		int i = 0;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(++i, prmhpf.getChdrcoy());
			ps.setString(++i, prmhpf.getChdrnum());
			ps.setString(++i, prmhpf.getValidflag());
			ps.setString(++i, prmhpf.getActind());
			ps.setInt(++i,  prmhpf.getFrmdate());
			ps.setInt(++i, prmhpf.getTodate());
			ps.setInt(++i, prmhpf.getTranno());
			ps.setInt(++i, prmhpf.getEffdate());
			ps.setString(++i, prmhpf.getApind());
			ps.setString(++i,  prmhpf.getLogtype());
			ps.setString(++i,  prmhpf.getCrtuser());
			ps.setInt(++i, prmhpf.getLtranno());
			ps.setString(++i,  prmhpf.getBillfreq());
			ps.setInt(++i,  prmhpf.getZmthos());
			ps.setString(++i, getJobnm());
            ps.setString(++i, getUsrprf());
            ps.setTimestamp(++i, new Timestamp(System.currentTimeMillis()));
            
            ps.executeUpdate();
		}
		catch(SQLException e) {
			LOGGER.error("insertPrmhenqRecord()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
	}
}