package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6663screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 19, 15, 67}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6663ScreenVars sv = (S6663ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6663screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6663ScreenVars screenVars = (S6663ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.payrname.setClassString("");
		screenVars.payraddr1.setClassString("");
		screenVars.payraddr2.setClassString("");
		screenVars.payraddr3.setClassString("");
		screenVars.payraddr4.setClassString("");
		screenVars.payraddr5.setClassString("");
		screenVars.incomeSeqNo.setClassString("");
		screenVars.cltpcode.setClassString("");
	}

/**
 * Clear all the variables in S6663screen
 */
	public static void clear(VarModel pv) {
		S6663ScreenVars screenVars = (S6663ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.payrnum.clear();
		screenVars.payrname.clear();
		screenVars.payraddr1.clear();
		screenVars.payraddr2.clear();
		screenVars.payraddr3.clear();
		screenVars.payraddr4.clear();
		screenVars.payraddr5.clear();
		screenVars.incomeSeqNo.clear();
		screenVars.cltpcode.clear();
	}
}
