package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:59
 * Description:
 * Copybook name: BSPRWFDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bsprwfdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bsprwfdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData bsprwfdKey = new FixedLengthStringData(64).isAPartOf(bsprwfdFileKey, 0, REDEFINE);
  	public FixedLengthStringData bsprwfdScheduleName = new FixedLengthStringData(10).isAPartOf(bsprwfdKey, 0);
  	public PackedDecimalData bsprwfdScheduleNumber = new PackedDecimalData(8, 0).isAPartOf(bsprwfdKey, 10);
  	public FixedLengthStringData bsprwfdCompany = new FixedLengthStringData(1).isAPartOf(bsprwfdKey, 15);
  	public FixedLengthStringData bsprwfdProcessName = new FixedLengthStringData(10).isAPartOf(bsprwfdKey, 16);
  	public PackedDecimalData bsprwfdProcessOccNum = new PackedDecimalData(3, 0).isAPartOf(bsprwfdKey, 26);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(bsprwfdKey, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bsprwfdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bsprwfdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}