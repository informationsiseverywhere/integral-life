/*
 * File: Nflnapl.java
 * Date: 29 August 2009 23:00:12
 * Author: Quipoz Limited
 * 
 * Class transformed from NFLNAPL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.BfrqTableDAM;
import com.csc.life.contractservicing.recordstructures.Nfloanrec;
import com.csc.life.contractservicing.reports.R6244Report;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5664rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;

/**
* <pre>
*
*REMARKS.
*
*        NON-FORFEITURES - LOANS SUBROUTINE.
*        ===================================
*
*  This program is cloned from NFLOAN subroutine to do APL -
*  Advance with change of premium.
*
* Overview.
* ---------
*
* This program is to be called from Overdue processing, for
*  Traditional contracts that are Overdue and in need of transfer
*  to Non-forfeiture ( APL - Advance with change of premium),
*  via T6654.
* This subroutine will basically decide whether the contract
*  being processed is to be allowed a Non-forfeiture Loan (APL).
* It will calculate the Current Surrender value of the Contract,
*  total all outstanding Loans against the contract and then
*  check the difference between the two. If the Surrender value
*  is greater than the debt then processing for this contract
*  can continue, otherwise this routine will write a line to a
*  report saying that the Contract Surrender value can no longer
*  support the debts against it.
*
* Processing.
* -----------
*
* Given: NFLOANREC copybook, containing.....
*          Company, Contract Number, Open-printer-flag,
*          Language, Branch, Effective date,
*
*
* Gives: Statuz:   'O-K'   ..... Policy can have an APL
*                  'NAPL'  ..... Policy cannot have an APL
*                  'APLA'  ..... Policy can have an APL - Advance
*                                with change of premium.
*             anything else is an ERROR.
*
*
*    i) Calculate current Surrender value of Contract:
*       For each Coverage/Rider attached to the Contract, use
*        the Surrender method specified on T5687 to access the
*        table T6598 we get the Surrender value calculation
*        subroutine which can then be called to calculate the
*        S/Value of the Coverage/Rider being processed.
*       Total the Surrender values of all the Coverages/Riders
*        to get the Contract Surrender value.
*
*   ii) Call the 'TOTLOAN' subroutine with a Function of SPACES
*        to calculate the current total of any Loans + associated
*        interest owed which are associated with this contract
*        we are processing.
*
*  iii) Check the difference between the Surrender value of the
*        contract and the current Loan total:
*
*       If the Surrender value is > current Loan total,
*        Exit the subroutine with a status of OK.
*
*       If the Surrender value is <= current Loan total,
*        write a line to the R6244 Overdue Processing report,
*        detailing the contract number, Surrender value and
*        Current Loan total.
*        Exit the subroutine with a status of 'NAPL' i.e. no APL
*         allowed.
*        Before we exit add in check to reduce frequency,
*        If Policy current frequency = '12'
*           Don't reduce to next frequency
*        else
*           Reduce to next frequency and check if new frequency
*           is able to pay from Cash Surrender value.
*           If enough cash
*              set status to 'APLA'
*           End-if
*        End-if
*       End-if.
*
* TABLES USED
* -----------
*
*   T1692  -   Branch table
*   T1693  -   Company table
*   T5679  -   Transaction Status requirements
*   T5687  -   Coverage/Rider details table
*   T6598  -   Calculation methods table
*   T6633  -   Loan Interest Rules table
*   T5567  -   Contract Fee Parameters
*   T5541  -   Frequency Alteration Basis
*   TR517  -   Waiver of Premium Component
*   T5664  -   Premium Rates (Age Based)
*
*****************************************************************
* </pre>
*/
public class Nflnapl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R6244Report printerFile = new R6244Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaOldCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaOldRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaOldPlnsfx = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRiskStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaTotalCash = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSurrValTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldSurrVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalDebt = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaYesToLoan = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrintALine = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaPageCount = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private String wsaaOpenPrintFlag = "N";
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaPolsumCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCtr = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaRate = new ZonedDecimalData(8, 2).init(ZERO).setUnsigned();
	private String wsaaWopRiderFlag = "N";
	private String wsaaWaiverFlag = "N";
	private FixedLengthStringData wsaaOldFreqFound = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaNewFreqFound = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaCNewFreq = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCOldFreq = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaOldLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaNewLfact = new ZonedDecimalData(5, 4);
	private PackedDecimalData wsaaNewAmt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotNewAmt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaWopPrem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotWopPrem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaWopSi = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaNewFreqFee = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaTr517Key = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaT5664Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5664Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5664Key, 0);
	private FixedLengthStringData wsaaT5664Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 4);
	private FixedLengthStringData wsaaT5664Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 5);

	private FixedLengthStringData wsaaT5567Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5567Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5567Key, 0);
	private FixedLengthStringData wsaaT5567Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5567Key, 3);

	private FixedLengthStringData wsaaFreqTable = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaFreqChg = FLSArrayPartOfStructure(10, 2, wsaaFreqTable, 0);

		/* WSAA-R-TABLE */
	private FixedLengthStringData[] wsaaRiderTable = FLSInittedArray (10, 23);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaRiderTable, 0);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaRiderTable, 4);
	private ZonedDecimalData[] wsaaPremAmt = ZDArrayPartOfArrayStructure(17, 2, wsaaRiderTable, 6);

	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);
		/* FORMATS */
	private static final String covrsurrec = "COVRSURREC";
	private static final String descrec = "DESCREC   ";
	private static final String itdmrec = "ITEMREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String bfrqrec = "BFRQREC";
	private static final String lextrec = "LEXTREC";
		/* ERRORS */
	private static final String f294 = "F294";
	private static final String t075 = "T075";
	private static final String h966 = "H966";
	private static final String f358 = "F358";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
	private static final String t6633 = "T6633";
	private static final String t5567 = "T5567";
	private static final String t5541 = "T5541";
	private static final String tr517 = "TR517";
	private static final String t5664 = "T5664";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r6244H01 = new FixedLengthStringData(83);
	private FixedLengthStringData r6244h01O = new FixedLengthStringData(83).isAPartOf(r6244H01, 0);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(r6244h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r6244h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r6244h01O, 11);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r6244h01O, 41);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r6244h01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r6244h01O, 53);

	private FixedLengthStringData r6244D01 = new FixedLengthStringData(67);
	private FixedLengthStringData r6244d01O = new FixedLengthStringData(67).isAPartOf(r6244D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r6244d01O, 0);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(r6244d01O, 8);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(r6244d01O, 11);
	private FixedLengthStringData rd01Totnox = new FixedLengthStringData(2).isAPartOf(r6244d01O, 14);
	private ZonedDecimalData rd01Surrval = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 16);
	private ZonedDecimalData rd01Loansum = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 33);
	private ZonedDecimalData rd01Outstamt = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 50);
		/*APL - Advance Billing Frequency Details*/
	private BfrqTableDAM bfrqIO = new BfrqTableDAM();
		/*Coverage and Rider Details - Full Surren*/
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6633rec t6633rec = new T6633rec();
	private T5567rec t5567rec = new T5567rec();
	private T5541rec t5541rec = new T5541rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T5664rec t5664rec = new T5664rec();
	private Nfloanrec nfloanrec = new Nfloanrec();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ExternalisedRules er = new ExternalisedRules();
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		a910Check
	}

	public Nflnapl() {
		super();
	}

public void mainline(Object... parmArray)
	{
		nfloanrec.nfloanRec = convertAndSetParam(nfloanrec.nfloanRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
			start100();
			exit190();
		}

protected void start100()
	{
		initialise1000();
		if (isNE(wsaaYesToLoan,"Y")) {
			return ;
		}
		getSurrValues2000();
		sumLoans3000();
		surrvalLoansumCalc4000();
		if (isEQ(wsaaPrintALine,"Y")) {
			printReportLine10000();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		wsaaYesToLoan.set(SPACES);
		wsaaPrintALine.set(SPACES);
		wsaaSurrValTot.set(ZERO);
		wsaaHeldSurrVal.set(ZERO);
		wsaaOldCoverage.set(SPACES);
		wsaaOldRider.set(SPACES);
		wsaaHeldCurrLoans.set(ZERO);
		wsaaTotalDebt.set(ZERO);
		nfloanrec.statuz.set(varcom.oK);
		if (isEQ(wsaaOpenPrintFlag,"N")) {
			setUpPrintFile1100();
		}
		readT66331200();
		if (isEQ(t6633rec.t6633Rec,SPACES)) {
			wsaaYesToLoan.set("N");
			nfloanrec.statuz.set("NAPL");
		}
		else {
			wsaaYesToLoan.set("Y");
		}
		readT56791300();
	}

protected void setUpPrintFile1100()
	{
		start1100();
		setUpHeadingCompany1110();
		setUpHeadingBranch1120();
		setUpHeadingDates1130();
	}

protected void start1100()
	{
		printerFile.openOutput();
		wsaaOpenPrintFlag = "Y";
	}

protected void setUpHeadingCompany1110()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(nfloanrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(nfloanrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		rh01Company.set(nfloanrec.company);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1120()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(nfloanrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(nfloanrec.batcbrn);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(nfloanrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		rh01Branch.set(nfloanrec.batcbrn);
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1130()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(nfloanrec.effdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void readT66331200()
	{
		start1200();
	}

protected void start1200()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(nfloanrec.chdrcoy);
		itdmIO.setItemtabl(t6633);
		wsaaT6633Cnttype.set(nfloanrec.cnttype);
		wsaaT6633Type.set("A");
		itdmIO.setItemitem(wsaaT6633Key);
		itdmIO.setItmfrm(nfloanrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(),nfloanrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6633)
		|| isNE(itdmIO.getItemitem(),wsaaT6633Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t6633rec.t6633Rec.set(SPACES);
		}
		else {
			t6633rec.t6633Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT56791300()
	{
		start1300();
	}

protected void start1300()
	{
		/* Read T5679*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(nfloanrec.chdrcoy);
		itemIO.setItemtabl(t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(nfloanrec.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError99000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void getSurrValues2000()
	{
			start2000();
		}

protected void start2000()
	{
		covrsurIO.setParams(SPACES);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setChdrcoy(nfloanrec.chdrcoy);
		covrsurIO.setChdrnum(nfloanrec.chdrnum);
		covrsurIO.setPlanSuffix(ZERO);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)
		&& isNE(covrsurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrsurIO.getChdrcoy(),nfloanrec.chdrcoy)
		|| isNE(covrsurIO.getChdrnum(),nfloanrec.chdrnum)
		|| isEQ(covrsurIO.getStatuz(),varcom.endp)) {
			covrsurIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaLife.set(covrsurIO.getLife());
		while ( !(isEQ(covrsurIO.getStatuz(),varcom.endp))) {
			processComponents2100();
		}
		
		wsaaHeldSurrVal.set(wsaaSurrValTot);
	}

protected void processComponents2100()
	{
		start2100();
	}

protected void start2100()
	{
		wsaaOldCoverage.set(covrsurIO.getCoverage());
		wsaaOldRider.set(covrsurIO.getRider());
		wsaaOldPlnsfx.set(covrsurIO.getPlanSuffix());
		surrSubroutine2200();
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		srcalcpy.polsum.set(nfloanrec.polsum);
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.ptdate.set(nfloanrec.ptdate);
		srcalcpy.effdate.set(nfloanrec.effdate);
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.language.set(nfloanrec.language);
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		srcalcpy.chdrCurr.set(nfloanrec.cntcurr);
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		if (isGT(covrsurIO.getInstprem(),ZERO)) {
			srcalcpy.singp.set(covrsurIO.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrsurIO.getSingp());
		}
		srcalcpy.billfreq.set(nfloanrec.billfreq);
		srcalcpy.status.set(SPACES);
		if (isEQ(covrsurIO.getPlanSuffix(),ZERO)
		&& isNE(nfloanrec.polsum,nfloanrec.polinc)
		&& isNE(nfloanrec.polinc,1)) {
			for (wsaaPolsumCount.set(1); !(isGT(wsaaPolsumCount,nfloanrec.polsum)); wsaaPolsumCount.add(1)){
				sumpolSvalCalc2150();
			}
		}
		else {
			while ( !(isEQ(srcalcpy.status,varcom.endp))) {
				calculateSurrValue2300();
			}
			
		}
		while ( !(isNE(wsaaOldCoverage,covrsurIO.getCoverage())
		|| isNE(wsaaOldRider,covrsurIO.getRider())
		|| isNE(wsaaOldPlnsfx,covrsurIO.getPlanSuffix())
		|| isEQ(covrsurIO.getStatuz(),varcom.endp))) {
			getNextComponent2400();
		}
		
	}

protected void sumpolSvalCalc2150()
	{
		/*START*/
		srcalcpy.planSuffix.set(wsaaPolsumCount);
		srcalcpy.status.set(varcom.oK);
		while ( !(isEQ(srcalcpy.status,varcom.endp))) {
			calculateSurrValue2300();
		}
		
		/*EXIT*/
	}

protected void surrSubroutine2200()
	{
		start2200();
	}

protected void start2200()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(covrsurIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(),covrsurIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covrsurIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covrsurIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			systemError99000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covrsurIO.getChdrcoy());
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError99000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
	}

protected void calculateSurrValue2300()
	{
			start2300();
		}

protected void start2300()
	{
		if (isEQ(t6598rec.calcprog,SPACES)) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		checkCovrsurStatii2350();
		if (isEQ(wsaaPremStatus,"N")
		|| isEQ(wsaaRiskStatus,"N")) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())) || isEQ(t6598rec.calcprog.toString(),"ZPRESER")) 
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);	
			chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);


			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status,varcom.oK)
		&& isNE(srcalcpy.status,varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			systemError99000();
		}
		/* Add values returned to our running Surrender value total*/
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		a000CallRounding();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		wsaaSurrValTot.add(srcalcpy.actualVal);
	}

protected void checkCovrsurStatii2350()
	{
		/*START*/
		wsaaPremStatus.set("N");
		wsaaRiskStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaPremStatus,"Y")); wsaaIndex.add(1)){
			premiumStatus2370();
		}
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaRiskStatus,"Y")); wsaaIndex.add(1)){
			riskStatus2390();
		}
		/*EXIT*/
	}

protected void premiumStatus2370()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(),ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrsurIO.getPstatcode()))) {
					wsaaPremStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(),ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],covrsurIO.getPstatcode()))) {
					wsaaPremStatus.set("Y");
					return ;
				}
			}
		}
	}

protected void riskStatus2390()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(),ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrsurIO.getStatcode()))) {
					wsaaRiskStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(),ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrsurIO.getStatcode()))) {
					wsaaRiskStatus.set("Y");
					return ;
				}
			}
		}
	}

protected void getNextComponent2400()
	{
		/*START*/
		covrsurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)
		&& isNE(covrsurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrsurIO.getChdrcoy(),nfloanrec.chdrcoy)
		|| isNE(covrsurIO.getChdrnum(),nfloanrec.chdrnum)
		|| isNE(covrsurIO.getLife(),wsaaLife)
		|| isEQ(covrsurIO.getStatuz(),varcom.endp)) {
			covrsurIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void sumLoans3000()
	{
		start3000();
	}

protected void start3000()
	{
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(nfloanrec.chdrcoy);
		totloanrec.chdrnum.set(nfloanrec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(nfloanrec.effdate);
		totloanrec.language.set(nfloanrec.language);
		totloanrec.function.set("LOAN");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError99000();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal,totloanrec.interest));
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(nfloanrec.chdrcoy);
		totloanrec.chdrnum.set(nfloanrec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(nfloanrec.effdate);
		totloanrec.function.set("CASH");
		totloanrec.language.set(nfloanrec.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError99000();
		}
		compute(wsaaTotalCash, 2).set(add(totloanrec.principal,totloanrec.interest));
	}

protected void surrvalLoansumCalc4000()
	{
		/*START*/
		compute(wsaaTotalDebt, 2).set(add(wsaaHeldCurrLoans,nfloanrec.outstamt));
		if ((setPrecision(wsaaTotalDebt, 2)
		&& isGT((add(wsaaHeldSurrVal,wsaaTotalCash)),wsaaTotalDebt))) {
			wsaaPrintALine.set("N");
		}
		else {
			wsaaPrintALine.set("Y");
			nfloanrec.statuz.set("NAPL");
			if (isEQ(nfloanrec.billfreq,"12")) {
				/*CONTINUE_STMT*/
			}
			else {
				a100AplChangeFreq();
			}
		}
		/*EXIT*/
	}

protected void a100AplChangeFreq()
	{
		a110Start();
	}

protected void a110Start()
	{
		wsaaFreqTable.set(SPACES);
		if (isEQ(nfloanrec.billfreq,"01")){
			wsaaFreqChg[1].set("02");
			wsaaFreqChg[2].set("04");
			wsaaFreqChg[3].set("12");
		}
		else if (isEQ(nfloanrec.billfreq,"02")){
			wsaaFreqChg[1].set("04");
			wsaaFreqChg[2].set("12");
		}
		else if (isEQ(nfloanrec.billfreq,"04")){
			wsaaFreqChg[1].set("12");
		}
		else{
			wsaaFreqTable.set(SPACES);
		}
		for (wsaaCtr.set(1); !(isGT(wsaaCtr,10)); wsaaCtr.add(1)){
			if (isNE(wsaaFreqChg[wsaaCtr.toInt()],SPACES)) {
				wsaaWopRiderFlag = "N";
				wsaaCNewFreq.set(wsaaFreqChg[wsaaCtr.toInt()]);
				wsaaCOldFreq.set(nfloanrec.billfreq);
				a800ReadT5567();
				a200AplChangePremNwop();
				if (isEQ(wsaaWopRiderFlag,"Y")) {
					a500AplChangePremWop();
					wsaaTotNewAmt.add(wsaaTotWopPrem);
				}
				compute(wsaaTotNewAmt, 2).set(add(wsaaTotNewAmt,wsaaNewFreqFee));
				compute(wsaaTotalDebt, 2).set(add(wsaaHeldCurrLoans,wsaaTotNewAmt));
				if ((setPrecision(wsaaTotalDebt, 2)
				&& isGT((add(wsaaHeldSurrVal,wsaaTotalCash)),wsaaTotalDebt))) {
					wsaaFreqTable.set(SPACES);
					nfloanrec.statuz.set("APLA");
					a300WriteBfrq();
				}
			}
		}
	}

protected void a200AplChangePremNwop()
	{
		a210Start();
	}

protected void a210Start()
	{
		initialize(wsaaNewAmt);
		initialize(wsaaTotNewAmt);
		for (wsaaI.set(1); !(isGT(wsaaI,10)); wsaaI.add(1)){
			wsaaCrtable[wsaaI.toInt()].set(SPACES);
			wsaaRider[wsaaI.toInt()].set(SPACES);
			wsaaPremAmt[wsaaI.toInt()].set(ZERO);
		}
		wsaaI.set(1);
		covrsurIO.setParams(SPACES);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setChdrcoy(nfloanrec.chdrcoy);
		covrsurIO.setChdrnum(nfloanrec.chdrnum);
		covrsurIO.setPlanSuffix(ZERO);
		while ( !(isEQ(covrsurIO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, covrsurIO);
			if (isNE(covrsurIO.getStatuz(),varcom.oK)
			&& isNE(covrsurIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrsurIO.getParams());
				syserrrec.statuz.set(covrsurIO.getStatuz());
				systemError99000();
			}
			if (isNE(covrsurIO.getChdrcoy(),nfloanrec.chdrcoy)
			|| isNE(covrsurIO.getChdrnum(),nfloanrec.chdrnum)
			|| isEQ(covrsurIO.getStatuz(),varcom.endp)) {
				covrsurIO.setStatuz(varcom.endp);
			}
			else {
				readT56791300();
				checkCovrsurStatii2350();
				if (isEQ(wsaaPremStatus,"N")
				|| isEQ(wsaaRiskStatus,"N")) {
					covrsurIO.setFunction(varcom.nextr);
				}
				else {
					wsaaTr517Key.set(covrsurIO.getCrtable());
					a400ReadTr517();
					if (isNE(tr517rec.tr517Rec,SPACES)) {
						wsaaWopRiderFlag = "Y";
						covrsurIO.setFunction(varcom.nextr);
					}
					else {
						a700ReadT5541();
						if (isEQ(wsaaOldFreqFound,"Y")
						&& isEQ(wsaaNewFreqFound,"Y")) {
							compute(wsaaNewAmt, 5).setRounded(mult((div((mult(covrsurIO.getInstprem(),wsaaCOldFreq)),wsaaCNewFreq)),(div(wsaaNewLfact,wsaaOldLfact))));
						}
						else {
							wsaaNewAmt.set(covrsurIO.getInstprem());
						}
						zrdecplrec.amountIn.set(wsaaNewAmt);
						a000CallRounding();
						wsaaNewAmt.set(zrdecplrec.amountOut);
						wsaaTotNewAmt.add(wsaaNewAmt);
						wsaaCrtable[wsaaI.toInt()].set(covrsurIO.getCrtable());
						wsaaRider[wsaaI.toInt()].set(covrsurIO.getRider());
						wsaaPremAmt[wsaaI.toInt()].set(wsaaNewAmt);
						wsaaI.add(1);
						covrsurIO.setFunction(varcom.nextr);
					}
				}
			}
		}
		
	}

protected void a300WriteBfrq()
	{
		a310Readh();
		a320Updat();
	}

protected void a310Readh()
	{
		bfrqIO.setParams(SPACES);
		bfrqIO.setChdrcoy(nfloanrec.chdrcoy);
		bfrqIO.setChdrnum(nfloanrec.chdrnum);
		bfrqIO.setEffdate(nfloanrec.effdate);
		bfrqIO.setFormat(bfrqrec);
		bfrqIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, bfrqIO);
		if (isNE(bfrqIO.getStatuz(),varcom.oK)
		&& isNE(bfrqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(bfrqIO.getParams());
			syserrrec.statuz.set(bfrqIO.getStatuz());
			systemError99000();
		}
	}

protected void a320Updat()
	{
		bfrqIO.setChdrcoy(nfloanrec.chdrcoy);
		bfrqIO.setChdrnum(nfloanrec.chdrnum);
		bfrqIO.setCnttype(nfloanrec.cnttype);
		bfrqIO.setCurrency(nfloanrec.cntcurr);
		bfrqIO.setFreq(nfloanrec.billfreq);
		bfrqIO.setBillfreq(wsaaCNewFreq);
		bfrqIO.setInstpramt(nfloanrec.outstamt);
		bfrqIO.setInstprem(wsaaTotNewAmt);
		bfrqIO.setEffdate(nfloanrec.effdate);
		bfrqIO.setValidflag("1");
		bfrqIO.setFormat(bfrqrec);
		bfrqIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, bfrqIO);
		if (isNE(bfrqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bfrqIO.getParams());
			syserrrec.statuz.set(bfrqIO.getStatuz());
			systemError99000();
		}
	}

protected void a400ReadTr517()
	{
		a410Read();
	}

protected void a410Read()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrsurIO.getChdrcoy());
		itdmIO.setItemtabl(tr517);
		itdmIO.setItemitem(wsaaTr517Key);
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(),covrsurIO.getChdrcoy())
		|| isNE(itdmIO.getItemitem(),wsaaTr517Key)
		|| isNE(itdmIO.getItemtabl(),tr517)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			tr517rec.tr517Rec.set(SPACES);
		}
		else {
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
		}
	}

protected void a500AplChangePremWop()
	{
		a510Start();
	}

protected void a510Start()
	{
		initialize(wsaaTotWopPrem);
		covrsurIO.setParams(SPACES);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setChdrcoy(nfloanrec.chdrcoy);
		covrsurIO.setChdrnum(nfloanrec.chdrnum);
		covrsurIO.setPlanSuffix(ZERO);
		while ( !(isEQ(covrsurIO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, covrsurIO);
			if (isNE(covrsurIO.getStatuz(),varcom.oK)
			&& isNE(covrsurIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrsurIO.getParams());
				syserrrec.statuz.set(covrsurIO.getStatuz());
				systemError99000();
			}
			if (isNE(covrsurIO.getChdrcoy(),nfloanrec.chdrcoy)
			|| isNE(covrsurIO.getChdrnum(),nfloanrec.chdrnum)
			|| isEQ(covrsurIO.getStatuz(),varcom.endp)) {
				covrsurIO.setStatuz(varcom.endp);
			}
			else {
				initialize(wsaaWopSi);
				initialize(wsaaWopPrem);
				readT56791300();
				checkCovrsurStatii2350();
				if (isEQ(wsaaPremStatus,"N")
				|| isEQ(wsaaRiskStatus,"N")) {
					covrsurIO.setFunction(varcom.nextr);
				}
				else {
					wsaaTr517Key.set(covrsurIO.getCrtable());
					a400ReadTr517();
					if (isEQ(tr517rec.tr517Rec,SPACES)) {
						covrsurIO.setFunction(varcom.nextr);
					}
					else {
						for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
							if (isEQ(wsaaCrtable[wsaaSub.toInt()],SPACES)) {
								wsaaSub.set(11);
							}
							else {
								a900CheckCodeWaiver();
							}
						}
						if (isGT(wsaaWopSi,0)) {
							if (isEQ(tr517rec.zrwvflg03,"Y")) {
								wsaaWopSi.add(wsaaNewFreqFee);
							}
							a600CalcWopPrem();
							wsaaTotWopPrem.add(wsaaWopPrem);
						}
						covrsurIO.setFunction(varcom.nextr);
					}
				}
			}
		}
		
	}

protected void a600CalcWopPrem()
	{
		a610Lext();
		a620Calc();
	}

protected void a610Lext()
	{
		wsaaAgerateTot.set(ZERO);
		wsaaRatesPerMillieTot.set(ZERO);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,8)); wsaaIndex.add(1)){
			wsaaLextOppc[wsaaIndex.toInt()].set(ZERO);
		}
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(covrsurIO.getChdrcoy());
		lextIO.setChdrnum(covrsurIO.getChdrnum());
		lextIO.setLife(covrsurIO.getLife());
		lextIO.setCoverage(covrsurIO.getCoverage());
		lextIO.setRider(covrsurIO.getRider());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		wsaaIndex.set(0);
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, lextIO);
			if (isNE(lextIO.getStatuz(),varcom.oK)
			&& isNE(lextIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(lextIO.getParams());
				systemError99000();
			}
			if (isNE(lextIO.getChdrcoy(),covrsurIO.getChdrcoy())
			|| isNE(lextIO.getChdrnum(),covrsurIO.getChdrnum())
			|| isNE(lextIO.getLife(),covrsurIO.getLife())
			|| isNE(lextIO.getCoverage(),covrsurIO.getCoverage())
			|| isNE(lextIO.getRider(),covrsurIO.getRider())
			|| isNE(lextIO.getStatuz(),varcom.endp)) {
				lextIO.setStatuz(varcom.endp);
			}
			else {
				if (isEQ(lextIO.getReasind(),"1")
				&& isLTE(nfloanrec.effdate,lextIO.getExtCessDate())) {
					wsaaIndex.add(1);
					wsaaLextOppc[wsaaIndex.toInt()].set(lextIO.getOppc());
					wsaaRatesPerMillieTot.add(lextIO.getInsprm());
					wsaaAgerateTot.add(lextIO.getAgerate());
				}
				lextIO.setFunction(varcom.nextr);
			}
		}
		
	}

protected void a620Calc()
	{
		itdmIO.setItemcoy(covrsurIO.getChdrcoy());
		itdmIO.setItemtabl(t5664);
		wsaaT5664Crtable.set(covrsurIO.getCrtable());
		wsaaT5664Mortcls.set(covrsurIO.getMortcls());
		wsaaT5664Sex.set(covrsurIO.getSex());
		itdmIO.setItemitem(wsaaT5664Key);
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			systemError99000();
		}
		if (isNE(wsaaT5664Key,itdmIO.getItemitem())
		|| isNE(covrsurIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5664)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT5664Key);
			syserrrec.statuz.set(f358);
			systemError99000();
		}
		else {
			t5664rec.t5664Rec.set(itdmIO.getGenarea());
		}
		compute(wsaaAdjustedAge, 0).set(add(wsaaAgerateTot,covrsurIO.getAnbAtCcd()));
		/*    COMPUTE WSAA-RATE      = T5664-INSPRM(WSAA-ADJUSTED-AGE)     */
		/*                           / T5664-PREM-UNIT.                    */
		if (isEQ(wsaaAdjustedAge,0)){
			compute(wsaaRate, 2).set(div(t5664rec.insprem,t5664rec.premUnit));
			/**      WHEN 100                                           <V73L03>*/
			/**       COMPUTE WSAA-RATE = T5664-INSTPR / T5664-PREM-UNIT<V73L03>*/
			/**  Extend the age band to 110.                                    */
		}
		else if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)){
			compute(wsaaRate, 2).set(div(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], t5664rec.premUnit));
		}
		else{
			compute(wsaaRate, 2).set(div(t5664rec.insprm[wsaaAdjustedAge.toInt()],t5664rec.premUnit));
		}
		compute(wsaaRate, 2).set(add(wsaaRatesPerMillieTot,wsaaRate));
		compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopSi,wsaaRate)),t5664rec.unit)));
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,8)); wsaaIndex.add(1)){
			if (isNE(wsaaLextOppc[wsaaIndex.toInt()],0)) {
				compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopPrem,wsaaLextOppc[wsaaIndex.toInt()])),100)));
			}
		}
		zrdecplrec.amountIn.set(wsaaWopPrem);
		a000CallRounding();
		wsaaWopPrem.set(zrdecplrec.amountOut);
	}

protected void a700ReadT5541()
	{
		a710ReadT5687();
		a720ReadT5541();
	}

protected void a710ReadT5687()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(covrsurIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(),covrsurIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covrsurIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covrsurIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			systemError99000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void a720ReadT5541()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrsurIO.getChdrcoy());
		itdmIO.setItemtabl(t5541);
		itdmIO.setItemitem(t5687rec.xfreqAltBasis);
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(),covrsurIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5541)
		|| isNE(itdmIO.getItemitem(),t5687rec.xfreqAltBasis)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(t5687rec.xfreqAltBasis);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(t075);
			systemError99000();
		}
		else {
			t5541rec.t5541Rec.set(itdmIO.getGenarea());
		}
		wsaaNewFreqFound.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaNewFreqFound,"Y")); wsaaIndex.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaIndex.toInt()],wsaaCNewFreq)) {
				wsaaNewLfact.set(t5541rec.lfact[wsaaIndex.toInt()]);
				wsaaNewFreqFound.set("Y");
			}
		}
		wsaaOldFreqFound.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaOldFreqFound,"Y")); wsaaIndex.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaIndex.toInt()],wsaaCOldFreq)) {
				wsaaOldLfact.set(t5541rec.lfact[wsaaIndex.toInt()]);
				wsaaOldFreqFound.set("Y");
			}
		}
	}

protected void a800ReadT5567()
	{
		a810Read();
	}

protected void a810Read()
	{
		itdmIO.setItemcoy(covrsurIO.getChdrcoy());
		itdmIO.setItemtabl(t5567);
		wsaaT5567Cnttype.set(nfloanrec.cnttype);
		wsaaT5567Cntcurr.set(nfloanrec.cntcurr);
		itdmIO.setItemitem(wsaaT5567Key);
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(),covrsurIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5567)
		|| isNE(itdmIO.getItemitem(),wsaaT5567Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT5567Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h966);
			systemError99000();
		}
		else {
			t5567rec.t5567Rec.set(itdmIO.getGenarea());
		}
		wsaaNewFreqFee.set(ZERO);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,10)); wsaaIndex.add(1)){
			if (isEQ(t5567rec.billfreq[wsaaIndex.toInt()],wsaaCNewFreq)) {
				wsaaNewFreqFee.set(t5567rec.cntfee[wsaaIndex.toInt()]);
				wsaaIndex.set(11);
			}
		}
	}

protected void a900CheckCodeWaiver()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case a910Check: 
					a910Check();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a910Check()
	{
		wsaaWaiverFlag = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,50)
		|| isEQ(wsaaWaiverFlag,"Y")); wsaaIndex.add(1)){
			if (isEQ(wsaaCrtable[wsaaSub.toInt()],tr517rec.ctable[wsaaIndex.toInt()])) {
				if (isEQ(tr517rec.zrwvflg04,"Y")) {
					if (isEQ(wsaaRider[wsaaSub.toInt()],"00")) {
						wsaaWopSi.add(wsaaPremAmt[wsaaSub.toInt()]);
					}
					else {
						wsaaWopSi.subtract(wsaaPremAmt[wsaaSub.toInt()]);
					}
				}
				else {
					wsaaWopSi.add(wsaaPremAmt[wsaaSub.toInt()]);
				}
				wsaaWaiverFlag = "Y";
			}
		}
		if (isEQ(wsaaWaiverFlag,"N")
		&& isNE(tr517rec.contitem,SPACES)) {
			wsaaTr517Key.set(tr517rec.contitem);
			a400ReadTr517();
			goTo(GotoLabel.a910Check);
		}
	}

protected void printReportLine10000()
	{
		writeDetail10050();
	}

protected void writeDetail10050()
	{
		if (newPageReq.isTrue()) {
			printerFile.printR6244h01(r6244H01, indicArea);
			wsaaOverflow.set("N");
		}
		rd01Chdrnum.set(nfloanrec.chdrnum);
		rd01Cnttype.set(nfloanrec.cnttype);
		rd01Cntcurr.set(nfloanrec.cntcurr);
		rd01Surrval.set(wsaaHeldSurrVal);
		rd01Loansum.set(wsaaHeldCurrLoans);
		rd01Totnox.set(totloanrec.loanCount);
		rd01Outstamt.set(nfloanrec.outstamt);
		printerFile.printR6244d01(r6244D01, indicArea);
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(nfloanrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(nfloanrec.cntcurr);
		zrdecplrec.batctrcde.set(nfloanrec.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError99000();
		}
		/*A900-EXIT*/
	}

protected void systemError99000()
	{
			start99000();
			exit99490();
		}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		nfloanrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
			start99500();
			exit99590();
		}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		nfloanrec.statuz.set(varcom.bomb);
		exit190();
	}
}
