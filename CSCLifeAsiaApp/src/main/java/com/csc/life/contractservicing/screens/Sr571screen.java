package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr571screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr571ScreenVars sv = (Sr571ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr571screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr571ScreenVars screenVars = (Sr571ScreenVars)pv;
		/*screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.zbinstprem.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubsect.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.riskCessAge.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		screenVars.premCessDateDisp.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.optextind.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.comind.setClassString("");
		screenVars.select.setClassString("");
		screenVars.pbind.setClassString("");
		screenVars.bappmeth.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.linstamt.setClassString("");
		screenVars.taxamt01.setClassString("");
		screenVars.taxind.setClassString("");
		screenVars.taxamt02.setClassString("");
		BRD-306 START 
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.loadper.setClassString("");
		screenVars.rateadj.setClassString("");
		screenVars.fltmort.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.adjustageamt.setClassString("");
		BRD-306 END 
		screenVars.prmbasis.setClassString("");//ILIFE-3421
		screenVars.dialdownoption.setClassString("");//BRD-NBP-011
		screenVars.exclind.setClassString("");
		screenVars.fuind.setClassString("");
		screenVars.cashvalarer.setClassString(""); //ICIL-299
*/	
		ScreenRecord.setClassStringFormatting(pv);
	}

/**
 * Clear all the variables in Sr571screen
 */
	public static void clear(VarModel pv) {
		Sr571ScreenVars screenVars = (Sr571ScreenVars) pv;
		/*screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.zbinstprem.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.zagelit.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubsect.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.numpols.clear();
		screenVars.planSuffix.clear();
		screenVars.sumin.clear();
		screenVars.currcd.clear();
		screenVars.riskCessAge.clear();
		screenVars.riskCessTerm.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		screenVars.premCessDateDisp.clear();
		screenVars.premCessDate.clear();
		screenVars.mortcls.clear();
		screenVars.liencd.clear();
		screenVars.optextind.clear();
		screenVars.instPrem.clear();
		screenVars.comind.clear();
		screenVars.select.clear();
		screenVars.pbind.clear();
		screenVars.bappmeth.clear();
		screenVars.zlinstprem.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.linstamt.clear();
		screenVars.taxamt01.clear();
		screenVars.taxind.clear();
		screenVars.taxamt02.clear();
		BRD-306 START 
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.loadper.clear();
		screenVars.rateadj.clear();
		screenVars.fltmort.clear();
		screenVars.premadj.clear();
		screenVars.adjustageamt.clear();
		BRD-306 END 
		screenVars.prmbasis.clear();
		screenVars.dialdownoption.clear(); //BRD-NBP-011
		screenVars.exclind.clear();
		screenVars.fuind.clear();
		screenVars.cashvalarer.clear(); //ICIL-299
*/		
		ScreenRecord.clear(pv);
	}
}
