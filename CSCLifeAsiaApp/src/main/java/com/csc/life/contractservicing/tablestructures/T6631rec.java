package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:34
 * Description:
 * Copybook name: T6631REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6631rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6631Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData autofreq = new ZonedDecimalData(2, 0).isAPartOf(t6631Rec, 0);
  	public FixedLengthStringData crtables = new FixedLengthStringData(96).isAPartOf(t6631Rec, 2);
  	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(24, 4, crtables, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(96).isAPartOf(crtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData crtable11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData crtable12 = new FixedLengthStringData(4).isAPartOf(filler, 44);
  	public FixedLengthStringData crtable13 = new FixedLengthStringData(4).isAPartOf(filler, 48);
  	public FixedLengthStringData crtable14 = new FixedLengthStringData(4).isAPartOf(filler, 52);
  	public FixedLengthStringData crtable15 = new FixedLengthStringData(4).isAPartOf(filler, 56);
  	public FixedLengthStringData crtable16 = new FixedLengthStringData(4).isAPartOf(filler, 60);
  	public FixedLengthStringData crtable17 = new FixedLengthStringData(4).isAPartOf(filler, 64);
  	public FixedLengthStringData crtable18 = new FixedLengthStringData(4).isAPartOf(filler, 68);
  	public FixedLengthStringData crtable19 = new FixedLengthStringData(4).isAPartOf(filler, 72);
  	public FixedLengthStringData crtable20 = new FixedLengthStringData(4).isAPartOf(filler, 76);
  	public FixedLengthStringData crtable21 = new FixedLengthStringData(4).isAPartOf(filler, 80);
  	public FixedLengthStringData crtable22 = new FixedLengthStringData(4).isAPartOf(filler, 84);
  	public FixedLengthStringData crtable23 = new FixedLengthStringData(4).isAPartOf(filler, 88);
  	public FixedLengthStringData crtable24 = new FixedLengthStringData(4).isAPartOf(filler, 92);
  	public ZonedDecimalData durmnth = new ZonedDecimalData(3, 0).isAPartOf(t6631Rec, 98);
  	public FixedLengthStringData payfroms = new FixedLengthStringData(24).isAPartOf(t6631Rec, 101);
  	public FixedLengthStringData[] payfrom = FLSArrayPartOfStructure(24, 1, payfroms, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(payfroms, 0, FILLER_REDEFINE);
  	public FixedLengthStringData payfrom01 = new FixedLengthStringData(1).isAPartOf(filler1, 0);
  	public FixedLengthStringData payfrom02 = new FixedLengthStringData(1).isAPartOf(filler1, 1);
  	public FixedLengthStringData payfrom03 = new FixedLengthStringData(1).isAPartOf(filler1, 2);
  	public FixedLengthStringData payfrom04 = new FixedLengthStringData(1).isAPartOf(filler1, 3);
  	public FixedLengthStringData payfrom05 = new FixedLengthStringData(1).isAPartOf(filler1, 4);
  	public FixedLengthStringData payfrom06 = new FixedLengthStringData(1).isAPartOf(filler1, 5);
  	public FixedLengthStringData payfrom07 = new FixedLengthStringData(1).isAPartOf(filler1, 6);
  	public FixedLengthStringData payfrom08 = new FixedLengthStringData(1).isAPartOf(filler1, 7);
  	public FixedLengthStringData payfrom09 = new FixedLengthStringData(1).isAPartOf(filler1, 8);
  	public FixedLengthStringData payfrom10 = new FixedLengthStringData(1).isAPartOf(filler1, 9);
  	public FixedLengthStringData payfrom11 = new FixedLengthStringData(1).isAPartOf(filler1, 10);
  	public FixedLengthStringData payfrom12 = new FixedLengthStringData(1).isAPartOf(filler1, 11);
  	public FixedLengthStringData payfrom13 = new FixedLengthStringData(1).isAPartOf(filler1, 12);
  	public FixedLengthStringData payfrom14 = new FixedLengthStringData(1).isAPartOf(filler1, 13);
  	public FixedLengthStringData payfrom15 = new FixedLengthStringData(1).isAPartOf(filler1, 14);
  	public FixedLengthStringData payfrom16 = new FixedLengthStringData(1).isAPartOf(filler1, 15);
  	public FixedLengthStringData payfrom17 = new FixedLengthStringData(1).isAPartOf(filler1, 16);
  	public FixedLengthStringData payfrom18 = new FixedLengthStringData(1).isAPartOf(filler1, 17);
  	public FixedLengthStringData payfrom19 = new FixedLengthStringData(1).isAPartOf(filler1, 18);
  	public FixedLengthStringData payfrom20 = new FixedLengthStringData(1).isAPartOf(filler1, 19);
  	public FixedLengthStringData payfrom21 = new FixedLengthStringData(1).isAPartOf(filler1, 20);
  	public FixedLengthStringData payfrom22 = new FixedLengthStringData(1).isAPartOf(filler1, 21);
  	public FixedLengthStringData payfrom23 = new FixedLengthStringData(1).isAPartOf(filler1, 22);
  	public FixedLengthStringData payfrom24 = new FixedLengthStringData(1).isAPartOf(filler1, 23);
  	public FixedLengthStringData payto = new FixedLengthStringData(1).isAPartOf(t6631Rec, 125);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(374).isAPartOf(t6631Rec, 126, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6631Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6631Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}