package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5130screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 23, 18, 5, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 15;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5130ScreenVars sv = (S5130ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5130screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5130screensfl, 
			sv.S5130screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5130ScreenVars sv = (S5130ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5130screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5130ScreenVars sv = (S5130ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5130screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5130screensflWritten.gt(0))
		{
			sv.s5130screensfl.setCurrentIndex(0);
			sv.S5130screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5130ScreenVars sv = (S5130ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5130screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5130ScreenVars screenVars = (S5130ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.action.setFieldName("action");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.elemkey.setFieldName("elemkey");
				screenVars.elemdesc.setFieldName("elemdesc");
				screenVars.hlife.setFieldName("hlife");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hcovt.setFieldName("hcovt");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.action.set(dm.getField("action"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.elemkey.set(dm.getField("elemkey"));
			screenVars.elemdesc.set(dm.getField("elemdesc"));
			screenVars.hlife.set(dm.getField("hlife"));
			screenVars.hcoverage.set(dm.getField("hcoverage"));
			screenVars.hrider.set(dm.getField("hrider"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.hcovt.set(dm.getField("hcovt"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5130ScreenVars screenVars = (S5130ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.action.setFieldName("action");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.elemkey.setFieldName("elemkey");
				screenVars.elemdesc.setFieldName("elemdesc");
				screenVars.hlife.setFieldName("hlife");
				screenVars.hcoverage.setFieldName("hcoverage");
				screenVars.hrider.setFieldName("hrider");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hcovt.setFieldName("hcovt");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("action").set(screenVars.action);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("elemkey").set(screenVars.elemkey);
			dm.getField("elemdesc").set(screenVars.elemdesc);
			dm.getField("hlife").set(screenVars.hlife);
			dm.getField("hcoverage").set(screenVars.hcoverage);
			dm.getField("hrider").set(screenVars.hrider);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("hcovt").set(screenVars.hcovt);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5130screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5130ScreenVars screenVars = (S5130ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.action.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.elemkey.clearFormatting();
		screenVars.elemdesc.clearFormatting();
		screenVars.hlife.clearFormatting();
		screenVars.hcoverage.clearFormatting();
		screenVars.hrider.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.hcovt.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5130ScreenVars screenVars = (S5130ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.action.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.elemkey.setClassString("");
		screenVars.elemdesc.setClassString("");
		screenVars.hlife.setClassString("");
		screenVars.hcoverage.setClassString("");
		screenVars.hrider.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.hcovt.setClassString("");
	}

/**
 * Clear all the variables in S5130screensfl
 */
	public static void clear(VarModel pv) {
		S5130ScreenVars screenVars = (S5130ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.action.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.elemkey.clear();
		screenVars.elemdesc.clear();
		screenVars.hlife.clear();
		screenVars.hcoverage.clear();
		screenVars.hrider.clear();
		screenVars.hcrtable.clear();
		screenVars.hcovt.clear();
	}
}
