package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:14
 * Description:
 * Copybook name: PTRNCWFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptrncwfkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptrncwfFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ptrncwfKey = new FixedLengthStringData(64).isAPartOf(ptrncwfFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptrncwfChdrcoy = new FixedLengthStringData(1).isAPartOf(ptrncwfKey, 0);
  	public FixedLengthStringData ptrncwfChdrnum = new FixedLengthStringData(8).isAPartOf(ptrncwfKey, 1);
  	public FixedLengthStringData ptrncwfBatctrcde = new FixedLengthStringData(4).isAPartOf(ptrncwfKey, 9);
  	public PackedDecimalData ptrncwfPtrneff = new PackedDecimalData(8, 0).isAPartOf(ptrncwfKey, 13);
  	public PackedDecimalData ptrncwfTranno = new PackedDecimalData(5, 0).isAPartOf(ptrncwfKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(ptrncwfKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptrncwfFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptrncwfFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}