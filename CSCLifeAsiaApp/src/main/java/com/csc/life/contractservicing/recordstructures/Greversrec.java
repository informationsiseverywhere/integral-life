package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:18
 * Description:
 * Copybook name: GREVERSREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Greversrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData reverseRec = new FixedLengthStringData(132);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(reverseRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(reverseRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(reverseRec, 5);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(reverseRec, 13);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(reverseRec, 16);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(reverseRec, 19);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(reverseRec, 21);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(reverseRec, 23);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(reverseRec, 25);
  	public PackedDecimalData newTranno = new PackedDecimalData(5, 0).isAPartOf(reverseRec, 29);
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(reverseRec, 32);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(reverseRec, 96).setUnsigned();
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(reverseRec, 104);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(reverseRec, 108).setUnsigned();
  	public PackedDecimalData transDate = new PackedDecimalData(6, 0).isAPartOf(reverseRec, 114);
  	public PackedDecimalData transTime = new PackedDecimalData(6, 0).isAPartOf(reverseRec, 118);
  	public PackedDecimalData contractAmount = new PackedDecimalData(17, 2).isAPartOf(reverseRec, 122);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(reverseRec, 131);


	public void initialize() {
		COBOLFunctions.initialize(reverseRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		reverseRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}