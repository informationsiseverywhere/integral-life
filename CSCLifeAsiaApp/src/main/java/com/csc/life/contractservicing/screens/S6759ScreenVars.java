package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6759
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6759ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(68);
	public FixedLengthStringData dataFields = new FixedLengthStringData(4).isAPartOf(dataArea, 0);
	public FixedLengthStringData hflags = new FixedLengthStringData(3).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] hflag = FLSArrayPartOfStructure(3, 1, hflags, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(hflags, 0, FILLER_REDEFINE);
	public FixedLengthStringData hflag01 = DD.hflag.copy().isAPartOf(filler,0);
	public FixedLengthStringData hflag02 = DD.hflag.copy().isAPartOf(filler,1);
	public FixedLengthStringData hflag03 = DD.hflag.copy().isAPartOf(filler,2);
	public FixedLengthStringData confirm = DD.confirm.copy().isAPartOf(dataFields,3);
	//public FixedLengthStringData hflag04 = DD.hflag.copy().isAPartOf(filler,3);

   public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 4);
	public FixedLengthStringData hflagsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] hflagErr = FLSArrayPartOfStructure(3, 4, hflagsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(hflagsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hflag01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData hflag02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData hflag03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	//public FixedLengthStringData hflag04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData confirmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 20);
	public FixedLengthStringData hflagsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(3, 12, hflagsOut, 0);
	public FixedLengthStringData[][] hflagO = FLSDArrayPartOfArrayStructure(12, 1, hflagOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(hflagsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hflag01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] hflag02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] hflag03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	//public FixedLengthStringData[] hflag04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] confirmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6759screenWritten = new LongData(0);
	public LongData S6759windowWritten = new LongData(0);
	public LongData S6759hideWritten = new LongData(0);
	public LongData S6759protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6759ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(hflag01Out,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(hflag02Out,new String[] {null, null, null, "11",null, null, null, null, null, null, null, null});
		fieldIndMap.put(hflag03Out,new String[] {null, null, null, "12",null, null, null, null, null, null, null, null});
		fieldIndMap.put(confirmOut,new String[] {null, null, null, "13",null, null, null, null, null, null, null, null});

		screenFields = new BaseData[] {hflag01, hflag02, hflag03, confirm};
		screenOutFields = new BaseData[][] {hflag01Out, hflag02Out, hflag03Out, confirmOut};
		screenErrFields = new BaseData[] {hflag01Err, hflag02Err, hflag03Err, confirmErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6759screen.class;
		protectRecord = S6759protect.class;
		hideRecord = S6759hide.class;
	}

}
