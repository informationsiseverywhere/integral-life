/*
 * File: Grevinci.java
 * Date: 29 August 2009 22:50:48
 * Author: Quipoz Limited
 * 
 * Class transformed from GREVINCI.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.unitlinkedprocessing.dataaccess.IncitrdTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
* Note
* Second version of program. Complete change of logic as a result
* of Aqr3962.
*
* Reversal subroutine for INCI records ( Bill Frequency Changes )
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This subroutine is called by reversal subroutines via
* table T5671. The parameter passed to this subroutine is
* contained in the GREVERSREC copybook.
*
* The following transactions are reveresed :
*
*  -  INCI
*
*         BEGN on the INCITRD file using the relevant rider
*             information, scan through the file in reverse
*             transaction number sequence to remove ALL valid
*             flag '1' records.
*         The logical will sort on ascending valid flag sequence
*             therefore after reading a v/f '2' record no further
*             deletions will be necessary.
*         Re-browse the logical without update (BEGN),
*  -  COVR
*         Access the COVR file to obtain the current premium
*             figure. This will form the basis of the browse
*             and re-instate operation.
*         Continue to scan through the file re-instating all
*             records. If the record is active then decrease the
*             held premium figure by the INCI record value.
*         Stop the process loop when the held premium value
*             has reached zero. If the value falls below zero
*             ,or ENDP is reached, or a change of contract key
*             detected then the program should force an ABEND.
*             The only permitted exit from the loop is when the
*             INCI records re-instated exactly match the current
*             premium value.
*
*****************************************************************
* </pre>
*/
public class Grevinci extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private PackedDecimalData holdInstprem = new PackedDecimalData(17, 2).init(0);
		/* FORMATS */
	private String incitrdrec = "INCITRDREC";
	private String covrmjarec = "COVRMJAREC";
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Greversrec greversrec = new Greversrec();
		/*Logical File Layout - Version 9305*/
	private IncitrdTableDAM incitrdIO = new IncitrdTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		bypassDeletion2020, 
		wipeoutVf1IncisExit2030, 
		exit3090, 
		bomb780
	}

	public Grevinci() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAIN*/
		greversrec.statuz.set(varcom.oK);
		readFirstInci1000();
		while ( !(isEQ(incitrdIO.getStatuz(),varcom.endp))) {
			wipeoutVf1Incis2000();
		}
		
		continueBrowse2500();
		while ( !(isEQ(incitrdIO.getStatuz(),varcom.endp))) {
			reinstateVf2Incis3000();
		}
		
		/*EXIT*/
		exitProgram();
	}

protected void readFirstInci1000()
	{
		readFirst1010();
	}

protected void readFirst1010()
	{
		incitrdIO.setChdrcoy(greversrec.chdrcoy);
		incitrdIO.setChdrnum(greversrec.chdrnum);
		incitrdIO.setLife(greversrec.life);
		incitrdIO.setCoverage(greversrec.coverage);
		incitrdIO.setRider(greversrec.rider);
		incitrdIO.setPlanSuffix(greversrec.planSuffix);
		incitrdIO.setValidflag(SPACES);
		incitrdIO.setTranno(99);
		incitrdIO.setSeqno(ZERO);
		incitrdIO.setInciNum(ZERO);
		incitrdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incitrdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incitrdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		incitrdIO.setFormat(incitrdrec);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			fatalErrors700();
		}
	}

protected void wipeoutVf1Incis2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					matchAndDel2010();
				}
				case bypassDeletion2020: {
					bypassDeletion2020();
				}
				case wipeoutVf1IncisExit2030: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void matchAndDel2010()
	{
		if (isNE(greversrec.chdrcoy,incitrdIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,incitrdIO.getChdrnum())
		|| isNE(greversrec.life,incitrdIO.getLife())
		|| isNE(greversrec.coverage,incitrdIO.getCoverage())
		|| isNE(greversrec.rider,incitrdIO.getRider())
		|| isNE(greversrec.planSuffix,incitrdIO.getPlanSuffix())) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			fatalErrors700();
		}
		if (isNE(incitrdIO.getValidflag(),"1")) {
			incitrdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.wipeoutVf1IncisExit2030);
		}
		if (isLT(incitrdIO.getTranno(),greversrec.tranno)) {
			goTo(GotoLabel.bypassDeletion2020);
		}
		incitrdIO.setFunction(varcom.deltd);
		incitrdIO.setFormat(incitrdrec);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			fatalErrors700();
		}
	}

protected void bypassDeletion2020()
	{
		incitrdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			fatalErrors700();
		}
	}

protected void continueBrowse2500()
	{
		contBrPara2510();
		getCurrentPremium2520();
	}

protected void contBrPara2510()
	{
		incitrdIO.setStatuz(varcom.oK);
		incitrdIO.setFunction(varcom.begn);
		incitrdIO.setFormat(incitrdrec);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			fatalErrors700();
		}
	}

protected void getCurrentPremium2520()
	{
		covrmjaIO.setChdrcoy(greversrec.chdrcoy);
		covrmjaIO.setChdrnum(greversrec.chdrnum);
		covrmjaIO.setLife(greversrec.life);
		covrmjaIO.setCoverage(greversrec.coverage);
		covrmjaIO.setRider(greversrec.rider);
		covrmjaIO.setPlanSuffix(greversrec.planSuffix);
		covrmjaIO.setFunction(varcom.readr);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalErrors700();
		}
		holdInstprem.set(covrmjaIO.getInstprem());
	}

protected void reinstateVf2Incis3000()
	{
		try {
			readVf2Record3010();
			getNextVf2Rec3020();
		}
		catch (GOTOException e){
		}
	}

protected void readVf2Record3010()
	{
		incitrdIO.setValidflag("1");
		incitrdIO.setTransactionDate(greversrec.transDate);
		incitrdIO.setTransactionTime(greversrec.transTime);
		incitrdIO.setUser(greversrec.user);
		incitrdIO.setTermid(greversrec.termid);
		incitrdIO.setTranno(greversrec.newTranno);
		incitrdIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			fatalError700();
		}
		if (isEQ(incitrdIO.getDormantFlag(),"N")) {
			holdInstprem.subtract(incitrdIO.getCurrPrem());
			if (isLT(holdInstprem,ZERO)) {
				syserrrec.params.set(incitrdIO.getParams());
				syserrrec.statuz.set(incitrdIO.getStatuz());
				fatalError700();
			}
			else {
				if (isEQ(holdInstprem,ZERO)) {
					incitrdIO.setStatuz(varcom.endp);
					goTo(GotoLabel.exit3090);
				}
			}
		}
	}

protected void getNextVf2Rec3020()
	{
		incitrdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incitrdIO);
		if (isNE(incitrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			fatalError700();
		}
		if ((isNE(incitrdIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(incitrdIO.getChdrnum(),greversrec.chdrnum))
		|| (isNE(incitrdIO.getCoverage(),greversrec.coverage))
		|| (isNE(incitrdIO.getLife(),greversrec.life))
		|| (isNE(incitrdIO.getRider(),greversrec.rider))
		|| (isNE(incitrdIO.getPlanSuffix(),greversrec.planSuffix))) {
			syserrrec.params.set(incitrdIO.getParams());
			syserrrec.statuz.set(incitrdIO.getStatuz());
			fatalError700();
		}
	}

protected void fatalError700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalErrors700();
				}
				case bomb780: {
					bomb780();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalErrors700()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.bomb780);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,2)) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void bomb780()
	{
		greversrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
