package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:50
 * Description:
 * Copybook name: UTRNREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnrevKey = new FixedLengthStringData(64).isAPartOf(utrnrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnrevChdrcoy = new FixedLengthStringData(1).isAPartOf(utrnrevKey, 0);
  	public FixedLengthStringData utrnrevChdrnum = new FixedLengthStringData(8).isAPartOf(utrnrevKey, 1);
  	public PackedDecimalData utrnrevTranno = new PackedDecimalData(5, 0).isAPartOf(utrnrevKey, 9);
  	public FixedLengthStringData utrnrevFeedbackInd = new FixedLengthStringData(1).isAPartOf(utrnrevKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(utrnrevKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}