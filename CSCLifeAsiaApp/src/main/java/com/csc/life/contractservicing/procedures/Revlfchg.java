/*
 * File: Revlfchg.java
 * Date: 30 August 2009 2:09:47
 * Author: Quipoz Limited
 * 
 * Class transformed from REVLFCHG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.contractservicing.dataaccess.AgcmbcrTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifetrnTableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv1TableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv2TableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.newbusiness.tablestructures.Tr627rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  REVLFCHG - Life Assured Changes Reversal (Clone from REVBCHG)
*  ------------------------------------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  THE FOLLOWING TRANSACTIONS ARE REVERSED :
*
*  - CHDR
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*            relevant contract.
*       DELET this record.
*
*            relevant  contract.  This should read a record with
*            valid flag of '2'. If not, then this is an error.
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*            altering  the  valid  flag from '2' to '1'.
*
*  - PAYR
*
*       It is assumed that the PAYRSEQNO is always 1 ie. multi-payor
*       has not been implemented yet. This is a very important
*       assumption.
*
*       READH on  the  Payor Detail record (PAYRLIF) for the contract.
*       DELET this record.
*       BEGN  on  the  Payor Detail record (PAYRLIF) for the contract.
*            This should read a record with valid flag '2'. If not,
*            then this is an error.
*       WRITD this Payor Detail record (PAYRLIF) after altering the
*            valid flag from '2' to '1'.
*
*  - COVR
*
*       Only reverse COVR records whose TRANNO is equal to the
*           Transaction number (of the PTRN) being reversed.
*       READH on  the  COVR  records for the relevant contract.
*       DELET this record.
*       NEXTR on  the  COVR  file should read a record with valid
*            flag  of  '2'. If not, then this is an error.
*       UPDAT this  COVR  record after altering the valid flag
*            from  '2'  to  '1' .
*
*       Check LIFE file to obtain the relevent LIFE rec for reverse.
*
*  - LIFE
*
*       Only reverse LIFE records whose TRANNO is equal to the
*           Transaction number (of the PTRN) being reversed.
*       READH on  the  LIFE  records for the relevant contract.
*       DELET this record.
*       NEXTR on  the  LIFE  file should read a record with valid
*            flag  of  '2'. If not, then this is an error.
*       UPDAT this  COVR  record after altering the valid flag
*            from  '2'  to  '1' .
*
*  - AGCMs
*
*       Read the first AGCM record (AGCMBCR).
*       Delete this record.
*       Read the next AGCM looking for valid flag 2 records.
*       Update these AGCM records with valid flag 1.
*
*
****************************************************************** ****
* </pre>
*/
public class Revlfchg extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVLFCHG";
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
	private ZonedDecimalData wsaaDobPlusTr627 = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private String wsaaAnbAtCcdFound = "N";
		/* TABLES */
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private static final String tr627 = "TR627";
	private AgcmbcrTableDAM agcmbcrIO = new AgcmbcrTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Fpcorv1TableDAM fpcorv1IO = new Fpcorv1TableDAM();
	private Fpcorv2TableDAM fpcorv2IO = new Fpcorv2TableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private LifetrnTableDAM lifetrnIO = new LifetrnTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Tr627rec tr627rec = new Tr627rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		processComponent3010, 
		exit3009, 
		exit3290, 
		findRecordToRevalidate3710
	}

	public Revlfchg() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		processChdrs2000();
		processPayrs2500();
		processCovrs3000();
		processFpcos4000();
		prcoessHpad5000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*INIT*/
		syserrrec.subrname.set(wsaaSubr);
		/*EXIT*/
	}

protected void processChdrs2000()
	{
		readContractHeader2001();
	}

protected void readContractHeader2001()
	{
		/*  delete valid flag 1 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getValidflag(), 1)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		chdrlifIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*  Select the next record which will be a validflag 2 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrlifIO.getValidflag(), 2)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*  Update the selected record from the contract header file*/
		chdrlifIO.setValidflag("1");
		chdrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processPayrs2500()
	{
		readPayorDetail2501();
	}

protected void readPayorDetail2501()
	{
		/*  delete valid flag 1 record*/
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		/* assumes that multi-payor has not been implemented yet.*/
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getValidflag(), 1)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		payrlifIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*  Select the next record which will be a validflag 2 record*/
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		/* assumes that multi-payor has not been implemented yet.*/
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), 2)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		/*  Update the selected record from the payr file*/
		payrlifIO.setValidflag("1");
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processCovrs3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covers3001();
				case processComponent3010: 
					processComponent3010();
				case exit3009: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covers3001()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrIO.getStatuz(), varcom.endp)
		|| isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)) {
			goTo(GotoLabel.exit3009);
		}
	}

protected void processComponent3010()
	{
		if (isNE(covrIO.getValidflag(), 1)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		wsaaCovrKey.set(covrIO.getDataKey());
		if (isEQ(covrIO.getTranno(), reverserec.tranno)) {
			deleteAndUpdateComp3100();
			processAgcms3600();
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp)
		|| isNE(covrIO.getDataKey(), wsaaCovrKey))) {
			getNextCovr3800();
		}
		
		if (isNE(covrIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.processComponent3010);
		}
	}

protected void deleteAndUpdateComp3100()
	{
		deleteRecs3101();
	}

protected void deleteRecs3101()
	{
		covrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/* Check for a increase record created by the forward*/
		/* transaction and delete it if one exists.*/
		checkLife3200();
		covrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/*    As we have just deleted the VALIDFLAG '1' record, there must*/
		/*    be a VALIDFLAG '2' record.*/
		if (isNE(covrIO.getValidflag(), 2)
		|| isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void checkLife3200()
	{
		try {
			readh3210();
			delet3220();
			updat3230();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readh3210()
	{
		lifetrnIO.setDataArea(SPACES);
		lifetrnIO.setChdrcoy(covrIO.getChdrcoy());
		lifetrnIO.setChdrnum(covrIO.getChdrnum());
		lifetrnIO.setLife(covrIO.getLife());
		lifetrnIO.setJlife("00");
		lifetrnIO.setTranno(covrIO.getTranno());
		lifetrnIO.setFunction(varcom.readh);
		lifetrnIO.setFormat(formatsInner.lifetrnrec);
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(), varcom.oK)
		&& isNE(lifetrnIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			syserrrec.params.set(lifetrnIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(lifetrnIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit3290);
		}
		wsaaAnbAtCcdFound = "Y";
	}

protected void delet3220()
	{
		lifetrnIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			syserrrec.params.set(lifetrnIO.getParams());
			xxxxFatalError();
		}
	}

protected void updat3230()
	{
		lifetrnIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifetrnIO.getParams());
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			xxxxFatalError();
		}
		/* As just deleted the VALIDFLAG '1' record, there must be a*/
		/* VALIDFLAG '2' record.*/
		if (isNE(lifetrnIO.getValidflag(), 2)
		|| isNE(lifetrnIO.getChdrcoy(), reverserec.company)
		|| isNE(lifetrnIO.getChdrnum(), reverserec.chdrnum)) {
			syserrrec.params.set(lifetrnIO.getParams());
			xxxxFatalError();
		}
		lifetrnIO.setValidflag("1");
		lifetrnIO.setCurrto(varcom.vrcmMaxDate);
		lifetrnIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, lifetrnIO);
		if (isNE(lifetrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifetrnIO.getParams());
			syserrrec.statuz.set(lifetrnIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processAgcms3600()
	{
		agcms3601();
	}

protected void agcms3601()
	{
		agcmbcrIO.setParams(SPACES);
		agcmbcrIO.setChdrcoy(covrIO.getChdrcoy());
		agcmbcrIO.setChdrnum(covrIO.getChdrnum());
		agcmbcrIO.setCoverage(covrIO.getCoverage());
		agcmbcrIO.setLife(covrIO.getLife());
		agcmbcrIO.setRider(covrIO.getRider());
		agcmbcrIO.setPlanSuffix(covrIO.getPlanSuffix());
		agcmbcrIO.setTranno(reverserec.tranno);
		agcmbcrIO.setSeqno(ZERO);
		agcmbcrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbcrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if ((isNE(agcmbcrIO.getStatuz(), varcom.oK))
		&& (isNE(agcmbcrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(agcmbcrIO.getChdrcoy(), covrIO.getChdrcoy()))
		|| (isNE(agcmbcrIO.getChdrnum(), covrIO.getChdrnum()))
		|| (isNE(agcmbcrIO.getCoverage(), covrIO.getCoverage()))
		|| (isNE(agcmbcrIO.getLife(), covrIO.getLife()))
		|| (isNE(agcmbcrIO.getRider(), covrIO.getRider()))
		|| (isNE(agcmbcrIO.getPlanSuffix(), covrIO.getPlanSuffix()))
		|| (isNE(agcmbcrIO.getTranno(), reverserec.tranno))) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmbcrIO.getStatuz(), varcom.endp))) {
			reverseAgcm3700();
		}
		
	}

protected void reverseAgcm3700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					reverse3701();
				case findRecordToRevalidate3710: 
					findRecordToRevalidate3710();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void reverse3701()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmbcrIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmbcrIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmbcrIO.getAgntnum());
		agcmdbcIO.setLife(agcmbcrIO.getLife());
		agcmdbcIO.setCoverage(agcmbcrIO.getCoverage());
		agcmdbcIO.setRider(agcmbcrIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmbcrIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmbcrIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmdbcIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmdbcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmdbcIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void findRecordToRevalidate3710()
	{
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(agcmdbcIO.getChdrcoy(), agcmbcrIO.getChdrcoy()))
		|| (isNE(agcmdbcIO.getChdrnum(), agcmbcrIO.getChdrnum()))
		|| (isNE(agcmdbcIO.getAgntnum(), agcmbcrIO.getAgntnum()))
		|| (isNE(agcmdbcIO.getLife(), agcmbcrIO.getLife()))
		|| (isNE(agcmdbcIO.getCoverage(), agcmbcrIO.getCoverage()))
		|| (isNE(agcmdbcIO.getRider(), agcmbcrIO.getRider()))
		|| (isNE(agcmdbcIO.getPlanSuffix(), agcmbcrIO.getPlanSuffix()))
		|| (isNE(agcmdbcIO.getSeqno(), agcmbcrIO.getSeqno()))) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getValidflag(), 2)) {
			agcmdbcIO.setFunction(varcom.nextr);
			goTo(GotoLabel.findRecordToRevalidate3710);
		}
		/*  Update The Selected Record on the Coverage File.*/
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmbcrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if ((isNE(agcmbcrIO.getStatuz(), varcom.oK))
		&& (isNE(agcmbcrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(agcmbcrIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if ((isNE(agcmbcrIO.getChdrcoy(), covrIO.getChdrcoy()))
		|| (isNE(agcmbcrIO.getChdrnum(), covrIO.getChdrnum()))
		|| (isNE(agcmbcrIO.getCoverage(), covrIO.getCoverage()))
		|| (isNE(agcmbcrIO.getLife(), covrIO.getLife()))
		|| (isNE(agcmbcrIO.getRider(), covrIO.getRider()))
		|| (isNE(agcmbcrIO.getPlanSuffix(), covrIO.getPlanSuffix()))
		|| (isNE(agcmbcrIO.getTranno(), reverserec.tranno))) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
	}

protected void getNextCovr3800()
	{
		/*GET-COVR*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processFpcos4000()
	{
		para4000();
	}

protected void para4000()
	{
		notFlexiblePremiumContract.setTrue();
		readT57294100();
		if (notFlexiblePremiumContract.isTrue()) {
			return ;
		}
		fpcorv1IO.setChdrcoy(reverserec.company);
		fpcorv1IO.setChdrnum(reverserec.chdrnum);
		fpcorv1IO.setEffdate(reverserec.ptrneff);
		fpcorv1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorv1IO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		fpcorv1IO.setFormat(formatsInner.fpcorv1rec);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(), varcom.oK)
		|| isNE(fpcorv1IO.getChdrcoy(), reverserec.company)
		|| isNE(fpcorv1IO.getChdrnum(), reverserec.chdrnum)
		|| isNE(fpcorv1IO.getEffdate(), reverserec.ptrneff)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		while ( !(isEQ(fpcorv1IO.getStatuz(), varcom.endp))) {
			reverseFpco4200();
		}
		
	}

protected void readT57294100()
	{
		para4100();
	}

protected void para4100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(reverserec.company, SPACES);
			stringVariable1.addExpression(t5729, SPACES);
			stringVariable1.addExpression(chdrlifIO.getCnttype(), SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), reverserec.company)
		|| isNE(itdmIO.getItemtabl(), t5729)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isGT(itdmIO.getItmfrm(), chdrlifIO.getOccdate())
		|| isLT(itdmIO.getItmto(), chdrlifIO.getOccdate())) {
			return ;
		}
		flexiblePremiumContract.setTrue();
	}

protected void reverseFpco4200()
	{
		para4200();
	}

protected void para4200()
	{
		fpcorv2IO.setChdrcoy(fpcorv1IO.getChdrcoy());
		fpcorv2IO.setChdrnum(fpcorv1IO.getChdrnum());
		fpcorv2IO.setLife(fpcorv1IO.getLife());
		fpcorv2IO.setCoverage(fpcorv1IO.getCoverage());
		fpcorv2IO.setRider(fpcorv1IO.getRider());
		fpcorv2IO.setPlanSuffix(fpcorv1IO.getPlanSuffix());
		fpcorv2IO.setTargfrom(fpcorv1IO.getTargfrom());
		/*    Reverse all FPCO records where the effective date*/
		/*    is equal to the PTRN effective date.*/
		fpcorv1IO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		fpcorv2IO.setFormat(formatsInner.fpcorv2rec);
		fpcorv2IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			xxxxFatalError();
		}
		fpcorv2IO.setValidflag("1");
		fpcorv2IO.setCurrto(varcom.vrcmMaxDate);
		fpcorv2IO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			xxxxFatalError();
		}
		fpcorv1IO.setFunction(varcom.nextr);
		fpcorv1IO.setFormat(formatsInner.fpcorv1rec);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(), varcom.oK)
		&& isNE(fpcorv1IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		if (isNE(fpcorv1IO.getChdrcoy(), reverserec.company)
		|| isNE(fpcorv1IO.getChdrnum(), reverserec.chdrnum)
		|| isNE(fpcorv1IO.getEffdate(), reverserec.ptrneff)) {
			fpcorv1IO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, fpcorv1IO);
			if (isNE(fpcorv1IO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fpcorv1IO.getParams());
				xxxxFatalError();
			}
			fpcorv1IO.setStatuz(varcom.endp);
		}
	}

protected void prcoessHpad5000()
	{
		start5010();
	}

protected void start5010()
	{
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(reverserec.company);
		hpadIO.setChdrnum(reverserec.chdrnum);
		hpadIO.setFunction(varcom.readr);
		hpadIO.setFormat(formatsInner.hpadrec);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(hpadIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		if (isNE(hpadIO.getValidflag(), "1")) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			xxxxFatalError();
		}
		readChdrlnb7000();
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(tr627);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		lifeenqIO.setDataKey(SPACES);
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(reverserec.company);
		lifeenqIO.setChdrnum(reverserec.chdrnum);
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			xxxxFatalError();
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setParams(SPACES);
		cltsIO.setStatuz(varcom.oK);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy("9");
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			xxxxFatalError();
		}
		tr627rec.tr627Rec.set(itemIO.getGenarea());
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(cltsIO.getCltdob());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(tr627rec.zsufcage);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.subrname.set("DATCON2");
			xxxxFatalError();
		}
		wsaaDobPlusTr627.set(datcon2rec.intDate2);
		if (isNE(wsaaAnbAtCcdFound, "Y")) {
			return ;
		}
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon2rec.frequency.set("01");
		compute(datcon2rec.freqFactor, 0).set(sub(tr627rec.zsufcage, lifetrnIO.getAnbAtCcd()));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.subrname.set("DATCON2");
			xxxxFatalError();
		}
		if (isGT(wsaaDobPlusTr627, datcon2rec.intDate2)) {
			datcon2rec.intDate1.set(datcon2rec.intDate2);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.subrname.set("DATCON2");
				xxxxFatalError();
			}
		}
		if (isEQ(cltsIO.getClntnum(), chdrlnbIO.getCownnum())) {
			return ;
		}
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(reverserec.company);
		hpadIO.setChdrnum(reverserec.chdrnum);
		hpadIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hpadIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		hpadIO.setFormat(formatsInner.hpadrec);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(hpadIO.getChdrcoy(), reverserec.company)
		|| isNE(hpadIO.getChdrnum(), reverserec.chdrnum)) {
			syserrrec.params.set(hpadIO.getParams());
			xxxxFatalError();
		}
		hpadIO.setValidflag("1");
		hpadIO.setFunction(varcom.writd);
		hpadIO.setFormat(formatsInner.hpadrec);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readChdrlnb7000()
	{
		/*START*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(reverserec.company);
		chdrlnbIO.setChdrnum(reverserec.chdrnum);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorBomb();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData lifetrnrec = new FixedLengthStringData(10).init("LIFETRNREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData fpcorv1rec = new FixedLengthStringData(10).init("FPCORV1REC");
	private FixedLengthStringData fpcorv2rec = new FixedLengthStringData(10).init("FPCORV2REC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
}
}
