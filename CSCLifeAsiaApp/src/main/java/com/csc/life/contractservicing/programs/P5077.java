/*
 * File: P5077.java
 * Date: 30 August 2009 0:03:51
 * Author: Quipoz Limited
 *
 * Class transformed from P5077.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;


import com.csc.fsu.agents.dataaccess.model.Agntpf;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.S5077ScreenVars;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnenqTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.BnkoutpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.impl.BnkoutpfDAOImpl;
import com.csc.life.newbusiness.dataaccess.model.Bnkoutpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                    CONTRACT SERVICING AGENT
*
* This program allows creation or modification of Servicing Agent
* Of the Contract. The Contract Header record file is updated.
*
* A "PTRN" record will be written.
*
* Initialise
* ----------
*
* The details of the contract being worked on will be stored in
* the CHDRMNA I/O module.  Retrieve the details.
* In order to format  the  required  names  the  client details
* record is read and  the  relevant copybook for formatting the
* names must be included in the program.
*
* Read  the  agent  details (AGNTLNB) and format the name using
* the copybooks.
*
*
* Validation
* ----------
*
* If KILL is requested, then skip validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
* Agent validation.
*
*       - if the Servicing agent (ex screen) equals spaces, then
*           exit from the validation.
*
*      - if an  agent  number  is entered, then read the client
*           details  and  format the name and return the output
*           to the screen.
*
*
*      - if CALC is pressed, then redisplay.
*
*Updating
*--------
*
* Updating occurs  with  the  updating  of  the contract header
* record, and the creation of a PTRN record.
*
* If the 'KILL' function key was pressed, skip the updating.
*
* CONTRACT HEADER UPDATE
*
*      - if there were  no  changes,  then  do  not  update the
*           contract header record.
*
*      - if there were  no  changes,  then  do  not  update the
*           contract header record.
*
*      - if there is an agent number screen entry, then replace
*           the servicing agent with this new agent number.
*
*      - rewrite (WRITS) the contract header record.
*
* GENERAL HOUSEKEEPING
*
*       - Write a PTRN record (if updating the CHDR):
*
*           - contract key from contract header
*           - transaction number from WSSP
*           - transaction effective date from VRCM-DATE
*           - batch key information WSSP batchkey
*
*      - Update the header by calling BATCUP with a function of
*           SPACES and the following parameters:
*
*           - transaction count equals 1
*           - all other amounts equal to zero
*           - batch key from WSSP
*
*      - Release contract SFTLOCK.
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5077 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5077");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaOrigagnt = new FixedLengthStringData(10).init(SPACES);
	private ZonedDecimalData wsaaLastTranno = new ZonedDecimalData(5, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 1).setUnsigned();

	private Agntpf agntpf=null;
	private AgntpfDAO agntDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
		/* ERRORS */
	private static final String e305 = "E305";
	private static final String e186 = "E186";
	private static final String e304 = "E304";
	private static final String e455 = "E455";
	private static final String t089 = "T089";
	private static final String e475 = "E475";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5688 = "T5688";
	private static final String t5500 = "T5500";
	private static final String tr384 = "TR384";
	private static final String ptrnrec = "PTRNREC";
	private static final String resnrec = "RESNREC";
	private static final String chdrmnarec = "CHDRMNAREC";
	private static final String resnenqrec = "RESNENQREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
		/*Joind FSU and Life agent headers - new b*/
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private Bnkoutpf bnkoutpf=new Bnkoutpf();
	private BnkoutpfDAO bnkoutpfDAO=new BnkoutpfDAOImpl();
	private static final String rrbu = "RRBU";
	private static final String rrbo = "RRB0";
	private static final String rrdo = "RRDO";
	private static final String rrdp = "RRDP";
	String bnkManagerName = null;
	String bnkTellerName = null;
	private boolean isBancassurance=false;
		/*Client role and relationships logical fi*/
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Reason Details  Logical File*/
	private ResnTableDAM resnIO = new ResnTableDAM();
		/*Reasn Details Enquiry*/
	private ResnenqTableDAM resnenqIO = new ResnenqTableDAM();
	private Batckey wsaaBatcKey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec1 = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Wssplife wssplife = new Wssplife();
	private S5077ScreenVars sv = ScreenProgram.getScreenVars( S5077ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkForErrors2080,
		exit2090,
		relsfl3070,
		readTr3846020
	}

	public P5077() {
		super();
		screenVars = sv;
		new ScreenModel("S5077", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
	}

protected void initialise1010()
	{
		wsaaBatcKey.set(wsspcomn.batchkey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		sv.cownnum.set(chdrmnaIO.getCownnum());
		sv.payrnum.set(chdrmnaIO.getPayrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		sv.billfreq.set(chdrmnaIO.getBillfreq());
		sv.mop.set(chdrmnaIO.getBillchnl());
		sv.agntnum.set(chdrmnaIO.getAgntnum());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmnaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		isBancassurance = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "AGMTN015", appVars, "IT");
		if(!isBancassurance){
			sv.bnkoutOut[varcom.nd.toInt()].set("Y");
			sv.bnksmOut[varcom.nd.toInt()].set("Y");
			sv.bnktelOut[varcom.nd.toInt()].set("Y");			
			
		}else{
			sv.bnkoutOut[varcom.nd.toInt()].set("N");
			sv.bnksmOut[varcom.nd.toInt()].set("N");
			sv.bnktelOut[varcom.nd.toInt()].set("N");			
		} 
		if(isBancassurance){
		readBanassurance();
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		if (isNE(sv.cownnum,SPACES)) {
			formatClientName1700();
		}
		if (isNE(sv.payrnum,SPACES)) {
			formatPayorName1800();
		}
		if (isNE(sv.agntnum,SPACES)) {
			formatAgentName1900();
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		if (isEQ(chdrmnaIO.getAgntnum(),SPACES)) {
			sv.oragntnam.set(SPACES);
			sv.agntsel.set(SPACES);
		}
		else {
			sv.agntsel.set(chdrmnaIO.getAgntnum());
			wsaaOrigagnt.set(chdrmnaIO.getAgntnum());
			sv.agentname.set(sv.oragntnam);
		}
		formatResnDetails5000();
		
	}

protected void readBanassurance(){
	bnkoutpf=bnkoutpfDAO.getBankRecord(sv.chdrnum.toString());
	if(bnkoutpf==null)
	{
		sv.bnkout.set(SPACES);
		sv.bnksm.set(SPACES);
		sv.bnktel.set(SPACES);
		sv.bnkoutname.set(SPACES);
		sv.bnksmname.set(SPACES);
		sv.bnktelname.set(SPACES);
	
	}else {
		sv.bnkout.set(bnkoutpf.getBnkout());
		sv.bnkoutname.set(bnkoutpf.getBnkoutname());
		sv.bnksm.set(bnkoutpf.getBnksm());		
		sv.bnksmname.set(bnkoutpf.getBnksmname());
		sv.bnktel.set(bnkoutpf.getBnktel());
		sv.bnktelname.set(bnkoutpf.getBnktelname());
		sv.agntsel.set(bnkoutpf.getAgntnum());
	}
}
protected void formatNewAgent1500()
	{
			readAgent1510();
		}

protected void readAgent1510()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntsel);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		if (isEQ(aglflnbIO.getStatuz(),varcom.mrnf)) {
			sv.agntselErr.set(e305);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(aglflnbIO.getAgntbr(),chdrmnaIO.getCntbranch())) {
			sv.agntselErr.set(e455);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isLT(aglflnbIO.getDtetrm(),wsaaToday)
		|| isLT(aglflnbIO.getDteexp(),wsaaToday)
		|| isGT(aglflnbIO.getDteapp(),wsaaToday)) {
			sv.agntselErr.set(e475);
			wsspcomn.edterror.set("Y");
		}
		sv.agentname.set(SPACES);
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isNE(cltsIO.getCltdod(),99999999)
		&& isLTE(cltsIO.getCltdod(),wsaaToday)) {
			sv.agntselErr.set(t089);
			wsspcomn.edterror.set("Y");
		}
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void formatClientName1700()
	{
		readClientRecord1710();
	}



protected void validateBanassurancefields()
{	
	
		
	if(isNE(sv.bnkout,SPACES))
	{
		if((sv.bnkout.toString().trim().length() < 14))
		{
			sv.bnkoutErr.set(rrbo);			
		} else {
			checkBanassrance();	
		}
		if(isNE(sv.bnkout,SPACES) && isEQ(sv.bnktel,SPACES) ) {
			sv.bnktelErr.set(e186);			
			return;
		}	
	}
	if((sv.bnktel.toString().trim().length() < 10))
	{
		sv.bnktelErr.set(rrdp);
		return;
	} else {
		bnkTellerName = agntDAO.getBanassuranceDetails(sv.bnktel.toString().trim(),"BT");
		if (bnkTellerName != null) {
			sv.bnktelname.set(bnkTellerName);
		} else {
			sv.bnktelErr.set(rrdo);
		}	
	}
	
}


protected void checkBanassrance()
{
	// ICIL-149 start			
				bnkoutpf = bnkoutpfDAO.getAgentRefcode(sv.bnkout.toString().trim());
				if (bnkoutpf != null) {
					sv.bnkout.set(bnkoutpf.getBnkout());
					sv.bnkoutname.set(bnkoutpf.getBnkoutname());
					sv.bnksm.set(bnkoutpf.getBnksm());
					sv.agntsel.set(bnkoutpf.getAgntnum());
					
				} else {
					sv.bnkoutErr.set(rrbu);
				}

				if (!isEQ(sv.bnksm, SPACES)) {
					bnkManagerName = agntDAO.getBanassuranceDetails(sv.bnksm.toString().trim(),null);
					if (bnkManagerName != null) {
						sv.bnksmname.set(bnkManagerName);
					}
				}
									
			// ICIL-149 end
	
}
protected void readClientRecord1710()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.cownnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void formatPayorName1800()
	{
		readClientRecord1810();
	}

protected void readClientRecord1810()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.payrnumErr.set(e304);
			sv.payorname.set(SPACES);
		}
		else {
			plainname();
			sv.payorname.set(wsspcomn.longconfname);
		}
	}

protected void formatAgentName1900()
	{
		readAgent1910();
	}

protected void readAgent1910()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntnum);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		sv.oragntnam.set(SPACES);
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.oragntnam.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					reascd2050();
				case checkForErrors2080:
					checkForErrors2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isNE(chdrmnaIO.getAgntnum(),SPACES)) {
			if (isEQ(sv.agntsel,SPACES)) {
				goTo(GotoLabel.exit2090);
			}
		}
		if (isEQ(sv.agntsel,SPACES)) {
			sv.agntselErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		formatNewAgent1500();
		if (isNE(wsspcomn.edterror,varcom.oK)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		/* Only read T5500 for the description if the user has not already */
		/* provided one.                                                   */
		if (isNE(sv.resndesc, SPACES)
		&& isNE(scrnparams.statuz, "CALC")) {
			goTo(GotoLabel.checkForErrors2080);
		}
		if(isBancassurance) {
			if(isEQ(chdrmnaIO.getSrcebus(),"BA"))
			 validateBanassurancefields();
			}
	}

protected void reascd2050()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5500);
		descIO.setDescitem(sv.reasoncd);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.resndesc.set(descIO.getLongdesc());
		}
		else {
			if (isNE(sv.reasoncd,SPACES)) {
				sv.resndesc.set("?");
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updatebanassurance();
					updateDatabase3010();
					writeReasncd3050();
					callBldenrl3060();
				case relsfl3070:
					relsfl3070();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}


private void updatebanassurance() {
	if(isBancassurance) //ICIL-149 start
	{
	bnkoutpf.setChdrnum(sv.chdrnum.toString());
	bnkoutpf.setBnkout(sv.bnkout.toString());
	bnkoutpf.setBnkoutname(sv.bnkoutname.toString());
	bnkoutpf.setBnksm(sv.bnksm.toString());
	bnkoutpf.setBnksmname(sv.bnksmname.toString());
	bnkoutpf.setBnktel(sv.bnktel.toString());
	bnkoutpf.setBnktelname(sv.bnktelname.toString());
	bnkoutpf.setAgntnum(sv.agntsel.toString().trim());
	bnkoutpfDAO.updateBnkoutRecord(bnkoutpf);
	}//ICIL-149 end
}

protected void updateDatabase3010()
	{
	
		if (isEQ(wsspcomn.company,chdrmnaIO.getAgntcoy())
		&& isEQ(sv.agntsel,chdrmnaIO.getAgntnum())) {
			goTo(GotoLabel.relsfl3070);
		}
		if (isEQ(sv.agntsel,SPACES)
		|| isEQ(sv.agntsel,wsaaOrigagnt)) {
			goTo(GotoLabel.relsfl3070);
		}
		chdrmnaIO.setFunction(varcom.rlse);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.readh);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		wsaaLastTranno.set(chdrmnaIO.getTranno());
		chdrmnaIO.setValidflag("2");
		chdrmnaIO.setFunction(varcom.rewrt);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setAgntnum(sv.agntsel);
		chdrmnaIO.setValidflag("1");
		setPrecision(chdrmnaIO.getTranno(), 0);
		chdrmnaIO.setTranno(add(chdrmnaIO.getTranno(),1));
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		chdrmnaIO.setFunction(varcom.writr);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		writeLetter6000();
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		wsaaBatcKey.set(wsspcomn.batchkey);
		ptrnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmnaIO.getChdrnum());
		ptrnIO.setTranno(chdrmnaIO.getTranno());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		batcuprec1.batcupRec.set(SPACES);
		batcuprec1.batchkey.set(wsspcomn.batchkey);
		batcuprec1.trancnt.set(1);
		batcuprec1.etreqcnt.set(ZERO);
		batcuprec1.sub.set(ZERO);
		batcuprec1.bcnt.set(ZERO);
		batcuprec1.bval.set(ZERO);
		batcuprec1.ascnt.set(ZERO);
		batcuprec1.statuz.set(ZERO);
		batcuprec1.function.set(SPACES);
		callProgram(Batcup.class, batcuprec1.batcupRec);
		if (isNE(batcuprec1.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec1.batcupRec);
			fatalError600();
		}
			}

protected void writeReasncd3050()
	{
		if (isNE(sv.resndesc,SPACES)
		|| isNE(sv.reasoncd,SPACES)) {
			resnIO.setDataKey(SPACES);
			resnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
			resnIO.setChdrnum(chdrmnaIO.getChdrnum());
			resnIO.setTranno(chdrmnaIO.getTranno());
			resnIO.setReasoncd(sv.reasoncd);
			resnIO.setResndesc(sv.resndesc);
			resnIO.setTrancde(wsaaBatcKey.batcBatctrcde);
			resnIO.setTransactionDate(varcom.vrcmDate);
			resnIO.setTransactionTime(varcom.vrcmTime);
			resnIO.setUser(varcom.vrcmUser);
			resnIO.setFormat(resnrec);
			resnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, resnIO);
			if (isNE(resnIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(resnIO.getParams());
				fatalError600();
			}
		}
		
		
	}

protected void callBldenrl3060()
	{
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrmnaIO.getChdrpfx());
		bldenrlrec.company.set(chdrmnaIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrmnaIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
	}

protected void relsfl3070()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void formatResnDetails5000()
	{
		start5010();
	}

protected void start5010()
	{
		resnenqIO.setParams(SPACES);
		resnenqIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		resnenqIO.setChdrnum(chdrmnaIO.getChdrnum());
		resnenqIO.setTrancde(wsaaBatcKey.batcBatctrcde);
		resnenqIO.setTranno(999);
		resnenqIO.setFormat(resnenqrec);
		resnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		resnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		resnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANCDE");
		SmartFileCode.execute(appVars, resnenqIO);
		if (isNE(resnenqIO.getStatuz(),varcom.oK)
		&& isNE(resnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmnaIO.getChdrcoy(),resnenqIO.getChdrcoy())
		|| isNE(chdrmnaIO.getChdrnum(),resnenqIO.getChdrnum())
		|| isNE(wsaaBatcKey.batcBatctrcde,resnenqIO.getTrancde())
		|| isEQ(resnenqIO.getStatuz(),varcom.endp)) {
			sv.reasoncd.set(SPACES);
			sv.resndesc.set(SPACES);
		}
		else {
			sv.reasoncd.set(resnenqIO.getReasoncd());
			sv.resndesc.set(resnenqIO.getResndesc());
		}
	}

protected void writeLetter6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start6010();
				case readTr3846020:
					readTr3846020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6010()
	{
		wsaaItemBatctrcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
	}

protected void readTr3846020()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)
			&& isNE(wsaaItemCnttype,"***")) {
				wsaaItemCnttype.set("***");
				goTo(GotoLabel.readTr3846020);
			}
			else {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType,SPACES)) {
			return ;
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.branch.set(wsspcomn.branch);
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(wsspcomn.language);
		wsaaOkeyTranno.set(wsaaLastTranno);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatcKey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}
}
