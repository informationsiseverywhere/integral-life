package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:21
 * Description:
 * Copybook name: T6760REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6760rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6760Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(t6760Rec, 0);
  	public FixedLengthStringData prgm = new FixedLengthStringData(5).isAPartOf(t6760Rec, 1);
  	public FixedLengthStringData jobq = new FixedLengthStringData(10).isAPartOf(t6760Rec, 6);
  	public FixedLengthStringData filler = new FixedLengthStringData(484).isAPartOf(t6760Rec, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6760Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6760Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}