package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:49
 * Description:
 * Copybook name: TLOANTRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tloantrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData tloantrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData tloantrKey = new FixedLengthStringData(64).isAPartOf(tloantrFileKey, 0, REDEFINE);
  	public FixedLengthStringData tloantrChdrcoy = new FixedLengthStringData(1).isAPartOf(tloantrKey, 0);
  	public FixedLengthStringData tloantrChdrnum = new FixedLengthStringData(8).isAPartOf(tloantrKey, 1);
  	public PackedDecimalData tloantrLoanNumber = new PackedDecimalData(2, 0).isAPartOf(tloantrKey, 9);
  	public FixedLengthStringData tloantrValidflag = new FixedLengthStringData(1).isAPartOf(tloantrKey, 11);
  	public PackedDecimalData tloantrFirstTranno = new PackedDecimalData(5, 0).isAPartOf(tloantrKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(tloantrKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tloantrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tloantrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}