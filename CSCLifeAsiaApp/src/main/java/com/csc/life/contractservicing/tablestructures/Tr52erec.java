package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:26
 * Description:
 * Copybook name: TR52EREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr52erec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr52eRec = new FixedLengthStringData(501);
  	public FixedLengthStringData taxinds = new FixedLengthStringData(13).isAPartOf(tr52eRec, 0);
  	public FixedLengthStringData[] taxind = FLSArrayPartOfStructure(13, 1, taxinds, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(taxinds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData taxind01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData taxind02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData taxind03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData taxind04 = new FixedLengthStringData(1).isAPartOf(filler, 3);
  	public FixedLengthStringData taxind05 = new FixedLengthStringData(1).isAPartOf(filler, 4);
  	public FixedLengthStringData taxind06 = new FixedLengthStringData(1).isAPartOf(filler, 5);
  	public FixedLengthStringData taxind07 = new FixedLengthStringData(1).isAPartOf(filler, 6);
  	public FixedLengthStringData taxind08 = new FixedLengthStringData(1).isAPartOf(filler, 7);
  	public FixedLengthStringData taxind09 = new FixedLengthStringData(1).isAPartOf(filler, 8);
  	public FixedLengthStringData taxind10 = new FixedLengthStringData(1).isAPartOf(filler, 9);
  	public FixedLengthStringData taxind11 = new FixedLengthStringData(1).isAPartOf(filler, 10);
  	public FixedLengthStringData taxind12 = new FixedLengthStringData(1).isAPartOf(filler, 11);
  	public FixedLengthStringData taxind13 = new FixedLengthStringData(1).isAPartOf(filler, 12);
  	public FixedLengthStringData txitem = new FixedLengthStringData(4).isAPartOf(tr52eRec, 13);
  	public FixedLengthStringData zbastyp = new FixedLengthStringData(1).isAPartOf(tr52eRec, 17);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(483).isAPartOf(tr52eRec, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr52eRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr52eRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}