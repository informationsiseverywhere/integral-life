
/*
 * File: Pr576.java
 * Date: 30 August 2009 1:45:41
 * Author: Quipoz Limited
 * 
 * Class transformed from PR576.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.life.contractservicing.dataaccess.AnntmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CprpTableDAM;
import com.csc.life.contractservicing.dataaccess.CvuwTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.screens.Sr576ScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.terminationclaims.dataaccess.AnntTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CovppfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Covppf;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgwTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*
*
*
*  COMPONENT CHANGE - DISABILITY COMPONENTS.
*  -----------------------------------------
*
*  This screen/program PR576 is  used  to  capture any  changes
*  to  the    coverage   and   rider  details  for  Disability
*  components.
*
*  Initialise.
*  -----------
*
*  Skip this section  if    returning    from    an    optional
*  selection (current stack position action flag = '*').
*
*  Read  CHDRLIF  (RETRV)  in    order  to  obtain the contract
*  header information.
*
*  The key for Coverage/rider  to    be    worked  on  will  be
*  available  from  COVR.   This is obtained by using the RETRV
*  function.
*
*  Firstly  we  need  to check  if  there  has  already    been
*  an   alteration   to   this     coverage/rider  within  this
*  transaction. If this  is   the  case  there  will    be    a
*  record      present     on    the  temporary  coverage/rider
*  transaction  file    COVTPF.    Read  this  file  using  the
*  logical  view    COVTMAJ  and  the  key  from  the retrieved
*  coverage/rider record. If  a  record  is  found  go  to  the
*  SET-UP-SCREEN section.
*
*  Compare  the  plan  suffix  of  the  retrieved  record  with
*  the contract header no of policies  in  plan    field.    If
*  the  plan  suffix  is '00' go to SET-UP-COVT.  If it is less
*  than or equal to this field the we need  to  'breakout'  the
*  amount fields as follows:
*
*  Divide the COVR sum assured  field  by  the CHDR no of
*                policies in plan.
*  Divide  the COVR premium  field  by  the  CHDR  no  of
*                policies in plan.
*
*  ******* SET-UP-COVT.
*
*  Move    the  appropriate fields  from  the  COVR  record  to
*  the COVTMAJ record.
*
*  ******* SET-UP-SCREEN.
*
*  Read  the  contract definition  details  from   T5688    for
*  the  contract  type  held on  CHDRLIF.  Access  the  version
*  of this item for the original commencement date of the risk.
*
*  Read  the  general  coverage/rider  details from  T5687  and
*  the  traditional/Disability   edit  rules from T5608 for the
*  coverage type held  on  COVTMAJ.  Access  the    version  of
*  these item for the original commencement date of the risk.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To  obtain the life assured  and joint-life details (if any)
*  do the following;-
*
*       - read the life  details  using  LIFEMAJ  (life  number
*       from  COVTMAJ,  joint  life number  '00').  Look up the
*       name from the client details (CLTS)  and  format  as  a
*       "confirmation name".
*
*       -  read  the  joint    life details using LIFEMAJ (life
*       number from COVTMAJ,  joint  life  number  '01').    If
*       found,  look  up  the  name  from  the  client  details
*       (CLTS) and format as a "confirmation name".
*
*  To determine which premium  calculation    method    to  use
*  decide  whether  or  not  it  is  a   Single  or  Joint-life
*  case  (it is a joint life case, the  joint    life    record
*  was  found  above). Then:
*
*       -  if  it  is a single-life case use, the single-method
*       from T5687. The age to be used  for validation will  be
*       the age of the main life.
*
*       -  if  the joint-life  indicator (from T5687) is blank,
*       and  if  it  is  a    Joint-life    case,    use    the
*       joint-method  from  T5687.  The  age  to  be  used  for
*       validation will be the age of the main life.
*
*       -  if the Joint-life  indicator  is  'N',   then    use
*       the  Single-method.    But,  if  there  is a joint-life
*       (this must be a rider to have got  this  far)    prompt
*       for  the  joint  life  indicator  to  determine   which
*       life  the rider is to attach to.  In all  other  cases,
*       the  joint  life  indicator should be non-displayed and
*       protected.  The  age  to  be used for  validation  will
*       be  the    age  of the main or joint life, depending on
*       the one selected.
*
*       - use the premium-method  selected  from   T5687,    if
*       not  blank, to access T5675.  This gives the subroutine
*       to use for the calculation.
*
*      COVERAGE/RIDER DETAILS
*
*  The fields to be displayed  on  the  screen  are  determined
*  from the entry in table T5608 read earlier as follows:
*
*       -  if  the  maximum and minimum sum insured amounts are
*       both zero, non-display  and  protect  the  sum  insured
*       amount.    If  the  minimum and maximum  are  both  the
*       same, display the  amount  protected.  Otherwise  allow
*       input.
*
*       -    if    all    the   valid   mortality  classes  are
*       blank, non-display and protect this   field.  If  there
*       is  only one mortality class, display  and  protect  it
*       (no validation will be required).
*
*       - if all the valid  lien codes are  blank,  non-display
*       and  protect this field. Otherwise, this is an optional
*       field.
*
*       - if the cessation  AGE  section  on  T5608  is  blank,
*       protect the two age related fields.
*
*       -    if  the  cessation  Disability  section  on  T5608
*       is  blank, protect the two Disability related fields.
*
*       - using the age next  birthday  (ANB  at   RCD)    from
*       the  applicable  life (see above), look up Issue Age on
*       the AGE and Disability sections.   If  the    age  fits
*       into  only  one  "slot" in one of these  sections,  and
*       the risk cessation limits are the same,  default    and
*       protect    the    risk  cessation fields.  Also  do the
*       same  for  the  premium  cessation details.    In  this
*       case,    also    calculate    the    risk   and premium
*       cessation dates.
*
*       -  please note that it  is  possible  for   the    risk
*       and  premium  cessation  dates    to   be  overwritten,
*       i.e.  even after they are calculated.
*
*      OPTIONS AND EXTRAS
*
*  If options and extras are  not   allowed    (as  defined  by
*  T5608) non-display and protect the fields.
*
*  Otherwise,  read  the  options  and  extras  details for the
*  current coverage/rider.   If any records  exist,    put    a
*  '+'    in   the Options/Extras indicator (to show that there
*  are some).
*
*  If options and extras  are  not  allowed,  then  non-display
*  and  protect  the  special  terms    narrative  clause  code
*  fields. If special terms are present,    then    obtain  the
*  clause  codes  from  the  coverage  record and output to the
*  screen.
*
* REASSURANCE
*
* If Reassurance is not allowed ( as defined by T5687 ), then
* non-display and protect the field ( S5123-RATYPIND ).
*
* Otherwise,  read  the  Reassurance  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the  Reassurance  indicator (to show that there are some).
*
*  Finally after setting up  all  of  the    screen  attributes
*  move over the relevant fields from COVTMAJ where applicable.
*
*  Validation.
*  -----------
*
*  Skip  this  section  if    returning    from    an  optional
*  selection (current stack position action flag = '*').
*
*  If CF11 (KILL) is requested then skip the validation.
*
*  Before the premium amount  is  calculated,  the screen  must
*  be  valid.    So  all  editing    is  completed  before  the
*  premium is calculated.
*
*  Table T5608 (previously read)  is    used    to  obtain  the
*  editing  rules.    Edit  the  screen according  to the rules
*  defined by the help. In particular:-
*
*       1) Check the  sum-assured,  if   applicable,    against
*       the limits.
*
*       2)   Check  the  consistency  of    the  risk  age  and
*       Disability  fields  and  premium  age  and   Disability
*       fields.  Either,  risk  age and and premium age must be
*       used  or  risk  Disability and premium Disability  must
*       be  used.    They must not be combined. Note that these
*       only need validating if they were not defaulted.
*
*       3) Mortality-Class, if the  mortality    class  appears
*       on  a  coverage/rider  screen  it is a compulsory field
*       because  it  will    be  used  in    calculating    the
*       premium    amount.    The  mortality class entered must
*       one  of the ones in the edit rules table.
*
*  Calculate the following:-
*
*       - risk cessation date
*
*       - premium cessation date
*
*       - note, the risk and premium  cessation  dates  may  be
*       overwritten by the user.
*
*  OPTIONS AND EXTRAS
*
*  If  options/extras  already  exist, there  will  be a '+' in
*  this field.  A request to access  the  details  is  made  by
*  entering  'X'.    No other values  (other  than  blank)  are
*  allowed.  If  options  and  extras  are  requested,  DO  NOT
*  CALCULATE THE PREMIUM THIS TIME AROUND.
*
* REASSURANCE
*
* If  Reassurance  already exists, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed.
*
*  PREMIUM CALCULATION.
*  --------------------
*
*  The    premium   amount is  required  on  all  products  and
*  all validating  must  be   successfully  completed    before
*  it    is  calculated.  If  there  is    no   premium  method
*  defined (i.e the relevant code  was  blank),    the  premium
*  amount  must  be  entered.  Otherwise,  it  is  optional and
*  always calculated.
*
*  To  calculate  it,    call    the    relevant    calculation
*  subroutine worked out above passing:
*
*       - 'CALC' for the function
*       - Coverage code (crtable)
*       - Company
*       - Contract number
*       - Life number
*       -  Joint life number (if the screen indicator is set to
*       'J' ,otherwise pass '00')
*       - Coverage number
*       - Rider number (0 for coverages)
*       - Effective date (Premium / Risk)
*       - Termination date (i.e. Disability cessation date)
*       - Currency
*       - Sum insured
*       - Calculation "Basis" (i.e. mortality code)
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium amount ("instalment")
*       - Return Status
*       - Rating Date (effective date of transaction)
*
*  Subroutine may look up:
*
*       - Life and Joint-life details
*       - Options/Extras
*
*  Having calculated it, the    entered    value,  if  any,  is
*  compared  with  it  to check that it  is  within  acceptable
*  limits of the automatically  calculated  figure.    If    it
*  is    less    than    the  amount    calculated  and  within
*  tolerance  then  the  manually entered  amount  is  allowed.
*  If    the    entered  value  exceeds the calculated one, the
*  calculated value is used.
*
*  To check the tolerance  amount,  read  the  tolerance  limit
*  from  T5667  (item  is  transaction  and  coverage  code, if
*  not found, item and '****'). Although this  is    a    dated
*  table, just read the latest one (using ITEM).
*
*      If 'CALC' was entered then re-display the screen.
*
*  Updating.
*  ---------
*
*  Updating  occurs  if  any of the  fields  on the screen have
*  been amended from the original COVTMAJ details.
*
*  If the 'KILL' function key was pressed skip the updating.
*
*  Update the COVTMAJ record with the details from  the  screen
*  and write/rewrite the record to the database.
*
*  IF no Coverage/rider details have been changed, but
*    Reassurance or Options/Extras details have been amended,
*    we force the program to produce a COVT record so that the
*    Component change AT module is executed, thus writing a PTRN
*    indicating that something has happened to the Coverage/Rider
*    details.
*
*  Next Program.
*  -------------
*
*  The    first  thing  to   consider   is   the   handling  of
*  an options/extras request. If  the    indicator  is  'X',  a
*  request  to  visit options and extras has been made. In this
*  case:
*
*       - change the options/extras request indicator to '?',
*
*       - save the next 8 programs from the program stack,
*
*       - call GENSSWCH with and  action  of  'A'  to  retrieve
*       the  program  switching  required, and move them to the
*       stack,
*
*       - set the current stack "action" to '*',
*
*       - add one to the program pointer and exit.
*
*  On return from this  request,  the  current  stack  "action"
*  will  be    '*' and the  options/extras  indicator  will  be
*  '?'.  To handle the return from options and extras:
*
*       -  calculate  the  premium  as  described   above    in
*       the  'Validation'  section,  and  check    that  it  is
*       within the tolerance limit,
*
*       -  blank  out  the  stack   "action",    restore    the
*       saved programs to the program stack,
*
*       -  if  the  tolerance   checking  results  in  an error
*       being detected, set WSSP-NEXTPROG  to    the    current
*       screen name (thus returning to re-display the screen).
*
* If Reassurance has been requested (the indicator = 'X'), then..
*
*  - change the Reassurance request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with and  action  of 'B' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance  indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
*  If  control  is passed to this  part  of the 4000 section on
*  the way  out  of the program,  ie.   after    screen    I/O,
*  then    the  current  stack  position  action  flag  will be
*  blank.  If the 4000 section  is  being    performed    after
*  returning      from  processing  another  program  then  the
*  current  stack  position action flag will be '*'.
*
*  If 'Enter' has been pressed  add 1 to  the  program  pointer
*  and exit.
*
*  If  'KILL'  has  been requested, (CF11), then move spaces to
*  the current program entry in the program stack and exit.
*
*  ANNUITY DETAILS SELECT SCREEN
*  =============================
*
*  As part of the 9405 Annuities Development a new field,
*  Annuity Details, is displayed if the component is an annuity
*  component.
*
*  These components are identified by their coverage code
*  (CRTABLE) being a valid item on the Annuity Component Edit
*  Rules Table (T6625). The new field should only be displayed
*  if the coverage code is on this table.
*
*  If the details have not already been created for this
*  component, an X is displayed  in ANNTIND, otherwise a +
*  is displayed.
*
*****************************************************************
* </pre>
*/
public class Pr576 extends ScreenProgCS {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR576");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTempPrev = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaPrevDue = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNextDue = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaDay1 = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaDay2 = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaProrate = new ZonedDecimalData(11, 6).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaWorkingAnb = new PackedDecimalData(3, 0);
	protected ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaSuspAvail = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaBenCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaPovrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPovrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaBcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaPcestermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcestermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaBcestermStore = new ZonedDecimalData(3, 0);
	private String wsaaPovrModified = "N";
	private final int wsaaMaxOcc = 8;
	private final int wsaaMaxMort = 6;
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);
	private String wsaaAnnuity = "";

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private Validator premNotReqd = new Validator(wsaaPremStatuz, "N");
	private Validator userPremEntered = new Validator(wsaaPremStatuz, "U");
	private PackedDecimalData wsaaCalcPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFullPrem = new PackedDecimalData(17, 2);
		/* WSAA-TMP1 */
	private ZonedDecimalData wsaaTmpprem = new ZonedDecimalData(18, 3).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(wsaaTmpprem, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaDecimal3 = new ZonedDecimalData(1, 0).isAPartOf(filler, 17).setUnsigned();

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator policyLevel = new Validator(wsaaPlanproc, "N");
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");

	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	protected Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	protected Validator addComp = new Validator(wsaaFlag, "A");
	private Validator modifyComp = new Validator(wsaaFlag, "M");
	private Validator compEnquiry = new Validator(wsaaFlag, "I");
	private Validator compApp = new Validator(wsaaFlag, "S","P");
	private Validator addApp = new Validator(wsaaFlag, "P");
	private Validator revComp = new Validator(wsaaFlag, "R");
	private Validator addLife = new Validator(wsaaFlag, "D");
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaProprem = new ZonedDecimalData(15, 5);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNextSeqnbr = new PackedDecimalData(3, 0).init(999);
	private PackedDecimalData wsaaStoreSumin = new PackedDecimalData(15, 0);
	protected PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(15, 0);
	private ZonedDecimalData wsaaOldPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSuminsDiff = new PackedDecimalData(17, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaAnb = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaProtax = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaProtaxRate = new ZonedDecimalData(10, 5);
	private ZonedDecimalData wsaaStoreZbinstprem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreSingp = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreInstprem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPrevSumins = new PackedDecimalData(17, 0);
	private PackedDecimalData wsaaKeepSumins = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaOptext = new FixedLengthStringData(1);
	private Validator optextYes = new Validator(wsaaOptext, "Y");
	private Validator optextNo = new Validator(wsaaOptext, "N");

	private FixedLengthStringData wsaaIfSuminsChanged = new FixedLengthStringData(1);
	private Validator wsaaSuminsChanged = new Validator(wsaaIfSuminsChanged, "I","D");
	private Validator wsaaSuminsIncreased = new Validator(wsaaIfSuminsChanged, "I");
	private Validator wsaaSuminsDecreased = new Validator(wsaaIfSuminsChanged, "D");

	private FixedLengthStringData wsaaIfPremChanged = new FixedLengthStringData(1);
	private Validator wsaaPremChanged = new Validator(wsaaIfPremChanged, "I","D");
	private Validator wsaaPremIncreased = new Validator(wsaaIfPremChanged, "I");
	private Validator wsaaPremDecreased = new Validator(wsaaIfPremChanged, "D");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
		/* WSAA-MAIN-LIFE-DETS */
	protected PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler1, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler3, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);

		/* Plan selection.*/
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	protected FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	protected FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	protected FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private FixedLengthStringData filler5 = new FixedLengthStringData(49).isAPartOf(wsaaCovrKey, 15, FILLER).init(SPACES);
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaOldMortcls = new FixedLengthStringData(1);
		/* WSBB-JOINT-LIFE-DETS */
	protected PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
	private ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddBenCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddBenCessTerm = new ZonedDecimalData(3, 0);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzBenCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzBenCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsccSingPremFlag = new FixedLengthStringData(1);
	private Validator singlePremium = new Validator(wsccSingPremFlag, "Y");
	private FixedLengthStringData wsaaLextUpdates = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private AnntTableDAM anntIO = new AnntTableDAM();
	private AnntmjaTableDAM anntmjaIO = new AnntmjaTableDAM();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovrrgwTableDAM covrrgwIO = new CovrrgwTableDAM();
	protected CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private CprpTableDAM cprpIO = new CprpTableDAM();
	private CvuwTableDAM cvuwIO = new CvuwTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
	//private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM(); //ILB-456
	private MandTableDAM mandIO = new MandTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	protected PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	protected Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Gensswrec gensswrec = new Gensswrec();
	protected Premiumrec premiumrec = new Premiumrec();
	private T2240rec t2240rec = new T2240rec();
	protected T5606rec t5606rec = getT5606rec();
	protected T5606rec getT5606rec() {
		return new T5606rec();
	}
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	protected T5675rec t5675rec = new T5675rec();
	protected T5687rec t5687rec = new T5687rec();
	private T6005rec t6005rec = new T6005rec();
	private Th506rec th506rec = new Th506rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Wssplife wssplife = new Wssplife();
	private Sr576ScreenVars sv = getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();

	/*TSD-308 START*/
    private Datcon4rec datcon4rec = new Datcon4rec();	// TSD-308
	private ZonedDecimalData wsaaMnthVers = new ZonedDecimalData(8, 0);	// TSD-308
	private ZonedDecimalData wsaaLastAnnv = new ZonedDecimalData(8, 0);	// TSD-308
	private CovrTableDAM covrIO = new CovrTableDAM();	// TSD-308
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	boolean isFeaAllowed = false;
	/*TSD-308 END*/

	private boolean stampDutyflag = false;
	private String stateCode="";
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private String occuptationCode="";
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	protected ExternalisedRules er = new ExternalisedRules();
	private boolean loadingFlag = false;/* BRD-306 */
	//BRD-009-starts
	//private LifepfDAO lifeDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAOImpl.class);
	private boolean occFlag = false;
	private String occupation;
	//BRD-009-ends
	private boolean waitperiodFlag=false;
	private boolean bentrmFlag=false;
	private boolean poltypFlag=false;
	private boolean prmbasisFlag=false;
	private boolean dialdownFlag = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private boolean exclFlag = false;
	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);
	private RegpTableDAM regpIO = new RegpTableDAM();
	private static final String regprec = "REGPREC";
	private List<Itempf> itempfList;
	private List<Itempf> itempfList1;
	private List<Regppf> regppfList;
	private List<Regppf> regpList;
	private Itempf itempf = null;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private FixedLengthStringData wsaaT6696Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6696Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6696Key, 0);
	private FixedLengthStringData wsaaT6696Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6696Key, 4);
	private T6696rec t6696rec = new T6696rec();
	private T6693rec t6693rec = new T6693rec();
	private Regpsubrec regpsubrec = new Regpsubrec();
	private Regppf regppf = null;
	private boolean RegpayFlag = false;
	private static final String t6625 ="T6625";
	private static final String t6696 ="T6696";
	private static final String t6693 ="T6693";
    private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
    boolean CSMIN003Permission  = false;
    private static final String t6a7 = "T6A7";
    private static final String t6a8 = "T6A8";
    PackedDecimalData wsaaCashValArr = new PackedDecimalData(17, 2);
 	/*
 	 * fwang3 ICIL-560
 	 */
 	private Th528rec th528rec = new Th528rec();
 	private boolean hasCashValue = false;
 	private PackedDecimalData wsaaHoldSumin = new PackedDecimalData(15, 0);
 	private boolean chinaLocalPermission;
 

	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private T3644rec t3644rec = new T3644rec();
	boolean NBPRP056Permission  = false;
	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	private Ta610rec ta610rec = new Ta610rec();
	private T5661rec t5661rec = new T5661rec();
	boolean isFollowUpRequired  = false;
	private Fluppf fluppf;
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();	//ICIL-1494
	private int fupno = 0;
	private List<Fluppf> fluppfList = new ArrayList<Fluppf>();
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private Descpf fupDescpf;
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private static final String NBPRP056 = "NBPRP056";
	private static final String t5661 = "T5661";
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> itempfList2 = new ArrayList<Itempf>();	//ICIL-1494
	
	//ILB-456 starts
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;
	private int covrpfCount = 0;
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private List<Lifepf> lifeList = null;
	//ILJ-46
	private boolean contDtCalcFlag = false;
	private String cntDteFeature = "NBPRP113";
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private Covtpf covtmjapf = new Covtpf();
	//IBPLIFE-2135 Start
	private boolean covrprpseFlag = false;
	private static final String t1688 = "T1688";
	private CovppfDAO covppfDAO= getApplicationContext().getBean("covppfDAO",CovppfDAO.class);
	private Covppf covppf = new Covppf();
	private List<Covppf> listCovppf = new ArrayList<>();
	private Descpf descpf;
	//IBPLIFE-2135 End
	
	//end
    /**
    * Contains all possible labels used by goTo action.
    */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setScreen1060, 
		exit1090, 
		exit1190, 
		preExit, 
		cont2030, 
		beforeExit2080, 
		exit2090, 
		checkSuminCont2526, 
		checkRcessFields2120, 
		validDate2140, 
		datesCont2140, 
		ageAnniversary2140, 
		check2140, 
		checkOccurance2140, 
		checkTermFields2150, 
		checkComplete2160, 
		checkMortcls2170, 
		loop2175, 
		checkLiencd2180, 
		loop2185, 
		checkMore2190, 
		cont, 
		exit2290, 
		updatePcdt3060, 
		exit3090, 
		exit3290, 
		exit3490, 
		exit50z0, 
		exit5290, 
		exit5390, 
		covtExist6025, 
		a200CallTaxSubr
	}
	
	protected Sr576ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sr576ScreenVars.class);
	}
	
	public Pr576() {
		super();
		screenVars = sv;
		new ScreenModel("Sr576", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					contractHeader1020();
					contractCoverage1030();
					headerToScreen1040();
					lifeDetails1050();
				case setScreen1060: 
					setScreen1060();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{	
		//ILJ-46
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
		
		/* If returning from a multiple selection then go to exit.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		NBPRP056Permission  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), NBPRP056, appVars, "IT");
		/* Move WSSP key details to working storage for later updates*/
		/*   and table reads.*/
		/* Move WSSP FLAG to working storage for later program condition*/
		/*   checks.*/
		wsaaBatckey.set(wsspcomn.batchkey);
		if(isNE(wsspcomn.flag,"X"))
		{
			wsaaFlag.set(wsspcomn.flag); 
		}
		else {
			if (isEQ(wsspcomn.sbmaction,"C")) { 
				wsaaFlag.set("P"); 
				
			}
			 
		}
		
        CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
		isFeaAllowed = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "MTL30801", appVars,smtpfxcpy.item.toString());
		RegpayFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM006", appVars, "IT");
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);	//ICIL-1494
		}
		//IBPLIFE-2135 Start
		covrprpseFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP126", appVars, "IT");
		if(covrprpseFlag){
            sv.nbprp126lag.set("Y");
		}else{
           sv.nbprp126lag.set("N");
		}
		//IBPLIFE-2135 End
		/* Intialise the Screen.*/
		sv.dataArea.set(SPACES);
		// ILJ-46
		if (!contDtCalcFlag) {
			sv.riskCessAgeOut[varcom.nd.toInt()].set("Y");
		}
		// end
		sv.anbAtCcd.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		
		sv.adjustageamt.set(0);
		prmbasisFlag=false;
		poltypFlag=false;
		bentrmFlag=false;
		waitperiodFlag=false;
		/*BRD-306 END */
		sv.zstpduty01.set(ZERO);
		sv.waitperiod.set(SPACES);
		sv.bentrm.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.poltyp.set(SPACES);
		sv.dialdownoption.set(SPACES);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.benCessAge.set(ZERO);
		sv.benCessTerm.set(ZERO);
		sv.linstamt.set(ZERO);
		sv.taxamt01.set(ZERO);
		sv.taxamt02.set(ZERO);
		sv.sumin.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaSumin.set(ZERO);
		wsaaOldPrem.set(ZERO);
		wsaaOldSumins.set(ZERO);
		wsaaPremDiff.set(ZERO);
		wsaaSuminsDiff.set(ZERO);
		wsaaTaxamt.set(ZERO);
		wsaaProtax.set(ZERO);
		wsaaIndex.set(ZERO);
		wsaaAnnuity = "N";
		wsaaSuspAvail.set(ZERO);
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.benCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		wsaaPcesageStore.set(ZERO);
		wsaaRcesageStore.set(ZERO);
		wsaaBcesageStore.set(ZERO);
		wsaaPcestermStore.set(ZERO);
		wsaaRcestermStore.set(ZERO);
		wsaaBcestermStore.set(ZERO);
		wsaaFirstTaxCalc.set("Y");
		sv.statcode.set(SPACES);//BRD-009
		wsaaPrevSumins.set(ZERO);
		sv.cashvalarer.set(ZERO);//ICIL-299
		wsaaCashValArr.set(ZERO);//ICIL-299
		//IBPLIFE-2135 Start
//		if(covrprpseFlag){
//			sv.effdatex.set(varcom.vrcmMaxDate);
//			sv.trancd.set(SPACE);
//			sv.covrprpse.set(SPACE);
//			sv.validflag.set("1");
//		}
		//IBPLIFE-2135 End
	}

protected void contractHeader1020()
	{
		/* Retrieve contract header information.*/
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*BRD-306 START */
		sv.cnttype.set(chdrpf.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		loadingFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		/*BRD-306 END */
		stampDutyflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.zstpduty01Out[varcom.nd.toInt()].set("N");
			if(isNE(covrpf.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
				sv.zstpduty01.set(covrpf.getZstpduty01());
			}
			
		}
		datcon2rec.intDate1.set(chdrpf.getOccdate());
		datcon2rec.intDate2.set(chdrpf.getOccdate());
		if (addComp.isTrue()) {
			while ( !(isGT(datcon2rec.intDate2, wsspcomn.currfrom))) {
				getNextEffdate1400();
			}
			
			wsaaPrevDue.set(wsaaTempPrev);
			wsaaNextDue.set(datcon2rec.intDate2);
		}
		if (addComp.isTrue() && !covrprpseFlag) {
			sv.effdate.set(wsspcomn.currfrom);
		}
		else {
			//sv.effdate.set(chdrpf.getOccdate());
			sv.effdate.set(chdrpf.getPtdate());//ICIL-560
		}

	}
//IBPLIFE-2135 Start
protected void checkCoverPurpose1022(){
//	
//	sv.trancd.set(wsaaBatckey.batcBatctrcde);
//	descpf=descDAO.getdescData("IT", "T1688", wsaaBatckey.batcBatctrcde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
//    if (null!=descpf) {
//        sv.trandesc.set(descpf.getLongdesc());
//    }
//    else{
//    	 sv.trandesc.set("?");
//    }
//		if (isEQ(wsaaToday, 0)) {
//		    datcon1rec.function.set(varcom.tday);
//		    Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
//		    wsaaToday.set(datcon1rec.intDate);
//		}
//		sv.effdatex.set(wsaaToday);
		
		if (modifyComp.isTrue() || compEnquiry.isTrue()  || compApp.isTrue()) {
			Covppf covppf = covppfDAO.getCovppfCrtable(covtmjaIO.getChdrcoy().toString(), 
					covtmjaIO.getChdrnum().toString(), covtmjaIO.getCrtable().toString(),
					covtmjaIO.getCoverage().toString(),covtmjaIO.getRider().toString());
			if(covppf != null){
				sv.effdatex.set(wsaaToday);
				sv.covrprpse.set(covppf.getCovrprpse());
				sv.trancd.set(covppf.getTranCode());
				descpf=descDAO.getdescData("IT", "T1688", sv.trancd.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
			    if (null!=descpf) {
			        sv.trandesc.set(descpf.getLongdesc());
			    }
			    else{
			    	 sv.trandesc.set(SPACE);
			    }
			   }
		}
}
//IBPLIFE-2135 End
/**
 * fwang3 ICIL-560
 */
private void processCashValue() {
	chinaLocalPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM007", appVars, "IT");
	if(chinaLocalPermission) {
		List<Itempf> items = itemDAO.getItemsbyItemcodeLikeOperator("IT", wsspcomn.company.toString(), tablesInner.th528.toString(), covrpf.getCrtable());//IJTI-1410
		if (items.isEmpty()) {
			sv.cashvalarerOut[varcom.nd.toInt()].set("Y");
			hasCashValue = false;
		} else {
			loadTh528(items);
			hasCashValue = true;
		}
	} else {
		sv.cashvalarerOut[varcom.nd.toInt()].set("Y");
	}
}

/**
 * @author fwang3
 * ICIL-560
 * @param items
 */
private void loadTh528(List<Itempf> items) {
	String item4parts = covrpf.getCrtable() + "" + covrpf.getAnbAtCcd() + "" + covrpf.getMortcls()	+ "" + covrpf.getSex();
	String item2parts = covrpf.getCrtable() + "" + covrpf.getAnbAtCcd() + "**";
	for (Itempf item : items) {
		if ((new Integer(covrpf.getCrrcd()).compareTo(new Integer(item.getItmfrm().toString()))) >= 0)
		{
			if (item.getItemitem().equals(item4parts)) {
				th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
				return;
			}
			if (item.getItemitem().equals(item2parts)) {
				th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
				return;
			}
			if (item.getItemitem().equals(covrpf.getCrtable() + "****")) {
				th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
				return;
			}
		}
	}
}

protected void contractCoverage1030()
	{
		/* Retrieve the coverage details saved from either p5131 (modify/*/
		/*   enquiry) or p5128 (add).*/
	 
				 //ILB-456 
			covrpf = covrpfDAO.getCacheObject(covrpf);
			if(null==covrpf) {
				covrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, covrmjaIO);
				if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(covrmjaIO.getParams());
					fatalError600();
			}
			else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		processCashValue();//fwang3 ICIL-560
		covrIO.setChdrcoy(covrpf.getChdrcoy());
		covrIO.setChdrnum(covrpf.getChdrnum());
		covrIO.setLife(covrpf.getLife());
		covrIO.setCoverage(covrpf.getCoverage());
		covrIO.setRider("00");
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		/** TSD-308 Start **/
		//TODO:		
		if(isFeaAllowed){
			caluclateTransactionDate();	
			calculateMonthiversary();
		}
		if (addComp.isTrue() && !covrprpseFlag) {
			/*if (isFeaAllowed) {
				wsspcomn.currfrom.set(wsaaMnthVers);
			}*/
			sv.effdate.set(wsspcomn.currfrom);
		}
		else {
			//sv.effdate.set(covrpf.getCurrfrom());
			sv.effdate.set(chdrpf.getPtdate());//ICIL-560
		}		

		/*  Read T2240 for age definition                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRMJA-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				} //MTL002
			}
		}
		/*  Read TR52D for Taxcode.                                        */
		sv.taxamt01Out[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamt01Out[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		readTh5061600();
		/* Read the PAYR record to get the Billing Details.                */
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setPayrseqno(covrpf.getPayrseqno());
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		wsaaCrtable.set(covrpf.getCrtable());
		/* Release the coverage details for processing of Whole Plan.*/
		/*   i.e. If entering on an action of Component ADD then the*/
		/*        Component to be added must exist for all policies*/
		/*        within the plan.*/
		/*   i.e. If entering on an action of Component Modify/Enquiry*/
		/*        and Whole Plan selected then we will process the*/
		/*        component for all policies within the plan.*/
		if (isEQ(wsaaPlanSuffix, ZERO)) {
			covrpfDAO.deleteCacheObject(covrpf);
		}
		/*    If modify of Component requested and the component           */
		/*    in question is a Single premium Component then we move 'y'   */
		/*    to comp enquiry and spaces to comp modify and an error       */
		/*    informing the user that single premium component cannot      */
		/*    be modified.                                                 */
		if (modifyComp.isTrue()
		&& isEQ(payrIO.getBillfreq(), ZERO)) {
			/*    MOVE 'I'                 TO WSAA-FLAG                <005>*/
			wsccSingPremFlag.set("Y");
		}
		if (addComp.isTrue()
		&& isEQ(th506rec.indic, "Y")) {
			loadSusp1500();
		}
	}

protected void headerToScreen1040()
	{
		/* Load the screen header title.*/
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		/* The following will centre the title.*/
		loadHeading1100();
		/* The following lines check whether a window to Annuity        */
		/* Details is required by checking the coverage against         */
		/* T6625.                                                       */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		/* Check that the record is either found or at EOF              */
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/* Check if the BEGN point is the correct record                */
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t6625, itdmIO.getItemtabl())
		|| isNE(covrpf.getCrtable(), itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		/* Check if the record is on the table                          */
		getAnny8000();
		if (isEQ(itdmIO.getStatuz(), varcom.oK)) {
			sv.anntindOut[varcom.nd.toInt()].set("N");
			wsaaAnnuity = "Y";
			if (isEQ(annyIO.getStatuz(), varcom.mrnf)
			&& addComp.isTrue()) {
				sv.anntind.set("X");
				sv.anntindOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.anntind.set("+");
			}
		}
		else {
			sv.anntindOut[varcom.nd.toInt()].set("Y");
			sv.anntindOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void lifeDetails1050()
	{
		/* LIFE ASSURED AND JOINT LIFE DETAILS*/
		/* To obtain the life assured and joint-life details (if any) do*/
		/* the following;-*/
		/*  - Read  the life details using LIFEMJA (life number from*/
		/*    COVTMJA, joint life number '00').  Look up the name*/
		/*    from the  client  details  (CLTS)  and  format as a*/
		/*    "confirmation name".*/
		//ILB-456
		lifepf = lifepfDAO.getLifeRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
		if (lifepf == null) {
			fatalError600();
		}

		/* Save Main Life details within Working Storage for later use.*/
		/*IF  ADD-COMP                                                 */
		/*    MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1           */
		/*    MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2           */
		/*    MOVE '01'                   TO DTC3-FREQUENCY            */
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC                    */
		/*    ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD       */
		/*ELSE                                                         */
		/*    MOVE LIFEMJA-ANB-AT-CCD     TO WSAA-ANB-AT-CCD.          */
		/*    Chdrmja-btdate is always used to calculate the age of life   */
		/*    assured.                                                     */
		/*    MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1          <015>*/
		/*MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2          <015>*/
		/*    MOVE PAYR-BTDATE            TO DTC3-INT-DATE-2          <036>*/
		/*    MOVE '01'                   TO DTC3-FREQUENCY           <015>*/
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC                   <015>*/
		/*    ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB-AT-CCD      <015>*/
		calcAge1200();
		wsaaAnbAtCcd.set(wsaaWorkingAnb);
		if (!addComp.isTrue()) {
			wsaaAnbAtCcd.set(lifepf.getAnbAtCcd());
		}
		wsaaCltdob.set(lifepf.getCltdob());
		wsaaSex.set(lifepf.getCltsex());
		/* Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		/* MOVE LIFEMJA-CHDRCOY        TO CLTS-CLNTCOY.                 */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifepf.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifepf.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		stampDutyflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(isNE(cltsIO.getCltaddr05(), SPACES)&& stampDutyflag){
			stateCode=cltsIO.getCltaddr05().toString();
		}
		if(isNE(cltsIO.getStatcode(), SPACES)&& (incomeProtectionflag || premiumflag)){
			occuptationCode=cltsIO.getStatcode().toString();
		}

		/*  - read the joint life details using LIFEMJA (life number*/
		/*    from COVTMJA,  joint  life number '01').  If found,*/
		/*    look up the name from the client details (CLTS) and*/
		/*    format as a "confirmation name".*/
		//ILB-456
		lifeList = lifepfDAO.getLifeData(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");

		if(lifeList.isEmpty()){
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbAnbAtCcd.set(0);
			wsaaLifeind.set("S");
			wsbbSex.set(SPACES);
			goTo(GotoLabel.setScreen1060);
		}
		else {
			wsaaLifeind.set("J");
		}
		/*    IF  ADD-COMP                                                 */
		/*        MOVE LIFEMJA-CLTDOB         TO DTC3-INT-DATE-1           */
		/*MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2           */
		/*        MOVE PAYR-BTDATE            TO DTC3-INT-DATE-2      <036>*/
		/*        MOVE '01'                   TO DTC3-FREQUENCY            */
		/*        CALL 'DATCON3' USING DTC3-DATCON3-REC                    */
		/*        ADD .99999 DTC3-FREQ-FACTOR GIVING WSBB-ANB-AT-CCD       */
		/*    ELSE                                                         */
		/*        MOVE LIFEMJA-ANB-AT-CCD     TO WSBB-ANB-AT-CCD.          */
		if (addComp.isTrue()) {
			calcAge1200();
			wsbbAnbAtCcd.set(wsaaWorkingAnb);
		}
		else {
			wsbbAnbAtCcd.set(lifepf.getAnbAtCcd());
		}
		wsbbCltdob.set(lifepf.getCltdob());
		wsbbSex.set(lifepf.getCltsex());
		/* Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		/* MOVE LIFEMJA-CHDRCOY        TO CLTS-CLNTCOY.                 */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifepf.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jlifcnum.set(lifepf.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void setScreen1060()
	{
		/* Set up the screen header details which are constant for all*/
		/*   policies within the plan.*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.numpols.set(chdrpf.getPolinc());
		sv.life.set(lifepf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		/*MOVE CHDRMJA-CNTCURR        TO SR576-CURRCD.                 */
		sv.currcd.set(payrIO.getCntcurr());
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		if (isEQ(covrpf.getPlanSuffix(), ZERO)
		&& !addComp.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			covrpf.setPlanSuffix(9999);
			covtmjaIO.setPlanSuffix(9999);
		//	covrmjaIO.setFunction(varcom.begn);
			covrpfList=covrpfDAO.selectCovrRecord(covrpf);
			covtmjaIO.setFunction(varcom.begn);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanproc.set("Y");
			/*****    IF ADD-COMP                                               */
			/*****       NEXT SENTENCE                                          */
			/*****       MOVE ZEROES           TO COVRMJA-COVERAGE              */
			/*****                                COVRMJA-RIDER                 */
			/*****    ELSE                                                      */
			/*****       NEXT SENTENCE                                          */
		}
		else {
			wsaaPlanproc.set("N");
			covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
			covtmjaIO.setFunction(varcom.readr);
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		
		dialdownFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		premiumflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(incomeProtectionflag || premiumflag || dialdownFlag)
		{
			callReadRCVDPF();
		}

		if(!incomeProtectionflag){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
			sv.poltypOut[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.waitperiodOut[varcom.nd.toInt()].set("N");
			sv.bentrmOut[varcom.nd.toInt()].set("N");
			sv.poltypOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("") && isEQ(sv.waitperiod,SPACES)){
					sv.waitperiod.set(rcvdPFObject.getWaitperiod());
				}
				if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("") && isEQ(sv.poltyp,SPACES)){
					sv.poltyp.set(rcvdPFObject.getPoltyp());
				}
				if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("") && isEQ(sv.bentrm,SPACES)){
					sv.bentrm.set(rcvdPFObject.getBentrm());
				}
			}
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
					}
				}
		}
		//BRD-NBP-011 ends
		exclFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP020", appVars, "IT");//IJTI-1410
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}
		datcon2rec.intDate1.set(chdrpf.getOccdate());
		datcon2rec.intDate2.set(chdrpf.getOccdate());
		if (addComp.isTrue()) {
			while ( !(isGT(datcon2rec.intDate2, wsspcomn.currfrom))) {
				getNextEffdate1400();
			}
			
			wsaaPrevDue.set(wsaaTempPrev);
			wsaaNextDue.set(datcon2rec.intDate2);
		}
		if (addComp.isTrue()) {
			sv.effdate.set(wsspcomn.currfrom);
		}
		else {
		//	sv.effdate.set(chdrpf.getOccdate());
			sv.effdate.set(chdrpf.getPtdate());//ICIL-560
		}
		
		/* If this is a component modify, check whether this component  */
		/* is subject to Automatic Increases (CPI-DATE holds a valid    */
		/* date).  If so, display a message stating that future         */
		/* increases will not be offered.                               */
		if (modifyComp.isTrue()
		&& isNE(covrpf.getCpiDate(), ZERO)
		&& isNE(covrpf.getCpiDate(), varcom.vrcmMaxDate)) {
			scrnparams.errorCode.set(errorsInner.f862);
		}
		/* The following 5000-section relates to the main coverage details*/
		/*   for each individual policy and as such will initially be load*/
		/*   -ed from here. But, if the Whole Plan has been selected or*/
		/*   Adding the Component then the 5000-section will be repeated*/
		/*   in the !000-section to load each policy.*/
		policyLoad5000();
		//BRD-009-STARTS
		occFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP05", appVars, "IT");
		if((occFlag) &&
		isEQ(t5687rec.premmeth,"PM30")){
			rcvdPFObject= new Rcvdpf();
			callReadRCVDPF();
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getStatcode()!=null && !rcvdPFObject.getStatcode().trim().equals("") ){
					sv.statcode.set(rcvdPFObject.getStatcode());
				}
			}
		}
		else{
			sv.statcodeOut[varcom.nd.toInt()].set("Y");
		}
		//BRD-009-ENDS
		/* If the benefit is a WOP & uses 'SUM' then do not display        */
		/* the benefit amount and description on screen....                */
		/*    PERFORM 9300-CHECK-T5602.                               <052>*/
		if (isNE(tr52drec.txcode, SPACES)
		&& isEQ(t5687rec.bbmeth, SPACES)) {
			sv.taxamt01Out[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamt01Out[varcom.pr.toInt()].set("N");
			sv.taxindOut[varcom.pr.toInt()].set("N");
			a200CheckCalcTax();
		}
		sv.taxamt01Out[varcom.pr.toInt()].set("Y");
		sv.instPremOut[varcom.pr.toInt()].set("Y");
		//IBPLIFE-2135 Start
		if(covrprpseFlag){
			checkCoverPurpose1022();
		}
		//IBPLIFE-2135 End
	}
protected void checkExcl()
{
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getCrtable(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
}
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
	rcvdPFObject.setChdrnum(covrpf.getChdrnum());
	rcvdPFObject.setLife(covrpf.getLife());
	rcvdPFObject.setCoverage(covrpf.getCoverage());
	rcvdPFObject.setRider(covrpf.getRider());
	rcvdPFObject.setCrtable(covrpf.getCrtable());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}
protected void loadHeading1100()
	{
		try {
			loadScreen1110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1110()
	{
		/* Count the number of spaces at the end of the line*/
		/*   (this is assuming there are none at the beginning)*/
		wsaaHeading.set(SPACES);
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		/* VARYING WSAA-X           FROM 24 BY -1                    */
		/*SUBTRACT WSAA-X             FROM 24 GIVING WSAA-Y.           */
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/* WSAA-X is the size of the heading string*/
		/* WSAA-Y is the number of spaces in the front*/
		/* WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1130();
		}
		/* WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1190);
	}

protected void moveChar1130()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void calcAge1200()
	{
		init1210();
	}

protected void init1210()
	{
		/* Routine to calculate Age next/nearest/last birthday             */
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(lifepf.getCltdob());
		agecalcrec.intDate2.set(payrIO.getBtdate());
		/* MOVE PAYR-BTDATE            TO AGEC-INT-DATE-2.         <050>*/
		/*    MOVE CHDRMJA-OCCDATE        TO AGEC-INT-DATE-2.      <CAS1.0>*/
		/* TSD-308 START */
		//TODO:		
		if (isFeaAllowed) {
			caluculateAnniversarydate(); 
			
			agecalcrec.intDate2.set(wsaaLastAnnv);	
			if (addComp.isTrue()) {	
				agecalcrec.intDate2.set(wsaaMnthVers);
			}
			caluclateTransactionDate(); //ILIFE-2366
			agecalcrec.intDate2.set(datcon1rec.intDate); //ILIFE-2366
		}
		/* TSD-308 END */
		wsaaAnb.set(lifepf.getAnbAtCcd());
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaWorkingAnb.set(agecalcrec.agerating);		
}
/** TSD-308 Start **/
protected void caluclateTransactionDate(){
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	if (isNE(datcon1rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon1rec.datcon1Rec);
		syserrrec.statuz.set(datcon1rec.statuz);
		fatalError600();
	}
	
}

protected void calculateMonthiversary(){
	datcon3rec.intDate1.set(chdrpf.getOccdate());
	datcon3rec.intDate2.set(datcon1rec.intDate);
	datcon3rec.frequency.set("12");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	ZonedDecimalData var = new ZonedDecimalData(11, 5).init(0);
	compute(var).set(add(datcon3rec.freqFactor, .99999));

	datcon2rec.intDate1.set(covrIO.getCrrcd());
	datcon2rec.frequency.set("12");
	datcon2rec.freqFactor.set(var);
	
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	wsaaMnthVers.set(datcon2rec.intDate2);

}
protected void caluculateAnniversarydate(){
	
	datcon4rec.datcon4Rec.set(SPACES);
	datcon4rec.frequency.set("01");
	datcon4rec.freqFactor.set(001);                               
	datcon4rec.intDate1.set(covrIO.getCurrfrom());                       
	datcon4rec.billmonth.set(covrIO.getCurrfrom().toStringRaw().substring(4, 6));
	datcon4rec.billday.set(covrIO.getCurrfrom().toStringRaw().substring(6, 8));
	while ( !(isGT(datcon4rec.intDate2,wsaaMnthVers))) {
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		wsaaLastAnnv.set(datcon4rec.intDate1);
		datcon4rec.intDate1.set(datcon4rec.intDate2);
	}	
}
/** TSD-308 End **/
protected void setupBonus1300()
	{
		para1300();
	}

protected void para1300()
	{
		/* Check if Coverage/Rider is a SUM product. If not SUM product    */
		/* protect field.                                                  */
		/* If SUM product and default exists setup BAPPMETH and protect    */
		/* field. If SUM product and default not setup allow entry.        */
		sv.bappmethOut[varcom.pr.toInt()].set(SPACES);
		sv.bappmethOut[varcom.nd.toInt()].set(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covtmjaIO.getChdrcoy());
		itemIO.setItemitem(covtmjaIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
			return ;
		}
		t6005rec.t6005Rec.set(itemIO.getGenarea());
		if (isEQ(t6005rec.ind, "1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void getNextEffdate1400()
	{
		/*GET-EFFDATE*/
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set(chdrpf.getBillfreq());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaTempPrev.set(datcon2rec.intDate1);
		datcon2rec.intDate1.set(datcon2rec.intDate2);
		wsaaFreq.add(1);
		/*EXIT*/
	}

protected void loadSusp1500()
	{
		begn1510();
	}

protected void begn1510()
	{
		r100ReadCvuw();
		if (isEQ(cvuwIO.getStatuz(), varcom.oK)) {
			sv.effdate.set(cvuwIO.getEffdate());
		}
		cprpIO.setRecKeyData(SPACES);
		cprpIO.setChdrpfx(chdrpf.getChdrpfx());
		cprpIO.setChdrcoy(chdrpf.getChdrcoy());
		cprpIO.setChdrnum(chdrpf.getChdrnum());
		cprpIO.setLife("99");
		cprpIO.setCoverage("00");
		cprpIO.setRider("00");
		cprpIO.setFunction("READR ");
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)
		&& isNE(cprpIO.getStatuz(), varcom.mrnf))//ILIFE-8096
 {
			syserrrec.params.set(cprpIO.getParams());
			fatalError600();
		}
		wsaaSuspAvail.set(cprpIO.getInstprem());
	}

protected void readTh5061600()
	{
		begn1600();
	}

protected void begn1600()
	{
		/*    Read table TH506 to determine, whether the component         */
		/*    add anytime has been enabled for this contract type          */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.th506);
		itemIO.setItemitem(chdrpf.getCnttype());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*                                                         <V4L005>*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			if (isNE(itemIO.getItemitem(), "***")) {
				itemIO.setItemitem("***");
				itemIO.setFormat(formatsInner.itemrec);
				itemIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, itemIO);
				if (isNE(itemIO.getStatuz(), varcom.oK)
				&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
					syserrrec.params.set(itemIO.getParams());
					fatalError600();
				}
			}
		}
		/*                                                         <V4L005>*/
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			th506rec.th506Rec.set(itemIO.getGenarea());
		}
		else {
			th506rec.th506Rec.set(SPACES);
		}
	}

protected void r100ReadCvuw()
	{
		/*R100-START*/
		cvuwIO.setRecKeyData(SPACES);
		cvuwIO.setRecNonKeyData(SPACES);
		cvuwIO.setChdrcoy(chdrpf.getChdrcoy());
		cvuwIO.setChdrnum(chdrpf.getChdrnum());
		cvuwIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(), varcom.oK)
		&& isNE(cvuwIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cvuwIO.getParams());
			fatalError600();
		}
		/*R100-EXIT*/
	}

protected void proratePremium1600()
	{
		prorate1610();
	}

protected void prorate1610()
	{
		/*    CALCULATE THE FRACTION IN THE DUE BETWEEN PREV & NEXT DUE.   */
		datcon3rec.intDate1.set(wsspcomn.currfrom);
		datcon3rec.intDate2.set(wsaaNextDue);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaDay1.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(wsaaPrevDue);
		datcon3rec.intDate2.set(wsaaNextDue);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaDay2.set(datcon3rec.freqFactor);
		compute(wsaaProrate, 6).set(div(wsaaDay1, wsaaDay2));
		/*    MAY BE MORE THAN 1 DUE INVOLVED WHEN YOU CONSIDER FROM       */
		/*    MAY BE MORE THAN 1 DUE INVOLVED                              */
		if (isLT(wsaaNextDue, chdrpf.getBtdate())) {
			wsaaFreq.set(ZERO);
			datcon2rec.intDate1.set(wsaaNextDue);
			datcon2rec.intDate2.set(wsaaNextDue);
			while ( !(isGT(datcon2rec.intDate2, chdrpf.getBtdate()))) {
				getNextEffdate1400();
			}
			
			compute(wsaaProrate, 6).set(sub(add(wsaaProrate, wsaaFreq), 1));
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preStart()
	{
	if (isEQ(wsspcomn.sbmaction,"D") || isEQ(wsspcomn.sbmaction,"C") ) { //Ticket #ILIFE-7244
		scrnparams.function.set(varcom.prot);
	}
		/* Skip this section  if  returning  from  an optional selection   */
		/* (current stack position action flag = '*').                     */
		/*   i.e. returning from Options and Extras data screen.           */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/* IF SINGLE-PREMIUM                                       <005>*/
		/*    MOVE F008                TO SCRN-ERROR-CODE.         <005>*/
		/* Test the POVR file for the existance of a Premium Breakdown     */
		/* record. If one exists then allow the option to display the      */
		/* select window for enquiry....if not then protect.               */
		/* Test here so that the latest POVR is retreived when either      */
		/* returning from a selection or having calculated new values.     */
		readPovr9000();
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(), wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(), wsaaPovrLife)
		|| isNE(povrIO.getCoverage(), wsaaPovrCoverage)
		|| isNE(povrIO.getRider(), wsaaPovrRider)) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
		}
		stampDutyflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
		}else{
			if(isNE(covtmjaIO.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
				sv.zstpduty01.set(covtmjaIO.getZstpduty01());
			}
		}
		sv.statcodeOut[varcom.pr.toInt()].set("Y");//BRD-009
        if (CSMIN003Permission &&
                ((isEQ(wsaaBatckey.batcBatctrcde, t6a7) ||isEQ(wsaaBatckey.batcBatctrcde, t6a8)))) {
            sv.fuindOut[varcom.nd.toInt()].set("N");
            sv.fuindOut[varcom.pr.toInt()].set("N");
        }
        else {
            sv.fuind.set("+");
            sv.fuindOut[varcom.nd.toInt()].set("Y");
            sv.fuindOut[varcom.pr.toInt()].set("Y");
        }
	}

	/**
	* <pre>
	* Check the date terms for error and enable correction by         
	* re-setting the date fields. The dates are then re-set           
	* based on chanded age or term fields...                          
	**** IF      SR576-RCESDTE-OUT(PR)    = 'Y'                  <052>
	****    AND (   SR576-RCESDTE-ERR NOT = SPACES               <052>
	****         OR SR576-RCESDTE-ERR NOT = SPACES               <052>
	****         OR SR576-RCESDTE-ERR NOT = SPACES )             <052>
	****            MOVE VRCM-MAX-DATE TO SR576-PREM-CESS-DATE   <052>
	****                                  SR576-RISK-CESS-DATE   <052>
	****                                  SR576-BEN-CESS-DATE.   <052>
	*****MOVE 'SUMIN   '          TO SCRN-POSITION-CURSOR.            
	* </pre>
	*/
protected void callScreenIo2010()
	{
		/* Display the screen.                                             */
		wsaaOldSumins.set(sv.sumin);
		wsaaPrevSumins.set(sv.sumin);
		wsaaOldPrem.set(sv.instPrem);
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
					validate2020();
				case cont2030: 
					cont2030();
				case beforeExit2080: 
					beforeExit2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'SR576IO' USING        SCRN-SCREEN-PARAMS               */
		/*                                SR576-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If CF11 (KILL) is requested*/
		/* then skip the remainder of the validation.*/
		wsaaIfSuminsChanged.set("N");
		wsaaIfPremChanged.set("N");
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if(RegpayFlag){
			if(compApp.isTrue()){
				if(isNE(covrpf.getSumins(),sv.sumin))
					CheckT6625();
			}
			if(isNE(wsaaPrevSumins,sv.sumin)){
				if(isEQ(wsaaBatckey.batcBatctrcde,"T555"))
					CheckT6625();
			}
		}
		/* If Component Enquiry then skip validation.                      */
		/*    IF  COMP-ENQUIRY                                     <V4L004>*/
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.cont2030);
		}
		/* Check the situation where the date fields are protected but     */
		/* age and term fields differ from the last call to the premium    */
		/* calculation routine. Blank the dates if ages or terms have      */
		/* changed hence ensuring a re-calculation of dates & premium.     */
		if (isEQ(sv.rcesdteOut[varcom.pr.toInt()], "Y")
		&& (isNE(sv.premCessAge, wsaaPcesageStore)
		|| isNE(sv.riskCessAge, wsaaRcesageStore)
		|| isNE(sv.benCessAge, wsaaBcesageStore)
		|| isNE(sv.premCessTerm, wsaaPcestermStore)
		|| isNE(sv.riskCessTerm, wsaaRcestermStore)
		|| isNE(sv.benCessTerm, wsaaBcestermStore))) {
			sv.premCessDate.set(varcom.vrcmMaxDate);
			sv.riskCessDate.set(varcom.vrcmMaxDate);
			sv.benCessDate.set(varcom.vrcmMaxDate);
		}
	}

protected void validate2020()
	{
		/* If  in  enquiry  mode,  skip  all field validation EXCEPT the*/
		/* options/extras indicator.*/
		/*    IF NOT COMP-ENQUIRY                                          */
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			validateAllowedCovergaeDuration();
			editCoverage2100();
			if(NBPRP056Permission) {
				validateOccupationOrOccupationClass();
			}
		}
	}

protected void validateAllowedCovergaeDuration(){
	
}

/**
 * fwang3 ICIL-560
 */
private void processCashValueInArrears() {
	// fwang3 ICIL-560 judge if the component has cash value and than code is come from submenu
	if (chinaLocalPermission && isEQ(wsaaFlag, "M") && hasCashValue && isEQ("T6A8", wsaaBatckey.batcBatctrcde)) {
		if (isLT(sv.sumin, wsaaCashValArr)) {
			sv.suminErr.set(errorsInner.rrj4);// ICIL-299
		}
		sv.cashvalarer.set(ZERO);
		if (isGT(sv.sumin, wsaaHoldSumin)) {
			int years = DateUtils.calYears(chdrpf.getOccdate().toString(), chdrpf.getPtdate().toString());
			PackedDecimalData data = new PackedDecimalData(11, 5).init(100000);
			compute(sv.cashvalarer,2).setRounded(mult(div(th528rec.insprm[years + 1], data), sub(sv.sumin, wsaaHoldSumin)));//fix ICIL-706
		}
	}
}

protected void cont2030()
	{
		/* If  options/extras already exist, there will be a '+' in this*/
		/*   field. A request  to access the details is made by entering*/
		/*   'X'. No other  values  (other  than  blank) are allowed. If*/
		/*   options and extras  are  requested,  DO  NOT  CALCULATE THE*/
		/*   PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		/* Check the premium breakdown indicator.                          */
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind, "+")
		&& isNE(sv.pbind, "X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		/* Check the Taxcode indicator.                                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		/* If  reassurance   already exists, there will be a '+' in this   */
		/* field.  A  request  to access the details is made by entering   */
		/* 'X'.  No  other  values  (other  than  blank) are allowed.      */
		/* IF SR576-RATYPIND           NOT = ' ' AND            <R96REA>*/
		/*                             NOT = '+' AND            <R96REA>*/
		/*                             NOT = 'X'                <R96REA>*/
		/*    MOVE G620                TO SR576-RATYPIND-ERR.   <R96REA>*/
		/*                                                      <R96REA>*/
		/* If the item selected prompts Annuity details, 'X' will       */
		/* occur here. If details exist '+' will occur. No other        */
		/* values are allowed.                                          */
		if (isNE(sv.anntind, " ")
		&& isNE(sv.anntind, "+")
		&& isNE(sv.anntind, "X")) {
			sv.anntindErr.set(errorsInner.g620);
		}
		if (exclFlag && isNE(sv.exclind, " ")
		&& isNE(sv.exclind, "+")
		&& isNE(sv.exclind, "X")) {
			sv.exclindErr.set(errorsInner.g620);
		}
		if(isEQ(sv.dialdownoptionErr,"E034") && !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		/* If Component Enquiry then skip validation.                      */
		/*    IF  COMP-ENQUIRY                                     <V4L004>*/
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.beforeExit2080);
		}
		/* If sum assured is changed and reassurance details exist force   */
		/* to go onto the reassuranc screen,to check if sum assured is     */
		/* still greater then the amount to be reassured ..                */
		/*                                                      <R96REA>*/
		/* IF MODIFY-COMP                                       <R96REA>*/
		/*  IF SR576-RATYPIND               = '+'               <R96REA>*/
		/*    IF SR576-SUMIN           NOT = WSAA-OLD-SUMINS    <R96REA>*/
		/*       MOVE 'X'              TO SR576-RATYPIND.       <R96REA>*/
		/*                                                      <R96REA>*/
		if (isNE(sv.sumin, wsaaOldSumins)) {
			compute(wsaaSuminsDiff, 0).set(sub(sv.sumin, wsaaOldSumins));
			if (isLT(wsaaSuminsDiff, 0)) {
				compute(wsaaSuminsDiff, 0).set(mult(wsaaSuminsDiff, -1));
				wsaaIfSuminsChanged.set("D");
			}
			else {
				wsaaIfSuminsChanged.set("I");
			}
		}
		if (isNE(sv.instPrem, wsaaOldPrem)) {
			compute(wsaaPremDiff, 2).set(sub(sv.instPrem, wsaaOldPrem));
			if (isLT(wsaaPremDiff, 0)) {
				compute(wsaaPremDiff, 2).set(mult(wsaaPremDiff, -1));
				wsaaIfPremChanged.set("D");
			}
			else {
				wsaaIfPremChanged.set("I");
			}
		}
		processCashValueInArrears();// fwang3 ICIL-560
		/* If everything else is O-K calculate the Installment Premium.*/
		if (isNE(sv.optextind, "X")
		&& isNE(sv.anntind, "X")
		&& isEQ(sv.errorIndicators, SPACES)
		&& addComp.isTrue()) {
			calcPremium2200();
		}
		if (isNE(sv.optextind, "X")
		&& isNE(sv.comind, "X")
		&& isNE(sv.anntind, "X")
		&& isEQ(sv.errorIndicators, SPACES)
		&& modifyComp.isTrue()) {
			/*AND (WSAA-PREM-CHANGED OR WSAA-SUMINS-CHANGED)               */
			calcPremium2200();
		}
	}
protected void CheckT6625(){
	itempf = new Itempf();

	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t6625);
	itempf.setItemitem(covrpf.getCrtable());//IJTI-1410
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
	itempfList = itemDAO.findByItemDates(itempf);
	if(itempfList.size() > 0) {
		ChangeRegp();
	}
}
protected void ChangeRegp(){
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	regppfList = regpDAO.readRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), sv.life.toString(), sv.coverage.toString(), sv.rider.toString());//IJTI-1410
	if (regppfList != null && regppfList.size() > 0) {
	for(Regppf regpobj : regppfList){
		if(isEQ(regpobj.getValidflag(),"1")){
			regpIO.setNonKey(SPACES);
			regpIO.setFormat(regprec);
			regpIO.setChdrcoy(chdrpf.getChdrcoy());
			regpIO.setChdrnum(chdrpf.getChdrnum());
			regpIO.setLife(sv.life);
			regpIO.setCoverage(sv.coverage);
			regpIO.setRider(sv.rider);
			regpIO.setValidflag("1");
			regpIO.setRgpynum(regpobj.getRgpynum());

			regpIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, regpIO);
			if (isEQ(regpIO.getStatuz(), varcom.mrnf)) {
				return;
			}
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			regpsubrec.subCompany.set(chdrpf.getChdrcoy());
			regpsubrec.subBatctrcde.set(wsaaBatckey.batcBatctrcde);
			compute(wsaaKeepSumins,2).set(div(mult(sv.sumin,regpobj.getPrcnt()),100));
			if(isLTE(regpIO.getNextPaydate(),datcon1rec.intDate)){
				if(isGT(regpIO.getFinalPaydate(),datcon1rec.intDate) && isGT(regpIO.getRevdte(),datcon1rec.intDate)){
					read6693Action();
					if(itempfList1.size() > 0) {
						wsaaT6696Crtable.set(regpIO.getCrtable());
						wsaaT6696Cltype.set(regpIO.getPayreason());
						itempf.setItempfx("IT");
						itempf.setItemtabl(t6696);
						itempf.setItemcoy(chdrpf.getChdrcoy().toString());
						itempf.setItemitem(wsaaT6696Key.toString());
						itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
						itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
						itempfList = itemDAO.findByItemDates(itempf);
						if(itempfList.size() > 0) {
							itempf = itempfList.get(0);
							t6696rec.t6696Rec.set(StringUtil.rawToString(itempf.getGenarea()));
							regpIO.setPymt(wsaaKeepSumins.toString());
							regpIO.setFunction(varcom.keeps);
							SmartFileCode.execute(appVars, regpIO);
							if (isNE(regpIO.getStatuz(), varcom.oK)) {
								syserrrec.params.set(regpIO.getParams());
								fatalError600();
							}
							callProgram(t6696rec.inxsbm, regpsubrec.batcsubRec);
							if (isNE(regpsubrec.subStatuz, varcom.oK)) {
									releaseregp();
									return ;
								}
								/*  Retrieve the REGP record to obtain indexation changes*/
								regpIO.setFunction(varcom.retrv);
								SmartFileCode.execute(appVars, regpIO);
								if (isNE(regpIO.getStatuz(), varcom.oK)) {
									regpsubrec.subStatuz.set(regpIO.getStatuz());
									releaseregp();
									return ;
								}
								/*    Rewrite the existing REGP with the validflag set to '2'*/
								ChangeAcctoFreq();
									regpobj.setValidflag("2");
									regpDAO.updateRegpValidFlag(regpobj);
									Regppf regp = new Regppf(regpobj);
									regp.setValidflag("1");
									regp.setPymt(regpIO.getPymt().getbigdata());
									regpList = new ArrayList<>();
									regpList.add(regp);
									regpDAO.insertRegppfRecord(regpList);
								releaseregp();
							}
						}
					}
				}
			}
		}
	}
}
protected void read6693Action(){
	/*Read T6693 to check if the selected record has a valid action*/
	wsaaT6693Paystat.set(regpIO.getRgpystat());
	wsaaT6693Crtable.set(regpIO.getCrtable());
	itempf=new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t6693);
	itempf.setItemitem(wsaaT6693Key.toString());
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
	itempfList1 = itemDAO.findByItemDates(itempf);
}

protected void ChangeAcctoFreq(){
	if(isEQ(regpIO.regpayfreq,"12")){
		compute(wsaaKeepSumins,2).set(div(regpIO.getPymt(),12));
		regpIO.setPymt(wsaaKeepSumins.toString());
	}
	if(isEQ(regpIO.regpayfreq,"02")){
		compute(wsaaKeepSumins,2).set(div(regpIO.getPymt(),2));
		regpIO.setPymt(wsaaKeepSumins.toString());
	}
	if(isEQ(regpIO.regpayfreq,"04")){
		compute(wsaaKeepSumins,2).set(div(regpIO.getPymt(),4));
		regpIO.setPymt(wsaaKeepSumins.toString());
	}
	if(isEQ(regpIO.regpayfreq,"26")){
		compute(wsaaKeepSumins,2).set(div(regpIO.getPymt(),26));
		regpIO.setPymt(wsaaKeepSumins.toString());
	}
}
protected void releaseregp(){
	regpIO.setFunction(varcom.rlse);
	SmartFileCode.execute(appVars, regpIO);
	if (isNE(regpIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(regpIO.getParams());
		fatalError600();
	}
}

protected void beforeExit2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* If 'CALC' was entered then re-display the screen.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void editCoverage2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkSumin2110();
				case checkSuminCont2526: 
					checkSuminCont2526();
				case checkRcessFields2120: 
					checkRcessFields2120();
					checkPcessFields2130();
					checkBcessFields2135();
					checkAgeTerm2140();
				case validDate2140: 
					validDate2140();
				case datesCont2140: 
					datesCont2140();
				case ageAnniversary2140: 
					ageAnniversary2140();
				case check2140: 
					check2140();
				case checkOccurance2140: 
					checkOccurance2140();
				case checkTermFields2150: 
					checkTermFields2150();
				case checkComplete2160: 
					checkComplete2160();
				case checkMortcls2170: 
					checkMortcls2170();
				case loop2175: 
					loop2175();
				case checkLiencd2180: 
					checkLiencd2180();
				case loop2185: 
					loop2185();
				case checkMore2190: 
					checkMore2190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkSumin2110()
	{
		a100CheckLimit();
		/* Check sum insured ranges.*/
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkRcessFields2120);
		}
		if (isEQ(sv.sumin, covrpf.getSumins())) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkSuminCont2526);
		}
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin, chdrpf.getPolinc()), chdrpf.getPolsum())));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
	}

protected void checkSuminCont2526()
	{
		if (isLT(wsaaSumin, t5606rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		/*IF WSAA-SUMIN              < T5606-SUM-INS-MIN               */
		/*   MOVE E417                TO SR576-SUMIN-ERR.              */
		if (isGT(wsaaSumin, t5606rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

protected void checkRcessFields2120()
	{
		/* Check the consistency of the risk age and term fields and*/
		/*   premium age and term fields. Either, risk age and premium*/
		/*   age must be used or risk term and premium term must be used.*/
		/*   They must not be combined.*/
		/* Note that these only need validating if they were not defaulted*/
		/*     NOTE: Age and Term fields may now be mixed.                 */
		if (isEQ(sv.select, "J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()) {
			checkDefaults5300();
		}
		if (isGT(sv.riskCessAge, 0)
		&& isGT(sv.riskCessTerm, 0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.riskCessAge, 0)
		&& isEQ(sv.riskCessTerm, 0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2130()
	{
		if (isGT(sv.premCessAge, 0)
		&& isGT(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)
		&& isEQ(sv.rcessageErr, SPACES)
		&& isEQ(sv.rcesstrmErr, SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkBcessFields2135()
	{
		if (isGT(sv.benCessAge, 0)
		&& isGT(sv.benCessTerm, 0)) {
			sv.bcessageErr.set(errorsInner.f220);
			sv.bcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.benCessAge, 0)
		&& isEQ(sv.benCessTerm, 0)
		&& isEQ(sv.rcessageErr, SPACES)
		&& isEQ(sv.rcesstrmErr, SPACES)) {
			sv.benCessAge.set(sv.riskCessAge);
			sv.benCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.benCessAge, 0)
		&& isEQ(sv.benCessTerm, 0)) {
			sv.bcessageErr.set(errorsInner.e560);
			sv.bcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2140()
	{
		if ((isNE(sv.rcessageErr, SPACES))
		|| (isNE(sv.rcesstrmErr, SPACES))
		|| (isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.bcessageErr, SPACES))
		|| (isNE(sv.bcesstrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2170);
		}
		/*    IF SR576-RISK-CESS-AGE      > 0 AND                          */
		/*       SR576-PREM-CESS-AGE      = 0                              */
		/*       MOVE F224                TO SR576-PCESSAGE-ERR            */
		/*       IF NOT DEFAULT-RA                                         */
		/*          MOVE F224             TO SR576-RCESSAGE-ERR.           */
		/*    IF SR576-RISK-CESS-TERM     > 0 AND                          */
		/*       SR576-PREM-CESS-TERM     = 0                              */
		/*       MOVE F225                TO SR576-PCESSTRM-ERR            */
		/*       IF NOT DEFAULT-RT                                         */
		/*          MOVE F225             TO SR576-RCESSTRM-ERR.           */
		if ((isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.rcessageErr, SPACES))
		|| (isNE(sv.rcesstrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2170);
		}
		/* To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (T5606).*/
		/*    IF T5606-EAAGE              NOT = SPACES                     */
		/*    OR SR576-RISK-CESS-DATE     = VRCM-MAX-DATE                  */
		/*       PERFORM 5400-RISK-CESS-DATE                               */
		/*    ELSE                                                         */
		/*       IF SR576-RCESDTE-ERR     = SPACES                         */
		/*          IF SR576-RISK-CESS-AGE NOT = 0                         */
		/*             MOVE WSZZ-CLTDOB   TO DTC2-INT-DATE-1               */
		/*             MOVE SR576-RISK-CESS-AGE                            */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*             PERFORM 5500-CALL-DATCON2                           */
		/*                IF DTC2-INT-DATE-2 < SR576-RISK-CESS-DATE        */
		/*                   MOVE U029    TO SR576-RCESSAGE-ERR            */
		/*                                   SR576-RCESDTE-ERR             */
		/*                ELSE                                             */
		/*                   SUBTRACT 1   FROM DTC2-FREQ-FACTOR            */
		/*                   PERFORM 5500-CALL-DATCON2                     */
		/*                   IF DTC2-INT-DATE-2 NOT < SR576-RISK-CESS-DATE */
		/*                      MOVE U029 TO SR576-RCESSAGE-ERR            */
		/*                                   SR576-RCESDTE-ERR             */
		/*                   ELSE                                          */
		/*                      NEXT SENTENCE                              */
		/*          ELSE                                                   */
		/*             IF SR576-RISK-CESS-TERM NOT = 0                     */
		/*                IF NOT ADD-COMP                                  */
		/*                   MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1   */
		/*                ELSE                                             */
		/*                   MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1   */
		/*                MOVE SR576-RISK-CESS-TERM                        */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*                PERFORM 5500-CALL-DATCON2                        */
		/*                   IF DTC2-INT-DATE-2 < SR576-RISK-CESS-DATE     */
		/*                      MOVE U029 TO SR576-RCESSTRM-ERR            */
		/*                                   SR576-RCESDTE-ERR             */
		/*                   ELSE                                          */
		/*                      SUBTRACT 1 FROM DTC2-FREQ-FACTOR           */
		/*                      PERFORM 5500-CALL-DATCON2                  */
		/*                      IF DTC2-INT-DATE-2                         */
		/*                                   NOT < SR576-RISK-CESS-DATE    */
		/*                         MOVE U029 TO SR576-RCESSTRM-ERR         */
		/*                                      SR576-RCESDTE-ERR.         */
		/*    IF T5606-EAAGE              NOT = SPACES                     */
		/*    OR SR576-PREM-CESS-DATE     = VRCM-MAX-DATE                  */
		/*       PERFORM 5450-PREM-CESS-DATE                               */
		/*    ELSE                                                         */
		/*       IF SR576-PCESDTE-ERR     = SPACES                         */
		/*          IF SR576-PREM-CESS-AGE NOT = 0                         */
		/*             MOVE WSZZ-CLTDOB   TO DTC2-INT-DATE-1               */
		/*             MOVE SR576-PREM-CESS-AGE                            */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*             PERFORM 5500-CALL-DATCON2                           */
		/*                IF DTC2-INT-DATE-2 < SR576-PREM-CESS-DATE        */
		/*                   MOVE U029    TO SR576-PCESSAGE-ERR            */
		/*                                   SR576-PCESDTE-ERR             */
		/*                ELSE                                             */
		/*                   SUBTRACT 1   FROM DTC2-FREQ-FACTOR            */
		/*                   PERFORM 5500-CALL-DATCON2                     */
		/*                   IF DTC2-INT-DATE-2 NOT < SR576-PREM-CESS-DATE */
		/*                      MOVE U029 TO SR576-PCESSAGE-ERR            */
		/*                                   SR576-PCESDTE-ERR             */
		/*                   ELSE                                          */
		/*                      NEXT SENTENCE                              */
		/*          ELSE                                                   */
		/*             IF SR576-PREM-CESS-TERM NOT = 0                     */
		/*                IF NOT ADD-COMP                                  */
		/*                   MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1   */
		/*                ELSE                                             */
		/*                   MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1   */
		/*                MOVE SR576-PREM-CESS-TERM                        */
		/*                                TO DTC2-FREQ-FACTOR              */
		/*                PERFORM 5500-CALL-DATCON2                        */
		/*                   IF DTC2-INT-DATE-2 < SR576-PREM-CESS-DATE     */
		/*                      MOVE U029 TO SR576-PCESSTRM-ERR            */
		/*                                   SR576-PCESDTE-ERR             */
		/*                   ELSE                                          */
		/*                      SUBTRACT 1   FROM DTC2-FREQ-FACTOR         */
		/*                      PERFORM 5500-CALL-DATCON2                  */
		/*                      IF DTC2-INT-DATE-2                         */
		/*                                NOT < SR576-PREM-CESS-DATE       */
		/*                         MOVE U029 TO SR576-PCESSTRM-ERR         */
		/*                                      SR576-PCESDTE-ERR.         */
		/* IF SR576-RISK-CESS-DATE = VRCM-MAX-DATE                 <026>*/
		riskCessDate5400();
		/* END-IF.                                                 <026>*/
		/* IF SR576-PREM-CESS-DATE = VRCM-MAX-DATE                 <026>*/
		premCessDate5450();
		/* END-IF.                                                 <026>*/
		/* IF SR576-BEN-CESS-DATE  = VRCM-MAX-DATE                 <051>*/
		benCessDate5470();
		/* END-IF.                                                 <051>*/
		if (isNE(sv.rcesdteErr, SPACES)
		|| isNE(sv.pcesdteErr, SPACES)
		|| isNE(sv.bcesdteErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if (isEQ(sv.premCessDate, varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isEQ(sv.benCessDate, varcom.vrcmMaxDate)) {
			sv.benCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate, sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		if (isEQ(sv.rider, "00")) {
			goTo(GotoLabel.datesCont2140);
		}
		wsaaStorePlanSuffix.set(covrpf.getPlanSuffix());
		/* Read the Temporary COVT file for the Coverage record which must */
		/* have been written before a rider can be selected.               */
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setCoverage(covrpf.getCoverage());
		covtmjaIO.setRider("00");
		covtmjaIO.setSeqnbr(ZERO);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(covtmjaIO.getLife(), covrpf.getLife())
		|| isNE(covtmjaIO.getCoverage(), covrpf.getCoverage())
		|| isNE(covtmjaIO.getRider(), "00")) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtmjaIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.validDate2140);
		}
		/* Read the Temporary COVT file for the Coverage record which must */
		/* have been written before a rider can be selected.               */
		/* if a policy that was summarised was selected a                  */
		/* the plan-suffix was in correct (fell over because               */
		/* could not find a record with that plan-suffix)                  */
		/* need to access the summaried reccord (plan-suffix of 0)         */
		if (modifyComp.isTrue()) {
			if (isEQ(chdrpf.getPolsum(), ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isGT(chdrpf.getPolsum(), covrpf.getPlanSuffix())
				|| isEQ(chdrpf.getPolsum(), covrpf.getPlanSuffix())) {
					covrpf.setPlanSuffix(0);
				}
			}
		}
		if (addComp.isTrue()) {
			/*    MOVE ZERO                TO COVRMJA-PLAN-SUFFIX      <041>*/
			/*    IF CHDRMJA-POLSUM         = ZEROS  AND               <041>*/
			/*       CHDRMJA-POLINC         = 1                        <041>*/
			/*       MOVE ZERO             TO COVRMJA-PLAN-SUFFIX      <041>*/
			/*    ELSE                                                 <041>*/
			/*       IF CHDRMJA-POLINC      > 1                        <041>*/
			/*       MOVE 1                TO COVRMJA-PLAN-SUFFIX.     <041>*/
			if (isEQ(chdrpf.getPolsum(), ZERO)
			&& isNE(chdrpf.getPolinc(), 1)) {
				covrpf.setPlanSuffix(1);
			}
			else {
				covrpf.setPlanSuffix(0);
			}
		}
		covrpf.setRider("00");
		//ILB-456
		covrpf=covrpfDAO.getCovrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),"1");
		if (covrpf == null) {
			fatalError600();
		}
		covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
		covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
		covtmjaIO.setBenCessDate(covrpf.getBenCessDate());
	}

protected void validDate2140()
	{
		/* Validate that the riders Risk and Premium Cessation dates are   */
		/* not greater than the Coverages to which it is attached. A rider */
		/* can not mature after its driving coverage.                      */
		if (isGT(sv.riskCessDate, covtmjaIO.getRiskCessDate())) {
			sv.rcesdteErr.set(errorsInner.h033);
		}
		if (isGT(sv.premCessDate, covtmjaIO.getPremCessDate())) {
			sv.pcesdteErr.set(errorsInner.h044);
		}
		if (isGT(sv.benCessDate, covtmjaIO.getBenCessDate())) {
			sv.bcesdteErr.set(errorsInner.d030);
		}
		/* Re-Read the Rider record in order to position the file and data */
		/* in the correct position for the information to be processed.    */
		/* (in regard to the covt file)                                    */
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(sv.life);
		covtmjaIO.setCoverage(sv.coverage);
		covtmjaIO.setRider(sv.rider);
		covtmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		/* Re-Read the Rider record in order to position the file and data */
		/* in the correct position for the information to be processed.    */
		/* (in regard to the  covr file).....                              */
		covrpf.setChdrcoy(covtmjaIO.getChdrcoy().toString());
		covrpf.setChdrnum(covtmjaIO.getChdrnum().toString());
		covrpf.setLife(sv.life.toString());
		covrpf.setCoverage(sv.coverage.toString());
		covrpf.setRider(sv.rider.toString());
		covrpf.setPlanSuffix(wsaaStorePlanSuffix.toInt());
		//ILIFE-8143
		covrpfList = covrpfDAO.searchCovrpfRecord(covrpf);
		if (covrpfList.isEmpty()) {
				goTo(GotoLabel.datesCont2140);
		}
		covrpf = covrpfList.get(0);
	}

protected void datesCont2140()
	{
		/* If modify mode and the record being modified is the coverage    */
		/* check that the cessation dates are not less than those on the   */
		/* riders.                                                         */
		/* Once the check has been completed re read the COVR to ensure    */
		/* the correct record details are retained                         */
		if (isEQ(sv.rider, "00")
		&& modifyComp.isTrue()) {
			covrrgwIO.setParams(SPACES);
			covrrgwIO.setChdrcoy(covrpf.getChdrcoy());
			covrrgwIO.setChdrnum(covrpf.getChdrnum());
			covrrgwIO.setLife(covrpf.getLife());
			covrrgwIO.setCoverage(covrpf.getCoverage());
			covrrgwIO.setRider(covrpf.getRider());
			covrrgwIO.setPlanSuffix(covrpf.getPlanSuffix());
			covrrgwIO.setFormat(formatsInner.covrrgwrec);
			covrrgwIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, covrrgwIO);
			if (isNE(covrrgwIO.getStatuz(), varcom.oK)
			&& isNE(covrrgwIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(covrrgwIO.getParams());
				syserrrec.statuz.set(covrrgwIO.getStatuz());
				fatalError600();
			}
			while ( !(isEQ(covrrgwIO.getStatuz(), varcom.endp))) {
				riderCessation2140();
			}
			
		}
		if (isNE(sv.rcesdteErr, SPACES)
		|| isNE(sv.pcesdteErr, SPACES)
		|| isNE(sv.bcesdteErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2170);
		}
		/*  Calculate cessasion age and term.*/
		/* MOVE WSZZ-CLTDOB            TO DTC3-INT-DATE-1.              */
		/* MOVE SR576-RISK-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-RISK-CESS-AGE.           */
		/* MOVE SR576-PREM-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-AGE.           */
		/* MOVE SR576-BEN-CESS-DATE    TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-BEN-CESS-AGE.            */
		/* MOVE CHDRMJA-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* MOVE SR576-RISK-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-RISK-CESS-TERM.          */
		/* MOVE SR576-PREM-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2300-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-TERM.          */
		/* MOVE SR576-BEN-CESS-DATE    TO DTC3-INT-DATE-2.      <CAS1.0>*/
		/* PERFORM 2300-CALL-DATCON3.                           <CAS1.0>*/
		/*                                                      <CAS1.0>*/
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-BEN-CESS-TERM.   <CAS1.0>*/
		if (isEQ(t5606rec.eaage, "A")) {
			goTo(GotoLabel.ageAnniversary2140);
		}
		if (isEQ(sv.riskCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		if (isEQ(sv.benCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32300();
			wszzBenCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzBenCessAge.set(sv.benCessAge);
		}
		goTo(GotoLabel.check2140);
	}

protected void ageAnniversary2140()
	{
		if (isEQ(sv.riskCessAge, ZERO)) {
			/*       MOVE PAYR-BTDATE           TO DTC3-INT-DATE-1             */
			datcon3rec.intDate1.set(chdrpf.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			compute(wszzRiskCessAge, 5).set(add(datcon3rec.freqFactor, wsaaAnb));
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			/*       MOVE PAYR-BTDATE           TO DTC3-INT-DATE-1     <V4L005>*/
			datcon3rec.intDate1.set(chdrpf.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			compute(wszzPremCessAge, 5).set(add(datcon3rec.freqFactor, wsaaAnb));
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		if (isEQ(sv.benCessAge, ZERO)) {
			/*       MOVE PAYR-BTDATE           TO DTC3-INT-DATE-1     <V4L005>*/
			datcon3rec.intDate1.set(chdrpf.getOccdate());
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32300();
			compute(wszzBenCessAge, 5).set(add(datcon3rec.freqFactor, wsaaAnb));
		}
		else {
			wszzBenCessAge.set(sv.benCessAge);
		}
	}

protected void check2140()
	{
		/* When calculating SR576-RISK-CESS-DATE, it checks to see if      */
		/* the transaction is add or modify and uses different dates       */
		/* accordingly. We must do the same here.                          */
		if (addComp.isTrue()) {
			/*        MOVE PAYR-BTDATE        TO DTC3-INT-DATE-1       <V4L005>*/
			datcon3rec.intDate1.set(chdrpf.getOccdate());
		}
		else {
			datcon3rec.intDate1.set(covrpf.getCrrcd());
		}
		if (isEQ(sv.riskCessTerm, ZERO)
		|| addComp.isTrue()) {
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm, ZERO)
		|| addComp.isTrue()) {
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		if (isEQ(sv.benCessTerm, ZERO)
		|| addComp.isTrue()) {
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32300();
			wszzBenCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzBenCessTerm.set(sv.benCessTerm);
		}
		/* Assume the dates are invalid until proved otherwise.*/
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.bcessageErr.set(errorsInner.d028);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		sv.bcesstrmErr.set(errorsInner.d029);
		x.set(0);
	}

protected void checkOccurance2140()
	{
		/* Check each possible option.*/
		x.add(1);
		if (isGT(x, wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2160);
		}
		if ((isEQ(t5606rec.ageIssageFrm[x.toInt()], 0)
		&& isEQ(t5606rec.ageIssageTo[x.toInt()], 0))
		|| (isLT(wszzAnbAtCcd, t5606rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5606rec.ageIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkTermFields2150);
		}
		if (isGTE(wszzRiskCessAge, t5606rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge, t5606rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge, t5606rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge, t5606rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
		if (isGTE(wszzBenCessAge, t5606rec.benCessageFrom[x.toInt()])
		&& isLTE(wszzBenCessAge, t5606rec.benCessageTo[x.toInt()])) {
			sv.bcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2150()
	{
		if ((isEQ(t5606rec.termIssageFrm[x.toInt()], 0)
		&& isEQ(t5606rec.termIssageTo[x.toInt()], 0))
		|| (isLT(wszzAnbAtCcd, t5606rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5606rec.termIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkOccurance2140);
		}
		if (isGTE(wszzRiskCessTerm, t5606rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm, t5606rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm, t5606rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm, t5606rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzBenCessTerm, t5606rec.benCesstermFrm[x.toInt()])
		&& isLTE(wszzBenCessTerm, t5606rec.benCesstermTo[x.toInt()])) {
			sv.bcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2140);
	}

protected void checkComplete2160()
	{
		if (isNE(sv.rcesstrmErr, SPACES)
		&& isEQ(sv.riskCessTerm, ZERO)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr, SPACES)
		&& isEQ(sv.premCessTerm, ZERO)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.bcesstrmErr, SPACES)
		&& isEQ(sv.benCessTerm, ZERO)) {
			sv.bcesdteErr.set(sv.bcesstrmErr);
			sv.bcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr, SPACES)
		&& isEQ(sv.riskCessAge, ZERO)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr, SPACES)
		&& isEQ(sv.premCessAge, ZERO)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
		if (isNE(sv.bcessageErr, SPACES)
		&& isEQ(sv.benCessAge, ZERO)) {
			sv.bcesdteErr.set(sv.bcessageErr);
			sv.bcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2170()
	{
		/* Mortality-Class, if the mortality class appears on a coverage/*/
		/*   rider screen it is a compulsory field because it will be used*/
		/*   in calculating the premium amount. The mortality class*/
		/*   entered must one of the ones in the edit rules table.*/
		if (isEQ(sv.mortclsOut[varcom.pr.toInt()], "Y")) {
			goTo(GotoLabel.checkLiencd2180);
		}
		if(isEQ(sv.mortcls, SPACES)){
			sv.mortclsErr.set(errorsInner.e420);	
		}
		x.set(0);
	}

protected void loop2175()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2180);
		}
		if (isNE(t5606rec.mortcls[x.toInt()], sv.mortcls)) {
			goTo(GotoLabel.loop2175);
		}
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
		if(incomeProtectionflag)
		{
			if((isEQ(sv.waitperiod,SPACES)) && waitperiodFlag )
				sv.waitperiodErr.set(errorsInner.e186);
			
			if((isEQ(sv.bentrm,SPACES)) && bentrmFlag )
				sv.bentrmErr.set(errorsInner.e186);
			
			if((isEQ(sv.poltyp,SPACES)) && poltypFlag )
				sv.poltypErr.set(errorsInner.e186);
		
		}
		
		if(premiumflag)
		{
			if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
				sv.prmbasisErr.set(errorsInner.e186);
		}
	}
}
protected void checkIPPfields()
{
	boolean t5606Flag=false;
	for(int counter=1; counter<t5606rec.waitperiod.length;counter++){
		if(isNE(sv.waitperiod,SPACES)){
			if (isEQ(t5606rec.waitperiod[counter], sv.waitperiod)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.waitperiod,SPACES)){
		sv.waitperiodErr.set("RFUY");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.bentrm.length;counter++){
		if(isNE(sv.bentrm,SPACES)){
			if (isEQ(t5606rec.bentrm[counter], sv.bentrm)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.bentrm,SPACES)){
		sv.bentrmErr.set("RFUZ");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.poltyp.length;counter++){
		if(isNE(sv.poltyp,SPACES)){
			if (isEQ(t5606rec.poltyp[counter], sv.poltyp)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.poltyp,SPACES)){
		sv.poltypErr.set("RFV0");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.prmbasis.length;counter++){
		if(isNE(sv.prmbasis,SPACES)){
			if (isEQ(t5606rec.prmbasis[counter], sv.prmbasis)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.prmbasis,SPACES)){
		sv.prmbasisErr.set("RFV1");// new error code
	}
	
}

protected void checkLiencd2180()
	{
		/* Lien Code - Validate against the maximum.*/
		if (isEQ(sv.liencdOut[varcom.pr.toInt()], "Y")
		|| isEQ(sv.liencd, SPACES)) {
			goTo(GotoLabel.checkMore2190);
		}
		x.set(0);
		
	}

protected void loop2185()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2190);
		}
		if (isEQ(t5606rec.liencd[x.toInt()], SPACES)
		|| isNE(t5606rec.liencd[x.toInt()], sv.liencd)) {
			goTo(GotoLabel.loop2185);
		}
	}

protected void checkMore2190()
	{
		/* Check to see if BONUS APPLICATION METHOD is valid for           */
		/* coverage/rider.                                                 */
		if (isNE(sv.bappmeth, SPACES)
		&& isNE(sv.bappmeth, t6005rec.bappmeth01)
		&& isNE(sv.bappmeth, t6005rec.bappmeth02)
		&& isNE(sv.bappmeth, t6005rec.bappmeth03)
		&& isNE(sv.bappmeth, t6005rec.bappmeth04)
		&& isNE(sv.bappmeth, t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
			wsspcomn.edterror.set("Y");
		}
		/* Check joint life selection indicator (if applicable).*/
		if (isNE(sv.selectOut[varcom.pr.toInt()], "Y")) {
			if (isNE(sv.select, SPACES)
			&& isNE(sv.select, "J")
			&& isNE(sv.select, "L")) {
				/*       MOVE U028             TO SR576-SELECT-ERR.             */
				sv.selectErr.set(errorsInner.h093);
			}
		}
		/*Z0-EXIT*/
		if(incomeProtectionflag || premiumflag){
			checkIPPmandatory();
			checkIPPfields();//ILIFE-3438
		}
	}

protected void riderCessation2140()
	{
		para2140();
	}

protected void para2140()
	{
		/*  Check the riders for cessation dates greater than the          */
		/*  coverage cessation date. As soon as an invalid date is         */
		/*  found display error. On finding an error the user will         */
		/*  have to exit th coverage screen and change the riders          */
		/*  cessation dates before changing the coverage cessation dates   */
		if (isNE(covrrgwIO.getChdrcoy(), wsaaCovrChdrcoy)
		|| isNE(covrrgwIO.getChdrnum(), wsaaCovrChdrnum)
		|| isNE(covrrgwIO.getLife(), wsaaCovrLife)
		|| isNE(covrrgwIO.getCoverage(), wsaaCovrCoverage)) {
			covrrgwIO.setStatuz(varcom.endp);
			return ;
		}
		if (isGT(covrrgwIO.getRiskCessDate(), sv.riskCessDate)) {
			sv.rcesdteErr.set(errorsInner.h033);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isGT(covrrgwIO.getPremCessDate(), sv.premCessDate)) {
			sv.pcesdteErr.set(errorsInner.h044);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isNE(covrrgwIO.getBenCessDate(), varcom.vrcmMaxDate) && isGT(covrrgwIO.getBenCessDate(), sv.benCessDate)) {
			sv.bcesdteErr.set(errorsInner.d030);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrrgwIO.getStatuz(), varcom.endp)) {
			return ;
		}
		covrrgwIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrrgwIO);
		if (isNE(covrrgwIO.getStatuz(), varcom.oK)
		&& isNE(covrrgwIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrrgwIO.getParams());
			syserrrec.statuz.set(covrrgwIO.getStatuz());
			fatalError600();
		}
	}

protected void calcPremium2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2210();
					callSubr2220();
					adjustPrem2225();
				case cont: 
					cont();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2210()
	{
		/* If benefit billed, do not calculate premium*/
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.exit2290);
		}
		/* If there is no premium calculation method, set premium          */
		/* required field to YES.                                          */
		if (isEQ(t5675rec.premsubr, SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			wsaaPremStatuz.set("N");
		}
		/* PREMIUM CALCULATION*/
		/* The  premium amount is  required  on  all  products  and  all*/
		/* validating  must  be  successfully  completed  before  it  is*/
		/* calculated. If there is  no  premium  method defined (i.e the*/
		/* relevant code was blank), the premium amount must be entered.*/
		/* Otherwise, it is optional and always calculated.*/
		/* Note that a premium calculation subroutine may calculate the*/
		/* premium from the sum insured OR the sum insured from the*/
		/* premium.*/
		/*    IF PREM-REQD                NOT = 'N'                        */
		/*       IF SR576-INST-PREM       = 0                              */
		/*          MOVE U019             TO SR576-INSTPRM-ERR             */
		/*          GO TO 2290-EXIT.                                       */
		/* If a premium is required check if this premium > 0              */
		if (premReqd.isTrue()) {
			/*       IF SR576-INST-PREM       = 0                      <V4L004>*/
			/*       MOVE U019             TO SR576-INSTPRM-ERR        <022>*/
			if (isEQ(sv.linstamt, 0)) {
				/*          MOVE G818             TO SR576-INSTPRM-ERR     <V4L004>*/
				sv.linstamtErr.set(errorsInner.g818);
			}
			goTo(GotoLabel.exit2290);
		}
		/* If the premium is greater than zero and the user has modified   */
		/* a premium then skip premium calculation.                        */
		if (modifyComp.isTrue()
		&& userPremEntered.isTrue()
		&& premReqd.isTrue()) {
			/*   MOVE 'N'                 TO WSAA-PREM-STATUZ              */
			if (isGT(sv.instPrem, 0)) {
				goTo(GotoLabel.exit2290);
			}
		}
		/* If the premium is greater than zero and the user is modifying   */
		/* a component then skip premium calculation.                      */
		if (isGT(sv.instPrem, 0)
		&& premReqd.isTrue()) {
			if (wsaaPremChanged.isTrue()
			&& modifyComp.isTrue()) {
				wsaaPremStatuz.set("U");
				sv.instprmErr.set(errorsInner.f404);
				goTo(GotoLabel.exit2290);
			}
		}
		if (isGT(sv.instPrem, 0)
		&& premReqd.isTrue()) {
			if (!wsaaPremChanged.isTrue()) {
				goTo(GotoLabel.exit2290);
			}
		}
	}

protected void callSubr2220()
	{
		/* To calculate  it,  call  the  relevant calculation subroutine*/
		/*   worked out above passing:*/
		premiumrec.function.set("CALC");
		/*MOVE COVTMJA-CRTABLE        TO CPRM-CRTABLE.                 */
		/* MOVE COVRMJA-CRTABLE        TO CPRM-CRTABLE.            <041>*/
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if (isEQ(sv.select, "J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		/*    MOVE CHDRMJA-OCCDATE        TO CPRM-EFFECTDT.*/
		/*MOVE CHDRMJA-BTDATE         TO CPRM-EFFECTDT.                */
		premiumrec.effectdt.set(payrIO.getBtdate());
		if (addComp.isTrue()) {
			premiumrec.effectdt.set(wsspcomn.currfrom);
		}
		premiumrec.termdate.set(sv.premCessDate);
		/*MOVE CHDRMJA-CNTCURR        TO CPRM-CURRCODE.                */
		premiumrec.currcode.set(payrIO.getCntcurr());
		/*  (wsaa-sumin already adjusted for plan processing)*/
		/*IF   ADD-COMP                                                */
		/*     MOVE WSAA-SUMIN             TO CPRM-SUMIN               */
		/*ELSE                                                         */
		/*     IF  PLAN-LEVEL  AND                                     */
		/*         SR576-PLAN-SUFFIX NOT > CHDRMJA-POLSUM              */
		/*         COMPUTE WSAA-SUMIN ROUNDED                          */
		/*               = WSAA-SUMINS-DIFF * CHDRMJA-POLINC           */
		/*                 / CHDRMJA-POLSUM                            */
		/*         MOVE WSAA-SUMIN             TO CPRM-SUMIN           */
		/*    ELSE                                                    */
		/*         MOVE WSAA-SUMINS-DIFF       TO CPRM-SUMIN.          */
		/*IF CPRM-SUMIN               = ZERO                      <017>*/
		/*     IF OPTEXT-YES                                      <017>*/
		/*         MOVE WSAA-STORE-SUMIN        TO CPRM-SUMIN.    <017>*/
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		/*MOVE CHDRMJA-BILLFREQ       TO CPRM-BILLFREQ.                */
		/*MOVE CHDRMJA-BILLCHNL       TO CPRM-MOP                      */
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrpf.getOccdate());
		premiumrec.reRateDate.set(chdrpf.getOccdate());
		/*IF   ADD-COMP*/
		/*     MOVE SR576-INST-PREM        TO CPRM-CALC-PREM           */
		/*ELSE                                                         */
		/*     MOVE WSAA-PREM-DIFF         TO CPRM-CALC-PREM.          */
		premiumrec.calcPrem.set(sv.instPrem);
		premiumrec.calcBasPrem.set(sv.instPrem);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.cnttype.set(chdrpf.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.benCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaBenCessTerm.set(datcon3rec.freqFactor);
		premiumrec.benCessTerm.set(wsaaBenCessTerm);
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, chdrpf.getPolinc()), chdrpf.getPolsum())));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, chdrpf.getPolinc()), chdrpf.getPolsum())));
		}
		if (isEQ(wsaaAnnuity, "Y")) {
			getNewAnnuityDetails8100();
		}
		else {
			premiumrec.freqann.set(SPACES);
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		/* If the coverage uses the calculation package SUM and the        */
		/* annuity frequency is not being used then pass the benefit       */
		/* frequency to the calculation method using this field.           */
		/*    IF     WSAA-SUMFLAG NOT = SPACE                         <052>*/
		/*       AND CPRM-FREQANN     = SPACES                        <052>*/
		/*           MOVE T5606-BENFREQ   TO CPRM-FREQANN.            <052>*/
		/* If a component has a premium breakdown record (indicates also   */
		/* it is a SUM product) and it is being modified, then set the     */
		/* valid flag on the premium breakdown record to '2'. Note this    */
		/* must be done only the once & so set and check a flag.           */
		/* Note that as of 18/4/95 a function of 'MALT' will only apply    */
		/* to coverage's using SUM.                                        */
		if (modifyComp.isTrue()
		&& isEQ(sv.pbindOut[varcom.nd.toInt()], "N")) {
			premiumrec.function.set("MALT");
			if (isNE(wsaaPovrModified, "Y")) {
				vf2MaltPovr9400();
				wsaaPovrModified = "Y";
			}
		}
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{	
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			if(isNE(sv.waitperiod,SPACES)){
				premiumrec.waitperiod.set(sv.waitperiod);
			}
			if(isNE(sv.bentrm,SPACES)){
				premiumrec.bentrm.set(sv.bentrm);
			}
			if(isNE(sv.prmbasis,SPACES)){
				//brd-009-starts
				//premiumrec.prmbasis.set(sv.prmbasis);
				if(isEQ(sv.prmbasis,"S")){
					premiumrec.prmbasis.set("Y");
				}
				else{
				premiumrec.prmbasis.set("");
				}
			}
			//brd-009-ends
			if(isNE(sv.poltyp,SPACES)){
				premiumrec.poltyp.set(sv.poltyp);
			}
			//premiumrec.occpcode.set(occuptationCode);// TO BE MAPPED LATER GAURAV
			//brd-009-starts
			premiumrec.occpclass.set(SPACES);
			occupation	= lifepfDAO.getOccRecord(covrpf.getChdrcoy().trim(), covrpf.getChdrnum(),covrpf.getLife());//IJTI-1410
			if(isNE(occupation,SPACES)){
				premiumrec.occpcode.set(occupation);//IJTI-1410
			}
			else
				premiumrec.occpcode.set(cltsIO.getOccpcode().toString());
			//brd-009-ends
			if(isNE(sv.dialdownoption,SPACES)){
				if(sv.dialdownoption.toString().startsWith("0"))
					premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
				else
					premiumrec.dialdownoption.set(sv.dialdownoption);
			}
			else
				premiumrec.dialdownoption.set("100");
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			if (isEQ(premiumrec.statuz, varcom.bomb)) {
				syserrrec.params.set(premiumrec.premiumRec);
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			if (isNE(premiumrec.statuz, varcom.oK)) {
				sv.instprmErr.set(premiumrec.statuz);
				goTo(GotoLabel.exit2290);
			}
			if(stampDutyflag){
				sv.zstpduty01.set(ZERO);
				premiumrec.rstate01.set(stateCode.toUpperCase());
				premiumrec.rstate02.set(SPACES);
				premiumrec.zstpduty01.set(ZERO);
				premiumrec.zstpduty02.set(ZERO);
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("STAMPDUTY")))
				{
					callProgram("STAMPDUTY", premiumrec.premiumRec,vpxlextrec,vpxacblrec.vpxacblRec);
					sv.zstpduty01.set(premiumrec.zstpduty01);
					
					if (isNE(premiumrec.zstpduty01, ZERO)) {
						compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
					}
				}
			}
		}
		
		
		getOccupVpms2011CustomerSpecific();
		/*Ticket #IVE-792 - End*/
		//****Ticket #ILIFE-2005 end
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2290);
		}
		/* Store age and term fields used in the calculation to ensure     */
		/* any values stored on file match calculation parameters.         */
		wsaaPcesageStore.set(sv.premCessAge);
		wsaaRcesageStore.set(sv.riskCessAge);
		wsaaBcesageStore.set(sv.benCessAge);
		wsaaPcestermStore.set(sv.premCessTerm);
		wsaaRcestermStore.set(sv.riskCessTerm);
		wsaaBcestermStore.set(sv.benCessTerm);
	}

	/**
	* <pre>
	* Adjust premium calculated for plan processing.
	*****IF  NOT ADD-COMP                                        <021>
	*****    IF WSAA-SUMINS-DIFF    = ZERO                       <021>
	*****      IF OPTEXT-YES                                     <021>
	*****         GO TO 2225-ADJUST-PREM.                        <021>
	*****IF  MODIFY-COMP                                              
	*****    PERFORM 2800-ADJUST-SUMINS-PREM                          
	*****    GO TO 2290-EXIT.                                         
	* </pre>
	*/
protected void adjustPrem2225()
	{
		/* Adjust premium calculated for plan processing.*/
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, chdrpf.getPolsum()), chdrpf.getPolinc())));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, chdrpf.getPolsum()), chdrpf.getPolinc())));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, chdrpf.getPolsum()), chdrpf.getPolinc())));
		}
		/* Put possibly calculated sum insured back on the screen.*/
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(), ZERO)) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin, chdrpf.getPolsum()), chdrpf.getPolinc())));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		/*BRD-306 END */
		//BRD-009-STARTS
				sv.statcode.set(premiumrec.occpclass);
				if (isEQ(sv.statcode, "N")) {
						sv.statcodeErr.set(errorsInner.rfw4);
					}
				if (isEQ(sv.statcode, "R")) {
					sv.statcodeErr.set(errorsInner.rfw5);
				}
		//BRD-009-ENDS

		/*    To apply pro-rating on cprm-calc-prem                        */
		/*    rounding to the nearest cent.                                */
		sv.linstamt.set(premiumrec.calcPrem);
		if (isNE(tr52drec.txcode, SPACES)) {
			a200CheckCalcTax();
		}
		if (addComp.isTrue()) {
			proratePremium1600();
			/*       PERFORM 1700-READ-T5541                                   */
			compute(wsaaProprem, 6).set(mult(premiumrec.calcPrem, wsaaProrate));
			/*                                * WSAA-T5541-FACTOR              */
			wsaaTmpprem.set(wsaaProprem);
			if (isGTE(wsaaDecimal3, 5)) {
				wsaaTmpprem.add(0.01);
			}
			wsaaDecimal3.set(0);
			wsaaProprem.set(wsaaTmpprem);
			/*       COMPUTE CPRM-CALC-PREM =WSAA-PROPREM + 0.009999   <V74L01>*/
			premiumrec.calcPrem.set(wsaaProprem);
			if (isEQ(wsspcomn.currfrom, chdrpf.getBtdate())) {
				sv.instPrem.set(ZERO);
				premiumrec.calcPrem.set(ZERO);
			}
			else {
				sv.instPrem.set(premiumrec.calcPrem);
			}
			a250ProrateTax();
			/*      IF SR576-INST-PREM > WSAA-SUSP-AVAIL              <V74L01>*/
			if ((setPrecision(wsaaSuspAvail, 2)
			&& isGT((add(sv.instPrem, wsaaProtax)), wsaaSuspAvail))) {
				/*          MOVE F702         TO   SCRN-ERROR-CODE                 */
				sv.instprmErr.set(errorsInner.rl33);
			}
		}
		else {
			if (isEQ(sv.instPrem, 0)) {
				sv.instPrem.set(premiumrec.calcPrem);
				goTo(GotoLabel.exit2290);
			}
		}
		/* Having calculated it, the  entered value, if any, is compared*/
		/*   with it to check that it is within acceptable limits of the*/
		/*   automatically calculated figure. If it  is  less  than  the*/
		/*   amount calculated and within tolerance  then  the  manually*/
		/*   entered amount is allowed. If the entered value exceeds the*/
		/*   calculated one, the calculated value is used.*/
		if (isEQ(sv.instPrem, 0)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
		if (isGTE(sv.instPrem, premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
		/*    THIS SECTION CALCULATES THE NEW PREMIUM PAYMENT AND*/
		/*    COMPARES THE MANDATE AMOUT WITH IT.*/
		/*    THE FORMULA USED IS AS FOLLOWS.*/
		/*    NP = OP - (CP - SP).*/
		/*  WHERE :*/
		/*    NP = NEW PREMIUM (TOTAL)*/
		/*    OP = OLD PREMIUM (TOTAL)*/
		/*    CP = OLD SINGLE PREMIUM*/
		/*    SP = NEW SINGLE PREMIUM*/
		/*IF CHDRMJA-BILLCHNL         NOT = 'B'                   <018>*/
		/*   IF PAYR-BILLCHNL            NOT = 'B'                   <037>*/
		/*      GO TO CONT.                                          <037>*/
		if (isLTE(payrIO.getMandref(), SPACES)) {
			goTo(GotoLabel.cont);
		}
		/* IF CHDRMJA-PAYRNUM          NOT = SPACES             <CAS1.0>*/
		/*    MOVE CHDRMJA-PAYRNUM     TO MAND-PAYRNUM          <CAS1.0>*/
		/*    MOVE CHDRMJA-PAYRCOY     TO MAND-PAYRCOY          <CAS1.0>*/
		/*MOVE CHDRMJA-MANDREF     TO MAND-MANDREF             <018>*/
		/* ELSE                                                 <CAS1.0>*/
		/*    MOVE CHDRMJA-COWNNUM     TO MAND-PAYRNUM          <CAS1.0>*/
		/*    MOVE CHDRMJA-COWNCOY     TO MAND-PAYRCOY          <CAS1.0>*/
		/*MOVE CHDRMJA-MANDREF     TO MAND-MANDREF.            <018>*/
		/* END-IF.                                              <CAS1.0>*/
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		mandIO.setPayrcoy(clrfIO.getClntcoy());
		mandIO.setPayrnum(clrfIO.getClntnum());
		mandIO.setMandref(payrIO.getMandref());
		mandIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		if (isEQ(mandIO.getMandAmt(), ZERO)) {
			goTo(GotoLabel.cont);
		}
		if (isGT(chdrpf.getBillfreq(), 0)) {
			compute(wsaaCalcPrem, 2).set(sub(covrpf.getInstprem(), (sub(payrIO.getSinstamt06(), sv.instPrem))));
		}
		if (isNE(mandIO.getMandAmt(), wsaaCalcPrem)) {
			sv.instprmErr.set(errorsInner.ev01);
			wsspcomn.edterror.set("Y");
		}
	}

protected void cont()
	{
		/* check the tolerance amount against the table entry read above.*/
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, sv.instPrem));
		sv.instprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)
		|| isEQ(sv.instprmErr, SPACES)); wsaaSub.add(1)){
			searchForTolerance2240();
		}
		/* If the premium is within the tolerance limits, then we have to  */
		/* adjust the basic premium (the loaded premium will not change).  */
		if (isEQ(sv.instprmErr, SPACES)) {
			if(stampDutyflag){
				compute(sv.zbinstprem, 2).set(sub(sub(sv.instPrem, sv.zlinstprem),sv.zstpduty01));
			}else{
			compute(sv.zbinstprem, 2).set(sub(sv.instPrem, sv.zlinstprem));
		}
		}
		goTo(GotoLabel.exit2290);
	}

protected void searchForTolerance2240()
	{
		/* Calculate tolerance Limit and Check whether it is greater*/
		/*   than the maximum tolerance amount in table T5667.*/
		/*IF CHDRMJA-BILLFREQ         = T5667-FREQ (WSAA-SUB)          */
		if (isEQ(payrIO.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtol[wsaaSub.toInt()])), 100));
			if (isLTE(wsaaDiff, wsaaTol)
			&& isLTE(wsaaDiff, t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.instprmErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()], ZERO)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtoln[wsaaSub.toInt()])), 100));
				if (isLTE(wsaaDiff, wsaaTol)
				&& isLTE(wsaaDiff, t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.instprmErr.set(SPACES);
				}
			}
		}
	}

protected void callDatcon32300()
	{
		/*CALL*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	**2800-ADJUST-SUMINS-PREM SECTION.                                
	**2800-START.                                                     
	**Adjust premium calculated for plan processing.                  
	*****IF PLAN-LEVEL                                                
	*****    AND                                                      
	*****   SR576-PLAN-SUFFIX  NOT > CHDRMJA-POLSUM                   
	*****     AND                                                     
	*****   CHDRMJA-POLSUM         > ZERO                             
	*****   COMPUTE CPRM-CALC-PREM ROUNDED = (CPRM-CALC-PREM          
	*****                                    * CHDRMJA-POLSUM         
	*****                                    / CHDRMJA-POLINC).       
	**Put possibly calculated sum insured back on the screen.         
	*****IF PLAN-LEVEL                                                
	*****    AND                                                      
	*****   SR576-PLAN-SUFFIX  NOT > CHDRMJA-POLSUM                   
	*****     AND                                                     
	*****   CHDRMJA-POLSUM         > ZERO                             
	*****   COMPUTE CPRM-SUMIN    ROUNDED = (CPRM-SUMIN               
	*****                                    * CHDRMJA-POLSUM         
	*****                                    / CHDRMJA-POLINC).       
	*****IF  WSAA-SUMINS-INCREASED                                    
	*****    COMPUTE SR576-INST-PREM                                  
	*****            = WSAA-OLD-PREM + CPRM-CALC-PREM                 
	*****    COMPUTE SR576-SUMIN                                      
	*****            = WSAA-OLD-SUMINS      + CPRM-SUMIN              
	*****ELSE                                                         
	*****    COMPUTE SR576-INST-PREM                                  
	*****            = WSAA-OLD-PREM  - CPRM-CALC-PREM                
	*****    COMPUTE SR576-SUMIN                                      
	*****            = WSAA-OLD-SUMINS  - CPRM-SUMIN.                 
	**2890-EXIT.                                                      
	*****  EXIT.                                                      
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					lextpf3010CustomerSpecific();
					if(incomeProtectionflag || premiumflag || dialdownFlag){
					insertAndUpdateRcvdpf();
					}
					updateDatabase3010();
					//IBPLIFE-2135 Start
					if(covrprpseFlag){
					updateCoverPurpose3020();
					}
					//IBPLIFE-2135 End
				case updatePcdt3060: 
					updatePcdt3060();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
protected void insertAndUpdateRcvdpf(){
	boolean rcvdUpdateFlag=false;
if(rcvdPFObject!=null){
	rcvdPFObject.setChdrcoy(covtmjaIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtmjaIO.getChdrnum().toString());
	rcvdPFObject.setCoverage(covtmjaIO.getCoverage().toString());
	rcvdPFObject.setCrtable(covtmjaIO.getCrtable().toString());
	rcvdPFObject.setLife(covtmjaIO.getLife().toString());
	rcvdPFObject.setRider(covtmjaIO.getRider().toString());
	if(rcvdPFObject.getWaitperiod() != null){
		if (isNE(sv.waitperiod, rcvdPFObject.getWaitperiod())) {
			rcvdPFObject.setWaitperiod(sv.waitperiod.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getPoltyp() != null){
		if (isNE(sv.poltyp, rcvdPFObject.getPoltyp())) {
			rcvdPFObject.setPoltyp(sv.poltyp.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getPrmbasis()!=null){
		if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getBentrm()!=null){
		if (isNE(sv.bentrm, rcvdPFObject.getBentrm())) {
			rcvdPFObject.setBentrm(sv.bentrm.toString());
			rcvdUpdateFlag=true;
		}
	}
		//BRD-009-STARTS
	if(rcvdPFObject.getStatcode() != null){
		if (isNE(sv.statcode, rcvdPFObject.getStatcode())) {
			rcvdPFObject.setStatcode(sv.statcode.toString());
			rcvdUpdateFlag=true;
		}
	}
				//BRD-009-ENDS
		//BRD-NBP-011 starts
		if(rcvdPFObject.getDialdownoption()!=null){
		if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
			rcvdUpdateFlag=true;
		}
		}
		//BRD-NBP-011 ends
		if(rcvdUpdateFlag){
			rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
		}
	}else{
		rcvdPFObject=new Rcvdpf();
		rcvdPFObject.setChdrcoy(covtmjaIO.getChdrcoy().toString());
		rcvdPFObject.setChdrnum(covtmjaIO.getChdrnum().toString());
		rcvdPFObject.setCoverage(covtmjaIO.getCoverage().toString());
		rcvdPFObject.setCrtable(covtmjaIO.getCrtable().toString());
		rcvdPFObject.setLife(covtmjaIO.getLife().toString());
		rcvdPFObject.setRider(covtmjaIO.getRider().toString());

		if(isNE(sv.waitperiod,SPACES)){
			rcvdPFObject.setWaitperiod(sv.waitperiod.toString());
		}
		if(isNE(sv.poltyp,SPACES)){
			rcvdPFObject.setPoltyp(sv.poltyp.toString());
		}
		if(isNE(sv.prmbasis,SPACES)){
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
		}
		if(isNE(sv.bentrm,SPACES)){
			rcvdPFObject.setBentrm(sv.bentrm.toString());
		}
		rcvdPFObject.setUsrprf(covtmjaIO.getUserProfile().toString());
		rcvdPFObject.setJobnm(covtmjaIO.getJobName().toString());
		//rcvdpf.setDatime(covtrbnIO.getDatime());
		//BRD-009-STARTS
		if(isNE(sv.statcode,SPACES)){
			rcvdPFObject.setStatcode(sv.statcode.toString());
		}
		//BRD-009-STARTS
		//BRD-NBP-011 starts
		if(isNE(sv.dialdownoption,SPACES)){
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
		}
		//BRD-NBP-011 ends
		rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
	}
	
}
//IBPLIFE-2135 Start
protected void updateCoverPurpose3020(){
	Covtpf covt = covtpfDAO.getCovtlnbData(covtmjaIO.getChdrcoy().toString(), covtmjaIO.getChdrnum().toString(), sv.life.toString(),
			sv.coverage.toString(), sv.rider.toString());
	if(covt != null){
		Covppf covpp = covppfDAO.getCovppfCrtable(covtmjaIO.getChdrcoy().toString(), covtmjaIO.getChdrnum().toString(), covt.getCrtable()
				,covtmjaIO.getCoverage().toString(),covtmjaIO.getRider().toString());
		if(covpp == null){
			Covppf covp = new Covppf();
			covp.setChdrcoy(covtmjaIO.getChdrcoy().toString());
			covp.setChdrnum(covtmjaIO.getChdrnum().toString());
			covp.setCrtable(covt.getCrtable());
			covp.setEffdate(sv.effdatex.toString().trim().equals("")?wsaaToday.toInt():sv.effdatex.toInt());
			covp.setValidflag("1");
			covp.setChdrpfx("CH");
			covp.setCovrprpse(sv.covrprpse.toString());
			covp.setTranCode(sv.trancd.toString().trim().equals("")?wsaaBatckey.batcBatctrcde.toString():sv.trancd.toString());
			covp.setCoverage(sv.coverage.toString());
			covp.setRider(sv.rider.toString());
			covppfDAO.insertCovppfRecord(covp);
		}
		else{
			Covppf covp = new Covppf();
			covp.setChdrcoy(covtmjaIO.getChdrcoy().toString());
			covp.setChdrnum(covtmjaIO.getChdrnum().toString());
			covp.setCrtable(covt.getCrtable());
			covp.setEffdate(sv.effdatex.toString().trim().equals("")?wsaaToday.toInt():sv.effdatex.toInt());
			covp.setCovrprpse(sv.covrprpse.toString());
			covp.setCoverage(sv.coverage.toString());
			covp.setRider(sv.rider.toString());
			covppfDAO.updateCovppfCrtable(covp);
		}
	}
}
//IBPLIFE-2135 End
//updateExcl() removed as a part of PINNACLE-2855

protected void updateDatabase3010()
	{
		/* Skip this section  if  returning  from  an optional selection   */
		/* (current stack position action flag = '*').                     */
		/*   i.e. returning from Options and Extras data screen.           */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);
		}
		/* If termination of processing or enquiry go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3090);
		}
		/*    IF  COMP-ENQUIRY                                             */
		if (revComp.isTrue()) {
			deleteAnnt3400();
		}
		if (compEnquiry.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		if (compApp.isTrue()) {
			goTo(GotoLabel.updatePcdt3060);
		}
		/* If premium breakdown selected then exit....                     */
		if (isEQ(sv.pbind, "X")) {
			goTo(GotoLabel.exit3090);
		}
		setupCovtmja3100();
		updateCovtmja3200();
		if(isFollowUpRequired){
			createFollowUps3400();
		}
	}

protected void updatePcdt3060()
	{
		if (isEQ(wsaaUpdateFlag, "N")) {
			return ;
		}
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrpf.getChdrcoy(), pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), pcdtmjaIO.getChdrnum())
		|| isNE(sv.life, pcdtmjaIO.getLife())
		|| isNE(sv.coverage, pcdtmjaIO.getCoverage())
		|| isNE(sv.rider, pcdtmjaIO.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(), pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			defaultPcdt3800();
		}
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void setupCovtmja3100()
	{
		updateReqd3110();
	}

protected void updateReqd3110()
	{
		/* Check for changes in the COVTMJA Record.*/
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate, covtmjaIO.getRiskCessDate())) {
			covtmjaIO.setRiskCessDate(sv.riskCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate, covtmjaIO.getPremCessDate())) {
			covtmjaIO.setPremCessDate(sv.premCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessDate, covtmjaIO.getBenCessDate())) {
			covtmjaIO.setBenCessDate(sv.benCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge, covtmjaIO.getRiskCessAge())) {
			covtmjaIO.setRiskCessAge(sv.riskCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge, covtmjaIO.getPremCessAge())) {
			covtmjaIO.setPremCessAge(sv.premCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessAge, covtmjaIO.getBenCessAge())) {
			covtmjaIO.setBenCessAge(sv.benCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm, covtmjaIO.getRiskCessTerm())) {
			covtmjaIO.setRiskCessTerm(sv.riskCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm, covtmjaIO.getPremCessTerm())) {
			covtmjaIO.setPremCessTerm(sv.premCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessTerm, covtmjaIO.getBenCessTerm())) {
			covtmjaIO.setBenCessTerm(sv.benCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin, covtmjaIO.getSumins())) {
			covtmjaIO.setSumins(sv.sumin);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zbinstprem, covtmjaIO.getZbinstprem())) {
			covtmjaIO.setZbinstprem(sv.zbinstprem);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem, covtmjaIO.getZlinstprem())) {
			covtmjaIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covtmjaIO.setLoadper(sv.loadper);
			covtmjaIO.setRateadj(sv.rateadj);
			covtmjaIO.setFltmort(sv.fltmort);
			covtmjaIO.setPremadj(sv.premadj);
			covtmjaIO.setAgeadj(sv.adjustageamt);
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		/*IF CHDRMJA-BILLFREQ         = '00'                      <001>*/
		if (isEQ(payrIO.getBillfreq(), "00")) {
			/*       IF SR576-INST-PREM       NOT = COVTMJA-SINGP              */
			/*          MOVE SR576-INST-PREM  TO COVTMJA-SINGP                 */
			if (isNE(sv.linstamt, covtmjaIO.getSingp())) {
				covtmjaIO.setSingp(sv.linstamt);
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			/*       IF SR576-INST-PREM       NOT = COVTMJA-INSTPREM   <V4L005>*/
			/*          MOVE SR576-INST-PREM  TO COVTMJA-INSTPREM      <V4L005>*/
			if (isNE(sv.linstamt, covtmjaIO.getInstprem())) {
				covtmjaIO.setInstprem(sv.linstamt);
				wsaaUpdateFlag.set("Y");
			}
		}
		if (isNE(sv.mortcls, covtmjaIO.getMortcls())) {
			covtmjaIO.setMortcls(sv.mortcls);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd, covtmjaIO.getLiencd())) {
			covtmjaIO.setLiencd(sv.liencd);
			wsaaUpdateFlag.set("Y");
		}
		if ((isEQ(sv.select, "J")
		&& (isEQ(covtmjaIO.getJlife(), "00")
		|| isEQ(covtmjaIO.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select, "L"))
		&& isEQ(covtmjaIO.getJlife(), "01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select, "J")) {
				covtmjaIO.setJlife("01");
			}
			else {
				covtmjaIO.setJlife("00");
			}
		}
		/*IF CHDRMJA-BILLFREQ         NOT = COVTMJA-BILLFREQ           */
		/*   MOVE CHDRMJA-BILLFREQ    TO COVTMJA-BILLFREQ              */
		if (isNE(payrIO.getBillfreq(), covtmjaIO.getBillfreq())) {
			covtmjaIO.setBillfreq(payrIO.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		/*IF CHDRMJA-BILLCHNL         NOT = COVTMJA-BILLCHNL           */
		/*   MOVE CHDRMJA-BILLCHNL    TO COVTMJA-BILLCHNL              */
		if (isNE(payrIO.getBillchnl(), covtmjaIO.getBillchnl())) {
			covtmjaIO.setBillchnl(payrIO.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		/*    IF CHDRMJA-OCCDATE          NOT = COVTMJA-EFFDATE*/
		/*       MOVE CHDRMJA-OCCDATE     TO COVTMJA-EFFDATE*/
		/*       MOVE 'Y'                 TO WSAA-UPDATE-FLAG.*/
		if (isNE(wsaaAnbAtCcd, covtmjaIO.getAnbccd(1))) {
			covtmjaIO.setAnbccd(1, wsaaAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex, covtmjaIO.getSex(1))) {
			covtmjaIO.setSex(1, wsaaSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd, covtmjaIO.getAnbccd(2))) {
			covtmjaIO.setAnbccd(2, wsbbAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex, covtmjaIO.getSex(2))) {
			covtmjaIO.setSex(2, wsbbSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth, covtmjaIO.getBappmeth())) {
			covtmjaIO.setBappmeth(sv.bappmeth);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zstpduty01, covtmjaIO.getZstpduty01()) && stampDutyflag) {
			covtmjaIO.setZstpduty01(sv.zstpduty01);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(stateCode, covtmjaIO.getZclstate()) && stampDutyflag) {
			covtmjaIO.setZclstate(stateCode);
			wsaaUpdateFlag.set("Y");
		}
	}

protected void updateCovtmja3200()
	{
		try {
			noUpdate3210();
			update3220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void noUpdate3210()
	{
		/* If there is no change of detail then skip the UPDAT.*/
		/* If Options and extras selected and no change of detail then we*/
		/*   must have a COVTMJA record to pass on.*/
		if (isEQ(wsaaUpdateFlag, "N")
		&& isNE(sv.optextind, "X")) {
			goTo(GotoLabel.exit3290);
		}
	}

protected void update3220()
	{
		/* Update the COVRMJA record if it exists or write a new one if it*/
		/*   doesn't.*/
		/* Release the COVTMJA record.*/
		covtmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		covtmjaIO.setTermid(varcom.vrcmTermid);
		covtmjaIO.setUser(varcom.vrcmUser);
		covtmjaIO.setTransactionDate(varcom.vrcmDate);
		covtmjaIO.setTransactionTime(varcom.vrcmTime);
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setCoverage(wsaaCovrCoverage);
		/*MOVE CHDRMJA-BTDATE         TO COVTMJA-EFFDATE.              */
		covtmjaIO.setEffdate(payrIO.getBtdate());
		if (addComp.isTrue()) {
			covtmjaIO.setEffdate(wsspcomn.currfrom);
		}
		covtmjaIO.setRider(wsaaCovrRider);
		covtmjaIO.setPayrseqno(1);
		if (addComp.isTrue()) {
			covtmjaIO.setPlanSuffix(0);
			covtmjaIO.setSeqnbr(0);
		}
		else {
			covtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
			covtmjaIO.setSeqnbr(wsaaNextSeqnbr);
		}
		wsaaNextSeqnbr.subtract(1);
		/*      MOVE COVRMJA-PLAN-SUFFIX    TO COVTMJA-PLAN-SUFFIX,     */
		/*                                COVTMJA-SEQNBR.               */
		covtmjaIO.setCrtable(wsaaCrtable);
		covtmjaIO.setReserveUnitsInd(SPACES);
		covtmjaIO.setReserveUnitsDate(ZERO);
		covtmjaIO.setPolinc(ZERO);
		/* IF   ADD-COMP                                                */
		/*      MOVE WRITR             TO COVTMJA-FUNCTION              */
		/* ELSE                                                         */
		covtmjaIO.setFunction(varcom.updat);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (addComp.isTrue()) {
			writeCprp3300();
		}
	}

protected void writeCprp3300()
	{
		initCprp3310();
	}

protected void initCprp3310()
	{
		cprpIO.setChdrpfx(chdrpf.getChdrpfx());
		cprpIO.setChdrcoy(chdrpf.getChdrcoy());
		cprpIO.setChdrnum(chdrpf.getChdrnum());
		cprpIO.setLife(wsaaCovrLife);
		cprpIO.setCoverage(wsaaCovrCoverage);
		cprpIO.setRider(wsaaCovrRider);
		setPrecision(cprpIO.getTranno(), 0);
		cprpIO.setTranno(add(chdrpf.getTranno(), 1));
		cprpIO.setFunction("READH ");
		cprpIO.setFormat(formatsInner.cprprec);				//MLIL-341(Base Issue)
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)) {
			cprpIO.setFunction("WRITR");
		}
		else {
			cprpIO.setFunction("REWRT");
		}
		cprpIO.setChdrpfx(chdrpf.getChdrpfx());
		cprpIO.setChdrcoy(chdrpf.getChdrcoy());
		cprpIO.setChdrnum(chdrpf.getChdrnum());
		cprpIO.setLife(wsaaCovrLife);
		cprpIO.setCoverage(wsaaCovrCoverage);
		cprpIO.setRider(wsaaCovrRider);
		cprpIO.setInstprem(sv.instPrem);
		setPrecision(cprpIO.getTranno(), 0);
		cprpIO.setTranno(add(chdrpf.getTranno(), 1));
		cprpIO.setValidflag("1");
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cprpIO.getParams());
			fatalError600();
		}
		/*-- CODE TO DEDUCT CPRP-99-APA.                                   */
		if (isEQ(sv.optextind, "X")) {
			return ;
		}
		/*   COMPUTE WSAA-SUSP-AVAIL = WSAA-SUSP-AVAIL - SR576-INST-PREM.<*/
		compute(wsaaSuspAvail, 2).set(sub(sub(wsaaSuspAvail, sv.instPrem), wsaaProtax));
		cprpIO.setChdrpfx(chdrpf.getChdrpfx());
		cprpIO.setChdrcoy(chdrpf.getChdrcoy());
		cprpIO.setChdrnum(chdrpf.getChdrnum());
		cprpIO.setLife("99");
		cprpIO.setCoverage("00");
		cprpIO.setRider("00");
		setPrecision(cprpIO.getTranno(), 0);
		cprpIO.setTranno(add(chdrpf.getTranno(), 1));
		cprpIO.setFunction("UPDATE");
		cprpIO.setInstprem(wsaaSuspAvail);
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cprpIO.getParams());
			fatalError600();
		}
	}

protected void deleteAnnt3400()
	{
		try {
			begin3400();
			readhAnnt3420();
			deletAnnt3450();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begin3400()
	{
		anntIO.setChdrcoy(covtmjaIO.getChdrcoy());
		anntIO.setChdrnum(covtmjaIO.getChdrnum());
		anntIO.setLife(covtmjaIO.getLife());
		anntIO.setCoverage(covtmjaIO.getCoverage());
		anntIO.setRider(covtmjaIO.getRider());
		anntIO.setSeqnbr(ZERO);
		anntIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		anntIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		anntIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, anntIO);
		if (isEQ(anntIO.getStatuz(), varcom.endp)
		|| isNE(anntIO.getChdrnum(), covtmjaIO.getChdrnum())
		|| isNE(anntIO.getChdrcoy(), covtmjaIO.getChdrcoy())
		|| isNE(anntIO.getLife(), covtmjaIO.getLife())
		|| isNE(anntIO.getCoverage(), covtmjaIO.getCoverage())
		|| isNE(anntIO.getRider(), covtmjaIO.getRider())) {
			goTo(GotoLabel.exit3490);
		}
	}

protected void readhAnnt3420()
	{
		anntIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, anntIO);
		if (isNE(anntIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(anntIO.getParams());
			syserrrec.statuz.set(anntIO.getStatuz());
			fatalError600();
		}
	}

protected void deletAnnt3450()
	{
		anntIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, anntIO);
		if (isNE(anntIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(anntIO.getParams());
			syserrrec.statuz.set(anntIO.getStatuz());
			fatalError600();
		}
	}

protected void defaultPcdt3800()
	{
		moveValues3800();
		call3830();
	}

protected void moveValues3800()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setInstprem(ZERO);
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));
		wsaaIndex.set(1);
	}

	/**
	* <pre>
	*    MOVE CHDRMJA-CHDRCOY        TO PCDDMJA-CHDRCOY.              
	*    MOVE CHDRMJA-CHDRNUM        TO PCDDMJA-CHDRNUM.              
	*    MOVE BEGN                   TO PCDDMJA-FUNCTION.             
	*    MOVE ZEROES                 TO WSAA-INDEX.                   
	*3810-PCDD-LOOP.                                                  
	*    ADD 1                       TO WSAA-INDEX.                   
	*    IF WSAA-INDEX > 10                                           
	*       GO TO 3830-CALL.                                          
	*    MOVE PCDDMJAREC             TO PCDDMJA-FORMAT.               
	*    CALL 'PCDDMJAIO'            USING PCDDMJA-PARAMS.            
	*    IF PCDDMJA-CHDRNUM          NOT = CHDRMJA-CHDRNUM OR         
	*       PCDDMJA-CHDRCOY          NOT = CHDRMJA-CHDRCOY            
	*       MOVE ENDP                TO PCDDMJA-STATUZ.               
	*    IF PCDDMJA-STATUZ           NOT = O-K                        
	*    AND PCDDMJA-STATUZ          NOT = ENDP                       
	*       MOVE PCDDMJA-PARAMS      TO SYSR-PARAMS                   
	*       PERFORM 600-FATAL-ERROR.                                  
	*    IF PCDDMJA-STATUZ              = ENDP                        
	*        GO TO 3820-ADD-PCDT.                                     
	*    MOVE PCDDMJA-AGNTNUM      TO PCDTMJA-AGNTNUM (WSAA-INDEX).   
	*    MOVE PCDDMJA-SPLIT-BCOMM  TO PCDTMJA-SPLITC (WSAA-INDEX).    
	*    MOVE NEXTR                  TO PCDDMJA-FUNCTION.             
	*    GO TO 3810-PCDD-LOOP.                                        
	*3820-ADD-PCDT.                                                   
	*    IF WSAA-INDEX > 10                                           
	*       GO TO 3830-CALL.                                          
	*    MOVE ZEROES               TO PCDTMJA-SPLITC (WSAA-INDEX).    
	*    ADD  1                    TO WSAA-INDEX.                     
	*    GO TO 3820-ADD-PCDT.                                         
	* </pre>
	*/
protected void call3830()
	{
		pcdtmjaIO.setAgntnum(wsaaIndex, chdrpf.getAgntnum());
		pcdtmjaIO.setSplitc(wsaaIndex, 100);
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		/* As the program can be forced back into the 2000-section using*/
		/*   SCRN-SCRNAME in WSSP-NEXTPROG, in order to exit we must*/
		/*   ensure that the Program name has been moved to WSSP-NEXTPROG.*/
		wsspcomn.nextprog.set(wsaaProg);
		/* Determine processing according to whether we are selectin*/
		/*   or returning from the Options and Extras data screen.*/
		if (isEQ(sv.pbind, "X")) {
			pbindExe9100();
			return ;
		}
		else {
			if (isEQ(sv.pbind, "?")) {
				pbindRet9200();
				return ;
			}
		}
		if (isEQ(sv.optextind, "X")) {
			optionsExe4500();
			return ;
		}
		else {
			if (isEQ(sv.optextind, "?")) {
				optionsRet4600();
				return ;
			}
		}
		/* IF SR576-RATYPIND           = 'X'                    <R96REA>*/
		/*    PERFORM 4200-REASSURANCE-EXE                      <R96REA>*/
		/*    GO TO 4090-EXIT                                   <R96REA>*/
		/* ELSE                                                 <R96REA>*/
		/* IF SR576-RATYPIND           = '?'                    <R96REA>*/
		/*    PERFORM 4300-REASSURANCE-RET                      <R96REA>*/
		/*       GO TO 4090-EXIT.                               <R96REA>*/
		/*                                                      <R96REA>*/
		if (isEQ(sv.anntind, "X")) {
			annuityExe4900();
			return ;
		}
		else {
			if (isEQ(sv.anntind, "?")) {
				annuityRet4950();
				return ;
			}
		}
		if (isEQ(sv.comind, "X")) {
			comindExe4a00();
			return ;
		}
		else {
			if (isEQ(sv.comind, "?")) {
				comindRet4a50();
				return ;
			}
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe4a00();
			return ;
		}
		else {
			if (isEQ(sv.taxind, "?")) {
				taxRet4b00();
				return ;
			}
		}
		if (isEQ(sv.exclind, "X")) {
			exclExe6000();
			return;
		}
		if (isEQ(sv.exclind, "?")) {
			exclRet6100();
			return;
		}
        if (isEQ(sv.fuind, "X")) {
            fuindExe4d00();
            return;
        }else {
            if (isEQ(sv.fuind,"?")) {
                fuindRec4d50();
                return ;
            }
        }
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		if (planLevel.isTrue()) {
			//covrmjaIO.setFunction(varcom.nextr);
			covrpfCount++; //ILB-456
			clearScreen4700();
			policyLoad5000();
			/*if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
				wsspcomn.nextprog.set(wsaaProg);
				wayout4800();
			}*/
			if (covrpf != null) { 
				wsspcomn.nextprog.set(wsaaProg);
				wayout4800();
			}
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4800();
		}
	}

    protected void fuindExe4d00()
    {
        start4d00();
    }

    protected void start4d00()
    {
        /* Keep the COVRMJA record if Whole Plan selected or Add component*/
        /*   as this will have been released within the 1000-section to*/
        /*   enable all policies to be processed.*/
    	//ILB-456
    	chdrpfDAO.setCacheObject(chdrpf);
        /* The first thing to consider is the handling of an Options/*/
        /*   Extras request. If the indicator is 'X', a request to visit*/
        /*   options and extras has been made. In this case:*/
        /*    - change the options/extras request indicator to '?',*/
        sv.fuind.set("?");
        /* Save the next 8 programs from the program stack.*/
        compute(sub1, 0).set(add(1,wsspcomn.programPtr));
        sub2.set(1);
        for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
            save4510();
        }
        /* Call GENSSWCH with and action of 'H' to retrieve the program*/
        /*   switching required, and move them to the stack.*/
        gensswrec.function.set("J");
        gensww4210();
        compute(sub1, 0).set(add(1,wsspcomn.programPtr));
        sub2.set(1);
        for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
            load4530();
        }
        /* Set the current stack "action" to '*'.*/
        /* Add one to the program pointer and exit.*/
        wsspcomn.nextprog.set(wsaaProg);
        wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
        wsspcomn.programPtr.add(1);
    }

    protected void fuindRec4d50(){

        /* Restore the saved programs to the program stack                 */
        compute(sub1, 0).set(add(1, wsspcomn.programPtr));
        sub2.set(1);
        for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
            restore4610();
        }
        //ILB-456
        chdrpfDAO.setCacheObject(chdrpf);
        readFlupal1500();
        /* Blank out the stack  action".                                   */
        wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
        /* Set WSSP-NEXTPROG to the current screen name (thus returning    */
        /*   returning to re-display the screen).                          */
        wsspcomn.nextprog.set(scrnparams.scrname);
    }

    protected void readFlupal1500()
    {
        readFlupal1510();
    }

    protected void readFlupal1510()
    {
        List<Fluppf> fluprevIOList = fluppfDAO.searchFlupRecordByChdrNum(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());//IJTI-1410
        if(fluprevIOList!=null&&!fluprevIOList.isEmpty()){
            sv.fuind.set("+");
        }else{
            sv.fuind.set(" ");
        }
    }

	/**
	* <pre>
	*4200-REASSURANCE-EXE SECTION.                            <R96REA>
	****                                                      <R96REA>
	*4200-PARA.                                               <R96REA>
	****                                                      <R96REA>
	**Keep the COVRMJA record if Whole Plan selected or Add co<R96REA>
	****as this will have been released within the 1000-sectio<R96REA>
	****enable all policies to be processed.                  <R96REA>
	**** MOVE KEEPS               TO COVRMJA-FUNCTION.        <R96REA>
	****                                                      <R96REA>
	**** CALL 'COVRMJAIO' USING COVRMJA-PARAMS.               <R96REA>
	****                                                      <R96REA>
	**** IF COVRMJA-STATUZ        NOT = O-K                   <R96REA>
	****    MOVE COVRMJA-PARAMS   TO SYSR-PARAMS              <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	****                                                      <R96REA>
	**** If the reassurance details have been selected,(value <R96REA>
	**** then set an asterisk in the program stack action fiel<R96REA>
	**** ensure that control returns here, set the parameters <R96REA>
	**** generalised secondary switching and save the original<R96REA>
	**** programs from the program stack.                     <R96REA>
	****                                                      <R96REA>
	**** MOVE '?'               TO SR576-RATYPIND.            <R96REA>
	**** COMPUTE SUB1           =  WSSP-PROGRAM-PTR + 1.      <R96REA>
	**** MOVE 1                 TO SUB2.                      <R96REA>
	**** PERFORM 4510-SAVE      8 TIMES.                      <R96REA>
	**** MOVE 'C'               TO GENS-FUNCTION.             <R96REA>
	**** PERFORM                4210-GENSWW.                  <R96REA>
	****                                                      <R96REA>
	**** COMPUTE SUB1           =  WSSP-PROGRAM-PTR + 1.      <R96REA>
	**** MOVE 1                 TO SUB2.                      <R96REA>
	**** PERFORM 4530-LOAD               8 TIMES.             <R96REA>
	****                                                      <R96REA>
	**** MOVE '*'                 TO WSSP-SEC-ACTN(WSSP-PROGRA<R96REA>
	**** ADD 1                    TO WSSP-PROGRAM-PTR.        <R96REA>
	****                                                      <R96REA>
	*4209-EXIT.                                               <R96REA>
	****  EXIT.                                               <R96REA>
	*   If a value has been placed in the GENS-FUNCTION then call     
	*   the generalised secondary switching module to obtain the      
	*   next 8 programs and load them into the program stack.         
	* </pre>
	*/
protected void exclExe6000(){
	covrpfDAO.setCacheObject(covrpf);
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("G");
		wsspcomn.chdrChdrnum.set(covrpf.getChdrnum());
		wsspcomn.crtable.set(wsaaCrtable);
		
		
		if(isNE(wsspcomn.flag,"I")){
		wsspcomn.flag.set("X");
		wsspcomn.cmode.set("IF");}
		else
			wsspcomn.cmode.set("IFE");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),wsaaCrtable.toString(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());//IJTI-1410
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
	
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}
protected void gensww4210()
	{
		para4211();
	}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*    MOVE V045                TO SCRN-ERROR-CODE          (012)*/
			scrnparams.errorCode.set(errorsInner.h039);
			wsspcomn.nextprog.set(scrnparams.scrname);
			/*    IF LEXT-STATUZ            = ENDP                     <012>*/
			if (isEQ(lextrevIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

	/**
	* <pre>
	*4300-REASSURANCE-RET SECTION.                            <R96REA>
	****                                                      <R96REA>
	*4310-PARA.                                               <R96REA>
	****                                                      <R96REA>
	**** COMPUTE SUB1        = WSSP-PROGRAM-PTR + 1.          <R96REA>
	**** MOVE 1              TO SUB2.                         <R96REA>
	**** PERFORM 4610-RESTORE 8 TIMES.                        <R96REA>
	**** MOVE ' '            TO WSSP-SEC-ACTN(WSSP-PROGRAM-PTR<R96REA>
	**** PERFORM             5650-CHECK-RACT.                 <R96REA>
	*****ADD 1               TO WSSP-PROGRAM-PTR.             <R96REA>
	**** MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.           <R96REA>
	****                                                      <R96REA>
	*4319-EXIT.                                               <R96REA>
	****  EXIT.                                               <R96REA>
	* </pre>
	*/
protected void optionsExe4500()
	{
		keepCovr4510();
	}

protected void keepCovr4510()
	{
		/* Keep the COVRMJA record if Whole Plan selected or Add component*/
		/*   as this will have been released within the 1000-section to*/
		/*   enable all policies to be processed.*/
		//ILB-456
		covrpfDAO.setCacheObject(covrpf);
		/* The first thing to consider is the handling of an Options/*/
		/*   Extras request. If the indicator is 'X', a request to visit*/
		/*   options and extras has been made. In this case:*/
		/*    - change the options/extras request indicator to '?',*/
		sv.optextind.set("?");
		/* Save the next 8 programs from the program stack.*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		/* Call GENSSWCH with and action of 'A' to retrieve the program*/
		/*   switching required, and move them to the stack.*/
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*'.*/
		/* Add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		para4600();
	}

protected void para4600()
	{
		/* On return from this  request, the current stack "action" will*/
		/*   be '*' and the options/extras indicator  will  be  '?'.  To*/
		/*   handle the return from options and extras:*/
		/* - calculate the  premium  as  described  above  in  the*/
		/*   'Validation' section, and check that it is within the*/
		/*   tolerance limit,*/
		/*    PERFORM 5200-CALC-PREMIUM.*/
		/* MOVE 'Y'                    TO WSAA-OPTEXT.                  */
		/* PERFORM 2200-CALC-PREMIUM.                                   */
		/* If enquiry on a component then do not recalculate the Premium.  */
		/*    IF  NOT COMP-ENQUIRY                                 <V4L004>*/
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			wsaaOptext.set("Y");
			calcPremium2200();
		}
		wsaaOptext.set("N");
		/* Restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		/* Blank out the stack "action".*/
		/*    Only blank out the action and set the screen name            */
		/*    if none of the other check boxes have been selected.         */
		/* MOVE ' '                 TO WSSP-SEC-ACTN (WSSP-PROGRAM-PTR).*/
		checkLext5600();
		/* Store the same values in the POVR key as used in 5600- for use  */
		/* in returning ... returning||                                    */
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(covtmjaIO.getLife());
		wsaaPovrCoverage.set(covtmjaIO.getCoverage());
		wsaaPovrRider.set(covtmjaIO.getRider());
		/* Set WSSP-NEXTPROG to the current screen name (thus returning*/
		/*   returning to re-display the screen).*/
		/* MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.                   */
		if (isNE(sv.anntind, "X")
		&& isNE(sv.comind, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void clearScreen4700()
	{
		/*PARA*/
		/* Clear all screen fields which are variable NOT header details.*/
		sv.premCessDate.set(ZERO);
		sv.benCessDate.set(ZERO);
		sv.riskCessDate.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.benCessAge.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.benCessTerm.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.liencd.set(SPACES);
		sv.mortcls.set(SPACES);
		sv.optextind.set(SPACES);
		sv.select.set(SPACES);
		sv.statFund.set(SPACES);
		sv.statSect.set(SPACES);
		sv.bappmeth.set(SPACES);
		sv.statSubsect.set(SPACES);
		/*EXIT*/
	}

protected void wayout4800()
	{
		/*PROGRAM-EXIT*/
		/* If control is passed to this  part of the 4000 section on the*/
		/*   way out of the program, i.e. after  screen  I/O,  then  the*/
		/*   current stack position action  flag will be  blank.  If the*/
		/*   4000-section  is  being   performed  after  returning  from*/
		/*   processing another program then  the current stack position*/
		/*   action flag will be '*'.*/
		/* If 'KILL' has been requested, (CF11), then move spaces to the*/
		/*   current program entry in the program stack and exit.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/* Add 1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void annuityExe4900()
	{
		para4900();
	}

protected void para4900()
	{
		//ILB-456
		covrpfDAO.setCacheObject(covrpf);
		/* A new COVT only exists for component add and modify, so      */
		/* do not attempt a KEEPS on the COVT if in enquiry mode.       */
		if (!compEnquiry.isTrue()) {
			covtmjaIO.setFormat(formatsInner.covtmjarec);
			covtmjaIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, covtmjaIO);
			if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtmjaIO.getParams());
				fatalError600();
			}
		}
		/* If the ANNUITY details have been selected,(value - 'X'),     */
		/* then a '?' is moved to the ANNTIND so that next time the     */
		/* program is run it moves to the ANNUITY-RET(URN) section      */
		/* rather than returning here.                                  */
		sv.anntind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4510();
		}
		/* The 'D' switches the program towards P5235 as determined     */
		/* in Table T1675.                                              */
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		/* Move the benefit amount to linkage to check in P5220 that    */
		/* the capital content is not greater than the benefit amount.  */
		/* Also, move the Paid-To-Date from the PAYR through linkage    */
		/* to read T6625 in P5235.                                      */
		wssplife.bigAmt.set(sv.sumin);
		wssplife.effdate.set(payrIO.getPtdate());
		if (addComp.isTrue()) {
			wssplife.effdate.set(wsspcomn.currfrom);
		}
	}

protected void annuityRet4950()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/*  - calculate the premium as described above in the Validation   */
		/*    section, and check that it is within the tolerance limit.    */
		sv.anntind.set("+");
		/* PERFORM 2200-CALC-PREMIUM.                                   */
		/* If enquiry on a component then do not recalculate the Premium.  */
		/*    IF  NOT COMP-ENQUIRY                                 <V4L004>*/
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			calcPremium2200();
		}
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		/*    Only blank out the action and set the screen name            */
		/*    if the commission check box has not been selected.           */
		/* MOVE ' '                    TO WSSP-SEC-ACTN         <RA9606>*/
		/*                                (WSSP-PROGRAM-PTR).   <RA9606>*/
		/* MOVE SCRN-SCRNAME           TO WSSP-NEXTPROG.        <RA9606>*/
		if (isNE(sv.comind, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		/* Turn the protect switch off.                                 */
		sv.anntindOut[varcom.pr.toInt()].set("N");
		/*EXIT*/
	}

protected void comindExe4a00()
	{
		para4a00();
	}

protected void para4a00()
	{
		wsspcomn.tranrate.set(sv.instPrem);
		//ILB-456
		covrpfDAO.setCacheObject(covrpf);
		/* A new COVT only exists for component add and modify, so      */
		/* do not attempt a KEEPS on the COVT if in enquiry mode.       */
		if (!compEnquiry.isTrue()) {
			covtmjaIO.setFunction(varcom.keeps);
			covtmjaIO.setFormat(formatsInner.covtmjarec);
			SmartFileCode.execute(appVars, covtmjaIO);
			if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtmjaIO.getParams());
				fatalError600();
			}
		}
		/* If the COMMISSION details have been selected,(value - 'X'),  */
		/* then a '?' is moved to the COMMIND so that next time the     */
		/* program is run it moves to the COMMIND-RET(URN) section      */
		/* rather than returning here.                                  */
		sv.comind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			save4510();
		}
		/* The 'B' switches the program towards P5153 as determined     */
		/* in Table T1675.                                              */
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void comindRet4a50()
	{
		/*A50-PARA*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/*  - calculate the premium as described above in the Validation   */
		/*    section, and check that it is within the tolerance limit.    */
		sv.comind.set("+");
		/* PERFORM 2200-CALC-PREMIUM.                                   */
		/* If enquiry on a component then do not recalculate the Premium.  */
		/*    IF  NOT COMP-ENQUIRY                                 <V4L004>*/
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			calcPremium2200();
		}
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.nextprog.set(scrnparams.scrname);
		/* Turn the protect switch off.                                 */
		sv.comindOut[varcom.pr.toInt()].set("N");
		/*A99-EXIT*/
	}

protected void taxExe4a00()
	{
		start4a10();
	}

protected void start4a10()
	{
		/* Keep the CHDR/COVR record                                       */
		//ILB-456
		
		covtmjapf = new Covtpf();
		covtmjapf.setCrtable(covtmjaIO.getCrtable().toString());
		covtmjapf.setLife(covtmjaIO.getLife().toString());
		covtmjapf.setCoverage(covtmjaIO.getCoverage().toString());
		covtmjapf.setRider(covtmjaIO.getRider().toString());
		covtmjapf.setPlnsfx(covtmjaIO.getPlanSuffix().toInt());
		covtmjapf.setEffdate(covtmjaIO.getEffdate().toInt());
		chdrpfDAO.setCacheObject(chdrpf);
		wsaaStoreZbinstprem.set(covtmjaIO.getZbinstprem());
		wsaaStoreSingp.set(covtmjaIO.getSingp());
		wsaaStoreInstprem.set(covtmjaIO.getInstprem());
		covtmjapf.setZbinstprem(sv.zbinstprem.getbigdata());
		if (isEQ(payrIO.getBillfreq(), "00")) {
			covtmjapf.setSingp(sv.linstamt.getbigdata());
			covtmjapf.setInstprem(BigDecimal.ZERO);
		}
		else {
			covtmjapf.setInstprem(sv.linstamt.getbigdata());
			covtmjapf.setSingp(BigDecimal.ZERO);
		}
		
		covtpfDAO.setCacheObject(covtmjapf);
		
		wssplife.effdate.set(chdrpf.getBtdate());
		/* Perform the standard GENSWCH setup  and execution               */
		sv.taxind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet4b00()
	{
		/*B10-START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		sv.taxind.set("+");
		covtmjaIO.setZbinstprem(wsaaStoreZbinstprem);
		covtmjaIO.setSingp(wsaaStoreSingp);
		covtmjaIO.setInstprem(wsaaStoreInstprem);
		/* Restore the saved programs to the program stack                 */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*B90-EXIT*/
	}

protected void policyLoad5000()
	{
		try {
			loadPolicy5010();
			readCovt5020();
			fieldsToScreen5080();
			mortalityLien5090();
			ageProcessing50a0();
			optionsExtras50b0();
			enquiryProtect50c0();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadPolicy5010()
	{
		/* If Whole plan selected or component ADD, then we need to read*/
		/*   the COVRMJA file to find either the record to be changed or*/
		/*   the policies to which the component must be added.*/
		/* If COVRMJA statuz = ENDP then all policies have been processed*/
		/*   so exit this section.*/
		/* Else attempt a search of the COVTMJA temporary file for detail*/
		/*   changes.*/
		wsaaOldPrem.set(0);
		wsaaOldSumins.set(0);
		if (planLevel.isTrue()) {
			readCovr5100();
			for (Covrpf covr : covrpfList) {
				if (covr == null) {
					goTo(GotoLabel.exit50z0);				
				}
				else {
					covtmjaIO.setFunction(varcom.readr);
					if (addComp.isTrue()) {
						covtmjaIO.setCoverage(wsaaCovrCoverage);
						covtmjaIO.setRider(wsaaCovrRider);
						covtmjaIO.setPlanSuffix(covr.getPlanSuffix());
					}
					else {
						/*             MOVE COVRMJA-DATA-KEY    TO COVTMJA-DATA-KEY*/
						covtmjaIO.setDataKey(wsaaCovrKey);
						if (isNE(covr.getPlanSuffix(), ZERO)) {
							covtmjaIO.setPlanSuffix(covr.getPlanSuffix());
						}
					}
				}
				covrpf.setPlanSuffix(covr.getPlanSuffix());
			}
		}
		wsaaOldSumins.set(covrpf.getSumins());
		wsaaOldPrem.set(covrpf.getInstprem());
		/* The header is set to show which the policy is being actioned.*/
		/* If all policies are within the summarised record then display*/
		/*   the number of policies summarised and set the Hi-light on*/
		/*   Plan suffix to display the literal -*/
		/*                  'Policy Number : 10 to 1'*/
		/*                                      u001au001au001au001a*/
		if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
			sv.planSuffix.set(chdrpf.getPolsum());
			sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		}
		else {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
	}

protected void readCovt5020()
	{
		/* Read the Temporary COVT record which will contain any previous*/
		/*   changes before AT submission creates the COVRMJA record.*/
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)
		&& isNE(covtmjaIO.getStatuz(), varcom.mrnf)
		&& isNE(covtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(covtmjaIO.getLife(), covrpf.getLife())
		|| isNE(covtmjaIO.getCoverage(), covrpf.getCoverage())
		|| isNE(covtmjaIO.getRider(), covrpf.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)
		|| isEQ(covtmjaIO.getStatuz(), varcom.mrnf)) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		/* If entering on Component Change then just Display the details*/
		/*   as no defaults are necessary.*/
		/* Else we must load the Tables required for defaults.*/
		/*   note - Both statuz's are checked as we may previously have*/
		/*          read the file directly or sequentially.*/
		if (!addComp.isTrue()) {
			recordToScreen6000();
			goTo(GotoLabel.exit50z0);
		}
		else {
			if (isEQ(covtmjaIO.getStatuz(), varcom.mrnf)
			|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
				/*          MOVE COVRMJA-DATA-KEY TO COVTMJA-DATA-KEY*/
				covtmjaIO.setNonKey(SPACES);
				covtmjaIO.setAnbccd(1, ZERO);
				covtmjaIO.setAnbccd(2, ZERO);
				covtmjaIO.setSingp(ZERO);
				covtmjaIO.setPlanSuffix(ZERO);
				covtmjaIO.setInstprem(ZERO);
				covtmjaIO.setZlinstprem(ZERO);
				/*BRD-306 START */
				sv.loadper.set(ZERO);
				sv.rateadj.set(ZERO);
				sv.fltmort.set(ZERO);
				sv.premadj.set(ZERO);
				sv.adjustageamt.set(ZERO);
				/*BRD-306 END */
				if(stampDutyflag){
					sv.zstpduty01.set(ZERO);
				}
				covtmjaIO.setZbinstprem(ZERO);
				covtmjaIO.setPremCessAge(ZERO);
				covtmjaIO.setPremCessTerm(ZERO);
				covtmjaIO.setRiskCessAge(ZERO);
				covtmjaIO.setRiskCessTerm(ZERO);
				covtmjaIO.setBenCessAge(ZERO);
				covtmjaIO.setBenCessTerm(ZERO);
				covtmjaIO.setReserveUnitsDate(ZERO);
				covtmjaIO.setSumins(ZERO);
				/*MOVE CHDRMJA-BTDATE  TO  COVTMJA-EFFDATE               */
				covtmjaIO.setEffdate(payrIO.getBtdate());
				if (addComp.isTrue()) {
					covtmjaIO.setEffdate(wsspcomn.currfrom);
				}
				covtmjaIO.setPremCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setRiskCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setPolinc(chdrpf.getPolinc());
				covtmjaIO.setCrtable(wsaaCrtable);
				tableLoads7000();
			}
			else {
				tableLoads7000();
			}
		}
		if (isEQ(covtmjaIO.getJlife(), "01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
	}

protected void fieldsToScreen5080()
	{
		/* Move fields to screen.*/
		sv.planSuffix.set(chdrpf.getPolinc());
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.premCessDate.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.riskCessDate.set(covtmjaIO.getRiskCessDate());
		sv.riskCessAge.set(covtmjaIO.getRiskCessAge());
		sv.riskCessTerm.set(covtmjaIO.getRiskCessTerm());
		sv.benCessDate.set(covtmjaIO.getBenCessDate());
		sv.benCessAge.set(covtmjaIO.getBenCessAge());
		sv.benCessTerm.set(covtmjaIO.getBenCessTerm());
		/*    MOVE COVTMJA-INSTPREM       TO SR576-INST-PREM.              */
		if (addComp.isTrue()
		|| addApp.isTrue()
		|| revComp.isTrue()) {
			r200ReadCprp();
			if (isEQ(cprpIO.getStatuz(), varcom.oK)) {
				sv.instPrem.set(cprpIO.getInstprem());
			}
			else {
				sv.instPrem.set(covtmjaIO.getInstprem());
			}
		}
		else {
			sv.instPrem.set(covtmjaIO.getInstprem());
		}
		sv.linstamt.set(covtmjaIO.getInstprem());
		sv.zbinstprem.set(covtmjaIO.getZbinstprem());
		sv.zlinstprem.set(covtmjaIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtmjaIO.getLoadper());
		sv.rateadj.set(covtmjaIO.getRateadj());
		sv.fltmort.set(covtmjaIO.getFltmort());
		sv.premadj.set(covtmjaIO.getPremadj());
		sv.adjustageamt.set(covtmjaIO.getAgeadj());
		/*BRD-306 END */
		if(stampDutyflag){
			if(isNE(covtmjaIO.getZstpduty01(),ZERO)&& isEQ(sv.zstpduty01,ZERO)){
				sv.zstpduty01.set(covtmjaIO.getZstpduty01());
			}
			if(isNE(covtmjaIO.getZclstate(),SPACES)){
				stateCode=covtmjaIO.getZclstate().toString();
			}
		}
		
		sv.sumin.set(covtmjaIO.getSumins());
		setupBonus1300();
		sv.bappmeth.set(covtmjaIO.getBappmeth());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/* The fields to  be displayed on the screen are determined from*/
		/*   the entry in table T5606 read earlier as follows:*/
		/*  - if the  maximum  and  minimum  sum insured amounts are*/
		/*       both  zero, non-display and protect the sum insured*/
		/*       amount.  If  the  minimum  and maximum are both the*/
		/*       same, display the amount protected. Otherwise allow*/
		/*       input.*/
		/* NOTE - the sum insured applies to the PLAN, so if it is to*/
		/*        be defaulted, scale it down according to the number*/
		/*        of policies applicable.*/
		if (isEQ(t5606rec.sumInsMax, 0)
		&& isEQ(t5606rec.sumInsMin, 0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5606rec.sumInsMax);
		}
		/*  - if  the amount is to be dispayed/entered,  look up the*/
		/*       benefit frequency short description (T3590).*/
		if (isNE(t5606rec.sumInsMax, 0)
		|| isNE(t5606rec.sumInsMin, 0)) {
			descIO.setDataArea(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(tablesInner.t3590);
			descIO.setDescitem(t5606rec.benfreq);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)
			&& isNE(descIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					sv.frqdesc.set(descIO.getShortdesc());
				}
				else {
					sv.frqdesc.fill("?");
				}
			}
		}
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560
		}
	}

protected void mortalityLien5090()
	{
		/* If all the valid mortality classes are blank, non-display and*/
		/*   protect this field. If there is only one mortality class,*/
		/*   display and protect it (no validation will be required).*/
		if (isEQ(t5606rec.mortclss, SPACES)) {
		/*	sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5606rec.mortcls01, SPACES)
		&& isEQ(t5606rec.mortcls02, SPACES)
		&& isEQ(t5606rec.mortcls03, SPACES)
		&& isEQ(t5606rec.mortcls04, SPACES)
		&& isEQ(t5606rec.mortcls05, SPACES)
		&& isEQ(t5606rec.mortcls06, SPACES)) {
			sv.mortcls.set(t5606rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/* If all the valid lien codes are blank, non-display and protect*/
		/*   this field. Otherwise, this is an optional field.*/
		if (isEQ(t5606rec.liencds, SPACES)) {
			//sv.liencdOut[varcom.nd.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void ageProcessing50a0()
	{
		/* Using the age next birthday (ANB at RCD) from the applicable*/
		/*   life (see above), look up Issue Age on the AGE and TERM*/
		/*   sections. If the age fits into a "slot" in one of these*/
		/*   sections, and the risk cessation limits are the same,*/
		/*   default and protect the risk cessation fields. Also do the*/
		/*   same for the premium cessation details. In this case, also*/
		/*   calculate the risk and premium cessation dates.*/
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (isEQ(covtmjaIO.getJlife(), "01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults5300();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& isNE(t5606rec.eaage, SPACES)) {
			riskCessDate5400();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& isNE(t5606rec.eaage, SPACES)) {
			premCessDate5450();
		}
		if ((wsaaDefaultsInner.defaultBa.isTrue()
		|| wsaaDefaultsInner.defaultBt.isTrue())
		&& isNE(t5606rec.eaage, SPACES)) {
			benCessDate5470();
		}
		if (isNE(t5606rec.eaage, SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void optionsExtras50b0()
	{
		/* If options and extras are  not  allowed (as defined by T5606)*/
		/*   non-display and protect the fields.*/
		/* Otherwise,  read the  options and extras  details  for  the*/
		/*   current coverage/rider. If any records  exist, put a '+' in*/
		/*   the Options/Extras indicator (to show that there are some).*/
		if (isEQ(t5606rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
		/*B2-PREMIUM-BREAKDOWN*/
		/* As per other 'window box' checks the premium breakdown file     */
		/* needs to be checked for the existance of a record. Store the    */
		/* values here so that a general check can be made prior to        */
		/* displaying the screen in all cases ......                       */
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
	}

protected void enquiryProtect50c0()
	{
		/* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to*/
		/*   protect  all input capable  fields  except  the  indicators*/
		/*   controlling where to  switch  to  next  (options and extras*/
		/*   indicator).*/
		/*    IF COMP-ENQUIRY                                              */
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void readCovr5100()
	{
		/*READ-COVRMJA*/
		/* Read the Coverage file.*/
		//ILB-456 starts
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr:covrpfList) {
			if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
					|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
					|| isNE(covr.getLife(),wsaaCovrLife)
					|| isNE(covr.getCoverage(),wsaaCovrCoverage)
					|| isNE(covr.getRider(),wsaaCovrRider)) {
						return;
					}
		}
	//ILB-456 ends

		/*EXIT*/
	}

	

protected void calcPremium5200()
	{
		try {
			calcPremium5210();
			methodTable5220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void calcPremium5210()
	{
		/* Use the premium-method selected from T5687, if not blank THEN*/
		/*   access T5675. This gives the subroutine for the calculation.*/
		/* If the benefit billing method is not blank, non-display and*/
		/*   protect the premium field (so skip the following).*/
		/* MOVE 'N'                    TO PREM-REQD.                    */
		wsaaPremStatuz.set("N");
		if (isNE(t5687rec.bbmeth, SPACES)) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit5290);
		}
	}

protected void methodTable5220()
	{
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/* MOVE 'Y'                 TO PREM-REQD                     */
			wsaaPremStatuz.set("Y");
		}
		else {
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
			t5675rec.t5675Rec.set(itemIO.getGenarea());
		}
	}

protected void checkDefaults5300()
	{
		try {
			searchTable5310();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	* Using the age next birthday (ANB at RCD) from the applicable
	*   life (see above), look up Issue Age on the AGE and TERM
	*   sections. If the age fits into a "slot" in one of these
	*   sections, and the risk cessation limits are the same,
	*   default and protect the risk cessation fields. Also do the
	*   same for the premium cessation details. In this case, also
	*   calculate the risk and premium cessation dates.
	* </pre>
	*/
protected void searchTable5310()
	{
		sub1.set(1);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
		while ( !(isGT(sub1, wsaaMaxOcc))) {
			nextColumn5320();
		}
		
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultBa.isTrue()) {
			sv.benCessAge.set(wsddBenCessAge);
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultBt.isTrue()) {
			sv.benCessTerm.set(wsddBenCessTerm);
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		goTo(GotoLabel.exit5390);
	}

protected void nextColumn5320()
	{
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.riskCessageFrom[sub1.toInt()], t5606rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(t5606rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.premCessageFrom[sub1.toInt()], t5606rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(t5606rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefBa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.benCessageFrom[sub1.toInt()], t5606rec.benCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultBa.isTrue()
				&& !wsaaDefaultsInner.defaultBt.isTrue()) {
					wsddBenCessAge.set(t5606rec.benCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultBa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultBa.set("N");
					wsaaDefaultsInner.wsaaDefaultBt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.riskCesstermFrom[sub1.toInt()], t5606rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(t5606rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.premCesstermFrom[sub1.toInt()], t5606rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(t5606rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefBt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.benCesstermFrm[sub1.toInt()], t5606rec.benCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultBt.isTrue()
				&& !wsaaDefaultsInner.defaultBa.isTrue()) {
					wsddBenCessTerm.set(t5606rec.benCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultBt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultBa.set("N");
					wsaaDefaultsInner.wsaaDefaultBt.set("N");
				}
			}
		}
		sub1.add(1);
	}

	/**
	* <pre>
	*5400-RISK-CESS-DATE SECTION.                                     
	*5400-PARA.                                                       
	*    IF SR576-RISK-CESS-AGE      = 0                              
	*       PERFORM 5410-RISK-CESS-TERM                               
	*       GO TO 5440-EXIT.                                          
	*    IF T5606-EAAGE              = 'A'                            
	*    IF NOT ADD-COMP                                              
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR576-RISK-CESS-AGE         
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5606-EAAGE              = 'A'                            
	*    IF ADD-COMP                                                  
	*       MOVE CHDRMJA-PTDATE     TO DTC2-INT-DATE-1                
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR576-RISK-CESS-AGE         
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5606-EAAGE              = 'E' OR SPACE                   
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       MOVE SR576-RISK-CESS-AGE TO DTC2-FREQ-FACTOR.             
	*    PERFORM 5500-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ         TO SR576-RCESDTE-ERR             
	*    ELSE                                                         
	*       MOVE DTC2-INT-DATE-2     TO SR576-RISK-CESS-DATE.         
	*    GO TO 5440-EXIT.                                             
	*5410-RISK-CESS-TERM.                                             
	*    IF SR576-RISK-CESS-TERM     = 0                              
	*        MOVE E186               TO SR576-RCESDTE-ERR             
	*        GO TO 5440-EXIT.                                         
	*    IF T5606-EAAGE              = 'A' OR SPACE                   
	*       MOVE SR576-RISK-CESS-TERM TO DTC2-FREQ-FACTOR             
	*    IF NOT ADD-COMP                                              
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*    ELSE                                                         
	*       MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1.              
	*    IF T5606-EAAGE              = 'E'                            
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       ADD SR576-RISK-CESS-TERM, WSZZ-ANB-AT-CCD                 
	*                                GIVING DTC2-FREQ-FACTOR          
	*    IF NOT ADD-COMP                                              
	*       IF CHDRMJA-OCCDATE NOT = WSZZ-CLTDOB                      
	*          SUBTRACT 1            FROM DTC2-FREQ-FACTOR.           
	*    IF T5606-EAAGE              = 'E'                            
	*    IF ADD-COMP                                                  
	*       IF CHDRMJA-BTDATE  NOT = WSZZ-CLTDOB                      
	*          SUBTRACT 1            FROM DTC2-FREQ-FACTOR.           
	*    PERFORM 5500-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ         TO SR576-RCESDTE-ERR             
	*    ELSE                                                         
	*       MOVE DTC2-INT-DATE-2     TO SR576-RISK-CESS-DATE.         
	*5440-EXIT.                                                       
	*    EXIT.                                                        
	*5450-PREM-CESS-DATE SECTION.                                     
	*5450-PARA.                                                       
	*    IF SR576-PREM-CESS-AGE      = 0                              
	*       PERFORM 5460-PREM-CESS-TERM                               
	*       GO TO 5490-EXIT.                                          
	*    IF T5606-EAAGE              = 'A'                            
	*    IF NOT ADD-COMP                                              
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR576-PREM-CESS-AGE         
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5606-EAAGE              = 'A'                            
	*    IF ADD-COMP                                                  
	*       MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1               
	*       SUBTRACT WSZZ-ANB-AT-CCD FROM SR576-PREM-CESS-AGE         
	*                         GIVING DTC2-FREQ-FACTOR.                
	*    IF T5606-EAAGE              = 'E' OR SPACE                   
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       MOVE SR576-PREM-CESS-AGE TO DTC2-FREQ-FACTOR.             
	*    PERFORM 5500-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ         TO SR576-PCESDTE-ERR             
	*    ELSE                                                         
	*       MOVE DTC2-INT-DATE-2     TO SR576-PREM-CESS-DATE.         
	*    GO TO 5490-EXIT.                                             
	*5460-PREM-CESS-TERM.                                             
	*    IF SR576-PREM-CESS-TERM     = 0                              
	*        GO TO 5490-EXIT.                                         
	*    IF T5606-EAAGE              = 'A' OR SPACE                   
	*       MOVE SR576-PREM-CESS-TERM TO DTC2-FREQ-FACTOR             
	*    IF NOT ADD-COMP                                              
	*       MOVE CHDRMJA-OCCDATE     TO DTC2-INT-DATE-1               
	*    ELSE                                                         
	*       MOVE CHDRMJA-BTDATE      TO DTC2-INT-DATE-1.              
	*    IF T5606-EAAGE              = 'E'                            
	*       MOVE WSZZ-CLTDOB         TO DTC2-INT-DATE-1               
	*       ADD SR576-PREM-CESS-TERM, WSZZ-ANB-AT-CCD                 
	*                                GIVING DTC2-FREQ-FACTOR          
	*    IF NOT ADD-COMP                                              
	*       IF CHDRMJA-OCCDATE       NOT = WSZZ-CLTDOB                
	*          SUBTRACT 1            FROM DTC2-FREQ-FACTOR.           
	*    IF T5606-EAAGE              = 'E'                            
	*    IF ADD-COMP                                                  
	*       IF CHDRMJA-BTDATE        NOT = WSZZ-CLTDOB                
	*          SUBTRACT 1            FROM DTC2-FREQ-FACTOR.           
	*    PERFORM 5500-CALL-DATCON2.                                   
	*    IF DTC2-STATUZ              NOT = O-K                        
	*       MOVE DTC2-STATUZ         TO SR576-PCESDTE-ERR             
	*    ELSE                                                         
	*       MOVE DTC2-INT-DATE-2     TO SR576-PREM-CESS-DATE.         
	*5490-EXIT.                                                       
	*     EXIT.                                                       
	* </pre>
	*/
protected void riskCessDate5400()
	{
		riskCessDatePara5400();
	}

protected void riskCessDatePara5400()
	{
		if (isEQ(sv.riskCessAge, ZERO)
		&& isEQ(sv.riskCessTerm, ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.riskCessAge, 0), true)
		&& isEQ(isGT(sv.riskCessTerm, 0), false)){
			if (isEQ(t5606rec.eaage, "A")) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <026>*/
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1  <V4L005>*/
					datcon2rec.intDate1.set(chdrpf.getOccdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge, wsaaAnb));
			}
			if (isEQ(t5606rec.eaage, "E")
			|| isEQ(t5606rec.eaage, SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.riskCessAge);
			}
		}
		else if (isEQ(isGT(sv.riskCessAge, 0), false)
		&& isEQ(isGT(sv.riskCessTerm, 0), true)){
			if (isEQ(t5606rec.eaage, "A")
			|| isEQ(t5606rec.eaage, SPACES)) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <026>*/
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1  <V4L005>*/
					datcon2rec.intDate1.set(chdrpf.getOccdate());
					wsaaTempPrev.set(chdrpf.getOccdate());
					datcon2rec.intDate2.set(ZERO);
					datcon2rec.freqFactor.set(1);
					while ( !(isGT(datcon2rec.intDate2, sv.effdate))) {
						callDatcon25500();
						wsaaTempPrev.set(datcon2rec.intDate1);
						datcon2rec.intDate1.set(datcon2rec.intDate2);
					}
					
					datcon2rec.intDate1.set(wsaaTempPrev);
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.riskCessTerm);
			}
			if (isEQ(t5606rec.eaage, "E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(add(sv.riskCessTerm, wsaaAnb));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.riskCessDate.set(datcon2rec.intDate2);
		}
	}

protected void premCessDate5450()
	{
		premCessDatePara5450();
	}

protected void premCessDatePara5450()
	{
		if (isEQ(sv.premCessAge, ZERO)
		&& isEQ(sv.premCessTerm, ZERO)) {
			/*        GO TO 5490-EXIT.                                    <027>*/
			return ;
		}
		if (isEQ(isGT(sv.premCessAge, 0), true)
		&& isEQ(isGT(sv.premCessTerm, 0), false)){
			if (isEQ(t5606rec.eaage, "A")) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <026>*/
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1  <V4L005>*/
					datcon2rec.intDate1.set(chdrpf.getOccdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge, wsaaAnb));
			}
			if (isEQ(t5606rec.eaage, "E")
			|| isEQ(t5606rec.eaage, SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.premCessAge);
			}
		}
		else if (isEQ(isGT(sv.premCessAge, 0), false)
		&& isEQ(isGT(sv.premCessTerm, 0), true)){
			if (isEQ(t5606rec.eaage, "A")
			|| isEQ(t5606rec.eaage, SPACES)) {
				if (addComp.isTrue()) {
					/*          MOVE CHDRMJA-BTDATE   TO DTC2-INT-DATE-1     <026>*/
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1  <V4L005>*/
					datcon2rec.intDate1.set(chdrpf.getOccdate());
					wsaaTempPrev.set(chdrpf.getOccdate());
					datcon2rec.intDate2.set(ZERO);
					datcon2rec.freqFactor.set(1);
					while ( !(isGT(datcon2rec.intDate2, sv.effdate))) {
						callDatcon25500();
						wsaaTempPrev.set(datcon2rec.intDate1);
						datcon2rec.intDate1.set(datcon2rec.intDate2);
					}
					
					datcon2rec.intDate1.set(wsaaTempPrev);
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.premCessTerm);
			}
			if (isEQ(t5606rec.eaage, "E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(add(sv.premCessTerm, wsaaAnb));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.premCessDate.set(datcon2rec.intDate2);
		}
	}

protected void benCessDate5470()
	{
		benCessDatePara5470();
	}

protected void benCessDatePara5470()
	{
		if (isEQ(sv.benCessAge, ZERO)
		&& isEQ(sv.benCessTerm, ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.benCessAge, 0), true)
		&& isEQ(isGT(sv.benCessTerm, 0), false)){
			if (isEQ(t5606rec.eaage, "A")) {
				if (addComp.isTrue()) {
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1  <V4L005>*/
					datcon2rec.intDate1.set(chdrpf.getOccdate());
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.benCessAge, wsaaAnb));
			}
			if (isEQ(t5606rec.eaage, "E")
			|| isEQ(t5606rec.eaage, SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.benCessAge);
			}
		}
		else if (isEQ(isGT(sv.benCessAge, 0), false)
		&& isEQ(isGT(sv.benCessTerm, 0), true)){
			if (isEQ(t5606rec.eaage, "A")
			|| isEQ(t5606rec.eaage, SPACES)) {
				if (addComp.isTrue()) {
					/*               MOVE PAYR-BTDATE      TO DTC2-INT-DATE-1  <V4L005>*/
					datcon2rec.intDate1.set(chdrpf.getOccdate());
					wsaaTempPrev.set(chdrpf.getOccdate());
					datcon2rec.intDate2.set(ZERO);
					datcon2rec.freqFactor.set(1);
					while ( !(isGT(datcon2rec.intDate2, sv.effdate))) {
						callDatcon25500();
						wsaaTempPrev.set(datcon2rec.intDate1);
						datcon2rec.intDate1.set(datcon2rec.intDate2);
					}
					
					datcon2rec.intDate1.set(wsaaTempPrev);
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.benCessTerm);
			}
			if (isEQ(t5606rec.eaage, "E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(add(sv.benCessTerm, wsaaAnb));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.bcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.benCessDate.set(datcon2rec.intDate2);
		}
	}

protected void callDatcon25500()
	{
		/*CALL*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkLext5600()
	{
		readLext5610();
	}

protected void readLext5610()
	{
		/* Read the Options and Extras record for the Component.*/
		/* MOVE COVTMJA-CHDRCOY        TO LEXT-CHDRCOY.                 */
		/* MOVE COVTMJA-CHDRNUM        TO LEXT-CHDRNUM.                 */
		/* MOVE COVTMJA-LIFE           TO LEXT-LIFE.                    */
		/* MOVE COVTMJA-COVERAGE       TO LEXT-COVERAGE.                */
		/* MOVE COVTMJA-RIDER          TO LEXT-RIDER.                   */
		/* MOVE 0                      TO LEXT-SEQNBR.                  */
		/* MOVE LEXTREC                TO LEXT-FORMAT.                  */
		/* MOVE BEGN                   TO LEXT-FUNCTION.                */
		/* CALL 'LEXTIO' USING LEXT-PARAMS.                             */
		/* IF LEXT-STATUZ              NOT = O-K AND                    */
		/*                             NOT = ENDP                       */
		/*    MOVE LEXT-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* IF COVTMJA-CHDRCOY          NOT = LEXT-CHDRCOY               */
		/* OR COVTMJA-CHDRNUM          NOT = LEXT-CHDRNUM               */
		/* OR COVTMJA-LIFE             NOT = LEXT-LIFE                  */
		/* OR COVTMJA-COVERAGE         NOT = LEXT-COVERAGE              */
		/* OR COVTMJA-RIDER            NOT = LEXT-RIDER                 */
		/*    MOVE ENDP                TO LEXT-STATUZ.                  */
		/* IF LEXT-STATUZ              = ENDP                           */
		/*    MOVE ' '                 TO SR576-OPTEXTIND               */
		/* ELSE                                                         */
		/*    MOVE '+'                 TO SR576-OPTEXTIND.              */
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrcoy(chdrpf.getChdrcoy());
		lextrevIO.setChdrnum(chdrpf.getChdrnum());
		lextrevIO.setLife(covtmjaIO.getLife());
		lextrevIO.setCoverage(covtmjaIO.getCoverage());
		lextrevIO.setRider(covtmjaIO.getRider());
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.oK)
		&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), lextrevIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), lextrevIO.getChdrnum())
		|| isNE(covtmjaIO.getLife(), lextrevIO.getLife())
		|| isNE(covtmjaIO.getCoverage(), lextrevIO.getCoverage())
		|| isNE(covtmjaIO.getRider(), lextrevIO.getRider())) {
			lextrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextrevIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
		/* Extra check in here to see if any LEXT records associated with  */
		/*  this component have been amended whilst we have been here in   */
		/*  component Add/Modify - check TRANNOs on LEXTREV records        */
		/*  against the current TRANNO held on the Contract Header we      */
		/*  are currently using in the CHDRMJA logical.                    */
		if (isNE(lextrevIO.getStatuz(), varcom.endp)) {
			if (isGT(lextrevIO.getTranno(), chdrpf.getTranno())) {
				lextrevIO.setStatuz(varcom.endp);
			}
		}
		while ( !(isEQ(lextrevIO.getStatuz(), varcom.endp))) {
			lextrevIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, lextrevIO);
			if (isNE(lextrevIO.getStatuz(), varcom.oK)
			&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(lextrevIO.getParams());
				syserrrec.statuz.set(lextrevIO.getStatuz());
				fatalError600();
			}
			if (isNE(chdrpf.getChdrcoy(), lextrevIO.getChdrcoy())
			|| isNE(chdrpf.getChdrnum(), lextrevIO.getChdrnum())
			|| isNE(covtmjaIO.getLife(), lextrevIO.getLife())
			|| isNE(covtmjaIO.getCoverage(), lextrevIO.getCoverage())
			|| isNE(covtmjaIO.getRider(), lextrevIO.getRider())) {
				lextrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isGT(lextrevIO.getTranno(), chdrpf.getTranno())) {
					lextrevIO.setStatuz(varcom.endp);
				}
			}
		}
        readFlupal1500();
	}

	/**
	* <pre>
	*5650-CHECK-RACT SECTION.                                 <R96REA>
	*************************                                 <R96REA>
	*5650-READ-RACT.                                          <R96REA>
	****                                                      <R96REA>
	**** MOVE CHDRMJA-CHDRCOY        TO RACT-CHDRCOY.         <R96REA>
	**** MOVE CHDRMJA-CHDRNUM        TO RACT-CHDRNUM.         <R96REA>
	**** MOVE SR576-LIFE             TO RACT-LIFE.            <R96REA>
	**** MOVE SR576-COVERAGE         TO RACT-COVERAGE.        <R96REA>
	**** MOVE SR576-RIDER            TO RACT-RIDER.           <R96REA>
	**** MOVE RACTREC                TO RACT-FORMAT.          <R96REA>
	**** MOVE BEGN                   TO RACT-FUNCTION.        <R96REA>
	**** CALL 'RACTIO' USING RACT-PARAMS.                     <R96REA>
	**** IF RACT-STATUZ              NOT = O-K AND            <R96REA>
	****                             NOT = ENDP               <R96REA>
	****    MOVE RACT-PARAMS         TO SYSR-PARAMS           <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	****                                                      <R96REA>
	**** IF CHDRMJA-CHDRCOY   NOT = RACT-CHDRCOY              <R96REA>
	****  OR CHDRMJA-CHDRNUM  NOT = RACT-CHDRNUM              <R96REA>
	****  OR SR576-LIFE       NOT = RACT-LIFE                 <R96REA>
	****  OR SR576-COVERAGE   NOT = RACT-COVERAGE             <R96REA>
	****  OR SR576-RIDER      NOT = RACT-RIDER                <R96REA>
	****    MOVE ENDP                TO RACT-STATUZ.          <R96REA>
	**** IF RACT-STATUZ              = ENDP                   <R96REA>
	****    MOVE ' '                 TO SR576-RATYPIND        <R96REA>
	**** ELSE                                                 <R96REA>
	****    MOVE '+'                 TO SR576-RATYPIND.       <R96REA>
	****                                                      <R96REA>
	****                                                      <R96REA>
	**Extra check in here to see if any RACT records associate<R96REA>
	***this component have been amended whilst we have been he<R96REA>
	***component Add/Modify - check TRANNOs on RACTRCC records<R96REA>
	***against the current TRANNO held on the Contract Header <R96REA>
	***are currently using in the CHDRMJA logical.            <R96REA>
	****                                                      <R96REA>
	**** IF RACT-STATUZ              NOT = ENDP               <R96REA>
	****     IF RACT-TRANNO          > CHDRMJA-TRANNO         <R96REA>
	****         MOVE 'Y'            TO WSAA-RACT-UPDATES     <R96REA>
	****         MOVE ENDP           TO RACT-STATUZ           <R96REA>
	****         GO TO 5690-EXIT                              <R96REA>
	****     END-IF                                           <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	**** MOVE SPACES                 TO RACTRCC-PARAMS.       <R96REA>
	**** MOVE CHDRMJA-CHDRCOY        TO RACTRCC-CHDRCOY.      <R96REA>
	**** MOVE CHDRMJA-CHDRNUM        TO RACTRCC-CHDRNUM.      <R96REA>
	**** MOVE SR576-LIFE             TO RACTRCC-LIFE.         <R96REA>
	**** MOVE SR576-COVERAGE         TO RACTRCC-COVERAGE.     <R96REA>
	**** MOVE SR576-RIDER            TO RACTRCC-RIDER.        <R96REA>
	**** MOVE SPACES                 TO RACTRCC-RASNUM.       <R96REA>
	**** MOVE SPACES                 TO RACTRCC-RATYPE.       <R96REA>
	**** MOVE SPACES                 TO RACTRCC-VALIDFLAG.    <R96REA>
	**** MOVE RACTRCCREC             TO RACTRCC-FORMAT.       <R96REA>
	**** MOVE BEGN                   TO RACTRCC-FUNCTION.     <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTRCC-STATUZ           NOT = O-K                <R96REA>
	****    AND RACTRCC-STATUZ       NOT = ENDP               <R96REA>
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS           <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	****                                                      <R96REA>
	**** IF CHDRMJA-CHDRCOY          NOT = RACTRCC-CHDRCOY    <R96REA>
	****  OR CHDRMJA-CHDRNUM         NOT = RACTRCC-CHDRNUM    <R96REA>
	****  OR SR576-LIFE              NOT = RACTRCC-LIFE       <R96REA>
	****  OR SR576-COVERAGE          NOT = RACTRCC-COVERAGE   <R96REA>
	****  OR SR576-RIDER             NOT = RACTRCC-RIDER      <R96REA>
	****     MOVE ENDP               TO RACTRCC-STATUZ        <R96REA>
	****  END-IF.                                             <R96REA>
	****                                                      <R96REA>
	**** PERFORM                     UNTIL RACTRCC-STATUZ = EN<R96REA>
	****                                                      <R96REA>
	****     MOVE NEXTR              TO RACTRCC-FUNCTION      <R96REA>
	****                                                      <R96REA>
	****     CALL 'RACTRCCIO'        USING RACTRCC-PARAMS     <R96REA>
	****                                                      <R96REA>
	****     IF RACTRCC-STATUZ       NOT = O-K                <R96REA>
	****        AND RACTRCC-STATUZ   NOT = ENDP               <R96REA>
	****         MOVE RACTRCC-PARAMS TO SYSR-PARAMS           <R96REA>
	****         MOVE RACTRCC-STATUZ TO SYSR-STATUZ           <R96REA>
	****         PERFORM 600-FATAL-ERROR                      <R96REA>
	****     END-IF                                           <R96REA>
	****                                                      <R96REA>
	****     IF CHDRMJA-CHDRCOY      NOT = RACTRCC-CHDRCOY    <R96REA>
	****      OR CHDRMJA-CHDRNUM     NOT = RACTRCC-CHDRNUM    <R96REA>
	****      OR SR576-LIFE          NOT = RACTRCC-LIFE       <R96REA>
	****      OR SR576-COVERAGE      NOT = RACTRCC-COVERAGE   <R96REA>
	****      OR SR576-RIDER         NOT = RACTRCC-RIDER      <R96REA>
	****        MOVE ENDP            TO RACTRCC-STATUZ        <R96REA>
	****     ELSE                                             <R96REA>
	****         IF RACTRCC-TRANNO   > CHDRMJA-TRANNO         <R96REA>
	****             MOVE 'Y'        TO WSAA-RACT-UPDATES     <R96REA>
	****             MOVE ENDP       TO RACTRCC-STATUZ        <R96REA>
	****         END-IF                                       <R96REA>
	****     END-IF                                           <R96REA>
	****                                                      <R96REA>
	**** END-PERFORM.                                         <R96REA>
	****                                                      <R96REA>
	*5690-EXIT.                                               <R96REA>
	**** EXIT.                                                <R96REA>
	* </pre>
	*/
protected void recordToScreen6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covrRecord6010();
					fieldsToScreen6020();
				case covtExist6025: 
					covtExist6025();
					enquiryProtect6030();
					optionsExtras603a();
					premiumBreakdown603c();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covrRecord6010()
	{
		/* Move the COVR Details to the COVT record as the Coding moves*/
		/*   the COVT fields to the screen.*/
		if (isEQ(covtmjaIO.getStatuz(), varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			covtmjaIO.setSex01(wsaaSex);
			covtmjaIO.setSex02(wsbbSex);
			covtmjaIO.setAnbAtCcd01(wsaaAnbAtCcd);
			covtmjaIO.setAnbAtCcd02(wsbbAnbAtCcd);
			covtmjaIO.setCrtable(covrpf.getCrtable());
			covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
			covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
			covtmjaIO.setBenCessDate(covrpf.getBenCessDate());
			covtmjaIO.setRiskCessAge(covrpf.getRiskCessAge());
			covtmjaIO.setPremCessAge(covrpf.getPremCessAge());
			covtmjaIO.setBenCessAge(covrpf.getBenCessAge());
			covtmjaIO.setRiskCessTerm(covrpf.getRiskCessTerm());
			covtmjaIO.setPremCessTerm(covrpf.getPremCessTerm());
			covtmjaIO.setBenCessTerm(covrpf.getBenCessTerm());
			covtmjaIO.setReserveUnitsDate(covrpf.getReserveUnitsDate());
			covtmjaIO.setReserveUnitsInd(covrpf.getReserveUnitsInd());
			covtmjaIO.setSumins(covrpf.getSumins());
			covtmjaIO.setInstprem(covrpf.getInstprem());
			covtmjaIO.setZbinstprem(covrpf.getZbinstprem());
			covtmjaIO.setZlinstprem(covrpf.getZlinstprem());
			/*BRD-306 START */
			covtmjaIO.setLoadper(covrpf.getLoadper());
			covtmjaIO.setRateadj(covrpf.getRateadj());
			covtmjaIO.setFltmort(covrpf.getFltmort());
			covtmjaIO.setPremadj(covrpf.getPremadj());
			covtmjaIO.setAgeadj(covrpf.getAgeadj());
			/*BRD-306 END */
			if(stampDutyflag){ 
				if(isNE(covrpf.getZclstate(),SPACES)){
					covtmjaIO.setZclstate(covrpf.getZclstate());
				}
				if(isNE(covrpf.getZstpduty01(),ZERO)&& isEQ(sv.zstpduty01,ZERO)){
					covtmjaIO.setZstpduty01(covrpf.getZstpduty01());
				}
			}
			covtmjaIO.setMortcls(covrpf.getMortcls());
			covtmjaIO.setLiencd(covrpf.getLiencd());
			covtmjaIO.setJlife(covrpf.getJlife());
			covtmjaIO.setEffdate(covrpf.getCrrcd());
			/*  MOVE CHDRMJA-BILLFREQ       TO COVTMJA-BILLFREQ           */
			/*  MOVE CHDRMJA-BILLCHNL       TO COVTMJA-BILLCHNL           */
			covtmjaIO.setBillfreq(payrIO.getBillfreq());
			covtmjaIO.setBillchnl(payrIO.getBillchnl());
			covtmjaIO.setSingp(covrpf.getSingp());
			covtmjaIO.setBappmeth(covrpf.getBappmeth());
			sv.statFund.set(covrpf.getStatFund());
			sv.statSect.set(covrpf.getStatSect());
			sv.statSubsect.set(covrpf.getStatSubsect());
		}
		covtmjaIO.setPolinc(chdrpf.getPolinc());
		covtmjaIO.setPayrseqno(covrpf.getPayrseqno());
	}

protected void fieldsToScreen6020()
	{
		if (isEQ(covtmjaIO.getJlife(), "01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
		if (isEQ(covtmjaIO.getStatuz(), varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.covtExist6025);
		}
		/* If a summarised record then breakout the Inst.Premium & Sumin*/
		/*   for the number of summarised policies within the Plan, this*/
		/*   is only applicable if a COVT does not already exist in which*/
		/*   case it has already been broken down before.*/
		/* Note - the 1st policy in the summarised record will contain*/
		/*        any division remainder.*/
		if (isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix, 1)) {
				compute(sv.sumin, 2).set((sub(covtmjaIO.getSumins(), (div(mult(covtmjaIO.getSumins(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
				/*          COMPUTE SR576-INST-PREM =                              */
				compute(sv.linstamt, 2).set((sub(covtmjaIO.getInstprem(), (div(mult(covtmjaIO.getInstprem(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
				compute(sv.zbinstprem, 2).set((sub(covtmjaIO.getZbinstprem(), (div(mult(covtmjaIO.getZbinstprem(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
				compute(sv.zlinstprem, 2).set((sub(covtmjaIO.getZlinstprem(), (div(mult(covtmjaIO.getZlinstprem(), (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
			}
			else {
				compute(sv.sumin, 3).setRounded(div(covtmjaIO.getSumins(), chdrpf.getPolsum()));
				compute(sv.linstamt, 3).setRounded(div(covtmjaIO.getInstprem(), chdrpf.getPolsum()));
				compute(sv.zbinstprem, 3).setRounded(div(covtmjaIO.getZbinstprem(), chdrpf.getPolsum()));
				compute(sv.zlinstprem, 3).setRounded(div(covtmjaIO.getZlinstprem(), chdrpf.getPolsum()));
			}
		}
		else {
			sv.sumin.set(covtmjaIO.getSumins());
			sv.zbinstprem.set(covtmjaIO.getZbinstprem());
			sv.zlinstprem.set(covtmjaIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtmjaIO.getLoadper());
			sv.rateadj.set(covtmjaIO.getRateadj());
			sv.fltmort.set(covtmjaIO.getFltmort());
			sv.premadj.set(covtmjaIO.getPremadj());
			sv.adjustageamt.set(covtmjaIO.getAgeadj());
			/*BRD-306 END */
			if(stampDutyflag){
				if(isNE(covtmjaIO.getZstpduty01(),ZERO)&& isEQ(sv.zstpduty01,ZERO)){
					sv.zstpduty01.set(covtmjaIO.getZstpduty01());
				}
				if(isNE(covtmjaIO.getZclstate(),SPACES)){
					stateCode=covtmjaIO.getZclstate().toString();
				}
			}

			/*       MOVE COVTMJA-INSTPREM TO SR576-INST-PREM.                 */
			sv.linstamt.set(covtmjaIO.getInstprem());
		}
		if (isLTE(sv.planSuffix, chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix, 1)) {
				compute(wsaaOldSumins, 0).set((sub(wsaaOldSumins, (div(mult(wsaaOldSumins, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
				compute(wsaaOldPrem, 2).set((sub(wsaaOldPrem, (div(mult(wsaaOldPrem, (sub(chdrpf.getPolsum(), 1))), chdrpf.getPolsum())))));
			}
			else {
				compute(wsaaOldSumins, 1).setRounded(div(wsaaOldSumins, chdrpf.getPolsum()));
				compute(wsaaOldPrem, 3).setRounded(div(wsaaOldPrem, chdrpf.getPolsum()));
			}
		}
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560
		}
	}

protected void covtExist6025()
	{
		if (isEQ(covtmjaIO.getStatuz(), varcom.oK)) {
			sv.sumin.set(covtmjaIO.getSumins());
			sv.zbinstprem.set(covtmjaIO.getZbinstprem());
			sv.zlinstprem.set(covtmjaIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtmjaIO.getLoadper());
			sv.rateadj.set(covtmjaIO.getRateadj());
			sv.fltmort.set(covtmjaIO.fltmort);
			sv.premadj.set(covtmjaIO.getPremadj());
			sv.adjustageamt.set(covtmjaIO.getAgeadj());
			/*BRD-306 END */
			if(stampDutyflag){
				if(isNE(covtmjaIO.getZstpduty01(),ZERO)&& isEQ(sv.zstpduty01,ZERO)){
					sv.zstpduty01.set(covtmjaIO.getZstpduty01());
				}
				if(isNE(covtmjaIO.getZclstate(),SPACES)){
					stateCode=covtmjaIO.getZclstate().toString();
				}
			}

			/*       MOVE COVTMJA-INSTPREM TO SR576-INST-PREM.                 */
			sv.linstamt.set(covtmjaIO.getInstprem());
			if (addComp.isTrue()
			|| addApp.isTrue()
			|| revComp.isTrue()) {
				r200ReadCprp();
				if (isEQ(cprpIO.getStatuz(), varcom.oK)) {
					sv.instPrem.set(cprpIO.getInstprem());
				}
				else {
					sv.instPrem.set(covtmjaIO.getInstprem());
				}
			}
			else {
				sv.instPrem.set(covtmjaIO.getInstprem());
			}
		}
		sv.liencd.set(covtmjaIO.getLiencd());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.premCessDate.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.riskCessDate.set(covtmjaIO.getRiskCessDate());
		sv.riskCessAge.set(covtmjaIO.getRiskCessAge());
		sv.riskCessTerm.set(covtmjaIO.getRiskCessTerm());
		sv.benCessDate.set(covtmjaIO.getBenCessDate());
		sv.benCessAge.set(covtmjaIO.getBenCessAge());
		sv.benCessTerm.set(covtmjaIO.getBenCessTerm());
		setupBonus1300();
		sv.bappmeth.set(covtmjaIO.getBappmeth());
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560
		}
	}

protected void enquiryProtect6030()
	{
		/* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to*/
		/*   protect all input  capable  fields  except  the  indicators*/
		/*   controlling where to  switch  to  next  (options and extras*/
		/*   indicator).*/
		/*    IF WSSP-FLAG                = 'I'                            */
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
		/* If we have a record to display then we must load all the tables*/
		/*   required for Validation in the 2000-section.*/
		if (!addComp.isTrue()) {
			tableLoads7000();
		}
		/* If the  maximum  and  minimum  sum insured amounts are both     */
		/* zero, non-display and protect the sum insured field. If the     */
		/* minimum and maximum are both the same, display the amount       */
		/* protected. Otherwise allow input.                               */
		if (isEQ(t5606rec.sumInsMax, 0)
		&& isEQ(t5606rec.sumInsMin, 0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5606rec.sumInsMax);
		}
	}

protected void optionsExtras603a()
	{
		/* If options and extras are  not  allowed (as defined by T5606)   */
		/*   non-display and protect the fields.                           */
		/* Otherwise,  read the  options and extras  details  for  the     */
		/*   current coverage/rider. If any records  exist, put a '+' in   */
		/*   the Options/Extras indicator (to show that there are some).   */
		if (isEQ(t5606rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
	}

	/**
	* <pre>
	*603B-REASSURANCE.                                        <R96REA>
	****                                                      <R96REA>
	**REASSURANCE                                             <R96REA>
	****                                                      <R96REA>
	**If reassurance is not allowed                           <R96REA>
	**non-display and protect the field.                      <R96REA>
	****                                                      <R96REA>
	**Otherwise,  read the  reassurance details  for  the  cur<R96REA>
	**coverage/rider.  If any  records  exist, put a '+' in  t<R96REA>
	**reassurance indicator (to show that there is one).      <R96REA>
	****                                                      <R96REA>
	**** IF T5687-RIIND              = 'N' OR SPACES          <R96REA>
	****    MOVE 'Y'                 TO SR576-RATYPIND-OUT (ND<R96REA>
	**** ELSE                                                 <R96REA>
	****    PERFORM 5650-CHECK-RACT.                          <R96REA>
	****                                                      <R96REA>
	****                                                      <R96REA>
	* </pre>
	*/
protected void premiumBreakdown603c()
	{
		/* As per other 'window box' checks the premium breakdown file     */
		/* needs to be checked for the existance of a record. Store the    */
		/* values here so that a general check can be made prior to        */
		/* displaying the screen in all cases .....                        */
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/*EXIT*/
	}

protected void r200ReadCprp()
	{
		r200Begin();
	}

protected void r200Begin()
	{
		cprpIO.setRecKeyData(SPACES);
		cprpIO.setChdrpfx(chdrpf.getChdrpfx());
		cprpIO.setChdrcoy(chdrpf.getChdrcoy());
		cprpIO.setChdrnum(chdrpf.getChdrnum());
		cprpIO.setLife(covtmjaIO.getLife());
		cprpIO.setCoverage(covtmjaIO.getCoverage());
		cprpIO.setRider(covtmjaIO.getRider());
		cprpIO.setFunction("READR ");
		SmartFileCode.execute(appVars, cprpIO);
		if (isNE(cprpIO.getStatuz(), varcom.oK)
		&& isNE(cprpIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cprpIO.getParams());
			fatalError600();
		}
	}

protected void tableLoads7000()
	{
		t5687Load7010();
		t5671Load7020();
		editRules7030();
		t5606Load7040();
		t5667Load7045();
		t5675Load7050();
	}

protected void t5687Load7010()
	{
		/* General Coverage/Rider details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtmjaIO.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covtmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	**** IF T5687-RIIND              = 'N' OR SPACES          <R96REA>
	****    MOVE SPACES              TO SR576-RATYPIND        <R96REA>
	****    MOVE 'Y'                 TO SR576-RATYPIND-OUT(ND)<R96REA>
	* </pre>
	*/
protected void t5671Load7020()
	{
		/* Coverage/Rider Switching.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(covtmjaIO.getCrtable());
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
	}

protected void editRules7030()
	{
		/* Find the Edit rules associated with the program.*/
		if (isEQ(t5671rec.pgm[1], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
	}

protected void t5606Load7040()
	{
		/* Term Component edit rules.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5606);
		/*MOVE CHDRMJA-CNTCURR        TO WSBB-CURRENCY.                */
		wsbbCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5606, itdmIO.getItemtabl())
		|| isNE(wsbbTranCurrency, itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)) {
			t5606rec.t5606Rec.set(itdmIO.getGenarea());
		}
		else {
			t5606rec.t5606Rec.set(SPACES);
			t5606rec.ageIssageFrms.fill("0");
			t5606rec.ageIssageTos.fill("0");
			t5606rec.termIssageFrms.fill("0");
			t5606rec.termIssageTos.fill("0");
			t5606rec.premCessageFroms.fill("0");
			t5606rec.premCessageTos.fill("0");
			t5606rec.premCesstermFroms.fill("0");
			t5606rec.premCesstermTos.fill("0");
			t5606rec.riskCessageFroms.fill("0");
			t5606rec.riskCessageTos.fill("0");
			t5606rec.riskCesstermFroms.fill("0");
			t5606rec.riskCesstermTos.fill("0");
			t5606rec.benCessageFroms.fill("0");
			t5606rec.benCessageTos.fill("0");
			t5606rec.benCesstermFrms.fill("0");
			t5606rec.benCesstermTos.fill("0");
			t5606rec.sumInsMax.set(ZERO);
			t5606rec.sumInsMin.set(ZERO);
			if (isEQ(scrnparams.errorCode, SPACES)) {
				scrnparams.errorCode.set(errorsInner.f334);
			}
		}
		if(isEQ(t5606rec.waitperiod, SPACES)){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
		}
		else{
			waitperiodFlag=true;
		}
		if(isEQ(t5606rec.bentrm, SPACES)){
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
		}
		else{
			bentrmFlag=true;
		}
		if(isEQ(t5606rec.poltyp, SPACES)){
			sv.poltypOut[varcom.nd.toInt()].set("Y");
		}
		else{
			poltypFlag=true;
		}
		if(isEQ(t5606rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else{
			prmbasisFlag=true;
		}
	}

protected void t5667Load7045()
	{
		/* Read the latest premium tollerance allowed.                     */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
	}

protected void t5675Load7050()
	{
		/* Premium Calculation Methods.*/
		/* To determine  which premium calculation method to use decide*/
		/*   whether or not it  is  a Single or Joint-life case (it is a*/
		/*   joint life case,  the  joint  life record was found above).*/
		/* Then:*/
		/*  - if it  is  a  single-life  case use, the single-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the joint-life indicator (from T5687) is blank, and*/
		/*       if  it  is  a Joint-life case, use the joint-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the  Joint-life  indicator  is  'N',  then  use the*/
		/*       Single-method.  But, if there is a joint-life (this*/
		/*       must be  a  rider  to have got this far) prompt for*/
		/*       the joint  life  indicator  to determine which life*/
		/*       the rider is to attach to.  In all other cases, the*/
		/*       joint life  indicator  should  be non-displayed and*/
		/*       protected.  The  age to be used for validation will*/
		/*       be the age  of the main or joint life, depending on*/
		/*       the one selected.*/
		if (singlif.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(itemIO.getItemitem(), SPACES)) {
			/* MOVE 'Y'                 TO PREM-REQD                     */
			wsaaPremStatuz.set("Y");
		}
		else {
			calcPremium5200();
		}
		/*EXIT*/
	}

protected void getAnny8000()
	{
		read8010();
	}

protected void read8010()
	{
		annyIO.setChdrcoy(chdrpf.getChdrcoy());
		annyIO.setChdrnum(chdrpf.getChdrnum());
		annyIO.setLife(covrpf.getLife());
		annyIO.setCoverage(covrpf.getCoverage());
		annyIO.setRider(covrpf.getRider());
		if (isNE(wsaaPlanSuffix, ZERO)
		&& isLTE(chdrpf.getPolsum(), wsaaPlanSuffix)) {
			annyIO.setPlanSuffix(ZERO);
		}
		else {
			annyIO.setPlanSuffix(wsaaStorePlanSuffix);
		}
		annyIO.setFormat(formatsInner.annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
	}

protected void getNewAnnuityDetails8100()
	{
		checkForAnnt8110();
	}

protected void checkForAnnt8110()
	{
		anntmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		anntmjaIO.setChdrnum(chdrpf.getChdrnum());
		anntmjaIO.setLife(covrpf.getLife());
		anntmjaIO.setCoverage(covrpf.getCoverage());
		anntmjaIO.setRider(covrpf.getRider());
		anntmjaIO.setPlanSuffix(wsaaStorePlanSuffix);
		anntmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anntmjaIO);
		if (isNE(anntmjaIO.getStatuz(), varcom.oK)
		&& isNE(anntmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(anntmjaIO.getParams());
			syserrrec.statuz.set(anntmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anntmjaIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(anntmjaIO.getFreqann());
			premiumrec.advance.set(anntmjaIO.getAdvance());
			premiumrec.arrears.set(anntmjaIO.getArrears());
			premiumrec.guarperd.set(anntmjaIO.getGuarperd());
			premiumrec.intanny.set(anntmjaIO.getIntanny());
			premiumrec.capcont.set(anntmjaIO.getCapcont());
			premiumrec.withprop.set(anntmjaIO.getWithprop());
			premiumrec.withoprop.set(anntmjaIO.getWithoprop());
			premiumrec.ppind.set(anntmjaIO.getPpind());
			premiumrec.nomlife.set(anntmjaIO.getNomlife());
			premiumrec.dthpercn.set(anntmjaIO.getDthpercn());
			premiumrec.dthperco.set(anntmjaIO.getDthperco());
			return ;
		}
		/* If the ANNT record has not been found, read the ANNY record. */
		annyIO.setChdrcoy(chdrpf.getChdrcoy());
		annyIO.setChdrnum(chdrpf.getChdrnum());
		annyIO.setLife(covrpf.getLife());
		annyIO.setCoverage(covrpf.getCoverage());
		annyIO.setRider(covrpf.getRider());
		if (isNE(wsaaPlanSuffix, ZERO)
		&& isLTE(chdrpf.getPolsum(), wsaaPlanSuffix)) {
			annyIO.setPlanSuffix(ZERO);
		}
		else {
			annyIO.setPlanSuffix(wsaaStorePlanSuffix);
		}
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		premiumrec.freqann.set(annyIO.getFreqann());
		premiumrec.advance.set(annyIO.getAdvance());
		premiumrec.arrears.set(annyIO.getArrears());
		premiumrec.guarperd.set(annyIO.getGuarperd());
		premiumrec.intanny.set(annyIO.getIntanny());
		premiumrec.capcont.set(annyIO.getCapcont());
		premiumrec.withprop.set(annyIO.getWithprop());
		premiumrec.withoprop.set(annyIO.getWithoprop());
		premiumrec.ppind.set(annyIO.getPpind());
		premiumrec.nomlife.set(annyIO.getNomlife());
		premiumrec.dthpercn.set(annyIO.getDthpercn());
		premiumrec.dthperco.set(annyIO.getDthperco());
	}

protected void readPovr9000()
	{
		start9010();
	}

protected void start9010()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)
		&& isNE(povrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void pbindExe9100()
	{
		start9110();
	}

protected void start9110()
	{
		/*  - Keep the POVR/CHDR record for Premium Breakdown enquiry.     */
		/*    Ensure we get the latest one.                                */
		readPovr9000();
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(), wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(), wsaaPovrLife)
		|| isNE(povrIO.getCoverage(), wsaaPovrCoverage)
		|| isNE(povrIO.getRider(), wsaaPovrRider)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		//ILB-456
		chdrpfDAO.setCacheObject(chdrpf);
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.pbind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the     */
		/*    next program.                                                */
		gensswrec.function.set("E");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet9200()
	{
		start9210();
	}

protected void start9210()
	{
		/* Release the POVR records as no longer required ....             */
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.pbind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

	/**
	* <pre>
	*9300-CHECK-T5602 SECTION.                                   <052>
	*9310-START.                                                 <052>
	* Read the SUM Coverage table & check if the coverage is          
	* a WOP. If so then non-display and protect the benefit amount    
	* and benefit frequency description on screen.....                
	*                                                            <052>
	*    MOVE SPACES                 TO ITEM-PARAMS.             <052>
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.            <052>
	*    MOVE CHDRMJA-CHDRCOY        TO ITEM-ITEMCOY.            <052>
	*    MOVE T5602                  TO ITEM-ITEMTABL.           <052>
	*    MOVE WSAA-CRTABLE           TO ITEM-ITEMITEM.           <052>
	*    MOVE ITEMREC                TO ITEM-FORMAT.             <052>
	*    MOVE READR                  TO ITEM-FUNCTION.           <052>
	*    CALL 'ITEMIO' USING ITEM-PARAMS.                        <052>
	*                                                            <052>
	*    IF ITEM-STATUZ = O-K                                    <052>
	*       MOVE 'Y'                 TO WSAA-SUMFLAG             <052>
	*       MOVE ITEM-GENAREA        TO T5602-T5602-REC          <052>
	*       IF T5602-INDIC-01 NOT = SPACE                        <052>
	*          MOVE ZERO             TO SR576-SUMIN              <052>
	*          MOVE 'Y'              TO SR576-SUMIN-OUT (ND)     <052>
	*                                   SR576-SUMIN-OUT (PR)     <052>
	*       END-IF                                               <052>
	*    ELSE                                                    <052>
	*       MOVE SPACE               TO WSAA-SUMFLAG.            <052>
	*9390-EXIT.                                                  <052>
	*    EXIT.                                                   <052>
	* </pre>
	*/
protected void vf2MaltPovr9400()
	{
		start9410();
	}

protected void start9410()
	{
		/* Get the premium breakdown record .......                        */
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* Update the valid flag to '2' ......                             */
		povrIO.setValidflag("2");
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void a100CheckLimit()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		chkrlrec.clntnum.set(sv.lifcnum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.currcd);
		chkrlrec.cnttype.set(chdrpf.getCnttype());
		chkrlrec.crtable.set(covtmjaIO.getCrtable());
		chkrlrec.sumins.set(sv.sumin);
		chkrlrec.life.set(covtmjaIO.getLife());
		chkrlrec.chdrnum.set(chdrpf.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz, varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.suminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}

protected void a200CheckCalcTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a200Start();
				case a200CallTaxSubr: 
					a200CallTaxSubr();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a200Start()
	{
		wsaaProtax.set(0);
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.a200CallTaxSubr);
		}
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covtmjaIO.getCrtable());
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
		wsaaFirstTaxCalc.set("N");
	}

protected void a200CallTaxSubr()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			txcalcrec.chdrnum.set(chdrpf.getChdrnum());
			txcalcrec.life.set(covtmjaIO.getLife());
			txcalcrec.coverage.set(covtmjaIO.getCoverage());
			txcalcrec.rider.set(covtmjaIO.getRider());
			txcalcrec.planSuffix.set(covtmjaIO.getPlanSuffix());
			txcalcrec.crtable.set(covtmjaIO.getCrtable());
			txcalcrec.cnttype.set(chdrpf.getCnttype());
			txcalcrec.register.set(chdrpf.getReg());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrpf.getCntcurr());
			wsaaCntCurr.set(chdrpf.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				txcalcrec.amountIn.set(sv.linstamt);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrpf.getBtdate());
			txcalcrec.tranno.set(chdrpf.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(0);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				compute(sv.taxamt01, 2).set(add(wsaaTaxamt, sv.linstamt));
				wsaaProtax.set(0);
				if (addComp.isTrue()
				&& isNE(sv.effdate, chdrpf.getBtdate())) {
					compute(wsaaProtaxRate, 6).setRounded(div(sv.instPrem, sv.linstamt));
					compute(wsaaProtax, 6).setRounded(mult(wsaaTaxamt, wsaaProtaxRate));
					compute(sv.taxamt02, 2).set(add(wsaaProtax, sv.instPrem));
				}
				if (isEQ(sv.taxind, SPACES)) {
					sv.taxind.set("+");
				}
			}
		}
		sv.taxamt01Out[varcom.pr.toInt()].set("Y");
		sv.instPremOut[varcom.pr.toInt()].set("Y");
	}

protected void a250ProrateTax()
	{
		/*A250-START*/
		wsaaProtax.set(0);
		compute(wsaaProtaxRate, 6).setRounded((div(sv.instPrem, sv.linstamt)));
		compute(wsaaProtax, 6).setRounded(mult(wsaaTaxamt, wsaaProtaxRate));
		compute(sv.taxamt02, 2).set(add(wsaaProtax, sv.instPrem));
		/*A250-EXIT*/
	}

protected void a300ReadTr52e()
	{
		a310Start();
	}

protected void a310Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrpf.getPtdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void validateOccupationOrOccupationClass() 
	{
		readTa610();
		String occupation = lifepf.getOccup();//IJTI-1410
		if( occupation != null && !occupation.trim().isEmpty()) {
			for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
					&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
				if(isEQ(occupation,ta610rec.occclassdes[wsaaCount.toInt()])){
					isFollowUpRequired = true;
					scrnparams.errorCode.set(errorsInner.rrsu);
					break;
				}
			}
			
			if(!isFollowUpRequired){
				getOccupationClass2900(occupation);
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu);
						break;
					}
				}	
			}
		}
	}

protected void getOccupationClass2900(String occupation) 
	{
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itemDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
	}

protected void readTa610() 
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tA610);
		itempf.setItemitem(covtmjaIO.getCrtable().toString());
		itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
		itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
		itempfList2  = itemDAO.findByItemDates(itempf);	//ICIL-1494
		if (itempfList2.size()>0 && itempfList2.get(0).getGenarea()!=null) {
			ta610rec.tA610Rec.set(StringUtil.rawToString(itempfList2.get(0).getGenarea()));
		}
	}

protected void createFollowUps3400()
	{
		try {
			fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());//IJTI-1410
			fupno = fluppfAvaList.size();	//ICIL-1494
			writeFollowUps3410();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void writeFollowUps3410()
	{
	for (wsaaCount.set(1); !(isGT(wsaaCount, 5)) && isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)) {
		boolean entryFlag = false;
		//ILIFE-9209 STARTS
		if(fluppfAvaList!=null && !fluppfAvaList.isEmpty()) {
			for (Fluppf fluppfdata : fluppfAvaList) {
				if(isEQ(fluppfdata.getFupCde(),ta610rec.fupcdes[wsaaCount.toInt()])) 
					entryFlag=true;
			}
				
		}
		//ILIFE-9209 ENDS
		if(!entryFlag) {		//ILIFE-9209 
		writeFollowUp3500();
		}
	}
		fluppfDAO.insertFlupRecord(fluppfList);
	}

protected void writeFollowUp3500()
	{
		try {
			fluppf = new Fluppf();
			lookUpStatus3510();
			lookUpDescription3520();
			writeRecord3530();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void lookUpStatus3510()
	{
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itemDAO.getItempfRecord(itempf);
	
		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520()
	{
		fupDescpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	}

private void writeRecord3530() 
	{
		fupno++;
		fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
		fluppf.setChdrnum(chdrpf.getChdrnum());
		setPrecision(fupno, 0);
		fluppf.setFupNo(fupno);
		fluppf.setLife("01");
		fluppf.setTranNo(chdrpf.getTranno());
		fluppf.setFupDt(wsaaToday.toInt());
		fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString().trim());
		fluppf.setFupTyp('P');
		fluppf.setFupRmk(fupDescpf.getLongdesc());
		fluppf.setjLife("00");
		fluppf.setTrdt(varcom.vrcmDate.toInt());
		fluppf.setTrtm(varcom.vrcmTime.toInt());
		fluppf.setUserT(varcom.vrcmUser.toInt());
		fluppf.setEffDate(wsaaToday.toInt());
		fluppf.setCrtUser(varcom.vrcmUser.toString());
		fluppf.setCrtDate(wsaaToday.toInt());
		fluppf.setLstUpUser(varcom.vrcmUser.toString());
		fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
		fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		fluppfList.add(fluppf);
	}


/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultBa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultBa = new Validator(wsaaDefaultBa, "Y");
	private Validator nodefBa = new Validator(wsaaDefaultBa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 4);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
	private FixedLengthStringData wsaaDefaultBt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 5);
	private Validator defaultBt = new Validator(wsaaDefaultBt, "Y");
	private Validator nodefBt = new Validator(wsaaDefaultBt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 

	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f334 = new FixedLengthStringData(4).init("F334");
	private FixedLengthStringData f404 = new FixedLengthStringData(4).init("F404");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData ev01 = new FixedLengthStringData(4).init("EV01");
	private FixedLengthStringData h033 = new FixedLengthStringData(4).init("H033");
	private FixedLengthStringData h044 = new FixedLengthStringData(4).init("H044");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	private FixedLengthStringData d028 = new FixedLengthStringData(4).init("D028");
	private FixedLengthStringData d029 = new FixedLengthStringData(4).init("D029");
	private FixedLengthStringData d030 = new FixedLengthStringData(4).init("D030");
	private FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData f862 = new FixedLengthStringData(4).init("F862");
	private FixedLengthStringData rl33 = new FixedLengthStringData(4).init("RL33");
	private FixedLengthStringData rfw4 = new FixedLengthStringData(4).init("RFW4");
	private FixedLengthStringData rfw5 = new FixedLengthStringData(4).init("RFW5");
	private FixedLengthStringData rrj4 = new FixedLengthStringData(4).init("RRJ4");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData th506 = new FixedLengthStringData(5).init("TH506");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData th528 = new FixedLengthStringData(5).init("TH528");//fwang3 ICIL-560
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC   ");
	private FixedLengthStringData covrrgwrec = new FixedLengthStringData(10).init("COVRRGWREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
	private FixedLengthStringData annyrec = new FixedLengthStringData(10).init("ANNYREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC   ");
	private FixedLengthStringData cprprec = new FixedLengthStringData(10).init("CPRPREC");
}

protected void lextpf3010CustomerSpecific(){
	
}
protected void  getOccupVpms2011CustomerSpecific(){ }


public Premiumrec getPremiumrec() {
	return new Premiumrec();
}

}
