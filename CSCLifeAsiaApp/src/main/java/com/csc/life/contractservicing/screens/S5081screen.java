package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5081screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5081ScreenVars sv = (S5081ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5081screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5081ScreenVars screenVars = (S5081ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.cownnum.setClassString("");
		 /* ILIFE-7277-start  */
		screenVars.cownnum2.setClassString("");
		screenVars.cownnum3.setClassString("");
		screenVars.cownnum4.setClassString("");
		screenVars.cownnum5.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.jownnum.setClassString("");
		screenVars.jownername.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.clntwin.setClassString("");
		/* ILIFE-7277-start  */
		screenVars.clntwin2.setClassString("");
		screenVars.clntwin3.setClassString("");
		screenVars.clntwin4.setClassString("");
		screenVars.clntwin5.setClassString("");
		/* ILIFE-7277-end  */
		screenVars.clientnum.setClassString("");
		screenVars.clientname.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.resndesc.setClassString("");
		screenVars.clntname.setClassString("");
		/* ILIFE-7277-start  */
		screenVars.clntname2.setClassString("");
		screenVars.clntname3.setClassString("");
		screenVars.clntname4.setClassString("");
		screenVars.clntname5.setClassString("");
		screenVars.relationwithlife.setClassString("");
		screenVars.relationwithlifejoint.setClassString("");
		/* ILIFE-7277-end  */
	}

/**
 * Clear all the variables in S5081screen
 */
	public static void clear(VarModel pv) {
		S5081ScreenVars screenVars = (S5081ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.cownnum.clear();
		screenVars.cownnum2.clear();
		screenVars.cownnum3.clear();
		screenVars.cownnum4.clear();
		screenVars.cownnum5.clear();
		screenVars.ownername.clear();
		screenVars.jownnum.clear();
		screenVars.jownername.clear();
		screenVars.agntnum.clear();
		screenVars.agentname.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.billfreq.clear();
		screenVars.mop.clear();
		screenVars.clntwin.clear();
		screenVars.clntwin2.clear();
		screenVars.clntwin3.clear();
		screenVars.clntwin4.clear();
		screenVars.clntwin5.clear();
		screenVars.clientnum.clear();
		screenVars.clientname.clear();
		screenVars.reasoncd.clear();
		screenVars.resndesc.clear();
		screenVars.clntname.clear();
		screenVars.clntname2.clear();
		screenVars.clntname3.clear();
		screenVars.clntname4.clear();
		screenVars.clntname5.clear();
		screenVars.relationwithlife.clear();
		screenVars.relationwithlifejoint.clear();
		
	}
}
