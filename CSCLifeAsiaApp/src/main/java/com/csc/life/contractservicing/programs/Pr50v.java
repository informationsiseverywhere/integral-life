/*
 * File: Pr50v.java
 * Date: 30 August 2009 1:33:54
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50V.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import java.util.List;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.FupepfDAO;
import com.csc.life.contractservicing.dataaccess.model.Fupepf;
import com.csc.life.contractservicing.screens.Sr50vScreenVars;
import com.csc.life.newbusiness.dataaccess.FlupaltTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupenqTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.terminationclaims.dataaccess.FlupclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.FluprgpTableDAM;
import com.csc.smart.dataaccess.UsrlTableDAM;
import com.csc.smart.procedures.Usrname;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanckyr;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Usrnamerec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*    Follow-up Extended Text
*
*    This screen allows entry/update of the follow-up code's
*    extended text. Initially, the extended text displayed is
*    generated from table T5661, if one is available. The entire
*    text ot protions of it however, may be updated or overwritten.
*
*    Extended text created here will be written to a file. It
*    will carry the sequence number of the follow-up code it
*    is attahced to. Furthermore, it will have a second sequence
*    number to identifu the sequence of the extended text message
*    itself.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
***********************************************************************
* </pre>
*/
public class Pr50v extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50V");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData ix = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaDocseq = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaUserid = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaDesctabl = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaDescitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaScrsize = new PackedDecimalData(2, 0).init(15).setUnsigned();
	private PackedDecimalData wsaaSpcRrn = new PackedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t5661 = "T5661";
		/* ERRORS */
	private static final String f499 = "F499";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private FlupaltTableDAM flupaltIO = new FlupaltTableDAM();
	private FlupclmTableDAM flupclmIO = new FlupclmTableDAM();
	private FlupenqTableDAM flupenqIO = new FlupenqTableDAM();
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	private FluprgpTableDAM fluprgpIO = new FluprgpTableDAM();
	private UsrlTableDAM usrlIO = new UsrlTableDAM();
	private Sanckyr wsaaSanckey = new Sanckyr();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Usrnamerec usrnamerec = new Usrnamerec();
	private T5661rec t5661rec = new T5661rec();
	private Sr50vScreenVars sv = ScreenProgram.getScreenVars( Sr50vScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaFlupKeyInner wsaaFlupKeyInner = new WsaaFlupKeyInner();
	private ChdrpfDAO chdrpfDAO=getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
  	private List<Lifepf> lifeList;
  	private FupepfDAO fupepfDAO = getApplicationContext().getBean("fupepfDAO", FupepfDAO.class); 
  	private Fupepf fupepf;
  	private List<Fupepf> fupeList;
  	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
  	private List<Itempf> itempfList;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1580, 
		exit2090, 
		fluplnb3120, 
		flupalt3130, 
		flupclm3140, 
		fluprgp3150, 
		flupenq3160, 
		exit3190, 
		exit3390, 
		a290Exit
	}

	public Pr50v() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50v", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(" ");
		sv.subfileArea.set(" ");
		wsaaCount.set(0);
		wsaaRemainder.set(0);
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		a100CallSr50vio();
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*    Dummy field initilisation for prototype version.*/
		a200RetrvFlup();
		a300ReadChdrlnb();
		a400ReadLifelnb();
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cownnum.set(chdrpf.getCownnum());
		sv.jownnum.set(chdrpf.getJownnum());
		sv.fupcode.set(wsaaFlupKeyInner.wsaaFlupFupcde);
		sv.crtuser.set(wsaaFlupKeyInner.wsaaFlupCrtuser);
		sv.crtdate.set(wsaaFlupKeyInner.wsaaFlupCrtdate);
		sv.lstupuser.set(wsaaFlupKeyInner.wsaaFlupLstupuser);
		sv.zlstupdt.set(wsaaFlupKeyInner.wsaaFlupZlstupdt);
		wsaaDesctabl.set(t5688);
		wsaaDescitem.set(chdrpf.getCnttype());
		readDesc1200();
		if(descpf!=null)
		sv.ctypdesc.set(descpf.getLongdesc());
		else
		sv.ctypdesc.set("?");
		sv.longdesc.set(wsaaFlupKeyInner.wsaaFlupFupremk);
		wsaaClntnum.set(chdrpf.getCownnum());
		callNamadrs1300();
		sv.clntname01.set(namadrsrec.name);
		wsaaClntnum.set(chdrpf.getJownnum());
		callNamadrs1300();
		sv.clntname02.set(namadrsrec.name);
		wsaaUserid.set(wsaaFlupKeyInner.wsaaFlupCrtuser);
		getUsername1400();
		sv.name01.set(usrnamerec.username);
		wsaaUserid.set(wsaaFlupKeyInner.wsaaFlupLstupuser);
		getUsername1400();
		sv.name02.set(usrnamerec.username);
		loadSubfile1500();
	}

protected void readDesc1200()
	{
	descpf=descDAO.getdescData(smtpfxcpy.item.toString(), wsaaDesctabl.toString(), wsaaDescitem.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	}



protected void callNamadrs1300()
	{
		start1310();
	}

protected void start1310()
	{
		if (wsaaClntnum.equals(" ")) {
			namadrsrec.name.set(" ");
			return ;
		}
		namadrsrec.namadrsRec.set(" ");
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(wsaaClntnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
	}

protected void getUsername1400()
	{
		start1410();
	}

protected void start1410()
	{
		if (wsaaUserid.equals(" ")) {
			usrnamerec.username.set(" ");
			return ;
		}
		usrlIO.setParams(" ");
		usrlIO.setStatuz(varcom.oK);
		usrlIO.setUserid(wsaaUserid);
		usrlIO.setFunction(varcom.readr);
		usrlIO.setFormat(formatsInner.usrlrec);
		SmartFileCode.execute(appVars, usrlIO);
		if (isNE(usrlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(usrlIO.getParams());
			syserrrec.statuz.set(usrlIO.getStatuz());
			fatalError600();
		}
		wsaaSanckey.sancSanccoy.set(usrlIO.getCompany());
		wsaaSanckey.sancSancusr.set(usrlIO.getUsernum());
		usrnamerec.sanckey.set(wsaaSanckey.sancKey);
		usrnamerec.function.set("USRNM");
		callProgram(Usrname.class, usrnamerec.usrnameRec);
		if (isNE(usrnamerec.statuz, varcom.oK)) {
			syserrrec.statuz.set(usrnamerec.statuz);
			syserrrec.params.set(usrnamerec.usrnameRec);
			fatalError600();
		}
	}

protected void loadSubfile1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1510();
					callFupeio1530();
				case cont1580: 
					cont1580();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1510()
	{
		
		fupeList=fupepfDAO.readRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), wsaaFlupKeyInner.wsaaFlupFupno.toInt());
		if (fupeList.size()==0) {
			/*        IF WSAA-COUNT   EQUAL   ZEROS*/
			loadSubfileFromT56611600();
			/*        END-IF*/
			goTo(GotoLabel.cont1580);
		}
	}

protected void callFupeio1530()
	{
		for(Fupepf fupepf:fupeList){
		sv.message.set(fupepf.getMessage());
		scrnparams.function.set(varcom.sadd);
		a100CallSr50vio();
		}
		goTo(GotoLabel.cont1580);
	}

protected void cont1580()
	{
		if (wsspcomn.flag.equals("I")
		|| wsaaCount.toInt()==wsaaScrsize.toInt()) {
			scrnparams.subfileRrn.set(1);
			return ;
		}
		compute(wsaaRemainder, 0).setDivide(wsaaCount, (wsaaScrsize));
		wsaaRemainder.setRemainder(wsaaRemainder);
		compute(wsaaRemainder, 0).set(sub(wsaaScrsize, wsaaRemainder));
		for (ix.set(1); !(isGT(ix, wsaaRemainder)); ix.add(1)){
			sv.message.set(" ");
			scrnparams.function.set(varcom.sadd);
			a100CallSr50vio();
		}
		/*    MOVE 'Y'                    TO SCRN-SUBFILE-MORE.*/
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void loadSubfileFromT56611600()
	{
		start1610();
	}

protected void start1610()
	{
		
		/*    MOVE WSAA-FLUP-FUPCDE       TO ITEM-ITEMITEM.*/
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(wsaaFlupKeyInner.wsaaFlupFupcde);
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),t5661, wsaaT5661Key.toString());
		if (itempfList.size()!=0) {
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		while ( !(isGT(wsaaCount, 4))) {
			wsaaCount.add(1);
			sv.message.set(t5661rec.message[wsaaCount.toInt()]);
			scrnparams.function.set(varcom.sadd);
			a100CallSr50vio();
		}
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		/*    IF WSSP-FLAG         = 'I' OR 'G'*/
		if (wsspcomn.flag.equals("I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		if (scrnparams.statuz.equals(varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (scrnparams.statuz.equals(varcom.calc)
		|| scrnparams.statuz.equals(varcom.rolu)) {
			wsspcomn.edterror.set("Y");
		}
		if (scrnparams.statuz.equals(varcom.rolu)) {
			scrnparams.errorCode.set(f499);
		}
		/*VALIDATE-SCREEN*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, " ")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		wsaaSpcRrn.set(0);
		scrnparams.function.set(varcom.sstrt);
		a100CallSr50vio();
		while (!scrnparams.statuz.equals(varcom.endp)) {
			validateSubfile2600();
		}
		
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void validateSubfile2600()
	{
		/*VALIDATION*/
		if (isNE(sv.message, " ")) {
			wsaaSpcRrn.set(scrnparams.subfileRrn);
		}
		/*UPDATE-ERROR-INDICATORS*/
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srdn);
		a100CallSr50vio();
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (scrnparams.statuz.equals(varcom.kill)
		|| wsspcomn.flag.equals("I")) {
			return ;
		}
		updateFlup3100();
		deleteFupe3200();
		wsaaDocseq.set(0);
		scrnparams.function.set(varcom.sstrt);
		a100CallSr50vio();
		while ( !scrnparams.statuz.equals(varcom.endp)) {
			writeFupe3300();
		}
		
		/*EXIT*/
	}

protected void updateFlup3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3110();
				case fluplnb3120: 
					fluplnb3120();
				case flupalt3130: 
					flupalt3130();
				case flupclm3140: 
					flupclm3140();
				case fluprgp3150: 
					fluprgp3150();
				case flupenq3160: 
					flupenq3160();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3110()
	{
		if (wsaaFlupKeyInner.wsaaFlupFile.equals(formatsInner.fluplnbrec)){
			goTo(GotoLabel.fluplnb3120);
		}
		else if (wsaaFlupKeyInner.wsaaFlupFile.equals(formatsInner.flupaltrec)){
			goTo(GotoLabel.flupalt3130);
		}
		else if (wsaaFlupKeyInner.wsaaFlupFile.equals(formatsInner.flupclmrec)){
			goTo(GotoLabel.flupclm3140);
		}
		else if (wsaaFlupKeyInner.wsaaFlupFile.equals(formatsInner.fluprgprec)){
			goTo(GotoLabel.fluprgp3150);
		}
		else if (wsaaFlupKeyInner.wsaaFlupFile.equals(formatsInner.flupenqrec)){
			goTo(GotoLabel.flupenq3160);
		}
	}

protected void fluplnb3120()
	{
		fluplnbIO.setFunction(varcom.readr);
		fluplnbIO.setFormat(formatsInner.fluplnbrec);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			fatalError600();
		}
		fluplnbIO.setLstupuser(wsspcomn.userid);
		fluplnbIO.setZlstupdt(datcon1rec.intDate);
		fluplnbIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			fatalError600();
		}
		goTo(GotoLabel.exit3190);
	}

protected void flupalt3130()
	{
		flupaltIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupaltIO);
		if (isNE(flupaltIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupaltIO.getParams());
			syserrrec.statuz.set(flupaltIO.getStatuz());
			fatalError600();
		}
		flupaltIO.setLstupuser(wsspcomn.userid);
		flupaltIO.setZlstupdt(datcon1rec.intDate);
		flupaltIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, flupaltIO);
		if (isNE(flupaltIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupaltIO.getParams());
			syserrrec.statuz.set(flupaltIO.getStatuz());
			fatalError600();
		}
		goTo(GotoLabel.exit3190);
	}

protected void flupclm3140()
	{
		flupclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupclmIO.getParams());
			syserrrec.statuz.set(flupclmIO.getStatuz());
			fatalError600();
		}
		flupclmIO.setLstupuser(wsspcomn.userid);
		flupclmIO.setZlstupdt(datcon1rec.intDate);
		flupclmIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupclmIO.getParams());
			syserrrec.statuz.set(flupclmIO.getStatuz());
			fatalError600();
		}
		goTo(GotoLabel.exit3190);
	}

protected void fluprgp3150()
	{
		fluprgpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprgpIO.getParams());
			syserrrec.statuz.set(fluprgpIO.getStatuz());
			fatalError600();
		}
		fluprgpIO.setLstupuser(wsspcomn.userid);
		fluprgpIO.setZlstupdt(datcon1rec.intDate);
		fluprgpIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprgpIO.getParams());
			syserrrec.statuz.set(fluprgpIO.getStatuz());
			fatalError600();
		}
		goTo(GotoLabel.exit3190);
	}

protected void flupenq3160()
	{
		flupenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupenqIO.getParams());
			syserrrec.statuz.set(flupenqIO.getStatuz());
			fatalError600();
		}
		flupenqIO.setLstupuser(wsspcomn.userid);
		flupenqIO.setZlstupdt(datcon1rec.intDate);
		flupenqIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupenqIO.getParams());
			syserrrec.statuz.set(flupenqIO.getStatuz());
			fatalError600();
		}
		return ;
	}

protected void deleteFupe3200()
	{
	fupepfDAO.deleteRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), wsaaFlupKeyInner.wsaaFlupFupno.toInt());
	}

protected void writeFupe3300()
	{
		try {
			start3310();
			nextSubfileLine3380();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start3310()
	{
		if (isGT(scrnparams.subfileRrn, wsaaSpcRrn)) {
			scrnparams.statuz.set(varcom.endp);
			goTo(GotoLabel.exit3390);
		}
		wsaaDocseq.add(1);
		fupepf=new Fupepf();
		fupepf.setChdrcoy(chdrpf.getChdrcoy().toString());
		fupepf.setChdrnum(chdrpf.getChdrnum());
		fupepf.setFupno(wsaaFlupKeyInner.wsaaFlupFupno.toInt());
		fupepf.setClamnum(wsaaFlupKeyInner.wsaaFlupClamnum.toString());
		fupepf.setTranno(wsaaFlupKeyInner.wsaaFlupTranno.toInt());
		fupepf.setDocseq(wsaaDocseq.toString());
		fupepf.setMessage(sv.message.toString());
		fupepfDAO.insertRecord(fupepf);
	}

protected void nextSubfileLine3380()
	{
		scrnparams.function.set(varcom.srdn);
		a100CallSr50vio();
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a100CallSr50vio()
	{
		/*A110-START*/
		processScreen("SR50V", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*A190-EXIT*/
	}

protected void a200RetrvFlup()
	{
		try {
			a210Fluplnb();
			a220Flupenq();
			a230Flupalt();
			a240Flupclm();
			a250Fluprgp();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void a210Fluplnb()
	{
		fluplnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(), varcom.oK)
		&& isNE(fluplnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(fluplnbIO.getParams());
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			fatalError600();
		}
		if (fluplnbIO.getStatuz().equals(varcom.oK)) {
			wsaaFlupKeyInner.wsaaFlupChdrcoy.set(fluplnbIO.getChdrcoy());
			wsaaFlupKeyInner.wsaaFlupChdrnum.set(fluplnbIO.getChdrnum());
			wsaaFlupKeyInner.wsaaFlupFupno.set(fluplnbIO.getFupno());
			wsaaFlupKeyInner.wsaaFlupClamnum.set(fluplnbIO.getClamnum());
			wsaaFlupKeyInner.wsaaFlupTranno.set(fluplnbIO.getTranno());
			wsaaFlupKeyInner.wsaaFlupFupcde.set(fluplnbIO.getFupcode());
			wsaaFlupKeyInner.wsaaFlupCrtuser.set(fluplnbIO.getCrtuser());
			wsaaFlupKeyInner.wsaaFlupCrtdate.set(fluplnbIO.getCrtdate());
			wsaaFlupKeyInner.wsaaFlupLstupuser.set(fluplnbIO.getLstupuser());
			if (isNE(fluplnbIO.getZlstupdt(), NUMERIC)) {
				fluplnbIO.setZlstupdt(varcom.vrcmMaxDate);
			}
			wsaaFlupKeyInner.wsaaFlupZlstupdt.set(fluplnbIO.getZlstupdt());
			wsaaFlupKeyInner.wsaaFlupFupremk.set(fluplnbIO.getFupremk());
			wsaaFlupKeyInner.wsaaFlupFile.set(formatsInner.fluplnbrec);
			fluplnbIO.setFormat(formatsInner.fluplnbrec);
			fluplnbIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, fluplnbIO);
			if (isNE(fluplnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fluplnbIO.getParams());
				syserrrec.statuz.set(fluplnbIO.getStatuz());
				fatalError600();
			}
			goTo(GotoLabel.a290Exit);
		}
	}

protected void a220Flupenq()
	{
		flupenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(), varcom.oK)
		&& isNE(flupenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(flupenqIO.getParams());
			syserrrec.statuz.set(flupenqIO.getStatuz());
			fatalError600();
		}
		if (flupenqIO.getStatuz().equals(varcom.oK)) {
			wsaaFlupKeyInner.wsaaFlupChdrcoy.set(flupenqIO.getChdrcoy());
			wsaaFlupKeyInner.wsaaFlupChdrnum.set(flupenqIO.getChdrnum());
			wsaaFlupKeyInner.wsaaFlupFupno.set(flupenqIO.getFupno());
			wsaaFlupKeyInner.wsaaFlupClamnum.set(flupenqIO.getClamnum());
			wsaaFlupKeyInner.wsaaFlupTranno.set(flupenqIO.getTranno());
			wsaaFlupKeyInner.wsaaFlupFupcde.set(flupenqIO.getFupcode());
			wsaaFlupKeyInner.wsaaFlupCrtuser.set(flupenqIO.getCrtuser());
			wsaaFlupKeyInner.wsaaFlupCrtdate.set(flupenqIO.getCrtdate());
			wsaaFlupKeyInner.wsaaFlupLstupuser.set(flupenqIO.getLstupuser());
			if (isNE(flupenqIO.getZlstupdt(), NUMERIC)) {
				flupenqIO.setZlstupdt(varcom.vrcmMaxDate);
			}
			wsaaFlupKeyInner.wsaaFlupZlstupdt.set(flupenqIO.getZlstupdt());
			wsaaFlupKeyInner.wsaaFlupFupremk.set(flupenqIO.getFupremk());
			wsaaFlupKeyInner.wsaaFlupFile.set(formatsInner.flupenqrec);
			flupenqIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, flupenqIO);
			if (isNE(flupenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(flupenqIO.getParams());
				syserrrec.statuz.set(flupenqIO.getStatuz());
				fatalError600();
			}
			goTo(GotoLabel.a290Exit);
		}
	}

protected void a230Flupalt()
	{
		flupaltIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, flupaltIO);
		if (isNE(flupaltIO.getStatuz(), varcom.oK)
		&& isNE(flupaltIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(flupaltIO.getParams());
			syserrrec.statuz.set(flupaltIO.getStatuz());
			fatalError600();
		}
		if (flupaltIO.getStatuz().equals(varcom.oK)) {
			wsaaFlupKeyInner.wsaaFlupChdrcoy.set(flupaltIO.getChdrcoy());
			wsaaFlupKeyInner.wsaaFlupChdrnum.set(flupaltIO.getChdrnum());
			wsaaFlupKeyInner.wsaaFlupFupno.set(flupaltIO.getFupno());
			wsaaFlupKeyInner.wsaaFlupClamnum.set(flupaltIO.getClamnum());
			wsaaFlupKeyInner.wsaaFlupTranno.set(flupaltIO.getTranno());
			wsaaFlupKeyInner.wsaaFlupFupcde.set(flupaltIO.getFupcode());
			wsaaFlupKeyInner.wsaaFlupCrtuser.set(flupaltIO.getCrtuser());
			wsaaFlupKeyInner.wsaaFlupCrtdate.set(flupaltIO.getCrtdate());
			wsaaFlupKeyInner.wsaaFlupLstupuser.set(flupaltIO.getLstupuser());
			if (isNE(flupaltIO.getZlstupdt(), NUMERIC)) {
				flupaltIO.setZlstupdt(varcom.vrcmMaxDate);
			}
			wsaaFlupKeyInner.wsaaFlupZlstupdt.set(flupaltIO.getZlstupdt());
			wsaaFlupKeyInner.wsaaFlupFupremk.set(flupaltIO.getFupremk());
			wsaaFlupKeyInner.wsaaFlupFile.set(formatsInner.flupaltrec);
			flupaltIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, flupaltIO);
			if (isNE(flupaltIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(flupaltIO.getParams());
				syserrrec.statuz.set(flupaltIO.getStatuz());
				fatalError600();
			}
			goTo(GotoLabel.a290Exit);
		}
	}

protected void a240Flupclm()
	{
		flupclmIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(), varcom.oK)
		&& isNE(flupclmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(flupclmIO.getParams());
			syserrrec.statuz.set(flupclmIO.getStatuz());
			fatalError600();
		}
		if (flupclmIO.getStatuz().equals(varcom.oK)) {
			wsaaFlupKeyInner.wsaaFlupChdrcoy.set(flupclmIO.getChdrcoy());
			wsaaFlupKeyInner.wsaaFlupChdrnum.set(flupclmIO.getChdrnum());
			wsaaFlupKeyInner.wsaaFlupFupno.set(flupclmIO.getFupno());
			wsaaFlupKeyInner.wsaaFlupClamnum.set(flupclmIO.getClamnum());
			wsaaFlupKeyInner.wsaaFlupTranno.set(flupclmIO.getTranno());
			wsaaFlupKeyInner.wsaaFlupFupcde.set(flupclmIO.getFupcode());
			wsaaFlupKeyInner.wsaaFlupCrtuser.set(flupclmIO.getCrtuser());
			wsaaFlupKeyInner.wsaaFlupCrtdate.set(flupclmIO.getCrtdate());
			wsaaFlupKeyInner.wsaaFlupLstupuser.set(flupclmIO.getLstupuser());
			if (isNE(flupclmIO.getZlstupdt(), NUMERIC)) {
				flupclmIO.setZlstupdt(varcom.vrcmMaxDate);
			}
			wsaaFlupKeyInner.wsaaFlupZlstupdt.set(flupclmIO.getZlstupdt());
			wsaaFlupKeyInner.wsaaFlupFupremk.set(flupclmIO.getFupremk());
			wsaaFlupKeyInner.wsaaFlupFile.set(formatsInner.flupclmrec);
			flupclmIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, flupclmIO);
			if (isNE(flupclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(flupclmIO.getParams());
				syserrrec.statuz.set(flupclmIO.getStatuz());
				fatalError600();
			}
			goTo(GotoLabel.a290Exit);
		}
	}

protected void a250Fluprgp()
	{
		fluprgpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprgpIO.getParams());
			syserrrec.statuz.set(fluprgpIO.getStatuz());
			fatalError600();
		}
		if (fluprgpIO.getStatuz().equals(varcom.oK)) {
			wsaaFlupKeyInner.wsaaFlupChdrcoy.set(fluprgpIO.getChdrcoy());
			wsaaFlupKeyInner.wsaaFlupChdrnum.set(fluprgpIO.getChdrnum());
			wsaaFlupKeyInner.wsaaFlupFupno.set(fluprgpIO.getFupno());
			wsaaFlupKeyInner.wsaaFlupClamnum.set(fluprgpIO.getClamnum());
			wsaaFlupKeyInner.wsaaFlupTranno.set(fluprgpIO.getTranno());
			wsaaFlupKeyInner.wsaaFlupFupcde.set(fluprgpIO.getFupcode());
			wsaaFlupKeyInner.wsaaFlupCrtuser.set(fluprgpIO.getCrtuser());
			wsaaFlupKeyInner.wsaaFlupCrtdate.set(fluprgpIO.getCrtdate());
			wsaaFlupKeyInner.wsaaFlupLstupuser.set(fluprgpIO.getLstupuser());
			if (isNE(fluprgpIO.getZlstupdt(), NUMERIC)) {
				fluprgpIO.setZlstupdt(varcom.vrcmMaxDate);
			}
			wsaaFlupKeyInner.wsaaFlupZlstupdt.set(fluprgpIO.getZlstupdt());
			wsaaFlupKeyInner.wsaaFlupFupremk.set(fluprgpIO.getFupremk());
			wsaaFlupKeyInner.wsaaFlupFile.set(formatsInner.fluprgprec);
			fluprgpIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, fluprgpIO);
			if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fluprgpIO.getParams());
				syserrrec.statuz.set(fluprgpIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void a300ReadChdrlnb()
	{
		a310Start();
	}

protected void a310Start()
	{
		/* MOVE RETRV                  TO CHDRLNB-FUNCTION.             */
		/* CALL 'CHDRLNBIO'            USING CHDRLNB-PARAMS.            */
		/* IF  CHDRLNB-STATUZ           = O-K                           */
		/*     GO TO A390-EXIT                                          */
		/* END-IF.                                                      */
		/* MOVE RETRV                  TO CHDRENQ-FUNCTION.             */
		/* CALL 'CHDRENQIO'            USING CHDRENQ-PARAMS.            */
		/* IF  CHDRENQ-STATUZ          NOT = O-K                        */
		/*     MOVE CHDRENQ-PARAMS     TO SYSR-PARAMS                   */
		/*     MOVE CHDRENQ-STATUZ     TO SYSR-STATUZ                   */
		/*     PERFORM 600-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		/* Check for CHDRLNB first if not found then check for CHDRENQ.    */
		/* If both do not exist issue an error.                            */
		chdrpf=chdrpfDAO.getChdrlnbRecord(wsaaFlupKeyInner.wsaaFlupChdrcoy.toString(), wsaaFlupKeyInner.wsaaFlupChdrnum.toString());
		if (chdrpf!=null) {
			return ;
		}
		chdrpf=chdrpfDAO.getChdrenqRecord(wsaaFlupKeyInner.wsaaFlupChdrcoy.toString(), wsaaFlupKeyInner.wsaaFlupChdrnum.toString());
		if (chdrpf==null) {
			/*    Create when contract is not save before, only show contract# */
			chdrpf=new Chdrpf();
			chdrpf.setChdrcoy(wsaaFlupKeyInner.wsaaFlupChdrcoy.charat(0));
			chdrpf.setChdrnum(wsaaFlupKeyInner.wsaaFlupChdrnum.toString());
			chdrpf.setCnttype(" ");
			chdrpf.setCownnum(" ");
			chdrpf.setJownnum(" ");
			return ;
		}
	}

protected void a400ReadLifelnb()
	{
		a410MainLife();
		a450JointLife();
	}

protected void a410MainLife()
	{
		lifeList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");
		if (lifeList.size()==0) {
			sv.lifcnum.set(" ");
			wsaaClntnum.set(" ");
		}
		else{
		sv.lifcnum.set(lifeList.get(0).getLifcnum());
		wsaaClntnum.set(lifeList.get(0).getLifcnum());
		}
		callNamadrs1300();
		sv.clntname03.set(namadrsrec.name);
	}

protected void a450JointLife()
	{
		lifeList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "01");
		if (lifeList.size()==0) {
			sv.jlifcnum.set(" ");
			wsaaClntnum.set(" ");
		}
		else{
		sv.jlifcnum.set(lifeList.get(0).getLifcnum());
		wsaaClntnum.set(lifeList.get(0).getLifcnum());
		}
		callNamadrs1300();
		sv.clntname04.set(namadrsrec.name);
	}
/*
 * Class transformed  from Data Structure WSAA-FLUP-KEY--INNER
 */
private static final class WsaaFlupKeyInner { 
		/* WSAA-FLUP-KEY */
	private FixedLengthStringData wsaaFlupFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFlupChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFlupChdrnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaFlupFupno = new PackedDecimalData(2, 0);
	private FixedLengthStringData wsaaFlupClamnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaFlupTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaFlupFupcde = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaFlupCrtuser = new FixedLengthStringData(10);
	private PackedDecimalData wsaaFlupCrtdate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaFlupLstupuser = new FixedLengthStringData(10);
	private PackedDecimalData wsaaFlupZlstupdt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaFlupFupremk = new FixedLengthStringData(40);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData usrlrec = new FixedLengthStringData(10).init("USRLREC");
	private FixedLengthStringData fluplnbrec = new FixedLengthStringData(10).init("FLUPLNBREC");
	private FixedLengthStringData flupaltrec = new FixedLengthStringData(10).init("FLUPALTREC");
	private FixedLengthStringData flupclmrec = new FixedLengthStringData(10).init("FLUPCLMREC");
	private FixedLengthStringData flupenqrec = new FixedLengthStringData(10).init("FLUPENQREC");
	private FixedLengthStringData fluprgprec = new FixedLengthStringData(10).init("FLUPRGPREC");
	}
}
