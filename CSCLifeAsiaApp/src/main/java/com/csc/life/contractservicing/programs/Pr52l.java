/*
 * File: Pr52l.java
 * Date: December 5, 2013 10:30:19 AM ICT
 * Author: CSC
 *
 * Class transformed from PR52L.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.financials.dataaccess.dao.PreqpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Preqpf;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3698rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.FcfiTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.screens.Sr52lScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52mrec;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.dao.FpcopfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.life.flexiblepremium.dataaccess.model.Fpcopf;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.BnfypfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.PcddpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.UnltpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.newbusiness.dataaccess.model.Unltpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitdpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MedipfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Hitdpf;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.reassurance.dataaccess.dao.LirrpfDAO;
import com.csc.life.reassurance.dataaccess.dao.LrrhpfDAO;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Lirrpf;
import com.csc.life.reassurance.dataaccess.model.Lrrhpf;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.ZctnpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.ZptnpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.dataaccess.model.Zctnpf;
import com.csc.life.regularprocessing.dataaccess.model.Zptnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.IncipfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UlnkpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Incipf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.procedures.Ufprice;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrcpy2;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*               PR52L - Freelook Refund Details
*
* This program will basically display the refund details of the
* policy to be processed under Freelookk cancellation.
*
* INITIALIZATION.
* ---------------
* - Get today's date by calling DATCON1. then move to
*   unit reserve date.
* - Get the batch keys details and refund basis.
* - If it is refund premium basis then protect and hide
*   the unit reserve date field.
* - Retrieve the contract details from IO module by reading
*   CHDRLNB.
* - Get the sub account details by 1100-READ-T5645 SECTION.
* - Get the contract type details by 1200-READ-T5688-T1688
*   SECTION.
* - Get the literals by 1300-READ-TR386 SECTION.
* - Get the refund basis details applicable for the contract by
*   1400-READ-TR52M SECTION.
* - Display other fields by 1500-SETUP-FIELD SECTION .
* - Get the component statu details by 1600-READ-T5679 SECTION.
* - Get the unit link contract details by 1700-READ-T6647 SECTI.
* - Get the medical fee amount by 1900-GET-PENDING-MEDI.
*
* Validation.
* -----------
*
* - If Refund Unit basis
*
*   Validate that Unit Reverse date cannot be greater than today's
*   date
*   Validate that Unit Reverse date cannot be less than issue date
*
*   Check for SP Top Up transactions (TA69 and T679), if exists, t en
*   then Unit Reverse date cannot be less than Top Up transaction  ate.
*
* - Recalculate the total amounts on the screen.
* - Calculate the total admin charge to be recovered,validate that
*   total admin charge to be recovered cannot be greater than syst m
*   calculated one.
*
* - Calculate the total medical fee to be recovered,validate that
*   total medical fee to be recovered cannot be greater than syste
*   calculated one.
*
* - If refund payable to client is less than 0 then display error  sg.
*
* - Validate the reason code entered by 2100-CHECK-REASON SECTION.
*
* - If there are adjustments entered then validate the sub account
*   types entered and display the sub account description:
*
* Updating.
* ---------
*
* - If cancel key (F12) is pressed, then exit this section.
* - Process any clawback from agent by 3100-CHARGE-TO-AGENT SECTIO .
* - If refund premium then perform 3200-REFUND-PREMIUM
* - If refund unit then perform 3300-REFUND-UNIT
* - Write the cancellation reason details by 3800-WRITE-RESN SECTI N.
* - Remove softlock on the policy by calling SFTLOCK subroutine
*
****************************************************************** ****
* </pre>
*/
public class Pr52l extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR52L");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaCashdate = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaFundCode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3);
	private PackedDecimalData wsaaAmountAll = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPolTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaEstimatedVal = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaCnttot = new ZonedDecimalData(2, 0);

	private FixedLengthStringData wsaaRefundBasis = new FixedLengthStringData(1);
	private Validator refundPrem = new Validator(wsaaRefundBasis, "P");
	private Validator refundUnit = new Validator(wsaaRefundBasis, "U");

	private FixedLengthStringData wsaaTrancd = new FixedLengthStringData(4);
	private Validator spTopUp = new Validator(wsaaTrancd, "T679");
	private Validator spTopUpSa = new Validator(wsaaTrancd, "TA69");

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexPrem = new Validator(wsaaFlexPrem, "Y");
	private ZonedDecimalData wsaaRecovAdmin = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaRecovMed = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaCurrSusp = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaChargeFee = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaInvest = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaNonInvest = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaPpRiderPrem = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaOsDebt = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaContDebt = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaFundDebt = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaTotPremPaid = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaUnitUtrs = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaUnitHits = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaUnitValue = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaSeqnbr = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaCoverageKeys = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCoverageKeys, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCoverageKeys, 1);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaCoverageKeys, 9);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCoverageKeys, 11);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaCoverageKeys, 13);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaCoverageKeys, 15).setUnsigned();

		/* WSAA-T3698 */
	private FixedLengthStringData[] wsaaT3698Gl = FLSInittedArray (2, 21);
	private FixedLengthStringData[] wsaaT3698Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT3698Gl, 0);
	private FixedLengthStringData[] wsaaT3698Sacstyp = FLSDArrayPartOfArrayStructure(2, wsaaT3698Gl, 2);
	private ZonedDecimalData[] wsaaT3698Contot = ZDArrayPartOfArrayStructure(2, 0, wsaaT3698Gl, 4);
	private FixedLengthStringData[] wsaaT3698Glcode = FLSDArrayPartOfArrayStructure(14, wsaaT3698Gl, 6);
	private FixedLengthStringData[] wsaaT3698Glsign = FLSDArrayPartOfArrayStructure(1, wsaaT3698Gl, 20);
	private FixedLengthStringData wsaaT3698Key = new FixedLengthStringData(6);
//	private ZonedDecimalData wsaaTableOffset = new ZonedDecimalData(2, 0).setUnsigned();
//	private PackedDecimalData wsaaT5645Sub = new PackedDecimalData(5, 0);
	//private static final int wsaaT5645Size = 30;
	//ILIFE-2628 fixed--Array size increased
//	private static final int wsaaT5645Size = 1000;

		/* WSAA-T5645 */
	private FixedLengthStringData[] wsaaT5645Rec = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Rec, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Rec, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Rec, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Rec, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Rec, 20);

		/* WSAA-CHARGES */
	private FixedLengthStringData[] wsaaChargesRec = FLSInittedArray (30, 6);
	private FixedLengthStringData[] wsaaChgSacscode = FLSDArrayPartOfArrayStructure(2, wsaaChargesRec, 0);
	private FixedLengthStringData[] wsaaChgSacstype = FLSDArrayPartOfArrayStructure(2, wsaaChargesRec, 2);
	private ZonedDecimalData[] wsaaChgCnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaChargesRec, 4);

		/* WSAA-COMMISSIONS */
	private FixedLengthStringData[] wsaaCommsRec = FLSInittedArray (30, 6);
	private FixedLengthStringData[] wsaaCommSacscode = FLSDArrayPartOfArrayStructure(2, wsaaCommsRec, 0);
	private FixedLengthStringData[] wsaaCommSacstype = FLSDArrayPartOfArrayStructure(2, wsaaCommsRec, 2);
	private ZonedDecimalData[] wsaaCommCnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaCommsRec, 4);

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 4);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(2).isAPartOf(wsaaTr386Key, 6);

	private FixedLengthStringData wsaaTr52mItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52mBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaTr52mItem, 0);
	private FixedLengthStringData wsaaTr52mCnttype = new FixedLengthStringData(4).isAPartOf(wsaaTr52mItem, 4);
	private ZonedDecimalData wsaaTempMed = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaTempAdm = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaTempOth = new ZonedDecimalData(12, 2);

		/* WSAA-SPLIT-AGNTNUM */
	private FixedLengthStringData[] wsaaSplitRec = FLSInittedArray (10, 56);
	private FixedLengthStringData[] wsaaAgntnum = FLSDArrayPartOfArrayStructure(8, wsaaSplitRec, 0);
	private ZonedDecimalData[] wsaaSplit = ZDArrayPartOfArrayStructure(12, 2, wsaaSplitRec, 8);
	private ZonedDecimalData[] wsaaAgentMed = ZDArrayPartOfArrayStructure(12, 2, wsaaSplitRec, 20);
	private ZonedDecimalData[] wsaaAgentAdm = ZDArrayPartOfArrayStructure(12, 2, wsaaSplitRec, 32);
	private ZonedDecimalData[] wsaaAgentOth = ZDArrayPartOfArrayStructure(12, 2, wsaaSplitRec, 44);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaFound = new FixedLengthStringData(1);
	private Validator found = new Validator(wsaaFound, "Y");
	private Validator notFound = new Validator(wsaaFound, "N");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(1);
	private Validator trcdeMatch = new Validator(wsaaTrcde, "Y");
	private Validator trcdeNotMatch = new Validator(wsaaTrcde, "N");

	private FixedLengthStringData wsaaFile = new FixedLengthStringData(1);
	private Validator unitised = new Validator(wsaaFile, "Y");
	private Validator notUnitised = new Validator(wsaaFile, "N");
		/* WSAA-SACS */
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsbbRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsbbChdrnum = new FixedLengthStringData(8).isAPartOf(wsbbRldgacct, 0);
	private FixedLengthStringData wsbbLife = new FixedLengthStringData(2).isAPartOf(wsbbRldgacct, 8);
	private FixedLengthStringData wsbbCoverage = new FixedLengthStringData(2).isAPartOf(wsbbRldgacct, 10);
	private FixedLengthStringData wsbbRider = new FixedLengthStringData(2).isAPartOf(wsbbRldgacct, 12);
	private FixedLengthStringData wsbbPlnsfx = new FixedLengthStringData(2).isAPartOf(wsbbRldgacct, 14);
//	private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
//	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
//	private AcmvsacTableDAM acmvsacIO = new AcmvsacTableDAM();
//	private AgcmrevTableDAM agcmrevIO = new AgcmrevTableDAM();
//	private BextrevTableDAM bextrevIO = new BextrevTableDAM();
//	private BnfylnbTableDAM bnfylnbIO = new BnfylnbTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
//	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FcfiTableDAM fcfiIO = new FcfiTableDAM();
//	private FlupTableDAM flupIO = new FlupTableDAM();
//	private FluprevTableDAM fluprevIO = new FluprevTableDAM();
//	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
//	private Fpcolf1TableDAM fpcolf1IO = new Fpcolf1TableDAM();
	private FprmTableDAM fprmIO = new FprmTableDAM();
//	private HitdTableDAM hitdIO = new HitdTableDAM();
//	private HitdrevTableDAM hitdrevIO = new HitdrevTableDAM();
//	private HitrTableDAM hitrIO = new HitrTableDAM();
//	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
//	private HitsTableDAM hitsIO = new HitsTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
//	private InciTableDAM inciIO = new InciTableDAM();
//	private IncirevTableDAM incirevIO = new IncirevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
//	private LifecfiTableDAM lifecfiIO = new LifecfiTableDAM();
//	private LinsTableDAM linsIO = new LinsTableDAM();
//	private LinscfiTableDAM linscfiIO = new LinscfiTableDAM();
//	private LirrTableDAM lirrIO = new LirrTableDAM();
//	private LirrconTableDAM lirrconIO = new LirrconTableDAM();
//	private LrrhTableDAM lrrhIO = new LrrhTableDAM();
//	private LrrhconTableDAM lrrhconIO = new LrrhconTableDAM();
//	private MediTableDAM mediIO = new MediTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
//	private PcddlnbTableDAM pcddlnbIO = new PcddlnbTableDAM();
//	private PreqrlgTableDAM preqrlgIO = new PreqrlgTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
//	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
//	private RacdTableDAM racdIO = new RacdTableDAM();
//	private RacdseqTableDAM racdseqIO = new RacdseqTableDAM();
	private ResnTableDAM resnIO = new ResnTableDAM();
//	private TaxdTableDAM taxdIO = new TaxdTableDAM();
//	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
//	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
//	private UlnkrevTableDAM ulnkrevIO = new UlnkrevTableDAM();
//	private UnltrevTableDAM unltrevIO = new UnltrevTableDAM();
//	private UtrnTableDAM utrnIO = new UtrnTableDAM();
//	private UtrnaloTableDAM utrnaloIO = new UtrnaloTableDAM();
//	private UtrssurTableDAM utrssurIO = new UtrssurTableDAM();
//	private ZctnTableDAM zctnIO = new ZctnTableDAM();
//	private ZctnrevTableDAM zctnrevIO = new ZctnrevTableDAM();
//	private ZptnTableDAM zptnIO = new ZptnTableDAM();
//	private ZptnrevTableDAM zptnrevIO = new ZptnrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T3698rec t3698rec = new T3698rec();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
	private Th605rec th605rec = new Th605rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tr52mrec tr52mrec = new Tr52mrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Dbcstrcpy2 dbcstrcpy2 = new Dbcstrcpy2();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Sr52lScreenVars sv = ScreenProgram.getScreenVars( Sr52lScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> t5645Map = null;
	private Map<String, List<Itempf>> t5515Map = null;
	private Map<String, List<Itempf>> t5540Map = null;
	private List<Acblpf> acblenqList = null;
	private List<Acmvpf> acmvrevList = null;
	private List<Covrpf> covrlnbList = null;
	private List<Utrnpf> utrnList = null;
	private List<Hitrpf> hitrList = null;
	private List<Utrspf> utrssurList = null;
	private List<Hitspf> hitsList = null;
	private int wsaaTableOffset01;
	private int wsaaTableOffset23;
	private int wsaaTableOffset45;
	private List<Utrnpf> insertUtrnIOList = null;
	private List<Hitrpf> insertHitrIOList = null;
	private List<Covrpf> updateCovrlnbList = null;
	private List<Covrpf> insertCovrlnbList = null;
	private List<Fpcopf> updateFpcoIOList = null;
	private List<Fpcopf> insertFpcolf1IOList = null;
	private List<Agcmpf> updateAgcmrevIOList = null;
	private List<Unltpf> insertUnltrevIOList = null;
	private List<Zptnpf> insertZptnpfList = null;
	private List<Zctnpf> insertZctnpfList = null;
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);
	private AcblpfDAO acblpfDAO	 = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private AcmvpfDAO acmvpfDAO	 = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private CovrpfDAO covrpfDAO	 = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private UtrnpfDAO utrnpfDAO	 = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	private PcddpfDAO pcddpfDAO	 = getApplicationContext().getBean("pcddpfDAO", PcddpfDAO.class);
	private HitdpfDAO hitdpfDAO = getApplicationContext().getBean("hitdpfDAO", HitdpfDAO.class);
	private FpcopfDAO fpcopfDAO = getApplicationContext().getBean("fpcopfDAO", FpcopfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
	private BextpfDAO bextpfDAO = getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private BnfypfDAO bnfypfDAO = getApplicationContext().getBean("bnfypfDAO", BnfypfDAO.class);
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private TaxdpfDAO taxdpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private UnltpfDAO unltpfDAO = getApplicationContext().getBean("unltpfDAO", UnltpfDAO.class);
	private UlnkpfDAO ulnkpfDAO = getApplicationContext().getBean("ulnkpfDAO", UlnkpfDAO.class);
	private IncipfDAO incipfDAO = getApplicationContext().getBean("incipfDAO", IncipfDAO.class);
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private LrrhpfDAO lrrhpfDAO = getApplicationContext().getBean("lrrhpfDAO", LrrhpfDAO.class);
	private LirrpfDAO lirrpfDAO = getApplicationContext().getBean("lirrpfDAO", LirrpfDAO.class);
	private ZptnpfDAO zptnpfDAO = getApplicationContext().getBean("zptnpfDAO", ZptnpfDAO.class);
	private ZctnpfDAO zctnpfDAO = getApplicationContext().getBean("zctnpfDAO", ZctnpfDAO.class);
	private PreqpfDAO preqpfDAO = getApplicationContext().getBean("preqpfDAO", PreqpfDAO.class);
	private MedipfDAO medipfDAO = getApplicationContext().getBean("medipfDAO", MedipfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private boolean onePflag = false;
	private static final String  ONE_P_CASHLESS="NBPRP124"; 
	
	
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		begnHits1515,
		calc1518,
		begin1525,
		exit1525,
		loopAcmv1532,
		exit1539,
		callCovrlnb1542,
		nextCovrlnb1548,
		exit1549,
		hitr1556,
		begin1581,
		exit1589,
		readMedi1920,
		next1930,
		begnPreqrlg1950,
		exit2090,
		call2420,
		exit2490,
		exit3110,
		call3330,
		next3330,
		exit3330,
		callUtrn3350,
		readHitr3350,
		callHitr3350,
		drCr3350,
		callUtrnalo3360,
		exit3360,
		callHitralo3370,
		exit3370,
		call3380,
		exit3380,
		callHitdrev3390,
		exit3390,
		callUtrssur3420,
		exit3490,
		callHits3520,
		exit3590,
		callCovrlnb3700,
		nextCovrlnb3700,
		exit3700,
		callFpco3720,
		nextFpco3720,
		exit3720,
		callAgcmrev3740,
		exit3740,
		callBextrev3750,
		exit3750,
		callBnfylnb3760,
		exit3760,
		callFluprev3770,
		exit3770,
		callLifecfi3780,
		exit3780,
		callLinscfi3790,
		callTaxdrev3790,
		nextLinscfi3790,
		exit3790,
		callUlnkrev3810,
		exit3810,
		callIncirev3830,
		exit3830,
		callRacd3840,
		exit3840,
		callLrrhcon3850,
		exit3850,
		callLirrcon3860,
		exit3860,
		callZptnrev3920,
		exit3920,
		callZctnrev3940,
		exit3940,
		callCovrlnb5102,
		nextCovrlnb5108,
		exit5109,
		loopAcbl5602,
		nextAcbl5608,
		exit5609,
		exit6390
	}

	public Pr52l() {
		super();
		screenVars = sv;
		new ScreenModel("Sr52l", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		sv.dataArea.set(SPACES);
		sv.adjamt01.set(ZERO);
		sv.adjamt02.set(ZERO);
		sv.admamt01.set(ZERO);
		sv.admamt02.set(ZERO);
		sv.medamt01.set(ZERO);
		sv.medamt02.set(ZERO);
		sv.reserveUnitsDate.set(varcom.vrcmMaxDate);
		onePflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), ONE_P_CASHLESS, appVars, "IT");
		for (ix.set(1); !(isGT(ix, 2)); ix.add(1)){
			wsaaT3698Contot[ix.toInt()].set(ZERO);
		}
		for (ix.set(1); !(isGT(ix, 30)); ix.add(1)){
			wsaaT5645Cnttot[ix.toInt()].set(ZERO);
			wsaaCommCnttot[ix.toInt()].set(ZERO);
			wsaaChgCnttot[ix.toInt()].set(ZERO);
		}
		/*    Get today's date by using DATCON1*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		sv.reserveUnitsDate.set(wsaaToday);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaRefundBasis.set(wsspcomn.clonekey);
		sv.transcode.set(wsaaBatckey.batcBatctrcde);
		if (refundPrem.isTrue()) {
			sv.rundteOut[varcom.pr.toInt()].set("Y");
			sv.rundteOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.rundteOut[varcom.pr.toInt()].set("N");
			sv.rundteOut[varcom.nd.toInt()].set("N");
		}
		wsaaItem.set(SPACES);
		/* Retrieve contract fields from I/O module*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		/* Read HPAD to get issue date*/
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		if (refundPrem.isTrue()) {
			wsaaTranno.set(chdrlnbIO.getTranno());
		}
		else {
			compute(wsaaTranno, 0).set(add(chdrlnbIO.getTranno(), 1));
		}
		/* Get rest of details to display on screen*/
		readT56451100();
		sv.sacscode01.set(wsaaT5645Sacscode[4]);
		sv.sacscode02.set(wsaaT5645Sacscode[5]);
		readT5688T16881200();
		readTr3861300();
		readTr52m1400();
		readT56791600();
		setupField1500();
		getPendingMedicalFee1900();

	}

protected void readT56451100()
	{
		t5645Map = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), wsspcomn.company.toString(), "T5645");
		wsaaTableOffset01 = 1;
		wsaaTableOffset23 = 1;
		wsaaTableOffset45 = 1;
		if(t5645Map!=null&&t5645Map.containsKey(wsaaProg.toString())){
			List<Itempf> itempfList = t5645Map.get(wsaaProg.toString());
			for(Itempf item:itempfList){
				callT56451150(item);
			}
		}else{
			syserrrec.params.set("T5645:" + wsaaProg);
			fatalError600();
		}
	}

protected void callT56451150(Itempf item)
	{
		int wsaaT5645Sub = 1;
		t5645rec.t5645Rec.set(StringUtil.rawToString(item.getGenarea()));
		if (isEQ(item.getItemseq(), SPACES)
		|| isEQ(item.getItemseq(), "01")) {
			while ( !(isGT(wsaaT5645Sub, 15))) {
				wsaaT5645Cnttot[wsaaTableOffset01].set(t5645rec.cnttot[wsaaT5645Sub]);
				wsaaT5645Glmap[wsaaTableOffset01].set(t5645rec.glmap[wsaaT5645Sub]);
				wsaaT5645Sacscode[wsaaTableOffset01].set(t5645rec.sacscode[wsaaT5645Sub]);
				wsaaT5645Sacstype[wsaaTableOffset01].set(t5645rec.sacstype[wsaaT5645Sub]);
				wsaaT5645Sign[wsaaTableOffset01].set(t5645rec.sign[wsaaT5645Sub]);
				wsaaT5645Sub++;
				wsaaTableOffset01++;
			}
		}
		if (isEQ(item.getItemseq(), "02")
		|| isEQ(item.getItemseq(), "03")) {
			while ( !(isGT(wsaaT5645Sub, 15))) {
				wsaaChgSacscode[wsaaTableOffset23].set(t5645rec.sacscode[wsaaT5645Sub]);
				wsaaChgSacstype[wsaaTableOffset23].set(t5645rec.sacstype[wsaaT5645Sub]);
				wsaaChgCnttot[wsaaTableOffset23].set(t5645rec.cnttot[wsaaT5645Sub]);
				wsaaT5645Sub++;
				wsaaTableOffset23++;
			}
		}
		if (isEQ(item.getItemseq(), "04")
		|| isEQ(item.getItemseq(), "05")) {
			while ( !(isGT(wsaaT5645Sub, 15))) {
				wsaaCommSacscode[wsaaTableOffset45].set(t5645rec.sacscode[wsaaT5645Sub]);
				wsaaCommSacstype[wsaaTableOffset45].set(t5645rec.sacstype[wsaaT5645Sub]);
				wsaaCommCnttot[wsaaTableOffset45].set(t5645rec.cnttot[wsaaT5645Sub]);
				wsaaT5645Sub++;
				wsaaTableOffset45++;
			}
		}
	}

protected void readT5688T16881200()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrlnbIO.getCnttype())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Get Contract type description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(tablesInner.t5688);
		wsaaItem.set(chdrlnbIO.getCnttype());
		getDescriptions1950();
		sv.ctypedes.set(descIO.getLongdesc());
		/* Get Screen title from transaction we are processing*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(tablesInner.t1688);
		wsaaItem.set(wsaaBatckey.batcBatctrcde);
		getDescriptions1950();
		wsaaDesc.set(descIO.getLongdesc());
	}

protected void readTr3861300()
	{
		/* Read TR386 table to get screen literals*/
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itemIO.setItemitem(wsaaTr386Key);
		if (refundPrem.isTrue()) {
			itemIO.setItemseq("01");
		}
		else {
			itemIO.setItemseq(SPACES);
		}
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		ix.set(ZERO);
		for (int loopVar1 = 0; !(loopVar1 == 9); loopVar1 += 1){
			ix.add(1);
			dbcstrcpy2.dbcsInputString.set(tr386rec.progdesc[ix.toInt()]);
			dbcstrcpy2.dbcsOutputLength.set(30);
			dbcstrcpy2.dbcsStatuz.set(SPACES);
			dbcsTrnc2(dbcstrcpy2.rec);
			if (isNE(dbcstrcpy2.dbcsStatuz, "****")) {
				dbcstrcpy2.dbcsOutputString.fill("?");
			}
			sv.dtldesc[ix.toInt()].set(dbcstrcpy2.dbcsOutputString);
		}

	}

protected void readTr52m1400()
	{
		/* Read TR52M for Taxcode*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52m);
		wsaaTr52mBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaTr52mCnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaTr52mItem);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52m);
			wsaaTr52mBatctrcde.set(wsaaBatckey.batcBatctrcde);
			wsaaTr52mCnttype.set("***");
			itemIO.setItemitem(wsaaTr52mItem);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52mrec.tr52mRec.set(itemIO.getGenarea());
		if (refundUnit.isTrue()) {
			if (isEQ(tr52mrec.basind, "1")) {
				sv.reserveUnitsDate.set(hpadIO.getHissdte());
			}
			if (isEQ(tr52mrec.basind, "1")
			|| isEQ(tr52mrec.basind, "2")) {
				sv.rundteOut[varcom.pr.toInt()].set("Y");
			}
		}

	}

protected void setupField1500()
	{
		acblenqList = acblpfDAO.searchAcblenqRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if (refundUnit.isTrue()) {
			readT66471700();
			getUnitValue1510();
			getPremAndDebt1520(acblenqList);
			calcTolerance1530(acblenqList);
			calcOsDebt1540();
			loopUtrnHitr1550();
			getCurrentSuspense1560(acblenqList);
			getChargesAndFees1570(acblenqList);
			/* Refund Estimate Value*/
			/*    - Unit refund extimate value*/
			/*    - Plus (Total premium paid - Invested premium)*/
			/*    - Minus (Contract Debt - Fund Debt)*/
			/*    - Plus Charges need to refund*/
			/*    - Plus Current Contract Suspense*/
			/*    - Minus Premium Tolerance*/
			compute(wsaaPpRiderPrem, 3).setRounded(sub(sub(wsaaTotPremPaid, wsaaNonInvest), wsaaInvest));
			compute(sv.premi, 3).setRounded(sub(add(add(sub(add(add(wsaaUnitValue, wsaaPpRiderPrem), wsaaNonInvest), wsaaOsDebt), wsaaChargeFee), wsaaCurrSusp), wsaaTolerance));
		}
		else {
			getCurrentSuspense1560(acblenqList);
			sv.premi.set(wsaaCurrSusp);
		}
		/* Get the medical fee expense balance (LP/ME) of the policy*/
		/* by reading ACBL*/
		Acblpf acblenqIO = null;
		for(Acblpf acblenq:acblenqList){
			if (isEQ(acblenq.getRldgacct(), chdrlnbIO.getChdrnum())
					&& isEQ(acblenq.getSacstyp(), wsaaT5645Sacstype[8])
					&& isEQ(acblenq.getSacscode(), wsaaT5645Sacscode[8])
					&& isEQ(acblenq.getRldgcoy(), chdrlnbIO.getChdrcoy())
					&& isEQ(acblenq.getOrigcurr(), chdrlnbIO.getBillcurr())) {
				acblenqIO = acblenq;
				break;
			}
		}
		if (acblenqIO == null) {
			acblenqIO = new Acblpf();
			acblenqIO.setRldgacct(chdrlnbIO.getChdrnum().toString());
			acblenqIO.setRldgcoy(chdrlnbIO.getChdrcoy().toString());
			acblenqIO.setOrigcurr(chdrlnbIO.getBillcurr().toString());
			acblenqIO.setSacscode(wsaaT5645Sacscode[8].toString());
			acblenqIO.setSacstyp(wsaaT5645Sacstype[8].toString());
			acblenqIO.setSacscurbal(BigDecimal.ZERO);
		}
		/* Calculate the client portion of medical fee expense based*/
		/* on the percentage defined in TR52M*/
		compute(sv.medfee01, 2).set(div(mult(acblenqIO.getSacscurbal(), tr52mrec.medpc01), 100));
		zrdecplrec.amountIn.set(sv.medfee01);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		a100CallRounding();
		sv.medfee01.set(zrdecplrec.amountOut);
		compute(sv.medfee02, 2).set(div(mult(acblenqIO.getSacscurbal(), tr52mrec.medpc02), 100));
		zrdecplrec.amountIn.set(sv.medfee02);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		a100CallRounding();
		sv.medfee02.set(zrdecplrec.amountOut);
		/*  adjust the client portion*/
		if ((setPrecision(100, 0)
		&& isEQ((add(tr52mrec.medpc01, tr52mrec.medpc02)), 100))) {
			compute(sv.medfee02, 2).set(sub(acblenqIO.getSacscurbal(), sv.medfee01));
		}
		/*  Calculate the total medical fee to be recovered*/
		compute(wsaaRecovMed, 2).set(add(sv.medamt01, sv.medamt02));
		if (isEQ(wsspcomn.sectionno, 1000)
		&& isEQ(wsaaRecovMed, 0)) {
			sv.medamt01.set(sv.medfee01);
			sv.medamt02.set(sv.medfee02);
		}
		/*  Calculate the agent portion of admin fee*/
		compute(sv.admfee01, 2).set(div(mult(tr52mrec.admfee, tr52mrec.admpc01), 100));
		zrdecplrec.amountIn.set(sv.admfee01);
		zrdecplrec.currency.set(sv.billcurr);
		a100CallRounding();
		sv.admfee01.set(zrdecplrec.amountOut);
		/*  Calculate the client portion of admin fee*/
		compute(sv.admfee02, 2).set(div(mult(tr52mrec.admfee, tr52mrec.admpc02), 100));
		zrdecplrec.amountIn.set(sv.admfee02);
		zrdecplrec.currency.set(sv.billcurr);
		a100CallRounding();
		sv.admfee02.set(zrdecplrec.amountOut);
		if ((setPrecision(100, 0)
		&& isEQ((add(tr52mrec.admpc01, tr52mrec.admpc02)), 100))) {
			compute(sv.admfee02, 2).set(sub(tr52mrec.admfee, sv.admfee01));
		}
		/*  Calculate the total admin fee to be recovered*/
		compute(wsaaRecovAdmin, 2).set(add(sv.admamt01, sv.admamt02));
		if (isEQ(wsspcomn.sectionno, 1000)
		&& isEQ(wsaaRecovAdmin, 0)) {
			sv.admamt01.set(sv.admfee01);
			sv.admamt02.set(sv.admfee02);
		}
		/*  Calculate the total deduction from agent*/
		compute(sv.tdeduct, 2).set(add(add(add(0, sv.medamt01), sv.admamt01), sv.adjamt01));
		/*  Calculate the refund payable to the client*/
		compute(sv.ztotamt, 2).set(sub(sub(sub(sv.premi, sv.medamt02), sv.admamt02), sv.adjamt02));
	}

protected void getUnitValue1510()
 {

		String chdrnum = chdrlnbIO.getChdrnum().toString();
		utrssurList = utrspfDAO.searchUtrsRecord(chdrlnbIO.getChdrcoy().toString(), chdrnum);
		wsaaUnitValue.set(ZERO);
		wsaaUnitUtrs.set(ZERO);
		wsaaUnitHits.set(ZERO);
		t5515Map = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), wsspcomn.company.toString(), "T5515");
		for (Utrspf u : utrssurList) {
			readUtrssur1512(u);
		}
		List<String> chdrnumList = new ArrayList<>();
		chdrnumList.add(chdrnum);
		Map<String, List<Hitspf>> hitsMap = hitspfDAO.searchHitsRecordByChdrcoy(chdrlnbIO.getChdrcoy().toString(),
				chdrnumList);
		if (hitsMap != null && hitsMap.containsKey(chdrnum)) {
			hitsList = hitsMap.get(chdrnum);
			for (Hitspf h : hitsList) {
				readHits1516(h);
			}
		}
		compute(wsaaUnitValue, 2).set(add(wsaaUnitUtrs, wsaaUnitHits));
	}


protected void readUtrssur1512(Utrspf utrssurIO)
	{
		wsaaFile.set("Y");
		ufpricerec.ufpriceRec.set(SPACES);
		ufpricerec.function.set("PRICE");
		ufpricerec.company.set(utrssurIO.getChdrcoy());
		ufpricerec.unitVirtualFund.set(utrssurIO.getUnitVirtualFund());
		ufpricerec.unitType.set(utrssurIO.getUnitType());
		ufpricerec.effdate.set(sv.reserveUnitsDate);
		ufpricerec.nowDeferInd.set(t6647rec.dealin);
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);
		if (isNE(ufpricerec.statuz, varcom.oK)
		&& isNE(ufpricerec.statuz, varcom.endp)
		&& isNE(ufpricerec.statuz, "ESTM")) {
			syserrrec.params.set(ufpricerec.ufpriceRec);
			syserrrec.statuz.set(ufpricerec.statuz);
			fatalError600();
		}
		if (isEQ(ufpricerec.statuz, varcom.endp)) {
			sv.rundteErr.set(errorsInner.g094);
		}
		if (isEQ(t6647rec.bidoffer, "B")) {
			compute(wsaaEstimatedVal, 6).setRounded(mult(ufpricerec.bidPrice, utrssurIO.getCurrentUnitBal()));
		}
		else {
			compute(wsaaEstimatedVal, 6).setRounded(mult(ufpricerec.offerPrice, utrssurIO.getCurrentUnitBal()));
		}
		wsaaFundCode.set(utrssurIO.getUnitVirtualFund());
		readT55156000();
		wsaaCurrcode.set(t5515rec.currcode);
		wsaaCashdate.set(sv.reserveUnitsDate);
		convertValue6100();
		wsaaUnitUtrs.add(conlinkrec.amountOut);
	}

protected void readHits1516(Hitspf hitsIO)
	{
		wsaaEstimatedVal.setRounded(hitsIO.getZcurprmbal());
		wsaaFundCode.set(hitsIO.getZintbfnd());
		readT55156000();
		wsaaCurrcode.set(t5515rec.currcode);
		wsaaCashdate.set(sv.reserveUnitsDate);
		convertValue6100();
		wsaaUnitHits.add(conlinkrec.amountOut);
	}

protected void calc1518()
	{
		
		/*EXIT*/
	}

protected void getPremAndDebt1520(List<Acblpf> acblenqList)
 {
		/* Get the sub account balances of premium income by going thru */
		/* the sub-account code/type -LC/LP,LE/LP, LC/SP and LE/SP */
		wsaaTotPremPaid.set(ZERO);
		wsaaContDebt.set(ZERO);
		wsaaFundDebt.set(ZERO);
		
		for (Acblpf acblenqIO : acblenqList) {
			for (ix.set(12); !(isGT(ix, 15) || isEQ(wsaaT5645Sacscode[ix.toInt()], SPACES)); ix.add(1)) {

				if (isEQ(acblenqIO.getRldgcoy(), chdrlnbIO.getChdrcoy())
						&& isEQ(acblenqIO.getSacscode(), wsaaT5645Sacscode[ix.toInt()])
						&& isEQ(acblenqIO.getSacstyp(), wsaaT5645Sacstype[ix.toInt()])
						&& isEQ(subString(acblenqIO.getRldgacct(), 1, 8), chdrlnbIO.getChdrnum())
						&& isEQ(acblenqIO.getOrigcurr(), chdrlnbIO.getCntcurr())) {
					if (isGT(ix, 11) && isLT(ix, 16)) {
						compute(wsaaTotPremPaid, 2).set(add(wsaaTotPremPaid, mult(acblenqIO.getSacscurbal(), (-1))));
					}
					if (isGT(ix, 17) && isLT(ix, 20)) {
						compute(wsaaContDebt, 2).set(add(wsaaContDebt, acblenqIO.getSacscurbal()));
					}
					if (isGT(ix, 19) && isLT(ix, 22)) {
						compute(wsaaFundDebt, 2).set(add(wsaaFundDebt, mult(acblenqIO.getSacscurbal(), (-1))));
					}
				}
			}
		}

		for (Acblpf acblenqIO : acblenqList) {
			for (ix.set(18); !(isGT(ix, 21) || isEQ(wsaaT5645Sacscode[ix.toInt()], SPACES)); ix.add(1)) {

				if (isEQ(acblenqIO.getRldgcoy(), chdrlnbIO.getChdrcoy())
						&& isEQ(acblenqIO.getSacscode(), wsaaT5645Sacscode[ix.toInt()])
						&& isEQ(acblenqIO.getSacstyp(), wsaaT5645Sacstype[ix.toInt()])
						&& isEQ(subString(acblenqIO.getRldgacct(), 1, 8), chdrlnbIO.getChdrnum())
						&& isEQ(acblenqIO.getOrigcurr(), chdrlnbIO.getCntcurr())) {
					if (isGT(ix, 11) && isLT(ix, 16)) {
						compute(wsaaTotPremPaid, 2).set(add(wsaaTotPremPaid, mult(acblenqIO.getSacscurbal(), (-1))));
					}
					if (isGT(ix, 17) && isLT(ix, 20)) {
						compute(wsaaContDebt, 2).set(add(wsaaContDebt, acblenqIO.getSacscurbal()));
					}
					if (isGT(ix, 19) && isLT(ix, 22)) {
						compute(wsaaFundDebt, 2).set(add(wsaaFundDebt, mult(acblenqIO.getSacscurbal(), (-1))));
					}
				}
			}
		}
	}



protected void calcTolerance1530(List<Acblpf> acblenqList)
 {
		wsaaTolerance.set(ZERO);
		wsaaPolTolerance.set(ZERO);
		for (Acblpf acblenqIO : acblenqList) {
			if (isEQ(acblenqIO.getRldgcoy(), chdrlnbIO.getChdrcoy())
					&& isEQ(acblenqIO.getSacscode(), wsaaT5645Sacscode[3])
					&& isEQ(acblenqIO.getSacstyp(), wsaaT5645Sacstype[3])
					&& isEQ(acblenqIO.getRldgacct(), chdrlnbIO.getChdrnum())
					&& isEQ(acblenqIO.getOrigcurr(), chdrlnbIO.getCntcurr())) {
				wsaaPolTolerance.set(acblenqIO.getSacscurbal());
				break;
			}
		}
		wsaaTolerance.add(wsaaPolTolerance);
		acmvrevList = acmvpfDAO.searchAcmvrevRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		for (Acmvpf acmvrevIO : acmvrevList) {
			if (isEQ(acmvrevIO.getSacscode(), wsaaT5645Sacscode[24])
					&& isEQ(acmvrevIO.getSacstyp(), wsaaT5645Sacstype[24])) {
				PackedDecimalData origamt = new PackedDecimalData(17, 2);
				origamt.set(acmvrevIO.getOrigamt());
				wsaaTolerance.add(origamt);
			}
		}
	}

protected void calcOsDebt1540()
 {
		wsaaOsDebt.set(0);
		covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		for (Covrpf covrlnbIO : covrlnbList) {
			if (isEQ(covrlnbIO.getTranno(), chdrlnbIO.getTranno()) || isEQ(covrlnbIO.getValidflag(), "2")
					|| isEQ(covrlnbIO.getCoverageDebt(), 0)) {
				continue;
			}

			/* Verify the Contract Risk Statuz */
			trcdeNotMatch.setTrue();
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12) || trcdeMatch.isTrue()); wsaaSub.add(1)) {
				if (isEQ(covrlnbIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
					trcdeMatch.setTrue();
				}
			}
			if (trcdeNotMatch.isTrue()) {
				continue;
			}
			/* Accumulate any outstanding coverage debts */
			PackedDecimalData origamt = new PackedDecimalData(17, 2);
			origamt.set(covrlnbIO.getCoverageDebt());
			wsaaOsDebt.add(origamt);
		}
	}

protected void loopUtrnHitr1550()
 {
		wsaaInvest.set(ZERO);
		wsaaNonInvest.set(ZERO);
		utrnList = utrnpfDAO.searchUtrnRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		for (Utrnpf utrnIO : utrnList) {
			if ((isEQ(utrnIO.getSacscode(), wsaaT5645Sacscode[10]) && isEQ(utrnIO.getSacstyp(), wsaaT5645Sacstype[10]))
					|| (isEQ(utrnIO.getSacscode(), wsaaT5645Sacscode[11]) && isEQ(utrnIO.getSacstyp(),
							wsaaT5645Sacstype[11]))) {
				compute(wsaaNonInvest, 2).set(add(wsaaNonInvest, utrnIO.getContractAmount()));
			}
			if ((isEQ(utrnIO.getSacscode(), wsaaT5645Sacscode[22]) && isEQ(utrnIO.getSacstyp(), wsaaT5645Sacstype[22]))
					|| (isEQ(utrnIO.getSacscode(), wsaaT5645Sacscode[23]) && isEQ(utrnIO.getSacstyp(),
							wsaaT5645Sacstype[23]))) {
				compute(wsaaInvest, 2).set(add(wsaaInvest, utrnIO.getContractAmount()));
			}
		}

		hitrList = hitrpfDAO.searchHitrRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		for (Hitrpf hitrIO : hitrList) {
			if ((isEQ(hitrIO.getSacscode(), wsaaT5645Sacscode[10]) && isEQ(hitrIO.getSacstyp(), wsaaT5645Sacstype[10]))
					|| (isEQ(hitrIO.getSacscode(), wsaaT5645Sacscode[11]) && isEQ(hitrIO.getSacstyp(),
							wsaaT5645Sacstype[11]))) {
				compute(wsaaNonInvest, 2).set(add(wsaaNonInvest, hitrIO.getContractAmount()));
			}
			if ((isEQ(hitrIO.getSacscode(), wsaaT5645Sacscode[22]) && isEQ(hitrIO.getSacstyp(), wsaaT5645Sacstype[22]))
					|| (isEQ(hitrIO.getSacscode(), wsaaT5645Sacscode[23]) && isEQ(hitrIO.getSacstyp(),
							wsaaT5645Sacstype[23]))) {
				compute(wsaaInvest, 2).set(add(wsaaInvest, hitrIO.getContractAmount()));
			}
		}
	}

protected void getCurrentSuspense1560(List<Acblpf> acblenqList)
 {
		
	if(onePflag){
		wsaaCurrSusp.set(ZERO);
		for (Acblpf acblenqIO : acblenqList) {
			if (isNE(acblenqIO.getRldgacct(), chdrlnbIO.getChdrnum())
				|| (isNE(acblenqIO.getSacstyp(), wsaaT5645Sacstype[27]) && isNE(acblenqIO.getSacstyp(), wsaaT5645Sacstype[1]))
				|| (isNE(acblenqIO.getSacscode(), wsaaT5645Sacscode[27]) && isNE(acblenqIO.getSacscode(), wsaaT5645Sacscode[1]))
				|| isNE(acblenqIO.getRldgcoy(), chdrlnbIO.getChdrcoy())) {
			continue;
		}
		compute(wsaaEstimatedVal, 2).set(mult(acblenqIO.getSacscurbal(), (-1)));
		wsaaCurrcode.set(acblenqIO.getOrigcurr());
		wsaaCashdate.set(sv.reserveUnitsDate);
		convertValue6100();
		wsaaCurrSusp.add(conlinkrec.amountOut);
		}
	}
	
	else {
		wsaaCurrSusp.set(ZERO);
		for (Acblpf acblenqIO : acblenqList) {
			if (isNE(acblenqIO.getRldgacct(), chdrlnbIO.getChdrnum())
				|| isNE(acblenqIO.getSacstyp(), wsaaT5645Sacstype[1])
				|| isNE(acblenqIO.getSacscode(), wsaaT5645Sacscode[1])
				|| isNE(acblenqIO.getRldgcoy(), chdrlnbIO.getChdrcoy())) {
			continue;
		}
		compute(wsaaEstimatedVal, 2).set(mult(acblenqIO.getSacscurbal(), (-1)));
		wsaaCurrcode.set(acblenqIO.getOrigcurr());
		wsaaCashdate.set(sv.reserveUnitsDate);
		convertValue6100();
		wsaaCurrSusp.add(conlinkrec.amountOut);
		}
	}
}

protected void getChargesAndFees1570(List<Acblpf> acblenqList)
 {
		/* START */
		/* Get the sub account balances of charges and fees by going thru */
		/* all the sub account codes and types stored in WSAA-CHARGES-REC */
		wsaaChargeFee.set(ZERO);
		for (ix.set(1); !(isGT(ix, 30) || isEQ(wsaaChgSacscode[ix.toInt()], SPACES)); ix.add(1)) {
			for (Acblpf acblenqIO : acblenqList) {
				if (isEQ(acblenqIO.getRldgcoy(), chdrlnbIO.getChdrcoy())
						&& isEQ(acblenqIO.getSacscode(), wsaaChgSacscode[ix.toInt()])
						&& isEQ(acblenqIO.getSacstyp(), wsaaChgSacstype[ix.toInt()])
						&& isEQ(subString(acblenqIO.getRldgacct(), 1, 8), chdrlnbIO.getChdrnum())
						&& isEQ(acblenqIO.getOrigcurr(), chdrlnbIO.getCntcurr())) {
					compute(wsaaChargeFee, 2).set(add(wsaaChargeFee, mult(acblenqIO.getSacscurbal(), (-1))));
				}
			}
		}
		/* EXIT */
	}

protected void readT56791600()
	{
		/*   Read T5679 table*/
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());

	}

protected void readT66471700()
	{
		/*--- Read T6647 table*/
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t6647);
		wsaaT6647Batctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaT6647Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT6647Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6647rec.t6647Rec.set(itemIO.getGenarea());
	}


protected void getPendingMedicalFee1900()
 {
		sv.amnt.set(ZERO);
		List<Medipf> mediIOList = medipfDAO.searchMediRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum()
				.toString());
		if (mediIOList != null && !mediIOList.isEmpty()) {
			for (Medipf mediIO : mediIOList) {
				if (isEQ(mediIO.getPaydte(), varcom.vrcmMaxDate)) {
					continue;
				}
				PackedDecimalData tempFee = new PackedDecimalData(17, 2);
				tempFee.set(mediIO.getZmedfee());
				sv.amnt.add(tempFee);
				sv.billcurr.set(chdrlnbIO.getCntcurr());
			}
		}
		List<Preqpf> preqrlgIOList = preqpfDAO.searchPreqrlgRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO
				.getChdrnum().toString(), wsaaT5645Sacscode[9].toString(), wsaaT5645Sacstype[9].toString());
		if (preqrlgIOList != null && !preqrlgIOList.isEmpty()) {
			for (Preqpf preqrlgIO : preqrlgIOList) {
				PackedDecimalData tempFee = new PackedDecimalData(17, 2);
				tempFee.set(preqrlgIO.getOrigamt());
				sv.amnt.add(tempFee);
			}
		}
	}

protected void getDescriptions1950()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaItem);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setDescitem(wsaaItem);
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(sv.amnt, 0)) {
			sv.amntOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(sv.billcurr, SPACES)) {
			sv.billcurrOut[varcom.nd.toInt()].set("Y");
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			if(validate2020()){
				checkForErrors2080();
			}
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected boolean validate2020()
	{
		/* Check if KILL pressed*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return false;
		}
		/* Check if refresh pressed*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/* If Refund Unit basis*/
		if (refundUnit.isTrue()) {
			if (isEQ(tr52mrec.basind, "3")) {
				if (isLT(sv.reserveUnitsDate, hpadIO.getHissdte())) {
					sv.rundteErr.set(errorsInner.rlci);
				}
				if (isEQ(sv.reserveUnitsDate, varcom.vrcmMaxDate)) {
					sv.reserveUnitsDate.set(wsaaToday);
				}
				if (isGT(sv.reserveUnitsDate, wsaaToday)) {
					sv.rundteErr.set(errorsInner.rlcc);
				}
			}
			validateSp2400();
		}
		/* Recalculate the total amounts on the screen*/
		setupField1500();
		/* Conduct rounding validation to ensure those input value are     */
		/* tally with currency rounding rule:                              */
		if (isNE(sv.medamt01, ZERO)) {
			zrdecplrec.amountIn.set(sv.medamt01);
			zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
			a100CallRounding();
			if (isNE(sv.medamt01, zrdecplrec.amountOut)) {
				sv.medamt01Err.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.medamt02, ZERO)) {
			zrdecplrec.amountIn.set(sv.medamt02);
			zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
			a100CallRounding();
			if (isNE(sv.medamt02, zrdecplrec.amountOut)) {
				sv.medamt02Err.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.admamt01, ZERO)) {
			zrdecplrec.amountIn.set(sv.admamt01);
			zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
			a100CallRounding();
			if (isNE(sv.admamt01, zrdecplrec.amountOut)) {
				sv.admamt01Err.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.admamt02, ZERO)) {
			zrdecplrec.amountIn.set(sv.admamt02);
			zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
			a100CallRounding();
			if (isNE(sv.admamt02, zrdecplrec.amountOut)) {
				sv.admamt02Err.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.adjamt01, ZERO)) {
			zrdecplrec.amountIn.set(sv.adjamt01);
			zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
			a100CallRounding();
			if (isNE(sv.adjamt01, zrdecplrec.amountOut)) {
				sv.adjamt01Err.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.adjamt02, ZERO)) {
			zrdecplrec.amountIn.set(sv.adjamt02);
			zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
			a100CallRounding();
			if (isNE(sv.adjamt02, zrdecplrec.amountOut)) {
				sv.adjamt02Err.set(errorsInner.rfik);
			}
		}
		/* Calculate the total admin charge to be recovered*/
		compute(wsaaRecovAdmin, 2).set(add(sv.admamt01, sv.admamt02));
		/* Validate that total admin charge to be recovered cannot be*/
		/* greater than system calculated one*/
		if ((setPrecision(wsaaRecovAdmin, 2)
		&& isGT(wsaaRecovAdmin, (add(sv.admfee01, sv.admfee02))))) {
			if (isGT(sv.admamt02, ZERO)) {
				sv.admamt02Err.set(errorsInner.rlcd);
			}
			else {
				sv.admamt01Err.set(errorsInner.rlcd);
			}
		}
		/* Calculate the total medical fee to be recovered*/
		compute(wsaaRecovMed, 2).set(add(sv.medamt01, sv.medamt02));
		/* Validate that total medical fee to be recovered cannot*/
		/* be greater than system calculated one*/
		if ((setPrecision(wsaaRecovMed, 2)
		&& isGT(wsaaRecovMed, (add(sv.medfee01, sv.medfee02))))) {
			if (isGT(sv.medamt02, 0)) {
				sv.medamt02Err.set(errorsInner.rlce);
			}
			else {
				sv.medamt01Err.set(errorsInner.rlce);
			}
		}
		/* If refund payable to client is less than 0 then display error me*/
		if (isLT(sv.ztotamt, ZERO)) {
			if (refundUnit.isTrue()) {
				/* If no more refund unit value, user have to adjust the deduction*/
				if (isGT(wsaaUnitValue, ZERO)) {
					scrnparams.errorCode.set(errorsInner.rlcg);
				}
				else {
					sv.ztotamtErr.set(errorsInner.rlcj);
				}
			}
			/* If refund premium, user have to adjust the deduction*/
			if (refundPrem.isTrue()) {
				sv.ztotamtErr.set(errorsInner.rlcj);
			}
		}
		checkReason2100();
		sv.zdesc01.set(SPACES);
		sv.zdesc02.set(SPACES);
		if (isNE(sv.sacstypw01, SPACES)
		|| isNE(sv.adjamt01, ZERO)) {
			if (isEQ(sv.adjamt01, ZERO)) {
				sv.adjamt01Err.set(errorsInner.e199);
			}
			else {
				wsaaSacscode.set(sv.sacscode01);
				wsaaSacstype.set(sv.sacstypw01);
				checkT36982300();
				if (isEQ(descIO.getLongdesc(), SPACES)) {
					sv.sacstypw01Err.set(errorsInner.e192);
				}
				else {
					sv.zdesc01.set(descIO.getLongdesc());
					wsaaT3698Sacscode[1].set(wsaaSacscode);
					wsaaT3698Sacstyp[1].set(t3698rec.sacstyp[wsaaSub.toInt()]);
					wsaaT3698Contot[1].set(t3698rec.cnttot[wsaaSub.toInt()]);
					wsaaT3698Glcode[1].set(t3698rec.glmap[wsaaSub.toInt()]);
					wsaaT3698Glsign[1].set(t3698rec.sign[wsaaSub.toInt()]);
				}
			}
		}
		else {
			sv.zdesc01.set(SPACES);
		}
		if (isNE(sv.sacstypw02, SPACES)
		|| isNE(sv.adjamt02, ZERO)) {
			if (isEQ(sv.adjamt02, ZERO)) {
				sv.adjamt02Err.set(errorsInner.e199);
			}
			else {
				wsaaSacscode.set(sv.sacscode02);
				wsaaSacstype.set(sv.sacstypw02);
				checkT36982300();
				if (isEQ(descIO.getLongdesc(), SPACES)) {
					sv.sacstypw02Err.set(errorsInner.e192);
				}
				else {
					sv.zdesc02.set(descIO.getLongdesc());
					wsaaT3698Sacscode[2].set(wsaaSacscode);
					wsaaT3698Sacstyp[2].set(t3698rec.sacstyp[wsaaSub.toInt()]);
					wsaaT3698Contot[2].set(t3698rec.cnttot[wsaaSub.toInt()]);
					wsaaT3698Glcode[2].set(t3698rec.glmap[wsaaSub.toInt()]);
					wsaaT3698Glsign[2].set(t3698rec.sign[wsaaSub.toInt()]);
				}
			}
		}
		else {
			sv.zdesc02.set(SPACES);
		}
		return true;
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkReason2100()
	{
		if (isEQ(sv.reasoncd, SPACES)) {
			sv.reasoncdErr.set(errorsInner.h102);
			sv.resndesc.fill("?");
			return ;
		}
		descIO.setParams(SPACES);
		descIO.setDesctabl(tablesInner.t5500);
		wsaaItem.set(sv.reasoncd);
		getDescriptions1950();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.resndesc.set(descIO.getLongdesc());
		}
		else {
			sv.reasoncdErr.set(errorsInner.rlcf);
			sv.resndesc.fill("?");
		}

	}

protected void checkT36982300()
	{
		descIO.setLongdesc(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t3698);
		wsaaT3698Key.set(SPACES);
		wsaaT3698Key.setSub1String(1, 4, wsaaBatckey.batcBatctrcde);
		wsaaT3698Key.setSub1String(5, 2, wsaaSacscode);
		itemIO.setItemitem(wsaaT3698Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			t3698rec.t3698Rec.set(itemIO.getGenarea());
		}
		wsaaFound.set("N");
		for (ix.set(1); !(isEQ(t3698rec.sacstyp[ix.toInt()], SPACES)
		|| found.isTrue()); ix.add(1)){
			if (isEQ(t3698rec.sacstyp[ix.toInt()], wsaaSacstype)) {
				wsaaSub.set(ix);
				wsaaFound.set("Y");
			}
		}
		if (notFound.isTrue()) {
			return ;
		}
		descIO.setParams(SPACES);
		descIO.setDesctabl(tablesInner.t3695);
		wsaaItem.set(wsaaSacstype);
		getDescriptions1950();
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			fatalError600();
		}

	}

protected void validateSp2400()
 {
		List<Ptrnpf> ptrnrevIOList = ptrnpfDAO.getPtrnrevData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), "99999");
		if (ptrnrevIOList != null && !ptrnrevIOList.isEmpty()) {
			for (Ptrnpf ptrnrevIO : ptrnrevIOList) {
				wsaaTrancd.set(ptrnrevIO.getBatctrcde());
				if ((spTopUp.isTrue() || spTopUpSa.isTrue()) && isLT(sv.reserveUnitsDate, ptrnrevIO.getPtrneff())) {
					sv.rundteErr.set(errorsInner.rlch);
				}
			}
		}
	}


	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/* Convert the suspense account (LP/S) in other currencies into con*/
		/* currency*/
		setupLifacmv6200();
		if(acblenqList == null){
			acblenqList = acblpfDAO.searchAcblenqRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}

		for(Acblpf acblenqIO:acblenqList){
			postSuspExchg6300(acblenqIO);
		}
	
		/* Process any clawback from agent.*/
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			wsaaSplit[ix.toInt()].set(ZERO);
			wsaaAgentMed[ix.toInt()].set(ZERO);
			wsaaAgentAdm[ix.toInt()].set(ZERO);
			wsaaAgentOth[ix.toInt()].set(ZERO);
		}
		chargeToAgent3100();
		if (refundPrem.isTrue()) {
			refundPremium3200();
		}
		else {
			refundUnit3300();
		}
		/* Write the cancellation reason details.*/
		writeResn3800();
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}

	}

protected void chargeToAgent3100()
	{
		/* MOVE 0                      TO WSAA-JRNSEQ.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.trandesc.set(wsaaDesc);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		checkAgentSplit3110();
		splitCharges3120();
		postCharges3130();
	}

protected void checkAgentSplit3110()
	{
		List<Pcddpf> pcddlnbList = pcddpfDAO.searchPcddRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		ix.set(0);
		if(pcddlnbList == null || pcddlnbList.isEmpty()){
			wsaaAgntnum[1].set(chdrlnbIO.getAgntnum());
			wsaaSplit[1].set(100);
		}else{
			for(Pcddpf pcddlnbIO:pcddlnbList){
				if(isEQ(pcddlnbIO.getValidFlag(),"1")){
					ix.add(1);
					wsaaAgntnum[ix.toInt()].set(pcddlnbIO.getAgntNum());
					wsaaSplit[ix.toInt()].set(pcddlnbIO.getSplitC());
				}
			}
		}
	}

protected void splitCharges3120()
	{
		wsaaTempMed.set(0);
		wsaaTempAdm.set(0);
		wsaaTempOth.set(0);
		/* For the 2nd split agent up to the 10th split agent perform*/
		/* the following:*/
		for (ix.set(2); !(isGT(ix, 10)); ix.add(1)){
			compute(wsaaAgentMed[ix.toInt()], 2).set(div(mult(sv.medamt01, wsaaSplit[ix.toInt()]), 100));
			zrdecplrec.amountIn.set(wsaaAgentMed[ix.toInt()]);
			zrdecplrec.currency.set(sv.billcurr);
			a100CallRounding();
			wsaaAgentMed[ix.toInt()].set(zrdecplrec.amountOut);
			compute(wsaaAgentAdm[ix.toInt()], 2).set(div(mult(sv.admamt01, wsaaSplit[ix.toInt()]), 100));
			zrdecplrec.amountIn.set(wsaaAgentAdm[ix.toInt()]);
			zrdecplrec.currency.set(sv.billcurr);
			a100CallRounding();
			wsaaAgentAdm[ix.toInt()].set(zrdecplrec.amountOut);
			compute(wsaaAgentOth[ix.toInt()], 2).set(div(mult(sv.adjamt01, wsaaSplit[ix.toInt()]), 100));
			zrdecplrec.amountIn.set(wsaaAgentOth[ix.toInt()]);
			zrdecplrec.currency.set(sv.billcurr);
			a100CallRounding();
			wsaaAgentOth[ix.toInt()].set(zrdecplrec.amountOut);
			wsaaTempMed.add(wsaaAgentMed[ix.toInt()]);
			wsaaTempAdm.add(wsaaAgentAdm[ix.toInt()]);
			wsaaTempOth.add(wsaaAgentOth[ix.toInt()]);
		}
		/*  Calculate the 1st split agent charges as the remaining amount:*/
		compute(wsaaAgentMed[1], 2).set(sub(sv.medamt01, wsaaTempMed));
		compute(wsaaAgentAdm[1], 2).set(sub(sv.admamt01, wsaaTempAdm));
		compute(wsaaAgentOth[1], 2).set(sub(sv.adjamt01, wsaaTempOth));
	}


protected void postCharges3130()
	{
		/*  Go through each split agent charges to post any medical fee,*/
		/*  admin charges or other adjustment that will be charged to each*/
		/*  split agent:*/
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			if (isNE(wsaaAgentMed[ix.toInt()], 0)) {
				/*  Debit LA/MB to clawback the medical fee from agent:*/
				lifacmvrec.rldgacct.set(wsaaAgntnum[ix.toInt()]);
				lifacmvrec.origamt.set(wsaaAgentMed[ix.toInt()]);
				wsaaIx.set(7);
				callLifacmv3150();
				/*  Credit LP/ME to clawback the medical fee expense:*/
				lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaAgentMed[ix.toInt()]);
				wsaaIx.set(8);
				callLifacmv3150();
			}
			if (isNE(wsaaAgentAdm[ix.toInt()], 0)) {
				/*  Debit LA/CX to clawback the admin charges from agent:*/
				lifacmvrec.rldgacct.set(wsaaAgntnum[ix.toInt()]);
				lifacmvrec.origamt.set(wsaaAgentAdm[ix.toInt()]);
				wsaaIx.set(4);
				callLifacmv3150();
				/*  Credit LP/CG to post the admin charges:*/
				lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaAgentAdm[ix.toInt()]);
				wsaaIx.set(5);
				callLifacmv3150();
			}
			if (isNE(wsaaAgentOth[ix.toInt()], 0)) {
				/*  Debit LA/xx (depending on the sub account type entered)*/
				/*  to clawback the other charges from agent:*/
				lifacmvrec.rldgacct.set(wsaaAgntnum[ix.toInt()]);
				lifacmvrec.origamt.set(wsaaAgentOth[ix.toInt()]);
				wsaaJrnseq.add(1);
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				lifacmvrec.sacscode.set(wsaaT3698Sacscode[1]);
				lifacmvrec.sacstyp.set(wsaaT3698Sacstyp[1]);
				lifacmvrec.contot.set(wsaaT3698Contot[1]);
				lifacmvrec.glcode.set(wsaaT3698Glcode[1]);
				lifacmvrec.glsign.set(wsaaT3698Glsign[1]);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				if (isNE(lifacmvrec.statuz, varcom.oK)) {
					syserrrec.params.set(lifacmvrec.lifacmvRec);
					syserrrec.statuz.set(lifacmvrec.statuz);
					fatalError600();
				}
				/*  Credit LP/OG to post the other charges:*/
				lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaAgentOth[ix.toInt()]);
				wsaaIx.set(6);
				callLifacmv3150();
			}
		}
	}


protected void callLifacmv3150()
	{
		if (isEQ(lifacmvrec.origamt, 0)) {
			return ;
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaIx.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaIx.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[wsaaIx.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaIx.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaIx.toInt()]);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}

protected void refundPremium3200()
	{
		/*  Transfer premium suspense balance from LP/S by debiting LP/S:*/
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(sv.premi);
		wsaaIx.set(1);
		callLifacmv3150();
		/*  Credit LP/ME to clawback medical bill expense from client:*/
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(sv.medamt[2]);
		wsaaIx.set(8);
		callLifacmv3150();
		/*  Credit LP/CG to clawback administration charges from client:*/
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(sv.admamt[2]);
		wsaaIx.set(5);
		callLifacmv3150();
		/*  Credit LP/xx (depending on sub account type entered) to clawbac*/
		/*  other charges from client:*/
		if (isNE(sv.adjamt02, ZERO)) {
			lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
			lifacmvrec.origamt.set(sv.adjamt[2]);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.sacscode.set(wsaaT3698Sacscode[2]);
			lifacmvrec.sacstyp.set(wsaaT3698Sacstyp[2]);
			lifacmvrec.contot.set(wsaaT3698Contot[2]);
			lifacmvrec.glcode.set(wsaaT3698Glcode[2]);
			lifacmvrec.glsign.set(wsaaT3698Glsign[2]);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError600();
			}
		}
		/*  Credit LP/PS to post the amount to be refunded to the client:*/
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(sv.ztotamt);
		wsaaIx.set(2);
		callLifacmv3150();
		/*  Record the freelook cancellation history*/
		writeFcfi3600();
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
	}

protected void refundUnit3300()
	{
		/*  Update contract header details*/
		updateChdr3310();
		/*  Create policy transaction history*/
		createPtrn3320();
		/*  Reverse all commission postings*/
		reverseCommPostings3330();
		/*  Process posting for refund unit*/
		refundUnitPosting3350();
		/*  Delete un-processed unit linked transaction records*/
		deleteUnpprocUtrn3360();
		/*  Delete un-processed interest bearing transaction records*/
		deleteUnprocHitr3370();
		/*  Delete interest bearing fund interest records*/
		deleteHitd3390();
		/*  Write UTRN and HITR records to sell the funds*/
		if (isGT(wsaaUnitValue, 0)) {
			writeUtrn3400();
			writeHitr3500();
		}
		/*  Write FCFI records for further unit deal processing*/
		writeFcfi3600();
		/*  Update coverage status*/
		updateCovr3700();
		/*  Update payer details*/
		updatePayr3710();
		/*  Update flexible premium coverage details*/
		updateFpco3720();
		/*  Update flexible premium details*/
		updateFprm3730();
		/*  Update contract commission details*/
		updateAgcm3740();
		/*  Update billing extract details*/
		deleteBext3750();
		/*  Update contract beneficiary details*/
		updateBnfy3760();
		/*  Delete any Follow-up records associated with the contract*/
		deleteFlup3770();
		/*  Update contract life details*/
		updateLife3780();
		/*  Delete any life installment records associated with the*/
		/*  contract*/
		deleteLins3790();
		/*  Delete any unit linked coverage rider details associated*/
		/*  with the contract*/
		deleteUlnk3810();
		/*  Delete any individual coverage rider increase details*/
		/*  associated with the contract*/
		deleteInci3830();
		/*  Delete any reassurance cession details associated with the*/
		/*  contract*/
		deleteRacd3840();
		/*  Delete any life retention & reassurance details associated*/
		/*  with the contract*/
		deleteLrrh3850();
		/*  Delete any re-assurer experience history details associated*/
		/*  with the contract*/
		deleteLirr3860();
		/*  Update Bonus Workbench Extraction details*/
		updateBonusWorkbench3900();
	}

protected void updateChdr3310()
	{
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.readh);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		chdrlnbIO.setValidflag("2");
		chdrlnbIO.setCurrto(wsaaToday);
		chdrlnbIO.setFunction(varcom.rewrt);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		chdrlnbIO.setStatdate(chdrlnbIO.getCcdate());
		chdrlnbIO.setStatcode(t5679rec.setCnRiskStat);
		chdrlnbIO.setPstatcode(t5679rec.setCnPremStat);
		setPrecision(chdrlnbIO.getPstattran(), 0);
		chdrlnbIO.setPstattran(add(chdrlnbIO.getTranno(), 1));
		setPrecision(chdrlnbIO.getTranno(), 0);
		chdrlnbIO.setTranno(add(chdrlnbIO.getTranno(), 1));
		chdrlnbIO.setValidflag("1");
		chdrlnbIO.setCurrto(varcom.vrcmMaxDate);
		chdrlnbIO.setTranid(varcom.vrcmCompTranid);
		chdrlnbIO.setPstatdate(chdrlnbIO.getCcdate());
		chdrlnbIO.setBtdate(chdrlnbIO.getCcdate());
		chdrlnbIO.setPtdate(chdrlnbIO.getCcdate());
		chdrlnbIO.setBillcd(chdrlnbIO.getCcdate());
		chdrlnbIO.setAvlisu("N");
		chdrlnbIO.setFunction(varcom.writr);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
	}

protected void createPtrn3320()
	{
		/* Write a PTRN record.*/
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		ptrnIO.setTranno(chdrlnbIO.getTranno());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setPtrneff(chdrlnbIO.getOccdate());
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError600();
		}
	}

protected void reverseCommPostings3330()
 {
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.trandesc.set(wsaaDesc);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		if (acmvrevList == null) {
			acmvrevList = acmvpfDAO.searchAcmvrevRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}

		for (Acmvpf acmvrevIO : acmvrevList) {
			if (isEQ(acmvrevIO.getTranno(), chdrlnbIO.getTranno())) {
				break;
			}
			if (isEQ(acmvrevIO.getTranno(), 0)) {
				continue;
			}

			wsaaFound.set("N");
			for (ix.set(1); !(isGT(ix, 30) || found.isTrue() || isEQ(wsaaCommSacscode[ix.toInt()], SPACES)); ix.add(1)) {
				if (isEQ(wsaaCommSacscode[ix.toInt()], acmvrevIO.getSacscode())
						&& isEQ(wsaaCommSacstype[ix.toInt()], acmvrevIO.getSacstyp())) {
					wsaaCnttot.set(wsaaCommCnttot[ix.toInt()]);
					wsaaFound.set("Y");
				}
			}
			if (found.isTrue()) {
				wsaaJrnseq.add(1);
				lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
				lifacmvrec.tranref.set(acmvrevIO.getTranref());
				lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
				lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
				lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
				lifacmvrec.glcode.set(acmvrevIO.getGlcode());
				lifacmvrec.glsign.set(acmvrevIO.getGlsign());
				lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
				lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), (-1)));
				compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), (-1)));
				lifacmvrec.crate.set(acmvrevIO.getCrate());
				lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
				lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
				lifacmvrec.contot.set(wsaaCnttot);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				if (isNE(lifacmvrec.statuz, varcom.oK)) {
					syserrrec.params.set(lifacmvrec.lifacmvRec);
					syserrrec.statuz.set(lifacmvrec.statuz);
					fatalError600();
				}
			}
		}
	}

protected void refundUnitPosting3350()
	{
		begin3350();
		if(utrnList == null){
			utrnList = utrnpfDAO.searchUtrnRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		/*  Go through all non-invest premium postings (LC/NI or LE/NI)*/
		/*  by looping thru UTRN records for the policy and use the amounts*/
		/*  to reverse the premium income postings (LC/LP-SP or LE/LP-SP)*/
		/*  by debiting them*/
		for(Utrnpf utrnIO:utrnList){
			callUtrn3350(utrnIO);
		}
		if(hitrList == null){
			hitrList = hitrpfDAO.searchHitrRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		for (Hitrpf hitrIO : hitrList) {
			callHitr3350(hitrIO);
		}
		drCr3350();
		noUnitsRefund3350();
	}

protected void begin3350()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.trandesc.set(wsaaDesc);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		/*  Accumulate the total refund payable to the client excluding*/
		/*  the unit value since it could be still estimated amount:*/
		if (isGT(wsaaUnitValue, 0)) {
			compute(wsaaAmountAll, 3).setRounded(sub(sub(add(add(add(wsaaPpRiderPrem, wsaaNonInvest), wsaaCurrSusp), wsaaChargeFee), wsaaOsDebt), wsaaTolerance));
		}
		else {
			compute(wsaaAmountAll, 3).setRounded(sub(sub(sub(sub(sub(add(add(add(wsaaPpRiderPrem, wsaaNonInvest), wsaaCurrSusp), wsaaChargeFee), wsaaOsDebt), wsaaTolerance), sv.medamt02), sv.admamt02), sv.adjamt02));
		}
		/*  When policy with unit balance to refund, then credit LC/CO to p*/
		/*  the cancellation outstanding payment calculated previously:*/
		if (isGT(wsaaUnitValue, 0)) {
			lifacmvrec.origamt.set(wsaaAmountAll);
			wsaaIx.set(16);
			callLifacmv3150();
		}
		/*  Credit LP/PT to reverse the tolerance postings.*/
		lifacmvrec.origamt.set(wsaaPolTolerance);
		wsaaIx.set(3);
		callLifacmv3150();
		/*  Reverse any outstanding coverage debts.                       /*/
		reverseOsCovrDebt5100();
		/*  Reverse Premium Paying Riders postings.                       /*/
		reversePpRiderPosts5500();
		/*  Reverse all charges and fees postings (LE/MC, LE/FE, LE/IF, LE/*/
		/*  LE/AF, etc.)*/
		reverseChargesAndFees3380();
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
	}

protected void callUtrn3350(Utrnpf utrnIO)
 {
		if (isEQ(wsaaT5645Sacscode[10], utrnIO.getSacscode()) && isEQ(wsaaT5645Sacstype[10], utrnIO.getSacstyp())) {
			lifacmvrec.origamt.set(utrnIO.getContractAmount());
			wsaaTrancd.set(utrnIO.getBatctrcde());
			if (isEQ(chdrlnbIO.getBillfreq(), "00") || isEQ(utrnIO.getInciNum(), ZERO) || spTopUp.isTrue()
					|| spTopUpSa.isTrue()) {
				wsaaIx.set(14);
			} else {
				wsaaIx.set(12);
			}
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
			lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
			lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
			callLifacmv3150();
		}
		if (isEQ(wsaaT5645Sacscode[11], utrnIO.getSacscode()) && isEQ(wsaaT5645Sacstype[11], utrnIO.getSacstyp())) {
			lifacmvrec.origamt.set(utrnIO.getContractAmount());
			wsaaTrancd.set(utrnIO.getBatctrcde());
			if (isEQ(chdrlnbIO.getBillfreq(), "00") || isEQ(utrnIO.getInciNum(), ZERO) || spTopUp.isTrue()
					|| spTopUpSa.isTrue()) {
				wsaaIx.set(15);
			} else {
				wsaaIx.set(13);
			}
			wsbbChdrnum.set(utrnIO.getChdrnum());
			wsbbLife.set(utrnIO.getLife());
			wsbbCoverage.set(utrnIO.getCoverage());
			wsbbRider.set(utrnIO.getRider());
			wsaaPlnsfx.set(utrnIO.getPlanSuffix());
			wsbbPlnsfx.set(wsaaPlnsfx);
			lifacmvrec.rldgacct.set(wsbbRldgacct);
			lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
			if (covrlnbList == null) {
				covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
			}
			Covrpf covrlnbIO = null;
			for (Covrpf c : covrlnbList) {
				if (c.getLife().equals(utrnIO.getLife()) && c.getCoverage().equals(utrnIO.getCoverage())
						&& c.getRider().equals(utrnIO.getRider()) && c.getPlanSuffix() == utrnIO.getPlanSuffix()
						&& isEQ(c.getValidflag(), "1")) {

					covrlnbIO = c;
					break;
				}
			}
			if (covrlnbIO == null) {
				syserrrec.params.set(chdrlnbIO.getChdrnum());
				fatalError600();
			} else { //IJTI-462
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable()); //IJTI-462
				callLifacmv3150(); //IJTI-462
			} //IJTI-462
		}
	}


	/**
	* <pre>
	*  Go through all non-invest premium postings (LC/NI or LE/NI)
	*  by looping thru HITR records for the policy and use the amounts
	*  to reverse the premium income postings (LC/LP-SP or LE/LP-SP)
	*  by debiting them
	* </pre>
	*/
protected void callHitr3350(Hitrpf hitrIO)
 {
		if (isEQ(wsaaT5645Sacscode[10], hitrIO.getSacscode()) && isEQ(wsaaT5645Sacstype[10], hitrIO.getSacstyp())) {
			lifacmvrec.origamt.set(hitrIO.getContractAmount());
			wsaaTrancd.set(hitrIO.getBatctrcde());
			if (isEQ(chdrlnbIO.getBillfreq(), "00") || isEQ(hitrIO.getInciNum(), ZERO) || spTopUp.isTrue()
					|| spTopUpSa.isTrue()) {
				wsaaIx.set(14);
			} else {
				wsaaIx.set(12);
			}
			lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
			lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
			lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
			callLifacmv3150();
		}
		if (isEQ(wsaaT5645Sacscode[11], hitrIO.getSacscode()) && isEQ(wsaaT5645Sacstype[11], hitrIO.getSacstyp())) {
			lifacmvrec.origamt.set(hitrIO.getContractAmount());
			wsaaTrancd.set(hitrIO.getBatctrcde());
			if (isEQ(chdrlnbIO.getBillfreq(), "00") || isEQ(hitrIO.getInciNum(), ZERO) || spTopUp.isTrue()
					|| spTopUpSa.isTrue()) {
				wsaaIx.set(15);
			} else {
				wsaaIx.set(13);
			}
			wsbbChdrnum.set(hitrIO.getChdrnum());
			wsbbLife.set(hitrIO.getLife());
			wsbbCoverage.set(hitrIO.getCoverage());
			wsbbRider.set(hitrIO.getRider());
			wsaaPlnsfx.set(hitrIO.getPlanSuffix());
			wsbbPlnsfx.set(wsaaPlnsfx);
			lifacmvrec.rldgacct.set(wsbbRldgacct);
			lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
			if (covrlnbList == null) {
				covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
			}
			Covrpf covrlnbIO = null;
			for (Covrpf c : covrlnbList) {
				if (c.getLife().equals(hitrIO.getLife()) && c.getCoverage().equals(hitrIO.getCoverage())
						&& c.getRider().equals(hitrIO.getRider()) && c.getPlanSuffix() == hitrIO.getPlanSuffix()
						&& isEQ(c.getValidflag(), "1")) {
					covrlnbIO = c;
					break;
				}
			}
			if (covrlnbIO == null) {
				syserrrec.params.set(chdrlnbIO.getChdrnum());
				fatalError600();
			} else { //IJTI-462
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable()); //IJTI-462
				callLifacmv3150(); //IJTI-462
			} //IJTI-462
		}
	}

protected void drCr3350()
	{
		/*  Debit LP/S to refund any remaining suspense amount*/
		lifacmvrec.origamt.set(wsaaCurrSusp);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		wsaaIx.set(1);
		lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		callLifacmv3150();
	}

protected void noUnitsRefund3350()
	{
		/*  Only conduct below posting when there is no unit balance*/
		if (isNE(wsaaUnitValue, 0)) {
			return ;
		}
		/*  Credit LP/ME to charge the medical bill expense to client*/
		lifacmvrec.origamt.set(sv.medamt02);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		wsaaIx.set(8);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		callLifacmv3150();
		/*  Credit LP/CG to charge admin charge to client*/
		lifacmvrec.origamt.set(sv.admamt02);
		wsaaIx.set(5);
		callLifacmv3150();
		/*  Credit LP/xx (depending on the sub account type entered)*/
		/*  to charge other adjustment to client:*/
		if (isNE(sv.adjamt02, 0)) {
			lifacmvrec.origamt.set(sv.adjamt[2]);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.sacscode.set(wsaaT3698Sacscode[2]);
			lifacmvrec.sacstyp.set(wsaaT3698Sacstyp[2]);
			lifacmvrec.contot.set(wsaaT3698Contot[2]);
			lifacmvrec.glcode.set(wsaaT3698Glcode[2]);
			lifacmvrec.glsign.set(wsaaT3698Glsign[2]);
			lifacmvrec.batckey.set(wsspcomn.batchkey);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError600();
			}
		}
		/*  Credit LP/PS to post the amount to be refunded to the client:*/
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(sv.ztotamt);
		wsaaIx.set(2);
		callLifacmv3150();
	}

protected void deleteUnpprocUtrn3360()
	{
		Utrnpf utrnpf = new Utrnpf();
		utrnpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		utrnpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		List<Utrnpf> utrnaloList = utrnpfDAO.readUtrnpf(utrnpf);
		utrnpfDAO.deleteUtrnrevRecord(utrnaloList);
	}

protected void deleteUnprocHitr3370()
	{
		List<Hitrpf> hitraloList = hitrpfDAO.searchHitraloRecordForDel(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		hitrpfDAO.deleteHitrpfRecord(hitraloList);
	}

protected void reverseChargesAndFees3380()
 {
		if (acmvrevList == null) {
			acmvrevList = acmvpfDAO.searchAcmvrevRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		for (Acmvpf acmvrevIO : acmvrevList) {
			if (isEQ(acmvrevIO.getTranno(), chdrlnbIO.getTranno())) {
				break;
			}
			wsaaFound.set("N");
			for (ix.set(1); !(isGT(ix, 30) || found.isTrue() || isEQ(wsaaChgSacscode[ix.toInt()], SPACES)); ix.add(1)) {
				if (isEQ(wsaaChgSacscode[ix.toInt()], acmvrevIO.getSacscode())
						&& isEQ(wsaaChgSacstype[ix.toInt()], acmvrevIO.getSacstyp())) {
					wsaaCnttot.set(wsaaChgCnttot[ix.toInt()]);
					wsaaFound.set("Y");
				}
			}
			if (found.isTrue()) {
				wsaaJrnseq.add(1);
				lifacmvrec.tranref.set(acmvrevIO.getTranref());
				lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
				lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
				lifacmvrec.glcode.set(acmvrevIO.getGlcode());
				lifacmvrec.glsign.set(acmvrevIO.getGlsign());
				lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
				lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), (-1)));
				compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), (-1)));
				lifacmvrec.crate.set(acmvrevIO.getCrate());
				lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
				lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
				lifacmvrec.contot.set(wsaaCnttot);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				if (isNE(lifacmvrec.statuz, varcom.oK)) {
					syserrrec.params.set(lifacmvrec.lifacmvRec);
					syserrrec.statuz.set(lifacmvrec.statuz);
					fatalError600();
				}
			}
		}
	}



protected void deleteHitd3390()
	{
	
		List<Hitdpf> hitdrevList = hitdpfDAO.searchHitdrevRecordForDel(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		hitdpfDAO.deleteHitdpfRecord(hitdrevList);
	}

protected void writeUtrn3400()
	{
		if(utrssurList == null){
			utrssurList = utrspfDAO.searchUtrsRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		for(Utrspf utrssurIO:utrssurList){
			callUtrssur3420(utrssurIO);
		}
		if(insertUtrnIOList!=null&&!insertUtrnIOList.isEmpty()){
			utrnpfDAO.insertUtrnRecoed(insertUtrnIOList);
			insertUtrnIOList.clear();
		}
	}

protected void callUtrssur3420(Utrspf utrssurIO)
	{
		if (covrlnbList == null) {
			covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		Covrpf covrlnbIO = null;
		for (Covrpf c : covrlnbList) {
			if (c.getLife().equals(utrssurIO.getLife()) && c.getCoverage().equals(utrssurIO.getCoverage())
					&& c.getRider().equals(utrssurIO.getRider()) && c.getPlanSuffix() == utrssurIO.getPlanSuffix()
					&& isEQ(c.getValidflag(), "1")) {
				covrlnbIO = c;
				break;
			}
		}
		if (covrlnbIO == null) {
			syserrrec.params.set(chdrlnbIO.getChdrnum());
			fatalError600();
		}
		Utrnpf utrnIO = new Utrnpf();
		utrnIO.setUstmno(0);
		utrnIO.setFundRate(BigDecimal.ZERO);
		utrnIO.setStrpdate(0l);
		utrnIO.setNofUnits(BigDecimal.ZERO);
		utrnIO.setNofDunits(BigDecimal.ZERO);
		utrnIO.setMoniesDate(0l);
		utrnIO.setPriceDateUsed(0l);
		utrnIO.setJobnoPrice(BigDecimal.ZERO);
		utrnIO.setPriceUsed(BigDecimal.ZERO);
		utrnIO.setUnitBarePrice(BigDecimal.ZERO);
		utrnIO.setInciNum(0);
		utrnIO.setInciPerd01(0);
		utrnIO.setInciPerd02(0);
		utrnIO.setInciprm01(BigDecimal.ZERO);
		utrnIO.setInciprm02(BigDecimal.ZERO);
		utrnIO.setContractAmount(BigDecimal.ZERO);
		utrnIO.setFundAmount(BigDecimal.ZERO);
		utrnIO.setDiscountFactor(BigDecimal.ZERO);
		utrnIO.setCrComDate(0l);
		utrnIO.setChdrcoy(utrssurIO.getChdrcoy());
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		utrnIO.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		utrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		utrnIO.setChdrnum(utrssurIO.getChdrnum());
		utrnIO.setPlanSuffix(utrssurIO.getPlanSuffix());
		utrnIO.setLife(utrssurIO.getLife());
		utrnIO.setCoverage(utrssurIO.getCoverage());
		utrnIO.setRider(utrssurIO.getRider());
		utrnIO.setCrtable(covrlnbIO.getCrtable());
		utrnIO.setMoniesDate(sv.reserveUnitsDate.toLong());
		utrnIO.setProcSeqNo(t6647rec.procSeqNo.toInt());
		utrnIO.setSurrenderPercent(new BigDecimal(100));
		utrnIO.setUnitVirtualFund(utrssurIO.getUnitVirtualFund());
		utrnIO.setCnttyp(chdrlnbIO.getCnttype().toString());
		utrnIO.setNowDeferInd(t6647rec.dealin.toString());
		utrnIO.setTriggerModule("FTRGCFI");
		utrnIO.setTriggerKey(SPACES.toString());
		utrnIO.setTransactionDate(varcom.vrcmDate.toInt());
		utrnIO.setTransactionTime(varcom.vrcmTime.toInt());
		utrnIO.setTranno(chdrlnbIO.getTranno().toInt());
		utrnIO.setUser(varcom.vrcmUser.toInt());
		utrnIO.setUnitType(utrssurIO.getUnitType());
		utrnIO.setSvp(BigDecimal.ONE);
		utrnIO.setTermid(SPACES.toString());
		utrnIO.setCntcurr(chdrlnbIO.getCntcurr().toString());
		if (isEQ(utrnIO.getUnitType(), "A")) {
			utrnIO.setUnitSubAccount("ACUM");
		}
		else {
			utrnIO.setUnitSubAccount("INIT");
		}
		if(t5515Map == null || t5515Map.isEmpty()){
			t5515Map = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), wsspcomn.company.toString(), "T5515");
		}
		boolean foundFlag = false;
		if (t5515Map != null && t5515Map.containsKey(utrnIO.getUnitVirtualFund())) {
			List<Itempf> itempfList = t5515Map.get(utrnIO.getUnitVirtualFund());
			for (Itempf itempf : itempfList) {
				if (itempf.getItmfrm().compareTo(new BigDecimal(utrnIO.getMoniesDate())) <= 0) {
					t5515rec.t5515Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					foundFlag = true;
				}
			}
		}
		if (!foundFlag) {
			t5515rec.t5515Rec.set(SPACES);
		}
		utrnIO.setFundCurrency(t5515rec.currcode.toString());
		
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(wsaaT5645Sacscode[17].toString());
			utrnIO.setSacstyp(wsaaT5645Sacstype[17].toString());
			utrnIO.setGenlcde(wsaaT5645Glmap[17].toString());
		}
		else {
			utrnIO.setSacscode(wsaaT5645Sacscode[16].toString());
			utrnIO.setSacstyp(wsaaT5645Sacstype[16].toString());
			utrnIO.setGenlcde(wsaaT5645Glmap[16].toString());
		}
		if(insertUtrnIOList == null){
			insertUtrnIOList = new LinkedList<>();	
		}
		utrnIO.setFeedbackInd(SPACE);//ILIFE-8428
		insertUtrnIOList.add(utrnIO);
	}

protected void writeHitr3500()
	{
		if(hitsList == null){
			List<String> chdrnumList = new ArrayList<>();
			String chdrnum = chdrlnbIO.getChdrnum().toString();
			chdrnumList.add(chdrnum);
			Map<String, List<Hitspf>> hitsMap = hitspfDAO.searchHitsRecordByChdrcoy(chdrlnbIO.getChdrcoy().toString(),
					chdrnumList);
			if (hitsMap != null && hitsMap.containsKey(chdrnum)) {
				hitsList = hitsMap.get(chdrnum);
			}
		}
		if(hitsList!=null){
			for (Hitspf h : hitsList) {
				callHits3520(h);
			}
		}
		if(insertHitrIOList != null&& !insertHitrIOList.isEmpty()){
			hitrpfDAO.insertHitrpfRecord(insertHitrIOList);
			insertHitrIOList.clear();
		}
	}

protected void callHits3520(Hitspf hitsIO)
	{
		if (covrlnbList == null) {
			covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		Covrpf covrlnbIO = null;
		for (Covrpf c : covrlnbList) {
			if (c.getLife().equals(hitsIO.getLife()) && c.getCoverage().equals(hitsIO.getCoverage())
					&& c.getRider().equals(hitsIO.getRider()) && c.getPlanSuffix() == hitsIO.getPlanSuffix()
					&& isEQ(c.getValidflag(), "1")) {
				covrlnbIO = c;
				break;
			}
		}
		if (covrlnbIO == null) {
			syserrrec.params.set(chdrlnbIO.getChdrnum());
			fatalError600();
		}
		Hitrpf hitrIO = new Hitrpf();
		hitrIO.setUstmno(0);
		hitrIO.setFundRate(BigDecimal.ZERO);
		hitrIO.setInciNum(0);
		hitrIO.setInciPerd01(0);
		hitrIO.setInciPerd02(0);
		hitrIO.setInciprm01(BigDecimal.ZERO);
		hitrIO.setInciprm02(BigDecimal.ZERO);
		hitrIO.setContractAmount(BigDecimal.ZERO);
		hitrIO.setFundAmount(BigDecimal.ZERO);
		hitrIO.setChdrcoy(hitsIO.getChdrcoy());
		hitrIO.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		hitrIO.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		hitrIO.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		hitrIO.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		hitrIO.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		hitrIO.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		hitrIO.setChdrnum(hitsIO.getChdrnum());
		hitrIO.setPlanSuffix(hitsIO.getPlanSuffix());
		hitrIO.setLife(hitsIO.getLife());
		hitrIO.setCoverage(hitsIO.getCoverage());
		hitrIO.setRider(hitsIO.getRider());
		hitrIO.setCrtable(covrlnbIO.getCrtable());
		hitrIO.setProcSeqNo(t6647rec.procSeqNo.toInt());
		hitrIO.setSurrenderPercent(new BigDecimal(100));
		hitrIO.setZintbfnd(hitsIO.getZintbfnd());
		hitrIO.setCnttyp(chdrlnbIO.getCnttype().toString());
		hitrIO.setTriggerModule("FTRGCFI");
		hitrIO.setTriggerKey(SPACES.toString());
		hitrIO.setTranno(chdrlnbIO.getTranno().toInt());
		hitrIO.setSvp(BigDecimal.ONE);
		hitrIO.setZrectyp("P");
		hitrIO.setCntcurr(chdrlnbIO.getCntcurr().toString());
		hitrIO.setEffdate(wsaaToday.toInt());
		hitrIO.setZintalloc("N");
		hitrIO.setZintrate(BigDecimal.ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate.toInt());
		hitrIO.setZlstintdte(varcom.vrcmMaxDate.toInt());
		if(t5515Map == null || t5515Map.isEmpty()){
			t5515Map = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), wsspcomn.company.toString(), "T5515");
		}
		boolean foundFlag = false;
		if (t5515Map != null && t5515Map.containsKey(hitrIO.getZintbfnd())) {
			List<Itempf> itempfList = t5515Map.get(hitrIO.getZintbfnd());
			for (Itempf itempf : itempfList) {
				if (itempf.getItmfrm().compareTo(wsaaToday.getbigdata()) <= 0) {
					t5515rec.t5515Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					foundFlag = true;
				}
			}
		}
		if (!foundFlag) {
			t5515rec.t5515Rec.set(SPACES);
		}
		hitrIO.setFeedbackInd(SPACE);
		hitrIO.setFundCurrency(t5515rec.currcode.toString());
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			hitrIO.setSacscode(wsaaT5645Sacscode[17].toString());
			hitrIO.setSacstyp(wsaaT5645Sacstype[17].toString());
			hitrIO.setGenlcde(wsaaT5645Glmap[17].toString());
		}
		else {
			hitrIO.setSacscode(wsaaT5645Sacscode[16].toString());
			hitrIO.setSacstyp(wsaaT5645Sacstype[16].toString());
			hitrIO.setGenlcde(wsaaT5645Glmap[16].toString());
		}
		if(insertHitrIOList == null){
			insertHitrIOList = new LinkedList<>();
		}
		insertHitrIOList.add(hitrIO);
	}

protected void writeFcfi3600()
	{
		fcfiIO.setParams(SPACES);
		fcfiIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		fcfiIO.setChdrnum(chdrlnbIO.getChdrnum());
		fcfiIO.setTranno(chdrlnbIO.getTranno());
		fcfiIO.setEffdate(wsaaToday);
		fcfiIO.setRatebas(wsaaRefundBasis);
		fcfiIO.setMedamt(1, sv.medamt[1]);
		fcfiIO.setMedamt(2, sv.medamt[2]);
		fcfiIO.setAdmamt(1, sv.admamt[1]);
		fcfiIO.setAdmamt(2, sv.admamt[2]);
		fcfiIO.setAdjamt(1, sv.adjamt[1]);
		fcfiIO.setSacscode01(sv.sacscode[1]);
		fcfiIO.setSacstyp01(sv.sacstypw[1]);
		fcfiIO.setAdjamt(2, sv.adjamt[2]);
		fcfiIO.setSacscode02(sv.sacscode[2]);
		fcfiIO.setSacstyp02(sv.sacstypw[2]);
		fcfiIO.setReserveUnitsDate(sv.reserveUnitsDate);
		if (refundPrem.isTrue()) {
			fcfiIO.setSusamt(sv.premi);
			fcfiIO.setActval(0);
			fcfiIO.setZtotamt(sv.ztotamt);
		}
		else {
			fcfiIO.setSusamt(wsaaAmountAll);
			fcfiIO.setActval(0);
			fcfiIO.setZtotamt(0);
		}
		fcfiIO.setFormat(formatsInner.fcfirec);
		fcfiIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fcfiIO);
		if (isNE(fcfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fcfiIO.getParams());
			syserrrec.statuz.set(fcfiIO.getStatuz());
			fatalError600();
		}
	}

protected void updateCovr3700()
	{
		if (covrlnbList == null) {
			covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		for (Covrpf c : covrlnbList) {
			callCovrlnb3700(c);
		}
		if (updateCovrlnbList != null && !updateCovrlnbList.isEmpty()) {
			covrpfDAO.updateCovrValidFlag(updateCovrlnbList);
			updateCovrlnbList.clear();
		}
		if (insertCovrlnbList != null && !insertCovrlnbList.isEmpty()) {
			covrpfDAO.insertCovrRecord(insertCovrlnbList);
			insertCovrlnbList.clear();
		}	
	}

protected void callCovrlnb3700(Covrpf covrlnbIO)
	{
		if (isEQ(covrlnbIO.getTranno(), chdrlnbIO.getTranno())
		|| isEQ(covrlnbIO.getValidflag(), "2")) {
			return;
		}
		/*    Verify the Contract Risk Statuz*/
		trcdeNotMatch.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| trcdeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(covrlnbIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				trcdeMatch.setTrue();
			}
		}
		if (trcdeNotMatch.isTrue()) {
			return;
		}
		Covrpf covrpfOld = new Covrpf(covrlnbIO);
		covrpfOld.setValidflag("2");
		covrpfOld.setCurrto(wsaaToday.toInt());
		if(updateCovrlnbList == null){
			updateCovrlnbList = new LinkedList<>();
		}
		updateCovrlnbList.add(covrpfOld);
		
		Covrpf covrpfNew = new Covrpf(covrlnbIO);
		covrpfNew.setValidflag("1");
		covrpfNew.setCurrto(0);
		covrpfNew.setTranno(chdrlnbIO.getTranno().toInt());
		covrpfNew.setRerateDate(0);
		covrpfNew.setBenBillDate(0);
		covrpfNew.setCoverageDebt(BigDecimal.ZERO);
		if (isEQ(covrpfNew.getRider(), "00")) {
			covrpfNew.setStatcode(t5679rec.setCovRiskStat.toString());
			covrpfNew.setPstatcode(t5679rec.setCovPremStat.toString());
		}
		else {
			covrpfNew.setStatcode(t5679rec.setRidRiskStat.toString());
			covrpfNew.setPstatcode(t5679rec.setRidPremStat.toString());
		}
		if(insertCovrlnbList == null){
			insertCovrlnbList = new LinkedList<>();
		}
		insertCovrlnbList.add(covrpfNew);
	}

protected void updatePayr3710()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		payrIO.setPayrseqno(ZERO);
		payrIO.setFunction(varcom.begn);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrnum(), payrIO.getChdrnum())
		|| isNE(chdrlnbIO.getChdrcoy(), payrIO.getChdrcoy())
		|| isEQ(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		payrIO.setTranno(chdrlnbIO.getTranno());
		payrIO.setBtdate(chdrlnbIO.getCcdate());
		payrIO.setPtdate(chdrlnbIO.getCcdate());
		payrIO.setBillcd(chdrlnbIO.getCcdate());
		payrIO.setEffdate(chdrlnbIO.getCcdate());
		payrIO.setNextdate(chdrlnbIO.getCcdate());
		payrIO.setPstatcode(chdrlnbIO.getPstatcode());
		payrIO.setTermid(varcom.vrcmTermid);
		payrIO.setUser(varcom.vrcmUser);
		payrIO.setTransactionDate(varcom.vrcmDate);
		payrIO.setTransactionTime(varcom.vrcmTime);
		payrIO.setFunction(varcom.writd);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void updateFpco3720()
	{
		if(begin3720()){
			List<Fpcopf> fpcoIOList = fpcopfDAO.searchFpcoRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
			for(Fpcopf fpcoIO:fpcoIOList){
				callFpco3720(fpcoIO);
			}
			if(insertFpcolf1IOList !=null && !insertFpcolf1IOList.isEmpty()){
				fpcopfDAO.insertFpcoRecord(insertFpcolf1IOList);
				insertFpcolf1IOList.clear();
			}
			if(updateFpcoIOList != null && !updateFpcoIOList.isEmpty()){
				fpcopfDAO.updateInvalidFpcoRecord(updateFpcoIOList);
				updateFpcoIOList.clear();
			}
		}
	}

protected boolean begin3720()
	{
		wsaaFlexPrem.set("N");
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5729);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItemcoy(), wsspcomn.company)
		&& isEQ(itdmIO.getItemtabl(), tablesInner.t5729)
		&& isEQ(subString(itdmIO.getItemitem(), 1, 3), chdrlnbIO.getCnttype())) {
			wsaaFlexPrem.set("Y");
		}
		else {
			return false;
		}
		return true;
	}

protected void callFpco3720(Fpcopf fpcoIO)
	{
		if (isEQ(fpcoIO.getTranno(), chdrlnbIO.getTranno())) {
			return;
		}
		Fpcopf fpcoIOOld = new Fpcopf();
		fpcoIOOld.setUniqueNumber(fpcoIO.getUniqueNumber());
		fpcoIOOld.setValidflag("2");
		fpcoIOOld.setCurrto(chdrlnbIO.getCcdate().toInt());
		if(updateFpcoIOList == null){
			updateFpcoIOList = new LinkedList<>();
		}
		updateFpcoIOList.add(fpcoIOOld);
		
		Fpcopf fpcolf1IO = new Fpcopf();
		fpcolf1IO.setChdrcoy(fpcoIO.getChdrcoy());
		fpcolf1IO.setChdrnum(fpcoIO.getChdrnum());
		fpcolf1IO.setValidflag("1");
		fpcolf1IO.setCurrto(varcom.vrcmMaxDate.toInt());
		fpcolf1IO.setCurrfrom(chdrlnbIO.getCcdate().toInt());
		fpcolf1IO.setEffdate(chdrlnbIO.getCcdate().toInt());
		fpcolf1IO.setTargfrom(fpcoIO.getTargfrom());
		fpcolf1IO.setTargto(fpcoIO.getTargto());
		fpcolf1IO.setLife(fpcoIO.getLife());
		fpcolf1IO.setCoverage(fpcoIO.getCoverage());
		fpcolf1IO.setRider(fpcoIO.getRider());
		fpcolf1IO.setPlnsfx(fpcoIO.getPlnsfx());
		fpcolf1IO.setActind("N");
		fpcolf1IO.setAnproind("N");
		fpcolf1IO.setPrmper(fpcoIO.getPrmper());
		fpcolf1IO.setPrmrcdp(BigDecimal.ZERO);
		fpcolf1IO.setBilledp(BigDecimal.ZERO);
		fpcolf1IO.setOvrminreq(BigDecimal.ZERO);
		fpcolf1IO.setMinovrpro(0);
		fpcolf1IO.setTranno(chdrlnbIO.getTranno().toInt());
		fpcolf1IO.setCbanpr(fpcoIO.getCbanpr());
		
		if(insertFpcolf1IOList == null){
			insertFpcolf1IOList = new LinkedList<Fpcopf>();
		}
		insertFpcolf1IOList.add(fpcolf1IO);
	}

protected void updateFprm3730()
	{
		if (!flexPrem.isTrue()) {
			return ;
		}
		fprmIO.setDataKey(SPACES);
		fprmIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		fprmIO.setChdrnum(chdrlnbIO.getChdrnum());
		fprmIO.setPayrseqno(1);
		fprmIO.setFormat(formatsInner.fprmrec);
		fprmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmIO.getStatuz());
			syserrrec.params.set(fprmIO.getParams());
			fatalError600();
		}
		fprmIO.setValidflag("2");
		fprmIO.setCurrto(chdrlnbIO.getCcdate());
		fprmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmIO.getStatuz());
			syserrrec.params.set(fprmIO.getParams());
			fatalError600();
		}
		fprmIO.setCurrfrom(chdrlnbIO.getCcdate());
		fprmIO.setCurrto(chdrlnbIO.getCcdate());
		fprmIO.setValidflag("1");
		fprmIO.setTotalRecd(0);
		fprmIO.setTotalBilled(0);
		fprmIO.setMinPrmReqd(0);
		fprmIO.setTranno(chdrlnbIO.getTranno());
		fprmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmIO.getStatuz());
			syserrrec.params.set(fprmIO.getParams());
			fatalError600();
		}
	}

protected void updateAgcm3740()
	{
		Agcmpf a = new Agcmpf();
		a.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		a.setChdrnum(chdrlnbIO.getChdrnum().toString());
		List<Agcmpf> agcmrevIOList = agcmpfDAO.searchAgcmpfRecord(a);
		if(agcmrevIOList!=null&&!agcmrevIOList.isEmpty()){
			for(Agcmpf agcmrevIO:agcmrevIOList){
				if(isNE(agcmrevIO.getValidflag(),"2")){
					callAgcmrev3740(agcmrevIO);
				}
			}
			agcmpfDAO.updateAgcmrevdRecord(updateAgcmrevIOList);
			updateAgcmrevIOList.clear();
		}
	}

protected void callAgcmrev3740(Agcmpf agcmrevIO)
	{
		agcmrevIO.setCompay(BigDecimal.ZERO);
		agcmrevIO.setComern(BigDecimal.ZERO);
		agcmrevIO.setRnlcdue(BigDecimal.ZERO);
		agcmrevIO.setRnlcearn(BigDecimal.ZERO);
		agcmrevIO.setScmdue(BigDecimal.ZERO);
		agcmrevIO.setScmearn(BigDecimal.ZERO);
		agcmrevIO.setPtdate(chdrlnbIO.getCcdate().toInt());
		
		if(updateAgcmrevIOList == null){
			updateAgcmrevIOList = new LinkedList<>();
		}
		updateAgcmrevIOList.add(agcmrevIO);
	}

protected void deleteBext3750()
	{
		// bextrevIO.setBtdate(varcom.vrcmMaxDate);
		List<Bextpf> bextrevIOList = bextpfDAO.searchBextrevRecordForDel(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(bextrevIOList!=null&&!bextrevIOList.isEmpty()){
			bextpfDAO.deleteBextpfRecord(bextrevIOList);
		}
	}

protected void updateBnfy3760()
 {
		List<Bnfypf> bnfylnbIOList = bnfypfDAO.searchBnfypfRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if (bnfylnbIOList!=null && !bnfylnbIOList.isEmpty()) {
			for (Bnfypf bnfylnbIO : bnfylnbIOList) {
				bnfylnbIO.setTranno(chdrlnbIO.getTranno().toInt());
				bnfylnbIO.setValidflag('2');
			}
			bnfypfDAO.updateBnfyRecord(bnfylnbIOList);
		}
	}

protected void deleteFlup3770()
	{
		List<Fluppf> fluprevIOList = fluppfDAO.searchFlupRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(fluprevIOList!=null&&!fluprevIOList.isEmpty()){
			fluppfDAO.deleteFluppfRecord(fluprevIOList);
		}
	}

protected void updateLife3780()
	{
		// TODO lifecfi
		List<Lifepf> lifecfiIOList = lifepfDAO.getLifeList(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if (lifecfiIOList!=null && !lifecfiIOList.isEmpty()) {
			for (Lifepf lifecfiIO : lifecfiIOList) {
				callLifecfi3780(lifecfiIO);
			}
			lifepfDAO.updateLifeRecord(lifecfiIOList);
		}
	}

protected void callLifecfi3780(Lifepf lifecfiIO)
	{
		lifecfiIO.setTranno(chdrlnbIO.getTranno().toInt());
		lifecfiIO.setTermid(varcom.vrcmTermid.toString());
		lifecfiIO.setUser(varcom.vrcmUser.toInt());
		lifecfiIO.setTransactionDate(varcom.vrcmDate.toInt());
		lifecfiIO.setTransactionTime(varcom.vrcmTime.toInt());
		if (isEQ(lifecfiIO.getJlife(), "00")
		|| isEQ(lifecfiIO.getJlife(), SPACES)) {
			if (isNE(t5679rec.setLifeStat, SPACES)) {
				lifecfiIO.setStatcode(t5679rec.setLifeStat.toString());
			}
		}
		else {
			if (isNE(t5679rec.setJlifeStat, SPACES)) {
				lifecfiIO.setStatcode(t5679rec.setJlifeStat.toString());
			}
		}
	}

protected void deleteLins3790()
	{
		List<Linspf> linscfiIOList = linspfDAO.searchLinscfiRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(linscfiIOList!=null&&!linscfiIOList.isEmpty()){
			linspfDAO.deleteLinspfRecord(linscfiIOList);
			// taxdrevIO.setEffdate(0);
			List<Taxdpf> taxdrevIOList = taxdpfDAO.searchTaxdrevRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
			if(taxdrevIOList!=null&&!taxdrevIOList.isEmpty()){
				taxdpfDAO.deleteTaxdpfRecord(taxdrevIOList);
			}
		}
	}

protected void writeResn3800()
	{
		if (isEQ(sv.reasoncd, SPACES)) {
			return ;
		}
		resnIO.setDataKey(SPACES);
		resnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		resnIO.setChdrnum(chdrlnbIO.getChdrnum());
		resnIO.setTranno(chdrlnbIO.getTranno());
		resnIO.setReasoncd(sv.reasoncd);
		resnIO.setResndesc(sv.resndesc);
		resnIO.setTrancde(wsaaBatckey.batcBatctrcde);
		resnIO.setTransactionDate(varcom.vrcmDate);
		resnIO.setTransactionTime(varcom.vrcmTime);
		resnIO.setUser(varcom.vrcmUser);
		resnIO.setFormat(formatsInner.resnrec);
		resnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, resnIO);
		if (isNE(resnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(resnIO.getParams());
			syserrrec.statuz.set(resnIO.getStatuz());
			fatalError600();
		}
	}

protected void deleteUlnk3810()
	{
		wsaaCoverageKeys.set(SPACES);
		wsaaPlanSuffix.set(0);
		wsaaSeqnbr.set(0);
		List<Ulnkpf> ulnkrevIOList = ulnkpfDAO.searchUlnkrevRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(ulnkrevIOList!=null&&!ulnkrevIOList.isEmpty()){
			for(Ulnkpf ulnkrevIO:ulnkrevIOList){
				callUlnkrev3810(ulnkrevIO);
			}
			ulnkpfDAO.deleteUlnkpfRecord(ulnkrevIOList);
			unltpfDAO.insertUnltpfList(insertUnltrevIOList);
		}
	}

protected void callUlnkrev3810(Ulnkpf ulnkrevIO)
	{
		if (isNE(ulnkrevIO.getChdrcoy(), wsaaChdrcoy)
		|| isNE(ulnkrevIO.getChdrnum(), wsaaChdrnum)
		|| isNE(ulnkrevIO.getLife(), wsaaLife)
		|| isNE(ulnkrevIO.getCoverage(), wsaaCoverage)
		|| isNE(ulnkrevIO.getRider(), wsaaRider)
		|| isNE(ulnkrevIO.getPlanSuffix(), wsaaPlanSuffix)) {
			createUnlt3820(ulnkrevIO);
			wsaaChdrcoy.set(ulnkrevIO.getChdrcoy());
			wsaaChdrnum.set(ulnkrevIO.getChdrnum());
			wsaaLife.set(ulnkrevIO.getLife());
			wsaaCoverage.set(ulnkrevIO.getCoverage());
			wsaaRider.set(ulnkrevIO.getRider());
			wsaaPlanSuffix.set(ulnkrevIO.getPlanSuffix());
		}
		
	}

protected void createUnlt3820(Ulnkpf ulnkrevIO)
	{
		wsaaSeqnbr.add(1);
		Unltpf unltrevIO = new Unltpf();
		unltrevIO.setChdrcoy(ulnkrevIO.getChdrcoy());
		unltrevIO.setChdrnum(ulnkrevIO.getChdrnum());
		unltrevIO.setLife(ulnkrevIO.getLife());
		unltrevIO.setCoverage(ulnkrevIO.getCoverage());
		unltrevIO.setRider(ulnkrevIO.getRider());
		unltrevIO.setSeqnbr(wsaaSeqnbr.toInt());
		unltrevIO.setCurrfrom(ulnkrevIO.getCurrfrom());
		unltrevIO.setCurrto(varcom.vrcmMaxDate.toInt());
		unltrevIO.setTranno(chdrlnbIO.getTranno().toInt());
		unltrevIO.setPtopup(SPACES.toString());
		unltrevIO.setValidflag("1");
		unltrevIO.setPrcamtind(ulnkrevIO.getPercOrAmntInd());
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			unltrevIO.setUalfnd(ix, ulnkrevIO.getUalfnd(ix.toInt()));
			unltrevIO.setUalprc(ix, ulnkrevIO.getUalprc(ix.toInt()));
			unltrevIO.setUspcpr(ix, ulnkrevIO.getUspcpr(ix.toInt()));
		}
		if (isEQ(ulnkrevIO.getPlanSuffix(), ZERO)) {
			if (isGT(chdrlnbIO.getPolinc(), 1)) {
				unltrevIO.setNumapp(chdrlnbIO.getPolsum().toInt());
			}
			else {
				unltrevIO.setNumapp(1);
			}
		}
		else {
			unltrevIO.setNumapp(1);
		}
		if(insertUnltrevIOList == null){
			insertUnltrevIOList = new LinkedList<>();
		}
		insertUnltrevIOList.add(unltrevIO);
	}

protected void deleteInci3830()
	{
		List<Incipf> incirevIOList = incipfDAO.searchIncirevRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(incirevIOList!=null && !incirevIOList.isEmpty()){
			incipfDAO.deleteIncipfRecord(incirevIOList);
		}
	}

protected void deleteRacd3840()
	{
		List<Racdpf> racdIOList = racdpfDAO.searchRacdRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(racdIOList!=null && !racdIOList.isEmpty()){
			racdpfDAO.deleteRacdRcds(racdIOList);
		}
	}

protected void deleteLrrh3850()
	{
		List<Lrrhpf> lrrhconIOList = lrrhpfDAO.searchlLrrhconRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(lrrhconIOList!=null && !lrrhconIOList.isEmpty()){
			lrrhpfDAO.deleteLrrhpfRecord(lrrhconIOList);
		}
	}

protected void deleteLirr3860()
	{
		List<Lirrpf> lirrconIOList = lirrpfDAO.searchlLirrconRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(lirrconIOList!=null && !lirrconIOList.isEmpty()){
			lirrpfDAO.deleteLirrpfRecord(lirrconIOList);
		}
	}

protected void updateBonusWorkbench3900()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(wsspcomn.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
			/****     GO TO 3090-EXIT                                           */
		}
		/*  Create the reversal extraction for the Bonus Workbench*/
		/*  Premium History*/
		processZptn3920();
		/*  Create the reversal extraction for the Bonus Workbench*/
		/*  Commission History*/
		processZctn3940();
	}

protected void processZptn3920()
	{
		List<Zptnpf> zptnrevIOList = zptnpfDAO.searchZptnrevRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(zptnrevIOList!=null&&!zptnrevIOList.isEmpty()){
			for(Zptnpf zptnrevIO:zptnrevIOList){
				if (isEQ(zptnrevIO.getTranno(), chdrlnbIO.getTranno())) {
					break;
				}
				callZptnrev3920(zptnrevIO);
			}
			zptnpfDAO.insertZptnpf(insertZptnpfList);
		}
	}

protected void callZptnrev3920(Zptnpf zptnrevIO)
	{
		Zptnpf zptnIO = new Zptnpf();
		zptnIO.setChdrcoy(zptnrevIO.getChdrcoy());
		zptnIO.setChdrnum(zptnrevIO.getChdrnum());
		zptnIO.setLife(zptnrevIO.getLife());
		zptnIO.setCoverage(zptnrevIO.getCoverage());
		zptnIO.setRider(zptnrevIO.getRider());
		zptnIO.setTranno(chdrlnbIO.getTranno().toInt());
		setPrecision(zptnIO.getOrigamt(), 2);
		zptnIO.setOrigamt(mult(zptnrevIO.getOrigamt(), (-1)).getbigdata());
		zptnIO.setTransCode(wsaaBatckey.batcBatctrcde.toString());
		zptnIO.setEffdate(zptnrevIO.getEffdate());
		zptnIO.setBillcd(zptnrevIO.getBillcd());
		zptnIO.setInstfrom(zptnrevIO.getInstfrom());
		zptnIO.setInstto(zptnrevIO.getInstto());
		zptnIO.setTrandate(wsaaToday.toInt());
		zptnIO.setZprflg(zptnrevIO.getZprflg());
		if(insertZptnpfList == null){
			insertZptnpfList = new LinkedList<Zptnpf>();
		}
		insertZptnpfList.add(zptnIO);
	}

protected void processZctn3940()
	{
		List<Zctnpf> zctnrevIOList = zctnpfDAO.searchZctnrevRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		if(zctnrevIOList!=null&&!zctnrevIOList.isEmpty()){
			for(Zctnpf zctnrevIO:zctnrevIOList){
				if (isEQ(zctnrevIO.getTranno(), chdrlnbIO.getTranno())) {
					break;
				}
				callZctnrev3940(zctnrevIO);
			}
			zctnpfDAO.insertZctnpf(insertZctnpfList);
		}
	}

protected void callZctnrev3940(Zctnpf zctnrevIO)
	{
		Zctnpf zctnIO = new Zctnpf();
		zctnIO.setAgntcoy(zctnrevIO.getAgntcoy());
		zctnIO.setAgntnum(zctnrevIO.getAgntnum());
		zctnIO.setChdrcoy(zctnrevIO.getChdrcoy());
		zctnIO.setChdrnum(zctnrevIO.getChdrnum());
		zctnIO.setLife(zctnrevIO.getLife());
		zctnIO.setCoverage(zctnrevIO.getCoverage());
		zctnIO.setRider(zctnrevIO.getRider());
		zctnIO.setTranno(chdrlnbIO.getTranno().toInt());
		zctnIO.setCommAmt(mult(zctnrevIO.getCommAmt(), (-1)).getbigdata());
		zctnIO.setPremium(mult(zctnrevIO.getPremium(), (-1)).getbigdata());
		zctnIO.setSplitBcomm(zctnrevIO.getSplitBcomm());
		zctnIO.setTransCode(wsaaBatckey.batcBatctrcde.toString());
		zctnIO.setEffdate(zctnrevIO.getEffdate());
		zctnIO.setTrandate(wsaaToday.toInt());
		zctnIO.setZprflg(zctnrevIO.getZprflg());
		
		if(insertZctnpfList == null){
			insertZctnpfList = new LinkedList<Zctnpf>();
		}
		insertZctnpfList.add(zctnIO);
	}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void reverseOsCovrDebt5100()
 {
		if (isEQ(wsaaOsDebt, 0)) {
			return;
		}
		if (covrlnbList == null) {
			covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		for (Covrpf covrlnbIO : covrlnbList) {
			if (isEQ(covrlnbIO.getTranno(), chdrlnbIO.getTranno()) || isEQ(covrlnbIO.getValidflag(), "2")
					|| isEQ(covrlnbIO.getCoverageDebt(), 0)) {
				continue;
			}
			/* Verify the Contract Risk Statuz */
			trcdeNotMatch.setTrue();
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12) || trcdeMatch.isTrue()); wsaaSub.add(1)) {
				if (isEQ(covrlnbIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
					trcdeMatch.setTrue();
				}
			}
			if (trcdeNotMatch.isTrue()) {
				continue;
			}
			/* Accumulate any outstanding coverage debts */
			lifacmvrec.origamt.set(covrlnbIO.getCoverageDebt());
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsbbChdrnum.set(covrlnbIO.getChdrnum());
				wsbbLife.set(covrlnbIO.getLife());
				wsbbCoverage.set(covrlnbIO.getCoverage());
				wsbbRider.set(covrlnbIO.getRider());
				wsaaPlnsfx.set(covrlnbIO.getPlanSuffix());
				wsbbPlnsfx.set(wsaaPlnsfx);
				lifacmvrec.rldgacct.set(wsbbRldgacct);
				wsaaIx.set(19);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
				callLifacmv3150();
			} else {
				lifacmvrec.rldgacct.set(SPACES);
				lifacmvrec.rldgacct.set(covrlnbIO.getChdrnum());
				wsaaIx.set(18);
				lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
				callLifacmv3150();
			}
		}
	}


protected void reversePpRiderPosts5500()
	{
		if (isEQ(wsaaPpRiderPrem, 0)) {
			return ;
		}
		if (isNE(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaPpRiderPrem);
			if (isEQ(chdrlnbIO.getBillfreq(), "00")) {
				wsaaIx.set(14);
			}
			else {
				wsaaIx.set(12);
			}
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
			lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
			lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
			callLifacmv3150();
		}
		else {
			refundPpPrem5600();
		}
	}


protected void refundPpPrem5600()
 {
		if (isEQ(chdrlnbIO.getBillfreq(), "00")) {
			wsaaSacscode.set(wsaaT5645Sacscode[15]);
			wsaaSacstype.set(wsaaT5645Sacstype[15]);
			wsaaCnttot.set(wsaaT5645Cnttot[15]);
		} else {
			wsaaSacscode.set(wsaaT5645Sacscode[13]);
			wsaaSacstype.set(wsaaT5645Sacstype[13]);
			wsaaCnttot.set(wsaaT5645Cnttot[13]);
		}
		
		if (acblenqList == null) {
			acblenqList = acblpfDAO.searchAcblenqRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}
		
		List<Acmvpf> acmvsacList = acmvpfDAO.searchAcmvsacRecordWithLike(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		t5540Map = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T5540");
		if (covrlnbList == null) {
			covrlnbList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		}

		for (Acblpf acblenqIO : acblenqList) {
			if (isEQ(acblenqIO.getSacscode(), wsaaSacscode) && isEQ(acblenqIO.getSacstyp(), wsaaSacstype)
					&& isEQ(subString(acblenqIO.getRldgacct(), 1, 8), chdrlnbIO.getChdrnum())) {
				if (isEQ(acblenqIO.getSacscurbal(), 0)) {
					continue;
				}
				for(Acmvpf acmvsacIO:acmvsacList){
					if (isEQ(acmvsacIO.getRldgcoy(), acblenqIO.getRldgcoy())
							&& isEQ(acmvsacIO.getSacscode(), acblenqIO.getSacscode())
							&& isEQ(acmvsacIO.getRldgacct(), acblenqIO.getRldgacct())
							&& isEQ(acmvsacIO.getSacstyp(), acblenqIO.getSacstyp())
							&& isEQ(acmvsacIO.getOrigcurr(), acblenqIO.getOrigcurr())) {
						processAcmvsac5700(acmvsacIO);
					}
				}
			}
		}
	}

protected void processAcmvsac5700(Acmvpf acmvsacIO)
 {
		/* Get the component code by reading COVR */
		if (isEQ(acmvsacIO.getTranno(), wsaaTranno)) {
			return;
		}
		wsbbRldgacct.set(acmvsacIO.getRldgacct());
		wsaaPlnsfx.set(wsbbPlnsfx);

		Covrpf covrlnbIO = null;
		for (Covrpf c : covrlnbList) {
			if (c.getChdrcoy().equals(acmvsacIO.getRldgcoy()) && c.getChdrnum().equals(wsbbChdrnum)
					&& c.getLife().equals(wsbbLife) && c.getCoverage().equals(wsbbCoverage)
					&& c.getRider().equals(wsbbRider) && c.getPlanSuffix() == wsaaPlnsfx.toInt()
					&& isEQ(c.getValidflag(), "1")) {

				covrlnbIO = c;
				break;
			}
		}
		if (covrlnbIO == null) {
			syserrrec.params.set(wsbbChdrnum);
			fatalError600();
		} //IJTI-462 START
		else {
			/* Check whether it is tradtional premium paying component */
			if (t5540Map != null && t5540Map.containsKey(covrlnbIO.getCrtable())) {
				for (Itempf item : t5540Map.get(covrlnbIO.getCrtable())) {
					if (item.getItmfrm().compareTo(chdrlnbIO.getOccdate().getbigdata()) <= 0) {
						return;
					}
				}
			}
			lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			wsaaJrnseq.add(1);
			lifacmvrec.rdocnum.set(acmvsacIO.getRdocnum());
			lifacmvrec.tranref.set(acmvsacIO.getTranref());
			lifacmvrec.sacscode.set(acmvsacIO.getSacscode());
			lifacmvrec.sacstyp.set(acmvsacIO.getSacstyp());
			lifacmvrec.rldgcoy.set(acmvsacIO.getRldgcoy());
			lifacmvrec.glcode.set(acmvsacIO.getGlcode());
			lifacmvrec.glsign.set(acmvsacIO.getGlsign());
			lifacmvrec.rldgacct.set(acmvsacIO.getRldgacct());
			lifacmvrec.origcurr.set(acmvsacIO.getOrigcurr());
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			compute(lifacmvrec.acctamt, 2).set(mult(acmvsacIO.getAcctamt(), (-1)));
			compute(lifacmvrec.origamt, 2).set(mult(acmvsacIO.getOrigamt(), (-1)));
			lifacmvrec.crate.set(acmvsacIO.getCrate());
			lifacmvrec.genlcoy.set(acmvsacIO.getGenlcoy());
			lifacmvrec.genlcur.set(acmvsacIO.getGenlcur());
			lifacmvrec.contot.set(wsaaCnttot);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError600();
			}
		} //IJTI-462 END
	}


protected void readT55156000()
 {
		boolean foundFlag = false;
		if (t5515Map != null && t5515Map.containsKey(wsaaFundCode.toString())) {
			List<Itempf> itempfList = t5515Map.get(wsaaFundCode.toString());
			for (Itempf itempf : itempfList) {
				if (itempf.getItmfrm().compareTo(sv.reserveUnitsDate.getbigdata()) <= 0) {
					t5515rec.t5515Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					foundFlag = true;
				}
			}
		}
		if (!foundFlag) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set("t5515:" + wsaaFundCode);
			fatalError600();
		}
	}

protected void convertValue6100()
	{
		if (isEQ(wsaaCurrcode, chdrlnbIO.getCntcurr())) {
			conlinkrec.amountOut.set(wsaaEstimatedVal);
			return ;
		}
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountIn.set(wsaaEstimatedVal);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(wsaaCashdate);
		conlinkrec.currIn.set(wsaaCurrcode);
		conlinkrec.currOut.set(chdrlnbIO.getCntcurr());
		conlinkrec.company.set(chdrlnbIO.getChdrcoy());
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
	}

protected void setupLifacmv6200()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranref.set(chdrlnbIO.getChdrnum());
		lifacmvrec.trandesc.set(wsaaDesc);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
	}

protected void postSuspExchg6300(Acblpf acblenqIO)
	{
		if (isEQ(acblenqIO.getRldgacct(), chdrlnbIO.getChdrnum())
		&& isEQ(acblenqIO.getSacstyp(), wsaaT5645Sacstype[1])
		&& isEQ(acblenqIO.getSacscode(), wsaaT5645Sacscode[1])
		&& isEQ(acblenqIO.getRldgcoy(), chdrlnbIO.getChdrcoy())
		&& isNE(acblenqIO.getOrigcurr(), chdrlnbIO.getCntcurr())) {
			compute(wsaaEstimatedVal, 2).set(mult(acblenqIO.getSacscurbal(), (-1)));
			wsaaCurrcode.set(acblenqIO.getOrigcurr());
			convertValue6100();
			post6310(acblenqIO);
		}
	}

protected void post6310(Acblpf acblenqIO)
	{
		lifacmvrec.origcurr.set(acblenqIO.getOrigcurr());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(wsaaEstimatedVal);
		wsaaIx.set(1);
		callLifacmv3150();
		lifacmvrec.origcurr.set(acblenqIO.getOrigcurr());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(wsaaEstimatedVal);
		wsaaIx.set(25);
		callLifacmv3150();
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(conlinkrec.amountOut);
		wsaaIx.set(26);
		callLifacmv3150();
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		compute(lifacmvrec.origamt, 2).set(mult(conlinkrec.amountOut, (-1)));
		wsaaIx.set(1);
		callLifacmv3150();
	}

protected void a100CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A190-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData fcfirec = new FixedLengthStringData(10).init("FCFIREC");
	private FixedLengthStringData fprmrec = new FixedLengthStringData(10).init("FPRMREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData resnrec = new FixedLengthStringData(10).init("RESNREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	private FixedLengthStringData t3695 = new FixedLengthStringData(5).init("T3695");
	private FixedLengthStringData t3698 = new FixedLengthStringData(5).init("T3698");
	private FixedLengthStringData t5500 = new FixedLengthStringData(5).init("T5500");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
	private FixedLengthStringData tr52m = new FixedLengthStringData(5).init("TR52M");
	private FixedLengthStringData th605 = new FixedLengthStringData(5).init("TH605");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e199 = new FixedLengthStringData(4).init("E199");
	private FixedLengthStringData e192 = new FixedLengthStringData(4).init("E192");
	private FixedLengthStringData g094 = new FixedLengthStringData(4).init("G094");
	private FixedLengthStringData h102 = new FixedLengthStringData(4).init("H102");
	private FixedLengthStringData rlcc = new FixedLengthStringData(4).init("RLCC");
	private FixedLengthStringData rlci = new FixedLengthStringData(4).init("RLCI");
	private FixedLengthStringData rlch = new FixedLengthStringData(4).init("RLCH");
	private FixedLengthStringData rlcd = new FixedLengthStringData(4).init("RLCD");
	private FixedLengthStringData rlce = new FixedLengthStringData(4).init("RLCE");
	private FixedLengthStringData rlcf = new FixedLengthStringData(4).init("RLCF");
	private FixedLengthStringData rlcg = new FixedLengthStringData(4).init("RLCG");
	private FixedLengthStringData rlcj = new FixedLengthStringData(4).init("RLCJ");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
}
}
