package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52F
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr52fScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(853);
	public FixedLengthStringData dataFields = new FixedLengthStringData(261).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData dtyamts = new FixedLengthStringData(55).isAPartOf(dataFields, 1);
	public ZonedDecimalData[] dtyamt = ZDArrayPartOfStructure(5, 11, 0, dtyamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(dtyamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData dtyamt01 = DD.dtyamt.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData dtyamt02 = DD.dtyamt.copyToZonedDecimal().isAPartOf(filler,11);
	public ZonedDecimalData dtyamt03 = DD.dtyamt.copyToZonedDecimal().isAPartOf(filler,22);
	public ZonedDecimalData dtyamt04 = DD.dtyamt.copyToZonedDecimal().isAPartOf(filler,33);
	public ZonedDecimalData dtyamt05 = DD.dtyamt.copyToZonedDecimal().isAPartOf(filler,44);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,56);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,64);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,72);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData shortdss = new FixedLengthStringData(20).isAPartOf(dataFields, 110);
	public FixedLengthStringData[] shortds = FLSArrayPartOfStructure(2, 10, shortdss, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(shortdss, 0, FILLER_REDEFINE);
	public FixedLengthStringData shortds01 = DD.shortds.copy().isAPartOf(filler1,0);
	public FixedLengthStringData shortds02 = DD.shortds.copy().isAPartOf(filler1,10);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,130);
	public FixedLengthStringData txabsinds = new FixedLengthStringData(2).isAPartOf(dataFields, 135);
	public FixedLengthStringData[] txabsind = FLSArrayPartOfStructure(2, 1, txabsinds, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(txabsinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData txabsind01 = DD.txabsind.copy().isAPartOf(filler2,0);
	public FixedLengthStringData txabsind02 = DD.txabsind.copy().isAPartOf(filler2,1);
	public FixedLengthStringData txfxamtas = new FixedLengthStringData(35).isAPartOf(dataFields, 137);
	public ZonedDecimalData[] txfxamta = ZDArrayPartOfStructure(5, 7, 0, txfxamtas, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(35).isAPartOf(txfxamtas, 0, FILLER_REDEFINE);
	public ZonedDecimalData txfxamta1 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 0);
	public ZonedDecimalData txfxamta2 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 7);
	public ZonedDecimalData txfxamta3 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 14);
	public ZonedDecimalData txfxamta4 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 21);
	public ZonedDecimalData txfxamta5 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 28);
	public FixedLengthStringData txfxamtbs = new FixedLengthStringData(35).isAPartOf(dataFields, 172);
	public ZonedDecimalData[] txfxamtb = ZDArrayPartOfStructure(5, 7, 0, txfxamtbs, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(35).isAPartOf(txfxamtbs, 0, FILLER_REDEFINE);
	public ZonedDecimalData txfxamtb1 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 0);
	public ZonedDecimalData txfxamtb2 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 7);
	public ZonedDecimalData txfxamtb3 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 14);
	public ZonedDecimalData txfxamtb4 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 21);
	public ZonedDecimalData txfxamtb5 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 28);
	public FixedLengthStringData txrateas = new FixedLengthStringData(25).isAPartOf(dataFields, 207);
	public ZonedDecimalData[] txratea = ZDArrayPartOfStructure(5, 5, 2, txrateas, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(25).isAPartOf(txrateas, 0, FILLER_REDEFINE);
	public ZonedDecimalData txratea01 = DD.txratea.copyToZonedDecimal().isAPartOf(filler5,0);
	public ZonedDecimalData txratea02 = DD.txratea.copyToZonedDecimal().isAPartOf(filler5,5);
	public ZonedDecimalData txratea03 = DD.txratea.copyToZonedDecimal().isAPartOf(filler5,10);
	public ZonedDecimalData txratea04 = DD.txratea.copyToZonedDecimal().isAPartOf(filler5,15);
	public ZonedDecimalData txratea05 = DD.txratea.copyToZonedDecimal().isAPartOf(filler5,20);
	public FixedLengthStringData txratebs = new FixedLengthStringData(25).isAPartOf(dataFields, 232);
	public ZonedDecimalData[] txrateb = ZDArrayPartOfStructure(5, 5, 2, txratebs, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(25).isAPartOf(txratebs, 0, FILLER_REDEFINE);
	public ZonedDecimalData txrateb01 = DD.txrateb.copyToZonedDecimal().isAPartOf(filler6,0);
	public ZonedDecimalData txrateb02 = DD.txrateb.copyToZonedDecimal().isAPartOf(filler6,5);
	public ZonedDecimalData txrateb03 = DD.txrateb.copyToZonedDecimal().isAPartOf(filler6,10);
	public ZonedDecimalData txrateb04 = DD.txrateb.copyToZonedDecimal().isAPartOf(filler6,15);
	public ZonedDecimalData txrateb05 = DD.txrateb.copyToZonedDecimal().isAPartOf(filler6,20);
	public FixedLengthStringData txtypes = new FixedLengthStringData(4).isAPartOf(dataFields, 257);
	public FixedLengthStringData[] txtype = FLSArrayPartOfStructure(2, 2, txtypes, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(4).isAPartOf(txtypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData txtype01 = DD.txtype.copy().isAPartOf(filler7,0);
	public FixedLengthStringData txtype02 = DD.txtype.copy().isAPartOf(filler7,2);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(148).isAPartOf(dataArea, 261);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData dtyamtsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] dtyamtErr = FLSArrayPartOfStructure(5, 4, dtyamtsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(20).isAPartOf(dtyamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData dtyamt01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData dtyamt02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData dtyamt03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData dtyamt04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData dtyamt05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData shortdssErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] shortdsErr = FLSArrayPartOfStructure(2, 4, shortdssErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(8).isAPartOf(shortdssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData shortds01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData shortds02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData txabsindsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] txabsindErr = FLSArrayPartOfStructure(2, 4, txabsindsErr, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(txabsindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData txabsind01Err = new FixedLengthStringData(4).isAPartOf(filler10, 0);
	public FixedLengthStringData txabsind02Err = new FixedLengthStringData(4).isAPartOf(filler10, 4);
	public FixedLengthStringData txfxamtasErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] txfxamtaErr = FLSArrayPartOfStructure(5, 4, txfxamtasErr, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(20).isAPartOf(txfxamtasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData txfxamta1Err = new FixedLengthStringData(4).isAPartOf(filler11, 0);
	public FixedLengthStringData txfxamta2Err = new FixedLengthStringData(4).isAPartOf(filler11, 4);
	public FixedLengthStringData txfxamta3Err = new FixedLengthStringData(4).isAPartOf(filler11, 8);
	public FixedLengthStringData txfxamta4Err = new FixedLengthStringData(4).isAPartOf(filler11, 12);
	public FixedLengthStringData txfxamta5Err = new FixedLengthStringData(4).isAPartOf(filler11, 16);
	public FixedLengthStringData txfxamtbsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData[] txfxamtbErr = FLSArrayPartOfStructure(5, 4, txfxamtbsErr, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(20).isAPartOf(txfxamtbsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData txfxamtb1Err = new FixedLengthStringData(4).isAPartOf(filler12, 0);
	public FixedLengthStringData txfxamtb2Err = new FixedLengthStringData(4).isAPartOf(filler12, 4);
	public FixedLengthStringData txfxamtb3Err = new FixedLengthStringData(4).isAPartOf(filler12, 8);
	public FixedLengthStringData txfxamtb4Err = new FixedLengthStringData(4).isAPartOf(filler12, 12);
	public FixedLengthStringData txfxamtb5Err = new FixedLengthStringData(4).isAPartOf(filler12, 16);
	public FixedLengthStringData txrateasErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData[] txrateaErr = FLSArrayPartOfStructure(5, 4, txrateasErr, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(20).isAPartOf(txrateasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData txratea01Err = new FixedLengthStringData(4).isAPartOf(filler13, 0);
	public FixedLengthStringData txratea02Err = new FixedLengthStringData(4).isAPartOf(filler13, 4);
	public FixedLengthStringData txratea03Err = new FixedLengthStringData(4).isAPartOf(filler13, 8);
	public FixedLengthStringData txratea04Err = new FixedLengthStringData(4).isAPartOf(filler13, 12);
	public FixedLengthStringData txratea05Err = new FixedLengthStringData(4).isAPartOf(filler13, 16);
	public FixedLengthStringData txratebsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData[] txratebErr = FLSArrayPartOfStructure(5, 4, txratebsErr, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(20).isAPartOf(txratebsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData txrateb01Err = new FixedLengthStringData(4).isAPartOf(filler14, 0);
	public FixedLengthStringData txrateb02Err = new FixedLengthStringData(4).isAPartOf(filler14, 4);
	public FixedLengthStringData txrateb03Err = new FixedLengthStringData(4).isAPartOf(filler14, 8);
	public FixedLengthStringData txrateb04Err = new FixedLengthStringData(4).isAPartOf(filler14, 12);
	public FixedLengthStringData txrateb05Err = new FixedLengthStringData(4).isAPartOf(filler14, 16);
	public FixedLengthStringData txtypesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData[] txtypeErr = FLSArrayPartOfStructure(2, 4, txtypesErr, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(8).isAPartOf(txtypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData txtype01Err = new FixedLengthStringData(4).isAPartOf(filler15, 0);
	public FixedLengthStringData txtype02Err = new FixedLengthStringData(4).isAPartOf(filler15, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(444).isAPartOf(dataArea, 409);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData dtyamtsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] dtyamtOut = FLSArrayPartOfStructure(5, 12, dtyamtsOut, 0);
	public FixedLengthStringData[][] dtyamtO = FLSDArrayPartOfArrayStructure(12, 1, dtyamtOut, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(60).isAPartOf(dtyamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] dtyamt01Out = FLSArrayPartOfStructure(12, 1, filler16, 0);
	public FixedLengthStringData[] dtyamt02Out = FLSArrayPartOfStructure(12, 1, filler16, 12);
	public FixedLengthStringData[] dtyamt03Out = FLSArrayPartOfStructure(12, 1, filler16, 24);
	public FixedLengthStringData[] dtyamt04Out = FLSArrayPartOfStructure(12, 1, filler16, 36);
	public FixedLengthStringData[] dtyamt05Out = FLSArrayPartOfStructure(12, 1, filler16, 48);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData shortdssOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(2, 12, shortdssOut, 0);
	public FixedLengthStringData[][] shortdsO = FLSDArrayPartOfArrayStructure(12, 1, shortdsOut, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(24).isAPartOf(shortdssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] shortds01Out = FLSArrayPartOfStructure(12, 1, filler17, 0);
	public FixedLengthStringData[] shortds02Out = FLSArrayPartOfStructure(12, 1, filler17, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData txabsindsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] txabsindOut = FLSArrayPartOfStructure(2, 12, txabsindsOut, 0);
	public FixedLengthStringData[][] txabsindO = FLSDArrayPartOfArrayStructure(12, 1, txabsindOut, 0);
	public FixedLengthStringData filler18 = new FixedLengthStringData(24).isAPartOf(txabsindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] txabsind01Out = FLSArrayPartOfStructure(12, 1, filler18, 0);
	public FixedLengthStringData[] txabsind02Out = FLSArrayPartOfStructure(12, 1, filler18, 12);
	public FixedLengthStringData txfxamtasOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] txfxamtaOut = FLSArrayPartOfStructure(5, 12, txfxamtasOut, 0);
	public FixedLengthStringData[][] txfxamtaO = FLSDArrayPartOfArrayStructure(12, 1, txfxamtaOut, 0);
	public FixedLengthStringData filler19 = new FixedLengthStringData(60).isAPartOf(txfxamtasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] txfxamta1Out = FLSArrayPartOfStructure(12, 1, filler19, 0);
	public FixedLengthStringData[] txfxamta2Out = FLSArrayPartOfStructure(12, 1, filler19, 12);
	public FixedLengthStringData[] txfxamta3Out = FLSArrayPartOfStructure(12, 1, filler19, 24);
	public FixedLengthStringData[] txfxamta4Out = FLSArrayPartOfStructure(12, 1, filler19, 36);
	public FixedLengthStringData[] txfxamta5Out = FLSArrayPartOfStructure(12, 1, filler19, 48);
	public FixedLengthStringData txfxamtbsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 240);
	public FixedLengthStringData[] txfxamtbOut = FLSArrayPartOfStructure(5, 12, txfxamtbsOut, 0);
	public FixedLengthStringData[][] txfxamtbO = FLSDArrayPartOfArrayStructure(12, 1, txfxamtbOut, 0);
	public FixedLengthStringData filler20 = new FixedLengthStringData(60).isAPartOf(txfxamtbsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] txfxamtb1Out = FLSArrayPartOfStructure(12, 1, filler20, 0);
	public FixedLengthStringData[] txfxamtb2Out = FLSArrayPartOfStructure(12, 1, filler20, 12);
	public FixedLengthStringData[] txfxamtb3Out = FLSArrayPartOfStructure(12, 1, filler20, 24);
	public FixedLengthStringData[] txfxamtb4Out = FLSArrayPartOfStructure(12, 1, filler20, 36);
	public FixedLengthStringData[] txfxamtb5Out = FLSArrayPartOfStructure(12, 1, filler20, 48);
	public FixedLengthStringData txrateasOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 300);
	public FixedLengthStringData[] txrateaOut = FLSArrayPartOfStructure(5, 12, txrateasOut, 0);
	public FixedLengthStringData[][] txrateaO = FLSDArrayPartOfArrayStructure(12, 1, txrateaOut, 0);
	public FixedLengthStringData filler21 = new FixedLengthStringData(60).isAPartOf(txrateasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] txratea01Out = FLSArrayPartOfStructure(12, 1, filler21, 0);
	public FixedLengthStringData[] txratea02Out = FLSArrayPartOfStructure(12, 1, filler21, 12);
	public FixedLengthStringData[] txratea03Out = FLSArrayPartOfStructure(12, 1, filler21, 24);
	public FixedLengthStringData[] txratea04Out = FLSArrayPartOfStructure(12, 1, filler21, 36);
	public FixedLengthStringData[] txratea05Out = FLSArrayPartOfStructure(12, 1, filler21, 48);
	public FixedLengthStringData txratebsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 360);
	public FixedLengthStringData[] txratebOut = FLSArrayPartOfStructure(5, 12, txratebsOut, 0);
	public FixedLengthStringData[][] txratebO = FLSDArrayPartOfArrayStructure(12, 1, txratebOut, 0);
	public FixedLengthStringData filler22 = new FixedLengthStringData(60).isAPartOf(txratebsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] txrateb01Out = FLSArrayPartOfStructure(12, 1, filler22, 0);
	public FixedLengthStringData[] txrateb02Out = FLSArrayPartOfStructure(12, 1, filler22, 12);
	public FixedLengthStringData[] txrateb03Out = FLSArrayPartOfStructure(12, 1, filler22, 24);
	public FixedLengthStringData[] txrateb04Out = FLSArrayPartOfStructure(12, 1, filler22, 36);
	public FixedLengthStringData[] txrateb05Out = FLSArrayPartOfStructure(12, 1, filler22, 48);
	public FixedLengthStringData txtypesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 420);
	public FixedLengthStringData[] txtypeOut = FLSArrayPartOfStructure(2, 12, txtypesOut, 0);
	public FixedLengthStringData[][] txtypeO = FLSDArrayPartOfArrayStructure(12, 1, txtypeOut, 0);
	public FixedLengthStringData filler23 = new FixedLengthStringData(24).isAPartOf(txtypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] txtype01Out = FLSArrayPartOfStructure(12, 1, filler23, 0);
	public FixedLengthStringData[] txtype02Out = FLSArrayPartOfStructure(12, 1, filler23, 12);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr52fscreenWritten = new LongData(0);
	public LongData Sr52fprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52fScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, txtype01, shortds01, txtype02, shortds02, dtyamt01, txratea01, txrateb01, dtyamt02, txratea02, txrateb02, dtyamt03, txratea03, txrateb03, dtyamt04, txratea04, txrateb04, dtyamt05, txratea05, txrateb05, txabsind01, txabsind02};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, txtype01Out, shortds01Out, txtype02Out, shortds02Out, dtyamt01Out, txratea01Out, txrateb01Out, dtyamt02Out, txratea02Out, txrateb02Out, dtyamt03Out, txratea03Out, txrateb03Out, dtyamt04Out, txratea04Out, txrateb04Out, dtyamt05Out, txratea05Out, txrateb05Out, txabsind01Out, txabsind02Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, txtype01Err, shortds01Err, txtype02Err, shortds02Err, dtyamt01Err, txratea01Err, txrateb01Err, dtyamt02Err, txratea02Err, txrateb02Err, dtyamt03Err, txratea03Err, txrateb03Err, dtyamt04Err, txratea04Err, txrateb04Err, dtyamt05Err, txratea05Err, txrateb05Err, txabsind01Err, txabsind02Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52fscreen.class;
		protectRecord = Sr52fprotect.class;
	}

}
