/*
 * File: Revbchg.java
 * Date: 30 August 2009 2:05:39
 * Author: Quipoz Limited
 * 
 * Class transformed from REVBCHG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.AgcmbcrTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.dataaccess.LinsrevTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CoprpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Coprpf;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv1TableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv2TableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.model.Bextpf;

/**
* <pre>
*REMARKS.
*
*  REVBCHG - Billing Changes Reversal
*  ----------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  THE FOLLOWING TRANSACTIONS ARE REVERSED :
*
*  - CHDR
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*            relevant contract.
*       DELET this record.
*            relevant  contract.  This should read a record with
*            valid flag of '2'. If not, then this is an error.
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*            altering  the  valid  flag from '2' to '1'.
*
*  - PAYR
*
*       *** It is assumed that the PAYRSEQNO is always 1 ie. multi-payor
*           has not been implemented yet. This is a very important
*           assumption.
*
*       READH on  the  Payor Detail record (PAYRLIF) for the contract.
*       DELET this record.
*       BEGNH on  the  Payor Detail record (PAYRLIF) for the contract.
*            This should read a record with valid flag '2'. If not,
*            then this is an error.
*       REWRT this Payor Detail record (PAYRLIF) after altering the
*            valid flag from '2' to '1'.
*
*  - COVR
*
*       Only reverse COVR records whose TRANNO is equal to the
*           Transaction number (of the PTRN) being reversed.
*       READH on  the  COVR  records for the relevant contract.
*       DELET this record.
*       NEXTR on  the  COVR  file should read a record with valid
*            flag  of  '2'. If not, then this is an error.
*       UPDAT this  COVR  record after altering the valid flag
*            from  '2'  to  '1' .
*
*  - INCIs
*
*       Call the T5671 generic subroutine to reverse INCI
*       records.
*
*  - AGCMs
*
*       Read the first AGCM record (AGCMBCR).
*       Delete this record.
*       Read the next AGCM looking for valid flag 2 records.
*       Update these AGCM records with valid flag 1.
*
*
*****************************************************************
* </pre>
*/
public class Revbchg extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "REVBCHG";

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
		/* TABLES */
	private String t5671 = "T5671";
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private String covrrec = "COVRREC";
	private String incrselrec = "INCRSELREC";
	private String fpcorv1rec = "FPCORV1REC";
	private String fpcorv2rec = "FPCORV2REC";
		/*AGENT COMMISSION BILLING CHANGE REVERSE*/
	private AgcmbcrTableDAM agcmbcrIO = new AgcmbcrTableDAM();
		/*Agent Commission Dets- Billing Change*/
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*FPCO for reversals  - Version 9604*/
	private Fpcorv1TableDAM fpcorv1IO = new Fpcorv1TableDAM();
		/*FPCO logical for Reversals*/
	private Fpcorv2TableDAM fpcorv2IO = new Fpcorv2TableDAM();
	private Greversrec greversrec = new Greversrec();
		/*Automatic Increase Refusal Select File*/
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Payr logical file*/
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Varcom varcom = new Varcom();
	private boolean BTPRO033Permission;
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private CoprpfDAO coprpfDAO = getApplicationContext().getBean("coprpfDAO", CoprpfDAO.class);
	private LinspfDAO linspfDAO =  getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private BextpfDAO bextpfDAO = getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private RertpfDAO rertpfDAO = getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	private int payrEffdate;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		processComponent3010, 
		exit3009, 
		exit3390, 
		exit3590, 
		findRecordToRevalidate3710, 
		exit3790, 
		exit4000, 
		exit4100, 
		xxxxErrorBomb
	}

	public Revbchg() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		processChdrs2000();
		processPayrs2500();
		processCovrs3000();
		if (BTPRO033Permission) {
			deleteCopr();
			deleteLins();
			deleteBext();
		}
		deleteAndUpdateRert();
		processFpcos4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*INIT*/
		syserrrec.subrname.set(wsaaSubr);
		BTPRO033Permission = FeaConfg.isFeatureExist(reverserec.company.toString(), "BTPRO033", appVars,"IT");
		/*EXIT*/
	}

protected void processChdrs2000()
	{
		readContractHeader2001();
	}

protected void readContractHeader2001()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getValidflag(),1)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		chdrlifIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrlifIO.getValidflag(),2)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setValidflag("1");
		chdrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processPayrs2500()
	{
		readPayorDetail2501();
	}

protected void readPayorDetail2501()
	{
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getValidflag(),1)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		payrEffdate = payrlifIO.getEffdate().toInt();
		payrlifIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(payrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(),2)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setValidflag("1");
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processCovrs3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					covers3001();
				}
				case processComponent3010: {
					processComponent3010();
				}
				case exit3009: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covers3001()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			goTo(GotoLabel.exit3009);
		}
	}

protected void processComponent3010()
	{
		if (isNE(covrIO.getValidflag(),1)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		wsaaCovrKey.set(covrIO.getDataKey());
		if (isEQ(covrIO.getTranno(),reverserec.tranno)) {
			deleteAndUpdateComp3100();
			if (BTPRO033Permission) {
				processRacd3900();
			}
			processGenericSubr3300();
			processAgcms3600();
		}
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getDataKey(),wsaaCovrKey))) {
			getNextCovr3800();
		}
		
		if (isNE(covrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.processComponent3010);
		}
	}

protected void deleteAndUpdateComp3100()
	{
		deleteRecs3101();
	}

protected void deleteRecs3101()
	{
		covrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		checkIncr3500();
		covrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getValidflag(),2)
		|| isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processRacd3900() {
	List<Racdpf> racdList = new ArrayList<Racdpf>();
	List<Racdpf> updateRacdList = new ArrayList<Racdpf>();
	racdList = racdpfDAO.searchRacdstRecord(covrIO.getChdrcoy().toString(), covrIO.getChdrnum().toString(), 
			covrIO.getLife().toString(), covrIO.getCoverage().toString(),
			covrIO.getRider().toString(), covrIO.getPlanSuffix().toInt(), "1");
	for (Racdpf racd : racdList) {
		racd.setTranno(reverserec.newTranno.toInt());
		racd.setPendcstto(0);
		racd.setProrateflag("");
		updateRacdList.add(racd);
	}
	racdpfDAO.updateRacdPendcsttoProrateflagBulk(updateRacdList);
}

protected void deleteCopr() {
	Coprpf coprpf = new Coprpf();
	coprpf.setChdrcoy(reverserec.company.toString());
	coprpf.setChdrnum(reverserec.chdrnum.toString());
	coprpf.setValidflag("1");
	List<Coprpf> coprList = coprpfDAO.getCoprpfByTranno(coprpf);
	List<Coprpf> deleteCoprList = new ArrayList<Coprpf>();
	if(!coprList.isEmpty()) {
		for(Coprpf c:coprList) {
			deleteCoprList.add(c);
		}
		coprpfDAO.deleteCoprpf(deleteCoprList);
	}
}

protected void deleteBext() {
	Bextpf bext = bextpfDAO.getMapOfBextpfByTranno(reverserec.chdrnum.toString(),reverserec.company.toString(), reverserec.tranno.toInt());
	if(bext!=null && bext.getInstfrom()<=payrEffdate  && bext.getInstto()>=payrEffdate) {
		List<Bextpf> deleteBextList = new ArrayList<>();
		deleteBextList.add(bext);
		bextpfDAO.deleteBextpfRecord(deleteBextList);
	}
}

protected void deleteLins() {
	Linspf lins = linspfDAO.getProratedRecord(reverserec.company.toString(), reverserec.chdrnum.toString(), "Y");
	if(lins!=null && lins.getInstfrom()<=payrEffdate  && lins.getInstto()>=payrEffdate) {
		List<Linspf> deleteLinsList = new ArrayList<>();
		deleteLinsList.add(lins);
		linspfDAO.deleteLinspfRecord(deleteLinsList);
	}
}

protected void deleteAndUpdateRert() {
	List<Rertpf> rertList = rertpfDAO.getRertpfList(reverserec.company.toString(), reverserec.chdrnum.toString(),"1", 0);
	if (!rertList.isEmpty()) {
		List<Rertpf> deleteRertList = new ArrayList<Rertpf>();
		for (Rertpf r : rertList) {
			deleteRertList.add(r);
		}
		rertpfDAO.deleteRertpf(deleteRertList);
	}
	List<Rertpf> rertList2 = rertpfDAO.getRertpfList(reverserec.company.toString(), reverserec.chdrnum.toString(),"2", 0);
	if (!rertList2.isEmpty()) {
		List<Rertpf> updateRertList = new ArrayList<Rertpf>();
		for (Rertpf r : rertList2) {
			r.setTranno(reverserec.newTranno.toInt());
			r.setValidflag("1");
			updateRertList.add(r);
		}
		rertpfDAO.updateRertList(updateRertList);
	}
}

protected void processGenericSubr3300()
	{
		try {
			readStatusCodes3301();
		}
		catch (GOTOException e){
		}
	}

protected void readStatusCodes3301()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Trancode.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3390);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.life.set(covrIO.getLife());
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.effdate.set(reverserec.effdate1);
		greversrec.tranno.set(reverserec.tranno);
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.crtable.set(covrIO.getCrtable());
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.user.set(reverserec.user);
		greversrec.termid.set(reverserec.termid);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callSubprog3400();
		}
	}

protected void callSubprog3400()
	{
		/*SUBPROG*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
		}
		if (isNE(greversrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(greversrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void checkIncr3500()
	{
		try {
			readh3510();
		}
		catch (GOTOException e){
		}
	}

protected void readh3510()
	{
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(covrIO.getChdrcoy());
		incrselIO.setChdrnum(covrIO.getChdrnum());
		incrselIO.setLife(covrIO.getLife());
		incrselIO.setCoverage(covrIO.getCoverage());
		incrselIO.setRider(covrIO.getRider());
		incrselIO.setPlanSuffix(covrIO.getPlanSuffix());
		incrselIO.setTranno(reverserec.tranno);
		incrselIO.setFunction(varcom.readh);
		incrselIO.setFormat(incrselrec);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)
		&& isNE(incrselIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(incrselIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3590);
		}
		incrselIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
	}

protected void processAgcms3600()
	{
		agcms3601();
	}

protected void agcms3601()
	{
		agcmbcrIO.setParams(SPACES);
		agcmbcrIO.setChdrcoy(covrIO.getChdrcoy());
		agcmbcrIO.setChdrnum(covrIO.getChdrnum());
		agcmbcrIO.setCoverage(covrIO.getCoverage());
		agcmbcrIO.setLife(covrIO.getLife());
		agcmbcrIO.setRider(covrIO.getRider());
		agcmbcrIO.setPlanSuffix(covrIO.getPlanSuffix());
		agcmbcrIO.setTranno(reverserec.tranno);
		agcmbcrIO.setSeqno(ZERO);
		agcmbcrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbcrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if ((isNE(agcmbcrIO.getStatuz(),varcom.oK))
		&& (isNE(agcmbcrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(agcmbcrIO.getChdrcoy(),covrIO.getChdrcoy()))
		|| (isNE(agcmbcrIO.getChdrnum(),covrIO.getChdrnum()))
		|| (isNE(agcmbcrIO.getCoverage(),covrIO.getCoverage()))
		|| (isNE(agcmbcrIO.getLife(),covrIO.getLife()))
		|| (isNE(agcmbcrIO.getRider(),covrIO.getRider()))
		|| (isNE(agcmbcrIO.getPlanSuffix(),covrIO.getPlanSuffix()))
		|| (isNE(agcmbcrIO.getTranno(),reverserec.tranno))) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmbcrIO.getStatuz(),varcom.endp))) {
			reverseAgcm3700();
		}
		
	}

protected void reverseAgcm3700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					reverse3701();
				}
				case findRecordToRevalidate3710: {
					findRecordToRevalidate3710();
				}
				case exit3790: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void reverse3701()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmbcrIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmbcrIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmbcrIO.getAgntnum());
		agcmdbcIO.setLife(agcmbcrIO.getLife());
		agcmdbcIO.setCoverage(agcmbcrIO.getCoverage());
		agcmdbcIO.setRider(agcmbcrIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmbcrIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmbcrIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmdbcIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmdbcIO.setFunction(varcom.begn);
	}

protected void findRecordToRevalidate3710()
	{
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(agcmdbcIO.getChdrcoy(),agcmbcrIO.getChdrcoy()))
		|| (isNE(agcmdbcIO.getChdrnum(),agcmbcrIO.getChdrnum()))
		|| (isNE(agcmdbcIO.getAgntnum(),agcmbcrIO.getAgntnum()))
		|| (isNE(agcmdbcIO.getLife(),agcmbcrIO.getLife()))
		|| (isNE(agcmdbcIO.getCoverage(),agcmbcrIO.getCoverage()))
		|| (isNE(agcmdbcIO.getRider(),agcmbcrIO.getRider()))
		|| (isNE(agcmdbcIO.getPlanSuffix(),agcmbcrIO.getPlanSuffix()))
		|| (isNE(agcmdbcIO.getSeqno(),agcmbcrIO.getSeqno()))) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getValidflag(),2)) {
			agcmdbcIO.setFunction(varcom.nextr);
			goTo(GotoLabel.findRecordToRevalidate3710);
		}
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmbcrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if ((isNE(agcmbcrIO.getStatuz(),varcom.oK))
		&& (isNE(agcmbcrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(agcmbcrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3790);
		}
		if ((isNE(agcmbcrIO.getChdrcoy(),covrIO.getChdrcoy()))
		|| (isNE(agcmbcrIO.getChdrnum(),covrIO.getChdrnum()))
		|| (isNE(agcmbcrIO.getCoverage(),covrIO.getCoverage()))
		|| (isNE(agcmbcrIO.getLife(),covrIO.getLife()))
		|| (isNE(agcmbcrIO.getRider(),covrIO.getRider()))
		|| (isNE(agcmbcrIO.getPlanSuffix(),covrIO.getPlanSuffix()))
		|| (isNE(agcmbcrIO.getTranno(),reverserec.tranno))) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
	}

protected void getNextCovr3800()
	{
		/*GET-COVR*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processFpcos4000()
	{
		try {
			para4000();
		}
		catch (GOTOException e){
		}
	}

protected void para4000()
	{
		notFlexiblePremiumContract.setTrue();
		readT57294100();
		if (notFlexiblePremiumContract.isTrue()) {
			goTo(GotoLabel.exit4000);
		}
		/*Do not bomb the program if FPCO record is not found. Defect #914*/
		fpcorv1IO.setStatuz(SPACES);
		fpcorv1IO.setChdrcoy(reverserec.company);
		fpcorv1IO.setChdrnum(reverserec.chdrnum);
		fpcorv1IO.setEffdate(reverserec.ptrneff);
		fpcorv1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorv1IO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		fpcorv1IO.setFormat(fpcorv1rec);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(),varcom.oK)
		|| isNE(fpcorv1IO.getChdrcoy(),reverserec.company)
		|| isNE(fpcorv1IO.getChdrnum(),reverserec.chdrnum)
		|| isNE(fpcorv1IO.getEffdate(),reverserec.ptrneff)) {
		/*Do not bomb the program if FPCO record is not found. Defect #914*/	
		/*	syserrrec.params.set(fpcorv1IO.getParams()); */
		/*	xxxxFatalError();                            */
			fpcorv1IO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(fpcorv1IO.getStatuz(),varcom.endp))) {
			reverseFpco4200();
		}
		
	}

protected void readT57294100()
	{
		try {
			para4100();
		}
		catch (GOTOException e){
		}
	}

protected void para4100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(reverserec.company, SPACES));
			stringVariable1.append(delimitedExp(t5729, SPACES));
			stringVariable1.append(delimitedExp(chdrlifIO.getCnttype(), SPACES));
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),reverserec.company)
		|| isNE(itdmIO.getItemtabl(),t5729)
		|| isNE(itdmIO.getItemitem(),chdrlifIO.getCnttype())
		|| isGT(itdmIO.getItmfrm(),chdrlifIO.getOccdate())
		|| isLT(itdmIO.getItmto(),chdrlifIO.getOccdate())) {
			goTo(GotoLabel.exit4100);
		}
		flexiblePremiumContract.setTrue();
	}

protected void reverseFpco4200()
	{
		para4200();
	}

protected void para4200()
	{
		fpcorv2IO.setChdrcoy(fpcorv1IO.getChdrcoy());
		fpcorv2IO.setChdrnum(fpcorv1IO.getChdrnum());
		fpcorv2IO.setLife(fpcorv1IO.getLife());
		fpcorv2IO.setCoverage(fpcorv1IO.getCoverage());
		fpcorv2IO.setRider(fpcorv1IO.getRider());
		fpcorv2IO.setPlanSuffix(fpcorv1IO.getPlanSuffix());
		fpcorv2IO.setTargfrom(fpcorv1IO.getTargfrom());
		fpcorv1IO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		fpcorv2IO.setFormat(fpcorv2rec);
		fpcorv2IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			xxxxFatalError();
		}
		fpcorv2IO.setValidflag("1");
		fpcorv2IO.setCurrto(varcom.vrcmMaxDate);
		fpcorv2IO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			xxxxFatalError();
		}
		fpcorv1IO.setFunction(varcom.nextr);
		fpcorv1IO.setFormat(fpcorv1rec);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(),varcom.oK)
		&& isNE(fpcorv1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		if (isNE(fpcorv1IO.getChdrcoy(),reverserec.company)
		|| isNE(fpcorv1IO.getChdrnum(),reverserec.chdrnum)
		|| isNE(fpcorv1IO.getEffdate(),reverserec.ptrneff)) {
			fpcorv1IO.setStatuz(varcom.endp);
		}
	}

protected void xxxxFatalError()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					xxxxFatalErrors();
				}
				case xxxxErrorBomb: {
					xxxxErrorBomb();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.xxxxErrorBomb);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
}
