package com.csc.life.contractservicing.screens;

import com.csc.life.newbusiness.screens.S6226ScreenVars;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for HIDE
 * @version 1.0 generated on 11/22/13 6:31 AM
 * @author CSC
 */
public class S6226hide extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6226ScreenVars sv = (S6226ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6226hideWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6226ScreenVars screenVars = (S6226ScreenVars)pv;
	}

/**
 * Clear all the variables in S6226hide
 */
	public static void clear(VarModel pv) {
		S6226ScreenVars screenVars = (S6226ScreenVars) pv;
	}
}
