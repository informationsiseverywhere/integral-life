/*
 * File: Revbbac.java
 * Date: 30 August 2009 2:05:20
 * Author: Quipoz Limited
 * 
 * Class transformed from REVBBAC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.AcmvldgTableDAM;
import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.dataaccess.dao.ZfmcpfDAO;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZctnrevTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.dataaccess.ZptnrevTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrbbrTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.recordstructures.Fmcvpmsrec;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstnudTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Zfmcpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   BENEFIT BILLING REVERSAL ( ACCOUNTING MOVEMENTS ONLY )
*   ------------------------------------------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse all Non-cash Accounting
*   records (i.e ACMVs) which match on the key specified...
*   the key being Component, Transaction Number and Transaction
*   Code.
*  It will also amend the Coverage Debt and correct the Benefit
*   Billing date, both of which are held on the COVR file.
*
* Processing.
* -----------
*
* This subroutine is called from the Full contract Reversal AT
*  module REVGENAT via an entry in T6661. All parameters required
*  by this subroutine are passed in the REVERSEREC copybook.
*
*  Read Contract file using CHDRENQ logical file to get
*   Contract type
*
*  Read table T5688 to check for Contract or Component Level
*   Accounting.
*
*  Read table T5645 to get Sub-account code & type and GLMAP
*   values for use when deciding whether to update the
*   Coverage debt & benefit billing date on the COVR file.
*
*  Read through the ACMV records ( using ACMVREV logical )
*
*    FOR each ACMVREV record that matches on the key of Company,
*        Contract number and Transaction Number (passed in the
*        REVERSEREC copybook)
*
*        Post an ACMV for the opposite amount just retrieved on
*          the ACMVREV record using the LIFACMV subroutine
*          ( multiply ACMVREV-ORIGAMT by -1 )
*
*        IF the Subaccount code & type on the ACMVREV
*          record match the ones read above from T5645,
*
*           Read & lock coverage record on coverage (COVR) file
*            in preparation for updating - the COVR key is
*            determined by what is in the TRANREF field on the
*            ACMV we are trying to reverse.
*            Use the COVRBBR logical view
*
*             The coverage debt will always be updated on
*             the Coverage record, even if the Rider is the
*             benefit-billed part.
*            The benefit-billed date will be updated on either
*             the coverage or rider record, depending on which
*             is being benefit-billed.
*
*           Subtract the ACMVREV-ORIGAMT from the coverage debt
*            field, COVRBBR-COVERAGE-DEBT.
*
*           Read T5687 to get benefit billing method for coverage
*           Read T5534 to get Unit Deduction Frequency
*
*           Adjust coverage benefit billing date by reversing
*           it 1 period, as specified by the value on T5534.
*
*           Update transaction number & transaction history
*            fields on COVRBBR record
*
*           REWRiTe coverage (COVRBBR) record
*
*        END-IF
*
*    Read NEXT ACMVREV record & repeat above
*
* The following note is only for the reversal of ACMVs which were
*   written prior to the time when the TRANREF field was set up
*   containing the key of the Component that is Benefit Billed.
*
* NOTE.....  It should be noted that if a contract has more than
*             one Coverage and Contract Level Accounting is in
*             force, the adjustment to the coverage debt and
*             benefit billing date will DEFAULT to the 1st
*             Coverage/Rider on the contract which has a benefit
*             billing method on T5687, whether it is the correct
*             one or not. We have no way of distinguishing which
*             contract component the ACMV has been posted from
*             UNLESS Component Level Accounting is used.
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5534 - Benefit Billing methods
* T5645 - Transaction Accounting Rules
* T5687 - General Coverage/Rider details
* T5688 - Contract Structure
*
*
*****************************************************************
* </pre>
*/
public class Revbbac extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVBBAC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0).init(SPACES);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8).init(SPACES);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10).init(SPACES);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12).init(SPACES);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14).init(SPACES);

	private FixedLengthStringData wsaaPlanSuffix = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanSuffix, 0).setUnsigned();

	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaTrefChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTranref, 0);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTranref, 1);
	private FixedLengthStringData wsaaTrefLife = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 9);
	private FixedLengthStringData wsaaTrefCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 11);
	private FixedLengthStringData wsaaTrefRider = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 13);
	private FixedLengthStringData wsaaTrefPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaTranref, 15);
	private FixedLengthStringData wsaaBbCovrFound = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaHoldRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6).setUnsigned();
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String h053 = "H053";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5534 = "T5534";
	private static final String t5645 = "T5645";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t5644 = "T5644";
	private static final String th605 = "TH605";
	private AcmvldgTableDAM acmvldgIO = new AcmvldgTableDAM();
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrbbrTableDAM covrbbrIO = new CovrbbrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZctnrevTableDAM zctnrevIO = new ZctnrevTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZptnrevTableDAM zptnrevIO = new ZptnrevTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private ZrstnudTableDAM zrstnudIO = new ZrstnudTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5534rec t5534rec = new T5534rec();
	private T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private Th605rec th605rec = new Th605rec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private T5644rec t5644rec = new T5644rec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaVariablesInner wsaaVariablesInner = new WsaaVariablesInner();
	//IBPLIFE-1433 Starts
	private Fmcvpmsrec fmcvpmsrec = new Fmcvpmsrec();
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private FixedLengthStringData wsaaCovrUpd = new FixedLengthStringData(1).init(SPACES);
	private static final String  FMC_FEATURE_ID = "BTPRO026";
	private boolean fmcOnFlag = false;
	private Zfmcpf zfmctrnIO = new Zfmcpf();
	private ZfmcpfDAO zfmcpfDAO = getApplicationContext().getBean("zfmcpfDAO", ZfmcpfDAO.class);
	private List<Hitspf> hitspfList = new ArrayList<Hitspf>();
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);
	//IBPLIFE-1433 End 

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		a280Nextr, 
		a290Exit, 
		b100Nextr, 
		call11020, 
		exit11090, 
		call12020, 
		exit12090
	}

	public Revbbac() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
			start1000();
			exit1090();
		}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		wsaaRldgacct.set(SPACES);
		wsaaSacscode.set(SPACES);
		wsaaSacstype.set(SPACES);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processAcmvs3000();
		processAcmvOptical3010();
		//IBPLIFE-1433 Starts
		fmcOnFlag = FeaConfg.isFeatureExist(reverserec.company.toString().trim(), FMC_FEATURE_ID, appVars, "IT");
		if(fmcOnFlag) {
			if (isEQ(wsaaCovrUpd, SPACES)) {
				a100UpdateCovr();
			}
		} else {
			a100UpdateCovr();
		}
		//IBPLIFE-1433 End
		/* Read table TH605 to see if Bonus Workbench Extraction used      */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(th605);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.bonusInd,"Y")) {
			return ;
		}
		/* Perform to delete the service tax record.               <V74L01>*/
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(reverserec.effdate1);
		taxdrevIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			deleteTax13000();
		}
		
		/* Bonus Workbench  *                                              */
		processZptn11000();
		processZctn12000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError9000();
		}
		readContract2100();
		readT56882200();
		readT56452300();
		/* get correct SACSCODE/TYPE from T5645 entry for posting*/
		/*  Coverage debt later on*/
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstype.set(t5645rec.sacstype02);
			wsaaVariablesInner.wsaaSacscode01.set(t5645rec.sacscode07);
			wsaaVariablesInner.wsaaSacstype01.set(t5645rec.sacstype07);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstype.set(t5645rec.sacstype01);
			wsaaVariablesInner.wsaaSacscode01.set(t5645rec.sacscode08);
			wsaaVariablesInner.wsaaSacstype01.set(t5645rec.sacstype08);
		}
		wsaaVariablesInner.wsaaSacscodeComm.set(t5645rec.sacscode03);
		wsaaVariablesInner.wsaaSacstypeComm.set(t5645rec.sacstype03);
		wsaaVariablesInner.wsaaSacscodeRenl.set(t5645rec.sacscode04);
		wsaaVariablesInner.wsaaSacstypeRenl.set(t5645rec.sacstype04);
		wsaaVariablesInner.wsaaSacscodeSvcm.set(t5645rec.sacscode05);
		wsaaVariablesInner.wsaaSacstypeSvcm.set(t5645rec.sacstype05);
		wsaaVariablesInner.wsaaSacscodeOrcm.set(t5645rec.sacscode06);
		wsaaVariablesInner.wsaaSacstypeOrcm.set(t5645rec.sacstype06);
		//IBPLIFE-1433
		if(fmcOnFlag) {
			wsaaVariablesInner.wsaaFmSacstype.set(t5645rec.sacstype09);
			wsaaVariablesInner.wsaaZcSacstype.set(t5645rec.sacstype11);
		}
		/* get transaction description for putting on ACMV later on.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("??????????????????????????????");
		}
	}

protected void readContract2100()
	{
		/*START*/
		/* Need to get Contract type*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(reverserec.company);
		chdrenqIO.setChdrnum(reverserec.chdrnum);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			databaseError9500();
		}
		/*EXIT*/
	}

protected void readT56882200()
	{
		start2200();
	}

protected void start2200()
	{
		/* Read table T5688 to see if Component Level Accounting used*/
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItemitem(chdrenqIO.getCnttype());
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(formatsInner.itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company,itdmIO.getItemcoy())
		|| isNE(chdrenqIO.getCnttype(),itdmIO.getItemitem())
		|| isNE(t5688,itdmIO.getItemtabl())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrenqIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			databaseError9500();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readT56452300()
	{
		start2300();
	}

protected void start2300()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5645);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void processAcmvs3000()
	{
		start3000();
	}

protected void start3000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvs3100();
		}
		
	}

protected void processAcmvOptical3010()
	{
		start3010();
	}

protected void start3010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError9500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
				}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
				}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
				}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs3100();
				}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError9500();
			}
			}
		}

protected void reverseAcmvs3100()
	{
		start3100();
		readNextAcmv3180();
	}

protected void start3100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(),-1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		wsaaTranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError9000();
		}
		/* Check whether we need to update the COVRBBR record - the*/
		/*  Coverage Debt & Benefit Billing date.*/
		if (isEQ(acmvrevIO.getSacscode(),wsaaSacscode)
		&& isEQ(acmvrevIO.getSacstyp(),wsaaSacstype)) {
			updateCovrbbr4000();
			b100ProcessZrst();
		}
		//IBPLIFE-1433 Stars
		if (fmcOnFlag) {
			if (isEQ(acmvrevIO.getSacscode(),wsaaSacscode) && isEQ(acmvrevIO.getSacstyp(),wsaaVariablesInner.wsaaZcSacstype)) {
				updateAcChgs15000();
				wsaaCovrUpd.set("Y");
			}
			if (isEQ(acmvrevIO.getSacscode(),wsaaSacscode) && isEQ(acmvrevIO.getSacstyp(),wsaaVariablesInner.wsaaFmSacstype)) {
				wsaaCovrUpd.set("Y");
				updateZfmctrn14000();
			}
		}
		//IBPLIFE-1433 End
		if ((isEQ(acmvrevIO.getSacscode(), wsaaVariablesInner.wsaaSacscodeComm)
		&& isEQ(acmvrevIO.getSacstyp(), wsaaVariablesInner.wsaaSacstypeComm))
		|| (isEQ(acmvrevIO.getSacscode(), wsaaVariablesInner.wsaaSacscodeRenl)
		&& isEQ(acmvrevIO.getSacstyp(), wsaaVariablesInner.wsaaSacstypeRenl))
		|| (isEQ(acmvrevIO.getSacscode(), wsaaVariablesInner.wsaaSacscodeSvcm)
		&& isEQ(acmvrevIO.getSacstyp(), wsaaVariablesInner.wsaaSacstypeSvcm))
		|| (isEQ(acmvrevIO.getSacscode(), wsaaVariablesInner.wsaaSacscodeOrcm)
		&& isEQ(acmvrevIO.getSacstyp(), wsaaVariablesInner.wsaaSacstypeOrcm))) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(acmvrevIO.getRldgcoy());
			stringVariable1.addExpression(acmvrevIO.getTranref());
			stringVariable1.setStringInto(wsaaTranref);
			updateAgentCommission10000();
		}
	}

//IBPLIFE-1433 Starts
protected void updateZfmctrn14000() {

	wsaaRldgacct.set(acmvrevIO.getRldgacct());
	wsaaTranref.set(acmvrevIO.getTranref());
	covrbbrIO.setParams(SPACES);
	covrbbrIO.setFunction(varcom.readh);
	covrbbrIO.setFormat(formatsInner.covrbbrrec);
	covrbbrIO.setChdrcoy(reverserec.company);
	wsaaRldgacct.set(acmvrevIO.getRldgacct());
	covrbbrIO.setChdrnum(wsaaRldgChdrnum);
	covrbbrIO.setLife(wsaaRldgLife);
	covrbbrIO.setCoverage(wsaaRldgCoverage);
	covrbbrIO.setRider(wsaaRldgRider);
	covrbbrIO.setPlanSuffix(wsaaRldgPlanSuffix);
	wsaaPlanSuffix.set(wsaaRldgPlanSuffix);
	SmartFileCode.execute(appVars, covrbbrIO);
	if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(covrbbrIO.getParams());
		syserrrec.statuz.set(covrbbrIO.getStatuz());
		databaseError9500();
	}
	readT56874200();
	readT55344300();

	setPrecision(covrbbrIO.getCoverageDebt(), 2);
	covrbbrIO.setCoverageDebt(sub(covrbbrIO.getCoverageDebt(),acmvrevIO.getOrigamt()));
	covrbbrIO.setFunction(varcom.rewrt);
	SmartFileCode.execute(appVars, covrbbrIO);
	if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(covrbbrIO.getParams());
		syserrrec.statuz.set(covrbbrIO.getStatuz());
		databaseError9500();
	}

	b100ProcessZrst();
	zfmctrnIO=zfmcpfDAO.getZfmcRecord(reverserec.company.toString().trim(), reverserec.chdrnum.toString().trim());
	
	datcon4rec.datcon4Rec.set(SPACES);
	datcon4rec.frequency.set(t5534rec.unitFreq);
	List<Utrspf> utrspfList = utrspfDAO.searchUtrsRecord(reverserec.company.toString(), reverserec.chdrnum.toString());
	if(!utrspfList.isEmpty()) {
		for (Utrspf utrs : utrspfList) {
			fmcvpmsrec.product.set(chdrenqIO.getCnttype());
			fmcvpmsrec.covrCoverage.set(covrbbrIO.crtable);
			fmcvpmsrec.effectdt.set(chdrenqIO.getOccdate());
			fmcvpmsrec.ratedt.set(chdrenqIO.getOccdate());
			fmcvpmsrec.fund.set(utrs.getUnitVirtualFund());
			callProgram("FMCAPPCB", fmcvpmsrec);
			datcon4rec.frequency.set(fmcvpmsrec.frequnce);
		}
	} else {
		Hitspf hitspf = new Hitspf();
		hitspf.setChdrcoy(reverserec.company.toString());
		hitspf.setChdrnum(reverserec.chdrnum.toString());
		hitspf.setLife(reverserec.life.toString());
		hitspf.setCoverage(reverserec.coverage.toString());
		hitspf.setRider(reverserec.rider.toString());
		hitspfList = hitspfDAO.searchHitsRecord(hitspf);
		if(!hitspfList.isEmpty()) {
			for (Hitspf hits : hitspfList){
				fmcvpmsrec.product.set(chdrenqIO.getCnttype());
				fmcvpmsrec.covrCoverage.set(covrbbrIO.crtable);
				fmcvpmsrec.fund.set(hits.getZintbfnd());
				callProgram("FMCAPPCB", fmcvpmsrec);
				datcon4rec.frequency.set(fmcvpmsrec.frequnce);
			}
		}
	}
	datcon4rec.freqFactor.set(-1);
	datcon4rec.intDate1.set(zfmctrnIO.getZfmcdat());
	wsaaOccdate.set(zfmctrnIO.getZfmcdat());
	datcon4rec.billdayNum.set(wsaaOccDd);
	datcon4rec.billmonthNum.set(wsaaOccMm);
	callProgram(Datcon4.class, datcon4rec.datcon4Rec);
	if (isNE(datcon4rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon4rec.datcon4Rec);
		syserrrec.statuz.set(datcon4rec.statuz);
		systemError9000();
	}
	zfmctrnIO.setZfmcdat(datcon4rec.intDate2.toInt());
	zfmctrnIO.setZnofdue(zfmctrnIO.getZnofdue()-1);
	zfmctrnIO.setTranno(reverserec.tranno.toInt());
	zfmcpfDAO.updateZfmcRecord(zfmctrnIO);
}

protected void updateAcChgs15000() {

	wsaaRldgacct.set(acmvrevIO.getRldgacct());
	wsaaTranref.set(acmvrevIO.getTranref());
	covrbbrIO.setParams(SPACES);
	covrbbrIO.setFunction(varcom.readh);
	covrbbrIO.setFormat(formatsInner.covrbbrrec);
	covrbbrIO.setChdrcoy(reverserec.company);
	wsaaRldgacct.set(acmvrevIO.getRldgacct());
	covrbbrIO.setChdrnum(wsaaRldgChdrnum);
	covrbbrIO.setLife(wsaaRldgLife);
	covrbbrIO.setCoverage(wsaaRldgCoverage);
	covrbbrIO.setRider(wsaaRldgRider);
	covrbbrIO.setPlanSuffix(wsaaRldgPlanSuffix);
	wsaaPlanSuffix.set(wsaaRldgPlanSuffix);
	SmartFileCode.execute(appVars, covrbbrIO);
	if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(covrbbrIO.getParams());
		syserrrec.statuz.set(covrbbrIO.getStatuz());
		databaseError9500();
	}
	readT56874200();
	readT55344300();

	setPrecision(covrbbrIO.getCoverageDebt(), 2);
	covrbbrIO.setCoverageDebt(sub(covrbbrIO.getCoverageDebt(), acmvrevIO.getOrigamt()));
	covrbbrIO.setFunction(varcom.rewrt);
	SmartFileCode.execute(appVars, covrbbrIO);
	if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(covrbbrIO.getParams());
		syserrrec.statuz.set(covrbbrIO.getStatuz());
		databaseError9500();
	}

	b100ProcessZrst();
}
//IBPLIFE-1433 End

protected void readNextAcmv3180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall,"CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			databaseError9500();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void updateCovrbbr4000()
	{
			start4000();
		}

protected void start4000()
	{
		/* Read lock the COVRBBR record for updating*/
		/*    MOVE SPACES                 TO COVRBBR-PARAMS.               */
		/* MOVE BEGNH                  TO COVRBBR-FUNCTION.             */
		/*    MOVE READH                  TO COVRBBR-FUNCTION.     <LA4464>*/
		/*    MOVE COVRBBRREC             TO COVRBBR-FORMAT.               */
		/*    MOVE REVE-COMPANY           TO COVRBBR-CHDRCOY.              */
		/* MOVE SPACES                 TO WSAA-RLDGACCT.                */
		/* MOVE ZEROS                  TO WSAA-PLAN-SUFFIX.             */
		/* MOVE ACMVREV-RLDGACCT       TO WSAA-RLDGACCT.                */
		/* MOVE WSAA-RLDG-CHDRNUM      TO COVRBBR-CHDRNUM.              */
		/*    MOVE WSAA-TREF-CHDRCOY      TO COVRBBR-CHDRCOY.      <LA4464>*/
		/*    MOVE WSAA-TREF-CHDRNUM      TO COVRBBR-CHDRNUM.      <LA4464>*/
		/*    MOVE WSAA-TREF-LIFE         TO COVRBBR-LIFE.         <LA4464>*/
		/*    MOVE WSAA-TREF-COVERAGE     TO COVRBBR-COVERAGE.     <LA4464>*/
		/*    MOVE WSAA-TREF-RIDER        TO COVRBBR-RIDER.        <LA4464>*/
		/* check if Plan Suffix is set on TRANREF  field - if so,          */
		/*  then the Coverage debt and benefit billed dates can be         */
		/*  amended correctly as per the reversal procedure. IF not set,   */
		/*  then we cannot identify which contract component is benefit    */
		/*  billed - don't forget, the debt automatically gets posted to   */
		/*  the coverage, even if a rider is benefit billed.               */
		/*  SO,                                                            */
		/*  IF the ACMVREV record we are processing was written prior      */
		/*   to the new updated Benefit Billing subroutines, then we go    */
		/*   go to the 5000 section instead to try and use other methods   */
		/*   to reverse the Benefit Billing details held on the COVRBBR    */
		/*   records.                                                      */
		/*    IF WSAA-TREF-PLAN-SUFFIX    = SPACES                 <LA4464>*/
		/*        PERFORM 5000-ALT-COVR-REVERSAL                   <LA4464>*/
		/*        GO TO 4090-EXIT                                  <LA4464>*/
		/*    ELSE                                                 <LA4464>*/
		/*        MOVE WSAA-TREF-PLAN-SUFFIX  TO WSAA-PLAN-SUFFIX  <LA4464>*/
		/*        MOVE WSAA-PLNSFX           TO COVRBBR-PLAN-SUFFIX<LA4464>*/
		/*    END-IF.                                              <LA4464>*/
		/* IF WSAA-RLDG-PLAN-SUFFIX        NOT = SPACES                 */
		/*     MOVE WSAA-RLDG-PLAN-SUFFIX  TO WSAA-PLAN-SUFFIX          */
		/*     MOVE WSAA-PLNSFX            TO COVRBBR-PLAN-SUFFIX       */
		/*     MOVE WSAA-RLDG-LIFE         TO COVRBBR-LIFE              */
		/*     MOVE WSAA-RLDG-COVERAGE     TO COVRBBR-COVERAGE          */
		/* ELSE                                                         */
		/*     MOVE '01'                   TO COVRBBR-LIFE              */
		/*     MOVE '01'                   TO COVRBBR-COVERAGE          */
		/*     MOVE ZEROS                  TO COVRBBR-PLAN-SUFFIX       */
		/* END-IF.                                                      */
		/* Before we start updating COVRBBR records, we must realise the*/
		/*  following:*/
		/*   1) If a Coverage is benefit billed, then update*/
		/*        the benefit billed date & Coverage debt fields on the*/
		/*        Coverage COVRBBR record,*/
		/*   2) If a Rider is benefit billed, then update*/
		/*        the benefit billed date on the COVRBBR record for the*/
		/*        Rider and update the Coverage debt on the COVRBBR record*/
		/*        for the Coverage.*/
		/* If the ACMVREV record we are processing is for a Rider,*/
		/*  i.e. WSAA-RLDG-RIDER NOT = '00', then first amend the*/
		/*      Coverage debt on the COVRBBR record for the Coverage and*/
		/*      then we will go and update the benefit billed date on the*/
		/*      Riders COVRBBR record.*/
		/* set rider to '00', since we amending coverage debt first and*/
		/*  this is always held against the coverage COVRBBR record*/
		/*  anyway.*/
		/*    MOVE '00'                   TO COVRBBR-RIDER.                */
		/*    CALL 'COVRBBRIO'            USING COVRBBR-PARAMS.            */
		/*    IF COVRBBR-STATUZ           NOT = O-K                        */
		/*        MOVE COVRBBR-PARAMS     TO SYSR-PARAMS                   */
		/*        MOVE COVRBBR-STATUZ     TO SYSR-STATUZ                   */
		/*        PERFORM 9500-DATABASE-ERROR                              */
		/*    END-IF.                                                      */
		/* calculate new revised Coverage debt value*/
		/*    COMPUTE COVRBBR-COVERAGE-DEBT = COVRBBR-COVERAGE-DEBT        */
		/*                                    - ACMVREV-ORIGAMT.           */
		/* Check to see if we have the full component key from the ACMVREV*/
		/* record to enable us to identify which Coverage/Rider we are*/
		/* processing and if so then decide whether we are going to update*/
		/* a benefit-billed rider or coverage.....*/
		/*  IS the ACMV record we are processing posted for a Coverage or  */
		/*   a Rider. IF its for a Rider, then WSAA-TREF-RIDER = '00' so   */
		/*   we want to update the coverage debt on the Coverage record    */
		/*   and then update the Benefit Billed date on the Rider.         */
		/*   IF its for a Coverage, update the debt and the Benefit        */
		/*   Billed date on the Coverage.                                  */
		/* IF WSAA-RLDG-PLAN-SUFFIX    NOT = SPACES                     */
		/*    AND WSAA-RLDG-RIDER      NOT = SPACES                     */
		/*    AND WSAA-RLDG-RIDER      NOT = '00'                       */
		/*    IF WSAA-TREF-RIDER          NOT = '00'               <LA4464>*/
		/*        rewrite amended COVRBBR coverage record & get*/
		/*        the COVRBBR rider record*/
		/*     MOVE REVE-NEW-TRANNO    TO COVRBBR-TRANNO                */
		/*     MOVE REVE-TRANS-DATE    TO COVRBBR-TRANSACTION-DATE      */
		/*     MOVE REVE-TRANS-TIME    TO COVRBBR-TRANSACTION-TIME      */
		/*     MOVE REVE-USER          TO COVRBBR-USER                  */
		/*     MOVE REVE-TERMID        TO COVRBBR-TERMID                */
		/*        MOVE REWRT              TO COVRBBR-FUNCTION              */
		/*        CALL 'COVRBBRIO'        USING COVRBBR-PARAMS             */
		/*        IF COVRBBR-STATUZ       NOT = O-K                        */
		/*            MOVE COVRBBR-PARAMS TO SYSR-PARAMS                   */
		/*            MOVE COVRBBR-STATUZ TO SYSR-STATUZ                   */
		/*            PERFORM 9500-DATABASE-ERROR                          */
		/*        END-IF                                                   */
		/*        now read & hold the COVRBBR rider record*/
		/*     MOVE WSAA-RLDG-RIDER    TO COVRBBR-RIDER                 */
		/*        MOVE WSAA-TREF-RIDER    TO COVRBBR-RIDER         <LA4464>*/
		/*        MOVE READH              TO COVRBBR-FUNCTION              */
		/*        CALL 'COVRBBRIO'        USING COVRBBR-PARAMS             */
		/*        IF COVRBBR-STATUZ       NOT = O-K                        */
		/*            MOVE COVRBBR-PARAMS TO SYSR-PARAMS                   */
		/*            MOVE COVRBBR-STATUZ TO SYSR-STATUZ                   */
		/*            PERFORM 9500-DATABASE-ERROR                          */
		/*        END-IF                                                   */
		/*    END-IF.                                                      */
		/* reverse last benefit billing date held on COVRBBR record*/
		/*    PERFORM 4100-REVERSE-BB-DATE.                                */
		/* rewrite COVRBBR record*/
		/* MOVE REVE-NEW-TRANNO        TO COVRBBR-TRANNO.               */
		/* MOVE REVE-TRANS-DATE        TO COVRBBR-TRANSACTION-DATE.     */
		/* MOVE REVE-TRANS-TIME        TO COVRBBR-TRANSACTION-TIME.     */
		/* MOVE REVE-USER              TO COVRBBR-USER.                 */
		/* MOVE REVE-TERMID            TO COVRBBR-TERMID.               */
		/*    MOVE REWRT                  TO COVRBBR-FUNCTION.             */
		/*    CALL 'COVRBBRIO'            USING COVRBBR-PARAMS.            */
		/*    IF COVRBBR-STATUZ           NOT = O-K                        */
		/*        MOVE COVRBBR-PARAMS     TO SYSR-PARAMS                   */
		/*        MOVE COVRBBR-STATUZ     TO SYSR-STATUZ                   */
		/*        PERFORM 9500-DATABASE-ERROR                              */
		/*    END-IF.                                                      */
		/* *  To reverse COVRBBR for the coverage-debt,                    */
		wsaaRldgacct.set(acmvrevIO.getRldgacct());
		wsaaTranref.set(acmvrevIO.getTranref());
		covrbbrIO.setParams(SPACES);
		covrbbrIO.setFunction(varcom.readh);
		covrbbrIO.setFormat(formatsInner.covrbbrrec);
		covrbbrIO.setChdrcoy(reverserec.company);
		wsaaRldgacct.set(acmvrevIO.getRldgacct());
		covrbbrIO.setChdrnum(wsaaRldgChdrnum);
		covrbbrIO.setLife(wsaaRldgLife);
		covrbbrIO.setCoverage(wsaaRldgCoverage);
		covrbbrIO.setRider(wsaaRldgRider);
		covrbbrIO.setPlanSuffix(wsaaRldgPlanSuffix);
		wsaaPlanSuffix.set(wsaaRldgPlanSuffix);
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			databaseError9500();
		}
		setPrecision(covrbbrIO.getCoverageDebt(), 2);
		covrbbrIO.setCoverageDebt(sub(covrbbrIO.getCoverageDebt(),acmvrevIO.getOrigamt()));
			covrbbrIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, covrbbrIO);
			if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrbbrIO.getParams());
				syserrrec.statuz.set(covrbbrIO.getStatuz());
				databaseError9500();
			}
		/* To reverse the benefit billing located at the coverage/rider * */
		covrbbrIO.setParams(SPACES);
		covrbbrIO.setFormat(formatsInner.covrbbrrec);
		covrbbrIO.setChdrcoy(wsaaTrefChdrcoy);
		covrbbrIO.setChdrnum(wsaaTrefChdrnum);
		covrbbrIO.setLife(wsaaTrefLife);
		covrbbrIO.setCoverage(wsaaTrefCoverage);
			covrbbrIO.setRider(wsaaTrefRider);
		covrbbrIO.setPlanSuffix(ZERO);
			covrbbrIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, covrbbrIO);
			if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrbbrIO.getParams());
				syserrrec.statuz.set(covrbbrIO.getStatuz());
				databaseError9500();
			}
		reverseBbDate4100();
		covrbbrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			databaseError9500();
		}
	}

protected void reverseBbDate4100()
	{
			start4100();
		}

protected void start4100()
	{
		/* Read tables T5687 and T5534 first*/
		readT56874200();
		readT55344300();
		/* Subtract 1 frequency off current COVRBBR benefit billing date*/
		/* If the benefit billed date is zeros, then don't try to*/
		/*  use DATCON2 - this may be the case if we are processing*/
		/*  ACMVREV records which were posted using Contract Level*/
		/*  Accounting and thus we don't know which individual Coverages*/
		/*  or Riders relate to the Benefit billed ACMVREV records*/
		if (isEQ(covrbbrIO.getBenBillDate(),ZERO)) {
			return ;
		}
		/*    MOVE SPACES                 TO DTC2-DATCON2-REC.             */
		/*    MOVE T5534-UNIT-FREQ        TO DTC2-FREQUENCY.               */
		/*    MOVE -1                     TO DTC2-FREQ-FACTOR.             */
		/*    MOVE COVRBBR-BEN-BILL-DATE  TO DTC2-INT-DATE-1.              */
		/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC.          */
		/*    IF DTC2-STATUZ              NOT = O-K                        */
		/*        MOVE DTC2-DATCON2-REC   TO SYSR-PARAMS                   */
		/*        MOVE DTC2-STATUZ        TO SYSR-STATUZ                   */
		/*        PERFORM 9000-SYSTEM-ERROR                                */
		/*    END-IF.                                                      */
		/*    MOVE DTC2-INT-DATE-2        TO COVRBBR-BEN-BILL-DATE.        */
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.frequency.set(t5534rec.unitFreq);
		datcon4rec.freqFactor.set(-1);
		datcon4rec.intDate1.set(covrbbrIO.getBenBillDate());
		wsaaOccdate.set(covrbbrIO.getBenBillDate());
		datcon4rec.billdayNum.set(wsaaOccDd);
		datcon4rec.billmonthNum.set(wsaaOccMm);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			systemError9000();
		}
		covrbbrIO.setBenBillDate(datcon4rec.intDate2);
	}

protected void readT56874200()
	{
		start4200();
	}

protected void start4200()
	{
		/* Read table T5687 to get benefit billing method*/
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItemitem(covrbbrIO.getCrtable());
		itdmIO.setItmfrm(covrbbrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(formatsInner.itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company,itdmIO.getItemcoy())
		|| isNE(covrbbrIO.getCrtable(),itdmIO.getItemitem())
		|| isNE(t5687,itdmIO.getItemtabl())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covrbbrIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h053);
			databaseError9500();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

protected void readT55344300()
	{
		start4300();
	}

protected void start4300()
	{
		/* Read T5534 benefit billing method table to find out unit*/
		/*  deduction frequency.*/
		itemIO.setParams(SPACES);
		itemIO.setItemitem(t5687rec.bbmeth);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5534);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		t5534rec.t5534Rec.set(itemIO.getGenarea());
	}

protected void altCovrReversal5000()
	{
		/*START*/
		/* This section will control all ACMV reversals where the TRANREF  */
		/*  field hasn't been set up with the Component Key of the         */
		/*  Coverage/Rider creating the debt.                              */
		/* NB. We will only get to this section if a version of Benefit    */
		/*      Billing prior to the May 0593 Release is being used.       */
		/* First check whether the ACMV we are trying to reverse was       */
		/*  written with Component or Contract Level Accounting.           */
		/* IF the ACMV was written with Component Level Accounting, then   */
		/*  it will have been posted against the Coverage/Rider which is   */
		/*  being Benefit Billed - therefore, we should be able to reverse */
		/*  correctly.                                                     */
		/* IF the ACMV was written with Contract Level Accounting, we have */
		/*  no idea as to which of the Contracts components generated the  */
		/*  posting, so we will start reading through the COVR             */
		/*  coverage/Rider records until we find one (for the contract we  */
		/*   are processing) that has a Benefit billing method defined on  */
		/*   T5687 - we will then update this coverage and/or rider,       */
		/*   assuming it is the one that has had benefit billing done to   */
		/*   it. IE we are adopting the policy that 'first one found gets  */
		/*   the update'.                                                  */
		wsaaRldgacct.set(SPACES);
		wsaaRldgacct.set(acmvrevIO.getRldgacct());
		if (isNE(wsaaRldgPlanSuffix,SPACES)) {
			/*    Component Level Accounting in force                          */
			compLevelReversal6000();
		}
		else {
			contLevelReversal7000();
		}
		/*EXIT*/
	}

protected void compLevelReversal6000()
	{
		start6000();
	}

protected void start6000()
	{
		/* Read-lock the COVRBBR Coverage record for updating              */
		covrbbrIO.setParams(SPACES);
		covrbbrIO.setFunction(varcom.readh);
		covrbbrIO.setFormat(formatsInner.covrbbrrec);
		wsaaPlanSuffix.set(wsaaRldgPlanSuffix);
		covrbbrIO.setPlanSuffix(wsaaPlnsfx);
		covrbbrIO.setLife(wsaaRldgLife);
		covrbbrIO.setCoverage(wsaaRldgCoverage);
		covrbbrIO.setChdrcoy(reverserec.company);
		covrbbrIO.setChdrnum(wsaaRldgChdrnum);
		covrbbrIO.setRider("00");
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			databaseError9500();
		}
		/* calculate new revised Coverage debt value                       */
		setPrecision(covrbbrIO.getCoverageDebt(), 2);
		covrbbrIO.setCoverageDebt(sub(covrbbrIO.getCoverageDebt(),acmvrevIO.getOrigamt()));
		/*  If the debt is against the Rider, then Rewrite the Coverage    */
		/*   record and then get the Rider record to update the Benefit    */
		/*   billed date on it.                                            */
		wsaaRider.set(wsaaRldgRider);
		updateCovrs8000();
	}

protected void contLevelReversal7000()
	{
			start7000();
		}

protected void start7000()
	{
		/* If we get to this section, we have a Benefit-billing ACMV to    */
		/*  reverse but because Contract Level Accounting is in force, we  */
		/*  don't know which Coverage/Rider on the Contract has generated  */
		/*  the posting - so therefore, we will read through the COVR      */
		/*  records for the contract and the first one we find that is     */
		/*  benefit billed will be treat as the one which generated the    */
		/*  posting.                                                       */
		covrbbrIO.setParams(SPACES);
		covrbbrIO.setFormat(formatsInner.covrbbrrec);
		covrbbrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrbbrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrbbrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrbbrIO.setChdrcoy(reverserec.company);
		covrbbrIO.setChdrnum(wsaaRldgChdrnum);
		covrbbrIO.setPlanSuffix(ZERO);
		wsaaBbCovrFound.set("N");
		while ( !(isEQ(covrbbrIO.getStatuz(),varcom.endp))) {
			searchCovrs7100();
		}
		
		if (isEQ(wsaaBbCovrFound,"N")) {
			return ;
		}
		/* Get here so we have found a Coverage/Rider with a Benefit       */
		/*  Bill method on T5687.                                          */
		/* If it is a Rider that is Benefit billed, then store the         */
		/*  Rider number so that we can amend the debt on the Coverage     */
		/*  before updating the benefit-billed date on the Rider.          */
		if (isNE(covrbbrIO.getRider(),"00")) {
			wsaaHoldRider.set(SPACES);
			wsaaHoldRider.set(covrbbrIO.getRider());
			covrbbrIO.setRider("00");
		}
		else {
			wsaaHoldRider.set("00");
		}
		/* Lock Coverage record to amend debt first                        */
		covrbbrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			databaseError9500();
		}
		/* calculate new revised Coverage debt value                       */
		setPrecision(covrbbrIO.getCoverageDebt(), 2);
		covrbbrIO.setCoverageDebt(sub(covrbbrIO.getCoverageDebt(),acmvrevIO.getOrigamt()));
		wsaaRider.set(wsaaHoldRider);
		updateCovrs8000();
	}

protected void searchCovrs7100()
	{
			start7100();
		}

protected void start7100()
	{
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)
		&& isNE(covrbbrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			systemError9000();
		}
		if (isNE(reverserec.company,covrbbrIO.getChdrcoy())
		|| isNE(wsaaRldgChdrnum,covrbbrIO.getChdrnum())
		|| isEQ(covrbbrIO.getStatuz(),varcom.endp)) {
			covrbbrIO.setStatuz(varcom.endp);
			return ;
		}
		/* We now have a Coverage/Rider for this contract, so let us go    */
		/*  and find out if it is Benefit Billed                           */
		/* Read table T5687 to get benefit billing method                  */
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItemitem(covrbbrIO.getCrtable());
		itdmIO.setItmfrm(covrbbrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(formatsInner.itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company,itdmIO.getItemcoy())
		|| isNE(covrbbrIO.getCrtable(),itdmIO.getItemitem())
		|| isNE(t5687,itdmIO.getItemtabl())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covrbbrIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h053);
			databaseError9500();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (isNE(t5687rec.bbmeth,SPACES)) {
			wsaaBbCovrFound.set("Y");
			covrbbrIO.setStatuz(varcom.endp);
			return ;
		}
		covrbbrIO.setFunction(varcom.nextr);
	}

protected void updateCovrs8000()
	{
		start8000();
	}

protected void start8000()
	{
		if (isNE(wsaaRider,"00")) {
			/*        rewrite amended COVRBBR coverage record & get            */
			/*        the COVRBBR rider record                                 */
			/*     MOVE REVE-NEW-TRANNO    TO COVRBBR-TRANNO           <001>*/
			/*     MOVE REVE-TRANS-DATE    TO COVRBBR-TRANSACTION-DATE <001>*/
			/*     MOVE REVE-TRANS-TIME    TO COVRBBR-TRANSACTION-TIME <001>*/
			/*     MOVE REVE-USER          TO COVRBBR-USER             <001>*/
			/*     MOVE REVE-TERMID        TO COVRBBR-TERMID           <001>*/
			covrbbrIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, covrbbrIO);
			if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrbbrIO.getParams());
				syserrrec.statuz.set(covrbbrIO.getStatuz());
				databaseError9500();
			}
			/*        now read & hold the COVRBBR rider record                 */
			covrbbrIO.setRider(wsaaRider);
			covrbbrIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, covrbbrIO);
			if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrbbrIO.getParams());
				syserrrec.statuz.set(covrbbrIO.getStatuz());
				databaseError9500();
			}
		}
		/* reverse last benefit billing date held on COVRBBR record        */
		reverseBbDate4100();
		/* rewrite COVRBBR record                                          */
		/* MOVE REVE-NEW-TRANNO        TO COVRBBR-TRANNO.          <001>*/
		/* MOVE REVE-TRANS-DATE        TO COVRBBR-TRANSACTION-DATE.<001>*/
		/* MOVE REVE-TRANS-TIME        TO COVRBBR-TRANSACTION-TIME.<001>*/
		/* MOVE REVE-USER              TO COVRBBR-USER.            <001>*/
		/* MOVE REVE-TERMID            TO COVRBBR-TERMID.          <001>*/
		covrbbrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			databaseError9500();
		}
	}

protected void a100UpdateCovr()
	{
		/*A110-COVR*/
		covrbbrIO.setParams(SPACES);
		covrbbrIO.setChdrcoy(reverserec.company);
		covrbbrIO.setChdrnum(reverserec.chdrnum);
		covrbbrIO.setPlanSuffix(ZERO);
		covrbbrIO.setFunction(formatsInner.covrbbrrec);
		covrbbrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrbbrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrbbrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrbbrIO.getStatuz(),varcom.endp))) {
			a200CallCovr();
		}
		
		/*A190-EXIT*/
	}

protected void a200CallCovr()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a210Covr();
				case a280Nextr: 
					a280Nextr();
				case a290Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210Covr()
	{
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)
		&& isNE(covrbbrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrbbrIO.getParams());
			databaseError9500();
		}
		if (isNE(covrbbrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrbbrIO.getChdrnum(),reverserec.chdrnum)
		|| isEQ(covrbbrIO.getStatuz(),varcom.endp)) {
			covrbbrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a290Exit);
		}
		if (isLTE(covrbbrIO.getBenBillDate(),reverserec.ptrneff)
		|| isEQ(covrbbrIO.getBenBillDate(),varcom.vrcmMaxDate)) {
			goTo(GotoLabel.a280Nextr);
		}
		reverseBbDate4100();
		covrbbrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			databaseError9500();
		}
	}

protected void a280Nextr()
	{
		covrbbrIO.setFunction(varcom.nextr);
	}

protected void b100ProcessZrst()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
		try {
				switch (nextMethod) {
				case DEFAULT: 
			b100Start();
				case b100Nextr: 
					b100Nextr();
				}
				break;
		}
		catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b100Start()
	{
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrcoy(reverserec.company);
		zrstnudIO.setChdrnum(reverserec.chdrnum);
		/*    MOVE WSAA-TREF-LIFE         TO ZRSTNUD-LIFE.         <LA4464>*/
		/*    MOVE WSAA-TREF-COVERAGE     TO ZRSTNUD-COVERAGE.     <LA4464>*/
		zrstnudIO.setLife(wsaaRldgLife);
		zrstnudIO.setCoverage(wsaaRldgCoverage);
		zrstnudIO.setRider(wsaaRldgRider);
		zrstnudIO.setFormat(formatsInner.zrstnudrec);
		zrstnudIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zrstnudIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zrstnudIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE");
	}

protected void b100Nextr()
	{
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(),varcom.oK)
		&& isNE(zrstnudIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			syserrrec.params.set(zrstnudIO.getParams());
			databaseError9500();
		}
		if (isEQ(zrstnudIO.getStatuz(),varcom.endp)
		|| isNE(zrstnudIO.getChdrcoy(),reverserec.company)
		|| isNE(zrstnudIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(zrstnudIO.getLife(), wsaaRldgLife)
		|| isNE(zrstnudIO.getCoverage(), wsaaRldgCoverage)
		|| isNE(zrstnudIO.getRider(), wsaaRldgRider)) {
			return ;
		}
		/*    PERFORM B200-DELETE-ZRST UNTIL ZRSTNUD-STATUZ = ENDP.<LA4464>*/
			b200DeleteZrst();
		zrstnudIO.setFunction(varcom.nextr);
		goTo(GotoLabel.b100Nextr);
	}

protected void b200DeleteZrst()
	{
		b200Start();
	}

protected void b200Start()
	{
		/*    IF ZRSTNUD-CHDRCOY           = REVE-COMPANY AND      <LA4464>*/
		/*       ZRSTNUD-CHDRNUM           = REVE-CHDRNUM          <LA4464>*/
		/*    MOVE READH                  TO ZRSTNUD-FUNCTION   <V4LAQR>*/
		/*       MOVE READD                  TO ZRSTNUD-FUNCTION   <LA4464>*/
		/*       CALL 'ZRSTNUDIO'         USING ZRSTNUD-PARAMS     <LA4464>*/
		/*       IF ZRSTNUD-STATUZ          NOT = O-K              <LA4464>*/
		/*          MOVE ZRSTNUD-STATUZ      TO SYSR-STATUZ        <LA4464>*/
		/*          MOVE ZRSTNUD-PARAMS      TO SYSR-PARAMS        <LA4464>*/
		/*          PERFORM 9500-DATABASE-ERROR                    <LA4464>*/
		/*       END-IF                                            <LA4464>*/
		/*       IF ZRSTNUD-TRANNO         = REVE-TRANNO           <LA4464>*/
		/*       MOVE DELET               TO ZRSTNUD-FUNCTION   <V4LAQR>*/
		/*          MOVE DELTD               TO ZRSTNUD-FUNCTION   <LA4464>*/
		/*       ELSE                                              <LA4464>*/
		/*        COMPUTE ZRSTNUD-ZRAMOUNT02 = ZRSTNUD-ZRAMOUNT02 -<LA4464>*/
		/*                                    ACMVREV-ORIGAMT      <LA4464>*/
		/*       MOVE REWRT               TO ZRSTNUD-FUNCTION   <V4LAQR>*/
		/*          MOVE WRITD               TO ZRSTNUD-FUNCTION   <LA4464>*/
		/*       END-IF                                            <LA4464>*/
		/*       CALL 'ZRSTNUDIO'         USING ZRSTNUD-PARAMS     <LA4464>*/
		/*       IF ZRSTNUD-STATUZ        NOT = O-K                <LA4464>*/
		/*          MOVE ZRSTNUD-STATUZ      TO SYSR-STATUZ        <LA4464>*/
		/*          MOVE ZRSTNUD-PARAMS      TO SYSR-PARAMS        <LA4464>*/
		/*          PERFORM 9500-DATABASE-ERROR                    <LA4464>*/
		/*       END-IF                                            <LA4464>*/
		/*    END-IF.                                              <LA4464>*/
		/*    MOVE NEXTR                  TO ZRSTNUD-FUNCTION.     <LA4464>*/
		/*    CALL 'ZRSTNUDIO'         USING ZRSTNUD-PARAMS.       <LA4464>*/
		/*    IF  ZRSTNUD-STATUZ       NOT = O-K                   <LA4464>*/
		/*    AND ZRSTNUD-STATUZ       NOT = ENDP                  <LA4464>*/
		/*        MOVE ZRSTNUD-STATUZ     TO SYSR-STATUZ           <LA4464>*/
		/*        MOVE ZRSTNUD-PARAMS     TO SYSR-PARAMS           <LA4464>*/
		/*        PERFORM 9500-DATABASE-ERROR                      <LA4464>*/
		/*    END-IF.                                              <LA4464>*/
		/*    IF ZRSTNUD-STATUZ            = ENDP         OR       <LA4464>*/
		/*       ZRSTNUD-CHDRCOY       NOT = REVE-COMPANY OR       <LA4464>*/
		/*       ZRSTNUD-CHDRNUM       NOT = REVE-CHDRNUM OR       <LA4464>*/
		/*       ZRSTNUD-LIFE          NOT = WSAA-TREF-LIFE OR     <LA4464>*/
		/*       ZRSTNUD-COVERAGE      NOT = WSAA-TREF-COVERAGE    <LA4464>*/
		/*       MOVE ENDP                TO ZRSTNUD-STATUZ        <LA4464>*/
		/*    END-IF.                                              <LA4464>*/
		zrstIO.setParams(SPACES);
		zrstIO.setRrn(zrstnudIO.getRrn());
		zrstIO.setFormat(formatsInner.zrstrec);
		zrstIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrstIO.getStatuz());
			syserrrec.params.set(zrstIO.getParams());
				databaseError9500();
			}
		if (isEQ(zrstIO.getTranno(), reverserec.tranno)) {
			zrstIO.setFunction(varcom.deltd);
			}
			else {
			setPrecision(zrstIO.getZramount02(), 2);
			zrstIO.setZramount02(sub(zrstIO.getZramount02(), acmvrevIO.getOrigamt()));
			zrstIO.setFunction(varcom.writd);
			}
		SmartFileCode.execute(appVars, zrstIO);
		if (isNE(zrstIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrstIO.getStatuz());
			syserrrec.params.set(zrstIO.getParams());
				databaseError9500();
			}
		}

protected void c000CallRounding()
	{
		/*C100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(reverserec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(acmvrevIO.getOrigcurr());
		zrdecplrec.batctrcde.set(reverserec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError9000();
		}
		/*C900-EXIT*/
	}

protected void systemError9000()
	{
			start9000();
			exit9490();
		}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
			start9500();
			exit9990();
		}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void updateAgentCommission10000()
	{
		update10000();
	}

protected void update10000()
	{
		readAcmvldg10400();
		agcmbchIO.setDataKey(SPACES);
		agcmbchIO.setChdrcoy(wsaaTrefChdrcoy);
		agcmbchIO.setChdrnum(wsaaTrefChdrnum);
		agcmbchIO.setLife(wsaaTrefLife);
		agcmbchIO.setCoverage(wsaaTrefCoverage);
		agcmbchIO.setRider(wsaaTrefRider);
		agcmbchIO.setPlanSuffix(wsaaPlnsfx);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmbchIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)
		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			systemError9000();
		}
		if (isNE(agcmbchIO.getChdrcoy(),wsaaTrefChdrcoy)
		|| isNE(agcmbchIO.getChdrnum(),wsaaTrefChdrnum)
		|| isNE(agcmbchIO.getLife(),wsaaTrefLife)
		|| isNE(agcmbchIO.getCoverage(),wsaaTrefCoverage)
		|| isNE(agcmbchIO.getRider(),wsaaTrefRider)
		|| isNE(agcmbchIO.getPlanSuffix(),wsaaPlnsfx)) {
			agcmbchIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmbchIO.getStatuz(),varcom.endp))) {
			processCommission10100();
		}
		
	}

protected void processCommission10100()
	{
					process10110();
					nextAgcm10120();
				}

protected void process10110()
	{
		/* When a dormant AGCM record is encountered, do not process    */
		/* the record but rewrite it to release the record lock and go  */
		/* on to the next record.                                       */
		/* Also, donot reverse sp top up AGCMs (PT date = 0)            */
		if (isEQ(agcmbchIO.getDormantFlag(),"Y")
		|| isEQ(agcmbchIO.getPtdate(),0)) {
			return ;
		}
		if (isGT(agcmbchIO.getEfdate(),reverserec.effdate1)
		|| isLT(agcmbchIO.getCurrto(),reverserec.effdate1)) {
			return ;
		}
		/* Donot process AGCM any further if the amount we are             */
		/* reversing is all over target and the seqno is > 1 as we         */
		/* all over target to the first AGCM seqno.                        */
		/* Read COVRBBR                                                    */
		covrbbrIO.setDataKey(SPACES);
		covrbbrIO.setChdrcoy(wsaaTrefChdrcoy);
		covrbbrIO.setChdrnum(wsaaTrefChdrnum);
		covrbbrIO.setLife(wsaaTrefLife);
		covrbbrIO.setCoverage(wsaaTrefCoverage);
		covrbbrIO.setRider(wsaaTrefRider);
		covrbbrIO.setPlanSuffix(wsaaPlnsfx);
		covrbbrIO.setFunction(varcom.readr);
		covrbbrIO.setFormat(formatsInner.covrbbrrec);
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			systemError9000();
		}
		/* set up common linkage for the commission reversal subroutine    */
		comlinkrec.agent.set(agcmbchIO.getAgntnum());
		comlinkrec.chdrcoy.set(reverserec.company);
		comlinkrec.chdrnum.set(reverserec.chdrnum);
		comlinkrec.language.set(reverserec.language);
		comlinkrec.life.set(agcmbchIO.getLife());
		comlinkrec.coverage.set(agcmbchIO.getCoverage());
		comlinkrec.rider.set(agcmbchIO.getRider());
		comlinkrec.planSuffix.set(agcmbchIO.getPlanSuffix());
		comlinkrec.crtable.set(covrbbrIO.getCrtable());
		comlinkrec.jlife.set(covrbbrIO.getJlife());
		comlinkrec.agentClass.set(agcmbchIO.getAgentClass());
		comlinkrec.effdate.set(agcmbchIO.getEfdate());
		comlinkrec.billfreq.set(t5534rec.unitFreq);
		comlinkrec.annprem.set(agcmbchIO.getAnnprem());
		comlinkrec.targetPrem.set(ZERO);
		comlinkrec.currto.set(covrbbrIO.getCurrto());
		comlinkrec.seqno.set(agcmbchIO.getSeqno());
		comlinkrec.ptdate.set(covrbbrIO.getBenBillDate());
		for (wsaaVariablesInner.wsaaCommType.set(1); !(isEQ(wsaaVariablesInner.wsaaCommType, 4)); wsaaVariablesInner.wsaaCommType.add(1)){
			callSubroutine10200();
		}
		rewriteAgcm10300();
	}

protected void nextAgcm10120()
	{
		agcmbchIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.endp)
		&& isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			systemError9000();
		}
		if (isNE(agcmbchIO.getChdrnum(),covrbbrIO.getChdrnum())
		|| isNE(agcmbchIO.getChdrcoy(),covrbbrIO.getChdrcoy())
		|| isNE(agcmbchIO.getCoverage(),covrbbrIO.getCoverage())
		|| isNE(agcmbchIO.getLife(),covrbbrIO.getLife())
		|| isNE(agcmbchIO.getRider(),covrbbrIO.getRider())
		|| isNE(agcmbchIO.getPlanSuffix(),covrbbrIO.getPlanSuffix())) {
			agcmbchIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void callSubroutine10200()
	{
			go10210();
		}

protected void go10210()
	{
		wsaaVariablesInner.wsaaPayamnt[wsaaVariablesInner.wsaaCommType.toInt()].set(ZERO);
		wsaaVariablesInner.wsaaErndamt[wsaaVariablesInner.wsaaCommType.toInt()].set(ZERO);
		/* set up linkage for the commission reversal subroutine           */
		comlinkrec.function.set(SPACES);
		comlinkrec.statuz.set(SPACES);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.instprem.set(ZERO);
		if (isEQ(wsaaVariablesInner.wsaaCommType, 1)) {
			if (isEQ(agcmbchIO.getBascpy(),SPACES)
			|| (isEQ(agcmbchIO.getCompay(),ZERO)
			&& isEQ(agcmbchIO.getComern(),ZERO))) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmbchIO.getBascpy());
			}
		}
		if (isEQ(wsaaVariablesInner.wsaaCommType, 2)) {
			if (isEQ(agcmbchIO.getRnwcpy(),SPACES)
			|| isEQ(agcmbchIO.getRnlcdue(),ZERO)) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmbchIO.getRnwcpy());
			}
		}
		if (isEQ(wsaaVariablesInner.wsaaCommType, 3)) {
			if (isEQ(agcmbchIO.getSrvcpy(),SPACES)
			|| isEQ(agcmbchIO.getScmdue(),ZERO)
			|| isGT(agcmbchIO.getSeqno(),1)) {
				return ;
			}
			else {
				itemIO.setItemitem(agcmbchIO.getSrvcpy());
			}
		}
		comlinkrec.method.set(itemIO.getItemitem());
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5644);
		itemIO.setItempfx("IT");
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			syserrrec.params.set(itemIO.getParams());
			systemError9000();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isEQ(t5644rec.subrev,SPACES)) {
			return ;
		}
		if (isEQ(wsaaVariablesInner.wsaaCommType, 1)) {
			comlinkrec.instprem.set(acmvrevIO.getOrigamt());
			comlinkrec.icommtot.set(agcmbchIO.getInitcom());
			comlinkrec.icommpd.set(agcmbchIO.getCompay());
			comlinkrec.icommernd.set(agcmbchIO.getComern());
		}
		else {
			comlinkrec.instprem.set(acmvldgIO.getOrigamt());
		}
		callProgram(t5644rec.subrev, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			systemError9000();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		c000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		c000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaVariablesInner.wsaaPayamnt[wsaaVariablesInner.wsaaCommType.toInt()].set(comlinkrec.payamnt);
		wsaaVariablesInner.wsaaErndamt[wsaaVariablesInner.wsaaCommType.toInt()].set(comlinkrec.erndamt);
	}

protected void rewriteAgcm10300()
	{
		rewrite10310();
	}

protected void rewrite10310()
	{
		if (isNE(agcmbchIO.getBascpy(),SPACES)) {
			setPrecision(agcmbchIO.getCompay(), 2);
			agcmbchIO.setCompay(add(agcmbchIO.getCompay(), wsaaVariablesInner.wsaaPayamnt[1]));
			setPrecision(agcmbchIO.getComern(), 2);
			agcmbchIO.setComern(add(agcmbchIO.getComern(), wsaaVariablesInner.wsaaErndamt[1]));
		}
		if (isNE(agcmbchIO.getRnwcpy(),SPACES)) {
			setPrecision(agcmbchIO.getRnlcdue(), 2);
			agcmbchIO.setRnlcdue(sub(agcmbchIO.getRnlcdue(), wsaaVariablesInner.wsaaPayamnt[2]));
			setPrecision(agcmbchIO.getRnlcearn(), 2);
			agcmbchIO.setRnlcearn(sub(agcmbchIO.getRnlcearn(), wsaaVariablesInner.wsaaErndamt[2]));
		}
		if (isNE(agcmbchIO.getSrvcpy(),SPACES)) {
			setPrecision(agcmbchIO.getScmdue(), 2);
			agcmbchIO.setScmdue(sub(agcmbchIO.getScmdue(), wsaaVariablesInner.wsaaPayamnt[3]));
			setPrecision(agcmbchIO.getScmearn(), 2);
			agcmbchIO.setScmearn(sub(agcmbchIO.getScmearn(), wsaaVariablesInner.wsaaErndamt[3]));
		}
		agcmbchIO.setPtdate(covrbbrIO.getBenBillDate());
		agcmbchIO.setFunction(varcom.updat);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			systemError9000();
		}
	}

protected void readAcmvldg10400()
	{
		read10410();
	}

protected void read10410()
	{
		acmvldgIO.setDataKey(SPACES);
		acmvldgIO.setRldgcoy(acmvrevIO.getRldgcoy());
		acmvldgIO.setRldgacct(acmvrevIO.getTranref());
		acmvldgIO.setSacscode(wsaaVariablesInner.wsaaSacscode01);
		acmvldgIO.setSacstyp(wsaaVariablesInner.wsaaSacstype01);
		acmvldgIO.setOrigcurr(acmvrevIO.getOrigcurr());
		acmvldgIO.setTranno(acmvrevIO.getTranno());
		acmvldgIO.setFormat(formatsInner.acmvldgrec);
		acmvldgIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acmvldgIO);
		if (isNE(acmvldgIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(acmvldgIO.getParams());
			syserrrec.statuz.set(acmvldgIO.getStatuz());
			systemError9000();
		}
	}

protected void processZptn11000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc11010();
				case call11020: 
					call11020();
					writ11030();
					nextr11080();
				case exit11090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc11010()
	{
		zptnrevIO.setDataArea(SPACES);
		zptnrevIO.setChdrcoy(reverserec.company);
		zptnrevIO.setChdrnum(reverserec.chdrnum);
		zptnrevIO.setTranno(reverserec.tranno);
		zptnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zptnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zptnrevIO.setFormat(formatsInner.zptnrevrec);
	}

protected void call11020()
	{
		SmartFileCode.execute(appVars, zptnrevIO);
		if (isNE(zptnrevIO.getStatuz(),varcom.oK)
		&& isNE(zptnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zptnrevIO.getStatuz());
			syserrrec.params.set(zptnrevIO.getParams());
			systemError9000();
		}
		if (isEQ(zptnrevIO.getStatuz(),varcom.endp)
		|| isNE(zptnrevIO.getChdrcoy(),reverserec.company)
		|| isNE(zptnrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(zptnrevIO.getTranno(),reverserec.tranno)) {
			zptnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit11090);
		}
	}

protected void writ11030()
	{
		zptnIO.setDataArea(SPACES);
		zptnIO.setChdrcoy(zptnrevIO.getChdrcoy());
		zptnIO.setChdrnum(zptnrevIO.getChdrnum());
		zptnIO.setLife(zptnrevIO.getLife());
		zptnIO.setCoverage(zptnrevIO.getCoverage());
		zptnIO.setRider(zptnrevIO.getRider());
		zptnIO.setTranno(reverserec.newTranno);
		setPrecision(zptnIO.getOrigamt(), 2);
		zptnIO.setOrigamt(mult(zptnrevIO.getOrigamt(),-1));
		zptnIO.setTransCode(reverserec.batctrcde);
		zptnIO.setEffdate(zptnrevIO.getEffdate());
		zptnIO.setBillcd(zptnrevIO.getBillcd());
		zptnIO.setInstfrom(zptnrevIO.getInstfrom());
		zptnIO.setInstto(zptnrevIO.getInstto());
		zptnIO.setTrandate(datcon1rec.intDate);
		zptnIO.setZprflg(zptnrevIO.getZprflg());
		zptnIO.setFormat(formatsInner.zptnrec);
		zptnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zptnIO.getStatuz());
			syserrrec.params.set(zptnIO.getParams());
			systemError9000();
		}
	}

protected void nextr11080()
	{
		zptnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call11020);
	}

protected void processZctn12000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc12010();
				case call12020: 
					call12020();
					writ12030();
					nextr12080();
				case exit12090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc12010()
	{
		zctnrevIO.setDataArea(SPACES);
		zctnrevIO.setChdrcoy(reverserec.company);
		zctnrevIO.setChdrnum(reverserec.chdrnum);
		zctnrevIO.setTranno(reverserec.tranno);
		zctnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zptnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zctnrevIO.setFormat(formatsInner.zctnrevrec);
	}

protected void call12020()
	{
		SmartFileCode.execute(appVars, zctnrevIO);
		if (isNE(zctnrevIO.getStatuz(),varcom.oK)
		&& isNE(zctnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zctnrevIO.getStatuz());
			syserrrec.params.set(zctnrevIO.getParams());
			systemError9000();
		}
		if (isEQ(zctnrevIO.getStatuz(),varcom.endp)
		|| isNE(zctnrevIO.getChdrcoy(),reverserec.company)
		|| isNE(zctnrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(zctnrevIO.getTranno(),reverserec.tranno)) {
			zctnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit12090);
		}
	}

protected void writ12030()
	{
		zctnIO.setDataArea(SPACES);
		zctnIO.setAgntcoy(zctnrevIO.getAgntcoy());
		zctnIO.setAgntnum(zctnrevIO.getAgntnum());
		zctnIO.setChdrcoy(zctnrevIO.getChdrcoy());
		zctnIO.setChdrnum(zctnrevIO.getChdrnum());
		zctnIO.setLife(zctnrevIO.getLife());
		zctnIO.setCoverage(zctnrevIO.getCoverage());
		zctnIO.setRider(zctnrevIO.getRider());
		zctnIO.setTranno(reverserec.newTranno);
		setPrecision(zctnIO.getCommAmt(), 2);
		zctnIO.setCommAmt(mult(zctnrevIO.getCommAmt(),-1));
		setPrecision(zctnIO.getPremium(), 2);
		zctnIO.setPremium(mult(zctnrevIO.getPremium(),-1));
		zctnIO.setSplitBcomm(zctnrevIO.getSplitBcomm());
		zctnIO.setTransCode(reverserec.batctrcde);
		zctnIO.setEffdate(zctnrevIO.getEffdate());
		zctnIO.setTrandate(datcon1rec.intDate);
		zctnIO.setZprflg(zctnrevIO.getZprflg());
		zctnIO.setFormat(formatsInner.zctnrec);
		zctnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zctnIO);
		if (isNE(zctnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zctnIO.getStatuz());
			syserrrec.params.set(zctnIO.getParams());
			systemError9000();
		}
	}

protected void nextr12080()
	{
		zctnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call12020);
	}

protected void deleteTax13000()
	{
		start13000();
	}

protected void start13000()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			syserrrec.params.set(taxdrevIO.getParams());
			systemError9000();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(taxdrevIO.getStatuz(), varcom.oK)
		&& isEQ(reverserec.company, taxdrevIO.getChdrcoy())
		&& isEQ(reverserec.chdrnum, taxdrevIO.getChdrnum())
		&& isEQ(taxdrevIO.getEffdate(), reverserec.effdate1)
		&& isEQ(taxdrevIO.getTranno(), reverserec.tranno)
		&& isEQ(taxdrevIO.getBillcd(), varcom.vrcmMaxDate)) {
			taxdrevIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(taxdrevIO.getStatuz());
				syserrrec.params.set(taxdrevIO.getParams());
				systemError9000();
			}
		}
		taxdrevIO.setFunction(varcom.nextr);
	}
/*
 * Class transformed  from Data Structure WSAA-VARIABLES--INNER
 */
private static final class WsaaVariablesInner { 
		/* WSAA-VARIABLES */
	private PackedDecimalData[] wsaaPayamnt = PDInittedArray(3, 17, 2);
	private PackedDecimalData[] wsaaErndamt = PDInittedArray(3, 17, 2);
	private ZonedDecimalData wsaaCommType = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaSacscodeComm = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstypeComm = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacscodeRenl = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstypeRenl = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacscodeSvcm = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstypeSvcm = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacscodeOrcm = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstypeOrcm = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacscode01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaFmSacstype = new FixedLengthStringData(2);	//IBPLIFE-1433
	private FixedLengthStringData wsaaZcSacstype = new FixedLengthStringData(2);	//IBPLIFE-1433
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData covrbbrrec = new FixedLengthStringData(10).init("COVRBBRREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData zrstnudrec = new FixedLengthStringData(10).init("ZRSTNUDREC");
	private FixedLengthStringData zrstrec = new FixedLengthStringData(10).init("ZRSTREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData acmvldgrec = new FixedLengthStringData(10).init("ACMVLDGREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData zptnrevrec = new FixedLengthStringData(10).init("ZPTNREVREC");
	private FixedLengthStringData zctnrevrec = new FixedLengthStringData(10).init("ZCTNREVREC");
}
}
