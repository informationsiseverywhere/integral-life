/*
 * File: Pcpltxbil.java
 * Date: December 3, 2013 3:17:14 AM ICT
 * Author: CSC
 * 
 * Class transformed from PCPLTXBIL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.TaxdbilTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description............................................. .
*
*  001  Installment from date
*  002  Tax Desc   (i.e. Service Tax)
*  003  Tax Amount
*  004  Total Premium with Tax
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved. This
*  does not include 001  Installment from date which is not an
*  occurance field.
*
*  Processing.
*  -----------
*  This subroutine is used in letter printing to extract the total
*  taxes as at the contract billed to date.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Pcpltxbil extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLTXBIL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(5000).init(SPACES);
		/* Subroutine specific fields.*/
	private String wsaaTrantype = "";
	private ZonedDecimalData wsaaTotalAmt1 = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaTotalAmt2 = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaTotalTax1 = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaTotalTax2 = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaTaxDesc1 = new FixedLengthStringData(24).init(SPACES);
	private FixedLengthStringData wsaaTaxDesc2 = new FixedLengthStringData(24).init(SPACES);
	private ZonedDecimalData wsaaPremWithTax = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaPremTax = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (100, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String descrec = "DESCREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String taxdbilrec = "TAXDBILREC";
		/* TABLES */
	private static final String tr52b = "TR52B";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private TaxdbilTableDAM taxdbilIO = new TaxdbilTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcpltxbil() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		/*    IF  PCPD-IDCODE-COUNT    NOT = WSAA-LAST-IDCODE-COUNT        */
		if (isNE(letcIO.getRrn(), wsaaStoreRrn)) {
			initialise100();
			getTax200();
			formatValues400();
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		if (isEQ(wsaaCounter, ZERO)) {
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		else {
			if (isGT(pcpdatarec.fldOccur, wsaaCounter)
			|| isGT(pcpdatarec.groupOccurance, wsaaCounter)) {
				pcpdatarec.data.set(SPACES);
				pcpdatarec.dataLen.set(1);
				pcpdatarec.statuz.set(varcom.endp);
				return ;
			}
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a costant number of positions between fields.*/
		if (isEQ(pcpdatarec.fldOffset, 1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset, (mult((sub(pcpdatarec.fldOccur, 1)), wsaaConstant))));
		}
		if (isNE(strpos[offset.toInt()], NUMERIC)
		|| isNE(fldlen[offset.toInt()], NUMERIC)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		if (isEQ(strpos[offset.toInt()], 0)
		|| isEQ(fldlen[offset.toInt()], 0)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		/*  INITIALIZE                     WSAA-START-AND-LENGTH.        */
		for (offset.set(1); !(isGT(offset, 100)); offset.add(1)){
			wsaaStartAndLength[offset.toInt()].set(ZERO);
		}
		wsaaStoreRrn.set(letcIO.getRrn());
		if (isNE(pcpdatarec.groupOccurance, NUMERIC)) {
			pcpdatarec.groupOccurance.set(1);
		}
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(letcIO.getRdoccoy());
		chdrlnbIO.setChdrnum(letcIO.getRdocnum());
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError900();
		}
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		wsaaCounter.set(ZERO);
		/* Reserve the first offset for Installment From Date*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(chdrlnbIO.getSinstfrom());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			getAppVars().addDiagnostic("BOMB AT \"DATCON1\" FOR DATE : "+datcon1rec.intDate);
		}
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			datcon1rec.extDate.set(SPACES);
		}
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void getTax200()
	{
		start200();
	}

protected void start200()
	{
		taxdbilIO.setParams(SPACES);
		taxdbilIO.setTrantype("PREM");
		wsaaTrantype = "PREM";
		taxdbilIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		taxdbilIO.setChdrnum(chdrlnbIO.getChdrnum());
		taxdbilIO.setInstfrom(chdrlnbIO.getSinstfrom());
		taxdbilIO.setInstto(chdrlnbIO.getBtdate());
		taxdbilIO.setLife(SPACES);
		taxdbilIO.setCoverage(SPACES);
		taxdbilIO.setRider(SPACES);
		taxdbilIO.setPlansfx(ZERO);
		taxdbilIO.setFormat(taxdbilrec);
		taxdbilIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdbilIO.getStatuz(), varcom.endp))) {
			callTaxdbil300();
		}
		
		taxdbilIO.setParams(SPACES);
		taxdbilIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		taxdbilIO.setChdrnum(chdrlnbIO.getChdrnum());
		taxdbilIO.setInstfrom(chdrlnbIO.getSinstfrom());
		taxdbilIO.setInstto(chdrlnbIO.getBtdate());
		taxdbilIO.setLife(SPACES);
		taxdbilIO.setCoverage(SPACES);
		taxdbilIO.setRider(SPACES);
		taxdbilIO.setPlansfx(ZERO);
		taxdbilIO.setFormat(taxdbilrec);
		taxdbilIO.setFunction(varcom.begn);
		taxdbilIO.setTrantype("CNTF");
		wsaaTrantype = "CNTF";
		while ( !(isEQ(taxdbilIO.getStatuz(), varcom.endp))) {
			callTaxdbil300();
		}
		
	}

protected void callTaxdbil300()
	{
		init300();
	}

protected void init300()
	{
		SmartFileCode.execute(appVars, taxdbilIO);
		if (isNE(taxdbilIO.getStatuz(), varcom.oK)
		&& isNE(taxdbilIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdbilIO.getParams());
			syserrrec.statuz.set(taxdbilIO.getStatuz());
			fatalError900();
		}
		if (isEQ(taxdbilIO.getStatuz(), varcom.endp)
		|| isNE(taxdbilIO.getChdrcoy(), letcIO.getRdoccoy())
		|| isNE(taxdbilIO.getChdrnum(), letcIO.getRdocnum())
		|| isNE(taxdbilIO.getTrantype(), wsaaTrantype)
		|| isNE(taxdbilIO.getInstfrom(), chdrlnbIO.getSinstfrom())
		|| isNE(taxdbilIO.getInstto(), chdrlnbIO.getBtdate())) {
			taxdbilIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaTotalAmt1.add(taxdbilIO.getTaxamt(1));
		wsaaTotalAmt2.add(taxdbilIO.getTaxamt(2));
		wsaaPremWithTax.add(taxdbilIO.getBaseamt());
		if (isNE(taxdbilIO.getTxtype(1), SPACES)
		&& isEQ(wsaaTaxDesc1, SPACES)) {
			descIO.setRecKeyData(SPACES);
			descIO.setDescitem(taxdbilIO.getTxtype(1));
			descIO.setDesctabl(tr52b);
			readDesc800();
			wsaaTaxDesc1.set(descIO.getLongdesc());
		}
		if (isNE(taxdbilIO.getTxtype(2), SPACES)
		&& isEQ(wsaaTaxDesc2, SPACES)) {
			descIO.setRecKeyData(SPACES);
			descIO.setDescitem(taxdbilIO.getTxtype(2));
			descIO.setDesctabl(tr52b);
			readDesc800();
			wsaaTaxDesc2.set(descIO.getLongdesc());
		}
		taxdbilIO.setFunction(varcom.nextr);
	}

protected void formatValues400()
	{
		start400();
		format410();
		format415();
		format420();
		format435();
	}

protected void start400()
	{
		/*  The following counter is incremented by 1 per each*/
		/*  successful read of tax info .It is used to stop*/
		/*  retrieving recurring fields when there are no more*/
		/*  records.*/
		wsaaCounter.add(1);
		/*FORMAT*/
		/*  Tax Description 1.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(wsaaTaxDesc1, " ", "CHARACTERS", "  ", null));
		if (isNE(wsaaTaxDesc1, SPACES)) {
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaTaxDesc1, 1, fldlen[offset.toInt()]));
		}
	}

protected void format410()
	{
		/*  Tax amount 1.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaTotalTax1.set(wsaaTotalAmt1);
		fldlen[offset.toInt()].set(length(wsaaTotalTax1));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTotalTax1);
	}

protected void format415()
	{
		/*  Tax Description 2.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(wsaaTaxDesc2, " ", "CHARACTERS", "  ", null));
		if (isNE(wsaaTaxDesc2, SPACES)) {
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaTaxDesc2, 1, fldlen[offset.toInt()]));
		}
	}

protected void format420()
	{
		/*  Unlike most extracting subroutine (such as PCPLFLUP), now      */
		/*  WSAA-COUNTER should be incremented by 1 for every taxdesc      */
		/*  /tasamount pair.Because we used 'PERFORM UNTIL' to get         */
		/*  all taxes before and only list them one by one here.           */
		wsaaCounter.add(1);
		/*  Tax amount 2.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaTotalTax2.set(wsaaTotalAmt2);
		fldlen[offset.toInt()].set(length(wsaaTotalTax2));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTotalTax2);
	}

protected void format435()
	{
		/*  Total Prem with tax                                            */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaPremWithTax.add(wsaaTotalAmt1);
		wsaaPremWithTax.add(wsaaTotalAmt2);
		wsaaPremTax.set(wsaaPremWithTax);
		fldlen[offset.toInt()].set(length(wsaaPremTax));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremTax);
		/*EXIT*/
	}

protected void readDesc800()
	{
		start800();
	}

protected void start800()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(letcIO.getRdoccoy());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError900();
		}
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES)
		|| isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
