package com.csc.life.contractservicing.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList; //ILIFE-8786
import java.util.List; //ILIFE-8786

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.PcdtpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Pcdtpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;

import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PcdtpfDAOImpl extends BaseDAOImpl<Pcdtpf> implements PcdtpfDAO {
	
 private static final Logger LOGGER = LoggerFactory.getLogger(PcdtpfDAOImpl.class);
 
 public Pcdtpf getPcdtmjaByKey(Pcdtpf pcdtpf){
	  StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PLNSFX, LIFE, COVERAGE, RIDER, TRANNO, AGNTNUM01, AGNTNUM02, AGNTNUM03, AGNTNUM04,");
	  sb.append(" AGNTNUM05, AGNTNUM06, AGNTNUM07, AGNTNUM08, AGNTNUM09, AGNTNUM10, SPLITC01, SPLITC02, SPLITC03, SPLITC04, SPLITC05, SPLITC06, SPLITC07, SPLITC08,");
	  sb.append(" SPLITC09, SPLITC10, INSTPREM, USRPRF, JOBNM FROM PCDTPF WHERE CHDRCOY=? AND CHDRNUM=? AND PLNSFX=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND TRANNO>=? ");
	  sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, PLNSFX ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, TRANNO ASC, UNIQUE_NUMBER DESC");
		
	  PreparedStatement ps = getPrepareStatement(sb.toString());
      ResultSet rs = null;	
      
      Pcdtpf result = null;
      
      try {
    	  ps.setString(1, pcdtpf.getChdrcoy());
    	  ps.setString(2, pcdtpf.getChdrnum());
    	  ps.setInt(3, pcdtpf.getPlnsfx());
    	  ps.setString(4, pcdtpf.getLife());
    	  ps.setString(5, pcdtpf.getCoverage());
    	  ps.setString(6, pcdtpf.getRider());
    	  ps.setInt(7, pcdtpf.getTranno());
    	  
    	  rs = ps.executeQuery();
    	  
    	  if(rs.next()) {
    		  result = new Pcdtpf();
    		  result.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
    		  result.setChdrcoy(rs.getString("CHDRCOY"));
    		  result.setChdrnum(rs.getString("CHDRNUM"));
    		  result.setPlnsfx(rs.getInt("PLNSFX"));
    		  result.setLife(rs.getString("LIFE"));
    		  result.setCoverage(rs.getString("COVERAGE"));
    		  result.setRider(rs.getString("RIDER"));
    		  result.setTranno(rs.getInt("TRANNO"));
    		  result.setAgntnum01(rs.getString("AGNTNUM01"));
    		  result.setAgntnum02(rs.getString("AGNTNUM02"));
    		  result.setAgntnum03(rs.getString("AGNTNUM03"));
    		  result.setAgntnum04(rs.getString("AGNTNUM04"));
    		  result.setAgntnum05(rs.getString("AGNTNUM05"));
    		  result.setAgntnum06(rs.getString("AGNTNUM06"));
    		  result.setAgntnum07(rs.getString("AGNTNUM07"));
    		  result.setAgntnum08(rs.getString("AGNTNUM08"));
    		  result.setAgntnum09(rs.getString("AGNTNUM09"));
    		  result.setAgntnum10(rs.getString("AGNTNUM10"));
    		  result.setSplitc01(rs.getBigDecimal("SPLITC01"));
    		  result.setSplitc02(rs.getBigDecimal("SPLITC02"));
    		  result.setSplitc03(rs.getBigDecimal("SPLITC03"));
    		  result.setSplitc04(rs.getBigDecimal("SPLITC04"));
    		  result.setSplitc05(rs.getBigDecimal("SPLITC05"));
    		  result.setSplitc06(rs.getBigDecimal("SPLITC06"));
    		  result.setSplitc07(rs.getBigDecimal("SPLITC07"));
    		  result.setSplitc08(rs.getBigDecimal("SPLITC08"));
    		  result.setSplitc09(rs.getBigDecimal("SPLITC09"));
    		  result.setSplitc10(rs.getBigDecimal("SPLITC10"));
    		  result.setInstprem(rs.getBigDecimal("INSTPREM"));
    		  result.setUsrprf(rs.getString("USRPRF"));
    		  result.setJobnm(rs.getString("JOBNM"));
    		  
    	  }
      } catch (SQLException e) {
          LOGGER.error("getPcdtmjaByKey()", e);//IJTI-1561
          throw new SQLRuntimeException(e);
      } finally {
          close(ps, rs);
      }
      
     return result;
 }
 
 public int insertPcdtpfPcdtpf (Pcdtpf pcdtpf){
	 
	 StringBuilder sb = new StringBuilder("insert into PCDTPF(CHDRCOY, CHDRNUM, PLNSFX, LIFE, COVERAGE, RIDER, TRANNO, AGNTNUM01, AGNTNUM02, AGNTNUM03, AGNTNUM04, ");
	 sb.append(" AGNTNUM05, AGNTNUM06, AGNTNUM07, AGNTNUM08, AGNTNUM09, AGNTNUM10, SPLITC01, SPLITC02, SPLITC03, SPLITC04, SPLITC05, SPLITC06, SPLITC07, SPLITC08,");
	 sb.append(" SPLITC09, SPLITC10, INSTPREM, USRPRF, JOBNM, DATIME) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	 
	 PreparedStatement ps = getPrepareStatement(sb.toString());
     ResultSet rs = null;	
     
     int result  = 0;
     try {
    	 ps.setString(1, pcdtpf.getChdrcoy());
    	 ps.setString(2, pcdtpf.getChdrnum());
    	 ps.setInt(3, pcdtpf.getPlnsfx());
    	 ps.setString(4, pcdtpf.getLife());
    	 ps.setString(5, pcdtpf.getCoverage());
    	 ps.setString(6, pcdtpf.getRider());
    	 ps.setInt(7, pcdtpf.getTranno());
    	 ps.setString(8, pcdtpf.getAgntnum01());
    	 ps.setString(9, pcdtpf.getAgntnum02());
    	 ps.setString(10, pcdtpf.getAgntnum03());
    	 ps.setString(11, pcdtpf.getAgntnum04());
    	 ps.setString(12, pcdtpf.getAgntnum05());
    	 ps.setString(13, pcdtpf.getAgntnum06());
    	 ps.setString(14, pcdtpf.getAgntnum07());
    	 ps.setString(15, pcdtpf.getAgntnum08());
    	 ps.setString(16, pcdtpf.getAgntnum09());
    	 ps.setString(17, pcdtpf.getAgntnum10());
    	 ps.setBigDecimal(18, pcdtpf.getSplitc01());
    	 ps.setBigDecimal(19, pcdtpf.getSplitc02());
    	 ps.setBigDecimal(20, pcdtpf.getSplitc03());
    	 ps.setBigDecimal(21, pcdtpf.getSplitc04());
    	 ps.setBigDecimal(22, pcdtpf.getSplitc05());
    	 ps.setBigDecimal(23, pcdtpf.getSplitc06());
    	 ps.setBigDecimal(24, pcdtpf.getSplitc07());
    	 ps.setBigDecimal(25, pcdtpf.getSplitc08());
    	 ps.setBigDecimal(26, pcdtpf.getSplitc09());
    	 ps.setBigDecimal(27, pcdtpf.getSplitc10());
    	 ps.setBigDecimal(28, pcdtpf.getInstprem());
    	 ps.setString(29, getUsrprf());
    	 ps.setString(30, getJobnm());
    	 ps.setTimestamp(31, new Timestamp(System.currentTimeMillis()));
    	 
    	 result = ps.executeUpdate();
    	 
     } catch (SQLException e) {
         LOGGER.error("insertPcdtpfPcdtpf()", e);//IJTI-1561
         throw new SQLRuntimeException(e);
     } finally {
         close(ps, rs);
     }
	 
     return result;
 }
//ILIFE-8786 start
@Override
public List<Pcdtpf> getPcdtmja(String chdrcoy, String chdrnum, int tranno) {
		Pcdtpf pcdtpf = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		List<Pcdtpf> list = new ArrayList<>();
		
		sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PLNSFX, LIFE, COVERAGE, RIDER, TRANNO, AGNTNUM01, AGNTNUM02, AGNTNUM03, AGNTNUM04,");
		sb.append(" AGNTNUM05, AGNTNUM06, AGNTNUM07, AGNTNUM08, AGNTNUM09, AGNTNUM10, SPLITC01, SPLITC02, SPLITC03, SPLITC04, SPLITC05, SPLITC06, SPLITC07, SPLITC08,");
		sb.append(" SPLITC09, SPLITC10, INSTPREM, USRPRF, JOBNM FROM PCDTPF WHERE CHDRCOY=? AND CHDRNUM=? and TRANNO=? ");
		sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, PLNSFX ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, TRANNO ASC, UNIQUE_NUMBER DESC");
		
		ps = getPrepareStatement(sb.toString());
		try {
	   	  ps.setString(1, chdrcoy);
	   	  ps.setString(2, chdrnum);
	   	  ps.setInt(3, tranno);
   	  
	   	  rs = ps.executeQuery();
	   	  
	   	  if(rs.next()) {
	   		  pcdtpf = new Pcdtpf();
	   		  pcdtpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
	   		  pcdtpf.setChdrcoy(rs.getString("CHDRCOY"));
	   		  pcdtpf.setChdrnum(rs.getString("CHDRNUM"));
	   		  pcdtpf.setPlnsfx(rs.getInt("PLNSFX"));
	   		  pcdtpf.setLife(rs.getString("LIFE"));
	   		  pcdtpf.setCoverage(rs.getString("COVERAGE"));
	   		  pcdtpf.setRider(rs.getString("RIDER"));
	   		  pcdtpf.setTranno(rs.getInt("TRANNO"));
	   		  pcdtpf.setAgntnum01(rs.getString("AGNTNUM01"));
	   		  pcdtpf.setAgntnum02(rs.getString("AGNTNUM02"));
	   		  pcdtpf.setAgntnum03(rs.getString("AGNTNUM03"));
	   		  pcdtpf.setAgntnum04(rs.getString("AGNTNUM04"));
	   		  pcdtpf.setAgntnum05(rs.getString("AGNTNUM05"));
	   		  pcdtpf.setAgntnum06(rs.getString("AGNTNUM06"));
	   		  pcdtpf.setAgntnum07(rs.getString("AGNTNUM07"));
	   		  pcdtpf.setAgntnum08(rs.getString("AGNTNUM08"));
	   		  pcdtpf.setAgntnum09(rs.getString("AGNTNUM09"));
	   		  pcdtpf.setAgntnum10(rs.getString("AGNTNUM10"));
	   		  pcdtpf.setSplitc01(rs.getBigDecimal("SPLITC01"));
	   		  pcdtpf.setSplitc02(rs.getBigDecimal("SPLITC02"));
	   		  pcdtpf.setSplitc03(rs.getBigDecimal("SPLITC03"));
	   		  pcdtpf.setSplitc04(rs.getBigDecimal("SPLITC04"));
	   		  pcdtpf.setSplitc05(rs.getBigDecimal("SPLITC05"));
	   		  pcdtpf.setSplitc06(rs.getBigDecimal("SPLITC06"));
	   		  pcdtpf.setSplitc07(rs.getBigDecimal("SPLITC07"));
	   		  pcdtpf.setSplitc08(rs.getBigDecimal("SPLITC08"));
	   		  pcdtpf.setSplitc09(rs.getBigDecimal("SPLITC09"));
	   		  pcdtpf.setSplitc10(rs.getBigDecimal("SPLITC10"));
	   		  pcdtpf.setInstprem(rs.getBigDecimal("INSTPREM"));
	   		  pcdtpf.setUsrprf(rs.getString("USRPRF"));
	   		  pcdtpf.setJobnm(rs.getString("JOBNM"));
	   		  list.add(pcdtpf);
	   		  
	   	  }
	     } catch (SQLException e) {
	         LOGGER.error("getPcdtmja()", e);
	         throw new SQLRuntimeException(e);
	     } finally {
	         close(ps, rs);
	     }
	     
	    return list;
}

@Override
public int deletePcdtmjaRecord(List<Long> ids) {
	if (ids == null || ids.isEmpty()) {
		return 0;
	}
	try {	
		String sql = "DELETE FROM PCDTPF where UNIQUE_NUMBER in (";
		for (int i = 0; i < ids.size(); i++) {
			sql += "?";
			if ( i < ids.size() - 1) {
				sql += ",";
			}
		}
		sql += ")";
		PreparedStatement pre = getPrepareStatement(sql);
		for (int i = 0; i < ids.size(); i++) {
			pre.setLong(i + 1, ids.get(i));
		}
		return pre.executeUpdate();
	} catch (SQLException e) {
		LOGGER.error("deletePcdtmjaRecord()", e);
		throw new SQLRuntimeException(e);
	}
} 
}
//ILIFE-8786 end
