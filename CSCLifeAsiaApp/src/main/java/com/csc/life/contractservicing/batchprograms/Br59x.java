package com.csc.life.contractservicing.batchprograms;


import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CocontpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Cocontpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Tr59xrec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UlnkpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Br59x extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Br59x");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Tr59xrec tr59xrec = new Tr59xrec();
	private static final String ivrm = "IVRM";
	private P6671par p6671par = new P6671par();
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCocontpay = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLiscpay = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaBatchTrancde = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0);
	private ZonedDecimalData wsaaOldTranno = new ZonedDecimalData(5, 0);
	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData wsaaPremCcy = new FixedLengthStringData(3).isAPartOf(wsaaTransactionRec, 21);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 24);
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaT5645Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5645Prog = new FixedLengthStringData(5).isAPartOf(wsaaT5645Item, 0);
	private FixedLengthStringData filler3 = new FixedLengthStringData(3).isAPartOf(wsaaT5645Item, 5, FILLER).init(SPACES);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	CocontpfDAO cocontpfDAO = getApplicationContext().getBean("cocontpfDAO",CocontpfDAO.class);
  	Cocontpf cocont = new Cocontpf();
  	Chdrpf chdrpf = new Chdrpf();
	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList = new ArrayList<Covrpf>();
	CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	List<Utrnpf> utrnpfList = new ArrayList<>();
	private FormatsInner formatsInner = new FormatsInner();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private String wsaaUpdateFlag = "N";
	private String wsaaProcessedFlag = "N";
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	private boolean isUpdated = false;
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Ptrnpf ptrnpf;
	private List<Ptrnpf> insertPtrnList = new ArrayList<Ptrnpf>();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private List<Chdrpf> chdrpfList = new ArrayList<Chdrpf>();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Batckey wsaaBatckey = new Batckey();
	private T5645rec t5645rec = new T5645rec();
	private DescTableDAM descIO = new DescTableDAM();
	private TablesInner tablesInner = new TablesInner();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private T6647rec t6647rec = new T6647rec();
	private T5515rec t5515rec = new T5515rec();
	private Map<String, List<Itempf>> t5515Map = null;
	private Map<String, List<Itempf>> t6647Map = null;
	private Map<String, List<Itempf>> tr59xMap = null;
	private Map<String, List<Itempf>> t5645Map = null;
	private List<Descpf>  t1688List;
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
    String szdesc="";
    UlnkpfDAO ulnkpfDAO = getApplicationContext().getBean("ulnkpfDAO",UlnkpfDAO.class);
	Map<String,BigDecimal> mapOfUlnkpf =null;
	private boolean cocontFlag = false;
	private boolean liscFlag = false;
	private Map<String, Cocontpf> cocontpfMap;
	private Iterator<Entry<String, Cocontpf>> itr;
	private Map.Entry<String, Cocontpf> entry;
	private Sftlockrec sftlockrec = new Sftlockrec();
    
	public Br59x() {
		super();
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
		}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
		}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
		}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
		}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
		}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
		}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
		}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
		}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
		}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
		}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
		}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
		}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
		}
	
	/**
	* The mainline method is the default entry point to the class
	*/
	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
			try {
					super.mainline();
				}
			catch (COBOLExitProgramException e) {
			}
	}

	
	protected void restart0900() {
		/*RESTART*/
		/*EXIT*/
	}

	
	protected void initialise1000() {
		initialise1010();
	}
	
	protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaaBatchTrancde.set(bprdIO.getAuthCode());
		wsaaTransactionDate.set(datcon1rec.intDate);
		wsaaTransactionTime.set(getCobolTime());
		varcom.vrcmTranid.set(batcdorrec.tranid);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaChdrnumFrom.set(p6671par.chdrnum);
		wsaaChdrnumTo.set(p6671par.chdrnum1);
		readCocont();
	}

	
	protected void readCocont() {
			cocontpfMap = cocontpfDAO.fetchAllCocont(wsaaCompany.toString(), p6671par.chdrnum.toString(), p6671par.chdrnum1.toString());
			if(cocontpfMap == null && cocontpfMap.isEmpty()) {
				wsspEdterror.set(varcom.endp);
			}
			else{
				itr = cocontpfMap.entrySet().iterator();
			}
	}

	protected void readFile2000() {
		if(!itr.hasNext()){
			wsspEdterror.set(varcom.endp);
			return;
		}
		readFile2010();
	}
	
	
	protected void readFile2010() {
		
		String coy = bsprIO.getCompany().toString();
        String pfx = smtpfxcpy.item.toString();
		tr59xMap = itemDAO.loadSmartTable(pfx, coy, "TR59X");
		
		entry = itr.next();
		p6671par.chdrnum.set(entry.getKey());
		
		cocont = cocontpfDAO.checkUnProcessed(wsaaCompany.toString(), p6671par.chdrnum.toString());
		
		chdrpf = chdrpfDAO.getchdrRecord(wsaaCompany.toString(), p6671par.chdrnum.toString());
		if (chdrpf != null) {
			wsaaCnttype.set(chdrpf.getCnttype().trim());
			wsaaOldTranno.set(chdrpf.getTranno());
		}
		
		if(cocont != null) {
			if("N".equals(cocont.getPrcdflag())) {
				checkTr59x();
			}else {
				wsspEdterror.set(Varcom.endp);
			}
		}else {
			wsspEdterror.set(Varcom.endp);
		}
		
		
		covrpfList = covrpfDAO.searchCovrmjaByChdrnumCoy(chdrpf.getChdrnum(), chdrpf.getChdrcoy().toString());/* IJTI-1523 */
		
		t5515Map = itemDAO.loadSmartTable(pfx, coy, "T5515");
		t6647Map = itemDAO.loadSmartTable(pfx, coy, "T6647");
		t5645Map = itemDAO.loadSmartTable(pfx, coy, "T5645");
		wsaaUpdateFlag = "N";
		
	}

	protected void checkTr59x() {

		if(tr59xMap == null || tr59xMap.isEmpty()){
			tr59xMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), wsaaCompany.toString(), "TR59X");
		}
		boolean recordFound = false;
		if (tr59xMap != null && tr59xMap.containsKey(wsaaCnttype)) {
			List<Itempf> itempfList = tr59xMap.get(wsaaCnttype);
			for (Itempf itempf : itempfList) {
				tr59xrec.tr59xrec.set(StringUtil.rawToString(itempf.getGenarea()));
					recordFound = true;
			}
		}
		if (!recordFound) {
			tr59xrec.tr59xrec.set(SPACES);
			fatalError600();
		}
		
		wsaaCocontpay.set(tr59xrec.cocontpay);
		wsaaLiscpay.set(tr59xrec.liscpay);
		
		if(isEQ(wsaaCocontpay, SPACES) && isEQ(wsaaLiscpay, SPACES)) {
			wsspEdterror.set(Varcom.endp);
		}
		
		if(isNE(wsaaCocontpay, SPACES)) {
			cocontFlag = true;
		}
		
		if(isNE(wsaaLiscpay, SPACES)) {
			liscFlag = true;
		}
	}

	protected void edit2500() {
		
		sftlockrec.function.set("LOCK");
		lockOpertion8000();
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

	
	protected void update3000() {
		if("N".equals(cocont.getPrcdflag()) && isEQ(wsaaUpdateFlag, "N")) {
			updateCocont3010();
			updateZctx3020();
			updatechdrpf();
			createAcmv();
			readWriteUtrn();
			writePtrnTransaction6000();
			sftlockrec.function.set("UNLK");
			lockOpertion8000();
		}
		//wsspEdterror.set(Varcom.endp);
		
	}

	
	protected void updateCocont3010() {
		wsaaProcessedFlag = "Y";
		isUpdated = cocontpfDAO.updateProcessedflag(wsaaCompany.toString(), p6671par.chdrnum.toString(), wsaaProcessedFlag);
		wsaaUpdateFlag = "Y";
		
	}

	protected void updateZctx3020() {
		if(cocont != null && isUpdated) {
			isUpdated = zctxpfDAO.updateCocolisc(wsaaCompany.toString(), p6671par.chdrnum.toString(), cocont.getCocontamnt(), cocont.getLiscamnt());
		}
		
		if(!isUpdated){
			cocontpfDAO.updateProcessedflag(wsaaCompany.toString(), p6671par.chdrnum.toString(), cocont.getPrcdflag());
		}
		 
	}

	
	
	protected void createAcmv() {
		lifacmvrec.function.set("PSTW");
		wsaaBatckey.set(batcdorrec.batchkey);
		lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		readT5645();
		lifacmvrec.rdocnum.set(chdrpf.getChdrnum());
		lifacmvrec.tranno.set(chdrpf.getTranno());
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.rldgcoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(cocont.getCocontamnt());
		lifacmvrec.tranref.set(chdrpf.getTranno());
		lifacmvrec.trandesc.set(szdesc);
		lifacmvrec.crate.set(ZERO);
		compute(lifacmvrec.acctamt, 10).setRounded(mult(lifacmvrec.origamt, lifacmvrec.crate));
		lifacmvrec.genlcoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.effdate.set(wsaaEffdate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(chdrpf.getChdrnum());
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec.glcode.set(t5645rec.glmap[1]);
		lifacmvrec.glsign.set(t5645rec.sign[1]);
		lifacmvrec.contot.set(t5645rec.cnttot[1]);
		
		if(cocontFlag) {
			if(isGT(cocont.getCocontamnt(), ZERO)) {
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(chdrpf.getChdrcoy(), lifacmvrec.rldgcoy)
					|| isNE(chdrpf.getChdrnum(), lifacmvrec.rdocnum)
					|| isNE(chdrpf.getTranno(), lifacmvrec.tranno)) {
						return ;
				}
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
					syserrrec.params.set(lifacmvrec.lifacmvRec);
					fatalError600();
				}
		}
		}
		
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.origamt.set(cocont.getLiscamnt());
		lifacmvrec.sacscode.set(t5645rec.sacscode[2]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[2]);
		lifacmvrec.glcode.set(t5645rec.glmap[2]);
		lifacmvrec.glsign.set(t5645rec.sign[2]);
		lifacmvrec.contot.set(t5645rec.cnttot[2]);
		
		
		if(liscFlag) {
			if(isGT(cocont.getLiscamnt(), ZERO)) {
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(chdrpf.getChdrcoy(), lifacmvrec.rldgcoy)
			|| isNE(chdrpf.getChdrnum(), lifacmvrec.rdocnum)
			|| isNE(chdrpf.getTranno(), lifacmvrec.tranno)) {
				return ;
			}
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				fatalError600();
			}
		}
		}
		
		lifacmvrec.substituteCode[6].set(covrpfList.get(0).getCrtable());
		
		if(cocontFlag && isGT(cocont.getCocontamnt(), ZERO)) {
			lifacmvrec.origamt.set(cocont.getCocontamnt());
		}
		
		if(liscFlag && isGT(cocont.getLiscamnt(), ZERO)) {
			lifacmvrec.origamt.set(cocont.getLiscamnt());
		}
		
		if(cocontFlag && liscFlag) {
			lifacmvrec.origamt.set(add(cocont.getLiscamnt() , cocont.getCocontamnt()));
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode[3]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[3]);
		lifacmvrec.glcode.set(t5645rec.glmap[3]);
		lifacmvrec.glsign.set(t5645rec.sign[3]);
		lifacmvrec.contot.set(t5645rec.cnttot[3]);
		
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(chdrpf.getChdrcoy(), lifacmvrec.rldgcoy)
		|| isNE(chdrpf.getChdrnum(), lifacmvrec.rdocnum)
		|| isNE(chdrpf.getTranno(), lifacmvrec.tranno)) {
			return ;
		}
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
		
		
	}
	
	protected void readWriteUtrn() {
		String utrnFund = " ";
		utrnpfList = utrnpfDAO.searchUtrnRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1523 */
		List<Utrnpf> utrnInsertList = new ArrayList<>();
		if(utrnpfList != null && !utrnpfList.isEmpty()) {
			for(Utrnpf utrn: utrnpfList) {
				if(isNE(utrn.getUnitVirtualFund(), SPACES)) {
					if(utrnFund.equals(utrn.getUnitVirtualFund())) {
						continue;
					}
					utrn.setTranno(chdrpf.getTranno());
					utrn.setTransactionTime(varcom.vrcmTime.toInt());
					utrn.setTransactionDate(varcom.vrcmDate.toInt());
					utrn.setUser(varcom.vrcmUser.toInt());
					utrn.setBatccoy(batcdorrec.company.toString());
					utrn.setBatcbrn(batcdorrec.branch.toString());
					utrn.setBatcactmn(batcdorrec.actmonth.toInt());
					utrn.setBatcactyr(batcdorrec.actyear.toInt());
					utrn.setBatctrcde(batcdorrec.trcde.toString());
					utrn.setBatcbatch(batcdorrec.batch.toString());
					utrn.setCnttyp(chdrpf.getCnttype());
					utrn.setCrtable(covrpfList.get(0).getCrtable());
					utrn.setCntcurr(chdrpf.getCntcurr());
					utrn.setFeedbackInd(SPACE);
					utrn.setSacscode(t5645rec.sacscode04.toString());
					utrn.setSacstyp(t5645rec.sacstype04.toString());
					utrn.setGenlcde(t5645rec.glmap04.toString());
					utrn.setSvp(new BigDecimal("1"));
					utrn.setCrComDate(new Long(covrpf.getCrrcd()));
					if(t5515Map == null || t5515Map.isEmpty()){
						t5515Map = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), wsaaCompany.toString(), "T5515");
					}
					boolean foundFlag = false;
					if (t5515Map != null && t5515Map.containsKey(utrn.getUnitVirtualFund())) {
						List<Itempf> itempfList = t5515Map.get(utrn.getUnitVirtualFund());
						for (Itempf itempf : itempfList) {
								t5515rec.t5515Rec.set(StringUtil.rawToString(itempf.getGenarea()));
								foundFlag = true;
						}
					}
					if (!foundFlag) {
						t5515rec.t5515Rec.set(SPACES);
					}
					
					utrn.setFundCurrency(t5515rec.currcode.toString());
					if(t6647Map == null || t6647Map.isEmpty()){
						t6647Map = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), wsaaCompany.toString(), "T6647");
					}
					boolean foundFlag1 = false;
					if (t6647Map != null && t6647Map.containsKey(batcdorrec.trcde.toString().trim()+chdrpf.getCnttype().trim())) {
						List<Itempf> itempfList = t6647Map.get(batcdorrec.trcde.toString().trim()+chdrpf.getCnttype().trim());
						for (Itempf itempf : itempfList) {
							t6647rec.t6647Rec.set(StringUtil.rawToString(itempf.getGenarea()));
								foundFlag1 = true;
						}
					}
					if (!foundFlag1) {
						t6647rec.t6647Rec.set(SPACES);
					}
					
					if (isLT(utrn.getContractAmount(),0)) {
						utrn.setNowDeferInd(t6647rec.dealin.toString());
					}
					else {
						utrn.setNowDeferInd(t6647rec.aloind.toString());
					}
					if (isNE(covrpfList.get(0).getReserveUnitsDate(),ZERO) && isNE(covrpfList.get(0).getReserveUnitsDate(),varcom.vrcmMaxDate)) {
						utrn.setMoniesDate(new Long(covrpfList.get(0).getReserveUnitsDate()));
					}
					datcon1rec.function.set(varcom.tday);
					Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
					wsaaMoniesDate.set(datcon1rec.intDate);
					if (isEQ(t6647rec.efdcode,"DD")) {
						utrn.setMoniesDate(new Long(chdrpf.getOccdate()));
					}
					if (isEQ(t6647rec.efdcode,"LO")) {
						if (isGT(chdrpf.getOccdate(),wsaaMoniesDate)) {
							utrn.setMoniesDate(new Long(chdrpf.getOccdate()));
						}
					}
					else {
						utrn.setMoniesDate(wsaaMoniesDate.toLong());
					}
					utrn.setProcSeqNo(t6647rec.procSeqNo.toInt());
					utrn.setUstmno(0);
					utrn.setNofUnits(new BigDecimal(0));
					utrn.setNofDunits(new BigDecimal(0));
					mapOfUlnkpf = getUlnkpfRecord();
					BigDecimal disamnt = new BigDecimal(0);
					if(mapOfUlnkpf!=null && mapOfUlnkpf.size() > 0){
						BigDecimal ualprc = mapOfUlnkpf.get(utrn.getUnitVirtualFund());
						
						disamnt = (div((mult(lifacmvrec.origamt.toString(),ualprc)),100)).getbigdata();
					
					}
					utrn.setContractAmount(disamnt);
					utrn.setFundAmount(disamnt);
					utrn.setInciprm01(disamnt);
					utrnFund = utrn.getUnitVirtualFund();
					utrnInsertList.add(utrn);
					
				}
			}
		}
		utrnpfDAO.insertUtrnRecoed(utrnInsertList);
	}
	
	private Map<String,BigDecimal> getUlnkpfRecord(){
		
		Ulnkpf ulnkpf = new Ulnkpf();
		ulnkpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
		ulnkpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		List<Ulnkpf> ulnkpfList = ulnkpfDAO.getUlnkpfByChdrnum(ulnkpf);
		Map<String,BigDecimal> map = new HashMap<>();
		if(ulnkpfList!=null && !ulnkpfList.isEmpty()){
			ulnkpf = ulnkpfList.get(0);
			map.put(ulnkpf.getUnitAllocFund01(), ulnkpf.getUnitAllocPercAmt01());
			map.put(ulnkpf.getUnitAllocFund02(), ulnkpf.getUnitAllocPercAmt02());	
			map.put(ulnkpf.getUnitAllocFund03(), ulnkpf.getUnitAllocPercAmt03());	
			map.put(ulnkpf.getUnitAllocFund04(), ulnkpf.getUnitAllocPercAmt04());	
			map.put(ulnkpf.getUnitAllocFund05(), ulnkpf.getUnitAllocPercAmt05());	
			map.put(ulnkpf.getUnitAllocFund06(), ulnkpf.getUnitAllocPercAmt06());	
			map.put(ulnkpf.getUnitAllocFund07(), ulnkpf.getUnitAllocPercAmt07());	
			map.put(ulnkpf.getUnitAllocFund08(), ulnkpf.getUnitAllocPercAmt08());	
			map.put(ulnkpf.getUnitAllocFund09(), ulnkpf.getUnitAllocPercAmt09());	
			map.put(ulnkpf.getUnitAllocFund10(), ulnkpf.getUnitAllocPercAmt10());	
		}
		
		return map;
	}

	protected void readT5645() {
		
		
		if(t5645Map == null || t5645Map.isEmpty()){
			t5645Map = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), wsaaCompany.toString(), "T5645");
		}
		boolean foundFlag = false;
		if (t5645Map != null && t5645Map.containsKey("BR59X")) {
			List<Itempf> itempfList = t5645Map.get("BR59X");
			for (Itempf itempf : itempfList) {
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					foundFlag = true;
			}
		}
		if (!foundFlag) {
			t5645rec.t5645Rec.set(SPACES);
			fatalError600();
		}
		
		t1688List = descDAO.getItemByDescItem(smtpfxcpy.item.toString(), wsaaCompany.toString(), "T1688", "BAPH", bsscIO.language.toString());
		if (!t1688List.isEmpty()) {
			szdesc = t1688List.get(0).getLongdesc();
		}
	}
		


	protected void updatechdrpf() {
		
		List<Chdrpf> chdrpfList = new ArrayList<Chdrpf>();
		chdrpf.setChdrcoy(chdrpf.getChdrcoy());
		chdrpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
		chdrpf.setTranno(add(chdrpf.getTranno(),1).toInt());
		chdrpfList.add(chdrpf);
		if(chdrpfList != null && chdrpfList.size() > 0 ){
			chdrpfDAO.updateChdrTranno(chdrpfList);
			chdrpfList.clear();
			chdrpfList = null;
		}
		wsaaNewTranno.set(chdrpf.getTranno());
	}

	
	
	protected void writePtrnTransaction6000() {	
		Gettoday();
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setTranno(wsaaNewTranno.toInt());
		ptrnpf.setPtrneff(wsaaEffdate.toInt());
		/* MOVE WSAA-EFFECTIVE-DATE TO ptrnpf-DATESUB. <LA5184> */
		ptrnpf.setDatesub(wsaaToday.toInt());
		ptrnpf.setChdrpfx(chdrpf.getChdrpfx());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setValidflag("1");
		ptrnpf.setPrtflg(" ");
		ptrnpf.setRecode(chdrpf.getRecode());
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		ptrnpf.setUserT(wsaaUser.toInt());
		ptrnpf.setTermid(wsaaTermid.toString());
		insertPtrnList.add(ptrnpf);
	}

	protected void Gettoday() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}
	
	protected void lockOpertion8000() {
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(ZERO);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	protected void commit3500() {
		if (insertPtrnList.size()> 0){
			ptrnpfDAO.insertPtrnPF(insertPtrnList);
			}
		
	}

	
	protected void close4000() {
		/*CLOSE-FILES*/
		if (insertPtrnList  != null) {
			insertPtrnList.clear();
        }
		
		if (t1688List != null) {
			t1688List.clear();
		}
		
		if (utrnpfList != null) {
			utrnpfList.clear();
		}
		
		if (covrpfList != null) {
			covrpfList.clear();
		}
		
		if (tr59xMap != null) {
			tr59xMap.clear();
		}
		
		if (t5515Map != null) {
			t5515Map.clear();
		}
		
		if (t6647Map != null) {
			t6647Map.clear();
		}
		
		if (t5645Map != null) {
			t5645Map.clear();
		}
		
		
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

	
	protected void rollback3600() {
		
		/*ROLL*/
		/*EXIT*/
	}
	
	private static final class FormatsInner {
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	}
	
	private static final class TablesInner {
		private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
		private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	}
	
	
}
