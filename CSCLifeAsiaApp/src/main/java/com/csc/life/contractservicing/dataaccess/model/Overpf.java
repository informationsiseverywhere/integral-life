package com.csc.life.contractservicing.dataaccess.model;

import java.util.Date;

public class Overpf {
	
	private Long unique_number; 
	private String chdrcoy;
	private String cnttype;
	private String chdrnum;
	private String validflag;
	private String overdueflag;
	private int effectivedate;
	private int enddate;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private Date created_at;

	public Overpf() {
		
	}

	public Overpf(Overpf overpf){
		this.unique_number=overpf.unique_number;
		this.chdrcoy=overpf.chdrcoy;
		this.cnttype=overpf.cnttype;
		this.chdrcoy=overpf.chdrcoy;
		this.validflag=overpf.validflag;
		this.overdueflag=overpf.overdueflag;
		this.effectivedate=overpf.effectivedate;
		this.enddate=overpf.enddate;
		this.usrprf=overpf.usrprf;
		this.jobnm=overpf.jobnm;
		this.datime=overpf.datime;
		this.created_at=overpf.created_at;
		
	}	
	
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getOverdueflag() {
		return overdueflag;
	}
	public void setOverdueflag(String overdueflag) {
		this.overdueflag = overdueflag;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		this.datime = datime;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public int getEffectivedate() {
		return effectivedate;
	}

	public void setEffectivedate(int effectivedate) {
		this.effectivedate = effectivedate;
	}

	public int getEnddate() {
		return enddate;
	}

	public void setEnddate(int enddate) {
		this.enddate = enddate;
	}

	public Long getUnique_number() {
		return unique_number;
	}

	public void setUnique_number(Long unique_number) {
		this.unique_number = unique_number;
	}
	
}