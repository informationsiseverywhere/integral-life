/*
 * File: Intcalc.java
 * Date: 29 August 2009 22:57:20
 * Author: Quipoz Limited
 * 
 * Class transformed from INTCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.AcmvlonTableDAM;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.recordstructures.Loandtarec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*           INTEREST CALCULATION SUBROUTINE.
*           --------------------------------
* Overview.
* ---------
*
* This Subroutine is called to calculate Interest due on a given
*  Loan between 2 specified dates passed in the INTCALC parameter
*  area. The interest is calculated in the Loan Currency and
*  returned in the Loan currency to the calling program.
*
*
*Given:
*      Loan no, Company, contract no, interest from date,
*      interest to date, loan prin at last capn date,
*      last loan capn date, loan commencement date, contract type
*
*Gives:
*      interest amount
*
*Functionality.
*--------------
*Read T6633 Loan methods table using the loan commencement date
* in the ITMFRM parameter.
*
*Check if there is a Fixed rate period or not....
*                                  - T6633-GTDFIXPRD = Y or N
*
*If there is,
*     check whether the interest to date is inside this period.
*If there is & it is,
*            use T6633-INT-RATE from this T6633 record.
*If there isn't OR it is outside,
*            REREAD T6633 using effective date as ITMFRM.
*
*We now have an interest rate.
*                                            (last cap loan amt)
*1) Calculate interest for period on the given loan principal
*    amount at last capitalisation date....
*    where the 'period' is between the from & to dates specified
*
*    Loan principal * T6633-int-rate * ( date diff / 365 )
*
*2) Check whether Loan principal has changed since the interest
*    from date ......
*
*  Now look for any changes to the Loan Principal subaccount made
*   since the interest from date and calculate any interest
*   accrued over the period from when the change to the principal
*   was made up to today ( effective date ) - it may be an
*   increase or a decrease.....
*   Do a BEGN on ACMV file - using the ACMVLON logical file,
*    the Loan principal ACMVS are to be processed......
*
*    The logical view ACMVLON is used to read the ACMV file so
*     that it can be read with the Rldgacct field set as the
*     Contract number//Loan number
*
*    Get 1st ACMVLON record
*
*        IF acmv effdate > interest to date,
*           ignore ACMVLON and do NEXTR
*
*        IF acmv effdate < interest from date,
*           ignore ACMVLON and do NEXTR
*        ENDIF
*
*        set datcon3 date 1 to acmv-effdate
*        set datcon3 date 2 to interest to date
*
*        Call DATCON3 to no days between 2 dates
*
*        Calculate interest on ACMVLON amount over above No days
*
*        Interest =
*          ACMVLON-amount * T6633int-rate * (datcon3 days / 365)
*
*        Make sure interest in the the Loan currency
*
*        Add Interest to running total
*
*        Get NEXTR ACMVLON record
*
*
* TABLES USED
* -----------
*
* T6633   -   Loan Interest Rules
* T5645   -   Transaction Accounting Rules
*
* NOTE.                                                  !!!!!
*------
*      ALL VALUES CALCULATED & RETURNED FROM THIS PROGRAM ARE
*       IN LOAN CURRENCY.
*          ----
*
*
*
*****************************************************************
* </pre>
*/
public class Instintcalc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "INTCALC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData wsaaLoanPrinInt = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotalInterest = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaInterest = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaLoanCurrVal = new PackedDecimalData(17, 2);
	protected FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaSign = new FixedLengthStringData(1);

	protected FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	protected FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);

	protected FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	protected FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	protected ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private int wsaaMaxDate = 99999999;
		/* ERRORS */
	private String e723 = "E723";
		/* TABLES */
	protected String t6633 = "T6633";
	private String t5645 = "T5645";
		/* FORMATS */
	private String acmvlonrec = "ACMVLONREC";
	private String itdmrec = "ITEMREC";
	private String itemrec = "ITEMREC";
		/*Loans ACMV enquiry logical file*/
	private AcmvlonTableDAM acmvlonIO = new AcmvlonTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	protected Datcon3rec datcon3rec = new Datcon3rec();
	protected Intcalcrec intcalcrec = getIntcalcrec();
	protected Loandtarec loandtarec = new Loandtarec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	protected T5645rec t5645rec = new T5645rec();
	protected T6633rec t6633rec = getT6633rec();
	protected Varcom varcom = new Varcom();

	protected ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected List<Itempf> itempfList;
	protected Itempf itempf = null;
	
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	protected List<Acmvpf> acmvpfList;
	private Acmvpf acmvpf = null;
	private ExternalisedRules er = new ExternalisedRules();

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		readNextAcmv2270, 
		exit2290, 
		exit99490, 
		exit99590
	}

	public Instintcalc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		intcalcrec.intcalcRec = convertAndSetParam(intcalcrec.intcalcRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		control100();
		exit190();
	}

protected void control100()
	{
		initialise1000();
		mainProcessing2000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		try {
			start1000();
		}
		catch (GOTOException e){
		}
	}

protected void start1000()
	{
		wsaaSacscode.set(SPACES);
		wsaaSacstype.set(SPACES);
		wsaaSign.set(SPACES);
		intcalcrec.statuz.set(varcom.oK);
		readT56451300();						
		wsaaT6633Cnttype.set(intcalcrec.cnttype);
		wsaaT6633Type.set(intcalcrec.loanType);		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemtabl(t6633);
		itempf.setItemcoy(intcalcrec.chdrcoy.toString());
		itempf.setItemitem(wsaaT6633Key.toString());
		itempf.setItmfrm(intcalcrec.loanStartDate.getbigdata());
		itempf.setItmto(intcalcrec.loanStartDate.getbigdata());
		
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
					t6633rec.t6633Rec.set(StringUtil.rawToString(it.getGenarea()));
				
			}
		}
		if (isEQ(t6633rec.inttype,"F")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(intcalcrec.loanStartDate);
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(t6633rec.mperiod);
			callDatcon21200();
			if (isLT(intcalcrec.interestTo,datcon2rec.intDate2)
			|| isEQ(intcalcrec.interestTo,datcon2rec.intDate2)) {
				goTo(GotoLabel.exit1090);
			}
		}
	
		itempf.setItempfx("IT");
		itempf.setItemcoy(intcalcrec.chdrcoy.toString());
		itempf.setItemitem(wsaaT6633Key.toString());
		itempf.setItemtabl(t6633);
		itempf.setItmfrm(intcalcrec.interestTo.getbigdata());
		itempf.setItmto(intcalcrec.interestTo.getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
					t6633rec.t6633Rec.set(StringUtil.rawToString(it.getGenarea()));
				
			}
		} 
	}

protected void readT66331100()
	{
		start1100();
	}

protected void start1100()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		if (isNE(itdmIO.getItemcoy(),intcalcrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6633)
		|| isNE(itdmIO.getItemitem(),wsaaT6633Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT6633Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e723);
			databaseError99500();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
	}

protected void callDatcon21200()
	{
		/*START*/
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError99000();
		}
		/*EXIT*/
	}

protected void readT56451300()
	{
		start1300();
	}

protected void start1300()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(intcalcrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void mainProcessing2000()
	{
		start2000();
	}

protected void start2000()
	{
		lastCapnAmtInt2100();
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(intcalcrec.chdrnum);
		wsaaRldgLoanno.set(intcalcrec.loanNumber);

	if (isEQ(intcalcrec.loanType,"P")){
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstype.set(t5645rec.sacstype01);
			wsaaSign.set(t5645rec.sign01);
		}
		else if (isEQ(intcalcrec.loanType,"A")){
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstype.set(t5645rec.sacstype02);
			wsaaSign.set(t5645rec.sign02);
		}
		else if (isEQ(intcalcrec.loanType,"E")){
			wsaaSacscode.set(t5645rec.sacscode03);
			wsaaSacstype.set(t5645rec.sacstype03);
			wsaaSign.set(t5645rec.sign03);
		}
		else if (isEQ(intcalcrec.loanType,"D")){
			wsaaSacscode.set(t5645rec.sacscode04);
			wsaaSacstype.set(t5645rec.sacstype04);
			wsaaSign.set(t5645rec.sign04);
		}		
		
		acmvpfList = acmvpfDAO.loadDataByBatch(wsaaRldgacct.toString(), wsaaSacscode.toString(), wsaaSacstype.toString(), intcalcrec.lastCaplsnDate.toInt(), intcalcrec.chdrcoy.toString());
	
		int i=0;
		if(acmvpfList != null && acmvpfList.size() > 0)
		{
			for(Acmvpf acmv : acmvpfList)
			{
				i++;
				datcon3rec.datcon3Rec.set(SPACES);
				if (isLT(acmv.getEffdate(),intcalcrec.interestFrom)) {
					datcon3rec.intDate1.set(intcalcrec.interestFrom);
				}
				else {
					datcon3rec.intDate1.set(acmv.getEffdate());
				}
				datcon3rec.intDate2.set(intcalcrec.interestTo);
				datcon3rec.frequency.set("DY");
				callDatcon32400();
				wsaaInterest.set(ZERO);
				wsaaLoanCurrVal.set(ZERO);
				if (isNE(intcalcrec.loanCurrency,acmv.getOrigcurr())) {
					currencyConvert2300();
				}
				else {
					wsaaLoanCurrVal.set(acmv.getOrigamt());
				}
               //#ILIFE-5744 LIFE VPMS Externalization - Code promotion for Policy Loans Calculation for TEN product Start
				loandtarec.loanType.set(intcalcrec.loanType);
				loandtarec.loanNumber.set(intcalcrec.loanNumber);
				loandtarec.loanCurrency.set(intcalcrec.loanCurrency);
				if (isEQ(i, 1)) {
					loandtarec.loanSign01.set(wsaaSign);
					loandtarec.loanStartDate01.set(intcalcrec.loanStartDate);
					loandtarec.interestFrom01.set(datcon3rec.intDate1);
					loandtarec.interestTo01.set(datcon3rec.intDate2);
					loandtarec.loanorigam01.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 2)) {
					loandtarec.loanStartDate02.set(intcalcrec.loanStartDate);
					loandtarec.loanSign02.set(wsaaSign);
					loandtarec.interestFrom02.set(datcon3rec.intDate1);
					loandtarec.interestTo02.set(datcon3rec.intDate2);
					loandtarec.loanorigam02.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 3)) {
					loandtarec.loanStartDate03.set(intcalcrec.loanStartDate);
					loandtarec.loanSign03.set(wsaaSign);
					loandtarec.interestFrom03.set(datcon3rec.intDate1);
					loandtarec.interestTo03.set(datcon3rec.intDate2);
					loandtarec.loanorigam03.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 4)) {
					loandtarec.loanStartDate04.set(intcalcrec.loanStartDate);
					loandtarec.loanSign04.set(wsaaSign);
					loandtarec.interestFrom04.set(datcon3rec.intDate1);
					loandtarec.interestTo04.set(datcon3rec.intDate2);
					loandtarec.loanorigam04.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 5)) {
					loandtarec.loanStartDate05.set(intcalcrec.loanStartDate);
					loandtarec.loanSign05.set(wsaaSign);
					loandtarec.interestFrom05.set(datcon3rec.intDate1);
					loandtarec.interestTo05.set(datcon3rec.intDate2);
					loandtarec.loanorigam05.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 6)) {
					loandtarec.loanStartDate06.set(intcalcrec.loanStartDate);
					loandtarec.loanSign06.set(wsaaSign);
					loandtarec.interestFrom06.set(datcon3rec.intDate1);
					loandtarec.interestTo06.set(datcon3rec.intDate1);
					loandtarec.loanorigam06.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 7)) {
					loandtarec.loanStartDate07.set(intcalcrec.loanStartDate);
					loandtarec.loanSign07.set(wsaaSign);
					loandtarec.interestFrom07.set(datcon3rec.intDate1);
					loandtarec.interestTo07.set(datcon3rec.intDate2);
					loandtarec.loanorigam07.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 8)) {
					loandtarec.loanStartDate08.set(intcalcrec.loanStartDate);
					loandtarec.loanSign08.set(wsaaSign);
					loandtarec.interestFrom08.set(datcon3rec.intDate1);
					loandtarec.interestTo08.set(datcon3rec.intDate2);
					loandtarec.loanorigam08.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 9)) {
					loandtarec.loanStartDate09.set(intcalcrec.loanStartDate);
					loandtarec.loanSign09.set(wsaaSign);
					loandtarec.interestFrom09.set(datcon3rec.intDate1);
					loandtarec.interestTo09.set(datcon3rec.intDate2);
					loandtarec.loanorigam09.set(wsaaLoanCurrVal);
				} else if (isEQ(i, 10)) {
					loandtarec.loanStartDate10.set(intcalcrec.loanStartDate);
					loandtarec.loanSign10.set(wsaaSign);
					loandtarec.interestFrom10.set(datcon3rec.intDate1);
					loandtarec.interestTo10.set(datcon3rec.intDate2);
					loandtarec.loanorigam10.set(wsaaLoanCurrVal);
				}
				//ILIFE-5934
				//ILIFE-6172
				//if (!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMPOLICYLOAN") && er.isExternalized(intcalcrec.cnttype.toString(), null) && isEQ(intcalcrec.loanCurrency,"SGD") && (isEQ(intcalcrec.cnttype,"TEN") || isEQ(intcalcrec.cnttype,"TWL")))) {
					compute(wsaaInterest, 6).setRounded(mult(mult(wsaaLoanCurrVal, (div(t6633rec.intRate, 100))),
							(div(datcon3rec.freqFactor, 365))));
					if (isEQ(acmv.getGlsign(), wsaaSign)) {
						wsaaTotalInterest.add(wsaaInterest);
					} else {
						wsaaTotalInterest.subtract(wsaaInterest);
					}
				//}
			}
		}
		loandtarec.nofloan.set(i);
		//ILIFE-5934
		//ILIFE-6172
		/*if (AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMPOLICYLOAN") && er.isExternalized(intcalcrec.cnttype.toString(), null) && isEQ(intcalcrec.loanCurrency,"SGD") && (isEQ(intcalcrec.cnttype,"TEN") || isEQ(intcalcrec.cnttype,"TWL"))) {
			callProgram("VPMPOLICYLOAN", instintcalcrec, loandtarec);
			wsaaTotalInterest.add(instintcalcrec.interestAmount);

		}*/
		intcalcrec.interestAmount.set(wsaaTotalInterest);
	}
    //#ILIFE-5744 LIFE VPMS Externalization - Code promotion for Policy Loans Calculation for TEN product end
protected void lastCapnAmtInt2100()
	{
		/*START*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(intcalcrec.interestFrom);
		datcon3rec.intDate2.set(intcalcrec.interestTo);
		datcon3rec.frequency.set("DY");
		callDatcon32400();
		wsaaTotalInterest.set(ZERO);
		wsaaLoanPrinInt.set(ZERO);
		//compute(wsaaLoanPrinInt, 6).setRounded(mult(mult(intcalcrec.loanorigam,(div(t6633rec.intRate,100))),(div(datcon3rec.freqFactor,365))));
		wsaaTotalInterest.add(wsaaLoanPrinInt);
		/*EXIT*/
	}

protected void currencyConvert2300()
	{
		start2300();
	}

protected void start2300()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountIn.set(acmvlonIO.getOrigamt());
		conlinkrec.statuz.set(SPACES);
		conlinkrec.function.set("CVRT");
		conlinkrec.currIn.set(acmvlonIO.getOrigcurr());
		conlinkrec.cashdate.set(wsaaMaxDate);
		conlinkrec.currOut.set(intcalcrec.loanCurrency);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(intcalcrec.chdrcoy);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			systemError99000();
		}
		else {
			wsaaLoanCurrVal.set(conlinkrec.amountOut);
		}
	}

protected void callDatcon32400()
	{
		/*START*/
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError99000();
		}
		/*EXIT*/
	}

protected void systemError99000()
	{
		try {
			start99000();
		}
		catch (GOTOException e){
		}
		finally{
			exit99490();
		}
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
	intcalcrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		try {
			start99500();
		}
		catch (GOTOException e){
		}
		finally{
			exit99590();
		}
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
	intcalcrec.statuz.set(varcom.bomb);
		exit190();
	}

protected T6633rec getT6633rec() {
	return new T6633rec() ;
}

protected Intcalcrec getIntcalcrec() {
	return new Intcalcrec();
	}
}
