/*
 * File: Pr51m.java
 * Date: 30 August 2009 1:37:11
 * Author: Quipoz Limited
 *
 * Class transformed from PR51M.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.csc.diary.procedures.Dryproces;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PhrtTableDAM;
import com.csc.life.contractservicing.dataaccess.PrmhactTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.contractservicing.recordstructures.Covrmjakey;
import com.csc.life.contractservicing.screens.Sr51mScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.AcblenqTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.procedures.Uwlmtchk;
import com.csc.life.enquiries.recordstructures.Uwlmtrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.procedures.Hcrtfup;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Hcrtfuprec;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.productdefinition.tablestructures.Tr51prec;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.procedures.Csncalc;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.recordstructures.Csncalcrec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.recordstructures.Calprpmrec;
import com.csc.life.terminationclaims.recordstructures.Pr676cpy;
import com.csc.life.terminationclaims.tablestructures.Tr686rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*          Permium Holiday Reinstatement Processing
*          ----------------------------------------
*
*   This is the Premium Holiday Reinstatement Process program to
*   reinstate, or conduct PH Reinstatement Quotation for a policy
*   under premium holiday.
*
*   User is able to select component/rider that required reinstating,
*   but it is mandatory to select the contract main coverage.
*
*   For the lapsed premium paying rider, whether allow for reinstating,
*   is able to control through the new Premium Holiday product rule
*   table, TR51P's set up.
*
*   For those valid riders, the selection of reinstating is able to
*   change through the modification transaction, before issue the
*   pending Premium Holiday Reinstatement.
*
*   For PH reinstatement, the Projected PTD will be calculated base
*   on the PH reinstatement date, Billing frequency, and it must >=
*   Today's date.
*
*   Payback/Not payback arrears option is also provided, and can
*   be modified before issue the Premium Holiday Reinstatement.
*   - Payback:
*
*   The required total reinstatement amount is also calculated base
*   on the payback/not payback option.
*   The PH Reinstatement is not allowed when there is an insufficient
*   fund in the billing currency suspense account to cover the
*   outstanding premium and fee, if any.
*
*   Upon PH reinstatement issue completion, process for above 2 options
*   will be different:
*
*   - Payback option:
*     i.   Original PTD, BTD and Billing Date will be remained un-change
*          and set up for the new Validflag '1' CHDR and PAYR records
*     ii.  Component with premium method, its CURRFROM for validflag
*          '1' will be the same as the PTD on the CHDR
*
*   - No Payback option:
*     i.   The Reinstatement Date will be used to set up PTD, BTD and
*          Billing Date for the new Validflag '1' CHDR and PAYR records
*     ii.  Component with premium method, its CURRFROM for validflag
*          '1' will be the same as Reinstatement Date.
*
*   Note for Benefit Billing Rider:
*     - Those still under IF/PP status Benefit Billing riders are not
*       required.
*     - Lapse Benefit Billing Rider, upon reinstate, its CURRFROM and
*       Benefit Billing date, will be set up the same as the Reinstatemen
*       Date, regardless of payback or not, i.e. COI will be only
*       resumed start from the Reinstatement Date.
*
*Initialise
*----------
*  The  details of the contract being worked with will have been
*  stored  in  the  CHDRMJA I/O module. Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*    -  Contract Type, (CNTTYPE) - long description from T5688,
*    -  Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*    -  Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*    -  The owner's client (CLTS) details.
*    -  The joint  owner's  client (CLTS) details if they exist.
*
*    -  Payment frequency, Payment Method and Regular Premium
*       from (PAYR) details
*    -  the no. of O/S premium instalements =
*                Current PTD until Projected PTD.
*       or
*       the no. of O/S premium instalements =
*                Reinstatement date until Projected PTD.
*
*    -  Total O/S premium = Model premium *
*                           no. of O/S premium instalements.
*
*Validate.
*--------
*  If the suspense amount is not enough to cover the sum of
*  outstanding premium and reinstatement fee, a warning message
*  will be displayed.
*
*Updating
*--------
*
*  Update CHDR, with TRANNO + 1.
*  Update PTRN.
*  Update selected componet/rider into COVR.
*  Update PHRT and PRMH.
*  Release Soft Lock.
*
***********************************************************************
* </pre>
*/
public class Pr51m extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR51M");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData iz = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaHolidayTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaLastRerateDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaEarlistRerateDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaMlDob = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaJlDob = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaJlAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaWopCompRrn = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaWaiverSumins = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaForwardTranscode = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaSelect = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaStdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaRerateDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaLastAnniDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaPrjptdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaTolerance = new ZonedDecimalData(7, 2);
	private ZonedDecimalData wsaaTolerance2 = new ZonedDecimalData(7, 2);
	private PackedDecimalData wsaaLifeCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaJlifeCltdob = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaTotSusamt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaSwitchFlag = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaFreqMatch = new FixedLengthStringData(1);
	private Validator freqMatch = new Validator(wsaaFreqMatch, "Y");

	private FixedLengthStringData wsaaAgtTerminateFlag = new FixedLengthStringData(1);
	private Validator agtTerminated = new Validator(wsaaAgtTerminateFlag, "Y");
	private Validator agtNotTerminated = new Validator(wsaaAgtTerminateFlag, "N");

	private FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	private FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 4);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);

	private FixedLengthStringData wsaaOsFollowup = new FixedLengthStringData(1).init("N");
	private Validator osFollowup = new Validator(wsaaOsFollowup, "Y");
	private String wsaaOutstanflu = "";
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccdateMm = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();
	private ZonedDecimalData wsaaOccdateDd = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6).setUnsigned();

		/* WSAA-CALC-OS */
	private FixedLengthStringData[] wsaaOsDetails = FLSInittedArray (99, 42);
	private ZonedDecimalData[] wsaaOsDate = ZDArrayPartOfArrayStructure(8, 0, wsaaOsDetails, 0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaOsPremium = ZDArrayPartOfArrayStructure(17, 2, wsaaOsDetails, 8);
	private ZonedDecimalData[] wsaaOsDiscount = ZDArrayPartOfArrayStructure(17, 2, wsaaOsDetails, 25);
	private FixedLengthStringData wsaaZrwvflg01 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg02 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg03 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg04 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaJlSex = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPremiumMethod = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLifenum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaJlifenum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaUserArea = new FixedLengthStringData(768);
	private FixedLengthStringData wsaaSave1Batchkey = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaSave1Batchkey2 = new FixedLengthStringData(22);

	private FixedLengthStringData wsaaValidComponent = new FixedLengthStringData(1);
	private Validator validComponent = new Validator(wsaaValidComponent, "Y");

	private FixedLengthStringData wsaaJointLife = new FixedLengthStringData(1);
	private Validator jointLife = new Validator(wsaaJointLife, "Y");

	private FixedLengthStringData wsaaSkipUpdate = new FixedLengthStringData(1);
	private Validator skipUpdate = new Validator(wsaaSkipUpdate, "Y");

	private FixedLengthStringData wsaaRerateComp = new FixedLengthStringData(1);
	private Validator rerateComp = new Validator(wsaaRerateComp, "Y");

	private FixedLengthStringData wsaaHbnfComp = new FixedLengthStringData(1);
	private Validator hbnfComp = new Validator(wsaaHbnfComp, "Y");

	private FixedLengthStringData wsaaWaiveIt = new FixedLengthStringData(1);
	private Validator waiveIt = new Validator(wsaaWaiveIt, "Y");

	private FixedLengthStringData wsaaCtables = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaCtable = FLSArrayPartOfStructure(100, 4, wsaaCtables, 0);

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FlupTableDAM flupIO = new FlupTableDAM();
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PhrtTableDAM phrtIO = new PhrtTableDAM();
	private PrmhactTableDAM prmhactIO = new PrmhactTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Covrmjakey wsaaCovrmjakey = new Covrmjakey();
	private Covrmjakey wsaaWopCovrmjakey = new Covrmjakey();
	private Covrmjakey wsaaMainCovrmjakey = new Covrmjakey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Optswchrec optswchrec = new Optswchrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Hcrtfuprec hcrtfuprec = new Hcrtfuprec();
	private Uwlmtrec uwlmtrec = new Uwlmtrec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Csncalcrec csncalcrec = new Csncalcrec();
	private Actvresrec actvresrec = new Actvresrec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Pr676cpy pr676cpy = new Pr676cpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5645rec t5645rec = new T5645rec();
	private T5667rec t5667rec = new T5667rec();
	private T5679rec t5679rec = new T5679rec();
	private T5675rec t5675rec = new T5675rec();
	private T6661rec t6661rec = new T6661rec();
	private T5674rec t5674rec = new T5674rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T5688rec t5688rec = new T5688rec();
	private T5661rec t5661rec = new T5661rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Tr51prec tr51prec = new Tr51prec();
	private Tr686rec tr686rec = new Tr686rec();
	private T7508rec t7508rec = new T7508rec();
	private Wssplife wssplife = new Wssplife();
	private Sr51mScreenVars sv = ScreenProgram.getScreenVars( Sr51mScreenVars.class);
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();	//ILIFE-3997
	private Txcalcrec txcalcrec = new Txcalcrec();
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private Tr52drec tr52drec = new Tr52drec();
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private TablesInner tablesInner = new TablesInner();
	private T5687rec t5687rec = new T5687rec();
	private Tr52erec tr52erec = new Tr52erec();
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCcy = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private PackedDecimalData wsaaTotalTaxedPremium = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private boolean prmhldtrad = false;
	private Ta524rec ta524rec = new Ta524rec();
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private T6654rec t6654rec = new T6654rec();
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO",PrmhpfDAO.class);
	private ZonedDecimalData paidTodate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaPtd = new ZonedDecimalData(8, 0).setUnsigned();
	private List<Covrpf> covrpfList;	//ILIFE-8509
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Calprpmrec calprpmrec = new Calprpmrec();
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private Map<String, BigDecimal> proratePremMap = new HashMap<>();
	private StringBuilder key;
	private BigDecimal tempCntfee;
	private boolean lapseReinstated;
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	//PINNACLE-2368
	private static final String CSOTH019 = "CSOTH019";
	private boolean CSOTH019Permission = false;
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1190,
		checkComponent1250,
		addToSfl1270,
		exit1290,
		noMoreTr5171480,
		exit1490,
		exit1690,
		exit2490,
		subfileError2670,
		exit2690,
		nextSfl3580,
		exit3590,
		callNextSfl4020,
		exit4090,
		readSfl5120,
		nextSfl5180,
		exit5190,
		premCalcDone5280,
		setUp5285,
		backpayCalc5350,
		exit5390,
		exit5590,
		nextFslRec5620,
		exit5690,
		readSfl5720,
		cont5740,
		nextSfl5780,
		exit5790,
		exit5890,
		setUp5980,
		readSfl6120,
		nextSfl6180,
		exit6190,
		a490Exit,
		a690Exit,
		f050FuturePremium,
		f090Exit,
		f220ReadSfl,
		f280NextSfl,
		f290Exit,
		f380PremCalcDone,
		f620NextFslRec,
		f690Exit,
		f720ReadSfl,
		f740Cont,
		f780NextSfl,
		f790Exit,
		f890Exit,
		f980SetUp,
		nextr7180,
		exit7190,
		nextr7280,
		exit7290,
		exit7490,
		update7750,
		exit7790
	}

	public Pr51m() {
		super();
		screenVars = sv;
		new ScreenModel("Sr51m", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("2000");
			return ;
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		CSOTH019Permission = FeaConfg.isFeatureExist("2", CSOTH019, appVars, "IT");//PINNACLE-2368
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		wsaaCtables.set(SPACES);
		iy.set(ZERO);
		wsaaTotSusamt.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wssplife.effdate.set(wsspcomn.currfrom);
		/* Get Screen Title*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.acdes.set(descIO.getLongdesc());
		}
		else {
			sv.acdes.fill("?");
		}
		/* Initalisation the Subfile*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Initilise screen fileds*/
		sv.btdate.set(varcom.maxdate);
		sv.effdate.set(varcom.maxdate);
		sv.occdate.set(varcom.maxdate);
		sv.hprjptdate.set(varcom.maxdate);
		sv.ptdate.set(varcom.maxdate);
		sv.xamt.set(ZERO);
		sv.totalfee.set(ZERO);
		sv.cmax.set(ZERO);
		sv.reqamt.set(ZERO);
		sv.susamt.set(ZERO);
		sv.manadj.set(ZERO);
		sv.tolerance.set(ZERO);
		sv.osbal.set(ZERO);
		/*    Set up protection*/
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(wsspcomn.flag, "C")
		|| isEQ(wsspcomn.flag, "D")) {
			sv.manadjOut[varcom.pr.toInt()].set("Y");
			sv.apindOut[varcom.pr.toInt()].set("Y");
			sv.optindOut[varcom.pr.toInt()].set("Y");
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		sv.effdate.set(wsspcomn.currfrom);
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.cownnum.set(chdrmjaIO.getCownnum());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.occdate.set(chdrmjaIO.getOccdate());
		wsaaOccdate.set(chdrmjaIO.getOccdate());
		sv.billfreq.set(chdrmjaIO.getBillfreq());
		sv.mop.set(chdrmjaIO.getBillchnl());
		sv.register.set(chdrmjaIO.getRegister());
		sv.totalfee.set(chdrmjaIO.getSinstamt02());
		tempCntfee = sv.totalfee.getbigdata();	//ILIFE-8509
		/* Get Contract Descriptions*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		/* Get Contract Status Descriptions*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		else {
			sv.chdrstatus.fill("?");
		}
		/* Get Premium Status Descriptions*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.premstatus.set(descIO.getShortdesc());
		}
		else {
			sv.premstatus.fill("?");
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(chdrmjaIO.getCownpfx());
		namadrsrec.clntCompany.set(chdrmjaIO.getCowncoy());
		namadrsrec.clntNumber.set(chdrmjaIO.getCownnum());
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMF");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.ownername.set(namadrsrec.name);
		/*  Get Life name*/
		lifelnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(chdrmjaIO.getCownpfx());
		namadrsrec.clntCompany.set(chdrmjaIO.getCowncoy());
		namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMF");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		sv.lifename.set(namadrsrec.name);
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		/* Retrieve the PAYR  record.*/
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		a200CheckHbnf();
		/* Initialize AGEC-AGECALC-REC fields.*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
		agecalcrec.company.set(wsspcomn.fsuco);
		if (isEQ(wsspcomn.flag, "P")) {
			a500DefaultFollowup();
		}
		else {
			chdrlnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
			chdrlnbIO.setChdrnum(chdrmjaIO.getChdrnum());
			chdrlnbIO.setFunction(varcom.reads);
			chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
		}
		//ILIFE-8179
		prmhldtrad = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString(), "CSOTH010", appVars, "IT");
		optionSwitching1700();
		readTables1100();
		premiumSuspense1500();
		if (isEQ(sv.apind, SPACES)) {
			sv.apind.set("N");
		}
		if (isNE(wsspcomn.flag, "P")
		&& isNE(wsspcomn.flag, "Q")) {
			a000RetrvePhrt();
		}
		else {
			wsaaPtd.set(chdrmjaIO.getPtdate()); //ILIFE-8179
			a100ProjectPtdate();
		}
		premiumHolidayTranno1900();
		wsaaTotalPremium.set(0);
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			loadSubfile1200();
		}
		//ILIFE-8179
		if(prmhldtrad) {	//ILIFE-8509
			if(isNE(ta524rec.ta524Rec, SPACES)) {
				if(isLT(sv.effdate, sv.btdate)) {
					getBtPtdate();
					wsaaPtd.set(sv.ptdate);
					a100ProjectPtdate();
				}
				sv.apindOut[varcom.pr.toInt()].set("Y");
			}
		}
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES)) {
			premiumCalc5000();
			f000ProcessOsPremium();
		}
		else if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES))
			calcProratePrem();
		readTr52erec2000();
		compute(sv.cmax, 2).set(add(sv.totalfee, sv.xamt));
		compute(sv.reqamt, 2).set(add(sv.osbal, sv.manadj));
		compute(sv.reqamt, 2).set(add(sv.reqamt,wsaaTaxamt));
		scrnparams.subfileRrn.set(1);
		if (isNE(wsspcomn.flag, "D")) {
			a700CalcTolerance();
		}
		/*    When issue the PH Reinstatement, make sure there is insuffici*/
		/*    money in suspense.*/
		if (isEQ(wsspcomn.flag, "C")
		|| isEQ(wsspcomn.flag, "I")) {
			if ((setPrecision(sv.reqamt, 2)
			&& isGT(sv.reqamt, (add(sv.susamt, sv.tolerance))))) {
				scrnparams.errorCode.set(errorsInner.e961);
			}
		}
		
	}


protected void readTr52erec2000()
{
	/*if(isEQ(chdrlnbIO.getRegister(),"SGP"))
	{*/
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
	itemIO.setItemtabl(tablesInner.tr52d);
	itemIO.setItemitem(chdrlnbIO.getRegister());
	itemIO.setFormat(formatsInner.itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem("***");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}
	tr52drec.tr52dRec.set(itemIO.getGenarea());
	//}
	
	/* Read table TR52E                                                */
	itdmIO.setDataKey(SPACES);
	itdmIO.setItempfx("IT");
	itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
	itdmIO.setItemtabl(tablesInner.tr52e);
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
	wsaaTr52eCrtable.set("****");
	itdmIO.setItemitem(wsaaTr52eKey);
	itdmIO.setItmfrm(chdrlnbIO.getOccdate());
	itdmIO.setFormat(formatsInner.itemrec);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(itdmIO.getStatuz());
		fatalError600();
	}
	if (isEQ(itdmIO.getItemcoy(), chdrlnbIO.getChdrcoy())
	&& isEQ(itdmIO.getItemtabl(), tablesInner.tr52e)
	&& isEQ(itdmIO.getItemitem(), wsaaTr52eKey)
	&& isEQ(itdmIO.getStatuz(), varcom.oK)) {
		gotItem5000();
	}
	/* re-read using generic component                                */
	itdmIO.setDataKey(SPACES);
	itdmIO.setItempfx("IT");
	itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
	itdmIO.setItemtabl(tablesInner.tr52e);
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
	wsaaTr52eCrtable.set("****");
	itdmIO.setItemitem(wsaaTr52eKey);
	itdmIO.setItmfrm(chdrlnbIO.getOccdate());
	itdmIO.setFormat(formatsInner.itemrec);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(itdmIO.getStatuz());
		fatalError600();
	}
	if (isEQ(itdmIO.getItemcoy(), chdrlnbIO.getChdrcoy())
	&& isEQ(itdmIO.getItemtabl(), tablesInner.tr52e)
	&& isEQ(itdmIO.getItemitem(), wsaaTr52eKey)
	&& isEQ(itdmIO.getStatuz(), varcom.oK)) {
		gotItem5000();
	}
	/* re-read using generic contract and component                   */
	itdmIO.setDataKey(SPACES);
	itdmIO.setItempfx("IT");
	itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
	itdmIO.setItemtabl(tablesInner.tr52e);
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set("***");
	wsaaTr52eCrtable.set("****");
	itdmIO.setItemitem(wsaaTr52eKey);
	itdmIO.setItmfrm(chdrlnbIO.getOccdate());
	itdmIO.setFormat(formatsInner.itemrec);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(itdmIO.getStatuz());
		fatalError600();
	}
	if (isNE(itdmIO.getItemcoy(), chdrlnbIO.getChdrcoy())
	|| isNE(itdmIO.getItemtabl(), tablesInner.tr52e)
	|| isNE(itdmIO.getItemitem(), wsaaTr52eKey)
	|| isNE(itdmIO.getStatuz(), varcom.oK)) {
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(varcom.endp);
		fatalError600();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		a300ReadTr52e();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a300ReadTr52e();
	}
	/*    Obtain the long description from DESC using DESCIO.*/
/*	descIO.setDataArea(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(chdrlnbIO.getChdrcoy());
	descIO.setDesctabl("T5687");
	descIO.setDescitem("****");
	descIO.setLanguage("E");
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(), varcom.oK)
	&& isNE(descIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	    MOVE DESC-LONGDESC          TO WSAA-HEDLINE.                 
	if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
		wsaaHedline.fill("?");
	}
	else {
		wsaaHedline.set(descIO.getLongdesc());
	}*/
	
	calcTax();
}


				protected void gotItem5000()
				{
					tr52erec.tr52eRec.set(itdmIO.getGenarea());
				}
				
				protected void a300ReadTr52e()
				{
					List<Itempf> itempfList = new ArrayList<Itempf>();
					Itempf itempf = new Itempf();
					tr52erec.tr52eRec.set(SPACES);
					itempf.setItempfx("IT");
					itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
					itempf.setItemtabl(tablesInner.tr52e.toString());
					itempf.setItemitem(wsaaTr52eKey.toString());
					itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
					itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
					itempfList = itempfDAO.findByItemDates(itempf);
					if (itempfList.size()==0
					&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
						syserrrec.params.set(wsaaTr52eKey);
						fatalError600();
					}
					if (itempfList.size()>0) {
						tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
					}
				}

				protected void calcTax(){
					
					if (isEQ(tr52erec.taxind01, "Y")) {
						txcalcrec.function.set("CALC");
						txcalcrec.statuz.set(varcom.oK);
						txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
						txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
						txcalcrec.life.set(SPACES);
						txcalcrec.coverage.set(SPACES);
						txcalcrec.rider.set(SPACES);
						txcalcrec.planSuffix.set(ZERO);
						txcalcrec.jrnseq.set(ZERO);
						txcalcrec.crtable.set("****");
						txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
						txcalcrec.register.set(chdrlnbIO.getRegister());
						txcalcrec.taxrule.set(wsaaTr52eKey);
						wsaaRateItem.set(SPACES);
						txcalcrec.ccy.set(chdrlnbIO.getCntcurr().toString());
						wsaaCcy.set(chdrlnbIO.getCntcurr().toString());
						wsaaTxitem.set(tr52erec.txitem);
						txcalcrec.rateItem.set(wsaaRateItem);
							if (isEQ(tr52erec.zbastyp.trim(), "Y")) {	//ILIFE-8509
								if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, sv.btdate))
									compute(txcalcrec.amountIn, 2).set(add(payrIO.getProramt().toString(), ZERO));
								else
									compute(txcalcrec.amountIn, 2).set(add(chdrlnbIO.getSinstamt01().toString(), ZERO));
							}
							else {
								if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, sv.btdate))
									compute(txcalcrec.amountIn, 2).set(add(payrIO.getProramt().toString(), ZERO));
								else
									compute(txcalcrec.amountIn, 2).set(add(chdrlnbIO.getSinstamt01().toString(), ZERO));
							}
					txcalcrec.transType.set("PREM");
					txcalcrec.effdate.set(chdrlnbIO.getOccdate());
					txcalcrec.tranno.set(chdrlnbIO.getTranno());
					txcalcrec.taxType[1].set(SPACES);
					txcalcrec.taxType[2].set(SPACES);
					txcalcrec.taxAmt[1].set(ZERO);
					txcalcrec.taxAmt[2].set(ZERO);
					txcalcrec.taxAbsorb[1].set(SPACES);
					txcalcrec.taxAbsorb[2].set(SPACES);
					txcalcrec.txcode.set(tr52drec.txcode);
					if(isGT(txcalcrec.amountIn,0)){
					callProgram(tr52drec.txsubr, txcalcrec.linkRec);
					if (isNE(txcalcrec.statuz, varcom.oK) && isEQ(txcalcrec.statuz.toString().indexOf("F109"),-1) ) {
						syserrrec.statuz.set(txcalcrec.statuz);
						fatalError600();
					}
					}
					if(isEQ(txcalcrec.statuz.toString().indexOf("F109"),-1) ){
						wsaaTaxamt.set(ZERO);
					}
					 if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
						wsaaTaxamt.add(txcalcrec.taxAmt[1]);
					}
					if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
						wsaaTaxamt.add(txcalcrec.taxAmt[2]);
					}
				}
				}





protected void readTables1100()
	{
		try {
			t56451110();
			t56791120();
			if(prmhldtrad) { //ILIFE-8179
				readTa524();
			}
			else {
				ta524rec.ta524Rec.set(SPACES);
			}
			if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES))
				tr51p1130();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void t56451110()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.chdrnumErr.set(errorsInner.h134);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1190);
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void t56791120()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void tr51p1130()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr51p);
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			if(!prmhldtrad) //ILIFE-8179
				scrnparams.errorCode.set(errorsInner.rlbs);
			else
				scrnparams.errorCode.set(errorsInner.rrrh);
			return ;
		}
		tr51prec.tr51pRec.set(itemIO.getGenarea());
	}

	//ILIFE-8179
	protected void readTa524() {
		/*  Read TA524*/
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrmjaIO.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.ta524.toString());
		itempf.setItemitem(chdrmjaIO.getCnttype().toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if(itempf != null && itempf.getGenarea() != null) {
			ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			ta524rec.ta524Rec.set(SPACES);
		}
		itempf = null;
		if (BTPRO028Permission) {
			itempf = itempfDAO.findItemByItem(chdrmjaIO.getChdrcoy().toString(), tablesInner.t6654.toString(), payrIO.getBillchnl().toString().trim()+chdrmjaIO.getCnttype().toString().trim()+payrIO.getBillfreq().toString().trim());	
		}
		else {
		itempf = itempfDAO.findItemByItem(chdrmjaIO.getChdrcoy().toString(), tablesInner.t6654.toString(), payrIO.getBillchnl().toString().trim()+chdrmjaIO.getCnttype().toString());
		}
		if(itempf != null) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1210();
					covrmjaProcess1220();
				case checkComponent1250:
					checkComponent1250();
				case addToSfl1270:
					addToSfl1270();
				case exit1290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1210()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
	}

protected void covrmjaProcess1220()
	{
		sv.subfileArea.set(SPACES);
		sv.sumins.set(ZERO);
		sv.instprem.set(ZERO);
		sv.ind.set(SPACES);
		sv.rerateDate.set(varcom.vrcmMaxDate);
		sv.origSum.set(ZERO);
		sv.singp.set(ZERO);
		sv.rtrnwfreq.set(ZERO);
		sv.bftpaym.set("N");
		sv.premind.set("N");
		readT56871300();
		readTr5171400();
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrmjaIO.getCrtable());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.crtabled.set(descIO.getLongdesc());
		}
		else {
			sv.crtabled.fill("?");
		}
		sv.zbinstprem.set(0);
		sv.zlinstprem.set(0);
		sv.life.set(covrmjaIO.getLife());
		sv.coverage.set(covrmjaIO.getCoverage());
		sv.rider.set(covrmjaIO.getRider());
		sv.statcde.set(covrmjaIO.getStatcode());
		sv.pstatcode.set(covrmjaIO.getPstatcode());
		sv.crtable.set(covrmjaIO.getCrtable());
		sv.sumins.set(covrmjaIO.getSumins());
		sv.instprem.set(covrmjaIO.getInstprem());
		sv.datakey.set(covrmjaIO.getDataKey());
		sv.zbinstprem.set(covrmjaIO.getZbinstprem());
		sv.zlinstprem.set(covrmjaIO.getZlinstprem());
		if (isEQ(hbnfIO.getStatuz(), varcom.oK)
		&& isEQ(hbnfIO.getCrtable(), covrmjaIO.getCrtable())) {
			sv.benpln.set(hbnfIO.getBenpln());
			sv.livesno.set(hbnfIO.getLivesno());
			sv.zrwvflg05.set(hbnfIO.getWaiverprem());
			sv.waivercode.set(tr686rec.waivercode);
			sv.zunit.set(hbnfIO.getZunit());
		}
		else {
			sv.benpln.set(SPACES);
			sv.livesno.set(SPACES);
			sv.zrwvflg05.set(SPACES);
			sv.waivercode.set(SPACES);
			sv.zunit.set(1);
		}
		/*    Check whether Component is Benefit Billing Rider*/
		if (isEQ(t5687rec.premmeth, SPACES)
		&& isNE(t5687rec.bbmeth, SPACES)) {
			sv.bftpaym.set("Y");
		}
		/*    Check whether Component is Premium Paying Rider*/
		if (isNE(t5687rec.premmeth, SPACES)
		&& isEQ(t5687rec.bbmeth, SPACES)) {
			sv.premind.set("Y");
		}
		/*    Set up Select or Non-select component*/
		if (isEQ(wsspcomn.flag, "P")
		|| isEQ(wsspcomn.flag, "Q")) {
			componentValidityCheck1600();
		}
		else {
			goTo(GotoLabel.checkComponent1250);
		}
		if (!validComponent.isTrue()) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.select.set("N");
			sv.chgflag.set("N");
		}
		else {
			sv.select.set("Y");
			sv.chgflag.set("Y");
			/* Main Coverage is mandatory to reinstate*/
			if (isEQ(covrmjaIO.getLife(), "01")
			&& isEQ(covrmjaIO.getCoverage(), "01")
			&& isEQ(covrmjaIO.getRider(), "00")) {
				sv.selectOut[varcom.pr.toInt()].set("Y");
			}
			else {
				/*    Exclude Maturity/Expire component as well*/
				if (isLTE(covrmjaIO.getRiskCessDate(), sv.effdate)) {
					sv.selectOut[varcom.pr.toInt()].set("Y");
					sv.select.set("N");
					sv.chgflag.set("N");
				}
			}
			if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && ("Y").equals(ta524rec.prmstatus.toString()))
				sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		goTo(GotoLabel.addToSfl1270);
	}

protected void checkComponent1250()
	{
		if (isEQ(covrmjaIO.getTranno(), phrtIO.getTranno())) {
			sv.select.set("Y");
			sv.chgflag.set("Y");
		}
		else {
			sv.select.set("N");
			sv.chgflag.set("N");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(covrmjaIO.getLife(), "01")
		&& isEQ(covrmjaIO.getCoverage(), "01")
		&& isEQ(covrmjaIO.getRider(), "00")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if ((isEQ(wsspcomn.flag, "M"))
		&& ((!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES) ) ? isEQ(tr51prec.reinstflg, "Y") : isEQ(ta524rec.prmstatus, "Y"))
		&& ((isEQ(sv.premind, "Y")
		|| isEQ(sv.bftpaym, "Y")))
		&& (isEQ(covrmjaIO.getTranno(), wsaaHolidayTranno))) {
			sv.select.set("N");
			sv.chgflag.set("N");
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(wsspcomn.flag, "C")
		|| isEQ(wsspcomn.flag, "D")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && ("Y").equals(ta524rec.prmstatus.toString()))
			sv.selectOut[varcom.pr.toInt()].set("Y");
	}

protected void addToSfl1270()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*NEXT-COVRMJA*/
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void readT56871300()
	{
		start1310();
	}

protected void start1310()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		sv.genarea.set(itdmIO.getGenarea());
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

protected void readTr5171400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1410();
					readContitem1420();
				case noMoreTr5171480:
					noMoreTr5171480();
				case exit1490:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1410()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			goTo(GotoLabel.exit1490);
		}
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		sv.waiverprem.set("Y");
		sv.zrwvflg01.set(tr517rec.zrwvflg01);
		sv.zrwvflg02.set(tr517rec.zrwvflg02);
		sv.zrwvflg03.set(tr517rec.zrwvflg03);
		sv.zrwvflg04.set(tr517rec.zrwvflg04);
		iy.set(ZERO);
		wsaaCtables.set(SPACES);
		for (ix.set(1); !(isGT(ix, 50)); ix.add(1)){
			if (isNE(tr517rec.ctable[ix.toInt()], SPACES)) {
				iy.add(1);
				wsaaCtable[iy.toInt()].set(tr517rec.ctable[ix.toInt()]);
			}
		}
		if (isEQ(tr517rec.contitem, SPACES)) {
			goTo(GotoLabel.noMoreTr5171480);
		}
	}

protected void readContitem1420()
	{
		itdmIO.setItemitem(tr517rec.contitem);
		itdmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.noMoreTr5171480);
		}
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		for (ix.set(1); !(isGT(ix, 50)); ix.add(1)){
			if (isNE(tr517rec.ctable[ix.toInt()], SPACES)) {
				iy.add(1);
				wsaaCtable[iy.toInt()].set(tr517rec.ctable[ix.toInt()]);
			}
		}
		if (isEQ(tr517rec.contitem, SPACES)) {
			goTo(GotoLabel.noMoreTr5171480);
		}
		else {
			readContitem1420();
			return ;
		}
	}

protected void noMoreTr5171480()
	{
		sv.workAreaData.set(wsaaCtables);
	}

protected void premiumSuspense1500()
	{
		start1510();
	}

protected void start1510()
	{
		/* Get Premium Suspense*/
		acblenqIO.setRldgcoy(chdrmjaIO.getChdrcoy());
		acblenqIO.setSacscode(t5645rec.sacscode01);
		acblenqIO.setSacstyp(t5645rec.sacstype01);
		acblenqIO.setRldgacct(chdrmjaIO.getChdrnum());
		acblenqIO.setOrigcurr(chdrmjaIO.getCntcurr());
		acblenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblenqIO);
		if (isNE(acblenqIO.getStatuz(), varcom.oK)
		&& isNE(acblenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblenqIO.getParams());
			fatalError600();
		}
		if (isEQ(acblenqIO.getStatuz(), varcom.oK)) {
			compute(wsaaTotSusamt, 2).set(add(wsaaTotSusamt, acblenqIO.getSacscurbal()));
		}
		compute(sv.susamt, 2).set(mult(wsaaTotSusamt, -1));
	}

protected void componentValidityCheck1600()
	{
		try {
			covrRiskStatus1610();
			covrPremStatus1630();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void covrRiskStatus1610()
	{
		wsaaValidComponent.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.covRiskStat[ix.toInt()])) {
					wsaaValidComponent.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.ridRiskStat[ix.toInt()])) {
					wsaaValidComponent.set("Y");
					ix.set(13);
				}
			}
		}
		if (!validComponent.isTrue()) {
			goTo(GotoLabel.exit1690);
		}
	}

protected void covrPremStatus1630()
	{
		wsaaValidComponent.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.covPremStat[ix.toInt()])) {
					wsaaValidComponent.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.ridPremStat[ix.toInt()])) {
					wsaaValidComponent.set("Y");
					ix.set(13);
				}
			}
		}
		if (!validComponent.isTrue()) {
			return ;
		}
		/*    Allow to reinstate Premium Paying Rider?*/
		if (isEQ(sv.premind, "Y")) {
			if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES)) { //ILIFE-8179
				if(isNE(ta524rec.prmstatus, "Y")) {
					wsaaValidComponent.set("N");
				}
				else {
					wsaaValidComponent.set("Y");
				}
			}
			else {
				if (isNE(tr51prec.reinstflg, "Y")) {
					wsaaValidComponent.set("N");
				}
				else {
					if (isNE(covrmjaIO.getTranno(), wsaaHolidayTranno)) {
						wsaaValidComponent.set("N");
					}
					else {
						wsaaValidComponent.set("Y");
					}
				}
			}
		}
	}

protected void optionSwitching1700()
	{
		init1710();
	}

protected void init1710()
	{
		optswchStartup1800();
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		optswchrec.optsUser.set(varcom.vrcmUser);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*    MOVE 'Y'                    TO SR51M-OPTIND-OUT(PR)*/
		/*                                   SR51M-OPTIND-OUT(ND).*/
		if (isEQ(optswchrec.optsType[1], "X")) {
			sv.optindOut[varcom.pr.toInt()].set("N");
			sv.optindOut[varcom.nd.toInt()].set("N");
			sv.optdsc.set(optswchrec.optsDsc[1]);
			sv.optind.set(optswchrec.optsInd[1]);
		}
	}

protected void optswchStartup1800()
	{
		begn1810();
	}

protected void begn1810()
	{
		chdrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isEQ(chdrenqIO.getStatuz(), varcom.oK)) {
			chdrenqIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
		}
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		chdrenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void premiumHolidayTranno1900()
	{
		start1910();
	}

protected void start1910()
	{
		wsaaHolidayTranno.set(0);
		prmhactIO.setParams(SPACES);
		prmhactIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		prmhactIO.setChdrnum(chdrmjaIO.getChdrnum());
		prmhactIO.setTranno(99999);
		prmhactIO.setFormat(formatsInner.prmhactrec);
		prmhactIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		prmhactIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		prmhactIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, prmhactIO);
		if (isNE(prmhactIO.getStatuz(), varcom.oK)
		&& isNE(prmhactIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(prmhactIO.getParams());
			fatalError600();
		}
		if (isNE(prmhactIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(prmhactIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(prmhactIO.getStatuz(), varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.rlbr);
			return ;
		}
		wsaaHolidayTranno.set(prmhactIO.getTranno());
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		validateSubfile2060();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		optswchCheck2100();
		if (isNE(sv.apind, "Y")
		&& isNE(sv.apind, "N")) {
			sv.apindErr.set(errorsInner.f136);
		}
		if (isEQ(wsspcomn.flag, "C")) {
			approvalChecking2200();
		}
	}

protected void validateSubfile2060()
	{
		if (isEQ(wsspcomn.flag, "P")
		|| isEQ(wsspcomn.flag, "M")
		|| isEQ(wsspcomn.flag, "Q")) {
			if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES)) {
				premiumCalc5000();
				f000ProcessOsPremium();
			}
			else if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES))
				calcProratePrem(); 
		}
		wsaaSelect.set("N");
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}

		compute(sv.cmax, 2).set(add(sv.totalfee, sv.xamt));
		compute(sv.reqamt, 2).set(add(sv.osbal, sv.manadj));
		compute(sv.reqamt, 2).set(add(sv.reqamt,wsaaTaxamt));
		scrnparams.subfileRrn.set(1);
		if (isNE(wsaaSelect, "Y")) {
			scrnparams.errorCode.set(errorsInner.s106);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.manadj, ZERO)) {
			zrdecplrec.amountIn.set(sv.manadj);
			zrdecplrec.currency.set(sv.cntcurr);
			a100CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.manadj)) {
				sv.manadjErr.set(errorsInner.rfik);
				wsspcomn.edterror.set("Y");
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void optswchCheck2100()
	{
		call2110();
	}

protected void call2110()
	{
		if (isEQ(sv.optind, SPACES)) {
			return ;
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set("CBOX");
		optswchrec.optsStatuz.set(varcom.oK);
		optswchrec.optsInd[1].set(sv.optind);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			sv.optindErr.set(optswchrec.optsStatuz);
		}
	}

protected void approvalChecking2200()
	{
		/*START*/
		if (isEQ(sv.optind, SPACES)
		|| isEQ(sv.optind, "+")) {
			checkFollowup2300();
		}
		checkAuthority2500();
		/*    When issue the PH Reinstatement, make sure there is insuffici*/
		/*    money in suspense.*/
		if ((setPrecision(sv.reqamt, 2)
		&& isGT(sv.reqamt, (add(sv.susamt, sv.tolerance))))) {
			scrnparams.errorCode.set(errorsInner.e961);
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void checkFollowup2300()
	{
		/*START*/
		wsaaOsFollowup.set("N");
		flupIO.setParams(SPACES);
		flupIO.setChdrcoy(wsspcomn.company);
		flupIO.setChdrnum(sv.chdrnum);
		flupIO.setFupno(ZERO);
		flupIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		flupIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		flupIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(flupIO.getStatuz(), varcom.endp)
		|| osFollowup.isTrue())) {
			readFlup2400();
		}

		if (osFollowup.isTrue()) {
			scrnparams.errorCode.set(errorsInner.h017);
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void readFlup2400()
	{
		try {
			start2410();
			nextr2480();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start2410()
	{
		SmartFileCode.execute(appVars, flupIO);
		if (isNE(flupIO.getStatuz(), varcom.oK)
		&& isNE(flupIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(flupIO.getParams());
			fatalError600();
		}
		if (isNE(flupIO.getChdrcoy(), wsspcomn.company)
		|| isNE(flupIO.getChdrnum(), sv.chdrnum)
		|| isEQ(flupIO.getStatuz(), varcom.endp)) {
			flupIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2490);
		}
		/*  Are any of them outstanding?*/
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t5661);
		/*    MOVE FLUP-FUPCODE           TO ITEM-ITEMITEM.*/
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(flupIO.getFupcode());
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(errorsInner.w085);
			wsspcomn.edterror.set("Y");
			flupIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2490);
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		/*  Assume the current status will not be found,*/
		/*     therefore, the follow-up is outstanding.*/
		wsaaOutstanflu = "Y";
		for (ix.set(1); !(isGT(ix, 10)
		|| isEQ(wsaaOutstanflu, "N")); ix.add(1)){
			if (isEQ(flupIO.getFupstat(), t5661rec.fuposs[ix.toInt()])) {
				wsaaOutstanflu = "N";
			}
		}
		/*  Set indicator if outstanding Follow Up found. This will be*/
		/*  lost on next read as we no longer abort on first outstanding*/
		/*  Follow Up.*/
		if (isEQ(wsaaOutstanflu, "Y")) {
			wsaaOsFollowup.set(wsaaOutstanflu);
		}
	}

protected void nextr2480()
	{
		flupIO.setFunction(varcom.nextr);
	}

protected void checkAuthority2500()
	{
		/*START*/
		uwlmtrec.function.set("CHCK");
		uwlmtrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		uwlmtrec.chdrnum.set(chdrmjaIO.getChdrnum());
		uwlmtrec.cnttyp.set(chdrmjaIO.getCnttype());
		uwlmtrec.chdrcoy.set(wsspcomn.company);
		uwlmtrec.userid.set(wsspcomn.userid);
		callProgram(Uwlmtchk.class, uwlmtrec.rec);
		if (isNE(uwlmtrec.statuz, varcom.oK)) {
			scrnparams.errorCode.set(uwlmtrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					validation2610();
				case subfileError2670:
					subfileError2670();
				case exit2690:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit2690);
		}
		if (isEQ(sv.select, "Y")) {
			wsaaTotalPremium.add(sv.instprem);
			wsaaSelect.set("Y");
		}
		if (isEQ(sv.select, SPACES)) {
			sv.selectErr.set(errorsInner.o001);
			goTo(GotoLabel.subfileError2670);
		}
	}

protected void subfileError2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*NEXT-SFL*/
		scrnparams.function.set(varcom.srdn);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(wsspcomn.flag, "Q")) {
			return ;
		}
		if (isEQ(sv.optind, "X")) {
			return ;
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile3100();
		}

		if (isEQ(wsspcomn.flag, "P")
		|| isEQ(wsspcomn.flag, "M")) {
			updateRecords3200();
		}
		if (isEQ(wsspcomn.flag, "D")) {
			pendingReversal3700();
		}
		if (isEQ(wsspcomn.flag, "C")) {
			phReinstatement3800();
			if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES))
				updateIncr();
		}
	}

protected void validateSubfile3100()
	{
		validation3110();
	}

protected void validation3110()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			return ;
		}
		/*NEXT-SFL*/
		scrnparams.function.set(varcom.srdn);
	}

protected void updateRecords3200()
	{
		start3210();
	}

protected void start3210()
	{
		if (isEQ(wsspcomn.flag, "P")) {
			updateChdrmja3300();
			updatePtrn3400();
			updatePhrt7700();
			if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES))
				updateIncr();
		}
		wsaaSkipUpdate.set("Y");
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile3500();
		}

		if (isEQ(wsspcomn.flag, "M")) {
			if (isNE(sv.manadj, phrtIO.getManadj())) {
				wsaaSkipUpdate.set("N");
			}
			if (!skipUpdate.isTrue()) {
				updatePhrt7700();
				modifyChdrmja3310();
			}
		}
		releaseSoftlock3900();
	}

protected void updateChdrmja3300()
	{
		start3300();
	}

protected void start3300()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(wsaaToday);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setValidflag("1");
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setCurrfrom(wsaaToday);
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}
		if (isNE(t5679rec.setCnPremStat, SPACES)) {
			chdrmjaIO.setPstatcode(t5679rec.setCnPremStat);
		}
		if (isEQ(wsspcomn.flag, "P")) {
			setPrecision(chdrmjaIO.getSinstamt06(), 2);
			if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, sv.btdate)) {
				chdrmjaIO.setSinstamt02(tempCntfee);
			}
			else {
				chdrmjaIO.setSinstamt01(sv.xamt);
				chdrmjaIO.setSinstamt02(sv.totalfee);
			}
			chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(), chdrmjaIO.getSinstamt02()), chdrmjaIO.getSinstamt03()), chdrmjaIO.getSinstamt04()), chdrmjaIO.getSinstamt05()));
		}
		else {
			if (isEQ(sv.apind, "Y")) {
				chdrmjaIO.setPtdate(sv.ptdate);
				chdrmjaIO.setBtdate(sv.ptdate);
				chdrmjaIO.setBillcd(sv.ptdate);
			}
			else {
				chdrmjaIO.setPtdate(sv.hprjptdate);
				chdrmjaIO.setBtdate(sv.hprjptdate);
				chdrmjaIO.setBillcd(sv.hprjptdate);
			}
		}
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		//ILIFE-3997 Starts
		if (isEQ(chdrmjaIO.getNlgflg(), "Y") && isEQ(wsspcomn.flag, "C")) {
			nlgflg3300();
		}
		//ILIFE-3997 End
	}
//ILIFE-3997 Starts
protected void nlgflg3300() {
	nlgcalcrec.fsuco.set(wsspcomn.fsuco);
	nlgcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
	nlgcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
	nlgcalcrec.effdate.set(wsaaToday);
	nlgcalcrec.tranno.set(chdrmjaIO.getTranno());
	nlgcalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	nlgcalcrec.frmdate.set(chdrmjaIO.getOccdate());
	nlgcalcrec.todate.set(varcom.maxdate);
	nlgcalcrec.cnttype.set(chdrmjaIO.getCnttype());
	nlgcalcrec.language.set(wsspcomn.language);
	nlgcalcrec.inputAmt.set(sv.reqamt);
	nlgcalcrec.Cntcurr.set(chdrmjaIO.getCntcurr());
	nlgcalcrec.billchnl.set(chdrmjaIO.getBillchnl());
	nlgcalcrec.billfreq.set(chdrmjaIO.getBillfreq());
	nlgcalcrec.occdate.set(chdrmjaIO.getOccdate());
	nlgcalcrec.function.set("LINST");
	nlgcalcrec.status.set(Varcom.oK);
	callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
	if(isNE(nlgcalcrec.status,varcom.oK)) {
		syserrrec.params.set(nlgcalcrec.nlgcalcRec);
		fatalError600();
	}
}
//ILIFE-3997 End

protected void modifyChdrmja3310()
	{
		start3310();
	}

protected void start3310()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		setPrecision(chdrmjaIO.getSinstamt06(), 2);	//ILIFE-8509
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, sv.btdate)) {
			chdrmjaIO.setSinstamt02(tempCntfee);
		}
		else {
			chdrmjaIO.setSinstamt01(sv.xamt);
			chdrmjaIO.setSinstamt02(sv.totalfee);
		}
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(), chdrmjaIO.getSinstamt02()), chdrmjaIO.getSinstamt03()), chdrmjaIO.getSinstamt04()), chdrmjaIO.getSinstamt05()));
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void updatePtrn3400()
	{
		start3410();
	}

protected void start3410()
	{
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setValidflag("1");
		if((prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isGT(sv.effdate, prmhactIO.todate)))	//ILIFE-8509
			ptrnIO.setPtrneff(prmhactIO.todate);
		else
			ptrnIO.setPtrneff(sv.effdate);
		ptrnIO.setTermid(SPACES);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);
		ptrnIO.setTransactionDate(getCobolDate());
		ptrnIO.setTransactionTime(getCobolTime());
		ptrnIO.setUser(999999);
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void readSubfile3500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					validation3510();
				case nextSfl3580:
					nextSfl3580();
				case exit3590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation3510()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit3590);
		}
		if (isEQ(wsspcomn.flag, "P")) {
			if (isNE(sv.select, "Y")) {
				goTo(GotoLabel.nextSfl3580);
			}
			processCovrmja3600();
			goTo(GotoLabel.nextSfl3580);
		}
		/*    For modification, if "Back Pay" flag was not change:*/
		if (isEQ(sv.apind, phrtIO.getApind())) {
			if (isEQ(sv.select, sv.chgflag)) {
				if (isEQ(sv.select, "Y")
				&& isEQ(sv.waiverprem, "Y")) {
					wopCovr3620();
				}
				goTo(GotoLabel.nextSfl3580);
			}
			else {
				if (isEQ(sv.select, "Y")) {
					processCovrmja3600();
				}
				else {
					updateCovr3650();
				}
				wsaaSkipUpdate.set("N");
			}
			goTo(GotoLabel.nextSfl3580);
		}
		/*    For modification, if "Back Pay" flag is changed:*/
		wsaaSkipUpdate.set("N");
		if (isEQ(sv.select, sv.chgflag)) {
			if (isEQ(sv.select, "Y")) {
				processCovrmja3680();
			}
		}
		else {
			if (isEQ(sv.select, "N")) {
				updateCovr3650();
			}
			else {
				processCovrmja3600();
			}
		}
	}

protected void nextSfl3580()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void processCovrmja3600()
	{
		start3600();
		validflag23600();
		validflag13600();
	}

protected void start3600()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if(covrmjaIO.getReinstated()!=null && "Y".equals(covrmjaIO.getReinstated().toString().trim()) && !lapseReinstated)
			lapseReinstated = true;
	}

protected void validflag23600()
	{
		covrmjaIO.setValidflag("2");
		/*    With back pay or without back pay, set up CURRTO accordingly*/
		if (isEQ(sv.apind, "Y")) {
			/*    For Benefit Billing Rider, there is no back pay for COI*/
			if (isEQ(sv.bftpaym, "Y")) {
				covrmjaIO.setCurrto(sv.effdate);
			}
			else {
				covrmjaIO.setCurrto(sv.ptdate);
			}
		}
		else {
			covrmjaIO.setCurrto(sv.effdate);
		}
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13600()
	{
		covrmjaIO.setValidflag("1");
		/*    With back pay or without back pay, set up CURRFROM accordingl*/
		if (isEQ(sv.apind, "Y")) {
			if (isEQ(sv.bftpaym, "Y")) {
				covrmjaIO.setCurrfrom(sv.effdate);
				covrmjaIO.setBenBillDate(sv.effdate);
			}
			else {
				covrmjaIO.setCurrfrom(sv.ptdate);
			}
		}
		else {
			covrmjaIO.setCurrfrom(sv.effdate);
			if (isEQ(sv.bftpaym, "Y")) {
				covrmjaIO.setBenBillDate(sv.effdate);
			}
		}
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(phrtIO.getTranno());
		covrmjaIO.setInstprem(sv.instprem);
		covrmjaIO.setZbinstprem(sv.zbinstprem);
		covrmjaIO.setZlinstprem(sv.zlinstprem);
		if (isEQ(sv.waiverprem, "Y")) {
			covrmjaIO.setSumins(sv.sumins);
		}
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrmjaIO.getRider(), "00")) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
				covrmjaIO.setPstatcode(t5679rec.setSngpCovStat);
			}
			else {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
				covrmjaIO.setPstatcode(t5679rec.setSngpRidStat);
			}
		}
		if (isNE(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrmjaIO.getRider(), "00")) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
			else {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, sv.btdate)) {
			if(proratePremMap!=null && !proratePremMap.isEmpty())
				covrmjaIO.setProrateprem(proratePremMap.get(covrmjaIO.getLife().toString().trim()+covrmjaIO.getCoverage().toString().trim()+covrmjaIO.getRider().toString().trim()));
		}
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void wopCovr3620()
	{
		start3620();
		validflag13620();
	}

protected void start3620()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13620()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setSumins(sv.sumins);
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void updateCovr3650()
	{
		start3650();
		validflag13650();
	}

protected void start3650()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13650()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(covrmjaIO.getChdrcoy());
		covrIO.setChdrnum(covrmjaIO.getChdrnum());
		covrIO.setLife(covrmjaIO.getLife());
		covrIO.setCoverage(covrmjaIO.getCoverage());
		covrIO.setRider(covrmjaIO.getRider());
		covrIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), covrIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), covrIO.getCoverage())
		|| isNE(covrmjaIO.getRider(), covrIO.getRider())
		|| isNE(covrIO.getValidflag(), "2")
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void processCovrmja3680()
	{
		start3680();
		validflag23680();
		covrIo3680();
	}

protected void start3680()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag23680()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(sv.apind, "N")) {
			covrmjaIO.setCurrfrom(sv.effdate);
		}
		else {
			if (isEQ(sv.bftpaym, "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				covrmjaIO.setCurrfrom(sv.ptdate);
			}
		}
		if (isEQ(sv.waiverprem, "Y")) {
			covrmjaIO.setSumins(sv.sumins);
		}
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		/*3680-VALIDFLAG-2.                                                */
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(covrmjaIO.getChdrcoy());
		covrIO.setChdrnum(covrmjaIO.getChdrnum());
		covrIO.setLife(covrmjaIO.getLife());
		covrIO.setCoverage(covrmjaIO.getCoverage());
		covrIO.setRider(covrmjaIO.getRider());
		covrIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		covrIO.setFormat(formatsInner.covrrec);
	}

protected void covrIo3680()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), covrIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), covrIO.getCoverage())
		|| isNE(covrmjaIO.getRider(), covrIO.getRider())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getValidflag(), "1")) {
			covrIO.setFunction(varcom.nextr);
			covrIo3680();
			return ;
		}
		if (isEQ(sv.apind, "N")) {
			covrIO.setCurrto(sv.effdate);
		}
		else {
			if (isEQ(sv.bftpaym, "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				covrIO.setCurrto(sv.ptdate);
			}
		}
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void pendingReversal3700()
	{
		start3710();
		transferToAt3750();
	}

protected void start3710()
	{
		/* Obtain Forward Transaction Code from T6661.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaForwardTranscode.set(t6661rec.trcode);
		a300CheckPtrnrev();
		phrtIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
	}

protected void transferToAt3750()
	{
		/*  transfer the contract soft lock to AT*/
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.enttyp.set(chdrmjaIO.getChdrpfx());
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrmjaIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransAreaInner.wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransAreaInner.wsaaTranDate.set(varcom.vrcmDate);
		wsaaTransAreaInner.wsaaTranTime.set(varcom.vrcmTime);
		wsaaTransAreaInner.wsaaUser.set(varcom.vrcmUser);
		wsaaTransAreaInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransAreaInner.wsaaTranNum.set(ptrnrevIO.getTranno());
		wsaaTransAreaInner.wsaaTranCode.set(ptrnrevIO.getBatctrcde());
		wsaaTransAreaInner.wsaaPlnsfx.set(ZERO);
		wsaaTransAreaInner.wsaaSupflag.set("N");
		wsaaTransAreaInner.wsaaTodate.set(ptrnrevIO.getPtrneff());
		wsaaTransAreaInner.wsaaSuppressTo.set(ZERO);
		wsaaTransAreaInner.wsaaCfiafiTranno.set(ZERO);
		wsaaTransAreaInner.wsaaCfiafiTranCode.set(SPACES);
		wsaaTransAreaInner.wsaaBbldat.set(ZERO);
		atreqrec.transArea.set(wsaaTransAreaInner.wsaaTransArea);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
	}

protected void phReinstatement3800()
	{
		start3810();
	}

protected void start3810()
	{
		updateChdrmja3300();
		updatePtrn3400();
		processPayr6000();
		processComponent6100();
		setupLifacmvrec6300();
		postAdjustmentFee6400();
		updatePrmh6500();
		updatePhrt6600();
		//PINNACLE-2368
		if(!CSOTH019Permission) {
		processReassurance7000();
		}
		printLetter7500();
		statistic7600();
		dryProcessing8000();
		releaseSoftlock3900();
	}

protected void releaseSoftlock3900()
	{
		start3910();
	}

protected void start3910()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4010();
				case callNextSfl4020:
					callNextSfl4020();
					nextSfl4080();
				case exit4090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.optind, "X")) {
			optswchrec.optsSelOptno.set(sv.optind);
			optswchrec.optsSelType.set(SPACES);
			optswchCall4500();
			sv.optind.set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			scrnparams.function.set(varcom.srdn);
		}
		else {
			scrnparams.function.set(varcom.sstrt);
		}
	}

protected void callNextSfl4020()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				optswchrec.optsSelType.set(SPACES);
				optswchrec.optsSelOptno.set(ZERO);
				optswchCall4500();
			}
			else {
				wsspcomn.programPtr.add(1);
			}
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit4090);
		}
	}

protected void nextSfl4080()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.callNextSfl4020);
	}

protected void optswchCall4500()
	{
		call4510();
	}

protected void call4510()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optind.set(optswchrec.optsInd[1]);
		if (isEQ(optswchrec.optsStatuz, varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void premiumCalc5000()
	{
		/*START*/
		wsaaTotalPremium.set(0);
		wsaaEarlistRerateDate.set(varcom.vrcmMaxDate);
		wsaaLastRerateDate.set(varcom.vrcmMaxDate);
		calcNonWopPremium5100();
		calcContFee5500();
		calcWopPremium5600();
		sv.xamt.set(wsaaTotalPremium);
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void calcNonWopPremium5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					init5110();
				case readSfl5120:
					readSfl5120();
					readCovr5140();
					updtSfl5170();
				case nextSfl5180:
					nextSfl5180();
				case exit5190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init5110()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl5120()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit5190);
		}
		if (isNE(sv.select, "Y")) {
			goTo(GotoLabel.nextSfl5180);
		}
		if (isEQ(sv.waiverprem, "Y")) {
			goTo(GotoLabel.nextSfl5180);
		}
	}

protected void readCovr5140()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(sv.genarea);
		calcPremium5200();
	}

protected void updtSfl5170()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void nextSfl5180()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl5120);
	}

protected void calcPremium5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					singlePremium5210();
					lifeAndJointLife5220();
					readT56755230();
					initPremiumrec5250();
				case premCalcDone5280:
					premCalcDone5280();
				case setUp5285:
					setUp5285();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void singlePremium5210()
	{
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			premiumrec.calcPrem.set(covrmjaIO.getInstprem());
			if (isEQ(chdrmjaIO.getBillfreq(), "00")) {
				premiumrec.calcPrem.add(covrmjaIO.getSingp());
			}
			premiumrec.calcBasPrem.set(covrmjaIO.getZbinstprem());
			premiumrec.calcLoaPrem.set(covrmjaIO.getZlinstprem());
			goTo(GotoLabel.premCalcDone5280);
		}
		if (isNE(t5687rec.bbmeth, SPACES)) {
			premiumrec.calcPrem.set(covrmjaIO.getInstprem());
			if (isEQ(chdrmjaIO.getBillfreq(), "00")) {
				premiumrec.calcPrem.add(covrmjaIO.getSingp());
			}
			premiumrec.calcBasPrem.set(covrmjaIO.getZbinstprem());
			premiumrec.calcLoaPrem.set(covrmjaIO.getZlinstprem());
			goTo(GotoLabel.premCalcDone5280);
		}
	}

protected void lifeAndJointLife5220()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			if (isEQ(t5687rec.bbmeth, SPACES)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("No Premium Calculation Method for ");
				stringVariable1.addExpression(covrmjaIO.getCrtable());
				stringVariable1.setStringInto(syserrrec.params);
				syserrrec.statuz.set(varcom.mrnf);
				fatalError600();
			}
			else {
				sv.instprem.set(ZERO);
				sv.zbinstprem.set(ZERO);
				sv.zlinstprem.set(ZERO);
				goTo(GotoLabel.setUp5285);
			}
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
			sv.rtrnwfreq.set(t5687rec.rtrnwfreq);
		}
		else {
			sv.rtrnwfreq.set(0);
			wsaaRerateComp.set("N");
		}
	}

protected void readT56755230()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void initPremiumrec5250()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.language.set(wsspcomn.language);
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.mop.set(sv.mop);
		premiumrec.billfreq.set(sv.billfreq);
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.sumin.set(covrmjaIO.getSumins());
		premiumrec.calcPrem.set(covrmjaIO.getInstprem());
		premiumrec.calcBasPrem.set(covrmjaIO.getZbinstprem());
		premiumrec.calcLoaPrem.set(covrmjaIO.getZlinstprem());
		premiumrec.benpln.set(sv.benpln);
		pr676cpy.livesno.set(sv.livesno);
		pr676cpy.waiverCrtable.set(sv.waivercode);
		pr676cpy.waiverprem.set(sv.zrwvflg05);
		/* Sex*/
		premiumrec.lsex.set(covrmjaIO.getSex());
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		/* Age*/
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()
		|| isEQ(sv.apind, "Y")) {
			traceLastRerateDate5300();
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
			premiumrec.effectdt.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Annuity info.*/
		annyIO.setParams(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		/*  Calc age at paid to date.*/
		if (rerateComp.isTrue()
		|| isEQ(sv.apind, "Y")) {
			agecalcrec.intDate1.set(wsaaMlDob);
			if (isEQ(sv.apind, "Y")) {
				agecalcrec.intDate2.set(sv.hprjptdate);
			}
			else {
				agecalcrec.intDate2.set(sv.ptdate);
			}
			calcAge5400();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5400();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.lage.set(wsaaJlAnbAtCcd);
			}
		}
		/*  Call Premium Calc Subr.*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		if (isEQ(sv.benpln, SPACES)) {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			
		}
		else {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				//ILIFE-2465 Sr676_Hospital & Surgical Benefit
				callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
			}
		}
		/*Ticket #IVE-792 - End*/
		
		if (isNE(premiumrec.statuz, varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
	}

protected void premCalcDone5280()
	{
		if (isLTE(sv.zunit, 1)) {
			sv.instprem.set(premiumrec.calcPrem);
			wsaaTotalPremium.add(premiumrec.calcPrem);
			sv.zbinstprem.set(premiumrec.calcBasPrem);
			sv.zlinstprem.set(premiumrec.calcLoaPrem);
		}
		else {
			compute(sv.instprem, 3).setRounded(mult(sv.zunit, premiumrec.calcPrem));
			compute(wsaaTotalPremium, 3).setRounded(add(wsaaTotalPremium, sv.instprem));
			compute(sv.zbinstprem, 3).setRounded(mult(sv.zunit, premiumrec.calcBasPrem));
			compute(sv.zlinstprem, 3).setRounded(mult(sv.zunit, premiumrec.calcLoaPrem));
		}
	}

protected void setUp5285()
	{
		sv.sumins.set(covrmjaIO.getSumins());
		sv.rerateDate.set(wsaaLastRerateDate);
		sv.ind.set(wsaaRerateComp);
		if (isLTE(wsaaLastRerateDate, wsaaEarlistRerateDate)) {
			wsaaEarlistRerateDate.set(wsaaLastRerateDate);
		}
		/*EXIT*/
	}

protected void traceLastRerateDate5300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start5310();
				case backpayCalc5350:
					backpayCalc5350();
				case exit5390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5310()
	{
		if (isEQ(sv.apind, "Y")) {
			goTo(GotoLabel.backpayCalc5350);
		}
		wsaaLastRerateDate.set(covrmjaIO.getRerateDate());
		datcon4rec.billmonth.set(wsaaOccdateMm);
		datcon4rec.billday.set(wsaaOccdateDd);
		compute(datcon4rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));
		datcon4rec.frequency.set("01");
		while ( !(isLTE(wsaaLastRerateDate, sv.ptdate))) {
			datcon4rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastRerateDate.set(datcon4rec.intDate2);
		}

		goTo(GotoLabel.exit5390);
	}

protected void backpayCalc5350()
	{
		wsaaLastRerateDate.set(covrmjaIO.getRerateDate());
		datcon4rec.billmonth.set(wsaaOccdateMm);
		datcon4rec.billday.set(wsaaOccdateDd);
		datcon4rec.freqFactor.set(-1);
		datcon4rec.frequency.set("01");
		while ( !(isLTE(wsaaLastRerateDate, sv.hprjptdate))) {
			datcon4rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastRerateDate.set(datcon4rec.intDate2);
		}

	}

protected void calcAge5400()
	{
		/*START*/
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(agecalcrec.agecalcRec);
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcContFee5500()
	{
		try {
			readT56885510();
			readT56745520();
			callContractFeeRoutine5530();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readT56885510()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		if (isEQ(t5688rec.feemeth, SPACES)) {
			goTo(GotoLabel.exit5590);
		}
	}

protected void readT56745520()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		if (isEQ(t5674rec.commsubr, SPACES)) {
			goTo(GotoLabel.exit5590);
		}
	}

protected void callContractFeeRoutine5530()
	{
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.company.set(chdrmjaIO.getChdrcoy());
		mgfeelrec.cnttype.set(chdrmjaIO.getCnttype());
		mgfeelrec.effdate.set(chdrmjaIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrmjaIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		mgfeelrec.billfreq.set(sv.billfreq);
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		sv.totalfee.set(mgfeelrec.mgfee);
	}

protected void calcWopPremium5600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					init5610();
				case nextFslRec5620:
					nextFslRec5620();
					processWopSflRec5630();
					sreadForWopCoverage5650();
					supdForWopCoverage5670();
				case exit5690:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init5610()
	{
		iy.set(ZERO);
		wsaaWopCompRrn.set(ZERO);
	}

protected void nextFslRec5620()
	{
		iy.add(1);
		scrnparams.subfileRrn.set(iy);
		scrnparams.function.set(varcom.sread);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.mrnf)) {
			goTo(GotoLabel.exit5690);
		}
		if (isNE(sv.select, "Y")) {
			nextFslRec5620();
			return ;
		}
		if (isNE(sv.waiverprem, "Y")) {
			nextFslRec5620();
			return ;
		}
	}

protected void processWopSflRec5630()
	{
		wsaaWopCompRrn.set(iy);
		sumWopSumins5700();
		if (isEQ(wsaaZrwvflg03, "Y")) {
			wsaaWaiverSumins.add(sv.totalfee);
		}
	}

protected void sreadForWopCoverage5650()
	{
		scrnparams.subfileRrn.set(wsaaWopCompRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wopPremium5900();
	}

protected void supdForWopCoverage5670()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Repeat for next subfile rec.*/
		goTo(GotoLabel.nextFslRec5620);
	}

protected void sumWopSumins5700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start5710();
				case readSfl5720:
					readSfl5720();
					waiveCheck5730();
				case cont5740:
					cont5740();
				case nextSfl5780:
					nextSfl5780();
				case exit5790:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5710()
	{
		wsaaWaiverSumins.set(ZERO);
		wsaaWopCovrmjakey.set(sv.datakey);
		wsaaCtables.set(sv.workAreaData);
		wsaaZrwvflg02.set(sv.zrwvflg02);
		wsaaZrwvflg03.set(sv.zrwvflg03);
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl5720()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit5790);
		}
		if (isEQ(scrnparams.subfileRrn, wsaaWopCompRrn)) {
			goTo(GotoLabel.nextSfl5780);
		}
		if (isNE(sv.select, "Y")) {
			goTo(GotoLabel.nextSfl5780);
		}
	}

protected void waiveCheck5730()
	{
		wsaaCovrmjakey.set(sv.datakey);
		if (isNE(wsaaZrwvflg02, "Y")) {
			if (isNE(wsaaCovrmjakey.covrmjaLife, wsaaWopCovrmjakey.covrmjaLife)) {
				goTo(GotoLabel.nextSfl5780);
			}
		}
		if (isEQ(wsaaZrwvflg02, "Y")) {
			goTo(GotoLabel.cont5740);
		}
	}

protected void cont5740()
	{
		wsaaWaiveIt.set("N");
		for (ix.set(1); !(isGT(ix, 100)
		|| waiveIt.isTrue()
		|| isEQ(wsaaCtable[ix.toInt()], SPACES)); ix.add(1)){
			if (isEQ(sv.crtable, wsaaCtable[ix.toInt()])) {
				wsaaWaiveIt.set("Y");
			}
		}
		/* Not waive, skip.*/
		if (!waiveIt.isTrue()) {
			goTo(GotoLabel.nextSfl5780);
		}
		/* None Accelerated Crisis Waiver, just add the premium as WOP*/
		/* sum assured.*/
		if (isNE(tr517rec.zrwvflg04, "Y")) {
			wsaaWaiverSumins.add(sv.instprem);
			goTo(GotoLabel.nextSfl5780);
		}
		/* Accelerated Crisis Waiver, .....*/
		if (isNE(wsaaCovrmjakey.covrmjaLife, wsaaWopCovrmjakey.covrmjaLife)) {
			goTo(GotoLabel.nextSfl5780);
		}
		if (isNE(wsaaCovrmjakey.covrmjaCoverage, wsaaWopCovrmjakey.covrmjaCoverage)) {
			goTo(GotoLabel.exit5790);
		}
		if (isEQ(wsaaCovrmjakey.covrmjaRider, "00")) {
			wsaaMainCovrmjakey.set(sv.datakey);
			wsaaWaiverSumins.set(sv.sumins);
		}
		else {
			wsaaWaiverSumins.subtract(sv.sumins);
			calcBenefitAmount5800();
		}
	}

protected void nextSfl5780()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl5720);
	}

protected void calcBenefitAmount5800()
	{
		try {
			readMainCoverage5810();
			lifeAndJointLife5820();
			readT56875830();
			readT56755840();
			initPremiumrec5850();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readMainCoverage5810()
	{
		covrmjaIO.setDataKey(wsaaMainCovrmjakey);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void lifeAndJointLife5820()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
	}

protected void readT56875830()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			goTo(GotoLabel.exit5890);
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
			sv.rtrnwfreq.set(t5687rec.rtrnwfreq);
		}
		else {
			wsaaRerateComp.set("N");
			sv.rtrnwfreq.set(0);
		}
	}

protected void readT56755840()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void initPremiumrec5850()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.language.set(wsspcomn.language);
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.mop.set(sv.mop);
		premiumrec.billfreq.set(sv.billfreq);
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.sumin.set(wsaaWaiverSumins);
		/* Sex*/
		premiumrec.lsex.set(covrmjaIO.getSex());
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		/* Age*/
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()
		|| isEQ(sv.apind, "Y")) {
			traceLastRerateDate5300();
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
			premiumrec.effectdt.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Annuity info.*/
		annyIO.setParams(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		/*  Calc age at Paid to Date.*/
		if (rerateComp.isTrue()
		|| isEQ(sv.apind, "Y")) {
			agecalcrec.intDate1.set(wsaaMlDob);
			if (isEQ(sv.apind, "Y")) {
				agecalcrec.intDate2.set(sv.hprjptdate);
			}
			else {
				agecalcrec.intDate2.set(sv.ptdate);
			}
			calcAge5400();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5400();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.lage.set(wsaaJlAnbAtCcd);
			}
		}
		/*  Call Premium Calc Subr.*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		if (isEQ(sv.benpln, SPACES)) {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
		}
		else {
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				//ILIFE-2465 Sr676_Hospital & Surgical Benefit
				callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
			}
		}
		/*Ticket #IVE-792 - End*/

		if (isNE(premiumrec.statuz, varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		wsaaWaiverSumins.set(premiumrec.calcPrem);
		sv.rerateDate.set(wsaaLastRerateDate);
		sv.ind.set(wsaaRerateComp);
		if (isLTE(wsaaLastRerateDate, wsaaEarlistRerateDate)) {
			wsaaEarlistRerateDate.set(wsaaLastRerateDate);
		}
	}

protected void wopPremium5900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readWopCoverage5910();
					lifeAndJointLife5920();
					readT56875930();
					readT56755940();
					initPremiumrec5950();
				case setUp5980:
					setUp5980();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readWopCoverage5910()
	{
		covrmjaIO.setDataKey(wsaaWopCovrmjakey);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void lifeAndJointLife5920()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
	}

protected void readT56875930()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			if (isEQ(t5687rec.bbmeth, SPACES)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("No Premium Calculation Method for ");
				stringVariable1.addExpression(covrmjaIO.getCrtable());
				stringVariable1.setStringInto(syserrrec.params);
				syserrrec.statuz.set(varcom.mrnf);
				fatalError600();
			}
			else {
				sv.instprem.set(ZERO);
				sv.zbinstprem.set(ZERO);
				sv.zlinstprem.set(ZERO);
				goTo(GotoLabel.setUp5980);
			}
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
			sv.rtrnwfreq.set(t5687rec.rtrnwfreq);
		}
		else {
			wsaaRerateComp.set("N");
			sv.rtrnwfreq.set(0);
		}
	}

protected void readT56755940()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*LIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142  End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void initPremiumrec5950()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.language.set(wsspcomn.language);
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.mop.set(sv.mop);
		premiumrec.billfreq.set(sv.billfreq);
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.sumin.set(wsaaWaiverSumins);
		/* Sex*/
		premiumrec.lsex.set(covrmjaIO.getSex());
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		/* Age*/
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()
		|| isEQ(sv.apind, "Y")) {
			traceLastRerateDate5300();
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
			premiumrec.effectdt.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Annuity info.*/
		annyIO.setParams(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		/*  Calc age at Paid to Date.*/
		if (rerateComp.isTrue()
		|| isEQ(sv.apind, "Y")) {
			agecalcrec.intDate1.set(wsaaMlDob);
			if (isEQ(sv.apind, "Y")) {
				agecalcrec.intDate2.set(sv.hprjptdate);
			}
			else {
				agecalcrec.intDate2.set(sv.ptdate);
			}
			calcAge5400();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5400();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.lage.set(wsaaJlAnbAtCcd);
			}
		}
		/*  Call Premium Calc Subr.*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		if (isEQ(sv.benpln, SPACES)) {
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
		}
		else {
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				//ILIFE-2465 Sr676_Hospital & Surgical Benefit
				callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			
		}
		/*Ticket #IVE-792 - End*/
		if (isNE(premiumrec.statuz, varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		sv.instprem.set(premiumrec.calcPrem);
		wsaaTotalPremium.add(premiumrec.calcPrem);
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
	}

protected void setUp5980()
	{
		sv.sumins.set(wsaaWaiverSumins);
		sv.rerateDate.set(wsaaLastRerateDate);
		sv.ind.set(wsaaRerateComp);
		/*EXIT*/
	}

protected void processPayr6000()
	{
		readPayr6010();
		validflag26020();
		validflag16030();
	}

protected void readPayr6010()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag26020()
	{
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag16030()
	{
		payrIO.setValidflag("1");
		payrIO.setPstatcode(chdrmjaIO.getPstatcode());
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setEffdate(sv.ptdate);
		setPrecision(payrIO.getSinstamt06(), 2);
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, sv.btdate)) {
			payrIO.setSinstamt02(tempCntfee);
		}
		else {
			payrIO.setSinstamt02(sv.totalfee);
		}
		if (isEQ(sv.apind, "Y")) {
			payrIO.setPtdate(sv.ptdate);
			payrIO.setBtdate(sv.ptdate);
			payrIO.setBillcd(sv.ptdate);
		}
		else {
			payrIO.setPtdate(sv.hprjptdate);
			payrIO.setBtdate(sv.hprjptdate);
			payrIO.setBillcd(sv.hprjptdate);
		}
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, sv.btdate)) {
			payrIO.setBillcd(getNextdate(-1, sv.btdate.toString(), chdrmjaIO.getBillfreq().toString()));
			payrIO.setNextdate(getNextdate(t6654rec.leadDays.toInt()*(-1), payrIO.getBillcd().toString(), "DY"));
			payrIO.setProrcntfee(sv.totalfee);
			payrIO.setProramt(sv.xamt);
		}
		else
			payrIO.setSinstamt01(sv.xamt);
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05()));
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void processComponent6100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					startSfl6110();
				case readSfl6120:
					readSfl6120();
				case nextSfl6180:
					nextSfl6180();
				case exit6190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void startSfl6110()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl6120()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit6190);
		}
		if (isNE(sv.select, "Y")) {
			goTo(GotoLabel.nextSfl6180);
		}
		/*PROCESS-SFL*/
		processCovr6200();
	}

protected void nextSfl6180()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl6120);
	}

protected void processCovr6200()
	{
		readCovrmja6210();
		validflag26220();
		validflag16230();
	}

protected void readCovrmja6210()
	{
		t5687rec.t5687Rec.set(sv.genarea);
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if(covrmjaIO.getReinstated()!=null && "Y".equals(covrmjaIO.getReinstated().toString().trim()) && !lapseReinstated)
			lapseReinstated = true;
	}

protected void validflag26220()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(covrmjaIO.getCurrfrom());
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag16230()
	{
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setInstprem(sv.instprem);
		covrmjaIO.setZbinstprem(sv.zbinstprem);
		covrmjaIO.setZlinstprem(sv.zlinstprem);
		if (isEQ(sv.waiverprem, "Y")) {
			covrmjaIO.setSumins(sv.sumins);
		}
		if (isEQ(sv.apind, "Y")) {
			if (isEQ(sv.ind, "Y")) {
				datcon4rec.datcon4Rec.set(SPACES);
				datcon4rec.intDate1.set(sv.rerateDate);
				datcon4rec.billmonth.set(wsaaOccdateMm);
				datcon4rec.billday.set(wsaaOccdateDd);
				datcon4rec.freqFactor.set(sv.rtrnwfreq);
				datcon4rec.frequency.set("01");
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					syserrrec.statuz.set(datcon4rec.statuz);
					fatalError600();
				}
				covrmjaIO.setRerateFromDate(datcon4rec.intDate2);
				covrmjaIO.setRerateDate(datcon4rec.intDate2);
			}
			if (isEQ(sv.waiverprem, "Y")) {
				datcon4rec.datcon4Rec.set(SPACES);
				datcon4rec.intDate1.set(covrmjaIO.getRerateDate()); //MIBT-378
				datcon4rec.billmonth.set(wsaaOccdateMm);
				datcon4rec.billday.set(wsaaOccdateDd);
				datcon4rec.freqFactor.set(1);
				datcon4rec.frequency.set("01");
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					syserrrec.statuz.set(datcon4rec.statuz);
					fatalError600();
				}
				covrmjaIO.setRerateFromDate(datcon4rec.intDate2);
				covrmjaIO.setRerateDate(datcon4rec.intDate2);
			}
		}
		if (isNE(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrmjaIO.getRider(), "00")) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
			else {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, sv.btdate)) {
			if(proratePremMap!=null && !proratePremMap.isEmpty())
				covrmjaIO.setProrateprem(proratePremMap.get(covrmjaIO.getLife().toString()+covrmjaIO.getCoverage().toString()+covrmjaIO.getRider().toString()));
		}
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void setupLifacmvrec6300()
	{
		start6310();
	}

protected void start6310()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
		lifacmvrec.tranno.set(chdrmjaIO.getTranno());
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.rldgcoy.set(chdrmjaIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrmjaIO.getChdrcoy());
		lifacmvrec.origcurr.set(chdrmjaIO.getBillcurr());
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.transactionDate.set(getCobolDate());
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.trandesc.set(sv.acdes);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.effdate.set(sv.effdate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
	}

protected void postAdjustmentFee6400()
	{
		start6410();
	}

protected void start6410()
	{
		if (isEQ(sv.manadj, ZERO)) {
			return ;
		}
		lifacmvrec.jrnseq.set(1);
		lifacmvrec.origamt.set(sv.manadj);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		/*  Credit the Adjustment.*/
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.jrnseq.set(1);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}

protected void updatePrmh6500()
	{
		start6510();
	}

protected void start6510()
	{
		if((!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES)) || (prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && isLTE(sv.effdate, prmhactIO.todate)))
		prmhactIO.setTodate(sv.effdate);
		prmhactIO.setCrtuser(prmhactIO.getUserProfile());
		prmhactIO.setValidflag("2");
		prmhactIO.setFunction(varcom.writd);
		prmhactIO.setFormat(formatsInner.prmhactrec);
		SmartFileCode.execute(appVars, prmhactIO);
		if (isNE(prmhactIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(prmhactIO.getParams());
			syserrrec.statuz.set(prmhactIO.getStatuz());
			fatalError600();
		}
		prmhactIO.setValidflag("1");
		prmhactIO.setCrtuser(SPACES);
		prmhactIO.setActiveInd("T");
		prmhactIO.setApind(sv.apind);
		if (isEQ(sv.apind, "N")) {
			datcon3rec.intDate1.set(prmhactIO.getFrmdate());
			datcon3rec.intDate2.set(sv.effdate);
			datcon3rec.frequency.set("12");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				syserrrec.statuz.set(datcon3rec.statuz);
				fatalError600();
			}
			prmhactIO.setZmthos(datcon3rec.freqFactor);
		}
		else {
			prmhactIO.setZmthos(0);
		}
		prmhactIO.setLastTranno(prmhactIO.getTranno());
		prmhactIO.setTranno(chdrmjaIO.getTranno());
		prmhactIO.setBillfreq(payrIO.getBillfreq());
		/*    MOVE WRITD                  TO PRMHACT-FUNCTION.             */
		prmhactIO.setFunction(varcom.writr);
		prmhactIO.setFormat(formatsInner.prmhactrec);
		SmartFileCode.execute(appVars, prmhactIO);
		if (isNE(prmhactIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(prmhactIO.getParams());
			syserrrec.statuz.set(prmhactIO.getStatuz());
			fatalError600();
		}
	}

protected void updatePhrt6600()
	{
		start6610();
	}

protected void start6610()
	{
		phrtIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
		phrtIO.setDteiss(wsaaToday);
		phrtIO.setLastTranno(phrtIO.getTranno());
		phrtIO.setTranno(chdrmjaIO.getTranno());
		phrtIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
	}

protected void a000RetrvePhrt()
	{
		a010Start();
	}

protected void a010Start()
	{
		phrtIO.setParams(SPACES);
		phrtIO.setFormat(formatsInner.phrtrec);
		phrtIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
		sv.effdate.set(phrtIO.getEffdate());
		sv.hprjptdate.set(phrtIO.getHprjptdate());
		wsaaPrjptdate.set(phrtIO.getHprjptdate());
		sv.manadj.set(phrtIO.getManadj());
		sv.apind.set(phrtIO.getApind());
	}

protected void a100ProjectPtdate()
	{
		a110Start();
	}

protected void a110Start()
	{
		/*    For PH Reinstatement Quotation, system allows future date*/
		/*    to be inputted*/
		if (isEQ(wsspcomn.flag, "Q")
		&& isGT(wsspcomn.currfrom, wsaaToday)) {
			wsaaPrjptdate.set(wsspcomn.currfrom);
			sv.hprjptdate.set(wsspcomn.currfrom);
			return ;
		}
		datcon4rec.intDate1.set(wsaaPtd); //ILIFE-8179
		datcon4rec.intDate2.set(wsaaPtd);
		datcon4rec.billmonth.set(wsaaOccdateMm);
		datcon4rec.billday.set(wsaaOccdateDd);
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES))
			datcon4rec.freqFactor.set(0);
		else
			datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(chdrmjaIO.getBillfreq());
		//ILIFE-8179
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES)) {
			while ( !(isGTE(datcon4rec.intDate2, wsaaToday))) {
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					syserrrec.statuz.set(datcon4rec.statuz);
					fatalError600();
				}
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}
			
			wsaaPrjptdate.set(datcon4rec.intDate2);
			sv.hprjptdate.set(datcon4rec.intDate2);
		}
		else {
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			datcon4rec.intDate1.set(datcon4rec.intDate2);
			wsaaPrjptdate.set(datcon4rec.intDate2);
			sv.hprjptdate.set(datcon4rec.intDate2);
		}
		//ILIFE-8179
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES)) {
			if (isNE(wsaaPrjptdate, wsaaToday)) {
				datcon4rec.intDate1.set(wsaaPrjptdate);
				datcon4rec.billmonth.set(wsaaOccdateMm);
				datcon4rec.billday.set(wsaaOccdateDd);
				datcon4rec.freqFactor.set(-1);
				datcon4rec.frequency.set(chdrmjaIO.getBillfreq());
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					syserrrec.statuz.set(datcon4rec.statuz);
					fatalError600();
				}
				wsaaPrjptdate.set(datcon4rec.intDate2);
				sv.hprjptdate.set(datcon4rec.intDate2);
			}
		}
	}

protected void a200CheckHbnf()
	{
		a210Start();
	}

protected void a210Start()
	{
		hbnfIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrmjaIO.getChdrnum());
		hbnfIO.setFunction(varcom.readr);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hbnfIO.getStatuz());
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		if (isEQ(hbnfIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		if (isNE(hbnfIO.getWaiverprem(), "Y")) {
			tr686rec.waivercode.set(SPACES);
			return ;
		}
		/*    Read TR686 for waiver code*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		/*    MOVE COVRMJA-CHDRCOY        TO ITEM-ITEMCOY.*/
		itemIO.setItemcoy(hbnfIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr686);
		/*    MOVE COVRMJA-CRTABLE        TO ITEM-ITEMITEM.*/
		itemIO.setItemitem(hbnfIO.getCrtable());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set("RL84");
			wsspcomn.edterror.set("Y");
		}
		else {
			tr686rec.tr686Rec.set(itemIO.getGenarea());
		}
	}

protected void a300CheckPtrnrev()
	{
		/*A310-START*/
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		while ( !(isEQ(ptrnrevIO.getStatuz(), varcom.endp))) {
			a400CallPtrnrevio();
		}

		/*A390-EXIT*/
	}

protected void a400CallPtrnrevio()
	{
		try {
			a410Start();
			a480Nextr();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void a410Start()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(ptrnrevIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(ptrnrevIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(ptrnrevIO.getStatuz(), varcom.endp)) {
			ptrnrevIO.setStatuz(varcom.endp);
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isEQ(ptrnrevIO.getBatctrcde(), wsaaForwardTranscode)) {
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a490Exit);
		}
	}

protected void a480Nextr()
	{
		ptrnrevIO.setFunction(varcom.nextr);
	}

protected void a500DefaultFollowup()
	{
		a510Start();
	}

protected void a510Start()
	{
		chdrlnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		chdrlnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		chdrlnbIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifelnbIO.setLife(SPACES);
		lifelnbIO.setJlife(SPACES);
		lifelnbIO.setFunction(varcom.begn);
		while ( !(isEQ(lifelnbIO.getStatuz(), varcom.endp))) {
			a600ReadLifelnbio();
		}

	}

protected void a600ReadLifelnbio()
	{
		try {
			a610Start();
			a680Nextr();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void a610Start()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isNE(lifelnbIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(lifelnbIO.getStatuz(), varcom.endp)) {
			/*       MOVE ENDP                TO SYSR-STATUZ*/
			lifelnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a690Exit);
		}
		/* Check for automatic follow-ups by calling HCRTFUP*/
		hcrtfuprec.statuz.set(varcom.oK);
		hcrtfuprec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		hcrtfuprec.chdrnum.set(chdrmjaIO.getChdrnum());
		hcrtfuprec.cnttype.set(chdrmjaIO.getCnttype());
		hcrtfuprec.lrkcls.set(SPACES);
		hcrtfuprec.life.set(lifelnbIO.getLife());
		hcrtfuprec.jlife.set(lifelnbIO.getJlife());
		hcrtfuprec.lifcnum.set(lifelnbIO.getLifcnum());
		hcrtfuprec.anbccd.set(lifelnbIO.getAnbAtCcd());
		compute(hcrtfuprec.tranno, 0).set(add(chdrmjaIO.getTranno(), 1));
		hcrtfuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		hcrtfuprec.language.set(wsspcomn.language);
		hcrtfuprec.fsuco.set(wsspcomn.fsuco);
		hcrtfuprec.user.set(varcom.vrcmUser);
		hcrtfuprec.transactionTime.set(varcom.vrcmTime);
		hcrtfuprec.transactionDate.set(varcom.vrcmDate);
		callProgram(Hcrtfup.class, hcrtfuprec.crtfupRec);
		if (isNE(hcrtfuprec.statuz, varcom.oK)) {
			syserrrec.params.set(hcrtfuprec.crtfupRec);
			syserrrec.statuz.set(hcrtfuprec.statuz);
			fatalError600();
		}
	}

protected void a680Nextr()
	{
		lifelnbIO.setFunction(varcom.nextr);
	}

protected void a700CalcTolerance()
	{
		/*A710-START*/
		sv.tolerance.set(0);
		wsaaTolerance.set(0);
		wsaaTolerance2.set(0);
		a800CheckAgent();
		a900CalcTolerance();
		/*A790-EXIT*/
	}

protected void a800CheckAgent()
	{
		a810ReadAglf();
	}

protected void a810ReadAglf()
	{
		wsaaAgtTerminateFlag.set("N");
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrmjaIO.getAgntcoy());
		aglfIO.setAgntnum(chdrmjaIO.getAgntnum());
		aglfIO.setFormat(formatsInner.aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isLT(aglfIO.getDtetrm(), wsaaToday)
		|| isLT(aglfIO.getDteexp(), wsaaToday)
		|| isGT(aglfIO.getDteapp(), wsaaToday)) {
			wsaaAgtTerminateFlag.set("Y");
		}
	}

protected void a900CalcTolerance()
	{
		a910Tolerance();
	}

protected void a910Tolerance()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsaaT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5667Curr.set(chdrmjaIO.getBillcurr());
		itemIO.setItemitem(wsaaT5667Key);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(errorsInner.f247);
			scrnparams.function.set(varcom.prot);
			wsspcomn.edterror.set("Y");
			return ;
		}
		t5667rec.t5667Rec.set(itemIO.getGenarea());
		wsaaFreqMatch.set(SPACES);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt, 6)
		|| freqMatch.isTrue()); wsaaCnt.add(1)){
			if (isEQ(t5667rec.freq[wsaaCnt.toInt()], payrIO.getBillfreq())) {
				freqMatch.setTrue();
				compute(wsaaCnt, 0).set(sub(wsaaCnt, 1));
			}
		}
		if (freqMatch.isTrue()) {
			compute(wsaaTolerance, 2).set(div(mult(sv.xamt, t5667rec.prmtol[wsaaCnt.toInt()]), 100));
			if (isGT(wsaaTolerance, t5667rec.maxAmount[wsaaCnt.toInt()])) {
				wsaaTolerance.set(t5667rec.maxAmount[wsaaCnt.toInt()]);
			}
			if (isNE(t5667rec.maxamt[wsaaCnt.toInt()], 0)) {
				if (agtNotTerminated.isTrue()
				|| (agtTerminated.isTrue()
				&& isEQ(t5667rec.sfind, "2"))) {
					compute(wsaaTolerance2, 2).set(div(mult(sv.xamt, t5667rec.prmtoln[wsaaCnt.toInt()]), 100));
					if (isGT(wsaaTolerance2, t5667rec.maxamt[wsaaCnt.toInt()])) {
						wsaaTolerance2.set(t5667rec.maxamt[wsaaCnt.toInt()]);
					}
				}
			}
		}
		if (isNE(wsaaTolerance2, 0)) {
			sv.tolerance.set(wsaaTolerance2);
		}
		else {
			sv.tolerance.set(wsaaTolerance);
		}
	}

protected void f000ProcessOsPremium()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					f010Start();
				case f050FuturePremium:
					f050FuturePremium();
				case f090Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void f010Start()
	{
		if (isEQ(sv.apind, "N")
		&& isGTE(sv.effdate, sv.hprjptdate)) {
			goTo(GotoLabel.f050FuturePremium);
		}
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			wsaaOsDate[ix.toInt()].set(0);
			wsaaOsPremium[ix.toInt()].set(0);
		}
		sv.osbal.set(0);
		wsaaCnt.set(0);
		/*    If not back pay, the prem start date will be the PH reinstate*/
		/*    effective date*/
		if (isEQ(sv.apind, "N")) {
			wsaaStdate.set(sv.effdate);
		}
		else {
			wsaaStdate.set(payrIO.getPtdate());
		}
		//ILIFE-8179
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES)) {
			while ( !(isGT(wsaaStdate, wsaaPrjptdate)
			|| isGT(wsaaCnt, 99))) {
				datcon4rec.intDate1.set(wsaaStdate);
				datcon4rec.billmonth.set(wsaaOccdateMm);
				datcon4rec.billday.set(wsaaOccdateDd);
				datcon4rec.freqFactor.set(1);
				datcon4rec.frequency.set(chdrmjaIO.getBillfreq());
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					syserrrec.statuz.set(datcon4rec.statuz);
					fatalError600();
				}
				wsaaCnt.add(1);
				wsaaOsDate[wsaaCnt.toInt()].set(datcon4rec.intDate2);
				wsaaStdate.set(datcon4rec.intDate2);
			}
		}
		else {	//ILIFE-8179
			while ( !(isGTE(wsaaStdate, wsaaPrjptdate)
					|| isGT(wsaaCnt, 99))) {
						datcon4rec.intDate1.set(wsaaStdate);
						datcon4rec.billmonth.set(wsaaOccdateMm);
						datcon4rec.billday.set(wsaaOccdateDd);
						datcon4rec.freqFactor.set(1);
						datcon4rec.frequency.set(chdrmjaIO.getBillfreq());
						callProgram(Datcon4.class, datcon4rec.datcon4Rec);
						if (isNE(datcon4rec.statuz, varcom.oK)) {
							syserrrec.params.set(datcon4rec.datcon4Rec);
							syserrrec.statuz.set(datcon4rec.statuz);
							fatalError600();
						}
						wsaaCnt.add(1);
						wsaaOsDate[wsaaCnt.toInt()].set(datcon4rec.intDate2);
						wsaaStdate.set(datcon4rec.intDate2);
					}
		}

		for (iz.set(1); !(isGT(iz, 99)
		|| isEQ(wsaaOsDate[iz.toInt()], 0)); iz.add(1)){
			f100CalcOsPremium();
		}
		for (iz.set(1); !(isGT(iz, 99)
		|| isEQ(wsaaOsDate[iz.toInt()], 0)); iz.add(1)){
			sv.osbal.add(wsaaOsPremium[iz.toInt()]);
			sv.osbal.add(sv.totalfee);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.f090Exit);
	}

protected void f050FuturePremium()
	{
		compute(sv.osbal, 3).setRounded((add(sv.xamt, sv.totalfee)));
		
	}

protected void f100CalcOsPremium()
	{
		f100Start();
	}

protected void f100Start()
	{
		datcon4rec.intDate1.set(wsaaOsDate[iz.toInt()]);
		datcon4rec.billmonth.set(wsaaOccdateMm);
		datcon4rec.billday.set(wsaaOccdateDd);
		datcon4rec.freqFactor.set(-1);
		datcon4rec.frequency.set(chdrmjaIO.getBillfreq());
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		wsaaRerateDate.set(datcon4rec.intDate2);
		f200CalcNonWopPremium();
		f600CalcWopPremium();
	}

protected void f200CalcNonWopPremium()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					f210Init();
				case f220ReadSfl:
					f220ReadSfl();
					f240ReadCovr();
					f270UpdtSfl();
				case f280NextSfl:
					f280NextSfl();
				case f290Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void f210Init()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void f220ReadSfl()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.f290Exit);
		}
		if (isNE(sv.select, "Y")) {
			goTo(GotoLabel.f280NextSfl);
		}
		if (isEQ(sv.waiverprem, "Y")) {
			goTo(GotoLabel.f280NextSfl);
		}
	}

protected void f240ReadCovr()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(sv.genarea);
		f300CalcPremium();
	}

protected void f270UpdtSfl()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void f280NextSfl()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.f220ReadSfl);
	}

protected void f300CalcPremium()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					f310SinglePremium();
					f320LifeAndJointLife();
					f330ReadT5675();
					f350InitPremiumrec();
				case f380PremCalcDone:
					f380PremCalcDone();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void f310SinglePremium()
	{
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			premiumrec.calcPrem.set(covrmjaIO.getInstprem());
			if (isEQ(chdrmjaIO.getBillfreq(), "00")) {
				premiumrec.calcPrem.add(covrmjaIO.getSingp());
			}
			premiumrec.calcBasPrem.set(covrmjaIO.getZbinstprem());
			premiumrec.calcLoaPrem.set(covrmjaIO.getZlinstprem());
			goTo(GotoLabel.f380PremCalcDone);
		}
		if (isNE(t5687rec.bbmeth, SPACES)) {
			premiumrec.calcPrem.set(covrmjaIO.getInstprem());
			if (isEQ(chdrmjaIO.getBillfreq(), "00")) {
				premiumrec.calcPrem.add(covrmjaIO.getSingp());
			}
			premiumrec.calcBasPrem.set(covrmjaIO.getZbinstprem());
			premiumrec.calcLoaPrem.set(covrmjaIO.getZlinstprem());
			goTo(GotoLabel.f380PremCalcDone);
		}
	}

protected void f320LifeAndJointLife()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("No Premium Calculation Method for ");
			stringVariable1.addExpression(covrmjaIO.getCrtable());
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600();
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
	}

protected void f330ReadT5675()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142  End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void f350InitPremiumrec()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.language.set(wsspcomn.language);
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.mop.set(sv.mop);
		premiumrec.billfreq.set(sv.billfreq);
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.sumin.set(covrmjaIO.getSumins());
		premiumrec.calcPrem.set(covrmjaIO.getInstprem());
		premiumrec.benpln.set(sv.benpln);
		pr676cpy.livesno.set(sv.livesno);
		pr676cpy.waiverCrtable.set(sv.waivercode);
		pr676cpy.waiverprem.set(sv.zrwvflg05);
		/* Sex*/
		premiumrec.lsex.set(covrmjaIO.getSex());
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		/* Age*/
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			f400TraceLastRerateDate();
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
			premiumrec.effectdt.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Annuity info.*/
		annyIO.setParams(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		/*  Calc age at paid to date.*/
		if (rerateComp.isTrue()) {
			f550TraceLastAnniDate();
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastAnniDate);
			f500CalcAge();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				f500CalcAge();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.lage.set(wsaaJlAnbAtCcd);
			}
		}
		/*  Call Premium Calc Subr.*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		if (isEQ(sv.benpln, SPACES)) {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
		}
		else {
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				//ILIFE-2465 Sr676_Hospital & Surgical Benefit
				callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
			}
		}
		/*Ticket #IVE-792 - End*/

		if (isNE(premiumrec.statuz, varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
	}

protected void f380PremCalcDone()
	{
		if (isLTE(sv.zunit, 1)) {
			sv.singp.set(premiumrec.calcPrem);
		}
		else {
			compute(sv.singp, 3).setRounded(mult(premiumrec.calcPrem, sv.zunit));
		}
		sv.origSum.set(covrmjaIO.getSumins());
		wsaaOsPremium[iz.toInt()].add(sv.singp);
		/*F390-EXIT*/
	}

protected void f400TraceLastRerateDate()
	{
		f410Start();
	}

protected void f410Start()
	{
		wsaaLastRerateDate.set(covrmjaIO.getRerateDate());
		datcon4rec.billmonth.set(wsaaOccdateMm);
		datcon4rec.billday.set(wsaaOccdateDd);
		compute(datcon4rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));
		datcon4rec.frequency.set("01");
		while ( !(isLTE(wsaaLastRerateDate, wsaaRerateDate))) {
			datcon4rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastRerateDate.set(datcon4rec.intDate2);
		}

	}

protected void f500CalcAge()
	{
		/*F500-START*/
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(agecalcrec.agecalcRec);
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		/*F500-EXIT*/
	}

protected void f550TraceLastAnniDate()
	{
		f551Start();
	}

protected void f551Start()
	{
		wsaaLastAnniDate.set(covrmjaIO.getRerateDate());
		datcon4rec.billmonth.set(wsaaOccdateMm);
		datcon4rec.billday.set(wsaaOccdateDd);
		compute(datcon4rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));
		datcon4rec.frequency.set("01");
		while ( !(isLTE(wsaaLastAnniDate, wsaaRerateDate))) {
			datcon4rec.intDate1.set(wsaaLastAnniDate);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastAnniDate.set(datcon4rec.intDate2);
		}

	}

protected void f600CalcWopPremium()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					f610Init();
				case f620NextFslRec:
					f620NextFslRec();
					f630ProcessWopSflRec();
					f650SreadForWopCoverage();
					f670SupdForWopCoverage();
				case f690Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void f610Init()
	{
		iy.set(ZERO);
		wsaaWopCompRrn.set(ZERO);
	}

protected void f620NextFslRec()
	{
		iy.add(1);
		scrnparams.subfileRrn.set(iy);
		scrnparams.function.set(varcom.sread);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.mrnf)) {
			goTo(GotoLabel.f690Exit);
		}
		if (isNE(sv.select, "Y")) {
			f620NextFslRec();
			return ;
		}
		if (isNE(sv.waiverprem, "Y")) {
			f620NextFslRec();
			return ;
		}
	}

protected void f630ProcessWopSflRec()
	{
		wsaaWopCompRrn.set(iy);
		f700SumWopSumins();
		if (isEQ(wsaaZrwvflg03, "Y")) {
			wsaaWaiverSumins.add(sv.totalfee);
		}
	}

protected void f650SreadForWopCoverage()
	{
		scrnparams.subfileRrn.set(wsaaWopCompRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		f900WopPremium();
	}

protected void f670SupdForWopCoverage()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Repeat for next subfile rec.*/
		goTo(GotoLabel.f620NextFslRec);
	}

protected void f700SumWopSumins()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					f710Start();
				case f720ReadSfl:
					f720ReadSfl();
					f730WaiveCheck();
				case f740Cont:
					f740Cont();
				case f780NextSfl:
					f780NextSfl();
				case f790Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void f710Start()
	{
		wsaaWaiverSumins.set(ZERO);
		wsaaWopCovrmjakey.set(sv.datakey);
		wsaaCtables.set(sv.workAreaData);
		wsaaZrwvflg02.set(sv.zrwvflg02);
		wsaaZrwvflg03.set(sv.zrwvflg03);
		scrnparams.function.set(varcom.sstrt);
	}

protected void f720ReadSfl()
	{
		processScreen("SR51M", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.f790Exit);
		}
		if (isEQ(scrnparams.subfileRrn, wsaaWopCompRrn)) {
			goTo(GotoLabel.f780NextSfl);
		}
		if (isNE(sv.select, "Y")) {
			goTo(GotoLabel.f780NextSfl);
		}
	}

protected void f730WaiveCheck()
	{
		wsaaCovrmjakey.set(sv.datakey);
		if (isNE(wsaaZrwvflg02, "Y")) {
			if (isNE(wsaaCovrmjakey.covrmjaLife, wsaaWopCovrmjakey.covrmjaLife)) {
				goTo(GotoLabel.f780NextSfl);
			}
		}
		if (isEQ(wsaaZrwvflg02, "Y")) {
			goTo(GotoLabel.f740Cont);
		}
	}

protected void f740Cont()
	{
		wsaaWaiveIt.set("N");
		for (ix.set(1); !(isGT(ix, 100)
		|| waiveIt.isTrue()
		|| isEQ(wsaaCtable[ix.toInt()], SPACES)); ix.add(1)){
			if (isEQ(sv.crtable, wsaaCtable[ix.toInt()])) {
				wsaaWaiveIt.set("Y");
			}
		}
		/* Not waive, skip.*/
		if (!waiveIt.isTrue()) {
			goTo(GotoLabel.f780NextSfl);
		}
		/* None Accelerated Crisis Waiver, just add the premium as WOP*/
		/* sum assured.*/
		if (isNE(tr517rec.zrwvflg04, "Y")) {
			wsaaWaiverSumins.add(sv.singp);
			goTo(GotoLabel.f780NextSfl);
		}
		/* Accelerated Crisis Waiver, .....*/
		if (isNE(wsaaCovrmjakey.covrmjaLife, wsaaWopCovrmjakey.covrmjaLife)) {
			goTo(GotoLabel.f780NextSfl);
		}
		if (isNE(wsaaCovrmjakey.covrmjaCoverage, wsaaWopCovrmjakey.covrmjaCoverage)) {
			goTo(GotoLabel.f790Exit);
		}
		if (isEQ(wsaaCovrmjakey.covrmjaRider, "00")) {
			wsaaMainCovrmjakey.set(sv.datakey);
			wsaaWaiverSumins.set(sv.origSum);
		}
		else {
			wsaaWaiverSumins.subtract(sv.origSum);
			f800CalcBenefitAmount();
		}
	}

protected void f780NextSfl()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.f720ReadSfl);
	}

protected void f800CalcBenefitAmount()
	{
		try {
			f810ReadMainCoverage();
			f820LifeAndJointLife();
			f830ReadT5687();
			f840ReadT5675();
			f850InitPremiumrec();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void f810ReadMainCoverage()
	{
		covrmjaIO.setDataKey(wsaaMainCovrmjakey);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void f820LifeAndJointLife()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
	}

protected void f830ReadT5687()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			goTo(GotoLabel.f890Exit);
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
	}

protected void f840ReadT5675()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void f850InitPremiumrec()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.language.set(wsspcomn.language);
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.mop.set(sv.mop);
		premiumrec.billfreq.set(sv.billfreq);
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.sumin.set(wsaaWaiverSumins);
		/* Sex*/
		premiumrec.lsex.set(covrmjaIO.getSex());
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		/* Age*/
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			f400TraceLastRerateDate();
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
			premiumrec.effectdt.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Annuity info.*/
		annyIO.setParams(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		/*  Calc age at Paid to Date.*/
		if (rerateComp.isTrue()) {
			f550TraceLastAnniDate();
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastAnniDate);
			f500CalcAge();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				f500CalcAge();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.lage.set(wsaaJlAnbAtCcd);
			}
		}
		/*  Call Premium Calc Subr.*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		if (isEQ(sv.benpln, SPACES)) {
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
			//****Ticket #ILIFE-2005 end
		}
		else {
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
			}
			else
			{		
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				//ILIFE-2465 Sr676_Hospital & Surgical Benefit
				callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
			}

		}
		/*Ticket #IVE-792 - End*/

		if (isNE(premiumrec.statuz, varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		wsaaWaiverSumins.set(premiumrec.calcPrem);
	}

protected void f900WopPremium()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					f910ReadWopCoverage();
					f920LifeAndJointLife();
					f930ReadT5687();
					f940ReadT5675();
					f950InitPremiumrec();
				case f980SetUp:
					f980SetUp();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void f910ReadWopCoverage()
	{
		covrmjaIO.setDataKey(wsaaWopCovrmjakey);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void f920LifeAndJointLife()
	{
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltsex());
			wsaaJlDob.set(lifemjaIO.getCltdob());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
	}

protected void f930ReadT5687()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod, SPACES)) {
			if (isEQ(t5687rec.bbmeth, SPACES)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("No Premium Calculation Method for ");
				stringVariable1.addExpression(covrmjaIO.getCrtable());
				stringVariable1.setStringInto(syserrrec.params);
				syserrrec.statuz.set(varcom.mrnf);
				fatalError600();
			}
			else {
				sv.singp.set(ZERO);
				goTo(GotoLabel.f980SetUp);
			}
		}
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
	}

protected void f940ReadT5675()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*IVE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void f950InitPremiumrec()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.language.set(wsspcomn.language);
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.mop.set(sv.mop);
		premiumrec.billfreq.set(sv.billfreq);
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.sumin.set(wsaaWaiverSumins);
		premiumrec.benpln.set(sv.benpln);
		pr676cpy.livesno.set(sv.livesno);
		pr676cpy.waiverCrtable.set(sv.waivercode);
		pr676cpy.waiverprem.set(sv.zrwvflg05);
		/* Sex*/
		premiumrec.lsex.set(covrmjaIO.getSex());
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		/* Age*/
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			f400TraceLastRerateDate();
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
			premiumrec.effectdt.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Annuity info.*/
		annyIO.setParams(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)
		&& isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		/*  Calc age at Paid to Date.*/
		if (rerateComp.isTrue()) {
			f550TraceLastAnniDate();
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastAnniDate);
			f500CalcAge();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				f500CalcAge();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.lage.set(wsaaJlAnbAtCcd);
			}
		}
		/*  Call Premium Calc Subr.*/
/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/		
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/

			//****Ticket #ILIFE-2005 end
		if (isNE(premiumrec.statuz, varcom.oK)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		sv.singp.set(premiumrec.calcPrem);
	}

protected void f980SetUp()
	{
		sv.origSum.set(wsaaWaiverSumins);
		wsaaOsPremium[iz.toInt()].add(sv.singp);
		/*F990-EXIT*/
	}

protected void processReassurance7000()
	{
		start7010();
	}

protected void start7010()
	{
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifeenqIO.setLife(SPACES);
		lifeenqIO.setJlife(SPACES);
		lifeenqIO.setFunction(varcom.begn);
		wsaaLife.set(SPACES);
		while ( !(isEQ(lifeenqIO.getStatuz(), varcom.endp))) {
			callCsncalc7100();
		}

		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(SPACES);
		covrmjaIO.setCoverage(SPACES);
		covrmjaIO.setRider(SPACES);
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			processCovr7200();
		}

	}

protected void callCsncalc7100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start7110();
				case nextr7180:
					nextr7180();
				case exit7190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7110()
	{
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)
		&& isNE(lifeenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isNE(lifeenqIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(lifeenqIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(lifeenqIO.getStatuz(), varcom.endp)) {
			lifeenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit7190);
		}
		if (isEQ(lifeenqIO.getLife(), wsaaLife)) {
			goTo(GotoLabel.nextr7180);
		}
		wsaaLife.set(lifeenqIO.getLife());
		chdrlnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		chdrlnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		chdrlnbIO.setFunction(varcom.reads);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		csncalcrec.csncalcRec.set(SPACES);
		csncalcrec.function.set("COVR");
		csncalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		csncalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		csncalcrec.life.set(lifeenqIO.getLife());
		csncalcrec.cnttype.set(chdrmjaIO.getCnttype());
		csncalcrec.currency.set(chdrmjaIO.getCntcurr());
		csncalcrec.fsuco.set(wsspcomn.fsuco);
		csncalcrec.language.set(wsspcomn.language);
		csncalcrec.incrAmt.set(ZERO);
		csncalcrec.effdate.set(chdrmjaIO.getPtdate());
		csncalcrec.effdate.set(chdrmjaIO.getPtdate());
		csncalcrec.tranno.set(chdrmjaIO.getTranno());
		csncalcrec.planSuffix.set(ZERO);
		csncalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Csncalc.class, csncalcrec.csncalcRec);
		if (isNE(csncalcrec.statuz, varcom.oK)
		&& isNE(csncalcrec.statuz, "FACL")) {
			syserrrec.statuz.set(csncalcrec.statuz);
			syserrrec.params.set(csncalcrec.csncalcRec);
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.rlse);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
	}

protected void nextr7180()
	{
		lifeenqIO.setFunction(varcom.nextr);
	}

protected void processCovr7200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start7210();
				case nextr7280:
					nextr7280();
				case exit7290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7210()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit7290);
		}
		validateStatus7400();
		if (!validComponent.isTrue()) {
			goTo(GotoLabel.nextr7280);
		}
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(covrmjaIO.getChdrnum());
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		wsaaLifenum.set(lifeenqIO.getLifcnum());
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			wsaaJlifenum.set(lifeenqIO.getLifcnum());
		}
		else {
			wsaaJlifenum.set(SPACES);
		}
		activateRacd7300();
	}

protected void nextr7280()
	{
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void activateRacd7300()
	{
		start7310();
	}

protected void start7310()
	{
		/* Call ACTVRES to activate new RACD records. This will update*/
		/* them from validflag '3' to validflag '1'.*/
		actvresrec.actvresRec.set(SPACES);
		actvresrec.chdrcoy.set(covrmjaIO.getChdrcoy());
		actvresrec.chdrnum.set(covrmjaIO.getChdrnum());
		actvresrec.cnttype.set(chdrmjaIO.getCnttype());
		actvresrec.currency.set(chdrmjaIO.getCntcurr());
		actvresrec.tranno.set(chdrmjaIO.getTranno());
		actvresrec.life.set(covrmjaIO.getLife());
		actvresrec.coverage.set(covrmjaIO.getCoverage());
		actvresrec.rider.set(covrmjaIO.getRider());
		actvresrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		actvresrec.crtable.set(covrmjaIO.getCrtable());
		actvresrec.effdate.set(chdrmjaIO.getPtdate());
		actvresrec.clntcoy.set("9");
		actvresrec.l1Clntnum.set(wsaaLifenum);
		actvresrec.jlife.set(covrmjaIO.getJlife());
		actvresrec.l2Clntnum.set(wsaaJlifenum);
		actvresrec.oldSumins.set(ZERO);
		actvresrec.newSumins.set(covrmjaIO.getSumins());
		actvresrec.function.set("ACT8");
		actvresrec.crrcd.set(covrmjaIO.getCrrcd());
		actvresrec.language.set(wsspcomn.language);
		actvresrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			fatalError600();
		}
	}

protected void validateStatus7400()
	{
		try {
			covrRiskStatus7410();
			covrPremStatus7430();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void covrRiskStatus7410()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		wsaaValidComponent.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isEQ(covrmjaIO.getStatcode(), t5679rec.setCnRiskStat)) {
				wsaaValidComponent.set("Y");
			}
		}
		else {
			if (isEQ(covrmjaIO.getStatcode(), t5679rec.setRidRiskStat)) {
				wsaaValidComponent.set("Y");
			}
		}
		if (!validComponent.isTrue()) {
			goTo(GotoLabel.exit7490);
		}
	}

protected void covrPremStatus7430()
	{
		wsaaValidComponent.set("N");
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrmjaIO.getRider(), "00")) {
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.setSngpCovStat)) {
					wsaaValidComponent.set("Y");
				}
			}
			else {
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.setSngpRidStat)) {
					wsaaValidComponent.set("Y");
				}
			}
		}
		else {
			if (isEQ(covrmjaIO.getRider(), "00")) {
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.setCnPremStat)) {
					wsaaValidComponent.set("Y");
				}
			}
			else {
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.setRidPremStat)) {
					wsaaValidComponent.set("Y");
				}
			}
		}
		if (!validComponent.isTrue()) {
			return ;
		}
	}

protected void printLetter7500()
	{
		start7510();
	}

protected void start7510()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***");
			stringVariable2.addExpression(wsaaBatckey.batcBatctrcde);
			stringVariable2.setStringInto(itemIO.getItemitem());
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				itemIO.setGenarea(SPACES);
			}
			else {
				tr384rec.tr384Rec.set(itemIO.getGenarea());
			}
		}
		else {
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrmjaIO.getChdrpfx());
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdoccoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.despnum.set(chdrmjaIO.getDespnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		letcokcpy.recCode.set("LD");
		letcokcpy.ldDate.set(sv.effdate);
		letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(letrqstrec.statuz);
			syserrrec.params.set(letrqstrec.params);
			fatalError600();
		}
	}

protected void statistic7600()
	{
		start7610();
	}

protected void start7610()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void updatePhrt7700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start7710();
				case update7750:
					update7750();
				case exit7790:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7710()
	{
		if (isEQ(wsspcomn.flag, "M")) {
			goTo(GotoLabel.update7750);
		}
		phrtIO.setParams(SPACES);
		phrtIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		phrtIO.setChdrnum(chdrmjaIO.getChdrnum());
		phrtIO.setDteiss(99999999);
		phrtIO.setPtdate(chdrmjaIO.getPtdate());
		phrtIO.setEffdate(sv.effdate);
		phrtIO.setApind(sv.apind);
		phrtIO.setHprjptdate(sv.hprjptdate);
		phrtIO.setManadj(sv.manadj);
		phrtIO.setTranno(ptrnIO.getTranno());
		phrtIO.setLastTranno(ZERO);
		phrtIO.setFunction(varcom.writr);
		phrtIO.setFormat(formatsInner.phrtrec);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
		goTo(GotoLabel.exit7790);
	}

protected void update7750()
	{
		phrtIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
		phrtIO.setApind(sv.apind);
		phrtIO.setManadj(sv.manadj);
		phrtIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, phrtIO);
		if (isNE(phrtIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(phrtIO.getStatuz());
			syserrrec.params.set(phrtIO.getParams());
			fatalError600();
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present*/
		/* If so, the appropriate parameters are filled and the*/
		/* diary processor is called.*/
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(wsaaToday);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(wsaaToday);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.*/
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(varcom.vrcmMaxDate);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void a100CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A190-EXIT1*/
	}
	//ILIFE-8179
	protected void getBtPtdate() {
		Prmhpf prmhpf;
		prmhpf = prmhpfDAO.getPrmhRecord(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
		int i=1;
		if(!BTPRO028Permission) {
		for(; i<=6; i++)
			if(isEQ(t6654rec.zrfreq[i], chdrmjaIO.getBillfreq()))
				break;
		}
		paidTodate.set(prmhpf.getFrmdate());
		while(isLTE(paidTodate, sv.effdate)) {
			datcon4rec.intDate1.set(paidTodate);
			datcon4rec.billmonth.set(wsaaOccdateMm);
			datcon4rec.billday.set(wsaaOccdateDd);
			datcon4rec.freqFactor.set(t6654rec.zrincr[i].toInt());
			datcon4rec.frequency.set(chdrmjaIO.getBillfreq());
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			paidTodate.set(datcon4rec.intDate2);
		}
		sv.ptdate.set(paidTodate);
		sv.btdate.set(paidTodate);
	}
	
	protected String getNextdate(int freqfact, String intdate1, String freq) {
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(freqfact);
		datcon2Pojo.setIntDate1(intdate1);
		datcon2Pojo.setFrequency(freq);
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		return datcon2Pojo.getIntDate2();
	}
	//ILIFE-8509
	protected void calcProratePrem() {
		BigDecimal totlprm = BigDecimal.ZERO;
		covrpfList = covrpfDAO.searchCovrmjaByChdrnumCoy(chdrmjaIO.getChdrnum().toString(), chdrmjaIO.getChdrcoy().toString());
		if(covrpfList!=null && !covrpfList.isEmpty()) {
			for(Covrpf covr: covrpfList) {
				if(isLTE(sv.effdate, sv.btdate)) {
					key = new StringBuilder().append(covr.getLife()).append(covr.getCoverage()).append(covr.getRider());
					setCalprpmrec(null, covr.getInstprem(), BigDecimal.ZERO);
					callProgram("PRORATECALC", calprpmrec.calprpmRec);
					proratePremMap.put(key.toString(), calprpmrec.totlprem.getbigdata());
				}
				totlprm = totlprm.add(covr.getInstprem());
			}
		}
		if(isLTE(sv.effdate, sv.btdate)) {
			setCalprpmrec(null, totlprm, sv.totalfee.getbigdata());
			callProgram("PRORATECALC", calprpmrec.calprpmRec);
			sv.xamt.set(calprpmrec.totlprem);
			sv.osbal.set(calprpmrec.totlprem);
			sv.totalfee.set(calprpmrec.cntfee);
			sv.osbal.add(sv.totalfee);
			return;
		}
		sv.xamt.set(totlprm);
		sv.osbal.set(totlprm);
		sv.osbal.add(sv.totalfee);
	}
	
	protected void setCalprpmrec(Covrpf covr, BigDecimal premAmt, BigDecimal cntfee) {
		calprpmrec.increasePrem.set(ZERO);
		calprpmrec.zlinstprem.set(ZERO);
		calprpmrec.rerateprem.set(ZERO);
		calprpmrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		calprpmrec.chdrnum.set(chdrmjaIO.getChdrnum());
		calprpmrec.reinstDate.set(sv.effdate);
		calprpmrec.transcode.set(wsaaBatckey.batcBatctrcde);
		calprpmrec.language.set(wsspcomn.language);
		if(isLTE(sv.effdate, sv.btdate)) {
			calprpmrec.lastPTDate.set(getNextdate(-1, sv.btdate.toString(), chdrmjaIO.getBillfreq().toString()));
			calprpmrec.nextPTDate.set(sv.btdate);
			calprpmrec.prem.set(premAmt);
			calprpmrec.cntfee.set(cntfee);
			calprpmrec.cnttype.set(chdrmjaIO.getCnttype());
		}
		else {
			calprpmrec.lastPTDate.set(payrIO.getPtdate());
			calprpmrec.nextPTDate.set(sv.hprjptdate);//sv.hprjptdate
			calprpmrec.uniqueno.set(covr.getUniqueNumber());
		}
	}
	
	protected void updateIncr() {
		if(lapseReinstated) {
			List<Incrpf> incrpfList;
			incrpfList = incrpfDAO.getIncrpfList(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
			if(incrpfList!=null && !incrpfList.isEmpty()) {
				for(int i=0; i<incrpfList.size(); i++) {
					incrpfList.get(i).setValidflag("2");
				}
				incrpfDAO.updateValidFlag(incrpfList);
				for(int i=0; i<incrpfList.size(); i++) {
					incrpfList.get(i).setValidflag("1");
					incrpfList.get(i).setStatcode(chdrmjaIO.getStatcode().toString());
					incrpfList.get(i).setPstatcode(chdrmjaIO.getPstatcode().toString());
					incrpfList.get(i).setTranno(chdrmjaIO.getTranno().toInt());
				}
				incrpfDAO.insertIncrList(incrpfList);
			}
		}
	}
/*
 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
 */
private static final class WsaaTransAreaInner {

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e961 = new FixedLengthStringData(4).init("E961");
	private FixedLengthStringData f136 = new FixedLengthStringData(4).init("F136");
	private FixedLengthStringData f247 = new FixedLengthStringData(4).init("F247");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData h017 = new FixedLengthStringData(4).init("H017");
	private FixedLengthStringData h134 = new FixedLengthStringData(4).init("H134");
	private FixedLengthStringData o001 = new FixedLengthStringData(4).init("O001");
	private FixedLengthStringData rlbr = new FixedLengthStringData(4).init("RLBR");
	private FixedLengthStringData rlbs = new FixedLengthStringData(4).init("RLBS");
	private FixedLengthStringData s106 = new FixedLengthStringData(4).init("S106");
	private FixedLengthStringData w085 = new FixedLengthStringData(4).init("W085");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData rrrh = new FixedLengthStringData(4).init("RRRH");	//ILIFE-8179
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
	private FixedLengthStringData t5661 = new FixedLengthStringData(5).init("T5661");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	private FixedLengthStringData t6661 = new FixedLengthStringData(5).init("T6661");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData tr51p = new FixedLengthStringData(5).init("TR51P");
	private FixedLengthStringData tr686 = new FixedLengthStringData(5).init("TR686");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData ta524 = new FixedLengthStringData(5).init("TA524");	//ILIFE-8179
	private FixedLengthStringData t6654 = new FixedLengthStringData(5).init("T6654");	//ILIFE-8179

}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData lifeenqrec = new FixedLengthStringData(10).init("LIFEENQREC");
	private FixedLengthStringData prmhactrec = new FixedLengthStringData(10).init("PRMHACTREC");
	private FixedLengthStringData phrtrec = new FixedLengthStringData(10).init("PHRTREC");
	private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}