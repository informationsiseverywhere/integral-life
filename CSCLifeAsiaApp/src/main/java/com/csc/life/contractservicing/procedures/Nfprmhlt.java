/*
 * File: Nfprmhl.java
 * Date: 29 August 2009 23:00:33
 * Author: Quipoz Limited
 * 
 * Class transformed from NFPRMHL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.AcmvldgTableDAM;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.life.contractservicing.dataaccess.PrmhenqTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.reports.Rr51qReport;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.dataaccess.UtrnrnlTableDAM;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Auto Premium Holiday
*
*   Non forfeiture routine for Premium Holiday.
*   This subroutine will write record in PRMHPF file when process
*   is successful. For fund value is insufficient to cover number
*   of COI that required by TR51P's set up, Premium Holiday Exception
*   report will be printed out.
*
***********************************************************************
* </pre>
*/
public class Nfprmhlt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "NFPRMHLT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private static final String ta524 = "TA524";
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Ta524rec ta524rec = new Ta524rec();
	private Trmreasrec trmreasrec = new Trmreasrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Prmhpf prmhpf;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO", PrmhpfDAO.class);
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);

	public Nfprmhlt() {
		super();
	}

	/**
	* The mainline method is the default entry point to the class
	*/
	public void mainline(Object... parmArray) {
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
			//Expected exception for control flow purposes
		}
	}
	
	protected void mainline100() {
		para110();
		exit190();
	}
	
	protected void para110() {
		ovrduerec.newSumins.set(ovrduerec.sumins);
		ovrduerec.statuz.set(varcom.oK);
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		readTa524();
		writePrmh2000();
		processReassurance3000();
	}
	
	protected void exit190() {
		exitProgram();
	}
	
	protected void readTa524() {
		Itempf itempf;
		itempf = itempfDAO.findItemByDate("IT", ovrduerec.chdrcoy.toString(), ta524, ovrduerec.cnttype.toString(), ovrduerec.effdate.toString(), "1");
		if(itempf != null) {
			ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}
	
	protected void writePrmh2000() {
		try {
			prmhpf = prmhpfDAO.getPrmhenqRecord(ovrduerec.chdrcoy.toString(), ovrduerec.chdrnum.toString());
			if(prmhpf != null && prmhpf.getTranno() == ovrduerec.tranno.toInt()) {
				exit190();
			}
			prmhpf  = new Prmhpf();
			prmhpf.setChdrcoy(ovrduerec.chdrcoy.toString());
			prmhpf.setChdrnum(ovrduerec.chdrnum.toString());
			prmhpf.setValidflag("1");
			prmhpf.setActind("A");
			prmhpf.setFrmdate(ovrduerec.ptdate.toInt());
			prmhpf.setTodate(Integer.parseInt(getTodate()));
			prmhpf.setTranno(ovrduerec.tranno.toInt());
			prmhpf.setEffdate(datcon1rec.intDate.toInt());
			prmhpf.setCrtuser(SPACE);
			prmhpf.setApind(SPACE);
			if (isEQ(ovrduerec.function, "OLPT")) {
				prmhpf.setLogtype("C");
			}
			else {
				prmhpf.setLogtype("A");
			}
			prmhpf.setBillfreq(SPACE);
			prmhpfDAO.insertPrmhenqRecord(prmhpf);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}
	
	protected String getTodate() {
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(ta524rec.maxphprd.toInt());
		datcon2Pojo.setIntDate1(String.valueOf(prmhpf.getFrmdate()));
		datcon2Pojo.setFrequency("12");
		datcon2Utils.calDatcon2(datcon2Pojo);
	
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		
		return datcon2Pojo.getIntDate2();
	}
	
	protected void processReassurance3000() {
		trmreas3010();
	}
	
	protected void trmreas3010() {
		wsaaBatckey.batcBatcpfx.set("BA");
		wsaaBatckey.batcBatccoy.set(ovrduerec.chdrcoy);
		wsaaBatckey.batcBatcbrn.set(ovrduerec.batcbrn);
		wsaaBatckey.batcBatcactyr.set(ovrduerec.acctyear);
		wsaaBatckey.batcBatcactmn.set(ovrduerec.acctmonth);
		wsaaBatckey.batcBatctrcde.set(ovrduerec.trancode);
		wsaaBatckey.batcBatcbatch.set(ovrduerec.batcbatch);
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("CHGR");
		trmreasrec.chdrcoy.set(ovrduerec.chdrcoy);
		trmreasrec.chdrnum.set(ovrduerec.chdrnum);
		trmreasrec.life.set(ovrduerec.life);
		trmreasrec.coverage.set(ovrduerec.coverage);
		trmreasrec.rider.set(ovrduerec.rider);
		trmreasrec.planSuffix.set(ovrduerec.planSuffix);
		trmreasrec.cnttype.set(ovrduerec.cnttype);
		trmreasrec.crtable.set(ovrduerec.crtable);
		trmreasrec.polsum.set(ovrduerec.polsum);
		trmreasrec.effdate.set(ovrduerec.ptdate);
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(ovrduerec.tranno);
		trmreasrec.language.set(ovrduerec.language);
		trmreasrec.billfreq.set(ovrduerec.billfreq);
		trmreasrec.ptdate.set(ovrduerec.ptdate);
		trmreasrec.origcurr.set(ovrduerec.cntcurr);
		trmreasrec.acctcurr.set(ovrduerec.cntcurr);
		trmreasrec.crrcd.set(ovrduerec.crrcd);
		trmreasrec.convUnits.set(ZERO);
		trmreasrec.jlife.set(SPACES);
		trmreasrec.singp.set(ovrduerec.instprem);
		trmreasrec.pstatcode.set(ovrduerec.pstatcode);
		trmreasrec.oldSumins.set(ovrduerec.sumins);
		trmreasrec.newSumins.set(ovrduerec.newSumins);
		trmreasrec.clmPercent.set(ZERO);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			fatalError600();
		}
	}
	
	protected void fatalError600() {
		start610();
		errorBomb620();
	}
	
	protected void start610() {
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}
	
	protected void errorBomb620() {
		ovrduerec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
