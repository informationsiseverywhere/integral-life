package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5154screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5154ScreenVars sv = (S5154ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5154screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5154ScreenVars screenVars = (S5154ScreenVars)pv;
		/*screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.zbinstprem.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubsect.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.frqdesc.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.riskCessAge.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		screenVars.premCessDateDisp.setClassString("");
		screenVars.benCessAge.setClassString("");
		screenVars.benCessTerm.setClassString("");
		screenVars.benCessDateDisp.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.bappmeth.setClassString("");
		screenVars.optextind.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.anntind.setClassString("");
		screenVars.comind.setClassString("");
		screenVars.select.setClassString("");
		screenVars.pbind.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.taxind.setClassString("");
		 BRD-306 START 
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.loadper.setClassString("");
		screenVars.rateadj.setClassString("");
		screenVars.fltmort.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.adjustageamt.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.linstamt.setClassString("");
		 BRD-306 END 
		screenVars.zstpduty01.setClassString("");
		screenVars.waitperiod.setClassString("");
		screenVars.bentrm.setClassString("");
		screenVars.poltyp.setClassString("");
		screenVars.prmbasis.setClassString("");
		screenVars.statcode.setClassString("");//BRD-009
		screenVars.dialdownoption.setClassString("");//BRD-NBP-011
		screenVars.exclind.setClassString("");*/
		
		ScreenRecord.setClassStringFormatting(pv);	
	}

/**
 * Clear all the variables in S5154screen
 */
	public static void clear(VarModel pv) {
		S5154ScreenVars screenVars = (S5154ScreenVars) pv;
		/*screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.zbinstprem.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.zagelit.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubsect.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.numpols.clear();
		screenVars.planSuffix.clear();
		screenVars.sumin.clear();
		screenVars.frqdesc.clear();
		screenVars.currcd.clear();
		screenVars.riskCessAge.clear();
		screenVars.riskCessTerm.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		screenVars.premCessDateDisp.clear();
		screenVars.premCessDate.clear();
		screenVars.benCessAge.clear();
		screenVars.benCessTerm.clear();
		screenVars.benCessDateDisp.clear();
		screenVars.benCessDate.clear();
		screenVars.mortcls.clear();
		screenVars.liencd.clear();
		screenVars.bappmeth.clear();
		screenVars.optextind.clear();
		screenVars.instPrem.clear();
		screenVars.anntind.clear();
		screenVars.comind.clear();
		screenVars.select.clear();
		screenVars.pbind.clear();
		screenVars.zlinstprem.clear();
		screenVars.taxamt.clear();
		screenVars.taxind.clear();
		 BRD-306 START 
		screenVars.effdateDisp.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.loadper.clear();
		screenVars.rateadj.clear();
		screenVars.fltmort.clear();
		screenVars.premadj.clear();
		screenVars.adjustageamt.clear();
		screenVars.linstamt.clear();
		 BRD-306 END 
		screenVars.zstpduty01.clear();
		screenVars.waitperiod.clear();
		screenVars.bentrm.clear();
		screenVars.poltyp.clear();
		screenVars.prmbasis.clear();
		screenVars.statcode.clear();//BRD-009
		screenVars.dialdownoption.clear(); //BRD-NBP-011
		screenVars.exclind.clear();*/
		ScreenRecord.clear(pv);	
	}
}
