package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: TaxdpfTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:47
 * Class transformed from TAXDPF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class TaxdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 167;
	public FixedLengthStringData taxdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData taxdpfRecord = taxdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(taxdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(taxdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(taxdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(taxdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(taxdrec);
	public PackedDecimalData plansfx = DD.plansfx.copy().isAPartOf(taxdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(taxdrec);
	public FixedLengthStringData tranref = DD.tranref.copy().isAPartOf(taxdrec);
	public PackedDecimalData instfrom = DD.instfrom.copy().isAPartOf(taxdrec);
	public PackedDecimalData instto = DD.instto.copy().isAPartOf(taxdrec);
	public PackedDecimalData billcd = DD.billcd.copy().isAPartOf(taxdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(taxdrec);
	public PackedDecimalData baseamt = DD.baseamt.copy().isAPartOf(taxdrec);
	public FixedLengthStringData trantype = DD.trantype.copy().isAPartOf(taxdrec);
	public PackedDecimalData taxamt01 = DD.taxamt.copy().isAPartOf(taxdrec);
	public PackedDecimalData taxamt02 = DD.taxamt.copy().isAPartOf(taxdrec);
	public PackedDecimalData taxamt03 = DD.taxamt.copy().isAPartOf(taxdrec);
	public FixedLengthStringData txabsind01 = DD.txabsind.copy().isAPartOf(taxdrec);
	public FixedLengthStringData txabsind02 = DD.txabsind.copy().isAPartOf(taxdrec);
	public FixedLengthStringData txabsind03 = DD.txabsind.copy().isAPartOf(taxdrec);
	public FixedLengthStringData txtype01 = DD.txtype.copy().isAPartOf(taxdrec);
	public FixedLengthStringData txtype02 = DD.txtype.copy().isAPartOf(taxdrec);
	public FixedLengthStringData txtype03 = DD.txtype.copy().isAPartOf(taxdrec);
	public FixedLengthStringData postflg = DD.postflg.copy().isAPartOf(taxdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(taxdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(taxdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(taxdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public TaxdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for TaxdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public TaxdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for TaxdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public TaxdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for TaxdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public TaxdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("TAXDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLANSFX, " +
							"EFFDATE, " +
							"TRANREF, " +
							"INSTFROM, " +
							"INSTTO, " +
							"BILLCD, " +
							"TRANNO, " +
							"BASEAMT, " +
							"TRANTYPE, " +
							"TAXAMT01, " +
							"TAXAMT02, " +
							"TAXAMT03, " +
							"TXABSIND01, " +
							"TXABSIND02, " +
							"TXABSIND03, " +
							"TXTYPE01, " +
							"TXTYPE02, " +
							"TXTYPE03, " +
							"POSTFLG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     plansfx,
                                     effdate,
                                     tranref,
                                     instfrom,
                                     instto,
                                     billcd,
                                     tranno,
                                     baseamt,
                                     trantype,
                                     taxamt01,
                                     taxamt02,
                                     taxamt03,
                                     txabsind01,
                                     txabsind02,
                                     txabsind03,
                                     txtype01,
                                     txtype02,
                                     txtype03,
                                     postflg,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		plansfx.clear();
  		effdate.clear();
  		tranref.clear();
  		instfrom.clear();
  		instto.clear();
  		billcd.clear();
  		tranno.clear();
  		baseamt.clear();
  		trantype.clear();
  		taxamt01.clear();
  		taxamt02.clear();
  		taxamt03.clear();
  		txabsind01.clear();
  		txabsind02.clear();
  		txabsind03.clear();
  		txtype01.clear();
  		txtype02.clear();
  		txtype03.clear();
  		postflg.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getTaxdrec() {
  		return taxdrec;
	}

	public FixedLengthStringData getTaxdpfRecord() {
  		return taxdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setTaxdrec(what);
	}

	public void setTaxdrec(Object what) {
  		this.taxdrec.set(what);
	}

	public void setTaxdpfRecord(Object what) {
  		this.taxdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(taxdrec.getLength());
		result.set(taxdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}