package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:58
 * Description:
 * Copybook name: INCRBK1KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrbk1key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incrbk1FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incrbk1Key = new FixedLengthStringData(64).isAPartOf(incrbk1FileKey, 0, REDEFINE);
  	public FixedLengthStringData incrbk1Chdrcoy = new FixedLengthStringData(1).isAPartOf(incrbk1Key, 0);
  	public FixedLengthStringData incrbk1Chdrnum = new FixedLengthStringData(8).isAPartOf(incrbk1Key, 1);
  	public PackedDecimalData incrbk1PlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incrbk1Key, 9);
  	public FixedLengthStringData incrbk1Life = new FixedLengthStringData(2).isAPartOf(incrbk1Key, 12);
  	public FixedLengthStringData incrbk1Coverage = new FixedLengthStringData(2).isAPartOf(incrbk1Key, 14);
  	public FixedLengthStringData incrbk1Rider = new FixedLengthStringData(2).isAPartOf(incrbk1Key, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(incrbk1Key, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incrbk1FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incrbk1FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}