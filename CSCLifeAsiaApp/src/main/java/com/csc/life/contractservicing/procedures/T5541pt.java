/*
 * File: T5541pt.java
 * Date: 30 August 2009 2:22:15
 * Author: Quipoz Limited
 * 
 * Class transformed from T5541PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5541.
*
*
*****************************************************************
* </pre>
*/
public class T5541pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Frequency Alteration Basis                     S5541");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(50);
	private FixedLengthStringData filler9 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine004, 18, FILLER).init("Frequency         Loading Factor");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(46);
	private FixedLengthStringData filler11 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 21);
	private FixedLengthStringData filler12 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine005, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine005, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(46);
	private FixedLengthStringData filler13 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 21);
	private FixedLengthStringData filler14 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine006, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(46);
	private FixedLengthStringData filler15 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 21);
	private FixedLengthStringData filler16 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine007, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(46);
	private FixedLengthStringData filler17 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 21);
	private FixedLengthStringData filler18 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine008, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(46);
	private FixedLengthStringData filler19 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 21);
	private FixedLengthStringData filler20 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine009, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(46);
	private FixedLengthStringData filler21 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 21);
	private FixedLengthStringData filler22 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine010, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(46);
	private FixedLengthStringData filler23 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 21);
	private FixedLengthStringData filler24 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine011, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(46);
	private FixedLengthStringData filler25 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 21);
	private FixedLengthStringData filler26 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine012, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(46);
	private FixedLengthStringData filler27 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 21);
	private FixedLengthStringData filler28 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine013, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(46);
	private FixedLengthStringData filler29 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 21);
	private FixedLengthStringData filler30 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine014, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(46);
	private FixedLengthStringData filler31 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 21);
	private FixedLengthStringData filler32 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine015, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine015, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(46);
	private FixedLengthStringData filler33 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 21);
	private FixedLengthStringData filler34 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine016, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 4).isAPartOf(wsaaPrtLine016, 40).setPattern("Z.ZZZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(28);
	private FixedLengthStringData filler35 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5541rec t5541rec = new T5541rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5541pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5541rec.t5541Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo008.set(t5541rec.lfact01);
		fieldNo010.set(t5541rec.lfact02);
		fieldNo012.set(t5541rec.lfact03);
		fieldNo014.set(t5541rec.lfact04);
		fieldNo016.set(t5541rec.lfact05);
		fieldNo018.set(t5541rec.lfact06);
		fieldNo020.set(t5541rec.lfact07);
		fieldNo022.set(t5541rec.lfact08);
		fieldNo024.set(t5541rec.lfact09);
		fieldNo026.set(t5541rec.lfact10);
		fieldNo028.set(t5541rec.lfact11);
		fieldNo030.set(t5541rec.lfact12);
		fieldNo007.set(t5541rec.freqcy01);
		fieldNo009.set(t5541rec.freqcy02);
		fieldNo011.set(t5541rec.freqcy03);
		fieldNo013.set(t5541rec.freqcy04);
		fieldNo015.set(t5541rec.freqcy05);
		fieldNo017.set(t5541rec.freqcy06);
		fieldNo019.set(t5541rec.freqcy07);
		fieldNo021.set(t5541rec.freqcy08);
		fieldNo023.set(t5541rec.freqcy09);
		fieldNo025.set(t5541rec.freqcy10);
		fieldNo027.set(t5541rec.freqcy11);
		fieldNo029.set(t5541rec.freqcy12);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
