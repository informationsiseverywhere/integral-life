package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LhdrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:43
 * Class transformed from LHDRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LhdrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 43;
	public FixedLengthStringData lhdrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData lhdrpfRecord = lhdrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(lhdrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(lhdrrec);
	public FixedLengthStringData loanType = DD.loantype.copy().isAPartOf(lhdrrec);
	public FixedLengthStringData loanCurrency = DD.loancurr.copy().isAPartOf(lhdrrec);
	public PackedDecimalData loanOriginalAmount = DD.loanorigam.copy().isAPartOf(lhdrrec);
	public PackedDecimalData loanStartDate = DD.loanstdate.copy().isAPartOf(lhdrrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(lhdrrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(lhdrrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(lhdrrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(lhdrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LhdrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for LhdrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LhdrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LhdrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LhdrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LhdrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LhdrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LHDRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LOANTYPE, " +
							"LOANCURR, " +
							"LOANORIGAM, " +
							"LOANSTDATE, " +
							"TERMID, " +
							"USER_T, " +
							"TRDT, " +
							"TRTM, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     loanType,
                                     loanCurrency,
                                     loanOriginalAmount,
                                     loanStartDate,
                                     termid,
                                     user,
                                     transactionDate,
                                     transactionTime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		loanType.clear();
  		loanCurrency.clear();
  		loanOriginalAmount.clear();
  		loanStartDate.clear();
  		termid.clear();
  		user.clear();
  		transactionDate.clear();
  		transactionTime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLhdrrec() {
  		return lhdrrec;
	}

	public FixedLengthStringData getLhdrpfRecord() {
  		return lhdrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLhdrrec(what);
	}

	public void setLhdrrec(Object what) {
  		this.lhdrrec.set(what);
	}

	public void setLhdrpfRecord(Object what) {
  		this.lhdrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(lhdrrec.getLength());
		result.set(lhdrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}