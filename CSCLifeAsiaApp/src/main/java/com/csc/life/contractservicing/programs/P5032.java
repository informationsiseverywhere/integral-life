/*
 * File: P5032.java
 * Date: 29 August 2009 23:58:26
 * Author: Quipoz Limited
 * 
 * Class transformed from P5032.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.screens.S5032ScreenVars;
import com.csc.life.interestbearing.dataaccess.HitrrnlTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnextTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*               P5032 - Automatic Increase Refusal Submenu.
*               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Initialise
* ----------
*
* Initialise the screen for display.
*
* Validation
* ----------
*
* Validate contract number  entered by  performing  a READR on
* the Logical file CHDRMJA.
*
*   Validate Key - 1
*
*    If S5032-CHDRSEL not  equal  to  spaces call the CHDRMJA
*    I/O module for  the  contract.  If  any  combination  of
*    CHDRSEL and SUBP-KEY1 other than O-K and 'Y' then error.
*
*    Check STATCODE to validate  status of the contract is In
*    Force (IF), against T5679.
*
*    If NOT valid then Error.
*
*   Validate Key - 2
*
*    If  the  screen  effective  date  has  changed  then
*    validate this date against the header record (CHDRMJA).
*
* The  UDEL  file  is  checked  to  make  sure  there  are  no
* unprocessed  UTRNs pending for the contract we are trying to
* work with.
*
* Updating
* --------
*
* Softlock the contract header by calling SFTLOCK.
*
* If a valid contract header record found then perform a KEEPS
* on  the  record  in  order  to  store  it  for  use in later
* programs.
*
* Next Program.
* -------------
*
* Add 1 to the Program pointer and exit if not 'BACH' facility
* selected.
*
*****************************************************************
* </pre>
*/
public class P5032 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5032");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaStatFlag = new FixedLengthStringData(1);
	private Validator wsaaExitStat = new Validator(wsaaStatFlag, "Y");
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e455 = "E455";
	private static final String e544 = "E544";
	private static final String e767 = "E767";
	private static final String f910 = "F910";
	private static final String i030 = "I030";
	private static final String hl08 = "HL08";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrsurrec = "CHDRSURREC";
	private static final String hitrrnlrec = "HITRRNLREC";
		/*    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private HitrrnlTableDAM hitrrnlIO = new HitrrnlTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnextTableDAM utrnextIO = new UtrnextTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Subprogrec subprogrec = new Subprogrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private S5032ScreenVars sv = ScreenProgram.getScreenVars( S5032ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2590, 
		keeps3070, 
		exit3090
	}

	public P5032() {
		super();
		screenVars = sv;
		new ScreenModel("S5032", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		/*    Initialise screen.*/
		sv.dataArea.set(SPACES);
		wsspcomn.flag.set(SPACES);
		wsaaStatFlag.set("N");
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S5032IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5032-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/*    Validate all fields including batch request if required.*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, varcom.bach)) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2500();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		/*    Call subprog routine to load the stack with the next*/
		/*    programs.*/
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		/*    Call sanctn routine to check that the user has been*/
		/*    sanctioned to execute Journaling.*/
		sanctnrec.function.set(varcom.subm);
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		validateKey12210();
	}

protected void validateKey12210()
	{
		/*    Read the Header record for the contract number entered.*/
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrmjaIO);
		}
		else {
			chdrmjaIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*    If no record found and Key1 in T1690 is set to 'Y' then*/
		/*    error.*/
		if (isEQ(chdrmjaIO.getStatuz(), varcom.mrnf)
		&& isEQ(subprogrec.key1, "Y")) {
			sv.chdrselErr.set(e544);
			wsspcomn.edterror.set("Y");
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		/*    Validate contract selected is of the correct status and*/
		/*    from the correct branch.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.action, "A")
		&& isEQ(sv.chdrselErr, SPACES)) {
			checkStatus2300();
		}
		/*    If incorrect Branch then error.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)
		&& isNE(wsspcomn.branch, chdrmjaIO.getCntbranch())) {
			sv.chdrselErr.set(e455);
		}
		/*    Billed to Date must equal Paid to Date if transaction*/
		/*    is to be actioned.*/
		/* Check the UTRN file to ensure that no unprocessed UTRN is*/
		/* pending for the specified contract.*/
		/* If a record is found on the UTRN file, do not allow the*/
		/* transaction to go ahead.*/
		/* Only check for unprocessed UTRN's if a valid Contract Number*/
		/* has been entered.*/
		if ((isEQ(subprogrec.key1, "Y"))
		&& (isEQ(sv.chdrselErr, SPACES))) {
			checkUdel2600();
			checkHitr2700();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkStatus2300()
	{
		readStatusTable2310();
	}

protected void readStatusTable2310()
	{
		/*    Read the Status table to check it is an in force contract.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaSub.set(ZERO);
		wsaaStatFlag.set("N");
		/*    Look through the statii.*/
		while ( !(wsaaExitStat.isTrue())) {
			lookForStat2400();
		}
		
	}

protected void lookForStat2400()
	{
		/*SEARCH*/
		/*    Table Loop.*/
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(e767);
			wsaaStatFlag.set("Y");
		}
		else {
			if (isNE(chdrmjaIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaStatFlag.set("N");
			}
			else {
				wsaaStatFlag.set("Y");
			}
		}
		/*EXIT*/
	}

protected void verifyBatchControl2500()
	{
		try {
			validateRequest2510();
			retrieveBatchProgs2520();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2510()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit2590);
		}
	}

protected void retrieveBatchProgs2520()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void checkUdel2600()
	{
		go2610();
	}

protected void go2610()
	{
		utrnextIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		utrnextIO.setChdrnum(chdrmjaIO.getChdrnum());
		utrnextIO.setProcSeqNo(ZERO);
		utrnextIO.setUnitVirtualFund(SPACES);
		utrnextIO.setUnitType(SPACES);
		utrnextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, utrnextIO);
		if (isEQ(utrnextIO.getStatuz(), varcom.endp)
		|| isNE(utrnextIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(utrnextIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			return ;
		}
		else {
			if (isNE(utrnextIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(utrnextIO.getParams());
				fatalError600();
			}
			else {
				sv.chdrselErr.set(i030);
			}
		}
	}

protected void checkHitr2700()
	{
		hitr2710();
	}

protected void hitr2710()
	{
		/*    Check that the contract has no unprocessed HITRs pending.    */
		hitrrnlIO.setParams(SPACES);
		hitrrnlIO.setChdrcoy(wsspcomn.company);
		hitrrnlIO.setChdrnum(chdrmjaIO.getChdrnum());
		hitrrnlIO.setFormat(hitrrnlrec);
		hitrrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrrnlIO);
		if (isNE(hitrrnlIO.getStatuz(), varcom.oK)
		&& isNE(hitrrnlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hitrrnlIO.getParams());
			fatalError600();
		}
		if (isEQ(hitrrnlIO.getStatuz(), varcom.oK)) {
			sv.chdrselErr.set(hl08);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
					softLock3030();
				case keeps3070: 
					keeps3070();
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		/*    Update the WSSP storage area.*/
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, varcom.bach)) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isNE(sv.errorIndicators, SPACES)) {
			batching3080();
			goTo(GotoLabel.exit3090);
		}
		/*    If inquiry option selected then we do not need the use of*/
		/*    soft-lock.*/
		if (isEQ(sv.action, "B")) {
			wsspcomn.flag.set("I");
			goTo(GotoLabel.keeps3070);
		}
	}

protected void softLock3030()
	{
		/*    Soft lock the contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void keeps3070()
	{
		/*   Store the contract header for use by the transaction programs*/
		chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*   To utilise the plan processing screen P5015, it is necessary t*/
		/*   store CHDRSUR.*/
		chdrsurIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		chdrsurIO.setChdrnum(chdrmjaIO.getChdrnum());
		chdrsurIO.setFunction(varcom.readr);
		chdrsurIO.setFormat(chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		chdrsurIO.setFunction(varcom.keeps);
		chdrsurIO.setFormat(chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
	}

protected void batching3080()
	{
		/*    Update Batch control if required.*/
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, varcom.bach)) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
