package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:11
 * Description:
 * Copybook name: RTRNREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rtrnrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData rtrnrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData rtrnrevKey = new FixedLengthStringData(64).isAPartOf(rtrnrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData rtrnrevRldgcoy = new FixedLengthStringData(1).isAPartOf(rtrnrevKey, 0);
  	public FixedLengthStringData rtrnrevRdocnum = new FixedLengthStringData(9).isAPartOf(rtrnrevKey, 1);
  	public PackedDecimalData rtrnrevTranno = new PackedDecimalData(5, 0).isAPartOf(rtrnrevKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(rtrnrevKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(rtrnrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rtrnrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}