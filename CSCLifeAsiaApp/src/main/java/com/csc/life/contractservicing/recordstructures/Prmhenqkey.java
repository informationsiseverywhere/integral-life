package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:04
 * Description:
 * Copybook name: PRMHENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Prmhenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData prmhenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData prmhenqKey = new FixedLengthStringData(256).isAPartOf(prmhenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData prmhenqChdrcoy = new FixedLengthStringData(1).isAPartOf(prmhenqKey, 0);
  	public FixedLengthStringData prmhenqChdrnum = new FixedLengthStringData(8).isAPartOf(prmhenqKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(prmhenqKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(prmhenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		prmhenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}