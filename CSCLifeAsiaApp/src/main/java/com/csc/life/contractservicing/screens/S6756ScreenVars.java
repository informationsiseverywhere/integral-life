package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6756
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6756ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(278);
	public FixedLengthStringData dataFields = new FixedLengthStringData(102).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData contractType = DD.cntyp.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,22);
	public ZonedDecimalData efdate = DD.efdate.copyToZonedDecimal().isAPartOf(dataFields,52);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,60);
	public FixedLengthStringData shortdss = new FixedLengthStringData(30).isAPartOf(dataFields, 68);
	public FixedLengthStringData[] shortds = FLSArrayPartOfStructure(3, 10, shortdss, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(shortdss, 0, FILLER_REDEFINE);
	public FixedLengthStringData shortds01 = DD.shortds.copy().isAPartOf(filler,0);
	public FixedLengthStringData shortds02 = DD.shortds.copy().isAPartOf(filler,10);
	public FixedLengthStringData shortds03 = DD.shortds.copy().isAPartOf(filler,20);
	public FixedLengthStringData tranCode = DD.trncde.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 102);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData efdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData shortdssErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] shortdsErr = FLSArrayPartOfStructure(3, 4, shortdssErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(shortdssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData shortds01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData shortds02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData shortds03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData trncdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 146);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] efdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData shortdssOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(3, 12, shortdssOut, 0);
	public FixedLengthStringData[][] shortdsO = FLSDArrayPartOfArrayStructure(12, 1, shortdsOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(shortdssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] shortds01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] shortds02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] shortds03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] trncdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData efdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S6756screenWritten = new LongData(0);
	public LongData S6756protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6756ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(efdateOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trncdeOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, contractType, shortds02, shortds01, shortds03, cnstcur, efdate, tranCode, descrip, ptdate, btdate};
		screenOutFields = new BaseData[][] {chdrnumOut, cntypOut, shortds02Out, shortds01Out, shortds03Out, cnstcurOut, efdateOut, trncdeOut, descripOut, ptdateOut, btdateOut};
		screenErrFields = new BaseData[] {chdrnumErr, cntypErr, shortds02Err, shortds01Err, shortds03Err, cnstcurErr, efdateErr, trncdeErr, descripErr, ptdateErr, btdateErr};
		screenDateFields = new BaseData[] {efdate, ptdate, btdate};
		screenDateErrFields = new BaseData[] {efdateErr, ptdateErr, btdateErr};
		screenDateDispFields = new BaseData[] {efdateDisp, ptdateDisp, btdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6756screen.class;
		protectRecord = S6756protect.class;
	}

}
