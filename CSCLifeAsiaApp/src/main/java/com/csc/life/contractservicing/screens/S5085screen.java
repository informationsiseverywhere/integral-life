package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5085screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5085ScreenVars sv = (S5085ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5085screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5085ScreenVars screenVars = (S5085ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.srcebus.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.reptype.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.instpramt.setClassString("");
		screenVars.lapind.setClassString("");
		screenVars.lapsfromDisp.setClassString("");
		screenVars.lapstoDisp.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.billind.setClassString("");
		screenVars.billfromDisp.setClassString("");
		screenVars.billtoDisp.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.notind.setClassString("");
		screenVars.notsfromDisp.setClassString("");
		screenVars.notstoDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.comind.setClassString("");
		screenVars.commfromDisp.setClassString("");
		screenVars.commtoDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.bnsind.setClassString("");
		screenVars.bnsfromDisp.setClassString("");
		screenVars.bnstoDisp.setClassString("");
		screenVars.renind.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.currtoDisp.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.resndesc.setClassString("");
	}

/**
 * Clear all the variables in S5085screen
 */
	public static void clear(VarModel pv) {
		S5085ScreenVars screenVars = (S5085ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.srcebus.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.reptype.clear();
		screenVars.register.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.instpramt.clear();
		screenVars.lapind.clear();
		screenVars.lapsfromDisp.clear();
		screenVars.lapsfrom.clear();
		screenVars.lapstoDisp.clear();
		screenVars.lapsto.clear();
		screenVars.cntcurr.clear();
		screenVars.billfreq.clear();
		screenVars.billind.clear();
		screenVars.billfromDisp.clear();
		screenVars.billfrom.clear();
		screenVars.billtoDisp.clear();
		screenVars.billto.clear();
		screenVars.mop.clear();
		screenVars.notind.clear();
		screenVars.notsfromDisp.clear();
		screenVars.notsfrom.clear();
		screenVars.notstoDisp.clear();
		screenVars.notsto.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.comind.clear();
		screenVars.commfromDisp.clear();
		screenVars.commfrom.clear();
		screenVars.commtoDisp.clear();
		screenVars.commto.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.bnsind.clear();
		screenVars.bnsfromDisp.clear();
		screenVars.bnsfrom.clear();
		screenVars.bnstoDisp.clear();
		screenVars.bnsto.clear();
		screenVars.renind.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.currtoDisp.clear();
		screenVars.currto.clear();
		screenVars.reasoncd.clear();
		screenVars.resndesc.clear();
	}
}
