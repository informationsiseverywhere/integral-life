/*
 * File: P6258.java
 * Date: 30 August 2009 0:39:54
 * Author: Quipoz Limited
 * 
 * Class transformed from P6258.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.screens.S6258ScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Pay to Date Advance main-line front-end to the renewal type
* processing of the pay to date advance AT module.
*
* This program displays relevent information about the contract
* and accepts the pay-to-date to which the contract should be
* advanced to.
*
* Validation :
*
* ! The pay-to-date Advance must be entered.
*
* ! The date is greater than the bill to date and equally
*   the pay-to-date on the contract (after 'windback').
*
* ! The number of frequencies (ie number of months if
*   the billing frequency is 12, etc) must be a whole number.
*   This can be calculated as  the absolute difference between
*   the  bill to date and the date  entered using  the DATCON3
*   subroutine and checking that it is an integer.
*
* If there is  a paid to date advance  performed on a contract
* there  is  the possibility that the contract may  be due for
* increase at a  point  during this  process.  As the  correct
* percentage increase may not be available at this time  it is
* proposed that the following occur when PTD Advance is taking
* place:-
*
* - The  next increase   date  will  be  displayed  on the PTD
*   Advance screen.
*
* - If  the  date  chosen to  advance to  is past the increase
*   date the transaction can not be performed.
*
* In order  for  increases  to  be offered to a contract where
* the PTD Advance  advances past an Increase date  some manual
* process will need to be introduced.
*
* a) Advance PTD to Increase Date
*
* b) Refuse Increase (new online transaction)
*
*    Run batch job to process increase
*
* c) Perform PTD Advance transaction.
*
* After successful entry of the date the softlocked contract
* should be passed to the AT module using a softlock
* function of TOAT, before the AT module is called.
*
*****************************************************************
* </pre>
*/
public class P6258 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6258");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaOutstAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaNetPrem = new PackedDecimalData(17, 2);
		/* WSAA-FREQ-FACTOR-WS */
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(11, 5).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(11).isAPartOf(wsaaFreqFactor, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaFreqFactorRem = new ZonedDecimalData(5, 0).isAPartOf(filler, 6).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);
	private FixedLengthStringData filler2 = new FixedLengthStringData(27).isAPartOf(wsaaPrimaryKey, 9, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(199);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 5);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 9);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 13);
	private PackedDecimalData wsaaAdvptdat = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 17);
	private PackedDecimalData wsaaNoOfFreqs = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 22).setUnsigned();
	private PackedDecimalData wsaaUnitEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 26);
	private PackedDecimalData wsaaApaRequired = new PackedDecimalData(15, 2).isAPartOf(wsaaTransArea, 31);
	private FixedLengthStringData filler3 = new FixedLengthStringData(160).isAPartOf(wsaaTransArea, 39, FILLER).init(SPACES);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator statusValid = new Validator(wsaaValidStatus, "Y");
	private Validator statusNotValid = new Validator(wsaaValidStatus, "N");
	private String wsaaCovrValid = "";
	private PackedDecimalData wsaaTaxEffdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalTax = new PackedDecimalData(17, 2);
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaLowRerateDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaNextCpiDate = new ZonedDecimalData(8, 0);

		/* WSAA-MISCELLANEOUS-FLAGS */
	private FixedLengthStringData wsaaCoverExit = new FixedLengthStringData(1);
	private Validator wsaaCoverEof = new Validator(wsaaCoverExit, "Y");
	private ZonedDecimalData wsaaLinsBtdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLinsBtdateFmt = new FixedLengthStringData(8).isAPartOf(wsaaLinsBtdate, 0, REDEFINE);
	private ZonedDecimalData wsaaLinsDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLinsBtdateFmt, 6).setUnsigned();
	private ZonedDecimalData wsaaAdvanceDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaAdvanceDatteFmt = new FixedLengthStringData(8).isAPartOf(wsaaAdvanceDate, 0, REDEFINE);
	private ZonedDecimalData wsaaAddateMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaAdvanceDatteFmt, 4).setUnsigned();
	private ZonedDecimalData wsaaAddateDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaAdvanceDatteFmt, 6).setUnsigned();
	private ZonedDecimalData wsaaEomDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaEomDateFmt = new FixedLengthStringData(8).isAPartOf(wsaaEomDate, 0, REDEFINE);
	private ZonedDecimalData wsaaEomYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaEomDateFmt, 0).setUnsigned();
	private FixedLengthStringData wsaaEomYearFmt = new FixedLengthStringData(4).isAPartOf(wsaaEomYear, 0, REDEFINE);
	private ZonedDecimalData wsaaCenturyYear = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomYearFmt, 2).setUnsigned();
	private ZonedDecimalData wsaaEomMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomDateFmt, 4).setUnsigned();
	private ZonedDecimalData wsaaEomDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomDateFmt, 6).setUnsigned();
	private static final String wsaaDaysInMonthNorm = "312831303130313130313031";
	private static final String wsaaDaysInMonthLeap = "312931303130313130313031";
	private FixedLengthStringData wsaaDaysInMonth = new FixedLengthStringData(24);

	private FixedLengthStringData wsaaDaysInMonthRed = new FixedLengthStringData(24).isAPartOf(wsaaDaysInMonth, 0, REDEFINE);
	private ZonedDecimalData[] wsaaMonthDays = ZDArrayPartOfStructure(12, 2, 0, wsaaDaysInMonthRed, 0, UNSIGNED_TRUE);
	private String wsaaLeapYear = "";
	private String wsaaEndOfMonth = "";
	private FixedLengthStringData wsaaEom1stDate = new FixedLengthStringData(1);
	private String wsaaWholeMonth = "";
	private PackedDecimalData wsaaResult = new PackedDecimalData(5, 0).setUnsigned();
	private String wsaaAgtTerminatedFlag = "";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5645rec t5645rec = new T5645rec();
	private T5667rec t5667rec = new T5667rec();
	private T5679rec t5679rec = new T5679rec();
	private T3695rec t3695rec = new T3695rec();
	private T6687rec t6687rec = new T6687rec();
	private T5688rec t5688rec = new T5688rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Atreqrec atreqrec = new Atreqrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Prasrec prasrec = new Prasrec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Tr517rec tr517rec = new Tr517rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S6258ScreenVars sv = ScreenProgram.getScreenVars( S6258ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);	
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO", AglfpfDAO.class);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Itempf itempf = new Itempf();
	private Itempf itdmpf = null;
	private Clrrpf clrrpf = null;
    private Payrpf payrpf = null;
    private Linspf linspf = null;
    private Clntpf clntpf = null;
    private Aglfpf aglfpf = null;
    private Acblpf acblpf = null;
    private Covrpf covrpf = null;
  //ILJ-49 Starts
    private boolean cntDteFlag = false;
    private String cntDteFeature = "NBPRP113";
    //ILJ-49 End 
    
	private Iterator<Covrpf> covrItr = null;
	private int covrCount = 0;

    private Map<String, Descpf> descMap = new HashMap<String, Descpf>();
	
	private FixedLengthStringData chdrlifchdrnum = new FixedLengthStringData(16); //ILIFE-7082
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		catchAll2030, 
		exit2090, 
		exit6090, 
		readCovrmja6120, 
		exit6190, 
		exit6590, 
		readContitem6720, 
		noMoreTr5176780, 
		exit6790
	}

	public P6258() {
		super();
		screenVars = sv;
		new ScreenModel("S6258", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End 
		sv.payToDateAdvance.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		prasrec.taxrelamt.set(ZERO);
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
				
		payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString());
		
		if (payrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set("2".concat(chdrlifIO.getChdrnum().toString()));
			fatalError600();
		}
		
		clrrpf = clrrpfDAO.getClrfRecord(chdrlifIO.getChdrpfx().toString(), payrpf.getChdrcoy(), (payrpf.getChdrnum()+payrpf.getPayrseqno()).substring(0, 9), "PY");

		if (clrrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(payrpf.getChdrcoy().concat(payrpf.getChdrnum()+payrpf.getPayrseqno()));
			fatalError600();
		}
		
		linspf = linspfDAO.getLinsRecordByCoyAndNumAndflag(payrpf.getChdrcoy(), payrpf.getChdrnum(), payrpf.getValidflag());

		if (linspf != null) {
			wsaaLinsBtdate.set(linspf.getInstfrom());
			if (isNE(linspf.getBillchnl(),payrpf.getBillfreq())) {
				wsaaLinsBtdate.set(payrpf.getBtdate());
			}
		}
		else {
			wsaaLinsBtdate.set(payrpf.getBtdate());
			if (isNE("",payrpf.getBillfreq())) {
				wsaaLinsBtdate.set(payrpf.getBtdate());
			}
		}
		
		sv.occdate.set(chdrlifIO.getOccdate());
		sv.chdrnum.set(chdrlifIO.getChdrnum());
		sv.agntnum.set(chdrlifIO.getAgntnum());
		sv.cnttype.set(chdrlifIO.getCnttype());

		findDesc1300();
		if(descMap==null || descMap.size()<=0 || descMap.get("T5688")==null ){
			sv.ctypdesc.fill("?");
		}
		else {
			sv.ctypdesc.set(descMap.get("T5688").getLongdesc());
		}
		
		sv.cownnum.set(chdrlifIO.getCownnum());
		
		clntpf = new Clntpf();
		clntpf.setClntnum(chdrlifIO.getCownnum().toString());
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf = clntpfDAO.selectClient(clntpf);
		
		if( clntpf==null || clntpf.getClntnum() == null || clntpf.getClntnum().trim().equals("") || !clntpf.getValidflag().trim().equals("1")){
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		//IJTI-462 START
		if (clntpf != null) { 
			sv.payrnum.set(clrrpf.getClntnum());
			clntpf.setClntnum(clrrpf.getClntnum());
			clntpf.setClntpfx("CN");
			clntpf.setClntcoy(wsspcomn.fsuco.toString());
			clntpf = clntpfDAO.selectClient(clntpf);
		} //IJTI-462 END
		
		if( clntpf==null || clntpf.getClntnum() == null || clntpf.getClntnum().trim().equals("") || !clntpf.getValidflag().trim().equals("1")){
			sv.payornameErr.set(errorsInner.e304);
			sv.payorname.set(SPACES);
		}
		else {
			plainname();
			sv.payorname.set(wsspcomn.longconfname);
		}
		
		if (isNE(sv.agntnum,SPACES)) {
			getAgentName1200();
		}
		/* READ TR52D*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
		itempf.setItemtabl("TR52D");
		itempf.setItemitem(chdrlifIO.getRegister().toString());
        itempf = itemDao.getItemRecordByItemkey(itempf);
	
		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
			itempf.setItemtabl("TR52D");
			itempf.setItemitem("***");
			itempf = itemDao.getItemRecordByItemkey(itempf);
			if (itempf == null) {
				syserrrec.statuz.set("MRNF");
				syserrrec.params.set("IT".concat(chdrlifIO.getChdrcoy().toString()).concat("TR52D"));
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isNE(tr52drec.txcode, SPACES)) {
			wsaaTaxEffdate.set(payrpf.getPtdate());
			calcTax6100();
			calcCntfTax6200();
		}
		compute(sv.taxamt, 2).set(add(payrpf.getSinstamt06(), wsaaTax));
		/* Get the tax relief method from T5688.                           */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5688");
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), "T5688")
		|| isNE(itdmIO.getItemitem(),chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.f290);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T6687");
		itempf.setItemitem(t5688rec.taxrelmth.toString());
		
		itempf = itemDao.getItemRecordByItemkey(itempf);
	
		if (itempf != null) {
			t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		if (isNE(t5688rec.taxrelmth,SPACES)) {
			prasrec.clntnum.set(clrrpf.getClntnum());
			prasrec.clntcoy.set(clrrpf.getClntcoy());
			prasrec.incomeSeqNo.set(payrpf.getIncomeSeqNo());
			prasrec.cnttype.set(chdrlifIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			prasrec.effdate.set(payrpf.getBillcd());
			prasrec.company.set(payrpf.getChdrcoy());
			prasrec.grossprem.set(payrpf.getSinstamt06());
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
		}
		zrdecplrec.amountIn.set(prasrec.taxrelamt);
		a000CallRounding();
		prasrec.taxrelamt.set(zrdecplrec.amountOut);
		compute(wsaaNetPrem, 2).set(sub(chdrlifIO.getSinstamt06(),prasrec.taxrelamt));
		sv.btdate.set(chdrlifIO.getBtdate());
		sv.cntinst.set(wsaaNetPrem);
		sv.ptdate.set(chdrlifIO.getPtdate());
	
		if(descMap==null || descMap.size()<=0 || descMap.get("T3623")==null ){
			sv.rstate.fill("?");
		}
		else {
			sv.rstate.set(descMap.get("T3623").getLongdesc());
		}

		/*  Look up premium status*/
		if(descMap==null || descMap.size()<=0 || descMap.get("T3588")==null ){
			sv.pstate.fill("?");
		}
		else {
			sv.pstate.set(descMap.get("T3588").getLongdesc());
		}
		

		sv.cntcurr.set(chdrlifIO.getCntcurr());
		sv.billcurr.set(payrpf.getBillcurr());
		sv.billfreq.set(payrpf.getBillfreq());
		calculateSuspense1600();
		calculateApa1800();
		wsaaLowRerateDate.set(ZERO);
		
		List<Covrpf> list = covrpfDAO.getCovrmjaByComAndNum(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString());
		wsaaCoverExit.set("N");
		wsaaLowRerateDate.set(varcom.vrcmMaxDate);
		wsaaNextCpiDate.set(varcom.vrcmMaxDate);
		if(list !=null && list.size()!=0){
		  for (int i=0; i<list.size(); i++ ) {
			  covrpf = list.get(i);
			  lowestRerateDate1700();
		  }
		}
		sv.cpiDate.set(wsaaNextCpiDate);
	}

protected void getAgentName1200()
	{
			
		if (isEQ(sv.agntnum,SPACES)) {
			sv.agntnumErr.set(errorsInner.e186);
			return ;
		}
	
		clntpf = clntpfDAO.getAgntAndClnt("AG", wsspcomn.company.toString(), sv.agntnum.toString(), wsspcomn.fsuco.toString());
	
		sv.agentname.set(SPACES);
		if (clntpf == null) {
			return ;
		}

		if (clntpf.getClntnum().trim().equals("")) {
			syserrrec.params.set("AG".concat(wsspcomn.company.toString()).concat(sv.agntnum.toString()));
			fatalError600();
		}
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void findDesc1300()
	{
		/*READ*/
	    Map<String, String> itemMap = new HashMap<String, String>();
	    itemMap.put("T5688", chdrlifIO.getCnttype().toString());
	    itemMap.put("T3623", chdrlifIO.getStatcode().toString());
	    itemMap.put("T3588", payrpf.getPstatcode());/* IJTI-1523 */
	    
	    descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);
	    
		/*EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		if(clntpf.getLsurname() !=null && clntpf.getLgivname() !=null ){
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		}
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void calculateSuspense1600()
	{
		calculateSuspense1610();
		getSign1020();
	}

protected void calculateSuspense1610()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5645");
		itempf.setItemitem(wsaaProg.toString());
        itempf.setItemseq("  ");
		itempf = itemDao.getItemRecordByItemkey(itempf);
		
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void getSign1020()
	{
	    itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T3695");
		itempf.setItemitem(t5645rec.sacstype01.toString().trim());
		itempf = itemDao.getItemRecordByItemkey(itempf);
		chdrlifchdrnum.set(chdrlifIO.getChdrnum()); //ILIFE-7082
		
		if (itempf==null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T3695").concat(t5645rec.sacstype01.toString()));
			fatalError600();
		}
		t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode01.toString(), chdrlifchdrnum.toString(), payrpf.getBillcurr(), t5645rec.sacstype01.toString());
		
		if (acblpf == null) {
			sv.sacscurbal.set(ZERO);
		}
		else {
			if (isEQ(t3695rec.sign,"-")) {
				compute(sv.sacscurbal, 2).set(mult(acblpf.getSacscurbal(),-1));
			}
			else {
				sv.sacscurbal.set(acblpf.getSacscurbal());
			}
		}
	}

protected void lowestRerateDate1700()
	{
		if (isLT(covrpf.getPremCessDate(),wsaaLowRerateDate)) {
			wsaaLowRerateDate.set(covrpf.getRerateDate());
		}
		if (isLT(covrpf.getCpiDate(),wsaaNextCpiDate)
		&& isNE(covrpf.getCpiDate(),0)) {
			wsaaNextCpiDate.set(covrpf.getCpiDate());
		}
	}

protected void calculateApa1800()
	{
//MIBT-220

	initialize(rlpdlonrec.rec);
	rlpdlonrec.function.set(varcom.info);
	rlpdlonrec.chdrcoy.set(chdrlifIO.getChdrcoy());
	rlpdlonrec.chdrnum.set(chdrlifIO.getChdrnum());
	rlpdlonrec.prmdepst.set(ZERO);
	rlpdlonrec.language.set(wsspcomn.language);
	
	datcon1rec.function.set(varcom.tday); 
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec); 
	
	rlpdlonrec.effdate.set(datcon1rec.intDate) ;
	callProgram(Rlpdlon.class, rlpdlonrec.rec);
	if (isNE(rlpdlonrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(rlpdlonrec.statuz);
		syserrrec.params.set(rlpdlonrec.rec);
		fatalError600();
	}
	sv.prmdepst.set(rlpdlonrec.prmdepst);
}

protected void preScreenEdit()
	{
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{	
	screenIo2010();
	validate2020();

	}


protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(sv.payToDateAdvance,varcom.vrcmMaxDate)) {
			sv.advptdatErr.set(errorsInner.h082);
			catchAll2030();
			return ;
		}
		if (isNE(wsaaNextCpiDate,varcom.vrcmMaxDate)) {
			if (isGT(sv.payToDateAdvance,wsaaNextCpiDate)) {
				sv.cpidteErr.set(errorsInner.h099);
				catchAll2030();
				return ;
			}
		}
		if (isGT(sv.payToDateAdvance,wsaaLowRerateDate)) {
			sv.advptdatErr.set(errorsInner.h156);
			catchAll2030();
			return ;
		}
		/*    IF S6258-PAY-TO-DATE-ADVANCE NOT > CHDRLIF-BTDATE*/
		/*                              OR NOT > CHDRLIF-PTDATE            */
		/*       MOVE U056                TO S6258-ADVPTDAT-ERR*/
		/*       GO TO 2030-CATCH-ALL.*/
		/* IF CHDRLIF-PTDATE NOT = CHDRLIF-BTDATE                       */
		if (isNE(payrpf.getPtdate(),payrpf.getBtdate())) {
			/*     MOVE U059                TO S6258-ADVPTDAT-ERR            */
			/*    MOVE H013                TO S6258-ADVPTDAT-ERR       <008>*/
			/*    MOVE 'Y'                 TO WSSP-EDTERROR.           <002>*/
			sv.ptdateErr.set(errorsInner.h013);
			wsspcomn.edterror.set("Y");
			catchAll2030();
			return ;
		}
		if (isLTE(sv.payToDateAdvance,payrpf.getBtdate())) {
			/*     MOVE U056                TO S6258-ADVPTDAT-ERR            */
			sv.advptdatErr.set(errorsInner.h083);
			/*    MOVE 'Y'                 TO WSSP-EDTERROR.           <002>*/
			wsspcomn.edterror.set("Y");
			catchAll2030();
			return ;
		}
		validateAdvanceDate2100();
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.edterror,"Y")) {
			catchAll2030();
			return ;
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(payrpf.getBtdate());
		datcon3rec.intDate2.set(wsaaAdvanceDate);
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callDatcon32400();
		wsaaNoOfFreqs.set(wsaaFreqFactor);
		if (isNE(wsaaFreqFactorRem,ZERO)) {
			wsaaNoOfFreqs.add(1);
		}
		/* Calculate the total tax for each premium due that we will       */
		/* be collecting as part of the paid to date advance.              */
		if (isNE(tr52drec.txcode, SPACES)) {
			ix.set(ZERO);
			PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
			loopEndVar1.set(wsaaNoOfFreqs);
			for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
				/* Using DATCON2 calculate the new paid to date by adding the      */
				/* number of times we've iterated minus 1 to the PAYR-BTDATE       */
				datcon2rec.datcon2Rec.set(SPACES);
				datcon2rec.intDate1.set(payrpf.getBtdate());
				datcon2rec.freqFactor.set(ix);
				datcon2rec.frequency.set(payrpf.getBillfreq());
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					fatalError600();
				}
				wsaaTaxEffdate.set(datcon2rec.intDate2);
				ix.add(1);
				calcTax6100();
				calcCntfTax6200();
				wsaaTotalTax.add(wsaaTax);
			}
		}
		checkEnoughMoney5000();
	}

protected void catchAll2030()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateAdvanceDate2100()
	{
		
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrlifIO.getOccdate());
		datcon3rec.intDate2.set(wsaaLinsBtdate);
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callDatcon32400();
		if (isEQ(wsaaFreqFactorRem,ZERO)) {
			wsaaLinsBtdate.set(chdrlifIO.getOccdate());
		}
		wsaaEndOfMonth = "N";
		wsaaEomDate.set(chdrlifIO.getOccdate());
		checkEndOfMonth2200();
		wsaaEom1stDate.set(wsaaEndOfMonth);
		wsaaEndOfMonth = "N";
		wsaaEomDate.set(wsaaLinsBtdate);
		if (isEQ(wsaaEomMonth,2)) {
			checkEndOfMonth2200();
		}
		if (isEQ(wsaaEndOfMonth,"Y")
		&& isEQ(wsaaEom1stDate,"N")) {
			wsaaLinsBtdate.set(chdrlifIO.getOccdate());
		}
		wsaaAdvanceDate.set(sv.payToDateAdvance);
		wsaaWholeMonth = "Y";
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaLinsBtdate);
		datcon3rec.intDate2.set(wsaaAdvanceDate);
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callDatcon32400();
		if (isNE(wsaaFreqFactorRem,ZERO)) {
			wsaaWholeMonth = "N";
		}
		wsaaEomDate.set(wsaaAdvanceDate);
		wsaaEndOfMonth = "N";
		if (isEQ(wsaaAddateMonth,2)) {
			checkEndOfMonth2200();
		}
		if (isEQ(wsaaEndOfMonth,"Y")
		&& isGTE(wsaaLinsDay,wsaaAddateDay)) {
			wsaaWholeMonth = "Y";
		}
		if (isNE(wsaaWholeMonth,"Y")) {
			wsspcomn.edterror.set("Y");
			sv.advptdatErr.set(errorsInner.e525);
		}
	}

protected void checkEndOfMonth2200()
	{
		/*PARA*/
		wsaaEndOfMonth = "N";
		wsaaLeapYear = "N";
		checkLeapYear2300();
		if (isEQ(wsaaLeapYear,"Y")) {
			wsaaDaysInMonth.set(wsaaDaysInMonthLeap);
		}
		else {
			wsaaDaysInMonth.set(wsaaDaysInMonthNorm);
		}
		if (isGTE(wsaaEomDay,wsaaMonthDays[wsaaEomMonth.toInt()])) {
			wsaaEndOfMonth = "Y";
		}
		/*EXIT*/
	}

protected void checkLeapYear2300()
	{
		/*CHK-LEAP-YEAR*/
		/* If the year is the begining of the century                      */
		/*    Divide the year by 400 and if result is an integer value,    */
		/*    this indicates that this is a Century leap year.             */
		if (isEQ(wsaaCenturyYear,ZERO)) {
			compute(wsaaResult, 0).set(div(wsaaEomYear,400));
			wsaaResult.multiply(400);
			if (isEQ(wsaaResult,wsaaEomYear)) {
				wsaaLeapYear = "Y";
			}
			return ;
		}
		compute(wsaaResult, 0).set(div(wsaaEomYear,4));
		wsaaResult.multiply(4);
		if (isEQ(wsaaResult,wsaaEomYear)) {
			wsaaLeapYear = "Y";
		}
	}

protected void callDatcon32400()
	{
		/*CALL-DATCON3*/
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaFreqFactor.set(datcon3rec.freqFactor);
		/*EXIT*/
	}

protected void update3000()
	{
		
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P6258AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaCompany.set(wsspcomn.company);
		wsaaChdrnum.set(chdrlifIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaAdvptdat.set(sv.payToDateAdvance);
		wsaaUnitEffdate.set(sv.effdate);
		if (isLTE(wsaaOutstAmount,sv.sacscurbal)) {
			wsaaApaRequired.set(ZERO);
		}
		else {
			if ((setPrecision(wsaaOutstAmount, 2)
			&& isLTE(wsaaOutstAmount,add(sv.sacscurbal,sv.prmdepst)))) {
				compute(wsaaApaRequired, 2).set(sub(wsaaOutstAmount,sv.sacscurbal));
			}
			else {
				wsaaApaRequired.set(sv.prmdepst);
			}
		}
		atreqrec.transArea.set(wsaaTransArea);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void checkEnoughMoney5000()
	{
		
		wsaaOutstAmount.set(0);
		if ((setPrecision(1, 2)
		&& isLT(add(sv.sacscurbal,sv.prmdepst),1))) {
			/*       MOVE U061                TO S6258-ADVPTDAT-ERR     <008>*/
			/*                                   S6258-SACSCURBAL-ERR   <008>*/
			sv.advptdatErr.set(errorsInner.e961);
			wsspcomn.edterror.set("Y");
			return ;
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl("T5688");
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),"T5688")
		|| isNE(itdmIO.getItemitem(),chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.f290);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T6687");
		itempf.setItemitem(t5688rec.taxrelmth.toString());
		
		itempf = itemDao.getItemRecordByItemkey(itempf);
		
		if (itempf != null) {
			t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		if (isNE(t5688rec.taxrelmth,SPACES)) {
			prasrec.clntnum.set(clrrpf.getClntnum());
			prasrec.clntcoy.set(clrrpf.getClntcoy());
			prasrec.incomeSeqNo.set(payrpf.getIncomeSeqNo());
			prasrec.cnttype.set(chdrlifIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			prasrec.effdate.set(payrpf.getBillcd());
			prasrec.company.set(payrpf.getChdrcoy());
			prasrec.grossprem.set(payrpf.getSinstamt06());
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
		}
		zrdecplrec.amountIn.set(prasrec.taxrelamt);
		a000CallRounding();
		prasrec.taxrelamt.set(zrdecplrec.amountOut);
		compute(wsaaNetPrem, 2).set(sub(chdrlifIO.getSinstamt06(),prasrec.taxrelamt));
		compute(wsaaOutstAmount, 5).set(add(mult(wsaaNetPrem, wsaaFreqFactor), wsaaTotalTax));
		if ((setPrecision(wsaaOutstAmount, 2)
		&& isGT(wsaaOutstAmount,add(sv.sacscurbal,sv.prmdepst)))) {
			sv.advptdatErr.set(errorsInner.e961);
			checkAgentTerminate5100();
			if (isNE(sv.agntnumErr,SPACES)) {
				wsspcomn.edterror.set("Y");
			}
			checkTolerance6000();
			if (isNE(sv.advptdatErr,SPACES)) {
				wsspcomn.edterror.set("Y");
			}
		}
	}

protected void checkAgentTerminate5100()
	{
		
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		
		aglfpf = aglfpfDAO.searchAglfRecord(chdrlifIO.getAgntcoy().toString(), chdrlifIO.getAgntnum().toString());

		if (aglfpf == null) {
			sv.agntnumErr.set(errorsInner.g772);
			return ;
		}
		wsaaAgtTerminatedFlag = "N";
		if (isLT(aglfpf.getDtetrm(),datcon1rec.intDate)
		|| isLT(aglfpf.getDteexp(),datcon1rec.intDate)
		|| isGT(aglfpf.getDteapp(),datcon1rec.intDate)) {
			wsaaAgtTerminatedFlag = "Y";
		}
	}

protected void checkTolerance6000()
	{
		try {
			tolerance6010();
		}
		catch (GOTOException e){
		}
	}

protected void tolerance6010()
	{
	    itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
		itempf.setItemtabl("T5667");
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(chdrlifIO.getCntcurr());
		itempf.setItemitem(wsbbT5667Key.toString());
		itempf = itemDao.getItemRecordByItemkey(itempf);
		
		if (itempf == null) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		compute(wsaaDiff, 2).set(sub(wsaaOutstAmount,(add(sv.sacscurbal,sv.prmdepst))));
		for (wsaaSub.set(1); !(isGT(wsaaSub,11)
		|| isEQ(sv.advptdatErr,SPACES)); wsaaSub.add(1)){
			searchForTolerance6050();
		}
		goTo(GotoLabel.exit6090);
	}

protected void searchForTolerance6050()
	{
		if (isEQ(payrpf.getBillfreq(),t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(wsaaOutstAmount,t5667rec.prmtol[wsaaSub.toInt()])),100));
			if (isLTE(wsaaDiff,wsaaTol)
			&& isLTE(wsaaDiff,t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.advptdatErr.set(SPACES);
				return ;
			}
		}
		if (isNE(t5667rec.maxamt[wsaaSub.toInt()],0)) {
			if (isEQ(wsaaAgtTerminatedFlag,"N")
			|| (isEQ(wsaaAgtTerminatedFlag,"Y")
			&& isEQ(t5667rec.sfind,"2"))) {
				compute(wsaaTol, 3).setRounded(div((mult(wsaaOutstAmount,t5667rec.prmtoln[wsaaSub.toInt()])),100));
				if (isLTE(wsaaDiff,wsaaTol)
				&& isLTE(wsaaDiff,t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.advptdatErr.set(SPACES);
				}
			}
		}
	}

protected void calcTax6100()
	{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				begnCovr6110();
			case readCovrmja6120: 
				readCovrmja6120();
				readTr52e6140();
			case exit6190: 
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
	}


protected void begnCovr6110()
	{
		wsaaTax.set(0);
		readT56796600();

		List<Covrpf> covrlist = covrpfDAO.getCovrmjaByComAndNum(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString());
		if(covrlist == null || covrlist.size() == 0){
			goTo(GotoLabel.exit6190);
		} else { //IJTI-462
			covrItr = covrlist.iterator(); //IJTI-462
			covrCount = 0; //IJTI-462
		}	//IJTI-462
	}

protected void readCovrmja6120()
	{
	    covrCount++;
	    //ILIFE-7281
	    if(covrItr.hasNext()) {
		covrpf = covrItr.next();
		statusCheck6500();
		/* Exclude components that do not qualify                          */
		if (statusNotValid.isTrue()) {
			goTo(GotoLabel.readCovrmja6120);
		}
		if (isNE(payrpf.getSinstamt05(), ZERO)) {
			readTr5176700();
			/* Do not calculate the premium tax on the COVRMJA record if the   */
			/* component code exists in the TR517 item.                        */
			if (isEQ(wsaaCovrValid, "N")) {
				goTo(GotoLabel.readCovrmja6120);
			}
		}
	    }else {
	    	goTo(GotoLabel.exit6190);
	    }
	}

protected void readTr52e6140()
	{
		/* Read TR52E.                                                     */
		if (covrCount==1) {
			readTr52e6900();
		}
		if (isNE(tr52erec.taxind01, "Y")) {
			goTo(GotoLabel.readCovrmja6120);
		}
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
		txcalcrec.life.set(covrpf.getLife());
		txcalcrec.coverage.set(covrpf.getCoverage());
		txcalcrec.rider.set(covrpf.getRider());
		txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
		txcalcrec.crtable.set(covrpf.getCrtable());
		txcalcrec.cnttype.set(chdrlifIO.getCnttype());
		txcalcrec.register.set(chdrlifIO.getRegister());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.transType.set("PREM");
		txcalcrec.ccy.set(chdrlifIO.getCntcurr());
		txcalcrec.effdate.set(wsaaTaxEffdate);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlifIO.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(covrpf.getZbinstprem());
		}
		else {
			txcalcrec.amountIn.set(covrpf.getInstprem());
		}
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callTaxSubr6800();
	}


protected void calcCntfTax6200()
	{
		readTr52e6900();
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		/* Perform tax subroutine 2nd time                                 */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
		txcalcrec.cnttype.set(chdrlifIO.getCnttype());
		txcalcrec.register.set(chdrlifIO.getRegister());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.transType.set("CNTF");
		txcalcrec.ccy.set(chdrlifIO.getCntcurr());
		txcalcrec.effdate.set(wsaaTaxEffdate);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlifIO.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		txcalcrec.amountIn.set(payrpf.getSinstamt02());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callTaxSubr6800();
	}

protected void statusCheck6500()
	{
	
		try {
			covrRiskStatus6510();
			covrPremStatus6520();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void covrRiskStatus6510()
	{
		wsaaValidStatus.set("N");
		if (isEQ(covrpf.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrpf.getStatcode(), t5679rec.covRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrpf.getStatcode(), t5679rec.ridRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		if (statusNotValid.isTrue()) {
			goTo(GotoLabel.exit6590);
		}
		
	}

protected void covrPremStatus6520()
	{
		wsaaValidStatus.set("N");
		if (isEQ(covrpf.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrpf.getPstatcode(), t5679rec.covPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrpf.getPstatcode(), t5679rec.ridPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
	}

protected void readT56796600()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
        itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		wsaaBatckey.set(wsspcomn.batchkey);
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itemDao.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5679").concat(wsaaBatckey.batcBatctrcde.toString()));
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void readTr5176700()
	{
		wsaaCovrValid = "Y";

		itdmpf = new Itempf();
		List<Itempf> itdmList = itemDao.getItdmByFrmdate(wsspcomn.company.toString(), "TR517", covrpf.getCrtable(), covrpf.getCrrcd());
		if(itdmList == null || itdmList.size() == 0)
			return ;
		
		itdmpf = itdmList.get(0);

		tr517rec.tr517Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
		for (ix.set(1); !(isGT(ix, 50)); ix.add(1)){
			if (isNE(tr517rec.ctable[ix.toInt()], SPACES)
			&& isEQ(tr517rec.ctable[ix.toInt()], covrpf.getCrtable())) {
				wsaaCovrValid = "N";
			}
		}
		if (isEQ(tr517rec.contitem, SPACES)) {
			return ;
		}
		readContitem6720();
	}

protected void readContitem6720()
	{
	    List<Itempf> itdmList = itemDao.getItdmByFrmdate(wsspcomn.company.toString(), "TR517", tr517rec.contitem.toString(), Integer.parseInt(itdmpf.getItmfrm().toString()));
        
	    if(itdmList == null || itdmList.size() == 0){
	    	return ;
	    }
	
	    itdmpf = itdmList.get(0);
	    
		tr517rec.tr517Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
		for (ix.set(1); !(isGT(ix, 50)); ix.add(1)){
			if (isNE(tr517rec.ctable[ix.toInt()], SPACES)
			&& isEQ(tr517rec.ctable[ix.toInt()], covrpf.getCrtable())) {
				wsaaCovrValid = "N";
			}
		}
		if (isEQ(tr517rec.contitem, SPACES)) {
			return ;
		}
		else {
			readContitem6720();
		}
	}

protected void callTaxSubr6800()
	{
		
		/* Call TR52D tax subroutine                                       */
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/*EXIT*/
	}

protected void readTr52e6900()
	{
		
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
		itdmIO.setItemtabl("TR52E");
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlifIO.getBtdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), chdrlifIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), "TR52E"))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
			itdmIO.setItemtabl("TR52E");
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itdmIO.setItemitem(wsaaTr52eKey);
			itdmIO.setItmfrm(chdrlifIO.getBtdate());
			itdmIO.setFormat(formatsInner.itemrec);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
		}
		if ((isNE(itdmIO.getItemcoy(), chdrlifIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), "TR52E"))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
			itdmIO.setItemtabl("TR52E");
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itdmIO.setItemitem(wsaaTr52eKey);
			itdmIO.setItmfrm(chdrlifIO.getBtdate());
			itdmIO.setFormat(formatsInner.itemrec);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
		}
		tr52erec.tr52eRec.set(itdmIO.getGenarea());
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrlifIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData h082 = new FixedLengthStringData(4).init("H082");
	private FixedLengthStringData h083 = new FixedLengthStringData(4).init("H083");
	private FixedLengthStringData e525 = new FixedLengthStringData(4).init("E525");
	private FixedLengthStringData h013 = new FixedLengthStringData(4).init("H013");
	private FixedLengthStringData e961 = new FixedLengthStringData(4).init("E961");
	private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
	private FixedLengthStringData h156 = new FixedLengthStringData(4).init("H156");
	private FixedLengthStringData h099 = new FixedLengthStringData(4).init("H099");
	private FixedLengthStringData g772 = new FixedLengthStringData(4).init("G772");
}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 

	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
}
}
