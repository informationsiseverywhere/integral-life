package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:24
 * Description:
 * Copybook name: COVRRCCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrrcckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrrccFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrrccKey = new FixedLengthStringData(64).isAPartOf(covrrccFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrrccChdrcoy = new FixedLengthStringData(1).isAPartOf(covrrccKey, 0);
  	public FixedLengthStringData covrrccChdrnum = new FixedLengthStringData(8).isAPartOf(covrrccKey, 1);
  	public FixedLengthStringData covrrccLife = new FixedLengthStringData(2).isAPartOf(covrrccKey, 9);
  	public FixedLengthStringData covrrccCoverage = new FixedLengthStringData(2).isAPartOf(covrrccKey, 11);
  	public FixedLengthStringData covrrccRider = new FixedLengthStringData(2).isAPartOf(covrrccKey, 13);
  	public PackedDecimalData covrrccPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrrccKey, 15);
  	public PackedDecimalData covrrccTranno = new PackedDecimalData(5, 0).isAPartOf(covrrccKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(covrrccKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrrccFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrrccFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}