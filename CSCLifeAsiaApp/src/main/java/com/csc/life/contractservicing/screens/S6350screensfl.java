package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6350screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 1, 2, 99, 89}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 22, 3, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6350ScreenVars sv = (S6350ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6350screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6350screensfl, 
			sv.S6350screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6350ScreenVars sv = (S6350ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6350screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6350ScreenVars sv = (S6350ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6350screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6350screensflWritten.gt(0))
		{
			sv.s6350screensfl.setCurrentIndex(0);
			sv.S6350screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6350ScreenVars sv = (S6350ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6350screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6350ScreenVars screenVars = (S6350ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rider.setFieldName("rider");
				screenVars.coverage.setFieldName("coverage");
				screenVars.life.setFieldName("life");
				screenVars.select.setFieldName("select");
				screenVars.planSuffix.setFieldName("planSuffix");
				screenVars.covRiskStat.setFieldName("covRiskStat");
				screenVars.rstatdesc.setFieldName("rstatdesc");
				screenVars.covPremStat.setFieldName("covPremStat");
				screenVars.pstatdesc.setFieldName("pstatdesc");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.life.set(dm.getField("life"));
			screenVars.select.set(dm.getField("select"));
			screenVars.planSuffix.set(dm.getField("planSuffix"));
			screenVars.covRiskStat.set(dm.getField("covRiskStat"));
			screenVars.rstatdesc.set(dm.getField("rstatdesc"));
			screenVars.covPremStat.set(dm.getField("covPremStat"));
			screenVars.pstatdesc.set(dm.getField("pstatdesc"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6350ScreenVars screenVars = (S6350ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rider.setFieldName("rider");
				screenVars.coverage.setFieldName("coverage");
				screenVars.life.setFieldName("life");
				screenVars.select.setFieldName("select");
				screenVars.planSuffix.setFieldName("planSuffix");
				screenVars.covRiskStat.setFieldName("covRiskStat");
				screenVars.rstatdesc.setFieldName("rstatdesc");
				screenVars.covPremStat.setFieldName("covPremStat");
				screenVars.pstatdesc.setFieldName("pstatdesc");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("life").set(screenVars.life);
			dm.getField("select").set(screenVars.select);
			dm.getField("planSuffix").set(screenVars.planSuffix);
			dm.getField("covRiskStat").set(screenVars.covRiskStat);
			dm.getField("rstatdesc").set(screenVars.rstatdesc);
			dm.getField("covPremStat").set(screenVars.covPremStat);
			dm.getField("pstatdesc").set(screenVars.pstatdesc);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6350screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6350ScreenVars screenVars = (S6350ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.planSuffix.clearFormatting();
		screenVars.covRiskStat.clearFormatting();
		screenVars.rstatdesc.clearFormatting();
		screenVars.covPremStat.clearFormatting();
		screenVars.pstatdesc.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6350ScreenVars screenVars = (S6350ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.life.setClassString("");
		screenVars.select.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.covRiskStat.setClassString("");
		screenVars.rstatdesc.setClassString("");
		screenVars.covPremStat.setClassString("");
		screenVars.pstatdesc.setClassString("");
	}

/**
 * Clear all the variables in S6350screensfl
 */
	public static void clear(VarModel pv) {
		S6350ScreenVars screenVars = (S6350ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.rider.clear();
		screenVars.coverage.clear();
		screenVars.life.clear();
		screenVars.select.clear();
		screenVars.planSuffix.clear();
		screenVars.covRiskStat.clear();
		screenVars.rstatdesc.clear();
		screenVars.covPremStat.clear();
		screenVars.pstatdesc.clear();
	}
}
