package com.csc.life.contractservicing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: TloantrTableDAM.java
 * Date: Sun, 30 Aug 2009 03:49:43
 * Class transformed from TLOANTR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class TloantrTableDAM extends LoanpfTableDAM {

	public TloantrTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("TLOANTR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LOANNUMBER"
		             + ", VALIDFLAG"
		             + ", FTRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LOANNUMBER, " +
		            "LOANTYPE, " +
		            "LOANCURR, " +
		            "VALIDFLAG, " +
		            "FTRANNO, " +
		            "LTRANNO, " +
		            "LOANORIGAM, " +
		            "LOANSTDATE, " +
		            "LSTCAPLAMT, " +
		            "LSTCAPDATE, " +
		            "NXTCAPDATE, " +
		            "LSTINTBDTE, " +
		            "NXTINTBDTE, " +
		            "TPLSTMDTY, " +
		            "TERMID, " +
		            "USER_T, " +
		            "TRDT, " +
		            "TRTM, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LOANNUMBER ASC, " +
		            "VALIDFLAG ASC, " +
		            "FTRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LOANNUMBER DESC, " +
		            "VALIDFLAG DESC, " +
		            "FTRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               loanNumber,
                               loanType,
                               loanCurrency,
                               validflag,
                               firstTranno,
                               lastTranno,
                               loanOriginalAmount,
                               loanStartDate,
                               lastCapnLoanAmt,
                               lastCapnDate,
                               nextCapnDate,
                               lastIntBillDate,
                               nextIntBillDate,
                               tplstmdty,
                               termid,
                               user,
                               transactionDate,
                               transactionTime,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(49);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLoanNumber().toInternal()
					+ getValidflag().toInternal()
					+ getFirstTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, loanNumber);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, firstTranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(loanNumber.toInternal());
	nonKeyFiller60.setInternal(validflag.toInternal());
	nonKeyFiller70.setInternal(firstTranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(136);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getLoanType().toInternal()
					+ getLoanCurrency().toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getLastTranno().toInternal()
					+ getLoanOriginalAmount().toInternal()
					+ getLoanStartDate().toInternal()
					+ getLastCapnLoanAmt().toInternal()
					+ getLastCapnDate().toInternal()
					+ getNextCapnDate().toInternal()
					+ getLastIntBillDate().toInternal()
					+ getNextIntBillDate().toInternal()
					+ getTplstmdty().toInternal()
					+ getTermid().toInternal()
					+ getUser().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, loanType);
			what = ExternalData.chop(what, loanCurrency);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, lastTranno);
			what = ExternalData.chop(what, loanOriginalAmount);
			what = ExternalData.chop(what, loanStartDate);
			what = ExternalData.chop(what, lastCapnLoanAmt);
			what = ExternalData.chop(what, lastCapnDate);
			what = ExternalData.chop(what, nextCapnDate);
			what = ExternalData.chop(what, lastIntBillDate);
			what = ExternalData.chop(what, nextIntBillDate);
			what = ExternalData.chop(what, tplstmdty);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(Object what) {
		setLoanNumber(what, false);
	}
	public void setLoanNumber(Object what, boolean rounded) {
		if (rounded)
			loanNumber.setRounded(what);
		else
			loanNumber.set(what);
	}
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}
	public PackedDecimalData getFirstTranno() {
		return firstTranno;
	}
	public void setFirstTranno(Object what) {
		setFirstTranno(what, false);
	}
	public void setFirstTranno(Object what, boolean rounded) {
		if (rounded)
			firstTranno.setRounded(what);
		else
			firstTranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLoanType() {
		return loanType;
	}
	public void setLoanType(Object what) {
		loanType.set(what);
	}	
	public FixedLengthStringData getLoanCurrency() {
		return loanCurrency;
	}
	public void setLoanCurrency(Object what) {
		loanCurrency.set(what);
	}	
	public PackedDecimalData getLastTranno() {
		return lastTranno;
	}
	public void setLastTranno(Object what) {
		setLastTranno(what, false);
	}
	public void setLastTranno(Object what, boolean rounded) {
		if (rounded)
			lastTranno.setRounded(what);
		else
			lastTranno.set(what);
	}	
	public PackedDecimalData getLoanOriginalAmount() {
		return loanOriginalAmount;
	}
	public void setLoanOriginalAmount(Object what) {
		setLoanOriginalAmount(what, false);
	}
	public void setLoanOriginalAmount(Object what, boolean rounded) {
		if (rounded)
			loanOriginalAmount.setRounded(what);
		else
			loanOriginalAmount.set(what);
	}	
	public PackedDecimalData getLoanStartDate() {
		return loanStartDate;
	}
	public void setLoanStartDate(Object what) {
		setLoanStartDate(what, false);
	}
	public void setLoanStartDate(Object what, boolean rounded) {
		if (rounded)
			loanStartDate.setRounded(what);
		else
			loanStartDate.set(what);
	}	
	public PackedDecimalData getLastCapnLoanAmt() {
		return lastCapnLoanAmt;
	}
	public void setLastCapnLoanAmt(Object what) {
		setLastCapnLoanAmt(what, false);
	}
	public void setLastCapnLoanAmt(Object what, boolean rounded) {
		if (rounded)
			lastCapnLoanAmt.setRounded(what);
		else
			lastCapnLoanAmt.set(what);
	}	
	public PackedDecimalData getLastCapnDate() {
		return lastCapnDate;
	}
	public void setLastCapnDate(Object what) {
		setLastCapnDate(what, false);
	}
	public void setLastCapnDate(Object what, boolean rounded) {
		if (rounded)
			lastCapnDate.setRounded(what);
		else
			lastCapnDate.set(what);
	}	
	public PackedDecimalData getNextCapnDate() {
		return nextCapnDate;
	}
	public void setNextCapnDate(Object what) {
		setNextCapnDate(what, false);
	}
	public void setNextCapnDate(Object what, boolean rounded) {
		if (rounded)
			nextCapnDate.setRounded(what);
		else
			nextCapnDate.set(what);
	}	
	public PackedDecimalData getLastIntBillDate() {
		return lastIntBillDate;
	}
	public void setLastIntBillDate(Object what) {
		setLastIntBillDate(what, false);
	}
	public void setLastIntBillDate(Object what, boolean rounded) {
		if (rounded)
			lastIntBillDate.setRounded(what);
		else
			lastIntBillDate.set(what);
	}	
	public PackedDecimalData getNextIntBillDate() {
		return nextIntBillDate;
	}
	public void setNextIntBillDate(Object what) {
		setNextIntBillDate(what, false);
	}
	public void setNextIntBillDate(Object what, boolean rounded) {
		if (rounded)
			nextIntBillDate.setRounded(what);
		else
			nextIntBillDate.set(what);
	}	
	public PackedDecimalData getTplstmdty() {
		return tplstmdty;
	}
	public void setTplstmdty(Object what) {
		setTplstmdty(what, false);
	}
	public void setTplstmdty(Object what, boolean rounded) {
		if (rounded)
			tplstmdty.setRounded(what);
		else
			tplstmdty.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		loanNumber.clear();
		validflag.clear();
		firstTranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		loanType.clear();
		loanCurrency.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		lastTranno.clear();
		loanOriginalAmount.clear();
		loanStartDate.clear();
		lastCapnLoanAmt.clear();
		lastCapnDate.clear();
		nextCapnDate.clear();
		lastIntBillDate.clear();
		nextIntBillDate.clear();
		tplstmdty.clear();
		termid.clear();
		user.clear();
		transactionDate.clear();
		transactionTime.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}