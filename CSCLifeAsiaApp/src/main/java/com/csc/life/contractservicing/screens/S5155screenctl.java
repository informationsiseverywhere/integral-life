package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5155screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {17, 4, 22, 18, 5, 23, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S5155screensfl";
		lrec.subfileClass = S5155screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 13;
		lrec.pageSubfile = 12;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5155ScreenVars sv = (S5155ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5155screenctlWritten, sv.S5155screensflWritten, av, sv.s5155screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5155ScreenVars screenVars = (S5155ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.cntinst.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.osbal.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.supflag.setClassString("");
		screenVars.suppressToDisp.setClassString("");
		screenVars.longdesc.setClassString("");
	}

/**
 * Clear all the variables in S5155screenctl
 */
	public static void clear(VarModel pv) {
		S5155ScreenVars screenVars = (S5155ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.cntinst.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.osbal.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.billfreq.clear();
		screenVars.mop.clear();
		screenVars.supflag.clear();
		screenVars.suppressToDisp.clear();
		screenVars.suppressTo.clear();
		screenVars.longdesc.clear();
	}
}
