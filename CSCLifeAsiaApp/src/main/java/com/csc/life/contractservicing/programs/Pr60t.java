/*
 * File: Pr60t.java
 * Date: 30 August 2009 0:36:46
 * Author: Quipoz Limited
 * 
 * Class transformed from Pr60t.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.SchmpfDAO;
import com.csc.fsu.general.dataaccess.model.Schmpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.contractservicing.screens.Sr60tScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Underwriting Enquiry Facility
* *****************************
*
*****************************************************************
* </pre>
*/
public class Pr60t extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(Pr60t.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR60T");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	
	private FixedLengthStringData wsaaValidStatusFlag = new FixedLengthStringData(1);

	private String f321 = "F321";
	private String g957 = "G957";

	private String h387 = "H387";
	private String t5679 = "T5679";
	
	
	private T5679rec t5679rec = new T5679rec();
	
	private Clntkey wsaaClntkey = new Clntkey();
	Schmpf schmpf = null;
	private List<Zctxpf> zctxpfList = new ArrayList<Zctxpf>();
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	SchmpfDAO schmpfDAO = getApplicationContext().getBean("schmpfDAO", SchmpfDAO.class);
	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	List<Chdrpf> chdrList = new ArrayList<Chdrpf>();
	Chdrpf chdrRec = new Chdrpf();
	private Sr60tScreenVars sv = ScreenProgram.getScreenVars( Sr60tScreenVars.class);
	private PackedDecimalData wsaaSubfileCount = new PackedDecimalData(5, 0);
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Optswchrec optswchrec = new Optswchrec();
	private String wsaaOptionSelected = "";
	private FixedLengthStringData wsaaValidFkey = new FixedLengthStringData(1);
	private Validator validFkey = new Validator(wsaaValidFkey, "Y");
	private Batckey wsaaBatchkey = new Batckey();
	List<Itempf> listT5679 = new ArrayList<Itempf>();
	ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private FixedLengthStringData wsaaStat = new FixedLengthStringData(1);
	private Validator wsaaStatOk = new Validator(wsaaStat, "Y");
	private static final String h137 = "H137";
	SimpleDateFormat integralDateFormat = new SimpleDateFormat("yyyyMMdd");
	String CurTaxFromDate,PreTaxFromDate;
	private Datcon2rec datcon2rec = new Datcon2rec();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	Clntpf clntpf = null;
	ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	
	public Pr60t() {
		super();
		screenVars = sv;
		new ScreenModel("Sr60t", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{		
		initialise();
				
	}

protected void initialise()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				wsaaOptionSelected = "N";
				return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaBatchkey.set(wsspcomn.batchkey);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR60T", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		wsaaSubfileCount.set(ZERO);
		wsaaValidFkey.set("N");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datcon2rec.intDatex1.set(datcon1rec.intDate);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(-3);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaToday.set(datcon1rec.intDate);
		autogenerateFromTaxDate();
		initOptswch();
		checkHeaderStatus19101();
		screenHeader();		
		
	}

protected void initOptswch()
{
	/*  Initialize the Switching Options.*/
	optswchrec.optsFunction.set("INIT");
	optswchrec.optsCallingProg.set(wsaaProg);
	optswchrec.optsDteeff.set(ZERO);
	optswchrec.optsCompany.set(wsspcomn.company);
	optswchrec.optsItemCompany.set(wsspcomn.company);
	optswchrec.optsLanguage.set(wsspcomn.language);
	varcom.vrcmTranid.set(wsspcomn.tranid);
	optswchrec.optsUser.set(varcom.vrcmUser);
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if (isNE(optswchrec.optsStatuz, varcom.oK)) {
		syserrrec.function.set("INIT");
		syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
		syserrrec.statuz.set(optswchrec.optsStatuz);
		syserrrec.iomod.set("OPTSWCH");
		fatalError600();
	}
}

protected void screenHeader()
	{
		if(isNE(wsspcomn.chdrMplnum,SPACES)) {
			Chdrpf chdrpf = chdrpfDAO.getchdrRecordservunit(wsspcomn.chdrChdrcoy.toString(), wsspcomn.chdrChdrnum.toString());
			if(chdrpf != null){
				wsspcomn.chdrCnttype.set(chdrpf.getCnttype());
				zctxpfList = zctxpfDAO.readContributionTaxList(wsspcomn.chdrChdrcoy.toString(), wsspcomn.chdrChdrnum.toString());
				Zctxpf zctxpf = zctxpfList.get(0);
				if(zctxpfList.size()>1){
					sv.clntnum.set(zctxpf.getClntnum());
					formatClientName();
					sv.schmno.set(chdrpf.getSchmno());
					schmpf = schmpfDAO.getSchmpfRecord(sv.schmno.toString());
					/* No record found*/
					if (schmpf != null) {
						sv.schmnme.set(schmpf.getSchmNme());
					}
					loadSubfileChdr();}
				else{
					wsspcomn.confirmationKey.set(zctxpf.getDatefrm());}
				}
			return;
		}		
		
		if(isNE(wsspcomn.clntkey,SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			sv.clntnum.set(wsaaClntkey.clntClntnum);
			sv.ownername.set(wsspcomn.longconfname);
			loadSubfileClnt();
			return;
		}
		if(isNE(wsspwindow.value,SPACES)) {
			sv.schmno.set(wsspwindow.value);
			schmpf = schmpfDAO.getSchmpfRecord(sv.schmno.toString());
			/* No record found*/
			if (schmpf != null) {
				sv.schmnme.set(schmpf.getSchmNme());
			}
			loadSubfileScheme();
			return;
		}
	}
protected void formatClientName()
{
	plainname();		
}

protected void plainname()
{
	clntpf = clntpfDAO.findClientByClntnum(sv.clntnum.toString());
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(), "C")) {
		corpname();
		return ;
	}
	if (isNE(clntpf.getGivname(), SPACES)) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getSurname(), "  ");
		stringVariable1.addExpression(", ");
		stringVariable1.addExpression(clntpf.getGivname(), "  ");
		stringVariable1.setStringInto(sv.ownername);
	}
	else {
		sv.ownername.set(clntpf.getSurname());
	}
	/*PLAIN-EXIT*/
}

protected void corpname()
{
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(clntpf.getLsurname(), "  ");
	stringVariable1.addExpression(" ");
	stringVariable1.addExpression(clntpf.getLgivname(), "  ");
	stringVariable1.setStringInto(sv.ownername);
	/*CORP-EXIT*/
}


protected void loadSubfileChdr()
{	
	chdrList = new ArrayList<Chdrpf>();
	chdrList = chdrpfDAO.getChdrpfByKey("CH",wsspcomn.chdrChdrnum.toString(),wsspcomn.company.toString());
	chdrRec = chdrList.get(0);
	loadSubfilePage();
	
}

protected void loadSubfileClnt()
	{	
		chdrList = new ArrayList<Chdrpf>();
		chdrList = chdrpfDAO.getChdrpfByKey("CN",sv.clntnum.toString(),wsspcomn.company.toString());
		chdrRec = chdrList.get(0);
		loadSubfilePage();
		
	}

protected void loadSubfileScheme()
	{
		chdrList = new ArrayList<Chdrpf>();
		chdrList = chdrpfDAO.getChdrpfByKey("SH",sv.schmno.toString(),wsspcomn.company.toString());
		chdrRec = chdrList.get(0);
		loadSubfilePage();
	}

protected void loadSubfilePage()
	{
	
	
	int count=0;
	while (isLT(wsaaSubfileCount, chdrList.size()) && isLT(count, 10)) {
		loadSubfileRecords();
		count++;
	}
	/*EXIT*/
	/**     LOADS RECORDS INTO THE SUBFILE UNTIL THERE ARE NO MORE*/
	/**     DATABASE RECORDS OR THE SUBFILE PAGE IS FULL*/
	if (isGTE(wsaaSubfileCount.toInt(),chdrList.size())) {
		scrnparams.subfileMore.set("N");
	}
	else {
		scrnparams.subfileMore.set("Y");
	}
	
	/*CHECK-FOR-RECORDS-ADDED*/
	/* If there were no records added to the subfile output a message.*/
	if (isEQ(wsaaSubfileCount, ZERO)) {
		sv.chdrnumErr.set(g957);
	}
	/*EXIT*/
	/**     LOADS RECORDS INTO THE SUBFILE UNTIL THERE ARE NO MORE*/
	/**     DATABASE RECORDS OR THE SUBFILE PAGE IS FULL*/
	}

protected void loadSubfileRecords()
	{	 
		writeRecToSubfile();
		readNextRecord();
	}


protected void readNextRecord()
	{
		if(isLT(wsaaSubfileCount,chdrList.size())) {
			chdrRec = new Chdrpf();
			chdrRec = chdrList.get(wsaaSubfileCount.toInt());
		}
	}

protected void writeRecToSubfile()
	{	
		setSflRecord();
		obtainZctxRecord();
		
	}
protected void setSflRecord()
{
	sv.chdrnum.set(chdrRec.getChdrnum());
	sv.schdrnum.set(chdrRec.getChdrnum());
	sv.clntnumSfl.set(chdrRec.getCownnum());
	sv.schmnoSfl.set(chdrRec.getSchmno());
	sv.cnttype.set(chdrRec.getCnttype());
	sv.scnttype.set(chdrRec.getCnttype());
	sv.statcode.set(chdrRec.getStatcode());
	sv.pstatcode.set(chdrRec.getPstcde());
}

protected void obtainZctxRecord()
{
	zctxpfList = zctxpfDAO.readContributionTaxList(wsspcomn.company.toString(), sv.chdrnum.toString());
	if(zctxpfList.size() == 0) {
		wsaaSubfileCount.add(1);
		sv.taxPeriodFrom.set(varcom.vrcmMaxDate);
		sv.taxPeriodTo.set(varcom.vrcmMaxDate);
		sv.totamnt.set(ZERO);
		sv.dflag.set("Y");
		scrnparams.function.set(varcom.sadd);
		processScreen("SR60T", sv);	
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	else{
		for (Zctxpf zctxpf : zctxpfList) {
			wsaaSubfileCount.add(1);			
			sv.taxPeriodFrom.set(zctxpf.getDatefrm());
			sv.taxPeriodTo.set(zctxpf.getDateto());
			sv.totamnt.set(zctxpf.getAmount());
			
			if(isNE(sv.taxPeriodFrom,CurTaxFromDate)&&isNE(sv.taxPeriodFrom,PreTaxFromDate)&&isEQ(wsspcomn.flag,"A"))
				sv.dflag.set("Y");
			else
				sv.dflag.set("N");
			scrnparams.function.set(varcom.sadd);
			processScreen("SR60T", sv);	
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			/*ILIFE-8031 : Starts*/
			/* sv.chdrnum.set(SPACES);		
			sv.clntnumSfl.set(SPACES);
			sv.schmnoSfl.set(SPACES);
			sv.cnttype.set(SPACES);			
			sv.schdrnum.set(chdrRec.getChdrnum());	
			sv.scnttype.set(chdrRec.getCnttype());*/
			/*ILIFE-8031 : Ends*/ 
		}
	}

}
protected void autogenerateFromTaxDate(){
	Calendar proposalDate = Calendar.getInstance();
    Calendar taxCurFromDate = Calendar.getInstance();
    Calendar taxPreFromDate = Calendar.getInstance();
	try
	 {
	    proposalDate.setTime(integralDateFormat.parse(wsaaToday.toString()));
	    int month=proposalDate.get(Calendar.MONTH)+1;
	    if(month>=7)//ILIFE-3807
	    {
	    	taxCurFromDate.set(proposalDate.get(Calendar.YEAR), 6, 01);
	    	taxPreFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    }
	    else{ 
	    	taxCurFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    	taxPreFromDate.set(proposalDate.get(Calendar.YEAR)-2, 6, 01);
	    }
	  }catch (ParseException e) {
			
			LOGGER.error("error in parsing ,autogenerateFromTaxDate",e);
		}
		
		CurTaxFromDate=integralDateFormat.format(taxCurFromDate.getTime());
		PreTaxFromDate=integralDateFormat.format(taxPreFromDate.getTime());
}

protected void checkHeaderStatus19101()
	{
		String tranCde = wsaaBatchkey.batcBatctrcde.toString();
		listT5679 = itemDAO.getAllItemitem("IT",wsspcomn.company.toString(), t5679, tranCde);
		
		if (listT5679.size() == 0) {
			scrnparams.errorCode.set(f321);
			wsspcomn.edterror.set("Y");
			return ;
		}
		//checkChdrStatus8000();
		if (isEQ(wsaaValidStatusFlag,"Y")) {
			sv.chdrnum.set(sv.chdrnum);
			sv.cnttype.set(sv.cnttype);
		}		
	}



protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		
		wsspcomn.edterror.set(varcom.oK);
		if(isNE(wsspcomn.chdrMplnum,SPACES)&&(zctxpfList.size()==1)) {
			wsspcomn.sectionno.set("3000");
			return;
		}
		/* Bypass this section if returning from a previous use if this*/
		/* program.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}	
		scrnparams.subfileRrn.set(1);
		return ;
		
	}

protected void screenEdit2000()
	{
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			loadSubfilePage();
			wsspcomn.edterror.set(SPACES);
		
		}
		checkForErrors2020();
		
	}

protected void checkForErrors2020()
{	
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) 
	{
		wsaaOptionSelected = "N";
		return ;
	}
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/* Check the function key is a valid OPTSWCH entry on T1661.*/
	if (isEQ(scrnparams.statuz, "INSR")) {
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelType.set("F");
		optswchrec.optsSelCode.set(scrnparams.statuz);
		optswchrec.optsSelOptno.set(ZERO);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			scrnparams.errorCode.set(optswchrec.optsStatuz);
			wsspcomn.edterror.set("Y");
		}
		else {
			wsaaValidFkey.set("Y");
		}
	}
	/* F12 pressed to Kill*/
	if (isEQ(scrnparams.statuz, varcom.kill)) {
		return ;
	}
	scrnparams.function.set(varcom.sstrt);
	callSr60tio();
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		scrnparams.errorCode.set(h387);
		wsspcomn.edterror.set("Y");
		return ;
	}
	while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
		validateSubfile2600();
	}
	
}

protected void validateSubfile2600()
{
			validation();
			updateErrorIndicators();
	
}

protected void validation()
{
	/*    Validate subfile fields*/
	if (isEQ(sv.select, SPACES)) {
		return;
	}
	checkStatus();
	optswchrec.optsFunction.set("CHCK");
	optswchrec.optsCallingProg.set(wsaaProg);
	optswchrec.optsDteeff.set(ZERO);
	optswchrec.optsCompany.set(wsspcomn.company);
	optswchrec.optsItemCompany.set(wsspcomn.company);
	optswchrec.optsLanguage.set(wsspcomn.language);
	varcom.vrcmTranid.set(wsspcomn.tranid);
	optswchrec.optsUser.set(varcom.vrcmUser);
	optswchrec.optsSelCode.set(SPACES);
	optswchrec.optsSelOptno.set(sv.select);
	optswchrec.optsSelType.set("L");
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if (isNE(optswchrec.optsStatuz, varcom.oK)) {
		sv.selectErr.set(optswchrec.optsStatuz);
	}
}
protected void checkStatus()
{
	for(Itempf itempf:listT5679) {
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* Check the risk and premium statii against those*/
		/* on T5679*/
		wsaaStat.set("N");
		wsaaSub.set(ZERO);
		while ( !(isGT(wsaaSub,12)
		|| wsaaStatOk.isTrue())) {
			wsaaSub.add(1);
			if (isGT(wsaaSub,12)) {
				sv.selectErr.set(h137);
				return ;
			}
			if (isEQ(sv.statcode,t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaStat.set("Y");
			}
		}
		
		wsaaStat.set("N");
		wsaaSub.set(ZERO);
		while ( !(isGT(wsaaSub,12)
		|| wsaaStatOk.isTrue())) {
			wsaaSub.add(1);
			if (isGT(wsaaSub,12)) {
				sv.selectErr.set(h137);
				return ;
			}
			if (isEQ(sv.pstatcode,t5679rec.cnPremStat[wsaaSub.toInt()])) {
				wsaaStat.set("Y");
			}
		}
	}
}
protected void updateErrorIndicators()
{
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
		scrnparams.statuz.set(varcom.endp);
		return;
	}
	scrnparams.function.set(varcom.supd);
	callSr60tio();
	/*READ-NEXT-MODIFIED-RECORD*/
	scrnparams.function.set(varcom.srdn);
	callSr60tio();
	/*EXIT*/
	/**     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/
}


protected void update3000()
{
	return;
}

protected void whereNext4000()
{
			nextProgram();
}

protected void nextProgram()
{
	wsspcomn.nextprog.set(wsaaProg);
	if(isNE(wsspcomn.chdrMplnum,SPACES) && (zctxpfList.size()==1) &&
			isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {		
		optswchrec.optsSelOptno.set(1);
		line();
		wsaaOptionSelected = "Y";
		switchPage();
		return;
	}
	if (isEQ(scrnparams.statuz, varcom.kill)) {
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspwindow.value.set(SPACES);
		scrnparams.function.set("HIDEW");
		return;
	}
	/* If a valid Function Key has been pressed, switch to*/
	/* appropriate program*/
	if (validFkey.isTrue()) {
		optswchrec.optsSelType.set("F");
		optswchrec.optsSelCode.set(scrnparams.statuz);
		optswchrec.optsSelOptno.set(ZERO);
		wsaaValidFkey.set("N");
		wsaaOptionSelected = "Y";
		return;
	}
	if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		callSr60tio();
	}
	while ( !(isNE(sv.select, SPACES)
	|| isEQ(scrnparams.statuz, varcom.endp))) {
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.srdn);
		callSr60tio();
	}
	
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		optswchrec.optsSelType.set(SPACES);
		optswchrec.optsSelOptno.set(ZERO);
		switchPage();
		return;
	}
	if (isEQ(sv.select, "1")) {
		optswchrec.optsSelOptno.set(1);
		lineSelect();
		wsaaOptionSelected = "Y";
	}
	switchPage();
}

protected void switchPage()
{
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
	&& isEQ(wsaaOptionSelected, "N")) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);		
		programStack4300();
		if(isNE(wsspcomn.chdrMplnum,SPACES)&&(zctxpfList.size()==1)) 
			wsspcomn.programPtr.add(1);
		return ;
	}
	programStack4300();
	if (isEQ(optswchrec.optsStatuz, varcom.endp)) {
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	else {
		wsspcomn.programPtr.add(1);
	}
}

protected void lineSelect()
{
	line();
	sv.select.set(SPACES);
	wsspcomn.chdrChdrnum.set(sv.schdrnum);
	wsspcomn.chdrCnttype.set(sv.scnttype);
	wsspcomn.confirmationKey.set(sv.taxPeriodFrom);
	wsspcomn.currto.set(sv.taxPeriodFrom);//ILIFE-8031
	scrnparams.function.set(varcom.supd);
	callSr60tio();
}

protected void line()
{
	optswchrec.optsSelType.set("L");
	optswchrec.optsSelCode.set(SPACES);	
	
}

protected void programStack4300()
{
	/*STCK*/
	/* The 'STCK' function saves the original switching stack and*/
	/* replaces it with the stack specified on T1661.*/
	optswchrec.optsItemCompany.set(wsspcomn.company);
	optswchrec.optsFunction.set("STCK");
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if (isNE(optswchrec.optsStatuz, varcom.oK)
	&& isNE(optswchrec.optsStatuz, varcom.endp)) {
		syserrrec.function.set("STCK");
		syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
		syserrrec.statuz.set(optswchrec.optsStatuz);
		syserrrec.iomod.set("OPTSWCH");
		fatalError600();
	}
	/*EXIT*/
}
protected void callSr60tio()
{
	/*START*/
	processScreen("SR60T", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		syserrrec.params.set(scrnparams.screenParams);
		fatalError600();
	}
	/*EXIT*/
}

}
