package com.csc.life.contractservicing.dataaccess;

import com.csc.life.unitlinkedprocessing.dataaccess.UtrnpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZrutnrvTableDAM.java
 * Date: Sun, 30 Aug 2009 03:53:30
 * Class transformed from ZRUTNRV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZrutnrvTableDAM extends UtrnpfTableDAM {

	public ZrutnrvTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZRUTNRV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", PLNSFX"
		             + ", TRANNO"
		             + ", PRCSEQ"
		             + ", VRTFND"
		             + ", UNITYP";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VRTFND, " +
		            "UNITYP, " +
		            "TRANNO, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "JCTLJNUMPR, " +
		            "FUNDRATE, " +
		            "UNITSA, " +
		            "STRPDATE, " +
		            "NDFIND, " +
		            "NOFUNT, " +
		            "NOFDUNT, " +
		            "MONIESDT, " +
		            "PRICEDT, " +
		            "PRICEUSED, " +
		            "UBREPR, " +
		            "CRTABLE, " +
		            "CNTCURR, " +
		            "FDBKIND, " +
		            "COVDBTIND, " +
		            "INCINUM, " +
		            "INCIPERD01, " +
		            "INCIPERD02, " +
		            "INCIPRM01, " +
		            "INCIPRM02, " +
		            "CNTAMNT, " +
		            "FNDCURR, " +
		            "FUNDAMNT, " +
		            "SACSCODE, " +
		            "SACSTYP, " +
		            "GENLCDE, " +
		            "CONTYP, " +
		            "TRIGER, " +
		            "TRIGKY, " +
		            "PRCSEQ, " +
		            "SVP, " +
		            "DISCFA, " +
		            "PERSUR, " +
		            "CRCDTE, " +
		            "CIUIND, " +
		            "SWCHIND, " +
		            "USTMNO, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "PLNSFX ASC, " +
		            "TRANNO ASC, " +
		            "PRCSEQ ASC, " +
		            "VRTFND ASC, " +
		            "UNITYP ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "PLNSFX DESC, " +
		            "TRANNO DESC, " +
		            "PRCSEQ DESC, " +
		            "VRTFND DESC, " +
		            "UNITYP DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               unitVirtualFund,
                               unitType,
                               tranno,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               jobnoPrice,
                               fundRate,
                               unitSubAccount,
                               strpdate,
                               nowDeferInd,
                               nofUnits,
                               nofDunits,
                               moniesDate,
                               priceDateUsed,
                               priceUsed,
                               unitBarePrice,
                               crtable,
                               cntcurr,
                               feedbackInd,
                               covdbtind,
                               inciNum,
                               inciPerd01,
                               inciPerd02,
                               inciprm01,
                               inciprm02,
                               contractAmount,
                               fundCurrency,
                               fundAmount,
                               sacscode,
                               sacstyp,
                               genlcde,
                               contractType,
                               triggerModule,
                               triggerKey,
                               procSeqNo,
                               svp,
                               discountFactor,
                               surrenderPercent,
                               crComDate,
                               canInitUnitInd,
                               switchIndicator,
                               ustmno,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(38);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getPlanSuffix().toInternal()
					+ getTranno().toInternal()
					+ getProcSeqNo().toInternal()
					+ getUnitVirtualFund().toInternal()
					+ getUnitType().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, procSeqNo);
			what = ExternalData.chop(what, unitVirtualFund);
			what = ExternalData.chop(what, unitType);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller8 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller9 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller49 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller6.setInternal(planSuffix.toInternal());
	nonKeyFiller7.setInternal(unitVirtualFund.toInternal());
	nonKeyFiller8.setInternal(unitType.toInternal());
	nonKeyFiller9.setInternal(tranno.toInternal());
	nonKeyFiller49.setInternal(procSeqNo.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(294);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ getRider().toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ nonKeyFiller8.toInternal()
					+ nonKeyFiller9.toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getJobnoPrice().toInternal()
					+ getFundRate().toInternal()
					+ getUnitSubAccount().toInternal()
					+ getStrpdate().toInternal()
					+ getNowDeferInd().toInternal()
					+ getNofUnits().toInternal()
					+ getNofDunits().toInternal()
					+ getMoniesDate().toInternal()
					+ getPriceDateUsed().toInternal()
					+ getPriceUsed().toInternal()
					+ getUnitBarePrice().toInternal()
					+ getCrtable().toInternal()
					+ getCntcurr().toInternal()
					+ getFeedbackInd().toInternal()
					+ getCovdbtind().toInternal()
					+ getInciNum().toInternal()
					+ getInciPerd01().toInternal()
					+ getInciPerd02().toInternal()
					+ getInciprm01().toInternal()
					+ getInciprm02().toInternal()
					+ getContractAmount().toInternal()
					+ getFundCurrency().toInternal()
					+ getFundAmount().toInternal()
					+ getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ getGenlcde().toInternal()
					+ getContractType().toInternal()
					+ getTriggerModule().toInternal()
					+ getTriggerKey().toInternal()
					+ nonKeyFiller49.toInternal()
					+ getSvp().toInternal()
					+ getDiscountFactor().toInternal()
					+ getSurrenderPercent().toInternal()
					+ getCrComDate().toInternal()
					+ getCanInitUnitInd().toInternal()
					+ getSwitchIndicator().toInternal()
					+ getUstmno().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, nonKeyFiller8);
			what = ExternalData.chop(what, nonKeyFiller9);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, jobnoPrice);
			what = ExternalData.chop(what, fundRate);
			what = ExternalData.chop(what, unitSubAccount);
			what = ExternalData.chop(what, strpdate);
			what = ExternalData.chop(what, nowDeferInd);
			what = ExternalData.chop(what, nofUnits);
			what = ExternalData.chop(what, nofDunits);
			what = ExternalData.chop(what, moniesDate);
			what = ExternalData.chop(what, priceDateUsed);
			what = ExternalData.chop(what, priceUsed);
			what = ExternalData.chop(what, unitBarePrice);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, feedbackInd);
			what = ExternalData.chop(what, covdbtind);
			what = ExternalData.chop(what, inciNum);
			what = ExternalData.chop(what, inciPerd01);
			what = ExternalData.chop(what, inciPerd02);
			what = ExternalData.chop(what, inciprm01);
			what = ExternalData.chop(what, inciprm02);
			what = ExternalData.chop(what, contractAmount);
			what = ExternalData.chop(what, fundCurrency);
			what = ExternalData.chop(what, fundAmount);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, genlcde);
			what = ExternalData.chop(what, contractType);
			what = ExternalData.chop(what, triggerModule);
			what = ExternalData.chop(what, triggerKey);
			what = ExternalData.chop(what, nonKeyFiller49);
			what = ExternalData.chop(what, svp);
			what = ExternalData.chop(what, discountFactor);
			what = ExternalData.chop(what, surrenderPercent);
			what = ExternalData.chop(what, crComDate);
			what = ExternalData.chop(what, canInitUnitInd);
			what = ExternalData.chop(what, switchIndicator);
			what = ExternalData.chop(what, ustmno);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	public PackedDecimalData getProcSeqNo() {
		return procSeqNo;
	}
	public void setProcSeqNo(Object what) {
		setProcSeqNo(what, false);
	}
	public void setProcSeqNo(Object what, boolean rounded) {
		if (rounded)
			procSeqNo.setRounded(what);
		else
			procSeqNo.set(what);
	}
	public FixedLengthStringData getUnitVirtualFund() {
		return unitVirtualFund;
	}
	public void setUnitVirtualFund(Object what) {
		unitVirtualFund.set(what);
	}
	public FixedLengthStringData getUnitType() {
		return unitType;
	}
	public void setUnitType(Object what) {
		unitType.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public PackedDecimalData getJobnoPrice() {
		return jobnoPrice;
	}
	public void setJobnoPrice(Object what) {
		setJobnoPrice(what, false);
	}
	public void setJobnoPrice(Object what, boolean rounded) {
		if (rounded)
			jobnoPrice.setRounded(what);
		else
			jobnoPrice.set(what);
	}	
	public PackedDecimalData getFundRate() {
		return fundRate;
	}
	public void setFundRate(Object what) {
		setFundRate(what, false);
	}
	public void setFundRate(Object what, boolean rounded) {
		if (rounded)
			fundRate.setRounded(what);
		else
			fundRate.set(what);
	}	
	public FixedLengthStringData getUnitSubAccount() {
		return unitSubAccount;
	}
	public void setUnitSubAccount(Object what) {
		unitSubAccount.set(what);
	}	
	public PackedDecimalData getStrpdate() {
		return strpdate;
	}
	public void setStrpdate(Object what) {
		setStrpdate(what, false);
	}
	public void setStrpdate(Object what, boolean rounded) {
		if (rounded)
			strpdate.setRounded(what);
		else
			strpdate.set(what);
	}	
	public FixedLengthStringData getNowDeferInd() {
		return nowDeferInd;
	}
	public void setNowDeferInd(Object what) {
		nowDeferInd.set(what);
	}	
	public PackedDecimalData getNofUnits() {
		return nofUnits;
	}
	public void setNofUnits(Object what) {
		setNofUnits(what, false);
	}
	public void setNofUnits(Object what, boolean rounded) {
		if (rounded)
			nofUnits.setRounded(what);
		else
			nofUnits.set(what);
	}	
	public PackedDecimalData getNofDunits() {
		return nofDunits;
	}
	public void setNofDunits(Object what) {
		setNofDunits(what, false);
	}
	public void setNofDunits(Object what, boolean rounded) {
		if (rounded)
			nofDunits.setRounded(what);
		else
			nofDunits.set(what);
	}	
	public PackedDecimalData getMoniesDate() {
		return moniesDate;
	}
	public void setMoniesDate(Object what) {
		setMoniesDate(what, false);
	}
	public void setMoniesDate(Object what, boolean rounded) {
		if (rounded)
			moniesDate.setRounded(what);
		else
			moniesDate.set(what);
	}	
	public PackedDecimalData getPriceDateUsed() {
		return priceDateUsed;
	}
	public void setPriceDateUsed(Object what) {
		setPriceDateUsed(what, false);
	}
	public void setPriceDateUsed(Object what, boolean rounded) {
		if (rounded)
			priceDateUsed.setRounded(what);
		else
			priceDateUsed.set(what);
	}	
	public PackedDecimalData getPriceUsed() {
		return priceUsed;
	}
	public void setPriceUsed(Object what) {
		setPriceUsed(what, false);
	}
	public void setPriceUsed(Object what, boolean rounded) {
		if (rounded)
			priceUsed.setRounded(what);
		else
			priceUsed.set(what);
	}	
	public PackedDecimalData getUnitBarePrice() {
		return unitBarePrice;
	}
	public void setUnitBarePrice(Object what) {
		setUnitBarePrice(what, false);
	}
	public void setUnitBarePrice(Object what, boolean rounded) {
		if (rounded)
			unitBarePrice.setRounded(what);
		else
			unitBarePrice.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getFeedbackInd() {
		return feedbackInd;
	}
	public void setFeedbackInd(Object what) {
		feedbackInd.set(what);
	}	
	public FixedLengthStringData getCovdbtind() {
		return covdbtind;
	}
	public void setCovdbtind(Object what) {
		covdbtind.set(what);
	}	
	public PackedDecimalData getInciNum() {
		return inciNum;
	}
	public void setInciNum(Object what) {
		setInciNum(what, false);
	}
	public void setInciNum(Object what, boolean rounded) {
		if (rounded)
			inciNum.setRounded(what);
		else
			inciNum.set(what);
	}	
	public PackedDecimalData getInciPerd01() {
		return inciPerd01;
	}
	public void setInciPerd01(Object what) {
		setInciPerd01(what, false);
	}
	public void setInciPerd01(Object what, boolean rounded) {
		if (rounded)
			inciPerd01.setRounded(what);
		else
			inciPerd01.set(what);
	}	
	public PackedDecimalData getInciPerd02() {
		return inciPerd02;
	}
	public void setInciPerd02(Object what) {
		setInciPerd02(what, false);
	}
	public void setInciPerd02(Object what, boolean rounded) {
		if (rounded)
			inciPerd02.setRounded(what);
		else
			inciPerd02.set(what);
	}	
	public PackedDecimalData getInciprm01() {
		return inciprm01;
	}
	public void setInciprm01(Object what) {
		setInciprm01(what, false);
	}
	public void setInciprm01(Object what, boolean rounded) {
		if (rounded)
			inciprm01.setRounded(what);
		else
			inciprm01.set(what);
	}	
	public PackedDecimalData getInciprm02() {
		return inciprm02;
	}
	public void setInciprm02(Object what) {
		setInciprm02(what, false);
	}
	public void setInciprm02(Object what, boolean rounded) {
		if (rounded)
			inciprm02.setRounded(what);
		else
			inciprm02.set(what);
	}	
	public PackedDecimalData getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(Object what) {
		setContractAmount(what, false);
	}
	public void setContractAmount(Object what, boolean rounded) {
		if (rounded)
			contractAmount.setRounded(what);
		else
			contractAmount.set(what);
	}	
	public FixedLengthStringData getFundCurrency() {
		return fundCurrency;
	}
	public void setFundCurrency(Object what) {
		fundCurrency.set(what);
	}	
	public PackedDecimalData getFundAmount() {
		return fundAmount;
	}
	public void setFundAmount(Object what) {
		setFundAmount(what, false);
	}
	public void setFundAmount(Object what, boolean rounded) {
		if (rounded)
			fundAmount.setRounded(what);
		else
			fundAmount.set(what);
	}	
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}	
	public FixedLengthStringData getGenlcde() {
		return genlcde;
	}
	public void setGenlcde(Object what) {
		genlcde.set(what);
	}	
	public FixedLengthStringData getContractType() {
		return contractType;
	}
	public void setContractType(Object what) {
		contractType.set(what);
	}	
	public FixedLengthStringData getTriggerModule() {
		return triggerModule;
	}
	public void setTriggerModule(Object what) {
		triggerModule.set(what);
	}	
	public FixedLengthStringData getTriggerKey() {
		return triggerKey;
	}
	public void setTriggerKey(Object what) {
		triggerKey.set(what);
	}	
	public PackedDecimalData getSvp() {
		return svp;
	}
	public void setSvp(Object what) {
		setSvp(what, false);
	}
	public void setSvp(Object what, boolean rounded) {
		if (rounded)
			svp.setRounded(what);
		else
			svp.set(what);
	}	
	public PackedDecimalData getDiscountFactor() {
		return discountFactor;
	}
	public void setDiscountFactor(Object what) {
		setDiscountFactor(what, false);
	}
	public void setDiscountFactor(Object what, boolean rounded) {
		if (rounded)
			discountFactor.setRounded(what);
		else
			discountFactor.set(what);
	}	
	public PackedDecimalData getSurrenderPercent() {
		return surrenderPercent;
	}
	public void setSurrenderPercent(Object what) {
		setSurrenderPercent(what, false);
	}
	public void setSurrenderPercent(Object what, boolean rounded) {
		if (rounded)
			surrenderPercent.setRounded(what);
		else
			surrenderPercent.set(what);
	}	
	public PackedDecimalData getCrComDate() {
		return crComDate;
	}
	public void setCrComDate(Object what) {
		setCrComDate(what, false);
	}
	public void setCrComDate(Object what, boolean rounded) {
		if (rounded)
			crComDate.setRounded(what);
		else
			crComDate.set(what);
	}	
	public FixedLengthStringData getCanInitUnitInd() {
		return canInitUnitInd;
	}
	public void setCanInitUnitInd(Object what) {
		canInitUnitInd.set(what);
	}	
	public FixedLengthStringData getSwitchIndicator() {
		return switchIndicator;
	}
	public void setSwitchIndicator(Object what) {
		switchIndicator.set(what);
	}	
	public PackedDecimalData getUstmno() {
		return ustmno;
	}
	public void setUstmno(Object what) {
		setUstmno(what, false);
	}
	public void setUstmno(Object what, boolean rounded) {
		if (rounded)
			ustmno.setRounded(what);
		else
			ustmno.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getInciprms() {
		return new FixedLengthStringData(inciprm01.toInternal()
										+ inciprm02.toInternal());
	}
	public void setInciprms(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInciprms().getLength()).init(obj);
	
		what = ExternalData.chop(what, inciprm01);
		what = ExternalData.chop(what, inciprm02);
	}
	public PackedDecimalData getInciprm(BaseData indx) {
		return getInciprm(indx.toInt());
	}
	public PackedDecimalData getInciprm(int indx) {

		switch (indx) {
			case 1 : return inciprm01;
			case 2 : return inciprm02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInciprm(BaseData indx, Object what) {
		setInciprm(indx, what, false);
	}
	public void setInciprm(BaseData indx, Object what, boolean rounded) {
		setInciprm(indx.toInt(), what, rounded);
	}
	public void setInciprm(int indx, Object what) {
		setInciprm(indx, what, false);
	}
	public void setInciprm(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInciprm01(what, rounded);
					 break;
			case 2 : setInciprm02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInciperds() {
		return new FixedLengthStringData(inciPerd01.toInternal()
										+ inciPerd02.toInternal());
	}
	public void setInciperds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInciperds().getLength()).init(obj);
	
		what = ExternalData.chop(what, inciPerd01);
		what = ExternalData.chop(what, inciPerd02);
	}
	public PackedDecimalData getInciperd(BaseData indx) {
		return getInciperd(indx.toInt());
	}
	public PackedDecimalData getInciperd(int indx) {

		switch (indx) {
			case 1 : return inciPerd01;
			case 2 : return inciPerd02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInciperd(BaseData indx, Object what) {
		setInciperd(indx, what, false);
	}
	public void setInciperd(BaseData indx, Object what, boolean rounded) {
		setInciperd(indx.toInt(), what, rounded);
	}
	public void setInciperd(int indx, Object what) {
		setInciperd(indx, what, false);
	}
	public void setInciperd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInciPerd01(what, rounded);
					 break;
			case 2 : setInciPerd02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		planSuffix.clear();
		tranno.clear();
		procSeqNo.clear();
		unitVirtualFund.clear();
		unitType.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		rider.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		nonKeyFiller8.clear();
		nonKeyFiller9.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		jobnoPrice.clear();
		fundRate.clear();
		unitSubAccount.clear();
		strpdate.clear();
		nowDeferInd.clear();
		nofUnits.clear();
		nofDunits.clear();
		moniesDate.clear();
		priceDateUsed.clear();
		priceUsed.clear();
		unitBarePrice.clear();
		crtable.clear();
		cntcurr.clear();
		feedbackInd.clear();
		covdbtind.clear();
		inciNum.clear();
		inciPerd01.clear();
		inciPerd02.clear();
		inciprm01.clear();
		inciprm02.clear();
		contractAmount.clear();
		fundCurrency.clear();
		fundAmount.clear();
		sacscode.clear();
		sacstyp.clear();
		genlcde.clear();
		contractType.clear();
		triggerModule.clear();
		triggerKey.clear();
		nonKeyFiller49.clear();
		svp.clear();
		discountFactor.clear();
		surrenderPercent.clear();
		crComDate.clear();
		canInitUnitInd.clear();
		switchIndicator.clear();
		ustmno.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}