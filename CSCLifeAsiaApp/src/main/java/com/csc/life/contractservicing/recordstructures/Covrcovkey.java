package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:22
 * Description:
 * Copybook name: COVRCOVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrcovkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrcovFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrcovKey = new FixedLengthStringData(64).isAPartOf(covrcovFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrcovChdrcoy = new FixedLengthStringData(1).isAPartOf(covrcovKey, 0);
  	public FixedLengthStringData covrcovChdrnum = new FixedLengthStringData(8).isAPartOf(covrcovKey, 1);
  	public FixedLengthStringData covrcovLife = new FixedLengthStringData(2).isAPartOf(covrcovKey, 9);
  	public FixedLengthStringData covrcovCrtable = new FixedLengthStringData(4).isAPartOf(covrcovKey, 11);
  	public FixedLengthStringData covrcovCoverage = new FixedLengthStringData(2).isAPartOf(covrcovKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covrcovKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrcovFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrcovFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}