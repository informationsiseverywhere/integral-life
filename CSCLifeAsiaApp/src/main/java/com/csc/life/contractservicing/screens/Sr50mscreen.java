package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50mscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {19, 23, 2, 40}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50mScreenVars sv = (Sr50mScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50mscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50mScreenVars screenVars = (Sr50mScreenVars)pv;
		screenVars.grsprm.setClassString("");
		screenVars.cntfee.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.stampduty.setClassString("");
	}

/**
 * Clear all the variables in Sr50mscreen
 */
	public static void clear(VarModel pv) {
		Sr50mScreenVars screenVars = (Sr50mScreenVars) pv;
		screenVars.grsprm.clear();
		screenVars.cntfee.clear();
		screenVars.instPrem.clear();
		screenVars.stampduty.clear();
	}
}
