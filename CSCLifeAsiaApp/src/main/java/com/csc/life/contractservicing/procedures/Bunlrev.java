/*
 * File: Bunlrev.java
 * Date: 29 August 2009 22:37:56
 * Author: Quipoz Limited
 * 
 * Class transformed from BUNLREV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.IncirevTableDAM;
import com.csc.life.contractservicing.recordstructures.Bunlrevrec;
import com.csc.life.unitlinkedprocessing.dataaccess.InciTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*
* BUNLREV - Billing Change Generic Reversal Subroutine
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Overview
* ~~~~~~~~
*
* One of the  facilities  provided  by  the  billing  change
* subsystem  is to allow changing the billing frequency of a
* contract. The function of this program is to  support  the
* reversal  of  the  unit linked part of this facility. This
* program is to  be  obtained  from  T5671  which  keyed  on
* transaction  code  +  CRTABLE.  The program will be called
* once for every unit linked coverage.
*
*
* INCIREV
* ~~~~~~~
*
* Create a logical file INCIREV based on the  physical  file
* INCIPF  and  include  all the fields from the physical and
* with the following key order:-
*
*     CHDRCOY
*     CHDRNUM
*     LIFE
*     COVERAGE
*     RIDER
*     PLNSFX
*     TRANNO
*
* Last ion first out(LIFO).
*
*
* BUNLREVREC(Linkage)
* ~~~~~~~~~~~~~~~~~~~
*
* Create  a  copybook  BUNLREVREC  to  have  the   following
* fields:-
*
*     01 BUNL-CHGREV-REC.
*        03  BUNL-FUNCTION                PIC X(05).
*        03  BUNL-STATUZ                  PIC X(04).
*        03  BUNL-COMPANY                 PIC X(01).
*        03  BUNL-CHDRNUM                 PIC X(08).
*        03  BUNL-LIFE                    PIC X(02).
*        03  BUNL-COVERAGE                PIC X(02).
*        03  BUNL-RIDER                   PIC X(02).
*        03  BUNL-PLAN-SUFFIX             PIC S9(04).
*        03  BUNL-EFFDATE                 PIC 9(08).
*        03  BUNL-OLD-TRANNO              PIC 9(05).
*        03  BUNL-NEW-TRANNO              PIC 9(05).
*        03  BUNL-CRTABLE                 PIC X(04).
*        03  FILLER                       PIC X(25).
*
*
* BUNLREV
* ~~~~~~~
*
* Files: INCIREV - Individual Increase Details
*        INCI - Individual Increase Details
*
*
* 100-MAINLINE Section
* ~~~~~~~~~~~~~~~~~~~~
*
* Set BUNL-STATUZ to '****'.
*
* Perform 200-INITIALISATION.
*
* If BUNL-STATUZ not = '****'
*     Exit this section.
*
* If BUNL-FUNCTION = 'BCHGR'
*     Perform 300-Reversal.
*
* 190-EXIT.
*     EXIT PROGRAM.
*
*
* 200-INITIALISATION Section
* ~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Set WSAA-KEY-BREAK to 'N'.
*
*
* 300-Reversal Section
* ~~~~~~~~~~~~~~~~~~~~
*
* Perform 800-FIND-THE-FIRST-RECORD.
*
* Perform 1000-MAIN-PROCESSING
*     until WSAA-KEY-BREAK = 'Y'.
*
*
* 800-FIND-THE-FIRST-RECORD Section
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Set INCIREV-PARAMS to space.
* Set INCIREV-CHDRCOY to BUNL-COMPANY.
* Set INCIREV-CHDRNUM to BUNL-CHDRNUM.
* Set INCIREV-COVERAGE to BUNL-COVERAGE.
* Set INCIREV-LIFE to BUNL-LIFE.
* Set INCIREV-RIDER to BUNL-RIDER.
* Set INCIREV-PLAN-SUFFIX to BUNL-PLAN-SUFFIX.
* Set INCIREV-TRANNO to BUNL-OLD-TRANNO.
* Set INCIREV-FUNCTION to 'BEGN'
*
* Call 'INCIREVIO' to read the record.
*
* Perform normal error handling.
*
* If INCIREV-STATUZ = ENDP
*     Set WSAA-KEY-BREAK to 'Y'
*     Exit this section.
*
* If Key break(Up to and including TRANNO)
*     Set WSAA-KEY-BREAK to 'Y'
*     Perform normal error handling.
*
*
* 1000-MAIN-PROCESSING Section
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Rewrite   all  the  invalid  relevant  INCI  records  with
* validflag equals to '1' and delete all the valid records.
*
* Set up the key of INCI from INCIREV.
* Call 'INCIIO' with function 'READH' to read and  hold  the
* record.
* Perform normal error handling.
*
* Call  'INCIIO'  with  function 'DELET' to delete the valid
* record.
* Perform normal error checking.
*
* Set INCI-FUNCTION to 'NEXTR'.
*
* Call 'INCIIO' to read and hold the next record.
* Perform normal error checking.
*
* If key break
*     Perform fatal error.
*
* This record should be an invalid record(VALIDFLAG = 2).
*
* Set INCI-VALIDFLAG to '1'.
*
* Rewrite the  record  by  calling  'INCIIO'  with  function
* 'REWRT'.
* Perform normal error checking.
*
* Set INCIREV-FUNCTION to 'NEXTR'.
*
* Call 'INCIREVIO' to read and hold the next record.
*
* Perform normal error checking.
*
* If end of file
*     Set WSAA-KEY-BREAK to 'Y'
*     Exit this section.
*
* If key break(Up to and including TRANNO)
*     Set WSAA-KEY-BREAK to 'Y'
*     Exit this section.
*
*****************************************************************
* </pre>
*/
public class Bunlrev extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaKeyBreak = "";
	private Bunlrevrec bunlrevrec = new Bunlrevrec();
		/*Unit linked increases*/
	private InciTableDAM inciIO = new InciTableDAM();
		/*Individual Increase Details*/
	private IncirevTableDAM incirevIO = new IncirevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit190, 
		exit890, 
		exit1090, 
		exit590
	}

	public Bunlrev() {
		super();
	}

public void mainline(Object... parmArray)
	{
		bunlrevrec.chgrevRec = convertAndSetParam(bunlrevrec.chgrevRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
		}
		finally{
			exit190();
		}
	}

protected void main110()
	{
		bunlrevrec.statuz.set("****");
		initialise200();
		if (isNE(bunlrevrec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit190);
		}
		if (isEQ(bunlrevrec.function,"BCHGR")) {
			reversal300();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise200()
	{
		/*SET-VALUES*/
		wsaaKeyBreak = "N";
		/*EXIT*/
	}

protected void reversal300()
	{
		/*REVERSAL-SEQUENCE*/
		findTheFirstRecord800();
		while ( !(isEQ(wsaaKeyBreak,"Y"))) {
			mainProcessing1000();
		}
		
		/*EXIT*/
	}

protected void findTheFirstRecord800()
	{
		try {
			beginReading810();
		}
		catch (GOTOException e){
		}
	}

protected void beginReading810()
	{
		incirevIO.setParams(SPACES);
		incirevIO.setChdrcoy(bunlrevrec.company);
		incirevIO.setChdrnum(bunlrevrec.chdrnum);
		incirevIO.setCoverage(bunlrevrec.coverage);
		incirevIO.setLife(bunlrevrec.life);
		incirevIO.setRider(bunlrevrec.rider);
		incirevIO.setPlanSuffix(bunlrevrec.planSuffix);
		incirevIO.setTranno(bunlrevrec.oldTranno);
		incirevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incirevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, incirevIO);
		if ((isNE(incirevIO.getStatuz(),varcom.oK))
		&& (isNE(incirevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			databaseError500();
		}
		if (isEQ(incirevIO.getStatuz(),varcom.endp)) {
			wsaaKeyBreak = "Y";
			goTo(GotoLabel.exit890);
		}
		if ((isNE(incirevIO.getChdrcoy(),bunlrevrec.company))
		|| (isNE(incirevIO.getChdrnum(),bunlrevrec.chdrnum))
		|| (isNE(incirevIO.getCoverage(),bunlrevrec.coverage))
		|| (isNE(incirevIO.getRider(),bunlrevrec.rider))
		|| (isNE(incirevIO.getLife(),bunlrevrec.life))
		|| (isNE(incirevIO.getPlanSuffix(),bunlrevrec.planSuffix))
		|| (isNE(incirevIO.getTranno(),bunlrevrec.oldTranno))) {
			wsaaKeyBreak = "Y";
		}
	}

protected void mainProcessing1000()
	{
		try {
			readValidRecord1010();
			getNextRecord1020();
			reverseRecord1030();
			getNextRecord1040();
		}
		catch (GOTOException e){
		}
	}

protected void readValidRecord1010()
	{
		inciIO.setParams(SPACES);
		inciIO.setChdrcoy(incirevIO.getChdrcoy());
		inciIO.setChdrnum(incirevIO.getChdrnum());
		inciIO.setCoverage(incirevIO.getCoverage());
		inciIO.setLife(incirevIO.getLife());
		inciIO.setRider(incirevIO.getRider());
		inciIO.setPlanSuffix(incirevIO.getPlanSuffix());
		inciIO.setInciNum(incirevIO.getInciNum());
		inciIO.setSeqno(incirevIO.getSeqno());
		inciIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
		inciIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
	}

protected void getNextRecord1020()
	{
		inciIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, inciIO);
		if ((isNE(inciIO.getStatuz(),varcom.oK))
		&& (isNE(inciIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
		if (isEQ(inciIO.getStatuz(),varcom.endp)) {
			wsaaKeyBreak = "Y";
			goTo(GotoLabel.exit1090);
		}
		if ((isNE(inciIO.getChdrcoy(),incirevIO.getChdrcoy()))
		|| (isNE(inciIO.getChdrnum(),incirevIO.getChdrnum()))
		|| (isNE(inciIO.getCoverage(),incirevIO.getCoverage()))
		|| (isNE(inciIO.getLife(),incirevIO.getLife()))
		|| (isNE(inciIO.getRider(),incirevIO.getRider()))
		|| (isNE(inciIO.getPlanSuffix(),incirevIO.getPlanSuffix()))
		|| (isNE(inciIO.getValidflag(),"2"))) {
			wsaaKeyBreak = "Y";
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
	}

protected void reverseRecord1030()
	{
		inciIO.setValidflag("1");
		inciIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
	}

protected void getNextRecord1040()
	{
		incirevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incirevIO);
		if ((isNE(incirevIO.getStatuz(),varcom.oK))
		&& (isNE(incirevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			databaseError500();
		}
		if (isEQ(incirevIO.getStatuz(),varcom.endp)) {
			wsaaKeyBreak = "Y";
			goTo(GotoLabel.exit1090);
		}
		if ((isNE(incirevIO.getChdrcoy(),bunlrevrec.company))
		|| (isNE(incirevIO.getChdrnum(),bunlrevrec.chdrnum))
		|| (isNE(incirevIO.getCoverage(),bunlrevrec.coverage))
		|| (isNE(incirevIO.getRider(),bunlrevrec.rider))
		|| (isNE(incirevIO.getLife(),bunlrevrec.life))
		|| (isNE(incirevIO.getPlanSuffix(),bunlrevrec.planSuffix))
		|| (isNE(incirevIO.getTranno(),bunlrevrec.oldTranno))) {
			wsaaKeyBreak = "Y";
		}
	}

protected void databaseError500()
	{
		try {
			para500();
		}
		catch (GOTOException e){
		}
		finally{
			exit590();
		}
	}

protected void para500()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit590()
	{
		bunlrevrec.statuz.set(varcom.bomb);
		exit190();
	}
}
