/*
 * File: P6668.java
 * Date: 30 August 2009 0:49:31
 * Author: Quipoz Limited
 *
 * Class transformed from P6668.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.screens.S6668ScreenVars;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcolf1TableDAM;
import com.csc.life.general.dataaccess.AnrlcntTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5689rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Th611rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*  S6668/P6668 - Billing Transaction Screen
*  ----------------------------------------
*  This program is selected from the  Billing  Change  sub-menu
*  program P6666.
*  Initialise
*  ----------
*  Skip  this  section  if returning from an optional selection
*  (current stack position action flag '*').
*  The details of  the  contract  being  worked  on  have  been
*  stored  in  the  CHDR  I/O  module.  Retrieve  the necessary
*  details using the RETRV  command  and  the  CHDRMJA  logical
*  view.
*  Look  up  the following descriptions and names and output to
*  the screen:
*       Contract Type, (CNTTYPE)    - long description from
*                                     T5688,
*       Currency and Register from the contract header record,
*       Contract Status, (STATCODE) - short description from
*                                     T3623,
*       Premium Status, (PSTCDE) - short description from T3588
*       Contract commencement date, (OCCDATE) - from the
*                                     contract header,
*       Bill-to-date, (BTDATE)      - from the contract header,
*       Paid-to-date, (PTDATE)      - from the contract header,
*       Life Assured and Joint life details;
*            to obtain the life assured and joint-life details
*            (if any) do the following;-
*               - READR the life details using LIFEMJA (for
*                 this contract number, joint-life number
*                 '00'). Format the name accordingly.
*               - READR the joint-life details using LIFEMJA
*                 (for this contract number, joint-life
*                 number '01'). Format the name accordingly.
*       Owner, read the client details (CLTS),
*       Joint-owner, if any, read the client details (CLTS)
*  All of the above data is protected from being modified.  The
*  following fields are also output but these may be modified.
*       Billing frequency, BILLFREQ from the contract header,
*                          Description from T3590 (keyed by
*                          BILLFREQ)
*       Billing method,    BILLCHNL from the contract header,
*                          Description from T3590 (keyed by
*                          BILLCHNL)
*       Billing day,       The DD portion of BILLCD from the
*                          contract header.Display the Billing
*                          Date (protected),
*                          i.e. BILLING DATE: DD/MM/YYYY
*  If Payor already exists, then
*     Output '+' to the payor indicator
*     Store the current value of PAYOR.
*  If a DD mandate already exists, then
*     Output '+' to the DD mandate indicator
*     Store the current value of MANDREF.
*  If Group details already exist, then
*     Output '+' to the Group indicator
*     Store the current value of GROUP.
*  If Anniversary rules already exist, then
*     Output '+' to the Anniversary rules indicator.
*  Validation
*  ----------
*  Skip  this  section  if returning from an optional selection
*  (current stack position action flag '*').
*  If F11 (KILL) is pressed, then skip the validation and  exit
*  from the program.
*  Validate  the Billing frequency against the entries on T3590
*  (keyed by  frequency).  If  found,  obtain  the  description
*  otherwise, if not found, display an error message.
*  Validate  the Method of Payment (MOP) against the entries on
*  T3620 (keyed by BILLCHNL). If found, obtain the  description
*  otherwise if not found, display an error message.
*  Validate  the  billing  day against T5689 (keyed by contract
*  type) and ensure that for the Billing Channel  method  (MOP)
*  that  this day entered is on the table, otherwise display an
*  error message. If valid, re-display the BILLCD with the  new
*  day in it.
*  Validate  the  MOP gainst T3620 (key MOP).
*  If the Billing channel is altered to GROUP, then from  T3620
*  it  can be ascertained whether group details are allowed for
*  this type of billing. If, from  T3620,  they  are  required,
*  then  place an 'X' in the Group indicator. This will display
*  the group window.
*  Refer to the validation rules in P5004 that  are  applicable
*  to the relevant fields above.
*  Updating
*  --------
*  Skip  this  section  if returning from an optional selection
*  (current stack position action flag '*').
*  If F11 (KILL) is pressed, then skip the  updating  and  exit
*  from the program.
*
*  If any Increase records exist for this contract (INCR), then
*  Warning Message 'WARNING: Incr Pend'g del' will be displayed.
*  If the  user  presses enter then these INCR records will  be
*  deleted  and the  increase  recalculated as  normal  through
*  batch processing.  Additionally, any  COVR records that have
*  the COVR-INDEXATION-IND set to 'P' (Pending) will be updated
*  to have spaces in this field.
*
*  Next Program
*  ------------
*  If a selection has been made, then
*     RETRV the Contract Header (CHDRMJA).
*  Check  each  'selection'  indicator in turn. If an option is
*  selected, call  GENSSWCH  with  the  appropriate  action  to
*  retrieve the optional program switching:
*            OPTION            DESCRIPTION
*              A         -     P6663  Payor
*              B         -     P6669  Group
*              C         -     P6667  DD Mandate
*              D         -     P5605  Anniversary Rules
*  These  programs  are  accessed  from reading T1675, keyed by
*  Transaction number concatenated with  program  number,  e.g.
*  the table entry required for this program is T5226668.
*  For  the  first  one selected, save the next set of programs
*  (up to 8) from  the  program  stack  and  flag  the  current
*  position with '*'.
*  If  a selection had been made previously, its selected field
*  would contain a '?'. In this case, cross check  the  details
*  according   to  the  rules  defined  in  the  initialisation
*  section. Set the selection to blank  if  there  are  now  no
*  details, otherwise set it to '+' if there are.
*  If  a  selection  is  made  (selection  field  X),  load the
*  program  stack  with  the  programs  returned  by  GENSSWCH,
*  replace  the 'X' in the selection field with a '?', add 1 to
*  the program pointer and exit.
*  Once all the selections  have  been  serviced,  re-load  the
*  saved  programs  onto  the stack, and blank out the '*'. Set
*  the next  program  name  to  the  current  screen  name  (to
*  re-display the screen).
*  If  PAYOR is selected, then the DD mandate details must also
*  be selected by moving 'X' to the 'DD'  indicator.  This  has
*  the  effect  of  automatically windowing into the DD mandate
*  screen.
*  If the Frequency is changed, then  call  the  AT  submission
*  module  to  submit P6668AT, passing the Company and Contract
*  Number as  the  primary  number.  This  is  to  process  the
*  Contract header, Components and write a PTRN record.
*  Check for changes in the PAYOR, GROUP, MOP, BILLDAY,MANDATE.
*  If there are changes, then
*     PROCESS CONTRACT HEADER
*     Update the contract header as follows:-
*        - set the valid flag to '2' and set the effective
*          date to default to todays date and rewrite the
*          record using the REWRIT function
*        - write a new version of the contract header
*          as follows:-
*        - set the valid flag to '1'
*        - update the transaction number, increment by +1 and
*          update the transaction date using the default of
*          todays date
*        - update the contract status code with the status
*          code from T5679 (keyed by transaction number) only
*          if code from T5679 is other than blank
*        - alter the premium status if the status field on
*          T5679 for regular or single premium have entries
*          If entries are present, then check the Billing
*          Frequency from the Contract header. If Billing
*          Frequency is'00', i.e. single premium, otherwise
*          use the entry in the regular premium field
*        - set the 'Effective From' date to todays date
*        - set the 'Effective To' date to VRCM-MAX-DATE
*        - if the billing method has changed, set the billing
*          method to the screen value
*        - if the billing day has changed, set the billing day
*          of BILLCD to the screen value
*        - write the new CHDR record using the WRITR function
*     PROCESS POLICY TRANSACTION RECORD
*     ---------------------------------
*     Write a PTRN record as follows:-
*           - contract key from contract header
*           - transaction number from contract header
*           - transaction effective date is todays date
*  Otherwise
*     REWRT the old Contract header to release the READH.
*  If 'AT' is being invoked, then
*         CALL 'SFTLOCK' to transfer the contract lock to 'AT'
*                        using the 'TOAT' function
*  Otherwise
*         CALL 'SFTLOCK' using the UNLK function
*                        to unlock the contract
*  If  nothing is selected, continue by adding 1 to the program
*  pointer and exit from the program.
*
*****************************************************************
* </pre>
*/
public class P6668 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6668");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaFactor = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler = new FixedLengthStringData(11).isAPartOf(wsaaFactor, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaReminder = new ZonedDecimalData(5, 5).isAPartOf(filler, 6);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(191);
	private PackedDecimalData wsaaAtTransactionDate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaAtTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 5);
	private PackedDecimalData wsaaAtUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 9);
	private FixedLengthStringData wsaaAtTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 13);
	private FixedLengthStringData wsaaAtBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 17);
	private FixedLengthStringData wsaaAtBillchnl = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData wsaaAtBillday = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 21);
	private FixedLengthStringData wsaaAtMandref = new FixedLengthStringData(5).isAPartOf(wsaaTransactionRec, 31);
	private FixedLengthStringData wsaaAtGrupkey = new FixedLengthStringData(12).isAPartOf(wsaaTransactionRec, 36);
	private FixedLengthStringData wsaaAtMembsel = new FixedLengthStringData(10).isAPartOf(wsaaTransactionRec, 48);
	private ZonedDecimalData wsaaAtToday = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 58);
	private FixedLengthStringData wsaaAtFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 66);

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(16);
	private PackedDecimalData wsaaIdTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 0);
	private PackedDecimalData wsaaIdTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 4);
	private PackedDecimalData wsaaIdUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 8);
	private FixedLengthStringData wsaaIdTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 12);
		/* WSAA-STORE-FIELDS */
	private FixedLengthStringData wsaaGrupkey = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaPayrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaIncomeSeqno = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaMandref = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaMembsel = new FixedLengthStringData(10);
		/* WSAA-STORE-FIELDS-2 */
	private FixedLengthStringData wsaaNewGrupkey = new FixedLengthStringData(12);
	private String wsaaNewPayrnum = "";
	private ZonedDecimalData wsaaNewIncomeSeqno = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMandref = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaNewMembsel = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaFactor3 = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaFactor3, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaReminder3 = new ZonedDecimalData(5, 5).isAPartOf(filler3, 6);

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Mop = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);

	private FixedLengthStringData wsaaAtModule = new FixedLengthStringData(1);
	private Validator wsaaAtRequest = new Validator(wsaaAtModule, "Y");
	private Validator wsaaAtNoNeed = new Validator(wsaaAtModule, "N");

	private FixedLengthStringData wsaaRewrtCond = new FixedLengthStringData(1);
	private Validator wsaaRewrtChanges = new Validator(wsaaRewrtCond, "Y");
	private Validator wsaaRewrtNoChanges = new Validator(wsaaRewrtCond, "N");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);
		/* WSAA-MISC */
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaChkFinish = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaT5689Count = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);

	private FixedLengthStringData filler6 = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(8, 5, filler6, 0);

	private FixedLengthStringData wsaaInitBillcd = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaInitBilldd = new ZonedDecimalData(2, 0).isAPartOf(wsaaInitBillcd, 6).setUnsigned();
	private ZonedDecimalData wsaaInitBillcd9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaInitBillcd, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaNewBillcd = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaNewBillmm = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewBillcd, 4).setUnsigned();
	private ZonedDecimalData wsaaNewBilldd = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewBillcd, 6).setUnsigned();
	private ZonedDecimalData wsaaNewBillcd9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaNewBillcd, 0, REDEFINE).setUnsigned();
	private String wsaaFirstTime = "";
	private FixedLengthStringData wsaaAiindInd = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1);
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData wsaaNewFreqFound = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOldFreqFound = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaNewLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldinst = new ZonedDecimalData(9, 2).init(0);
	private ZonedDecimalData wsaaNewinst = new ZonedDecimalData(9, 2).init(0);
	private ZonedDecimalData wsaaTmpval = new ZonedDecimalData(9, 2).init(0);
	private String wsaaValidComp = "N";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub9 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaStatCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaMinPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaMaxPrem = new ZonedDecimalData(17, 2).init(0);
		/* WSAA-CALC-FREQS */
	private ZonedDecimalData wsaaCalcOldfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCalcNewfreq = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTh611Item = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTh611Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTh611Item, 0);
	private FixedLengthStringData wsaaTh611Currcode = new FixedLengthStringData(3).isAPartOf(wsaaTh611Item, 3);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
	private ZonedDecimalData wsaaAnnivExtractDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(wsaaAnnivExtractDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaAnnivExtractDay = new ZonedDecimalData(2, 0).isAPartOf(filler7, 6).setUnsigned();
	private String e032 = "E032";
	private String e186 = "E186";
	private String e482 = "E482";
	private String g620 = "G620";
	private String h048 = "H048";
	private String h049 = "H049";
	private String h060 = "H060";
	private String h062 = "H062";
	private String h063 = "H063";
	private String h093 = "H093";
	private String j008 = "J008";
	private String e398 = "E398";
	private String e368 = "E368";
	private String rl11 = "RL11";
	private String rl12 = "RL12";
	private String rl13 = "RL13";
	private String f321 = "F321";
	private String t075 = "T075";
	private String t076 = "T076";
	private String e300 = "E300";
		/* TABLES */
	private String t5688 = "T5688";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t3590 = "T3590";
	private String t3620 = "T3620";
	private String t5689 = "T5689";
	private String t5679 = "T5679";
	private String t6654 = "T6654";
	private String th611 = "TH611";
	private String t5541 = "T5541";
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private String t5687 = "T5687";
		/* FORMATS */
	private String chdrmjarec = "CHDRMJAREC";
	private String ptrnrec = "PTRNREC";
	private String payrrec = "PAYRREC";
	private String incrmjarec = "INCRMJAREC";
	private String covrmjarec = "COVRMJAREC";
	private String itdmrec = "ITEMREC   ";
	private String anrlcntrec = "ANRLCNTREC";
	private String fpcorec = "FPCOREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Anniversary Rules, Contract level acces*/
	private AnrlcntTableDAM anrlcntIO = new AnrlcntTableDAM();
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Flexible Premiums Coverage Logical*/
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
		/*Flexible Premium Coverage*/
	private Fpcolf1TableDAM fpcolf1IO = new Fpcolf1TableDAM();
	private Gensswrec gensswrec = new Gensswrec();
		/*Contract Additional Details*/
	private HpadTableDAM hpadIO = new HpadTableDAM();
		/*Automatic Increase Major Alts*/
	private IncrmjaTableDAM incrmjaIO = new IncrmjaTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T3620rec t3620rec = new T3620rec();
	private T5541rec t5541rec = new T5541rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5689rec t5689rec = new T5689rec();
	private T6654rec t6654rec = new T6654rec();
	private Th611rec th611rec = new Th611rec();
	private Batckey wsaaBatchkey = new Batckey();
	private S6668ScreenVars sv = ScreenProgram.getScreenVars( S6668ScreenVars.class);
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		modifyFields1040,
		exit1090,
		lgnmExit,
		plainExit,
		payeeExit,
		preExit,
		validateMop2040,
		validateBillday2060,
		checkForErrors2080,
		exit2090,
		findDateMatch2120,
		exit2190,
		exit2200,
		exit3090,
		exit3190,
		exit3290,
		readAnni3310,
		exit3390,
		softlock4080,
		exit4090,
		exit4390,
		exit6000,
		exit6100,
		exit6200
	}

	public P6668() {
		super();
		screenVars = sv;
		new ScreenModel("S6668", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case modifyFields1040: {
					modifyFields1040();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		sv.dataArea.set(SPACES);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaFirstTime = "Y";
		sv.billcd.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setDataArea(SPACES);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		wsaaChdrnum.set(chdrmjaIO.getChdrnum());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.register.set(chdrmjaIO.getRegister());
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.ptdate.set(payrIO.getPtdate());
		sv.btdate.set(payrIO.getBtdate());
		sv.mop.set(payrIO.getBillchnl());
		sv.billfreq.set(payrIO.getBillfreq());
		wsaaOldinst.set(payrIO.getSinstamt01());
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(payrIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.premstatus.set(descIO.getShortdesc());
		}
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isEQ(lifemjaIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrmjaIO.getCownnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.cownnum.set(chdrmjaIO.getCownnum());
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		if (isEQ(chdrmjaIO.getJownnum(),SPACES)) {
			sv.jownnum.set("NONE");
			sv.jownername.set("NONE");
			goTo(GotoLabel.modifyFields1040);
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrmjaIO.getJownnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jownnum.set(chdrmjaIO.getJownnum());
		plainname();
		sv.jownername.set(wsspcomn.longconfname);
	}

protected void modifyFields1040()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3590);
		descIO.setDescitem(payrIO.getBillfreq());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.freqdesc.set(descIO.getLongdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3620);
		descIO.setDescitem(chdrmjaIO.getBillchnl());
		descIO.setDescitem(payrIO.getBillchnl());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.mopdesc.set(descIO.getLongdesc());
		}
		sv.billcd.set(payrIO.getBillcd());
		sv.billday.set(payrIO.getBillday());
		wsaaInitBillcd9.set(payrIO.getBillcd());
		wsaaNewBillcd9.set(payrIO.getBillcd());
		if (isNE(payrIO.getMandref(),SPACES)) {
			wsaaMandref.set(payrIO.getMandref());
			sv.ddind.set("+");
		}
		if (isNE(chdrmjaIO.getPayrnum(),SPACES)) {
			wsaaPayrnum.set(chdrmjaIO.getPayrnum());
			sv.payind.set("+");
		}
		wsaaIncomeSeqno.set(payrIO.getIncomeSeqNo());
		anrlcntIO.setParams(SPACES);
		anrlcntIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		anrlcntIO.setChdrnum(chdrmjaIO.getChdrnum());
		anrlcntIO.setFunction(varcom.begnh);
		readAnniRules3300();
		if (isEQ(wsaaAiindInd,"Y")) {
			sv.aiind.set("+");
		}
		else {
			sv.aiindOut[varcom.nd.toInt()].set("Y");
			sv.aiind.set(SPACES);
		}
		if (isNE(payrIO.getGrupnum(),SPACES)) {
			wsaaGrupkey.set(payrIO.getGrupnum());
			wsaaMembsel.set(chdrmjaIO.getMembsel());
			sv.grpind.set("+");
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validateBillfreq2020();
					next000a();
					getOldPremium2040();
				}
				case validateMop2040: {
					validateMop2040();
				}
				case validateBillday2060: {
					validateBillday2060();
					validateSelectionFields2070();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateBillfreq2020()
	{
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.billfreq,SPACES)
		|| isEQ(sv.billfreq,"00")) {
			sv.billfreq.set(SPACES);
			sv.billfreqErr.set(e186);
			wsspcomn.edterror.set("Y");
			sv.freqdesc.set(SPACES);
			goTo(GotoLabel.validateMop2040);
		}
	}

protected void next000a()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3590);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(sv.billfreq);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.billfreqErr.set(h048);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.validateMop2040);
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.freqdesc.set(descIO.getLongdesc());
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon3rec.intDate2.set(chdrmjaIO.getPtdate());
		datcon3rec.frequency.set(sv.billfreq);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			sv.billfreqErr.set(h048);
			wsspcomn.edterror.set("Y");
			checkForErrors2080();
		}
		wsaaFactor.set(datcon3rec.freqFactor);
		if (isNE(wsaaReminder,0)) {
			sv.billfreqErr.set(rl13);
			wsspcomn.edterror.set("Y");
			checkForErrors2080();
		}
	}

protected void getOldPremium2040()
	{
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(sv.chdrnum);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		initialize(wsaaNewinst);
		while ( !(isEQ(covrmjaIO.getStatuz(),"ENDP")
		|| isNE(sv.errorIndicators,SPACES))) {
			getOldnewInstprem2200();
		}

		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(th611);
		wsaaTh611Cnttype.set(chdrmjaIO.getCnttype());
		wsaaTh611Currcode.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsaaTh611Item);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaTh611Item,itdmIO.getItemitem())
		|| isNE(chdrmjaIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),th611)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
			itdmIO.setItemtabl(th611);
			wsaaTh611Cnttype.set("***");
			wsaaTh611Currcode.set(payrIO.getCntcurr());
			itdmIO.setItemitem(wsaaTh611Item);
			itdmIO.setItmfrm(chdrmjaIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),"****")
			&& isNE(itdmIO.getStatuz(),"ENDP")) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if (isNE(wsaaTh611Item,itdmIO.getItemitem())
			|| isNE(chdrmjaIO.getChdrcoy(),itdmIO.getItemcoy())
			|| isNE(itdmIO.getItemtabl(),th611)
			|| isEQ(itdmIO.getStatuz(),"ENDP")) {
				itdmIO.setItemitem(wsaaTh611Item);
				sv.billfreqErr.set(rl12);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		th611rec.th611Rec.set(itdmIO.getGenarea());
		for (wsaaSub9.set(1); !(isGT(wsaaSub9,8)); wsaaSub9.add(1)){
			if (isEQ(sv.billfreq,th611rec.frequency[wsaaSub9.toInt()])) {
				wsaaMinPrem.set(th611rec.cmin[wsaaSub9.toInt()]);
				wsaaMaxPrem.set(th611rec.cmax[wsaaSub9.toInt()]);
			}
		}
		if (isLT(wsaaNewinst,wsaaMinPrem)
		|| isGT(wsaaNewinst,wsaaMaxPrem)) {
			sv.billfreqErr.set(rl11);
		}
	}

protected void validateMop2040()
	{
		if (isEQ(sv.mop,SPACES)) {
			sv.mopErr.set(e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.validateBillday2060);
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3620);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(sv.mop);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.mopErr.set(h049);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.validateBillday2060);
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.mopdesc.set(descIO.getLongdesc());
		}
	}

protected void validateBillday2060()
	{
		if (isEQ(sv.billday,SPACES)) {
			sv.billdayErr.set(e186);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if ((isNE(sv.mopErr,SPACES))
		|| (isNE(sv.billfreqErr,SPACES))
		|| (isNE(sv.billdayErr,SPACES))) {
			goTo(GotoLabel.checkForErrors2080);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5689);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5689)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.billdayErr.set(e368);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		else {
			t5689rec.t5689Rec.set(itdmIO.getGenarea());
		}
		wsaaChkFinish.set(SPACES);
		sv.billfreqOut[varcom.ri.toInt()].set("Y");
		sv.mopOut[varcom.ri.toInt()].set("Y");
		sv.billdayOut[varcom.ri.toInt()].set("Y");
		for (wsaaT5689Count.set(1); !((isEQ(wsaaChkFinish,"Y"))
		|| (isGT(wsaaT5689Count,30))); wsaaT5689Count.add(1)){
			validateBillcd2100();
		}
		if (isEQ(sv.billfreqOut[varcom.ri.toInt()],"Y")
		&& isEQ(sv.mopOut[varcom.ri.toInt()],"Y")) {
			sv.billfreqErr.set(h060);
		}
		if (isEQ(sv.billdayOut[varcom.ri.toInt()],"Y")) {
			sv.billdayErr.set(e482);
		}
		if (isNE(wsaaChkFinish,"Y")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		wsaaNewBilldd.set(sv.billday);
		sv.billcd.set(wsaaNewBillcd9);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sv.billcd);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isEQ(datcon1rec.statuz,"IVYR")
		|| isEQ(datcon1rec.statuz,"IVMN")
		|| isEQ(datcon1rec.statuz,"IVDY")) {
			sv.billdayErr.set(e032);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon3rec.intDate2.set(sv.billcd);
		datcon3rec.frequency.set(sv.billfreq);
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaFactor3.set(datcon3rec.freqFactor);
		if (isNE(wsaaReminder3,ZERO)) {
			sv.billdayErr.set(rl13);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t3620);
		itemIO.setItemitem(sv.mop);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.mopOut[varcom.ri.toInt()].set("Y");
			sv.mopErr.set(e398);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
		if (isNE(t3620rec.ddind,SPACES)) {
			if (isEQ(payrIO.getMandref(),SPACES)) {
				sv.ddind.set("X");
			}
			else {
				if (isEQ(sv.ddind,SPACES)) {
					sv.ddind.set("+");
				}
			}
		}
		else {
			if (isEQ(sv.ddind,"X")) {
				sv.ddindErr.set(h062);
				wsspcomn.edterror.set("Y");
			}
		}
		if (isNE(t3620rec.grpind,SPACES)) {
			if (isEQ(chdrmjaIO.getGrupkey(),SPACES)) {
				sv.grpind.set("X");
			}
			else {
				if (isEQ(sv.grpind,SPACES)) {
					sv.grpind.set("+");
				}
			}
		}
		else {
			if (isEQ(sv.grpind,"X")) {
				sv.grpindErr.set(h063);
				wsspcomn.edterror.set("Y");
			}
		}
	}

protected void validateSelectionFields2070()
	{
		if ((isNE(sv.payind,"X"))
		&& (isNE(sv.payind,"+"))
		&& (isNE(sv.payind,SPACES))) {
			sv.payindErr.set(g620);
		}
		if ((isNE(sv.grpind,"X"))
		&& (isNE(sv.grpind,"+"))
		&& (isNE(sv.grpind,SPACES))) {
			sv.grpindErr.set(g620);
		}
		if ((isNE(sv.ddind,"X"))
		&& (isNE(sv.ddind,"+"))
		&& (isNE(sv.ddind,SPACES))) {
			sv.ddindErr.set(g620);
		}
		if ((isNE(sv.aiind,"X"))
		&& (isNE(sv.aiind,"+"))
		&& (isNE(sv.aiind,SPACES))) {
			sv.aiindErr.set(g620);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateBillcd2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					matchMopBillfreq2110();
				}
				case findDateMatch2120: {
					findDateMatch2120();
				}
				case exit2190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void matchMopBillfreq2110()
	{
		if (isEQ(t5689rec.billfreq[wsaaT5689Count.toInt()],sv.billfreq)
		&& isEQ(t5689rec.mop[wsaaT5689Count.toInt()],sv.mop)) {
			sv.billfreqOut[varcom.ri.toInt()].set(SPACES);
			sv.mopOut[varcom.ri.toInt()].set(SPACES);
			goTo(GotoLabel.findDateMatch2120);
		}
		else {
			goTo(GotoLabel.exit2190);
		}
	}

protected void findDateMatch2120()
	{
		if (isEQ(t5689rec.rendd[wsaaT5689Count.toInt()],"99")) {
			sv.billdayOut[varcom.ri.toInt()].set(SPACES);
			wsaaChkFinish.set("Y");
			goTo(GotoLabel.exit2190);
		}
		/*DAY-CHECK*/
		if (isEQ(t5689rec.rendd[wsaaT5689Count.toInt()],sv.billday)) {
			sv.billdayOut[varcom.ri.toInt()].set(SPACES);
			wsaaChkFinish.set("Y");
			goTo(GotoLabel.exit2190);
		}
	}

protected void getOldnewInstprem2200()
	{
		try {
			begin2200();
		}
		catch (GOTOException e){
		}
	}

protected void begin2200()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(covrmjaIO.getChdrnum(),sv.chdrnum)
		|| isNE(covrmjaIO.getChdrcoy(),wsspcomn.company)) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2200);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatchkey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.chdrnumErr.set(f321);
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		wsaaValidComp = "N";
		if (isLTE(covrmjaIO.getInstprem(),0)) {
			covrmjaIO.setFunction(varcom.nextr);
		}
		wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(),"00")) {
			while ( !((isGT(wsaaStatCount,12))
			|| (isEQ(t5679rec.covPremStat[wsaaStatCount.toInt()],covrmjaIO.getPstatcode())))) {
				wsaaStatCount.add(1);
			}

			if (isLTE(wsaaStatCount,12)) {
				wsaaStatCount.set(1);
				while ( !((isGT(wsaaStatCount,12))
				|| (isEQ(t5679rec.covRiskStat[wsaaStatCount.toInt()],covrmjaIO.getStatcode())))) {
					wsaaStatCount.add(1);
				}

			}
		}
		else {
			while ( !((isGT(wsaaStatCount,12))
			|| (isEQ(t5679rec.ridPremStat[wsaaStatCount.toInt()],covrmjaIO.getStatcode())))) {
				wsaaStatCount.add(1);
			}

			if (isLTE(wsaaStatCount,12)) {
				wsaaStatCount.set(1);
				while ( !((isGT(wsaaStatCount,12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaStatCount.toInt()],covrmjaIO.getStatcode())))) {
					wsaaStatCount.add(1);
				}

			}
		}
		if (isLTE(wsaaStatCount,12)) {
			wsaaValidComp = "Y";
		}
		if (isEQ(wsaaValidComp,"Y")) {
			itdmIO.setDataArea(SPACES);
			itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
			itdmIO.setItemtabl(t5687);
			itdmIO.setItemitem(covrmjaIO.getCrtable());
			itdmIO.setItmfrm(chdrmjaIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
				sv.chdrnumErr.set(e300);
			}
			else {
				t5687rec.t5687Rec.set(itdmIO.getGenarea());
			}
			itdmIO.setDataArea(SPACES);
			itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
			itdmIO.setItemtabl(t5541);
			itdmIO.setItemitem(t5687rec.xfreqAltBasis);
			itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
			if (isNE(itdmIO.getItemcoy(),covrmjaIO.getChdrcoy())
			|| isNE(itdmIO.getItemtabl(),t5541)
			|| isNE(itdmIO.getItemitem(),t5687rec.xfreqAltBasis)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				sv.freqdescErr.set(t075);
				goTo(GotoLabel.exit2200);
			}
			else {
				t5541rec.t5541Rec.set(itdmIO.getGenarea());
			}
		}
		wsaaNewFreqFound.set(SPACES);
		wsaaCalcNewfreq.set(sv.billfreq);
		wsaaCalcOldfreq.set(chdrmjaIO.getBillfreq());
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaNewFreqFound,"Y")); wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaSub.toInt()],wsaaCalcNewfreq)) {
				wsaaNewLfact.set(t5541rec.lfact[wsaaSub.toInt()]);
				wsaaNewFreqFound.set("Y");
			}
		}
		wsaaOldFreqFound.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaOldFreqFound,"Y")); wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaSub.toInt()],wsaaCalcOldfreq)) {
				wsaaOldLfact.set(t5541rec.lfact[wsaaSub.toInt()]);
				wsaaOldFreqFound.set("Y");
			}
		}
		if (isGT(wsaaSub,12)) {
			sv.chdrnumErr.set(t076);
			goTo(GotoLabel.exit2200);
		}
		if (isEQ(wsaaOldFreqFound,"Y")
		&& isEQ(wsaaNewFreqFound,"Y")) {
			compute(wsaaNewinst, 4).set(mult(mult(wsaaOldinst,wsaaCalcOldfreq),wsaaNewLfact));
			compute(wsaaTmpval, 4).set(mult(wsaaCalcNewfreq,wsaaOldLfact));
			compute(wsaaNewinst, 3).setRounded(div(wsaaNewinst,wsaaTmpval));
		}
		else {
			sv.chdrnumErr.set(t076);
			goTo(GotoLabel.exit2200);
		}
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
		incrmjaIO.setDataArea(SPACES);
		incrmjaIO.setChdrcoy(wsspcomn.company);
		incrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incrmjaIO.setLife(ZERO);
		incrmjaIO.setRider(ZERO);
		incrmjaIO.setPlanSuffix(ZERO);
		incrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		incrmjaIO.setFormat(incrmjarec);
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(),varcom.oK)
		&& isNE(incrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(incrmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(incrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(incrmjaIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaFirstTime,"Y")) {
			sv.chdrnumErr.set(j008);
			wsspcomn.edterror.set("Y");
			wsaaFirstTime = "N";
			goTo(GotoLabel.exit3090);
		}
		incrmjaIO.setDataArea(SPACES);
		incrmjaIO.setChdrcoy(wsspcomn.company);
		incrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		incrmjaIO.setLife(ZERO);
		incrmjaIO.setRider(ZERO);
		incrmjaIO.setPlanSuffix(ZERO);
		incrmjaIO.setFunction(varcom.begnh);
		incrmjaIO.setFormat(incrmjarec);
		wsaaEof.set("N");
		while ( !(endOfFile.isTrue())) {
			updateIncr3100();
		}

		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(ZERO);
		covrmjaIO.setJlife(ZERO);
		covrmjaIO.setCoverage(ZERO);
		covrmjaIO.setRider(ZERO);
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.begnh);
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			updateCovr3200();
		}

	}

protected void updateIncr3100()
	{
		try {
			nextIncr3120();
		}
		catch (GOTOException e){
		}
	}

protected void nextIncr3120()
	{
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(),varcom.oK)
		&& isNE(incrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(incrmjaIO.getStatuz(),varcom.endp)) {
			wsaaEof.set("Y");
			goTo(GotoLabel.exit3190);
		}
		if (isNE(incrmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(incrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())) {
			wsaaEof.set("Y");
			incrmjaIO.setFunction(varcom.rewrt);
		}
		else {
			incrmjaIO.setFunction(varcom.delet);
		}
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incrmjaIO.getParams());
			fatalError600();
		}
		incrmjaIO.setFunction(varcom.nextr);
	}

protected void updateCovr3200()
	{
		try {
			nextCovr3220();
		}
		catch (GOTOException e){
		}
	}

protected void nextCovr3220()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3290);
		}
		covrmjaIO.setIndexationInd(SPACES);
		covrmjaIO.setFunction(varcom.rewrt);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void readAnniRules3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case readAnni3310: {
					readAnni3310();
				}
				case exit3390: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readAnni3310()
	{
		anrlcntIO.setFormat(anrlcntrec);
		SmartFileCode.execute(appVars, anrlcntIO);
		if (isNE(anrlcntIO.getStatuz(),varcom.oK)
		&& isNE(anrlcntIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(anrlcntIO.getParams());
			fatalError600();
		}
		wsaaAiindInd.set(SPACES);
		if (isEQ(anrlcntIO.getStatuz(),varcom.endp)
		|| isNE(anrlcntIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(anrlcntIO.getChdrnum(),chdrmjaIO.getChdrnum())) {
			goTo(GotoLabel.exit3390);
		}
		if (isEQ(anrlcntIO.getValidflag(),"1")) {
			wsaaAiindInd.set("Y");
			goTo(GotoLabel.exit3390);
		}
		anrlcntIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readAnni3310);
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case softlock4080: {
					softlock4080();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			payrIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)
			&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
		}
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatchkey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		if (isEQ(sv.payind,"?")) {
			if (isEQ(chdrmjaIO.getPayrnum(),SPACES)) {
				sv.payind.set(SPACES);
			}
			else {
				sv.payind.set("+");
			}
		}
		if (isEQ(sv.aiind,"?")) {
			anrlcntIO.setParams(SPACES);
			anrlcntIO.setChdrcoy(chdrmjaIO.getChdrcoy());
			anrlcntIO.setChdrnum(chdrmjaIO.getChdrnum());
			anrlcntIO.setFunction(varcom.begnh);
			readAnniRules3300();
			if (isEQ(wsaaAiindInd,"Y")) {
				sv.aiind.set("+");
			}
			else {
				sv.aiind.set(SPACES);
			}
		}
		if (isEQ(sv.grpind,"?")) {
			if (isEQ(payrIO.getGrupnum(),SPACES)) {
				sv.grpind.set(SPACES);
			}
			else {
				sv.grpind.set("+");
			}
		}
		if (isEQ(sv.ddind,"?")) {
			if (isEQ(payrIO.getMandref(),SPACES)) {
				sv.ddind.set(SPACES);
			}
			else {
				sv.ddind.set("+");
			}
		}
		if (isEQ(sv.payind,"X")) {
			if (isNE(sv.ddind,"X")) {
				readT36205300();
				if (isNE(t3620rec.ddind,SPACES)) {
					sv.ddind.set("X");
				}
			}
			gensswrec.function.set("A");
			sv.payind.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.grpind,"X")) {
			gensswrec.function.set("B");
			sv.grpind.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.ddind,"X")) {
			gensswrec.function.set("C");
			sv.ddind.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(sv.aiind,"X")) {
			wsspcomn.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrmjaIO.getChdrnum());
			wsspcomn.chdrCnttype.set(chdrmjaIO.getCnttype());
			wsspcomn.chdrValidflag.set(chdrmjaIO.getValidflag());
			wsspcomn.currfrom.set(chdrmjaIO.getCurrfrom());
			gensswrec.function.set("D");
			sv.aiind.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		wsaaAtModule.set("N");
		if (isNE(sv.billfreq,payrIO.getBillfreq())) {
			wsaaAtModule.set("Y");
			callAtreq5000();
			goTo(GotoLabel.softlock4080);
		}
		wsaaRewrtCond.set("N");
		if (isNE(wsaaInitBilldd,sv.billday)) {
			wsaaRewrtCond.set("Y");
		}
		if (isNE(payrIO.getBillchnl(),sv.mop)) {
			wsaaRewrtCond.set("Y");
		}
		if (isNE(chdrmjaIO.getPayrnum(),wsaaPayrnum)) {
			wsaaRewrtCond.set("Y");
		}
		if (isNE(payrIO.getIncomeSeqNo(),wsaaIncomeSeqno)) {
			wsaaRewrtCond.set("Y");
		}
		if (isNE(payrIO.getMandref(),wsaaMandref)) {
			wsaaRewrtCond.set("Y");
		}
		if (isNE(payrIO.getGrupnum(),wsaaGrupkey)) {
			wsaaRewrtCond.set("Y");
		}
		if (isNE(chdrmjaIO.getMembsel(),wsaaMembsel)) {
			wsaaRewrtCond.set("Y");
		}
		if (wsaaRewrtChanges.isTrue()) {
			processContractPtrn5100();
		}
		if (wsaaRewrtNoChanges.isTrue()) {
			rewriteOldContract5200();
		}
	}

protected void softlock4080()
	{
		if (wsaaAtRequest.isTrue()) {
			sftlockrec.function.set("TOAT");
		}
		if (wsaaAtNoNeed.isTrue()) {
			sftlockrec.function.set("UNLK");
		}
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatchkey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		try {
			callSubroutine4310();
		}
		catch (GOTOException e){
		}
	}

protected void callSubroutine4310()
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.params.set(gensswrec.gensswRec);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4390);
		}
		compute(wsaaX, 0).set(add(1,wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callAtreq5000()
	{
		setUpAtreq5010();
	}

protected void setUpAtreq5010()
	{
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P6668AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaChdrcoy.set(chdrmjaIO.getChdrcoy());
		wsaaChdrnum.set(chdrmjaIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaAtFsuco.set(wsspcomn.fsuco);
		wsaaAtTransactionTime.set(varcom.vrcmTime);
		wsaaAtUser.set(varcom.vrcmUser);
		wsaaAtTermid.set(varcom.vrcmTermid);
		wsaaAtBillfreq.set(sv.billfreq);
		wsaaAtBillchnl.set(sv.mop);
		wsaaAtBillday.set(sv.billday);
		wsaaAtTransactionDate.set(wsaaToday);
		readT36205300();
		if (isEQ(t3620rec.ddind,SPACES)) {
			wsaaAtMandref.set(SPACES);
		}
		else {
			wsaaAtMandref.set(payrIO.getMandref());
		}
		wsaaAtGrupkey.set(payrIO.getGrupnum());
		wsaaAtMembsel.set(chdrmjaIO.getMembsel());
		wsaaAtToday.set(wsaaToday);
		atreqrec.transArea.set(wsaaTransactionRec);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void processContractPtrn5100()
	{
		processRecords5110();
	}

protected void processRecords5110()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		wsaaNewGrupkey.set(payrIO.getGrupnum());
		wsaaNewMembsel.set(chdrmjaIO.getMembsel());
		wsaaNewMandref.set(payrIO.getMandref());
		wsaaNewIncomeSeqno.set(payrIO.getIncomeSeqNo());
		chdrmjaIO.setGrupkey(wsaaGrupkey);
		chdrmjaIO.setMembsel(wsaaMembsel);
		chdrmjaIO.setPayrnum(wsaaPayrnum);
		chdrmjaIO.setMandref(wsaaMandref);
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(chdrmjaIO.getPtdate());
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatchkey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		if (isNE(t5679rec.setCnRiskStat,SPACES)) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}
		if (isNE(t5679rec.setSngpCnStat,SPACES)) {
			chdrmjaIO.setPstatcode(t5679rec.setSngpCnStat);
		}
		wsaaTranno.set(chdrmjaIO.getTranno());
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(wsaaTranno,1));
		wsaaIdTransactionDate.set(wsaaToday);
		chdrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		if (isNE(sv.billday,wsaaInitBilldd)
		&& isNE(sv.billday,SPACES)) {
			wsaaNewBilldd.set(sv.billday);
			chdrmjaIO.setBillcd(wsaaNewBillcd9);
			flexiblePremProcessing6000();
		}
		if ((isNE(sv.mop,chdrmjaIO.getBillchnl()))
		&& (isNE(sv.mop,SPACES))) {
			chdrmjaIO.setBillchnl(sv.mop);
		}
		wsaaIdTransactionTime.set(varcom.vrcmTime);
		wsaaIdTermid.set(varcom.vrcmTerm);
		wsaaIdUser.set(varcom.vrcmUser);
		chdrmjaIO.setTranid(wsaaTranid);
		readT36205300();
		if (isEQ(t3620rec.ddind,SPACES)) {
			wsaaNewMandref.set(SPACES);
		}
		if (isEQ(t3620rec.grpind,SPACES)) {
			wsaaNewGrupkey.set(SPACES);
			wsaaNewMembsel.set(SPACES);
			payrIO.setGrupcoy(SPACES);
			payrIO.setGrupnum(SPACES);
			payrIO.setMembsel(SPACES);
			payrIO.setBillnet(SPACES);
		}
		chdrmjaIO.setGrupkey(wsaaNewGrupkey);
		chdrmjaIO.setMembsel(wsaaNewMembsel);
		chdrmjaIO.setPayrnum(wsaaNewPayrnum);
		chdrmjaIO.setMandref(wsaaNewMandref);
		chdrmjaIO.setBillchnl(sv.mop);
		chdrmjaIO.setBillcd(sv.billcd);
		chdrmjaIO.setBillfreq(sv.billfreq);
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setFunction(varcom.writr);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		payrIO.setGrupnum(wsaaGrupkey);
		payrIO.setMembsel(wsaaMembsel);
		payrIO.setMandref(wsaaMandref);
		payrIO.setIncomeSeqNo(wsaaIncomeSeqno);
		payrIO.setValidflag("2");
		chdrmjaIO.setCurrto(chdrmjaIO.getPtdate());
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatchkey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		if (isNE(t5679rec.setSngpCnStat,SPACES)) {
			payrIO.setPstatcode(t5679rec.setSngpCnStat);
		}
		if (isNE(sv.billday,wsaaInitBilldd)
		&& isNE(sv.billday,SPACES)) {
			wsaaNewBilldd.set(sv.billday);
			payrIO.setBillcd(wsaaNewBillcd9);
			updateNextdate5400();
		}
		if ((isNE(sv.mop,payrIO.getBillchnl()))
		&& (isNE(sv.mop,SPACES))) {
			payrIO.setBillchnl(sv.mop);
		}
		payrIO.setTransactionDate(wsaaToday);
		payrIO.setTransactionTime(varcom.vrcmTime);
		payrIO.setTermid(varcom.vrcmTerm);
		payrIO.setUser(varcom.vrcmUser);
		readT36205300();
		if (isEQ(t3620rec.ddind,SPACES)) {
			wsaaNewMandref.set(SPACES);
		}
		if (isEQ(t3620rec.grpind,SPACES)) {
			wsaaNewGrupkey.set(SPACES);
			wsaaNewMembsel.set(SPACES);
			payrIO.setGrupcoy(SPACES);
			payrIO.setGrupnum(SPACES);
			payrIO.setMembsel(SPACES);
			payrIO.setBillnet(SPACES);
		}
		payrIO.setEffdate(payrIO.getBtdate());
		payrIO.setGrupnum(wsaaNewGrupkey);
		payrIO.setMembsel(wsaaNewMembsel);
		payrIO.setMandref(wsaaNewMandref);
		payrIO.setIncomeSeqNo(wsaaNewIncomeSeqno);
		payrIO.setBillchnl(sv.mop);
		payrIO.setBillcd(sv.billcd);
		payrIO.setBillday(sv.billday);
		payrIO.setBillmonth(wsaaNewBillmm);
		payrIO.setBillfreq(sv.billfreq);
		payrIO.setValidflag("1");
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setRecode(chdrmjaIO.getRecode());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setTransactionDate(wsaaToday);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setValidflag("1");
		ptrnIO.setDataKey(wsaaBatchkey);
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void rewriteOldContract5200()
	{
		rewriteContract5210();
	}

protected void rewriteContract5210()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setGrupkey(wsaaGrupkey);
		chdrmjaIO.setMembsel(wsaaMembsel);
		chdrmjaIO.setPayrnum(wsaaPayrnum);
		chdrmjaIO.setMandref(wsaaMandref);
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void readT36205300()
	{
		checkForBankDets5310();
	}

protected void checkForBankDets5310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t3620);
		itemIO.setItemitem(sv.mop);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
	}

protected void updateNextdate5400()
	{
		checkForBankDets5410();
	}

protected void checkForBankDets5410()
	{
		String key;
		if(BTPRO028Permission) {
			key = payrIO.getBillchnl().toString().trim() + chdrmjaIO.getCnttype().toString().trim() +  payrIO.getBillfreq().toString().toString().trim();
			if(!readT6654(key)) {
				key = payrIO.getBillchnl().toString().trim().concat(chdrmjaIO.getCnttype().toString().trim()).concat("**").toString();
				if(!readT6654(key)) {
					key = payrIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						syserrrec.params.set(itemIO.getParams());
						syserrrec.statuz.set(itemIO.getStatuz());
						fatalError600();
					}
				}
			}
			
		}
		else {
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6654);
		wsaaT6654Key.set(SPACES);
		wsaaT6654Mop.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrmjaIO.getCnttype());
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		else {
			wsaaT6654Cnttype.set("***");
			itemIO.setItemitem(wsaaT6654Key);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			else {
				t6654rec.t6654Rec.set(itemIO.getGenarea());
			}
		}
		}
		datcon2rec.intDate1.set(payrIO.getBillcd());
		datcon2rec.frequency.set("DY");
		compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays,-1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		payrIO.setNextdate(datcon2rec.intDate2);
	}

protected boolean readT6654(String key) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrmjaIO.getChdrcoy().toString());
		itempf.setItemtabl(t6654);
		itempf.setItemitem(key);
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null != itempf) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			return true;
		}
		return false;
	}

protected void flexiblePremProcessing6000()
	{
		try {
			para6000();
		}
		catch (GOTOException e){
		}
	}

protected void para6000()
	{
		notFlexiblePremiumContract.setTrue();
		readT57296100();
		if (notFlexiblePremiumContract.isTrue()) {
			goTo(GotoLabel.exit6000);
		}
		fpcoIO.setParams(SPACES);
		fpcoIO.setStatuz(varcom.oK);
		fpcoIO.setChdrcoy(wsspcomn.company);
		fpcoIO.setChdrnum(chdrmjaIO.getChdrnum());
		fpcoIO.setPlanSuffix(ZERO);
		fpcoIO.setTargfrom(ZERO);
		fpcoIO.setFormat(fpcorec);
		fpcoIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcoIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcoIO.setFitKeysSearch("CHDRCOY");
		while ( !(isEQ(fpcoIO.getStatuz(),varcom.endp))) {
			processFpco6200();
		}

	}

protected void readT57296100()
	{
		try {
			para6100();
		}
		catch (GOTOException e){
		}
	}

protected void para6100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(wsspcomn.company, SPACES));
			stringVariable1.append(delimitedExp(t5729, SPACES));
			stringVariable1.append(delimitedExp(chdrmjaIO.getCnttype(), SPACES));
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5729)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isGT(itdmIO.getItmfrm(),chdrmjaIO.getOccdate())
		|| isLT(itdmIO.getItmto(),chdrmjaIO.getOccdate())) {
			goTo(GotoLabel.exit6100);
		}
		flexiblePremiumContract.setTrue();
	}

protected void processFpco6200()
	{
		try {
			para6200();
		}
		catch (GOTOException e){
		}
	}

protected void para6200()
	{
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(),varcom.oK)
		&& isNE(fpcoIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError600();
		}
		if (isEQ(fpcoIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit6200);
		}
		if (isNE(fpcoIO.getChdrcoy(),wsspcomn.company)
		|| isNE(fpcoIO.getChdrnum(),wsaaChdrnum)) {
			fpcoIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit6200);
		}
		wsaaAnnivExtractDate.set(fpcoIO.getAnnivProcDate());
		if (isNE(wsaaNewBilldd,wsaaAnnivExtractDay)) {
			updateAnnivDate6210();
		}
		fpcoIO.setFunction(varcom.nextr);
	}

protected void updateAnnivDate6210()
	{
		para6210();
	}

protected void para6210()
	{
		fpcolf1IO.setChdrcoy(fpcoIO.getChdrcoy());
		fpcolf1IO.setChdrnum(fpcoIO.getChdrnum());
		fpcolf1IO.setLife(fpcoIO.getLife());
		fpcolf1IO.setCoverage(fpcoIO.getCoverage());
		fpcolf1IO.setRider(fpcoIO.getRider());
		fpcolf1IO.setPlanSuffix(fpcoIO.getPlanSuffix());
		fpcolf1IO.setTargfrom(fpcoIO.getTargfrom());
		fpcolf1IO.setActiveInd(fpcoIO.getActiveInd());
		fpcolf1IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcolf1IO);
		if (isNE(fpcolf1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcolf1IO.getParams());
			fatalError600();
		}
		fpcolf1IO.setCurrto(wsaaToday);
		fpcolf1IO.setValidflag("2");
		fpcolf1IO.setTranno(chdrmjaIO.getTranno());
		fpcolf1IO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcolf1IO);
		if (isNE(fpcolf1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcolf1IO.getParams());
			fatalError600();
		}
		wsaaAnnivExtractDay.set(wsaaNewBilldd);
		fpcolf1IO.setAnnivProcDate(wsaaAnnivExtractDate);
		fpcolf1IO.setValidflag("1");
		fpcolf1IO.setTranno(chdrmjaIO.getTranno());
		fpcolf1IO.setEffdate(wsaaToday);
		fpcolf1IO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcolf1IO);
		if (isNE(fpcolf1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcolf1IO.getParams());
			fatalError600();
		}
	}
}
