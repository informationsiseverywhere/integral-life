package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;

import com.csc.life.contractservicing.dataaccess.model.Coprpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CoprpfDAO extends BaseDAO<Coprpf> {
 public List<Coprpf> getCoprpfRecord(Coprpf coprpf);
 public void insertCoprpfRecord(List<Coprpf> coprList);
 public void updateCoprpf(List<Coprpf> coprList);
 public List<Coprpf> getCoprpfByTranno(Coprpf coprpf);
 public void deleteCoprpf(List<Coprpf> coprList);
}
