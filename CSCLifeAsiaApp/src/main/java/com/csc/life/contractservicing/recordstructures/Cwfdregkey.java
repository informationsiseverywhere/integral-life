package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:37
 * Description:
 * Copybook name: CWFDREGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cwfdregkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData cwfdregFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData cwfdregKey = new FixedLengthStringData(64).isAPartOf(cwfdregFileKey, 0, REDEFINE);
  	public FixedLengthStringData cwfdregChdrcoy = new FixedLengthStringData(1).isAPartOf(cwfdregKey, 0);
  	public FixedLengthStringData cwfdregChdrnum = new FixedLengthStringData(8).isAPartOf(cwfdregKey, 1);
  	public PackedDecimalData cwfdregTranno = new PackedDecimalData(5, 0).isAPartOf(cwfdregKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(cwfdregKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(cwfdregFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		cwfdregFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}