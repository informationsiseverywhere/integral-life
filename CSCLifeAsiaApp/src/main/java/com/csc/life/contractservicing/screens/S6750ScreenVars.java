package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6750
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6750ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(3);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(dataFields, 0, FILLER);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 1);
	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(errorIndicators, 0, FILLER);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 2);
	public FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(outputIndicators, 0, FILLER);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(85);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(35).isAPartOf(subfileArea, 0);
	public FixedLengthStringData descr = DD.descr.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,30);
	public FixedLengthStringData transCode = DD.trcde.copy().isAPartOf(subfileFields,31);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(12).isAPartOf(subfileArea, 35);
	public FixedLengthStringData descrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData trcdeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 47);
	public FixedLengthStringData[] descrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] trcdeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 83);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6750screensflWritten = new LongData(0);
	public LongData S6750screenctlWritten = new LongData(0);
	public LongData S6750screenWritten = new LongData(0);
	public LongData S6750protectWritten = new LongData(0);
	public GeneralTable s6750screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6750screensfl;
	}

	public S6750ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, descr, transCode};
		screenSflOutFields = new BaseData[][] {selectOut, descrOut, trcdeOut};
		screenSflErrFields = new BaseData[] {selectErr, descrErr, trcdeErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {};
		screenOutFields = new BaseData[][] {};
		screenErrFields = new BaseData[] {};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6750screen.class;
		screenSflRecord = S6750screensfl.class;
		screenCtlRecord = S6750screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6750protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6750screenctl.lrec.pageSubfile);
	}
}
