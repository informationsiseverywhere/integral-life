/*
 * File: Gdelulnk.java
 * Date: 29 August 2009 22:49:39
 * Author: Quipoz Limited
 * 
 * Class transformed from GDELULNK.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.UlnkrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*
* Generalised Reversal subroutine for DELETION of ULNK records
* ------------------------------------------------------------
*
* This subroutine is called by reversal subroutines via
* table T5671. The parameter passed to this subroutine is
* contained in the GREVERSREC copybook.
*
* SPECIFICALLY, this subroutine is called by REVCOMPA, the
*  Component Add Reversal subroutine to DELETe any ULNK records
*  created by the forward Component Add transaction.
*
* Processing is as follows:
*
*    Do a BEGN on the ULNK file using the ULNKREV logical view
*     and specifying the Company, Contract Number, Life,
*     Coverage, Rider and Plan Suffix. Set tranno to zeros.
*
*      For each ULNK record read which matches the above key,
*       Read-lock and DELETe.
*
*    Return to the calling subroutine when no more ULNKs are
*     found which match on the above specified component key.
*
*
*****************************************************************
* </pre>
*/
public class Gdelulnk extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String ulnkrevrec = "ULNKREVREC";
	private Greversrec greversrec = new Greversrec();
	private Syserrrec syserrrec = new Syserrrec();
		/*CFI/AFI/Reversals view*/
	private UlnkrevTableDAM ulnkrevIO = new UlnkrevTableDAM();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		exit2190, 
		exit9490
	}

	public Gdelulnk() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		greversrec.statuz.set(varcom.oK);
		processUlnks2000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processUlnks2000()
	{
		try {
			start2000();
		}
		catch (GOTOException e){
		}
	}

protected void start2000()
	{
		ulnkrevIO.setParams(SPACES);
		ulnkrevIO.setChdrcoy(greversrec.chdrcoy);
		ulnkrevIO.setChdrnum(greversrec.chdrnum);
		ulnkrevIO.setCoverage(greversrec.coverage);
		ulnkrevIO.setLife(greversrec.life);
		ulnkrevIO.setRider(greversrec.rider);
		ulnkrevIO.setPlanSuffix(greversrec.planSuffix);
		ulnkrevIO.setTranno(ZERO);
		ulnkrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ulnkrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ulnkrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		ulnkrevIO.setFormat(ulnkrevrec);
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)
		&& isNE(ulnkrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			systemError9000();
		}
		if (isEQ(ulnkrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(greversrec.chdrcoy,ulnkrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,ulnkrevIO.getChdrnum())
		|| isNE(greversrec.life,ulnkrevIO.getLife())
		|| isNE(greversrec.coverage,ulnkrevIO.getCoverage())
		|| isNE(greversrec.rider,ulnkrevIO.getRider())
		|| isNE(greversrec.planSuffix,ulnkrevIO.getPlanSuffix())) {
			ulnkrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		while ( !(isEQ(ulnkrevIO.getStatuz(),varcom.endp))) {
			deleteUlnks2100();
		}
		
	}

protected void deleteUlnks2100()
	{
		try {
			start2100();
		}
		catch (GOTOException e){
		}
	}

protected void start2100()
	{
		ulnkrevIO.setFunction(varcom.deltd);
		ulnkrevIO.setFormat(ulnkrevrec);
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			systemError9000();
		}
		ulnkrevIO.setFunction(varcom.nextr);
		ulnkrevIO.setFormat(ulnkrevrec);
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)
		&& isNE(ulnkrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			systemError9000();
		}
		if (isEQ(ulnkrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		if (isNE(greversrec.chdrcoy,ulnkrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,ulnkrevIO.getChdrnum())
		|| isNE(greversrec.life,ulnkrevIO.getLife())
		|| isNE(greversrec.coverage,ulnkrevIO.getCoverage())
		|| isNE(greversrec.rider,ulnkrevIO.getRider())
		|| isNE(greversrec.planSuffix,ulnkrevIO.getPlanSuffix())) {
			ulnkrevIO.setStatuz(varcom.endp);
		}
	}

protected void systemError9000()
	{
		try {
			start9000();
		}
		catch (GOTOException e){
		}
		finally{
			exit9490();
		}
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}
}
