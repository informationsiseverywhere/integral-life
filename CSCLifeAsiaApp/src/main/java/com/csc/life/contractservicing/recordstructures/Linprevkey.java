package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:11
 * Description:
 * Copybook name: LINPREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linprevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linprevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData linprevKey = new FixedLengthStringData(64).isAPartOf(linprevFileKey, 0, REDEFINE);
  	public FixedLengthStringData linprevChdrcoy = new FixedLengthStringData(1).isAPartOf(linprevKey, 0);
  	public FixedLengthStringData linprevChdrnum = new FixedLengthStringData(8).isAPartOf(linprevKey, 1);
  	public PackedDecimalData linprevInstfrom = new PackedDecimalData(8, 0).isAPartOf(linprevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(linprevKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linprevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linprevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}