package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:51
 * @author Quipoz
 */
public class St523screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St523ScreenVars sv = (St523ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.St523screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		St523ScreenVars screenVars = (St523ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.sdei.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.currds.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.orgamnt.setClassString("");
		screenVars.fromdateDisp.setClassString("");
		screenVars.tdbtdesc.setClassString("");
		screenVars.tdbtamt.setClassString("");
		screenVars.validflag.setClassString("");
		screenVars.tdbtrate.setClassString("");
		screenVars.todateDisp.setClassString("");
	}

/**
 * Clear all the variables in St523screen
 */
	public static void clear(VarModel pv) {
		St523ScreenVars screenVars = (St523ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.sdei.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.cownnum.clear();
		screenVars.lifcnum.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.linsname.clear();
		screenVars.ownername.clear();
		screenVars.rstate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.pstate.clear();
		screenVars.currcd.clear();
		screenVars.currds.clear();
		screenVars.tranno.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.orgamnt.clear();
		screenVars.fromdateDisp.clear();
		screenVars.fromdate.clear();
		screenVars.tdbtdesc.clear();
		screenVars.tdbtamt.clear();
		screenVars.validflag.clear();
		screenVars.tdbtrate.clear();
		screenVars.todateDisp.clear();
		screenVars.todate.clear();
	}
}
