package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5098screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5098ScreenVars sv = (S5098ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5098screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5098ScreenVars screenVars = (S5098ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.cntdesc.setClassString("");
		screenVars.crtabled.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.bonusDecDateDisp.setClassString("");
		screenVars.adjBonusDecDateDisp.setClassString("");
		screenVars.bonusValue.setClassString("");
		screenVars.arbAdjustAmt.setClassString("");
		screenVars.adjustedArb.setClassString("");
		screenVars.instprem.setClassString("");
	}

/**
 * Clear all the variables in S5098screen
 */
	public static void clear(VarModel pv) {
		S5098ScreenVars screenVars = (S5098ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.cnttype.clear();
		screenVars.crtable.clear();
		screenVars.lifenum.clear();
		screenVars.cntdesc.clear();
		screenVars.crtabled.clear();
		screenVars.lifename.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.numpols.clear();
		screenVars.planSuffix.clear();
		screenVars.cntcurr.clear();
		screenVars.bonusDecDateDisp.clear();
		screenVars.bonusDecDate.clear();
		screenVars.adjBonusDecDateDisp.clear();
		screenVars.adjBonusDecDate.clear();
		screenVars.bonusValue.clear();
		screenVars.arbAdjustAmt.clear();
		screenVars.adjustedArb.clear();
		screenVars.instprem.clear();
	}
}
