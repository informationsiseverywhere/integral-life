package com.csc.life.contractservicing.dataaccess;


import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * The Join-view DAM class
 * File: AglfmjaTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:39
 * Class transformed from AGLFMJA
 * Author: Quipoz Limited
 *
 * Copyright (2009) CSC Asia, all rights reserved
 *
 */
 
public class AglfmjaTableDAM extends PFAdapterDAM {
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(189);
	public FixedLengthStringData dataKey = new FixedLengthStringData(64).isAPartOf(dataArea, 0);
	public FixedLengthStringData keyData = new FixedLengthStringData(64).isAPartOf(dataKey, 0, REDEFINE);
	public FixedLengthStringData agntcoy = new FixedLengthStringData(1).isAPartOf(keyData, 0);
	public FixedLengthStringData agntnum = new FixedLengthStringData(8).isAPartOf(keyData, 1);
	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(keyData, 9, FILLER);
	public FixedLengthStringData nonKey = new FixedLengthStringData(125).isAPartOf(dataArea, 64);
	public FixedLengthStringData nonKeyData = new FixedLengthStringData(125).isAPartOf(nonKey, 0, REDEFINE);
	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(nonKeyData, 0, FILLER);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(nonKeyData, 1, FILLER);
	public FixedLengthStringData clntnum = new FixedLengthStringData(8).isAPartOf(nonKeyData, 9);
	public FixedLengthStringData agntbr = new FixedLengthStringData(2).isAPartOf(nonKeyData, 17);
	public PackedDecimalData dteapp = new PackedDecimalData(8, 0).isAPartOf(nonKeyData, 19);
	public PackedDecimalData dtetrm = new PackedDecimalData(8, 0).isAPartOf(nonKeyData, 24);
	public PackedDecimalData dteexp = new PackedDecimalData(8, 0).isAPartOf(nonKeyData, 29);
	public FixedLengthStringData bcmtab = new FixedLengthStringData(4).isAPartOf(nonKeyData, 34);
	public FixedLengthStringData rcmtab = new FixedLengthStringData(4).isAPartOf(nonKeyData, 38);
	public FixedLengthStringData scmtab = new FixedLengthStringData(4).isAPartOf(nonKeyData, 42);
	public FixedLengthStringData agentClass = new FixedLengthStringData(4).isAPartOf(nonKeyData, 46);
	public FixedLengthStringData reportag = new FixedLengthStringData(8).isAPartOf(nonKeyData, 50);
	public PackedDecimalData ovcpc = new PackedDecimalData(5, 2).isAPartOf(nonKeyData, 58);
	public FixedLengthStringData zrorcode = new FixedLengthStringData(4).isAPartOf(nonKeyData, 61);
	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(nonKeyData, 65);
	public FixedLengthStringData prdagent = new FixedLengthStringData(8).isAPartOf(nonKeyData, 70);
	public FixedLengthStringData agccqind = new FixedLengthStringData(1).isAPartOf(nonKeyData, 78);
	public FixedLengthStringData jobName = new FixedLengthStringData(10).isAPartOf(nonKeyData, 79);
	public FixedLengthStringData userProfile = new FixedLengthStringData(10).isAPartOf(nonKeyData, 89);
	public FixedLengthStringData datime = new FixedLengthStringData(26).isAPartOf(nonKeyData, 99);

	
	public AglfmjaTableDAM() {
		super();
		setKeyConstants();
	}	

	public void setTable() {
		TABLE = getTableName("AGLFMJA");
	}
	
	public String getTable() {
		return TABLE;
	}
	
	public void setKeyConstants() {
	
		KEYCOLUMNS = "AGNTCOY"
		             + ", AGNTNUM";
		
		QUALIFIEDCOLUMNS = 
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "CLNTNUM, " +
		            "AGNTBR, " +
		            "DTEAPP, " +
		            "DTETRM, " +
		            "DTEEXP, " +
		            "BCMTAB, " +
		            "RCMTAB, " +
		            "SCMTAB, " +
		            "AGCLS, " +
		            "REPORTAG, " +
		            "OVCPC, " +
		            "ZRORCODE, " +
		            "EFFDATE, " +
		            "PRDAGENT, " +
		            "AGCCQIND, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";											
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
					"UNIQUE_NUMBER DESC";
	
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               agntcoy,
                               agntnum,
                               clntnum,
                               agntbr,
                               dteapp,
                               dtetrm,
                               dteexp,
                               bcmtab,
                               rcmtab,
                               scmtab,
                               agentClass,
                               reportag,
                               ovcpc,
                               zrorcode,
                               effdate,
                               prdagent,
                               agccqind,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };		
	}
		
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/
	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {	
		return setShortHeader(what);
	}

	/****************************************************************/
	/* Getters and setters for key SKM group fields                 */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ filler.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, filler);
		
			return what;
		}
	}

	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
		filler1.setInternal(agntcoy.toInternal());
		filler2.setInternal(agntnum.toInternal());
	
	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(125);
		
		nonKeyData.set(
						  filler1.toInternal()
						+ filler2.toInternal()		
						+ getClntnum().toInternal()
						+ getAgntbr().toInternal()
						+ getDteapp().toInternal()
						+ getDtetrm().toInternal()
						+ getDteexp().toInternal()
						+ getBcmtab().toInternal()
						+ getRcmtab().toInternal()
						+ getScmtab().toInternal()
						+ getAgentClass().toInternal()
						+ getReportag().toInternal()
						+ getOvcpc().toInternal()
						+ getZrorcode().toInternal()
						+ getEffdate().toInternal()
						+ getPrdagent().toInternal()
						+ getAgccqind().toInternal()
						+ getJobName().toInternal()
						+ getUserProfile().toInternal()
						+ getDatime().toInternal()		
					  );
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, filler1);
			what = ExternalData.chop(what, filler2);			
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, agntbr);
			what = ExternalData.chop(what, dteapp);
			what = ExternalData.chop(what, dtetrm);
			what = ExternalData.chop(what, dteexp);
			what = ExternalData.chop(what, bcmtab);
			what = ExternalData.chop(what, rcmtab);
			what = ExternalData.chop(what, scmtab);
			what = ExternalData.chop(what, agentClass);
			what = ExternalData.chop(what, reportag);
			what = ExternalData.chop(what, ovcpc);
			what = ExternalData.chop(what, zrorcode);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, prdagent);
			what = ExternalData.chop(what, agccqind);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for all SKM fields                       */
	/****************************************************************/

	public void setAgntcoy(Object what) {
		this.agntcoy.set(what);
	}	
	public FixedLengthStringData getAgntcoy() {
		return this.agntcoy;
	}		

	public void setAgntnum(Object what) {
		this.agntnum.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return this.agntnum;
	}		

	public void setClntnum(Object what) {
		this.clntnum.set(what);
	}	
	public FixedLengthStringData getClntnum() {
		return this.clntnum;
	}		

	public void setAgntbr(Object what) {
		this.agntbr.set(what);
	}	
	public FixedLengthStringData getAgntbr() {
		return this.agntbr;
	}		

	public void setDteapp(Object what) {
		this.dteapp.set(what);
	}
	public void setDteapp(Object what, boolean rounded) {
		if (rounded)
			this.dteapp.setRounded(what);
		else
			this.dteapp.set(what);
	}
	public PackedDecimalData getDteapp() {
		return this.dteapp;
	}		

	public void setDtetrm(Object what) {
		this.dtetrm.set(what);
	}
	public void setDtetrm(Object what, boolean rounded) {
		if (rounded)
			this.dtetrm.setRounded(what);
		else
			this.dtetrm.set(what);
	}
	public PackedDecimalData getDtetrm() {
		return this.dtetrm;
	}		

	public void setDteexp(Object what) {
		this.dteexp.set(what);
	}
	public void setDteexp(Object what, boolean rounded) {
		if (rounded)
			this.dteexp.setRounded(what);
		else
			this.dteexp.set(what);
	}
	public PackedDecimalData getDteexp() {
		return this.dteexp;
	}		

	public void setBcmtab(Object what) {
		this.bcmtab.set(what);
	}	
	public FixedLengthStringData getBcmtab() {
		return this.bcmtab;
	}		

	public void setRcmtab(Object what) {
		this.rcmtab.set(what);
	}	
	public FixedLengthStringData getRcmtab() {
		return this.rcmtab;
	}		

	public void setScmtab(Object what) {
		this.scmtab.set(what);
	}	
	public FixedLengthStringData getScmtab() {
		return this.scmtab;
	}		

	public void setAgentClass(Object what) {
		this.agentClass.set(what);
	}	
	public FixedLengthStringData getAgentClass() {
		return this.agentClass;
	}		

	public void setReportag(Object what) {
		this.reportag.set(what);
	}	
	public FixedLengthStringData getReportag() {
		return this.reportag;
	}		

	public void setOvcpc(Object what) {
		this.ovcpc.set(what);
	}
	public void setOvcpc(Object what, boolean rounded) {
		if (rounded)
			this.ovcpc.setRounded(what);
		else
			this.ovcpc.set(what);
	}
	public PackedDecimalData getOvcpc() {
		return this.ovcpc;
	}		

	public void setZrorcode(Object what) {
		this.zrorcode.set(what);
	}	
	public FixedLengthStringData getZrorcode() {
		return this.zrorcode;
	}		

	public void setEffdate(Object what) {
		this.effdate.set(what);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			this.effdate.setRounded(what);
		else
			this.effdate.set(what);
	}
	public PackedDecimalData getEffdate() {
		return this.effdate;
	}		

	public void setPrdagent(Object what) {
		this.prdagent.set(what);
	}	
	public FixedLengthStringData getPrdagent() {
		return this.prdagent;
	}		

	public void setAgccqind(Object what) {
		this.agccqind.set(what);
	}	
	public FixedLengthStringData getAgccqind() {
		return this.agccqind;
	}		

	public void setJobName(Object what) {
		this.jobName.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return this.jobName;
	}		

	public void setUserProfile(Object what) {
		this.userProfile.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return this.userProfile;
	}		

	public void setDatime(Object what) {
		this.datime.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return this.datime;
	}		


	//=====================Getter Setter End ========================


	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		agntnum.clear();
		filler.clear();
	}

	public void clearRecNonKeyData() {
	
        filler1.clear();
        filler2.clear();
		clntnum.clear();
		agntbr.clear();
		dteapp.clear();
		dtetrm.clear();
		dteexp.clear();
		bcmtab.clear();
		rcmtab.clear();
		scmtab.clear();
		agentClass.clear();
		reportag.clear();
		ovcpc.clear();
		zrorcode.clear();
		effdate.clear();
		prdagent.clear();
		agccqind.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();
	}

	@Override
	protected void setColumnConstants() {
		// TODO Seems this may never get called		
	}

	@Override
	protected void setColumns() {
		//TODO Seems this may never get called		
	}				
}