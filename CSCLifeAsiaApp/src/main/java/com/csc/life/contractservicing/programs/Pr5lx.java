package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.life.contractservicing.screens.Sr5lxScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr5lxrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr5lx extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pr5lx");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tr5lxrec tr5lxrec = new Tr5lxrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr5lxScreenVars sv = ScreenProgram.getScreenVars( Sr5lxScreenVars.class);
	
	public Pr5lx() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5lx", AppVars.getInstance(), sv);
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray){
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			/*	EXIT	*/
		}
	}
	
	@Override
	protected void initialise1000(){
		initialise1010();
		readRecord();
		moveToScreen();
	}
	
	protected void initialise1010() {
		sv.dataArea.set(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}
	
	protected void readRecord() {
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
	
	protected void moveToScreen() {
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemtabl());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr5lxrec.tr5lxRec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			sv.taxPeriodFrm.set(tr5lxrec.taxPeriodFrm);
			sv.taxPeriodTo.set(tr5lxrec.taxPeriodTo);
		}
	}
	
	protected void preScreenEdit() {
		try {
			preStart();
		}
		catch (GOTOException e){
			/*	EXIT	*/
		}
	}
	
	protected void preStart(){
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
	}
	
	@Override
	protected void screenEdit2000() {
		wsspcomn.edterror.set(varcom.oK);
		if(isEQ(sv.taxPeriodFrm, 0)) {
			sv.taxPeriodFrmErr.set("E186");
		}
		if(isEQ(sv.taxPeriodTo, 0)) {
			sv.taxPeriodToErr.set("E186");
		}
		if(isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			exitScreenEdit();
		}
		exitScreenEdit();
	}
	
	protected void exitScreenEdit() {
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	@Override
	protected void update3000() {
		if (isEQ(wsspcomn.flag,"I")) {
			return;
		}
		validateUpdate();
		if(isEQ(wsaaUpdateFlag,"Y")) {
			fetchRecord();
			updateRecord();
		}
	}
	
	protected void validateUpdate() {
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		if(isNE(sv.taxPeriodFrm, tr5lxrec.taxPeriodFrm)) {
			wsaaUpdateFlag = "Y";
			tr5lxrec.taxPeriodFrm.set(sv.taxPeriodFrm);
		}
		else if(isNE(sv.taxPeriodTo, tr5lxrec.taxPeriodTo)) {
			wsaaUpdateFlag = "Y";
			tr5lxrec.taxPeriodTo.set(sv.taxPeriodTo);
		}
	}
	
	protected void fetchRecord() {
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}
	
	protected void updateRecord() {
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tr5lxrec.tr5lxRec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}
	
	@Override
	protected void whereNext4000()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}
}
