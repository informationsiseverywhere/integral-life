package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:26
 * Description:
 * Copybook name: TR52FREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr52frec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr52fRec = new FixedLengthStringData(500);
  	public FixedLengthStringData dtyamts = new FixedLengthStringData(55).isAPartOf(tr52fRec, 0);
  	public ZonedDecimalData[] dtyamt = ZDArrayPartOfStructure(5, 11, 0, dtyamts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(dtyamts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData dtyamt01 = new ZonedDecimalData(11, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData dtyamt02 = new ZonedDecimalData(11, 0).isAPartOf(filler, 11);
  	public ZonedDecimalData dtyamt03 = new ZonedDecimalData(11, 0).isAPartOf(filler, 22);
  	public ZonedDecimalData dtyamt04 = new ZonedDecimalData(11, 0).isAPartOf(filler, 33);
  	public ZonedDecimalData dtyamt05 = new ZonedDecimalData(11, 0).isAPartOf(filler, 44);
  	public FixedLengthStringData shortdss = new FixedLengthStringData(20).isAPartOf(tr52fRec, 55);
  	public FixedLengthStringData[] shortds = FLSArrayPartOfStructure(2, 10, shortdss, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(shortdss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData shortds01 = new FixedLengthStringData(10).isAPartOf(filler1, 0);
  	public FixedLengthStringData shortds02 = new FixedLengthStringData(10).isAPartOf(filler1, 10);
  	public FixedLengthStringData txabsinds = new FixedLengthStringData(2).isAPartOf(tr52fRec, 75);
  	public FixedLengthStringData[] txabsind = FLSArrayPartOfStructure(2, 1, txabsinds, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(txabsinds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData txabsind01 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
  	public FixedLengthStringData txabsind02 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
  	public FixedLengthStringData txfxamtas = new FixedLengthStringData(35).isAPartOf(tr52fRec, 77);
  	public ZonedDecimalData[] txfxamta = ZDArrayPartOfStructure(5, 7, 0, txfxamtas, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(35).isAPartOf(txfxamtas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData txfxamta1 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData txfxamta2 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 7);
  	public ZonedDecimalData txfxamta3 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 14);
  	public ZonedDecimalData txfxamta4 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 21);
  	public ZonedDecimalData txfxamta5 = new ZonedDecimalData(7, 0).isAPartOf(filler3, 28);
  	public FixedLengthStringData txfxamtbs = new FixedLengthStringData(35).isAPartOf(tr52fRec, 112);
  	public ZonedDecimalData[] txfxamtb = ZDArrayPartOfStructure(5, 7, 0, txfxamtbs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(35).isAPartOf(txfxamtbs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData txfxamtb1 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData txfxamtb2 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 7);
  	public ZonedDecimalData txfxamtb3 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 14);
  	public ZonedDecimalData txfxamtb4 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 21);
  	public ZonedDecimalData txfxamtb5 = new ZonedDecimalData(7, 0).isAPartOf(filler4, 28);
  	public FixedLengthStringData txrateas = new FixedLengthStringData(25).isAPartOf(tr52fRec, 147);
  	public ZonedDecimalData[] txratea = ZDArrayPartOfStructure(5, 5, 2, txrateas, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(25).isAPartOf(txrateas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData txratea01 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 0);
  	public ZonedDecimalData txratea02 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 5);
  	public ZonedDecimalData txratea03 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 10);
  	public ZonedDecimalData txratea04 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 15);
  	public ZonedDecimalData txratea05 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 20);
  	public FixedLengthStringData txratebs = new FixedLengthStringData(25).isAPartOf(tr52fRec, 172);
  	public ZonedDecimalData[] txrateb = ZDArrayPartOfStructure(5, 5, 2, txratebs, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(25).isAPartOf(txratebs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData txrateb01 = new ZonedDecimalData(5, 2).isAPartOf(filler6, 0);
  	public ZonedDecimalData txrateb02 = new ZonedDecimalData(5, 2).isAPartOf(filler6, 5);
  	public ZonedDecimalData txrateb03 = new ZonedDecimalData(5, 2).isAPartOf(filler6, 10);
  	public ZonedDecimalData txrateb04 = new ZonedDecimalData(5, 2).isAPartOf(filler6, 15);
  	public ZonedDecimalData txrateb05 = new ZonedDecimalData(5, 2).isAPartOf(filler6, 20);
  	public FixedLengthStringData txtypes = new FixedLengthStringData(4).isAPartOf(tr52fRec, 197);
  	public FixedLengthStringData[] txtype = FLSArrayPartOfStructure(2, 2, txtypes, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(4).isAPartOf(txtypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData txtype01 = new FixedLengthStringData(2).isAPartOf(filler7, 0);
  	public FixedLengthStringData txtype02 = new FixedLengthStringData(2).isAPartOf(filler7, 2);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(299).isAPartOf(tr52fRec, 201, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr52fRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr52fRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}