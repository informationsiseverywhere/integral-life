package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BfrqpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:01
 * Class transformed from BFRQPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BfrqpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 89;
	public FixedLengthStringData bfrqrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData bfrqpfRecord = bfrqrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData currency = DD.currency.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData freq = DD.freq.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(bfrqrec);
	public PackedDecimalData instpramt = DD.instpramt.copy().isAPartOf(bfrqrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(bfrqrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(bfrqrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(bfrqrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public BfrqpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for BfrqpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public BfrqpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for BfrqpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public BfrqpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for BfrqpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public BfrqpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("BFRQPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"CNTTYPE, " +
							"CURRENCY, " +
							"FREQ, " +
							"BILLFREQ, " +
							"INSTPRAMT, " +
							"INSTPREM, " +
							"EFFDATE, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     cnttype,
                                     currency,
                                     freq,
                                     billfreq,
                                     instpramt,
                                     instprem,
                                     effdate,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		cnttype.clear();
  		currency.clear();
  		freq.clear();
  		billfreq.clear();
  		instpramt.clear();
  		instprem.clear();
  		effdate.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getBfrqrec() {
  		return bfrqrec;
	}

	public FixedLengthStringData getBfrqpfRecord() {
  		return bfrqpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setBfrqrec(what);
	}

	public void setBfrqrec(Object what) {
  		this.bfrqrec.set(what);
	}

	public void setBfrqpfRecord(Object what) {
  		this.bfrqpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(bfrqrec.getLength());
		result.set(bfrqrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}