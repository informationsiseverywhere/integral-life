package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:47
 * Description:
 * Copybook name: UTRNBK1KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrnbk1key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrnbk1FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrnbk1Key = new FixedLengthStringData(64).isAPartOf(utrnbk1FileKey, 0, REDEFINE);
  	public FixedLengthStringData utrnbk1Chdrcoy = new FixedLengthStringData(1).isAPartOf(utrnbk1Key, 0);
  	public FixedLengthStringData utrnbk1Chdrnum = new FixedLengthStringData(8).isAPartOf(utrnbk1Key, 1);
  	public PackedDecimalData utrnbk1PlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrnbk1Key, 9);
  	public FixedLengthStringData utrnbk1Coverage = new FixedLengthStringData(2).isAPartOf(utrnbk1Key, 12);
  	public FixedLengthStringData utrnbk1Rider = new FixedLengthStringData(2).isAPartOf(utrnbk1Key, 14);
  	public FixedLengthStringData utrnbk1Life = new FixedLengthStringData(2).isAPartOf(utrnbk1Key, 16);
  	public FixedLengthStringData utrnbk1UnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrnbk1Key, 18);
  	public FixedLengthStringData utrnbk1UnitType = new FixedLengthStringData(1).isAPartOf(utrnbk1Key, 22);
  	public PackedDecimalData utrnbk1Tranno = new PackedDecimalData(5, 0).isAPartOf(utrnbk1Key, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(utrnbk1Key, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrnbk1FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrnbk1FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}