package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:48
 * Description:
 * Copybook name: PHRTISSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Phrtisskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData phrtissFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData phrtissKey = new FixedLengthStringData(256).isAPartOf(phrtissFileKey, 0, REDEFINE);
  	public FixedLengthStringData phrtissChdrcoy = new FixedLengthStringData(1).isAPartOf(phrtissKey, 0);
  	public FixedLengthStringData phrtissChdrnum = new FixedLengthStringData(8).isAPartOf(phrtissKey, 1);
  	public PackedDecimalData phrtissTranno = new PackedDecimalData(5, 0).isAPartOf(phrtissKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(phrtissKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(phrtissFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		phrtissFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}