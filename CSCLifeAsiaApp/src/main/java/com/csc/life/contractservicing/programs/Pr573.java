/*
 * File: Pr573.java
 * Date: 30 August 2009 1:43:53
 * Author: Quipoz Limited
 * 
 * Class transformed from PR573.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE; //ICIL-297
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Datcon3Pojo;
import com.csc.fsu.general.procedures.Datcon3Utils;
//import com.csc.fsu.general.procedures.Datcon2; //ILB-499
//import com.csc.fsu.general.procedures.Datcon3; //ICIL-297, //ILB-499
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg; //ICIL-297
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
//import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;//ILB-499
//import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;//ILB-499
import com.csc.life.contractservicing.dataaccess.dao.CprppfDAO;
import com.csc.life.contractservicing.dataaccess.dao.CvuwpfDAO;
//ILB-456 ends
import com.csc.life.contractservicing.dataaccess.model.Cprppf;
import com.csc.life.contractservicing.dataaccess.model.Cvuwpf;
import com.csc.life.contractservicing.screens.Sr573ScreenVars;
import com.csc.life.contractservicing.tablestructures.Td5izrec;
//import com.csc.life.general.procedures.Agecalc;  //ICIL-297, //ILB-499
//import com.csc.life.general.recordstructures.Agecalcrec; //ICIL-297, //ILB-499
//import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM; //ILB-499
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;//ICIL-297
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;//ICIL-297
import com.csc.life.newbusiness.programs.P5006;
import com.csc.life.newbusiness.tablestructures.Th506rec;
//ILB-456 starts
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Td5gfrec;   //ICIL-297
import com.csc.smart.procedures.BatcdorUtil;//ILB-499
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
//import com.csc.smart.procedures.Sftlock;//ILB-499
import com.csc.smart.procedures.SftlockUtil;//ILB-499
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec; //ICIL-297
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.SftlockRecBean;//ILB-499
//import com.csc.smart.recordstructures.Sftlockrec;//ILB-499
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1; //ICIL-297
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*               PR573 - Component CHANGES Submenu.
*               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Initialise
*----------
*
*  Initialise the screen for display.
*
*
*Validation
*----------
*
*  Validate contract number entered by performing a READR on the
*  Logical file CHDRMJA.
*
*  Read T5729 to check whether contract is flexible.
*
*  Validate Key - 1
*
*       If SR573-CHDRSEL not  equal  to  spaces call the CHDRMJA
*       I/O module for  the  contract.  If  any  combination  of
*       CHDRSEL and SUBP-KEY1 other than O-K and 'Y' then error.
*
*       Release the COVRMJA I/O module in case it has previously
*       been held.
*
*       Check STATCODE to validate  status of the contract is In
*       Force (IF), against T5679.
*
*       If NOT valid then Error.
*
*  Validate Key - 2
*
*       If the screen  effective  date has changed then validate
*       this date against the header record (CHDRMJA).
*
*Updating
*--------
*
*
*  At this point  we  must  distinguish  Journals from any other
*  Submenu as Journals  will  share  the following common select
*  programs. The reason  being, Selection accross the whole plan
*  (select option =  '0000')  is  only valid for Journals if the
*  number summarised equals  the  number  of policies within the
*  Plan. Fund Switching  however,  can select accross broken out
*  policies within a Plan.
*
*       If SCRN-ACTION = 'I' move 'J' to WSSP-FLAG
*       Else move 'N' to WSSP-FLAG.
*
*
*  If  WSSP-FLAG  is NOT = 'J' then softlock the contract header
*  by  calling SFTLOCK. If the record is already locked - Statuz
*  =  'LOCK'  then  error  with a code of F910 and redisplay the
*  screen.
*
*  If  a valid contract header record found then perform a KEEPS
*  on  the  record  in  order  to store it for use in the select
*  programs.
****** Version 4.0 development *****
*  WSSP-FLAG MAPPING
*  Action      Description           WSSP-FLAG
*  A           Add Proposal          A (Already in Base)
*  B           Modify Proposal       M (Already in Base)
*  C           Add Approval          P
*  D           Modify Approval       S
*  E           Reverse Proposal      R
*  F           Add Life              D (Already in Base)
*  G           Enquiry               I (Already in Base)
*  H           Contract Follow Ups   A
*
*
*Next Program.
*-------------
*
*  Otherwise add 1 to the Program pointer and exit to the Policy
*  selection screen.
*
*
*****************************************************************
* </pre>
*/
public class Pr573 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
//	public int numberOfParameters = 0; // to resolve findbug commented 
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR573");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaa1yrb4Btdate = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSuspAvail = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaLives = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	protected PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned(); //ICIL-297

	private FixedLengthStringData wsaaStatFlag = new FixedLengthStringData(1);
	private Validator wsaaExitStat = new Validator(wsaaStatFlag, "Y");
	private ZonedDecimalData wsaaT5645Offset = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private PackedDecimalData wsaaT5645Sub = new PackedDecimalData(5, 0);
	//private static final int wsaaT5645Size = 12;
	private FixedLengthStringData wsaaFreelookBasisChn = new FixedLengthStringData(1); //ICIL-297
	private Validator issueDateChnBasis = new Validator(wsaaFreelookBasisChn, "1"); //ICIL-297
	private Validator ackBasis = new Validator(wsaaFreelookBasisChn, "2"); //ICIL-297
	private Validator deemedBasis = new Validator(wsaaFreelookBasisChn, "3"); //ICIL-297
	private Validator minAckDeemedBasis = new Validator(wsaaFreelookBasisChn, "4"); //ICIL-297
	private ZonedDecimalData wsaaFreelookDate = new ZonedDecimalData(8, 0).setUnsigned(); //ICIL-297
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned(); //ICIL-297
	private ZonedDecimalData wsaaTdayno = new ZonedDecimalData(3, 0).setUnsigned(); //ICIL-297


	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(SPACES);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");
	
	    /* TABLES */
	private static final String td5gf = "TD5GF";  // ICIL-297
	private static final String td5iz = "TD5IZ";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	
		/*    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
//	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM(); //ILB-499
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	//private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Datcon1rec datcon1rec = new Datcon1rec(); //ICIL-297
	private Sanctnrec sanctnrec = new Sanctnrec();
//	private Sftlockrec sftlockrec = new Sftlockrec(); //ILB-499
	private Subprogrec subprogrec = new Subprogrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Th506rec th506rec = new Th506rec();
	private T5645rec t5645rec = new T5645rec();
	private T3695rec t3695rec = new T3695rec();
	private Td5gfrec td5gfrec = new Td5gfrec();      // ICIL-297
	private Td5izrec td5izrec = new Td5izrec();
	protected Sr573ScreenVars sv = ScreenProgram.getScreenVars( Sr573ScreenVars.class);
	protected ErrorsInner errorsInner = new ErrorsInner();

//ILIFE-3310-add by liwei
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class); //ICIL-297
	private Itempf itempf = new Itempf(); //ICIL-297  //ILIFE-8723
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); //ICIL-297
	private Hpadpf hpadpf; //ICIL-297
	private AcblpfDAO acblDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private CvuwpfDAO cvuwpfDAO = getApplicationContext().getBean("cvuwpfDAO", CvuwpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private CprppfDAO cprppfDAO = getApplicationContext().getBean("cprppfDAO", CprppfDAO.class);
	private Lifepf lifepf = new Lifepf(); //ICIL-297
	private Acblpf acblpf = null;
	private Cvuwpf cvuwpf = null;
	private Cprppf cprppf = null;
	
	boolean cscom004Permission = false; // ICIL-297
	
	//private ItemTableDAM itemIO = new ItemTableDAM(); //ILB-499
	private static final String t5679 = "T5679";
	private static final String t5688 = "T5688";
	private static final String t5729 = "T5729";
	private static final String th506 = "TH506";
	boolean CSMIN003Permission  = false;
	
	//ILB-499 shivika
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private SftlockUtil sftlockUtil = new SftlockUtil();
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Datcon3Utils datcon3Utils = getApplicationContext().getBean("datcon3Utils", Datcon3Utils.class); 
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class); 
	private AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class); 
	
	/* ERRORS */
	private static final String e455 = "E455";
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e544 = "E544";
	private static final String e767 = "E767";
	private static final String f910 = "F910";
	private static final String g436 = "G436";
	private static final String f616 = "F616";
	private static final String e331 = "E331";
	private static final String e370 = "E370";
	private static final String e268 = "E268";
	private static final String rl31 = "RL31";
	private static final String f779 = "F779";
	private static final String h791 = "H791";
	private static final String rl32 = "RL32";
	private static final String rl35 = "RL35";
	private static final String rlcb = "RLCB";
	private static final String rlaj = "RLAJ";
	private static final String rrgi = "RRGI";
	private static final String rrj5 = "RRJ5";
	private static final String e492 = "E492";
	//ILB-499 end 

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2590, 
		keeps3070, 
		exit3090
	}

	public Pr573() {
		super();
		screenVars = sv;
		new ScreenModel("Sr573", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		/*    Initialise screen.*/
		boolean uwFlag = FeaConfg.isFeatureExist("2", "NBPRP123",appVars, "IT");
		if(uwFlag) {
			ThreadLocalStore.put(Pr572.AUTO_TERMS_LIST_ADD, null);
		}
	    cscom004Permission  = FeaConfg.isFeatureExist("2", "CSCOM004", appVars, "IT");     // ICIL-297
		sv.dataArea.set(SPACES);
		wsaaStatFlag.set("N");
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070); //ILB-499
		}
		sv.effdate.set(varcom.vrcmMaxDate);
		wsaaLives.set(ZERO);
		
		//ICIL-297 start
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		//ICIL-end.
		CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
		boolean lincFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP116", appVars, "IT");
		if(CSMIN003Permission || lincFlag) {
			sv.fuflag.set("Y");
		} else {
			sv.fuflag.set("N");
		}
		/*EXIT*/
	}

/*protected void readT5679() {
	boolean itemFound = false;
	String keyItemitem = subprogrec.transcd.toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	if (t5679ListMap.containsKey(keyItemitem)) { 
		itempfList = t5679ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf
					.getGenarea()));
			itemFound = true;

		}
	}
	if (!itemFound) {
		syserrrec.params.set(t5679rec.t5679Rec);
		fatalError600();
	}
}*/

	/*protected void readT5645() {
		boolean itemFound = false;
		String keyItemitem = "P5132";
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t5645ListMap.containsKey(keyItemitem)) {
			itempfList = t5645ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if (isEQ(itempf.getItemseq(), SPACES)) {
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}

			}
		}
		if (!itemFound) {
			syserrrec.params.set(t5645rec.t5645Rec);
			fatalError600();
		}
	}*/
	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'SR573IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SR573-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/*    Validate all fields including batch request if required.*/
		
		//ILIFE-4450 For the loading submenu quickly, so the codes move to 2000 section
				
		validateAction2100();		
		
		
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2500();
			}
		}
		
				
			
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		/*    Call subprog routine to load the stack with the next*/
		/*    programs.*/
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		/*    Call sanctn routine to check that the user has been*/
		/*    sanctioned to execute Journaling.*/
		sanctnrec.function.set("SUBM");
		/* MOVE WSSP-PASSWORD          TO SNCT-PASSWORD.                */
		/* MOVE WSSP-USERID            TO SNCT-PASSWORD.                */
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			syserrrec.params.set(sanctnrec.sanctnRec);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		/*    Read the Header record for the contract number entered.*/
		chdrpf.setChdrcoy(wsspcomn.company.charat(0));
		chdrpf.setChdrnum(sv.chdrsel.toString());
		//chdrmjaIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			//ILB-456
			chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
			//SmartFileCode.execute(appVars, chdrmjaIO);
		}
		/*if (chdrpf == null) {
			sv.chdrselErr.set(errorsInner.e544);
			wsspcomn.edterror.set("Y");
			return;
		}*/
		if((isEQ(sv.chdrsel,SPACES)) && isEQ(subprogrec.key1,"Y") || chdrpf == null){
			sv.chdrselErr.set(e544); //ILB-499
			wsspcomn.edterror.set("Y");
			return;
		}
		/*else {
			chdrmjaIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		/*    If no record found and Key1 in T1690 is set to 'Y' then*/
		/*    error.*/
		/*if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(errorsInner.e544);
			wsspcomn.edterror.set("Y");
			return ;
		}*/
		
		validateContractModification();
		
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		readTh5065100();
		if (isEQ(sv.action,"A")
		&& isNE(th506rec.indic,"Y")
		&& isNE(sv.effdate,varcom.vrcmMaxDate)
		&& isNE(sv.effdate,ZERO)) {
			sv.effdateErr.set(rl31); //ILB-499
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(sv.action,"A")
		&& isNE(sv.effdate,varcom.vrcmMaxDate)) {
			sv.effdateErr.set(e492); //ILB-499
			wsspcomn.edterror.set("Y");
			return ;
		}
		//========add Follow ups link=======
		if (isEQ(subprogrec.key1,"Y")) {
			if (isEQ(sv.action,"H")) {
				if (isEQ(sv.chdrsel,SPACES)) {
					sv.chdrselErr.set(e544); //ILB-499
					return;
				}
				//chdrmjaIO.setParams(SPACES);
				chdrpf.setChdrcoy(wsspcomn.company.charat(0));
				chdrpf.setChdrnum(sv.chdrsel.toString());
				/*chdrpf.setChdrcoy(wsspcomn.company);
				chdrmjaIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, chdrmjaIO);
				if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
				&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
					syserrrec.params.set(chdrmjaIO.getParams());
					syserrrec.statuz.set(chdrmjaIO.getStatuz());
					fatalError600();
				}
				if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)) {
					sv.chdrselErr.set(errorsInner.e544);
					return;
				}*/
				chdrpf = chdrpfDAO.getchdrRecord(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum().replaceAll("\\s", ""));//ILIFE-8769

				if (chdrpf == null) {
					sv.chdrselErr.set(e544); //ILB-499
					return;
				}
				wsspcomn.currfrom.set(chdrpf.getPtdate());
				initialize(sdasancrec.sancRec);
				sdasancrec.function.set("VENTY");
				sdasancrec.userid.set(wsspcomn.userid);
				sdasancrec.entypfx.set(fsupfxcpy.chdr);
				sdasancrec.entycoy.set(wsspcomn.company);
				sdasancrec.entynum.set(sv.chdrsel);
				callProgram(Sdasanc.class, sdasancrec.sancRec);
				if (isNE(sdasancrec.statuz, varcom.oK)) {
					sv.chdrselErr.set(sdasancrec.statuz);
					return;
				}
				checkStatus2300();

		}
		}







     	/*ICIL-297 start*/
		if (((isEQ(sv.action,"A"))
		|| (isEQ(sv.action,"B")))
		&& (cscom004Permission)&&(isNE(chdrpf.getValidflag(),"3"))) {   //ILIFE-9322
			freelookCheckCHN();
			if (isNE(sv.chdrselErr, SPACES)) {
				return;
			}
		}
		/*ICIL-297 end*/
				
		if (isEQ(sv.action,"A")) {
			checkProcessDate();
			r100ReadCvuw();
			if (cvuwpf != null) {
				if (isEQ(sv.effdate,varcom.vrcmMaxDate)) {
					sv.effdate.set(cvuwpf.getEffdate());
				}
				else {
					if (isNE(sv.effdate,cvuwpf.getEffdate())) {
						sv.effdateErr.set(rl35);
						wsspcomn.edterror.set("Y");
						return ;
					}
				}
			}
		}
		if (isEQ(sv.action,"B")) {
			checkProcessDate();
		}
		if (isEQ(sv.action, "F")) {
			checkLives5000();
		}
		if (isEQ(wsaa1yrb4Btdate, 0)) {
			//ILB-456 start
			Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(-1);
			datcon2Pojo.setIntDate1(chdrpf.getBtdate().toString());
			datcon2Pojo.setFrequency(chdrpf.getBillfreq());//ILIFE-8769
			datcon2Utils.calDatcon2(datcon2Pojo);
		
			if (datcon2Pojo.getStatuz() == null) {
				syserrrec.statuz.set(datcon2Pojo.getStatuz());
				syserrrec.params.set(datcon2Pojo.toString());
				fatalError600();
			}
			wsaa1yrb4Btdate.set(datcon2Pojo.getIntDate2());
			
			/*datcon2rec.freqFactor.set(-1);
			datcon2rec.intDate1.set(chdrmjaIO.getBtdate());
			datcon2rec.frequency.set(chdrmjaIO.getBillfreq());
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isEQ(datcon2rec.statuz, varcom.bomb)) {
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaa1yrb4Btdate.set(datcon2rec.intDate2);*/
			//ILB-456 end
		}
		readT57292220();
		/* Release any previously kept COVRMJA record. */
		covrpfDAO.deleteCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK) && isNE(covrmjaIO.getStatuz(), varcom.mrnf)) {

			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		/* Validate contract selected is of the correct status and */
		/* from the correct branch. */
		if (isEQ(subprogrec.key1, "Y") && isEQ(sv.chdrselErr, SPACES)) {

			checkStatus2300();
		}
		/* If incorrect Branch then error. */
		if (isEQ(subprogrec.key1, "Y") && isEQ(sv.chdrselErr, SPACES) && isNE(wsspcomn.branch, chdrpf.getCntbranch())) {


			sv.chdrselErr.set(e455); //ILB-499
		}
		/* Billed to Date must equal Paid to Date if Component Change */
		/* is to be actioned. */
		if (isEQ(sv.chdrselErr, SPACES)) {
			/* IF CHDRMJA-BTDATE NOT = CHDRMJA-PTDATE */
			/* MOVE G436 TO SR573-CHDRSEL-ERR */
			if (isNE(chdrpf.getBtdate(), chdrpf.getPtdate())&& !flexiblePremium.isTrue()) {

				sv.chdrselErr.set(g436); //ILB-499
			}
		}
		/* If contract not in force redesplay screen */
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	 * @author fwang3
	 * ICIL-559
	 */
	private void checkProcessDate() {
		boolean chinaLocalPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM007", appVars, "IT");
		if(chinaLocalPermission) {
			List<Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), td5iz,
					chdrpf.getCnttype());//ILIFE-8769
			if (!items.isEmpty()) {
				td5izrec.td5izRec.set(StringUtil.rawToString(items.get(0).getGenarea()));
				if ((isEQ(sv.action, "A") && td5izrec.mthscompadd.toInt() != 0)
						|| (isEQ(sv.action, "B") && td5izrec.mthscompmod.toInt() != 0)) {
					checkCurrentDate();
				}
			}
		}
	}
	
	/**
	 * @author fwang3
	 * ICIL-559
	 */
	private void checkCurrentDate() {
		if (isNE(sv.effdate, varcom.vrcmMaxDate)) {
			sv.effdateErr.set(e492);  //ILB-499
			wsspcomn.edterror.set("Y");
			return ;
		}
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		String fromDateStr = "";
		if (isEQ(scrnparams.action, "A")) {
			fromDateStr = DateUtils.dateMinusMonths(chdrpf.getPtdate().toString(), td5izrec.mthscompadd.toInt());
		}
		if (isEQ(scrnparams.action, "B")) {
			fromDateStr = DateUtils.dateMinusMonths(chdrpf.getPtdate().toString(), td5izrec.mthscompmod.toInt());
		}
		if (!DateUtils.isInDateRange(datcon1rec.intDate.toString(), fromDateStr, chdrpf.getPtdate().toString())) {
			sv.actionErr.set(rrj5); //ILB-499
			wsspcomn.edterror.set("Y");
		}
	}
	
//*CIL-297 start*/
protected void freelookCheckCHN() 	
{
	/* Validate the cooling off period for freelook cancellation       */
	/* based on the rule defined in TD5GF.                             */
	getCoolOff();
	wsaaFreelookBasisChn.set(td5gfrec.ratebas);       
	if (!(issueDateChnBasis.isTrue()
	|| ackBasis.isTrue()
	|| deemedBasis.isTrue()		
	|| minAckDeemedBasis.isTrue())) {
		sv.chdrselErr.set(rlcb); //ILB-499
		return ;
	}
	hpadpf=hpadpfDAO.getHpadData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());//ILIFE-8769
	/* issue date*/
	if (issueDateChnBasis.isTrue()) {
		wsaaFreelookDate.set(hpadpf.getHissdte());
	}
	
	/* ack */
	if (ackBasis.isTrue()) {
		if (isNE(hpadpf.getPackdate(), varcom.vrcmMaxDate)
				&& isNE(hpadpf.getPackdate(), ZERO)) {
					wsaaFreelookDate.set(hpadpf.getPackdate());
				}
				else {
					sv.chdrselErr.set(rlaj); //ILB-499
					return ;
				}
	}
	
	/* deemed */
	if (deemedBasis.isTrue()) {
		if (isNE(hpadpf.getDeemdate(), varcom.vrcmMaxDate)
				&& isNE(hpadpf.getDeemdate(), ZERO)) {
					wsaaFreelookDate.set(hpadpf.getDeemdate());
				}
				else {
					sv.chdrselErr.set(rlaj); //ILB-499
					return ;
				}
	}
	
	/* min- ack or deemed */
	if (minAckDeemedBasis.isTrue()) {
		if (isGT(hpadpf.getDeemdate(), hpadpf.getPackdate())) {
			if (isNE(hpadpf.getPackdate(), varcom.vrcmMaxDate)
					&& isNE(hpadpf.getPackdate(), ZERO)) {
						wsaaFreelookDate.set(hpadpf.getPackdate());
					}
		}
		else {
			if (isNE(hpadpf.getDeemdate(), varcom.vrcmMaxDate)
					&& isNE(hpadpf.getDeemdate(), ZERO)) {
						wsaaFreelookDate.set(hpadpf.getDeemdate());
					}
					else {
						sv.chdrselErr.set(rlaj); //ILB-499
						return ;
					}
		}

	}
	//ILB-456 start
		Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
		datcon3Pojo.setIntDate1(wsaaFreelookDate.toString());
		datcon3Pojo.setIntDate2(wsaaToday.toString());
		datcon3Pojo.setFrequency("DY");
		datcon3Utils.calDatcon3(datcon3Pojo);
		String statuz = datcon3Pojo.getStatuz();
		if (isNE(statuz,varcom.oK)) {
			syserrrec.statuz.set(statuz);
			syserrrec.params.set(datcon3Pojo.toString());
			fatalError600();
		}
		if (isLT(datcon3Pojo.getFreqFactor(), wsaaTdayno)) {
			sv.chdrselErr.set(rrgi); //ILB-499
		}

		
		/*datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaFreelookDate);
		datcon3rec.intDate2.set(wsaaToday);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isLT(datcon3rec.freqFactor, wsaaTdayno)) {
			sv.chdrselErr.set(rrgi);
		}*/
		
		//ILB-456 end
}   

protected void getCoolOff() 
{
	lifepf = lifepfDAO.getLifeRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");//ILIFE-8769
	if (lifepf== null ) {
		syserrrec.params.set(lifepf);
		fatalError600();
	}
	//ILB-456 starts
	AgecalcPojo agecalcPojo = new AgecalcPojo();
	agecalcPojo.setFunction("CALCP");
	agecalcPojo.setLanguage(wsspcomn.language.toString());
	agecalcPojo.setIntDate1((Integer.toString(lifepf.getCltdob())));
	agecalcPojo.setIntDate2(wsaaToday.toString());
	agecalcPojo.setCompany(wsspcomn.fsuco.toString());
	agecalcPojo.setCnttype(chdrpf.getCnttype().trim());	//ICIL-1387
	agecalcUtils.calcAge(agecalcPojo);
	if (isNE(agecalcPojo.getStatuz(), varcom.oK)) {
		syserrrec.params.set(agecalcPojo.toString());
		syserrrec.statuz.set(agecalcPojo.getStatuz());
		fatalError600();
	}
	wsaaAnb.set(agecalcPojo.getAgerating());
	
	/*initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set(wsspcomn.language);
	agecalcrec.intDate1.set(lifepf.getCltdob());
	agecalcrec.intDate2.set(wsaaToday);
	agecalcrec.company.set(wsspcomn.fsuco);
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isNE(agecalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		fatalError600();
	}
	wsaaAnb.set(agecalcrec.agerating);*/
	//ILB-456 ends

	readTd5gf1100();

		if (isEQ(lifepf.getCltsex(),td5gfrec.gender01)  && isGTE(wsaaAnb,td5gfrec.frmage01) && isLTE(wsaaAnb,td5gfrec.toage01)) {
			td5gfrec.cooloff.set(td5gfrec.coolingoff1);
			wsaaTdayno.set(td5gfrec.coolingoff);
			
		}
		else {
			if (isEQ(lifepf.getCltsex(),td5gfrec.gender02) && isGTE(wsaaAnb,td5gfrec.frmage02) && isLTE(wsaaAnb,td5gfrec.toage02)) {
				td5gfrec.cooloff.set(td5gfrec.coolingoff2);
				wsaaTdayno.set(td5gfrec.coolingoff);
			}
			else {
				if (isEQ(lifepf.getCltsex(),td5gfrec.gender03) && isGTE(wsaaAnb,td5gfrec.frmage03) && isLTE(wsaaAnb,td5gfrec.toage03)) {
					td5gfrec.cooloff.set(td5gfrec.coolingoff3);
					wsaaTdayno.set(td5gfrec.coolingoff);
				}
				
				else {
					if (isEQ(lifepf.getCltsex(),td5gfrec.gender04) && isGTE(wsaaAnb,td5gfrec.frmage04) && isLTE(wsaaAnb,td5gfrec.toage04)) {
						td5gfrec.cooloff.set(td5gfrec.coolingoff4);
						wsaaTdayno.set(td5gfrec.coolingoff);
					}
				}
						
			}
		}
}
protected void readTd5gf1100() 
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(td5gf);//ILIFE-8769
	itempf.setItemitem(chdrpf.getCnttype() + chdrpf.getSrcebus());//ILIFE-8769
	itempf = itempfDAO.getItempfRecord(itempf);
	
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf);//ILIFE-8769
		itempf.setItemitem(chdrpf.getCnttype() +  "**");//ILIFE-8769
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf);//ILIFE-8769
		itempf.setItemitem("*****");
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	
	if (itempf != null) {
		td5gfrec.td5gfRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else {
		td5gfrec.td5gfRec.set(SPACES);
	}
	
}
/*ICIL-297 end*/

protected void readT57292220() {
		wsaaFlexiblePremium.set("N");
		itempf = new Itempf();	//ILJ-668
 //ILIFE-8723 start
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5729);
		itempf.setItemitem(chdrpf.getCnttype());
		itempf = itempfDAO.getItempfRecord(itempf);
		if(itempf!=null) {
			wsaaFlexiblePremium.set("Y");
		}
	}

protected void checkStatus2300()
	{
		
		/* Read the Status table to check it is an in force contract. */
		//readT5679();
		readStatusTable2310();
		wsaaSub.set(ZERO);
		wsaaStatFlag.set("N");
		/* Look through the statii. */
		while (!(wsaaExitStat.isTrue())) {
			lookForStat2400();
		}


	}

protected void readStatusTable2310()
{
	//ILB-499 start
	 //ILIFE-8723 start
	List<Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), "T5679", subprogrec.transcd.toString());
	if (null == items || items.isEmpty()) {
		syserrrec.params.set("T5679");
		fatalError600();
	} else {
		t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	}
 //ILIFE-8723 end

}
protected void lookForStat2400()
	{
		/*SEARCH*/
		/*    Table Loop.*/
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.chdrselErr.set(e767); //ILB-499
			wsaaStatFlag.set("Y");
		}
		else {
			if (isNE(chdrpf.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaStatFlag.set("N");
			}
			else {
				wsaaStatFlag.set("Y");
			}
		}
		/*EXIT*/
	}

protected void verifyBatchControl2500()
	{
		try {
			validateRequest2510();
			retrieveBatchProgs2520();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2510()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			/*      MOVE E073               TO SR573-ACTION-ERR.             */
			sv.actionErr.set(e073); //ILB-499
			goTo(GotoLabel.exit2590);
		}
	}

protected void retrieveBatchProgs2520()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*  Effective date must be entered for add component  .            
	*  It must be a valid date.                                       
	*  It must also not be prior to the contract commencement date.   
	* </pre>
	*/
protected void validateEffdate2700() {
	
		if (isNE(sv.effdateErr, SPACES)) {
			return;
		}
		if (isEQ(sv.effdate, varcom.vrcmMaxDate) || isNE(th506rec.indic, "Y")) {
			sv.effdate.set(chdrpf.getBtdate());
		} else {
			if (isLT(sv.effdate, chdrpf.getOccdate())) {
				sv.effdateErr.set(f616); //ILB-499
			}
		}
		if (isLT(sv.effdate, wsaa1yrb4Btdate)) {
			sv.effdateErr.set(rl32); //ILB-499
		}
		if (isGT(sv.effdate, chdrpf.getBtdate())) {
			sv.effdateErr.set(f779); //ILB-499
		}
		if (cvuwpf != null
		&& isNE(sv.effdate,cvuwpf.getEffdate())) {
			sv.effdateErr.set(rl35); //ILB-499
			return ;
		}
		wsspcomn.currfrom.set(sv.effdate);
		checkSuspense2800();
	}

protected void r100ReadCvuw()
	{
		/*R100-START*/
		cvuwpf = cvuwpfDAO.getCvuwpfRecord(wsspcomn.company.toString(), sv.chdrsel.toString());
		/*R100-EXIT*/
	}

protected void checkSuspense2800(){
	//	readT5645();
		List<Itempf> itempfList ;
		itempfList = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), "T5645", "P5132");
		if(itempfList.isEmpty()){
			syserrrec.params.set("T5645");			
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));

		acblpf = acblDAO.loadDataByBatch(chdrpf.getChdrcoy().toString(), t5645rec.sacscode01.toString(), chdrpf.getChdrnum(),chdrpf.getBillcurr(),t5645rec.sacstype01.toString());//ILIFE-8769
		
		
		itempfList = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), "T3695", t5645rec.sacstype01.toString().trim());
		if(itempfList.isEmpty()){
			syserrrec.params.set("T3695");
			fatalError600();
		}
		t3695rec.t3695Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));

		
		if (acblpf == null) {
			wsaaSuspAvail.set(0);
		}
		else {
			if (isEQ(t3695rec.sign,"-")) {
				compute(wsaaSuspAvail, 2).set(sub(0,acblpf.getSacscurbal()));
			}
			else {
				wsaaSuspAvail.set(acblpf.getSacscurbal());
			}
		}
		
		cprppf = new Cprppf();
		cprppf.setChdrpfx(chdrpf.getChdrpfx());//ILIFE-8769
		cprppf.setChdrcoy(chdrpf.getChdrcoy().toString());
		cprppf.setChdrnum(chdrpf.getChdrnum());//ILIFE-8769
		cprppf.setLife("99");
		cprppf.setCoverage("00");
		cprppf.setRider("00");
		cprppf.setInstprem(wsaaSuspAvail.toDouble());
		
		

		if (cvuwpf != null) {
			cprppf.setTranno(chdrpf.getTranno());
		}
		else {
			setPrecision(cprppf.getTranno(), 0);
			cprppf.setTranno(chdrpf.getTranno()+1);
		}
		cprppf.setValidflag("1");
		if(cprppfDAO.updateOrInstCprppf(cprppf)<=0){
			syserrrec.params.set(chdrpf.getChdrnum());//ILIFE-8769
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
					softLock3030();
				case keeps3070: 
					keeps3070();
					batching3080();
				case exit3090: 
				}

				if (isEQ(subprogrec.key1,"Y")
						&& isEQ(sv.action,"H")) {
					//ILB-456
				/*	chdrmjaIO.setFunction("KEEPS");
					chdrmjaIO.setFormat("CHDRMJAREC");
					SmartFileCode.execute(appVars, chdrmjaIO);
					if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
						syserrrec.params.set(chdrmjaIO.getParams());
						fatalError600();
					}*/
					chdrpfDAO.setCacheObject(chdrpf);
				}


				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		/*    Update the WSSP storage area.*/
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*    IF SCRN-ACTION              = 'A'                            */
		/*       MOVE 'A'                 TO WSSP-FLAG.                    */
		/*    IF SCRN-ACTION              = 'B'                            */
		/*       MOVE 'M'                 TO WSSP-FLAG.                    */
		/*    IF SCRN-ACTION              = 'C'                            */
		/*       MOVE 'I'                 TO WSSP-FLAG.                    */
		/*    IF SCRN-ACTION              = 'D'                    <V4L004>*/
		/*       MOVE 'A'                 TO WSSP-FLAG.            <V4L004>*/
		/*                                                         <V4L004>*/
		if (isEQ(scrnparams.action,"A")){
			wsspcomn.flag.set("A");
		}
		else if (isEQ(scrnparams.action,"B")){
			wsspcomn.flag.set("M");
		}
		else if (isEQ(scrnparams.action,"C")){
			wsspcomn.flag.set("P");
		}
		else if (isEQ(scrnparams.action,"D")){
			wsspcomn.flag.set("S");
		}
		else if (isEQ(scrnparams.action,"E")){
			wsspcomn.flag.set("R");
		}
		else if (isEQ(scrnparams.action,"F")){
			wsspcomn.flag.set("D");
		}
		else if (isEQ(scrnparams.action,"G")){
			wsspcomn.flag.set("I");
		}
		else if (isEQ(scrnparams.action,"H")){
			wsspcomn.flag.set("A");	//IBPLIFE-1093
		}
		if (isEQ(scrnparams.action,"A")
		&& isEQ(th506rec.indic,"Y")) {
			validateEffdate2700();
		}
		else {
			sv.effdate.set(chdrpf.getBtdate());
			wsspcomn.currfrom.set(chdrpf.getBtdate());
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			batching3080();
			goTo(GotoLabel.exit3090);
		}
		/*    If inquiry option selected then we do not need the use of*/
		/*    soft-lock.*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.keeps3070);
		}
	}

protected void softLock3030()
	{
		/*    Soft lock the contract.*/
	//ILB-499 start
		/*sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		 MOVE SR573-CHDRSEL          TO SFTL-ENTITY.                  
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}*/
		
		sftlockRecBean.setFunction("LOCK");
		sftlockRecBean.setCompany(wsspcomn.company.toString());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(chdrpf.getChdrnum());//ILIFE-8769
		sftlockRecBean.setTransaction(subprogrec.transcd.toString());
		sftlockRecBean.setUser(varcom.vrcmUser.toString());
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
			&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				fatalError600();
		}
		if (isEQ(sftlockRecBean.getStatuz(),"LOCK")) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		//ILB-499 end
	}

protected void keeps3070()
	{
		/*   Store the contract header for use by the transaction programs*/
		//ILB-456
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction("KEEPS");
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		/* In order for Full Pre-issue validation to work a keep must be*/
		/* done for CHDRLNB.*/
		 //ILB-499 start
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
		if(chdrpf == null ){
			fatalError600();
		}
		/*chdrlnbIO.setDataKey(chdrmjaIO.getDataKey());
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}*/
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}*/
		 //ILB-499 ends
	}

protected void batching3080()
	{
		/*    Update Batch control if required.*/
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
	//	callProgram(Batcdor.class, batcdorrec.batcdorRec); //ILB-499
		BatcdorUtil.getInstance().process(batcdorrec); //ILB-499
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

protected void checkLives5000()
	{		
 //ILIFE-8723 start
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t5688);
	itempf.setItemitem(chdrpf.getCnttype());
	itempf.setItempfx("IT");	//ILJ-668
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	
	if(itempf!=null) {
		t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else{ 
		scrnparams.errorCode.set(e268); //ILB-499
	}	
 //ILIFE-8723 end
		wsaaLives.set(0);
		List<Lifepf> lifeList = lifepfDAO.getLifeData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "01", "00");//ILIFE-8769
		if(lifeList != null && lifeList.size() > 0){
			wsaaLives.add(lifeList.size());
		}else{
			syserrrec.statuz.set(e331); //ILB-499
			fatalError600();
		}
		if (isGTE(wsaaLives,t5688rec.lifemax)) {
			sv.actionErr.set(e370); //ILB-499
			wsspcomn.edterror.set("Y");
		}
	}
protected void readTh5065100()
	{
	
		/*    Read table TH506 to determine, whether the component         */
		/*    add anytime has been enabled for this contract type          */
	 //ILIFE-8723 start
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(th506);
		itempf.setItemitem(chdrpf.getCnttype());
		itempf = itemDAO.getItempfRecord(itempf);
		if(itempf == null) {
			String keyItemitem = "***";
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(th506);
			itempf.setItemitem(keyItemitem);
			itempf = itemDAO.getItempfRecord(itempf);
		}
		if (itempf != null) {
			th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			th506rec.th506Rec.set(SPACES);
		}
		
		/*String keyItemitem = chdrpf.getCnttype().toString();			
		List<Itempf> itempfList ;			
		if (!th506ListMap.containsKey(keyItemitem)) {			
			keyItemitem = "***";				
		} 	
		if (th506ListMap.containsKey(keyItemitem)) {			
			itempfList = th506ListMap.get(keyItemitem);	
			Iterator<Itempf> iterator = itempfList.iterator();	
			if (iterator.hasNext()) {	
				Itempf itempf = new Itempf();	
				itempf = iterator.next();	
				th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
			}				
		}
		 else {			
			th506rec.th506Rec.set(SPACES);				
		}*/
 //ILIFE-8723 end
		
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
//ILB-499 start
private static final class ErrorsInner { 
		 //ERRORS 
	/*private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData g436 = new FixedLengthStringData(4).init("G436");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData e331 = new FixedLengthStringData(4).init("E331");
	private FixedLengthStringData e370 = new FixedLengthStringData(4).init("E370");
	private FixedLengthStringData e268 = new FixedLengthStringData(4).init("E268");
	private FixedLengthStringData rl31 = new FixedLengthStringData(4).init("RL31");
	private FixedLengthStringData f779 = new FixedLengthStringData(4).init("F779");
	private FixedLengthStringData h791 = new FixedLengthStringData(4).init("H791");
	private FixedLengthStringData rl32 = new FixedLengthStringData(4).init("RL32");
	private FixedLengthStringData e492 = new FixedLengthStringData(4).init("E492");
	private FixedLengthStringData rl35 = new FixedLengthStringData(4).init("RL35");
	private FixedLengthStringData rlcb = new FixedLengthStringData(4).init("RLCB"); //ICIL-297
	private FixedLengthStringData rlaj = new FixedLengthStringData(4).init("RLAJ"); //ICIL-297
	private FixedLengthStringData rrgi = new FixedLengthStringData(4).init("RRGI"); //ICIL-297
	private FixedLengthStringData rrj5 = new FixedLengthStringData(4).init("RRJ5");// fwang3 ICIL-559*/
	
	}
//ILB-499 end

protected void validateContractModification(){
	
}
}
