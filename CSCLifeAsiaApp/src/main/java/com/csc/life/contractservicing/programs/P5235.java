/*
 * File: P5235.java
 * Date: 30 August 2009 0:21:23
 * Author: Quipoz Limited
 * 
 * Class transformed from P5235.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.AnntmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.screens.S5235ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* OVERVIEW
* This program is part of the 9405 Annuities Development.  It is
* called  from the existing  Component  Change  transaction if a
* user is adding, modifying or enquiring on an Annuity  coverage
* on a contract.  The processing  for this  program is initiated
* by a selection field on  the  Component  Change Screen, P5154.
*
* This program allows the user to create, modify or enquire upon
* Annuity Details data.  All Annuity Detail fields are amendable
* when not  enquiring on the data and the screen is  redisplayed
* until it is error-free or the user exits from the transaction.
*
* When adding an Annuity  coverage, the fields  default from the
* values set up on the Annuity Component Edit Rules Table T6625.
* Once an ANNT  record  has  been  created, the  information  to
* display on the screen is retrieved from this file.
*
* When  amending an Annuity  Coverage, the ANNT  file is read to
* and the details are  displayed  on  screen if  they exist.  If
* none  are  found,  check the ANNY and  break out the record if
* necessary.
*
* If a client number is entered in the nominated life  field  it
* must be the client number of either the life or the joint life
* for the component.
*
*****************************************************************
* </pre>
*/
public class P5235 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5235");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaBreakout = "";

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator addComp = new Validator(wsaaFlag, "A");
	private Validator modifyComp = new Validator(wsaaFlag, "M");
	private Validator compEnquiry = new Validator(wsaaFlag, "I");
	private Validator compApp = new Validator(wsaaFlag, "S", "P");
	private Validator addApp = new Validator(wsaaFlag, "P");
	private Validator revComp = new Validator(wsaaFlag, "R");
		/* ERRORS */
	private String e925 = "E925";
	private String f045 = "F045";
	private String f046 = "F046";
	private String f047 = "F047";
	private String f048 = "F048";
	private String f049 = "F049";
	private String f064 = "F064";
	private String f090 = "F090";
	private String h347 = "H347";
	private String h147 = "H147";
		/* TABLES */
	private String t6625 = "T6625";
		/* FORMATS */
	private String annyrec = "ANNYREC";
	private String anntmjarec = "ANNTMJAREC";
	private String cltsrec = "CLTSREC";
	private String chdrmjarec = "CHDRMJAREC";
	private String lifelnbrec = "LIFELNBREC";
	private String itdmrec = "ITEMREC";
		/*Annuities Temporary Details Major Alts*/
	private AnntmjaTableDAM anntmjaIO = new AnntmjaTableDAM();
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private T6625rec t6625rec = new T6625rec();
	private Wssplife wssplife = new Wssplife();
	private S5235ScreenVars sv = ScreenProgram.getScreenVars( S5235ScreenVars.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		preExit
	}

	public P5235() {
		super();
		screenVars = sv;
		new ScreenModel("S5235", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaFlag.set(wsspcomn.flag);
		wsaaBreakout = "N";
		sv.dataArea.set(SPACES);
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
				else {
					covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		if (!compEnquiry.isTrue()) {
			covtmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covtmjaIO);
			if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covtmjaIO.getParams());
				fatalError600();
			}
		}
		if (modifyComp.isTrue()
		|| compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			chdrmjaIO.setChdrcoy(covrpf.getChdrcoy());
			chdrmjaIO.setChdrnum(covrpf.getChdrnum());
			chdrmjaIO.setFormat(chdrmjarec);
			chdrmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
		}
		sv.chdrnum.set(covrpf.getChdrnum());
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrnum(covrpf.getChdrnum());
		lifelnbIO.setChdrcoy(covrpf.getChdrcoy());
		lifelnbIO.setLife(covrpf.getLife());
		lifelnbIO.setJlife(ZERO);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrnum(covrpf.getChdrnum());
		lifelnbIO.setChdrcoy(covrpf.getChdrcoy());
		lifelnbIO.setLife(covrpf.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
		}
		else {
			sv.jlifcnum.set(lifelnbIO.getLifcnum());
			cltsIO.setFunction(varcom.readr);
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
			cltsIO.setFormat(cltsrec);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
		if (addComp.isTrue()) {
			readAnntmja1100();
			goTo(GotoLabel.exit1090);
		}
		if (compEnquiry.isTrue()) {
			if (isNE(covrpf.getPlanSuffix(),0)
			|| isGT(covrpf.getPlanSuffix(),chdrmjaIO.getPolsum())) {
				wsaaBreakout = "Y";
				annyIO.setPlanSuffix(ZERO);
			}
			else {
				annyIO.setPlanSuffix(covrpf.getPlanSuffix());
			}
			readAnny1200();
			goTo(GotoLabel.exit1090);
		}
		readAnntmja1100();
		if (isEQ(anntmjaIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit1090);
		}
		if (isNE(covrpf.getPlanSuffix(),0)
		|| isGT(covrpf.getPlanSuffix(),chdrmjaIO.getPolsum())) {
			wsaaBreakout = "Y";
			annyIO.setPlanSuffix(ZERO);
			readAnny1200();
		}
		else {
			annyIO.setPlanSuffix(covrpf.getPlanSuffix());
			readAnny1200();
		}
	}

protected void readAnntmja1100()
	{
		read1110();
	}

protected void read1110()
	{
		anntmjaIO.setFunction(varcom.readr);
		anntmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		anntmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		anntmjaIO.setLife(covtmjaIO.getLife());
		anntmjaIO.setCoverage(covtmjaIO.getCoverage());
		anntmjaIO.setRider(covtmjaIO.getRider());
		anntmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		anntmjaIO.setFormat(anntmjarec);
		SmartFileCode.execute(appVars, anntmjaIO);
		if (isNE(anntmjaIO.getStatuz(),varcom.oK)
		&& isNE(anntmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(anntmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(anntmjaIO.getStatuz(),varcom.mrnf)) {
			if (addComp.isTrue()) {
				t6625Read1300();
				sv.freqann.set(t6625rec.freqann);
				sv.guarperd.set(t6625rec.guarperd);
				sv.dthpercn.set(t6625rec.dthpercn);
				sv.dthperco.set(t6625rec.dthperco);
				sv.arrears.set(SPACES);
				sv.advance.set(SPACES);
				sv.ppind.set(SPACES);
				sv.withprop.set(SPACES);
				sv.withoprop.set(SPACES);
				sv.nomlife.set(SPACES);
				sv.intanny.set(ZERO);
				sv.capcont.set(ZERO);
			}
		}
		else {
			sv.freqann.set(anntmjaIO.getFreqann());
			sv.guarperd.set(anntmjaIO.getGuarperd());
			sv.dthpercn.set(anntmjaIO.getDthpercn());
			sv.dthperco.set(anntmjaIO.getDthperco());
			sv.arrears.set(anntmjaIO.getArrears());
			sv.advance.set(anntmjaIO.getAdvance());
			sv.ppind.set(anntmjaIO.getPpind());
			sv.withprop.set(anntmjaIO.getWithprop());
			sv.withoprop.set(anntmjaIO.getWithoprop());
			sv.nomlife.set(anntmjaIO.getNomlife());
			sv.intanny.set(anntmjaIO.getIntanny());
			sv.capcont.set(anntmjaIO.getCapcont());
		}
	}

protected void readAnny1200()
	{
		read1210();
	}

protected void read1210()
	{
		annyIO.setFunction(varcom.readr);
		annyIO.setChdrcoy(covrpf.getChdrcoy());
		annyIO.setChdrnum(covrpf.getChdrnum());
		annyIO.setLife(covrpf.getLife());
		annyIO.setCoverage(covrpf.getCoverage());
		annyIO.setRider(covrpf.getRider());
		annyIO.setFormat(annyrec);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
		sv.freqann.set(annyIO.getFreqann());
		sv.guarperd.set(annyIO.getGuarperd());
		sv.dthpercn.set(annyIO.getDthpercn());
		sv.dthperco.set(annyIO.getDthperco());
		sv.arrears.set(annyIO.getArrears());
		sv.advance.set(annyIO.getAdvance());
		sv.ppind.set(annyIO.getPpind());
		sv.withprop.set(annyIO.getWithprop());
		sv.withoprop.set(annyIO.getWithoprop());
		sv.nomlife.set(annyIO.getNomlife());
		sv.intanny.set(annyIO.getIntanny());
		if (isEQ(wsaaBreakout,"N")) {
			sv.capcont.set(annyIO.getCapcont());
		}
		else {
			compute(sv.capcont, 3).setRounded(div(annyIO.getCapcont(),chdrmjaIO.getPolsum()));
		}
	}

protected void t6625Read1300()
	{
		t6625Start1310();
	}

protected void t6625Start1310()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(wssplife.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(covrpf.getCrtable(),itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covrpf.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h147);
			fatalError600();
		}
		else {
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (compEnquiry.isTrue()
		|| compApp.isTrue()) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (!compEnquiry.isTrue()) {
			validateReqd2100();
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateReqd2100()
	{
		valStart2110();
	}

protected void valStart2110()
	{
		if (isEQ(sv.freqann,SPACES)) {
			sv.freqannErr.set(e925);
		}
		if (isEQ(sv.advance,SPACES)
		&& isEQ(sv.arrears,SPACES)) {
			sv.arrearsErr.set(f045);
			sv.advanceErr.set(f045);
		}
		else {
			if (isNE(sv.advance,SPACES)
			&& isNE(sv.arrears,SPACES)) {
				sv.arrearsErr.set(f047);
				sv.advanceErr.set(f047);
			}
		}
		if (isEQ(sv.withprop,SPACES)
		&& isEQ(sv.withoprop,SPACES)) {
			sv.withpropErr.set(f046);
			sv.withopropErr.set(f046);
		}
		else {
			if (isNE(sv.withprop,SPACES)
			&& isNE(sv.withoprop,SPACES)) {
				sv.withpropErr.set(f047);
				sv.withopropErr.set(f047);
			}
		}
		if (isNE(sv.advance,SPACES)
		&& isNE(sv.withprop,SPACES)) {
			sv.advanceErr.set(f048);
			sv.withpropErr.set(f048);
		}
		if (isGT(sv.capcont,wssplife.bigAmt)) {
			sv.capcontErr.set(f090);
		}
		if (isNE(sv.nomlife,SPACES)) {
			begnLifelnb2200();
		}
		if (isGT(sv.dthpercn,100)
		|| isGT(sv.dthperco,100)) {
			if (isGT(sv.dthpercn,100)) {
				sv.dthpercnErr.set(h347);
			}
			else {
				if (isGT(sv.dthperco,100)) {
					sv.dthpercoErr.set(h347);
				}
			}
		}
		if ((isGT(sv.dthpercn,0)
		|| isGT(sv.dthperco,0))
		&& isEQ(sv.nomlife,SPACES)) {
			sv.nomlifeErr.set(f064);
		}
	}

protected void begnLifelnb2200()
	{
		begnStart2210();
	}

protected void begnStart2210()
	{
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		lifelnbIO.setChdrcoy(covrpf.getChdrcoy());
		lifelnbIO.setChdrnum(covrpf.getChdrnum());
		lifelnbIO.setLife(covrpf.getLife());
		lifelnbIO.setJlife(ZERO);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		while ( !(isEQ(lifelnbIO.getLifcnum(),sv.nomlife)
		|| isEQ(lifelnbIO.getStatuz(),varcom.endp)
		|| isNE(lifelnbIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(lifelnbIO.getLife(),covrpf.getLife()))) {
			nextrLifelnb2300();
		}
		
		if (isEQ(lifelnbIO.getStatuz(),varcom.endp)
		|| isNE(lifelnbIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(lifelnbIO.getLife(),covrpf.getLife())) {
			sv.nomlifeErr.set(f049);
		}
	}

protected void nextrLifelnb2300()
	{
		/*NEXTR-START*/
		lifelnbIO.setFunction(varcom.nextr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (!compEnquiry.isTrue()) {
			updateReqd3100();
		}
		/*EXIT*/
	}

protected void updateReqd3100()
	{
		updateStart3110();
	}

protected void updateStart3110()
	{
		anntmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		anntmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		anntmjaIO.setLife(covtmjaIO.getLife());
		anntmjaIO.setCoverage(covtmjaIO.getCoverage());
		anntmjaIO.setRider(covtmjaIO.getRider());
		anntmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		anntmjaIO.setSeqnbr(covtmjaIO.getSeqnbr());
		anntmjaIO.setFreqann(sv.freqann);
		anntmjaIO.setGuarperd(sv.guarperd);
		anntmjaIO.setDthpercn(sv.dthpercn);
		anntmjaIO.setDthperco(sv.dthperco);
		anntmjaIO.setArrears(sv.arrears);
		anntmjaIO.setAdvance(sv.advance);
		anntmjaIO.setPpind(sv.ppind);
		anntmjaIO.setWithprop(sv.withprop);
		anntmjaIO.setWithoprop(sv.withoprop);
		anntmjaIO.setNomlife(sv.nomlife);
		anntmjaIO.setIntanny(sv.intanny);
		anntmjaIO.setCapcont(sv.capcont);
		anntmjaIO.setFunction(varcom.updat);
		anntmjaIO.setFormat(anntmjarec);
		SmartFileCode.execute(appVars, anntmjaIO);
		if (isNE(anntmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(anntmjaIO.getParams());
			syserrrec.statuz.set(anntmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
