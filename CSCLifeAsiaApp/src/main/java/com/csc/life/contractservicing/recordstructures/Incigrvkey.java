package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:56
 * Description:
 * Copybook name: INCIGRVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incigrvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incigrvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incigrvKey = new FixedLengthStringData(64).isAPartOf(incigrvFileKey, 0, REDEFINE);
  	public FixedLengthStringData incigrvChdrcoy = new FixedLengthStringData(1).isAPartOf(incigrvKey, 0);
  	public FixedLengthStringData incigrvChdrnum = new FixedLengthStringData(8).isAPartOf(incigrvKey, 1);
  	public FixedLengthStringData incigrvLife = new FixedLengthStringData(2).isAPartOf(incigrvKey, 9);
  	public FixedLengthStringData incigrvCoverage = new FixedLengthStringData(2).isAPartOf(incigrvKey, 11);
  	public FixedLengthStringData incigrvRider = new FixedLengthStringData(2).isAPartOf(incigrvKey, 13);
  	public PackedDecimalData incigrvPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incigrvKey, 15);
  	public PackedDecimalData incigrvInciNum = new PackedDecimalData(3, 0).isAPartOf(incigrvKey, 18);
  	public PackedDecimalData incigrvSeqno = new PackedDecimalData(2, 0).isAPartOf(incigrvKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(incigrvKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incigrvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incigrvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}