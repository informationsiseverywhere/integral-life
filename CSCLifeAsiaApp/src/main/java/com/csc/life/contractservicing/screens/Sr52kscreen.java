package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr52kscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52kScreenVars sv = (Sr52kScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52kscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52kScreenVars screenVars = (Sr52kScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.ratebas.setClassString("");
		screenVars.descr01.setClassString("");
		screenVars.descr02.setClassString("");
		screenVars.effdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr52kscreen
 */
	public static void clear(VarModel pv) {
		Sr52kScreenVars screenVars = (Sr52kScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.descrip.clear();
		screenVars.chdrnum.clear();
		screenVars.rstate.clear();
		screenVars.cnttype.clear();
		screenVars.cownnum.clear();
		screenVars.payrnum.clear();
		screenVars.agntnum.clear();
		screenVars.billfreq.clear();
		screenVars.ctypdesc.clear();
		screenVars.pstate.clear();
		screenVars.ownername.clear();
		screenVars.payorname.clear();
		screenVars.agentname.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.mop.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.ratebas.clear();
		screenVars.descr01.clear();
		screenVars.descr02.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
	}
}
