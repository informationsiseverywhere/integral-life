/*
 * File: Loanpymt.java
 * Date: 29 August 2009 22:58:40
 * Author: Quipoz Limited
 * 
 * Class transformed from LOANPYMT.CBL
 * 
 * Copyright (2007) CSC Asia, a	ll rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;


import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;

import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Addacmv;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Addacmvrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Rldgkey;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.contractservicing.recordstructures.Lnrepayrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;

import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              LOAN PAYMENTS SUBROUTINE.
*              =========================
*
* Overview.
* ---------
*
* This routine is called from within the Cash receipts subsystem
*  and from the various Claims subsystems.
* Its function is to post cash received against any Loan(s) the
*  contract may have.
* It may pay off Loan principal or Loan interest - it all depends
*  on what Sub-account Code & Type are passed in - thus sites can
*  dicate whether Loan principal or interest should be paid-off
*  first/if at all.
* IF the whole balance of an ACBL is paid-off, then the program
*  will check whether the Loan has any debt left outstanding on
*  it - ie check whether the Principal and Interest ACBLs are
*  both zero, and if so, the Loan is deemed to be Paid off and so
*  the program will V/Flag '2' the Loan record
* It will try and post as much of the Amount that is passed in as
* possible - depending on the Function specified, it may post any
* residual cash to Suspense or it may just return the Residual
* amount to the calling program.
*
* Given:     The CASHEDREC copybook, but we will only use the
*             following fields...
*              CSHD-SACSCODE, CSHD-SACSTYP, CSHD-ORIGAMT,
*              CSHD-ORIGCCY, CSHD-GENLKEY, CSHD-FUNCTION,
*              CSHD-CHDRNUM, CSHD-CHDRCOY, CSHD-SIGN.
*
*
* Gives:     CSHD-STATUZ, CSHD-DOCAMT ( this is the
*                                            residual ....
*                                            the cash left over )
*
* Processing.
* -----------
*
* Do a BEGN on the ACBLCLM file using the Company,
*  Contract number, Sub-account Code and Sub-account type
*  specified in the linkage area
*
*
*    IF CSHD-ORIGAMT  >=  ACBLCLM balance,
*       Write ACMV record for ACBLCLM balance using ACBLCLM key
*       Check whether Loan has been totally paid-off, and if so,
*         Update the LOAN record and Validflag '2' it
*       Subtract ACBLCLM balance from CSHD-ORIGAMT
*
*    ELSE
*       Write ACMV record for amount using key from ACBLCLM
*       set Residual amount to Zero
*       EXIT subroutine
*    END IF
*
* Read Next ACBLCLM and process as above.
*
*    IF no more ACBLCLM records and CSHD-AMOUNT still > Zero
*
*        IF CSHD-FUNCTION =  UPDAT
*           Post Amount left ( ie Residual ) to Suspense account
*
*        ELSE
*           Set Residual = Amount left
*           EXIT subroutine
*        END IF
*    END IF
*
*********
* NOTE !!
*********
*      This program uses T5645 and therefore expects to find
*       entries for the following....
*
*     entry No 1 .... Cash Suspense
*
*     entries 2 & 3   Loan Principal & Loan interest for Policy
*                      Loans
*
*     entries 4 & 5   Loan Principal & Loan interest for
*                      Non-forfeiture APLs ( Automatic Policy
*                                                      Loans )
*
*
*****************************************************************
* </pre>
*/
public class LoanRepay extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "LOANPYMT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlPrefix = new FixedLengthStringData(2).isAPartOf(wsaaGenlkey, 0);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);

	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private FixedLengthStringData wsaaTurnOffLoan = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaAcblSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcblSacstype = new FixedLengthStringData(2);

	
	private PackedDecimalData wsaaResidual = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPostAmount = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3);
	private static final int wsaaMaxDate = 99999999;
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(4, 0).setUnsigned();
	private String wsaaSkip = "N";
	private PackedDecimalData wsaaCount = new PackedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();

		/* WSAA-KEYS */
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
		/* FORMATS */
	private static final String acblrec = "ACBLREC   ";
	private static final String acblclmrec = "ACBLCLMREC";
	private static final String itemrec = "ITEMREC   ";
	private static final String loanrec = "LOANREC   ";
	private static final String chdrlifrec = "CHDRLIFREC";
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t5645 = "T5645";
		/* ERRORS */
	private static final String g418 = "G418";
	private Batckey wsaaBatckey = new Batckey();
	private Rdockey wsaaRdockey = new Rdockey();
	private Rldgkey wsaaRldgkey = new Rldgkey();
	private Varcom varcom = new Varcom();
	private Addacmvrec addacmvrec = new Addacmvrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T3629rec t3629rec = new T3629rec();
	private T5645rec t5645rec = new T5645rec();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private LoanpfDAO loanpfDAO = getApplicationContext().getBean("loanpfDAO", LoanpfDAO.class);
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Lnrepayrec lnrepayrec = new Lnrepayrec();
	private Acblpf acblpf = null;
	private Acblpf acblpf1 = null;
	private Loanpf loanpf = null;
	private List<Loanpf> loanpfList = new ArrayList<>();
	private Itempf itempf= null;
	List<Loanpf> loanUpdList = new ArrayList<>();
	List<Loanpf> loanInsList = new ArrayList<>();
	
	
/**
 * Contains all possible labels used by goTo action.
 */


	public LoanRepay() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lnrepayrec.lnrepayRec = convertAndSetParam(lnrepayrec.lnrepayRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
			start100();
			exit190();
		}

protected void start100()
	{
		initialise1000();
		if (isEQ(wsaaSkip,"Y")) {
			return ;
		}
		mainProcessing2000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		lnrepayrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaSkip = "N";
		wsaaGenlkey.set(SPACES);
		wsaaCurrency.set(SPACES);
		wsaaRldgacct.set(SPACES);
		
		wsaaAcblSacscode.set(SPACES);
		wsaaAcblSacstype.set(SPACES);
		wsaaTurnOffLoan.set(SPACES);
		wsaaAmount.set(ZERO);
		wsaaPostAmount.set(ZERO);
		wsaaAmount.set(lnrepayrec.origamt);
		wsaaCurrency.set(lnrepayrec.origccy);
		wsaaRdockey.rdocKey.set(lnrepayrec.doctkey);
		varcom.vrcmTranid.set(lnrepayrec.tranid);
		wsaaBatckey.batcKey.set(lnrepayrec.batchkey);
		wsaaRldgkey.rldgKey.set(lnrepayrec.trankey);
		wsaaGenlkey.set(lnrepayrec.genlkey);
		wsaaSequenceNo.set(ZERO);
		wsaaSequenceNo.set(lnrepayrec.transeq);
		wsaaCount.set(ZERO);
		wsaaNominalRate.set(ZERO);
		wsaaLedgerCcy.set(SPACES);
		/* read T5645 for use later on, whether for posting back to*/
		/*  suspense, or to check whether LOANs have been paid off or not*/
		readT56457000();
		
	}

protected void mainProcessing2000()
	{
		start2000();
	}

protected void start2000()
	{
		
		/* Read ACBL for suspense balance.*/
		acblpf1 = acblpfDAO.getAcblpfRecord(wsaaRldgkey.rldgRldgcoy.toString(), lnrepayrec.sacscode.toString(), wsaaRldgkey.rldgRldgacct.toString(), lnrepayrec.sacstyp.toString(), "");
		
		
		
		if (acblpf1 == null) {
			return ;
		}
		
		
		
		/* Check that the ACBL balance is not Zero*/
		if (isEQ(acblpf1.getSacscurbal(),ZERO)) {			
			lnrepayrec.repaidamt.set(ZERO);
			return ;
		}
		/* Negate the Account balances if it is negative (for the       */
		/* case of Cash accounts).                                      */
		if (isLT(acblpf1.getSacscurbal(),0)) {
			setPrecision(acblpf1.getSacscurbal(), 2);
			acblpf1.setSacscurbal(mult(acblpf1.getSacscurbal(),(-1)).getbigdata());
		}
		/* Get here so the ACBL we have read is OK*/
		/*  check that ACBL currency matches the currency of the amount*/
		/*  passed in in the CSHD-ORIGCCY field - if not, we must convert*/
		/*  the passed in amount, CSHD-ORIGCCY to the ACBL currency.*/
		if (isNE(wsaaCurrency,acblpf1.getOrigcurr())) {
			/* When currency conversion used posting should be done*/
			/* before and after.*/
			lifacmvrec.lifacmvRec.set(SPACES);
			lifacmvrec.origamt.set(wsaaAmount);
			lifacmvrec.origcurr.set(wsaaCurrency);
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			acmvPosting();
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.currOut.set(acblpf1.getOrigcurr());
			convertCurrency4000();
			wsaaCurrency.set(acblpf1.getOrigcurr());
			wsaaAmount.set(conlinkrec.amountOut);
			lifacmvrec.lifacmvRec.set(SPACES);
			lifacmvrec.origamt.set(wsaaAmount);
			lifacmvrec.origcurr.set(wsaaCurrency);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			acmvPosting();
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		
		lifacmvrec.origcurr.set(wsaaCurrency);
		lifacmvrec.sacscode.set(lnrepayrec.sacscode);
		lifacmvrec.sacstyp.set(lnrepayrec.sacstyp);
		lifacmvrec.glcode.set(lnrepayrec.genlAccount);
		lifacmvrec.glsign.set(lnrepayrec.sign);
		/* Now values are in correct currencies so lets do our sums*/
		if (isGT(wsaaAmount,acblpf1.getSacscurbal())
		|| isEQ(wsaaAmount,acblpf1.getSacscurbal())) {
			wsaaPostAmount.set(acblpf1.getSacscurbal());
			lifacmvrec.origamt.set(wsaaPostAmount);
			acmvPosting();			
			compute(wsaaAmount, 2).set(sub(wsaaAmount,acblpf1.getSacscurbal()));			
		}
		else {
			wsaaPostAmount.set(wsaaAmount);			
			lifacmvrec.origamt.set(wsaaPostAmount);
			acmvPosting();
			wsaaAmount.set(ZERO);			
		}
		lnrepayrec.repaidamt.set(wsaaPostAmount);
		/* we have paid off the whole of the ACBL balance, so we will go*/
		/*  and see whether the Loan is still active or whether all debts*/
		/*  have now been cleared*/
		updateLoan();
		lnrepayrec.docamt.set(wsaaAmount);
		if(isGT(wsaaAmount, 0) && isNE(lnrepayrec.origccy,acblpf1.getOrigcurr())) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.currOut.set(lnrepayrec.origccy);
			convertCurrency4000();
			lnrepayrec.docamt.set(conlinkrec.amountOut);
		}
		/* Return the Jrnseq count from the ACMV reocrds written to the*/
		/*  calling program.*/
		lnrepayrec.transeq.set(wsaaSequenceNo);
	}

protected void convertCurrency4000()
	{
		
		/* call the convert subroutine*/
		conlinkrec.amountIn.set(wsaaAmount);
		conlinkrec.statuz.set(SPACES);
		conlinkrec.function.set("REAL");
		conlinkrec.currIn.set(wsaaCurrency);
		conlinkrec.cashdate.set(wsaaMaxDate);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(lnrepayrec.chdrcoy);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			systemError99000();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}




protected void readT56457000()
	{		
		/* we are going to post residual cash to suspense, so read T5645*/
		/*  to get Subaccount code & type etc...*/
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(lnrepayrec.chdrcoy.toString());
	itempf.setItemtabl(t5645);
	itempf.setItemitem("LOANPYMT");
	itempf = itemDao.getItemRecordByItemkey(itempf);
	if (itempf == null) {
		syserrrec.params.set("IT".concat(addacmvrec.batccoy.toString()).concat(t3629)
				.concat(wsaaCurrency.toString()));
		systemError99000();
	}
	
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void updateLoan()
	{			
		/* Read LOAN record*/
		
		loanpfList = loanpfDAO.loadDataByLoanNum(wsaaRldgkey.rldgRldgcoy.toString(), lnrepayrec.chdrnum.toString().trim(), lnrepayrec.loanNo.toString().trim());
		if(loanpfList == null) {
			return ;
		}
		/* get here so we have read & locked the LOAN record*/
		/* we need to get the Loan type so that we can get the correct*/
		/*  entries from T5645.*/
		/* we know that when we get here we have just zeroised one of the*/
		/*  2 subaccount types that is associated with a Loan, whether it*/
		/*  be a Policy loan or an APL - so we need to check which one we*/
		/*  have just zeroised, and then read the ACBL balance for the*/
		/*  other one and it that one is also zero, then the Loan has been*/
		/*  paid off and we can Validflag '2' the Loan record.*/
		for(Loanpf loanpf : loanpfList){
			if(isEQ(loanpf.getValidflag(),'1'))
				if ((isEQ(acblpf1.getSacscode(),t5645rec.sacscode02)
				&& isEQ(acblpf1.getSacstyp(),t5645rec.sacstype02)) || 
				(isEQ(acblpf1.getSacscode(),t5645rec.sacscode04)
				&& isEQ(acblpf1.getSacstyp(),t5645rec.sacstype04))) 
				{	
					
					loanpf.setLtranno(lnrepayrec.tranno.toLong());
					loanUpdList.add(loanpf);
					loanpfDAO.rewrtLoanRecords(loanUpdList, 0);
					loanpf.setLstcaplamt(sub(loanpf.getLstcaplamt(),wsaaPostAmount.getbigdata()).toDouble());
					loanpf.setLtranno(0);
					loanpf.setFtranno(lnrepayrec.tranno.toLong());
					loanInsList.add(loanpf);
					loanpfDAO.insertLoanRec(loanInsList); 
				}	
			
		}
		
	}

protected void checkAccBalance10000()
	{
			/* Read ACBL file with given parameters*/
		acblpf = acblpfDAO.loadDataByBatch(acblpf1.getRldgcoy(), wsaaAcblSacscode.toString(),
				wsaaRldgkey.rldgRldgacct.toString(), acblpf1.getOrigcurr(), wsaaAcblSacstype.toString());/* IJTI-1523 */
		if (acblpf == null) {
			wsaaTurnOffLoan.set("Y");
			return ;
		} else {
				if (isEQ(acblpf.getSacscurbal(),ZERO)) {
						wsaaTurnOffLoan.set("Y");
				} else {
						wsaaTurnOffLoan.set("N");
				}
		}
	}
protected void acmvPosting()
	{
		lifacmvrec.contot.set(0);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);		
		lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.rdocnum.set(lnrepayrec.chdrnum);
		lifacmvrec.rldgcoy.set(wsaaRldgkey.rldgRldgcoy);
		lifacmvrec.rldgacct.set(wsaaRldgkey.rldgRldgacct);
		lifacmvrec.genlcoy.set(wsaaGlCompany);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.effdate.set(lnrepayrec.trandate);
		lifacmvrec.trandesc.set(lnrepayrec.trandesc);
		lifacmvrec.tranref.set(lnrepayrec.chdrnum);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.tranno.set(lnrepayrec.tranno);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(lnrepayrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(acblpf1.getOrigcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError99000();
		}
		/*A900-EXIT*/
	}

protected void systemError99000()
	{
			start99000();
			exit99490();
		}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		lnrepayrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
			start99500();
			exit99590();
		}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		lnrepayrec.statuz.set(varcom.bomb);
		exit190();
	}
}
