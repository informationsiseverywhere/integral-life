/*
 * File: Zrulrvalc.java
 * Date: 30 August 2009 2:57:20
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRULRVALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.life.contractservicing.dataaccess.IncigrvTableDAM;
import com.csc.life.contractservicing.dataaccess.ZrraTableDAM;
import com.csc.life.contractservicing.dataaccess.ZrutnrvTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.Hitrrv1TableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*   Generalised Reversal Subroutine for UTRNs, HITRs & INCIs
*   --------------------------------------------------------
*
*   This routine performs the same function as GREVUNIT except that
*   it reverses UTRNs, HITRs & INCIs at component level.
*
*   This subroutine is normally called by REVCOLL reversal subroutine
*   via Table T5671. The parameters passed to this subroutine are
*   contained in the GREVERSREC copybook.
*
*   A new file has been introduced, ZRRA, to retain the monies date
*   of the reversed component. This will be used in conjunction with
*   the allocation routine, (ZRULALOC), to ensure that if this
*   component is put forward again, the UTRN will use the same date
*   to select the allocation prices.
*
*   UTRN, HITR and INCI records are reversed by this process.
*
*   To reverse the correct UTRN & HITR records for a component the
*   original processing sequence number is retrieved from T6647 &
*   to this the rider number is added. This method ensures the
*   correct UTRN's & HITR's are located when a unit holding Rider
*   is attached to the main Coverage.
*
*   To aid retrieving the correct UTRN's and HITR's, new logical
*   files ZRUTNRV and HITRRV1 are used respectively to total the
*   value of premiums to be reversed.
*
*   Following this all the active INCI records for the component
*   are read and the premiums are totalled.
*
****If the total of UTRN and HITR does not equal the INCI total,
****the program abends.
*
*   If the UTRN and HITR total does not equal a whole multiple of the
*   INCI value the program abends.
*
*   If the totals match then the INCI records, UTRN records and
*   HITR records are reversed as follows:
*
*   INCI records.
*
*   The value of the current premium (CURR-PREM) for each INCI record
*   processed is added to the current amount of premium required for the
*   most recent period able to absorb the premium (CURR-PREM0n).
*   If only part of the premium can be absorbed by a given period, the
*   remainder is applied to the next most recent period.
*
*   If any premium is outstanding at the end of this process, the program
*   ends with an error.
*
*   UTRN records.
*
*   The processing for UTRN records depends on whether they have been
*   processed by NEWUNITD or not.
*
*   If the Feedback Indicator is set, a new negative copy of the UTRN
*   is written, otherwise the UTRN record is deleted.
*
*   HITR records.
*
*   The processing for HITR records depends on whether they have been
*   processed by NEWUNITD or not.
*
*   If the Feedback Indicator is set, a new negative copy of the HITR
*   is written, otherwise the HITR record is deleted.
*
*****************************************************************
*
* </pre>
*/
public class Zrulrvalc extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zrulrvalc.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private static final String t6647 = "T6647";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String incigrvrec = "INCIGRVREC";
	private static final String zrutnrvrec = "ZRUTNRVREC";
	private static final String zrrarec = "ZRRAREC   ";
	private static final String hitrrv1rec = "HITRRV1REC";
		/* ERRORS */
	private static final String f018 = "F018";
	private static final String h965 = "H965";
	private static final String t743 = "T743";
	private static final String t744 = "T744";

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4);
		/* WSAA-RIDER */
	private FixedLengthStringData wsaaRiderAlpha = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaRiderNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiderAlpha, 0, REDEFINE).setUnsigned();
	private ZonedDecimalData wsaaTotalUtrnValue = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaTotalInciValue = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaTotalHitrValue = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaStoredValueToAdd = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaStoredDifference = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaSeqNo = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaTotalCompValue = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaNoOfPremiums = new ZonedDecimalData(11, 0).init(ZERO);
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(11, 0).init(ZERO);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Hitrrv1TableDAM hitrrv1IO = new Hitrrv1TableDAM();
	private IncigrvTableDAM incigrvIO = new IncigrvTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();
	private ZrraTableDAM zrraIO = new ZrraTableDAM();
	private ZrutnrvTableDAM zrutnrvIO = new ZrutnrvTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T6647rec t6647rec = new T6647rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Greversrec greversrec = new Greversrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		rewriteInci415
	}

	public Zrulrvalc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void control000()
	{
			start000();
			exit000();
		}

protected void start000()
	{
		getComponentDetails100();
		wsaaTotalUtrnValue.set(ZERO);
		wsaaTotalInciValue.set(ZERO);
		wsaaTotalHitrValue.set(ZERO);
		wsaaRemainder.set(ZERO);
		wsaaNoOfPremiums.set(ZERO);
		wsaaTotalCompValue.set(ZERO);
		getWsaaTotalUtrnValue200();
		getWsaaTotalInciValue300();
		getWsaaTotalHitrValue600();
		compute(wsaaTotalCompValue, 2).set(add(wsaaTotalUtrnValue,wsaaTotalHitrValue));
		compute(wsaaNoOfPremiums, 2).setDivide(wsaaTotalCompValue, (wsaaTotalInciValue));
		wsaaRemainder.setRemainder(wsaaNoOfPremiums);
		/* IF WSAA-TOTAL-UTRN-VALUE NOT = WSAA-TOTAL-INCI-VALUE <INTBR> */
		/*    IF WSAA-TOTAL-UTRN-VALUE     + WSAA-TOTAL-HITR-VALUE <INTBR> */
		/*                             NOT = WSAA-TOTAL-INCI-VALUE <INTBR> */
		if (isNE(wsaaRemainder,ZERO)) {
			syserrrec.statuz.set(t743);
			fatalError900();
		}
		if (isEQ(wsaaTotalUtrnValue,ZERO)
		&& isEQ(wsaaTotalHitrValue,ZERO)) {
			return ;
		}
		else {
			reverseInciPrems400();
			if (isNE(wsaaTotalUtrnValue, ZERO)) {
			reverseUtrns500();
			}
			if (isNE(wsaaTotalHitrValue, ZERO)) {
			reverseHitrs700();
		}
	}
	}

protected void exit000()
	{
		exitProgram();
	}

protected void getComponentDetails100()
	{
		start100();
	}

protected void start100()
	{
		/* Read Contract Header record.*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(greversrec.chdrcoy);
		chdrenqIO.setChdrnum(greversrec.chdrnum);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError900();
		}
		/* Read Policy Transaction history record.*/
		ptrnenqIO.setDataArea(SPACES);
		ptrnenqIO.setChdrcoy(greversrec.chdrcoy);
		ptrnenqIO.setChdrnum(greversrec.chdrnum);
		ptrnenqIO.setTranno(greversrec.tranno);
		ptrnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		SmartFileCode.execute(appVars, ptrnenqIO);
		if ((isNE(ptrnenqIO.getStatuz(),varcom.oK))
		&& (isNE(ptrnenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ptrnenqIO.getParams());
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			fatalError900();
		}
		if ((isNE(ptrnenqIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(ptrnenqIO.getChdrnum(),greversrec.chdrnum))
		|| (isNE(ptrnenqIO.getTranno(),greversrec.tranno))
		|| (isEQ(ptrnenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ptrnenqIO.getParams());
			syserrrec.statuz.set(f018);
			fatalError900();
		}
		/* Read T6647 to obtain original processing sequence number*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(greversrec.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaBatctrcde.set(ptrnenqIO.getBatctrcde());
		wsaaCnttype.set(chdrenqIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(ptrnenqIO.getPtrneff());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getItemcoy(),greversrec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(),t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(wsaaT6647Key);
			syserrrec.statuz.set(h965);
			fatalError900();
		}
		/* Determine the processing sequence number for unit transactions*/
		/* for this component.*/
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		wsaaRiderAlpha.set(greversrec.rider);
		compute(wsaaSeqNo, 0).set(add(wsaaRiderNum,t6647rec.procSeqNo));
		/* Set up ZRRA record, to retain the original monies date.*/
		/* The record is written in the 500 Section.*/
		zrraIO.setParams(SPACES);
		zrraIO.setChdrcoy(greversrec.chdrcoy);
		zrraIO.setChdrnum(greversrec.chdrnum);
		zrraIO.setLife(greversrec.life);
		zrraIO.setCoverage(greversrec.coverage);
		zrraIO.setRider(greversrec.rider);
		zrraIO.setPlanSuffix(greversrec.planSuffix);
		zrraIO.setTranno(greversrec.tranno);
		zrraIO.setBillfreq(chdrenqIO.getBillfreq());
		zrraIO.setInstfrom(greversrec.effdate);
		zrraIO.setBatctrcde(ptrnenqIO.getBatctrcde());
		zrraIO.setFormat(zrrarec);
		zrraIO.setFunction(varcom.writr);
	}

protected void getWsaaTotalUtrnValue200()
	{
		start200();
	}

protected void start200()
	{
		zrutnrvIO.setParams(SPACES);
		zrutnrvIO.setChdrcoy(greversrec.chdrcoy);
		zrutnrvIO.setChdrnum(greversrec.chdrnum);
		zrutnrvIO.setLife(greversrec.life);
		zrutnrvIO.setCoverage(greversrec.coverage);
		zrutnrvIO.setPlanSuffix(ZERO);
		zrutnrvIO.setTranno(greversrec.tranno);
		zrutnrvIO.setProcSeqNo(wsaaSeqNo);
		zrutnrvIO.setUnitVirtualFund(SPACES);
		zrutnrvIO.setUnitType(SPACES);
		zrutnrvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zrutnrvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zrutnrvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "TRANNO", "PRCSEQ");
		zrutnrvIO.setFormat(zrutnrvrec);
		SmartFileCode.execute(appVars, zrutnrvIO);
		if ((isNE(zrutnrvIO.getStatuz(),varcom.oK))
		&& (isNE(zrutnrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,zrutnrvIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,zrutnrvIO.getChdrnum()))
		|| (isNE(greversrec.life,zrutnrvIO.getLife()))
		|| (isNE(greversrec.coverage,zrutnrvIO.getCoverage()))
		|| (isNE(greversrec.tranno,zrutnrvIO.getTranno()))
		|| (isNE(wsaaSeqNo,zrutnrvIO.getProcSeqNo()))
		|| (isEQ(zrutnrvIO.getStatuz(),varcom.endp))) {
			zrutnrvIO.setStatuz(varcom.endp);
		}
		else {
			while ( !(isEQ(zrutnrvIO.getStatuz(),varcom.endp))) {
				addUpUtrnValue210();
			}
			
		}
	}

protected void addUpUtrnValue210()
	{
		/*START*/
	

	    if (isNE(zrutnrvIO.getInciprm02(),ZERO))
		   wsaaTotalUtrnValue.add(sub(zrutnrvIO.getContractAmount(),zrutnrvIO.getInciprm02()));
		else
			wsaaTotalUtrnValue.add( zrutnrvIO.getContractAmount());
			
			
		zrutnrvIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, zrutnrvIO);
		if ((isNE(zrutnrvIO.getStatuz(),varcom.oK))
		&& (isNE(zrutnrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,zrutnrvIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,zrutnrvIO.getChdrnum()))
		|| (isNE(greversrec.life,zrutnrvIO.getLife()))
		|| (isNE(greversrec.coverage,zrutnrvIO.getCoverage()))
		|| (isNE(greversrec.tranno,zrutnrvIO.getTranno()))
		|| (isNE(wsaaSeqNo,zrutnrvIO.getProcSeqNo()))
		|| (isEQ(zrutnrvIO.getStatuz(),varcom.endp))) {
			zrutnrvIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getWsaaTotalInciValue300()
	{
		start1200();
	}

protected void start1200()
	{
		incigrvIO.setDataArea(SPACES);
		incigrvIO.setChdrcoy(greversrec.chdrcoy);
		incigrvIO.setChdrnum(greversrec.chdrnum);
		incigrvIO.setLife(greversrec.life);
		incigrvIO.setCoverage(greversrec.coverage);
		incigrvIO.setRider(greversrec.rider);
		incigrvIO.setPlanSuffix(ZERO);
		incigrvIO.setInciNum(ZERO);
		incigrvIO.setSeqno(ZERO);
		incigrvIO.setFormat(incigrvrec);
		incigrvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incigrvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incigrvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, incigrvIO);
		if ((isNE(incigrvIO.getStatuz(),varcom.oK))
		&& (isNE(incigrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(incigrvIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(incigrvIO.getChdrnum(),greversrec.chdrnum))
		|| (isNE(incigrvIO.getLife(),greversrec.life))
		|| (isNE(incigrvIO.getCoverage(),greversrec.coverage))
		|| (isNE(incigrvIO.getRider(),greversrec.rider))
		|| (isEQ(incigrvIO.getStatuz(),varcom.endp))) {
			incigrvIO.setStatuz(varcom.endp);
		}
		else {
			while ( !(isEQ(incigrvIO.getStatuz(),varcom.endp))) {
				addUpInciValue310();
			}
			
		}
	}

protected void addUpInciValue310()
	{
		/*START*/
		wsaaTotalInciValue.add(incigrvIO.getCurrPrem());
		incigrvIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incigrvIO);
		if ((isNE(incigrvIO.getStatuz(),varcom.oK))
		&& (isNE(incigrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,incigrvIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,incigrvIO.getChdrnum()))
		|| (isNE(incigrvIO.getLife(),greversrec.life))
		|| (isNE(incigrvIO.getCoverage(),greversrec.coverage))
		|| (isNE(incigrvIO.getRider(),greversrec.rider))
		|| (isEQ(incigrvIO.getStatuz(),varcom.endp))) {
			incigrvIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void reverseInciPrems400()
	{
		start400();
	}

protected void start400()
	{
		incigrvIO.setDataArea(SPACES);
		incigrvIO.setChdrcoy(greversrec.chdrcoy);
		incigrvIO.setChdrnum(greversrec.chdrnum);
		incigrvIO.setLife(greversrec.life);
		incigrvIO.setCoverage(greversrec.coverage);
		incigrvIO.setRider(greversrec.rider);
		incigrvIO.setPlanSuffix(ZERO);
		incigrvIO.setInciNum(ZERO);
		incigrvIO.setSeqno(ZERO);
		/* MOVE BEGNH                  TO INCIGRV-FUNCTION.             */
		incigrvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incigrvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incigrvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		incigrvIO.setFormat(incigrvrec);
		SmartFileCode.execute(appVars, incigrvIO);
		if (isNE(incigrvIO.getStatuz(),varcom.oK)
		&& isNE(incigrvIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(incigrvIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(incigrvIO.getChdrnum(),greversrec.chdrnum))
		|| (isNE(incigrvIO.getLife(),greversrec.life))
		|| (isNE(incigrvIO.getCoverage(),greversrec.coverage))
		|| (isNE(incigrvIO.getRider(),greversrec.rider))
		|| (isEQ(incigrvIO.getStatuz(),varcom.endp))) {
			incigrvIO.setStatuz(varcom.endp);
		}
		else {
			while ( !(isEQ(incigrvIO.getStatuz(),varcom.endp))) {
				updateCurrPremFields410();
			}
			
		}
	}

protected void updateCurrPremFields410()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start410();
				case rewriteInci415: 
					rewriteInci415();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start410()
	{
		wsaaStoredValueToAdd.set(incigrvIO.getCurrPrem());
		if (isNE(incigrvIO.getPremCurr04(),incigrvIO.getPremStart04())) {
			compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart04(),incigrvIO.getPremCurr04()));
			if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
				setPrecision(incigrvIO.getPremCurr04(), 2);
				incigrvIO.setPremCurr04(add(incigrvIO.getPremCurr04(),wsaaStoredValueToAdd));
				wsaaStoredValueToAdd.set(ZERO);
				goTo(GotoLabel.rewriteInci415);
			}
			else {
				incigrvIO.setPremCurr04(incigrvIO.getPremStart04());
				wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
			}
		}
		if (isNE(incigrvIO.getPremCurr03(),incigrvIO.getPremStart03())) {
			compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart03(),incigrvIO.getPremCurr03()));
			if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
				setPrecision(incigrvIO.getPremCurr03(), 2);
				incigrvIO.setPremCurr03(add(incigrvIO.getPremCurr03(),wsaaStoredValueToAdd));
				wsaaStoredValueToAdd.set(ZERO);
				goTo(GotoLabel.rewriteInci415);
			}
			else {
				incigrvIO.setPremCurr03(incigrvIO.getPremStart03());
				wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
			}
		}
		if (isNE(incigrvIO.getPremCurr02(),incigrvIO.getPremStart02())) {
			compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart02(),incigrvIO.getPremCurr02()));
			if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
				setPrecision(incigrvIO.getPremCurr02(), 2);
				incigrvIO.setPremCurr02(add(incigrvIO.getPremCurr02(),wsaaStoredValueToAdd));
				wsaaStoredValueToAdd.set(ZERO);
				goTo(GotoLabel.rewriteInci415);
			}
			else {
				incigrvIO.setPremCurr02(incigrvIO.getPremStart02());
				wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
			}
		}
		if (isNE(incigrvIO.getPremCurr01(),incigrvIO.getPremStart01())) {
			compute(wsaaStoredDifference, 2).set(sub(incigrvIO.getPremStart01(),incigrvIO.getPremCurr01()));
			if (isGTE(wsaaStoredDifference,wsaaStoredValueToAdd)) {
				setPrecision(incigrvIO.getPremCurr01(), 2);
				incigrvIO.setPremCurr01(add(incigrvIO.getPremCurr01(),wsaaStoredValueToAdd));
				wsaaStoredValueToAdd.set(ZERO);
				goTo(GotoLabel.rewriteInci415);
			}
			else {
				incigrvIO.setPremCurr01(incigrvIO.getPremStart01());
				wsaaStoredValueToAdd.subtract(wsaaStoredDifference);
			}
		}
		if (isNE(wsaaStoredValueToAdd,ZERO)) {
			syserrrec.statuz.set(t744);
			fatalError900();
		}
	}

protected void rewriteInci415()
	{
		/* MOVE REWRT                  TO INCIGRV-FUNCTION.             */
		incigrvIO.setFunction(varcom.writd);
		incigrvIO.setTransactionDate(greversrec.transDate);
		incigrvIO.setTransactionTime(greversrec.transTime);
		incigrvIO.setUser(greversrec.user);
		incigrvIO.setTermid(greversrec.termid);
		SmartFileCode.execute(appVars, incigrvIO);
		if (isNE(incigrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		incigrvIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incigrvIO);
		if ((isNE(incigrvIO.getStatuz(),varcom.oK))
		&& (isNE(incigrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(incigrvIO.getParams());
			syserrrec.statuz.set(incigrvIO.getStatuz());
			fatalError900();
		}
		/* IF ((INCIGRV-CHDRCOY     NOT = INCIGRV-CHDRCOY    OR         */
		/*      INCIGRV-CHDRNUM     NOT = INCIGRV-CHDRNUM    OR         */
		if (((isNE(incigrvIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(incigrvIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(incigrvIO.getLife(),greversrec.life)
		|| isNE(incigrvIO.getCoverage(),greversrec.coverage)
		|| isNE(incigrvIO.getRider(),greversrec.rider))
		&& (isNE(incigrvIO.getStatuz(),varcom.endp)))) {
			/*      MOVE REWRT             TO INCIGRV-FUNCTION              */
			/*      CALL 'INCIGRVIO'    USING INCIGRV-PARAMS                */
			/*      IF INCIGRV-STATUZ     NOT = O-K                         */
			/*         MOVE INCIGRV-PARAMS      TO SYSR-PARAMS              */
			/*         MOVE INCIGRV-STATUZ      TO SYSR-STATUZ              */
			/*         PERFORM 900-FATAL-ERROR                              */
			/*      END-IF                                                  */
			incigrvIO.setStatuz(varcom.endp);
		}
	}

protected void reverseUtrns500()
	{
		start500();
	}

protected void start500()
	{
		/* Get Todays Date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError900();
		}
		/* Read T6647 to obtain processing sequence number for current*/
		/* Transaction Code.*/
		wsaaBatckey.set(greversrec.batckey);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(greversrec.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(chdrenqIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getItemcoy(),greversrec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(),t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(wsaaT6647Key);
			syserrrec.statuz.set(h965);
			fatalError900();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		zrutnrvIO.setParams(SPACES);
		zrutnrvIO.setChdrcoy(greversrec.chdrcoy);
		zrutnrvIO.setChdrnum(greversrec.chdrnum);
		zrutnrvIO.setLife(greversrec.life);
		zrutnrvIO.setCoverage(greversrec.coverage);
		zrutnrvIO.setPlanSuffix(ZERO);
		zrutnrvIO.setTranno(greversrec.tranno);
		zrutnrvIO.setProcSeqNo(wsaaSeqNo);
		zrutnrvIO.setUnitVirtualFund(SPACES);
		zrutnrvIO.setUnitType(SPACES);
		zrutnrvIO.setFormat(zrutnrvrec);
		zrutnrvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zrutnrvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zrutnrvIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "TRANNO", "PRCSEQ");

		SmartFileCode.execute(appVars, zrutnrvIO);
		if ((isNE(zrutnrvIO.getStatuz(),varcom.oK))
		&& (isNE(zrutnrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,zrutnrvIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,zrutnrvIO.getChdrnum()))
		|| (isNE(greversrec.life,zrutnrvIO.getLife()))
		|| (isNE(greversrec.coverage,zrutnrvIO.getCoverage()))
		|| (isNE(greversrec.tranno,zrutnrvIO.getTranno()))
		|| (isNE(wsaaSeqNo,zrutnrvIO.getProcSeqNo()))
		|| (isEQ(zrutnrvIO.getStatuz(),varcom.endp))) {
			zrutnrvIO.setStatuz(varcom.endp);
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			fatalError900();
		}
		else {
			zrraIO.setMoniesDate(zrutnrvIO.getMoniesDate());
			while ( !(isEQ(zrutnrvIO.getStatuz(),varcom.endp))) {
				updateUtrns510();
			}
			
			SmartFileCode.execute(appVars, zrraIO);
			if ((isNE(zrraIO.getStatuz(),varcom.oK))
			&& (isNE(zrraIO.getStatuz(),varcom.dupr))) {
				syserrrec.params.set(zrraIO.getParams());
				syserrrec.statuz.set(zrraIO.getStatuz());
				fatalError900();
			}
		}
	}

protected void updateUtrns510()
	{
		start510();
	}

protected void start510()
	{
		if (isEQ(zrutnrvIO.getFeedbackInd(),SPACES)) {
			deleteUtrn511();
		}
		else {
			writeNegativeUtrn512();
		}
		zrutnrvIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, zrutnrvIO);
		if ((isNE(zrutnrvIO.getStatuz(),varcom.oK))
		&& (isNE(zrutnrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			fatalError900();
		}
		if ((isNE(greversrec.chdrcoy,zrutnrvIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,zrutnrvIO.getChdrnum()))
		|| (isNE(greversrec.life,zrutnrvIO.getLife()))
		|| (isNE(greversrec.coverage,zrutnrvIO.getCoverage()))
		|| (isNE(greversrec.tranno,zrutnrvIO.getTranno()))
		|| (isNE(wsaaSeqNo,zrutnrvIO.getProcSeqNo()))
		|| (isEQ(zrutnrvIO.getStatuz(),varcom.endp))) {
			zrutnrvIO.setStatuz(varcom.endp);
		}
	}

protected void deleteUtrn511()
	{
		start511();
	}

protected void start511()
	{
		zrutnrvIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, zrutnrvIO);
		if (isNE(zrutnrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			fatalError900();
		}
		zrutnrvIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, zrutnrvIO);
		if (isNE(zrutnrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			fatalError900();
		}
	}

protected void writeNegativeUtrn512()
	{
		start512();
	}

protected void start512()
	{
		zrutnrvIO.setFormat(zrutnrvrec);
		zrutnrvIO.setFeedbackInd(SPACES);
		zrutnrvIO.setSwitchIndicator(SPACES);
		zrutnrvIO.setSurrenderPercent(ZERO);
		setPrecision(zrutnrvIO.getContractAmount(), 2);
		zrutnrvIO.setContractAmount(mult(zrutnrvIO.getContractAmount(),-1));
		setPrecision(zrutnrvIO.getFundAmount(), 2);
		zrutnrvIO.setFundAmount(mult(zrutnrvIO.getFundAmount(),-1));
		setPrecision(zrutnrvIO.getNofDunits(), 5);
		zrutnrvIO.setNofDunits(mult(zrutnrvIO.getNofDunits(),-1));
		setPrecision(zrutnrvIO.getNofUnits(), 5);
		zrutnrvIO.setNofUnits(mult(zrutnrvIO.getNofUnits(),-1));
		setPrecision(zrutnrvIO.getInciprm01(), 2);
		zrutnrvIO.setInciprm01(mult(zrutnrvIO.getInciprm01(),-1));
		setPrecision(zrutnrvIO.getInciprm02(), 2);
		zrutnrvIO.setInciprm02(mult(zrutnrvIO.getInciprm02(),-1));
		zrutnrvIO.setTransactionDate(greversrec.transDate);
		zrutnrvIO.setTransactionTime(greversrec.transTime);
		zrutnrvIO.setTranno(greversrec.newTranno);
		zrutnrvIO.setTermid(greversrec.termid);
		zrutnrvIO.setUser(greversrec.user);
		zrutnrvIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		zrutnrvIO.setBatccoy(wsaaBatckey.batcBatccoy);
		zrutnrvIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		zrutnrvIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		zrutnrvIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		zrutnrvIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		zrutnrvIO.setUstmno(ZERO);
		zrutnrvIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zrutnrvIO);
		if (isNE(zrutnrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			fatalError900();
		}
	}

protected void getWsaaTotalHitrValue600()
	{
		start601();
	}

	/**
	* <pre>
	***************************************                   <INTBR> 
	* </pre>
	*/
protected void start601()
	{
		hitrrv1IO.setParams(SPACES);
		hitrrv1IO.setChdrcoy(greversrec.chdrcoy);
		hitrrv1IO.setChdrnum(greversrec.chdrnum);
		hitrrv1IO.setLife(greversrec.life);
		hitrrv1IO.setCoverage(greversrec.coverage);
		hitrrv1IO.setPlanSuffix(ZERO);
		hitrrv1IO.setTranno(greversrec.tranno);
		hitrrv1IO.setProcSeqNo(wsaaSeqNo);
		hitrrv1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrrv1IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrrv1IO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "TRANNO", "PRCSEQ");
		hitrrv1IO.setFormat(hitrrv1rec);
		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)
		&& isNE(hitrrv1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			fatalError900();
		}
		if (isNE(greversrec.chdrcoy,hitrrv1IO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitrrv1IO.getChdrnum())
		|| isNE(greversrec.life,hitrrv1IO.getLife())
		|| isNE(greversrec.coverage,hitrrv1IO.getCoverage())
		|| isNE(greversrec.tranno,hitrrv1IO.getTranno())
		|| isNE(wsaaSeqNo,hitrrv1IO.getProcSeqNo())
		|| isEQ(hitrrv1IO.getStatuz(),varcom.endp)) {
			hitrrv1IO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hitrrv1IO.getStatuz(),varcom.endp))) {
			addUpHitrValue610();
		}
		
	}

protected void addUpHitrValue610()
	{
		/*START*/
		wsaaTotalHitrValue.add(hitrrv1IO.getContractAmount());
		hitrrv1IO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)
		&& isNE(hitrrv1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			fatalError900();
		}
		if (isNE(greversrec.chdrcoy,hitrrv1IO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitrrv1IO.getChdrnum())
		|| isNE(greversrec.life,hitrrv1IO.getLife())
		|| isNE(greversrec.coverage,hitrrv1IO.getCoverage())
		|| isNE(greversrec.tranno,hitrrv1IO.getTranno())
		|| isNE(wsaaSeqNo,hitrrv1IO.getProcSeqNo())
		|| isEQ(hitrrv1IO.getStatuz(),varcom.endp)) {
			hitrrv1IO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void reverseHitrs700()
	{
		start701();
	}

protected void start701()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError900();
		}
		wsaaBatckey.set(greversrec.batckey);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(greversrec.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(chdrenqIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(),greversrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6647)
		|| isNE(itdmIO.getItemitem(),wsaaT6647Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT6647Key);
			syserrrec.statuz.set(h965);
			fatalError900();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		hitrrv1IO.setParams(SPACES);
		hitrrv1IO.setChdrcoy(greversrec.chdrcoy);
		hitrrv1IO.setChdrnum(greversrec.chdrnum);
		hitrrv1IO.setLife(greversrec.life);
		hitrrv1IO.setCoverage(greversrec.coverage);
		hitrrv1IO.setPlanSuffix(ZERO);
		hitrrv1IO.setTranno(greversrec.tranno);
		hitrrv1IO.setProcSeqNo(wsaaSeqNo);
		hitrrv1IO.setFormat(hitrrv1rec);
		hitrrv1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrrv1IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrrv1IO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "TRANNO", "PRCSEQ");

		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)
		&& isNE(hitrrv1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			fatalError900();
		}
		if (isNE(greversrec.chdrcoy,hitrrv1IO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitrrv1IO.getChdrnum())
		|| isNE(greversrec.life,hitrrv1IO.getLife())
		|| isNE(greversrec.coverage,hitrrv1IO.getCoverage())
		|| isNE(greversrec.tranno,hitrrv1IO.getTranno())
		|| isNE(wsaaSeqNo,hitrrv1IO.getProcSeqNo())
		|| isEQ(hitrrv1IO.getStatuz(),varcom.endp)) {
			hitrrv1IO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hitrrv1IO.getStatuz(),varcom.endp))) {
			updateHitrs710();
		}
		
	}

protected void updateHitrs710()
	{
		/*START*/
		if (isEQ(hitrrv1IO.getFeedbackInd(),SPACES)) {
			deleteHitr720();
		}
		else {
			writeNegativeHitr730();
		}
		hitrrv1IO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)
		&& isNE(hitrrv1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			fatalError900();
		}
		if (isNE(greversrec.chdrcoy,hitrrv1IO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitrrv1IO.getChdrnum())
		|| isNE(greversrec.life,hitrrv1IO.getLife())
		|| isNE(greversrec.coverage,hitrrv1IO.getCoverage())
		|| isNE(greversrec.tranno,hitrrv1IO.getTranno())
		|| isNE(wsaaSeqNo,hitrrv1IO.getProcSeqNo())
		|| isEQ(hitrrv1IO.getStatuz(),varcom.endp)) {
			hitrrv1IO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void deleteHitr720()
	{
		/*START*/
		hitrrv1IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			fatalError900();
		}
		hitrrv1IO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			fatalError900();
		}
		/*EXIT*/
	}

protected void writeNegativeHitr730()
	{
		start731();
	}

protected void start731()
	{
		hitrrv1IO.setFormat(hitrrv1rec);
		hitrrv1IO.setFeedbackInd(SPACES);
		hitrrv1IO.setSwitchIndicator(SPACES);
		hitrrv1IO.setSurrenderPercent(ZERO);
		setPrecision(hitrrv1IO.getContractAmount(), 2);
		hitrrv1IO.setContractAmount(mult(hitrrv1IO.getContractAmount(),-1));
		setPrecision(hitrrv1IO.getFundAmount(), 2);
		hitrrv1IO.setFundAmount(mult(hitrrv1IO.getFundAmount(),-1));
		setPrecision(hitrrv1IO.getInciprm01(), 2);
		hitrrv1IO.setInciprm01(mult(hitrrv1IO.getInciprm01(),-1));
		setPrecision(hitrrv1IO.getInciprm02(), 2);
		hitrrv1IO.setInciprm02(mult(hitrrv1IO.getInciprm02(),-1));
		setPrecision(hitrrv1IO.getProcSeqNo(), 0);
		hitrrv1IO.setProcSeqNo(mult(hitrrv1IO.getProcSeqNo(),-1));
		hitrrv1IO.setTranno(greversrec.newTranno);
		hitrrv1IO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrrv1IO.setBatccoy(wsaaBatckey.batcBatccoy);
		hitrrv1IO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hitrrv1IO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrrv1IO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrrv1IO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		hitrrv1IO.setUstmno(ZERO);
		hitrrv1IO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			fatalError900();
		}
	}

protected void fatalError900()
	{
					start900();
					end950();
				}

protected void start900()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void end950()
	{
		greversrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
