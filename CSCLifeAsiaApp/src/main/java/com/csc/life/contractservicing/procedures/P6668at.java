/*
 * File: P6668at.java
 * Date: 30 August 2009 0:49:57
 * Author: Quipoz Limited
 *
 * Class transformed from P6668AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Bchgallrec;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5664rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.regularprocessing.dataaccess.IncrhstTableDAM;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;

/**
* <pre>
*
*REMARKS.
*       P6668AT - Billing Transaction AT Module
*       ---------------------------------------
*   This AT module is called by the Billing Transaction program
*   P6668 and the contract number is passed in the AT
*   parameters area as the 'primary key'.
*
*  AGCMBCH
*  ~~~~~~~
*   Create a logical file AGCMBCH based on the physical file
*   AGCMPF and include all the fields from the physical file
*   also with the following key order:-
*
*        CHDRCOY
*        CHDRNUM
*        LIFE
*        COVERAGE
*        RIDER
*        PLNSFX
*        AGNTNUM
*
*    Select on VALIDFLAG = '1' or ' '.
*
*  AT PROCESSING
*  -------------
*    Files:  AGCMBCH - Agent commission details
*            COVRMJA - Coverage/Rider Details
*            CHDRMJA - Contract Header Details
*
*    Tables: T5679 - Valid Status Codes
*            T5541 - Frequency Conversion Factors
*            T5671 - Coverage/Rider Generic Processing
*            T5687 - General Coverage/Rider Details
*
*    Check the return status codes for each of the following
*    records being updated.
*
*   PROCESS CONTRACT HEADER
*    Update the contract header as follows:-
*      - set the valid flag to '2' and set the effective
*        date to default to todays date and rewrite the
*        record using the REWRT function.
*      - write a new version of the contract header
*        as follows:-
*      - set the valid flag to '1'
*      - update the transaction number, increment by +1 and
*        update the transaction date using the default of
*        todays date
*      - update the contract status code with the status
*        code from T5679 (keyed by transaction number)
*        only if there is an entry on T5679
*      - update the premium status field if the status
*        field from T5679 has entries. If entries are
*        present, then check the Bill frequency from the
*        the AT parameters. If it is '00', i.e. single
*        premium, otherwise use the regular premium status
*        field
*      - set the 'Effective From' date to todays date
*      - set the 'Effective To' date to VRCM-MAX-DATE
*      - if the billing frequency has changed, you should
*        not be here unless it has changed, set the billing
*        frequency to the value from the AT linkage
*      - if the billing method has changed, set the billing
*        method to the value from the AT linkage
*      - if the billing day has changed, set the billing day
*        of BILLCD to the value from the AT linkage
*      - if the Payor has changed, set to the value passed
*        in AT linkage
*      - if Group number and member number have changed,
*        then set to the value from AT linkage
*      - if Mandate reference has changed, then set to the
*        value from AT linkage
*      - write the new CHDR record using the WRITR function.
*
*   PROCESS COMPONENTS
*
*    Read all the Coverage/Rider records (COVRMJA) for this
*    contract and check their status against T5679 (keyed by
*    Transaction number) and only update the Component if a
*    match exists on T5679. Also, only update the Component if
*    the INSTPREM is greater than 0.
*
*   Update each COVR record as follows:-
*      - BEGNH on the COVR record for the relevant contract,
*
*    DOWHILE there are COVRs for this contract
*      - set the valid flag to '2' and the 'Effective To'
*        date to todays date and UPDAT the record,
*      - calculate the new premium as follows, read T5687
*        using CRTABLE as the item to get the frequency
*        alteration basis which is then used to read T5541
*        Both the old and the new frequencies are read to
*        obtain their respective loading factors.
*        When they are found and no errors appear, the premium
*        is calculated as follows :
*
*        New Premium =    Old Premium
*                       * (Old frequency / New Frequency)
*                       * (New Freq loading factor /
*                          Old Freq loading factor  )
*
*      - update the status code, if an entry exists on
*        T5679. Use the regular premium status code.
*      - write a new COVR record as follows, update the
*        transaction number with the transaction number from
*        the contract header. Set the CURRFROM to BTD
*        set the CURRTO to VRCM-MAX-DATE and set
*        the valid flag to '1'. Write the record with a
*        WRITR function.
*      - Set AGCMBCH-PARAMS to space.
*        Set AGCMBCH-CHDRCOY to COVRMJA-CHDRCOY.
*        Set AGCMBCH-CHDRNUM to COVRMJA-CHDRNUM.
*        Set AGCMBCH-LIFE to COVRMJA-LIFE.
*        Set AGCMBCH-COVERAGE to COVRMJA-COVERAGE.
*        Set AGCMBCH-RIDER to COVRMJA-RIDER.
*        Set AGCMBCH-PLAN-SUFFIX to COVRMJA-PLAN-SUFFIX.
*        Set AGCMBCH-FORMAT to 'AGCMBCHREC'.
*        Set AGCMBCH-FUNCTION to BEGNH.
*    Call 'AGCMBCHIO' to read and hold the record.
*        Perform normal error checking.
*        If end of file
*            next sentence
*        Else
*            If key break(Up to and include plan suffix)
*                Rewrite the record with function 'REWRT'
*                Perform normal error checking
*                Set AGCMBCH-STATUZ to ENDP.
*        Perform PROCESS-READ-AGCMBCH
*            until AGCMBCH-STATUZ = ENDP.
*      - GENERIC SUBROUTINE, access T5671 (keyed by
*        Transaction number and CRTABLE) for each
*        component and obtain the Generic subroutine. If an
*        entry is found, call the relevant program to
*        process the generic components
*      - NEXTR on the COVRs
*     ENDDO
*
*   GENERAL HOUSEKEEPING
*
*   1) Write a PTRN record:
*      - contract key from contract header
*      - transaction number from contract header
*      - transaction effective date is todays date
*   2) Update the batch header by calling BATCUP with a
*      function of WRITS and the following parameters:
*       - Transaction count equals 1
*       - zeroise all amounts
*       - batch key from the AT linkage.
*   3) Release Softlock
*       - Release the 'SFTLOCK' using the UNLK function.
*
*   PROCESS-READ-AGCMBCH Section
*   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*   Set AGCMBCH-CURRTO to EFFDATE.
*   Set AGCMBCH-VALIDFLAG to '2'.
*   Set AGCMBCH-FUNCTION to REWRT.
*   Call 'AGCMBCHIO' to rewrite the record.
*   Perform normal error checking.
*   Set AGCMBCH-TRANNO to the new TRANNO.
*   Set AGCMBCH-VALIDFLAG to '1'.
*   Set AGCMBCH-CURRFROM to BTD
*   Set AGCMBCH-CURRTO to 99999999.
*   Compute AGCMBCH-ANNPREM rounded = AGCMBCH-ANNPREM *
*                                  (New loading factor/
*                                   Old loading factor)
*   Note that old factor and new factor must be numeric.
*   Set AGCMBCH-FUNCTION to WRITR.
*   Call 'AGCMBCHIO' to write the new record.
*   Perform normal error checking.
*   Set AGCMBCH-FUNCTION to NEXTR.
*   Call 'AGCMBCHIO' to read and hold the next record.
*   Perform normal error checking.
*   If end of file
*     Next sentence
*   Else
*    If key break(Up to and include plan suffix)
*        Rewrite the record with function 'REWRT'
*        Perform normal error checking
*        Set AGCMBCH-STATUZ to ENDP.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*
*****************************************************************
* </pre>
*/
public class P6668at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("P6668AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(16);
	private ZonedDecimalData wsaaTermid = new ZonedDecimalData(4, 0).isAPartOf(wsaaTranid, 0).setUnsigned();
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
		/* CORRECT DATE REDEFINITION
		01  WSAA-BILL-DATE-9                 REDEFINES WSAA-BILL-DATE.   */
	private ZonedDecimalData wsaaBillDate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaBillDate9 = new FixedLengthStringData(8).isAPartOf(wsaaBillDate, 0, REDEFINE);
	private ZonedDecimalData wsaaBillMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillDate9, 4).setUnsigned();
	private ZonedDecimalData wsaaBillDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillDate9, 6).setUnsigned();
	private ZonedDecimalData wsaaBtdate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaBtdate9 = new FixedLengthStringData(8).isAPartOf(wsaaBtdate, 0, REDEFINE);
	private ZonedDecimalData wsaaBtMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBtdate9, 4).setUnsigned();
	private ZonedDecimalData wsaaBtDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBtdate9, 6).setUnsigned();
	private PackedDecimalData wsaaShortTransDate = new PackedDecimalData(6, 0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Mop = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);

	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkey1 = new FixedLengthStringData(1).isAPartOf(wsaaOkeys, 0);
	private ZonedDecimalData wsaaPrevTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOkeys, 1).setUnsigned();

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItem, 3);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private String wsaaWopRiderFlag = "N";
	private FixedLengthStringData wsaaTr517Key = new FixedLengthStringData(4).init(SPACES);
	private String wsaaWaiverFlag = "N";
	private PackedDecimalData wsaaWopPrem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaWopSi = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaNewFreqFee = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOldFreqFee = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private ZonedDecimalData wsaaRate = new ZonedDecimalData(8, 2).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private String wsaaWopAccrFlag = "N";
	private String wsaaWopAccrFee = "N";

	private FixedLengthStringData wsaaT5567Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5567Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5567Key, 0);
	private FixedLengthStringData wsaaT5567Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5567Key, 3);

	private FixedLengthStringData wsaaT5664Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5664Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5664Key, 0);
	private FixedLengthStringData wsaaT5664Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 4);
	private FixedLengthStringData wsaaT5664Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 5);

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);

		/* WSAA-R-TABLE */
	private FixedLengthStringData[] wsaaCompTable = FLSInittedArray (10, 27);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 0);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 2);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaCompTable, 4);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 8);
	private ZonedDecimalData[] wsaaPremAmt = ZDArrayPartOfArrayStructure(17, 2, wsaaCompTable, 10);
		/* FORMATS */
	private static final String ptrnrec = "PTRNREC";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String agcmbchrec = "AGCMBCHREC";
	private static final String itemrec = "ITEMREC";
	private static final String payrrec = "PAYRREC";
	private static final String incrhstrec = "INCRHSTREC";
	private static final String lextrec = "LEXTREC";
		/* ERRORS */
	private static final String f294 = "F294";
	private static final String f290 = "F290";
	private static final String f151 = "F151";
	private static final String f358 = "F358";
	private static final String h966 = "H966";
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private IncrhstTableDAM incrhstIO = new IncrhstTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Varcom varcom = new Varcom();
	private Bchgallrec bchgallrec = new Bchgallrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5679rec t5679rec = new T5679rec();
	private T5541rec t5541rec = new T5541rec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T5674rec t5674rec = new T5674rec();
	private T6654rec t6654rec = new T6654rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T7508rec t7508rec = new T7508rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T5664rec t5664rec = new T5664rec();
	private T5567rec t5567rec = new T5567rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private WsaaTransactionRecInner wsaaTransactionRecInner = new WsaaTransactionRecInner();
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextrCovr336,
		wopAccr385,
		updateStatus386,
		nextrCovr360a,
		retrieveNextRecord393,
		exit719,
		a110Check
	}

	public P6668at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		main110();
		exit190();
	}

protected void main110()
	{
		initialise100();
		processContractHeader200();
		processPayrRecord250();
		getFirstComponent300();
		wsaaMiscellaneousInner.wsaaI.set(ZERO);
		wsaaWopRiderFlag = "N";
		wsaaWopAccrFlag = "N";
		wsaaWopAccrFee = "N";
		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			processComponent330();
		}

		if (isEQ(wsaaWopRiderFlag, "Y")) {
			getFirstComponent300();
			covrmjaIO.setStatuz(SPACES);
			while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
				processCompWop330a();
			}

		}
		accumInstpremWrtChdrmja400();
		writeLetter710();
		writeHistoryRlseLocks500();
		a000Statistics();
	}

protected void exit190()
	{
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxx1ErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxx1ErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		initVariables110();
		retrieveContractHeader120();
		convertTransactionDate130();
	}

protected void initVariables110()
	{
		wsaaMiscellaneousInner.wsaaTotInstprem.set(ZERO);
	}

protected void retrieveContractHeader120()
	{
		wsaaTransactionRecInner.wsaaTransactionRec.set(atmodrec.transArea);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(wsaaChdrcoy);
		chdrmjaIO.setChdrnum(wsaaChdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if ((isNE(chdrmjaIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/* Read the PAYR file*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(wsaaChdrcoy);
		payrIO.setChdrnum(wsaaChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if ((isNE(payrIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		covrmjaIO.setParams(SPACES);
		wsaaMiscellaneousInner.sub.set(9);
	}

protected void convertTransactionDate130()
	{
		/* Truncate left two digits of date, the century, to leave YYMMDD  */
		/* format.  WSAA-AT-TRANSACTION-DATE then be in CCYYMMDD format,   */
		/* whilst WSAA-SHORT-TRANS-DATE will be in YYMMDD format.          */
		wsaaShortTransDate.set(wsaaTransactionRecInner.wsaaAtTransactionDate);
		/*EXIT1*/
	}

protected void processContractHeader200()
	{
		updateOriginalChdr210();
		createNewContractHeader240();
	}

protected void updateOriginalChdr210()
	{
		wsaaPrevTranno.set(chdrmjaIO.getTranno());
		wsaaItemCnttype.set(chdrmjaIO.getCnttype());
		chdrmjaIO.setValidflag("2");
		/* MOVE WSAA-AT-TODAY               TO CHDRMJA-CURRTO.          */
		/* MOVE CHDRMJA-BTDATE              TO DTC2-INT-DATE-1.    <003>*/
		chdrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		/* MOVE CHDRMJA-BILLFREQ            TO DTC2-FREQUENCY.     <003>*/
		/* MOVE -1                          TO DTC2-FREQ-FACTOR.   <003>*/
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC.                  <003>*/
		/* MOVE DTC2-INT-DATE-2             TO CHDRMJA-CURRTO.     <003>*/
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void createNewContractHeader240()
	{
		/*    Read Table T5679*/
		wsaaBatchkey.set(atmodrec.batchKey);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatchkey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		/*   Update the Status field*/
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}
		/*   Update the Premium Status field*/
		if (isNE(t5679rec.setCnPremStat, SPACES)) {
			chdrmjaIO.setPstatcode(t5679rec.setCnPremStat);
		}
		/*   Update relevant billing fields*/
		chdrmjaIO.setValidflag("1");
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		/*MOVE WSAA-AT-TRANSACTION-DATE    TO WSAA-DATE.               */
		wsaaDate.set(wsaaShortTransDate);
		wsaaTermid.set(wsaaTransactionRecInner.wsaaAtTermid);
		wsaaUser.set(wsaaTransactionRecInner.wsaaAtUser);
		chdrmjaIO.setTranid(wsaaTranid);
		chdrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		wsaaMiscellaneousInner.wsaaCompFreq.set(chdrmjaIO.getBillfreq());
		chdrmjaIO.setBillfreq(wsaaTransactionRecInner.wsaaAtBillfreq);
		if (isNE(chdrmjaIO.getBillchnl(), wsaaTransactionRecInner.wsaaAtBillchnl)) {
			chdrmjaIO.setBillchnl(wsaaTransactionRecInner.wsaaAtBillchnl);
		}
		wsaaBillDate.set(chdrmjaIO.getBillcd());
		if (isNE(wsaaBillDd, wsaaTransactionRecInner.wsaaAtBillday)) {
			wsaaBillDd.set(wsaaTransactionRecInner.wsaaAtBillday);
			chdrmjaIO.setBillcd(wsaaBillDate);
		}
		if ((isNE(chdrmjaIO.getPayrnum(), wsaaTransactionRecInner.wsaaAtPayrnum))
		&& (isNE(wsaaTransactionRecInner.wsaaAtPayrnum, SPACES))) {
			chdrmjaIO.setPayrnum(wsaaTransactionRecInner.wsaaAtPayrnum);
		}
		if ((isNE(chdrmjaIO.getGrupkey(), wsaaTransactionRecInner.wsaaAtGrupkey))
		&& (isNE(wsaaTransactionRecInner.wsaaAtGrupkey, SPACES))) {
			chdrmjaIO.setGrupkey(wsaaTransactionRecInner.wsaaAtGrupkey);
		}
		/* IF (CHDRMJA-MANDREF           NOT = WSAA-AT-MANDREF) AND     */
		/*    (WSAA-AT-MANDREF           NOT = SPACES)                  */
		/*    MOVE WSAA-AT-MANDREF             TO CHDRMJA-MANDREF       */
		/* END-IF.                                                      */
		if (isNE(chdrmjaIO.getMandref(), wsaaTransactionRecInner.wsaaAtMandref)) {
			chdrmjaIO.setMandref(wsaaTransactionRecInner.wsaaAtMandref);
		}
	}

protected void processPayrRecord250()
	{
		updateOriginalPayr210();
		createNewPayrRecord240();
	}

protected void updateOriginalPayr210()
	{
		payrIO.setValidflag("2");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void createNewPayrRecord240()
	{
		/*                                                      <A05691>*/
		/* This table has already been read, no need to read a 2nd time */
		/* Read Table T5679                                     <A05691>*/
		/*                                                      <A05691>*/
		/* MOVE ATMD-BATCH-KEY              TO WSAA-BATCHKEY.   <A05691>*/
		/* MOVE SPACES                      TO ITEM-DATA-KEY.   <A05691>*/
		/* MOVE 'IT'                        TO ITEM-ITEMPFX.    <A05691>*/
		/* MOVE ATMD-COMPANY                TO ITEM-ITEMCOY.    <A05691>*/
		/* MOVE T5679                       TO ITEM-ITEMTABL.   <A05691>*/
		/* MOVE WSKY-BATC-BATCTRCDE         TO ITEM-ITEMITEM.   <A05691>*/
		/* MOVE READR                       TO ITEM-FUNCTION.   <A05691>*/
		/*                                                      <A05691>*/
		/* CALL 'ITEMIO' USING ITEM-PARAMS.                     <A05691>*/
		/*                                                      <A05691>*/
		/* IF ITEM-STATUZ                NOT = O-K              <A05691>*/
		/*     MOVE ITEM-PARAMS             TO SYSR-PARAMS      <A05691>*/
		/*     MOVE ITEM-STATUZ             TO SYSR-STATUZ      <A05691>*/
		/*     PERFORM XXXX-FATAL-ERROR                         <A05691>*/
		/* ELSE                                                 <A05691>*/
		/*     MOVE ITEM-GENAREA            TO T5679-T5679-REC  <A05691>*/
		/* END-IF.                                              <A05691>*/
		/*   Update the Premium Status field                               */
		if (isNE(t5679rec.setCnPremStat, SPACES)) {
			payrIO.setPstatcode(t5679rec.setCnPremStat);
		}
		/*   Update relevant billing fields                                */
		payrIO.setValidflag("1");
		payrIO.setTranno(chdrmjaIO.getTranno());
		/*MOVE WSAA-AT-TRANSACTION-DATE    TO PAYR-TRANSACTION-DATE.<00*/
		payrIO.setTransactionDate(wsaaShortTransDate);
		payrIO.setTermid(wsaaTransactionRecInner.wsaaAtTermid);
		payrIO.setUser(wsaaTransactionRecInner.wsaaAtUser);
		wsaaMiscellaneousInner.wsaaCompFreq.set(payrIO.getBillfreq());
		/* MOVE WSAA-AT-BILLFREQ            TO PAYR-BILLFREQ.      <002>*/
		if (isNE(wsaaTransactionRecInner.wsaaAtBillfreq, payrIO.getBillfreq())) {
			payrIO.setBillfreq(wsaaTransactionRecInner.wsaaAtBillfreq);
			payrIO.setBillday(wsaaBillDd);
			payrIO.setBillmonth(wsaaBillMm);
			wsaaBtdate.set(payrIO.getBtdate());
			payrIO.setDuedd(wsaaBtDd);
			payrIO.setDuemm(wsaaBtMm);
		}
		if (isNE(payrIO.getBillchnl(), wsaaTransactionRecInner.wsaaAtBillchnl)) {
			payrIO.setBillchnl(wsaaTransactionRecInner.wsaaAtBillchnl);
		}
		wsaaBillDate.set(payrIO.getBillcd());
		if (isNE(wsaaBillDd, wsaaTransactionRecInner.wsaaAtBillday)) {
			wsaaBillDd.set(wsaaTransactionRecInner.wsaaAtBillday);
			payrIO.setBillday(wsaaTransactionRecInner.wsaaAtBillday);
			payrIO.setBillmonth(wsaaBillMm);
			payrIO.setBillcd(wsaaBillDate);
			updateNextdate370();
		}
		/* IF (PAYR-GRUPKEY              NOT = WSAA-AT-GRUPKEY) AND<002>*/
		/*    (WSAA-AT-GRUPKEY           NOT = SPACES)             <002>*/
		/*    MOVE WSAA-AT-GRUPKEY             TO PAYR-GRUPKEY     <002>*/
		/* END-IF.                                                 <002>*/
		if ((isNE(payrIO.getGrupnum(), wsaaTransactionRecInner.wsaaAtGrupkey))
		&& (isNE(wsaaTransactionRecInner.wsaaAtGrupkey, SPACES))) {
			payrIO.setGrupnum(wsaaTransactionRecInner.wsaaAtGrupkey);
		}
		/* IF (PAYR-MANDREF              NOT = WSAA-AT-MANDREF) AND<002>*/
		/*    (WSAA-AT-MANDREF           NOT = SPACES)             <002>*/
		/*    MOVE WSAA-AT-MANDREF             TO PAYR-MANDREF     <002>*/
		/* END-IF.                                                 <002>*/
		if (isNE(payrIO.getMandref(), wsaaTransactionRecInner.wsaaAtMandref)) {
			payrIO.setMandref(wsaaTransactionRecInner.wsaaAtMandref);
		}
		payrIO.setEffdate(payrIO.getBtdate());
	}

protected void getFirstComponent300()
	{
		begin310();
	}

	/**
	* <pre>
	*    Retrieve first Coverage for this contract.
	* </pre>
	*/
protected void begin310()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		//covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrmjaIO);
		if ((isNE(covrmjaIO.getStatuz(), varcom.oK))
		&& (isNE(covrmjaIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if ((isNE(chdrmjaIO.getChdrnum(), covrmjaIO.getChdrnum()))
		|| (isNE(chdrmjaIO.getChdrcoy(), covrmjaIO.getChdrcoy()))
		|| (isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			covrmjaIO.setStatuz(varcom.endp);
		}
	}

protected void processComponent330()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readTr517330();
					readT5687340();
					checkStatusAndAction350();
				case nextrCovr336:
					nextrCovr336();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*340-GET-STATUS-CODES.
	****Read Table T5679 to obtain status codes for Coverage/Rider
	**** MOVE ATMD-BATCH-KEY              TO WSAA-BATCHKEY.
	**** MOVE SPACES                      TO ITEM-DATA-KEY.
	**** MOVE 'IT'                        TO ITEM-ITEMPFX.
	**** MOVE ATMD-COMPANY                TO ITEM-ITEMCOY.
	**** MOVE T5679                       TO ITEM-ITEMTABL.
	**** MOVE WSKY-BATC-BATCTRCDE         TO ITEM-ITEMITEM.
	**** MOVE READR                       TO ITEM-FUNCTION.
	**** CALL 'ITEMIO' USING ITEM-PARAMS.
	**** IF ITEM-STATUZ                NOT = O-K
	****     MOVE ITEM-PARAMS             TO SYSR-PARAMS
	****     MOVE ITEM-STATUZ             TO SYSR-STATUZ
	****     PERFORM XXXX-FATAL-ERROR
	**** ELSE
	****     MOVE ITEM-GENAREA            TO T5679-T5679-REC
	**** END-IF.
	* </pre>
	*/
protected void readTr517330()
	{
		/*    Read Table TR517 to check for WOP riders, If item found      */
		/*    don't process, except Accelerated Crisis Waiver              */
		wsaaTr517Key.set(covrmjaIO.getCrtable());
		a300ReadTr517();
		if (isNE(tr517rec.tr517Rec, SPACES)) {
			if (isNE(tr517rec.zrwvflg04, "Y")) {
				wsaaWopRiderFlag = "Y";
				goTo(GotoLabel.nextrCovr336);
			}
			else {
				wsaaWopAccrFlag = "Y";
				if (isEQ(tr517rec.zrwvflg03, "Y")) {
					wsaaWopAccrFee = "Y";
				}
			}
		}
	}

	/**
	* <pre>
	**** Read Table T5687 to obtain Single Premium Indicator and
	**** Frequency Alteration Basis.
	* </pre>
	*/
protected void readT5687340()
	{
		wsaaBatchkey.set(atmodrec.batchKey);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(itdmIO.getItemcoy(), atmodrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5687))
		|| (isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkStatusAndAction350()
	{
		/*    Check if component is to be updated.*/
		wsaaMiscellaneousInner.wsaaProcessComponent.set("N");
		if (isGT(covrmjaIO.getInstprem(), 0)
		&& isNE(t5687rec.singlePremInd, "Y")) {
			checkComponent360();
			if (wsaaMiscellaneousInner.processComponent.isTrue()) {
				updateComponent380();
			}
		}
	}

protected void nextrCovr336()
	{
		/*    Get next coverage record*/
		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if ((isNE(covrmjaIO.getStatuz(), varcom.oK))
		&& (isNE(covrmjaIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if ((isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy()))
		|| (isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum()))) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkComponent360()
	{
		check360();
	}

protected void check360()
	{
		/*  Check for match on Premium and Risk Statii for Coverage*/
		wsaaMiscellaneousInner.wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(), "00")) {
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.covPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.covRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
		else {
			/*   Check for match on Premium and Risk Statii for Rider*/
			wsaaMiscellaneousInner.wsaaStatCount.set(1);
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.ridPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
	}

protected void updateNextdate370()
	{
		nextdate370();
	}

protected void nextdate370()
	{
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		String key;
		if(BTPRO028Permission) {
			key = payrIO.getBillchnl().toString().trim() + chdrmjaIO.getCnttype().toString().trim() +  payrIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = payrIO.getBillchnl().toString().trim().concat(chdrmjaIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = payrIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						syserrrec.params.set(itemIO.getParams());
						syserrrec.statuz.set(itemIO.getStatuz());
						xxxxFatalError();
					}
				}
			}
			
		}
		else {
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6654);
		wsaaT6654Key.set(SPACES);
		wsaaT6654Mop.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrmjaIO.getCnttype());
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		else {
			wsaaT6654Cnttype.set("***");
			itemIO.setItemitem(wsaaT6654Key);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
			else {
				t6654rec.t6654Rec.set(itemIO.getGenarea());
			}
		}
		}
	
		/* Calculate the next billing extract date                         */
		/* NEXTDATE = BILLCD less the No of lead billing days.             */
		datcon2rec.intDate1.set(payrIO.getBillcd());
		datcon2rec.frequency.set("DY");
		compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays, -1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		payrIO.setNextdate(datcon2rec.intDate2);
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(payrIO.getChdrcoy().toString());
	itempf.setItemtabl(tablesInner.t6654.toString());
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void updateComponent380()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateOriginalCovr382();
					createNewCovr385();
				case wopAccr385:
					wopAccr385();
				case updateStatus386:
					updateStatus386();
					findGenericSubroutine387();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateOriginalCovr382()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		/*    MOVE WSAA-AT-TODAY               TO COVRMJA-CURRTO.          */
		covrmjaIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void createNewCovr385()
	{
		/* Table T5687 is now read in the 330 section.                     */
		/* Read Table T5687 to obtain frequency alteration basis        */
		/* MOVE ATMD-BATCH-KEY              TO WSAA-BATCHKEY.           */
		/* MOVE SPACES                      TO ITDM-DATA-KEY.           */
		/* MOVE ATMD-COMPANY                TO ITDM-ITEMCOY.            */
		/* MOVE T5687                       TO ITDM-ITEMTABL.           */
		/* MOVE COVRMJA-CRTABLE             TO ITDM-ITEMITEM.           */
		/* MOVE COVRMJA-CURRFROM            TO ITDM-ITMFRM.             */
		/* MOVE BEGN                        TO ITDM-FUNCTION.           */
		/* CALL 'ITDMIO'                    USING ITDM-PARAMS.          */
		/* IF (ITDM-STATUZ               NOT = O-K) AND                 */
		/*    (ITDM-STATUZ               NOT = ENDP)                    */
		/*     MOVE ITDM-PARAMS             TO SYSR-PARAMS              */
		/*     MOVE ITDM-STATUZ             TO SYSR-STATUZ              */
		/*     PERFORM XXXX-FATAL-ERROR                                 */
		/* END-IF.                                                      */
		/* IF (ITDM-ITEMCOY              NOT = ATMD-COMPANY)    OR      */
		/*    (ITDM-ITEMTABL             NOT = T5687)           OR      */
		/*    (ITDM-ITEMITEM             NOT = COVRMJA-CRTABLE) OR      */
		/*    (ITDM-STATUZ                   = ENDP)                    */
		/*    MOVE ITDM-PARAMS              TO SYSR-PARAMS              */
		/*    MOVE F294                     TO SYSR-STATUZ              */
		/*    PERFORM XXXX-FATAL-ERROR                                  */
		/* ELSE                                                         */
		/*     MOVE ITDM-GENAREA            TO T5687-T5687-REC          */
		/* END-IF.                                                      */
		/*    For Accelerated Crisis Wavier, check whether it waives policy*/
		/*    fee, if yes, then we have to subtract old policy fee from old*/
		/*    SI.                                                          */
		if (isEQ(wsaaWopAccrFlag, "Y")
		&& isEQ(wsaaWopAccrFee, "Y")
		&& isNE(t5688rec.feemeth, SPACES)) {
			a400ReadT5567();
			setPrecision(covrmjaIO.getSumins(), 2);
			covrmjaIO.setSumins(sub(covrmjaIO.getSumins(), wsaaOldFreqFee));
		}
		/* Read table T5541 to obtain loading factors for both          */
		/* old and new frequencies.                                     */
		wsaaMiscellaneousInner.wsaaInstpremFound.set(SPACES);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5541);
		itdmIO.setItemitem(t5687rec.xfreqAltBasis);
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5541)
		|| isNE(itdmIO.getItemitem(), t5687rec.xfreqAltBasis)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
			goTo(GotoLabel.updateStatus386);
		}
		else {
			t5541rec.t5541Rec.set(itdmIO.getGenarea());
		}
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaTransactionRecInner.wsaaAtBillfreq);
		wsaaMiscellaneousInner.wsaaCalcOldfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
		/* Get the Frequency Conversion Multiplication Factor           */
		/* Calculate the new INSTPREM                                   */
		/* Calculate the accumulating INSTPREM for SINSTAMT01           */
		wsaaMiscellaneousInner.wsaaNewFreqFound.set(SPACES);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 12)
		|| isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaMiscellaneousInner.wsaaCalcNewfreq)) {
				wsaaMiscellaneousInner.wsaaNewLfact.set(t5541rec.lfact[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaNewFreqFound.set("Y");
			}
		}
		/* PERFORM VARYING SUB FROM 1 BY 1                         <006>*/
		/*         UNTIL   SUB > 12                                <006>*/
		/*         OR      WSAA-NEW-FREQ-FOUND = 'Y'               <006>*/
		/*                                                         <006>*/
		/*     IF T5541-FREQCY(SUB)    = WSAA-CALC-NEWFREQ         <006>*/
		/*         MOVE T5541-LFACT(SUB) TO WSAA-NEW-LFACT         <006>*/
		/*         MOVE 'Y'            TO WSAA-NEW-FREQ-FOUND      <006>*/
		/*     END-IF                                              <006>*/
		/*                                                         <006>*/
		/* END-PERFORM.                                            <006>*/
		/*                                                         <006>*/
		wsaaMiscellaneousInner.wsaaOldFreqFound.set(SPACES);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 12)
		|| isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaMiscellaneousInner.wsaaCalcOldfreq)) {
				wsaaMiscellaneousInner.wsaaOldLfact.set(t5541rec.lfact[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaOldFreqFound.set("Y");
			}
		}
		/* PERFORM VARYING SUB FROM 1 BY 1                         <006>*/
		/*         UNTIL   SUB > 12                                <006>*/
		/*         OR      WSAA-OLD-FREQ-FOUND = 'Y'               <006>*/
		/*                                                         <006>*/
		/*     IF T5541-FREQCY(SUB)    = WSAA-CALC-OLDFREQ         <006>*/
		/*         MOVE T5541-LFACT(SUB) TO WSAA-OLD-LFACT         <006>*/
		/*         MOVE 'Y'            TO WSAA-OLD-FREQ-FOUND      <006>*/
		/*     END-IF                                              <006>*/
		/*                                                         <006>*/
		/* END-PERFORM.                                            <006>*/
		if (isEQ(wsaaWopAccrFlag, "Y")) {
			goTo(GotoLabel.wopAccr385);
		}
		if (isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")
		&& isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")) {
			wsaaMiscellaneousInner.wsaaInstpremFound.set("Y");
			setPrecision(covrmjaIO.getInstprem(), 3);
			covrmjaIO.setInstprem(mult((div((mult(covrmjaIO.getInstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(covrmjaIO.getInstprem());
			b000CallRounding();
			covrmjaIO.setInstprem(zrdecplrec.amountOut);
			/*      COMPUTE COVRMJA-ZBINSTPREM ROUNDED =              <LA3339>*/
			/*         COVRMJA-ZBINSTPREM * (WSAA-CALC-OLDFREQ /      <LA3339>*/
			setPrecision(covrmjaIO.getZbinstprem(), 3);
			covrmjaIO.setZbinstprem(mult((div((mult(covrmjaIO.getZbinstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(covrmjaIO.getZbinstprem());
			b000CallRounding();
			covrmjaIO.setZbinstprem(zrdecplrec.amountOut);
			/*     COMPUTE COVRMJA-ZLINSTPREM ROUNDED =              <LA3339>*/
			/*        COVRMJA-ZLINSTPREM * (WSAA-CALC-OLDFREQ /      <LA3339>*/
			setPrecision(covrmjaIO.getZlinstprem(), 3);
			covrmjaIO.setZlinstprem(mult((div((mult(covrmjaIO.getZlinstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(covrmjaIO.getZlinstprem());
			b000CallRounding();
			covrmjaIO.setZlinstprem(zrdecplrec.amountOut);
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
		}
		else {
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
		}
		/* Read Table T6630 to obtain relevant factor for new frequency */
		/* MOVE T5687-XFREQ-ALT-BASIS       TO WSAA-COMP-FREQBASIS.     */
		/* MOVE SPACES                      TO ITDM-DATA-KEY.           */
		/* MOVE COVRMJA-CHDRCOY             TO ITDM-ITEMCOY.            */
		/* MOVE T6630                       TO ITDM-ITEMTABL.           */
		/* MOVE WSAA-COMP-KEY               TO ITDM-ITEMITEM.           */
		/* MOVE COVRMJA-CURRFROM            TO ITDM-ITMFRM.             */
		/* MOVE BEGN                        TO ITDM-FUNCTION.           */
		/* CALL 'ITDMIO' USING ITDM-PARAMS.                             */
		/* IF ITDM-STATUZ                NOT = O-K AND NOT = ENDP       */
		/*     MOVE ITDM-PARAMS             TO SYSR-PARAMS              */
		/*     MOVE ITDM-STATUZ             TO SYSR-STATUZ              */
		/*     PERFORM XXXX-FATAL-ERROR                                 */
		/* END-IF.                                                      */
		/* IF ITDM-ITEMCOY               NOT = COVRMJA-CHDRCOY          */
		/*    OR ITDM-ITEMTABL           NOT = T6630                    */
		/*    OR ITDM-ITEMITEM           NOT = WSAA-COMP-KEY            */
		/*    OR ITDM-STATUZ                 = ENDP                     */
		/*     COMPUTE WSAA-TOT-INSTPREM = WSAA-TOT-INSTPREM +          */
		/*                                 COVRMJA-INSTPREM             */
		/*     GO TO 386-UPDATE-STATUS                                  */
		/* ELSE                                                         */
		/*    MOVE ITDM-GENAREA         TO T6630-T6630-REC.             */
		/* Get the Frequency Conversion Multiplication Factor           */
		/* Calculate the new INSTPREM                                   */
		/* Calculate the accumulating INSTPREM for SINSTAMT01           */
		/* MOVE SPACES                  TO WSAA-INSTPREM-FOUND.         */
		/* MOVE 1                        TO SUB.                        */
		/* PERFORM  UNTIL (SUB           > 8)   OR                      */
		/*   (WSAA-INSTPREM-FOUND        = 'Y')                         */
		/*   IF T6630-BILLFREQ(SUB)      = WSAA-AT-BILLFREQ             */
		/*      MOVE T6630-MULTFACT(SUB) TO WSAA-MULTFACT               */
		/*      MOVE 'Y'                 TO WSAA-INSTPREM-FOUND         */
		/*   END-IF                                                     */
		/*   ADD 1                       TO SUB                         */
		/* END-PERFORM.                                                 */
		/* IF WSAA-INSTPREM-FOUND        = 'Y'                          */
		/*    COMPUTE WSAA-TOT-INSTPREM ROUNDED =                       */
		/*                                 WSAA-TOT-INSTPREM +          */
		/*                                 (COVRMJA-INSTPREM *          */
		/*                                 WSAA-MULTFACT)               */
		/*    COMPUTE COVRMJA-INSTPREM ROUNDED =                        */
		/*                                 WSAA-MULTFACT *              */
		/*                                 COVRMJA-INSTPREM             */
		/* ELSE                                                         */
		/*     COMPUTE WSAA-TOT-INSTPREM = WSAA-TOT-INSTPREM +          */
		/*                                 COVRMJA-INSTPREM             */
		/* END-IF.                                                      */
		goTo(GotoLabel.updateStatus386);
	}

protected void wopAccr385()
	{
		if (isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")
		&& isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")) {
			setPrecision(covrmjaIO.getSumins(), 3);
			covrmjaIO.setSumins(mult((div((mult(covrmjaIO.getSumins(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			setPrecision(covrmjaIO.getSumins(), 2);
			covrmjaIO.setSumins(add(covrmjaIO.getSumins(), wsaaNewFreqFee));
		}
		zrdecplrec.amountIn.set(covrmjaIO.getSumins());
		b000CallRounding();
		covrmjaIO.setSumins(zrdecplrec.amountOut);
		wsaaWopSi.set(covrmjaIO.getSumins());
		a200CalcWopPrem();
		wsaaWopAccrFlag = "N";
	}

protected void updateStatus386()
	{
		/*   Update the Risk Status Codes*/
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovRiskStat, SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat, SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
			}
		}
		/*  Update Premium Status Code*/
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovPremStat, SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidPremStat, SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		/*    Write the updated record*/
		/* MOVE PAYR-PAYRSEQNO              TO COVRMJA-PAYRSEQNO.       */
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setTransactionDate(wsaaShortTransDate);
		covrmjaIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setFormat(covrmjarec);
		wsaaMiscellaneousInner.wsaaI.add(1);
		wsaaCrtable[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getCrtable());
		wsaaPremAmt[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getInstprem());
		wsaaLife[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getLife());
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/* Check for a non-refused/reversed historical INCR record      */
		/* for this component.  If one is found, write a new one with   */
		/* the new instalment premium details.                          */
		checkForIncrease600();
		/*   Update the Agent Commission Records   (AGCM)*/
		/*   Retrieve all AGCM records for each valid COVR record read*/
		agcmbchIO.setParams(SPACES);
		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
		agcmbchIO.setLife(covrmjaIO.getLife());
		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
		agcmbchIO.setRider(covrmjaIO.getRider());
		agcmbchIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		agcmbchIO.setFormat(agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmbchIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, agcmbchIO);
		if ((isNE(agcmbchIO.getStatuz(), varcom.oK))
		&& (isNE(agcmbchIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
		/*   Check if valid AGCM record retrived*/
		if (isEQ(agcmbchIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if ((isNE(agcmbchIO.getChdrcoy(), covrmjaIO.getChdrcoy()))
			|| (isNE(agcmbchIO.getChdrnum(), covrmjaIO.getChdrnum()))
			|| (isNE(agcmbchIO.getLife(), covrmjaIO.getLife()))
			|| (isNE(agcmbchIO.getCoverage(), covrmjaIO.getCoverage()))
			|| (isNE(agcmbchIO.getRider(), covrmjaIO.getRider()))
			|| (isNE(agcmbchIO.getPlanSuffix(), covrmjaIO.getPlanSuffix()))) {
				agcmbchIO.setStatuz(varcom.endp);
			}
		}
		/*   Process AGCM records until end of file*/
		while ( !(isEQ(agcmbchIO.getStatuz(), varcom.endp))) {
			processReadAgcmbch390();
		}

	}

protected void findGenericSubroutine387()
	{
		wsaaMiscellaneousInner.wsaaCompCrtable.set(covrmjaIO.getCrtable());
		wsaaMiscellaneousInner.wsaaCompTrancode.set(wsaaBatchkey.batcBatctrcde);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItemitem(wsaaMiscellaneousInner.wsaaCompKey2);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itemIO.getItemcoy(), atmodrec.company)
		|| isNE(itemIO.getItemtabl(), tablesInner.t5671)
		|| isNE(itemIO.getItemitem(), wsaaMiscellaneousInner.wsaaCompKey2)
		|| isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setStatuz(varcom.mrnf);
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		/*    Call all Non-blank Subroutines*/
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			bchgallrec.function.set("BCHG ");
			bchgallrec.company.set(covrmjaIO.getChdrcoy());
			bchgallrec.chdrnum.set(covrmjaIO.getChdrnum());
			bchgallrec.life.set(covrmjaIO.getLife());
			bchgallrec.coverage.set(covrmjaIO.getCoverage());
			bchgallrec.rider.set(covrmjaIO.getRider());
			bchgallrec.planSuffix.set(covrmjaIO.getPlanSuffix());
			bchgallrec.oldBillfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
			bchgallrec.newBillfreq.set(wsaaTransactionRecInner.wsaaAtBillfreq);
			bchgallrec.effdate.set(wsaaTransactionRecInner.wsaaAtToday);
			bchgallrec.tranno.set(covrmjaIO.getTranno());
			bchgallrec.crtable.set(covrmjaIO.getCrtable());
			bchgallrec.trancode.set(wsaaBatchkey.batcBatctrcde);
			bchgallrec.cntcurr.set(covrmjaIO.getSicurr());
			wsaaMiscellaneousInner.sub.set(1);
			while ( !(isGT(wsaaMiscellaneousInner.sub, 4))) {
				if (isNE(t5671rec.subprog[wsaaMiscellaneousInner.sub.toInt()], SPACES)) {
					callProgram(t5671rec.subprog[wsaaMiscellaneousInner.sub.toInt()], bchgallrec.bchgallRec);
					if (isNE(bchgallrec.statuz, varcom.oK)) {
						syserrrec.params.set(bchgallrec.bchgallRec);
						syserrrec.statuz.set(bchgallrec.statuz);
						xxxxFatalError();
					}
				}
				wsaaMiscellaneousInner.sub.add(1);
			}

		}
	}

protected void processCompWop330a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readTr517330a();
					readT5687340a();
					checkStatusAndAction350a();
				case nextrCovr360a:
					nextrCovr360a();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readTr517330a()
	{
		/*    Read Table TR517 to check for WOP riders, If item not found  */
		/*    don't process.                                               */
		wsaaTr517Key.set(covrmjaIO.getCrtable());
		a300ReadTr517();
		if (isEQ(tr517rec.tr517Rec, SPACES)) {
			goTo(GotoLabel.nextrCovr360a);
		}
		else {
			if (isEQ(tr517rec.zrwvflg04, "Y")) {
				goTo(GotoLabel.nextrCovr360a);
			}
		}
	}

	/**
	* <pre>
	*    Read Table T5687 to obtain Single Premium Indicator and
	*    Frequency Alteration Basis.
	* </pre>
	*/
protected void readT5687340a()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkStatusAndAction350a()
	{
		/*    Check if component is to be updated.                         */
		wsaaMiscellaneousInner.wsaaProcessComponent.set("N");
		if (isGT(covrmjaIO.getInstprem(), 0)
		&& isNE(t5687rec.singlePremInd, "Y")) {
			checkComponent360a();
			if (wsaaMiscellaneousInner.processComponent.isTrue()) {
				updateComponent380a();
			}
		}
	}

protected void nextrCovr360a()
	{
		/*    Get next coverage record                                     */
		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		/*A-EXIT*/
	}

protected void checkComponent360a()
	{
		check360a();
	}

protected void check360a()
	{
		/*  Check for match on Premium and Risk Statii for Coverage        */
		wsaaMiscellaneousInner.wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(), "00")) {
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| isEQ(t5679rec.covPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode()))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.covRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
		else {
			/*   Check for match on Premium and Risk Statii for Rider          */
			wsaaMiscellaneousInner.wsaaStatCount.set(1);
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.ridPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
	}

protected void updateComponent380a()
	{
		updateOriginalCovr382a();
		createNewCovr385a();
		updateStatus386a();
	}

protected void updateOriginalCovr382a()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		covrmjaIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void createNewCovr385a()
	{
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaTransactionRecInner.wsaaAtBillfreq);
		wsaaWopSi.set(ZERO);
		wsaaOldFreqFee.set(ZERO);
		wsaaNewFreqFee.set(ZERO);
		if (isNE(t5688rec.feemeth, SPACES)) {
			a400ReadT5567();
		}
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 10)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(wsaaCrtable[wsaaMiscellaneousInner.wsaaSub.toInt()], SPACES)) {
				wsaaMiscellaneousInner.wsaaSub.set(11);
			}
			else {
				a100CheckCodeWaiver();
			}
		}
		if (isGT(wsaaWopSi, 0)) {
			if (isEQ(tr517rec.zrwvflg03, "Y")) {
				wsaaWopSi.add(wsaaNewFreqFee);
			}
			a200CalcWopPrem();
			covrmjaIO.setSumins(wsaaWopSi);
		}
	}

protected void updateStatus386a()
	{
		/*   Update the Risk Status Codes                                  */
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovRiskStat, SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat, SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
			}
		}
		/*  Update Premium Status Code                                     */
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovPremStat, SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidPremStat, SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		/*    Write the updated record                                     */
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setTransactionDate(wsaaShortTransDate);
		covrmjaIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*   Update the Agent Commission Records   (AGCM)                  */
		/*   Retrieve all AGCM records for each valid COVR record read     */
		agcmbchIO.setParams(SPACES);
		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
		agcmbchIO.setLife(covrmjaIO.getLife());
		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
		agcmbchIO.setRider(covrmjaIO.getRider());
		agcmbchIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		agcmbchIO.setFormat(agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmbchIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
		/*   Check if valid AGCM record retrived                           */
		if (isEQ(agcmbchIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isNE(agcmbchIO.getChdrcoy(), covrmjaIO.getChdrcoy())
			|| isNE(agcmbchIO.getChdrnum(), covrmjaIO.getChdrnum())
			|| isNE(agcmbchIO.getLife(), covrmjaIO.getLife())
			|| isNE(agcmbchIO.getCoverage(), covrmjaIO.getCoverage())
			|| isNE(agcmbchIO.getRider(), covrmjaIO.getRider())
			|| isNE(agcmbchIO.getPlanSuffix(), covrmjaIO.getPlanSuffix())) {
				agcmbchIO.setStatuz(varcom.endp);
			}
		}
		/*   Process AGCM records until end of file                        */
		while ( !(isEQ(agcmbchIO.getStatuz(), varcom.endp))) {
			processReadAgcmbch390();
		}

	}

protected void processReadAgcmbch390()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					setUpDataArea391();
					createNewRecord397();
				case retrieveNextRecord393:
					retrieveNextRecord393();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*   Set up Fields for AGCM
	* </pre>
	*/
protected void setUpDataArea391()
	{
		/**    MOVE AGCMBCH-CHDRCOY             TO AGCM-CHDRCOY.            */
		/**    MOVE AGCMBCH-CHDRNUM             TO AGCM-CHDRNUM.            */
		/**    MOVE AGCMBCH-LIFE                TO AGCM-LIFE.               */
		/**    MOVE AGCMBCH-COVERAGE            TO AGCM-COVERAGE.           */
		/**    MOVE AGCMBCH-RIDER               TO AGCM-RIDER.              */
		/**    MOVE AGCMBCH-PLAN-SUFFIX         TO AGCM-PLAN-SUFFIX.        */
		/**    MOVE AGCMBCH-AGNTNUM             TO AGCM-AGNTNUM.            */
		/**    MOVE AGCMBCH-ANNPREM             TO AGCM-ANNPREM.            */
		/**    MOVE AGCMBCH-COMPAY              TO AGCM-COMPAY              */
		/**    MOVE AGCMBCH-CURRFROM            TO AGCM-CURRFROM.           */
		/**    MOVE AGCMBCH-INITCOM             TO AGCM-INITCOM.            */
		/**    MOVE AGCMBCH-TRANNO              TO AGCM-TRANNO.             */
		/**    MOVE AGCMBCH-EFDATE              TO AGCM-EFDATE.             */
		/**    MOVE AGCMBCH-COMERN              TO AGCM-COMERN.             */
		/**    MOVE AGCMBCH-COMPAY              TO AGCM-COMPAY.             */
		/**    MOVE AGCMBCH-BASCPY              TO AGCM-BASCPY.             */
		/**    MOVE AGCMBCH-BASIC-COMM-METH     TO AGCM-BASIC-COMM-METH.    */
		/**    MOVE AGCMBCH-SRVCPY              TO AGCM-SRVCPY.             */
		/**    MOVE AGCMBCH-SCMDUE              TO AGCM-SCMDUE.             */
		/**    MOVE AGCMBCH-SCMEARN             TO AGCM-SCMEARN.            */
		/**    MOVE AGCMBCH-RNWCPY              TO AGCM-RNWCPY.             */
		/**    MOVE AGCMBCH-RNLCDUE             TO AGCM-RNLCDUE.            */
		/**    MOVE AGCMBCH-RNLCEARN            TO AGCM-RNLCEARN.           */
		/**    MOVE AGCMBCH-AGENT-CLASS         TO AGCM-AGENT-CLASS.        */
		/**    MOVE AGCMBCH-PTDATE              TO AGCM-PTDATE.             */
		/**    MOVE AGCMBCH-SEQNO               TO AGCM-SEQNO.              */
		/**    MOVE AGCMBCH-CEDAGENT            TO AGCM-CEDAGENT.           */
		/**    MOVE AGCMBCH-OVRDCAT             TO AGCM-OVRDCAT.            */
		/**    MOVE AGCMBCH-TRANSACTION-DATE    TO AGCM-TRANSACTION-DATE.   */
		/**    MOVE AGCMBCH-TRANSACTION-TIME    TO AGCM-TRANSACTION-TIME.   */
		/**    MOVE AGCMBCH-USER                TO AGCM-USER.               */
		/**   Get next AGCM record                                          */
		/**393-RETRIEVE-NEXT-RECORD.                                        */
		/**    MOVE NEXTR                       TO AGCMBCH-FUNCTION.        */
		/**    CALL 'AGCMBCHIO'                 USING AGCMBCH-PARAMS.       */
		/**    IF (AGCMBCH-STATUZ            NOT = O-K) AND                 */
		/**       (AGCMBCH-STATUZ            NOT = ENDP)                    */
		/**       MOVE AGCMBCH-PARAMS           TO SYSR-PARAMS              */
		/**       MOVE AGCMBCH-STATUZ           TO SYSR-STATUZ              */
		/**       PERFORM XXXX-FATAL-ERROR                                  */
		/**    END-IF.                                                      */
		/**   Check if valid record retrieved                               */
		/**    IF AGCMBCH-STATUZ                 = ENDP                     */
		/**       NEXT SENTENCE                                             */
		/**    ELSE                                                         */
		/**       IF (COVRMJA-CHDRCOY        NOT = AGCMBCH-CHDRCOY)     OR  */
		/**          (COVRMJA-CHDRNUM        NOT = AGCMBCH-CHDRNUM)     OR  */
		/**          (COVRMJA-LIFE           NOT = AGCMBCH-LIFE)        OR  */
		/**          (COVRMJA-COVERAGE       NOT = AGCMBCH-COVERAGE)    OR  */
		/**          (COVRMJA-RIDER          NOT = AGCMBCH-RIDER)       OR  */
		/**          (COVRMJA-PLAN-SUFFIX    NOT = AGCMBCH-PLAN-SUFFIX)     */
		/**          MOVE ENDP                  TO AGCMBCH-STATUZ           */
		/**       END-IF                                                    */
		/**    END-IF.                                                      */
		/**   Update original AGCM records                                  */
		/*UPDATE-OLD-RECORD*/
		/*    MOVE WSAA-AT-TODAY               TO AGCM-CURRTO.             */
		/*    MOVE '2'                         TO AGCM-VALIDFLAG.          */
		/*    MOVE UPDAT                       TO AGCM-FUNCTION.           */
		/*    CALL 'AGCMIO'                    USING AGCM-PARAMS.          */
		/*    IF AGCM-STATUZ                NOT = O-K                      */
		/*       MOVE AGCM-PARAMS              TO SYSR-PARAMS              */
		/*       MOVE AGCM-STATUZ              TO SYSR-STATUZ              */
		/*       PERFORM XXXX-FATAL-ERROR                                  */
		/*    END-IF.                                                      */
		/* MOVE WSAA-AT-TODAY               TO AGCMBCH-CURRTO.     <014>*/
		/* Do not process single premium AGCMs.                         */
		if (isEQ(agcmbchIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.retrieveNextRecord393);
		}
		agcmbchIO.setCurrto(payrIO.getBtdate());
		agcmbchIO.setValidflag("2");
		/* MOVE UPDAT                       TO AGCMBCH-FUNCTION.<A05940>*/
		agcmbchIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	*   Create new AGCM record
	* </pre>
	*/
protected void createNewRecord397()
	{
		/*   Calculate new annual premium*/
		/*   The COVR calculation for the new premium uses the following   */
		/*   formula:                                                      */
		/*   X = P*(A/C)*(D/B)   where P = the old premium                 */
		/*                             A = old frequency                   */
		/*                             B = old loading factor              */
		/*                             C = new frequency                   */
		/*                             D = new loading factor              */
		/*                             X = new premium                     */
		/*   the new annualised premium, Y, should therefore be X*C so     */
		/*   Y = P*(A/C)*(D/B)*C                                           */
		/*     = P*A*(D/B)                                                 */
		/*     = Z*(D/B)         where Z = the old annualised premium      */
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaTransactionRecInner.wsaaAtBillfreq);
		wsaaMiscellaneousInner.wsaaCalcOldfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
		if (isEQ(wsaaMiscellaneousInner.wsaaInstpremFound, "Y")) {
			/*    COMPUTE AGCM-ANNPREM ROUNDED  = (AGCM-ANNPREM /           */
			setPrecision(agcmbchIO.getAnnprem(), 3);
			agcmbchIO.setAnnprem(mult(agcmbchIO.getAnnprem(), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
		}
		zrdecplrec.amountIn.set(agcmbchIO.getAnnprem());
		b000CallRounding();
		agcmbchIO.setAnnprem(zrdecplrec.amountOut);
		/*                                    WSAA-CALC-OLDFREQ)  *     */
		/*                                    WSAA-MULTFACT *           */
		/*                                    WSAA-NEW-LFACT *          */
		/*                                    WSAA-CALC-NEWFREQ         */
		/* END-IF.                                                      */
		/*   Write new AGCM record*/
		/* MOVE WSAA-AT-TRANSACTION-DATE    TO AGCM-EFDATE.        <008>*/
		/*MOVE WSAA-SHORT-TRANS-DATE       TO AGCM-TRANSACTION-DATE.  <008>*/
		/*MOVE WSAA-AT-TRANSACTION-TIME    TO AGCM-TRANSACTION-TIME.  <008>*/
		/*MOVE WSAA-AT-USER                TO AGCM-USER.              <008>*/
		/*    MOVE CHDRMJA-TRANNO              TO AGCM-TRANNO.             */
		/*    MOVE '1'                         TO AGCM-VALIDFLAG.          */
		/*    MOVE 99999999                    TO AGCM-CURRTO.             */
		/*    MOVE CHDRMJA-BTDATE              TO AGCM-CURRFROM.           */
		/*    MOVE AGCMREC                     TO AGCM-FORMAT.             */
		/*    MOVE WRITR                       TO AGCM-FUNCTION.           */
		/*    CALL 'AGCMIO'                    USING AGCM-PARAMS.          */
		/*    IF AGCM-STATUZ                NOT = O-K                      */
		/*       MOVE AGCM-PARAMS              TO SYSR-PARAMS              */
		/*       MOVE AGCM-STATUZ              TO SYSR-STATUZ              */
		/*       PERFORM XXXX-FATAL-ERROR                                  */
		/*    END-IF.                                                      */
		agcmbchIO.setTransactionDate(wsaaShortTransDate);
		agcmbchIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		agcmbchIO.setUser(wsaaTransactionRecInner.wsaaAtUser);
		agcmbchIO.setTranno(chdrmjaIO.getTranno());
		agcmbchIO.setValidflag("1");
		agcmbchIO.setCurrto(99999999);
		/* MOVE CHDRMJA-BTDATE              TO AGCMBCH-CURRFROM.   <014>*/
		agcmbchIO.setCurrfrom(payrIO.getBtdate());
		agcmbchIO.setFormat(agcmbchrec);
		agcmbchIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void retrieveNextRecord393()
	{
		agcmbchIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if ((isNE(agcmbchIO.getStatuz(), varcom.oK))
		&& (isNE(agcmbchIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			xxxxFatalError();
		}
		/*   Check if valid record retrieved                               */
		if (isEQ(agcmbchIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if ((isNE(covrmjaIO.getChdrcoy(), agcmbchIO.getChdrcoy()))
			|| (isNE(covrmjaIO.getChdrnum(), agcmbchIO.getChdrnum()))
			|| (isNE(covrmjaIO.getLife(), agcmbchIO.getLife()))
			|| (isNE(covrmjaIO.getCoverage(), agcmbchIO.getCoverage()))
			|| (isNE(covrmjaIO.getRider(), agcmbchIO.getRider()))
			|| (isNE(covrmjaIO.getPlanSuffix(), agcmbchIO.getPlanSuffix()))) {
				agcmbchIO.setStatuz(varcom.endp);
			}
		}
		if (isEQ(agcmbchIO.getTranno(), chdrmjaIO.getTranno())
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.retrieveNextRecord393);
		}
	}

protected void accumInstpremWrtChdrmja400()
	{
		accumInstPrem410();
	}

protected void accumInstPrem410()
	{
		/*    Read the contract definition details T5688 for the contract*/
		/*    type held on the contract header.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(f290);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		if (isNE(t5688rec.feemeth, SPACES)) {
			calcFee1200();
		}
		chdrmjaIO.setSinstamt01(wsaaMiscellaneousInner.wsaaTotInstprem);
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(), chdrmjaIO.getSinstamt02()), chdrmjaIO.getSinstamt03()), chdrmjaIO.getSinstamt04()), chdrmjaIO.getSinstamt05()));
		chdrmjaIO.setFunction(varcom.writr);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		/*  Write the PAYR record.                                         */
		payrIO.setSinstamt01(wsaaMiscellaneousInner.wsaaTotInstprem);
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), chdrmjaIO.getSinstamt03()), chdrmjaIO.getSinstamt04()), chdrmjaIO.getSinstamt05()));
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void writeHistoryRlseLocks500()
	{
		writePtrnRecord510();
		updateBatchHeader530();
		releaseSoftlock550();
	}

protected void writePtrnRecord510()
	{
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTermid(wsaaTermid);
		/*MOVE WSAA-AT-TRANSACTION-DATE    TO PTRN-TRANSACTION-DATE.   */
		ptrnIO.setTransactionDate(wsaaShortTransDate);
		ptrnIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(wsaaTransactionRecInner.wsaaAtToday);
		ptrnIO.setDatesub(wsaaTransactionRecInner.wsaaAtToday);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void updateBatchHeader530()
	{
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/* Maintain Diary entries if required.                             */
		dryProcessing2000();
	}

protected void releaseSoftlock550()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.user.set(wsaaTransactionRecInner.wsaaAtUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void checkForIncrease600()
	{
		begn610();
	}

protected void begn610()
	{
		incrhstIO.setParams(SPACES);
		incrhstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		incrhstIO.setChdrnum(covrmjaIO.getChdrnum());
		incrhstIO.setLife(covrmjaIO.getLife());
		incrhstIO.setCoverage(covrmjaIO.getCoverage());
		incrhstIO.setRider(covrmjaIO.getRider());
		incrhstIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		incrhstIO.setFormat(incrhstrec);
		incrhstIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, incrhstIO);
		if (isNE(incrhstIO.getStatuz(), varcom.oK)
		&& isNE(incrhstIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(incrhstIO.getParams());
			syserrrec.statuz.set(incrhstIO.getStatuz());
			xxxxFatalError();
		}
		/* If no record is found, do no further Increase processing.    */
		if (isEQ(incrhstIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/* Write a new historical INCR record with the instalment       */
		/* premium fields updated in line with the change in billing    */
		/* frequency.                                                   */
		if (isEQ(wsaaMiscellaneousInner.wsaaInstpremFound, "Y")) {
			/*COMPUTE INCRHST-ORIG-INST ROUNDED = INCRHST-ORIG-INST  <LA3339>*/
			/*     * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)         <LA3339>*/
			setPrecision(incrhstIO.getOrigInst(), 3);
			incrhstIO.setOrigInst(mult((div((mult(incrhstIO.getOrigInst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getOrigInst());
			b000CallRounding();
			incrhstIO.setOrigInst(zrdecplrec.amountOut);
			/*COMPUTE INCRHST-ZBORIGINST ROUNDED = INCRHST-ZBORIGINST<LA3339>*/
			/*      * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)        <LA3339>*/
			setPrecision(incrhstIO.getZboriginst(), 3);
			incrhstIO.setZboriginst(mult((div((mult(incrhstIO.getZboriginst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getZboriginst());
			b000CallRounding();
			incrhstIO.setZboriginst(zrdecplrec.amountOut);
			/*COMPUTE INCRHST-ZLORIGINST ROUNDED = INCRHST-ZLORIGINST<LA3339>*/
			/*        * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)      <LA3339>*/
			setPrecision(incrhstIO.getZloriginst(), 3);
			incrhstIO.setZloriginst(mult((div((mult(incrhstIO.getZloriginst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getZloriginst());
			b000CallRounding();
			incrhstIO.setZloriginst(zrdecplrec.amountOut);
			/* COMPUTE INCRHST-LAST-INST ROUNDED = INCRHST-LAST-INST <LA3339>*/
			/*         * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)     <LA3339>*/
			setPrecision(incrhstIO.getLastInst(), 3);
			incrhstIO.setLastInst(mult((div((mult(incrhstIO.getLastInst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getLastInst());
			b000CallRounding();
			incrhstIO.setLastInst(zrdecplrec.amountOut);
			/*COMPUTE INCRHST-ZBLASTINST ROUNDED = INCRHST-ZBLASTINST<LA3339>*/
			/*       * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)       <LA3339>*/
			setPrecision(incrhstIO.getZblastinst(), 3);
			incrhstIO.setZblastinst(mult((div((mult(incrhstIO.getZblastinst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getZblastinst());
			b000CallRounding();
			incrhstIO.setZblastinst(zrdecplrec.amountOut);
			/*COMPUTE INCRHST-ZLLASTINST ROUNDED = INCRHST-ZLLASTINST<LA3339>*/
			/*      * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)        <LA3339>*/
			setPrecision(incrhstIO.getZllastinst(), 3);
			incrhstIO.setZllastinst(mult((div((mult(incrhstIO.getZllastinst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getZllastinst());
			b000CallRounding();
			incrhstIO.setZllastinst(zrdecplrec.amountOut);
			/*      COMPUTE INCRHST-NEWINST ROUNDED = INCRHST-NEWINST<LA3339>*/
			/*              * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)<LA3339>*/
			setPrecision(incrhstIO.getNewinst(), 3);
			incrhstIO.setNewinst(mult((div((mult(incrhstIO.getNewinst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getNewinst());
			b000CallRounding();
			incrhstIO.setNewinst(zrdecplrec.amountOut);
			/*  COMPUTE INCRHST-ZBNEWINST ROUNDED = INCRHST-ZBNEWINST<LA3339>*/
			/*          * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)    <LA3339>*/
			setPrecision(incrhstIO.getZbnewinst(), 3);
			incrhstIO.setZbnewinst(mult((div((mult(incrhstIO.getZbnewinst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getZbnewinst());
			b000CallRounding();
			incrhstIO.setZbnewinst(zrdecplrec.amountOut);
			/*  COMPUTE INCRHST-ZLNEWINST ROUNDED = INCRHST-ZLNEWINST <LA3339>*/
			/*         * (WSAA-CALC-OLDFREQ / WSAA-CALC-NEWFREQ)      <LA3339>*/
			setPrecision(incrhstIO.getZlnewinst(), 3);
			incrhstIO.setZlnewinst(mult((div((mult(incrhstIO.getZlnewinst(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			zrdecplrec.amountIn.set(incrhstIO.getZlnewinst());
			b000CallRounding();
			incrhstIO.setZlnewinst(zrdecplrec.amountOut);
		}
		incrhstIO.setTranno(chdrmjaIO.getTranno());
		incrhstIO.setStatcode(covrmjaIO.getStatcode());
		incrhstIO.setPstatcode(covrmjaIO.getPstatcode());
		incrhstIO.setTransactionDate(wsaaShortTransDate);
		incrhstIO.setTransactionTime(wsaaTransactionRecInner.wsaaAtTransactionTime);
		incrhstIO.setTermid(wsaaTransactionRecInner.wsaaAtTermid);
		incrhstIO.setUser(wsaaTransactionRecInner.wsaaAtUser);
		incrhstIO.setFormat(incrhstrec);
		incrhstIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrhstIO);
		if (isNE(incrhstIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(incrhstIO.getStatuz());
			syserrrec.params.set(incrhstIO.getParams());
			xxxxFatalError();
		}
	}

protected void writeLetter710()
	{
		try {
			readT6634711();
			setUpParm713();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readT6634711()
	{
		wsaaItemBatctrcde.set(wsaaBatchkey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			readT6634Again720();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		/* IF   T6634-LETTER-TYPE           =  SPACE            <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			goTo(GotoLabel.exit719);
		}
	}

protected void setUpParm713()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaTransactionRecInner.wsaaAtToday);
		/* MOVE 'EN'                   TO LETRQST-RDOCPFX.      <PCPPRT>*/
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(atmodrec.company);
		/* MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.      <PCPPRT>*/
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		/*   MOVE SPACE                  TO WSAA-OKEYS.                   */
		wsaaOkey1.set(atmodrec.language);
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.function.set("ADD");
		/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			xxxxFatalError();
		}
	}

protected void readT6634Again720()
	{
		start721();
	}

protected void start721()
	{
		wsaaItemCnttype.set("***");
		wsaaItemBatctrcde.set(wsaaBatchkey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
	}

protected void calcFee1200()
	{
		readSubroutineTable1210();
	}

protected void readSubroutineTable1210()
	{
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(f151);
			return ;
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		/* Check subroutine NOT = SPACES before attempting call.*/
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrmjaIO.getCnttype());
		mgfeelrec.billfreq.set(chdrmjaIO.getBillfreq());
		mgfeelrec.effdate.set(chdrmjaIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrmjaIO.getCntcurr());
		mgfeelrec.company.set(chdrmjaIO.getChdrcoy());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			xxxxFatalError();
		}
		chdrmjaIO.setSinstamt02(mgfeelrec.mgfee);
		payrIO.setSinstamt02(mgfeelrec.mgfee);
	}

protected void dryProcessing2000()
	{
		start2010();
	}

protected void start2010()
	{
		/* This section will determine if the DIARY system is active       */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatchkey.batcBatctrcde);
		readT75082100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75082100();
		}
		/* If item not found no Batch Diary Processing is Required.        */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatchkey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatchkey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatchkey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaTransactionRecInner.wsaaAtFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(covrmjaIO.getCpiDate());
		drypDryprcRecInner.drypBbldate.set(covrmjaIO.getBenBillDate());
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrmjaIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75082100()
	{
		start2110();
	}

protected void start2110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatchkey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatchkey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatchkey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatchkey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatchkey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void a100CheckCodeWaiver()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case a110Check:
					a110Check();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Check()
	{
		wsaaWaiverFlag = "N";
		for (wsaaMiscellaneousInner.wsaaI.set(1); !(isGT(wsaaMiscellaneousInner.wsaaI, 50)
		|| isEQ(wsaaWaiverFlag, "Y")); wsaaMiscellaneousInner.wsaaI.add(1)){
			if (isNE(tr517rec.zrwvflg02, "Y")
			&& isNE(wsaaLife[wsaaMiscellaneousInner.wsaaSub.toInt()], covrmjaIO.getLife())) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isEQ(wsaaCrtable[wsaaMiscellaneousInner.wsaaSub.toInt()], tr517rec.ctable[wsaaMiscellaneousInner.wsaaI.toInt()])) {
					wsaaWopSi.add(wsaaPremAmt[wsaaMiscellaneousInner.wsaaSub.toInt()]);
					wsaaWaiverFlag = "Y";
				}
			}
		}
		if (isEQ(wsaaWaiverFlag, "N")
		&& isNE(tr517rec.contitem, SPACES)) {
			wsaaTr517Key.set(tr517rec.contitem);
			a300ReadTr517();
			goTo(GotoLabel.a110Check);
		}
	}

protected void a200CalcWopPrem()
	{
		a210Lext();
		a220Calc();
		a230WithoutLoading();
		a240WithLoading();
	}

protected void a210Lext()
	{
		wsaaAgerateTot.set(ZERO);
		wsaaRatesPerMillieTot.set(ZERO);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 8)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()].set(ZERO);
		}
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lextIO.setChdrnum(covrmjaIO.getChdrnum());
		lextIO.setLife(covrmjaIO.getLife());
		lextIO.setCoverage(covrmjaIO.getCoverage());
		lextIO.setRider(covrmjaIO.getRider());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, lextIO);
			if (isNE(lextIO.getStatuz(), varcom.oK)
			&& isNE(lextIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(lextIO.getParams());
				xxxxFatalError();
			}
			if (isNE(lextIO.getChdrcoy(), covrmjaIO.getChdrcoy())
			|| isNE(lextIO.getChdrnum(), covrmjaIO.getChdrnum())
			|| isNE(lextIO.getLife(), covrmjaIO.getLife())
			|| isNE(lextIO.getCoverage(), covrmjaIO.getCoverage())
			|| isNE(lextIO.getRider(), covrmjaIO.getRider())
			|| isNE(lextIO.getStatuz(), varcom.endp)) {
				lextIO.setStatuz(varcom.endp);
			}
			else {
				if (isEQ(lextIO.getReasind(), "1")
				&& isLTE(wsaaTransactionRecInner.wsaaAtToday, lextIO.getExtCessDate())) {
					wsaaMiscellaneousInner.wsaaSub.add(1);
					wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()].set(lextIO.getOppc());
					wsaaRatesPerMillieTot.add(lextIO.getInsprm());
					wsaaAgerateTot.add(lextIO.getAgerate());
				}
				lextIO.setFunction(varcom.nextr);
			}
		}

	}

protected void a220Calc()
	{
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5664);
		wsaaT5664Crtable.set(covrmjaIO.getCrtable());
		wsaaT5664Mortcls.set(covrmjaIO.getMortcls());
		wsaaT5664Sex.set(covrmjaIO.getSex());
		itdmIO.setItemitem(wsaaT5664Key);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(wsaaT5664Key, itdmIO.getItemitem())
		|| isNE(covrmjaIO.getChdrcoy(), itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5664)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(wsaaT5664Key);
			syserrrec.statuz.set(f358);
			xxxxFatalError();
		}
		else {
			t5664rec.t5664Rec.set(itdmIO.getGenarea());
		}
	}

protected void a230WithoutLoading()
	{
		if (isEQ(covrmjaIO.getAnbAtCcd(), 0)){
			compute(wsaaRate, 2).set(div(t5664rec.insprem, t5664rec.premUnit));
			/**      WHEN 100                                           <V73L03>*/
			/**       COMPUTE WSAA-RATE = T5664-INSTPR / T5664-PREM-UNIT<V73L03>*/
			/**  Extend the age band to 110.                                    */
		}
		else if (isGTE(covrmjaIO.getAnbAtCcd(), 100)
		&& isLTE(covrmjaIO.getAnbAtCcd(), 110)){
			compute(wsaaRate, 2).set(div(t5664rec.instpr[sub(covrmjaIO.getAnbAtCcd(), 99).toInt()], t5664rec.premUnit));
		}
		else{
			compute(wsaaRate, 2).set(div(t5664rec.insprm[covrmjaIO.getAnbAtCcd().toInt()], t5664rec.premUnit));
		}
		compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopSi, wsaaRate)), t5664rec.unit)));
		zrdecplrec.amountIn.set(wsaaWopPrem);
		b000CallRounding();
		wsaaWopPrem.set(zrdecplrec.amountOut);
		covrmjaIO.setZbinstprem(wsaaWopPrem);
	}

protected void a240WithLoading()
	{
		compute(wsaaAdjustedAge, 0).set(add(wsaaAgerateTot, covrmjaIO.getAnbAtCcd()));
		if (isEQ(wsaaAdjustedAge, 0)){
			compute(wsaaRate, 2).set(div(t5664rec.insprem, t5664rec.premUnit));
			/**      WHEN 100                                           <V73L03>*/
			/**       COMPUTE WSAA-RATE = T5664-INSTPR / T5664-PREM-UNIT<V73L03>*/
			/**  Extend the age band to 110.                                    */
		}
		else if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)){
			compute(wsaaRate, 2).set(div(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], t5664rec.premUnit));
		}
		else{
			compute(wsaaRate, 2).set(div(t5664rec.insprm[wsaaAdjustedAge.toInt()], t5664rec.premUnit));
		}
		compute(wsaaRate, 2).set(add(wsaaRatesPerMillieTot, wsaaRate));
		compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopSi, wsaaRate)), t5664rec.unit)));
		zrdecplrec.amountIn.set(wsaaWopPrem);
		b000CallRounding();
		wsaaWopPrem.set(zrdecplrec.amountOut);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 8)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isNE(wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()], 0)) {
				compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopPrem, wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()])), 100)));
			}
		}
		zrdecplrec.amountIn.set(wsaaWopPrem);
		b000CallRounding();
		wsaaWopPrem.set(zrdecplrec.amountOut);
		covrmjaIO.setInstprem(wsaaWopPrem);
		setPrecision(covrmjaIO.getZlinstprem(), 2);
		covrmjaIO.setZlinstprem(sub(covrmjaIO.getInstprem(), covrmjaIO.getZbinstprem()));
		compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
	}

protected void a300ReadTr517()
	{
		a310Read();
	}

protected void a310Read()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(wsaaTr517Key);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemitem(), wsaaTr517Key)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			tr517rec.tr517Rec.set(SPACES);
		}
		else {
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
		}
	}

protected void a400ReadT5567()
	{
		a410Read();
	}

protected void a410Read()
	{
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5567);
		wsaaT5567Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT5567Cntcurr.set(chdrmjaIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5567Key);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5567)
		|| isNE(itdmIO.getItemitem(), wsaaT5567Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT5567Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h966);
			xxxxFatalError();
		}
		else {
			t5567rec.t5567Rec.set(itdmIO.getGenarea());
		}
		wsaaNewFreqFee.set(ZERO);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 10)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5567rec.billfreq[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaTransactionRecInner.wsaaAtBillfreq)) {
				wsaaNewFreqFee.set(t5567rec.cntfee[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaSub.set(11);
			}
		}
	}

protected void b000CallRounding()
	{
		/*B100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(atmodrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			xxxxFatalError();
		}
		/*B900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-REC--INNER
 */
private static final class WsaaTransactionRecInner {

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(67);
	private PackedDecimalData wsaaAtTransactionDate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaAtTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 5);
	private PackedDecimalData wsaaAtUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 9);
	private FixedLengthStringData wsaaAtTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 13);
	private FixedLengthStringData wsaaAtBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 17);
	private FixedLengthStringData wsaaAtBillchnl = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData wsaaAtBillday = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 21);
	private FixedLengthStringData wsaaAtPayrnum = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 23);
	private FixedLengthStringData wsaaAtMandref = new FixedLengthStringData(5).isAPartOf(wsaaTransactionRec, 31);
	private FixedLengthStringData wsaaAtGrupkey = new FixedLengthStringData(12).isAPartOf(wsaaTransactionRec, 36);
	private FixedLengthStringData wsaaAtMembsel = new FixedLengthStringData(10).isAPartOf(wsaaTransactionRec, 48);
	private ZonedDecimalData wsaaAtToday = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 58);
	private FixedLengthStringData wsaaAtFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 66);
}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner {
		/* WSAA-MISCELLANEOUS */
	private ZonedDecimalData sub = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaStatCount = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaCompKey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCompFreq = new FixedLengthStringData(2).isAPartOf(wsaaCompKey, 4);

	private FixedLengthStringData wsaaCompKey2 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompTrancode = new FixedLengthStringData(4).isAPartOf(wsaaCompKey2, 0);
	private FixedLengthStringData wsaaCompCrtable = new FixedLengthStringData(4).isAPartOf(wsaaCompKey2, 4);

	private FixedLengthStringData wsaaCalcFreqs = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaCalcOldfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalcFreqs, 0).setUnsigned();
	private ZonedDecimalData wsaaCalcNewfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalcFreqs, 2).setUnsigned();

	private FixedLengthStringData wsaaProcessComponent = new FixedLengthStringData(1);
	private Validator processComponent = new Validator(wsaaProcessComponent, "Y");
	private Validator notProcessComponent = new Validator(wsaaProcessComponent, "N");
	private FixedLengthStringData wsaaInstpremFound = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTotInstprem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaNewFreqFound = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOldFreqFound = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaNewLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
	private FixedLengthStringData t5541 = new FixedLengthStringData(6).init("T5541");
	private FixedLengthStringData t5679 = new FixedLengthStringData(6).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");
	private FixedLengthStringData t5671 = new FixedLengthStringData(6).init("T5671");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
	private FixedLengthStringData t6654 = new FixedLengthStringData(5).init("T6654");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData t5664 = new FixedLengthStringData(5).init("T5664");
	private FixedLengthStringData t5567 = new FixedLengthStringData(5).init("T5567");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
