package com.csc.life.contractservicing.dataaccess;

import com.csc.fsu.financials.dataaccess.RtrnpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RtrnrevTableDAM.java
 * Date: Sun, 30 Aug 2009 03:46:32
 * Class transformed from RTRNREV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RtrnrevTableDAM extends RtrnpfTableDAM {

	public RtrnrevTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("RTRNREV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "RLDGCOY"
		             + ", RDOCNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "BATCPFX, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "RDOCPFX, " +
		            "RDOCCOY, " +
		            "RDOCNUM, " +
		            "RLDGPFX, " +
		            "RLDGCOY, " +
		            "RLDGACCT, " +
		            "GENLPFX, " +
		            "GENLCOY, " +
		            "GENLCUR, " +
		            "GLCODE, " +
		            "GLSIGN, " +
		            "BANKCODE, " +
		            "BANKKEY, " +
		            "POSTMONTH, " +
		            "POSTYEAR, " +
		            "EFFDATE, " +
		            "SACSTYP, " +
		            "SACSCODE, " +
		            "TRANSEQ, " +
		            "TRANDATE, " +
		            "TRANTIME, " +
		            "TRANDESC, " +
		            "ORIGAMT, " +
		            "ACCTAMT, " +
		            "ORIGCCY, " +
		            "CRATE, " +
		            "TRANNO, " +
		            "STCA, " +
		            "STCB, " +
		            "STCC, " +
		            "STCD, " +
		            "STCE, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "ACCPFX, " +
		            "ACCCOY, " +
		            "ACCNO, " +
		            "ACCTYP, " +
		            "PRTFLG, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "RLDGCOY ASC, " +
		            "RDOCNUM ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "RLDGCOY DESC, " +
		            "RDOCNUM DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               batcpfx,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               rdocpfx,
                               rdoccoy,
                               rdocnum,
                               rldgpfx,
                               rldgcoy,
                               rldgacct,
                               genlpfx,
                               genlcoy,
                               genlcur,
                               glcode,
                               glsign,
                               bankcode,
                               bankkey,
                               postmonth,
                               postyear,
                               effdate,
                               sacstyp,
                               sacscode,
                               transeq,
                               trandate,
                               trantime,
                               trandesc,
                               origamt,
                               acctamt,
                               origccy,
                               crate,
                               tranno,
                               chdrstcda,
                               chdrstcdb,
                               chdrstcdc,
                               chdrstcdd,
                               chdrstcde,
                               life,
                               coverage,
                               rider,
                               accpfx,
                               acccoy,
                               accno,
                               acctyp,
                               prtflg,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(51);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getRldgcoy().toInternal()
					+ getRdocnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, rldgcoy);
			what = ExternalData.chop(what, rdocnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(9);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller340 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller100.setInternal(rdocnum.toInternal());
	nonKeyFiller120.setInternal(rldgcoy.toInternal());
	nonKeyFiller340.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(257);
		
		nonKeyData.set(
					getBatcpfx().toInternal()
					+ getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getRdocpfx().toInternal()
					+ getRdoccoy().toInternal()
					+ nonKeyFiller100.toInternal()
					+ getRldgpfx().toInternal()
					+ nonKeyFiller120.toInternal()
					+ getRldgacct().toInternal()
					+ getGenlpfx().toInternal()
					+ getGenlcoy().toInternal()
					+ getGenlcur().toInternal()
					+ getGlcode().toInternal()
					+ getGlsign().toInternal()
					+ getBankcode().toInternal()
					+ getBankkey().toInternal()
					+ getPostmonth().toInternal()
					+ getPostyear().toInternal()
					+ getEffdate().toInternal()
					+ getSacstyp().toInternal()
					+ getSacscode().toInternal()
					+ getTranseq().toInternal()
					+ getTrandate().toInternal()
					+ getTrantime().toInternal()
					+ getTrandesc().toInternal()
					+ getOrigamt().toInternal()
					+ getAcctamt().toInternal()
					+ getOrigccy().toInternal()
					+ getCrate().toInternal()
					+ nonKeyFiller340.toInternal()
					+ getChdrstcda().toInternal()
					+ getChdrstcdb().toInternal()
					+ getChdrstcdc().toInternal()
					+ getChdrstcdd().toInternal()
					+ getChdrstcde().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getAccpfx().toInternal()
					+ getAcccoy().toInternal()
					+ getAccno().toInternal()
					+ getAcctyp().toInternal()
					+ getPrtflg().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, batcpfx);
			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, rdocpfx);
			what = ExternalData.chop(what, rdoccoy);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, rldgpfx);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, rldgacct);
			what = ExternalData.chop(what, genlpfx);
			what = ExternalData.chop(what, genlcoy);
			what = ExternalData.chop(what, genlcur);
			what = ExternalData.chop(what, glcode);
			what = ExternalData.chop(what, glsign);
			what = ExternalData.chop(what, bankcode);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, postmonth);
			what = ExternalData.chop(what, postyear);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, transeq);
			what = ExternalData.chop(what, trandate);
			what = ExternalData.chop(what, trantime);
			what = ExternalData.chop(what, trandesc);
			what = ExternalData.chop(what, origamt);
			what = ExternalData.chop(what, acctamt);
			what = ExternalData.chop(what, origccy);
			what = ExternalData.chop(what, crate);
			what = ExternalData.chop(what, nonKeyFiller340);
			what = ExternalData.chop(what, chdrstcda);
			what = ExternalData.chop(what, chdrstcdb);
			what = ExternalData.chop(what, chdrstcdc);
			what = ExternalData.chop(what, chdrstcdd);
			what = ExternalData.chop(what, chdrstcde);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, accpfx);
			what = ExternalData.chop(what, acccoy);
			what = ExternalData.chop(what, accno);
			what = ExternalData.chop(what, acctyp);
			what = ExternalData.chop(what, prtflg);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(Object what) {
		rldgcoy.set(what);
	}
	public FixedLengthStringData getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(Object what) {
		rdocnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getBatcpfx() {
		return batcpfx;
	}
	public void setBatcpfx(Object what) {
		batcpfx.set(what);
	}	
	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public FixedLengthStringData getRdocpfx() {
		return rdocpfx;
	}
	public void setRdocpfx(Object what) {
		rdocpfx.set(what);
	}	
	public FixedLengthStringData getRdoccoy() {
		return rdoccoy;
	}
	public void setRdoccoy(Object what) {
		rdoccoy.set(what);
	}	
	public FixedLengthStringData getRldgpfx() {
		return rldgpfx;
	}
	public void setRldgpfx(Object what) {
		rldgpfx.set(what);
	}	
	public FixedLengthStringData getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(Object what) {
		rldgacct.set(what);
	}	
	public FixedLengthStringData getGenlpfx() {
		return genlpfx;
	}
	public void setGenlpfx(Object what) {
		genlpfx.set(what);
	}	
	public FixedLengthStringData getGenlcoy() {
		return genlcoy;
	}
	public void setGenlcoy(Object what) {
		genlcoy.set(what);
	}	
	public FixedLengthStringData getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(Object what) {
		genlcur.set(what);
	}	
	public FixedLengthStringData getGlcode() {
		return glcode;
	}
	public void setGlcode(Object what) {
		glcode.set(what);
	}	
	public FixedLengthStringData getGlsign() {
		return glsign;
	}
	public void setGlsign(Object what) {
		glsign.set(what);
	}	
	public FixedLengthStringData getBankcode() {
		return bankcode;
	}
	public void setBankcode(Object what) {
		bankcode.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getPostmonth() {
		return postmonth;
	}
	public void setPostmonth(Object what) {
		postmonth.set(what);
	}	
	public FixedLengthStringData getPostyear() {
		return postyear;
	}
	public void setPostyear(Object what) {
		postyear.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}	
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getTranseq() {
		return transeq;
	}
	public void setTranseq(Object what) {
		transeq.set(what);
	}	
	public PackedDecimalData getTrandate() {
		return trandate;
	}
	public void setTrandate(Object what) {
		setTrandate(what, false);
	}
	public void setTrandate(Object what, boolean rounded) {
		if (rounded)
			trandate.setRounded(what);
		else
			trandate.set(what);
	}	
	public PackedDecimalData getTrantime() {
		return trantime;
	}
	public void setTrantime(Object what) {
		setTrantime(what, false);
	}
	public void setTrantime(Object what, boolean rounded) {
		if (rounded)
			trantime.setRounded(what);
		else
			trantime.set(what);
	}	
	public FixedLengthStringData getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(Object what) {
		trandesc.set(what);
	}	
	public PackedDecimalData getOrigamt() {
		return origamt;
	}
	public void setOrigamt(Object what) {
		setOrigamt(what, false);
	}
	public void setOrigamt(Object what, boolean rounded) {
		if (rounded)
			origamt.setRounded(what);
		else
			origamt.set(what);
	}	
	public PackedDecimalData getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(Object what) {
		setAcctamt(what, false);
	}
	public void setAcctamt(Object what, boolean rounded) {
		if (rounded)
			acctamt.setRounded(what);
		else
			acctamt.set(what);
	}	
	public FixedLengthStringData getOrigccy() {
		return origccy;
	}
	public void setOrigccy(Object what) {
		origccy.set(what);
	}	
	public PackedDecimalData getCrate() {
		return crate;
	}
	public void setCrate(Object what) {
		setCrate(what, false);
	}
	public void setCrate(Object what, boolean rounded) {
		if (rounded)
			crate.setRounded(what);
		else
			crate.set(what);
	}	
	public FixedLengthStringData getChdrstcda() {
		return chdrstcda;
	}
	public void setChdrstcda(Object what) {
		chdrstcda.set(what);
	}	
	public FixedLengthStringData getChdrstcdb() {
		return chdrstcdb;
	}
	public void setChdrstcdb(Object what) {
		chdrstcdb.set(what);
	}	
	public FixedLengthStringData getChdrstcdc() {
		return chdrstcdc;
	}
	public void setChdrstcdc(Object what) {
		chdrstcdc.set(what);
	}	
	public FixedLengthStringData getChdrstcdd() {
		return chdrstcdd;
	}
	public void setChdrstcdd(Object what) {
		chdrstcdd.set(what);
	}	
	public FixedLengthStringData getChdrstcde() {
		return chdrstcde;
	}
	public void setChdrstcde(Object what) {
		chdrstcde.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public FixedLengthStringData getAccpfx() {
		return accpfx;
	}
	public void setAccpfx(Object what) {
		accpfx.set(what);
	}	
	public FixedLengthStringData getAcccoy() {
		return acccoy;
	}
	public void setAcccoy(Object what) {
		acccoy.set(what);
	}	
	public FixedLengthStringData getAccno() {
		return accno;
	}
	public void setAccno(Object what) {
		accno.set(what);
	}	
	public FixedLengthStringData getAcctyp() {
		return acctyp;
	}
	public void setAcctyp(Object what) {
		acctyp.set(what);
	}	
	public FixedLengthStringData getPrtflg() {
		return prtflg;
	}
	public void setPrtflg(Object what) {
		prtflg.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		rldgcoy.clear();
		rdocnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		batcpfx.clear();
		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		rdocpfx.clear();
		rdoccoy.clear();
		nonKeyFiller100.clear();
		rldgpfx.clear();
		nonKeyFiller120.clear();
		rldgacct.clear();
		genlpfx.clear();
		genlcoy.clear();
		genlcur.clear();
		glcode.clear();
		glsign.clear();
		bankcode.clear();
		bankkey.clear();
		postmonth.clear();
		postyear.clear();
		effdate.clear();
		sacstyp.clear();
		sacscode.clear();
		transeq.clear();
		trandate.clear();
		trantime.clear();
		trandesc.clear();
		origamt.clear();
		acctamt.clear();
		origccy.clear();
		crate.clear();
		nonKeyFiller340.clear();
		chdrstcda.clear();
		chdrstcdb.clear();
		chdrstcdc.clear();
		chdrstcdd.clear();
		chdrstcde.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		accpfx.clear();
		acccoy.clear();
		accno.clear();
		acctyp.clear();
		prtflg.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}