package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:06
 * Description:
 * Copybook name: T6661REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6661rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6661Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData contRevFlag = new FixedLengthStringData(1).isAPartOf(t6661Rec, 0);
  	public FixedLengthStringData subprogs = new FixedLengthStringData(20).isAPartOf(t6661Rec, 1);
  	public FixedLengthStringData[] subprog = FLSArrayPartOfStructure(2, 10, subprogs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(subprogs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData subprog01 = new FixedLengthStringData(10).isAPartOf(filler, 0);
  	public FixedLengthStringData subprog02 = new FixedLengthStringData(10).isAPartOf(filler, 10);
  	public FixedLengthStringData trcode = new FixedLengthStringData(4).isAPartOf(t6661Rec, 21);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(475).isAPartOf(t6661Rec, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6661Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6661Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}