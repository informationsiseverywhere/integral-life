package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:04
 * Description:
 * Copybook name: PRMHREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Prmhrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData prmhrevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData prmhrevKey = new FixedLengthStringData(256).isAPartOf(prmhrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData prmhrevChdrcoy = new FixedLengthStringData(1).isAPartOf(prmhrevKey, 0);
  	public FixedLengthStringData prmhrevChdrnum = new FixedLengthStringData(8).isAPartOf(prmhrevKey, 1);
  	public PackedDecimalData prmhrevTranno = new PackedDecimalData(5, 0).isAPartOf(prmhrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(prmhrevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(prmhrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		prmhrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}