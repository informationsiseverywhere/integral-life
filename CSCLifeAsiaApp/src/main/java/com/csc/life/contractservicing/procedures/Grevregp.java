/*
 * File: Grevregp.java
 * Date: 29 August 2009 22:50:57
 * Author: Quipoz Limited
 * 
 * Class transformed from GREVREGP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.RegprevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE REGULAR PAYMENTS
*        ------------------------
*
* Overview.
* ---------
*
*  This is a new Subroutine & forms part of the 9405 Annuities
*  Development. It will reverse the results of a Regular
*  Payment. It differs from the Corresponding Sections of
*  REVREGP (existing Subroutine) only insofar as it is passed
*  GRVERSEREC as opposed to REVERSEREC in the Linkage Section.
*
* Processing.
* -----------
*
*  Reversal of Regular Payment ( REGPs ):
*
*  Read the Regular Payment (REGPPF) using the logical view
*  REGPREV with a key of company, contract number, Life,
*  Coverage, Rider, regular payment number and tranno.
*
*  For each record found:
*   Check it is validflag '1' - otherwise, do a NEXTR.
*   If the tranno on the REGP record matches tranno we are trying
*   to reverse, delete the REGP record, read the next REGP with
*   the same key.
*
*   If the REGP record matches key and the validflag = '2'
*   reset the validflag to '1' and REWRT the REGP, otherwise
*   read the next REGP.
*
*
*****************************************************************
* </pre>
*/
public class Grevregp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "GREVREGP";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaRgpynum = new PackedDecimalData(5, 0).init(ZERO);
		/* FORMATS */
	private static final String regprevrec = "REGPREVREC";
	private RegprevTableDAM regprevIO = new RegprevTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Greversrec greversrec = new Greversrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit3190
	}

	public Grevregp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		greversrec.statuz.set(varcom.oK);
		initialise2000();
		processRegps3000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		/*START*/
		wsaaRgpynum.set(ZERO);
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		/*EXIT*/
	}

protected void processRegps3000()
	{
		start3000();
	}

protected void start3000()
	{
		regprevIO.setParams(SPACES);
		regprevIO.setChdrcoy(greversrec.chdrcoy);
		regprevIO.setChdrnum(greversrec.chdrnum);
		regprevIO.setLife(SPACES);
		regprevIO.setCoverage(SPACES);
		regprevIO.setRider(SPACES);
		regprevIO.setRgpynum(ZERO);
		regprevIO.setTranno(9999);
		regprevIO.setFormat(regprevrec);
		regprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regprevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regprevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)
		&& isNE(regprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError4000();
		}
		if (isNE(greversrec.chdrcoy,regprevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,regprevIO.getChdrnum())
		|| isNE(regprevIO.getValidflag(),"1")
		|| isEQ(regprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError4000();
		}
		while ( !(isEQ(regprevIO.getStatuz(),varcom.endp))) {
			reverseRegps3100();
		}
		
	}

protected void reverseRegps3100()
	{
		try {
			start3100();
			getNextRegp3180();
		}
		catch (GOTOException e){
		}
	}

protected void start3100()
	{
		if (isEQ(regprevIO.getTranno(),greversrec.tranno)
		&& isEQ(regprevIO.getValidflag(),"1")) {
			wsaaLife.set(regprevIO.getLife());
			wsaaCoverage.set(regprevIO.getCoverage());
			wsaaRider.set(regprevIO.getRider());
			wsaaRgpynum.set(regprevIO.getRgpynum());
			deletRewrtRegps3200();
			goTo(GotoLabel.exit3190);
		}
	}

protected void getNextRegp3180()
	{
		regprevIO.setFunction(varcom.nextr);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)
		&& isNE(regprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError4000();
		}
		if (isNE(greversrec.chdrcoy,regprevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,regprevIO.getChdrnum())
		|| isEQ(regprevIO.getStatuz(),varcom.endp)) {
			regprevIO.setStatuz(varcom.endp);
		}
	}

protected void deletRewrtRegps3200()
	{
			start3200();
		}

protected void start3200()
	{
		regprevIO.setFunction(varcom.readh);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError4000();
		}
		regprevIO.setFunction(varcom.delet);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError4000();
		}
		regprevIO.setFunction(varcom.nextr);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)
		&& isNE(regprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError4000();
		}
		if (isNE(greversrec.chdrcoy,regprevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,regprevIO.getChdrnum())
		|| isEQ(regprevIO.getStatuz(),varcom.endp)) {
			regprevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(wsaaLife,regprevIO.getLife())
		|| isNE(wsaaCoverage,regprevIO.getCoverage())
		|| isNE(wsaaRider,regprevIO.getRider())
		|| isNE(wsaaRgpynum,regprevIO.getRgpynum())) {
			return ;
		}
		regprevIO.setFunction(varcom.readh);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError4000();
		}
		regprevIO.setValidflag("1");
		regprevIO.setFunction(varcom.rewrt);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError4000();
		}
	}

protected void databaseError4000()
	{
			start4100();
			exit4990();
		}

protected void start4100()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit4990()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}
}
