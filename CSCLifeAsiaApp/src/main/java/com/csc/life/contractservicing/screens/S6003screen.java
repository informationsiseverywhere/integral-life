package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6003screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6003ScreenVars sv = (S6003ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6003screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6003ScreenVars screenVars = (S6003ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.batctrcde.setClassString("");
		screenVars.longdesc.setClassString("");
	}

/**
 * Clear all the variables in S6003screen
 */
	public static void clear(VarModel pv) {
		S6003ScreenVars screenVars = (S6003ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.rstate.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.pstate.clear();
		screenVars.cownnum.clear();
		screenVars.payrnum.clear();
		screenVars.ownername.clear();
		screenVars.payorname.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.cntcurr.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.agntnum.clear();
		screenVars.agentname.clear();
		screenVars.batctrcde.clear();
		screenVars.longdesc.clear();
	}
}
