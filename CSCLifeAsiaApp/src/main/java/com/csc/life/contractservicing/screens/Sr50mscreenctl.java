package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50mscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr50mscreensfl";
		lrec.subfileClass = Sr50mscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 4;
		lrec.pageSubfile = 3;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 11, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50mScreenVars sv = (Sr50mScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50mscreenctlWritten, sv.Sr50mscreensflWritten, av, sv.sr50mscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50mScreenVars screenVars = (Sr50mScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttyp.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.freqdesc.setClassString("");
		screenVars.mopdesc.setClassString("");
		screenVars.register.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.effdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr50mscreenctl
 */
	public static void clear(VarModel pv) {
		Sr50mScreenVars screenVars = (Sr50mScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttyp.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.cownnum.clear();
		screenVars.lifenum.clear();
		screenVars.billfreq.clear();
		screenVars.mop.clear();
		screenVars.premstatus.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.ownername.clear();
		screenVars.lifename.clear();
		screenVars.freqdesc.clear();
		screenVars.mopdesc.clear();
		screenVars.register.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
	}
}
