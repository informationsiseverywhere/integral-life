/*
 * File: Revbling.java
 * Date: 30 August 2009 2:05:53
 * Author: Quipoz Limited
 * 
 * Class transformed from REVBLING.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.dataaccess.BextrevTableDAM;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LinsrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.flexiblepremium.dataaccess.FpcorevTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmrevTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.life.regularprocessing.dataaccess.ChdrrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*       REVBLING - BILLING REVERSAL
*       --------------------------
*
*       The parameters passed to this program are set up in the
*       copybook REVERSEREC.
*
*       Read Table T1688 to obtain Batch Transaction Details.
*
*       Read Contract Header
*       Firstly read the contract header record (CHDRPF) with the
*       logical view CHDRRNL using the company/contract key as
*       passed in linkage.
*
*       Read PAYR
*       Firstly read the PAYR file with the logical view
*       PAYR using the company as passed in linkage and
*       contract number from CHDR as the key.
*
*       Delete Media records.
*       Read the media file (BEXTPF) with the logical view
*       BEXTREV and delete any record found from the file for
*       this company/contract.
*
*       Delete Instalment Records.
*       Now the relevant instalment records which has been
*       previously created need to be deleted. To do this read
*       the instalment file (LINSPF) with a logical view of
*       LINSREV for the company/contract key. This view will
*       only select instalments which have not been paid. For
*       each instalment relevant to this contract.
*
*       If the instalment to date is greater than or equal to
*       the new billed to date:
*
*          Subtract the instalment amount from the outstanding
*          amount on the contract header record and PAYR record.
*          Delete the instalment record from the file.
*
*       Update the Contract Header.
*       Once all of the instalments are deleted amend the
*       billed-to dates on the PAYR and CHDR files.
*
*       Update the Billing Suppression to and from dates and
*       the Billing Suppression Flag on the PAYR and CHDR record
*       using:
*
*          If effective date 2 in linkage is not = 99999999
*
*            Move 'Y' to the billing suppress flag.
*            Move effective date 1 to the bill suppress from date
*            Move effective date 2 to the bill suppress to date.
*
*          Add one to the transaction number on the CHDR and
*          rewrite record back to the database.
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*     Set the accounting month/year with the batch
*     accounting month/year from linkage.
*     Set the tranno with the NEW tranno from linkage.
*     Set the batch key with new batch key from linkage.
*     Multiply the original currency amount by -1.
*     Multiply the accounting currency amount by -1.
*     Set the transaction reference with the NEW
*     transaction code from linkage.
*     Set the transaction description with the long
*     description of the new trans code from T1688.
*     Set the effective date with todays date.
*     Write the new record to the database by calling the
*     subroutine 'LIFACMV'.
*
*
*****************************************************************
* </pre>
*/
public class Revbling extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVBLING";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaBillspfrm = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaFebruary = new FixedLengthStringData(1).init("Y");
	private Validator february = new Validator(wsaaFebruary, "Y");
	private ZonedDecimalData wsaaBtdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaBillcd = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);
		/* TABLES */
	private static final String t1688 = "T1688";
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private static final String t5679 = "T5679";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private BextrevTableDAM bextrevIO = new BextrevTableDAM();
	protected ChdrrnlTableDAM chdrrnlIO = new ChdrrnlTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcorevTableDAM fpcorevIO = new FpcorevTableDAM();
	private FprmrevTableDAM fprmrevIO = new FprmrevTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private HdivrevTableDAM hdivrevIO = new HdivrevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LinsTableDAM linsIO = new LinsTableDAM();
	protected LinsrevTableDAM linsrevIO = new LinsrevTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	protected TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Batckey wsaaBatckey = new Batckey();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Varcom varcom = new Varcom();
	private T5679rec t5679rec = new T5679rec();
	protected Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Itempf itempf;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextCovr5270, 
		exit5290
	}

	public Revbling() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		start010();
		exit090();
	}

protected void start010()
	{
		wsaaBatckey.set(reverserec.batchkey);
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		/* get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Read Table T1688 to obtain Batch Trans Description for*/
		/* updating ACMV.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError8100();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
		/*  Load Contract statii.                                  <D9604>*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(reverserec.oldBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		readContractHeader1000();
		flexPremCheck3000();
		readPayrFile1050();
		deleteBext1100();
		/* PERFORM 1200-PROCESS-LINS.                           <D9604>*/
		if (flexiblePremiumContract.isTrue()) {
			calcBillspfrm4000();
			readCovrs5000();
		}
		else {
			processLins1200();
		}
		updateContractHeader1500();
		updatePayrFile1900();
		processAcmvs2000();
		processAcmvOptical2010();
		/* For Cash Dividend, reverse records created in premium           */
		/* settlement dividend processing option                           */
		premSettlement7000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readContractHeader1000()
	{
		/*START*/
		/*   Read contract header*/
		chdrrnlIO.setParams(SPACES);
		chdrrnlIO.setChdrcoy(reverserec.company);
		chdrrnlIO.setChdrnum(reverserec.chdrnum);
		chdrrnlIO.setFunction(varcom.readh);
		chdrrnlIO.setFormat(formatsInner.chdrrnlrec);
		SmartFileCode.execute(appVars, chdrrnlIO);
		if (isNE(chdrrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrnlIO.getParams());
			syserrrec.statuz.set(chdrrnlIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void readPayrFile1050()
	{
		readh1060();
	}

protected void readh1060()
	{
		/* Read PAYR file.*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(reverserec.company);
		payrIO.setChdrnum(chdrrnlIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			dbError8100();
		}
	}

protected void deleteBext1100()
	{
		start1100();
	}

protected void start1100()
	{
		/* Only delete the BEXT records for this contract where the*/
		/* BTDATE is greater than the reverse date.*/
		bextrevIO.setParams(SPACES);
		bextrevIO.setChdrcoy(reverserec.company);
		bextrevIO.setChdrnum(reverserec.chdrnum);
		bextrevIO.setBtdate(99999999);
		bextrevIO.setFormat(formatsInner.bextrevrec);
		bextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, bextrevIO);
		if (isNE(bextrevIO.getStatuz(),varcom.oK)
		&& isNE(bextrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(bextrevIO.getParams());
			syserrrec.statuz.set(bextrevIO.getStatuz());
			dbError8100();
		}
		while ( !(isEQ(bextrevIO.getStatuz(),varcom.endp)
		|| isNE(bextrevIO.getChdrcoy(),reverserec.company)
		|| isNE(bextrevIO.getChdrnum(),reverserec.chdrnum)
		|| isLT(bextrevIO.getBtdate(),reverserec.ptrneff))) {
			deleteBextRecs1700();
		}
		
	}

protected void processLins1200()
	{
		/*START*/
		linsrevIO.setParams(SPACES);
		linsrevIO.setChdrcoy(reverserec.company);
		linsrevIO.setChdrnum(reverserec.chdrnum);
		linsrevIO.setInstto(ZERO);
		linsrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		linsrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		linsrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		callLinsrevio1600();
		/*    Put in further checks for COMPANY or CHDRNUM. Change*/
		/*    to stop deleting LINS records for other contracts.*/
		if (isEQ(linsrevIO.getStatuz(),varcom.endp)
		|| isNE(linsrevIO.getChdrcoy(),reverserec.company)
		|| isNE(linsrevIO.getChdrnum(),reverserec.chdrnum)) {
			return ;
		}
		while ( !(isEQ(linsrevIO.getStatuz(),varcom.endp)
		|| isNE(linsrevIO.getChdrcoy(),reverserec.company)
		|| isNE(linsrevIO.getChdrnum(),reverserec.chdrnum))) {
			deleteOsInstalments1300();
		}
		
		/*EXIT*/
	}

protected void deleteOsInstalments1300()
	{
			start1300();
		}

protected void start1300()
	{
		/*   Delete O/S instalments for a contract*/
		/*    IF LINSREV-INSTTO           NOT > REVE-EFFDATE-1             */
		if (isLTE(linsrevIO.getInstto(),reverserec.ptrneff)) {
			linsrevIO.setFunction(varcom.nextr);
			callLinsrevio1600();
		}
		if (isNE(linsrevIO.getStatuz(),varcom.oK)) {
			return ;
		}
		/*    IF LINSREV-INSTTO           > REVE-EFFDATE-1                 */
		if (isGT(linsrevIO.getInstto(),reverserec.ptrneff)) {
			linsrevIO.setFunction(varcom.readh);
			callLinsrevio1600();
			if (isNE(linsrevIO.getStatuz(),varcom.oK)) {
				return ;
			}
			linsIO.setParams(SPACES);
			linsIO.setRrn(linsrevIO.getRrn());
			linsIO.setFunction(varcom.readd);
			linsIO.setFormat(formatsInner.linsrec);
			SmartFileCode.execute(appVars, linsIO);
			if (isNE(linsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(linsIO.getParams());
				syserrrec.statuz.set(linsIO.getStatuz());
				dbError8100();
			}
			setPrecision(chdrrnlIO.getOutstamt(), 2);
			chdrrnlIO.setOutstamt(sub(chdrrnlIO.getOutstamt(),linsrevIO.getInstamt06()));
			/* Update the Outstanding Amount on the PAYR record*/
			setPrecision(payrIO.getOutstamt(), 2);
			payrIO.setOutstamt(sub(payrIO.getOutstamt(),linsrevIO.getInstamt06()));
			callDatcon21400();
			linsrevIO.setFunction(varcom.delet);
			callLinsrevio1600();
			if (isNE(linsrevIO.getStatuz(),varcom.oK)) {
				return ;
			}
			taxdrevIO.setParams(SPACES);
			taxdrevIO.setChdrcoy(reverserec.company);
			taxdrevIO.setChdrnum(reverserec.chdrnum);
			taxdrevIO.setEffdate(ZERO);
			taxdrevIO.setFunction(varcom.begn);
			while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
				deleteTaxdrev1300b();
			}
			
		}
		/*  Begin on next record so as to release the hold on the record*/
		/*  This should position on the record after the deleted record*/
		linsrevIO.setFunction(varcom.begn);
		callLinsrevio1600();
	}

protected void deleteTaxdrev1300b()
	{
		para1300b();
	}

protected void para1300b()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(reverserec.company, taxdrevIO.getChdrcoy())
		&& isEQ(reverserec.chdrnum, taxdrevIO.getChdrnum())
		&& isEQ(linsIO.getInstfrom(), taxdrevIO.getInstfrom())
		&& isEQ(linsIO.getInstto(), taxdrevIO.getInstto())) {
			taxdrevIO.setStatuz(varcom.oK);
			taxdrevIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdrevIO.getParams());
				syserrrec.statuz.set(taxdrevIO.getStatuz());
				dbError8100();
			}
		}
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void callDatcon21400()
	{
		/*START*/
		datcon2rec.statuz.set(SPACES);
		datcon2rec.intDate1.set(linsrevIO.getInstto());
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			dbError8100();
		}
		wsaaBillspfrm.set(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void updateContractHeader1500()
	{
		start1500();
	}

protected void start1500()
	{
		/* Rewind the BTDATE first*/
		/*   Calculate the difference between the BTDATE and the Effective*/
		/*    date.Then use DATCON4 to wind the BTDATE back.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.statuz.set(SPACES);
		/* MOVE REVE-EFFDATE-1         TO DTC3-INT-DATE-1.              */
		/* Use the REVE-PTRNEFF instead of REVE-EFFDATE-1 to calculate     */
		/* the number of complete frequencies for the contract reversal.   */
		datcon3rec.intDate1.set(reverserec.ptrneff);
		datcon3rec.intDate2.set(payrIO.getBtdate());
		datcon3rec.frequency.set(payrIO.getBillfreq());
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			dbError8100();
		}
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(payrIO.getBtdate());
		datcon4rec.intDate2.set(99999999);
		datcon4rec.frequency.set(payrIO.getBillfreq());
		datcon4rec.billday.set(payrIO.getDuedd());
		datcon4rec.billmonth.set(payrIO.getDuemm());
		/* COMPUTE DTC4-FREQ-FACTOR    =  DTC3-FREQ-FACTOR * -1.        */
		if (isLT(datcon3rec.freqFactor,1)
		&& isGT(datcon3rec.freqFactor,-1)) {
			compute(datcon4rec.freqFactor, 6).setRounded(mult(datcon3rec.freqFactor,-1));
		}
		else {
			compute(datcon4rec.freqFactor, 5).set(mult(datcon3rec.freqFactor,-1));
		}
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			dbError8100();
		}
		/* If the month is February and the previous BTDATE was            */
		/* the 29th Feb, the BTDATE should be reversed back to the         */
		/* the 29th Feb and NOT the 28th Feb.                              */
		wsaaBtdate.set(payrIO.getBtdate());
		/* IF FEBRUARY AND                                      <LA3432>*/
		/*    PAYR-DUEDD               NOT = WSAA-BTDATE-DD     <LA3432>*/
		/*    MOVE DTC4-INT-DATE-2     TO WSAA-DTC4-INT-DATE-2  <LA3432>*/
		/*    MOVE PAYR-DUEDD          TO WSAA-DTC4-DD          <LA3432>*/
		/*    MOVE WSAA-DTC4-INT-DATE-2                         <LA3432>*/
		/*                             TO DTC4-INT-DATE-2       <LA3432>*/
		/* END-IF.                                              <LA3432>*/
		/* Update the BTDATE on the PAYR & CHDR records*/
		payrIO.setBtdate(datcon4rec.intDate2);
		chdrrnlIO.setBtdate(datcon4rec.intDate2);
		/* Rewind the BILLCD next*/
		/*   Calculate the difference between the BILLCD and the Effective*/
		/*    date.Then use DATCON4 to wind the BILLCD back.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.statuz.set(SPACES);
		/*    MOVE REVE-EFFDATE-1         TO DTC3-INT-DATE-1.              */
		datcon3rec.intDate1.set(reverserec.ptrneff);
		datcon3rec.intDate2.set(payrIO.getBillcd());
		datcon3rec.frequency.set(payrIO.getBillfreq());
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			dbError8100();
		}
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(payrIO.getBillcd());
		datcon4rec.intDate2.set(99999999);
		datcon4rec.frequency.set(payrIO.getBillfreq());
		datcon4rec.billday.set(payrIO.getBillday());
		datcon4rec.billmonth.set(payrIO.getBillmonth());
		/* COMPUTE DTC4-FREQ-FACTOR    =  DTC3-FREQ-FACTOR * -1.        */
		if (isLT(datcon3rec.freqFactor,1)
		&& isGT(datcon3rec.freqFactor,-1)) {
			compute(datcon4rec.freqFactor, 6).setRounded(mult(datcon3rec.freqFactor,-1));
		}
		else {
			compute(datcon4rec.freqFactor, 5).set(mult(datcon3rec.freqFactor,-1));
		}
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			dbError8100();
		}
		/* If the month is February and the previous BILLCD was            */
		/* the 29th Feb, the BILLCD should be reversed back to the         */
		/* the 29th Feb and NOT the 28th Feb.                              */
		wsaaBillcd.set(payrIO.getBillcd());
		/* IF FEBRUARY AND                                      <LA3432>*/
		/*    PAYR-DUEDD               NOT = WSAA-BILLCD-DD     <LA3432>*/
		/*    MOVE DTC4-INT-DATE-2     TO WSAA-DTC4-INT-DATE-2  <LA3432>*/
		/*    MOVE PAYR-DUEDD          TO WSAA-DTC4-DD          <LA3432>*/
		/*    MOVE WSAA-DTC4-INT-DATE-2                         <LA3432>*/
		/*                             TO DTC4-INT-DATE-2       <LA3432>*/
		/* END-IF.                                              <LA3432>*/
		/* Update the BILLCD on the PAYR & CHDR records*/
		payrIO.setBillcd(datcon4rec.intDate2);
		chdrrnlIO.setBillcd(datcon4rec.intDate2);
		/* Wind back PAYR-NEXTDATE the same number of instalments as*/
		/* the billing date. (PAYR-BILLCD).*/
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(payrIO.getNextdate());
		datcon2rec.intDate2.set(99999999);
		datcon2rec.frequency.set(payrIO.getBillfreq());
		/* COMPUTE DTC2-FREQ-FACTOR    =  DTC3-FREQ-FACTOR * -1.        */
		if (isLT(datcon3rec.freqFactor,1)
		&& isGT(datcon3rec.freqFactor,-1)) {
			compute(datcon2rec.freqFactor, 6).setRounded(mult(datcon3rec.freqFactor,-1));
		}
		else {
			compute(datcon2rec.freqFactor, 5).set(mult(datcon3rec.freqFactor,-1));
		}
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			dbError8100();
		}
		payrIO.setNextdate(datcon2rec.intDate2);
		/* Update Billing Suppression To Date and Billing Suppression*/
		/* Flag on the PAYR record*/
		if (isNE(reverserec.effdate2,99999999)) {
			payrIO.setBillspto(reverserec.effdate2);
			payrIO.setBillspfrom(wsaaBillspfrm);
			payrIO.setBillsupr("Y");
			chdrrnlIO.setBillspto(reverserec.effdate2);
			chdrrnlIO.setBillspfrom(wsaaBillspfrm);
			chdrrnlIO.setBillsupr("Y");
		}
		chdrrnlIO.setFormat(formatsInner.chdrrnlrec);
		chdrrnlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrrnlIO);
		if (isNE(chdrrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrnlIO.getParams());
			syserrrec.statuz.set(chdrrnlIO.getStatuz());
			dbError8100();
		}
		checkPH();
	}

protected void callLinsrevio1600()
	{
		/*START*/
		linsrevIO.setFormat(formatsInner.linsrevrec);
		SmartFileCode.execute(appVars, linsrevIO);
		if (isNE(linsrevIO.getStatuz(),varcom.oK)
		&& isNE(linsrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(linsrevIO.getParams());
			syserrrec.statuz.set(linsrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company,linsrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,linsrevIO.getChdrnum())) {
			linsrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void deleteBextRecs1700()
	{
		/*START*/
		/* Read BEXTPF using logical BEXTREV and Delete any records*/
		/* found for this Contract.*/
		bextrevIO.setFunction(varcom.readh);
		callBextrevio1800();
		bextrevIO.setFunction(varcom.delet);
		callBextrevio1800();
		bextrevIO.setFunction(varcom.nextr);
		callBextrevio1800();
		/*EXIT*/
	}

protected void callBextrevio1800()
	{
		/*START*/
		bextrevIO.setFormat(formatsInner.bextrevrec);
		SmartFileCode.execute(appVars, bextrevIO);
		if ((isNE(bextrevIO.getStatuz(),varcom.oK))
		&& (isNE(bextrevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(bextrevIO.getParams());
			syserrrec.statuz.set(bextrevIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void updatePayrFile1900()
	{
		/*REWRT*/
		/* Update the PAYR record with any changes that may have been*/
		/* made.*/
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void processAcmvs2000()
	{
		start2000();
	}

protected void start2000()
	{
		/*  Read the Account movements file (ACMVPF) using the logical*/
		/*  view ACMVREV with a key of contract number/transaction number*/
		/*  from linkage.*/
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvs2100();
		}
		
	}

protected void processAcmvOptical2010()
	{
		start2010();
	}

protected void start2010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			dbError8100();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
				}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			dbError8100();
				}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
				}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs2100();
				}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				dbError8100();
			}
			}
		}

protected void reverseAcmvs2100()
	{
		start2100();
		readNextAcmv2180();
	}

protected void start2100()
	{
		/* Check If ACMV just read has same trancode as transaction*/
		/* we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(),-1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.effdate.set(datcon1rec.intDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError8000();
		}
	}

protected void readNextAcmv2180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall,"CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void flexPremCheck3000()
	{
			start3010();
		}

protected void start3010()
	{
		notFlexiblePremiumContract.setTrue();
		/*  Read T5729.                                            <D9604>*/
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(chdrrnlIO.getChdrcoy());
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrrnlIO.getCnttype());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrrnlIO.getChdrcoy(), SPACES);
			stringVariable1.addExpression(t5729, SPACES);
			stringVariable1.addExpression(chdrrnlIO.getCnttype(), SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),chdrrnlIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5729)
		|| isNE(itdmIO.getItemitem(),chdrrnlIO.getCnttype())) {
			return ;
		}
		flexiblePremiumContract.setTrue();
	}

protected void calcBillspfrm4000()
	{
			start4010();
		}

protected void start4010()
	{
		/* NB If we are not suppressing billing then we do not need<D9604>*/
		/* to calculate wsaa-billspfrm                             <D9604>*/
		if (isEQ(reverserec.effdate2,varcom.vrcmMaxDate)) {
			return ;
		}
		datcon2rec.statuz.set(SPACES);
		/*    MOVE REVE-EFFDATE-1         TO DTC2-INT-DATE-1.      <LA4308>*/
		datcon2rec.intDate1.set(reverserec.ptrneff);
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set("12");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			dbError8100();
		}
		datcon2rec.statuz.set(SPACES);
		datcon2rec.intDate1.set(datcon2rec.intDate2);
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			dbError8100();
		}
		wsaaBillspfrm.set(datcon2rec.intDate2);
	}

protected void readCovrs5000()
	{
		/*START*/
		/* First we need to read the COVR file as we haven't got   <D9604>*/
		/* a full key to read the FPCO records with.               <D9604>*/
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setLife(SPACES);
		covrlnbIO.setCoverage(SPACES);
		covrlnbIO.setRider(SPACES);
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setChdrcoy(reverserec.company);
		covrlnbIO.setChdrnum(reverserec.chdrnum);
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		covrlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
			nextCovr5250();
		}
		
		/*EXIT*/
	}

protected void nextCovr5250()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5260();
				case nextCovr5270: 
					nextCovr5270();
				case exit5290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5260()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrlnbIO.getParams());
			dbError8100();
		}
		if (isNE(covrlnbIO.getChdrcoy(),reverserec.company)
		|| isNE(covrlnbIO.getChdrnum(),reverserec.chdrnum)
		|| isEQ(covrlnbIO.getStatuz(),varcom.endp)) {
			covrlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5290);
		}
		/* Check to see if coverage is of a valid status           <D9604>*/
		wsaaValidCoverage.set("N");
		if (isNE(covrlnbIO.getValidflag(),"1")) {
			goTo(GotoLabel.nextCovr5270);
		}
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()],covrlnbIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub,12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()],covrlnbIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
		/*  If the coverage is not of a valid status or the        <D9604>*/
		/*  instalment premium is zero (ie a rider record) then    <D9604>*/
		/*  skip FPCO read and read the next record on COVR file:  <D9604>*/
		if (!validCoverage.isTrue()) {
			goTo(GotoLabel.nextCovr5270);
		}
		if (isEQ(covrlnbIO.getInstprem(),0)) {
			goTo(GotoLabel.nextCovr5270);
		}
		readFpco5500();
	}

protected void nextCovr5270()
	{
		covrlnbIO.setFunction(varcom.nextr);
	}

protected void readFpco5500()
	{
		start5510();
	}

protected void start5510()
	{
		fpcorevIO.setChdrcoy(covrlnbIO.getChdrcoy());
		fpcorevIO.setChdrnum(covrlnbIO.getChdrnum());
		fpcorevIO.setLife(covrlnbIO.getLife());
		fpcorevIO.setCoverage(covrlnbIO.getCoverage());
		fpcorevIO.setRider(covrlnbIO.getRider());
		fpcorevIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		fpcorevIO.setTargfrom(varcom.vrcmMaxDate);
		fpcorevIO.setFormat(formatsInner.fpcorevrec);
		/* MOVE BEGNH                  TO FPCOREV-FUNCTION.     <LA3993>*/
		fpcorevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcorevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			dbError8100();
		}
		if (isNE(fpcorevIO.getChdrcoy(),covrlnbIO.getChdrcoy())
		|| isNE(fpcorevIO.getChdrnum(),covrlnbIO.getChdrnum())
		|| isNE(fpcorevIO.getLife(),covrlnbIO.getLife())
		|| isNE(fpcorevIO.getCoverage(),covrlnbIO.getCoverage())
		|| isNE(fpcorevIO.getRider(),covrlnbIO.getRider())
		|| isNE(fpcorevIO.getPlanSuffix(),covrlnbIO.getPlanSuffix())) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			dbError8100();
		}
		if (isEQ(fpcorevIO.getBilledInPeriod(),ZERO)) {
			
			while ( !(isEQ(fpcorevIO.getStatuz(),varcom.endp)))
		   {
				nextFpco6000();
				
			}
			
		}
		/* Reduce the billed amount by one frequency of the target <D9604>*/
		/* premium:                                                <D9604>*/
		setPrecision(fpcorevIO.getBilledInPeriod(), 2);
		fpcorevIO.setBilledInPeriod(sub(fpcorevIO.getBilledInPeriod(),covrlnbIO.getInstprem()));
		/* Now we need to update  the minimum required for overdue <D9604>*/
		/* processing:                                             <D9604>*/
		setPrecision(fpcorevIO.getOverdueMin(), 2);
		fpcorevIO.setOverdueMin(div(mult(fpcorevIO.getBilledInPeriod(),fpcorevIO.getMinOverduePer()),100));
		zrdecplrec.amountIn.set(fpcorevIO.getOverdueMin());
		c000CallRounding();
		fpcorevIO.setOverdueMin(zrdecplrec.amountOut);
		/*                                                         <D9604> */
		/* The active indicator should be reset to 'Y' if the prem <D9604>*/
		/* billed drops below the target                           <D9604>*/
		/*                                                         <D9604> */
		if (isLT(fpcorevIO.getBilledInPeriod(),fpcorevIO.getTargetPremium())) {
			fpcorevIO.setActiveInd("Y");
		}
		/* Then update the record on the FPCO file:                <D9604>*/
		/* MOVE REWRT                  TO FPCOREV-FUNCTION.     <LA3993>*/
		fpcorevIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			dbError8100();
		}
		/* Finally we need to update the total billed and minimum  <D9604>*/
		/* required fields on the FPRM record.                     <D9604>*/
		fprmrevIO.setChdrcoy(reverserec.company);
		fprmrevIO.setChdrnum(reverserec.chdrnum);
		fprmrevIO.setPayrseqno(payrIO.getPayrseqno());
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		fprmrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			dbError8100();
		}
		setPrecision(fprmrevIO.getTotalBilled(), 2);
		fprmrevIO.setTotalBilled(sub(fprmrevIO.getTotalBilled(),covrlnbIO.getInstprem()));
		setPrecision(fprmrevIO.getMinPrmReqd(), 2);
		fprmrevIO.setMinPrmReqd(sub(fprmrevIO.getMinPrmReqd(),(mult(div(fpcorevIO.getMinOverduePer(),100),covrlnbIO.getInstprem()))));
		zrdecplrec.amountIn.set(fprmrevIO.getMinPrmReqd());
		c000CallRounding();
		fprmrevIO.setMinPrmReqd(zrdecplrec.amountOut);
		/* Write the record back to the FPRM file:                 <D9604>*/
		fprmrevIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fprmrevIO.getParams());
			dbError8100();
		}
	}

protected void nextFpco6000()
	{
		/*START*/
		fpcorevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(),varcom.oK) && isNE(fpcorevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			dbError8100();
		}
		 
		if (isNE(fpcorevIO.getChdrcoy(),covrlnbIO.getChdrcoy())
		|| isNE(fpcorevIO.getChdrnum(),covrlnbIO.getChdrnum())
		|| isNE(fpcorevIO.getLife(),covrlnbIO.getLife())
		|| isNE(fpcorevIO.getCoverage(),covrlnbIO.getCoverage())
		|| isNE(fpcorevIO.getRider(),covrlnbIO.getRider())
		|| isNE(fpcorevIO.getPlanSuffix(),covrlnbIO.getPlanSuffix())) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void premSettlement7000()
	{
		prem7000();
	}

protected void prem7000()
	{
		/* Reverse the HDIV records to undo the withdrawal done in B5349   */
		hdivrevIO.setDataKey(SPACES);
		hdivrevIO.setChdrcoy(reverserec.company);
		hdivrevIO.setChdrnum(reverserec.chdrnum);
		hdivrevIO.setTranno(reverserec.tranno);
		hdivrevIO.setFormat(formatsInner.hdivrevrec);
		/* MOVE BEGNH                  TO HDIVREV-FUNCTION.     <LA3993>*/
		hdivrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hdivrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, hdivrevIO);
		if (isNE(hdivrevIO.getStatuz(),varcom.oK)
		&& isNE(hdivrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdivrevIO.getParams());
			syserrrec.statuz.set(hdivrevIO.getStatuz());
			dbError8100();
		}
		if (isNE(reverserec.company,hdivrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,hdivrevIO.getChdrnum())
		|| isNE(reverserec.tranno,hdivrevIO.getTranno())) {
			hdivrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivrevIO.getStatuz(),varcom.endp))) {
			if (isEQ(hdivrevIO.getDivdStmtNo(),ZERO)) {
				/*       MOVE DELET              TO HDIVREV-FUNCTION    <LA3993>*/
				hdivrevIO.setFunction(varcom.deltd);
				SmartFileCode.execute(appVars, hdivrevIO);
				if (isNE(hdivrevIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivrevIO.getParams());
					syserrrec.statuz.set(hdivrevIO.getStatuz());
					dbError8100();
				}
			}
			else {
				/* Otherwise write a mirror HDIV to counter off it                 */
				hdivIO.setDataArea(SPACES);
				hdivIO.setChdrcoy(hdivrevIO.getChdrcoy());
				hdivIO.setChdrnum(hdivrevIO.getChdrnum());
				hdivIO.setLife(hdivrevIO.getLife());
				hdivIO.setJlife(hdivrevIO.getJlife());
				hdivIO.setCoverage(hdivrevIO.getCoverage());
				hdivIO.setRider(hdivrevIO.getRider());
				hdivIO.setPlanSuffix(hdivrevIO.getPlanSuffix());
				hdivIO.setPuAddNbr(hdivrevIO.getPuAddNbr());
				hdivIO.setTranno(reverserec.newTranno);
				hdivIO.setBatccoy(reverserec.batccoy);
				hdivIO.setBatcbrn(reverserec.batcbrn);
				hdivIO.setBatcactyr(reverserec.batcactyr);
				hdivIO.setBatcactmn(reverserec.batcactmn);
				hdivIO.setBatctrcde(reverserec.batctrcde);
				hdivIO.setBatcbatch(reverserec.batcbatch);
				hdivIO.setCntcurr(hdivrevIO.getCntcurr());
				hdivIO.setEffdate(hdivrevIO.getEffdate());
				hdivIO.setDivdAllocDate(reverserec.effdate1);
				hdivIO.setDivdType(hdivrevIO.getDivdType());
				setPrecision(hdivIO.getDivdAmount(), 2);
				hdivIO.setDivdAmount(mult(hdivrevIO.getDivdAmount(),-1));
				hdivIO.setDivdRate(hdivrevIO.getDivdRate());
				hdivIO.setDivdRtEffdt(hdivrevIO.getDivdRtEffdt());
				hdivIO.setZdivopt(hdivrevIO.getZdivopt());
				hdivIO.setZcshdivmth(hdivrevIO.getZcshdivmth());
				hdivIO.setDivdOptprocTranno(hdivrevIO.getDivdOptprocTranno());
				hdivIO.setDivdCapTranno(hdivrevIO.getDivdCapTranno());
				hdivIO.setDivdIntCapDate(hdivrevIO.getDivdIntCapDate());
				hdivIO.setDivdStmtNo(0);
				hdivIO.setFormat(formatsInner.hdivrec);
				hdivIO.setFunction(varcom.writr);
				SmartFileCode.execute(appVars, hdivIO);
				if (isNE(hdivIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(hdivIO.getParams());
					syserrrec.statuz.set(hdivIO.getStatuz());
					dbError8100();
				}
			}
			hdivrevIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, hdivrevIO);
			if (isNE(hdivrevIO.getStatuz(),varcom.oK)
			&& isNE(hdivrevIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hdivrevIO.getParams());
				syserrrec.statuz.set(hdivrevIO.getStatuz());
				dbError8100();
			}
			if (isNE(hdivrevIO.getChdrcoy(),reverserec.company)
			|| isNE(hdivrevIO.getChdrnum(),reverserec.chdrnum)
			|| isNE(hdivrevIO.getTranno(),reverserec.tranno)) {
				hdivrevIO.setStatuz(varcom.endp);
			}
		}
		
	}

protected void systemError8000()
	{
			se8000();
			seExit8090();
		}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
			db8100();
			dbExit8190();
		}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		reverserec.statuz.set(varcom.bomb);
		exit090();
	}

protected void c000CallRounding()
	{
		/*C100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(reverserec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(acmvrevIO.getOrigcurr());
		zrdecplrec.batctrcde.set(reverserec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError8000();
		}
		/*C900-EXIT*/
	}

//ILIFE-8179
protected void checkPH() {
	boolean prmhldtrad = false;
	Ta524rec ta524rec = new Ta524rec();
	Ptrnpf ptrnpf;
	prmhldtrad = FeaConfg.isFeatureExist(chdrrnlIO.getChdrcoy().toString(), "CSOTH010", appVars, "IT");
	itempf = null;
	itempf = itempfDAO.findItemByDate("IT", chdrrnlIO.getChdrcoy().toString(), "TA524", chdrrnlIO.getCnttype().toString(), wsaaToday.toString(), "1");
	if(itempf != null)
		ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	else
		ta524rec.ta524Rec.set(SPACES);
	if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES)) {
		ptrnpf = ptrnpfDAO.getPtrnpfRecord(chdrrnlIO.getChdrcoy().toString(), chdrrnlIO.getChdrnum().toString(), reverserec.tranno.toInt()-1);
		if("TR7H".equals(ptrnpf.getBatctrcde()))
			payrIO.setTranno(payrIO.getTranno().toInt()-1);
	}
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData bextrevrec = new FixedLengthStringData(10).init("BEXTREVREC");
	private FixedLengthStringData chdrrnlrec = new FixedLengthStringData(10).init("CHDRRNLREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData linsrevrec = new FixedLengthStringData(10).init("LINSREVREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData fprmrevrec = new FixedLengthStringData(10).init("FPRMREVREC");
	private FixedLengthStringData fpcorevrec = new FixedLengthStringData(10).init("FPCOREVREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData hdivrevrec = new FixedLengthStringData(10).init("HDIVREVREC");
	private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
	private FixedLengthStringData linsrec = new FixedLengthStringData(10).init("LINSREC");
}
}
