/*
 * File: B5031.java
 * Date: 29 August 2009 20:52:45
 * Author: Quipoz Limited
 *
 * Class transformed from B5031.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.BextTableDAM;
import com.csc.fsu.general.dataaccess.ClbaddbTableDAM;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.AcmvlonTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.procedures.Intcalc;
import com.csc.life.contractservicing.procedures.Nfloan;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.recordstructures.Nfloanrec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Jctlkey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*          LOANS - CAPITALISATION OF INTEREST BATCH JOB
*          ============================================
*
* Overview.
* ---------
*
* This batch job will select via SQL all LOAN records which have
*  their 'Next Capitalisation Date' field set to or LESS than
*  the effective date -  the batch run date.
*
*  I.E.  Next-capitalisation-date  <= Effective date
*
* It will check whether the 'Next capitalisation date' is
*  NOT equal to the 'Last interest billing date' on the LOAN
*  record, and if so, will calculate the interest since the
*  last 'Interest billing date' and write any ACMVs & BEXT
*  records that may be required.
*
* Having made sure that all interest has been calculated & posted
*  up until the Batch effective date passed in, the program will
*  then read the ACBL file using the 1st line on the Accounting
*  rules table T5645 as part of the ACBL key. - the ACBL record
*  read will have a value for all interest since the last
*  Capitalisation date held on the Loan record.
*
* Calculate current Principal amount by reading ACMVs and
*  looking for changes in the principal since the Last
*  capitalisation date.
*
*  Add this value to the outstanding Loan Principal amount by
*  writing an ACMV to the Loan Principal subaccount. The program
*  will also post a -ve amount ACMV to the Loan interest sub-
*  account, thus zeroising the accrued interest over the last
*  year - ie we have 'Capitalised' - Added all outstanding
*  interest to the loan principal amount and cleared down the
*  interest owed account to zero.
*
* Sum New principal with interest accrued value from above
*  and amend LOAN record accordingly.
* Update the 'Last Capitalisation date' on the LOAN record.
*
* Now call the NFLOAN subroutine - this basically checks whether
*  the Contract Surrender Value can sustain the current Loan debt
*
* IF it can, NFLOAN returns a statuz of 'O-K' ,
*    else    NFLOAN returns a statuz of 'NAPL'
*
* IF NFLOAN has returned 'NAPL', then the subroutine has also
*  written a line to the R6244 report ( also produced from the
*  Overdue Processing job ) saying that the contract can no
*  longer support the Loan.
*
* NB It is assumed that this program will run at end of            004>
* capitalisation day.                                              004>
*                                                                  004>
* Processing.
* -----------
*
*Select LOAN records via SQL
*
*  Read T5645 using 'B5031' as the entry item
*
* Repeat for each LOAN record selected:
*
*    Softlock the CHDR contract header record
*    Read & Lock the CHDR contract header record
*    Rewrite the CHDR record with Validflag '2'
*    Increment the CHDR TRANNO by 1
*    Write a new CHDR contract header record
*
*    Check if the Last-interest-date on the LOAN record is
*     equal to the Effective-date:
*   IF   so, then interest on the Loan has been calculated and
*        posted up to & including the effective date
*   ELSE NOT, then there is some Interest caluclation & postings
*        which are outstanding for this Loan so we must do this
*        first before we try to Capitalise....
*
*          Calculate the interest due on the selected Loan by
*           using the Interest calculation subroutine INTCALC.
*
*          Calculate the next interest date and update the LOAN
*           record accordingly.
*
*          Post the interest due by using LIFACMV
*           to write ACMV records
*             - See entry 'B5031' in table T5645 for postings....
*                 write a loan debit record
*                 & write a loan interest income record
*
*          Read the PAYR file and MANDate file to get details
*           so that we can write a BEXT record for the Interest
*           Due
*   END IF
*
*   Now we can proceed with the Capitalisation process.....
*
*   Calculate the total Loan debt by reading the ACBL record
*    for Loan Interest and read through all the ACMV records
*    for Loan Principal since the Last capitalisation date...
*    this is to take account of any movements in the principal
*    account - Add the ACBL interest value to the sum of all
*    the Loan principal ACMVs to get the Current Loan debt.
*
*   Now Capitalise...
*    Use LIFACMV to write ACMV postings, as described in the
*    'B5031' entry in T5645...
*        write a Loan Principal Debit record for the Interest
*        owed ( from the ACBL record ),
*        write a Loan Interest Credit record for the Interest
*        owed, thus zeroising the Loan Interest Owed account.
*        hence, we have 'Capitalised', ie added all our interest
*        to our Outstanding principal.
*
*   Update the LOAN record with the Last Capitalised amount,
*     obtained from above and set the Next Capitalisation date.
*
*   Check the Surrender value of the Contract... Call the
*    subroutine NFLOAN to check whether the Contract can still
*    support the Loan(s) currently held against it....
*    IF it can, then no worries ...
*    IF it can't, then the NFLOAN subroutine will write a line
*       to the R6244 Overdue processing report saying that the
*       Loan debt now exceeds the Surrender value.
*
*    Write a PTRN to indicate change to the Contract
*
*    Release the Softlock on the Contract header
*
*    Get next LOAN record from SQL
*
* TABLES USED
* -----------
*
*    03  T3629     Currency Codes
*    03  T5645  -  Transaction Accounting rules
*    03  T5679     Transaction Status requirements
*    03  T6633  -  Loan Interest Rules
*
*
*****************************************************************
* </pre>
*/
public class B5031 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlloan1rs = null;
	private java.sql.PreparedStatement sqlloan1ps = null;
	private java.sql.Connection sqlloan1conn = null;
	private String sqlloan1 = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5031");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSqlEffdate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaFacthous = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBankacckey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaMandstat = new FixedLengthStringData(2);
	private PackedDecimalData wsaaIntFromDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaPayrpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPayrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaGlmapXx = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaSacscodeXx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstypeXx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaGlmap01 = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaSacscode01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private PackedDecimalData wsaaCurbal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPostedAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalLoanDebt = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaReadPayr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnumOld = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).setUnsigned();
		/* WSAA-TRANID */
	private static final String wsaaTermid = "";
	private static final int wsaaTransactionDate = 0;
	private static final int wsaaTransactionTime = 0;
	private static final int wsaaUser = 0;

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange(00,28));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaChdrValid = "";

	private FixedLengthStringData wsaaOkey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaOkey1 = new FixedLengthStringData(1).isAPartOf(wsaaOkey, 0);
	private ZonedDecimalData wsaaOkey2 = new ZonedDecimalData(2, 0).isAPartOf(wsaaOkey, 1).setUnsigned();
	private FixedLengthStringData wsaaOkey3 = new FixedLengthStringData(1).isAPartOf(wsaaOkey, 3);
	private ZonedDecimalData wsaaOkey4 = new ZonedDecimalData(5, 0).isAPartOf(wsaaOkey, 4).setUnsigned();
	private PackedDecimalData wsaaLoanTranno = new PackedDecimalData(5, 0);

		/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6).setUnsigned();
		/* ERRORS */
	private static final String e723 = "E723";
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t5645 = "T5645";
	private static final String t6633 = "T6633";
	private static final String tr384 = "TR384";
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler5, 5);

		/*  Control indicators*/
	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AcmvlonTableDAM acmvlonIO = new AcmvlonTableDAM();
	private BextTableDAM bextIO = new BextTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClbaddbTableDAM clbaddbIO = new ClbaddbTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	private MandTableDAM mandIO = new MandTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Jctlkey wsaaJctlkey = new Jctlkey();
	private Varcom varcom = new Varcom();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Intcalcrec intcalcrec = new Intcalcrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Nfloanrec nfloanrec = new Nfloanrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T3629rec t3629rec = new T3629rec();
	private T5645rec t5645rec = new T5645rec();
	private T6633rec t6633rec = new T6633rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T5679rec t5679rec = new T5679rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	private SqlLoanpfInner sqlLoanpfInner = new SqlLoanpfInner();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readPrimary420,
		capitalisation440,
		readNextRecord460,
		exit490,
		eof580,
		exit590,
		exit1090,
		datcon2Loop3150,
		datcon4Loop3160,
		exit3190,
		readT36295320,
		readNextAcmv6150,
		exit6190,
		datcon2Loop7250,
		exit7290,
		readTr38411020
	}

	public B5031() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readFirstRecord300();
		while ( !(endOfFile.isTrue())) {
			processRecords400();
		}

		finished900();
		/*EXIT*/
	}

protected void initialise200()
	{
		start200();
		openBatch210();
		readT5645230();
	}

protected void start200()
	{
		wsaaParmCompany.set(runparmrec.company);
		wsaaSqlEffdate.set(runparmrec.effdate);
		wsaaCnttype.set(SPACES);
		wsaaFacthous.set(SPACES);
		wsaaBankkey.set(SPACES);
		wsaaBankacckey.set(SPACES);
		wsaaMandstat.set(SPACES);
		wsaaTranno.set(ZERO);
		wsaaSequenceNo.set(ZERO);
		wsaaChdrnumOld.set(SPACES);
		wsaaJctlkey.jctlKey.set(SPACES);
		wsaaJctlkey.jctlJobsts.set("A");
		wsaaJctlkey.jctlJctlpfx.set("JC");
		wsaaJctlkey.jctlJctlcoy.set(runparmrec.company);
		wsaaJctlkey.jctlJctljn.set(runparmrec.jobname);
		wsaaJctlkey.jctlJctlacyr.set(runparmrec.acctyear);
		wsaaJctlkey.jctlJctlacmn.set(runparmrec.acctmonth);
		wsaaJctlkey.jctlJctljnum.set(runparmrec.jobnum);
	}

protected void openBatch210()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(wsaaParmCompany);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
		/*220-READ-T5679.                                                  */
		/*MOVE SPACES                 TO ITEM-PARAMS.                  */
		/*MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/*MOVE WSAA-PARM-COMPANY      TO ITEM-ITEMCOY.                 */
		/*MOVE T5679                  TO ITEM-ITEMTABL.                */
		/*MOVE PARM-TRANSCODE         TO ITEM-ITEMITEM.                */
		/*MOVE ITEMREC                TO ITEM-FORMAT.                  */
		/*MOVE READR                  TO ITEM-FUNCTION.                */
		/*CALL 'ITEMIO' USING ITEM-PARAMS.                             */
		/*IF ITEM-STATUZ              NOT = O-K                        */
		/*    MOVE ITEM-PARAMS        TO CONR-PARAMS                   */
		/*    MOVE ITEM-STATUZ        TO CONR-STATUZ                   */
		/*    PERFORM 006-DATABASE-ERROR.                              */
		/*MOVE ITEM-GENAREA           TO T5679-T5679-REC.              */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaParmCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(runparmrec.transcode);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readT5645230()
	{
		/*  Read the accounting rules table*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaParmCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readFirstRecord300()
	{
		/*BEGIN-READING*/
		/*  Define the query required by declaring a cursor*/
		sqlloan1 = " SELECT  LO.CHDRCOY, LO.CHDRNUM, LO.LOANNUMBER, LO.LOANTYPE, LO.LOANCURR, LO.VALIDFLAG, LO.LOANORIGAM, LO.LOANSTDATE, LO.LSTCAPLAMT, LO.LSTCAPDATE, LO.NXTCAPDATE, LO.LSTINTBDTE, LO.NXTINTBDTE" +
" FROM   " + getAppVars().getTableNameOverriden("LOANPF") + "  LO" +
" WHERE LO.CHDRCOY = ?" +
" AND LO.VALIDFLAG = '1'" +
" AND (LO.NXTCAPDATE < ?" +
" OR LO.NXTCAPDATE = ?)" +
" ORDER BY LO.CHDRCOY, LO.CHDRNUM, LO.LOANNUMBER";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlloan1conn = getAppVars().getDBConnectionForTable(new com.csc.life.contractservicing.dataaccess.LoanpfTableDAM());
			sqlloan1ps = getAppVars().prepareStatementEmbeded(sqlloan1conn, sqlloan1, "LOANPF");
			getAppVars().setDBString(sqlloan1ps, 1, wsaaParmCompany);
			getAppVars().setDBNumber(sqlloan1ps, 2, wsaaSqlEffdate);
			getAppVars().setDBNumber(sqlloan1ps, 3, wsaaSqlEffdate);
			sqlloan1rs = getAppVars().executeQuery(sqlloan1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError();
			throw new RuntimeException("B5031: ERROR (1) - Unexpected return from a transformed GO TO statement: SQL-ERROR");
		}
		/*EXIT*/
	}

protected void processRecords400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start400();
				case readPrimary420:
					readPrimary420();
				case capitalisation440:
					capitalisation440();
				case readNextRecord460:
					readNextRecord460();
				case exit490:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start400()
	{
		updateReqd004();
		if (isEQ(controlrec.flag, "N")) {
			goTo(GotoLabel.readNextRecord460);
		}
	}

protected void readPrimary420()
	{
		/* Get record retrieved via SQL*/
		fetchPrimary500();
		/* check for E-O-F.*/
		if ((endOfFile.isTrue())) {
			if (isNE(wsaaChdrnumOld, SPACES)) {
				/*    remove any existing softlocks*/
				removeSftlock10000();
			}
			goTo(GotoLabel.exit490);
		}
		/* IF Contract number has changed, we need to unlock record*/
		/*  previously locked*/
		if (isNE(wsaaChdrnumOld, sqlLoanpfInner.chdrnum)
		&& isNE(wsaaChdrnumOld, SPACES)) {
			removeSftlock10000();
		}
		/* Validate contract header status codes.  If contract status   */
		/* codes are not valid, do not process the loan record.         */
		wsaaChdrValid = "N";
		validateContract600();
		if (isNE(wsaaChdrValid, "Y")) {
			goTo(GotoLabel.readNextRecord460);
		}
		/* Update control total - we have a LOAN record to process*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/* process Loan record read - calculate interest since last*/
		/* interest billing date and post ACMVs accordingly.*/
		wsaaCurbal.set(ZERO);
		wsaaPostedAmount.set(ZERO);
		wsaaTotalLoanDebt.set(ZERO);
		wsaaSequenceNo.set(ZERO);
		wsaaReadPayr.set(SPACES);
		updateContractHeader1000();
		/* check whether we need to calculate any interest due*/
		/* - user may not have run Billing interest program first*/
		if (isGT(sqlLoanpfInner.lstintbdte, wsaaSqlEffdate)
		|| isEQ(sqlLoanpfInner.lstintbdte, wsaaSqlEffdate)) {
			goTo(GotoLabel.capitalisation440);
		}
		calculateInterest2000();
		updateLoanRecord3000();
		postInterestAcmvs4000();
		writeBextRecord5000();
	}

protected void capitalisation440()
	{
		totalLoanDebt6000();
		capitalise7000();
		checkSurrVal8000();
		ptrnBatcup9000();
	}

protected void readNextRecord460()
	{
		goTo(GotoLabel.readPrimary420);
	}

protected void fetchPrimary500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					fetchRecord510();
				case eof580:
					eof580();
				case exit590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fetchRecord510()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlloan1rs)) {
				getAppVars().getDBObject(sqlloan1rs, 1, sqlLoanpfInner.chdrcoy);
				getAppVars().getDBObject(sqlloan1rs, 2, sqlLoanpfInner.chdrnum);
				getAppVars().getDBObject(sqlloan1rs, 3, sqlLoanpfInner.loannumber);
				getAppVars().getDBObject(sqlloan1rs, 4, sqlLoanpfInner.loantype);
				getAppVars().getDBObject(sqlloan1rs, 5, sqlLoanpfInner.loancurr);
				getAppVars().getDBObject(sqlloan1rs, 6, sqlLoanpfInner.validflag);
				getAppVars().getDBObject(sqlloan1rs, 7, sqlLoanpfInner.loanorigam);
				getAppVars().getDBObject(sqlloan1rs, 8, sqlLoanpfInner.loanstdate);
				getAppVars().getDBObject(sqlloan1rs, 9, sqlLoanpfInner.lstcaplamt);
				getAppVars().getDBObject(sqlloan1rs, 10, sqlLoanpfInner.lstcapdate);
				getAppVars().getDBObject(sqlloan1rs, 11, sqlLoanpfInner.nxtcapdate);
				getAppVars().getDBObject(sqlloan1rs, 12, sqlLoanpfInner.lstintbdte);
				getAppVars().getDBObject(sqlloan1rs, 13, sqlLoanpfInner.nxtintbdte);
			}
			else {
				goTo(GotoLabel.eof580);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError();
			throw new RuntimeException("B5031: ERROR (2) - Unexpected return from a transformed GO TO statement: SQL-ERROR");
		}
		goTo(GotoLabel.exit590);
	}

protected void eof580()
	{
		wsaaEof.set("Y");
	}

protected void validateContract600()
	{
		readr610();
	}

protected void readr610()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(sqlLoanpfInner.chdrcoy);
		chdrmjaIO.setChdrnum(sqlLoanpfInner.chdrnum);
		chdrmjaIO.setFunction(varcom.readr);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		|| isNE(chdrmjaIO.getValidflag(), "1")) {
			conerrrec.params.set(chdrmjaIO.getParams());
			conerrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError006();
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			validateChdrStatus620();
		}
	}

protected void validateChdrStatus620()
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()], chdrmjaIO.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				validateChdrPremStatus640();
			}
		}
		/*EXIT*/
	}

protected void validateChdrPremStatus640()
	{
		/*CHECK*/
		if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()], chdrmjaIO.getPstatcode())) {
			wsaaSub.set(13);
			wsaaChdrValid = "Y";
		}
		/*EXIT*/
	}

protected void finished900()
	{
		/*START*/
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlloan1conn, sqlloan1ps, sqlloan1rs);
		/*EXIT*/
	}

protected void updateContractHeader1000()
	{
		try {
			start1000();
			saveFields1040();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start1000()
	{
		/* if contract has already been updated because there is more than*/
		/* one loan against the contract, do not increment the TRANNO.*/
		if (isEQ(wsaaChdrnumOld, sqlLoanpfInner.chdrnum)) {
			goTo(GotoLabel.exit1090);
		}
		/* Lock Contract Header record first*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsaaParmCompany);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sqlLoanpfInner.chdrnum);
		sftlockrec.transaction.set(runparmrec.transcode);
		sftlockrec.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			conerrrec.params.set(sftlockrec.sftlockRec);
			conerrrec.statuz.set(sftlockrec.statuz);
			systemError005();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			conerrrec.params.set(sftlockrec.sftlockRec);
			conerrrec.statuz.set(sftlockrec.statuz);
			systemError005();
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(sqlLoanpfInner.chdrcoy);
		chdrmjaIO.setChdrnum(sqlLoanpfInner.chdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		|| isNE(chdrmjaIO.getValidflag(), "1")) {
			conerrrec.params.set(chdrmjaIO.getParams());
			conerrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError006();
		}
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(chdrmjaIO.getParams());
			conerrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError006();
		}
	}

protected void saveFields1040()
	{
		/* Note contract currency, type and tranno*/
		wsaaTranno.set(chdrmjaIO.getTranno());
		wsaaCnttype.set(chdrmjaIO.getCnttype());
		wsaaChdrnumOld.set(chdrmjaIO.getChdrnum());
		/* Read T6633 for this contract type so that we can get interest*/
		/*  rates, next interest & capitalisation dates.*/
		readT66331200();
	}

protected void readT66331200()
	{
		start1200();
	}

protected void start1200()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsaaParmCompany);
		itdmIO.setItemtabl(t6633);
		itdmIO.setItemitem(wsaaCnttype);
		itdmIO.setItmfrm(wsaaSqlEffdate);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(itdmIO.getStatuz());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(), wsaaParmCompany)
		|| isNE(itdmIO.getItemtabl(), t6633)
		|| isNE(itdmIO.getItemitem(), wsaaCnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaCnttype);
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(e723);
			databaseError006();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
	}

protected void calculateInterest2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* Set up INTCALC linkage and call...*/
		/* Don't forget that the value returned is in LOAN currency.*/
		intcalcrec.intcalcRec.set(SPACES);
		intcalcrec.loanNumber.set(sqlLoanpfInner.loannumber);
		intcalcrec.chdrcoy.set(sqlLoanpfInner.chdrcoy);
		intcalcrec.chdrnum.set(sqlLoanpfInner.chdrnum);
		intcalcrec.cnttype.set(wsaaCnttype);
		intcalcrec.interestTo.set(wsaaSqlEffdate);
		intcalcrec.interestFrom.set(sqlLoanpfInner.lstintbdte);
		wsaaIntFromDate.set(sqlLoanpfInner.lstintbdte);
		intcalcrec.loanorigam.set(sqlLoanpfInner.lstcaplamt);
		intcalcrec.lastCaplsnDate.set(sqlLoanpfInner.lstcapdate);
		intcalcrec.loanStartDate.set(sqlLoanpfInner.loanstdate);
		intcalcrec.interestAmount.set(ZERO);
		intcalcrec.loanCurrency.set(sqlLoanpfInner.loancurr);
		intcalcrec.loanType.set(sqlLoanpfInner.loantype);
		callProgram(Intcalc.class, intcalcrec.intcalcRec);
		if (isNE(intcalcrec.statuz, varcom.oK)) {
			conerrrec.params.set(intcalcrec.intcalcRec);
			conerrrec.statuz.set(intcalcrec.statuz);
			databaseError006();
		}
		/* MOVE INTC-INTEREST-AMOUNT   TO ZRDP-AMOUNT-IN.               */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO INTC-INTEREST-AMOUNT.         */
		if (isNE(intcalcrec.interestAmount, 0)) {
			zrdecplrec.amountIn.set(intcalcrec.interestAmount);
			a000CallRounding();
			intcalcrec.interestAmount.set(zrdecplrec.amountOut);
		}
	}

protected void updateLoanRecord3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Update LOAN record - reset next & last interest billing dates*/
		loanIO.setParams(SPACES);
		loanIO.setChdrcoy(sqlLoanpfInner.chdrcoy);
		loanIO.setChdrnum(sqlLoanpfInner.chdrnum);
		loanIO.setLoanNumber(sqlLoanpfInner.loannumber);
		loanIO.setFunction(varcom.readh);
		loanIO.setFormat(formatsInner.loanrec);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(loanIO.getParams());
			conerrrec.statuz.set(loanIO.getStatuz());
			databaseError006();
		}
		loanIO.setValidflag("1");
		loanIO.setLastIntBillDate(wsaaSqlEffdate);
		/* Calculate when the next interest billing date will be*/
		nextIntBillDate3100();
		loanIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(loanIO.getParams());
			conerrrec.statuz.set(loanIO.getStatuz());
			databaseError006();
		}
	}

protected void nextIntBillDate3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3100();
				case datcon2Loop3150:
					datcon2Loop3150();
				case datcon4Loop3160:
					datcon4Loop3160();
				case exit3190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3100()
	{
		/* Check the interest  details on T6633 in the following*/
		/*  order: i) Calculate interest on Loan anniv ... Y/N*/
		/*        ii) Calculate interest on Policy anniv.. Y/N*/
		/*       iii) Check Int freq & whether a specific Day is chosen*/
		wsaaLoanDate.set(sqlLoanpfInner.loanstdate);
		wsaaEffdate.set(wsaaSqlEffdate);
		wsaaContractDate.set(chdrmjaIO.getOccdate());
		/* Check for loan anniversary flag set*/
		/* IF set,*/
		/*    set next interest billing date to be on the next loan anniv*/
		/*     date after the Effective date we are using now.*/
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(sqlLoanpfInner.loanstdate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			while ( !(isGT(datcon2rec.intDate2, wsaaSqlEffdate))) {
				callDatcon23300();
				datcon2rec.intDate1.set(datcon2rec.intDate2);
			}

			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			goTo(GotoLabel.exit3190);
		}
		/* IF T6633-LOAN-ANNIV-INTEREST = 'Y'                           */
		/*    AND ( WSAA-EFFDATE-MD     > WSAA-LOAN-MD OR               */
		/*          WSAA-EFFDATE-MD     = WSAA-LOAN-MD )                */
		/*     MOVE ZEROS                TO WSAA-NEW-DATE               */
		/*     MOVE WSAA-LOAN-MD         TO WSAA-NEW-MD                 */
		/*     MOVE WSAA-EFFDATE-YEAR    TO WSAA-NEW-YEAR               */
		/*now add 1 year to 'new' date for next interest billing date     */
		/*     MOVE SPACES               TO DTC2-DATCON2-REC            */
		/*     MOVE WSAA-NEW-DATE        TO DTC2-INT-DATE-1             */
		/*     MOVE '01'                 TO DTC2-FREQUENCY              */
		/*     MOVE 1                    TO DTC2-FREQ-FACTOR            */
		/*     PERFORM 3300-CALL-DATCON2                                */
		/*     MOVE DTC2-INT-DATE-2    TO LOAN-NEXT-INT-BILL-DATE       */
		/*     GO TO 3190-EXIT                                          */
		/* END-IF.                                                      */
		/* IF T6633-LOAN-ANNIV-INTEREST = 'Y'                           */
		/*    AND WSAA-EFFDATE-MD       < WSAA-LOAN-MD                  */
		/* set next interest billing date to be on the next loan anniv  */
		/*  date after the Effective date we are using now.             */
		/*     MOVE ZEROS              TO WSAA-NEW-DATE                 */
		/*     MOVE WSAA-LOAN-MD       TO WSAA-NEW-MD                   */
		/*     MOVE WSAA-EFFDATE-YEAR  TO WSAA-NEW-YEAR                 */
		/*     MOVE WSAA-NEW-DATE      TO LOAN-NEXT-INT-BILL-DATE       */
		/*     GO TO 3190-EXIT                                          */
		/* END-IF.                                                      */
		/* Check for contract anniversary flag set                         */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next contract    */
		/*     anniversary date after the Effective date we are using now. */
		if (isEQ(t6633rec.policyAnnivInterest, "Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			while ( !(isGT(datcon2rec.intDate2, wsaaSqlEffdate))) {
				callDatcon23300();
				datcon2rec.intDate1.set(datcon2rec.intDate2);
			}

			loanIO.setNextIntBillDate(datcon2rec.intDate2);
			goTo(GotoLabel.exit3190);
		}
		/* IF T6633-POLICY-ANNIV-INTEREST = 'Y'                         */
		/*    AND ( WSAA-EFFDATE-MD       > WSAA-CONTRACT-MD OR         */
		/*          WSAA-EFFDATE-MD       = WSAA-CONTRACT-MD )          */
		/*     MOVE ZEROS                TO WSAA-NEW-DATE               */
		/*     MOVE WSAA-CONTRACT-MD     TO WSAA-NEW-MD                 */
		/*     MOVE WSAA-EFFDATE-YEAR    TO WSAA-NEW-YEAR               */
		/*now add 1 year to 'new' date for next interest billing date     */
		/*     MOVE SPACES               TO DTC2-DATCON2-REC            */
		/*     MOVE WSAA-NEW-DATE        TO DTC2-INT-DATE-1             */
		/*     MOVE '01'                 TO DTC2-FREQUENCY              */
		/*     MOVE 1                    TO DTC2-FREQ-FACTOR            */
		/*     PERFORM 3300-CALL-DATCON2                                */
		/*     MOVE DTC2-INT-DATE-2    TO LOAN-NEXT-INT-BILL-DATE       */
		/*     GO TO 3190-EXIT                                          */
		/* END-IF.                                                      */
		/* IF T6633-POLICY-ANNIV-INTEREST = 'Y'                         */
		/*    AND WSAA-EFFDATE-MD         < WSAA-CONTRACT-MD            */
		/* set next interest billing date to be on the next loan anniv  */
		/*  date after the Effective date we are using now.             */
		/*     MOVE ZEROS              TO WSAA-NEW-DATE                 */
		/*     MOVE WSAA-CONTRACT-MD   TO WSAA-NEW-MD                   */
		/*     MOVE WSAA-EFFDATE-YEAR  TO WSAA-NEW-YEAR                 */
		/*     MOVE WSAA-NEW-DATE      TO LOAN-NEXT-INT-BILL-DATE       */
		/*     GO TO 3190-EXIT                                          */
		/* END-IF.                                                      */
		/* Get here so the next interest calc. date isn't based on loan*/
		/*  or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified*/
		/* ...if not, use the Loan day*/
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet3800();
		}
		else {
			if (isNE(t6633rec.interestDay, ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNewDate);
		datcon2rec.freqFactor.set(1);
		/* check if table T6633 has a fixed frequency for interest calcs,*/
		/* ...if not, use 1 year as the default interest calc. frequency.*/
		if (isEQ(t6633rec.interestFrequency, SPACES)) {
			datcon2rec.frequency.set("01");
		}
		else {
			/*        MOVE T6633-INTEREST-FREQUENCY TO DTC2-FREQUENCY          */
			datcon4rec.frequency.set(t6633rec.interestFrequency);
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate1.set(wsaaNewDate);
			wsaaOccdate.set(wsaaNewDate);
			datcon4rec.intDate2.set(0);
			datcon4rec.billdayNum.set(wsaaOccDd);
			datcon4rec.billmonthNum.set(wsaaOccMm);
			datcon4rec.freqFactor.set(1);
			goTo(GotoLabel.datcon4Loop3160);
		}
	}

protected void datcon2Loop3150()
	{
		callDatcon23300();
		if (isLTE(datcon2rec.intDate2, wsaaSqlEffdate)) {
			datcon2rec.intDate1.set(datcon2rec.intDate2);
			goTo(GotoLabel.datcon2Loop3150);
		}
		wsaaNewDate.set(datcon2rec.intDate2);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet3800();
			loanIO.setNextIntBillDate(wsaaNewDate);
		}
		else {
			loanIO.setNextIntBillDate(datcon2rec.intDate2);
		}
	}

protected void datcon4Loop3160()
	{
		callDatcon43400();
		if (isLTE(datcon4rec.intDate2, wsaaSqlEffdate)) {
			datcon4rec.intDate1.set(datcon4rec.intDate2);
			goTo(GotoLabel.datcon4Loop3160);
		}
		wsaaNewDate.set(datcon4rec.intDate2);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet3800();
			loanIO.setNextIntBillDate(wsaaNewDate);
		}
		else {
			loanIO.setNextIntBillDate(datcon4rec.intDate2);
		}
	}

protected void callDatcon23300()
	{
		/*START*/
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			conerrrec.params.set(datcon2rec.datcon2Rec);
			conerrrec.statuz.set(datcon2rec.statuz);
			systemError005();
		}
		/*EXIT*/
	}

protected void callDatcon43400()
	{
		/*START*/
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			conerrrec.params.set(datcon4rec.datcon4Rec);
			conerrrec.statuz.set(datcon4rec.statuz);
			systemError005();
		}
		/*EXIT*/
	}

protected void dateSet3800()
	{
		/*START*/
		/* we have to check that the date we are going to call Datcon2*/
		/*  with is a valid from-date... ie IF the interest/capn day in*/
		/*  T6633 is > 28, we have to make sure the from-date isn't*/
		/*  something like 31/02/nnnn or 31/06/nnnn*/
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
	}

protected void postInterestAcmvs4000()
	{
		start4000();
	}

protected void start4000()
	{
		/* Set up lifacmv fields.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batctrcde.set(runparmrec.transcode);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(sqlLoanpfInner.chdrcoy);
		lifacmvrec.genlcoy.set(sqlLoanpfInner.chdrcoy);
		lifacmvrec.rdocnum.set(sqlLoanpfInner.chdrnum);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(sqlLoanpfInner.loancurr);
		lifacmvrec.origamt.set(intcalcrec.interestAmount);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(wsaaSqlEffdate);
		lifacmvrec.tranref.set(sqlLoanpfInner.chdrnum);
		/* Get item description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(sqlLoanpfInner.chdrcoy);
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaProg);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(sqlLoanpfInner.chdrnum);
		wsaaLoanNumber.set(loanIO.getLoanNumber());
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		/* post the interest to the Loan debit account ( depending on*/
		/*  whether it is a policy loan or an APL ).*/
		/* IF LOAN-LOAN-TYPE = 'P'                                      */
		if (isEQ(sqlLoanpfInner.loantype, "P")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
		/* post the interest to the total interest accrued account*/
		/*  - either the Policy or APL subaccount.*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		/* IF LOAN-LOAN-TYPE = 'P'                                      */
		if (isEQ(sqlLoanpfInner.loantype, "P")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
	}

protected void writeBextRecord5000()
	{
		start5000();
	}

protected void start5000()
	{
		bextIO.setParams(SPACES);
		/* Set up all BEXT fields here before writing record - details*/
		/*  come from CHDRMJA and PAYR files.*/
		bextIO.setInstfrom(wsaaIntFromDate);
		bextIO.setInstto(wsaaSqlEffdate);
		bextIO.setBtdate(wsaaSqlEffdate);
		bextIO.setBilldate(wsaaSqlEffdate);
		bextIO.setBillcd(wsaaSqlEffdate);
		bextIO.setInstamt01(intcalcrec.interestAmount);
		bextIO.setInstamt02(ZERO);
		bextIO.setInstamt03(ZERO);
		bextIO.setInstamt04(ZERO);
		bextIO.setInstamt05(ZERO);
		bextIO.setInstamt06(intcalcrec.interestAmount);
		bextIO.setInstamt07(ZERO);
		bextIO.setInstamt08(ZERO);
		bextIO.setInstamt09(ZERO);
		bextIO.setInstamt10(ZERO);
		bextIO.setInstamt11(ZERO);
		bextIO.setInstamt12(ZERO);
		bextIO.setInstamt13(ZERO);
		bextIO.setInstamt14(ZERO);
		bextIO.setInstamt15(ZERO);
		bextIO.setInstjctl(wsaaJctlkey.jctlKey);
		bextIO.setChdrnum(sqlLoanpfInner.chdrnum);
		bextIO.setChdrcoy(sqlLoanpfInner.chdrcoy);
		bextIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		bextIO.setServunit(chdrmjaIO.getServunit());
		bextIO.setCnttype(chdrmjaIO.getCnttype());
		bextIO.setOccdate(chdrmjaIO.getOccdate());
		bextIO.setCcdate(chdrmjaIO.getCcdate());
		bextIO.setCownpfx(chdrmjaIO.getCownpfx());
		bextIO.setCowncoy(chdrmjaIO.getCowncoy());
		bextIO.setCownnum(chdrmjaIO.getCownnum());
		bextIO.setInstcchnl(chdrmjaIO.getCollchnl());
		bextIO.setCntbranch(chdrmjaIO.getCntbranch());
		bextIO.setAgntpfx(chdrmjaIO.getAgntpfx());
		bextIO.setAgntcoy(chdrmjaIO.getAgntcoy());
		bextIO.setAgntnum(chdrmjaIO.getAgntnum());
		bextIO.setOutflag(SPACES);
		bextIO.setPayflag(SPACES);
		bextIO.setBilflag(SPACES);
		bextIO.setGrupkey(SPACES);
		bextIO.setMembsel(SPACES);
		bextIO.setSupflag("N");
		readClientRole5100();
		bextIO.setPayrpfx(wsaaPayrpfx);
		bextIO.setPayrcoy(wsaaPayrcoy);
		bextIO.setPayrnum(wsaaPayrnum);
		getPayrDetails5200();
		checkMandateDetails5300();
		bextIO.setMandref(payrIO.getMandref());
		bextIO.setPtdate(payrIO.getPtdate());
		bextIO.setBillchnl(payrIO.getBillchnl());
		bextIO.setInstbchnl(payrIO.getBillchnl());
		bextIO.setInstfreq(payrIO.getBillfreq());
		bextIO.setCntcurr(payrIO.getBillcurr());
		bextIO.setBankkey(wsaaBankkey);
		bextIO.setBankacckey(wsaaBankacckey);
		bextIO.setFacthous(wsaaFacthous);
		bextIO.setNextdate(payrIO.getNextdate());
		bextIO.setMandstat(wsaaMandstat);
		bextIO.setBankcode(t3629rec.bankcode);
		bextIO.setGlmap(wsaaGlmapXx);
		bextIO.setSacscode(wsaaSacscodeXx);
		bextIO.setSacstyp(wsaaSacstypeXx);
		bextIO.setEffdatex(ZERO);
		bextIO.setDdderef(ZERO);
		bextIO.setJobno(ZERO);
		bextIO.setFunction(varcom.writr);
		bextIO.setFormat(formatsInner.bextrec);
		SmartFileCode.execute(appVars, bextIO);
		if (isNE(bextIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(bextIO.getParams());
			conerrrec.statuz.set(bextIO.getStatuz());
			databaseError006();
		}
	}

protected void readClientRole5100()
	{
		start5100();
	}

protected void start5100()
	{
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx(chdrmjaIO.getChdrpfx());
		clrfIO.setForecoy(sqlLoanpfInner.chdrcoy);
		wsaaChdrnum.set(sqlLoanpfInner.chdrnum);
		wsaaPayrseqno.set("1");
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(clrfIO.getParams());
			conerrrec.statuz.set(clrfIO.getStatuz());
			databaseError006();
		}
		wsaaPayrpfx.set(clrfIO.getClntpfx());
		wsaaPayrcoy.set(clrfIO.getClntcoy());
		wsaaPayrnum.set(clrfIO.getClntnum());
	}

protected void getPayrDetails5200()
	{
		start5200();
	}

protected void start5200()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(sqlLoanpfInner.chdrcoy);
		payrIO.setChdrnum(sqlLoanpfInner.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(payrIO.getParams());
			conerrrec.statuz.set(payrIO.getStatuz());
			databaseError006();
		}
		wsaaReadPayr.set("Y");
	}

protected void checkMandateDetails5300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start5300();
				case readT36295320:
					readT36295320();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5300()
	{
		if (isNE(payrIO.getMandref(), SPACES)) {
			/*     IF LOAN-LOAN-TYPE = 'P'                                  */
			if (isEQ(sqlLoanpfInner.loantype, "P")) {
				wsaaGlmapXx.set(t5645rec.glmap01);
				wsaaSacscodeXx.set(t5645rec.sacscode01);
				wsaaSacstypeXx.set(t5645rec.sacstype01);
			}
			else {
				wsaaGlmapXx.set(t5645rec.glmap05);
				wsaaSacscodeXx.set(t5645rec.sacscode05);
				wsaaSacstypeXx.set(t5645rec.sacstype05);
			}
		}
		else {
			wsaaGlmapXx.set(SPACES);
			wsaaSacscodeXx.set(SPACES);
			wsaaSacstypeXx.set(SPACES);
			wsaaBankacckey.set(SPACES);
			wsaaBankkey.set(SPACES);
			wsaaFacthous.set(SPACES);
			/*     MOVE SPACES             TO MAND-MANDSTAT                 */
			wsaaMandstat.set(SPACES);
			goTo(GotoLabel.readT36295320);
		}
		mandIO.setParams(SPACES);
		mandIO.setPayrcoy(wsaaPayrcoy);
		mandIO.setPayrnum(wsaaPayrnum);
		mandIO.setMandref(payrIO.getMandref());
		mandIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(mandIO.getParams());
			conerrrec.statuz.set(mandIO.getStatuz());
			databaseError006();
		}
		/*   Move the Bank Details for Direct Debit for later*/
		/*   use on the BEXT file*/
		wsaaBankkey.set(mandIO.getBankkey());
		wsaaBankacckey.set(mandIO.getBankacckey());
		wsaaMandstat.set(mandIO.getMandstat());
		clbaddbIO.setParams(SPACES);
		clbaddbIO.setClntpfx("CN");
		clbaddbIO.setClntcoy(wsaaPayrcoy);
		clbaddbIO.setClntnum(wsaaPayrnum);
		clbaddbIO.setBankkey(mandIO.getBankkey());
		clbaddbIO.setBankacckey(mandIO.getBankacckey());
		clbaddbIO.setFormat(formatsInner.clbaddbrec);
		clbaddbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clbaddbIO);
		if (isNE(clbaddbIO.getStatuz(), varcom.oK)
		&& isNE(clbaddbIO.getStatuz(), varcom.mrnf)) {
			conerrrec.params.set(clbaddbIO.getParams());
			conerrrec.statuz.set(clbaddbIO.getStatuz());
			databaseError006();
		}
		/* Move Factoring House for use in the BEXT file.*/
		wsaaFacthous.set(clbaddbIO.getFacthous());
	}

protected void readT36295320()
	{
		/* Read T3629 for bank code.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(payrIO.getBillcurr());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
	}

protected void totalLoanDebt6000()
	{
		start6000();
	}

protected void start6000()
	{
		/* find current amount of interest owed - read ACBL record*/
		wsaaCurbal.set(ZERO);
		acblIO.setParams(SPACES);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(sqlLoanpfInner.chdrnum);
		wsaaLoanNumber.set(sqlLoanpfInner.loannumber);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setRldgcoy(sqlLoanpfInner.chdrcoy);
		acblIO.setOrigcurr(sqlLoanpfInner.loancurr);
		/* IF LOAN-LOAN-TYPE           = 'P'                            */
		if (isEQ(sqlLoanpfInner.loantype, "P")) {
			acblIO.setSacscode(t5645rec.sacscode01);
			acblIO.setSacstyp(t5645rec.sacstype01);
		}
		else {
			acblIO.setSacscode(t5645rec.sacscode05);
			acblIO.setSacstyp(t5645rec.sacstype05);
		}
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			conerrrec.params.set(acblIO.getParams());
			conerrrec.statuz.set(acblIO.getStatuz());
			databaseError006();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			wsaaCurbal.set(ZERO);
		}
		else {
			wsaaCurbal.set(acblIO.getSacscurbal());
		}
		/* Find what the true current Loan Principal value is.....*/
		/* IF LOAN-LOAN-TYPE           = 'P'                            */
		if (isEQ(sqlLoanpfInner.loantype, "P")) {
			wsaaSacscode.set(t5645rec.sacscode03);
			wsaaSacstype.set(t5645rec.sacstype03);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode07);
			wsaaSacstype.set(t5645rec.sacstype07);
		}
		wsaaPostedAmount.set(ZERO);
		acmvlonIO.setParams(SPACES);
		acmvlonIO.setRldgcoy(sqlLoanpfInner.chdrcoy);
		wsaaRldgacct.set(SPACES);
		/*    MOVE CHDRNUM                TO WSAA-CHDRNUM.                 */
		wsaaRldgChdrnum.set(sqlLoanpfInner.chdrnum);
		wsaaLoanNumber.set(sqlLoanpfInner.loannumber);
		acmvlonIO.setRldgacct(wsaaRldgacct);
		acmvlonIO.setSacscode(wsaaSacscode);
		acmvlonIO.setSacstyp(wsaaSacstype);
		acmvlonIO.setEffdate(sqlLoanpfInner.lstcapdate);
		acmvlonIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvlonIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvlonIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "RLDGACCT", "SACSTYP");
		acmvlonIO.setFormat(formatsInner.acmvlonrec);
		while ( !(isEQ(acmvlonIO.getStatuz(), varcom.endp))) {
			processAcmvlons6100();
		}

		/* Add the last capitalised amount to the total of any changes*/
		/*  in the principal*/
		/* - this now gives us a new principal amount which we can add to*/
		/* the total interest over the last capitalisation period to get*/
		/* our total loan debt value.*/
		/* sum principal*/
		compute(wsaaPostedAmount, 2).set(add(sqlLoanpfInner.lstcaplamt, wsaaPostedAmount));
		/* sum interest accrued and principal*/
		compute(wsaaTotalLoanDebt, 2).set(add(wsaaCurbal, wsaaPostedAmount));
	}

protected void processAcmvlons6100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start6100();
				case readNextAcmv6150:
					readNextAcmv6150();
				case exit6190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6100()
	{
		SmartFileCode.execute(appVars, acmvlonIO);
		if (isNE(acmvlonIO.getStatuz(), varcom.oK)
		&& isNE(acmvlonIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(acmvlonIO.getParams());
			conerrrec.statuz.set(acmvlonIO.getStatuz());
			databaseError006();
		}
		/*    IF ACMVLON-RLDGCOY          NOT = CHDRCOY                    */
		/*       OR ACMVLON-SACSCODE      NOT = WSAA-SACSCODE              */
		/*       OR ACMVLON-SACSTYP       NOT = WSAA-SACSTYPE              */
		/*       OR ACMVLON-RLDGACCT      NOT = WSAA-RLDGACCT              */
		/*       OR ACMVLON-EFFDATE       NOT > LSTCAPDATE                 */
		/*       OR ACMVLON-STATUZ        = ENDP                           */
		/*        MOVE ENDP               TO ACMVLON-STATUZ                */
		/*        GO TO 6190-EXIT.                                         */
		/* have split these if statements up as you do not wish            */
		/* to stop processing if the first ACMV you get to is              */
		/* equal to the capitalisation date.                               */
		/* also you need to include any payments made on the               */
		/* loan that were made on the loan start date.                     */
		if (isNE(acmvlonIO.getRldgcoy(), sqlLoanpfInner.chdrcoy)
		|| isNE(acmvlonIO.getSacscode(), wsaaSacscode)
		|| isNE(acmvlonIO.getSacstyp(), wsaaSacstype)
		|| isNE(acmvlonIO.getRldgacct(), wsaaRldgacct)
		|| isEQ(acmvlonIO.getStatuz(), varcom.endp)) {
			acmvlonIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit6190);
		}
		if (isEQ(acmvlonIO.getEffdate(), sqlLoanpfInner.lstcapdate)) {
			if (isEQ(sqlLoanpfInner.loanstdate, sqlLoanpfInner.lstcapdate)
			&& isEQ(acmvlonIO.getGlsign(), "-")) {
				/*CONTINUE_STMT*/
			}
			else {
				goTo(GotoLabel.readNextAcmv6150);
			}
		}
		/* get here so we must have a valid record*/
		/* check currency of ACMV against currency of loan - if diff,*/
		/*  we must convert ACMV value to a value in the Loan currency*/
		if (isNE(acmvlonIO.getOrigcurr(), sqlLoanpfInner.loancurr)) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(acmvlonIO.getOrigamt());
			conlinkrec.statuz.set(SPACES);
			conlinkrec.function.set("CVRT");
			conlinkrec.currIn.set(acmvlonIO.getOrigcurr());
			conlinkrec.cashdate.set(99999999);
			conlinkrec.currOut.set(sqlLoanpfInner.loancurr);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(sqlLoanpfInner.chdrcoy);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				conerrrec.params.set(conlinkrec.clnk002Rec);
				conerrrec.statuz.set(conlinkrec.statuz);
				systemError005();
			}
			else {
				if (isEQ(acmvlonIO.getGlsign(), "-")) {
					compute(conlinkrec.amountOut, 2).set(mult(conlinkrec.amountOut, -1));
				}
				if (isNE(conlinkrec.amountOut, 0)) {
					zrdecplrec.amountIn.set(conlinkrec.amountOut);
					a000CallRounding();
					conlinkrec.amountOut.set(zrdecplrec.amountOut);
				}
				wsaaPostedAmount.add(conlinkrec.amountOut);
			}
		}
		else {
			if (isEQ(acmvlonIO.getGlsign(), "-")) {
				setPrecision(acmvlonIO.getOrigamt(), 2);
				acmvlonIO.setOrigamt(mult(acmvlonIO.getOrigamt(), -1));
			}
			wsaaPostedAmount.add(acmvlonIO.getOrigamt());
		}
	}

protected void readNextAcmv6150()
	{
		/* read next ACMV record*/
		acmvlonIO.setFunction(varcom.nextr);
	}

protected void capitalise7000()
	{
		start7000();
		updateLoan7050();
	}

protected void start7000()
	{
		/* if no interest accrued, just update loan record*/
		if (isEQ(wsaaCurbal, ZERO)) {
			return ;
		}
		/* Set up lifacmv fields.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(runparmrec.company);
		lifacmvrec.batcbrn.set(runparmrec.batcbranch);
		lifacmvrec.batcactyr.set(runparmrec.acctyear);
		lifacmvrec.batcactmn.set(runparmrec.acctmonth);
		lifacmvrec.batctrcde.set(runparmrec.transcode);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(sqlLoanpfInner.chdrcoy);
		lifacmvrec.genlcoy.set(sqlLoanpfInner.chdrcoy);
		lifacmvrec.rdocnum.set(sqlLoanpfInner.chdrnum);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.origcurr.set(sqlLoanpfInner.loancurr);
		/* move total interest accrued over period to Loan princip Sub A/C*/
		lifacmvrec.origamt.set(wsaaCurbal);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(wsaaSqlEffdate);
		lifacmvrec.tranref.set(sqlLoanpfInner.chdrnum);
		/* Get item description.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(sqlLoanpfInner.chdrcoy);
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaProg);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(sqlLoanpfInner.chdrnum);
		wsaaLoanNumber.set(sqlLoanpfInner.loannumber);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		/* post the interest to the Loan principal account*/
		/* IF LOAN-LOAN-TYPE           = 'P'                            */
		if (isEQ(sqlLoanpfInner.loantype, "P")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
		/* post the interest to the interest income account - this will*/
		/*  be posted -ve to clear down the interest accrued to zeros.*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		/* IF LOAN-LOAN-TYPE           = 'P'                            */
		if (isEQ(sqlLoanpfInner.loantype, "P")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			systemError005();
		}
	}

	/**
	* <pre>
	* Now update Loan record with new Capitalisation amount & dates..
	* </pre>
	*/
protected void updateLoan7050()
	{
		updateLoanCap7100();
		/*EXIT*/
	}

protected void updateLoanCap7100()
	{
		start7100();
	}

protected void start7100()
	{
		/* Update LOAN record - reset next & last interest billing dates*/
		loanIO.setParams(SPACES);
		loanIO.setChdrcoy(sqlLoanpfInner.chdrcoy);
		loanIO.setChdrnum(sqlLoanpfInner.chdrnum);
		loanIO.setLoanNumber(sqlLoanpfInner.loannumber);
		loanIO.setFunction(varcom.readh);
		loanIO.setFormat(formatsInner.loanrec);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(loanIO.getParams());
			conerrrec.statuz.set(loanIO.getStatuz());
			databaseError006();
		}
		loanIO.setValidflag("2");
		wsaaLoanTranno.set(loanIO.getFirstTranno());
		loanIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(loanIO.getParams());
			conerrrec.statuz.set(loanIO.getStatuz());
			databaseError006();
		}
		loanIO.setValidflag("1");
		loanIO.setLastCapnDate(wsaaSqlEffdate);
		loanIO.setFirstTranno(wsaaTranno);
		/* calculate Capitalised amount to put onto Loan record.*/
		loanIO.setLastCapnLoanAmt(wsaaTotalLoanDebt);
		/* Calculate when the next interest billing date will be*/
		nextCapnDate7200();
		/*    MOVE REWRT                  TO LOAN-FUNCTION.                */
		loanIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(loanIO.getParams());
			conerrrec.statuz.set(loanIO.getStatuz());
			databaseError006();
		}
	}

protected void nextCapnDate7200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start7200();
				case datcon2Loop7250:
					datcon2Loop7250();
				case exit7290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7200()
	{
		/* Check the Capitalisation details on T6633 in the following*/
		/*  order: i) Capitalise on Loan anniv ... Y/N*/
		/*        ii) Capitalise on Policy anniv.. Y/N*/
		/*       iii) Check Cap freq & whether a specific Day is chosen*/
		wsaaLoanDate.set(sqlLoanpfInner.loanstdate);
		wsaaEffdate.set(wsaaSqlEffdate);
		wsaaContractDate.set(chdrmjaIO.getOccdate());
		wsaaNewDate.set(ZERO);
		/* Check for loan anniversary flag set*/
		/* IF set,                                                         */
		/*    set next capitalisation  date to be on the next loan anniv   */
		/*     date after the Effective date we are using now.             */
		if (isEQ(t6633rec.annloan, "Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(sqlLoanpfInner.loanstdate);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			while ( !(isGT(datcon2rec.intDate2, wsaaSqlEffdate))) {
				callDatcon23300();
				datcon2rec.intDate1.set(datcon2rec.intDate2);
			}

			loanIO.setNextCapnDate(datcon2rec.intDate2);
			goTo(GotoLabel.exit7290);
		}
		/* Check whether today is the same month & day as the original  */
		/*  loan commencement date - if not we will have to reset the   */
		/*  capitalisation date back to annually on the loan            */
		/*  commencement month & day & increment the year accordingly.  */
		/* IF T6633-ANNLOAN            = 'Y'                            */
		/*    AND WSAA-LOAN-MD         > WSAA-EFFDATE-MD                */
		/*Ex.  Loan start date  880501 ( 1/5/88 )                         */
		/*Effdate (capn date) 910406 ( 6/4/91 )  (for whatever reason)  */
		/*loan month/day =  0501,  Capn month/day = 0406                */
		/*     so 0501 > 0406 .... so next capn date will be 910501     */
		/*     MOVE WSAA-LOAN-MD       TO WSAA-NEW-MD                   */
		/*     MOVE WSAA-EFFDATE-YEAR  TO WSAA-NEW-YEAR                 */
		/*     MOVE WSAA-NEW-DATE      TO LOAN-NEXT-CAPN-DATE           */
		/*     GO TO 7290-EXIT                                          */
		/* END-IF.                                                      */
		/* IF T6633-ANNLOAN            = 'Y'                            */
		/*    AND ( WSAA-LOAN-MD       < WSAA-EFFDATE-MD                */
		/*          OR WSAA-LOAN-MD    = WSAA-EFFDATE-MD )              */
		/*Ex.  Loan start date  880501 ( 1/5/88 )                         */
		/*Effdate (capn date) 910611 (11/6/91 )  (for whatever reason)  */
		/*loan month/day =  0501,  Capn month/day = 0611                */
		/*     so 0501 < 0611 .... so next capn date will be 920501     */
		/*     MOVE WSAA-LOAN-MD       TO WSAA-NEW-MD                   */
		/*     MOVE WSAA-EFFDATE-YEAR  TO WSAA-NEW-YEAR                 */
		/*     MOVE SPACES             TO DTC2-DATCON2-REC              */
		/*     MOVE WSAA-NEW-DATE      TO DTC2-INT-DATE-1               */
		/*     MOVE '01'               TO DTC2-FREQUENCY                */
		/*     MOVE 1                  TO DTC2-FREQ-FACTOR              */
		/*     PERFORM 3300-CALL-DATCON2                                */
		/*     MOVE DTC2-INT-DATE-2    TO LOAN-NEXT-CAPN-DATE           */
		/*     GO TO 7290-EXIT                                          */
		/* END-IF.                                                      */
		/* check for contract anniversary flag set                         */
		/* IF set,                                                         */
		/*    set next capitalisation  date to be on the next contract     */
		/*     anniversary date after the Effective date we are using now. */
		if (isEQ(t6633rec.annpoly, "Y")) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			while ( !(isGT(datcon2rec.intDate2, wsaaSqlEffdate))) {
				callDatcon23300();
				datcon2rec.intDate1.set(datcon2rec.intDate2);
			}

			loanIO.setNextCapnDate(datcon2rec.intDate2);
			goTo(GotoLabel.exit7290);
		}
		/* IF T6633-ANNPOLY            = 'Y'                            */
		/*    AND WSAA-CONTRACT-MD     > WSAA-EFFDATE-MD                */
		/*Ex.  CTRT start date  880501 ( 1/5/88 )                         */
		/*Effdate (capn date) 910406 ( 6/4/91 )  (for whatever reason)  */
		/*CTRT month/day =  0501,  Capn month/day = 0406                */
		/*     so 0501 > 0406 .... so next capn date will be 910501     */
		/*     MOVE WSAA-CONTRACT-MD   TO WSAA-NEW-MD                   */
		/*     MOVE WSAA-EFFDATE-YEAR  TO WSAA-NEW-YEAR                 */
		/*     MOVE WSAA-NEW-DATE      TO LOAN-NEXT-CAPN-DATE           */
		/*     GO TO 7290-EXIT                                          */
		/* END-IF.                                                      */
		/* IF T6633-ANNPOLY             = 'Y'                           */
		/*    AND ( WSAA-CONTRACT-MD    = WSAA-EFFDATE-MD               */
		/*          OR WSAA-CONTRACT-MD < WSAA-EFFDATE-MD )             */
		/*Ex.  CTRT start date  880501 ( 1/5/88 )                         */
		/*Effdate (capn date) 910611 (11/6/91 )  (for whatever reason)  */
		/*CTRT month/day =  0501,  Capn month/day = 0611                */
		/*     so 0611 > 0501 .... so next capn date will be 920501     */
		/*     MOVE WSAA-CONTRACT-MD   TO WSAA-NEW-MD                   */
		/*     MOVE WSAA-EFFDATE-YEAR  TO WSAA-NEW-YEAR                 */
		/*     MOVE SPACES             TO DTC2-DATCON2-REC              */
		/*     MOVE WSAA-NEW-DATE      TO DTC2-INT-DATE-1               */
		/*     MOVE '01'               TO DTC2-FREQUENCY                */
		/*     MOVE 1                  TO DTC2-FREQ-FACTOR              */
		/*     PERFORM 3300-CALL-DATCON2                                */
		/*     MOVE DTC2-INT-DATE-2    TO LOAN-NEXT-CAPN-DATE           */
		/*     GO TO 7290-EXIT                                          */
		/* END-IF.                                                      */
		/* Get here so the next capitalisation date isn't based on loan*/
		/*  or contract anniversarys.*/
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified*/
		/* ...if not, use the Loan day*/
		wsaaDayCheck.set(t6633rec.day);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet3800();
		}
		else {
			if (isNE(t6633rec.day, ZERO)) {
				wsaaNewDay.set(t6633rec.day);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaNewDate);
		datcon2rec.freqFactor.set(1);
		/* check if table T6633 has a fixed frequency for capitalisation,*/
		/* ...if not, use 1 year as the default capitalisation frequency.*/
		if (isEQ(t6633rec.compfreq, SPACES)) {
			datcon2rec.frequency.set("01");
		}
		else {
			datcon2rec.frequency.set(t6633rec.compfreq);
		}
	}

protected void datcon2Loop7250()
	{
		callDatcon23300();
		if (isLTE(datcon2rec.intDate2, wsaaSqlEffdate)) {
			datcon2rec.intDate1.set(datcon2rec.intDate2);
			goTo(GotoLabel.datcon2Loop7250);
		}
		wsaaNewDate.set(datcon2rec.intDate2);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaNewMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet3800();
			loanIO.setNextCapnDate(wsaaNewDate);
		}
		else {
			loanIO.setNextCapnDate(datcon2rec.intDate2);
		}
	}

protected void checkSurrVal8000()
	{
		start8000();
	}

protected void start8000()
	{
		if (isNE(wsaaReadPayr, "Y")) {
			getPayrDetails5200();
		}
		/* we just call NFLOAN subroutine here to see whether the contract*/
		/*  we are using can continue to support the loan(s) we have on it*/
		/* IF NFLOAN decides it cannot go on, it will write a line to a*/
		/*  report , R6244, & returns a statuz of NAPL*/
		/* IF NFLOAN decides the loan can continue, then it returns a*/
		/*  statuz of O-K.*/
		/*  WE are not worried whether it returns O-K or NAPL..... if*/
		/*   the statuz is NAPL, then a line will appear on the R6244*/
		/*   report and the user can then deal with the Contract + Loan(s)*/
		nfloanrec.nfloanRec.set(SPACES);
		nfloanrec.language.set(runparmrec.language);
		nfloanrec.chdrcoy.set(sqlLoanpfInner.chdrcoy);
		nfloanrec.chdrnum.set(sqlLoanpfInner.chdrnum);
		nfloanrec.cntcurr.set(chdrmjaIO.getCntcurr());
		nfloanrec.effdate.set(wsaaSqlEffdate);
		nfloanrec.ptdate.set(chdrmjaIO.getPtdate());
		nfloanrec.batcbrn.set(runparmrec.branch);
		nfloanrec.company.set(runparmrec.company);
		nfloanrec.billfreq.set(payrIO.getBillfreq());
		nfloanrec.cnttype.set(chdrmjaIO.getCnttype());
		nfloanrec.polsum.set(chdrmjaIO.getPolsum());
		nfloanrec.polinc.set(chdrmjaIO.getPolinc());
		nfloanrec.batcBatctrcde.set(runparmrec.transcode);
		nfloanrec.outstamt.set(ZERO);
		callProgram(Nfloan.class, nfloanrec.nfloanRec);
		if (isNE(nfloanrec.statuz, varcom.oK)
		&& isNE(nfloanrec.statuz, "NAPL")) {
			conerrrec.params.set(nfloanrec.nfloanRec);
			conerrrec.statuz.set(nfloanrec.statuz);
			systemError005();
		}
		if (isEQ(nfloanrec.statuz, "NAPL")) {
			writeLetter11000();
		}
	}

protected void ptrnBatcup9000()
	{
		start9000();
	}

protected void start9000()
	{
		if (isEQ(ptrnIO.getChdrnum(), sqlLoanpfInner.chdrnum)) {
			return ;
		}
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(sqlLoanpfInner.chdrcoy);
		ptrnIO.setChdrpfx(batcdorrec.prefix);
		ptrnIO.setChdrnum(sqlLoanpfInner.chdrnum);
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setPtrneff(wsaaSqlEffdate);
		ptrnIO.setDatesub(runparmrec.effdate);
		ptrnIO.setUser(runparmrec.user);
		ptrnIO.setBatccoy(runparmrec.company);
		ptrnIO.setBatcbrn(runparmrec.batcbranch);
		ptrnIO.setBatcactyr(runparmrec.acctyear);
		ptrnIO.setBatctrcde(runparmrec.transcode);
		ptrnIO.setBatcactmn(runparmrec.acctmonth);
		String userid = ((SMARTAppVars)SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(userid);//PINNACLE-2954
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(ptrnIO.getParams());
			conerrrec.statuz.set(ptrnIO.getStatuz());
			databaseError006();
		}
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set(batcdorrec.prefix);
		batcuprec.batccoy.set(runparmrec.company);
		batcuprec.batcbrn.set(runparmrec.batcbranch);
		batcuprec.batcactyr.set(runparmrec.acctyear);
		batcuprec.batctrcde.set(runparmrec.transcode);
		batcuprec.batcactmn.set(runparmrec.acctmonth);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			conerrrec.params.set(batcuprec.batcupRec);
			conerrrec.statuz.set(batcuprec.statuz);
			systemError005();
		}
	}

protected void removeSftlock10000()
	{
		start10000();
	}

protected void start10000()
	{
		/* Unlock Contract Header record*/
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(wsaaParmCompany);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(wsaaChdrnumOld);
		sftlockrec.transaction.set(runparmrec.transcode);
		sftlockrec.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			conerrrec.params.set(sftlockrec.sftlockRec);
			conerrrec.statuz.set(sftlockrec.statuz);
			systemError005();
		}
	}

protected void writeLetter11000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start11010();
				case readTr38411020:
					readTr38411020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start11010()
	{
		/* Read Table T6634 for get letter-type.                           */
		wsaaItemCnttype.set(chdrmjaIO.getCnttype());
		wsaaItemBatctrcde.set(runparmrec.transcode);
	}

	/**
	* <pre>
	*11020-READ-T6634.
	* </pre>
	*/
protected void readTr38411020()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634        TO ITEM-ITEMITEM.        <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		/* If record not found then read again using generic key.          */
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)
			&& isNE(wsaaItemCnttype, "***")) {
				wsaaItemCnttype.set("***");
				/*     GO TO 11020-READ-T6634                           <PCPPRT>*/
				goTo(GotoLabel.readTr38411020);
			}
			else {
				conerrrec.params.set(itemIO.getParams());
				conerrrec.statuz.set(itemIO.getStatuz());
				databaseError006();
			}
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		/* IF T6634-LETTER-TYPE        = SPACES                 <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*   Get set-up parameter for call 'HLETRQS'                   */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(runparmrec.company);
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(runparmrec.effdate);
		letrqstrec.rdocpfx.set(chdrmjaIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		wsaaOkey.set(SPACES);
		wsaaOkey1.set(runparmrec.language);
		wsaaOkey2.set(sqlLoanpfInner.loannumber);
		wsaaOkey3.set(sqlLoanpfInner.loantype);
		wsaaOkey4.set(wsaaLoanTranno);
		letrqstrec.otherKeys.set(wsaaOkey);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.function.set("ADD");
		/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			conerrrec.params.set(letrqstrec.params);
			conerrrec.statuz.set(letrqstrec.statuz);
			databaseError006();
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(runparmrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sqlLoanpfInner.loancurr);
		zrdecplrec.batctrcde.set(runparmrec.transcode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(zrdecplrec.statuz);
			conerrrec.params.set(zrdecplrec.zrdecplRec);
			systemError005();
		}
		/*A900-EXIT*/
	}

protected void sqlError()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC   ");
	private FixedLengthStringData acmvlonrec = new FixedLengthStringData(10).init("ACMVLONREC");
	private FixedLengthStringData bextrec = new FixedLengthStringData(10).init("BEXTREC   ");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData clbaddbrec = new FixedLengthStringData(10).init("CLBADDBREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC   ");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData loanrec = new FixedLengthStringData(10).init("LOANREC   ");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC   ");
}
/*
 * Class transformed  from Data Structure SQL-LOANPF--INNER
 */
private static final class SqlLoanpfInner {

		/* SQL-LOANPF */
	private FixedLengthStringData loanrec1 = new FixedLengthStringData(59);
	private FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(loanrec1, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(loanrec1, 1);
	private PackedDecimalData loannumber = new PackedDecimalData(2, 0).isAPartOf(loanrec1, 9);
	private FixedLengthStringData loantype = new FixedLengthStringData(1).isAPartOf(loanrec1, 11);
	private FixedLengthStringData loancurr = new FixedLengthStringData(3).isAPartOf(loanrec1, 12);
	private FixedLengthStringData validflag = new FixedLengthStringData(1).isAPartOf(loanrec1, 15);
	private PackedDecimalData loanorigam = new PackedDecimalData(17, 2).isAPartOf(loanrec1, 16);
	private PackedDecimalData loanstdate = new PackedDecimalData(8, 0).isAPartOf(loanrec1, 25);
	private PackedDecimalData lstcaplamt = new PackedDecimalData(17, 2).isAPartOf(loanrec1, 30);
	private PackedDecimalData lstcapdate = new PackedDecimalData(8, 0).isAPartOf(loanrec1, 39);
	private PackedDecimalData nxtcapdate = new PackedDecimalData(8, 0).isAPartOf(loanrec1, 44);
	private PackedDecimalData lstintbdte = new PackedDecimalData(8, 0).isAPartOf(loanrec1, 49);
	private PackedDecimalData nxtintbdte = new PackedDecimalData(8, 0).isAPartOf(loanrec1, 54);
}
}
