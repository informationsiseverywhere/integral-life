package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PcdtpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:01
 * Class transformed from PCDTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PcdtpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 186;
	public FixedLengthStringData pcdtrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData pcdtpfRecord = pcdtrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(pcdtrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(pcdtrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum01 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum02 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum03 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum04 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum05 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum06 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum07 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum08 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum09 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData agntnum10 = DD.agntnum.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm01 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm02 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm03 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm04 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm05 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm06 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm07 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm08 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm09 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData splitBcomm10 = DD.splitc.copy().isAPartOf(pcdtrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(pcdtrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(pcdtrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PcdtpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PcdtpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PcdtpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PcdtpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PcdtpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PcdtpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PcdtpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PCDTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PLNSFX, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"TRANNO, " +
							"AGNTNUM01, " +
							"AGNTNUM02, " +
							"AGNTNUM03, " +
							"AGNTNUM04, " +
							"AGNTNUM05, " +
							"AGNTNUM06, " +
							"AGNTNUM07, " +
							"AGNTNUM08, " +
							"AGNTNUM09, " +
							"AGNTNUM10, " +
							"SPLITC01, " +
							"SPLITC02, " +
							"SPLITC03, " +
							"SPLITC04, " +
							"SPLITC05, " +
							"SPLITC06, " +
							"SPLITC07, " +
							"SPLITC08, " +
							"SPLITC09, " +
							"SPLITC10, " +
							"INSTPREM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     planSuffix,
                                     life,
                                     coverage,
                                     rider,
                                     tranno,
                                     agntnum01,
                                     agntnum02,
                                     agntnum03,
                                     agntnum04,
                                     agntnum05,
                                     agntnum06,
                                     agntnum07,
                                     agntnum08,
                                     agntnum09,
                                     agntnum10,
                                     splitBcomm01,
                                     splitBcomm02,
                                     splitBcomm03,
                                     splitBcomm04,
                                     splitBcomm05,
                                     splitBcomm06,
                                     splitBcomm07,
                                     splitBcomm08,
                                     splitBcomm09,
                                     splitBcomm10,
                                     instprem,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		planSuffix.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		tranno.clear();
  		agntnum01.clear();
  		agntnum02.clear();
  		agntnum03.clear();
  		agntnum04.clear();
  		agntnum05.clear();
  		agntnum06.clear();
  		agntnum07.clear();
  		agntnum08.clear();
  		agntnum09.clear();
  		agntnum10.clear();
  		splitBcomm01.clear();
  		splitBcomm02.clear();
  		splitBcomm03.clear();
  		splitBcomm04.clear();
  		splitBcomm05.clear();
  		splitBcomm06.clear();
  		splitBcomm07.clear();
  		splitBcomm08.clear();
  		splitBcomm09.clear();
  		splitBcomm10.clear();
  		instprem.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPcdtrec() {
  		return pcdtrec;
	}

	public FixedLengthStringData getPcdtpfRecord() {
  		return pcdtpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPcdtrec(what);
	}

	public void setPcdtrec(Object what) {
  		this.pcdtrec.set(what);
	}

	public void setPcdtpfRecord(Object what) {
  		this.pcdtpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(pcdtrec.getLength());
		result.set(pcdtrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}