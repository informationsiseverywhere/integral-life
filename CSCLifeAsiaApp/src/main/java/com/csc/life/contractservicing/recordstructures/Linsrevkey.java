package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:12
 * Description:
 * Copybook name: LINSREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linsrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linsrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData linsrevKey = new FixedLengthStringData(64).isAPartOf(linsrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData linsrevChdrcoy = new FixedLengthStringData(1).isAPartOf(linsrevKey, 0);
  	public FixedLengthStringData linsrevChdrnum = new FixedLengthStringData(8).isAPartOf(linsrevKey, 1);
  	public PackedDecimalData linsrevInstto = new PackedDecimalData(8, 0).isAPartOf(linsrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(linsrevKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linsrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linsrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}