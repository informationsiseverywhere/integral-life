package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:20
 * Description:
 * Copybook name: COVRBBRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrbbrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrbbrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrbbrKey = new FixedLengthStringData(64).isAPartOf(covrbbrFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrbbrChdrcoy = new FixedLengthStringData(1).isAPartOf(covrbbrKey, 0);
  	public FixedLengthStringData covrbbrChdrnum = new FixedLengthStringData(8).isAPartOf(covrbbrKey, 1);
  	public FixedLengthStringData covrbbrLife = new FixedLengthStringData(2).isAPartOf(covrbbrKey, 9);
  	public FixedLengthStringData covrbbrCoverage = new FixedLengthStringData(2).isAPartOf(covrbbrKey, 11);
  	public FixedLengthStringData covrbbrRider = new FixedLengthStringData(2).isAPartOf(covrbbrKey, 13);
  	public PackedDecimalData covrbbrPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrbbrKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrbbrKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrbbrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrbbrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}