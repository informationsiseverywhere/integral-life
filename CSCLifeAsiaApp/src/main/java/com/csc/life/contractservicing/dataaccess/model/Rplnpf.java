package com.csc.life.contractservicing.dataaccess.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;

public class Rplnpf implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String chdrnum;
	private String repymop;	
	private double repaymentamt;
	private double totalplprncpl;
	private double totalplint;
	private double totalaplprncpl;
	private double totalaplint;
	private double remngplprncpl;
	private double remngplint;
	private double remngaplprncpl;
	private double remngaplint;
	private String status;
	private int aprvdate;
	private int tranno;
	private String mandref;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getRepymop() {
		return repymop;
	}
	public void setRepymop(String repymop) {
		this.repymop = repymop;
	}	
	public double getRepaymentamt() {
		return repaymentamt;
	}
	public void setRepaymentamt(double repaymentamt) {
		this.repaymentamt = repaymentamt;
	}
	public double getTotalplprncpl() {
		return totalplprncpl;
	}
	public void setTotalplprncpl(double totalplprncpl) {
		this.totalplprncpl = totalplprncpl;
	}
	public double getTotalplint() {
		return totalplint;
	}
	public void setTotalplint(double totalplint) {
		this.totalplint = totalplint;
	}
	public double getTotalaplprncpl() {
		return totalaplprncpl;
	}
	public void setTotalaplprncpl(double totalaplprncpl) {
		this.totalaplprncpl = totalaplprncpl;
	}
	public double getTotalaplint() {
		return totalaplint;
	}
	public void setTotalaplint(double totalaplint) {
		this.totalaplint = totalaplint;
	}
	public double getRemngplprncpl() {
		return remngplprncpl;
	}
	public void setRemngplprncpl(double remngplprncpl) {
		this.remngplprncpl = remngplprncpl;
	}
	public double getRemngplint() {
		return remngplint;
	}
	public void setRemngplint(double remngplint) {
		this.remngplint = remngplint;
	}
	public double getRemngaplprncpl() {
		return remngaplprncpl;
	}
	public void setRemngaplprncpl(double remngaplprncpl) {
		this.remngaplprncpl = remngaplprncpl;
	}
	public double getRemngaplint() {
		return remngaplint;
	}
	public void setRemngaplint(double remngaplint) {
		this.remngaplint = remngaplint;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getAprvdate() {
		return aprvdate;
	}
	public void setAprvdate(int aprvdate) {
		this.aprvdate = aprvdate;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}

}
