package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:36
 * Description:
 * Copybook name: UNLTREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Unltrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData unltrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData unltrevKey = new FixedLengthStringData(64).isAPartOf(unltrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData unltrevChdrcoy = new FixedLengthStringData(1).isAPartOf(unltrevKey, 0);
  	public FixedLengthStringData unltrevChdrnum = new FixedLengthStringData(8).isAPartOf(unltrevKey, 1);
  	public FixedLengthStringData unltrevLife = new FixedLengthStringData(2).isAPartOf(unltrevKey, 9);
  	public FixedLengthStringData unltrevCoverage = new FixedLengthStringData(2).isAPartOf(unltrevKey, 11);
  	public FixedLengthStringData unltrevRider = new FixedLengthStringData(2).isAPartOf(unltrevKey, 13);
  	public PackedDecimalData unltrevSeqnbr = new PackedDecimalData(3, 0).isAPartOf(unltrevKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(unltrevKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(unltrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		unltrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}