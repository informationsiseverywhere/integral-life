package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52K
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr52kScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(688);
	public FixedLengthStringData dataFields = new FixedLengthStringData(352).isAPartOf(dataArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,55);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,73);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData descrs = new FixedLengthStringData(60).isAPartOf(dataFields, 144);
	public FixedLengthStringData[] descr = FLSArrayPartOfStructure(2, 30, descrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(descrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData descr01 = DD.descr.copy().isAPartOf(filler,0);
	public FixedLengthStringData descr02 = DD.descr.copy().isAPartOf(filler,30);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,204);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,212);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,213);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,221);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,268);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,315);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,323);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,333);
	public FixedLengthStringData ratebas = DD.ratebas.copy().isAPartOf(dataFields,341);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,342);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 352);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData descrsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] descrErr = FLSArrayPartOfStructure(2, 4, descrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(descrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData descr01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData descr02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ratebasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 436);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData descrsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] descrOut = FLSArrayPartOfStructure(2, 12, descrsOut, 0);
	public FixedLengthStringData[][] descrO = FLSDArrayPartOfArrayStructure(12, 1, descrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(descrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] descr01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] descr02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ratebasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr52kscreenWritten = new LongData(0);
	public LongData Sr52kprotectWritten = new LongData(0);
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public boolean hasSubfile() {
		return false;
	}


	public Sr52kScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(cnttypeOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratebasOut,new String[] {null, "41",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {descrip, chdrnum, rstate, cnttype, cownnum, payrnum, agntnum, billfreq, ctypdesc, pstate, ownername, payorname, agentname, occdate, mop, btdate, ptdate, ratebas, descr01, descr02, effdate};
		screenOutFields = new BaseData[][] {descripOut, chdrnumOut, rstateOut, cnttypeOut, cownnumOut, payrnumOut, agntnumOut, billfreqOut, ctypdescOut, pstateOut, ownernameOut, payornameOut, agentnameOut, occdateOut, mopOut, btdateOut, ptdateOut, ratebasOut, descr01Out, descr02Out, effdateOut};
		screenErrFields = new BaseData[] {descripErr, chdrnumErr, rstateErr, cnttypeErr, cownnumErr, payrnumErr, agntnumErr, billfreqErr, ctypdescErr, pstateErr, ownernameErr, payornameErr, agentnameErr, occdateErr, mopErr, btdateErr, ptdateErr, ratebasErr, descr01Err, descr02Err, effdateErr};
		screenDateFields = new BaseData[] {occdate, btdate, ptdate, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, btdateErr, ptdateErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, btdateDisp, ptdateDisp, effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52kscreen.class;
		protectRecord = Sr52kprotect.class;
	}

}
