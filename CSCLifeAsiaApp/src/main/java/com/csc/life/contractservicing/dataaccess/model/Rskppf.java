package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Rskppf {
	private long unique_Number;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private String cnttype;
	private String crtable;
	private int datefrm;
	private int dateto;
	private BigDecimal instprem;
	private BigDecimal riskprem;
	private int effDate;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private BigDecimal covrinstprem;
	private BigDecimal polfee;
	
	public BigDecimal getPolfee() {
		return polfee;
	}
	public void setPolfee(BigDecimal polfee) {
		this.polfee = polfee;
	}
	
	public long getUnique_Number() {
		return unique_Number;
	}
	public BigDecimal getCovrinstprem() {
		return covrinstprem;
	}
	public void setCovrinstprem(BigDecimal covrinstprem) {
		this.covrinstprem = covrinstprem;
	}
	public BigDecimal getInstprem() {
		return instprem;
	}
	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public void setUnique_Number(long unique_Number) {
		this.unique_Number = unique_Number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getDatefrm() {
		return datefrm;
	}
	public void setDatefrm(int datefrm) {
		this.datefrm = datefrm;
	}
	public int getDateto() {
		return dateto;
	}
	public void setDateto(int dateto) {
		this.dateto = dateto;
	}
	public BigDecimal getRiskprem() {
		return riskprem;
	}
	public void setRiskprem(BigDecimal riskprem) {
		this.riskprem = riskprem;
	}
	public int getEffDate() {
		return effDate;
	}
	public void setEffDate(int effDate) {
		this.effDate = effDate;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
}
