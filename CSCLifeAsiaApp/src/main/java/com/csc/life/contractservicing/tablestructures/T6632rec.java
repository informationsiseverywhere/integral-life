package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:35
 * Description:
 * Copybook name: T6632REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6632rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6632Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData maxpcnt = new ZonedDecimalData(5, 2).isAPartOf(t6632Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(495).isAPartOf(t6632Rec, 5, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6632Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6632Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}