package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR51T
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr51tScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(179);
	public FixedLengthStringData dataFields = new FixedLengthStringData(67).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 67);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 95);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(187);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(41).isAPartOf(subfileArea, 0);
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData apind = DD.apind.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(subfileFields,2);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,12);
	public ZonedDecimalData frmdate = DD.frmdate.copyToZonedDecimal().isAPartOf(subfileFields,20);
	public FixedLengthStringData logtype = DD.logtype.copy().isAPartOf(subfileFields,28);
	public ZonedDecimalData seqnbr = DD.seqnbr.copyToZonedDecimal().isAPartOf(subfileFields,29);
	public ZonedDecimalData todate = DD.todate.copyToZonedDecimal().isAPartOf(subfileFields,32);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(subfileFields,40);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 41);
	public FixedLengthStringData actindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData apindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData frmdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData logtypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData seqnbrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData todateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 77);
	public FixedLengthStringData[] actindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] apindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] frmdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] logtypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] seqnbrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] todateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 185);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData frmdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData todateDisp = new FixedLengthStringData(10);

	public LongData Sr51tscreensflWritten = new LongData(0);
	public LongData Sr51tscreenctlWritten = new LongData(0);
	public LongData Sr51tscreenWritten = new LongData(0);
	public LongData Sr51tprotectWritten = new LongData(0);
	public GeneralTable sr51tscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr51tscreensfl;
	}

	public Sr51tScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {seqnbr, frmdate, todate, logtype, activeInd, apind, effdate, crtuser, validflag};
		screenSflOutFields = new BaseData[][] {seqnbrOut, frmdateOut, todateOut, logtypeOut, actindOut, apindOut, effdateOut, crtuserOut, validflagOut};
		screenSflErrFields = new BaseData[] {seqnbrErr, frmdateErr, todateErr, logtypeErr, actindErr, apindErr, effdateErr, crtuserErr, validflagErr};
		screenSflDateFields = new BaseData[] {frmdate, todate, effdate};
		screenSflDateErrFields = new BaseData[] {frmdateErr, todateErr, effdateErr};
		screenSflDateDispFields = new BaseData[] {frmdateDisp, todateDisp, effdateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, cntcurr, rstate, pstate, register};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, cntcurrOut, rstateOut, pstateOut, registerOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, cntcurrErr, rstateErr, pstateErr, registerErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr51tscreen.class;
		screenSflRecord = Sr51tscreensfl.class;
		screenCtlRecord = Sr51tscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr51tprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr51tscreenctl.lrec.pageSubfile);
	}
}
