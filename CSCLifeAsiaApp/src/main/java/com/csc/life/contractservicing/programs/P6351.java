/*
 * File: P6351.java
 * Date: 30 August 2009 0:44:30
 * Author: Quipoz Limited
 * 
 * Class transformed from P6351.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
// ILIFE-1074 vchawda
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
//import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;//ILB-456
import com.csc.life.contractservicing.dataaccess.dao.IncrpfDAO;
import com.csc.life.contractservicing.screens.S6351ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CpsupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cpsupf;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Gettim;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*             P6351 - COMPONENT SELECTION MAJOR ALTS
*             ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Initialise
*----------
*
*  Skip  this  section  if  returning from an optional selection
*  (current stack position action flag = '*').
*
*  Clear the subfile ready for loading.
*
*  The  details  of  the  contract  being  enquired upon will be
*  stored  in  the  CHDRMJA I/O module. Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       The joint  owner's  client (CLTS) details if they exist.
*
*  Load the subfile as follows:
*
*  Retrieve the COVRMJA file (RETRV), if the selected plan has a
*  plan suffix  which  is  contained  within the summarised plan
*  record:  less than  or  equal  to  the  number  of summarised
*  policies (CHDR-POLSUM):  then the policy must be 'broken out'
*  with each life,  coverage  and rider for an associated policy
*  written to the  subfile  along  with  the  description of the
*  cover/rider and their short status code descriptions.
*
*  COVR-STATCODE will be decoded against T5682 - Risk Status and
*  COVR-PSTATCODE  will  be  decoded  against  T5681  -  Premium
*  Status.
*
*       The first record of the subfile will have the first life
*       details for a contract. There will be one or many lives,
*       for each  life, there will be one or many coverages. For
*       each coverage, there will be none, one or many riders.
*
*       Use a key of CHDRCOY  and CHDRNUM, life  number 01 and a
*       joint life  number 00 to perform a BEGN on the life file
*       LIFEALT.
*
*       Display the  Life  number from the LIFEALT record in the
*       Component   Number   field.  Place  the  Client  Number,
*       (LIFE-LIFCNUM),   in   the   Component   field  and  the
*       "confirmation name"  (from  CLTS)  for  the  life as the
*       Component   Description.   Use   the   Contract  Company
*       (CHDRCOY),   Contract   Number  (CHDRNUM),  Life  Number
*       (LIFE),Plan suffix from COVRMJA (If contained within the
*       summarised records,  store  the  plan  suffix number and
*       move '0000' to Plan suffix.) with Coverage and Rider set
*       to '00',then perform a BEGN on COVRMJA. This will return
*       the first coverage for the Life.
*
*       If  the stored  Plan  Suffix  equal  to  '0000'  then  a
*       selection is  required across  the whole plan, therefore
*       load all  records  with  broken out Policies placing the
*       Plan suffix in  the Hidden Field. i.e. ignore changes in
*       Plan suffix,  exit  when change of company, and contract
*       number.
*
*       Coverage details  -  write  the  coverage  record to the
*       subfile with the coverage number in the Component Number
*       field.  Obtain the Coverage Code from T5673 and place it
*       in the Component field indented by 2 spaces. Look up its
*       description from  T5687  for the element description and
*       place   this   in   the   Component  Description  field.
*       Sequentially read the next coverage/rider record.
*
*       NOTE:   read  T5673  the  Contract  Structure  table  by
*       contract  type  (from  CHDRMJA)  and  effective  on  the
*       original   inception   date   of   the   contract.  When
*       processing this table item remember that it can continue
*       onto  additional  (unlimited) table items.  These should
*       be read in turn during the processing.
*
*       Rider details -  if  the  coverage number is the same as
*       the one  put  on  the coverage subfile record above, the
*       rider number will  not  be  zero  (if  it  is, this is a
*       database error).  This is a rider on the above coverage.
*       Write the  rider  record  to  the subfile with the rider
*       number in the Component Number field and its description
*       from   T5687   in   the   Component  Description  field.
*       Sequentially  read  the  next coverage/rider record.  If
*       this  is   another   rider   record   for   the  current
*       transaction, repeat  this  rider  processing.  If  it is
*       another  coverage   for   the   same  life,  repeat  the
*       processing from  the coverage section above.  If it is a
*       coverage  for   another   life,  repeat  all  the  above
*       processing for the  next  life.  If it is a coverage for
*       another proposal (or the end of the file) all components
*       have been  loaded. On each subfile load, store in hidden
*       fields the Plan Suffix, life, cover and rider numbers.
*
*  Load all pages  required  in  the subfile and set the subfile
*  more indicator to no.
*
*Validation
*----------
*
*  There is no validation in this program.
*
*Updating
*--------
*
*  If any  Increase records exist for this contract (INCR), then
*  Warning  Message 'WARNING: Incr Pend'g del' will be displayed.
*  If the  user  presses enter then  these INCR records will  be
*  deleted  and the  increase  recalculated  as  normal  through
*  batch processing.  Additionally, any  COVR  records that have
*  the COVR-INDEXATION-IND set to 'P' (Pending)  will be updated
*  to have spaces in this field.
*
*Next Program
*------------
*
*  If "KILL" was requested move spaces to  the  current  program
*  position and action field, add 1 to the  program  pointer and
*  exit.
*
*  At this point the program will be either  searching  for  the
*  FIRST  selected  record  in  order  to pass  control  to  the
*  appropriate  generic  enquiry  program   for   the   selected
*  component  or it will be returning from one  of  the  Enquiry
*  screens after displaying some details and  searching  for the
*  NEXT selected record.
*
*  It will be able  to determine which of these two states it is
*  in by examining the Stack Action Flag.
*
*  If not returning  from  a  component (stack action is blank),
*  save the next four  programs currently on the stack. Read the
*  first record from the subfile. If this is not  selected, read
*  the next one and so on, until a selected  record is found, or
*  the end of the subfile is reached.
*
*  If a subfile  record  has been selected, look up the programs
*  required to  be  processed  from  the coverage/rider programs
*  table (T5671  -  accessed  by transaction number concatenated
*  with coverage/rider code from the subfile record). Move these
*  four programs into  the  program  stack  and  set the current
*  stack action to '*'  (so  that the system will return to this
*  program to process the next one).
*
*  RLSE the COVRMJA record, then read with the key below. If O-K
*  set up the  key details of the coverage/rider to be processed
*  (in COVRMJA using  the KEEPS func) by the  called programs as
*  follows:
*
*            Company - WSSP company
*            Contract no - from CHDRMJA
*            Plan Suffix - from hidden Plan suffix field
*            Life number - from hidden life field
*            Coverage number -  from hidden coverage field
*            Rider number - from hidden rider field
*
*  Add one to  the  program  pointer  and  exit  to  process the
*  required generic component.
*
*  If nothing was selected or there are no  more  selections  to
*  process,  continue  by  moving blanks to  the  current  Stack
*  Action  field  reload  the  saved  four programs and exit.
*
*Notes.
*------
*
*  Tables Used:
*
*T5671 - Cover/Rider Secondary Switching Key: Transaction Code
*                                           + Program Number
*T3588 - Contract Premium Status         Key: PSTATCODE
*T3623 - Contract Risk Status            Key: STATCODE
*T3673 - Contract Structure              Key: CNTTYPE
*T5688 - Contract Structure              Key: CNTTYPE
*
*Examples.
*---------
*
*  In this  example  the Contract Header record, (CHDR), has the
*  following values:
*
*CHDR-POLSUM = 3
*CHDR-POLINC = 5
*CHDR-NXTSFX = 0004
*
*  This indicates that  the number of policies in plan is 5, the
*  number summarised  is  3  and  therefore the next plan suffix
*  number is 0004.
*
*<-- File Details -->          <--------  Screen Details -------->
*
* Plan  Life Cov Rid           Component     Component Description
*Suffix                        OO OOOOOOOO   OOOOOOOOOOOOOOOO
*
* 0000   01   01  00              0001
*                                 0002
*                                 0003
*                              01 12345678   Life Name #1
*                              01   COV1     Coverage #1
* 0000   01   01  01           01     RID1   Rider #1 of Coverage #1
* 0000   01   01  02           02     RID2   Rider #2 of Coverage #1
* 0000   01   02  00           01   COV2     Coverage #2
* 0000   01   02  01           01     RID1   Rider #1 of Coverage #2
* 0000   02   01  00           02 87654321   Life Name #2
*                              01   COV1     Coverage #1
* 0000   02   01  01           01     RID1   Rider #1 of Coverage #1
* 0000   02   01  02           02     RID2   Rider #2 of Coverage #1
*
*  The  above  records  show the lives and  components  for  the
*  summary records. In this example the first  three  lines show
*  which  policies  with  the  plan  are   summarised  by  these
*  components.
*
*  At this point  the program will come upon the first component
*  for the next  policy  within  the  plan  -  one that does not
*  summarise  anything   and   so   its  display  will  be  more
*  straightforward.
*
* Plan  Life Cov Rid           Component     Component Description
*Suffix                        OO OOOOOOOO   OOOOOOOOOOOOOOOO
*
* 0004   01   01  00              0004
*                             01 12345678   Life Name #1
*                             01   COV1     Coverage #1
* 0004   01   01  01          01     RID1   Rider #1 of Coverage #1
* 0004   01   01  02          02     RID2   Rider #2 of Coverage #1
* 0004   01   01  03          03     RID3   Rider #3 of Coverage #1
* 0004   01   02  00          01   COV2     Coverage #2
* 0004   01   02  01          01     RID1   Rider #1 of Coverage #2
* 0004   01   03  00          03   COV3     Coverage #3
* 0004   02   01  00          02 87654321   Life Name #2
*                             01   COV1     Coverage #1
* 0004   02   01  01          01     RID1   Rider #1 of Coverage #1
* 0004   02   01  02          02     RID2   Rider #2 of Coverage #1
* 0005   01   01  00             0005
*                             01 12345678   Life Name #1
*                             01   COV1     Coverage #1
* 0005   01   01  01          01     RID1   Rider #1 of Coverage #1
* 0005   01   01  02          02     RID2   Rider #2 of Coverage #1
* 0005   01   01  03          03     RID3   Rider #3 of Coverage #1
* 0005   01   02  00          01   COV2     Coverage #2
* 0005   01   02  01          01     RID1   Rider #1 of Coverage #2
* 0005   01   03  00          03   COV3     Coverage #3
* 0005   02   01  00          02 87654321   Life Name #2
*                             01   COV1     Coverage #1
* 0005   02   01  01          01     RID1   Rider #1 of Coverage #1
* 0005   02   01  02          02     RID2   Rider #2 of Coverage #1
*
*
*****************************************************************
* </pre>
*/
public class P6351 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6351");
	private PackedDecimalData wsaaPrsub = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatus = "";
	private String wsaaFirstTime = "";

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-MISCELLANEOUS-FLAGS */
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");
	private FixedLengthStringData wsaaSelectFlagLife = new FixedLengthStringData(1);
	private Validator wsaaSelectionlife = new Validator(wsaaSelectFlagLife, "Y");

	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");

	private FixedLengthStringData wsaaLifeExit = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaCoverExit = new FixedLengthStringData(1);
	private Validator wsaaCoverEof = new Validator(wsaaCoverExit, "Y");

	private FixedLengthStringData wsaaLifeFound = new FixedLengthStringData(1);

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");

	private FixedLengthStringData wsaaPlanComponent = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaPlanComponent, 0).setUnsigned();

	private FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).init(1);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaLifeNo = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSelectedlife = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaLastLifeno = new FixedLengthStringData(2).init(SPACES);
	private ZonedDecimalData wsaaLastSuffix = new ZonedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaLastCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaLastRider = new FixedLengthStringData(2).init(SPACES);
	private String wsaaLapse = "T514";
	private String wsaaPup = "T575";
	//ILIFE-1074 vchawda Starts
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private static final String wsaaPremiumLetter = "HPRDLET";
	//ILIFE-1074 vchawda Ends
	private FixedLengthStringData wsaaProgramSave = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(4, 5, wsaaProgramSave, 0);

	private FixedLengthStringData wsaaInvalidCtdate = new FixedLengthStringData(1);
	private Validator invalidCtdate = new Validator(wsaaInvalidCtdate, "Y");

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
	private String g433 = "G433";
	private String g931 = "G931";
	private String j008 = "J008";
	private String r078 = "R078";
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5671 = "T5671";
	private String t5681 = "T5681";
	private String t5682 = "T5682";
	private String t5687 = "T5687";
	private String t5688 = "T5688";
	private String t5679 = "T5679";
	private String tr386 = "TR386";
		/*Contract Header File - Major Alts*/
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();

	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr386rec tr386rec = new Tr386rec();
		/*Unit transaction summary*/
	protected Batckey wsaaBatckey = new Batckey();
	protected Wsspsmart wsspsmart = new Wsspsmart();
	//ILIFE-1074 vchawda Starts 
	private Letrqstrec letrqstrec = new Letrqstrec();
	//ILIFE-1074 vchawda Ends
	private S6351ScreenVars sv =   getLScreenVars();	
	private Lifepf lifepfIO;
	private int lifepfCount;
	private int lifepfCount1;//ILIFE-8108
	private List<Lifepf> lifepfList;
	//FWANG3
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	/*Client logical file with new fields*/
	private ClntpfDAO clntpfDAO =  getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private T5687rec t5687rec = new T5687rec();
	private Itempf itempf = new Itempf();
	private Descpf descpf = null;
	private static final String rrh7 = "RRH7";	
	private static final String rrh8 = "RRH8";
	boolean rpuConfig = false;	
	boolean rpuMetNotFund;	
	int subFileCount;
	String inForce="In Force";
	String prmPay="Prm Paying";
	String t575 ="T575";
	private Clntpf clts = null;
	private static final String rrmw = "RRMW";//ILIFE-7805
	boolean compSurrPosPermission = false;
	private Cpsupf cpsupf ;
	private CpsupfDAO cpsupfDAO = getApplicationContext().getBean("cpsupfDAO", CpsupfDAO.class);
	private FixedLengthStringData wsaaIndicStatus = new FixedLengthStringData(1);
	boolean indicFound = false;
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Ptrnpf ptrnpf = new Ptrnpf();
	private List<Ptrnpf> ptrnpfList = null;
	private Ptrnpf ptrnUpdpf = new Ptrnpf();
	protected ChdrmjaTableDAM chdrupIO = new ChdrmjaTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private static final String chdrmjarec = "CHDRMJAREC";
	protected Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	List<Payrpf> payrInstlist  = null;
	private Payrpf payrpf = new Payrpf();
	private FixedLengthStringData wsaaTranno = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaRevTranno = new FixedLengthStringData(5);
	private static final String tmStatus="TM";
	private static final String pos041Confg = "SUSUR008";
	private static final String taoz ="TAOZ";
	private static final String tapd ="TAPD";

	private static final String t679 = "T679";//ILIFE-7805
	private static final String revtranscode="TAOY";
	
	
	//ILB-456 starts

	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;
	private int covrpfCount = 0;
	
	private boolean firstflag = true;
	protected Map<String, List<Itempf>> t5671ListMap;
	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		listCoverages1150, 
		exit1190, 
		exit1390, 
		covPrem1850, 
		ridPrem1860, 
		valid1880, 
		exit1890, 
		preExit, 
		exit2090, 
		exit2690, 
		exit3090, 
		exit3190, 
		exit3290, 
		continue4080, 
		updateScreen4085, 
		exit4090, 
		exit4190, 
		exit4290, 
		exit4400, 
		readProgramTable4410, 
		loadProgsToWssp4420, 
		exit4490, 
		exit4590, 
		exit4790
	}

	public P6351() {
		super();
		screenVars = sv;
		new ScreenModel("S6351", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(clts.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(clts.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clts.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(clts.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(clts.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clts.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(clts.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(clts.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(clts.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(clts.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(clts.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clts.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clts.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			retrvContract1020();
			retrvCoverage1040();
			readLifeDetails1050();
			jointLifeDetails1060();
			contractTypeStatus1070();
			subfileLoad1080();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
	//ILIFE-1074 vchawda Starts 
	/* Get today's date and time.*/
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	wsaaToday.set(datcon1rec.intDate);
	callProgram(Gettim.class, wsaaTime);
	//ILIFE-1074 vchawda Ends
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			if (isEQ(wsaaBatckey.batcBatctrcde,"T551")) {
				sv.textfield.set(tr386rec.progdesc03);
				sv.textfieldOut[varcom.bl.toInt()].set("Y");
				sv.textfieldOut[varcom.nd.toInt()].set("N");
				goTo(GotoLabel.exit1090);
			}
			else {
				if (isEQ(wsaaBatckey.batcBatctrcde,"T676")) {
					sv.textfield.set(tr386rec.progdesc04);
					sv.textfieldOut[varcom.bl.toInt()].set("Y");
					sv.textfieldOut[varcom.nd.toInt()].set("N");
					goTo(GotoLabel.exit1090);
				}
				else {
					sv.textfield.set(SPACES);
					sv.textfieldOut[varcom.bl.toInt()].set("N");
					sv.textfieldOut[varcom.nd.toInt()].set("Y");
					goTo(GotoLabel.exit1090);
				}
			}
		}
		wsaaImmexitFlag.set(SPACES);
		wsaaSelectFlag.set(SPACES);
		wsaaSelectFlagLife.set(SPACES);
		wsaaLifeExit.set(SPACES);
		wsaaCoverExit.set(SPACES);
		wsaaLastLifeno.set(SPACES);
		wsaaLastCoverage.set(SPACES);
		wsaaLastRider.set(SPACES);
		wsaaLastSuffix.set(ZERO);
		wsaaSelected.set(ZERO);
		wsaaSelectedlife.set(ZERO);
		wsaaFirstTime = "Y";
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		
		rpuConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSPUP001", appVars, "IT");
		compSurrPosPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), pos041Confg, appVars, "IT");
		if(compSurrPosPermission && isEQ(wsspcomn.flag,"S")){
			sv.flag.set("Y");
		}else{
			sv.flag.set("N");
		}
		
		if(compSurrPosPermission && (isEQ(wsaaBatckey.batcBatctrcde,taoz) ||isEQ(wsaaBatckey.batcBatctrcde,tapd))){
			sv.flagdisable.set("Y");
		}else{
			sv.flagdisable.set("N");
		}
		if(rpuConfig){
			if(isNE(wsaaBatckey.batcBatctrcde,t575)){
				rpuConfig = false;
			}
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("S6351", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvContract1020()
	{
		//ILIFE-7968 start
			//ILIFE-8312 starts - updating cache with new chdr object
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null!=chdrpf) {
		chdrpf = chdrpfDAO.getChdrpf(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1386 */
		chdrpfDAO.deleteCacheObject(chdrpf);
		chdrpfDAO.setCacheObject(chdrpf);
	}
	//ILIFE-8312 ends
			if(null==chdrpf) {
				chdrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, chdrmjaIO);
				if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(chdrmjaIO.getParams());
					fatalError600();
				}
				else {
					chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
					if(null==chdrpf) {
						fatalError600();
					}
					else {
						chdrpfDAO.setCacheObject(chdrpf);
					}
				}
			}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/*HEADER-DETAIL*/
		sv.chdrnum.set(chdrpf.getChdrnum());/* IJTI-1386 */
		sv.cnttype.set(chdrpf.getCnttype());/* IJTI-1386 */
		sv.cntcurr.set(chdrpf.getCntcurr());/* IJTI-1386 */
		sv.register.set(chdrpf.getReg());/* IJTI-1386 */
		sv.numpols.set(chdrpf.getPolinc());
		payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(wsspcomn.company.toString(),sv.chdrnum.toString().trim());
		if (payrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set("2".concat(chdrpf.getChdrnum()));/* IJTI-1386 */
			fatalError600();
		}
		
		wsaaTranno.set(chdrpf.getTranno().intValue() + 1);
		wsaaRevTranno.set(chdrpf.getTranno().intValue());

		
		//ILIFE-7968 end
	}

protected void retrvCoverage1040()
	{
		//ILB-456 
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}else {
		covrpfDAO.setCacheObject(covrpf);
	}
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		sv.planSuffix.set(covrpf.getPlanSuffix());
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);

		//fwang3
		List<Itempf> itemList = this.itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), tr386, wsaaTr386Key.toString());
		if (itemList.isEmpty()) {
			syserrrec.params.set(tr386);
			fatalError600();
		}

		tr386rec.tr386Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		if (isEQ(sv.planSuffix,ZERO)) {
			wsaaPlanPolicyFlag.set("L");
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			sv.entity.set(tr386rec.progdesc01);
			sv.entityOut[varcom.hi.toInt()].set("Y");
		}
		else {
			wsaaPlanPolicyFlag.set("O");
			sv.plnsfxOut[varcom.nd.toInt()].set(SPACES);
			sv.entity.set(tr386rec.progdesc02);
			sv.entityOut[varcom.hi.toInt()].set(SPACES);
		}
		//ILIFE-7968
		if (isLTE(covrpf.getPlanSuffix(),chdrpf.getPolsum())
		&& isNE(covrpf.getPlanSuffix(),ZERO)
		&& isNE(chdrpf.getPolsum(),1)) {
			wsaaPlanSuffix.set(ZERO);
		}
		//ILB-456
		//covrpfDAO.deleteCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		if (isEQ(wsaaBatckey.batcBatctrcde,"T551")) {
			sv.textfield.set(tr386rec.progdesc03);
			sv.textfieldOut[varcom.bl.toInt()].set("Y");
			sv.textfieldOut[varcom.nd.toInt()].set("N");
		}
		else {
			if (isEQ(wsaaBatckey.batcBatctrcde,"T676")) {
				sv.textfield.set(tr386rec.progdesc04);
				sv.textfieldOut[varcom.bl.toInt()].set("Y");
				sv.textfieldOut[varcom.nd.toInt()].set("N");
			}
			else {
				sv.textfield.set(SPACES);
				sv.textfieldOut[varcom.bl.toInt()].set("N");
				sv.textfieldOut[varcom.nd.toInt()].set("Y");
			}
		}
	}

protected void readLifeDetails1050()
	{
		// ilife-3494 starts
		Lifepf lifeData = new Lifepf();		
		LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);		
		lifeData.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
		lifeData.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		lifeData.setLife("01");
		//lifeData.setJlife("00");
		lifeData.setValidflag("1");
		
		lifepfList = lifepfDAO.getLifeDetails(lifeData);//ILIFE-8108
		if(lifepfList.size() == 0)
		{
			syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum()).concat("01"));//ILIFE-7968 /* IJTI-1386 */
			fatalError600();
		}
		
		for(lifepfCount=0;lifepfCount<lifepfList.size();lifepfCount++)
		{   
			if(isEQ(lifepfList.get(lifepfCount).getJlife(), "00"))
			{
			  lifepfIO=lifepfList.get(lifepfCount);
				sv.lifenum.set(lifepfIO.getLifcnum());
				clts = new Clntpf();
				clts.setClntcoy(wsspcomn.fsuco.toString());
				clts.setClntpfx("CN");
				clts.setClntnum(lifepfIO.getLifcnum());/* IJTI-1386 */
				clts = clntpfDAO.getCltsRecordByKey(clts);
				if (clts == null) {
					syserrrec.params.set(wsspcomn.fsuco.toString().concat("CN").concat(lifepfIO.getLifcnum()));/* IJTI-1386 */
					fatalError600();
				}
				plainname();
				sv.lifename.set(wsspcomn.longconfname);
				break;
			}
		}
	}
//ilife-3494 ends

protected void jointLifeDetails1060()
	{

	// ilife-3494 starts
	
	if(lifepfList.size() >0)
	{  for(lifepfCount=0;lifepfCount<lifepfList.size();lifepfCount++)
		
	{
		if(isEQ(lifepfList.get(lifepfCount).getJlife(), "01"))
		{
		lifepfIO=lifepfList.get(lifepfCount);
		clts = new Clntpf();
		clts.setClntnum(lifepfIO.getLifcnum());
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts.setClntpfx("CN");
		clts = clntpfDAO.getCltsRecordByKey(clts);
		if (clts == null) {
			syserrrec.params.set("CN".concat(wsspcomn.fsuco.toString()).concat(lifepfIO.getLifcnum()));
			fatalError600();
		}
		else {
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
		}
		
		break;	
			}
	}
	}
	}
// ilife-3494 ends
protected void contractTypeStatus1070()
	{// TODO merge into one sql
		
		descpf = descDAO.getdescData("IT", t5688, chdrpf.getCnttype(), wsspcomn.company.toString(), wsspcomn.language.toString());//ILIFE-7968 /* IJTI-1386 */
		if(descpf == null) {
			sv.ctypedes.fill("?");
		}else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		descpf = descDAO.getdescData("IT", t3623, chdrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());//ILIFE-7968 /* IJTI-1386 */
		if(descpf == null) {
			sv.chdrstatus.fill("?");
		}else {
			sv.chdrstatus.set(descpf.getShortdesc());
		}
		descpf = descDAO.getdescData("IT", t3588, chdrpf.getPstcde(), wsspcomn.company.toString(), wsspcomn.language.toString());//ILIFE-7968 /* IJTI-1386 */
		if(descpf == null) {
			sv.premstatus.fill("?");
		}else {
			sv.premstatus.set(descpf.getShortdesc());
		}
	}

protected void subfileLoad1080()
	{
	
	wsaaLifeFound.set("N");
	/* MOVE  1                     TO WSAA-LIFE-NUM.*/
	wsaaLifeExit.set("N");
	for(lifepfCount1=0;lifepfCount1<lifepfList.size();lifepfCount1++)
	{   
		if(isEQ(lifepfList.get(lifepfCount1).getJlife(), "00"))
		{
		lifepfIO=lifepfList.get(lifepfCount1);
		processLife1100();
	    } 
	}
	
	scrnparams.subfileRrn.set(1);

	
	}

protected void processLife1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					mainLife1110();
					screenFields1120();
					subfileAdd1130();
					jlifeCheck1140();
				}
				case listCoverages1150: {
					listCoverages1150();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void mainLife1110()
	{
	//ILIFE-7968
	if (isNE(wsspcomn.company, lifepfIO.getChdrcoy())
			|| isNE(chdrpf.getChdrnum(), lifepfIO.getChdrnum())
			||lifepfCount1>lifepfList.size()) {
				wsaaLifeExit.set("Y");
				goTo(GotoLabel.exit1190);
			}

	}

protected void screenFields1120()
	{
		sv.subfileFields.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.selectOut[varcom.nd.toInt()].set("Y");
		sv.cmpntnum.set(lifepfIO.getLife());
		wsaaLifeNo.set(lifepfIO.getLife());
		sv.hlifeno.set(lifepfIO.getLife());
		sv.component.set(lifepfIO.getLifcnum());
		sv.hlifcnum.set(lifepfIO.getLifcnum());
		clts.setClntnum(lifepfIO.getLifcnum());
		sv.hsuffix.set(ZERO);
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts.setClntpfx("CN");
		clts = clntpfDAO.getCltsRecordByKey(clts);
		if (clts == null) {
			syserrrec.params.set(wsspcomn.fsuco.toString().concat("CN").concat(lifepfIO.getLifcnum()));
			fatalError600();
		}
		plainname();
		sv.deit.set(wsspcomn.longconfname);
	}

protected void subfileAdd1130()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S6351", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void jlifeCheck1140()
	{
		
	// ilife-3494 starts
	
	  lifepfCount++;
	//ILIFE-7968
	  if (isNE(lifepfIO.getChdrcoy(), chdrpf.getChdrcoy())
  			|| isNE(lifepfIO.getChdrnum(), chdrpf.getChdrnum())
  			||lifepfCount1>lifepfList.size() ) {
  				wsaaLifeExit.set("Y");
  			}
  
	
	if (isNE(lifepfIO.getLife(),wsaaLifeNo)) {
		goTo(GotoLabel.listCoverages1150);
	}
	
	if (isNE(lifepfIO.getJlife(), ZERO)) {
		jlifeToScreen1200();
	}
	 lifepfCount++;
}
// ilife-3494 ends

protected void listCoverages1150()
	{
		//ILB-456
		covrpf.setChdrcoy(wsspcomn.company.toString());
		covrpf.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		covrpf.setLife(wsaaLifeNo.toString());
		covrpf.setCoverage("00");
		covrpf.setRider("00");
		covrpf.setPlanSuffix(0);
		covrpf.setValidflag("1");//ILIFE-7814
		rpuMetNotFund = false;	
		wsaaCoverExit.set("N");
		/*while ( !(wsaaCoverEof.isTrue())) {
			listCoverages1300();
		}*/
		covrpfList=covrpfDAO.selectCoverage(covrpf);  //ILIFE-8285
		if (!covrpfList.isEmpty()) {
			listCoverages1300();
		}
		
		if(rpuConfig){
			if(rpuMetNotFund==true){
				scrnparams.subfileErrors.set(rrh7);
				wsspcomn.edterror.set("Y");	
			}
		}
		//fwang3
//		List<Covrpf> covrList = this.covrmjaDAO.getCovrlnbPF(wsspcomn.company.toString(), chdrmjaIO.getChdrnum().toString(), wsaaLifeNo.toString(), "00", "00", 0);
//		for(Covrpf pf: covrList){
//			listCoverages1300();
//		}
		lifepfCount++;
	}

protected void jlifeToScreen1200()
	{
		jlifeToSubfile1210();
		subfileAdd1220();
	}

protected void jlifeToSubfile1210()
	{
		sv.cmpntnum.set(" +");
		sv.component.set(lifepfIO.getLifcnum());
		clts = new Clntpf();
		clts.setClntnum(lifepfIO.getLifcnum());
		sv.hsuffix.set(ZERO);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		clts.setClntpfx("CN");
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts = clntpfDAO.getCltsRecordByKey(clts);
		if (clts == null ) {
			syserrrec.params.set(syserrrec.params.set(wsspcomn.fsuco.concat("CN").concat(lifepfIO.getLifcnum())));
			fatalError600();
		}
		plainname();
		sv.deit.set(wsspcomn.longconfname);
	}

protected void subfileAdd1220()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S6351", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void listCoverages1300()
	{
		try {
			readFirstCoverage1310();
		}
		catch (GOTOException e){
		}
	}

protected void readFirstCoverage1310() 
	{
		//ILB-456
	if(covrpfList != null && covrpfList.size() > 0)
	{
		for(Covrpf covr : covrpfList)
		{
			if (covr == null) {
				fatalError600();
			}
			if ( (covr == null)
					|| isNE(covr.getChdrcoy(),wsspcomn.company)
					|| isNE(covr.getChdrnum(),chdrpf.getChdrnum())
					|| isNE(covr.getLife(),wsaaLifeNo)) {
						wsaaCoverExit.set("Y");
						goTo(GotoLabel.exit1390);
					}
			if (isEQ(wsaaPlanSuffix,0)
					|| isLTE(wsaaPlanSuffix,chdrpf.getPolsum())) {
						if (isNE(covr.getPlanSuffix(),0)) {
							//covrpfCount++;
							goTo(GotoLabel.exit1390);
						}
					}
					if (isNE(covr.getPlanSuffix(),wsaaPlanSuffix)) {
						//covrpfCount++;
						goTo(GotoLabel.exit1390);
					}
			
		
		
		/*SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		//ILIFE-7968
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(covrmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covrmjaIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(covrmjaIO.getLife(),wsaaLifeNo)) {
			wsaaCoverExit.set("Y");
			goTo(GotoLabel.exit1390);
		}
		if (isEQ(wsaaPlanSuffix,0)
		|| isLTE(wsaaPlanSuffix,chdrpf.getPolsum())) {
			if (isNE(covrpf.getPlanSuffix(),0)) {
				covrmjaIO.setFunction(varcom.nextr); //fwang3
				goTo(GotoLabel.exit1390);
			}
		}
		//ILIFE-7968
		if (isNE(covrpf.getPlanSuffix(),wsaaPlanSuffix)) {
			covrmjaIO.setFunction(varcom.nextr); //fwang3
			goTo(GotoLabel.exit1390);
		}*/
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.selectOut[varcom.nd.toInt()].set(SPACES);
		if (isEQ(covr.getRider(),ZERO)) {
			coverageToScreen1400(covr);
		}
		else {
			riderToScreen1500(covr);
		}
		
		scrnparams.function.set(varcom.sadd);
		processScreen("S6351", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		}
	}
	}

protected void coverageToScreen1400(Covrpf covrpf)
	{
		/*COVRMJA-SUBFILE*/
		sv.cmpntnum.set(covrpf.getCoverage());
		wsaaCoverage.set(covrpf.getCrtable());
		sv.hcrtable.set(covrpf.getCrtable());
		sv.component.set(wsaaCovrComponent);
		getDescription1600(covrpf);
		covrStatusDescs1700(covrpf);
		validateComponent1600(covrpf);
		sv.hlifeno.set(covrpf.getLife());
		sv.hsuffix.set(covrpf.getPlanSuffix());
		sv.hcoverage.set(covrpf.getCoverage());
		sv.hrider.set(covrpf.getRider());
		
		if(rpuConfig){
			if(isNE(sv.component.toString(),sv.lifenum) 
					&& (isEQ( sv.statcode.toString().trim(),inForce) && isEQ(sv.pstatcode.toString().trim(),prmPay))){
				readT5687();							
			}			
		}
		/*EXIT*/
	}

protected void riderToScreen1500(Covrpf covrpf)
	{
		/*RIDER-SUBFILE*/
		sv.cmpntnum.set(covrpf.getRider());
		wsaaRider.set(covrpf.getCrtable());
		sv.hcrtable.set(covrpf.getCrtable());
		sv.component.set(wsaaRidrComponent);
		getDescription1600(covrpf);
		covrStatusDescs1700(covrpf);
		validateComponent1600(covrpf);
		sv.hlifeno.set(covrpf.getLife());
		sv.hsuffix.set(covrpf.getPlanSuffix());
		sv.hcoverage.set(covrpf.getCoverage());
		sv.hrider.set(covrpf.getRider());
		
		if(rpuConfig){
			if(isNE(sv.component.toString(),sv.lifenum) && isEQ(rpuMetNotFund,false) 
					&& (isEQ( sv.statcode.toString().trim(),inForce) && isEQ(sv.pstatcode.toString().trim(),prmPay))){
				readT5687();							
			}			
		}
		if(compSurrPosPermission  && isEQ(wsspcomn.flag,"S")){
			findFromCovt();
			if (isEQ(indicFound,true)) {
				sv.asterisk.set("*");
			}else{
				sv.asterisk.set(SPACES);
			}
			
			if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "C")){
				if(isEQ(covrpf.getStatcode().trim(),tmStatus) && isNE(covrpf.getPstatcode().trim(),tmStatus)){
					sv.select.set(1);
				}else{
					sv.select.set(SPACES);
				}
			}
			
		}
		/*EXIT*/
	}
protected void findFromCovt(){
	indicFound =false;
	cpsupf = null;
	 cpsupf = cpsupfDAO.getCpsupfItem(sv.chdrnum.toString().trim(),sv.hcrtable.toString().trim());
	if(cpsupf.getCrtable()!=null){
		if (isEQ(cpsupf.getStatus().trim(),"R")) {
			indicFound =true;
		}
	}
}

protected void validateComponent1600(Covrpf covrpf)
	{
		validateComponentPara1600(covrpf);
	}

protected void validateComponentPara1600(Covrpf covrpf)
	{// TODO
//		itemIO.setParams(SPACES);
//		itemIO.setItempfx("IT");
//		itemIO.setItemcoy(wsspcomn.company);
//		itemIO.setItemtabl(t5679);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsaaBatckey.batcBatctrcde.toString());
//		itemIO.getItemitem().setLeft(stringVariable1.toString());
//		itemIO.setFormat(itemrec);
//		itemIO.setFunction(varcom.readr); 
//		SmartFileCode.execute(appVars, itemIO);
//		if (isNE(itemIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(itemIO.getParams());
//			fatalError600();
//		}
		//fwang3
		FixedLengthStringData itemitem = new FixedLengthStringData(8);
		itemitem.setLeft(stringVariable1.toString());
		List<Itempf> itemlist = this.itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), t5679, itemitem.toString());
		if(itemlist.isEmpty()) {
			syserrrec.params.set(t5679);
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itemlist.get(0).getGenarea()));
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
			headerStatuzCheck1800(covrpf);
		}
		if (isNE(wsaaValidStatus,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void getDescription1600(Covrpf covrpf)
	{
		descpf = descDAO.getdescData("IT", t5687, covrpf.getCrtable(), wsspcomn.company.toString(), wsspcomn.language.toString());/* IJTI-1386 */
		if(descpf == null) {
			sv.deit.fill("?");
		}else {
			sv.deit.set(descpf.getLongdesc());
		}
	}

protected void covrStatusDescs1700(Covrpf covrpf)
	{
	
		descpf = descDAO.getdescData("IT", t5681, covrpf.getPstatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());/* IJTI-1386 */
		if(descpf == null) {
			sv.pstatcode.fill("?");
		}else {
			sv.pstatcode.set(descpf.getShortdesc());
		}
		
		descpf = descDAO.getdescData("IT", t5682, covrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());/* IJTI-1386 */
		if(descpf == null) {
			sv.statcode.fill("?");
		}else {
			sv.statcode.set(descpf.getShortdesc());
		}
		
	}

protected void headerStatuzCheck1800(Covrpf covrpf)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					headerStatuzCheck1810(covrpf);
				}
				case covPrem1850: {
					covPrem1850(covrpf);
				}
				case ridPrem1860: {
					ridPrem1860(covrpf);
				}
				case valid1880: {
					valid1880();
				}
				case exit1890: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void headerStatuzCheck1810(Covrpf covrpf)
	{
		wsaaPrsub.set(ZERO);
		if (isEQ(covrpf.getRider(),ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrpf.getStatcode()))) {
					goTo(GotoLabel.covPrem1850);
				}
			}
		}
		if (isGT(covrpf.getRider(),ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrpf.getStatcode()))) {
					goTo(GotoLabel.ridPrem1860);
				}
			}
		}
		goTo(GotoLabel.exit1890);
	}

protected void covPrem1850(Covrpf covrpf)
	{
		wsaaPrsub.add(1);
		if (isEQ(t5679rec.covPremStat[wsaaPrsub.toInt()],covrpf.getPstatcode())) {
			goTo(GotoLabel.valid1880);
		}
		if (isLT(wsaaPrsub,12)) {
			goTo(GotoLabel.covPrem1850);
		}
		else {
			goTo(GotoLabel.exit1890);
		}
	}

protected void ridPrem1860(Covrpf covrpf)
	{
		wsaaPrsub.add(1);
		if (isEQ(t5679rec.ridPremStat[wsaaPrsub.toInt()],covrpf.getPstatcode())) {
			goTo(GotoLabel.valid1880);
		}
		if (isLT(wsaaPrsub,12)) {
			goTo(GotoLabel.ridPrem1860);
		}
		else {
			goTo(GotoLabel.exit1890);
		}
	}

protected void valid1880()
	{
		wsaaValidStatus = "Y";
	}


protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*SCREEN-IO*/
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
 			validateSubfile2030();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		
		if (isEQ(scrnparams.statuz,"SUBM ")) {
			wsspcomn.flag.set("K");
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.rolu)) {			
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");			
		}
		scrnparams.subfileRrn.set(1);
	}

protected void validateSubfile2030()
	{	
		subfileStart();	
		if(isNE(wsaaSelectFlagLife,"Y")){
			wsaaSelectedlife.set(ZERO);
		}
		subFileCount=0;
 		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
 			
			validateSubfile2600();
			if(rpuMetNotFund) break;
			scrnparams.function.set(varcom.supd);
			subfileIo4400();
			scrnparams.function.set(varcom.srdn);
			subfileIo4400();
		} 		
 		if(!rpuMetNotFund){
			if (isEQ(wsaaSelected,0) && isEQ(rpuConfig,false)) {
				scrnparams.errorCode.set(g931);
				wsspcomn.edterror.set("Y");
			}			
			else if(isEQ(rpuConfig,true) && isLT(wsaaSelectedlife,subFileCount)){
				scrnparams.errorCode.set(rrh8);
				wsspcomn.edterror.set("Y");
			}
		
 		}else{ 			
 			scrnparams.subfileErrors.set(rrh7);
			wsspcomn.edterror.set("Y");	 			
 		}
		
	}
protected void subfileStart()	
{
	/*VALIDATE-SUBFILE*/
	scrnparams.function.set(varcom.sstrt);
	processScreen("S6351", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}


protected void subfileIo4400() {

	processScreen("S6351", sv);
	if (isNE(scrnparams.statuz, varcom.oK) && isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}
protected void validateSubfile2600()
	{
		try {
 			readNextRecord2610();
 			updateErrorIndicators2630();
			
		}
		catch (GOTOException e){
		}
	}

protected void readNextRecord2610()
	{
		
		/*VALIDATION*/				
		
		if(rpuConfig){
			if (isNE(sv.select,SPACES) 
					&& (isEQ( sv.statcode.toString().trim(),inForce) && isEQ(sv.pstatcode.toString().trim(),prmPay))) {
				wsaaSelectedlife.add(1);
			}
			if(isNE(sv.component.toString(),sv.lifenum)
					&& (isEQ( sv.statcode.toString().trim(),inForce) && isEQ(sv.pstatcode.toString().trim(),prmPay))){
				subFileCount=subFileCount+1;
			}			
		}else{
			if (isNE(sv.select,SPACES)) {
				wsaaSelected.add(1);
			}
		}
		
	}

protected void updateErrorIndicators2630()
	{
	
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2690);
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6351", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readT5687(){
	
	itempf = new Itempf();

	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("T5687");
	itempf.setItemitem(sv.component.toString());
	itempf.setItmfrm(wsaaToday.getbigdata());
	itempf.setItemseq("  ");
	itempf = itemDAO.getItemRecordByItemkey(itempf);

	if (itempf == null) {
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
		fatalError600();
	}
	t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
	if (isEQ(t5687rec.pumeth,SPACES)) {
		rpuMetNotFund = true;
		}
	
}



@Override
protected void update3000()
	{
//		try {// TODO
			updateDatabase3010();
//		}
//		catch (GOTOException e){
//		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
//			goTo(GotoLabel.exit3090);
			return;
		}
		

		IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAOP6351",IncrpfDAO.class);
		List<Incrpf> incrpfs = incrpfDAO.getIncrpfByPolicyNum(chdrpf.getChdrnum().trim(), wsspcomn);//ILIFE-7968 /* IJTI-1386 */
		if (incrpfs.isEmpty()) {
			wsaaEof.set("Y");
//			goTo(GotoLabel.exit3090);
			return;
		}
		if (isEQ(wsaaFirstTime,"Y")) {
			sv.chdrnumErr.set(j008);
			wsspcomn.edterror.set("Y");
			wsaaFirstTime = "N";
//			goTo(GotoLabel.exit3090);
			return;
		}

		List<Long> deleteRecord = new ArrayList<>();
		/*if (incrpfs.isEmpty()) {
			
			goTo(GotoLabel.exit3190);
		}*/
		//ILIFE-7968
		for (Incrpf incrpf : incrpfs) {
			if (wsspcomn.company.toString().trim().equals(incrpf.getChdrcoy().trim())
					&& chdrpf.getChdrnum().trim().equals(incrpf.getChdrnum().trim())) {/* IJTI-1386 */
				deleteRecord.add(incrpf.getUniqueNumber());
			}

		}
		incrpfDAO.deleteRecordByUniqueNumber(deleteRecord);
		 //DbDialectSupportObjFactory.getInstance().getSmartFileCodeSupport().updateSessionContextInfo(conn, userId);
		/*
		while ( !(endOfFile.isTrue())) {
			updateIncr3100();
		}
		*/
		//ILB-456
		//CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAOP6351",CovrpfDAO.class);
		//covrpfDAO.updateIndexationInd(chdrpf.getChdrnum().toString().trim(), SPACES.toString(), wsspcomn.company.toString().trim());
		covrpfDAO.updateCovrIndexationind(covrpf);
	
	}

protected void updateIncr3100()
	{
		try {
			nextIncr3120();
		}
		catch (GOTOException e){
		}
	}

protected void nextIncr3120()
	{
//		SmartFileCode.execute(appVars, incrmjaIO);
//		if (isNE(incrmjaIO.getStatuz(),varcom.oK)
//		&& isNE(incrmjaIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(incrmjaIO.getParams());
//			fatalError600();
//		}
//		if (isEQ(incrmjaIO.getStatuz(),varcom.endp)) {
//			wsaaEof.set("Y");
//			goTo(GotoLabel.exit3190);
//		}
//		if (isNE(incrmjaIO.getChdrcoy(),wsspcomn.company)
//		|| isNE(incrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())) {
//			wsaaEof.set("Y");
//			incrmjaIO.setFunction(varcom.rewrt);
//		}
//		else {
//			incrmjaIO.setFunction(varcom.delet);
//		}
//		SmartFileCode.execute(appVars, incrmjaIO);
//		if (isNE(incrmjaIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(incrmjaIO.getParams());
//			fatalError600();
//		}
//		incrmjaIO.setFunction(varcom.nextr);
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case continue4080: {
					continue4080();
				}
				case updateScreen4085: {
					updateScreen4085();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			releaseSftlck4400();
			wsaaSub.set(1);
			wsspcomn.secActn[wsaaSub.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			initSelection4100();
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		wsaaSelectFlag.set("N");
		wsaaImmexitFlag.set("N");
		while ( !(wsaaSelection.isTrue()
		|| wsaaImmExit.isTrue())) {
			next4200();
		}
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			if (wsaaImmExit.isTrue()) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wsspcomn.nextprog.set(scrnparams.scrname);
				goTo(GotoLabel.exit4090);
			}
		}
		if (wsaaImmExit.isTrue()) {
			//ILIFE-1074 vchawda Starts 
			//ILIFE-1243 vchawda
			if(isEQ(wsspcomn.sbmaction,'A') && 
			   isEQ(wsaaBatckey.batcBatctrcde,"T551")){
				letrqstrec.statuz.set(SPACES);
				letrqstrec.requestCompany.set(wsspcomn.company);
				letrqstrec.letterType.set(wsaaPremiumLetter);
				letrqstrec.letterRequestDate.set(wsaaToday);
	 			letrqstrec.clntcoy.set(clts.getClntcoy());
				letrqstrec.clntnum.set(clts.getClntnum());
				letrqstrec.servunit.set(chdrpf.getServunit());//ILIFE-7968 /* IJTI-1386 */
				letrqstrec.chdrcoy.set(chdrpf.getChdrcoy().toString());//ILIFE-7968
				letrqstrec.branch.set(wsspcomn.branch);
				letrqstrec.rdocpfx.set(chdrpf.getChdrpfx());//ILIFE-7968 /* IJTI-1386 */
			 	letrqstrec.rdoccoy.set(chdrpf.getChdrcoy().toString());//ILIFE-7968
				letrqstrec.rdocnum.set(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
				letrqstrec.chdrnum.set(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
				letrqstrec.tranno.set(add(chdrpf.getTranno(),1));
				letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
				letrqstrec.function.set("ADD");
				callProgram(Letrqst.class, letrqstrec.params);
				if (isNE(letrqstrec.statuz, varcom.oK)) {
					syserrrec.statuz.set(letrqstrec.statuz);
					fatalError600();
				}
			}
			//ILIFE-1074 vchawda Ends
			//POS41 Start
			if(compSurrPosPermission &&  isEQ(wsspcomn.flag,"S") ){
				
				if (isEQ(wsspcomn.sbmaction, "A") || isEQ(wsspcomn.sbmaction, "B")) {
					
					if (isEQ(wsspcomn.sbmaction, "A")) {
							updateinsertChdr();
					}else if(isEQ(wsspcomn.sbmaction, "B")){
						updatePtrnRcd();
						updateDeleteChdrpf();
					}
					writePtrn();
					releaseSftlck4400();
				}
			}
			
			//POS41 End
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(1);
			wsspcomn.nextprog.set(wsaaProg);
			goTo(GotoLabel.exit4090);
		}
		if (isNE(wsaaBatckey.batcBatctrcde,wsaaLapse)
		&& isNE(wsaaBatckey.batcBatctrcde,wsaaPup)) {
			goTo(GotoLabel.continue4080);
		}
		if (isEQ(sv.hlifeno,wsaaLastLifeno)
		&& isEQ(sv.hsuffix,wsaaLastSuffix)
		&& isEQ(sv.hcoverage,wsaaLastCoverage)
		&& isEQ(wsaaLastRider,ZERO)) {
			if(firstflag) {//ILIFE-8697
				firstflag = false;
			}else {
			sv.asterisk.set(SPACES);
			goTo(GotoLabel.updateScreen4085);
			}
		}
	}
		
		
protected void writePtrn() {
	ptrnpf = new Ptrnpf();
	ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
	ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
	ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
	ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
	ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
	ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
	ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	ptrnpf.setTranno(wsaaTranno.toInt());
	ptrnpf.setPtrneff(wsspcomn.currfrom.toInt());
	ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	ptrnpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
	ptrnpf.setChdrpfx(chdrpf.getChdrpfx());/* IJTI-1386 */
	ptrnpf.setValidflag("1");
	ptrnpf.setTrdt(wsaaToday.toInt());
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setUserT(varcom.vrcmUser.toInt());
	ptrnpf.setTermid(varcom.vrcmTermid.toString());
	ptrnpf.setDatesub(wsaaToday.toInt());

	ptrnpfList = new ArrayList<Ptrnpf>();
	ptrnpfList.add(ptrnpf);	
	boolean result;
	if(ptrnpfList != null && ptrnpfList.size() > 0){
		result = ptrnpfDAO.insertPtrnPF(ptrnpfList);
		if (!result) {
			syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum()));/* IJTI-1386 */
			fatalError600();
		} else ptrnpfList.clear();
	}
}

protected void updatePtrnRcd(){
	
	ptrnpfDAO.updateValidPtrnrevRecord(chdrpf.getChdrcoy().toString(), sv.chdrnum.toString(),revtranscode);
}


protected void updateinsertChdr(){	
	
	chdrlnbIO.setFunction("RETRV"); 
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		fatalError600();
	}
	chdrlnbIO.setFunction("RLSE"); 
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		fatalError600();
	}
	
	chdrlnbIO.setValidflag("2");
	initialize(datcon2rec.datcon2Rec);
	datcon2rec.freqFactor.set(-1);
	datcon2rec.frequency.set("DY");
	datcon2rec.intDate1.set(wsspcomn.currfrom);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);	
	chdrlnbIO.setCurrto(datcon2rec.intDate2.toInt());
	chdrlnbIO.setFunction(varcom.rewrt);	
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		fatalError600();
	}
	
	setPrecision(chdrpf.getTranno(), 0);
	chdrlnbIO.setTranno(add(chdrpf.getTranno(), 1));
	chdrlnbIO.setValidflag("1");
	chdrlnbIO.setStatcode(t5679rec.setCnRiskStat);
	chdrlnbIO.setCurrto(varcom.vrcmMaxDate);
	chdrlnbIO.setCurrfrom(wsspcomn.currfrom);	
	chdrlnbIO.setFunction(varcom.writr);	
	
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		fatalError600();
	}	
}


protected void updateDeleteChdrpf(){	
	
	chdrpfDAO.deleteRcdByFlag("1",sv.chdrnum.toString());
	
	chdrpf.setChdrnum(sv.chdrnum.toString());
	chdrpf.setValidflag('1');
	chdrpf.setTranno(wsaaTranno.toInt());
	chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	chdrpfDAO.updateChdrUniqueno(chdrpf);
	}

protected void continue4080()
	{
		coverRiderStore4300();
		programTables4400();
		wsaaLastLifeno.set(sv.hlifeno);
		wsaaLastSuffix.set(sv.hsuffix);
		wsaaLastCoverage.set(sv.hcoverage);
		wsaaLastRider.set(sv.hrider);
	}

protected void updateScreen4085()
	{
		screenUpdate4600();
	}

protected void initSelection4100()
	{
		try {
			saveNextProgs4110();
		}
		catch (GOTOException e){
		}
	}

protected void saveNextProgs4110()
	{
		compute(wsaaSub1, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
			loop14120();
		}
		scrnparams.function.set(varcom.sstrt);
		goTo(GotoLabel.exit4190);
	}

protected void loop14120()
	{
		wsaaSecProg[wsaaSub2.toInt()].set(wsspcomn.secProg[wsaaSub1.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
	}

protected void next4200()
	{
		try {
			nextRec4210();
			ifSubfileEndp4220();
		}
		catch (GOTOException e){
		}
	}

protected void nextRec4210()
	{
		processScreen("S6351", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4220()
	{
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			reloadProgsWssp4230();
			goTo(GotoLabel.exit4290);
		}
		if (isNE(sv.select,SPACES)) {
			sv.asterisk.set("*");
			sv.select.set(SPACES);
			wsaaSelectFlag.set("Y");
			wsaaSelectFlagLife.set("Y");
		}
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.exit4290);
	}

protected void reloadProgsWssp4230()
	{
		wsaaImmexitFlag.set("Y");
		compute(wsaaSub1, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loop34240();
		}
		goTo(GotoLabel.exit4290);
	}

protected void loop34240()
	{
		wsspcomn.secProg[wsaaSub1.toInt()].set(wsaaSecProg[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
	}

protected void coverRiderStore4300()
	{
		coverRiderRead4300();
	}

protected void coverRiderRead4300()
	{
		//ILB-456
		covrpf.setChdrcoy(wsspcomn.company.toString());
		covrpf.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968
		covrpf.setLife(sv.hlifeno.toString());
		covrpf.setPlanSuffix(sv.hsuffix.toInt());
		covrpf.setCoverage(sv.hcoverage.toString());
		covrpf.setRider(sv.hrider.toString());
		wsaaCrtable.set(sv.hcrtable);
		covrpfList=covrpfDAO.getcovrtrbRecord(wsspcomn.company.toString(),chdrpf.getChdrnum(), sv.hlifeno.toString(), sv.hcoverage.toString(), sv.hrider.toString());
		if (covrpfList == null) {
			fatalError600();
		}
		if(!covrpfList.isEmpty())
		{
			for(Covrpf covr : covrpfList)
			{
				covr.setPlanSuffix(sv.planSuffix.toInt());
				covrpfDAO.setCacheObject(covr);
			}
		}
	}

protected void releaseSftlck4400()
	{
		try {
			unlockContract4410();
		}
		catch (GOTOException e){
		}
	}

protected void unlockContract4410()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit4400);
		}
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(sv.chdrnum.toString());//ILIFE-7968
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void programTables4400()
	{
	//ILIFE-7805
	if(null != covrpf.getSingpremtype() && isEQ(covrpf.getSingpremtype(),"ROP")){
			scrnparams.errorCode.set(rrmw);
			wsspcomn.edterror.set("Y");
			return;
	}
	// readProgramTable4410
	
	t5671ListMap = itemDAO.loadSmartTable("IT",wsspcomn.company.toString(),t5671);	
	String keyItemitem = wsaaBatckey.batcBatctrcde.toString().concat(wsaaCrtable.toString());
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5671ListMap.containsKey(keyItemitem)){	
		itempfList = t5671ListMap.get(keyItemitem);
		Itempf itempf = itempfList.get(0);
		t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		itemFound = true;			
	}
	if (!itemFound) {
		wsaaCrtable.set("****");
		keyItemitem = wsaaBatckey.batcBatctrcde.toString().concat(wsaaCrtable.toString());
		itempfList = new ArrayList<Itempf>();
		boolean itemFound2 = false;
		if (t5671ListMap.containsKey(keyItemitem)){	
			itempfList = t5671ListMap.get(keyItemitem);
			Itempf itempf = itempfList.get(0);
			t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			itemFound2 = true;					
		}		
		if (!itemFound2) {
			sv.selectErr.set(g433);
			wsspcomn.edterror.set("Y");
			wsspcomn.nextprog.set(scrnparams.scrname);
			planReload4500();
			wsaaSelected.set(ZERO);
			//goTo(GotoLabel.exit4490);
		}
	}
	
	if (isEQ(wsspcomn.flag,"I")
		|| isEQ(chdrpf.getBillfreq(),"00")) {
		//goTo(GotoLabel.loadProgsToWssp4420);
	} else {
		RacdpfDAO racdpfDAO = this.getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
		List<Racdpf> racdpfs = racdpfDAO.searchRacdResult(chdrpf.getChdrcoy().toString().trim(), chdrpf.getChdrnum().trim(), /* IJTI-1386 */
				sv.hlifeno.toString().trim(), sv.hcoverage.toString().trim(), sv.hrider.toString().trim(), 
				sv.hsuffix.toString().trim());//ILIFE-7968
		wsaaInvalidCtdate.set("N");
		for (Racdpf racdpf : racdpfs) {
			if (isLT(PackedDecimalData.parseLong(racdpf.getCtdate()),chdrpf.getPtdate())) {
				wsaaInvalidCtdate.set("Y");
				break;
			}
		}
		
		if (invalidCtdate.isTrue()) {
			sv.selectErr.set(r078);
			wsspcomn.edterror.set("Y");
			wsspcomn.nextprog.set(scrnparams.scrname);
			planReload4500();
			wsaaSelected.set(ZERO);
			//goTo(GotoLabel.exit4490);
			return;
		}
	}
	// loadProgsToWssp4420
	wsaaSub1.set(1);
	compute(wsaaSub2, 0).set(add(1,wsspcomn.programPtr));
	for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
		loop24430();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
	wsspcomn.nextprog.set(wsaaProg);
	//goTo(GotoLabel.exit4490);
	
	/*
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case readProgramTable4410: {
					readProgramTable4410();
				}
				case loadProgsToWssp4420: {
					loadProgsToWssp4420();
				}
				case exit4490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
		*/
	}

protected void readProgramTable4410()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5671);
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString().concat(wsaaCrtable.toString()));
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			if (isEQ(wsaaCrtable,"****")) {
				sv.selectErr.set(g433);
				wsspcomn.edterror.set("Y");
				wsspcomn.nextprog.set(scrnparams.scrname);
				planReload4500();
				wsaaSelected.set(ZERO);
				goTo(GotoLabel.exit4490);
			}
			/*else {
				  wsaaCrtable.set("****");
				  goTo(GotoLabel.readProgramTable4410);
			}*/
		}
		else {
			t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(chdrpf.getBillfreq(),"00")) {
			goTo(GotoLabel.loadProgsToWssp4420);
		}
		
		RacdpfDAO racdpfDAO = this.getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
		List<Racdpf> racdpfs = racdpfDAO.searchRacdResult(chdrpf.getChdrcoy().toString().trim(), chdrpf.getChdrnum().trim(), /* IJTI-1386 */
				sv.hlifeno.toString().trim(), sv.hcoverage.toString().trim(), sv.hrider.toString().trim(), 
				sv.hsuffix.toString().trim());//ILIFE-7968
		wsaaInvalidCtdate.set("N");
		for (Racdpf racdpf : racdpfs) {
			if (isLT(PackedDecimalData.parseLong(racdpf.getCtdate()),chdrpf.getPtdate())) {
				wsaaInvalidCtdate.set("Y");
				break;
			}
		}
		
		
		if (invalidCtdate.isTrue()) {
			sv.selectErr.set(r078);
			wsspcomn.edterror.set("Y");
			wsspcomn.nextprog.set(scrnparams.scrname);
			planReload4500();
			wsaaSelected.set(ZERO);
			goTo(GotoLabel.exit4490);
		}
	}

protected void loadProgsToWssp4420()
	{
		wsaaSub1.set(1);
		compute(wsaaSub2, 0).set(add(1,wsspcomn.programPtr));
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
			loop24430();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		wsspcomn.nextprog.set(wsaaProg);
		goTo(GotoLabel.exit4490);
	}

protected void loop24430()
	{
		wsspcomn.secProg[wsaaSub2.toInt()].set(t5671rec.pgm[wsaaSub1.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
	}

protected void planReload4500()
	{
		try {
			reloadProgsWssp4510();
		}
		catch (GOTOException e){
		}
	}

protected void reloadProgsWssp4510()
	{
		compute(wsaaSub1, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
			loop34520();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		goTo(GotoLabel.exit4590);
	}

protected void loop34520()
	{
		wsspcomn.secProg[wsaaSub1.toInt()].set(wsaaSecProg[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
	}

protected void screenUpdate4600()
	{
		/*PARA*/
		scrnparams.function.set(varcom.supd);
		processScreen("S6351", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}


protected S6351ScreenVars getLScreenVars() 
	{
	return ScreenProgram.getScreenVars(S6351ScreenVars.class);
	}
}