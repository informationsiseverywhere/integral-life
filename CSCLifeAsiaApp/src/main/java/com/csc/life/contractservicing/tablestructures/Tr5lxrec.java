package com.csc.life.contractservicing.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Tr5lxrec extends ExternalData{
	public FixedLengthStringData tr5lxRec = new FixedLengthStringData(8);
	public FixedLengthStringData taxPeriodFrm = new FixedLengthStringData(4).isAPartOf(tr5lxRec, 0);
	public FixedLengthStringData taxPeriodTo = new FixedLengthStringData(4).isAPartOf(tr5lxRec, 4);

	@Override
	public void initialize() {
		COBOLFunctions.initialize(tr5lxRec);
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr5lxRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
}
