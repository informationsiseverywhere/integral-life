package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:21
 * Description:
 * Copybook name: T6757REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6757rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6757Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData jobnameJobs = new FixedLengthStringData(80).isAPartOf(t6757Rec, 0);
  	public FixedLengthStringData[] jobnameJob = FLSArrayPartOfStructure(8, 10, jobnameJobs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(80).isAPartOf(jobnameJobs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData jobnameJob01 = new FixedLengthStringData(10).isAPartOf(filler, 0);
  	public FixedLengthStringData jobnameJob02 = new FixedLengthStringData(10).isAPartOf(filler, 10);
  	public FixedLengthStringData jobnameJob03 = new FixedLengthStringData(10).isAPartOf(filler, 20);
  	public FixedLengthStringData jobnameJob04 = new FixedLengthStringData(10).isAPartOf(filler, 30);
  	public FixedLengthStringData jobnameJob05 = new FixedLengthStringData(10).isAPartOf(filler, 40);
  	public FixedLengthStringData jobnameJob06 = new FixedLengthStringData(10).isAPartOf(filler, 50);
  	public FixedLengthStringData jobnameJob07 = new FixedLengthStringData(10).isAPartOf(filler, 60);
  	public FixedLengthStringData jobnameJob08 = new FixedLengthStringData(10).isAPartOf(filler, 70);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(420).isAPartOf(t6757Rec, 80, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6757Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6757Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}