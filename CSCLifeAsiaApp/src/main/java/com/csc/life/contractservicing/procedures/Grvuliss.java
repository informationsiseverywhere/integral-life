/*
 * File: Grvuliss.java
 * Date: 29 August 2009 22:51:29
 * Author: Quipoz Limited
 * 
 * Class transformed from GRVULISS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.life.contractservicing.dataaccess.IncirevTableDAM;
import com.csc.life.contractservicing.dataaccess.UlnkrevTableDAM;
import com.csc.life.contractservicing.dataaccess.UnltrevTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrncfiTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Generalised subroutine for reversal of INCIs, ULNKs & UTRNs
* -----------------------------------------------------------
*
* Overview.
* ---------
*
* This subroutine is called via the generic processing table,
*  T5671, is is used when performing a 'Cancel from Inception'
*  or an 'Alter from Inception' on a Unit-linked contract. Its
*  purpose is to Delete all INCI records associated with the
*  contract being processed, to Reverse all ULNK records,
*  using the earliest unique ULNKs to produce corresponding
*  UNLTs, then deleting all the ULNKs and to reverse all
*  processed UTRNS and delete any unprocessed UTRNs,
*
* Processing.
* -----------
*
* This subroutine is called via an entry in T5671.
*  All parameters required by this subroutine are passed in
*   the GREVERSREC copybook.
* NOTE.
*    The records to be processed should be specified right down
*     to PLAN-SUFFIX level in the Linkage passed to this
*     subroutine.
*
*****************************************************************
* </pre>
*/
public class Grvuliss extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "GRVULISS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaDoneFirstUlnk = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaSeqnbr = new PackedDecimalData(3, 0).init(ZERO);
		/* FORMATS */
	private String chdrenqrec = "CHDRENQREC";
	private String incirevrec = "INCIREVREC";
	private String ulnkrevrec = "ULNKREVREC";
	private String unltrevrec = "UNLTREVREC";
	private String utrncfirec = "UTRNCFIREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Greversrec greversrec = new Greversrec();
		/*Individual Increase Details*/
	private IncirevTableDAM incirevIO = new IncirevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
		/*CFI/AFI/Reversals view*/
	private UlnkrevTableDAM ulnkrevIO = new UlnkrevTableDAM();
		/*CFI/AFI/Reversals view*/
	private UnltrevTableDAM unltrevIO = new UnltrevTableDAM();
		/*CFI/AFI view of UTRN file*/
	private UtrncfiTableDAM utrncfiIO = new UtrncfiTableDAM();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit3190, 
		exit4190, 
		exit9490, 
		exit9990
	}

	public Grvuliss() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		greversrec.statuz.set(varcom.oK);
		wsaaSub.set(ZERO);
		wsaaDoneFirstUlnk.set(SPACES);
		wsaaSeqnbr.add(1);
		processIncis2000();
		processUlnks3000();
		processUtrns4000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processIncis2000()
	{
		start2000();
	}

protected void start2000()
	{
		incirevIO.setParams(SPACES);
		incirevIO.setChdrcoy(greversrec.chdrcoy);
		incirevIO.setChdrnum(greversrec.chdrnum);
		incirevIO.setLife(greversrec.life);
		incirevIO.setCoverage(greversrec.coverage);
		incirevIO.setRider(greversrec.rider);
		incirevIO.setPlanSuffix(greversrec.planSuffix);
		incirevIO.setTranno(ZERO);
		incirevIO.setFormat(incirevrec);
		incirevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incirevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incirevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(incirevIO.getStatuz(),varcom.endp))) {
			deleteInci2100();
		}
		
	}

protected void deleteInci2100()
	{
		try {
			start2100();
		}
		catch (GOTOException e){
		}
	}

protected void start2100()
	{
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)
		&& isNE(incirevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(incirevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		if (isNE(greversrec.chdrcoy,incirevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,incirevIO.getChdrnum())
		|| isNE(greversrec.life,incirevIO.getLife())
		|| isNE(greversrec.coverage,incirevIO.getCoverage())
		|| isNE(greversrec.rider,incirevIO.getRider())
		|| isNE(greversrec.planSuffix,incirevIO.getPlanSuffix())) {
			incirevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2190);
		}
		incirevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			systemError9000();
		}
		incirevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			systemError9000();
		}
		incirevIO.setFunction(varcom.nextr);
	}

protected void processUlnks3000()
	{
		start3000();
	}

protected void start3000()
	{
		ulnkrevIO.setParams(SPACES);
		ulnkrevIO.setChdrcoy(greversrec.chdrcoy);
		ulnkrevIO.setChdrnum(greversrec.chdrnum);
		ulnkrevIO.setLife(greversrec.life);
		ulnkrevIO.setCoverage(greversrec.coverage);
		ulnkrevIO.setRider(greversrec.rider);
		ulnkrevIO.setPlanSuffix(greversrec.planSuffix);
		ulnkrevIO.setTranno(ZERO);
		ulnkrevIO.setFormat(ulnkrevrec);
		ulnkrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ulnkrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ulnkrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(ulnkrevIO.getStatuz(),varcom.endp))) {
			ulnkLoop3100();
		}
		
	}

protected void ulnkLoop3100()
	{
		try {
			start3100();
		}
		catch (GOTOException e){
		}
	}

protected void start3100()
	{
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)
		&& isNE(ulnkrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(ulnkrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3190);
		}
		if (isNE(greversrec.chdrcoy,ulnkrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,ulnkrevIO.getChdrnum())
		|| isNE(greversrec.life,ulnkrevIO.getLife())
		|| isNE(greversrec.coverage,ulnkrevIO.getCoverage())
		|| isNE(greversrec.rider,ulnkrevIO.getRider())
		|| isNE(greversrec.planSuffix,ulnkrevIO.getPlanSuffix())) {
			ulnkrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(wsaaDoneFirstUlnk,SPACES)) {
			if (isLTE(ulnkrevIO.getTranno(),greversrec.tranno)) {
				createUnlt3200();
			}
			wsaaDoneFirstUlnk.set("Y");
		}
		ulnkrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			systemError9000();
		}
		ulnkrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			systemError9000();
		}
		ulnkrevIO.setFunction(varcom.nextr);
	}

protected void createUnlt3200()
	{
		start3200();
	}

protected void start3200()
	{
		unltrevIO.setParams(SPACES);
		unltrevIO.setFormat(unltrevrec);
		unltrevIO.setFunction(varcom.writr);
		unltrevIO.setChdrcoy(ulnkrevIO.getChdrcoy());
		unltrevIO.setChdrnum(ulnkrevIO.getChdrnum());
		unltrevIO.setLife(ulnkrevIO.getLife());
		unltrevIO.setCoverage(ulnkrevIO.getCoverage());
		unltrevIO.setRider(ulnkrevIO.getRider());
		unltrevIO.setSeqnbr(wsaaSeqnbr);
		unltrevIO.setCurrfrom(ulnkrevIO.getCurrfrom());
		unltrevIO.setCurrto(varcom.vrcmMaxDate);
		unltrevIO.setTranno(greversrec.newTranno);
		unltrevIO.setPremTopupInd(SPACES);
		unltrevIO.setValidflag("1");
		unltrevIO.setPercOrAmntInd(ulnkrevIO.getPercOrAmntInd());
		if (isEQ(ulnkrevIO.getPlanSuffix(),ZERO)) {
			getPolsum3300();
			if (isGT(chdrenqIO.getPolinc(),1)) {
				unltrevIO.setNumapp(chdrenqIO.getPolsum());
			}
			else {
				unltrevIO.setNumapp(1);
			}
		}
		else {
			unltrevIO.setNumapp(1);
		}
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			unltrevIO.setUalfnd(wsaaSub, ulnkrevIO.getUalfnd(wsaaSub));
			unltrevIO.setUalprc(wsaaSub, ulnkrevIO.getUalprc(wsaaSub));
			unltrevIO.setUspcpr(wsaaSub, ulnkrevIO.getUspcpr(wsaaSub));
		}
		SmartFileCode.execute(appVars, unltrevIO);
		if (isNE(unltrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(unltrevIO.getParams());
			syserrrec.statuz.set(unltrevIO.getStatuz());
			systemError9000();
		}
	}

protected void getPolsum3300()
	{
		/*START*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setChdrcoy(ulnkrevIO.getChdrcoy());
		chdrenqIO.setChdrnum(ulnkrevIO.getChdrnum());
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.params.set(chdrenqIO.getStatuz());
			systemError9000();
		}
		/*EXIT*/
	}

protected void processUtrns4000()
	{
		start4000();
	}

protected void start4000()
	{
		utrncfiIO.setParams(SPACES);
		utrncfiIO.setChdrcoy(greversrec.chdrcoy);
		utrncfiIO.setChdrnum(greversrec.chdrnum);
		utrncfiIO.setLife(greversrec.life);
		utrncfiIO.setCoverage(greversrec.coverage);
		utrncfiIO.setRider(greversrec.rider);
		utrncfiIO.setPlanSuffix(greversrec.planSuffix);
		utrncfiIO.setTranno(greversrec.tranno);
		utrncfiIO.setFormat(utrncfirec);
		utrncfiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrncfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(utrncfiIO.getStatuz(),varcom.endp))) {
			reverseUtrns4100();
		}
		
	}

protected void reverseUtrns4100()
	{
		try {
			start4100();
		}
		catch (GOTOException e){
		}
	}

protected void start4100()
	{
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(),varcom.oK)
		&& isNE(utrncfiIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(utrncfiIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit4190);
		}
		if (isNE(utrncfiIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(utrncfiIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(utrncfiIO.getLife(),greversrec.life)
		|| isNE(utrncfiIO.getCoverage(),greversrec.coverage)
		|| isNE(utrncfiIO.getRider(),greversrec.rider)
		|| isNE(utrncfiIO.getPlanSuffix(),greversrec.planSuffix)
		|| isNE(utrncfiIO.getTranno(),greversrec.tranno)) {
			utrncfiIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit4190);
		}
		deleteRewriteUtrn4200();
		utrncfiIO.setFunction(varcom.nextr);
	}

protected void deleteRewriteUtrn4200()
	{
		start4200();
	}

protected void start4200()
	{
		if (isEQ(utrncfiIO.getFeedbackInd(),SPACES)) {
			utrncfiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, utrncfiIO);
			if (isNE(utrncfiIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(utrncfiIO.getParams());
				syserrrec.statuz.set(utrncfiIO.getStatuz());
				databaseError9500();
			}
			utrncfiIO.setFunction(varcom.delet);
		}
		else {
			utrncfiIO.setFeedbackInd(SPACES);
			utrncfiIO.setTriggerModule(SPACES);
			utrncfiIO.setTriggerKey(SPACES);
			utrncfiIO.setSurrenderPercent(ZERO);
			setPrecision(utrncfiIO.getContractAmount(), 2);
			utrncfiIO.setContractAmount(mult(utrncfiIO.getContractAmount(),-1));
			setPrecision(utrncfiIO.getFundAmount(), 2);
			utrncfiIO.setFundAmount(mult(utrncfiIO.getFundAmount(),-1));
			setPrecision(utrncfiIO.getNofDunits(), 5);
			utrncfiIO.setNofDunits(mult(utrncfiIO.getNofDunits(),-1));
			setPrecision(utrncfiIO.getNofUnits(), 5);
			utrncfiIO.setNofUnits(mult(utrncfiIO.getNofUnits(),-1));
			setPrecision(utrncfiIO.getProcSeqNo(), 0);
			utrncfiIO.setProcSeqNo(mult(utrncfiIO.getProcSeqNo(),-1));
			utrncfiIO.setTransactionDate(greversrec.transDate);
			utrncfiIO.setTransactionTime(greversrec.transTime);
			utrncfiIO.setTranno(greversrec.newTranno);
			utrncfiIO.setTermid(greversrec.termid);
			utrncfiIO.setUser(greversrec.user);
			wsaaBatckey.set(greversrec.batckey);
			utrncfiIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			utrncfiIO.setBatccoy(wsaaBatckey.batcBatccoy);
			utrncfiIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			utrncfiIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			utrncfiIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			utrncfiIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			utrncfiIO.setUstmno(ZERO);
			utrncfiIO.setFunction(varcom.writr);
		}
		SmartFileCode.execute(appVars, utrncfiIO);
		if (isNE(utrncfiIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrncfiIO.getParams());
			syserrrec.statuz.set(utrncfiIO.getStatuz());
			databaseError9500();
		}
	}

protected void systemError9000()
	{
		try {
			start9000();
		}
		catch (GOTOException e){
		}
		finally{
			exit9490();
		}
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		try {
			start9500();
		}
		catch (GOTOException e){
		}
		finally{
			exit9990();
		}
	}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9990);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}
}
