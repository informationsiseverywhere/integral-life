/*
 * File: Revpsur.java
 * Date: 30 August 2009 2:10:52
 * Author: Quipoz Limited
 * 
 * Class transformed from REVPSUR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.RtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.PtsdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.PtshclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UdelTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UdivTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      -----------------------------------
*            PART SURRENDER REVERSAL
*      -----------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*
*  Note:-  surrender implies Part Surrender.
*
*  REVERSE SURRENDER CLAIM HEADER
*
*  READH on the Surrender Claim Header record (PTSHCLM) for
*                the relevant contract.
*
*  Store the transaction number.
*
*  DELET this record.
*
*
*  REVERSE COMPONENTS.
*
*  Read all  the Coverage/Rider records (COVR) for this contract
*  and update each COVR record as follows:
*
*  READH on  the  COVR  records  (COVR)    for the relevant
*  contract.
*
*  DELET this record.
*
*  NEXTR on  the  COVR  file should read a record with  a  valid
*  flag  of  '2', if not, then this is an error
*
*  REWRT this  COVR  detail record after altering
*  the  valid  flag  from  '2'  to  '1'  as this is to
*  re-instate the coverage prior to the surrender.
*
*
*  REVERSE CLAIM DETAIL RECORDS
*
*  READH on  the  Surrender  Claim details record (PTSDCLM)
*  for the relevant contract.
*
*  DELET all records.
*
*
*  REVERSAL OF UTRNS
*
*       Call the T5671 generic subroutine to reverse Unit
*       transactions for the TRANNO being reversed.
*       The subroutine GREVUTRN should be entered in the
*       relevant table entry.
*
*
*  CONTRACT REVERSAL ACCOUNTING
*
*       Do a BEGNH on the ACMV file for this transaction no. and
*       call LIFACMV to post the reversal to the sub-account.
*
*****************************************************************
* </pre>
*/
public class Revpsur extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVPSUR";
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaStoredPtshTranno = new ZonedDecimalData(5, 0).init(ZERO);
		/* WSAA-SUBSCRIPTS */
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaT5671 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 4);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtsdclmTableDAM ptsdclmIO = new PtsdclmTableDAM();
	private PtshclmTableDAM ptshclmIO = new PtshclmTableDAM();
	private RtrnrevTableDAM rtrnrevIO = new RtrnrevTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private UdelTableDAM udelIO = new UdelTableDAM();
	private UdivTableDAM udivIO = new UdivTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private T5671rec t5671rec = new T5671rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNext4650
	}

	public Revpsur() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this subroutine using the fatal error section   *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		/* Get todays date from DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Read in the contract header details*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
	}

protected void process2000()
	{
		/*PROCESS*/
		reverseClaimsHeader2100();
		processComponents3300();
		reverseClaimDetails4100();
		processAcmvs4500();
		processAcmvOptical4510();
		processRtrns5000();
		deleteTax6000();
		processUdel7000();
		processUdiv8000();
		/*EXIT*/
	}

protected void reverseClaimsHeader2100()
	{
		readClaimHeader2101();
	}

protected void readClaimHeader2101()
	{
		ptshclmIO.setParams(SPACES);
		ptshclmIO.setChdrcoy(reverserec.company);
		ptshclmIO.setChdrnum(reverserec.chdrnum);
		ptshclmIO.setPlanSuffix(ZERO);
		ptshclmIO.setTranno(reverserec.tranno);
		/* MOVE BEGNH                  TO PTSHCLM-FUNCTION.             */
		ptshclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptshclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptshclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		ptshclmIO.setFormat(formatsInner.ptshclmrec);
		SmartFileCode.execute(appVars, ptshclmIO);
		if ((isNE(ptshclmIO.getStatuz(),varcom.oK))
		&& (isNE(ptshclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ptshclmIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(ptshclmIO.getChdrcoy(),reverserec.company))
		|| (isNE(ptshclmIO.getChdrnum(),reverserec.chdrnum))
		|| (isNE(ptshclmIO.getTranno(),reverserec.tranno))) {
			ptshclmIO.setStatuz(varcom.endp);
			xxxxFatalError();
		}
		wsaaStoredPtshTranno.set(ptshclmIO.getTranno());
		greversrec.planSuffix.set(ptshclmIO.getPlanSuffix());
		/* MOVE DELET                  TO PTSHCLM-FUNCTION.             */
		ptshclmIO.setFunction(varcom.deltd);
		ptshclmIO.setFormat(formatsInner.ptshclmrec);
		SmartFileCode.execute(appVars, ptshclmIO);
		if (isNE(ptshclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptshclmIO.getParams());
			xxxxFatalError();
		}
	}

protected void processComponents3300()
	{
		covers3301();
	}

protected void covers3301()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setTranno(ZERO);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setFormat(formatsInner.covrrec);
		/* MOVE BEGNH                  TO COVR-FUNCTION.                */
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !((isEQ(covrIO.getStatuz(),varcom.endp))
		|| (isEQ(covrIO.getTranno(),reverserec.tranno)))) {
			SmartFileCode.execute(appVars, covrIO);
			if ((isNE(covrIO.getStatuz(),varcom.oK))
			&& (isNE(covrIO.getStatuz(),varcom.endp))) {
				syserrrec.params.set(covrIO.getParams());
				xxxxFatalError();
			}
			/*    IF (REVE-CHDRNUM      NOT = COVR-CHDRNUM) OR              */
			/*       (REVE-COMPANY      NOT = COVR-CHDRCOY)                 */
			/*       MOVE COVR-PARAMS      TO SYSR-PARAMS                   */
			/*       PERFORM XXXX-FATAL-ERROR                               */
			/*    END-IF                                                    */
			if (isNE(reverserec.chdrnum,covrIO.getChdrnum())
			|| isNE(reverserec.company,covrIO.getChdrcoy())
			|| isEQ(covrIO.getStatuz(),varcom.endp)) {
				covrIO.setStatuz(varcom.endp);
			}
			covrIO.setFunction(varcom.nextr);
		}
		
		if (isNE(covrIO.getValidflag(),"1")) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
			deleteAndUpdatComp3400();
		}
		
	}

protected void deleteAndUpdatComp3400()
	{
			deleteRecs3405();
		}

protected void deleteRecs3405()
	{
		/* valid flag equal to 1*/
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		readGenericProcTab3800();
		covrIO.setFunction(varcom.delet);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK))
		&& (isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrIO.getStatuz(),varcom.endp))
		|| (isNE(covrIO.getChdrnum(),reverserec.chdrnum))
		|| (isNE(covrIO.getChdrcoy(),reverserec.company))) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(covrIO.getValidflag(),"2")) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		/* MOVE REVE-TRANS-DATE        TO COVR-TRANSACTION-DATE.        */
		/* MOVE REVE-TRANS-TIME        TO COVR-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO COVR-USER.                    */
		/* MOVE REVE-TERMID            TO COVR-TERMID.                  */
		/* MOVE REVE-NEW-TRANNO        TO COVR-TRANNO.                  */
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		/* MOVE UPDAT                  TO COVR-FUNCTION.                */
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK))
		&& (isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrIO.getStatuz(),varcom.endp))
		|| (isNE(covrIO.getChdrnum(),reverserec.chdrnum))
		|| (isNE(reverserec.company,covrIO.getChdrcoy()))) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		while ( !((isEQ(covrIO.getStatuz(),varcom.endp))
		|| (isEQ(covrIO.getValidflag(),"1"))
		&& (isEQ(covrIO.getTranno(),reverserec.tranno)))) {
			getNextCovr3500();
		}
		
	}

protected void getNextCovr3500()
	{
		/*GET-RECS*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrIO.getStatuz(),varcom.endp))
		|| (isNE(covrIO.getChdrnum(),reverserec.chdrnum))
		|| (isNE(reverserec.company,covrIO.getChdrcoy()))) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void readGenericProcTab3800()
	{
		readStatusCodes3810();
	}

protected void readStatusCodes3810()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Batc.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(wsaaStoredPtshTranno);
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.life.set(covrIO.getLife());
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.batckey.set(wsaaBatckey);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callSubprog3900();
		}
	}

protected void callSubprog3900()
	{
		/*GO*/
		/* IF T5671-SUBPROG(WSAA-SUB) NOT = SPACES                      */
		/*    CALL T5671-SUBPROG(WSAA-SUB) USING GREV-REVERSE-REC       */
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(greversrec.statuz);
				xxxxFatalError();
			}
		}
		/*EXIT*/
	}

protected void reverseClaimDetails4100()
	{
		/*PROCESS-CLAIMS*/
		ptsdclmIO.setParams(SPACES);
		ptsdclmIO.setChdrcoy(reverserec.company);
		ptsdclmIO.setChdrnum(reverserec.chdrnum);
		ptsdclmIO.setTranno(wsaaStoredPtshTranno);
		ptsdclmIO.setPlanSuffix(reverserec.planSuffix);
		/* MOVE BEGNH                  TO PTSDCLM-FUNCTION.             */
		ptsdclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptsdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptsdclmIO.setFitKeysSearch("CHDRCOY","TRANNO");
		ptsdclmIO.setFormat(formatsInner.ptsdclmrec);
		while ( !(isEQ(ptsdclmIO.getStatuz(),varcom.endp))) {
			reverseClaims4300();
		}
		
		/*EXIT*/
	}

protected void reverseClaims4300()
	{
			update4301();
		}

protected void update4301()
	{
		SmartFileCode.execute(appVars, ptsdclmIO);
		if ((isNE(ptsdclmIO.getStatuz(),varcom.oK))
		&& (isNE(ptsdclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ptsdclmIO.getStatuz());
			xxxxFatalError();
		}
		if ((isEQ(ptsdclmIO.getStatuz(),varcom.endp))
		|| (isNE(ptsdclmIO.getChdrnum(),chdrlifIO.getChdrnum()))
		|| (isNE(ptsdclmIO.getChdrcoy(),reverserec.company))
		|| (isNE(ptsdclmIO.getTranno(),wsaaStoredPtshTranno))) {
			ptsdclmIO.setStatuz(varcom.endp);
			return ;
		}
		/* MOVE DELET                  TO PTSDCLM-FUNCTION.             */
		ptsdclmIO.setFunction(varcom.deltd);
		ptsdclmIO.setFormat(formatsInner.ptsdclmrec);
		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptsdclmIO.getStatuz());
			xxxxFatalError();
		}
		ptsdclmIO.setFunction(varcom.nextr);
	}

protected void processAcmvs4500()
	{
		readSubAcctTab4510();
	}

protected void readSubAcctTab4510()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setBatctrcde(SPACES);
		while ( !((isEQ(acmvrevIO.getStatuz(),varcom.endp))
		|| (isEQ(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)))) {
			SmartFileCode.execute(appVars, acmvrevIO);
			if ((isNE(acmvrevIO.getStatuz(),varcom.oK))
			&& (isNE(acmvrevIO.getStatuz(),varcom.endp))) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
			if ((isNE(acmvrevIO.getRldgcoy(),reverserec.company))
			|| (isNE(acmvrevIO.getRdocnum(),reverserec.chdrnum))
			|| (isNE(acmvrevIO.getTranno(),reverserec.tranno))) {
				acmvrevIO.setStatuz(varcom.endp);
			}
			acmvrevIO.setFunction(varcom.nextr);
		}
		
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvRecs4600();
		}
		
	}

protected void processAcmvOptical4510()
	{
		start4510();
	}

protected void start4510()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			xxxxFatalError();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs4600();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void reverseAcmvRecs4600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					reverse4610();
				case readNext4650: 
					readNext4650();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void reverse4610()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		/* MOVE ACMVREV-SACSCODE       TO LIFA-GLSIGN.                  */
		/* MOVE ACMVREV-SACSTYP        TO LIFA-GLSIGN.                  */
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		/* MOVE CHDRLIF-CHDRNUM        TO LIFA-RLDGACCT.                */
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		/* MOVE CHDRLIF-CNTCURR        TO LIFA-ORIGCURR.                */
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(wsaaStoredPtshTranno);
		descIO.setDescitem(wsaaStoredPtshTranno);
		getDescription4800();
		lifacmvrec.trandesc.set(wsaaRevTrandesc);
		/* MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE '99999999'             TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,"****")) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void readNext4650()
	{
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'         USING ACMVREV-PARAMS.               */
		if (isEQ(wsaaIoCall,"CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if ((isNE(acmvrevIO.getStatuz(),varcom.oK))
		&& (isNE(acmvrevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acmvrevIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(acmvrevIO.getRldgcoy(),reverserec.company))
		|| (isNE(acmvrevIO.getRdocnum(),reverserec.chdrnum))
		|| (isNE(acmvrevIO.getTranno(),reverserec.tranno))
		|| (isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			goTo(GotoLabel.readNext4650);
		}
	}

protected void getDescription4800()
	{
		para4801();
	}

protected void para4801()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			xxxxFatalError();
		}
		/* MOVE DESC-LONGDESC          TO LIFA-TRANDESC.                */
		wsaaRevTrandesc.set(descIO.getLongdesc());
	}

protected void processRtrns5000()
	{
		start5001();
	}

protected void start5001()
	{
		rtrnrevIO.setParams(SPACES);
		rtrnrevIO.setRldgcoy(reverserec.company);
		rtrnrevIO.setRdocnum(reverserec.chdrnum);
		rtrnrevIO.setTranno(reverserec.tranno);
		rtrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rtrnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(),varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company,rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,rtrnrevIO.getRdocnum())
		|| isNE(reverserec.tranno,rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(rtrnrevIO.getStatuz(),varcom.endp))) {
			reverseRtrns5100();
		}
		
	}

protected void reverseRtrns5100()
	{
					start5100();
					readNextRtrn5180();
				}

protected void start5100()
	{
		if (isNE(rtrnrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			return ;
		}
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(ZERO);
		lifrtrnrec.jrnseq.set(ZERO);
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(reverserec.batchkey);
		lifrtrnrec.rdocnum.set(rtrnrevIO.getRdocnum());
		lifrtrnrec.tranno.set(reverserec.newTranno);
		lifrtrnrec.tranref.set(reverserec.newTranno);
		lifrtrnrec.sacscode.set(rtrnrevIO.getSacscode());
		lifrtrnrec.sacstyp.set(rtrnrevIO.getSacstyp());
		lifrtrnrec.glcode.set(rtrnrevIO.getGlcode());
		lifrtrnrec.glsign.set(rtrnrevIO.getGlsign());
		lifrtrnrec.rldgcoy.set(rtrnrevIO.getRldgcoy());
		lifrtrnrec.genlcoy.set(rtrnrevIO.getGenlcoy());
		lifrtrnrec.origcurr.set(rtrnrevIO.getOrigccy());
		lifrtrnrec.rldgacct.set(rtrnrevIO.getRldgacct());
		compute(lifrtrnrec.acctamt, 2).set(mult(rtrnrevIO.getAcctamt(),-1));
		compute(lifrtrnrec.origamt, 2).set(mult(rtrnrevIO.getOrigamt(),-1));
		lifrtrnrec.genlcur.set(rtrnrevIO.getGenlcur());
		lifrtrnrec.crate.set(rtrnrevIO.getCrate());
		lifrtrnrec.postyear.set(SPACES);
		lifrtrnrec.postmonth.set(SPACES);
		lifrtrnrec.trandesc.set(wsaaRevTrandesc);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE REVE-EFFDATE-2         TO LIFR-EFFDATE.                 */
		/* MOVE WSAA-TODAY             TO LIFR-EFFDATE.                 */
		lifrtrnrec.effdate.set(rtrnrevIO.getEffdate());
		lifrtrnrec.termid.set(reverserec.termid);
		lifrtrnrec.user.set(reverserec.user);
		lifrtrnrec.transactionTime.set(reverserec.transTime);
		lifrtrnrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserrrec.statuz.set(lifrtrnrec.statuz);
			xxxxFatalError();
		}
	}

protected void readNextRtrn5180()
	{
		/*  Read the next RTRN record.                                     */
		rtrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(),varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(reverserec.company,rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,rtrnrevIO.getRdocnum())
		|| isNE(reverserec.tranno,rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void deleteTax6000()
	{
		/*START*/
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(ZERO);
		taxdrevIO.setFormat(formatsInner.taxdrevrec);
		taxdrevIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			delTax6100();
		}
		
		/*EXIT*/
	}

protected void delTax6100()
	{
		start6110();
	}

protected void start6110()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(reverserec.company, taxdrevIO.getChdrcoy())
		&& isEQ(reverserec.chdrnum, taxdrevIO.getChdrnum())
		&& isEQ(reverserec.tranno, taxdrevIO.getTranno())) {
			taxdrevIO.setStatuz(varcom.oK);
			taxdrevIO.setFunction(varcom.deltd);
			taxdrevIO.setFormat(formatsInner.taxdrevrec);
			SmartFileCode.execute(appVars, taxdrevIO);
			if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(taxdrevIO.getParams());
				xxxxFatalError();
			}
		}
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void processUdel7000()
	{
		processUdelInit7010();
	}

protected void processUdelInit7010()
	{
		udelIO.setDataArea(SPACES);
		udelIO.setFormat(formatsInner.udelrec);
		udelIO.setChdrcoy(reverserec.company);
		udelIO.setChdrnum(reverserec.chdrnum);
		udelIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, udelIO);
		if (isNE(udelIO.getStatuz(), varcom.oK)
		&& isNE(udelIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(udelIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(udelIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		udelIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, udelIO);
		if (isNE(udelIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(udelIO.getParams());
			xxxxFatalError();
		}
	}

protected void processUdiv8000()
	{
		processUdivInit8010();
	}

protected void processUdivInit8010()
	{
		udivIO.setDataArea(SPACES);
		udivIO.setFormat(formatsInner.udivrec);
		udivIO.setChdrcoy(reverserec.company);
		udivIO.setChdrnum(reverserec.chdrnum);
		udivIO.setTranno(reverserec.tranno);
		udivIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, udivIO);
		if (isNE(udivIO.getStatuz(), varcom.oK)
		&& isNE(udivIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(udivIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(udivIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		udivIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, udivIO);
		if (isNE(udivIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(udivIO.getParams());
			xxxxFatalError();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData ptshclmrec = new FixedLengthStringData(10).init("PTSHCLMREC");
	private FixedLengthStringData ptsdclmrec = new FixedLengthStringData(10).init("PTSDCLMREC");
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData taxdrevrec = new FixedLengthStringData(10).init("TAXDREVREC");
	private FixedLengthStringData udelrec = new FixedLengthStringData(10).init("UDELREC");
	private FixedLengthStringData udivrec = new FixedLengthStringData(10).init("UDIVREC");
}
}
