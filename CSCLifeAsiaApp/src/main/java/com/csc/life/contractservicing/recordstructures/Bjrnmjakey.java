package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:44
 * Description:
 * Copybook name: BJRNMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bjrnmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bjrnmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData bjrnmjaKey = new FixedLengthStringData(64).isAPartOf(bjrnmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData bjrnmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(bjrnmjaKey, 0);
  	public FixedLengthStringData bjrnmjaChdrnum = new FixedLengthStringData(8).isAPartOf(bjrnmjaKey, 1);
  	public PackedDecimalData bjrnmjaTranno = new PackedDecimalData(5, 0).isAPartOf(bjrnmjaKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(bjrnmjaKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bjrnmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bjrnmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}