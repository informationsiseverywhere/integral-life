package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;

import com.csc.life.contractservicing.dataaccess.model.Pcdtpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PcdtpfDAO extends BaseDAO<Pcdtpf> {
	public Pcdtpf getPcdtmjaByKey(Pcdtpf pcdtpf);
	public int insertPcdtpfPcdtpf (Pcdtpf pcdtpf);
	public List<Pcdtpf> getPcdtmja(String chdrcoy, String chdrnum, int tranno); //ILIFE-8786
	public int deletePcdtmjaRecord(List<Long> ids); //ILIFE-8786
}
