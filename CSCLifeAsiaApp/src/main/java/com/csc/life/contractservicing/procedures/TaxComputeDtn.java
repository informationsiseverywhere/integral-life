package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.DecimalFormat;

import com.csc.life.contractservicing.recordstructures.TaxDeductionRec;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;

public class TaxComputeDtn extends SMARTCodeModel {
	
	protected Syserrrec syserrrec = new Syserrrec();
	protected TaxDeductionRec taxdrec = new TaxDeductionRec();
	
	public void postAcmv(Batcdorrec batcdorrec,TaxDeductionRec taxdrec,Varcom varcom, T5645rec t5645rec,String trandesc,Chdrpf chdrpf, Covrpf covrpf) {
		Lifacmvrec lifacmvrec = new Lifacmvrec();
		this.taxdrec = taxdrec;
	    int wsaaJrnseq = 0;
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.rldgcoy.set(batcdorrec.company);
		lifacmvrec.genlcoy.set(batcdorrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.rdocnum.set(taxdrec.getChdrnum());
		lifacmvrec.rldgacct.set(taxdrec.getChdrnum());
		lifacmvrec.tranref.set(taxdrec.getChdrnum());
		lifacmvrec.tranno.set(chdrpf.getTranno()+1);
		lifacmvrec.effdate.set(taxdrec.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(taxdrec.getEffdate());
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.trandesc.set(trandesc);
		lifacmvrec.substituteCode[1].set(taxdrec.getCnttype());
		lifacmvrec.origamt.set(taxdrec.getTotalTaxAmt());
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		callLifacmv(lifacmvrec,++wsaaJrnseq);	
		dtnPostings(t5645rec, covrpf,taxdrec,lifacmvrec, wsaaJrnseq);
		if(taxdrec.isNoticeCorrection())
		{
			lifacmvrec.rldgacct.set(taxdrec.getChdrnum());
			lifacmvrec.substituteCode[1].set(taxdrec.getCnttype());
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			callLifacmv(lifacmvrec,++wsaaJrnseq);
		}
		
	}
	
	protected  void callLifacmv(Lifacmvrec lifacmvrec,int wsaaJrnseq) {
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		//wsaaJrnseq++;
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError();
		}
	}
	
	protected void fatalError(){
		error();
		exitProgram();
	}
	
	protected void error(){
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		taxdrec.statuz.set("BOMB");;
	}
	
	public  void dtnPostings(T5645rec t5645rec, Covrpf covrpf,TaxDeductionRec taxdrec,Lifacmvrec lifacmvrec,int wsaaJrnseq)
	{
		// child class will implement
	}

}
