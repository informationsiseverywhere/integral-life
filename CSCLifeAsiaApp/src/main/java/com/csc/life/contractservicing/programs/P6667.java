/*
 * File: P6667.java
 * Date: 30 August 2009 0:49:24
 * Author: Quipoz Limited
 *
 * Class transformed from P6667.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.tablestructures.T3678rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.MandmjaTableDAM;
import com.csc.life.contractservicing.screens.S6667ScreenVars;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*           P6667 - Mandate Details(Billing Change)
*           ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Overview
*~~~~~~~~
*
*One  of  the  facilities  provided  by  the  billing  change
*subsystem  is  to  allow  changing direct debit details. The
*function of this program is to support this facility and  be
*able  to  display  the  existing mandate details(if there is
*any)  and  capture  any   modified/new   details.   Standard
*windowing  facility  is  available for the mandate reference
*number.
*
*
*MANDMJA
*~~~~~~~
*
*Create a  logical  file  MANDMJA  based  on  MANDPF  and  to
*include  all  the  physical file fields and to have the same
*keys as MANDLNB.
*
*
*S6667
*~~~~~
*
*This is a window screen which can be cloned from S5070.  The
*size  of  it  must be the same as S6663 and S6664. It should
*have the exact details as S5070.
*
*Compile the screen with type *DSPF.
*
*
*P6667
*~~~~~
*
*Clone the program from P5070.
*
*Basically we are using the exact programming logic as  P5070
*but  we  will  be using different logical files because this
*belongs to different subsystem.
*
*Replace all 'P5070' with 'P6667'.
*Replace all 'S5070' with 'S6667'.
*
*Replace 'MANDLNBREC' with 'MANDMJAREC'.
*Replace 'MANDLNBSKM' with 'MANDMJASKM'.
*Replace all 'MANDLNBIO' with 'MANDMJAIO'.
*Replace all 'MANDLNB-' with 'MANDMJA-'.
*
*Replace 'CHDRLNBREC' with 'CHDRMJAREC'.
*Replace 'CHDRLNBSKM' with 'CHDRMJASKM'.
*Replace all 'CHDRLNBIO' with 'CHDRMJAIO'.
*
*Replace all 'CHDRLNB-' with 'CHDRMJA-'.
*
*Delete all commented out codings.
* </pre>
*/
public class P6667 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6667");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);

	private FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 0);
	private FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 30);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private static final String wsaaBillchnl = "D";
		/* ERRORS */
	private static final String e756 = "E756";
	private static final String f373 = "F373";
	private static final String f955 = "F955";
	private static final String g600 = "G600";
	private static final String h926 = "H926";
	private static final String h928 = "H928";
	private static final String i011 = "I011";
	private static final String i014 = "I014";
	private static final String i020 = "I020";
		/* TABLES */
	private static final String t3678 = "T3678";
	private static final String clrfrec = "CLRFREC";
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
		/*Client Roles by Foreign Key*/
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Mandate Details*/
	private MandmjaTableDAM mandmjaIO = new MandmjaTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
	private T3678rec t3678rec = new T3678rec();
	private Wssplife wssplife = new Wssplife();
	private S6667ScreenVars sv = ScreenProgram.getScreenVars( S6667ScreenVars.class);
	private boolean ispermission=false;
	//ILIFE-2771 Starts
	private MandTableDAM mandIO = new MandTableDAM();
	private static final String mandrec = "MANDREC";
	//ILIFE-2771 Ends
	private BabrpfDAO babrpfDao = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class); //IBPLIFE-12452
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkForErrors2080,
		exit2090
	}

	public P6667() {
		super();
		screenVars = sv;
		new ScreenModel("S6667", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		ispermission = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "MTL29603", appVars,"IT"); 
		if(!ispermission) {
			sv.mrbnkOut[varcom.nd.toInt()].set("Y");
		}
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		sv.payrnum.set(clrfIO.getClntnum());
		sv.numsel.set(clrfIO.getClntnum());
		wsaaClntkey.set(SPACES);
		if (isNE(clrfIO.getClntnum(),SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clrfIO.getClntpfx());
			stringVariable1.addExpression(clrfIO.getClntcoy());
			stringVariable1.addExpression(clrfIO.getClntnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		sv.currcode.set(SPACES);
		sv.facthous.set(SPACES);
		sv.billcd.set(payrIO.getBillcd());
		/* MOVE PAYR-MANDREF           TO S6667-MANDREF.        <V76F13>*/
		keepMand(); //ILIFE-2771
		if (isNE(payrIO.getMandref(), SPACES)) {
			if (isEQ(payrIO.getBillchnl(), "D")) {
		sv.mandref.set(payrIO.getMandref());
			}
			else {
				sv.mandref.set(SPACES);
				return ;
			}
		}
		if (isEQ(payrIO.getMandref(),SPACES)) {
			return ;
		}

		mandmjaIO.setParams(SPACES);
		mandmjaIO.setPayrcoy(wsspcomn.fsuco);
		mandmjaIO.setPayrnum(sv.payrnum);
		mandmjaIO.setMandref(payrIO.getMandref());
		mandmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandmjaIO);
		if (isNE(mandmjaIO.getStatuz(),varcom.oK)
		&& isNE(mandmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(mandmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(mandmjaIO.getStatuz(),varcom.mrnf)) {
			sv.mandrefErr.set("H929");
			return ;
		}
		clblIO.setParams(SPACES);
		clblIO.setBankkey(mandmjaIO.getBankkey());
		sv.bankkey.set(mandmjaIO.getBankkey());
		clblIO.setBankacckey(mandmjaIO.getBankacckey());
		sv.bankacckey.set(mandmjaIO.getBankacckey());
		clblIO.setClntcoy(clrfIO.getClntcoy());
		clblIO.setClntnum(clrfIO.getClntnum());
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isNE(clrfIO.getClntnum(),clblIO.getClntnum())) {
			sv.payrnumErr.set(f373);
			return ;
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.facthous.set(SPACES);
			sv.bankkey.set(SPACES);
			sv.bankacckey.set(SPACES);
			return ;
		}
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if(ispermission) {
			sv.mrbnk.set(clblIO.getMrbnk()); 			// ILIFE-2476
			sv.bnkbrn.set(clblIO.getBnkbrn()); 			// ILIFE-2476
		}else{
			sv.mrbnk.set(SPACES);
			sv.bnkbrn.set(SPACES);
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		sv.facthous.set(clblIO.getFacthous());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t3678);
		itemIO.setItemitem(mandmjaIO.getMandstat());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3678rec.t3678Rec.set(itemIO.getGenarea());
		if (isEQ(t3678rec.gonogoflg,"N")) {
			sv.mandrefErr.set(i014);
			return ;
		}
		if (isGT(clblIO.getCurrfrom(),mandmjaIO.getEffdate())) {
			sv.bankacckeyErr.set(i020);
			return ;
		}
		if (isNE(clblIO.getCurrto(),ZERO)) {
			if (isLT(clblIO.getCurrto(),mandmjaIO.getEffdate())) {
				sv.bankacckeyErr.set(i020);
				return ;
			}
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.clntkey.set(SPACES);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validate2020();
					bankbrndsc();//ILIFE-2476
				case checkForErrors2080:
					checkForErrors2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.mandref,SPACES)) {
			sv.mandrefErr.set(h926);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.mandref,SPACES)) {
			if (isNE(sv.mandref,NUMERIC)) {
				sv.mandrefErr.set(i011);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		mandmjaIO.setParams(SPACES);
		mandmjaIO.setPayrcoy(wsspcomn.fsuco);
		mandmjaIO.setPayrnum(sv.payrnum);
		mandmjaIO.setMandref(sv.mandref);
		mandmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandmjaIO);
		if (isNE(mandmjaIO.getStatuz(),varcom.oK)
		&& isNE(mandmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(mandmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(mandmjaIO.getStatuz(),varcom.mrnf)
				|| isEQ(mandmjaIO.getPayind(), "P")) { // ILIFE-2472
			sv.mandrefErr.set(h928);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(mandmjaIO.getCrcind(), "C")) {
			sv.mandrefErr.set(h928);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Move bank account details to corresponding fields*/
		sv.bankkey.set(mandmjaIO.getBankkey());
		sv.bankacckey.set(mandmjaIO.getBankacckey());
	}

protected void validate2020()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t3678);
		itemIO.setItemitem(mandmjaIO.getMandstat());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3678rec.t3678Rec.set(itemIO.getGenarea());
		if (isEQ(t3678rec.gonogoflg,"N")) {
			sv.mandrefErr.set(i014);
			goTo(GotoLabel.checkForErrors2080);
		}
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(e756);
			goTo(GotoLabel.checkForErrors2080);
		}
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)
		&& isNE(clblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankacckeyErr.set(g600);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isGT(clblIO.getCurrfrom(),mandmjaIO.getEffdate())) {
			sv.bankacckeyErr.set(i020);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(clblIO.getCurrto(),ZERO)) {
			if (isLT(clblIO.getCurrto(),mandmjaIO.getEffdate())) {
				sv.bankacckeyErr.set(i020);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.bankacckeyErr.set(f373);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(chdrmjaIO.getBillcurr(),clblIO.getCurrcode())) {
			sv.bankacckeyErr.set(f955);
			goTo(GotoLabel.checkForErrors2080);
		}
		if(ispermission) {
			sv.mrbnk.set(clblIO.getMrbnk()); 			// ILIFE-2476
			sv.bnkbrn.set(clblIO.getBnkbrn()); 			// ILIFE-2476
		}else{
			sv.mrbnk.set(SPACES);
			sv.bnkbrn.set(SPACES);
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
//ILIFE-2476 Starts
		protected void bankbrndsc() {

			if(isEQ(sv.bnkbrn, SPACES)){
				sv.bnkbrn.set("000"+ sv.bankacckey.sub1string(1, 3));
			}
//IBPLIFE-12452 start
			List<Babrpf> lstBabrpf = babrpfDao.getRecordsByBankKey(sv.bnkbrn.toString());
			if(lstBabrpf.isEmpty()){
				sv.bnkbrndesc.set("Branch Name not exist");
			}else{
				for(Babrpf babrpf : lstBabrpf){
					if("Y".equals(babrpf.getAppflag())){
						sv.bnkbrndesc.set(babrpf.getBankdesc());
					}
				}
//IBPLIFE-12452 end
			}
		}

		//ILIFE-2476 Ends
		//ILIFE-2771 Starts
		protected void keepMand(){
			mandIO.setPayrcoy(wsspcomn.fsuco);
			mandIO.setFormat(mandrec);
			mandIO.setMandref(sv.mandref);
			mandIO.setPayrnum(sv.payrnum);
			wsspcomn.chdrCownnum.set(sv.payrnum);
			mandIO.setTransactionDate(varcom.vrcmDate);
			mandIO.setTermid(varcom.vrcmTermid);
			mandIO.setUser(varcom.vrcmUser);
			mandIO.setTransactionTime(varcom.vrcmTime);
			mandIO.setTimesUse(999);
			mandIO.setStatChangeDate(varcom.vrcmMaxDate);
			mandIO.setLastUseDate(varcom.vrcmMaxDate);
			mandIO.setEffdate(varcom.vrcmMaxDate);
			mandIO.setLastUseAmt(ZERO);
			mandIO.setMandAmt(ZERO);
			mandIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, mandIO);
			if (isNE(mandIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(mandIO.getParams());
				fatalError600();
			}
		}
		//ILIFE-2771 Ends
protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isNE(wsaaClntkey,SPACES)) {
			wsspcomn.clntkey.set(wsaaClntkey);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		chdrmjaIO.setMandref(sv.mandref);
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			chdrmjaIO.setParams(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setMandref(sv.mandref);
		payrIO.setBillchnl(wsaaBillchnl);
		payrIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, payrIO);
		if ((isNE(payrIO.getStatuz(),varcom.oK))
		&& (isNE(payrIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
