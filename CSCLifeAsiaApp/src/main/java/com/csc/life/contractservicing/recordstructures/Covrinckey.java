package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:23
 * Description:
 * Copybook name: COVRINCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrinckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrincFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrincKey = new FixedLengthStringData(64).isAPartOf(covrincFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrincChdrcoy = new FixedLengthStringData(1).isAPartOf(covrincKey, 0);
  	public FixedLengthStringData covrincChdrnum = new FixedLengthStringData(8).isAPartOf(covrincKey, 1);
  	public FixedLengthStringData covrincLife = new FixedLengthStringData(2).isAPartOf(covrincKey, 9);
  	public FixedLengthStringData covrincCoverage = new FixedLengthStringData(2).isAPartOf(covrincKey, 11);
  	public FixedLengthStringData covrincRider = new FixedLengthStringData(2).isAPartOf(covrincKey, 13);
  	public PackedDecimalData covrincPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrincKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrincKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrincFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrincFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}