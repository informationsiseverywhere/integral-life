/*
 * File: Zorcompy.java
 * Date: December 3, 2013 4:06:41 AM ICT
 * Author: CSC
 * 
 * Class transformed from ZORCOMPY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.List;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.AgorpslTableDAM;
import com.csc.life.agents.dataaccess.AgorrptTableDAM;
import com.csc.life.agents.dataaccess.dao.AgorpfDAO;
import com.csc.life.agents.dataaccess.model.Agorpf;
import com.csc.life.agents.tablestructures.Th622rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.quipoz.framework.util.AppVars;
/**
 * <pre>
 *
 *(C) Copyright CSC Corporation Limited 1986 - 2000.
 *    All rights reserved. CSC Confidential.
 *
 *REMARKS.
 *   Overriding Commission Creation subroutine.
 *
 *   - First, need to check the Sub Account Code set up on T3695,
 *   if it's T3695- ZRORCOMM <> 'Y', then no overriding commission
 *   is required, just exit program.
 *
 *   - Check on AGORPSL, if found valid record, then check on
 *   Policy Year, check on TH622 to calculate Personal Sale  Overri e
 *   Commission.
 *
 *   - Check on AGORRPT to get valid report to agent, check against
 *   the effective date to ensure it is entitle  to have OR
 *   commission  Until End of File.Check on TH622 to calculate
 *   Override Commission.
 *
 *   - Conduct ACMV posting.
 *
 *   Some variables:
 *   For example,agent 60000463's type is AG,then WSAA-BASIC-TYPE = 'AG'
 *   It's OR details are:
 *
 *   Basic Agent Type/Agent/ Report To Type
 *    AG            60000465      UM
 *    UM            60000467      DM
 *
 *   Then :
 *   When treat first record in AGORRPT:
 *        WSAA-WORKING-TYPE ='AG'   WSAA-REPORTTO-TYPE = 'UM'
 *   When treat first record in AGORRPT:
 *        WSAA-WORKING-TYPE ='UM'   WSAA-REPORTTO-TYPE = 'DM'
 *
 ****************************************************************** ****
 * </pre>
 */
public class Zorcompy extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZORCOMPY";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaWorkingAgnt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaReporttoAgnt = new FixedLengthStringData(8);
	private String wsaaEndAgent = "";
	private ZonedDecimalData wsaaPercent = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaCommAmt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaBasicType = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaReptoType = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaWorkingType = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaTh622Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh622Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh622Key, 0);
	private FixedLengthStringData wsaaTh622BasTyp = new FixedLengthStringData(2).isAPartOf(wsaaTh622Key, 4);
	private FixedLengthStringData wsaaTh622ReptoTyp = new FixedLengthStringData(2).isAPartOf(wsaaTh622Key, 6);
	/* FORMATS */
	private static final String agntlagrec = "AGNTLAGREC";
	/* TABLES */
	private static final String th622 = "TH622";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t3695 = "T3695";
	/* ERRORS */
	private static final String e034 = "E034";
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private AgorpslTableDAM agorpslIO = new AgorpslTableDAM();
	private AgorrptTableDAM agorrptIO = new AgorrptTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T3695rec t3695rec = new T3695rec();
	private Th622rec th622rec = new Th622rec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private AgorpfDAO agorpfDAO = getApplicationContext().getBean("agorpfDAO", AgorpfDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private List<Agorpf> agorpfList;
	private Agorpf agorpf = null;
	private ExternalisedRules er = new ExternalisedRules();

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1580, 
		exit1590
	}

	public Zorcompy() {
		super();
	}



	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		zorlnkrec.zorlnkRec = convertAndSetParam(zorlnkrec.zorlnkRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void mainline100()
	{
		para110();
		exit190();
	}

	protected void para110()
	{
		zorlnkrec.statuz.set(varcom.oK);
		wsaaBatckey.set(zorlnkrec.batchKey);
		readT36951000();
		if (isNE(t3695rec.zrorcomm, "Y")) {
			return ;
		}
		getAgtype1100();
		readT56451200();
		readT56881250();
		wsaaEndAgent = "N";
		wsaaCommAmt.set(ZERO);
		wsaaPercent.set(ZERO);
		wsaaYear.set(ZERO);
		wsaaJrnseq.set(ZERO);
		wsaaWorkingAgnt.set(zorlnkrec.agent);
		checkYear1300();
		/* Calculate personal sale first*/
		agorpslIO.setParams(SPACES);
		agorpslIO.setStatuz(SPACES);
		agorpslIO.setAgntcoy(zorlnkrec.chdrcoy);
		agorpslIO.setAgntnum(zorlnkrec.agent);
		agorpslIO.setReportag(zorlnkrec.agent);
		agorpslIO.setFunction(varcom.readr);
		personalSale1400();
		wsaaPercent.set(ZERO);
		wsaaCommAmt.set(ZERO);
		/* Calculate override commission*/
		//agorrptIO.setParams(SPACES);
		agorpf = new Agorpf();
		agorpf.setAgntcoy(zorlnkrec.chdrcoy.toString());
		agorpf.setAgntnum(zorlnkrec.agent.toString());
		//agorrptIO.setFunction(varcom.begn);
		agorpfList = agorpfDAO.searchAllAgorpfRecurRecord(agorpf);
		if(agorpfList != null && agorpfList.size() > 0)
		{
			for(Agorpf agor : agorpfList)
			{
				beginAgorrpt1500(agor);
			}
		}
		/*while ( !(isEQ(wsaaEndAgent, "Y"))) {
			beginAgorrpt1500();
		}*/

	}

	protected void exit190()
	{
		exitProgram();
	}

	protected void readT36951000()
	{
		start1000();
	}

	protected void start1000()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zorlnkrec.chdrcoy);
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(zorlnkrec.sacstyp);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
	}

	protected void getAgtype1100()
	{
		/*START*/
		agntlagIO.setParams(SPACES);
		agntlagIO.setAgntcoy(zorlnkrec.chdrcoy);
		agntlagIO.setAgntnum(zorlnkrec.agent);
		agntlagIO.setFunction(varcom.readr);
		agntlagIO.setFormat(agntlagrec);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(), varcom.oK)
				&& isNE(agntlagIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		wsaaBasicType.set(agntlagIO.getAgtype());
		/*EXIT*/
	}

	protected void readT56451200()
	{
		start1210();
	}

	protected void start1210()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(zorlnkrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("ZORCOMPY");
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

	protected void readT56881250()
	{
		start1251();
	}

	protected void start1251()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");     //IBPLIFE-1454
		itempf.setItemcoy(zorlnkrec.chdrcoy.toString());
		itempf.setItemtabl(t5688);
		itempf.setItemitem(zorlnkrec.cnttype.toString());
		itempf.setItmfrm(zorlnkrec.effdate.getbigdata());
		itempf.setItmto(zorlnkrec.effdate.getbigdata());
		//itdmIO.setFunction(varcom.begn);
		//SmartFileCode.execute(appVars, itdmIO);
		/*if (isNE(zorlnkrec.chdrcoy, itdmIO.getItemcoy())
		|| isNE(t5688, itdmIO.getItemtabl())
		|| isNE(zorlnkrec.cnttype, itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}*/
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {		
				t5688rec.t5688Rec.set(StringUtil.rawToString(it.getGenarea()));								
			}
		} 
	}

	protected void checkYear1300()
	{
		begin1310();
	}

	protected void begin1310()
	{
		/* Check in which current year is this policy*/
		datcon3rec.intDate1.set(zorlnkrec.effdate);
		datcon3rec.intDate2.set(zorlnkrec.ptdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isLTE(datcon3rec.freqFactor, 1)){
			wsaaYear.set(1);
		}
		else if (isLTE(datcon3rec.freqFactor, 2)){
			wsaaYear.set(2);
		}
		else if (isLTE(datcon3rec.freqFactor, 3)){
			wsaaYear.set(3);
		}
		else if (isLTE(datcon3rec.freqFactor, 4)){
			wsaaYear.set(4);
		}
		else if (isLTE(datcon3rec.freqFactor, 5)){
			wsaaYear.set(5);
		}
		else if (isLTE(datcon3rec.freqFactor, 6)){
			wsaaYear.set(6);
		}
		else if (isLTE(datcon3rec.freqFactor, 7)){
			wsaaYear.set(7);
		}
		else if (isLTE(datcon3rec.freqFactor, 8)){
			wsaaYear.set(8);
		}
		else if (isLTE(datcon3rec.freqFactor, 9)){
			wsaaYear.set(9);
		}
		else if (isGT(datcon3rec.freqFactor, 9)){
			wsaaYear.set(10);
		}
	}

	protected void personalSale1400()
	{
		edit1410();
	}

	protected void edit1410()
	{
		/* Process on the personal sale, when AGNTNUM = REPORTAG*/
		SmartFileCode.execute(appVars, agorpslIO);
		if (isNE(agorpslIO.getStatuz(), varcom.oK)
				&& isNE(agorpslIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agorpslIO.getParams());
			syserrrec.statuz.set(agorpslIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agorpslIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		if ((isGTE(zorlnkrec.effdate, agorpslIO.getEffdate01())
				&& isLTE(zorlnkrec.effdate, agorpslIO.getEffdate02()))) {
			wsaaReporttoAgnt.set(zorlnkrec.agent);
			wsaaBasicType.set(agorpslIO.getAgtype01());
			wsaaWorkingType.set(agorpslIO.getAgtype01());
			wsaaReptoType.set(agorpslIO.getAgtype01());
			orCommCalc1600();
		}
	}

	protected void beginAgorrpt1500(Agorpf agor)
	{

		/* Perfrom this routine until Reporting To agent is done.*/
		/* Only those record within the date range will be process*/
		/*	SmartFileCode.execute(appVars, agorrptIO);
		if (isNE(agorrptIO.getStatuz(), varcom.oK)
		&& isNE(agorrptIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agorrptIO.getParams());
			syserrrec.statuz.set(agorrptIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agorrptIO.getStatuz(), varcom.endp)
		|| isNE(agorrptIO.getAgntcoy(), zorlnkrec.chdrcoy)
		|| isNE(agorrptIO.getAgntnum(), wsaaWorkingAgnt)) {
			wsaaEndAgent = "Y";
			goTo(GotoLabel.exit1590);
		}*/
		/* Check whether the "Report To Agent" is valid*/
		if (isLT(zorlnkrec.effdate, agor.getEffdate01())
				|| isGT(zorlnkrec.effdate, agor.getEffdate02())) {
			return;
		}
		if (isNE(agor.getAgntnum(), agor.getReportag())) {
			wsaaReporttoAgnt.set(agor.getReportag());
			wsaaWorkingType.set(agor.getAgtype01());
			if (isEQ(wsaaBasicType, SPACES)) {
				wsaaBasicType.set(agor.getAgtype01());
			}
			wsaaReptoType.set(agor.getAgtype02());
			orCommCalc1600();
			/* To initialise WSAA-YEAR before next OR calculation.*/
			checkYear1300();
		}
	}

	protected void next1580()
	{
		agorrptIO.setFunction(varcom.nextr);
	}

	protected void orCommCalc1600()
	{
		begin1610();
	}

	protected void begin1610()
	{
		 //ILIFE-8193  start
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable()  && er.isCallExternal("VPMZORCOMPY") && er.isExternalized(zorlnkrec.cnttype.toString(), zorlnkrec.crtable.toString())))	{
		readTh6221700();
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		if (isEQ(wsaaBasicType, wsaaWorkingType)) {
			/* Direct Agent*/
			wsaaPercent.set(th622rec.zryrperc[wsaaYear.toInt()]);
			if (isEQ(th622rec.keyopt01, "P")) {
				compute(wsaaCommAmt, 2).set(div(mult(zorlnkrec.annprem, wsaaPercent), 100));
			}
			else {
				compute(wsaaCommAmt, 2).set(div(mult(zorlnkrec.origamt, wsaaPercent), 100));
			}
		}
		else {
			/* Indirect Agent*/
			wsaaYear.add(10);
			wsaaPercent.set(th622rec.zryrperc[wsaaYear.toInt()]);
			if (isEQ(th622rec.keyopt02, "P")) {
				compute(wsaaCommAmt, 2).set(div(mult(zorlnkrec.annprem, wsaaPercent), 100));
			}
			else {
				compute(wsaaCommAmt, 2).set(div(mult(zorlnkrec.origamt, wsaaPercent), 100));
			}
		}
			}
		else{
			zorlnkrec.nyear.set(wsaaYear);
			zorlnkrec.basicAgentType.set(wsaaBasicType);
			zorlnkrec.workAgentType.set(wsaaWorkingType);
			zorlnkrec.reptAgentType.set(wsaaReptoType);
			callProgram("VPMZORCOMPY",zorlnkrec.zorlnkRec);
			wsaaCommAmt.set(zorlnkrec.commAmt);
		}
	    //ILIFE-8193  end
		if (isNE(wsaaCommAmt, ZERO)) {
			callLifeacmv1800();
		}
	}

	protected void readTh6221700()
	{
		begin1700();
	}

	protected void begin1700()
	{
		wsaaTh622Crtable.set(zorlnkrec.crtable);
		wsaaTh622BasTyp.set(wsaaWorkingType);
		wsaaTh622ReptoTyp.set(wsaaReptoType);
		itdmIO.setDataKey(SPACES);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(zorlnkrec.chdrcoy.toString());
		itempf.setItemtabl(th622);
		itempf.setItemitem(wsaaTh622Key.toString());
		itempf.setItmfrm(zorlnkrec.effdate.getbigdata());
		itempf.setItmto(zorlnkrec.effdate.getbigdata());
		//	itdmIO.setFunction(varcom.begn);
		/*SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), zorlnkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), th622)
		|| isNE(itdmIO.getItemitem(), wsaaTh622Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}*/
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {		
				th622rec.th622Rec.set(StringUtil.rawToString(it.getGenarea()));								
			}
		}
		else
		{
			itempf = new Itempf();
			wsaaTh622Crtable.set("****");
			wsaaTh622BasTyp.set(wsaaWorkingType);
			wsaaTh622ReptoTyp.set(wsaaReptoType);
			//itdmIO.setDataKey(SPACES);
			itempf.setItempfx("IT");
			itempf.setItemcoy(zorlnkrec.chdrcoy.toString());
			itempf.setItemtabl(th622);/* IJTI-1523 */
			itempf.setItemitem(wsaaTh622Key.toString());
			itempf.setItmfrm(zorlnkrec.effdate.getbigdata());
			itempf.setItmto(zorlnkrec.effdate.getbigdata());
			/*itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.statuz.set(itdmIO.getStatuz());
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if (isNE(itdmIO.getItemcoy(), zorlnkrec.chdrcoy)
			|| isNE(itdmIO.getItemtabl(), th622)
			|| isNE(itdmIO.getItemitem(), wsaaTh622Key)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				itdmIO.setItemitem(wsaaTh622Key);
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(e034);
				fatalError600();
			}
			else {*/
			itempfList = itemDAO.findByItemDates(itempf);
			if(itempfList.size() > 0) {
				for (Itempf it : itempfList) {		
					th622rec.th622Rec.set(StringUtil.rawToString(it.getGenarea()));								
				}
			}
		}
	}

	protected void callLifeacmv1800()
	{
		begin1810();
	}

	protected void begin1810()
	{
		/* Set ACMV file*/
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.batccoy.set(zorlnkrec.chdrcoy);
		lifacmvrec1.rldgcoy.set(zorlnkrec.chdrcoy);
		lifacmvrec1.genlcoy.set(zorlnkrec.chdrcoy);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.user.set(0);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.trandesc.set(zorlnkrec.trandesc);
		lifacmvrec1.tranno.set(zorlnkrec.tranno);
		lifacmvrec1.effdate.set(zorlnkrec.effdate);
		lifacmvrec1.rdocnum.set(zorlnkrec.chdrnum);
		lifacmvrec1.crate.set(zorlnkrec.crate);
		lifacmvrec1.genlcur.set(zorlnkrec.genlcur);
		lifacmvrec1.termid.set(zorlnkrec.termid);
		lifacmvrec1.origcurr.set(zorlnkrec.origcurr);
		/* Write 'LA' 'OC'*/
		lifacmvrec1.function.set("PSTW");
		if (isNE(zorlnkrec.clawback, "Y")) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode01);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec1.glcode.set(t5645rec.glmap01);
			lifacmvrec1.glsign.set(t5645rec.sign01);
			lifacmvrec1.contot.set(t5645rec.cnttot01);
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode04);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec1.glcode.set(t5645rec.glmap04);
			lifacmvrec1.glsign.set(t5645rec.sign04);
			lifacmvrec1.contot.set(t5645rec.cnttot04);
		}
		lifacmvrec1.rldgacct.set(wsaaReporttoAgnt);
		lifacmvrec1.origamt.set(wsaaCommAmt);
		lifacmvrec1.acctamt.set(wsaaCommAmt);
		lifacmvrec1.tranref.set(zorlnkrec.tranref);
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		lifacmvrec1.suprflag.set("Y");
		lifacmvrec1.substituteCode[1].set(zorlnkrec.cnttype);
		lifacmvrec1.substituteCode[6].set(zorlnkrec.crtable);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError600();
		}
		/* Write 'LE' 'OE'*/
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			if (isNE(zorlnkrec.clawback, "Y")) {
				lifacmvrec1.sacscode.set(t5645rec.sacscode02);
				lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
				lifacmvrec1.glcode.set(t5645rec.glmap02);
				lifacmvrec1.glsign.set(t5645rec.sign02);
				lifacmvrec1.contot.set(t5645rec.cnttot02);
			}
			else {
				lifacmvrec1.sacscode.set(t5645rec.sacscode05);
				lifacmvrec1.sacstyp.set(t5645rec.sacstype05);
				lifacmvrec1.glcode.set(t5645rec.glmap05);
				lifacmvrec1.glsign.set(t5645rec.sign05);
				lifacmvrec1.contot.set(t5645rec.cnttot05);
			}
		}
		else {
			if (isNE(zorlnkrec.clawback, "Y")) {
				lifacmvrec1.sacscode.set(t5645rec.sacscode03);
				lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
				lifacmvrec1.glcode.set(t5645rec.glmap03);
				lifacmvrec1.glsign.set(t5645rec.sign03);
				lifacmvrec1.contot.set(t5645rec.cnttot03);
			}
			else {
				lifacmvrec1.sacscode.set(t5645rec.sacscode06);
				lifacmvrec1.sacstyp.set(t5645rec.sacstype06);
				lifacmvrec1.glcode.set(t5645rec.glmap06);
				lifacmvrec1.glsign.set(t5645rec.sign06);
				lifacmvrec1.contot.set(t5645rec.cnttot06);
			}
		}
		lifacmvrec1.substituteCode[1].set(zorlnkrec.cnttype);
		lifacmvrec1.substituteCode[6].set(zorlnkrec.crtable);
		lifacmvrec1.rldgacct.set(wsaaReporttoAgnt);
		lifacmvrec1.origamt.set(wsaaCommAmt);
		lifacmvrec1.acctamt.set(wsaaCommAmt);
		lifacmvrec1.tranref.set(zorlnkrec.tranref);
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError600();
		}
	}

	protected void fatalError600()
	{
		start610();
		errorBomb620();
	}

	protected void start610()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void errorBomb620()
	{
		zorlnkrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
	//ILIFE-8193 start
public Zorlnkrec getZorlnkrec() {
	return zorlnkrec;
}

public ItemTableDAM getItemIO() {
	return itemIO;
}



public Varcom getVarcom() {
	return varcom;
}



public FixedLengthStringData getWsaaBasicType() {
	return wsaaBasicType;
}



public FixedLengthStringData getWsaaWorkingType() {
	return wsaaWorkingType;
}



public ZonedDecimalData getWsaaPercent() {
	return wsaaPercent;
}



public Th622rec getTh622rec() {
	return th622rec;
}



public ZonedDecimalData getWsaaYear() {
	return wsaaYear;
}



public ZonedDecimalData getWsaaCommAmt() {
	return wsaaCommAmt;
}



public FixedLengthStringData getWsaaReporttoAgnt() {
	return wsaaReporttoAgnt;
}



public Syserrrec getSyserrrec() {
	return syserrrec;
}



public PackedDecimalData getWsaaJrnseq() {
	return wsaaJrnseq;
}



public T5688rec getT5688rec() {
	return t5688rec;
}



public Lifacmvrec getLifacmvrec1() {
	return lifacmvrec1;
}



public Batckey getWsaaBatckey() {
	return wsaaBatckey;
}



public T5645rec getT5645rec() {
	return t5645rec;
}

public FixedLengthStringData getWsaaReptoType() {
	return wsaaReptoType;
}

//ILIFE-8193  ends
}


