package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6631screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6631ScreenVars sv = (S6631ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6631screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6631ScreenVars screenVars = (S6631ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.durmnth.setClassString("");
		screenVars.autofreq.setClassString("");
		screenVars.crtable01.setClassString("");
		screenVars.crtable02.setClassString("");
		screenVars.crtable03.setClassString("");
		screenVars.crtable04.setClassString("");
		screenVars.crtable05.setClassString("");
		screenVars.crtable06.setClassString("");
		screenVars.crtable07.setClassString("");
		screenVars.crtable08.setClassString("");
		screenVars.crtable09.setClassString("");
		screenVars.crtable10.setClassString("");
		screenVars.crtable11.setClassString("");
		screenVars.crtable12.setClassString("");
		screenVars.crtable13.setClassString("");
		screenVars.crtable14.setClassString("");
		screenVars.crtable15.setClassString("");
		screenVars.crtable16.setClassString("");
		screenVars.crtable17.setClassString("");
		screenVars.crtable18.setClassString("");
		screenVars.crtable19.setClassString("");
		screenVars.crtable20.setClassString("");
		screenVars.crtable21.setClassString("");
		screenVars.crtable22.setClassString("");
		screenVars.crtable23.setClassString("");
		screenVars.crtable24.setClassString("");
		screenVars.payto.setClassString("");
		screenVars.payfrom01.setClassString("");
		screenVars.payfrom02.setClassString("");
		screenVars.payfrom03.setClassString("");
		screenVars.payfrom04.setClassString("");
		screenVars.payfrom05.setClassString("");
		screenVars.payfrom06.setClassString("");
		screenVars.payfrom07.setClassString("");
		screenVars.payfrom08.setClassString("");
		screenVars.payfrom09.setClassString("");
		screenVars.payfrom10.setClassString("");
		screenVars.payfrom11.setClassString("");
		screenVars.payfrom12.setClassString("");
		screenVars.payfrom13.setClassString("");
		screenVars.payfrom14.setClassString("");
		screenVars.payfrom15.setClassString("");
		screenVars.payfrom16.setClassString("");
		screenVars.payfrom17.setClassString("");
		screenVars.payfrom18.setClassString("");
		screenVars.payfrom19.setClassString("");
		screenVars.payfrom20.setClassString("");
		screenVars.payfrom21.setClassString("");
		screenVars.payfrom22.setClassString("");
		screenVars.payfrom23.setClassString("");
		screenVars.payfrom24.setClassString("");
	}

/**
 * Clear all the variables in S6631screen
 */
	public static void clear(VarModel pv) {
		S6631ScreenVars screenVars = (S6631ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.durmnth.clear();
		screenVars.autofreq.clear();
		screenVars.crtable01.clear();
		screenVars.crtable02.clear();
		screenVars.crtable03.clear();
		screenVars.crtable04.clear();
		screenVars.crtable05.clear();
		screenVars.crtable06.clear();
		screenVars.crtable07.clear();
		screenVars.crtable08.clear();
		screenVars.crtable09.clear();
		screenVars.crtable10.clear();
		screenVars.crtable11.clear();
		screenVars.crtable12.clear();
		screenVars.crtable13.clear();
		screenVars.crtable14.clear();
		screenVars.crtable15.clear();
		screenVars.crtable16.clear();
		screenVars.crtable17.clear();
		screenVars.crtable18.clear();
		screenVars.crtable19.clear();
		screenVars.crtable20.clear();
		screenVars.crtable21.clear();
		screenVars.crtable22.clear();
		screenVars.crtable23.clear();
		screenVars.crtable24.clear();
		screenVars.payto.clear();
		screenVars.payfrom01.clear();
		screenVars.payfrom02.clear();
		screenVars.payfrom03.clear();
		screenVars.payfrom04.clear();
		screenVars.payfrom05.clear();
		screenVars.payfrom06.clear();
		screenVars.payfrom07.clear();
		screenVars.payfrom08.clear();
		screenVars.payfrom09.clear();
		screenVars.payfrom10.clear();
		screenVars.payfrom11.clear();
		screenVars.payfrom12.clear();
		screenVars.payfrom13.clear();
		screenVars.payfrom14.clear();
		screenVars.payfrom15.clear();
		screenVars.payfrom16.clear();
		screenVars.payfrom17.clear();
		screenVars.payfrom18.clear();
		screenVars.payfrom19.clear();
		screenVars.payfrom20.clear();
		screenVars.payfrom21.clear();
		screenVars.payfrom22.clear();
		screenVars.payfrom23.clear();
		screenVars.payfrom24.clear();
	}
}
