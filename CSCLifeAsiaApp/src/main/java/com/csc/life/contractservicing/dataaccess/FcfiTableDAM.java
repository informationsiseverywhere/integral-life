package com.csc.life.contractservicing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FcfiTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:59
 * Class transformed from FCFI.LF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FcfiTableDAM extends FcfipfTableDAM {

	public FcfiTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("FCFI");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "EFFDATE, " +
		            "RATEBAS, " +
		            "MEDAMT01, " +
		            "MEDAMT02, " +
		            "ADMAMT01, " +
		            "ADMAMT02, " +
		            "SACSCODE01, " +
		            "SACSCODE02, " +
		            "ADJAMT01, " +
		            "ADJAMT02, " +
		            "SACSTYP01, " +
		            "SACSTYP02, " +
		            "SUSAMT, " +
		            "RUNDTE, " +
		            "ACTVAL, " +
		            "ZTOTAMT, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               tranno,
                               effdate,
                               ratebas,
                               medamt01,
                               medamt02,
                               admamt01,
                               admamt02,
                               sacscode01,
                               sacscode02,
                               adjamt01,
                               adjamt02,
                               sacstyp01,
                               sacstyp02,
                               susamt,
                               reserveUnitsDate,
                               actval,
                               ztotamt,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(244);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(143);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getEffdate().toInternal()
					+ getRatebas().toInternal()
					+ getMedamt01().toInternal()
					+ getMedamt02().toInternal()
					+ getAdmamt01().toInternal()
					+ getAdmamt02().toInternal()
					+ getSacscode01().toInternal()
					+ getSacscode02().toInternal()
					+ getAdjamt01().toInternal()
					+ getAdjamt02().toInternal()
					+ getSacstyp01().toInternal()
					+ getSacstyp02().toInternal()
					+ getSusamt().toInternal()
					+ getReserveUnitsDate().toInternal()
					+ getActval().toInternal()
					+ getZtotamt().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, ratebas);
			what = ExternalData.chop(what, medamt01);
			what = ExternalData.chop(what, medamt02);
			what = ExternalData.chop(what, admamt01);
			what = ExternalData.chop(what, admamt02);
			what = ExternalData.chop(what, sacscode01);
			what = ExternalData.chop(what, sacscode02);
			what = ExternalData.chop(what, adjamt01);
			what = ExternalData.chop(what, adjamt02);
			what = ExternalData.chop(what, sacstyp01);
			what = ExternalData.chop(what, sacstyp02);
			what = ExternalData.chop(what, susamt);
			what = ExternalData.chop(what, reserveUnitsDate);
			what = ExternalData.chop(what, actval);
			what = ExternalData.chop(what, ztotamt);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getRatebas() {
		return ratebas;
	}
	public void setRatebas(Object what) {
		ratebas.set(what);
	}	
	public PackedDecimalData getMedamt01() {
		return medamt01;
	}
	public void setMedamt01(Object what) {
		setMedamt01(what, false);
	}
	public void setMedamt01(Object what, boolean rounded) {
		if (rounded)
			medamt01.setRounded(what);
		else
			medamt01.set(what);
	}	
	public PackedDecimalData getMedamt02() {
		return medamt02;
	}
	public void setMedamt02(Object what) {
		setMedamt02(what, false);
	}
	public void setMedamt02(Object what, boolean rounded) {
		if (rounded)
			medamt02.setRounded(what);
		else
			medamt02.set(what);
	}	
	public PackedDecimalData getAdmamt01() {
		return admamt01;
	}
	public void setAdmamt01(Object what) {
		setAdmamt01(what, false);
	}
	public void setAdmamt01(Object what, boolean rounded) {
		if (rounded)
			admamt01.setRounded(what);
		else
			admamt01.set(what);
	}	
	public PackedDecimalData getAdmamt02() {
		return admamt02;
	}
	public void setAdmamt02(Object what) {
		setAdmamt02(what, false);
	}
	public void setAdmamt02(Object what, boolean rounded) {
		if (rounded)
			admamt02.setRounded(what);
		else
			admamt02.set(what);
	}	
	public FixedLengthStringData getSacscode01() {
		return sacscode01;
	}
	public void setSacscode01(Object what) {
		sacscode01.set(what);
	}	
	public FixedLengthStringData getSacscode02() {
		return sacscode02;
	}
	public void setSacscode02(Object what) {
		sacscode02.set(what);
	}	
	public PackedDecimalData getAdjamt01() {
		return adjamt01;
	}
	public void setAdjamt01(Object what) {
		setAdjamt01(what, false);
	}
	public void setAdjamt01(Object what, boolean rounded) {
		if (rounded)
			adjamt01.setRounded(what);
		else
			adjamt01.set(what);
	}	
	public PackedDecimalData getAdjamt02() {
		return adjamt02;
	}
	public void setAdjamt02(Object what) {
		setAdjamt02(what, false);
	}
	public void setAdjamt02(Object what, boolean rounded) {
		if (rounded)
			adjamt02.setRounded(what);
		else
			adjamt02.set(what);
	}	
	public FixedLengthStringData getSacstyp01() {
		return sacstyp01;
	}
	public void setSacstyp01(Object what) {
		sacstyp01.set(what);
	}	
	public FixedLengthStringData getSacstyp02() {
		return sacstyp02;
	}
	public void setSacstyp02(Object what) {
		sacstyp02.set(what);
	}	
	public PackedDecimalData getSusamt() {
		return susamt;
	}
	public void setSusamt(Object what) {
		setSusamt(what, false);
	}
	public void setSusamt(Object what, boolean rounded) {
		if (rounded)
			susamt.setRounded(what);
		else
			susamt.set(what);
	}	
	public PackedDecimalData getReserveUnitsDate() {
		return reserveUnitsDate;
	}
	public void setReserveUnitsDate(Object what) {
		setReserveUnitsDate(what, false);
	}
	public void setReserveUnitsDate(Object what, boolean rounded) {
		if (rounded)
			reserveUnitsDate.setRounded(what);
		else
			reserveUnitsDate.set(what);
	}	
	public PackedDecimalData getActval() {
		return actval;
	}
	public void setActval(Object what) {
		setActval(what, false);
	}
	public void setActval(Object what, boolean rounded) {
		if (rounded)
			actval.setRounded(what);
		else
			actval.set(what);
	}	
	public PackedDecimalData getZtotamt() {
		return ztotamt;
	}
	public void setZtotamt(Object what) {
		setZtotamt(what, false);
	}
	public void setZtotamt(Object what, boolean rounded) {
		if (rounded)
			ztotamt.setRounded(what);
		else
			ztotamt.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSacstyps() {
		return new FixedLengthStringData(sacstyp01.toInternal()
										+ sacstyp02.toInternal());
	}
	public void setSacstyps(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSacstyps().getLength()).init(obj);
	
		what = ExternalData.chop(what, sacstyp01);
		what = ExternalData.chop(what, sacstyp02);
	}
	public FixedLengthStringData getSacstyp(BaseData indx) {
		return getSacstyp(indx.toInt());
	}
	public FixedLengthStringData getSacstyp(int indx) {

		switch (indx) {
			case 1 : return sacstyp01;
			case 2 : return sacstyp02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSacstyp(BaseData indx, Object what) {
		setSacstyp(indx.toInt(), what);
	}
	public void setSacstyp(int indx, Object what) {

		switch (indx) {
			case 1 : setSacstyp01(what);
					 break;
			case 2 : setSacstyp02(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSacscodes() {
		return new FixedLengthStringData(sacscode01.toInternal()
										+ sacscode02.toInternal());
	}
	public void setSacscodes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSacscodes().getLength()).init(obj);
	
		what = ExternalData.chop(what, sacscode01);
		what = ExternalData.chop(what, sacscode02);
	}
	public FixedLengthStringData getSacscode(BaseData indx) {
		return getSacscode(indx.toInt());
	}
	public FixedLengthStringData getSacscode(int indx) {

		switch (indx) {
			case 1 : return sacscode01;
			case 2 : return sacscode02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSacscode(BaseData indx, Object what) {
		setSacscode(indx.toInt(), what);
	}
	public void setSacscode(int indx, Object what) {

		switch (indx) {
			case 1 : setSacscode01(what);
					 break;
			case 2 : setSacscode02(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getMedamts() {
		return new FixedLengthStringData(medamt01.toInternal()
										+ medamt02.toInternal());
	}
	public void setMedamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMedamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, medamt01);
		what = ExternalData.chop(what, medamt02);
	}
	public PackedDecimalData getMedamt(BaseData indx) {
		return getMedamt(indx.toInt());
	}
	public PackedDecimalData getMedamt(int indx) {

		switch (indx) {
			case 1 : return medamt01;
			case 2 : return medamt02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMedamt(BaseData indx, Object what) {
		setMedamt(indx, what, false);
	}
	public void setMedamt(BaseData indx, Object what, boolean rounded) {
		setMedamt(indx.toInt(), what, rounded);
	}
	public void setMedamt(int indx, Object what) {
		setMedamt(indx, what, false);
	}
	public void setMedamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setMedamt01(what, rounded);
					 break;
			case 2 : setMedamt02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAdmamts() {
		return new FixedLengthStringData(admamt01.toInternal()
										+ admamt02.toInternal());
	}
	public void setAdmamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAdmamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, admamt01);
		what = ExternalData.chop(what, admamt02);
	}
	public PackedDecimalData getAdmamt(BaseData indx) {
		return getAdmamt(indx.toInt());
	}
	public PackedDecimalData getAdmamt(int indx) {

		switch (indx) {
			case 1 : return admamt01;
			case 2 : return admamt02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAdmamt(BaseData indx, Object what) {
		setAdmamt(indx, what, false);
	}
	public void setAdmamt(BaseData indx, Object what, boolean rounded) {
		setAdmamt(indx.toInt(), what, rounded);
	}
	public void setAdmamt(int indx, Object what) {
		setAdmamt(indx, what, false);
	}
	public void setAdmamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAdmamt01(what, rounded);
					 break;
			case 2 : setAdmamt02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAdjamts() {
		return new FixedLengthStringData(adjamt01.toInternal()
										+ adjamt02.toInternal());
	}
	public void setAdjamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAdjamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, adjamt01);
		what = ExternalData.chop(what, adjamt02);
	}
	public PackedDecimalData getAdjamt(BaseData indx) {
		return getAdjamt(indx.toInt());
	}
	public PackedDecimalData getAdjamt(int indx) {

		switch (indx) {
			case 1 : return adjamt01;
			case 2 : return adjamt02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAdjamt(BaseData indx, Object what) {
		setAdjamt(indx, what, false);
	}
	public void setAdjamt(BaseData indx, Object what, boolean rounded) {
		setAdjamt(indx.toInt(), what, rounded);
	}
	public void setAdjamt(int indx, Object what) {
		setAdjamt(indx, what, false);
	}
	public void setAdjamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAdjamt01(what, rounded);
					 break;
			case 2 : setAdjamt02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		effdate.clear();
		ratebas.clear();
		medamt01.clear();
		medamt02.clear();
		admamt01.clear();
		admamt02.clear();
		sacscode01.clear();
		sacscode02.clear();
		adjamt01.clear();
		adjamt02.clear();
		sacstyp01.clear();
		sacstyp02.clear();
		susamt.clear();
		reserveUnitsDate.clear();
		actval.clear();
		ztotamt.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}