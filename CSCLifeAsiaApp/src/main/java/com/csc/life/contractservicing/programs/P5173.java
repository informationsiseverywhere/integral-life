/*
 * File: P5173.java
 * Date: 30 August 2009 0:17:13
 * Author: Quipoz Limited
 * 
 * Class transformed from P5173.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.CwfdTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Errmesg;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* P5173 - Windforward AT Submission Module.
* -----------------------------------------
*
* This program submits the AT module for Windforward. It has
* no screen and is performed following Windforward Confirmation.
*
*    Program Details
*    ---------------
*
* Retrieve the Contract Header record.
*
* Validation Loop
* ---------------
*
*  For each Contract Windforward record (CWFD) for the contract:
*
*         Read the Schedule Definition file (BSCD) to get the
*         transaction code for the schedule.
*
*         Check that the submittor is sanctioned to the schedule
*         transaction code by calling SANCTN.
*
*         If the submittor is not sanctioned to the schedule,
*         set the error to F085, 'Not Sanctioned to Sched.' and
*         leave the program.
*
*         If the schedule has to be unique in the system:
*
*         Check through the Submitted Schedules file (BSSC) for
*         uncompleted schedules (STATUZ not = '90')
*
*         If any exist, set the error to E213, 'Previous Job
*         Incomplete' and leave the program.
*
* Call the AT submission module to submit P5173AT, passing
* the Contract number as the primary key.
*
***********************************************************************
* </pre>
*/
public class P5173 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5173");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaValid = "";

	private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(37);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaMsgarea, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsgVariable = new FixedLengthStringData(10).isAPartOf(wsaaMsgarea, 1);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaMsgarea, 11, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsg = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 12);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(201);
	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaAcctmonth = new PackedDecimalData(2, 0).isAPartOf(wsaaTransactionRec, 22);
	private PackedDecimalData wsaaAcctyear = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 24);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 27);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 29).setUnsigned();
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 37);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 41);
	private FixedLengthStringData filler3 = new FixedLengthStringData(156).isAPartOf(wsaaTransactionRec, 45, FILLER).init(SPACES);
		/* ERRORS */
	private String e011 = "E011";
	private String e045 = "E045";
	private String e213 = "E213";
	private String f084 = "F084";
	private String f085 = "F085";
	private String f910 = "F910";
		/* FORMATS */
	private String chdrenqrec = "CHDRENQREC";
	private String cwfdrec = "CWFDREC";
	private String bscdrec = "BSCDREC";
	private String bsscrec = "BSSCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Atreqrec atreqrec = new Atreqrec();
		/*Schedule Definition - Multi Thread Batch*/
	private BscdTableDAM bscdIO = new BscdTableDAM();
		/*Schedule File - Multi-Thread Batch*/
	private BsscTableDAM bsscIO = new BsscTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Windforward Logical*/
	private CwfdTableDAM cwfdIO = new CwfdTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Errmesgrec errmesgrec = new Errmesgrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit3190, 
		exit3290, 
		exit3590
	}

	public P5173() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("RLSE");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		wsaaTranid.set(wsspcomn.tranid);
		wsaaAcctmonth.set(wsspcomn.acctmonth);
		wsaaAcctyear.set(wsspcomn.acctyear);
		wsaaBranch.set(wsspcomn.branch);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		wsaaValid = "Y";
		cwfdIO.setChdrcoy(chdrenqIO.getChdrcoy());
		cwfdIO.setChdrnum(chdrenqIO.getChdrnum());
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		cwfdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		cwfdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		cwfdIO.setFormat(cwfdrec);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		if (isNE(cwfdIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(cwfdIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			cwfdIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		while ( !(isEQ(cwfdIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaValid,"N"))) {
			checkSchedule3100();
			if (isEQ(wsaaValid,"Y")) {
				readNextCwfd3400();
			}
		}
		
		if (isEQ(wsaaValid,"Y")) {
			submitAt3500();
		}
	}

protected void checkSchedule3100()
	{
		try {
			read3210();
		}
		catch (GOTOException e){
		}
	}

protected void read3210()
	{
		bscdIO.setScheduleName(cwfdIO.getScheduleName());
		bscdIO.setFunction(varcom.readr);
		bscdIO.setFormat(bscdrec);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(),varcom.oK)
		&& isNE(bscdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(bscdIO.getStatuz());
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		if (isEQ(bscdIO.getStatuz(),varcom.mrnf)) {
			errmesgrec.eror.set(f084);
			wsaaMsgVariable.set(cwfdIO.getScheduleName());
			formatError3300();
			goTo(GotoLabel.exit3190);
		}
		sanctnrec.sanctnRec.set(SPACES);
		sanctnrec.company.set("*");
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.function.set("TNCD");
		sanctnrec.transcd.set(bscdIO.getAuthCode());
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isNE(sanctnrec.statuz,varcom.oK)
		&& isNE(sanctnrec.statuz,e011)
		&& isNE(sanctnrec.statuz,e045)) {
			syserrrec.statuz.set(bscdIO.getStatuz());
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		if (isEQ(sanctnrec.statuz,e011)
		|| isEQ(sanctnrec.statuz,e045)) {
			errmesgrec.eror.set(f085);
			wsaaMsgVariable.set(cwfdIO.getScheduleName());
			formatError3300();
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(bscdIO.getUniqueInSys(),"N")) {
			goTo(GotoLabel.exit3190);
		}
		bsscIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bsscIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bsscIO.setFitKeysSearch("BSCHEDNAM");
		bsscIO.setScheduleName(bscdIO.getScheduleName());
		bsscIO.setScheduleNumber(ZERO);
		bsscIO.setFormat(bsscrec);
		while ( !(isEQ(bsscIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaValid,"N"))) {
			checkBssc3200();
		}
		
	}

protected void checkBssc3200()
	{
		try {
			call3210();
		}
		catch (GOTOException e){
		}
	}

protected void call3210()
	{
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(),varcom.oK)
		&& isNE(bsscIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(bsscIO.getStatuz());
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		if (isNE(bsscIO.getScheduleName(),bscdIO.getScheduleName())) {
			bsscIO.setStatuz(varcom.endp);
		}
		if (isEQ(bsscIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3290);
		}
		if (isNE(bsscIO.getScheduleStatus(),"90")) {
			errmesgrec.eror.set(e213);
			wsaaMsgVariable.set(cwfdIO.getScheduleName());
			formatError3300();
			goTo(GotoLabel.exit3290);
		}
		bsscIO.setFunction(varcom.nextr);
	}

protected void formatError3300()
	{
		call3310();
	}

protected void call3310()
	{
		errmesgrec.company.set(wsspcomn.company);
		errmesgrec.language.set(wsspcomn.language);
		errmesgrec.erorProg.set(wsaaProg);
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		if (isNE(errmesgrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(errmesgrec.statuz);
			syserrrec.params.set(errmesgrec.errmesgRec);
			fatalError600();
		}
		wsaaMsg.set(errmesgrec.errmesg[1]);
		wsspcomn.msgarea.set(wsaaMsgarea);
		wsaaValid = "N";
	}

protected void readNextCwfd3400()
	{
		/*READ*/
		cwfdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)
		&& isNE(cwfdIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		if (isNE(cwfdIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(cwfdIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			cwfdIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void submitAt3500()
	{
		try {
			submit3510();
		}
		catch (GOTOException e){
		}
	}

protected void submit3510()
	{
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrenqIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			errmesgrec.eror.set(f910);
			wsaaMsgVariable.set(chdrenqIO.getChdrnum());
			formatError3300();
			goTo(GotoLabel.exit3590);
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5173AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrenqIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(atreqrec.statuz);
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(wsaaValid,"Y")) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.programPtr.subtract(1);
			chdrenqIO.setFunction("KEEPS");
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
		}
		/*EXIT*/
	}
}
