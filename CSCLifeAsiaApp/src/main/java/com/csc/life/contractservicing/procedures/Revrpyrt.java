/*
 * File: Revrpyrt.java
 * Date: 30 August 2009 2:11:26
 * Author: Quipoz Limited
 * 
 * Class transformed from REVRPYRT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.RegprevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE PAYMENTS IN REVIEW AND PAYMENTS TERMINATING
*        ---------------------------------------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse the results of Payments in
*   Review or Payments Terminating processing. It will reverse
*   records in the following files - Contract Header (CHDR),
*   Payer (PAYR) and Regular Payment (REGP).
*   Should any generic prcessing be required, then it will be
*    initiated via standard T5671 processing.
*
* Processing.
* -----------
*
* Reverse Contract Header ( CHDRs ):
*
*  Read the Contract Header file (CHDRPF) using the logical
*  view CHDRRGP with a key of company and contract number
*  from linkage.
*
*  For the record found, check it is validflag '1'.
*    If not, then ERROR.
*  Delete validflag '1' CHDR record.
*  Read Next CHDR record which is validflag '2'.
*  Reset validflag to '1'.
*  REWRiTe CHDR record.
*
* Reverse Payer ( PAYRs ):
*
*  Read the Payer file (PAYRPF) using the logical view
*  PAYRLIF with a key of company and contract number
*  from linkage.
*
*  For the record found, check it is validflag '1'.
*    If not, then ERROR.
*  Delete validflag '1' PAYR record.
*  Read Next PAYR record which is validflag '2'.
*  Reset validflag to '1'.
*  REWRiTe PAYR record.
*
* Reverse Regular Payment ( REGPs ):
*
*  Read the Regular Payment (REGPPF) using the logical view
*  REGPREV with a key of company, contract number, Life,
*  Coverage, Rider, regular payment number and tranno.
*
*  For each record found:
*   Check it is validflag '1'.
*    If not, then Read NEXT.
*   Check tranno on REGP record matches tranno we are trying
*    to reverse.
*     If tranno matches,
*        delete REGP record
*        read NEXT REGP record with same key
*          If REGP record matches key and validflag = '2'
*            reset validflag = '1' and REWRiTe REGP
*          end-if
*     else
*        read NEXT REGP record
*     end-if
*
* Generic processing....
* reversal of Unit-Linked/Traditional parts of the Contract:
*
*  Loop through all the Coverages/Riders associated with the
*   Contract we are processing and call any Generic processing
*   subroutines that are specified in T5671.
*
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing
*
*****************************************************************
* </pre>
*/
public class Revrpyrt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "REVRPYRT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaRgpynum = new PackedDecimalData(5, 0).init(ZERO);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);
	private String t5671 = "T5671";
		/* FORMATS */
	private String chdrrgprec = "CHDRRGPREC";
	private String covrenqrec = "COVRENQREC";
	private String itemrec = "ITEMREC   ";
	private String payrlifrec = "PAYRLIFREC";
	private String regprevrec = "REGPREVREC";
		/*Regular Payments View.*/
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Greversrec greversrec = new Greversrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Payr logical file*/
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
		/*Regualr Payments Reversals logical file*/
	private RegprevTableDAM regprevIO = new RegprevTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit5190, 
		exit5290, 
		exit16190, 
		exit6290, 
		exit9490, 
		exit9990
	}

	public Revrpyrt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processChdrs3000();
		processPayrs4000();
		processRegps5000();
		genericProcessing6000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		wsaaRgpynum.set(ZERO);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError9000();
		}
	}

protected void processChdrs3000()
	{
		start3000();
	}

protected void start3000()
	{
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrcoy(reverserec.company);
		chdrrgpIO.setChdrnum(reverserec.chdrnum);
		chdrrgpIO.setFormat(chdrrgprec);
		chdrrgpIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrrgpIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(),varcom.oK)
		&& isNE(chdrrgpIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			syserrrec.statuz.set(chdrrgpIO.getStatuz());
			databaseError9500();
		}
		if (isNE(chdrrgpIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrrgpIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrrgpIO.getValidflag(),"1")
		|| isEQ(chdrrgpIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			syserrrec.statuz.set(chdrrgpIO.getStatuz());
			databaseError9500();
		}
		chdrrgpIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			syserrrec.statuz.set(chdrrgpIO.getStatuz());
			databaseError9500();
		}
		chdrrgpIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(),varcom.oK)
		&& isNE(chdrrgpIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			syserrrec.statuz.set(chdrrgpIO.getStatuz());
			databaseError9500();
		}
		if (isNE(chdrrgpIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrrgpIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrrgpIO.getValidflag(),"2")
		|| isEQ(chdrrgpIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			syserrrec.statuz.set(chdrrgpIO.getStatuz());
			databaseError9500();
		}
		chdrrgpIO.setValidflag("1");
		chdrrgpIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			syserrrec.statuz.set(chdrrgpIO.getStatuz());
			databaseError9500();
		}
	}

protected void processPayrs4000()
	{
		start4000();
	}

protected void start4000()
	{
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(ZERO);
		payrlifIO.setFormat(payrlifrec);
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)
		&& isNE(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError9500();
		}
		if (isNE(payrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(payrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(),"1")
		|| isEQ(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError9500();
		}
		payrlifIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError9500();
		}
		payrlifIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)
		&& isNE(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError9500();
		}
		if (isNE(payrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(payrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(),"2")
		|| isEQ(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError9500();
		}
		payrlifIO.setValidflag("1");
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError9500();
		}
	}

protected void processRegps5000()
	{
		start5000();
	}

protected void start5000()
	{
		regprevIO.setParams(SPACES);
		regprevIO.setChdrcoy(reverserec.company);
		regprevIO.setChdrnum(reverserec.chdrnum);
		regprevIO.setLife(SPACES);
		regprevIO.setCoverage(SPACES);
		regprevIO.setRider(SPACES);
		regprevIO.setRgpynum(ZERO);
		regprevIO.setTranno(9999);
		regprevIO.setFormat(regprevrec);
		regprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regprevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regprevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)
		&& isNE(regprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company,regprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,regprevIO.getChdrnum())
		|| isNE(regprevIO.getValidflag(),"1")
		|| isEQ(regprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		while ( !(isEQ(regprevIO.getStatuz(),varcom.endp))) {
			reverseRegps5100();
		}
		
	}

protected void reverseRegps5100()
	{
		try {
			start5100();
			getNextRegp5180();
		}
		catch (GOTOException e){
		}
	}

protected void start5100()
	{
		if (isEQ(regprevIO.getTranno(),reverserec.tranno)
		&& isEQ(regprevIO.getValidflag(),"1")) {
			wsaaLife.set(regprevIO.getLife());
			wsaaCoverage.set(regprevIO.getCoverage());
			wsaaRider.set(regprevIO.getRider());
			wsaaRgpynum.set(regprevIO.getRgpynum());
			deletRewrtRegps5200();
			goTo(GotoLabel.exit5190);
		}
	}

protected void getNextRegp5180()
	{
		regprevIO.setFunction(varcom.nextr);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)
		&& isNE(regprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company,regprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,regprevIO.getChdrnum())
		|| isEQ(regprevIO.getStatuz(),varcom.endp)) {
			regprevIO.setStatuz(varcom.endp);
		}
	}

protected void deletRewrtRegps5200()
	{
		try {
			start5200();
		}
		catch (GOTOException e){
		}
	}

protected void start5200()
	{
		regprevIO.setFunction(varcom.readh);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		regprevIO.setFunction(varcom.delet);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		regprevIO.setFunction(varcom.nextr);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)
		&& isNE(regprevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company,regprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,regprevIO.getChdrnum())
		|| isEQ(regprevIO.getStatuz(),varcom.endp)) {
			regprevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5290);
		}
		if (isNE(wsaaLife,regprevIO.getLife())
		|| isNE(wsaaCoverage,regprevIO.getCoverage())
		|| isNE(wsaaRider,regprevIO.getRider())
		|| isNE(wsaaRgpynum,regprevIO.getRgpynum())) {
			goTo(GotoLabel.exit5290);
		}
		regprevIO.setFunction(varcom.readh);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		regprevIO.setValidflag("1");
		regprevIO.setFunction(varcom.rewrt);
		regprevIO.setFormat(regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
	}

protected void genericProcessing6000()
	{
		/*START*/
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs6100();
		}
		
		/*EXIT*/
	}

protected void processCovrs6100()
	{
		try {
			start6100();
		}
		catch (GOTOException e){
		}
	}

protected void start6100()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(covrenqIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit16190);
		}
		if (isNE(reverserec.company,covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,covrenqIO.getChdrnum())) {
			covrenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit16190);
		}
		if (isNE(covrenqIO.getLife(),wsaaLife)
		|| isNE(covrenqIO.getCoverage(),wsaaCoverage)
		|| isNE(covrenqIO.getRider(),wsaaRider)
		|| isNE(covrenqIO.getPlanSuffix(),wsaaPlanSuffix)) {
			genericSubr6200();
			wsaaLife.set(covrenqIO.getLife());
			wsaaCoverage.set(covrenqIO.getCoverage());
			wsaaRider.set(covrenqIO.getRider());
			wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void genericSubr6200()
	{
		try {
			start6200();
		}
		catch (GOTOException e){
		}
	}

protected void start6200()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			goTo(GotoLabel.exit6290);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(ZERO);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callTrevsubs6300();
		}
	}

protected void callTrevsubs6300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz,varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError9000();
			}
		}
		/*EXIT*/
	}

protected void systemError9000()
	{
		try {
			start9000();
		}
		catch (GOTOException e){
		}
		finally{
			exit9490();
		}
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		try {
			start9500();
		}
		catch (GOTOException e){
		}
		finally{
			exit9990();
		}
	}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9990);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
