/*
 * File: P5081.java
 * Date: 30 August 2009 0:04:40
 * Author: Quipoz Limited
 *
 * Class transformed from P5081.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.financials.tablestructures.T3676rec;
import com.csc.fsu.clients.tablestructures.T3584rec;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.S5081ScreenVars;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnenqTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.procedures.Antisocl;
import com.csc.life.newbusiness.recordstructures.Antisoclkey;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.clients.tablestructures.T3584rec;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                    CONTRACT OWNERS
*
*This is a minor alteration,  to  change  the  Contract Owners
*(that is owner or joint owner or both) on the contract header
*using details captured from screen/program P5081.
*
* Initialise
* ----------
*
* Read CHDRMNA (RETRV)  in  order to obtain the contract header
* information.
*
* Read  the 'DESCIO' in  order to obtain the description of the
* risk and premium statuses.
*
* In order to format  the  required  names  the  client details
* record is read and  the  relevant copybook for formatting the
* names must be included in the program.
*
* Read  the  agent  details (AGNTLNB) and format the name using
* the copybooks.
*
* Validation
* ----------
*
* If KILL is requested, then skip the validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
* Owners validation.
*
*      - if the  owner  number  (ex screen) is equal to spaces,
*           then this is an error and exit from the validation.
*
*      - if the  joint-owner  number  (ex  screen)  is equal to
*           spaces, then it is O.K to proceed.
*
*      - if a  joint  number  is  entered, then read the client
*           details  and  format the name and return the output
*           to the screen.
*
*      - if CALC is pressed, then redisplay.
*
*Updating
*--------
*
* Updating occurs  with  the  updating  of  the contract header
* record, the deletion of a client role record, the addition of
* a new client role and the creation of a PTRN record.
*
* If the 'KILL' function key was pressed, skip the updating.
*
* CLIENT ROLE UPDATES
*
*      - if the owner, role is 'OW' (ex screen) is equal to the
*           owner (CHDR), then no change occurs.
*
*      - if the  owner  (ex  screen)  is not equal to the owner
*           (CHDR),  then  delete the client role record, add a
*           new  client  role  record  and  update the contract
*           header.
*
*      - if the  joint owner, role is 'JO' (ex screen) is equal
*           to  the  joint-owner  (CHDR), then no change occurs
*           and exit from the update  procedure  (only  if
*           owners are the same also).
*
*      - if the  joint-owner  is  not  equal to the joint-owner
*           (CHDR), then
*
*           - Delete  the  Role,  Add a new Role and update the
*                contract headers joint-owner number.
*
*      - if the  Despatch  number or the Payer number are equal
*           to  spaces,  delete  /  add  client  roles  for the
*           Despatch ('DA') or Payer ('PY') or both.
*
*           - also  update  the  CHDRMNA  record  with  the new
*             Despatch and/or Payer client numbers.
*
*  CONTRACT HEADER UPDATE
*
*      - if there were  no  changes,  then  do  not  update the
*           contract header record.
*
*      - if the screen entered owner number is not equal to the
*           owner  (CHDR),  update  the  owner  prefix with the
*           client   prefix,   the   owner   company  with  the
*           WSSP-FSUCO  and  the  owner  number  with the owner
*           number (ex screen).
*
*      - if the  screen  entered  joint-owner  number is spaces
*           then move spaces to the contract header joint-owner
*           update the
*           joint-owner  number with the joint-owner number (ex
*           screen).
*
*      - rewrite (WRITS) the contract header record.
*      
*
* GENERAL HOUSEKEEPING
*
*      - Write a PTRN record (if updating the CHDR):
*
*           - contract key from contract header
*           - transaction number from WSSP
*           - transaction effective date from VRCM-DATE
*           - batch key information WSSP batchkey
*
*      - Update the header by calling BATCUP with a function of
*           WRITS and the following parameters:
*
*           - transaction count equals 1
*           - all other amounts equal to zero
*           - batch key from WSSP
*
*      - Release contract SFTLOCK.
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5081 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5081");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private FixedLengthStringData wsaaOrigReasoncd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaOrigResndesc = new FixedLengthStringData(50);
	//TICKET #ILIFE-1191 STARTS
	private ZonedDecimalData wsaaLastTranno  = new ZonedDecimalData(5, 0); 
	private FixedLengthStringData wsaaCownnum = new FixedLengthStringData(8);
	//TICKET #ILIFE-1191 ENDS
	private String wsaaWriteLetter = "N";

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 1).setUnsigned();

		/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
		/* ERRORS */
	private static final String e040 = "E040";
	private static final String e304 = "E304";
	private static final String h358 = "H358";
	private static final String rlas = "RLAS";
	private static final String f782 = "F782";
	private static final String rrgz = "RRGZ";//ILIFE-7277
	private static final String rg43 = "RG43";//ILIFE-7277
	private static final String rrh5 = "RRH5";//ILIFE-7277
	private static final String rrgy = "RRGY";//ILIFE-7277

		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5688 = "T5688";
	private static final String t5500 = "T5500";
	private static final String tr384 = "TR384";
	private boolean multOwnerFlag = false;
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	protected ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private ResnTableDAM resnIO = new ResnTableDAM();
	private ResnenqTableDAM resnenqIO = new ResnenqTableDAM();
	private Batckey wsaaBatcKey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Wssplife wssplife = new Wssplife();
	protected S5081ScreenVars sv = getLScreenVars(); //ScreenProgram.getScreenVars( S5081ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private Clntpf clntpf= new Clntpf();
	private Lifepf  lifepf = new Lifepf();	
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private List<String> chkList=null;
	private Set<String> chkDuplicate=null;
	List<String> clrrToRemoveList = null;
	List<String> clrrToUpdateList = null;
	boolean isUpdate=false;
	boolean isRemove=false;

	private static final String h017 = "H017";
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private static final String t622 = "T622";
    private TablesInner tablesInner = new TablesInner();
    private Itempf itempf = null;
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private T5688rec t5688rec = new T5688rec();
    private T5677rec t5677rec = new T5677rec();
    private FixedLengthStringData wsaaT5677Key = new FixedLengthStringData(8);
    private FixedLengthStringData wsaaT5677Tranno = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 0);
    private FixedLengthStringData wsaaT5677FollowUp = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 4);
	boolean CSMIN003Permission  = false;

	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private boolean liferelflag = false;
	private static final String feaConfigLifeRel= "NBPRP087";
	
	 private FixedLengthStringData wsaarelationwithlife = new FixedLengthStringData(4);
	 private FixedLengthStringData wsaarelationwithlifejoint = new FixedLengthStringData(4);
	 private static final String rgh2 = "RGH2";
	 private static final String f658 = "F658";
	 private static final String t3584 = "T3584";
	 private T3584rec t3584rec = new T3584rec();
	 private ErrorsInner errorsInner = new ErrorsInner();
	 private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	 private FixedLengthStringData wsaalife = new FixedLengthStringData(4);
	//ILJ-49 Starts
	 private boolean cntDteFlag = false;
	 private String cntDteFeature = "NBPRP113";
	 //ILJ-49 End 
	 
	 private Antisoclkey antisoclkey = new Antisoclkey();
	 private boolean NBPRP117permission = false;
	
	 
	 protected S5081ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5081ScreenVars.class);
	}
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		reascd2050,
		checkForErrors2080,
		checkDesp3015,
		updJowner3020,
		addJownerRole3025,
		keepAndWritsChdr3030,
		relsfl3070
	}

	public P5081() {
		super();
		screenVars = sv;
		new ScreenModel("S5081", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
		readLifePf();
		CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
	}
protected void readLifePf()
{
	if(liferelflag){
	List <Lifepf>lifepfList=null;
	lifepfList = lifepfDAO.getLifeList(wsspcomn.company.trim(),chdrmnaIO.getChdrnum().trim() );
	if(lifepfList!=null && lifepfList.size()!=0)
	{
		lifepf=lifepfList.get(0);
		sv.relationwithlife.set(lifepf.getRelation());
		sv.relationwithlifejoint.set(lifepf.getLiferel());
		wsaarelationwithlife.set(lifepf.getRelation());
		wsaarelationwithlifejoint.set(lifepf.getLiferel());
   }
	}
	
}

protected void initialise1010()
	{
	
		wsaaBatcKey.set(wsspcomn.batchkey);
		/*    IF  WSAA-TODAY NOT = ZEROS                                   */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		
		
		liferelflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), feaConfigLifeRel, appVars, "IT");
		NBPRP117permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP117", appVars, "IT"); 
	
	}

protected void blankOutFields1020()
	{
	//ILIFE-7277-start
	multOwnerFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP078", appVars, "IT");
	if(multOwnerFlag){
		sv.multipleOwnerFlag.set("N");
		}
	else{
		sv.multipleOwnerFlag.set("Y");
	} //ILIFE-7277-end
	sv.dataArea.set(SPACES);
	//ILJ-49 Starts
			cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
					if(!cntDteFlag) {
						sv.occdateOut[varcom.nd.toInt()].set("Y");
						}
	//ILJ-49 End
		
		/* Retrieve contract fields from I/O module*/
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		/*  Load all fields from the Contract Header to the Screen*/
		//ILIFE-7277-start
		sv.cownnum.set(chdrmnaIO.getCownnum());
		sv.cownnum2.set(chdrmnaIO.getCownnum2());
		sv.cownnum3.set(chdrmnaIO.getCownnum3());
		sv.cownnum4.set(chdrmnaIO.getCownnum4());
		sv.cownnum5.set(chdrmnaIO.getCownnum5());
		sv.ownername2.set(getOwnerName(sv.cownnum2.toString().trim()));
		sv.ownername3.set(getOwnerName(sv.cownnum3.toString().trim()));
		sv.ownername4.set(getOwnerName(sv.cownnum4.toString().trim()));
		sv.ownername5.set(getOwnerName(sv.cownnum5.toString().trim()));
		//ILIFE-7277-end
		wsaaCownnum.set(chdrmnaIO.getCownnum()); //TICKET #ILIFE-1191
		sv.jownnum.set(chdrmnaIO.getJownnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		sv.billfreq.set(chdrmnaIO.getBillfreq());
		sv.mop.set(chdrmnaIO.getBillchnl());
		sv.agntnum.set(chdrmnaIO.getAgntnum());
		/*  Look up premium status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmnaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/*  Look up Risk status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*   Look up all other descriptions*/
		/*       - owner name*/
		/*       - joint owner name*/
		/*       - agent name*/
		if (isNE(sv.cownnum, SPACES)) {
			formatClientName1700();
		}
		if (isNE(sv.jownnum, SPACES)) {
			/*    PERFORM 1800-FORMAT-JOINT-OWNER                          */
			formatJointOwner1800();
		}
		if (isNE(sv.agntnum, SPACES)) {
			formatAgentName1900();
		}
		/* MOVE CHDRMNA-COWNNUM     TO S5081-COWNFR.                    */
		//ILIFE-7277-start
		sv.clntwin.set(chdrmnaIO.getCownnum());
		sv.clntwin2.set(sv.cownnum2);
		sv.clntwin3.set(sv.cownnum3);
		sv.clntwin4.set(sv.cownnum4);
		sv.clntwin5.set(sv.cownnum5);
		/* MOVE S5081-OWNERNAME     TO S5081-OWNERNEWNM.                */
		sv.clntname.set(sv.ownername);
		sv.clntname2.set(getOwnerName(sv.clntwin2.toString()));
		sv.clntname3.set(getOwnerName(sv.clntwin3.toString()));
		sv.clntname4.set(getOwnerName(sv.clntwin4.toString()));
		sv.clntname5.set(getOwnerName(sv.clntwin5.toString()));
		//ILIFE-7277-end
		if (isEQ(chdrmnaIO.getJownnum(), SPACES)) {
			/*    MOVE SPACES              TO S5081-JOWNERNAME              */
			sv.clientname.set(SPACES);
			sv.clientnum.set(SPACES);
			/*****                                S5081-JOWNER                  */
		}
		else {
			/*    MOVE CHDRMNA-JOWNNUM     TO S5081-JOWNER                  */
			/*    MOVE S5081-CLIENTNAME    TO S5081-JOWNERNAME.             */
			sv.clientnum.set(chdrmnaIO.getJownnum());
			sv.clientname.set(sv.jownername);
		}
		formatResnDetails5000();
	}

	/**
	* <pre>
	*      RETRIEVE OWNER DETAILS AND SET SCREEN
	* </pre>
	*/
protected void retrieve1550()
	{
		retrieve1560();
	}

protected void retrieve1560()
	{
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    Set screen fields*/
		/* MOVE SPACES                 TO S5081-OWNERNEWNM.             */
		sv.clntname.set(SPACES);
		/* If the client details are not found then an error is*/
		/* given.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			/*    MOVE E040                TO S5081-COWNFR-ERR              */
			sv.clntwinErr.set(e040);
			return ;
		}
		plainname();
		/* MOVE WSSP-LONGCONFNAME      TO S5081-OWNERNEWNM.             */
		sv.clntname.set(wsspcomn.longconfname);
	}

	/**
	* <pre>
	*      RETRIEVE JOINT OWNER DETAILS AND SET SCREEN
	* </pre>
	*/
protected void getJownerName1600()
	{
		getname1610();
	}

protected void getname1610()
	{
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    Set screen fields*/
		/* MOVE SPACES                 TO S5081-JOWNERNAME.             */
		sv.clientname.set(SPACES);
		/* If the client details are not found then an error is*/
		/* given.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			/*    MOVE E040                TO S5081-JOWNER-ERR              */
			sv.clientnumErr.set(e040);
			return ;
		}
		plainname();
		/* MOVE WSSP-LONGCONFNAME      TO S5081-JOWNERNAME.             */
		sv.clientname.set(wsspcomn.longconfname);
		/*    Check Death Date of new joint owner < Orig Comm Date         */
		if (isNE(cltsIO.getCltdod(), varcom.vrcmMaxDate)
		&& isGT(chdrmnaIO.getOccdate(), cltsIO.getCltdod()) || isLTE(cltsIO.getCltdod(), wsaaToday)) {
			sv.clientnumErr.set(f782);
		}
	}

protected void formatClientName1700()
	{
		readClientRecord1710();
	}

protected void readClientRecord1710()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.cownnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void formatJointOwner1800()
	{
		readClientRecord1810();
	}

protected void readClientRecord1810()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.jownnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.jownnumErr.set(e304);
			/*     MOVE SPACES             TO S5081-JOWNNAME                */
			sv.jownername.set(SPACES);
		}
		else {
			plainname();
			/*     MOVE WSSP-LONGCONFNAME  TO S5081-JOWNNAME.               */
			sv.jownername.set(wsspcomn.longconfname);
		}
	}

protected void formatAgentName1900()
	{
		readAgent1910();
	}

protected void readAgent1910()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntnum);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)
		&& isNE(aglflnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		sv.agentname.set(SPACES);
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		
		
		
		if (liferelflag) {
			if (isEQ(wsspcomn.flag, "I")) {
				sv.relationwithlifeOut[varcom.pr.toInt()].set("Y");
				sv.relationwithlifejointOut[varcom.pr.toInt()].set("Y");
			}
		} else {
			sv.relationwithlifeOut[varcom.nd.toInt()].set("Y");
			sv.relationwithlifejointOut[varcom.nd.toInt()].set("Y");
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
				case reascd2050:
					reascd2050();
				case checkForErrors2080:
					checkForErrors2080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		if(CSMIN003Permission){
			//validate follow up status if R/W then continue
			if (isEQ(wsaaBatcKey.batcBatctrcde, t622)) {
				chekIfRequired3410();
				readDefaultsTable3420();
				validateFollowUpStatus2a00();
			}
		}
		/*    CALL 'S5081IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5081-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isNE(chdrmnaIO.getCownnum(), SPACES) || isNE(chdrmnaIO.getCownnum2(), SPACES) || isNE(chdrmnaIO.getCownnum3(), SPACES) || isNE(chdrmnaIO.getCownnum4(), SPACES) || isNE(chdrmnaIO.getCownnum5(), SPACES)) {
			/*   IF  S5081-COWNFR          = SPACES                         */
			if (isEQ(sv.clntwin, SPACES)){
				sv.clntwin.set(chdrmnaIO.getCownnum());
			}
			/*         GO TO 2090-EXIT.                                     */

		}
		/* IF  S5081-COWNFR            = SPACES                         */
		/*     MOVE E186               TO S5081-COWNFR-ERR              */
		/* IF  S5081-CLNTWIN           = SPACES                 <LA5065>*/
		/*     MOVE E186               TO S5081-CLNTWIN-ERR     <LA5065>*/
		/*     GO TO 2080-CHECK-FOR-ERRORS.                             */
		cltsIO.setDataArea(SPACES);
		/* MOVE S5081-COWNFR           TO CLTS-CLNTNUM.                 */
		cltsIO.setClntnum(sv.clntwin);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		retrieve1550();
		
		//ILJ
	if(NBPRP117permission)
	{
			
		clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntnum(sv.clntwin.toString());
		clntpf = clntpfDAO.getCltsRecordByKey(clntpf); 
		
		initialize(antisoclkey.antisocialKey);
		antisoclkey.kjSName.set(clntpf.getLsurname());
		antisoclkey.kjGName.set(clntpf.getLgivname());
		antisoclkey.clntype.set(clntpf.getClttype());

		
		callProgram(Antisocl.class, antisoclkey.antisocialKey);
		
		if (isNE(antisoclkey.statuz, varcom.oK)) {
			
			scrnparams.errorCode.set(antisoclkey.statuz);
		
		}
	}
		//ILJ
		
		sv.clntname2.set(getOwnerName(sv.clntwin2.toString()));
		sv.clntname3.set(getOwnerName(sv.clntwin3.toString()));
		sv.clntname4.set(getOwnerName(sv.clntwin4.toString()));
		sv.clntname5.set(getOwnerName(sv.clntwin5.toString()));
		/*    Check Death Date of new owner < Orig Comm Date               */
		if (isNE(cltsIO.getCltdod(), varcom.vrcmMaxDate)
		&& isGT(chdrmnaIO.getOccdate(), cltsIO.getCltdod()) || isLTE(cltsIO.getCltdod(), wsaaToday)) {
			sv.clntwinErr.set(f782);
		}
		//ILIFE-7277-start
		if(isEQ(sv.clntwin2, SPACES)){
			sv.clntwin2.set(SPACES);
			sv.clntname2.set(SPACES);
		}
		if(isEQ(sv.clntwin3, SPACES)){
			sv.clntwin3.set(SPACES);
			sv.clntname3.set(SPACES);
		}
		if(isEQ(sv.clntwin4, SPACES)){
			sv.clntwin4.set(SPACES);
			sv.clntname4.set(SPACES);
		}
		if(isEQ(sv.clntwin5, SPACES)){
			sv.clntwin5.set(SPACES);
			sv.clntname5.set(SPACES);
		}
		if (((isEQ(sv.clntwin, sv.cownnum)) && ((isEQ(sv.clntwin2, sv.cownnum2)) && (isEQ(sv.clntwin3, sv.cownnum3)) &&(isEQ(sv.clntwin4, sv.cownnum4))&&(isEQ(sv.clntwin5, sv.cownnum5)))
		&& isEQ(sv.clientnum, sv.jownnum) &&(isEQ(sv.relationwithlife,wsaarelationwithlife) && isEQ(sv.relationwithlifejoint,wsaarelationwithlifejoint)))
		|| (((isEQ(sv.clntwin, SPACES)))
		&& isEQ(sv.clientnum, SPACES))) {
			sv.clntwinErr.set(rlas);
			sv.clientnumErr.set(rlas);
			goTo(GotoLabel.checkForErrors2080);
		}
		
		chkList=new LinkedList<String>();
			chkList.add(sv.clntwin.toString().trim());
			chkList.add(sv.clntwin2.toString().trim());
			chkList.add(sv.clntwin3.toString().trim());
			chkList.add(sv.clntwin4.toString().trim());
			chkList.add(sv.clntwin5.toString().trim());
		if(multOwnerFlag){
			if(isNE(sv.clientnum, SPACES)){
			sv.ownernameErr.set(rrh5);
			goTo(GotoLabel.checkForErrors2080);
			}else{
				validateClient(chkList);
			}
		}
		if(liferelflag){
			if(isEQ(sv.clntwin,SPACES) && isNE(sv.relationwithlife,SPACES))
			{
				sv.relationwithlifeErr.set(rgh2);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.checkForErrors2080);
			}

			if(isEQ(sv.relationwithlife,SPACES) && isNE(sv.clntwin,SPACES) )
			{			
				sv.relationwithlifeErr.set(f658);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		//ILIFE-7277-end
		/* IF  S5081-JOWNER          = SPACES                           */
		if (isEQ(sv.clientnum, SPACES)) {
			sv.clientname.set(SPACES);
			if(liferelflag){
				if(isNE(sv.relationwithlifejoint,SPACES))
				{				
					sv.relationwithlifejointErr.set(rgh2);
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.checkForErrors2080);
					
				}
			}
			goTo(GotoLabel.reascd2050);
		}
		
		if(liferelflag){
			if( isEQ(sv.relationwithlifejoint,SPACES) && isNE(sv.clientnum,SPACES) )
			{
				
				sv.relationwithlifejointErr.set(f658);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.checkForErrors2080);
			}
			wsaalife.set(SPACES);
			if(isNE(sv.relationwithlife,wsaarelationwithlife)){
				wsaalife.set(sv.relationwithlife);
			}
			if(isNE(sv.relationwithlifejoint,wsaarelationwithlifejoint)){
				wsaalife.set(sv.relationwithlifejoint);
			}
			if(isNE(wsaalife,SPACES))
				readTable3584();
				
		}
		
		
		/* IF  S5081-JOWNER          = S5081-COWNFR                     */
		if (isEQ(sv.clientnum, sv.clntwin)) {
			/*     MOVE H358            TO S5081-JOWNER-ERR                 */
			sv.clientnumErr.set(h358);
			goTo(GotoLabel.checkForErrors2080);
		}
			
	
		cltsIO.setDataArea(SPACES);
		/* MOVE S5081-JOWNER           TO CLTS-CLNTNUM.                 */
		cltsIO.setClntnum(sv.clientnum);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		getJownerName1600();
	}

    private void readTable3584() 
{
    	
    	itempf = new Itempf();
    	itempf.setItempfx("IT");
    	itempf.setItemcoy(wsspcomn.fsuco.toString());
    	itempf.setItemtabl(t3584);/* IJTI-1523 */
    	itempf.setItemitem(wsaalife.trim()) ;
    	itempf = itempfDAO.getItempfRecord(itempf);      	
    	
    	if (itempf != null) {
    		t3584rec.t3584Rec.set(StringUtil.rawToString(itempf.getGenarea()));
    	}
    	else {
    		t3584rec.t3584Rec.set(SPACES);	
		}
		
		if(isNE(t3584rec.insurown,SPACES) && isEQ(t3584rec.insurown,"Y") )
		{
			scrnparams.errorCode.set(errorsInner.rrsv); 
			return;
		}
    	
	}

	protected void validateFollowUpStatus2a00() {
        //In T5688, default is none, then break
        if (isEQ(t5688rec.defFupMeth, SPACES)) {
            return;
        }
		if (isEQ(t5677rec.fupcdess, SPACES)) {
			return;
		}
        boolean showOutstandingErr = false;
		List<String> flupList = new ArrayList<>();
        List<Fluppf> fluprevIOList = fluppfDAO.searchFlupRecordByChdrNum(chdrmnaIO.getChdrcoy().toString(),chdrmnaIO.getChdrnum().toString());
        if(fluprevIOList!=null&&!fluprevIOList.isEmpty()){
            for (Fluppf fluppf: fluprevIOList) {
                for (FixedLengthStringData fixedLengthStringData : t5677rec.fupcdes) {
					if(fixedLengthStringData !=null && !fixedLengthStringData.trim().equals("")) {
						if (isEQ(fluppf.getFupCde(),fixedLengthStringData.toString().trim())) {
							flupList.add(fluppf.getFupCde());
                            if(isEQ(fluppf.getFupSts(),"R") || isEQ(fluppf.getFupSts(),"W")){
                                showOutstandingErr = false;
                            }else{
                                showOutstandingErr = true;
                                break;
                            }
                        }
                    }
                }
				if(showOutstandingErr){
					break;
				}
            }
            if(flupList.isEmpty()){
            	return;
			}
            if(showOutstandingErr){
                scrnparams.errorCode.set(h017);
                wsspcomn.edterror.set("Y");
                goTo(GotoLabel.exit2090);
            }
        }
    }

    protected void chekIfRequired3410()
    {
        itempf = new Itempf();
        itempf.setItempfx("IT");
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tablesInner.t5688.toString());
        itempf.setItemitem(chdrmnaIO.getCnttype().toString().trim());
        itempf = itemDAO.getItemRecordByItemkey(itempf);
		if(itempf != null){
			t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
    }

    protected void readDefaultsTable3420()
    {
        if (isEQ(t5688rec.defFupMeth, SPACES)) {
            return;
        }
        wsaaT5677Tranno.set(wsaaBatcKey.batcBatctrcde);
        wsaaT5677FollowUp.set(t5688rec.defFupMeth);
        itempf = new Itempf();
        itempf.setItempfx("IT");
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tablesInner.t5677.toString());
        itempf.setItemitem(wsaaT5677Key.toString().trim());
        itempf = itemDAO.getItemRecordByItemkey(itempf);
        if(itempf != null){
            t5677rec.t5677Rec.set(StringUtil.rawToString(itempf.getGenarea()));
        }
    }

protected void reascd2050()
	{
		/* If the reason code is equal to spaces, skip the read of      */
		/* T5500.                                                       */
		if (isEQ(sv.reasoncd, SPACES)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5500);
		descIO.setDescitem(sv.reasoncd);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/* IF S5081-RESNDESC           = SPACES                         */
		/*    IF DESC-STATUZ              = O-K                         */
		/*       MOVE DESC-LONGDESC       TO S5081-RESNDESC             */
		/*    ELSE                                                      */
		/*        IF  S5081-REASONCD   NOT = SPACES                     */
		/*            MOVE '?'            TO S5081-RESNDESC.            */
		if (isEQ(descIO.getStatuz(), varcom.oK)
		&& isEQ(sv.resndesc, SPACES)) {
			sv.resndesc.set(descIO.getLongdesc());
		}
	}

protected void checkForErrors2080()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateDatabase3010();
				case checkDesp3015:
					checkDesp3015();
				case updJowner3020:
					updJowner3020();
				case addJownerRole3025:
					addJownerRole3025();
				case keepAndWritsChdr3030:
					keepAndWritsChdr3030();
					writeReasncd3050();
					callBldenrl3060();
				case relsfl3070:
					relsfl3070();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateRecordForRelationWithLifeAndJoint()
{		
	if (liferelflag) 
	{
	lifepfDAO.updateLifeandJointRecord(chdrmnaIO.getChdrcoy().trim(),chdrmnaIO.getChdrnum().trim(),sv.relationwithlife.trim(),sv.relationwithlifejoint.trim());
    }
}




protected void updateDatabase3010()
	{
		/* First read Payer file to get policy number                      */
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmnaIO.getChdrnum());
		payrIO.setPayrseqno(ZERO);
		payrIO.setFunction(varcom.begn);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Read the client role file to get the payer number.              */
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		/*   No updating if no changes made.*/
		/* IF  S5081-COWNFR            = SPACES                         */
		if (isEQ(sv.clntwin, SPACES)) {
			goTo(GotoLabel.relsfl3070);
		}
		/* IF  WSSP-FSUCO              = CHDRMNA-ASGNCOY AND            */
		/*     S5081-COWNFR            = CHDRMNA-COWNNUM AND            */
		/*     S5081-CLNTWIN           = CHDRMNA-COWNNUM AND    <A05168>*/
		/*     S5081-JOWNER            = CHDRMNA-JOWNNUM                */
		/*     S5081-CLIENTNUM         = CHDRMNA-JOWNNUM        <A05168>*/
		/*     GO TO 3070-RELSFL.                                       */
		if ((isEQ(sv.clntwin, chdrmnaIO.getCownnum()) && ((isEQ(sv.clntwin2, chdrmnaIO.getCownnum2())) && (isEQ(sv.clntwin3, chdrmnaIO.getCownnum3())) &&(isEQ(sv.clntwin4, chdrmnaIO.getCownnum4()))&&(isEQ(sv.clntwin5, chdrmnaIO.getCownnum5()))) )
		&& isEQ(sv.clientnum, chdrmnaIO.getJownnum())
		&& isEQ(sv.reasoncd, wsaaOrigReasoncd)
		&& isEQ(sv.resndesc, wsaaOrigResndesc) && isEQ(sv.relationwithlife,wsaarelationwithlife) && isEQ(sv.relationwithlifejoint,wsaarelationwithlifejoint)) {
			goTo(GotoLabel.relsfl3070);
		}
		/* IF  S5081-COWNFR            = CHDRMNA-COWNNUM                */
		/*     IF  S5081-JOWNER NOT    = CHDRMNA-JOWNNUM                */
		if (isEQ(sv.clntwin, chdrmnaIO.getCownnum())) {
			if (isNE(sv.clientnum, chdrmnaIO.getJownnum())) {
				goTo(GotoLabel.updJowner3020);
			}
		}
		/* IF  S5081-COWNFR            = CHDRMNA-COWNNUM                */
		if (isEQ(sv.clntwin, chdrmnaIO.getCownnum())) {
			/*     GO TO 3070-RELSFL.                                       */
			goTo(GotoLabel.keepAndWritsChdr3030);
		}
		/*   If changing Contract Owner then We must check the old PAYOR   */
		/*   to see if it is blank and if so move the old OWNER to the     */
		/*   PAYOR field as changing the PAYOR is a MAJOR ALTERATION.      */
		/*IF CHDRMNA-PAYRNUM          =  SPACES                   <005>*/
		/*   MOVE CHDRMNA-COWNNUM     TO CHDRMNA-PAYRNUM.         <005>*/
		if (isEQ(payrIO.getChdrnum(), SPACES)) {
			payrIO.setChdrnum(chdrmnaIO.getCownnum());
		}
		/*   If changing Contract Owner then We must check the old DESPATCH*/
		/*   ADDRESS to see if it is BLANK and if so move the OLD OWNER to */
		/*   the PAYOR field as changing the PAYOR is another MINOR        */
		/*   ALTERATION.                                                   */
		/* IF CHDRMNA-DESPNUM          =  SPACES                <LA3389>*/
		/*    MOVE CHDRMNA-COWNPFX     TO CHDRMNA-DESPPFX       <LA3389>*/
		/*    MOVE CHDRMNA-COWNCOY     TO CHDRMNA-DESPCOY       <LA3389>*/
		/*    MOVE CHDRMNA-COWNNUM     TO CHDRMNA-DESPNUM.      <LA3389>*/
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*   Read current Client Owner Role record prior to delete.*/
		/*MOVE CHDRMNA-COWNPFX        TO CLRR-CLNTPFX                  */
		/*MOVE CHDRMNA-COWNCOY        TO CLRR-CLNTCOY                  */
		/*MOVE CHDRMNA-COWNNUM        TO CLRR-CLNTNUM.                 */
		/*MOVE READH                  TO CLRR-FUNCTION.                */
		/*MOVE 'OW'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*CALL 'CLRRIO' USING CLRR-PARAMS.                             */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*Delete previous Owner record from Client Role File.           */
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*MOVE CHDRMNA-COWNNUM        TO CLRR-CLNTNUM.                 */
		/*MOVE CHDRMNA-COWNPFX        TO CLRR-CLNTPFX.                 */
		/*MOVE CHDRMNA-COWNCOY        TO CLRR-CLNTCOY.                 */
		/*MOVE 'OW'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*MOVE DELET                  TO CLRR-FUNCTION.                */
		/*CALL 'CLRRIO' USING CLRR-PARAMS.                             */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*    Remove owner role                                            */
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set(chdrmnaIO.getCownpfx());
		cltrelnrec.clntcoy.set(chdrmnaIO.getCowncoy());
		cltrelnrec.clntnum.set(chdrmnaIO.getCownnum());
		cltrelnrec.function.set("REM  ");
		cltrelnrec.clrrrole.set("OW");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.syserrStatuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*Add new role.                                                */
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*Use entered new contract owner.                              */
		/*MOVE 'CN'                   TO CLRR-CLNTPFX                  */
		/* MOVE S5081-COWNFR            TO CLRR-CLNTNUM            <003>*/
		/*MOVE S5081-CLNTWIN           TO CLRR-CLNTNUM            <003>*/
		/*MOVE WSSP-FSUCO             TO CLRR-CLNTCOY.                 */
		/*MOVE CLRRREC                TO CLRR-FORMAT.                  */
		/*MOVE WRITR                  TO CLRR-FUNCTION.                */
		/*MOVE 'OW'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*CALL 'CLRRIO'  USING CLRR-PARAMS.                            */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/* AND                                                         */
		/*   CLRR-STATUZ              NOT = DUPR                       */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*IF  CHDRMNA-PAYRNUM    NOT   = SPACES                        */
		/*    GO TO  3015-CHECK-DESP.                                  */
		/*    Add new role for owner                                       */
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntnum.set(sv.clntwin);
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.function.set("ADD  ");
		cltrelnrec.clrrrole.set("OW");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.syserrStatuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*IF  CHDRMNA-PAYRNUM    NOT  =  SPACES                   <008>*/
		/*    GO TO  3015-CHECK-DESP.                             <008>*/
		if (isNE(payrIO.getChdrnum(), SPACES)) {
			goTo(GotoLabel.checkDesp3015);
		}
		/*Read current Client Payer Role record prior to delete.        */
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*MOVE CHDRMNA-COWNPFX        TO CLRR-CLNTPFX                  */
		/*MOVE CHDRMNA-COWNCOY        TO CLRR-CLNTCOY                  */
		/*MOVE CHDRMNA-COWNNUM        TO CLRR-CLNTNUM.                 */
		/*MOVE READH                  TO CLRR-FUNCTION.                */
		/*MOVE 'PY'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*CALL 'CLRRIO' USING CLRR-PARAMS.                             */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*Delete previous Payer record from Client Role File.           */
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*MOVE CHDRMNA-COWNNUM        TO CLRR-CLNTNUM.                 */
		/*MOVE CHDRMNA-COWNPFX        TO CLRR-CLNTPFX.                 */
		/*MOVE CHDRMNA-COWNCOY        TO CLRR-CLNTCOY.                 */
		/*MOVE 'PY'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*MOVE DELET                  TO CLRR-FUNCTION.                */
		/*CALL 'CLRRIO' USING CLRR-PARAMS.                             */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*    Remove payer role                                            */
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set(chdrmnaIO.getCownpfx());
		cltrelnrec.clntcoy.set(chdrmnaIO.getCowncoy());
		/*MOVE CHDRMNA-COWNNUM        TO CLRN-CLNTNUM.            <008>*/
		cltrelnrec.clntnum.set(clrfIO.getClntnum());
		cltrelnrec.function.set("REM  ");
		cltrelnrec.clrrrole.set("PY");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		/*MOVE CHDRMNA-CHDRNUM        TO CLRN-FORENUM.            <008>*/
		cltrelnrec.forenum.set(wsaaForenum);
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.syserrStatuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*Add new role for payor.                                      */
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*Use entered new contract owner.                              */
		/*MOVE 'CN'                   TO CLRR-CLNTPFX                  */
		/* MOVE S5081-COWNFR           TO CLRR-CLNTNUM.            <003>*/
		/*MOVE S5081-CLNTWIN          TO CLRR-CLNTNUM.            <003>*/
		/*MOVE WSSP-FSUCO             TO CLRR-CLNTCOY.                 */
		/*MOVE CLRRREC                TO CLRR-FORMAT.                  */
		/*MOVE WRITR                  TO CLRR-FUNCTION.                */
		/*MOVE 'PY'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*CALL 'CLRRIO'  USING CLRR-PARAMS.                            */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/* AND                                                         */
		/*   CLRR-STATUZ              NOT = DUPR                       */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*    Add payer role                                               */
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntnum.set(sv.clntwin);
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.function.set("ADD  ");
		cltrelnrec.clrrrole.set("PY");
		cltrelnrec.forepfx.set("CH");
		/*MOVE CHDRMNA-CHDRCOY        TO CLRN-FORECOY.            <008>*/
		cltrelnrec.forecoy.set(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		/*MOVE CHDRMNA-CHDRNUM        TO CLRN-FORENUM.            <008>*/
		cltrelnrec.forenum.set(wsaaForenum);
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.syserrStatuz.set(cltrelnrec.statuz);
			fatalError600();
		}

	}

protected void checkDesp3015()
	{
		if (isNE(chdrmnaIO.getDespnum(), SPACES)) {
			goTo(GotoLabel.updJowner3020);
		}
		/*   Read current Despatch Payer Role record prior to delete.*/
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*MOVE CHDRMNA-COWNPFX        TO CLRR-CLNTPFX                  */
		/*MOVE CHDRMNA-COWNCOY        TO CLRR-CLNTCOY                  */
		/*MOVE CHDRMNA-COWNNUM        TO CLRR-CLNTNUM.                 */
		/*MOVE READH                  TO CLRR-FUNCTION.                */
		/*MOVE 'DA'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*CALL 'CLRRIO' USING CLRR-PARAMS.                             */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*Delete previous Payer record from Client Role File.           */
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*MOVE CHDRMNA-COWNNUM        TO CLRR-CLNTNUM.                 */
		/*MOVE CHDRMNA-COWNPFX        TO CLRR-CLNTPFX.                 */
		/*MOVE CHDRMNA-COWNCOY        TO CLRR-CLNTCOY.                 */
		/*MOVE 'DA'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*MOVE DELET                  TO CLRR-FUNCTION.                */
		/*CALL 'CLRRIO' USING CLRR-PARAMS.                             */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*    Remove despatch role                                         */
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set(chdrmnaIO.getCownpfx());
		cltrelnrec.clntcoy.set(chdrmnaIO.getCowncoy());
		cltrelnrec.clntnum.set(chdrmnaIO.getCownnum());
		cltrelnrec.function.set("REM  ");
		cltrelnrec.clrrrole.set("DA");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.syserrStatuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*Add new role for Despatch.                                   */
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*Use entered new contract owner.                              */
		/*MOVE 'CN'                   TO CLRR-CLNTPFX.                 */
		/*MOVE S5081-COWNFR           TO CLRR-CLNTNUM.            <003>*/
		/*MOVE S5081-CLNTWIN          TO CLRR-CLNTNUM.            <003>*/
		/*MOVE WSSP-FSUCO             TO CLRR-CLNTCOY.                 */
		/*MOVE CLRRREC                TO CLRR-FORMAT.                  */
		/*MOVE WRITR                  TO CLRR-FUNCTION.                */
		/*MOVE 'DA'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*CALL 'CLRRIO'  USING CLRR-PARAMS.                            */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/* AND                                                         */
		/*   CLRR-STATUZ              NOT = DUPR                       */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*    Add new role for despatch                                    */
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntnum.set(sv.clntwin);
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.function.set("ADD  ");
		cltrelnrec.clrrrole.set("DA");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.syserrStatuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

protected void updJowner3020()
	{
		/* IF  S5081-JOWNER            = CHDRMNA-JOWNNUM AND            */
		/*     S5081-COWNFR      NOT   = CHDRMNA-COWNNUM                */
		if (isEQ(sv.clientnum, chdrmnaIO.getJownnum())
		&& isNE(sv.clntwin, chdrmnaIO.getCownnum())) {
			goTo(GotoLabel.keepAndWritsChdr3030);
		}
		/* IF  S5081-JOWNER            = CHDRMNA-JOWNNUM                */
		if (isEQ(sv.clientnum, chdrmnaIO.getJownnum())) {
			goTo(GotoLabel.relsfl3070);
		}
		if (isEQ(chdrmnaIO.getJownnum(), SPACES)) {
			goTo(GotoLabel.addJownerRole3025);
		}
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*Read Client Role record prior to delete                       */
		/*MOVE CHDRMNA-COWNPFX        TO CLRR-CLNTPFX                  */
		/*MOVE CHDRMNA-COWNCOY        TO CLRR-CLNTCOY                  */
		/*MOVE CHDRMNA-JOWNNUM        TO CLRR-CLNTNUM.                 */
		/*MOVE READH                  TO CLRR-FUNCTION.                */
		/*MOVE 'JO'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*CALL 'CLRRIO' USING CLRR-PARAMS.                             */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*Delete the previous Joint Owner from the Client Role file.    */
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*MOVE CHDRMNA-JOWNNUM        TO CLRR-CLNTNUM.                 */
		/*MOVE CHDRMNA-COWNPFX        TO CLRR-CLNTPFX.                 */
		/*MOVE CHDRMNA-COWNCOY        TO CLRR-CLNTCOY.                 */
		/*MOVE 'JO'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*MOVE DELET                  TO CLRR-FUNCTION.                */
		/*CALL 'CLRRIO' USING CLRR-PARAMS.                             */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*    Remove Joint owner role                                      */
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set(chdrmnaIO.getCownpfx());
		cltrelnrec.clntcoy.set(chdrmnaIO.getCowncoy());
		/* MOVE CHDRMNA-COWNNUM        TO CLRN-CLNTNUM.            <008>*/
		cltrelnrec.clntnum.set(chdrmnaIO.getJownnum());
		cltrelnrec.function.set("REM  ");
		cltrelnrec.clrrrole.set("JO");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.syserrStatuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/* IF   S5081-JOWNER            = SPACES                        */
		if (isEQ(sv.clientnum, SPACES)) {
			goTo(GotoLabel.keepAndWritsChdr3030);
		}
	}

	/**
	* <pre>
	*    Add new role.
	* </pre>
	*/
protected void addJownerRole3025()
	{
		/*MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*Use entered new contract owner.                              */
		/*MOVE 'CN'                   TO CLRR-CLNTPFX                  */
		/*MOVE S5081-JOWNER           TO CLRR-CLNTNUM             <004>*/
		/*MOVE S5081-CLIENTNUM        TO CLRR-CLNTNUM             <004>*/
		/*MOVE WSSP-FSUCO             TO CLRR-CLNTCOY.                 */
		/*MOVE CLRRREC                TO CLRR-FORMAT.                  */
		/*MOVE WRITR                  TO CLRR-FUNCTION.                */
		/*MOVE 'JO'                   TO CLRR-CLRRROLE.                */
		/*MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*CALL 'CLRRIO'  USING CLRR-PARAMS.                            */
		/*IF CLRR-STATUZ              NOT = O-K                        */
		/* AND                                                         */
		/*   CLRR-STATUZ              NOT = DUPR                       */
		/*    MOVE CLRR-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*    Add joint owner role                                         */
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set("CN");
		/* MOVE S5081-CLNTWIN          TO CLRN-CLNTNUM.            <008>*/
		cltrelnrec.clntnum.set(sv.clientnum);
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.function.set("ADD  ");
		cltrelnrec.clrrrole.set("JO");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.syserrStatuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

protected void keepAndWritsChdr3030()
	{
		/* Set the variables after rewrite the previous CHDR record with   */
		/* validflag 2.                                                    */
		/* IF S5081-COWNFR   NOT       = SPACES  AND                    */
		/*    S5081-COWNFR   NOT       = CHDRMNA-COWNNUM                */
		/* IF S5081-CLNTWIN  NOT       = SPACES  AND            <P002>  */
		/*    S5081-CLNTWIN  NOT       = CHDRMNA-COWNNUM        <P002>  */
		/*       MOVE 'CN'               TO CHDRMNA-COWNPFX             */
		/*       MOVE WSSP-FSUCO         TO CHDRMNA-COWNCOY             */
		/*       MOVE S5081-COWNFR       TO CHDRMNA-COWNNUM.            */
		/*       MOVE S5081-CLNTWIN      TO CHDRMNA-COWNNUM.    <P002>  */
		/* IF S5081-JOWNER    NOT      = CHDRMNA-JOWNNUM                */
		/*       MOVE S5081-JOWNER       TO CHDRMNA-JOWNNUM.            */
		/* IF S5081-CLIENTNUM NOT      = CHDRMNA-JOWNNUM        <P002>  */
		/*       MOVE S5081-CLIENTNUM    TO CHDRMNA-JOWNNUM.    <P002>  */
		/* MOVE WSSP-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO VRCM-COMP-TERMID.             */
		/* MOVE VRCM-TRANID-N          TO VRCM-COMP-TRANID-N.           */
		/* MOVE VRCM-COMP-TRANID       TO CHDRMNA-TRANID.               */
		/*  Update contract header fields as follows*/
		/* MOVE KEEPS                  TO CHDRMNA-FUNCTION.             */
		/* MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.               */
		/* CALL 'CHDRMNAIO' USING CHDRMNA-PARAMS.                       */
		/* IF CHDRMNA-STATUZ           NOT = O-K                        */
		/*    MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* MOVE WRITS                  TO CHDRMNA-FUNCTION.             */
		/* MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.               */
		/* CALL 'CHDRMNAIO' USING CHDRMNA-PARAMS.                       */
		/* IF CHDRMNA-STATUZ           NOT = O-K                        */
		/*    MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* Instead of overwrite CHDR record, update this record with       */
		/* validflag 2 and write new record with new information.          */
		/*    MOVE RLSE                   TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/*    MOVE READH                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/*    MOVE CHDRMNA-TRANNO         TO WSAA-LAST-TRANNO.     <LA4261>*/
		/*    MOVE '2'                    TO CHDRMNA-VALIDFLAG.    <LA4261>*/
		/*    MOVE REWRT                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/* Write new record.                                               */
		/*    MOVE '1'                    TO CHDRMNA-VALIDFLAG.    <LA4261>*/
		/*    ADD   1                     TO CHDRMNA-TRANNO.       <LA4261>*/
		/*    MOVE WSSP-TRANID            TO VRCM-TRANID.          <LA4261>*/
		/*    MOVE VRCM-TERMID            TO VRCM-COMP-TERMID.     <LA4261>*/
		/*    MOVE VRCM-TRANID-N          TO VRCM-COMP-TRANID-N.   <LA4261>*/
		/*    MOVE VRCM-COMP-TRANID       TO CHDRMNA-TRANID.       <LA4261>*/
		/* MOVE CHDRMNA-COWNPFX        TO CHDRMNA-DESPPFX.      <LA3389>*/
		/* MOVE CHDRMNA-COWNCOY        TO CHDRMNA-DESPCOY.      <LA3389>*/
		/* MOVE CHDRMNA-COWNNUM        TO CHDRMNA-DESPNUM.      <LA3389>*/
		/*    IF S5081-CLNTWIN  NOT       = SPACES  AND            <LA4261>*/
		/*       S5081-CLNTWIN  NOT       = CHDRMNA-COWNNUM        <LA4261>*/
		/*          MOVE 'Y'                TO WSAA-WRITE-LETTER   <LA4261>*/
		/*          MOVE S5081-CLNTWIN      TO CHDRMNA-COWNNUM.    <LA4261>*/
		/*    IF S5081-CLIENTNUM NOT      = CHDRMNA-JOWNNUM        <LA4261>*/
		/*          MOVE S5081-CLIENTNUM    TO CHDRMNA-JOWNNUM.    <LA4261>*/
		/*    MOVE WRITR                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/* Instead of creating new CHDR record, only update existing       */
		/* CHDR record.                                                    */
		if (isNE(sv.clntwin, SPACES)
		&& isNE(sv.clntwin, chdrmnaIO.getCownnum())) {
			chdrmnaIO.setCownpfx("CN");
			chdrmnaIO.setCowncoy(wsspcomn.fsuco);
			chdrmnaIO.setCownnum(sv.clntwin);
			wsaaWriteLetter = "Y";
		}
		if (isNE(sv.clientnum, chdrmnaIO.getJownnum())) {
			chdrmnaIO.setJownnum(sv.clientnum);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		/*  Update contract header fields as follows                       */
		chdrmnaIO.setFunction(varcom.keeps);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		//TICKEt# ILIFE-1191 STARTS
		chdrmnaIO.setFunction(varcom.rlse);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		wsaaLastTranno.set(chdrmnaIO.tranno);
		chdrmnaIO.validflag.set("2");
		chdrmnaIO.cownnum.set(wsaaCownnum);
		chdrmnaIO.setFunction(varcom.rewrt);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.validflag.set("1");
		 /*ILIFE-6701 */
		//chdrmnaIO.tranno.add(1);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		chdrmnaIO.desppfx.set(chdrmnaIO.cownpfx);
		chdrmnaIO.despcoy.set(chdrmnaIO.cowncoy);
		chdrmnaIO.despnum.set(chdrmnaIO.cownnum);
		if (isNE(sv.clntwin, SPACES)
			&& isNE(sv.clntwin, chdrmnaIO.getCownnum())) {
			chdrmnaIO.setCownpfx("CN");
			chdrmnaIO.setCowncoy(wsspcomn.fsuco);
			wsaaCownnum.set(chdrmnaIO.cownnum);
			chdrmnaIO.setCownnum(sv.clntwin);
			wsaaWriteLetter = "Y";
		}
		if( multOwnerFlag && isNE(sv.clntwin2,chdrmnaIO.getCownnum2()) ||isNE(sv.clntwin3,chdrmnaIO.getCownnum3())|| isNE(sv.clntwin4,chdrmnaIO.getCownnum4())||isNE(sv.clntwin5,chdrmnaIO.getCownnum5()) ) {
			updateClrrpf();
		}
		if(isNE(sv.jownnum,chdrmnaIO.jownnum) || isNE(wsaaCownnum, chdrmnaIO.getCownnum()) || isNE(sv.clntwin2 , chdrmnaIO.getCownnum2()) || isNE(sv.clntwin3 , chdrmnaIO.getCownnum3()) || isNE(sv.clntwin4 , chdrmnaIO.getCownnum4()) || isNE(sv.clntwin5 , chdrmnaIO.getCownnum5()) || isNE(sv.relationwithlife,lifepf.getRelation()) )
		{
			chdrmnaIO.jownnum.set(sv.clientnum);
			chdrmnaIO.setCownnum2(sv.clntwin2);
			chdrmnaIO.setCownnum3(sv.clntwin3);
			chdrmnaIO.setCownnum4(sv.clntwin4);
			chdrmnaIO.setCownnum5(sv.clntwin5);
			chdrmnaIO.setFunction(varcom.writr);
			chdrmnaIO.setFormat(formatsInner.chdrmnarec);
			SmartFileCode.execute(appVars, chdrmnaIO);
			if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmnaIO.getParams());
				fatalError600();
			}
		}
		//ILIFE-1191 ends
		if (isEQ(wsaaWriteLetter, "Y")) {
			writeLetter6000();
		}
		updateRecordForRelationWithLifeAndJoint();

		/* Write a PTRN record.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		wsaaBatcKey.set(wsspcomn.batchkey);
		ptrnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmnaIO.getChdrnum());
		ptrnIO.setTranno(wsaaLastTranno);  /*ILIFE-6701 */
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
	
		
		/* MOVE VRCM-TERM              TO PTRN-TERMID.                  */
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		/* Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsspcomn.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.statuz.set(ZERO);
		batcuprec.function.set(SPACES);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
	}

protected void writeReasncd3050()
	{
		/* If the change originally had a reason record, delete it      */
		/* prior to writing a new reason record (if a new one is        */
		/* necessary).  This will ensure that reason records are        */
		/* deleted when the reason fields are blanked out and updated   */
		/* when a reason field or the owner or joint owner is changed.  */
		if (isNE(wsaaOrigReasoncd, SPACES)
		|| isNE(wsaaOrigResndesc, SPACES)) {
			deleteReason3100();
		}
		/* Create a RESN record if reason code or narrative was entered*/
		if (isNE(sv.resndesc, SPACES)
		|| isNE(sv.reasoncd, SPACES)) {
			resnIO.setDataKey(SPACES);
			resnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
			resnIO.setChdrnum(chdrmnaIO.getChdrnum());
			resnIO.setTranno(chdrmnaIO.getTranno());
			resnIO.setTrancde(wsaaBatcKey.batcBatctrcde);
			resnIO.setReasoncd(sv.reasoncd);
			resnIO.setResndesc(sv.resndesc);
			resnIO.setTransactionDate(varcom.vrcmDate);
			resnIO.setTransactionTime(varcom.vrcmTime);
			resnIO.setUser(varcom.vrcmUser);
			resnIO.setFormat(formatsInner.resnrec);
			resnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, resnIO);
			if (isNE(resnIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(resnIO.getParams());
				fatalError600();
			}
		}
	}

protected void callBldenrl3060()
	{
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrmnaIO.getChdrpfx());
		bldenrlrec.company.set(chdrmnaIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrmnaIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
	}

protected void relsfl3070()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		/*    MOVE WSAA-BATC-BATCTRCDE    TO SFTL-TRANSACTION.             */
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void deleteReason3100()
	{
		begn3110();
	}

protected void begn3110()
	{
		/* Retrieve the record to be deleted using the RESNENQ logical  */
		/* since this includes the transaction code and ensures that    */
		/* the reason record picked up will be for overdue suppression. */
		resnenqIO.setParams(SPACES);
		resnenqIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		resnenqIO.setChdrnum(chdrmnaIO.getChdrnum());
		resnenqIO.setTrancde(wsaaBatcKey.batcBatctrcde);
		resnenqIO.setTranno(99999);
		resnenqIO.setFormat(formatsInner.resnenqrec);
		resnenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, resnenqIO);
		if (isNE(resnenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(resnenqIO.getChdrcoy(), chdrmnaIO.getChdrcoy())
		|| isNE(resnenqIO.getChdrnum(), chdrmnaIO.getChdrnum())
		|| isNE(resnenqIO.getTrancde(), wsaaBatcKey.batcBatctrcde)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		/* Hold and delete the reason record using the RESN logical.    */
		resnIO.setParams(SPACES);
		resnIO.setChdrcoy(resnenqIO.getChdrcoy());
		resnIO.setChdrnum(resnenqIO.getChdrnum());
		resnIO.setTranno(resnenqIO.getTranno());
		resnIO.setFormat(formatsInner.resnrec);
		resnIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, resnIO);
		if (isNE(resnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(resnIO.getParams());
			syserrrec.statuz.set(resnIO.getStatuz());
			fatalError600();
		}
		resnIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, resnIO);
		if (isNE(resnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(resnIO.getParams());
			syserrrec.statuz.set(resnIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void formatResnDetails5000()
	{
		start5010();
	}

	/**
	* <pre>
	*    This section formats the latest reason detailes on screen *
	*    if reason record  present.                                 *
	* </pre>
	*/
protected void start5010()
	{
		resnenqIO.setParams(SPACES);
		resnenqIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		resnenqIO.setChdrnum(chdrmnaIO.getChdrnum());
		resnenqIO.setTrancde(wsaaBatcKey.batcBatctrcde);
		resnenqIO.setTranno(999);
		resnenqIO.setFormat(formatsInner.resnenqrec);
		resnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		resnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		resnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANCDE");
		SmartFileCode.execute(appVars, resnenqIO);
		if (isNE(resnenqIO.getStatuz(), varcom.oK)
		&& isNE(resnenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmnaIO.getChdrcoy(), resnenqIO.getChdrcoy())
		|| isNE(chdrmnaIO.getChdrnum(), resnenqIO.getChdrnum())
		|| isNE(wsaaBatcKey.batcBatctrcde, resnenqIO.getTrancde())
		|| isEQ(resnenqIO.getStatuz(), varcom.endp)) {
			sv.reasoncd.set(SPACES);
			wsaaOrigReasoncd.set(SPACES);
			sv.resndesc.set(SPACES);
			wsaaOrigResndesc.set(SPACES);
		}
		else {
			sv.reasoncd.set(resnenqIO.getReasoncd());
			wsaaOrigReasoncd.set(resnenqIO.getReasoncd());
			wsaaOrigResndesc.set(resnenqIO.getResndesc());
			sv.resndesc.set(resnenqIO.getResndesc());
		}
	}

protected void writeLetter6000()
	{
					start6010();
					readTr3846020();
				}

protected void start6010()
	{
		/* Read Table T6634 for get letter-type.                           */
		wsaaItemBatctrcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
	}

	/**
	* <pre>
	*6020-READ-T6634.                                         <PCPPRT>
	* </pre>
	*/
protected void readTr3846020()
{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634        TO ITEM-ITEMITEM.        <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* If record not found then read again using generic key.          */
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)
			&& isNE(wsaaItemCnttype, "***")) {
				wsaaItemCnttype.set("***");
				/*     GO TO 6020-READ-T6634                            <PCPPRT>*/
				readTr3846020();
				return ;
			}
			else {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF T6634-LETTER-TYPE        = SPACES                 <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*   Get Endorsement no.                                       */
		/* MOVE 'NEXT '                TO ALNO-FUNCTION.        <PCPPRT>*/
		/* MOVE 'EN'                   TO ALNO-PREFIX.          <PCPPRT>*/
		/* MOVE WSKY-BATC-BATCBRN      TO ALNO-GENKEY.          <PCPPRT>*/
		/* MOVE WSSP-COMPANY           TO ALNO-COMPANY.         <PCPPRT>*/
		/* CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                 <PCPPRT>*/
		/* IF ALNO-STATUZ              NOT = O-K                <PCPPRT>*/
		/*    MOVE ALNO-STATUZ         TO SYSR-STATUZ           <PCPPRT>*/
		/*    MOVE 'ALOCNO'            TO SYSR-PARAMS           <PCPPRT>*/
		/*    PERFORM 600-FATAL-ERROR.                          <PCPPRT>*/
		/*   Get set-up parameter for call 'HLETRQS'                   */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		/* MOVE 'EN'                   TO LETRQST-RDOCPFX.      <PCPPRT>*/
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		/* MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.      <PCPPRT>*/
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.branch.set(wsspcomn.branch);
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(wsspcomn.language);
		wsaaOkeyTranno.set(wsaaLastTranno);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatcKey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}
//ILIFE-7277-start
	protected String getOwnerName(String clntnum) {
		clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), clntnum);
		if (clntpf != null) {
			plainname(clntpf);
			return wsspcomn.longconfname.toString();
		}
		return "";
	}
	protected void plainname(Clntpf clntpf) {
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname(clntpf);
			return;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}
	protected void corpname(Clntpf clntpf) {
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
	}

	protected void validateClient(List<String> chkList) {
		clntpf = new Clntpf();
		String corpClnt="";
		for (int i = 0; i < chkList.size(); i++) {
				clntpf = clntpfDAO.findClientByClntnum(chkList.get(i));
				if (clntpf == null && !chkList.get(i).isEmpty() ) {
					sv.ownernameErr.set(e304);
				}else{
				if(i==0){
					corpClnt= clntpfDAO.findClientByClntnum(chkList.get(0)).getClttype();/* IJTI-1523 */
				}
				if( corpClnt.equals("C")){
					sv.ownernameErr.set(rrgy);
					goTo(GotoLabel.checkForErrors2080);
				}
				else if (clntpf != null && clntpf.getClttype().equals("C")) {/* IJTI-1523 */
					sv.ownernameErr.set(rg43);
					goTo(GotoLabel.checkForErrors2080);
				}
		}
		}
		isDuplicateClient();
	}

	protected void isDuplicateClient() {
		chkDuplicate = new HashSet<String>();
		for (String check : chkList) {
			if (!check.isEmpty()) {
				if (!chkDuplicate.add(check)) {
					sv.ownernameErr.set(rrgz);
					goTo(GotoLabel.checkForErrors2080);
					return;
				}
			}
		}
	}
	protected void updateClrrpf()
	{
		if (isNE(sv.clntwin2, chdrmnaIO.getCownnum2())) {
			performAction(sv.clntwin2.toString(),chdrmnaIO.getCownnum2().toString());
		}
		if(isNE(sv.clntwin3, chdrmnaIO.getCownnum3()))
		{
			performAction(sv.clntwin3.toString(),chdrmnaIO.getCownnum3().toString());
		}
		if(isNE(sv.clntwin4, chdrmnaIO.getCownnum4()))
		{
			performAction(sv.clntwin4.toString(),chdrmnaIO.getCownnum4().toString());
		}
		if(isNE(sv.clntwin5, chdrmnaIO.getCownnum5()))
		{
			performAction(sv.clntwin5.toString(),chdrmnaIO.getCownnum5().toString());
		}
		//Set Common values
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clrrrole.set("OW");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(wsspcomn.company);
		cltrelnrec.forenum.set(wsspcomn.chdrChdrnum);

		//If any owner is changed, delete the earlier owner's entry from CLRRPF
		if(isRemove)
			callRemCltreln(clrrToRemoveList);
		//If any owner newly added, add this new entry in CLRRPF
		if(isUpdate)
			callAddCltreln(clrrToUpdateList);
	}
	protected void performAction(String input1, String input2)
	{
		if(clrrToRemoveList == null)
			clrrToRemoveList = new ArrayList<>();
		if(clrrToUpdateList == null)
			clrrToUpdateList = new ArrayList<>();
		if(isEQ(wsspcomn.flag ,"M") && isNE(input2,SPACES) )
		{
			clrrToRemoveList.add(input2.trim());
			isRemove=true;
		}
		if(isNE(input1,SPACES))
		{
			clrrToUpdateList.add(input1.trim());
			isUpdate = true;
		}
	}
		protected void callRemCltreln(List<String> clrrToRemoveList)
		{
			for(String clntnum: clrrToRemoveList) {
				cltrelnrec.cltrelnRec.set(SPACES);
				cltrelnrec.clntpfx.set(chdrmnaIO.getCownpfx());
				cltrelnrec.clntcoy.set(chdrmnaIO.getCowncoy());
				cltrelnrec.clntnum.set(clntnum);
				cltrelnrec.function.set("REM  ");
				cltrelnrec.clrrrole.set("OW");
				cltrelnrec.forepfx.set("CH");
				cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
				cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
				callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
				if (isNE(cltrelnrec.statuz, Varcom.oK)) {
					syserrrec.statuz.set(cltrelnrec.statuz);
					fatalError600();
				}
			}
			isRemove=false;
		}
		protected void callAddCltreln(List<String> clrrToUpdateList)
		{
			for(String clntnum: clrrToUpdateList) {

				cltrelnrec.clntnum.set(clntnum);
				cltrelnrec.function.set("ADD");
				callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
				if (isNE(cltrelnrec.statuz,Varcom.oK)) {
					syserrrec.statuz.set(cltrelnrec.statuz);
					fatalError600();
				}
			}
			isUpdate=false;
		}
	//ILIFE-7277-end
		
		private static final class ErrorsInner { 
			/* ERRORS */
			private FixedLengthStringData rrsv = new FixedLengthStringData(4).init("RRSV");
		}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData resnrec = new FixedLengthStringData(10).init("RESNREC");
	private FixedLengthStringData chdrmnarec = new FixedLengthStringData(10).init("CHDRMNAREC");
	private FixedLengthStringData resnenqrec = new FixedLengthStringData(10).init("RESNENQREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
}
    /*
     * Class transformed  from Data Structure TABLES--INNER
     */
    private static final class TablesInner {
        /* TABLES */
        private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
        private FixedLengthStringData t5677 = new FixedLengthStringData(5).init("T5677");
    }

}
