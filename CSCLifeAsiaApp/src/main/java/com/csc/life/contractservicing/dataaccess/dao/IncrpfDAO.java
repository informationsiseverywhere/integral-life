package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface IncrpfDAO extends BaseDAO<Incrpf>  {

	public List<Incrpf> getIncrpfByPolicyNum(String contractNum, Wsspcomn wsspcomn);
	public int deleteRecordByUniqueNumber(List<Long> ids);
}
