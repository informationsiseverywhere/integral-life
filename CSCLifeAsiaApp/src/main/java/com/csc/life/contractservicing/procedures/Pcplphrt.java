/*
 * File: Pcplphrt.java
 * Date: 30 August 2009 1:02:40
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLPHRT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.PhrtissTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  EFFDATE    Effectiveline Date
*  002  DTEISS     Issue Date
*  003  APIND      Arrears Payment Indicator
*  004  HPRJPTDATE Projected Paid-to-date
*  005  MANADJ     Manual Adjustment Amount
*
***********************************************************************
* </pre>
*/
public class Pcplphrt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLPHRT";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaDatetext = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaDatetext1 = new FixedLengthStringData(1).isAPartOf(wsaaDatetext, 0);
	private FixedLengthStringData wsaaDatetext2 = new FixedLengthStringData(21).isAPartOf(wsaaDatetext, 1);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (5, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String phrtissrec = "PHRTISSREC";
	private LetcTableDAM letcIO = new LetcTableDAM();
	private PhrtissTableDAM phrtissIO = new PhrtissTableDAM();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplphrt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getPhrt200();
		}
		if (isEQ(wsaaCounter,ZERO)) {
			pcpdatarec.statuz.set("GEND");
			return ;
		}
		else {
			if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
				pcpdatarec.statuz.set(varcom.endp);
				return ;
			}
		}
		if (isEQ(pcpdatarec.fldOffset,1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaDataBuffer.set(SPACES);
		offset.set(ZERO);
		wsaaCounter.set(ZERO);
		phrtissIO.setParams(SPACES);
		phrtissIO.setChdrcoy(letcIO.getRdoccoy());
		phrtissIO.setChdrnum(letcIO.getRdocnum());
		phrtissIO.setTranno(letcIO.getTranno());
		phrtissIO.setFormat(phrtissrec);
		phrtissIO.setFunction(varcom.readr);
		phrtissIO.setStatuz(varcom.oK);
	}

protected void getPhrt200()
	{
		getData200();
		format201();
		format202();
		format203();
		format204();
		f0rmat205();
	}

protected void getData200()
	{
		SmartFileCode.execute(appVars, phrtissIO);
		if (isNE(phrtissIO.getStatuz(),varcom.oK)
		&& isNE(phrtissIO.getStatuz(),varcom.mrnf)) {
			letcIO.setStatuz(phrtissIO.getStatuz());
			syserrrec.statuz.set(phrtissIO.getStatuz());
			syserrrec.params.set(phrtissIO.getParams());
			fatalError600();
		}
		wsaaCounter.add(1);
	}

protected void format201()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getRequestCompany());
		datcon6rec.intDate1.set(phrtissIO.getEffdate());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			datcon6rec.intDate2.set(SPACES);
		}
		wsaaDatetext.set(datcon6rec.intDate2);
		if (isEQ(wsaaDatetext1,SPACES)) {
			wsaaDatetext.set(wsaaDatetext2);
		}
		fldlen[offset.toInt()].set(length(wsaaDatetext));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaDatetext);
	}

protected void format202()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getRequestCompany());
		datcon6rec.intDate1.set(phrtissIO.getDteiss());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			datcon6rec.intDate2.set(SPACES);
		}
		wsaaDatetext.set(datcon6rec.intDate2);
		if (isEQ(wsaaDatetext1,SPACES)) {
			wsaaDatetext.set(wsaaDatetext2);
		}
		fldlen[offset.toInt()].set(length(wsaaDatetext));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaDatetext);
	}

protected void format203()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(phrtissIO.getApind()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], phrtissIO.getApind());
	}

protected void format204()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getRequestCompany());
		datcon6rec.intDate1.set(phrtissIO.getHprjptdate());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			datcon6rec.intDate2.set(SPACES);
		}
		wsaaDatetext.set(datcon6rec.intDate2);
		if (isEQ(wsaaDatetext1,SPACES)) {
			wsaaDatetext.set(wsaaDatetext2);
		}
		fldlen[offset.toInt()].set(length(wsaaDatetext));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaDatetext);
	}

protected void f0rmat205()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(phrtissIO.getManadj());
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
