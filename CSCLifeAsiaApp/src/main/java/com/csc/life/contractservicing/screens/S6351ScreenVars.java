package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6351
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6351ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData entity = DD.entity.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,117);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,172);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,180);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,184);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData textfield = DD.textfield.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData flag =  DD.flag.copy().isAPartOf(dataFields,214);
	public FixedLengthStringData flagdisable =  DD.flag.copy().isAPartOf(dataFields,215);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData entityErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData textfieldErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData flagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData flagdisableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] entityOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] textfieldOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] flagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] flagdisableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData subfileArea = new FixedLengthStringData(318);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(92).isAPartOf(subfileArea, 0);
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData cmpntnum = DD.cmpntnum.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData component = DD.component.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData deit = DD.deit.copy().isAPartOf(subfileFields,11);
	public FixedLengthStringData hcoverage = DD.hcoverage.copy().isAPartOf(subfileFields,48);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData hlifcnum = DD.hlifcnum.copy().isAPartOf(subfileFields,54);
	public FixedLengthStringData hlifeno = DD.hlifeno.copy().isAPartOf(subfileFields,62);
	public FixedLengthStringData hrider = DD.hrider.copy().isAPartOf(subfileFields,64);
	public ZonedDecimalData hsuffix = DD.hsuffix.copyToZonedDecimal().isAPartOf(subfileFields,66);
	public FixedLengthStringData linetype = DD.linetype.copy().isAPartOf(subfileFields,70);
	public FixedLengthStringData pstatcode = DD.premdesc.copy().isAPartOf(subfileFields,71);
	public FixedLengthStringData statcode = DD.riskdesc.copy().isAPartOf(subfileFields,81);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,91);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(56).isAPartOf(subfileArea, 92);
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cmpntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData componentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData deitErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hcoverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hlifenoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hriderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData hsuffixErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData linetypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData premdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData riskdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(168).isAPartOf(subfileArea, 148);
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cmpntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] componentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] deitOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hcoverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hlifcnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hlifenoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hriderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] hsuffixOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] linetypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] premdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] riskdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 316);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S6351screensflWritten = new LongData(0);
	public LongData S6351screenctlWritten = new LongData(0);
	public LongData S6351screenWritten = new LongData(0);
	public LongData S6351protectWritten = new LongData(0);
	public GeneralTable s6351screensfl = new GeneralTable(AppVars.getInstance());
	public static int[] screenSflPfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 20, 21, 22, 23, 24,38,33};


	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6351screensfl;
	}

	public S6351ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"02","04","-01","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrstatusOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premstatusOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "06",null, null, null, null, null, null, null, null});
		fieldIndMap.put(entityOut,new String[] {null, null, "06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(textfieldOut,new String[] {null, null, null, "99-89",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hlifcnum, hlifeno, hsuffix, hcoverage, hrider, hcrtable, linetype, statcode, pstatcode, select, component, cmpntnum, deit, asterisk};
		screenSflOutFields = new BaseData[][] {hlifcnumOut, hlifenoOut, hsuffixOut, hcoverageOut, hriderOut, hcrtableOut, linetypeOut, riskdescOut, premdescOut, selectOut, componentOut, cmpntnumOut, deitOut, asteriskOut};
		screenSflErrFields = new BaseData[] {hlifcnumErr, hlifenoErr, hsuffixErr, hcoverageErr, hriderErr, hcrtableErr, linetypeErr, riskdescErr, premdescErr, selectErr, componentErr, cmpntnumErr, deitErr, asteriskErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields(); 
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6351screen.class;
		screenSflRecord = S6351screensfl.class;
		screenCtlRecord = S6351screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6351protect.class;
	}
	
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {};
	}
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {};//SGS001
	}
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {};
	}
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6351screenctl.lrec.pageSubfile);
	}
	public int getDataAreaSize() {
		return 488;
	}
	public int getDataFieldsSize(){
		return 216;
	}
	public int getErrorIndicatorSize(){
		return 68 ; 
	}
	public int getOutputFieldSize(){
		return 204; 
	}
	
	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, numpols, planSuffix, entity, textfield,flag,flagdisable};
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][]  {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, numpolsOut, plnsfxOut, entityOut, textfieldOut,flagOut,flagdisableOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[]  {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, numpolsErr, plnsfxErr, entityErr, textfieldErr,flagErr,flagdisableErr};
		
	}
}
