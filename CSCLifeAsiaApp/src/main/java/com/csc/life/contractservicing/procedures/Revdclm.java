/*
 * File: Revdclm.java
 * Date: 30 August 2009 2:07:04
 * Author: Quipoz Limited
 *
 * Class transformed from REVDCLM.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.FluprevTableDAM;
import com.csc.life.contractservicing.dataaccess.FuperevTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.dataaccess.LiferevTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrclrTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmdpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  REVDCLM - Death Claim Reversal
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*
*  REVERSE CLAIM HEADER
*  ~~~~~~~~~~~~~~~~~~~~
*  READH on  the  Claim  Header  record  (CLMHCLM)  for the
*  relevant contract.
*
*  Store the Contract Number and Contract Company for any
*  reads etc. on other files.
*
*  DELET this CLMHCLM record.
*
*
*  REVERSE CLAIM DETAIL RECORDS
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  BEGNH on  the  Claim  details  record  (CLMDCLM) for the
*  relevant contract.
*
*  DELET all CLMDCLM records found for this contract.
*
*
*  REVERSE CONTRACT HEADER
*  ~~~~~~~~~~~~~~~~~~~~~~~
*  BEGNH on  the  Contract  Header record (CHDRCLM) for the
*  relevant contract.
*
*  Store the transaction number and status of the contract.
*  Add 1 to the tranno and store.
*
*  This is the valid flag 1 record - there should be at least
*  one valid flag 1 record and one valid flag 2 record
*  following Death Claim Registration.
*
*  Perform the following until CHDRCLM-STATUZ = ENDP:-
*
*          Delete the valid flag 1 record
*
*          Using the transaction number stored:-
*            Perform REVERSE LIVES
*            Perform REVERSE COMPONENTS
*            Perform REVERSE UTRNS
*            Perform REVERSE FOLLOW-UPS until FLUPREV-STATUZ
*                                       = ENDP
*            Perform REVERSE ACMVS
*            Perform REVERSE PAID OFF LOANS
*
*          BEGNH on  the  Contract  Header record (CHDRCLM) to
*          get the valid flag 2 record for this contract. If
*          no record is found with a valid flag 2 then this
*          is an error.
*
*          REWRT this valid flag 2 record with a valid flag
*          of 1.
*
*          BEGNH on the Contract Header to get the next valid
*          flag 1 record.
*
*          This BEGNH should return the valid flag 1 record
*          just created :-
*          If the Status of this record is different to what
*          was stored at the beginning then,
*             The record has been brought back to the point
*             it was before Death Registration -
*             Move ENDP to CHDRCLM-STATUZ and go to EXIT.
*
*          Store the transaction number of this valid flag
*          1 Contract Header record - to be used the next
*          time into the loop.
*
*  REVERSE PAYOR DETAILS
*  ~~~~~~~~~~~~~~~~~~~~~
*  BEGNH on  the  Payor Details record (PAYRLIF) for the
*  relevant contract.
*
*  This is the valid flag 1 record - there should be at least
*  one valid flag 1 record and one valid flag 2 record
*  following Death Claim Registration. If not '1', then this is
*  an error.
*
*  Add 1 to the tranno and store.
*
*  DELET this record.
*
*  NEXTR on  the  Payor record (PAYRLIF) for the relevant
*  contract.  This should read a record with valid flag of
*  '2'; if not, then this is an error.
*
*  REWRT this Payor record (PAYRLIF) after altering the valid
*  flag from '2' to '1' and with the stored tranno as this is
*  to re-instate the  contract prior to the Death Claim
*  Registration.
*
*  REVERSE LIVES
*  ~~~~~~~~~~~~~
*  BEGNH on  the  LIFEREV logical view, which has TRANNO
*  as part of its key.
*
*  This should return for you the valid flag 1
*  record wrote at Death Claim Registration
*
*  Store the information found on this record to form
*  the key for the LIFE logical view.
*
*  Perform the following:-
*
*          DELET the valid flag 1 record.
*
*          BEGNH on the LIFE logical view using the key
*          stored, to find the valid flag 2 record.
*          If no valid flag 2 record is found then this
*          is an error.
*
*          REWRT this valid flag 2 record with a valid flag
*          of 1.
*
*
*  REVERSE COMPONENTS
*  ~~~~~~~~~~~~~~~~~~
*  BEGNH on  the  COVRCLR logical view, which has TRANNO
*  as part of its key.
*
*  This should return for you the valid flag 1
*  record wrote at Death Claim Registration
*
*  Store the information found on this record to form
*  the key for the COVR logical view.
*
*  Perform the following until COVRCLR-STATUZ = ENDP:-
*
*          DELET the valid flag 1 record.
*
*          BEGNH on the COVR logical view using the key
*          stored, to find the valid flag 2 record.
*
*          If no valid flag 2 record is found then this
*          is an error.
*
*          REWRT this valid flag 2 record with a valid flag
*          of 1.
*
*          BEGNH on COVRCLR logical view to see are there
*          other records for this transaction number.
*
*          If there are no other records
*             Move ENDP to COVRCLR-STATUZ.
*
*          Store the key for the next time through the loop.
*
*
*  REVERSE UTRNS
*  ~~~~~~~~~~~~~
*
*       Call the T5671 generic subroutine to reverse Unit
*       transactions for the TRANNO being reversed.
*       The subroutine GREVUTRN should be entered in the
*       relevant table entry.
*
*
*  REVERSE FOLLOW-UPS
*  ~~~~~~~~~~~~~~~~~~
*  BEGNH on  the  FLUPREV logical view, which has TRANNO
*  as part of its key.
*
*  DELET the record found.
*
*  This section should be repeated until no more records
*  are found.
*
*
*  REVERSE ACMVS
*  ~~~~~~~~~~~~~
*
*       Do a BEGNH on the ACMV file for this transaction no. and
*       call LIFACMV to post the reversal to the sub-account.
*
*
*  REVERSE PAID OFF LOANS
*  ----------------------
*
*       Do a BEGNH on the LOANENQ file for the tranno being
*        reversed and see if any Loans ( contract or APLs )
*        were paid off during the Death Claim processing
*        - check this by comparing the Last-Tranno field with
*          the tranno we are trying to reverse now
*        - if so, we must re-instate the Loans affected
*        Set the LOANENQ record Validflags back to '1' and
*         zeroise the Last-Tranno field
*
*
*****************************************************
* </pre>
*/
public class Revdclm extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVDCLM";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5671 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 4);
		/* WSAA-KEY-DATA */
	private FixedLengthStringData wsaaChdrcoyKey = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnumKey = new FixedLengthStringData(8);
		/* WSAA-COVR-KEY */
	private FixedLengthStringData wsaaChdrcoyCovr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnumCovr = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLifeCovr = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverageCovr = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRiderCovr = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPlnsfxCovr = new PackedDecimalData(4, 0);
		/* WSAA-LIFE-KEY */
	private FixedLengthStringData wsaaChdrcoyLife = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnumLife = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLifeLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaJlifeLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStatcodeStore = new FixedLengthStringData(2);
	private PackedDecimalData wsaaReversedTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaPayrlifTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private PackedDecimalData wsaaLstInterestDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNxtInterestDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00", "28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, "31");
	private Validator daysInFeb = new Validator(wsaaDayCheck, "28");
	private Validator daysInApr = new Validator(wsaaDayCheck, "30");

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, "01");
	private Validator february = new Validator(wsaaMonthCheck, "02");
	private Validator march = new Validator(wsaaMonthCheck, "03");
	private Validator april = new Validator(wsaaMonthCheck, "04");
	private Validator may = new Validator(wsaaMonthCheck, "05");
	private Validator june = new Validator(wsaaMonthCheck, "06");
	private Validator july = new Validator(wsaaMonthCheck, "07");
	private Validator august = new Validator(wsaaMonthCheck, "08");
	private Validator september = new Validator(wsaaMonthCheck, "09");
	private Validator october = new Validator(wsaaMonthCheck, "10");
	private Validator november = new Validator(wsaaMonthCheck, "11");
	private Validator december = new Validator(wsaaMonthCheck, "12");
	private int wsaaFebruaryDays = 28;
	private int wsaaAprilDays = 30;

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();

	private String wsaaIoCall = "";

	private FixedLengthStringData wsaaAcmvTrcde = new FixedLengthStringData(4).init(SPACES);
	private Validator loanInterestBilling = new Validator(wsaaAcmvTrcde, "BA69");
	private Validator loanInterestCapital = new Validator(wsaaAcmvTrcde, "BA68");
		/* TABLES */
	private String t5671 = "T5671";
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t6633 = "T6633";
		/* FORMATS */
	private String chdrclmrec = "CHDRCLMREC";
	private String clmdclmrec = "CLMDCLMREC";
	private String clmhclmrec = "CLMHCLMREC";
	private String covrclrrec = "COVRCLRREC";
	private String fluprevrec = "FLUPREVREC";
	private String fuperevrec = "FUPEREVREC";
	private String liferevrec = "LIFEREVREC";
	private String covrrec = "COVRREC";
	private String itemrec = "ITEMREC";
	private String acmvrevrec = "ACMVREVREC";
	private String loanenqrec = "LOANENQREC";
	private String itdmrec = "ITEMREC";
	private String payrlifrec = "PAYRLIFREC";
	private String incrselrec = "INCRSELREC";
		/* ERRORS */
	private String e723 = "E723";
	private ArcmTableDAM arcmIO = new ArcmTableDAM();

		/*ACCOUNT MOVEMENTS LOGICAL VIEW FOR REVER*/
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
		/*Claims Contract Header*/
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
		/*Death Claims Logical File*/
	private ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
		/*Claim Header file*/
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Coverage/Rider Death Claim Reversals.*/
	private CovrclrTableDAM covrclrIO = new CovrclrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Follow Up File for Reversals*/
	private FluprevTableDAM fluprevIO = new FluprevTableDAM();
		/*FollowUp Extended Text File for Reversal*/
	private FuperevTableDAM fuperevIO = new FuperevTableDAM();
	private Greversrec greversrec = new Greversrec();
		/*Automatic Increase Refusal Select File*/
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
		/*Life file for Reversals*/
	private LiferevTableDAM liferevIO = new LiferevTableDAM();
		/*LOAN Enquiries Logical file*/
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
		/*Payr logical file*/
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T6633rec t6633rec = new T6633rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	protected Cattpf cattpf = new Cattpf();	
	boolean CMDTH010Permission  = false;

	private static final String feaConfigPreRegistartion= "CMDTH010";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2390,
		exit6190,
		exit6590
	}

	public Revdclm() {
		super();
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*MOVE-LINKAGE*/
	CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		/*  Get todays date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void process2000()
	{
		/*PROCESS*/
		reverseClaimsHeader2100();
		reverseClaimDetails2200();
		if(CMDTH010Permission){
		reverseCattpf2600();
		}
		reverseContractHeader2400();
		reversePayrlif2500();
		
		/*EXIT*/
	}

protected void reverseCattpf2600()
   {
	deleteCattpf3610();
	updateCattpf3620();
		
	}
protected void deleteCattpf3610(){
	cattpfDAO.deleteRcdByChdrnum(clmhclmIO.getChdrcoy().toString(), clmhclmIO.getChdrnum().toString());	
}
protected void updateCattpf3620(){
	cattpfDAO.updateCattpfSetValidflag1(clmhclmIO.getChdrcoy().toString(), clmhclmIO.getChdrnum().toString());
}

protected void reverseClaimsHeader2100()
	{
		readClaimsHeader2101();
		deleteRecord2120();
	}

protected void readClaimsHeader2101()
	{
		clmhclmIO.setParams(SPACES);
		clmhclmIO.setChdrcoy(reverserec.company);
		clmhclmIO.setChdrnum(reverserec.chdrnum);
		clmhclmIO.setFormat(formatsInner.clmhclmrec);
		clmhclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			xxxxFatalError();
		}
		/* Store the CHDRCOY and CHDRNUM for any reads on files etc.*/
		wsaaChdrcoyKey.set(clmhclmIO.getChdrcoy());
		wsaaChdrnumKey.set(clmhclmIO.getChdrnum());
	}

protected void deleteRecord2120()
	{
		clmhclmIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void reverseClaimDetails2200()
	{
		/*GET-FIRST-CLMD-RECORD*/
		clmdclmIO.setParams(SPACES);
		clmdclmIO.setChdrcoy(wsaaChdrcoyKey);
		clmdclmIO.setChdrnum(wsaaChdrnumKey);
		clmdclmIO.setCoverage(ZERO);
		clmdclmIO.setRider(ZERO);
		clmdclmIO.setCrtable(SPACES);
		clmdclmIO.setFormat(formatsInner.clmdclmrec);
		clmdclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clmdclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(clmdclmIO.getStatuz(),varcom.endp))) {
			reverseClaims2300();
		}

		/*EXIT*/
	}

protected void reverseClaims2300()
	{
		try {
			update2301();
			deleteRecord2320();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void update2301()
	{
		SmartFileCode.execute(appVars, clmdclmIO);
		if ((isNE(clmdclmIO.getStatuz(),varcom.oK))
		&& (isNE(clmdclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(clmdclmIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(clmdclmIO.getChdrnum(),wsaaChdrnumKey))
		|| (isNE(clmdclmIO.getChdrcoy(),wsaaChdrcoyKey))
		|| (isEQ(clmdclmIO.getStatuz(),varcom.endp))) {
			clmdclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2390);
		}
	}

protected void deleteRecord2320()
	{
		clmdclmIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getStatuz());
			xxxxFatalError();
		}
		/*NEXTR*/
		clmdclmIO.setFunction(varcom.nextr);
	}

protected void reverseContractHeader2400()
	{
		reverseContractHeader2401();
		doDelete2410();
	}

protected void reverseContractHeader2401()
	{
		/* Get the first Contract Header record using CHDRCLM logical view*/
		chdrclmIO.setParams(SPACES);
		chdrclmIO.setChdrnum(wsaaChdrnumKey);
		chdrclmIO.setChdrcoy(wsaaChdrcoyKey);
		chdrclmIO.setFormat(formatsInner.chdrclmrec);
		chdrclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrclmIO);
		if ((isNE(chdrclmIO.getStatuz(),varcom.oK))
		&& (isNE(chdrclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(chdrclmIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(chdrclmIO.getChdrnum(),wsaaChdrnumKey))
		|| (isNE(chdrclmIO.getChdrcoy(),wsaaChdrcoyKey))
		|| (isNE(chdrclmIO.getValidflag(),"1"))
		|| (isEQ(chdrclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(chdrclmIO.getParams());
			xxxxFatalError();
		}
		/* Store the statcode of the contract*/
		/* Store the Transaction number being reversed for use by LOANS    */
		/*  later on                                                       */
		wsaaReversedTranno.set(chdrclmIO.getTranno());
	}

protected void doDelete2410()
	{
		/* Delete the valid flag 1 record*/
		chdrclmIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			xxxxFatalError();
		}
		reverseOtherRecs2600();
		/* Perform a BEGNH on the CHDRCLM logical view using the key stored*/
		/* to find the valid flag 2 record*/
		chdrclmIO.setParams(SPACES);
		chdrclmIO.setChdrcoy(wsaaChdrcoyKey);
		chdrclmIO.setChdrnum(wsaaChdrnumKey);
		chdrclmIO.setFormat(chdrclmrec);
		chdrclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		chdrclmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrclmIO);
		if ((isNE(chdrclmIO.getStatuz(),varcom.oK))
		&& (isNE(chdrclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(chdrclmIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(chdrclmIO.getChdrcoy(),wsaaChdrcoyKey))
		|| (isNE(chdrclmIO.getChdrnum(),wsaaChdrnumKey))
		|| (isNE(chdrclmIO.getValidflag(),"2"))
		|| (isEQ(chdrclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(chdrclmIO.getParams());
			xxxxFatalError();
		}
		/* Update the valid flag 2 record*/
		chdrclmIO.setValidflag("1");
		chdrclmIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			xxxxFatalError();
		}
	}

protected void reversePayrlif2500()
	{
		begnhPayrlif2500();
		deletPayrlifRecord2502();
		readNextrPayrlif2504();
		rewritePayrlif2506();
	}

protected void begnhPayrlif2500()
	{
		/* Read the first PAYR record for the contract/company at hand.    */
		wsaaPayrlifTranno.set(ZERO);
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(wsaaChdrcoyKey);
		payrlifIO.setChdrnum(wsaaChdrnumKey);
		payrlifIO.setPayrseqno(ZERO);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		payrlifIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		/* If this PAYR record isn't a valid one, there's a problem.       */
		if (isNE(payrlifIO.getValidflag(),"1")) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		compute(wsaaPayrlifTranno, 0).set(add(payrlifIO.getTranno(),1));
	}

protected void deletPayrlifRecord2502()
	{
		/* Delete the PAYR record just read.                               */
		payrlifIO.setFormat(formatsInner.payrlifrec);
		payrlifIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readNextrPayrlif2504()
	{
		/* Read the next PAYR record, anticipating it to be updated.       */
		payrlifIO.setFormat(formatsInner.payrlifrec);
		payrlifIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)
		&& isNE(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		/* If this PAYR record isn't a valid one, there's a problem.       */
		if (isNE(payrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(payrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isEQ(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getValidflag(),"2")) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rewritePayrlif2506()
	{
		/* Update the PAYR record just read with an incremented            */
		/* transaction number and a valid flag of "1".                     */
		payrlifIO.setTranno(wsaaPayrlifTranno);
		payrlifIO.setValidflag("1");
		payrlifIO.setFormat(formatsInner.payrlifrec);
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void reverseOtherRecs2600()
	{
		/*DO-REVERSAL*/
		while ( !(isEQ(liferevIO.getStatuz(),varcom.endp))) {
			processLifeRecords3000();
		}

		processComponents4000();
		while ( !(isEQ(fluprevIO.getStatuz(),varcom.endp))) {
			reverseFollowups6000();
		}

		fuperevIO.setParams(SPACES);
		while ( !(isEQ(fuperevIO.getStatuz(),varcom.endp))) {
			reverseFuperev6500();
		}

		processAcmvs7000();
		processAcmvOptical7010();
		checkLoans8000();
		/*EXIT*/
	}

protected void processLifeRecords3000()
	{
		lives3010();
	}

protected void lives3010()
	{
		liferevIO.setParams(SPACES);
		liferevIO.setChdrnum(wsaaChdrnumKey);
		liferevIO.setChdrcoy(wsaaChdrcoyKey);
		liferevIO.setTranno(reverserec.tranno);
		liferevIO.setFormat(formatsInner.liferevrec);
		liferevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		liferevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, liferevIO);
		if ((isNE(liferevIO.getStatuz(),varcom.oK))
		&& (isNE(liferevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(liferevIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(liferevIO.getChdrnum(),wsaaChdrnumKey))
		|| (isNE(liferevIO.getChdrcoy(),wsaaChdrcoyKey))
		|| (isNE(liferevIO.getValidflag(),1))
		|| (isEQ(liferevIO.getStatuz(),varcom.endp))
		|| (isNE(liferevIO.getTranno(),reverserec.tranno))) {
			liferevIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaChdrnumLife.set(liferevIO.getChdrnum());
		wsaaChdrcoyLife.set(liferevIO.getChdrcoy());
		wsaaLifeLife.set(liferevIO.getLife());
		wsaaJlifeLife.set(liferevIO.getJlife());
		deleteAndUpdate3200();
	}

protected void deleteAndUpdate3200()
	{
		deleteAndUpdate3210();
	}

protected void deleteAndUpdate3210()
	{
		/* Delete the valid flag 1 record*/
		/* MOVE DELET                  TO LIFEREV-FUNCTION.             */
		liferevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, liferevIO);
		if (isNE(liferevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(liferevIO.getParams());
			xxxxFatalError();
		}
		/* Perform a BEGNH on the LIFE logical view using the key stored,*/
		/* to find the valid flag two record*/
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(wsaaChdrcoyLife);
		lifeIO.setChdrnum(wsaaChdrnumLife);
		lifeIO.setLife(wsaaLifeLife);
		lifeIO.setJlife(wsaaJlifeLife);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		lifeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "JLIFE");
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(),varcom.oK))
		&& (isNE(lifeIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(lifeIO.getChdrnum(),wsaaChdrnumLife))
		|| (isNE(lifeIO.getChdrcoy(),wsaaChdrcoyLife))
		|| (isNE(lifeIO.getLife(),wsaaLifeLife))
		|| (isNE(lifeIO.getJlife(),wsaaJlifeLife))
		|| (isNE(lifeIO.getValidflag(),"2"))
		|| (isEQ(lifeIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
		lifeIO.setValidflag("1");
		lifeIO.setCurrto(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			xxxxFatalError();
		}
	}

protected void readGenericProcTab3800()
	{
		readStatusCodes3801();
	}

protected void readStatusCodes3801()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Batc.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),SPACES)) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		initialize(greversrec.reverseRec);
		greversrec.effdate.set(varcom.vrcmMaxDate);
		greversrec.transDate.set(varcom.vrcmMaxDate);
		greversrec.chdrcoy.set(covrclrIO.getChdrcoy());
		greversrec.chdrnum.set(covrclrIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrclrIO.getPlanSuffix());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.life.set(covrclrIO.getLife());
		greversrec.coverage.set(covrclrIO.getCoverage());
		greversrec.rider.set(covrclrIO.getRider());
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.effdate.set(reverserec.ptrneff);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callSubprog3900();
		}
	}

protected void callSubprog3900()
	{
		/*GO*/
		greversrec.statuz.set("****");
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz,"****")) {
				syserrrec.statuz.set(greversrec.statuz);
				xxxxFatalError();
			}
		}
		/*EXIT*/
	}

protected void processComponents4000()
	{
		getFirstCovr4100();
	}

protected void getFirstCovr4100()
	{
		covrclrIO.setParams(SPACES);
		covrclrIO.setChdrcoy(wsaaChdrcoyKey);
		covrclrIO.setChdrnum(wsaaChdrnumKey);
		covrclrIO.setTranno(reverserec.tranno);
		covrclrIO.setFormat(formatsInner.covrclrrec);
		covrclrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		covrclrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, covrclrIO);
		if ((isNE(covrclrIO.getStatuz(),varcom.oK))
		&& (isNE(covrclrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrclrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrclrIO.getStatuz(),varcom.endp))
		|| (isNE(covrclrIO.getChdrnum(),wsaaChdrnumKey))
		|| (isNE(covrclrIO.getChdrcoy(),wsaaChdrcoyKey))
		|| (isNE(covrclrIO.getValidflag(),"1"))
		|| (isNE(covrclrIO.getTranno(),reverserec.tranno))) {
			return ;
		}
		/* Store the key for the BEGN on the COVR logical view to find*/
		/* the valid flag 2 record*/
		wsaaChdrcoyCovr.set(covrclrIO.getChdrcoy());
		wsaaChdrnumCovr.set(covrclrIO.getChdrnum());
		wsaaLifeCovr.set(covrclrIO.getLife());
		wsaaCoverageCovr.set(covrclrIO.getCoverage());
		wsaaRiderCovr.set(covrclrIO.getRider());
		wsaaPlnsfxCovr.set(covrclrIO.getPlanSuffix());
		while ( !(isEQ(covrclrIO.getStatuz(),varcom.endp))) {
			deleteAndUpdatComp4500();
		}

	}

protected void deleteAndUpdatComp4500()
	{
		deleteAndUpdate4500();
	}

protected void deleteAndUpdate4500()
	{
		checkIncr4600();
		covrclrIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, covrclrIO);
		if (isNE(covrclrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrclrIO.getParams());
			xxxxFatalError();
		}
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(wsaaChdrcoyCovr);
		covrIO.setChdrnum(wsaaChdrnumCovr);
		covrIO.setLife(wsaaLifeCovr);
		covrIO.setCoverage(wsaaCoverageCovr);
		covrIO.setRider(wsaaRiderCovr);
		covrIO.setPlanSuffix(wsaaPlnsfxCovr);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK))
		&& (isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrIO.getStatuz(),varcom.endp))
		|| (isNE(covrIO.getValidflag(),"2"))
		|| (isNE(covrIO.getChdrcoy(),wsaaChdrcoyCovr))
		|| (isNE(covrIO.getChdrnum(),wsaaChdrnumCovr))
		|| (isNE(covrIO.getLife(),wsaaLifeCovr))) {
			syserrrec.params.set(covrIO.getParams());
			xxxxFatalError();
		}
		readGenericProcTab3800();
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/* Perform a BEGNH on COVRCLR logical view to get the next*/
		/* record which has been updated by Death Claim, if there is*/
		/* another*/
		covrclrIO.setParams(SPACES);
		covrclrIO.setChdrcoy(wsaaChdrcoyKey);
		covrclrIO.setChdrnum(wsaaChdrnumKey);
		covrclrIO.setTranno(reverserec.tranno);
		covrclrIO.setFormat(formatsInner.covrclrrec);
		covrclrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		covrclrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, covrclrIO);
		if ((isNE(covrclrIO.getStatuz(),varcom.oK))
		&& (isNE(covrclrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrclrIO.getParams());
			xxxxFatalError();
		}
		if ((isEQ(covrclrIO.getStatuz(),varcom.endp))
		|| (isNE(covrclrIO.getChdrnum(),wsaaChdrnumKey))
		|| (isNE(covrclrIO.getChdrcoy(),wsaaChdrcoyKey))
		|| (isNE(covrclrIO.getTranno(),reverserec.tranno))) {
			covrclrIO.setStatuz(varcom.endp);
			return ;
		}
		/* Store the key for the BEGNH on the COVR logical view*/
		wsaaChdrcoyCovr.set(covrclrIO.getChdrcoy());
		wsaaChdrnumCovr.set(covrclrIO.getChdrnum());
		wsaaLifeCovr.set(covrclrIO.getLife());
		wsaaCoverageCovr.set(covrclrIO.getCoverage());
		wsaaRiderCovr.set(covrclrIO.getRider());
		wsaaPlnsfxCovr.set(covrclrIO.getPlanSuffix());
	}

protected void checkIncr4600()
	{
		readh4610();
	}

protected void readh4610()
	{
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(covrclrIO.getChdrcoy());
		incrselIO.setChdrnum(covrclrIO.getChdrnum());
		incrselIO.setLife(covrclrIO.getLife());
		incrselIO.setCoverage(covrclrIO.getCoverage());
		incrselIO.setRider(covrclrIO.getRider());
		incrselIO.setPlanSuffix(covrclrIO.getPlanSuffix());
		incrselIO.setTranno(reverserec.tranno);
		incrselIO.setFunction(varcom.readh);
		incrselIO.setFormat(formatsInner.incrselrec);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)
		&& isNE(incrselIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(incrselIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		incrselIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
	}

protected void reverseFollowups6000()
	{
		try {
			begnh6110();
			delete6150();
		}
		catch (GOTOException e){
		}
	}

protected void begnh6110()
	{
		fluprevIO.setParams(SPACES);
		fluprevIO.setChdrnum(wsaaChdrnumKey);
		fluprevIO.setChdrcoy(wsaaChdrcoyKey);
		fluprevIO.setTranno(reverserec.tranno);
		fluprevIO.setFormat(formatsInner.fluprevrec);
		fluprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		fluprevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, fluprevIO);
		if ((isNE(fluprevIO.getStatuz(),varcom.oK))
		&& (isNE(fluprevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(fluprevIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(fluprevIO.getChdrnum(),wsaaChdrnumKey))
		|| (isNE(fluprevIO.getChdrcoy(),wsaaChdrcoyKey))
		|| (isNE(fluprevIO.getTranno(),reverserec.tranno))
		|| (isEQ(fluprevIO.getStatuz(),varcom.endp))) {
			fluprevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit6190);
		}
	}

protected void delete6150()
	{
		fluprevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fluprevIO);
		if ((isNE(fluprevIO.getStatuz(),varcom.oK))
		&& (isNE(fluprevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(fluprevIO.getParams());
			xxxxFatalError();
		}
	}

protected void reverseFuperev6500()
	{
		try {
			begnh6510();
			delete6550();
		}
		catch (GOTOException e){
		}
	}

protected void begnh6510()
	{
		fuperevIO.setParams(SPACES);
		fuperevIO.setChdrnum(reverserec.chdrnum);
		fuperevIO.setChdrcoy(reverserec.company);
		fuperevIO.setTranno(reverserec.tranno);
		fuperevIO.setFormat(formatsInner.fuperevrec);
		fuperevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		fuperevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, fuperevIO);
		if ((isNE(fuperevIO.getStatuz(),varcom.oK))
		&& (isNE(fuperevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(fuperevIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(fuperevIO.getChdrnum(),reverserec.chdrnum))
		|| (isNE(fuperevIO.getChdrcoy(),reverserec.company))
		|| (isNE(fuperevIO.getTranno(),reverserec.tranno))
		|| (isEQ(fuperevIO.getStatuz(),varcom.endp))) {
			fuperevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit6590);
		}
	}

protected void delete6550()
	{
		fuperevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fuperevIO);
		if ((isNE(fuperevIO.getStatuz(),varcom.oK))
		&& (isNE(fuperevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(fuperevIO.getParams());
			xxxxFatalError();
		}
	}

protected void processAcmvs7000()
	{
		readAcmvs7210();
	}

protected void readAcmvs7210()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(wsaaChdrnumKey);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		acmvrevIO.setBatctrcde(SPACES);
		while ( !((isEQ(acmvrevIO.getStatuz(),varcom.endp))
		|| (isEQ(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)))) {
			SmartFileCode.execute(appVars, acmvrevIO);
			if ((isNE(acmvrevIO.getStatuz(),varcom.oK))
			&& (isNE(acmvrevIO.getStatuz(),varcom.endp))) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
			if ((isNE(acmvrevIO.getRldgcoy(),reverserec.company))
			|| (isNE(acmvrevIO.getRdocnum(),wsaaChdrnumKey))
			|| (isNE(acmvrevIO.getTranno(),reverserec.tranno))) {
				acmvrevIO.setStatuz(varcom.endp);
			}
			acmvrevIO.setFunction(varcom.nextr);
		}

		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvRecs7500();
		}

	}

protected void processAcmvOptical7010()
	{
		start7010();
	}

protected void start7010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			xxxxFatalError();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs7500();
		}

		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void reverseAcmvRecs7500()
	{
		readAcmv7510();
		nextAcmv7550();
	}

protected void readAcmv7510()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(wsaaChdrnumKey);
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(wsaaChdrcoyKey);
		lifacmvrec.genlcoy.set(wsaaChdrcoyKey);
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(chdrclmIO.getCntcurr());
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(wsaaChdrcoyKey);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(reverserec.newTranno);
		descIO.setDescitem(reverserec.newTranno);
		getDescription7600();
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrclmIO.getCnttype());
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void nextAcmv7550()
	{
		acmvrevIO.setFunction(varcom.nextr);
		if (isEQ(wsaaIoCall,"CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if ((isNE(acmvrevIO.getStatuz(),varcom.oK))
		&& (isNE(acmvrevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if ((isEQ(acmvrevIO.getStatuz(),varcom.endp))
		|| (isNE(acmvrevIO.getRldgcoy(),reverserec.company))
		|| (isNE(acmvrevIO.getRdocnum(),wsaaChdrnumKey))) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			nextAcmv7550();
			return ;
		}
	}

protected void getDescription7600()
	{
		para7610();
	}

protected void para7610()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			xxxxFatalError();
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
	}

protected void checkLoans8000()
	{
		/*START*/
		loanenqIO.setParams(SPACES);
		loanenqIO.setChdrcoy(reverserec.company);
		loanenqIO.setChdrnum(reverserec.chdrnum);
		loanenqIO.setLoanNumber(ZERO);
		loanenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loanenqIO.setFormat(formatsInner.loanenqrec);
		while ( !(isEQ(loanenqIO.getStatuz(),varcom.endp))) {
			processLoans8100();
		}

		/*EXIT*/
	}

protected void processLoans8100()
	{
		start8100();
	}

protected void start8100()
	{
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(),varcom.oK)
		&& isNE(loanenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			xxxxFatalError();
		}
		/* If no Loan records found for this contract, then  EXIT section  */
		if (isNE(reverserec.company,loanenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum,loanenqIO.getChdrnum())
		|| isEQ(loanenqIO.getStatuz(),varcom.endp)) {
			loanenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* get here, so we have found a Loan record.......                 */
		/*  does it need changing ???                                      */
		/*  check this by comparing the Last Tranno on the Loan record     */
		/* IF it matches, we will update record, otherwise we will NEXTR   */
		if (isEQ(loanenqIO.getLastTranno(),wsaaReversedTranno)) {
			obtainInterestDates8200();
			loanenqIO.setLastIntBillDate(wsaaLstInterestDate);
			loanenqIO.setNextIntBillDate(wsaaNxtInterestDate);
			loanenqIO.setValidflag("1");
			loanenqIO.setLastTranno(ZERO);
			loanenqIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, loanenqIO);
			if (isNE(loanenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(loanenqIO.getParams());
				syserrrec.statuz.set(loanenqIO.getStatuz());
				xxxxFatalError();
			}
		}
		loanenqIO.setFunction(varcom.nextr);
	}

protected void obtainInterestDates8200()
	{
		start8200();
	}

protected void start8200()
	{
		readT5645Table8300();
		/* Use entries on the T5645 record read to get Loan interest       */
		/*  ACMVs for this loan number.                                    */
		if (isEQ(loanenqIO.getLoanType(),"P")) {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstype.set(t5645rec.sacstype01);
		}
		else {
			if (isEQ(loanenqIO.getLoanType(),"A")) {
				wsaaSacscode.set(t5645rec.sacscode02);
				wsaaSacstype.set(t5645rec.sacstype02);
			}
			else {
				wsaaSacscode.set(t5645rec.sacscode03);
				wsaaSacstype.set(t5645rec.sacstype03);
			}
		}
		wsaaRldgacct.set(SPACES);
		wsaaLstInterestDate.set(ZERO);
		wsaaChdrnum.set(loanenqIO.getChdrnum());
		wsaaLoanNumber.set(loanenqIO.getLoanNumber());
		/*  We will begin reading on the ACMV original surrender           */
		/*  transactions and read backwards to get the movements           */
		/*  before the original surrender (nextp)                          */
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(wsaaChdrnumKey);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			searchAcmvRecs8400();
		}

		/*  if any interest records for the loan being processed           */
		/*  (before the surrender movements) i.e there is a value          */
		/*  in WSAA-LST-INTEREST-DATE use this otherwise no interest       */
		/*  has been calculated since the loan start date                  */
		if (isEQ(wsaaLstInterestDate,ZERO)) {
			wsaaLstInterestDate.set(loanenqIO.getLoanStartDate());
		}
		/*  calculate next interest date from the last interest            */
		/*  date retrieved (i.e the effective date on the relevant         */
		/*  acmv                                                           */
		readT6633Table8500();
		calcNextInterestDate8600();
	}

protected void readT5645Table8300()
	{
		start8300();
	}

protected void start8300()
	{
		/*  Read the accounting rules table                                */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(loanenqIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void searchAcmvRecs8400()
	{
		start8400();
	}

protected void start8400()
	{
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(acmvrevIO.getRldgcoy(),loanenqIO.getChdrcoy())
		|| isNE(acmvrevIO.getRdocnum(),loanenqIO.getChdrnum())
		|| isEQ(acmvrevIO.getStatuz(),varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		/*  Read the accounting rules table                                */
		wsaaAcmvTrcde.set(acmvrevIO.getBatctrcde());
		if (isLT(acmvrevIO.getTranno(),reverserec.tranno)
		&& isEQ(acmvrevIO.getSacscode(),wsaaSacscode)
		&& isEQ(acmvrevIO.getSacstyp(),wsaaSacstype)
		&& isEQ(acmvrevIO.getRldgacct(),wsaaRldgacct)
		&& (loanInterestBilling.isTrue()
		|| loanInterestCapital.isTrue())) {
			wsaaLstInterestDate.set(acmvrevIO.getEffdate());
			acmvrevIO.setStatuz(varcom.endp);
		}
		else {
			acmvrevIO.setFunction(varcom.nextp);
		}
	}

protected void readT6633Table8500()
	{
		start8500();
	}

protected void start8500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(loanenqIO.getChdrcoy());
		itdmIO.setItemtabl(t6633);
		wsaaT6633Cnttype.set(chdrclmIO.getCnttype());
		wsaaT6633Type.set(loanenqIO.getLoanType());
		itdmIO.setItemitem(wsaaT6633Key);
		itdmIO.setItmfrm(wsaaLstInterestDate);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),loanenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t6633)
		|| isNE(itdmIO.getItemitem(),wsaaT6633Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT6633Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e723);
			xxxxFatalError();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
	}

protected void calcNextInterestDate8600()
	{
		start8600();
	}

protected void start8600()
	{
		/* Check the interest  details on T6633 in the following           */
		/*  order: i) Calculate interest on Loan anniv ... Y/N             */
		/*        ii) Calculate interest on Policy anniv.. Y/N             */
		/*       iii) Check Int freq & whether a specific Day is chosen    */
		wsaaLoanDate.set(loanenqIO.getLoanStartDate());
		wsaaEffdate.set(wsaaLstInterestDate);
		wsaaContractDate.set(chdrclmIO.getOccdate());
		/* Check for loan anniversary flag set                             */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next loan anniv  */
		/*     date after the Effective date we are using now.             */
		if (isEQ(t6633rec.loanAnnivInterest,"Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(loanenqIO.getLoanStartDate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2,wsaaEffdate))) {
				callDatcon48700();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}

			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Check for contract anniversary flag set                         */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next contract    */
		/*     anniversary date after the Effective date we are using now. */
		if (isEQ(t6633rec.policyAnnivInterest,"Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(chdrclmIO.getOccdate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2,wsaaEffdate))) {
				callDatcon48700();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}

			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Get here so the next interest calc. date isn't based on loan    */
		/*  or contract anniversarys.                                      */
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified     */
		/* ...if not, use the Loan day                                     */
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet8800();
		}
		else {
			if (isNE(t6633rec.interestDay,ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate2.set(ZERO);
		datcon4rec.intDate1.set(wsaaNewDate);
		datcon4rec.freqFactor.set(1);
		/* check if table T6633 has a fixed frequency for interest calcs,  */
		/* ...if not, use 1 year as the default interest calc. frequency.  */
		if (isEQ(t6633rec.interestFrequency,SPACES)) {
			datcon4rec.frequency.set("01");
		}
		else {
			datcon4rec.frequency.set(t6633rec.interestFrequency);
		}
		while ( !(isGT(datcon4rec.intDate2,wsaaEffdate))) {
			callDatcon48700();
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}

		wsaaNxtInterestDate.set(datcon4rec.intDate2);
	}

protected void callDatcon48700()
	{
		/*START*/
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void dateSet8800()
	{
		/*START*/
		/* we have to check that the date we are going to call Datcon4     */
		/*  with is a valid from-date... ie IF the interest/capn day in    */
		/*  T6633 is > 28, we have to make sure the from-date isn't        */
		/*  something like 31/02/nnnn or 31/06/nnnn                        */
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
	}
	/*
	 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
	 */
	private static final class WsaaMonthCheckInner {

		private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
		private Validator january = new Validator(wsaaMonthCheck, 1);
		private Validator february = new Validator(wsaaMonthCheck, 2);
		private Validator march = new Validator(wsaaMonthCheck, 3);
		private Validator april = new Validator(wsaaMonthCheck, 4);
		private Validator may = new Validator(wsaaMonthCheck, 5);
		private Validator june = new Validator(wsaaMonthCheck, 6);
		private Validator july = new Validator(wsaaMonthCheck, 7);
		private Validator august = new Validator(wsaaMonthCheck, 8);
		private Validator september = new Validator(wsaaMonthCheck, 9);
		private Validator october = new Validator(wsaaMonthCheck, 10);
		private Validator november = new Validator(wsaaMonthCheck, 11);
		private Validator december = new Validator(wsaaMonthCheck, 12);
	}
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
			/* FORMATS */
		private FixedLengthStringData chdrclmrec = new FixedLengthStringData(10).init("CHDRCLMREC");
		private FixedLengthStringData clmdclmrec = new FixedLengthStringData(10).init("CLMDCLMREC");
		private FixedLengthStringData clmhclmrec = new FixedLengthStringData(10).init("CLMHCLMREC");
		private FixedLengthStringData covrclrrec = new FixedLengthStringData(10).init("COVRCLRREC");
		private FixedLengthStringData fluprevrec = new FixedLengthStringData(10).init("FLUPREVREC");
		private FixedLengthStringData fuperevrec = new FixedLengthStringData(10).init("FUPEREVREC");
		private FixedLengthStringData liferevrec = new FixedLengthStringData(10).init("LIFEREVREC");
		private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
		private FixedLengthStringData loanenqrec = new FixedLengthStringData(10).init("LOANENQREC");
		private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
		private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
		private FixedLengthStringData incrselrec = new FixedLengthStringData(10).init("INCRSELREC");
	}
}
