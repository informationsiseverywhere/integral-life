/*
 * File: P6755.java
 * Date: 30 August 2009 0:56:07
 * Author: Quipoz Limited
 * 
 * Class transformed from P6755.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.CwfdTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*                 Delete Windforward
*                 ------------------
*
*   This is a non-screen program which is accessed from the
*   Display Windforward screen when a selection is made.
*
*   It deletes the Contract Windforward record for the relevant
*   contract number and processing sequence number.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6755 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6755");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String cwfdrec = "CWFDREC";
		/*Contract Windforward Logical*/
	private CwfdTableDAM cwfdIO = new CwfdTableDAM();
	private Wssplife wssplife = new Wssplife();

	public P6755() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		cwfdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cwfdIO.getParams());
			syserrrec.statuz.set(cwfdIO.getStatuz());
			fatalError600();
		}
		cwfdIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cwfdIO.getParams());
			syserrrec.statuz.set(cwfdIO.getStatuz());
			fatalError600();
		}
	}

protected void screenEdit2000()
	{
		/*START*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		cwfdIO.setFunction(varcom.readh);
		cwfdIO.setFormat(cwfdrec);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		cwfdIO.setFunction(varcom.delet);
		cwfdIO.setFormat(cwfdrec);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
