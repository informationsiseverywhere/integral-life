package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: TpdbpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:35
 * Class transformed from TPDBPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class TpdbpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 128;
	public FixedLengthStringData tpdbrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData tpdbpfRecord = tpdbrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(tpdbrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(tpdbrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(tpdbrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(tpdbrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(tpdbrec);
	public PackedDecimalData orgamnt = DD.orgamnt.copy().isAPartOf(tpdbrec);
	public PackedDecimalData fromdate = DD.fromdate.copy().isAPartOf(tpdbrec);
	public PackedDecimalData todate = DD.todate.copy().isAPartOf(tpdbrec);
	public FixedLengthStringData tdbtdesc = DD.tdbtdesc.copy().isAPartOf(tpdbrec);
	public PackedDecimalData tdbtrate = DD.tdbtrate.copy().isAPartOf(tpdbrec);
	public PackedDecimalData tdbtamt = DD.tdbtamt.copy().isAPartOf(tpdbrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(tpdbrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(tpdbrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(tpdbrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(tpdbrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public TpdbpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for TpdbpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public TpdbpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for TpdbpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public TpdbpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for TpdbpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public TpdbpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("TPDBPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"CNTTYPE, " +
							"EFFDATE, " +
							"ORGAMNT, " +
							"FROMDATE, " +
							"TODATE, " +
							"TDBTDESC, " +
							"TDBTRATE, " +
							"TDBTAMT, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     cnttype,
                                     effdate,
                                     orgamnt,
                                     fromdate,
                                     todate,
                                     tdbtdesc,
                                     tdbtrate,
                                     tdbtamt,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		cnttype.clear();
  		effdate.clear();
  		orgamnt.clear();
  		fromdate.clear();
  		todate.clear();
  		tdbtdesc.clear();
  		tdbtrate.clear();
  		tdbtamt.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getTpdbrec() {
  		return tpdbrec;
	}

	public FixedLengthStringData getTpdbpfRecord() {
  		return tpdbpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setTpdbrec(what);
	}

	public void setTpdbrec(Object what) {
  		this.tpdbrec.set(what);
	}

	public void setTpdbpfRecord(Object what) {
  		this.tpdbpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(tpdbrec.getLength());
		result.set(tpdbrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}