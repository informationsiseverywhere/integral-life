package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:51
 * @author Quipoz
 */
public class St528screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St528ScreenVars sv = (St528ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.St528screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		St528ScreenVars screenVars = (St528ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.oragntnam.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.clrole.setClassString("");
		screenVars.lsurname.setClassString("");
		screenVars.lgivname.setClassString("");
		screenVars.salutl.setClassString("");
		screenVars.tsalutsd.setClassString("");
		screenVars.zkanasurname.setClassString(""); // ILIFE-8682
		screenVars.zkanagivname.setClassString(""); // ILIFE-8682
			}

/**
 * Clear all the variables in St528screen
 */
	public static void clear(VarModel pv) {
		St528ScreenVars screenVars = (St528ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.payrnum.clear();
		screenVars.payorname.clear();
		screenVars.agntnum.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.billfreq.clear();
		screenVars.mop.clear();
		screenVars.oragntnam.clear();
		screenVars.clntnum.clear();
		screenVars.clrole.clear();
		screenVars.lsurname.clear();
		screenVars.lgivname.clear();
		screenVars.salutl.clear();
		screenVars.tsalutsd.clear();
		screenVars.zkanasurname.clear(); // ILIFE-8682
		screenVars.zkanagivname.clear(); // ILIFE-8682
			}
}
