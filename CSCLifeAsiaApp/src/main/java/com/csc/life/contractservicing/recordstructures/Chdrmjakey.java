package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:21
 * Description:
 * Copybook name: CHDRMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrmjaKey = new FixedLengthStringData(64).isAPartOf(chdrmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrmjaKey, 0);
  	public FixedLengthStringData chdrmjaChdrnum = new FixedLengthStringData(8).isAPartOf(chdrmjaKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrmjaKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}