/*
 * File: Tr52ept.java
 * Date: December 3, 2013 3:59:26 AM ICT
 * Author: CSC
 * 
 * Class transformed from TR52EPT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR52E.
*
*
*
****************************************************************** ****
* </pre>
*/
public class Tr52ept extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr52erec tr52erec = new Tr52erec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr52ept() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr52erec.tr52eRec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo013.set(tr52erec.taxind05);
		generalCopyLinesInner.fieldNo007.set(tr52erec.taxind01);
		generalCopyLinesInner.fieldNo009.set(tr52erec.taxind02);
		generalCopyLinesInner.fieldNo010.set(tr52erec.taxind03);
		generalCopyLinesInner.fieldNo012.set(tr52erec.taxind04);
		generalCopyLinesInner.fieldNo014.set(tr52erec.taxind06);
		generalCopyLinesInner.fieldNo016.set(tr52erec.taxind08);
		generalCopyLinesInner.fieldNo018.set(tr52erec.taxind10);
		generalCopyLinesInner.fieldNo015.set(tr52erec.taxind07);
		generalCopyLinesInner.fieldNo017.set(tr52erec.taxind09);
		generalCopyLinesInner.fieldNo019.set(tr52erec.taxind11);
		generalCopyLinesInner.fieldNo020.set(tr52erec.txitem);
		generalCopyLinesInner.fieldNo008.set(tr52erec.zbastyp);
		generalCopyLinesInner.fieldNo011.set(tr52erec.taxind12);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine001, 35, FILLER).init("Tax Control Table                   SR52E");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(11);
	private FixedLengthStringData filler9 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Tax Basis:");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(74);
	private FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine005, 8, FILLER).init("Modal Premium");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 30);
	private FixedLengthStringData filler12 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 31, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine005, 39, FILLER).init("(Tax Applied on Bais Premium");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 70);
	private FixedLengthStringData filler14 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 71, FILLER).init("  )");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(31);
	private FixedLengthStringData filler15 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine006, 8, FILLER).init("Contract Fee");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 30);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(64);
	private FixedLengthStringData filler17 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine007, 8, FILLER).init("Surrender Charge");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 30);
	private FixedLengthStringData filler19 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 42, FILLER).init("Reinstatement Fee");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 63);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(31);
	private FixedLengthStringData filler21 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine008, 8, FILLER).init("Agent Commission");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 30);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(25);
	private FixedLengthStringData filler23 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  (Non traditional rules)");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(31);
	private FixedLengthStringData filler24 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler25 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine010, 8, FILLER).init("Non Invest Premium");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 30);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(64);
	private FixedLengthStringData filler26 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler27 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine011, 8, FILLER).init("Mortality Charge");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 30);
	private FixedLengthStringData filler28 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine011, 31, FILLER).init(SPACES);
	private FixedLengthStringData filler29 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine011, 42, FILLER).init("Periodic Fee");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 63);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(64);
	private FixedLengthStringData filler30 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler31 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine012, 8, FILLER).init("Contract Issue");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 30);
	private FixedLengthStringData filler32 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 31, FILLER).init(SPACES);
	private FixedLengthStringData filler33 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine012, 42, FILLER).init("Admin Charges");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 63);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(64);
	private FixedLengthStringData filler34 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler35 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine013, 8, FILLER).init("Top up Fee");
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 30);
	private FixedLengthStringData filler36 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine013, 31, FILLER).init(SPACES);
	private FixedLengthStringData filler37 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine013, 42, FILLER).init("Switch Fee");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 63);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(34);
	private FixedLengthStringData filler38 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler39 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine014, 6, FILLER).init("Tax Rate Item      :");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 30);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(28);
	private FixedLengthStringData filler40 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
}
}
