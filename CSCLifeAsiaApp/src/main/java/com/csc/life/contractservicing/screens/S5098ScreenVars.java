package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5098
 * @version 1.0 generated on 30/08/09 06:34
 * @author Quipoz
 */
public class S5098ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(583);
	public FixedLengthStringData dataFields = new FixedLengthStringData(247).isAPartOf(dataArea, 0);
	public ZonedDecimalData adjustedArb = DD.adjarb.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData adjBonusDecDate = DD.adjbondate.copyToZonedDecimal().isAPartOf(dataFields,17);
	public ZonedDecimalData arbAdjustAmt = DD.arbadjamt.copyToZonedDecimal().isAPartOf(dataFields,25);
	public ZonedDecimalData bonusDecDate = DD.bondecdate.copyToZonedDecimal().isAPartOf(dataFields,42);
	public ZonedDecimalData bonusValue = DD.bonusvalue.copyToZonedDecimal().isAPartOf(dataFields,50);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData cntdesc = DD.cntdesc.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,111);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,113);
	public FixedLengthStringData crtabled = DD.crtabled.copy().isAPartOf(dataFields,117);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,147);
	public ZonedDecimalData instprem = DD.instprem.copyToZonedDecimal().isAPartOf(dataFields,155);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,221);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,229);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,233);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,237);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,245);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 247);
	public FixedLengthStringData adjarbErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData adjbondateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData arbadjamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bondecdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bonusvalueErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData crtabledErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData instpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 331);
	public FixedLengthStringData[] adjarbOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] adjbondateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] arbadjamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bondecdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bonusvalueOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] crtabledOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] instpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData adjBonusDecDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData bonusDecDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5098screenWritten = new LongData(0);
	public LongData S5098protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5098ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjbondateOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(arbadjamtOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, life, coverage, rider, cnttype, crtable, lifenum, cntdesc, crtabled, lifename, effdate, ptdate, numpols, planSuffix, cntcurr, bonusDecDate, adjBonusDecDate, bonusValue, arbAdjustAmt, adjustedArb, instprem};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, coverageOut, riderOut, cnttypeOut, crtableOut, lifenumOut, cntdescOut, crtabledOut, lifenameOut, effdateOut, ptdateOut, numpolsOut, plnsfxOut, cntcurrOut, bondecdateOut, adjbondateOut, bonusvalueOut, arbadjamtOut, adjarbOut, instpremOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, coverageErr, riderErr, cnttypeErr, crtableErr, lifenumErr, cntdescErr, crtabledErr, lifenameErr, effdateErr, ptdateErr, numpolsErr, plnsfxErr, cntcurrErr, bondecdateErr, adjbondateErr, bonusvalueErr, arbadjamtErr, adjarbErr, instpremErr};
		screenDateFields = new BaseData[] {effdate, ptdate, bonusDecDate, adjBonusDecDate};
		screenDateErrFields = new BaseData[] {effdateErr, ptdateErr, bondecdateErr, adjbondateErr};
		screenDateDispFields = new BaseData[] {effdateDisp, ptdateDisp, bonusDecDateDisp, adjBonusDecDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5098screen.class;
		protectRecord = S5098protect.class;
	}

}
