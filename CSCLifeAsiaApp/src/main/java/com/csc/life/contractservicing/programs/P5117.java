/*
 * File: P5117.java
 * Date: 30 August 2009 0:08:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P5117.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.screens.S5117ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UdelpfDAO;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*           P5117 - REVERSALS SUB MENU PROGRAM
*           ----------------------------------
*
* Overview
* --------
* This submenu provides the user with a selection of Reversal
*  transactions. Depending on what transaction is selected, the
*  program 'KEEPS' a copy of the current Contract Header CHDR
*  record for passing onto the next program in the processing
*  sequence.
*  Available Reversal transactions are...
*
*    A - Billing Reversal
*    B - Full contract Reversal
*    C - Lapse Reversal
*    D - Paid-up Reversal
*    E - Death Claim Reversal
*    F - Full Surrender Reversal
*    G - Maturity Reversal
*    H - Expiry Reversal
*    I - Manual Non-forfeiture Surrender Reversal
*    J - Regular Payment Reversal
*    K - Vesting Registration Reversal
*    L - First Death Registration Reversal
*
* Processing
* ----------
*
* The Contract is checked to make sure it exists.
* The Contract Risk Status is checked against the values
*  specified in T5679.
* The UDEL file is checked to make sure there are no unprocessed
*  UTRNs pending for the contract we are trying to work with.
*
* The following Contract Header CHDR logical views are used to
*  perform the KEEPS function to pass the CHDR details to the
*  next program in the processing chain.
*
*   Option      Logical view
* --------------------------
*   A or B   -    CHDRLIF
*   C or D   -    CHDRMJA
*   E or J   -    CHDRCLM
*   F or I   -    CHDRSUR
*   G or H   -    CHDRMAT
*   K        -    CHDRMJA
*   L        -    CHDRCLM
*
* Updating
* --------
*
*  Soft lock the contract header (call SFTLOCK). If the contract
*  is already locked, display an error message.
*
* Tables Used
* -----------
*
* T5679 - Status Requirements for transactions
*
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Check for unprocessed HITRs. If any exist, do not allow the         *
* transaction to proceed.                                             *
*                                                                     *
*****************************************************************
* </pre>
*/
public class P5117 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5117");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();

	protected FixedLengthStringData wsaaContractFoundFlag = new FixedLengthStringData(1);
	private Validator wsaaContractFound = new Validator(wsaaContractFoundFlag, "Y");
	private Validator wsaaContractNotFound = new Validator(wsaaContractFoundFlag, "N");

	protected FixedLengthStringData wsaaChdrDetails = new FixedLengthStringData(17);
	private FixedLengthStringData wsaaChdrnumStore = new FixedLengthStringData(8).isAPartOf(wsaaChdrDetails, 0);
	private FixedLengthStringData wsaaChdrcoyStore = new FixedLengthStringData(1).isAPartOf(wsaaChdrDetails, 8);
	private FixedLengthStringData wsaaChdrBranchStore = new FixedLengthStringData(2).isAPartOf(wsaaChdrDetails, 9);
	private FixedLengthStringData wsaaChdrStatuzStore = new FixedLengthStringData(4).isAPartOf(wsaaChdrDetails, 11);
	private FixedLengthStringData wsaaChdrStatStore = new FixedLengthStringData(2).isAPartOf(wsaaChdrDetails, 15);
	protected PackedDecimalData wsaaTrannoStore = new PackedDecimalData(5, 0);
		/* ERRORS */
	private static final String e070 = "E070";
	protected static final String f910 = "F910";
	private static final String e073 = "E073";
	private static final String e544 = "E544";
	private static final String e455 = "E455";
	private static final String e767 = "E767";
	private static final String f918 = "F918";
	private static final String i030 = "I030";
	private static final String g552 = "G552";
	private static final String hl08 = "HL08";
	private static final String e931 = "E931";
	private static final String e005 = "E005";
	private static final String jl60 = "JL60";
	private static final String f917 = "F917";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6661 = "T6661";
	/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrclmrec = "CHDRCLMREC";
	private static final String chdrsurrec = "CHDRSURREC";
	private static final String chdrmatrec = "CHDRMATREC";
		/* Dummy user wssp. Replace with required version.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	protected ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private Chdrpf chdrpf=new Chdrpf();
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	protected ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	protected ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	
	protected Batckey wsaaBatchkey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private T6661rec t6661rec = new T6661rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	protected Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	//ILB-552
	private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	private UdelpfDAO udelpfDAO = getApplicationContext().getBean("udelpfDAO", UdelpfDAO.class);
	private CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	private Itempf itempf = null;
	boolean CMDTH010Permission  = false;
	private static final String feaConfigPreRegistartion= "CMDTH010";
	
	protected S5117ScreenVars sv = getLScreenVars();//ScreenProgram.getScreenVars( S5117ScreenVars.class);

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		chdrmja2220, 
		chdrclm2240, 
		chdrsur2250, 
		chdrmat2260, 
		continue2280, 
		exit2290, 
		search2510, 
		exit12090,
		chdrmja3072, 
		chdrclm3075, 
		chdrsur3077, 
		chdrmat3078,
		batching3080, 
		exit3090
	}

	public P5117() {
		super();
		screenVars = sv;
		new ScreenModel("S5117", AppVars.getInstance(), sv);
	}
	protected S5117ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5117ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
				if (isEQ(sv.action,"A")
				&& isEQ(sv.errorIndicators,SPACES)) {
					billingReversal2700();
				}
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				validateKey12210();
				chdrlif2215();
			case chdrmja2220: 
				chdrmja2220();
			case chdrclm2240: 
				chdrclm2240();
			case chdrsur2250: 
				chdrsur2250();
			case chdrmat2260: 
				chdrmat2260();
			case continue2280: 
				continue2280();
			case exit2290: 
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}

	}

protected void validateKey12210()
{
	wsaaContractFoundFlag.set("Y");
	wsaaChdrDetails.set(SPACES);
	wsaaTrannoStore.set(ZERO);
	if (isEQ(subprogrec.key1,SPACES)) {
		goTo(GotoLabel.exit2290);
	}
	if (isEQ(sv.chdrsel,SPACES)) {
		wsaaContractFoundFlag.set("N");
		goTo(GotoLabel.continue2280);
	}
	if (isEQ(sv.action,"C")
	|| isEQ(sv.action,"D")
	|| isEQ(sv.action,"K")
	|| isEQ(sv.action,"N")) {
		goTo(GotoLabel.chdrmja2220);
	}
	if (isEQ(sv.action,"E")
	|| isEQ(sv.action,"J")
	|| isEQ(sv.action,"L")
	|| isEQ(sv.action,"M")) {
		goTo(GotoLabel.chdrclm2240);
	}
	if (isEQ(sv.action,"F")
	|| isEQ(sv.action,"I")) {
		goTo(GotoLabel.chdrsur2250);
	}
	if (isEQ(sv.action,"G")
	|| isEQ(sv.action,"H")) {
		goTo(GotoLabel.chdrmat2260);
	}
	if (!CMDTH010Permission && isEQ(sv.action, "Q")) {
		sv.actionErr.set(e005);
	}
	if(CMDTH010Permission && isEQ(sv.action,"Q")){
		cattpf2270();
	}
}

protected void chdrlif2215()
{
if (isNE(sv.chdrsel , SPACES)) 
{
	chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString(), sv.chdrsel.toString());
}


if(null==chdrpf)
{
	wsaaContractFoundFlag.set("N");
	goTo(GotoLabel.continue2280);
	//wsaaChdrStatuzStore.set(varcom.mrnf);
}

wsaaTrannoStore.set(chdrpf.getTranno());
wsaaChdrnumStore.set(chdrpf.getChdrnum());
wsaaChdrcoyStore.set(chdrpf.getChdrcoy());
wsaaChdrStatStore.set(chdrpf.getStatcode());
wsaaChdrBranchStore.set(chdrpf.getCntbranch());
wsaaChdrStatuzStore.set("****");
//wsaaChdrStatuzStore.set(varcom.oK);

//wsaaChdrStatuzStore.set(chdrpf.getStatuz());

goTo(GotoLabel.continue2280);
	/*chdrlifIO.setChdrcoy(wsspcomn.company);
	chdrlifIO.setChdrnum(sv.chdrsel);
	chdrlifIO.setFunction(varcom.readr);
	if (isNE(sv.chdrsel,SPACES)) {
		SmartFileCode.execute(appVars, chdrlifIO);
	}
	else {
		chdrlifIO.setStatuz(varcom.mrnf);
	}
	if (isNE(chdrlifIO.getStatuz(),varcom.oK)
	&& isNE(chdrlifIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(chdrlifIO.getParams());
		fatalError600();
	}
	if (isEQ(chdrlifIO.getStatuz(),varcom.mrnf)) {
		wsaaContractFoundFlag.set("N");
		goTo(GotoLabel.continue2280);
	}
	wsaaTrannoStore.set(chdrlifIO.getTranno());
	wsaaChdrnumStore.set(chdrlifIO.getChdrnum());
	wsaaChdrcoyStore.set(chdrlifIO.getChdrcoy());
	wsaaChdrStatStore.set(chdrlifIO.getStatcode());
	wsaaChdrBranchStore.set(chdrlifIO.getCntbranch());
	wsaaChdrStatuzStore.set(chdrlifIO.getStatuz());
	goTo(GotoLabel.continue2280);*/
}

protected void chdrmja2220()
{
	chdrmjaIO.setChdrcoy(wsspcomn.company);
	chdrmjaIO.setChdrnum(sv.chdrsel);
	chdrmjaIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, chdrmjaIO);
	if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
	&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(chdrmjaIO.getParams());
		fatalError600();
	}
	if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)) {
		wsaaContractFoundFlag.set("N");
		goTo(GotoLabel.continue2280);
	}
	wsaaTrannoStore.set(chdrmjaIO.getTranno());
	wsaaChdrnumStore.set(chdrmjaIO.getChdrnum());
	wsaaChdrcoyStore.set(chdrmjaIO.getChdrcoy());
	wsaaChdrStatStore.set(chdrmjaIO.getStatcode());
	wsaaChdrBranchStore.set(chdrmjaIO.getCntbranch());
	wsaaChdrStatuzStore.set(chdrmjaIO.getStatuz());
	goTo(GotoLabel.continue2280);
}



protected void chdrclm2240()
{
	chdrclmIO.setChdrcoy(wsspcomn.company);
	chdrclmIO.setChdrnum(sv.chdrsel);
	chdrclmIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, chdrclmIO);
	if ((isNE(chdrclmIO.getStatuz(),varcom.oK))
	&& (isNE(chdrclmIO.getStatuz(),varcom.mrnf))) {
		syserrrec.params.set(chdrclmIO.getParams());
		fatalError600();
	}
	if (isEQ(chdrclmIO.getStatuz(),varcom.mrnf)) {
		wsaaContractFoundFlag.set("N");
		goTo(GotoLabel.continue2280);
	}
	wsaaTrannoStore.set(chdrclmIO.getTranno());
	wsaaChdrnumStore.set(chdrclmIO.getChdrnum());
	wsaaChdrcoyStore.set(chdrclmIO.getChdrcoy());
	wsaaChdrStatStore.set(chdrclmIO.getStatcode());
	wsaaChdrBranchStore.set(chdrclmIO.getCntbranch());
	wsaaChdrStatuzStore.set(chdrclmIO.getStatuz());
	goTo(GotoLabel.continue2280);
}

protected void chdrsur2250()
{
	chdrsurIO.setChdrcoy(wsspcomn.company);
	chdrsurIO.setChdrnum(sv.chdrsel);
	chdrsurIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, chdrsurIO);
	if ((isNE(chdrsurIO.getStatuz(),varcom.oK))
	&& (isNE(chdrsurIO.getStatuz(),varcom.mrnf))) {
		syserrrec.params.set(chdrsurIO.getParams());
		fatalError600();
	}
	if (isEQ(chdrsurIO.getStatuz(),varcom.mrnf)) {
		wsaaContractFoundFlag.set("N");
		goTo(GotoLabel.continue2280);
	}
	wsaaTrannoStore.set(chdrsurIO.getTranno());
	wsaaChdrnumStore.set(chdrsurIO.getChdrnum());
	wsaaChdrcoyStore.set(chdrsurIO.getChdrcoy());
	wsaaChdrStatStore.set(chdrsurIO.getStatcode());
	wsaaChdrBranchStore.set(chdrsurIO.getCntbranch());
	wsaaChdrStatuzStore.set(chdrsurIO.getStatuz());
	goTo(GotoLabel.continue2280);
}

protected void chdrmat2260()
{
	chdrmatIO.setChdrcoy(wsspcomn.company);
	chdrmatIO.setChdrnum(sv.chdrsel);
	chdrmatIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, chdrmatIO);
	if ((isNE(chdrmatIO.getStatuz(),varcom.oK))
	&& (isNE(chdrmatIO.getStatuz(),varcom.mrnf))) {
		syserrrec.params.set(chdrmatIO.getParams());
		fatalError600();
	}
	if (isEQ(chdrmatIO.getStatuz(),varcom.mrnf)) {
		wsaaContractFoundFlag.set("N");
		goTo(GotoLabel.continue2280);
	}
	wsaaTrannoStore.set(chdrmatIO.getTranno());
	wsaaChdrnumStore.set(chdrmatIO.getChdrnum());
	wsaaChdrcoyStore.set(chdrmatIO.getChdrcoy());
	wsaaChdrStatStore.set(chdrmatIO.getStatcode());
	wsaaChdrBranchStore.set(chdrmatIO.getCntbranch());
	wsaaChdrStatuzStore.set(chdrmatIO.getStatuz());
}

protected void cattpf2270(){
	if (isNE(sv.chdrsel , SPACES)) 
	{
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString(), sv.chdrsel.toString());
	}
	if(null==chdrpf)
	{
		wsaaContractFoundFlag.set("N");
		goTo(GotoLabel.continue2280);
	}
	Cattpf cattpf = cattpfDAO.selectRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
	if(null == cattpf.getClaim()){
		sv.chdrselErr.set(jl60);
		return;	
	}
    else if(!cattpf.getClamstat().equals("PR")){
		sv.chdrselErr.set(e931);
	}
}
protected void continue2280()
	{
		if (wsaaContractNotFound.isTrue()
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(e544);
			if(CMDTH010Permission && isEQ(sv.action,"Q")) {
				sv.chdrselErr.set(f917);
				goTo(GotoLabel.exit2290);
			}
		}
		if (isEQ(wsaaChdrStatuzStore,varcom.oK)
		&& isEQ(subprogrec.key1,"N")) {
			sv.chdrselErr.set(f918);
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				return ;
			}
		}
		/*    For transactions on policies,*/
		/*       check the contract selected is of the correct status*/
		/*       and from correct branch.*/
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)
		&& isNE(wsspcomn.branch,wsaaChdrBranchStore)) {
			sv.chdrselErr.set(e455);
		}
		if ((isEQ(subprogrec.key1,"Y"))
		&& (isEQ(sv.chdrselErr,SPACES))) {
			checkUdel2600();
			if (isEQ(sv.chdrselErr,SPACES)) {
				checkHitr2650();
			}
		}
	}

protected void checkStatus2400()
{
	readStatusTable2410();
}

protected void readStatusTable2410()
{
	List<Itempf> itempfList = itemDAO.findItem("IT", wsspcomn.company.toString(), t5679, subprogrec.transcd.toString());

	if(itempfList!=null && !itempfList.isEmpty()){
		
		Itempf itempf = itempfList.get(0);
		
		if(itempf==null )
		{
	    
		
		fatalError600();
			
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	
	   /* itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());*/
		wsaaSub.set(ZERO);
		lookForStat2500();
	}

protected void lookForStat2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case search2510: {
					search2510();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search2510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.chdrselErr.set(e767);
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(wsaaChdrStatStore,t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search2510);
			}
		}
		/*EXIT*/
	}

protected void checkUdel2600()
   {
		go2610();
	}

protected void go2610()
   {
			//"

	    boolean existflag = udelpfDAO.isExistUdelRecord(wsaaChdrcoyStore.toString(), wsaaChdrnumStore.toString());
		/* If we are doing a full contract reversal and a UDEL exists      */
		/* the transaction cannot proceed and we issue error I030.         */
		/* However if we are doing a reversal of a Fund Switch or a        */
		/* part surrender for the transaction to be allowed a UDEL MUST    */
		/* exist.                                                          */
		if (isEQ(sv.action, "B") && existflag) {
			sv.chdrselErr.set(i030);
		}
		if (isEQ(sv.action, "O")
		|| isEQ(sv.action, "P")) {
		if (!existflag) {
				sv.chdrselErr.set(g552);
		}
		}
	}

protected void checkHitr2650()
   {
	hitr2651();
   }

protected void hitr2651()
   {
	    List<Hitrpf> list = hitrpfDAO.searchHitrrnlRecord(wsaaChdrcoyStore.toString(), wsaaChdrnumStore.toString());
		//ILIFE-8526
		if (!list.isEmpty()) {
			sv.chdrselErr.set(hl08);
		}
	}

protected void billingReversal2700()
   {
	start2700();
   }

protected void start2700()
   {
	List<Itempf> itempfList = itemDAO.findItem("IT", wsspcomn.company.toString(), t6661, subprogrec.transcd.toString());

	if(itempfList!=null && !itempfList.isEmpty()){
		
		Itempf itempf = itempfList.get(0);
		
		if(itempf==null )
		{
	    
		
		fatalError600();
			
		}
		t6661rec.t6661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		/*itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6661);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());*/
		List<Ptrnpf> list = ptrnpfDAO.searchPtrnrevData(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString());
		
		if(list!=null && list.size()>0) {
			for(Ptrnpf ptrnpf : list) {
				if(t6661rec.trcode.trim().equals(ptrnpf.getBatctrcde().trim())) {
					sv.actionErr.set(g552);
					wsspcomn.edterror.set("Y");
					break;
				}
			}
		}	
		
	}


protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateWssp3010();
					keeps3070();
					chdrlif3071();
				}
				case chdrmja3072: {
					chdrmja3072();
				}
				case chdrclm3075: {
					chdrclm3075();
				}
				case chdrsur3077: {
					chdrsur3077();
				}
				case chdrmat3078: {
					chdrmat3078();
				}				
				case batching3080: {
					batching3080();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		
		if(CMDTH010Permission){
		if (isEQ(scrnparams.action, "Q")){
			wsspcomn.flag.set("M");
		}
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		
		if (isEQ(sv.action,"A")
		|| isEQ(sv.action, "B")
		|| isEQ(sv.action, "O")
		|| isEQ(sv.action, "P")
		|| isEQ(sv.action, "Q")) {//ILJ-383
		  sftlockrec.entity.set(chdrpf.getChdrnum());
		}
		if (isEQ(sv.action,"C")
		|| isEQ(sv.action,"D")
		|| isEQ(sv.action,"K")
		|| isEQ(sv.action,"N")) {
		   sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		}
		if (isEQ(sv.action,"E")
		|| isEQ(sv.action,"J")
		|| isEQ(sv.action,"L")
		|| isEQ(sv.action,"M")) {
			sftlockrec.entity.set(chdrclmIO.getChdrnum());
		}
		if (isEQ(sv.action,"F")
		|| isEQ(sv.action,"I")) {
			sftlockrec.entity.set(chdrsurIO.getChdrnum());
		}
	   if (isEQ(sv.action,"G")
	   || isEQ(sv.action,"H")) {
		    sftlockrec.entity.set(chdrmatIO.getChdrnum());
	   }
				
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		if ((isEQ(subprogrec.key1,"Y"))
		&& (isEQ(sv.errorIndicators,SPACES))) {
			wsaaTrannoStore.add(1);
		}
	}

protected void keeps3070()
	{
	if (isEQ(sv.action,"C")
			|| isEQ(sv.action,"K")
			|| isEQ(sv.action,"N")
			|| isEQ(sv.action,"D")) {
				goTo(GotoLabel.chdrmja3072);
			}
			if (isEQ(sv.action,"E")
			|| isEQ(sv.action,"J")
			|| isEQ(sv.action,"L")
			|| isEQ(sv.action,"M")) {
				goTo(GotoLabel.chdrclm3075);
			}
			if (isEQ(sv.action,"F")
			|| isEQ(sv.action,"I")) {
				goTo(GotoLabel.chdrsur3077);
			}
			if (isEQ(sv.action,"G")
			|| isEQ(sv.action,"H")) {
				goTo(GotoLabel.chdrmat3078);
			}
	}

protected void chdrlif3071()
{
 chdrpf.setTranno(wsaaTrannoStore.toInt());
	chdrpfDAO.setCacheObject(chdrpf);	
//chdrlifIO.setTranno(wsaaTrannoStore);
	
	/*chdrlifIO.setFunction("KEEPS");
	chdrlifIO.setFormat(chdrlifrec);
	SmartFileCode.execute(appVars, chdrlifIO);
	if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrlifIO.getParams());
		fatalError600();
	}*/
	goTo(GotoLabel.batching3080);
}

protected void chdrmja3072()
{
	chdrmjaIO.setTranno(wsaaTrannoStore);
	chdrmjaIO.setFunction("KEEPS");
	chdrmjaIO.setFormat(chdrmjarec);
	SmartFileCode.execute(appVars, chdrmjaIO);
	if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrmjaIO.getParams());
		fatalError600();
	}
	goTo(GotoLabel.batching3080);
}

protected void chdrclm3075()
{
	chdrclmIO.setTranno(wsaaTrannoStore);
	chdrclmIO.setFunction(varcom.keeps);
	chdrclmIO.setFormat(chdrclmrec);
	SmartFileCode.execute(appVars, chdrclmIO);
	if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrclmIO.getParams());
		fatalError600();
	}
	goTo(GotoLabel.batching3080);
}

protected void chdrsur3077()
{
	chdrsurIO.setTranno(wsaaTrannoStore);
	chdrsurIO.setFunction(varcom.keeps);
	chdrsurIO.setFormat(chdrsurrec);
	SmartFileCode.execute(appVars, chdrsurIO);
	if (isNE(chdrsurIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrsurIO.getParams());
		fatalError600();
	}
	goTo(GotoLabel.batching3080);
}

protected void chdrmat3078()
{
	chdrmatIO.setFunction(varcom.keeps);
	chdrmatIO.setFormat(chdrmatrec);
	SmartFileCode.execute(appVars, chdrmatIO);
	if (isNE(chdrmatIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrmatIO.getParams());
		fatalError600();
	}
	goTo(GotoLabel.batching3080);
}



protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
