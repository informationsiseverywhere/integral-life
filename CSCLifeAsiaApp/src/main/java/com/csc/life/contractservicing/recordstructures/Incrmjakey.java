package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:59
 * Description:
 * Copybook name: INCRMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incrmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incrmjaKey = new FixedLengthStringData(64).isAPartOf(incrmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData incrmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(incrmjaKey, 0);
  	public FixedLengthStringData incrmjaChdrnum = new FixedLengthStringData(8).isAPartOf(incrmjaKey, 1);
  	public FixedLengthStringData incrmjaLife = new FixedLengthStringData(2).isAPartOf(incrmjaKey, 9);
  	public FixedLengthStringData incrmjaCoverage = new FixedLengthStringData(2).isAPartOf(incrmjaKey, 11);
  	public FixedLengthStringData incrmjaRider = new FixedLengthStringData(2).isAPartOf(incrmjaKey, 13);
  	public PackedDecimalData incrmjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incrmjaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(incrmjaKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incrmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incrmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}