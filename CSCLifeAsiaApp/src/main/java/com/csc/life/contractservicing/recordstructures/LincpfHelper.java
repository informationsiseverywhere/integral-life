package com.csc.life.contractservicing.recordstructures;

import java.math.BigDecimal;

/**
 * Helper class for passing data to subroutines for
 * LINC processing.
 * 
 * @author gsaluja2
 *
 */
public class LincpfHelper {
	private String clntnum;
	private String chdrcoy;
	private String chdrnum;
	private String owncoy;
	private String cownnum;
	private String lifcnum;
	private Integer cltdob;
	private Integer lcltdob;
	private String cltsex;
	private String lcltsex;
	private String clttype;
	private String zkanasnm;
	private String zkanagnm;
	private String zkanaddr01;
	private String zkanaddr02;
	private String zkanaddr03;
	private String zkanaddr04;
	private String zkanaddr05;
	private String givname;
	private String surname;
	private String lzkanasnm;
	private String lzkanagnm;
	private String lzkanaddr01;
	private String lzkanaddr02;
	private String lzkanaddr03;
	private String lzkanaddr04;
	private String lzkanaddr05;
	private String lgivname;
	private String lsurname;
	private int lifeage;
	private String cnttype;
	private int occdate;
	private int transactionDate;
	private String transcd;
	private String language;
	private boolean critSatisfied;
	private BigDecimal sumInsured;
	private BigDecimal hosbensSI;
	private int cdateduration;
	private int juvenileage;
	private int tranno;
	private int ptrnEff;
	private int trdt;
	private int trtm;
	private String termId;
	private String usrprf;
	private String jobnm;
	
	public LincpfHelper() {
		//does nothing
	}
	
	/**
	 * @return the clntnum
	 */
	public String getClntnum() {
		return clntnum;
	}
	/**
	 * @param clntnum the clntnum to set
	 */
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	/**
	 * @return the chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}
	/**
	 * @param chdrcoy the chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	/**
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}
	/**
	 * @param chdrnum the chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	/**
	 * @return the owncoy
	 */
	public String getOwncoy() {
		return owncoy;
	}
	/**
	 * @param owncoy the owner company to set
	 */
	public void setOwncoy(String owncoy) {
		this.owncoy = owncoy;
	}
	/**
	 * @return the cownnum
	 */
	public String getCownnum() {
		return cownnum;
	}
	/**
	 * @param cownnum the cownnum to set
	 */
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}
	/**
	 * @return the lifcnum
	 */
	public String getLifcnum() {
		return lifcnum;
	}
	/**
	 * @param lifcnum the lifcnum to set
	 */
	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}
	/**
	 * @return the cltdob
	 */
	public Integer getCltdob() {
		return cltdob;
	}
	/**
	 * @param cltdob the cltdob to set
	 */
	public void setCltdob(Integer cltdob) {
		this.cltdob = cltdob;
	}
	/**
	 * @return the lcltdob
	 */
	public Integer getLcltdob() {
		return lcltdob;
	}
	/**
	 * @param lcltdob the lcltdob to set
	 */
	public void setLcltdob(Integer lcltdob) {
		this.lcltdob = lcltdob;
	}
	/**
	 * @return the cltsex
	 */
	public String getCltsex() {
		return cltsex;
	}
	/**
	 * @param cltsex the cltsex to set
	 */
	public void setCltsex(String cltsex) {
		this.cltsex = cltsex;
	}
	/**
	 * @return the lcltsex
	 */
	public String getLcltsex() {
		return lcltsex;
	}
	/**
	 * @param lcltsex the lcltsex to set
	 */
	public void setLcltsex(String lcltsex) {
		this.lcltsex = lcltsex;
	}
	/**
	 * @return the clttype
	 */
	public String getClttype() {
		return clttype;
	}
	/**
	 * @param clttype the clttype to set
	 */
	public void setClttype(String clttype) {
		this.clttype = clttype;
	}
	/**
	 * @return the zkanasnm
	 */
	public String getZkanasnm() {
		return zkanasnm;
	}
	/**
	 * @param zkanasnm the zkanasnm to set
	 */
	public void setZkanasnm(String zkanasnm) {
		this.zkanasnm = zkanasnm;
	}
	/**
	 * @return the zkanagnm
	 */
	public String getZkanagnm() {
		return zkanagnm;
	}
	/**
	 * @param zkanagnm the zkanagnm to set
	 */
	public void setZkanagnm(String zkanagnm) {
		this.zkanagnm = zkanagnm;
	}
	/**
	 * @return the zkanaddr01
	 */
	public String getZkanaddr01() {
		return zkanaddr01;
	}
	/**
	 * @param zkanaddr01 the zkanaddr01 to set
	 */
	public void setZkanaddr01(String zkanaddr01) {
		this.zkanaddr01 = zkanaddr01;
	}
	/**
	 * @return the zkanaddr02
	 */
	public String getZkanaddr02() {
		return zkanaddr02;
	}
	/**
	 * @param zkanaddr02 the zkanaddr02 to set
	 */
	public void setZkanaddr02(String zkanaddr02) {
		this.zkanaddr02 = zkanaddr02;
	}
	/**
	 * @return the zkanaddr03
	 */
	public String getZkanaddr03() {
		return zkanaddr03;
	}
	/**
	 * @param zkanaddr03 the zkanaddr03 to set
	 */
	public void setZkanaddr03(String zkanaddr03) {
		this.zkanaddr03 = zkanaddr03;
	}
	/**
	 * @return the zkanaddr04
	 */
	public String getZkanaddr04() {
		return zkanaddr04;
	}
	/**
	 * @param zkanaddr04 the zkanaddr04 to set
	 */
	public void setZkanaddr04(String zkanaddr04) {
		this.zkanaddr04 = zkanaddr04;
	}
	/**
	 * @return the zkanaddr05
	 */
	public String getZkanaddr05() {
		return zkanaddr05;
	}
	/**
	 * @param zkanaddr05 the zkanaddr05 to set
	 */
	public void setZkanaddr05(String zkanaddr05) {
		this.zkanaddr05 = zkanaddr05;
	}
	/**
	 * @return the Kanji givname of client
	 */
	public String getGivname() {
		return givname;
	}
	/**
	 * @param givname the Kanji givname of client to set
	 */
	public void setGivname(String givname) {
		this.givname = givname;
	}
	/**
	 * @return the Kanji surname of client
	 */
	public String getSurname() {
		return surname;
	}
	/**
	 * @param surname the Kanji surname of client to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	/**
	 * @return the lzkanasnm
	 */
	public String getLzkanasnm() {
		return lzkanasnm;
	}
	/**
	 * @param lzkanasnm the lzkanasnm to set
	 */
	public void setLzkanasnm(String lzkanasnm) {
		this.lzkanasnm = lzkanasnm;
	}
	/**
	 * @return the lzkanagnm
	 */
	public String getLzkanagnm() {
		return lzkanagnm;
	}
	/**
	 * @param lzkanagnm the lzkanagnm to set
	 */
	public void setLzkanagnm(String lzkanagnm) {
		this.lzkanagnm = lzkanagnm;
	}
	/**
	 * @return the lzkanaddr01
	 */
	public String getLzkanaddr01() {
		return lzkanaddr01;
	}
	/**
	 * @param lzkanaddr01 the lzkanaddr01 to set
	 */
	public void setLzkanaddr01(String lzkanaddr01) {
		this.lzkanaddr01 = lzkanaddr01;
	}
	/**
	 * @return the lzkanaddr02
	 */
	public String getLzkanaddr02() {
		return lzkanaddr02;
	}
	/**
	 * @param lzkanaddr02 the lzkanaddr02 to set
	 */
	public void setLzkanaddr02(String lzkanaddr02) {
		this.lzkanaddr02 = lzkanaddr02;
	}
	/**
	 * @return the lzkanaddr03
	 */
	public String getLzkanaddr03() {
		return lzkanaddr03;
	}
	/**
	 * @param lzkanaddr03 the lzkanaddr03 to set
	 */
	public void setLzkanaddr03(String lzkanaddr03) {
		this.lzkanaddr03 = lzkanaddr03;
	}
	/**
	 * @return the lzkanaddr04
	 */
	public String getLzkanaddr04() {
		return lzkanaddr04;
	}
	/**
	 * @param lzkanaddr04 the lzkanaddr04 to set
	 */
	public void setLzkanaddr04(String lzkanaddr04) {
		this.lzkanaddr04 = lzkanaddr04;
	}
	/**
	 * @return the lzkanaddr05
	 */
	public String getLzkanaddr05() {
		return lzkanaddr05;
	}
	/**
	 * @param lzkanaddr05 the lzkanaddr05 to set
	 */
	public void setLzkanaddr05(String lzkanaddr05) {
		this.lzkanaddr05 = lzkanaddr05;
	}
	/**
	 * @return the Kanji givname of life assured
	 */
	public String getLgivname() {
		return lgivname;
	}
	/**
	 * @param lgivname the Kanji givname of life assured to set
	 */
	public void setLgivname(String lgivname) {
		this.lgivname = lgivname;
	}
	/**
	 * @return the Kanji surname of life assured
	 */
	public String getLsurname() {
		return lsurname;
	}
	/**
	 * @param lsurname the Kanji surname to set of life assured
	 */
	public void setLsurname(String lsurname) {
		this.lsurname = lsurname;
	}
	/**
	 * @return the life assured's age
	 */
	public int getLifeage() {
		return lifeage;
	}
	/**
	 * @param lifeage the life assured's age to set
	 */
	public void setLifeage(int lifeage) {
		this.lifeage = lifeage;
	}
	/**
	 * @return the cnttype
	 */
	public String getCnttype() {
		return cnttype;
	}
	/**
	 * @param cnttype the cnttype to set
	 */
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	/**
	 * @return the occdate
	 */
	public int getOccdate() {
		return occdate;
	}
	/**
	 * @param occdate the occdate to set
	 */
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	/**
	 * @return the transaction date
	 */
	public int getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transaction date the transaction date to set
	 */
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * @return the transcd
	 */
	public String getTranscd() {
		return transcd;
	}
	/**
	 * @param transcd the transaction code to set
	 */
	public void setTranscd(String transcd) {
		this.transcd = transcd;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return the critSatisfied
	 */
	public boolean isCritSatisfied() {
		return critSatisfied;
	}
	/**
	 * @param critSatisfied the critSatisfied to set
	 */
	public void setCritSatisfied(boolean critSatisfied) {
		this.critSatisfied = critSatisfied;
	}
	/**
	 * @return the sumInsured
	 */
	public BigDecimal getSumInsured() {
		return sumInsured;
	}
	/**
	 * @param sumInsured the sumInsured to set
	 */
	public void setSumInsured(BigDecimal sumInsured) {
		this.sumInsured = sumInsured;
	}
	/**
	 * @return the hosbensSI
	 */
	public BigDecimal getHosbensSI() {
		return hosbensSI;
	}
	/**
	 * @param hosbensSI the hosbensSI to set
	 */
	public void setHosbensSI(BigDecimal hosbensSI) {
		this.hosbensSI = hosbensSI;
	}
	/**
	 * @return the cdateduration
	 */
	public int getCdateduration() {
		return cdateduration;
	}
	/**
	 * @param cdateduration the duration from contract date to set
	 */
	public void setCdateduration(int cdateduration) {
		this.cdateduration = cdateduration;
	}
	/**
	 * @return the juvenileage
	 */
	public int getJuvenileage() {
		return juvenileage;
	}
	/**
	 * @param juvenileage the juvenile age to set
	 */
	public void setJuvenileage(int juvenileage) {
		this.juvenileage = juvenileage;
	}
	/**
	 * @return the tranno
	 */
	public int getTranno() {
		return tranno;
	}
	/**
	 * @param tranno the tranno to set
	 */
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	/**
	 * @return the ptrnEff
	 */
	public int getPtrnEff() {
		return ptrnEff;
	}
	/**
	 * @param ptrnEff the ptrnEff to set
	 */
	public void setPtrnEff(int ptrnEff) {
		this.ptrnEff = ptrnEff;
	}
	/**
	 * @return the trdt
	 */
	public int getTrdt() {
		return trdt;
	}
	/**
	 * @param trdt the trdt to set
	 */
	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	/**
	 * @return the trtm
	 */
	public int getTrtm() {
		return trtm;
	}
	/**
	 * @param trtm the trtm to set
	 */
	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	/**
	 * @return the termId
	 */
	public String getTermId() {
		return termId;
	}
	/**
	 * @param termId the termId to set
	 */
	public void setTermId(String termId) {
		this.termId = termId;
	}
	/**
	 * @return the usrprf
	 */
	public String getUsrprf() {
		return usrprf;
	}
	/**
	 * @param usrprf the usrprf to set
	 */
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	/**
	 * @return the jobnm
	 */
	public String getJobnm() {
		return jobnm;
	}
	/**
	 * @param jobnm the jobnm to set
	 */
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
}
