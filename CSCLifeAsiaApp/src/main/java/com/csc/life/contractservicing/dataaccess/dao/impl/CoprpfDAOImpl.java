package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.CoprpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Coprpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CoprpfDAOImpl extends BaseDAOImpl<Coprpf> implements CoprpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CoprpfDAOImpl.class);
	
	@Override
	public List<Coprpf> getCoprpfRecord(Coprpf coprpf) {
		List<Coprpf> list = null;
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,SUMINS,SINGP,");
		sb.append("ZSTPDUTY01,RISKPREM,COMMPREM,INSTPREM,ZBINSTPREM,ZLINSTPREM,INSTFROM,INSTTO FROM COPRPF ");
		sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND INSTFROM=? AND INSTTO=? AND VALIDFLAG=? "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			
			ps.setString(1, coprpf.getChdrcoy());
			ps.setString(2, coprpf.getChdrnum());
			ps.setInt(3, coprpf.getInstfrom());
			ps.setInt(4, coprpf.getInstto());
			ps.setString(5, coprpf.getValidflag());
			list = new ArrayList<Coprpf>();
			rs = ps.executeQuery();
			while(rs.next()) {
				Coprpf copr = new Coprpf();
				copr.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				copr.setChdrcoy(rs.getString("CHDRCOY"));
				copr.setChdrnum(rs.getString("CHDRNUM"));
				copr.setLife(rs.getString("LIFE"));
				copr.setJlife(rs.getString("JLIFE"));
				copr.setCoverage(rs.getString("COVERAGE"));
				copr.setRider(rs.getString("RIDER"));
				copr.setPlnsfx(rs.getInt("PLNSFX"));
				copr.setValidflag(rs.getString("VALIDFLAG"));
				copr.setTranno(rs.getInt("TRANNO"));
				copr.setSumins(rs.getBigDecimal("SUMINS"));
				copr.setSingp(rs.getBigDecimal("SINGP"));
				copr.setZstpduty01(rs.getBigDecimal("ZSTPDUTY01"));
				copr.setRiskprem(rs.getBigDecimal("RISKPREM"));
				copr.setCommprem(rs.getBigDecimal("COMMPREM"));
				copr.setInstprem(rs.getBigDecimal("INSTPREM"));
				copr.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
				copr.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
				copr.setInstfrom(rs.getInt("INSTFROM"));
				copr.setInstto(rs.getInt("INSTTO"));
				list.add(copr);
			}
			
		} catch (SQLException e) {
			LOGGER.error("getCoprpfRecord()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return list;
	}

	@Override
	public void insertCoprpfRecord(List<Coprpf> coprList) {
		StringBuilder sb = new StringBuilder("INSERT INTO COPRPF(CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,SUMINS,SINGP,");
		sb.append("ZSTPDUTY01,RISKPREM,COMMPREM,INSTPREM,ZBINSTPREM,ZLINSTPREM,INSTFROM,INSTTO,USRPRF,JOBNM,DATIME,CREATED_AT)");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Coprpf coprpf : coprList) {
				ps.setString(1, coprpf.getChdrcoy());
				ps.setString(2, coprpf.getChdrnum());
				ps.setString(3, coprpf.getLife());
				ps.setString(4, coprpf.getJlife());
				ps.setString(5, coprpf.getCoverage());
				ps.setString(6, coprpf.getRider());
				ps.setInt(7, coprpf.getPlnsfx());
				ps.setString(8, coprpf.getValidflag());
				ps.setInt(9, coprpf.getTranno());
				ps.setBigDecimal(10, coprpf.getSumins() == null ? BigDecimal.ZERO : coprpf.getSumins());
				ps.setBigDecimal(11, coprpf.getSingp() == null ? BigDecimal.ZERO : coprpf.getSingp());
				ps.setBigDecimal(12, coprpf.getZstpduty01() == null ? BigDecimal.ZERO : coprpf.getZstpduty01());
				ps.setBigDecimal(13, coprpf.getRiskprem() == null ? BigDecimal.ZERO : coprpf.getRiskprem());
				ps.setBigDecimal(14, coprpf.getCommprem() == null ? BigDecimal.ZERO : coprpf.getCommprem());
				ps.setBigDecimal(15, coprpf.getInstprem() == null ? BigDecimal.ZERO : coprpf.getInstprem());
				ps.setBigDecimal(16, coprpf.getZbinstprem() == null ? BigDecimal.ZERO : coprpf.getZbinstprem());
				ps.setBigDecimal(17, coprpf.getZlinstprem() == null ? BigDecimal.ZERO : coprpf.getZlinstprem());
				ps.setInt(18, coprpf.getInstfrom());
				ps.setInt(19, coprpf.getInstto());
				ps.setString(20, this.getUsrprf());
				ps.setString(21, this.getJobnm());
				ps.setTimestamp(22, new Timestamp(System.currentTimeMillis()));
				ps.setTimestamp(23, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
			
		} catch (SQLException e) {
			LOGGER.error("insertCoprpfRecord()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}	
	}

	@Override
	public void updateCoprpf(List<Coprpf> coprList) {
		String SQL_UPDATE = "UPDATE COPRPF SET VALIDFLAG=?,TRANNO=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";
        PreparedStatement psUpdate = getPrepareStatement(SQL_UPDATE);
        try {
            for (Coprpf c : coprList) {
            	psUpdate.setString(1, c.getValidflag());
            	psUpdate.setInt(2, c.getTranno());
                psUpdate.setString(3, getJobnm());
                psUpdate.setString(4, getUsrprf());
                psUpdate.setTimestamp(5, getDatime());
                psUpdate.setLong(6, c.getUniqueNumber());					
                psUpdate.addBatch();
            }
            psUpdate.executeBatch();
        } catch (SQLException e) {
            LOGGER.error("updateCoprpf()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psUpdate, null);
        }
	}

	@Override
	public List<Coprpf> getCoprpfByTranno(Coprpf coprpf) {

		List<Coprpf> list = null;
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,SUMINS,SINGP,");
		sb.append("ZSTPDUTY01,RISKPREM,COMMPREM,INSTPREM,ZBINSTPREM,ZLINSTPREM,INSTFROM,INSTTO FROM COPRPF ");
		sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG=? "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			
			ps.setString(1, coprpf.getChdrcoy());
			ps.setString(2, coprpf.getChdrnum());
			ps.setString(3, coprpf.getValidflag());
			list = new ArrayList<Coprpf>();
			rs = ps.executeQuery();
			while(rs.next()) {
				Coprpf copr = new Coprpf();
				copr.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				copr.setChdrcoy(rs.getString("CHDRCOY"));
				copr.setChdrnum(rs.getString("CHDRNUM"));
				copr.setLife(rs.getString("LIFE"));
				copr.setJlife(rs.getString("JLIFE"));
				copr.setCoverage(rs.getString("COVERAGE"));
				copr.setRider(rs.getString("RIDER"));
				copr.setPlnsfx(rs.getInt("PLNSFX"));
				copr.setValidflag(rs.getString("VALIDFLAG"));
				copr.setTranno(rs.getInt("TRANNO"));
				copr.setSumins(rs.getBigDecimal("SUMINS"));
				copr.setSingp(rs.getBigDecimal("SINGP"));
				copr.setZstpduty01(rs.getBigDecimal("ZSTPDUTY01"));
				copr.setRiskprem(rs.getBigDecimal("RISKPREM"));
				copr.setCommprem(rs.getBigDecimal("COMMPREM"));
				copr.setInstprem(rs.getBigDecimal("INSTPREM"));
				copr.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
				copr.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
				copr.setInstfrom(rs.getInt("INSTFROM"));
				copr.setInstto(rs.getInt("INSTTO"));
				list.add(copr);
			}
			
		} catch (SQLException e) {
			LOGGER.error("getCoprpfRecord()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return list;
	}

	@Override
	public void deleteCoprpf(List<Coprpf> coprList) {
		String sql = "DELETE FROM COPRPF WHERE UNIQUE_NUMBER=? ";
        PreparedStatement ps = getPrepareStatement(sql);
        try {
            for (Coprpf c : coprList) {
            	ps.setLong(1, c.getUniqueNumber());					
            	ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            LOGGER.error("deleteCoprpf()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
	}
}
