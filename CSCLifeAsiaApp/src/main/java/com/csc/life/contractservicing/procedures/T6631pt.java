/*
 * File: T6631pt.java
 * Date: 30 August 2009 2:27:58
 * Author: Quipoz Limited
 * 
 * Class transformed from T6631PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.contractservicing.tablestructures.T6631rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6631.
*
*
*****************************************************************
* </pre>
*/
public class T6631pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Policy Loan Rules                        S6631");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(47);
	private FixedLengthStringData filler7 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(39).isAPartOf(wsaaPrtLine003, 5, FILLER).init("Minimum Months Premiums Required. . :");
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine003, 44).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(47);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine004, 5, FILLER).init("Auto Frequency Alteration . . . . . :");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine004, 45).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(26);
	private FixedLengthStringData filler11 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine005, 5, FILLER).init("Payment Method Change");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(29);
	private FixedLengthStringData filler13 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 19, FILLER).init("To. . :");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 28);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(62);
	private FixedLengthStringData filler15 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 17, FILLER).init("From. . :");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 28);
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 31);
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 34);
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 37);
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 40);
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 43);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 46);
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 49);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 52);
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 55);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 58);
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 61);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(62);
	private FixedLengthStringData filler28 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 28);
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 31);
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 34);
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 37);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 40);
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 43);
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 46);
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 49);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 52);
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 55);
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 58);
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 61);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(34);
	private FixedLengthStringData filler40 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler41 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine009, 5, FILLER).init("Lapse Following Benefits. . :");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(74);
	private FixedLengthStringData filler42 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 28);
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 34);
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 40);
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 46);
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 52);
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 58);
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 64);
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 70);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(74);
	private FixedLengthStringData filler50 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 28);
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 34);
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 40);
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 46);
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 52);
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 58);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 64);
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 70);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(74);
	private FixedLengthStringData filler58 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 28);
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 34);
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 40);
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 46);
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 52);
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 56, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 58);
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 64);
	private FixedLengthStringData filler65 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 68, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 70);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6631rec t6631rec = new T6631rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6631pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6631rec.t6631Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t6631rec.durmnth);
		fieldNo006.set(t6631rec.autofreq);
		fieldNo032.set(t6631rec.crtable01);
		fieldNo033.set(t6631rec.crtable02);
		fieldNo034.set(t6631rec.crtable03);
		fieldNo035.set(t6631rec.crtable04);
		fieldNo036.set(t6631rec.crtable05);
		fieldNo037.set(t6631rec.crtable06);
		fieldNo038.set(t6631rec.crtable07);
		fieldNo039.set(t6631rec.crtable08);
		fieldNo040.set(t6631rec.crtable09);
		fieldNo041.set(t6631rec.crtable10);
		fieldNo042.set(t6631rec.crtable11);
		fieldNo043.set(t6631rec.crtable12);
		fieldNo044.set(t6631rec.crtable13);
		fieldNo045.set(t6631rec.crtable14);
		fieldNo046.set(t6631rec.crtable15);
		fieldNo047.set(t6631rec.crtable16);
		fieldNo048.set(t6631rec.crtable17);
		fieldNo049.set(t6631rec.crtable18);
		fieldNo050.set(t6631rec.crtable19);
		fieldNo051.set(t6631rec.crtable20);
		fieldNo052.set(t6631rec.crtable21);
		fieldNo053.set(t6631rec.crtable22);
		fieldNo054.set(t6631rec.crtable23);
		fieldNo055.set(t6631rec.crtable24);
		fieldNo007.set(t6631rec.payto);
		fieldNo008.set(t6631rec.payfrom01);
		fieldNo009.set(t6631rec.payfrom02);
		fieldNo010.set(t6631rec.payfrom03);
		fieldNo011.set(t6631rec.payfrom04);
		fieldNo012.set(t6631rec.payfrom05);
		fieldNo013.set(t6631rec.payfrom06);
		fieldNo014.set(t6631rec.payfrom07);
		fieldNo015.set(t6631rec.payfrom08);
		fieldNo016.set(t6631rec.payfrom09);
		fieldNo017.set(t6631rec.payfrom10);
		fieldNo018.set(t6631rec.payfrom11);
		fieldNo019.set(t6631rec.payfrom12);
		fieldNo020.set(t6631rec.payfrom13);
		fieldNo021.set(t6631rec.payfrom14);
		fieldNo022.set(t6631rec.payfrom15);
		fieldNo023.set(t6631rec.payfrom16);
		fieldNo024.set(t6631rec.payfrom17);
		fieldNo025.set(t6631rec.payfrom18);
		fieldNo026.set(t6631rec.payfrom19);
		fieldNo027.set(t6631rec.payfrom20);
		fieldNo028.set(t6631rec.payfrom21);
		fieldNo029.set(t6631rec.payfrom22);
		fieldNo030.set(t6631rec.payfrom23);
		fieldNo031.set(t6631rec.payfrom24);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
