/*
 * File: P5051.java
 * Date: 30 August 2009 0:00:40
 * Author: Quipoz Limited
 * 
 * Class transformed from P5051.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.RplnpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rplnpf;
import com.csc.life.contractservicing.screens.S5051ScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.CovrpfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*        POLICY LOANS SUB MENU.
*        ----------------------
*
* Overview.
* ---------
*
* This is the Policy Loans Submenu program. This screen (S5051)
*  allows the user to select a Contract, an Effective date and
*  an Action.
*  The Contract validation is described below, the date must be
*   a valid date and the  Action  must also be valid - see  the
*   P5051 entry in T1690 for allowable Actions.
*
* Validation.
* -----------
*
*    Key 1 - Contract number:
*    -----
*  The Contract must exist.
*  The Contract must have a valid status for transaction that is
*   being selected - check this via the table T5679.
*  The Contract must not have any claims on it - i.e. there must
*   not be any REGP records associated with it.
*  The Contract will have one or more Coverages/Riders attached
*   to it, and at least one of these must have a Loan method on
*   it ( entered via T5687 ) which is held in the Loan method
*   table T6632. NOTE - the only validation for Loan methods is
*   that this program checks that T5687-LOANMETH is not BLANK.
*   We do not read T6632 here.
*
*    The ACTION must be a valid selection.
*     Currently, the only Action is 'A' - Policy Registration
*
*    The Effective Date cannot be GREATER than todays date or
*     earlier than the Risk Commencement date.
*
* Updating.
* ---------
*
* Softlock the Contract Header - If already locked, display
*  an error message.
*
* Store CHDRENQ data for use by next program in stack - do KEEPS
*
*
*
*****************************************************************
* </pre>
*/
public class P5051 extends ScreenProgCS {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(P5051.class);

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5051");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private String wsaaStatusOk = "";
	private String wsaaLoanOk = "";
	private String wsaaRegpOk = "";

	private FixedLengthStringData wsaaPremStatusOk = new FixedLengthStringData(1);
	private Validator premStatusOk = new Validator(wsaaPremStatusOk, "Y");

		/* FORMATS */
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Batckey wsaaBatchkey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private S5051ScreenVars sv = ScreenProgram.getScreenVars( S5051ScreenVars.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);//ILIFE-5826
	private Chdrpf chdrpf = new Chdrpf();//ILIFE-5826
	//ICIL-14 Start
	boolean isLoanConfig = false;	
	private LoanpfDAO loanpfDAO = getApplicationContext().getBean("loanpfDAO" , LoanpfDAO.class);
	private RplnpfDAO rplnpfDAO = getApplicationContext().getBean("rplnpfDAO", RplnpfDAO.class);
	private Rplnpf rplnpf ;
	//ICIL-14 End
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAOImpl.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private Itempf itempf = null;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		validateKey22220, 
		exit2290, 
		start2510, 
		loopCovrenq2750, 
		exit2790, 
		exit12090, 
		batching3080, 
		exit3090
	}

	public P5051() {
		super();
		screenVars = sv;
		new ScreenModel("S5051", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		sv.effdate.set(varcom.vrcmMaxDate);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set("E070");
		}
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		
		//ICIL-14 Start
				isLoanConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSLND002", appVars, "IT");
				if(isLoanConfig)
				{
					sv.actionflag.set("Y");
				}
				else
				{
					sv.actionflag.set("N");
				}
		//ICIL-14 End
		
		
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S5051IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5051-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
				checkDate2800();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateKey12210();
				case validateKey22220: 
					validateKey22220();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateKey12210()
	{
		/*    Validate fields*/
		if (isEQ(subprogrec.key1, SPACES)) {
			goTo(GotoLabel.validateKey22220);
		}
		//ILIFE-5826
		
		 chdrpf = chdrpfDAO.getchdrRecordservunit(wsspcomn.company.toString(), sv.chdrsel.toString());
		 
		 
		
		if (chdrpf== null && isEQ(subprogrec.key1, "Y")) {
			sv.chdrselErr.set("H137");
		}
		
		if (chdrpf != null && isEQ(subprogrec.key1, "N")) {
			sv.chdrselErr.set("F918");	
		}
		
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				goTo(GotoLabel.exit2290);
			}
		}
		if(!isLoanConfig)
			if (isEQ(subprogrec.key1, "Y")
			&& isEQ(sv.chdrselErr, SPACES)) {
				/*       Contract exists - now check out other selection criteria*/
				checkContractDets2300();
			}
		
		
		//ICIL-14 Start		
		if(isLoanConfig ){
			if(isEQ(sv.action, "C") || isEQ(sv.action, "D") || isEQ(sv.action, "E")){
				 if(isEQ(subprogrec.key2, "N") && isNE(sv.effdate,varcom.vrcmMaxDate)){
					 sv.effdateErr.set("F617");
					 return;
				 }
			}
			rplnpf = rplnpfDAO.getRploanRecord(sv.chdrsel.toString().trim(),"P");
			if(isEQ(sv.action, "C")){
				
				if(isGT(sv.chdrsel.toString().trim().length(),0) ){			 
				 long recordCount = 0;
				 recordCount = loanpfDAO.getLoanRecord(wsspcomn.company.toString(),sv.chdrsel.toString());
				 
				 if (recordCount==0 ) {
					 if (isEQ(sv.chdrselErr,SPACES))
						sv.chdrselErr.set("RREK");
						return;
					 
					}
				}
				
				if(rplnpf!=null){					
					sv.actionErr.set("RRFH");										
					}
				}
			int recordCount = 0;
			recordCount = rplnpfDAO.getLoanRepayRecord(sv.chdrsel.toString());
			if(isEQ(sv.action, "D") || isEQ(sv.action, "E") ){	
				if(recordCount == 0){
					if (isEQ(sv.action, "D")){
						sv.actionErr.set("RREK");
					}
					else{
						sv.actionErr.set("E944");
					}

				}else if(rplnpf == null){					
						sv.actionErr.set("RRFB");								
				}
			}			
		}
		//ICIL-14 End
			
		
		
	}

protected void validateKey22220()
	{
		if (isEQ(subprogrec.key2, SPACES)
		|| isEQ(subprogrec.key2, "N")) {
			return ;
		}

		//ILIFE-5826
		chdrpf = chdrpfDAO.getchdrRecordservunit(wsspcomn.company.toString(), sv.chdrsel.toString());
		
		if(chdrpf == null){
			sv.chdrselErr.set("E544");
		}
	}

protected void checkContractDets2300()
	{
		start2310();
	}

protected void start2310()
	{
		wsaaStatusOk = "Y";
		checkStatus2400();
		if (isEQ(wsaaStatusOk, "N")) {
			sv.chdrselErr.set("E767");
			wsspcomn.edterror.set("Y");
			return ;
		}
		wsaaRegpOk = "N";
		checkRegPayments2600();
		if (isEQ(wsaaRegpOk, "N")) {
			sv.chdrselErr.set("G543");
			wsspcomn.edterror.set("Y");
			return ;
		}
		wsaaLoanOk = "N";
		checkLoanMethods2700();
		if (isEQ(wsaaLoanOk, "N")) {
			sv.chdrselErr.set("G544");
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkStatus2400()
	{
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(subprogrec.transcd.toString());
	    itempf = itemDAO.getItempfRecord(itempf);
		
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaSub.set(ZERO);
		statusSearch2500();
	}

protected void statusSearch2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case start2510: 
					start2510();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			wsaaStatusOk = "N";
		}
		else {
			if (isNE(chdrpf.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				/*      GO TO 2510-START.                                        */
				goTo(GotoLabel.start2510);
			}
			else {
				wsaaPremStatusOk.set(SPACES);
				premSearch2500a();
				if (premStatusOk.isTrue()) {
					return ;
				}
				else {
					goTo(GotoLabel.start2510);
				}
			}
		}
		/*EXIT*/
	}

protected void premSearch2500a()
	{
		/*A-START*/
		for (wsaaCnt.set(1); !(isGT(wsaaCnt, 12)
		|| premStatusOk.isTrue()); wsaaCnt.add(1)){
			if (isEQ(chdrpf.getPstcde(), t5679rec.cnPremStat[wsaaCnt.toInt()])) {
				premStatusOk.setTrue();
			}
		}
		/*A-EXIT*/
	}

protected void checkRegPayments2600()
	{
		List<Regppf> regpList = regppfDAO.readRegpRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(regpList == null || regpList.isEmpty()){
			wsaaRegpOk = "Y";
			return ;
		}
	}

protected void checkLoanMethods2700(){
	List<Covrpf> covrlist = covrpfDAO.SearchRecordsforMatd(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(),0);
	if(covrlist == null || covrlist.isEmpty()){
		return;
	}
	for (Covrpf covrpf: covrlist){
	    List<Itempf> itempfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(),"T5687",covrpf.getCrtable(),wsaaToday.toInt());
		if (itempfList == null || itempfList.isEmpty()) {
			syserrrec.statuz.set("F294");
			fatalError600();
		}
		t5687rec.t5687Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		if (isNE(t5687rec.loanmeth, SPACES)) {
			wsaaLoanOk = "Y";
			break;
		}
	}
}



protected void checkDate2800()
	{
		/*START*/	
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {			
			if(isEQ(sv.action, "C") || isEQ(sv.action, "D") || isEQ(sv.action, "E")){
				if(isLoanConfig){				
					sv.effdate.set(varcom.vrcmMaxDate);						
				}
			}else{
				sv.effdate.set(wsaaToday);
			}
			return ;
		}
	
		
		if (isEQ(sv.action, "A")) {
			if (isGT(sv.effdate, wsaaToday)) {
				sv.effdateErr.set("F073");
				return ;
			}
		}
		/*if (isNE(chdrenqIO.getStatuz(), varcom.mrnf)) {
			if (isLT(sv.effdate, chdrenqIO.getOccdate())) {
				sv.effdateErr.set("f616);
			}
		}*/
		if (chdrpf !=null) {
			if (isLT(sv.effdate, chdrpf.getOccdate())) {
				sv.effdateErr.set("F616");
			}
		}
		/*EXIT*/
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set("E073");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		wsspcomn.currfrom.set(sv.effdate);
		/* perform any KEEPS, SFTLOCKs etc required by the next program*/
		
		chdrpfDAO.setCacheObject(chdrpf);
		
		
		//ICIL-180
		if(isEQ(scrnparams.action, "C")){
			wsspcomn.flag.set("P");				
		}else if(isEQ(scrnparams.action, "D")){
			wsspcomn.flag.set("I");
		}else if(isEQ(scrnparams.action, "E")){
			wsspcomn.flag.set("I");			
		}
	
		/* If Loan Enquiry, skip SFTLOCK.                                  */
		if (isEQ(scrnparams.action, "B")) {
			goTo(GotoLabel.batching3080);
		}
		/* Softlock contract*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S5051-CHDRSEL          TO SFTL-ENTITY.                  */
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set("F910");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.sbmaction.set(scrnparams.action);
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

}