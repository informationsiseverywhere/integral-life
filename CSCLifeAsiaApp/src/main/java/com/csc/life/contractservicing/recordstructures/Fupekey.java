package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:48
 * Description:
 * Copybook name: FUPEKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fupekey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fupeFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData fupeKey = new FixedLengthStringData(256).isAPartOf(fupeFileKey, 0, REDEFINE);
  	public FixedLengthStringData fupeChdrcoy = new FixedLengthStringData(1).isAPartOf(fupeKey, 0);
  	public FixedLengthStringData fupeChdrnum = new FixedLengthStringData(8).isAPartOf(fupeKey, 1);
  	public PackedDecimalData fupeFupno = new PackedDecimalData(2, 0).isAPartOf(fupeKey, 9);
  	public PackedDecimalData fupeTranno = new PackedDecimalData(5, 0).isAPartOf(fupeKey, 11);
  	public FixedLengthStringData fupeDocseq = new FixedLengthStringData(4).isAPartOf(fupeKey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(fupeKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fupeFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fupeFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}