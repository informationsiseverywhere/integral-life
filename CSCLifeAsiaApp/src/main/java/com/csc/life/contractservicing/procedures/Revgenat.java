/*
 * File: Revgenat.java
 * Date: 30 August 2009 2:08:12
 * Author: Quipoz Limited
 *
 * Class transformed from REVGENAT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.csc.diary.cls.Chkdtaara;
import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.reports.R5155Report;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.RlvrpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Rlvrpf;
import com.csc.life.newbusiness.tablestructures.T6799rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.reassurance.dataaccess.CovtcsnTableDAM;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.SlckpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.SlckpfDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Slckpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*REMARKS.
*
* REVGENAT
* ~~~~~~~
*
* Overview
* ~~~~~~~~
* This module is  the  controlling  program for Full Contract
* Reversal. It is normally requested from program P5155 (Full
* Contract Reversal). As the  extent  of a reversal may be quite
* large this module  only attempts to reverse out policy history
* details  and   update  the  contract  header.  All  individual
* transaction reversals  are  dealt  with by generic subroutines
* which are called from  within  this module if a policy history
* record is found for that transaction.
*
* A report of  transactions  reversed  is  produced from this
* module  and a  policy  history  transaction  written  for  the
* reversal.
* All policy transactions which have been reversed are logically
* deleted using valid flag 2 on the record.
*
*
* Initial Processing
* ~~~~~~~~~~~~~~~~~~
* Firstly read contract  header  record  using the logical view
* CHDRLIF. The details of minor alteration is then saved to
* working storage.
*
* The Payer Details record is then read and similarly saved to
* working storage.
*
* Open the print file  and  write  the header line. This will
* consist of  the  company,  contract  and original commencement
* date from the contract  header  record and the reverse to date
* from linkage.
*
* Get the PTRN records by reading the PTRNREV logical view.
* (PTRNREV logical view is used because it omits all valid
*  flag '2' records).
*
* For each PTRN record found - carry out the following
* processing :
* (Once  all  the  records  have  been processed go to update
*  contract header.)
*
*
* Generic Reversal processing
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~
* All  updating will be carried out by generic subroutines as
* set up  in table T6661. The key into this table is transaction
* code which we get from the relevant PTRNREV record.
* (If no entry  is  found on the table get the next record)
*
* The  copybook  to  be  set  up  for the calls will be REVERSEREC,
* the fields are to be set up as follows:
*
*     Component key with just the contract number and
*     company.
*     Effective date 1 as reverse to date.
*     TRANNO from the PTRN record
*     New TRANNO = Contract Header TRANNO + 1
*
* A detail line also has to be produced for the report using the
* following details:
*
*     Transaction number from the policy history record
*     Transaction long description from T1688
*     Effective date of transaction from policy history
*     record
*
* Move '2' to the valid flag on the policy history record and
* write back to the database.
*
* Go  back  and  process  next  record from PTRNREV logical
* view.
*
*
* Update Contract Header and Payor Details
*-----------------------------------------
* The Contract Header is read, the minor alteration details are
* restored from working storage, the new transaction number is
* stamped on the contract header and an update of the record is
* performed.
*
* Similarly, the Payer details is read, minor alteration details
* are restored from working storage and then the database record
* is updated.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
* A100-UNDERWRITING SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~~~
* Call CRTUNDWRT with function equal to 'DEL' to delete records
* in underwriting file UNDRPF.
* Visit each component record in COVRTRB of same contract number
* , call CRTUNDWRT again with function equal to 'ADD' to create
* the underwriting record with each active component again.
*
*****************************************************************
* </pre>
*/
public class Revgenat extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5155Report printerFile = new R5155Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private final String wsaaSubr = "REVGENAT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaOverflow = "Y";

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaLibrary = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaLevel = new FixedLengthStringData(6).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaDataArea = new FixedLengthStringData(10).init(SPACES);

	private FixedLengthStringData wsaaValue = new FixedLengthStringData(23);
	private FixedLengthStringData wsaaStatus = new FixedLengthStringData(4).isAPartOf(wsaaValue, 0).init(SPACES);
	private ZonedDecimalData wsaaActThreads = new ZonedDecimalData(2, 0).isAPartOf(wsaaValue, 4).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSubThreads = new ZonedDecimalData(2, 0).isAPartOf(wsaaValue, 6).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaCancelMode = new FixedLengthStringData(7).isAPartOf(wsaaValue, 8).init(SPACES);
	private ZonedDecimalData wsaaRunNumber = new ZonedDecimalData(8, 0).isAPartOf(wsaaValue, 15).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaCfiafiFlag = new FixedLengthStringData(1).init(SPACES);
	protected ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData r5155h01Record = new FixedLengthStringData(59);
	private FixedLengthStringData r5155h01O = new FixedLengthStringData(59).isAPartOf(r5155h01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5155h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5155h01O, 1);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5155h01O, 31);
	private FixedLengthStringData occdate = new FixedLengthStringData(10).isAPartOf(r5155h01O, 39);
	private FixedLengthStringData todate = new FixedLengthStringData(10).isAPartOf(r5155h01O, 49);

	private FixedLengthStringData r5155d01Record = new FixedLengthStringData(49);
	private FixedLengthStringData r5155d01O = new FixedLengthStringData(49).isAPartOf(r5155d01Record, 0);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(r5155d01O, 0);
	private FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(r5155d01O, 4);
	private ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(r5155d01O, 34);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(r5155d01O, 39);

	private FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private FixedLengthStringData wsaaChdrAplsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrBillsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrCommsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrNotssupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrRnwlsupr = new FixedLengthStringData(1);
	private PackedDecimalData wsaaChdrAplspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrBillspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrCommspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrNotsspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrRnwlspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrAplspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrBillspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrCommspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrNotsspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrRnwlspto = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaChdrCownpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrCowncoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrCownnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrJownnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaChdrPolsum = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaChdrDesppfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrDespcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrDespnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrAgntpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrAgntcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPayrAplsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayrBillsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayrNotssupr = new FixedLengthStringData(1);
	private PackedDecimalData wsaaPayrAplspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrNotsspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrAplspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrNotsspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillcd = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaTranno = new FixedLengthStringData(5);
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t1693 = "T1693";
	private static final String t6654 = "T6654";
	protected static final String t6661 = "T6661";
	private static final String t6799 = "T6799";
	private static final String t7508 = "T7508";
		/* FORMATS */
	private static final String payrrec = "PAYRREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String ptrnrevrec = "PTRNREVREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	protected static final String itemrec = "ITEMREC";
	private static final String covrtrbrec = "COVRTRBREC";
	private static final String liferec = "LIFEREC";
	private static final String covtcsnrec = "COVTCSNREC";
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private CovtcsnTableDAM covtcsnIO = new CovtcsnTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected Ptrnpf ptrnrevIO = new Ptrnpf();
	private Batckey wsaaBatckey = new Batckey();
	private Crtundwrec crtundwrec = new Crtundwrec();
	protected Syserrrec syserrrec = new Syserrrec();
	private T6654rec t6654rec = new T6654rec();
	protected T6661rec t6661rec = new T6661rec();
	private T6799rec t6799rec = new T6799rec();
	private T7508rec t7508rec = new T7508rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	protected Reverserec reverserec = new Reverserec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	protected Varcom varcom = new Varcom();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	protected Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	protected WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
	private SlckpfDAO slckpfDAO = new SlckpfDAOImpl();
	private Slckpf slckpf=new Slckpf();
	
	private FixedLengthStringData slckEntity = new FixedLengthStringData(16);
	private Rlvrpf rlvrpf=null;
	private RlvrpfDAO rlvrpfDAO = getApplicationContext().getBean("rlvrpfDAO", RlvrpfDAO.class);
	private static final String ROLLOVER_FEATURE_ID="NBPRP104";
	private boolean rolloverFlag = false;
	
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private List<Ptrnpf> ptrnUpList = new ArrayList<>();
	//IBPLIFE-2472 Start
	private boolean onePflag = false;
	private static final String  ONE_P_CASHLESS="NBPRP124";
	protected static final String t5679 = "T5679";
	protected static final String tbgd = "TBGD"; //Tentative Approval
	protected static final String tbge = "TBGE"; //Tentative Approval Reversal
	private T5679rec t5679rec = new T5679rec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	//IBPLIFE-2472 End
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private String username;
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		detailLine2340
	}

	public Revgenat() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		onePflag = FeaConfg.isFeatureExist(chdrlifIO.getChdrcoy().toString(), ONE_P_CASHLESS, appVars, "IT"); //IBPLIFE-2472
		List<Ptrnpf> ptrnList =  ptrnpfDAO.getPtrnrevData(chdrlifIO.getChdrcoy().toString(),chdrlifIO.getChdrnum().toString(),"99999");
		if(ptrnList!=null && !ptrnList.isEmpty())	//PINNACLE-2052
			compute(wsaaNewTranno, 0).set(add(ptrnList.get(0).getTranno(),1));	//PINNACLE-2052
		Iterator<Ptrnpf> ptrnItr = null;
		if(ptrnList!=null && !ptrnList.isEmpty()) {			
			ptrnItr = ptrnList.iterator();
			
			while(ptrnItr.hasNext()) {
				ptrnrevIO = new Ptrnpf(ptrnItr.next());							
				if(!(processPtrn2000()))
					break;
			}
		}

		processChdrPayr3000();
		housekeeping4000();
		a000Statistics();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1010();
	}

protected void start1010()
	{
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		syserrrec.subrname.set(wsaaSubr);
		wsaaCfiafiFlag.set(SPACES);
		wsaaLibrary.set(SPACES);
		wsaaLevel.set(SPACES);
		wsaaStatuz.set(SPACES);
		wsaaValue.set(SPACES);
		wsaaDataArea.set(SPACES);
		wsaaActThreads.set(ZERO);
		wsaaSubThreads.set(ZERO);
		wsaaRunNumber.set(ZERO);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaTransAreaInner.wsaaTransArea.set(atmodrec.transArea);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		printerFile.openOutput();
		readChdr1100();
		readCovr1150();
		readPayr1200();
		//readPtrn1300();
	}

protected void readChdr1100()
	{
		start1110();
	}

protected void start1110()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		saveChdrMinorAlt1400();
	}

protected void readCovr1150()
	{
		start1160();
	}

protected void start1160()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		covrIO.setChdrnum(chdrlifIO.getChdrnum());
		covrIO.setLife(ZERO);
		covrIO.setCoverage(ZERO);
		covrIO.setRider(ZERO);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), covrIO.getChdrcoy())) {
			covrIO.setStatuz(varcom.endp);
			covrIO.setDataArea(SPACES);
			covrIO.setCpiDate(varcom.maxdate);
		}
	}

protected void readPayr1200()
	{
		start1210();
	}

protected void start1210()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		savePayrMinorAlt1500();
		wsaaPayrBillcd.set(payrIO.getBillcd());
	}

/*protected void readPtrn1300()
	{
		start1310();
	}

protected void start1310()
	{
		ptrnrevIO.setDataArea(SPACES);
		ptrnrevIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrnum(), ptrnrevIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}		
		
	}*/

protected void saveChdrMinorAlt1400()
	{
		start1401();
	}

protected void start1401()
	{
		wsaaChdrAplsupr.set(chdrlifIO.getAplsupr());
		wsaaChdrBillsupr.set(chdrlifIO.getBillsupr());
		wsaaChdrCommsupr.set(chdrlifIO.getCommsupr());
		wsaaChdrNotssupr.set(chdrlifIO.getNotssupr());
		wsaaChdrRnwlsupr.set(chdrlifIO.getRnwlsupr());
		wsaaChdrAplspfrom.set(chdrlifIO.getAplspfrom());
		wsaaChdrBillspfrom.set(chdrlifIO.getBillspfrom());
		wsaaChdrCommspfrom.set(chdrlifIO.getCommspfrom());
		wsaaChdrNotsspfrom.set(chdrlifIO.getNotsspfrom());
		wsaaChdrRnwlspfrom.set(chdrlifIO.getRnwlspfrom());
		wsaaChdrAplspto.set(chdrlifIO.getAplspto());
		wsaaChdrBillspto.set(chdrlifIO.getBillspto());
		wsaaChdrCommspto.set(chdrlifIO.getCommspto());
		wsaaChdrNotsspto.set(chdrlifIO.getNotsspto());
		wsaaChdrRnwlspto.set(chdrlifIO.getRnwlspto());
		wsaaChdrCownpfx.set(chdrlifIO.getCownpfx());
		wsaaChdrCowncoy.set(chdrlifIO.getCowncoy());
		wsaaChdrCownnum.set(chdrlifIO.getCownnum());
		wsaaChdrJownnum.set(chdrlifIO.getJownnum());
		wsaaChdrPolsum.set(chdrlifIO.getPolsum());
		wsaaChdrDesppfx.set(chdrlifIO.getDesppfx());
		wsaaChdrDespcoy.set(chdrlifIO.getDespcoy());
		wsaaChdrDespnum.set(chdrlifIO.getDespnum());
		wsaaChdrAgntpfx.set(chdrlifIO.getAgntpfx());
		wsaaChdrAgntcoy.set(chdrlifIO.getAgntcoy());
		wsaaChdrAgntnum.set(chdrlifIO.getAgntnum());
	}

protected void savePayrMinorAlt1500()
	{
		/*START*/
		wsaaPayrAplsupr.set(payrIO.getAplsupr());
		wsaaPayrBillsupr.set(payrIO.getBillsupr());
		wsaaPayrNotssupr.set(payrIO.getNotssupr());
		wsaaPayrAplspfrom.set(payrIO.getAplspfrom());
		wsaaPayrBillspfrom.set(payrIO.getBillspfrom());
		wsaaPayrNotsspfrom.set(payrIO.getNotsspfrom());
		wsaaPayrAplspto.set(payrIO.getAplspto());
		wsaaPayrBillspto.set(payrIO.getBillspto());
		wsaaPayrNotsspto.set(payrIO.getNotsspto());
		/*EXIT*/
	}

protected boolean processPtrn2000()
	{		
		/* If this AT has been submitted from the CFI/AFI subsystem        */
		/*  instead of the normal Reversals subsystem, then each           */
		/*  PTRN record is processed as per normal Reversals UNTIL         */
		/*  the Contract Issue PTRN is reached, and we go to do            */
		/*  some specific CFI/AFI processing instead...                    */
		if (isEQ(ptrnrevIO.getBatctrcde(), wsaaTransAreaInner.wsaaCfiafiTranCode)
		|| isEQ(ptrnrevIO.getTranno(), wsaaTransAreaInner.wsaaCfiafiTranno)) {
			/*       we have got the 'Contract Issue' PTRN, so we go to        */
			/*       do the CFI/AFI specific processing & then Exit.           */
			wsaaCfiafiFlag.set("Y");
			cfiafiProcessing5000();
			/*       don't forget to Validflag '2' the Issue PTRN we have      */
			/*       just processed and print a line on the report             */
			printDetailLine2300();
			rewritePtrn2500();
			return false;
		}
		if (isLT(ptrnrevIO.getTranno(), wsaaTransAreaInner.wsaaTranNum)) {
			return false;
		}
		callGenericProcessing2100();
		if (isNE(t6661rec.contRevFlag, "N")) {
			printDetailLine2300();
			rewritePtrn2500();
		}
		return true;
	}

protected void callGenericProcessing2100()
	{
		start2110();
	}

protected void start2110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		if (isEQ(t6661rec.contRevFlag, "N")) {
			return ;
		}
		reverserec.chdrnum.set(chdrlifIO.getChdrnum());
		reverserec.company.set(chdrlifIO.getChdrcoy());
		reverserec.tranno.set(ptrnrevIO.getTranno());
		reverserec.ptrneff.set(ptrnrevIO.getPtrneff());
		reverserec.oldBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.language.set(atmodrec.language);
		reverserec.newTranno.set(wsaaNewTranno);
		reverserec.effdate1.set(wsaaTransAreaInner.wsaaTodate);
		reverserec.effdate2.set(wsaaTransAreaInner.wsaaSuppressTo);
		reverserec.batchkey.set(atmodrec.batchKey);
		reverserec.ptrnBatcpfx.set(ptrnrevIO.getBatcpfx());
		reverserec.ptrnBatccoy.set(ptrnrevIO.getBatccoy());
		reverserec.ptrnBatcbrn.set(ptrnrevIO.getBatcbrn());
		reverserec.ptrnBatcactyr.set(ptrnrevIO.getBatcactyr());
		reverserec.ptrnBatcactmn.set(ptrnrevIO.getBatcactmn());
		reverserec.ptrnBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.ptrnBatcbatch.set(ptrnrevIO.getBatcbatch());
		reverserec.transDate.set(wsaaTransAreaInner.wsaaTranDate);
		reverserec.transTime.set(wsaaTransAreaInner.wsaaTranTime);
		reverserec.user.set(wsaaTransAreaInner.wsaaUser);
		reverserec.termid.set(wsaaTransAreaInner.wsaaTermid);
		reverserec.planSuffix.set(wsaaTransAreaInner.wsaaPlnsfx);
		reverserec.bbldat.set(wsaaTransAreaInner.wsaaBbldat);
		reverserec.statuz.set(varcom.oK);
		if (isNE(t6661rec.subprog01, SPACES)) {
			callProgram(t6661rec.subprog01, reverserec.reverseRec);
			if (isNE(reverserec.statuz, varcom.oK)) {
				syserrrec.params.set(reverserec.reverseRec);
				syserrrec.statuz.set(reverserec.statuz);
				xxxxFatalError();
			}
		} else if (onePflag && isEQ(ptrnrevIO.getBatctrcde(),tbgd)) {	//IBPLIFE-2472
			readT5679();
			updateChdrpf();
			updateHpadpf();
		}
		reverserec.statuz.set(varcom.oK);
		if (isNE(t6661rec.subprog02, SPACES)) {
			callProgram(t6661rec.subprog02, reverserec.reverseRec);
			if (isNE(reverserec.statuz, varcom.oK)) {
				syserrrec.params.set(reverserec.reverseRec);
				syserrrec.statuz.set(reverserec.statuz);
				xxxxFatalError();
			}
		}
	}

//IBPLIFE-2472 Start
protected void readT5679() {
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrlifIO.getChdrcoy().toString());
	itempf.setItemtabl(t5679);
	itempf.setItemitem(tbge);
	itempf = itemDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
}

protected void updateChdrpf() {
	Chdrpf chdrpf = chdrpfDAO.updateChdrpfStatcodePstcde(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString(), 
			t5679rec.setCnRiskStat.toString(), t5679rec.setCnPremStat.toString());
	if(chdrpf==null) {
		syserrrec.statuz.set(Varcom.mrnf);
		xxxxFatalError();
	}
}

protected void updateHpadpf() {
	hpadpfDAO.updateTntapdate(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString(), wsaaToday.toInt());
}
//IBPLIFE-2472 End

protected void printDetailLine2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2310();
				case detailLine2340:
					detailLine2340();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2310()
	{
		printerRec.set(SPACES);
		/* First time thru print headings,*/
		/* subsequently at EOP.*/
		if (isNE(wsaaOverflow, "Y")) {
			goTo(GotoLabel.detailLine2340);
		}
		/* Move details to heading line*/
		printerRec.set(SPACES);
		company.set(chdrlifIO.getChdrcoy());
		/* Convert Contract Commencement Date*/
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(chdrlifIO.getOccdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		occdate.set(datcon1rec.extDate);
		chdrnum.set(chdrlifIO.getChdrnum());
		/* Convert Reverse To Date (which is got from the linkage and is*/
		/* the effective of the PTRN selected)*/
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(wsaaTransAreaInner.wsaaTodate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		todate.set(datcon1rec.extDate);
		/* Get the Company Description from T1693*/
		descIO.setDataArea(SPACES);
		descIO.setDescitem(ptrnrevIO.getChdrcoy());
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setLanguage(atmodrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		companynm.set(descIO.getLongdesc());
		wsaaOverflow = "N";
		printerFile.printR5155h01(r5155h01Record);
	}

protected void detailLine2340()
	{
		printerRec.set(SPACES);
		/* Convert PTRN Effective Date*/
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(ptrnrevIO.getPtrneff());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		effdate.set(datcon1rec.extDate);
		/* Get the Transaction Description from T1688*/
		batctrcde.set(ptrnrevIO.getBatctrcde());
		descIO.setDescitem(ptrnrevIO.getBatctrcde());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ptrnrevIO.getChdrcoy());
		descIO.setDesctabl(t1688);
		descIO.setLanguage(atmodrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		trandesc.set(descIO.getLongdesc());
		tranno.set(ptrnrevIO.getTranno());
		wsaaOverflow = "N";
		printerFile.printR5155d01(r5155d01Record);
	}

protected void rewritePtrn2500()
	{
		start2510();
	}

protected void start2510()
	{
		ptrnrevIO.setUsrprf(username); //IJS-523
		ptrnrevIO.setValidflag("2");
		ptrnUpList.add(ptrnrevIO);
		ptrnpfDAO.updatePtrnRecRever(ptrnUpList);
	}

protected void processChdrPayr3000()
	{
		readhChdr3010();
		readhPayr3030();
		updateFields3040();
		rewrtChdr3050();
		rewrtPayr3070();
	}

protected void readhChdr3010()
	{
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		/* Rewrite I/O statuz check below to cater for when an             */
		/*  Alter from Inception has been done, since this only leaves     */
		/*  one CHDR record at proposal stage i.e. Validflag = '3'.        */
		/* IF CHDRLIF-STATUZ       NOT = O-K    OR                      */
		/*    CHDRLIF-VALIDFLAG    NOT = 1                              */
		/*    MOVE CHDRLIF-PARAMS      TO SYSR-PARAMS                   */
		/*    MOVE CHDRLIF-STATUZ      TO SYSR-STATUZ                   */
		/*    PERFORM XXXX-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if(onePflag) {	//IBPLIFE-2472
			if ((isNE(chdrlifIO.getValidflag(), "1") && isNE(chdrlifIO.getValidflag(), "3"))
				&& isEQ(wsaaCfiafiFlag, SPACES)) {
					syserrrec.params.set(chdrlifIO.getParams());
					syserrrec.statuz.set(chdrlifIO.getStatuz());
					xxxxFatalError();
			}
		}
		else if (isNE(chdrlifIO.getValidflag(), "1")
		&& isEQ(wsaaCfiafiFlag, SPACES)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		restoreChdrMinorAlt3100();
	}

protected void readhPayr3030()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		restorePayrMinorAlt3200();
	}

protected void updateFields3040()
	{
		if (isEQ(wsaaTransAreaInner.wsaaSupflag, "Y")) {
			chdrlifIO.setBillsupr("Y");
			payrIO.setBillsupr("Y");
			chdrlifIO.setBillspfrom(wsaaTransAreaInner.wsaaTodate);
			payrIO.setBillspfrom(wsaaTransAreaInner.wsaaTodate);
			chdrlifIO.setBillspto(wsaaTransAreaInner.wsaaSuppressTo);
			payrIO.setBillspto(wsaaTransAreaInner.wsaaSuppressTo);
		}
		if (isNE(payrIO.getBillcd(), wsaaPayrBillcd)) {
			updatePayrNextdate3300();
		}
	}

protected void rewrtChdr3050()
	{
		chdrlifIO.setTranno(wsaaNewTranno);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		varcom.vrcmDate.set(wsaaTransAreaInner.wsaaTranDate);
		varcom.vrcmTime.set(wsaaTransAreaInner.wsaaTranTime);
		varcom.vrcmUser.set(wsaaTransAreaInner.wsaaUser);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		varcom.vrcmCompTermid.set(wsaaTransAreaInner.wsaaTermid);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rewrtPayr3070()
	{
		payrIO.setTranno(wsaaNewTranno);
		payrIO.setTransactionDate(wsaaTransAreaInner.wsaaTranDate);
		payrIO.setTransactionTime(wsaaTransAreaInner.wsaaTranTime);
		payrIO.setTermid(wsaaTransAreaInner.wsaaTermid);
		payrIO.setUser(wsaaTransAreaInner.wsaaUser);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void restoreChdrMinorAlt3100()
	{
		start3101();
	}

protected void start3101()
	{
		chdrlifIO.setAplsupr(wsaaChdrAplsupr);
		chdrlifIO.setBillsupr(wsaaChdrBillsupr);
		chdrlifIO.setCommsupr(wsaaChdrCommsupr);
		chdrlifIO.setNotssupr(wsaaChdrNotssupr);
		chdrlifIO.setRnwlsupr(wsaaChdrRnwlsupr);
		chdrlifIO.setAplspfrom(wsaaChdrAplspfrom);
		chdrlifIO.setBillspfrom(wsaaChdrBillspfrom);
		chdrlifIO.setCommspfrom(wsaaChdrCommspfrom);
		chdrlifIO.setNotsspfrom(wsaaChdrNotsspfrom);
		chdrlifIO.setRnwlspfrom(wsaaChdrRnwlspfrom);
		chdrlifIO.setAplspto(wsaaChdrAplspto);
		chdrlifIO.setBillspto(wsaaChdrBillspto);
		chdrlifIO.setCommspto(wsaaChdrCommspto);
		chdrlifIO.setNotsspto(wsaaChdrNotsspto);
		chdrlifIO.setRnwlspto(wsaaChdrRnwlspto);
		chdrlifIO.setCownpfx(wsaaChdrCownpfx);
		chdrlifIO.setCowncoy(wsaaChdrCowncoy);
		chdrlifIO.setCownnum(wsaaChdrCownnum);
		chdrlifIO.setJownnum(wsaaChdrJownnum);
		chdrlifIO.setPolsum(wsaaChdrPolsum);
		chdrlifIO.setDesppfx(wsaaChdrDesppfx);
		chdrlifIO.setDespcoy(wsaaChdrDespcoy);
		chdrlifIO.setDespnum(wsaaChdrDespnum);
		chdrlifIO.setAgntpfx(wsaaChdrAgntpfx);
		chdrlifIO.setAgntcoy(wsaaChdrAgntcoy);
		chdrlifIO.setAgntnum(wsaaChdrAgntnum);
	}

protected void restorePayrMinorAlt3200()
	{
		/*START*/
		/* MOVE  WSAA-CHDR-APLSUPR      TO  CHDRLIF-APLSUPR.            */
		/* MOVE  WSAA-CHDR-BILLSUPR     TO  CHDRLIF-BILLSUPR.           */
		/* MOVE  WSAA-CHDR-NOTSSUPR     TO  CHDRLIF-NOTSSUPR.           */
		/* MOVE  WSAA-CHDR-APLSPFROM    TO  CHDRLIF-APLSPFROM.          */
		/* MOVE  WSAA-CHDR-BILLSPFROM   TO  CHDRLIF-BILLSPFROM.         */
		/* MOVE  WSAA-CHDR-NOTSSPFROM   TO  CHDRLIF-NOTSSPFROM.         */
		/* MOVE  WSAA-CHDR-APLSPTO      TO  CHDRLIF-APLSPTO.            */
		/* MOVE  WSAA-CHDR-BILLSPTO     TO  CHDRLIF-BILLSPTO.           */
		/* MOVE  WSAA-CHDR-NOTSSPTO     TO  CHDRLIF-NOTSSPTO.           */
		payrIO.setAplsupr(wsaaPayrAplsupr);
		payrIO.setBillsupr(wsaaPayrBillsupr);
		payrIO.setNotssupr(wsaaPayrNotssupr);
		payrIO.setAplspfrom(wsaaPayrAplspfrom);
		payrIO.setBillspfrom(wsaaPayrBillspfrom);
		payrIO.setNotsspfrom(wsaaPayrNotsspfrom);
		payrIO.setAplspto(wsaaPayrAplspto);
		payrIO.setBillspto(wsaaPayrBillspto);
		payrIO.setNotsspto(wsaaPayrNotsspto);
		/*EXIT*/
	}

protected void updatePayrNextdate3300()
	{
		start3301();
		continue3340();
	}

protected void start3301()
	{
		String key;
		if (BTPRO028Permission) {
			key  = payrIO.getBillchnl().toString().trim()+chdrlifIO.getCnttype().toString().trim()+payrIO.getBillfreq().toString().trim();
			if (!readT6654600(key)) {
				key  = payrIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if (!readT6654600(key)) {
					key  = payrIO.getBillchnl().toString().trim().concat("*****");
					if (!readT6654600(key)) {
						syserrrec.params.set(itemIO.getParams());
						syserrrec.statuz.set(itemIO.getStatuz());
						xxxxFatalError();
					}
				}
			}
		}
		else {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(payrIO.getChdrcoy());
			itemIO.setItemtabl(t6654);
			wsaaT6654Item.set(SPACES);
			wsaaT6654Billchnl.set(payrIO.getBillchnl());
			wsaaT6654Cnttype.set(chdrlifIO.getCnttype());
			itemIO.setItemitem(wsaaT6654Item);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				t6654rec.t6654Rec.set(itemIO.getGenarea());
				return ;
			}
			wsaaT6654Cnttype.set("***");
			itemIO.setItemitem(wsaaT6654Item);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
	}

protected boolean readT6654600(String key)
{
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(payrIO.getChdrcoy().toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}


protected void continue3340()
	{
		datcon2rec.intDate1.set(payrIO.getBillcd());
		datcon2rec.frequency.set("DY");
		compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays, -1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		payrIO.setNextdate(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void housekeeping4000()
	{
		/*START*/
		addNewPtrn4100();
		addNewBatch4500();
		dryProcessing8000();
		a100AddUnderwritting();
		releaseSoftlock4800();
		printerFile.close();
		/*EXIT*/
	}

protected void addNewPtrn4100()
	{
		start4110();
	}

protected void start4110()
	{
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(wsaaNewTranno);
		if(isEQ(wsaaBatckey.batcBatctrcde,"T646") || isEQ(wsaaBatckey.batcBatctrcde,"TR7K") || isEQ(wsaaBatckey.batcBatctrcde,"T607")) //PINNACLE-2848        
		ptrnIO.setPtrneff(chdrlifIO.getOccdate());
	    else
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransAreaInner.wsaaTranDate);
		ptrnIO.setTransactionTime(wsaaTransAreaInner.wsaaTranTime);
		ptrnIO.setUser(wsaaTransAreaInner.wsaaUser);
		ptrnIO.setTermid(wsaaTransAreaInner.wsaaTermid);
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(username); //qwertyasd
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void addNewBatch4500()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void releaseSoftlock4800()
	{
		/*START*/
	slckpf.setEnttyp(chdrlifIO.getChdrpfx().toString());
	slckpf.setCompany(chdrlifIO.getChdrcoy().toString());
	slckEntity.set(chdrlifIO.getChdrnum());
	slckpf.setEntity(slckEntity.toString());
	
	Slckpf result = slckpfDAO.readHSlckData(slckpf);
	
	if (result != null) {
		 
		
			sftlockrec.sftlockRec.set(SPACES);
			sftlockrec.enttyp.set(chdrlifIO.getChdrpfx());
			sftlockrec.company.set(chdrlifIO.getChdrcoy());
			sftlockrec.entity.set(chdrlifIO.getChdrnum());
			sftlockrec.transaction.set(atmodrec.batchKey);
			sftlockrec.function.set("UNLK");
			callProgram(Sftlock.class, sftlockrec.sftlockRec);
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(sftlockrec.statuz);
				xxxxFatalError();
			}
		
	}
		/*EXIT*/
	}

protected void cfiafiProcessing5000()
	{
		start5000();
	}

protected void start5000()
	{
		/* This is the Cancel/Alter from Inception specific                */
		/*  processing section.                                            */
		/* First read T6799, the CFI/AFI processing Subroutine table       */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6799);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t6799rec.t6799Rec.set(itemIO.getGenarea());
		reverserec.chdrnum.set(chdrlifIO.getChdrnum());
		reverserec.company.set(chdrlifIO.getChdrcoy());
		reverserec.tranno.set(ptrnrevIO.getTranno());
		reverserec.ptrneff.set(ptrnrevIO.getPtrneff());
		reverserec.oldBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.language.set(atmodrec.language);
		reverserec.newTranno.set(wsaaNewTranno);
		reverserec.effdate1.set(wsaaTransAreaInner.wsaaTodate);
		reverserec.effdate2.set(wsaaTransAreaInner.wsaaSuppressTo);
		reverserec.batchkey.set(atmodrec.batchKey);
		reverserec.ptrnBatcpfx.set(ptrnrevIO.getBatcpfx());
		reverserec.ptrnBatccoy.set(ptrnrevIO.getBatccoy());
		reverserec.ptrnBatcbrn.set(ptrnrevIO.getBatcbrn());
		reverserec.ptrnBatcactyr.set(ptrnrevIO.getBatcactyr());
		reverserec.ptrnBatcactmn.set(ptrnrevIO.getBatcactmn());
		reverserec.ptrnBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.ptrnBatcbatch.set(ptrnrevIO.getBatcbatch());
		reverserec.transDate.set(wsaaTransAreaInner.wsaaTranDate);
		reverserec.transTime.set(wsaaTransAreaInner.wsaaTranTime);
		reverserec.user.set(wsaaTransAreaInner.wsaaUser);
		reverserec.termid.set(wsaaTransAreaInner.wsaaTermid);
		reverserec.planSuffix.set(wsaaTransAreaInner.wsaaPlnsfx);
		reverserec.bbldat.set(wsaaTransAreaInner.wsaaBbldat);
		reverserec.statuz.set(varcom.oK);
		if (isNE(t6799rec.subprog, SPACES)) {
			callProgram(t6799rec.subprog, reverserec.reverseRec);
			if (isNE(reverserec.statuz, varcom.oK)) {
				syserrrec.params.set(reverserec.reverseRec);
				syserrrec.statuz.set(reverserec.statuz);
				xxxxFatalError();
			}
		}
	}

protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorBomb();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		/* MOVE WSAA-TRANNO            TO LIFS-TRANNO.             <005>*/
		lifsttrrec.tranno.set(wsaaNewTranno);
		/* MOVE PTRNREV-TRANNO         TO LIFS-TRANNOR.            <003>*/
		lifsttrrec.trannor.set(wsaaTransAreaInner.wsaaTranNum);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void a100AddUnderwritting()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		a300BegnLifeio();
		a200DelUnderwritting();
		/*                                                      <LA3425>*/
		/* If CFI, do not generate UNDRPF records.              <LA3425>*/
		/*                                                      <LA3425>*/
		if (isNE(t6799rec.ind, "Y")) {
			return ;
		}
		covrtrbIO.setRecKeyData(SPACES);
		covrtrbIO.setChdrcoy(chdrlifIO.getChdrcoy());
		covrtrbIO.setChdrnum(chdrlifIO.getChdrnum());
		covrtrbIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covrtrbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrtrbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		covrtrbIO.setStatuz(varcom.oK);
		while ( !(isNE(covrtrbIO.getStatuz(), varcom.oK))) {
			a400NextrCovrtrbio();
		}

		covtcsnIO.setRecKeyData(SPACES);
		covtcsnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		covtcsnIO.setChdrnum(chdrlifIO.getChdrnum());
		covtcsnIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		covtcsnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtcsnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		covtcsnIO.setStatuz(varcom.oK);
		while ( !(isNE(covtcsnIO.getStatuz(), varcom.oK))) {
			a500NextrCovtcsnio();
		}

	}

protected void a200DelUnderwritting()
	{
		a200Ctrl();
	}

protected void a200Ctrl()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrlifIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrlifIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
		
		//ILIFE-8123-starts
		rolloverFlag = FeaConfg.isFeatureExist(chdrlifIO.getChdrcoy().toString().trim(), ROLLOVER_FEATURE_ID, appVars,
				"IT");
		if (rolloverFlag) {
			updateRlvr();
		}
		
		//ILIFE-8123-ends
	}

		//ILIFE-8123-starts
		private void updateRlvr() {
			
			rlvrpf =new Rlvrpf();
			rlvrpf.setChdrcoy(chdrlifIO.getChdrcoy().toString());
			rlvrpf.setChdrnum(chdrlifIO.getChdrnum().toString());
			rlvrpf.setValidFlag("2");
			rlvrpf.setLife(wsaaTransAreaInner.wsaaLife.toString());
			rlvrpf.setCoverage(wsaaTransAreaInner.wsaaCover.toString());
			rlvrpfDAO.updateRollovRecord(rlvrpf);
		}
		//ILIFE-8123-ends

protected void a300BegnLifeio()
	{
		a300Ctrl();
	}

protected void a300Ctrl()
	{
		lifeIO.setRecKeyData(SPACES);
		lifeIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");*/

		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (!(isEQ(lifeIO.getStatuz(), varcom.oK)
		&& isEQ(chdrlifIO.getChdrcoy(), lifeIO.getChdrcoy())
		&& isEQ(chdrlifIO.getChdrnum(), lifeIO.getChdrnum()))) {
			lifeIO.setStatuz(varcom.endp);
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a400NextrCovrtrbio()
	{
		a400Ctrl();
	}

protected void a400Ctrl()
	{
		covrtrbIO.setFormat(covrtrbrec);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (!(isEQ(covrtrbIO.getStatuz(), varcom.oK)
		&& isEQ(chdrlifIO.getChdrcoy(), covrtrbIO.getChdrcoy())
		&& isEQ(chdrlifIO.getChdrnum(), covrtrbIO.getChdrnum()))) {
			covrtrbIO.setStatuz(varcom.endp);
			return ;
		}
		covrtrbIO.setFunction(varcom.nextr);
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrlifIO.getChdrcoy());
		crtundwrec.chdrnum.set(chdrlifIO.getChdrnum());
		crtundwrec.life.set(covrtrbIO.getLife());
		crtundwrec.crtable.set(covrtrbIO.getCrtable());
		crtundwrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		crtundwrec.sumins.set(covrtrbIO.getSumins());
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.function.set("ADD");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
	}

protected void a500NextrCovtcsnio()
	{
		a500Ctrl();
	}

protected void a500Ctrl()
	{
		covtcsnIO.setFormat(covtcsnrec);
		SmartFileCode.execute(appVars, covtcsnIO);
		if (isEQ(covtcsnIO.getStatuz(), varcom.endp)
		|| isNE(chdrlifIO.getChdrcoy(), covtcsnIO.getChdrcoy())
		|| isNE(chdrlifIO.getChdrnum(), covtcsnIO.getChdrnum())) {
			covtcsnIO.setStatuz(varcom.endp);
			return ;
		}
		covtcsnIO.setFunction(varcom.nextr);
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(covtcsnIO.getChdrcoy());
		crtundwrec.chdrnum.set(covtcsnIO.getChdrnum());
		crtundwrec.life.set(covtcsnIO.getLife());
		crtundwrec.crtable.set(covtcsnIO.getCrtable());
		crtundwrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		crtundwrec.sumins.set(covtcsnIO.getSumins());
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.function.set("ADD");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlifIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		//ILPI-148- FCR Unit deal issue
		drypDryprcRecInner.drypRunDate.set(wsaaToday);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(wsaaPrimaryChdrnum);
		//ILPI-148- FCR Unit Deal issue
		drypDryprcRecInner.drypEffectiveDate.set(wsaaToday);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaTransAreaInner.wsaaFsuCoy);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypCpiDate.set(covrIO.getCpiDate());
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(chdrlifIO.getAplspto());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		/* Retrieve run number (Needed when running untdel after reversal).*/
		/* Check the Level                                                 */
		wsaaLibrary.set(SPACES);
		wsaaDataArea.set("LCURRENT");
		callProgram(Chkdtaara.class, wsaaStatus, wsaaValue, wsaaLibrary, wsaaDataArea);
		wsaaLevel.set(wsaaValue);
		/* Get the run number from the data area.                          */
		wsaaDataArea.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaLevel, SPACES);
		stringVariable1.addExpression("EXC");
		stringVariable1.setStringInto(wsaaLibrary);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("DRY");
		stringVariable2.addExpression(wsaaLevel, SPACES);
		stringVariable2.setStringInto(wsaaDataArea);
		wsaaStatuz.set(varcom.oK);
		callProgram(Chkdtaara.class, wsaaStatuz, wsaaValue, wsaaLibrary, wsaaDataArea);
		drypDryprcRecInner.drypThreadNumber.set(ZERO);
		drypDryprcRecInner.drypRunNumber.set(wsaaRunNumber);
		drypDryprcRecInner.drypUser.set(wsaaTransAreaInner.wsaaUser);
		//ILIFE-1230 by smalchi2 STARTS
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		//ENDS
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

/*
 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
 */
protected static final class WsaaTransAreaInner {

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(75);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	public ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	public ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	public ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	public FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	public ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	public ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	public ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	public ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 70);
	private FixedLengthStringData wsaaCover = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 71);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 73);
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(2, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
