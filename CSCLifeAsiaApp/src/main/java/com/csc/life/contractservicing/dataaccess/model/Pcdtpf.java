package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;

import com.quipoz.framework.datatype.BaseData; //ILIFE-8786

public class Pcdtpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private int plnsfx;
	private String life;
	private String coverage;
	private String rider;
	private int tranno;
	private String agntnum01;
	private String agntnum02;
	private String agntnum03;
	private String agntnum04;
	private String agntnum05;
	private String agntnum06;
	private String agntnum07;
	private String agntnum08;
	private String agntnum09;
	private String agntnum10;
	private BigDecimal splitc01;
	private BigDecimal splitc02;
	private BigDecimal splitc03;
	private BigDecimal splitc04;
	private BigDecimal splitc05;
	private BigDecimal splitc06;
	private BigDecimal splitc07;
	private BigDecimal splitc08;
	private BigDecimal splitc09;
	private BigDecimal splitc10;
	private BigDecimal instprem;
	private String usrprf;
	private String jobnm;
 //ILIFE-8786 start	
	public Pcdtpf() {
		
	}
	
	public Pcdtpf(Pcdtpf pcdtpf) {
		this.uniqueNumber=pcdtpf.uniqueNumber;
		this.chdrcoy=pcdtpf.chdrcoy;
		this.chdrnum=pcdtpf.chdrnum;
		this.plnsfx=pcdtpf.plnsfx;
		this.life=pcdtpf.life;
		this.coverage=pcdtpf.coverage;
		this.rider=pcdtpf.rider;
		this.tranno=pcdtpf.tranno;
		this.agntnum01=pcdtpf.agntnum01;
		this.agntnum02=pcdtpf.agntnum02;
		this.agntnum03=pcdtpf.agntnum03;
		this.agntnum04=pcdtpf.agntnum04;
		this.agntnum05=pcdtpf.agntnum05;
		this.agntnum06=pcdtpf.agntnum06;
		this.agntnum07=pcdtpf.agntnum07;
		this.agntnum08=pcdtpf.agntnum08;
		this.agntnum09=pcdtpf.agntnum09;
		this.agntnum10=pcdtpf.agntnum10;
		this.splitc01=pcdtpf.splitc01;
		this.splitc02=pcdtpf.splitc02;
		this.splitc03=pcdtpf.splitc03;
		this.splitc04=pcdtpf.splitc04;
		this.splitc05=pcdtpf.splitc05;
		this.splitc06=pcdtpf.splitc06;
		this.splitc07=pcdtpf.splitc07;
		this.splitc08=pcdtpf.splitc08;
		this.splitc09=pcdtpf.splitc09;
		this.splitc10=pcdtpf.splitc10;
		this.instprem=pcdtpf.instprem;
		this.usrprf=pcdtpf.usrprf;
		this.jobnm=pcdtpf.jobnm;
		
	}
	 //ILIFE-8786 end
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getAgntnum01() {
		return agntnum01;
	}
	public void setAgntnum01(String agntnum01) {
		this.agntnum01 = agntnum01;
	}
	public String getAgntnum02() {
		return agntnum02;
	}
	public void setAgntnum02(String agntnum02) {
		this.agntnum02 = agntnum02;
	}
	public String getAgntnum03() {
		return agntnum03;
	}
	public void setAgntnum03(String agntnum03) {
		this.agntnum03 = agntnum03;
	}
	public String getAgntnum04() {
		return agntnum04;
	}
	public void setAgntnum04(String agntnum04) {
		this.agntnum04 = agntnum04;
	}
	public String getAgntnum05() {
		return agntnum05;
	}
	public void setAgntnum05(String agntnum05) {
		this.agntnum05 = agntnum05;
	}
	public String getAgntnum06() {
		return agntnum06;
	}
	public void setAgntnum06(String agntnum06) {
		this.agntnum06 = agntnum06;
	}
	public String getAgntnum07() {
		return agntnum07;
	}
	public void setAgntnum07(String agntnum07) {
		this.agntnum07 = agntnum07;
	}
	public String getAgntnum08() {
		return agntnum08;
	}
	public void setAgntnum08(String agntnum08) {
		this.agntnum08 = agntnum08;
	}
	public String getAgntnum09() {
		return agntnum09;
	}
	public void setAgntnum09(String agntnum09) {
		this.agntnum09 = agntnum09;
	}
	public String getAgntnum10() {
		return agntnum10;
	}
	public void setAgntnum10(String agntnum10) {
		this.agntnum10 = agntnum10;
	}
	public BigDecimal getSplitc01() {
		return splitc01;
	}
	public void setSplitc01(BigDecimal splitc01) {
		this.splitc01 = splitc01;
	}
	public BigDecimal getSplitc02() {
		return splitc02;
	}
	public void setSplitc02(BigDecimal splitc02) {
		this.splitc02 = splitc02;
	}
	public BigDecimal getSplitc03() {
		return splitc03;
	}
	public void setSplitc03(BigDecimal splitc03) {
		this.splitc03 = splitc03;
	}
	public BigDecimal getSplitc04() {
		return splitc04;
	}
	public void setSplitc04(BigDecimal splitc04) {
		this.splitc04 = splitc04;
	}
	public BigDecimal getSplitc05() {
		return splitc05;
	}
	public void setSplitc05(BigDecimal splitc05) {
		this.splitc05 = splitc05;
	}
	public BigDecimal getSplitc06() {
		return splitc06;
	}
	public void setSplitc06(BigDecimal splitc06) {
		this.splitc06 = splitc06;
	}
	public BigDecimal getSplitc07() {
		return splitc07;
	}
	public void setSplitc07(BigDecimal splitc07) {
		this.splitc07 = splitc07;
	}
	public BigDecimal getSplitc08() {
		return splitc08;
	}
	public void setSplitc08(BigDecimal splitc08) {
		this.splitc08 = splitc08;
	}
	public BigDecimal getSplitc09() {
		return splitc09;
	}
	public void setSplitc09(BigDecimal splitc09) {
		this.splitc09 = splitc09;
	}
	public BigDecimal getSplitc10() {
		return splitc10;
	}
	public void setSplitc10(BigDecimal splitc10) {
		this.splitc10 = splitc10;
	}
	public BigDecimal getInstprem() {
		return instprem;
	}
	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	 //ILIFE-8786 start
	public String getAgntnum(BaseData indx) {
		return getAgntnum(indx.toInt());
	}
	
	public String getAgntnum(int indx) {

		switch (indx) {
			case 1 : return agntnum01;
			case 2 : return agntnum02;
			case 3 : return agntnum03;
			case 4 : return agntnum04;
			case 5 : return agntnum05;
			case 6 : return agntnum06;
			case 7 : return agntnum07;
			case 8 : return agntnum08;
			case 9 : return agntnum09;
			case 10 : return agntnum10;
			default: return " "; 
		}
	
	}
	
	public BigDecimal getSplitc(BaseData indx) {
		return getSplitc(indx.toInt());
	}
	public BigDecimal getSplitc(int indx) {

		switch (indx) {
			case 1 : return splitc01;
			case 2 : return splitc02;
			case 3 : return splitc03;
			case 4 : return splitc04;
			case 5 : return splitc05;
			case 6 : return splitc06;
			case 7 : return splitc07;
			case 8 : return splitc08;
			case 9 : return splitc09;
			case 10 : return splitc10;
			default: return new BigDecimal(0); 
		}
	
	}
 //ILIFE-8786 end
}
