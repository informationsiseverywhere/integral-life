package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sh5mp
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class Sh5mpScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(778);
	public FixedLengthStringData dataFields = new FixedLengthStringData(346).isAPartOf(dataArea, 0);
	public ZonedDecimalData advptdat = DD.advptdat.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,66);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,68);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,90);
	public ZonedDecimalData cpiDate = DD.cpidte.copyToZonedDecimal().isAPartOf(dataFields,98);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,106);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,136);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,144);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,152);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,246);
	public ZonedDecimalData prmdepst = DD.prmdepst.copyToZonedDecimal().isAPartOf(dataFields,254);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,271);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,281);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,289);
	public ZonedDecimalData sacscurbal = DD.sacscurbal.copyToZonedDecimal().isAPartOf(dataFields,299);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,316);
	public FixedLengthStringData pmtfreq = DD.billfreq.copy().isAPartOf(dataFields,333);
	public FixedLengthStringData prorateflag=DD.action.copy().isAPartOf(dataFields,335);
	public FixedLengthStringData billday = DD.billday.copy().isAPartOf(dataFields,336);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,338);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 346);
	public FixedLengthStringData advptdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cpidteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData prmdepstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData sacscurbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData instPremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData pmtfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData prorateflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData billdayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(324).isAPartOf(dataArea, 454);
	public FixedLengthStringData[] advptdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cpidteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] prmdepstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] sacscurbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] instPremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] pmtfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] prorateflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] billdayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData advptdatDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cpiDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);

	public LongData Sh5mpscreenWritten = new LongData(0);
	public LongData Sh5mpprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh5mpScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(billfreqOut,new String[] {"01","15","-01","04", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ptdateOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(advptdatOut,new String[] {"05", "08", "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billdayOut,new String[] {"72","73", "-72","74", null, null, null, null, null, null, null, null});
		fieldIndMap.put(billcdOut,new String[] {"09","40","-09",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, rstate, pstate, cownnum, ownername, payrnum, payorname, agntnum, agentname, occdate, ptdate, cntcurr, btdate, billfreq, sacscurbal, advptdat, effdate, cpiDate, billcurr, prmdepst, instPrem,pmtfreq,prorateflag,billday,billcd};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, rstateOut, pstateOut, cownnumOut, ownernameOut, payrnumOut, payornameOut, agntnumOut, agentnameOut, occdateOut, ptdateOut, cntcurrOut, btdateOut, billfreqOut, sacscurbalOut, advptdatOut, effdateOut, cpidteOut, billcurrOut, prmdepstOut, instPremOut,pmtfreqOut,prorateflagOut,billdayOut,billcdOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, rstateErr, pstateErr, cownnumErr, ownernameErr, payrnumErr, payornameErr, agntnumErr, agentnameErr, occdateErr, ptdateErr, cntcurrErr, btdateErr, billfreqErr, sacscurbalErr, advptdatErr, effdateErr, cpidteErr, billcurrErr, prmdepstErr, instPremErr,pmtfreqErr,prorateflagErr,billdayErr,billcdErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, advptdat, effdate, cpiDate,billcd};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, advptdatErr, effdateErr, cpidteErr,billcdErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, advptdatDisp, effdateDisp, cpiDateDisp,billcdDisp};
		
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh5mpscreen.class;
		protectRecord = Sh5mpprotect.class;
	}

}
