/*
 * File: P5173cl.java
 * Date: 30 August 2009 2:59:26
 * Author: $Id$
 * 
 * Class transformed from P5173CL.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.batch.cls.BatchCLFunctions;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class P5173cl extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData status = new FixedLengthStringData(4);
	private FixedLengthStringData jobnum = new FixedLengthStringData(6);
	private FixedLengthStringData jobuser = new FixedLengthStringData(10);
	private FixedLengthStringData jobname = new FixedLengthStringData(10);
	private FixedLengthStringData msgid = new FixedLengthStringData(7);
	private FixedLengthStringData msgf = new FixedLengthStringData(10);
	private FixedLengthStringData msglib = new FixedLengthStringData(10);
	private FixedLengthStringData msgdta = new FixedLengthStringData(100);

	public P5173cl() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		jobname = convertAndSetParam(jobname, parmArray, 3);
		jobuser = convertAndSetParam(jobuser, parmArray, 2);
		jobnum = convertAndSetParam(jobnum, parmArray, 1);
		status = convertAndSetParam(status, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					status.set("****");
					//appVars.receiveExceptionMessage("SBMTHREAD", COBOLAppVars.LAST, msgdta, msgid, msgf, msglib, msgdta, msgid);
					//ILIFE-960
					/* Used to handle message CPC1221*/
					BatchCLFunctions.receiveMessage(null, null, "*LAST", msgdta, null, null, null, msgid, msgf, msglib);
					if (isNE(msgid,"CPC1221")) {
						qState = error;
						break;
					}
					jobname.set(subString(msgdta, 1, 10));
					jobuser.set(subString(msgdta, 11, 10));
					jobnum.set(subString(msgdta, 21, 6));
					/*@TODO The following command needs attention*/
					/*CLRMSGQ MSGQ ( QTEMP / SBMTHREAD )*/
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.sendMessageToQueue("Unexpected errors occurred", "*");
					status.set("EROR");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
