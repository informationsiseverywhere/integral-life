/*
 * File: P5085.java
 * Date: 30 August 2009 0:05:53
 * Author: Quipoz Limited
 *
 * Class transformed from P5085.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.S5085ScreenVars;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                    SUPPRESS RENEWAL COMMISSION
*
* This   is   a  minor  alteration,  to  suppress  the  Renewal
* Processing  on the contract header with details captured from
* screen/program P5085.
*
*Initialise
*----------
*
* Read CHDRMNA (RETRV)  in  order to obtain the contract header
* information.
*
* Read  the  DESCIO  in  order to obtain the description of the
* risk and premium statuses.
*
* Read  the clients details and format any names required using
* the relevant copybook.
*
*
*Validation
*----------
*
* If KILL is requested, then skip the validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
* Date check.
*
*  - From Date must not be less than the CCD
*  - From Date must not be greater than the To Date
*  - To Date must not be greater than the expiry-date
*  - ensure that the 'To Date' is not greater than
*    SINSTTO on the Contract header.
*
* The SINSTTO field is not a LIFE requirement so any  reference
* to this has been removed.
*
* Reason field check
*
*  - if the dates  have  changed and the reason code or the
*       reason description  are equal to spaces, then is an
*       error as  the reason code and description must also
*       change.
*
*  - if the  reason  code  is  supplied  and  there  is  no
*       description, then access table T5500 and output the
*       long description.  If the reason code is not found,
*       this is an error.
*
*
*Updating
*--------
*
* Updating occurs  with  the  updating  of  the contract header
* record, the creation of a Reason record and the creation of a
* Policy transaction (PTRN) record.
*
* If the 'KILL' function key was pressed, skip the updating.
*
*CONTRACT HEADER UPDATE
*
*  - if there were  no  changes,  then  do  not  update the
*       contract header record.
*
*  - if the current  'From  and  To Dates' are equal to the
*       VRCM-MAX-DATE,  then zeroise the Suppress-Renewal(from
*       and  to)  date  fields  and  set  the  Suppress-renewal
*       indicator  to  'N'. Otherwise, set the Suppress-renewal
*       date  to   the   current   'From   and   To'  dates
*       respectively  and set the Suppress-renewal indicator to
*       'Y'.
*
*  - rewrite the contract header record.
*
* CREATE REASON RECORD
*
*  - create  a  Reason details record (RESNLNB) in order to
*       hold   the   description   and   reason   for   the
*       suppression of the renewal commission processing.
*
* GENERAL HOUSEKEEPING
*
*       - Write a PTRN record (if updating the CHDR):
*
*           - contract key from contract header
*           - transaction number from WSSP
*           - transaction effective date from VRCM-DATE
*           - batch key information WSSP batchkey
*
*      - Update the header by calling BATCUP with a function of
*           SPACES and the following parameters:
*
*           - transaction count equals 1
*           - all other amounts equal to zero
*           - batch key from WSSP
*
*      - Release contract SFTLOCK.
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5085 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5085");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaOrigReasoncd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaOrigResndesc = new FixedLengthStringData(50);
	private int wsaaLastTranno = 0;
	private String wsaaWriteLetter = "N";

	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 1).setUnsigned();
		/* ERRORS */
	private String e017 = "E017";
	private String e304 = "E304";
	private String f515 = "F515";
	private String e374 = "E374";
	private String f665 = "F665";
		/* TABLES */
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t5688 = "T5688";
	private String t5500 = "T5500";
	private String tr384 = "TR384";
	private String ptrnrec = "PTRNREC";
	private String resnrec = "RESNREC";
	private String chdrmnarec = "CHDRMNAREC";
	private String resnenqrec = "RESNENQREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
		/*Joind FSU and Life agent headers - new b*/
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private Batcuprec batcuprec1 = new Batcuprec();
		/*Minor Alterations Contract Header*/
	protected ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
		/*Client role and relationships logical fi*/
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Letrqstrec letrqstrec = new Letrqstrec();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Reason Details  Logical File*/
	private ResnTableDAM resnIO = new ResnTableDAM();
		/*Reasn Details Enquiry*/
	private ResnenqTableDAM resnenqIO = new ResnenqTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Batckey wsaaBatcKey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	protected S5085ScreenVars sv = ScreenProgram.getScreenVars( S5085ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		lgnmExit,
		plainExit,
		payeeExit,
		preExit,
		checkToDate2015,
		reascd2050,
		resndesc2070,
		readTr3843220,
		exit3290
	}

	public P5085() {
		super();
		screenVars = sv;
		new ScreenModel("S5085", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
	}

protected void initialise1010()
	{
		wsaaBatcKey.set(wsspcomn.batchkey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		sv.occdate.set(chdrmnaIO.getOccdate());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		payrIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmnaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		sv.cownnum.set(chdrmnaIO.getCownnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		sv.btdate.set(chdrmnaIO.getBtdate());
		sv.billfreq.set(chdrmnaIO.getBillfreq());
		sv.mop.set(chdrmnaIO.getBillchnl());
		sv.srcebus.set(chdrmnaIO.getSrcebus());
		sv.reptype.set(chdrmnaIO.getReptype());
		sv.register.set(chdrmnaIO.getRegister());
		sv.instpramt.set(payrIO.getSinstamt06());
		sv.cntcurr.set(chdrmnaIO.getCntcurr());
		sv.lapind.set(chdrmnaIO.getAplsupr());
		if (isEQ(chdrmnaIO.getAplspfrom(),ZERO)) {
			sv.lapsfrom.set(varcom.vrcmMaxDate);
			sv.lapsto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.lapsfrom.set(chdrmnaIO.getAplspfrom());
			sv.lapsto.set(chdrmnaIO.getAplspto());
		}
		sv.notind.set(chdrmnaIO.getNotssupr());
		if (isEQ(chdrmnaIO.getNotsspfrom(),ZERO)) {
			sv.notsfrom.set(varcom.vrcmMaxDate);
			sv.notsto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.notsfrom.set(chdrmnaIO.getNotsspfrom());
			sv.notsto.set(chdrmnaIO.getNotsspto());
		}
		sv.billind.set(chdrmnaIO.getNotssupr());
		if (isEQ(chdrmnaIO.getBillspfrom(),ZERO)) {
			sv.billfrom.set(varcom.vrcmMaxDate);
			sv.billto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.billfrom.set(chdrmnaIO.getBillspfrom());
			sv.billto.set(chdrmnaIO.getBillspto());
		}
		sv.renind.set(chdrmnaIO.getRnwlsupr());
		if (isEQ(chdrmnaIO.getRnwlspfrom(),ZERO)) {
			sv.currfrom.set(varcom.vrcmMaxDate);
			sv.currto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.currfrom.set(chdrmnaIO.getRnwlspfrom());
			sv.currto.set(chdrmnaIO.getRnwlspto());
		}
		sv.comind.set(chdrmnaIO.getCommsupr());
		if (isEQ(chdrmnaIO.getCommspfrom(),ZERO)) {
			sv.commfrom.set(varcom.vrcmMaxDate);
			sv.commto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.commfrom.set(chdrmnaIO.getCommspfrom());
			sv.commto.set(chdrmnaIO.getCommspto());
		}
		sv.bnsind.set(SPACES);
		sv.bnsfrom.set(varcom.vrcmMaxDate);
		sv.bnsto.set(varcom.vrcmMaxDate);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmnaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		if (isNE(sv.cownnum,SPACES)) {
			formatClientName1700();
		}
		formatResnDetails5000();
	}

protected void formatClientName1700()
	{
		readClientRecord1710();
	}

protected void readClientRecord1710()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.cownnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case checkToDate2015: {
				}
				case reascd2050: {
					reascd2050();
				}
				case resndesc2070: {
					resndesc2070();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isLT(sv.currfrom,sv.occdate)) {
			sv.currfromErr.set(f515);
			goTo(GotoLabel.checkToDate2015);
		}
		if (isGT(sv.currfrom,sv.currto)) {
			sv.currfromErr.set(e017);
		}
		if (isEQ(sv.currfrom,varcom.vrcmMaxDate)
		&& isEQ(sv.currto,varcom.vrcmMaxDate)) {
			goTo(GotoLabel.reascd2050);
		}
	}

protected void reascd2050()
	{
		if (isEQ(sv.reasoncd,SPACES)) {
			goTo(GotoLabel.resndesc2070);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5500);
		descIO.setDescitem(sv.reasoncd);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)
		&& isEQ(sv.resndesc,SPACES)) {
			sv.resndesc.set(descIO.getLongdesc());
		}
		if (isEQ(sv.currfrom,varcom.vrcmMaxDate)
		&& isEQ(sv.currto,varcom.vrcmMaxDate)) {
			sv.currfromErr.set(f665);
		}
	}

protected void resndesc2070()
	{
		if (isNE(sv.resndesc,SPACES)
		&& isEQ(sv.currfrom,varcom.vrcmMaxDate)
		&& isEQ(sv.currto,varcom.vrcmMaxDate)) {
			sv.currfromErr.set(f665);
		}
		/*CHECK-FOR-ERRORS*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		updateDatabase3010();
		writeReasncd3050();
		relsfl3070();
	}

protected void updateDatabase3010()
	{
		if (isEQ(sv.currfrom,varcom.vrcmMaxDate)
		&& isEQ(sv.currto,varcom.vrcmMaxDate)) {
			chdrmnaIO.setRnwlspfrom(ZERO);
			chdrmnaIO.setRnwlspto(ZERO);
			chdrmnaIO.setRnwlsupr("N");
		}
		else {
			chdrmnaIO.setRnwlspfrom(sv.currfrom);
			chdrmnaIO.setRnwlspto(sv.currto);
			wsaaWriteLetter = "Y";
			chdrmnaIO.setRnwlsupr("Y");
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		chdrmnaIO.setFunction(varcom.keeps);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaWriteLetter,"Y")) {
			writeLetter3200();
		}
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		wsaaBatcKey.set(wsspcomn.batchkey);
		ptrnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmnaIO.getChdrnum());
		ptrnIO.setTranno(chdrmnaIO.getTranno());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		batcuprec1.batcupRec.set(SPACES);
		batcuprec1.batchkey.set(wsspcomn.batchkey);
		batcuprec1.trancnt.set(1);
		batcuprec1.etreqcnt.set(ZERO);
		batcuprec1.sub.set(ZERO);
		batcuprec1.bcnt.set(ZERO);
		batcuprec1.bval.set(ZERO);
		batcuprec1.ascnt.set(ZERO);
		batcuprec1.statuz.set(ZERO);
		batcuprec1.function.set(SPACES);
		callProgram(Batcup.class, batcuprec1.batcupRec);
		if (isNE(batcuprec1.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec1.batcupRec);
			fatalError600();
		}
	}

protected void writeReasncd3050()
	{
		if (isNE(wsaaOrigReasoncd,SPACES)
		|| isNE(wsaaOrigResndesc,SPACES)) {
			deleteReason3100();
		}
		if (isNE(sv.resndesc,SPACES)
		|| isNE(sv.reasoncd,SPACES)) {
			resnIO.setDataKey(SPACES);
			resnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
			resnIO.setChdrnum(chdrmnaIO.getChdrnum());
			resnIO.setTranno(chdrmnaIO.getTranno());
			resnIO.setReasoncd(sv.reasoncd);
			resnIO.setResndesc(sv.resndesc);
			resnIO.setTrancde(wsaaBatcKey.batcBatctrcde);
			resnIO.setTransactionDate(varcom.vrcmDate);
			resnIO.setTransactionTime(varcom.vrcmTime);
			resnIO.setUser(varcom.vrcmUser);
			resnIO.setFormat(resnrec);
			resnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, resnIO);
			if (isNE(resnIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(resnIO.getParams());
				fatalError600();
			}
		}
	}

protected void relsfl3070()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void deleteReason3100()
	{
		begn3110();
	}

protected void begn3110()
	{
		resnenqIO.setParams(SPACES);
		resnenqIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		resnenqIO.setChdrnum(chdrmnaIO.getChdrnum());
		resnenqIO.setTrancde(wsaaBatcKey.batcBatctrcde);
		resnenqIO.setTranno(99999);
		resnenqIO.setFormat(resnenqrec);
		resnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		resnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		resnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANCDE");
		SmartFileCode.execute(appVars, resnenqIO);
		if (isNE(resnenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(resnenqIO.getChdrcoy(),chdrmnaIO.getChdrcoy())
		|| isNE(resnenqIO.getChdrnum(),chdrmnaIO.getChdrnum())
		|| isNE(resnenqIO.getTrancde(),wsaaBatcKey.batcBatctrcde)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		resnIO.setParams(SPACES);
		resnIO.setChdrcoy(resnenqIO.getChdrcoy());
		resnIO.setChdrnum(resnenqIO.getChdrnum());
		resnIO.setTranno(resnenqIO.getTranno());
		resnIO.setFormat(resnrec);
		resnIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, resnIO);
		if (isNE(resnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(resnIO.getParams());
			syserrrec.statuz.set(resnIO.getStatuz());
			fatalError600();
		}
		resnIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, resnIO);
		if (isNE(resnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(resnIO.getParams());
			syserrrec.statuz.set(resnIO.getStatuz());
			fatalError600();
		}
	}

protected void writeLetter3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3210();
				}
				case readTr3843220: {
					readTr3843220();
				}
				case exit3290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3210()
	{
		wsaaItemBatctrcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
	}

protected void readTr3843220()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)
			&& isNE(wsaaItemCnttype,"***")) {
				wsaaItemCnttype.set("***");
				goTo(GotoLabel.readTr3843220);
			}
			else {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType,SPACES)) {
			goTo(GotoLabel.exit3290);
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.branch.set(wsspcomn.branch);
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(wsspcomn.language);
		wsaaOkeyTranno.set(wsaaLastTranno);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatcKey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void formatResnDetails5000()
	{
		start5010();
	}

protected void start5010()
	{
		resnenqIO.setParams(SPACES);
		resnenqIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		resnenqIO.setChdrnum(chdrmnaIO.getChdrnum());
		resnenqIO.setTrancde(wsaaBatcKey.batcBatctrcde);
		resnenqIO.setTranno(999);
		resnenqIO.setFormat(resnenqrec);
		resnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		resnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		resnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANCDE");
		SmartFileCode.execute(appVars, resnenqIO);
		if (isNE(resnenqIO.getStatuz(),varcom.oK)
		&& isNE(resnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmnaIO.getChdrcoy(),resnenqIO.getChdrcoy())
		|| isNE(chdrmnaIO.getChdrnum(),resnenqIO.getChdrnum())
		|| isNE(wsaaBatcKey.batcBatctrcde,resnenqIO.getTrancde())
		|| isEQ(resnenqIO.getStatuz(),varcom.endp)) {
			sv.reasoncd.set(SPACES);
			wsaaOrigReasoncd.set(SPACES);
			sv.resndesc.set(SPACES);
			wsaaOrigResndesc.set(SPACES);
		}
		else {
			sv.reasoncd.set(resnenqIO.getReasoncd());
			wsaaOrigReasoncd.set(resnenqIO.getReasoncd());
			wsaaOrigResndesc.set(resnenqIO.getResndesc());
			sv.resndesc.set(resnenqIO.getResndesc());
		}
	}
}
