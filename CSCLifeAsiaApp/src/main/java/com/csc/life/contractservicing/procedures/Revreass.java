/*
 * File: Revreass.java
 * Date: 30 August 2009 2:11:06
 * Author: Quipoz Limited
 * 
 * Class transformed from REVREASS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.RactrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE REASSURANCE PREMIUM COLLECTIONS.
*        ----------------------------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse the results of the Reassurance
*   premium collection process. It will reverse all Non-cash
*   Accounting records (ACMV) and Reassurance records (RACT)
*   which match on the specified key... the key being
*   Component, Transaction number and Transaction Code.
*
*
* Processing.
* -----------
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*     Set the accounting month/year with the batch
*     accounting month/year from linkage.
*     Set the tranno with the NEW tranno from linkage.
*     Set the batch key with new batch key from linkage.
*     Multiply the original currency amount by -1.
*     Multiply the accounting currency amount by -1.
*     Set the transaction reference with the NEW
*     transaction code from linkage.
*     Set the transaction description with the long
*     description of the new trans code from T1688.
*     Set the effective date with todays date.
*     Write the new record to the database by calling the
*     subroutine 'LIFACMV'.
*
* Reverse Reassurance records ( RACTs ):
*
*  Read the Reassurance file (RACTPF) using the logical
*  view RACTREV with a key of company, contract number, life,
*  coverage, rider, reassurance number and reassurance type.
*
*  For each record found for the contract being processed...
*
*      IF Single Premium type
*          IF RACTREV-VALIDFLAG = '1'
*              IF reass start date ( CRRCD ) = PTRN effdate
*                AND RACTREV-TRANNO = PTRN tranno
*
*                  Set RACTREV-BTDATE = RACTREV-CRRCD
*                  Set RACTREV-TRANNO to new tranno being
*                                                   reversed
*                  REWRiTe RACTREV record
*              ENDIF
*          ENDIF
*
*          IF RACTREV-VALIDFLAG = '2'
*              IF RACTREV-TRANNO = PTRN tranno being reversed
*
*                  Set validflag = '4'
*                  Set RACTREV-TRANNO to new tranno
*                  REWRiTe RACTREV record
*              ENDIF
*          ENDIF
*      END-IF
*
*      IF Instalment premium type
*          IF RACTREV-VALIDFLAG = '1'
*              Subtract 1 reassurance billing frequency from
*                            the current reass. billed-to
*                            date RACTREV-BTDATE
*
*              IF ( RACTREV-BTDATE - 1 ) = PTRN effdate
*
*                  Set RACTREV-BTDATE = RACTREV-BTDATE - 1
*                  Set RACTREV-TRANNO to new tranno
*                  REWRiTe RACTREV record
*              ENDIF
*          ENDIF
*
*          IF RACTREV-VALIDFLAG = '2'
*              IF RACTREV-TRANNO = PTRN tranno being reversed
*
*                  Set validflag = '4'
*                  Set RACTREV-BTDATE = RACTREV-BTDATE - 1 freq
*                  Set RACTREV-TRANNO to new tranno
*                  REWRiTe RACTREV record
*              ENDIF
*          ENDIF
*      END-IF
*
* Reversal of Unit-Linked/Traditional parts of the Contract:
*
*  Loop through all the Coverages/Riders associated with the
*   Contract we are processing and call any Generic processing
*   subroutines that are specified in T5671.
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing
*
*****************************************************************
* </pre>
*/
public class Revreass extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVREASS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
		/* FORMATS */
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String covrenqrec = "COVRENQREC";
	private static final String itemrec = "ITEMREC   ";
	private static final String ractrevrec = "RACTREVREC";
	private static final String arcmrec = "ARCMREC";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RactrevTableDAM ractrevIO = new RactrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Reverserec reverserec = new Reverserec();

	public Revreass() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processAcmvs3000();
		processAcmvOptical3010();
		processRacts4000();
		genericProcessing5000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError9000();
		}
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void processAcmvs3000()
	{
		start3000();
	}

protected void start3000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs3100();
		}
		
	}

protected void processAcmvOptical3010()
	{
		start3010();
	}

protected void start3010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError9500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs3100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError9500();
			}
		}
	}

protected void reverseAcmvs3100()
	{
		start3100();
		readNextAcmv3180();
	}

protected void start3100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError9000();
		}
	}

protected void readNextAcmv3180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processRacts4000()
	{
		start4000();
	}

protected void start4000()
	{
		ractrevIO.setParams(SPACES);
		ractrevIO.setChdrcoy(reverserec.company);
		ractrevIO.setChdrnum(reverserec.chdrnum);
		ractrevIO.setLife(SPACES);
		ractrevIO.setCoverage(SPACES);
		ractrevIO.setRider(SPACES);
		ractrevIO.setRasnum(SPACES);
		ractrevIO.setRatype(SPACES);
		ractrevIO.setTranno(ZERO);
		ractrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ractrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ractrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		ractrevIO.setFormat(ractrevrec);
		SmartFileCode.execute(appVars, ractrevIO);
		if (isNE(ractrevIO.getStatuz(), varcom.oK)
		&& isNE(ractrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ractrevIO.getParams());
			syserrrec.statuz.set(ractrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, ractrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, ractrevIO.getChdrnum())
		|| isEQ(ractrevIO.getStatuz(), varcom.endp)) {
			ractrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(ractrevIO.getStatuz(), varcom.endp))) {
			reverseRacts4100();
		}
		
	}

protected void reverseRacts4100()
	{
		start4100();
	}

protected void start4100()
	{
		/*    check whether RACTREV record we are processing is a*/
		/*    Single premium or Instalment premium case.*/
		if (isGT(ractrevIO.getSingp(), ZERO)
		&& isEQ(ractrevIO.getInstprem(), ZERO)) {
			processSingpRact4200();
		}
		else {
			processInstpremRact4500();
		}
		ractrevIO.setFunction(varcom.nextr);
		ractrevIO.setFormat(ractrevrec);
		SmartFileCode.execute(appVars, ractrevIO);
		if (isNE(ractrevIO.getStatuz(), varcom.oK)
		&& isNE(ractrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ractrevIO.getParams());
			syserrrec.statuz.set(ractrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, ractrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, ractrevIO.getChdrnum())
		|| isEQ(ractrevIO.getStatuz(), varcom.endp)) {
			ractrevIO.setStatuz(varcom.endp);
		}
	}

protected void processSingpRact4200()
	{
		/*START*/
		if (isEQ(ractrevIO.getValidflag(), "1")) {
			singpVf14300();
		}
		if (isEQ(ractrevIO.getValidflag(), "2")) {
			singpVf24400();
		}
		/*EXIT*/
	}

protected void singpVf14300()
	{
		start4300();
	}

protected void start4300()
	{
		if (isEQ(ractrevIO.getCrrcd(), reverserec.ptrneff)
		&& isEQ(ractrevIO.getTranno(), reverserec.tranno)) {
			ractrevIO.setFunction(varcom.readh);
			ractrevIO.setFormat(ractrevrec);
			SmartFileCode.execute(appVars, ractrevIO);
			if (isNE(ractrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(ractrevIO.getParams());
				syserrrec.statuz.set(ractrevIO.getStatuz());
				databaseError9500();
			}
			ractrevIO.setBtdate(ractrevIO.getCrrcd());
			ractrevIO.setTranno(reverserec.newTranno);
			ractrevIO.setFunction(varcom.rewrt);
			ractrevIO.setFormat(ractrevrec);
			SmartFileCode.execute(appVars, ractrevIO);
			if (isNE(ractrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(ractrevIO.getParams());
				syserrrec.statuz.set(ractrevIO.getStatuz());
				databaseError9500();
			}
		}
	}

protected void singpVf24400()
	{
		start4400();
	}

protected void start4400()
	{
		if (isEQ(ractrevIO.getTranno(), reverserec.tranno)) {
			ractrevIO.setFunction(varcom.readh);
			ractrevIO.setFormat(ractrevrec);
			SmartFileCode.execute(appVars, ractrevIO);
			if (isNE(ractrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(ractrevIO.getParams());
				syserrrec.statuz.set(ractrevIO.getStatuz());
				databaseError9500();
			}
			ractrevIO.setValidflag("4");
			ractrevIO.setTranno(reverserec.newTranno);
			ractrevIO.setFunction(varcom.rewrt);
			ractrevIO.setFormat(ractrevrec);
			SmartFileCode.execute(appVars, ractrevIO);
			if (isNE(ractrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(ractrevIO.getParams());
				syserrrec.statuz.set(ractrevIO.getStatuz());
				databaseError9500();
			}
		}
	}

protected void processInstpremRact4500()
	{
		/*START*/
		if (isEQ(ractrevIO.getValidflag(), "1")) {
			instpremVf14600();
		}
		if (isEQ(ractrevIO.getValidflag(), "2")) {
			instpremVf24700();
		}
		/*EXIT*/
	}

protected void instpremVf14600()
	{
		start4600();
	}

protected void start4600()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set(ractrevIO.getRapayfrq());
		datcon2rec.intDate1.set(ractrevIO.getBtdate());
		datcon2rec.intDate2.set(ZERO);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError9000();
		}
		if (isEQ(datcon2rec.intDate2, reverserec.ptrneff)) {
			ractrevIO.setFunction(varcom.readh);
			ractrevIO.setFormat(ractrevrec);
			SmartFileCode.execute(appVars, ractrevIO);
			if (isNE(ractrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(ractrevIO.getParams());
				syserrrec.statuz.set(ractrevIO.getStatuz());
				databaseError9500();
			}
			ractrevIO.setBtdate(datcon2rec.intDate2);
			ractrevIO.setTranno(reverserec.newTranno);
			ractrevIO.setFunction(varcom.rewrt);
			ractrevIO.setFormat(ractrevrec);
			SmartFileCode.execute(appVars, ractrevIO);
			if (isNE(ractrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(ractrevIO.getParams());
				syserrrec.statuz.set(ractrevIO.getStatuz());
				databaseError9500();
			}
		}
	}

protected void instpremVf24700()
	{
		start4700();
	}

protected void start4700()
	{
		if (isEQ(ractrevIO.getTranno(), reverserec.tranno)) {
			ractrevIO.setFunction(varcom.readh);
			ractrevIO.setFormat(ractrevrec);
			SmartFileCode.execute(appVars, ractrevIO);
			if (isNE(ractrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(ractrevIO.getParams());
				syserrrec.statuz.set(ractrevIO.getStatuz());
				databaseError9500();
			}
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.freqFactor.set(-1);
			datcon2rec.frequency.set(ractrevIO.getRapayfrq());
			datcon2rec.intDate1.set(ractrevIO.getBtdate());
			datcon2rec.intDate2.set(ZERO);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				systemError9000();
			}
			ractrevIO.setBtdate(datcon2rec.intDate2);
			ractrevIO.setValidflag("4");
			ractrevIO.setTranno(reverserec.newTranno);
			ractrevIO.setFunction(varcom.rewrt);
			ractrevIO.setFormat(ractrevrec);
			SmartFileCode.execute(appVars, ractrevIO);
			if (isNE(ractrevIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(ractrevIO.getParams());
				syserrrec.statuz.set(ractrevIO.getStatuz());
				databaseError9500();
			}
		}
	}

protected void genericProcessing5000()
	{
		/*START*/
		/* We will read through all the Coverage & riders attached to the*/
		/*  contract that we are processing, and for each one we will*/
		/*  do any required generic processing by calling subroutine(s)*/
		/* specified on table T5671*/
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs5100();
		}
		
		/*EXIT*/
	}

protected void processCovrs5100()
	{
		start5100();
	}

protected void start5100()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* check whether Life/Coverage/Rider have changed - if so, we*/
		/*  need to call any generic Subroutines that are on T5671*/
		/* Note that because GREVUTRN proceses at Rider level and not*/
		/*  Plan suffix level, we cannot call it properly with a full*/
		/*  component key at the moment - until GREVUTRN is amended to*/
		/*  operate at Plan suffix level we must just call it whenever*/
		/*  the Coverage/Rider changes.*/
		if (isNE(covrenqIO.getLife(), wsaaLife)
		|| isNE(covrenqIO.getCoverage(), wsaaCoverage)
		|| isNE(covrenqIO.getRider(), wsaaRider)) {
			genericSubr5200();
			wsaaLife.set(covrenqIO.getLife());
			wsaaCoverage.set(covrenqIO.getCoverage());
			wsaaRider.set(covrenqIO.getRider());
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void genericSubr5200()
	{
		start5200();
	}

protected void start5200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(ZERO);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs5300();
		}
	}

protected void callTrevsubs5300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError9000();
			}
		}
		/*EXIT*/
	}

protected void systemError9000()
	{
		start9000();
		exit9490();
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		start9500();
		exit9990();
	}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
