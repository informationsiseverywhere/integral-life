package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.dbdialect.DbDialectSupportObjFactory;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class IncrpfDAOImpl extends BaseDAOImpl<Incrpf> implements IncrpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(IncrpfDAOImpl.class);
	private static final com.quipoz.framework.dbdialect.SmartFileCodeSupport smartFileCodeSupport = DbDialectSupportObjFactory
            .getInstance().getSmartFileCodeSupport();
			
	private Wsspcomn wsspcomn;
	public Wsspcomn getWsspcomn() {
		return wsspcomn;
	}
	public void setWsspcomn(Wsspcomn wsspcomn) {
		this.wsspcomn = wsspcomn;
	}
	
	private final String SQLFORGetIncrpfByPolicyNum = "select UNIQUE_NUMBER, CHDRCOY, CHDRNUM, "
			+ "LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, EFFDATE, TRANNO, VALIDFLAG, STATCODE, "
			+ "PSTATCODE, CRRCD, CRTABLE, ORIGINST, LASTINST, NEWINST, ORIGSUM, LASTSUM, NEWSUM,"
			+ " ANNVRY, BASCMETH, PCTINC, REFFLAG, TRDT, TRTM, USER_T, TERMID, BASCPY, CEASEIND,"
			+ " RNWCPY, SRVCPY, ZBORIGINST, ZBLASTINST, ZBNEWINST, ZLORIGINST, ZLLASTINST,"
			+ " ZLNEWINST, USRPRF, JOBNM, DATIME from Incrpf where VALIDFLAG = '1' and CHDRCOY = ? and CHDRNUM = ?";
	@Override
	public List<Incrpf> getIncrpfByPolicyNum(String contractNum, Wsspcomn wsspcomn) {
		List<Incrpf> result = new ArrayList<>();
		try (PreparedStatement preStat = getPrepareStatement(SQLFORGetIncrpfByPolicyNum)) {
			preStat.setString(1, wsspcomn.company.toString().trim());
			preStat.setString(2, contractNum);
			ResultSet rs = this.executeQuery(preStat);
			while (rs.next()) {
				Incrpf incrpf = new Incrpf();
				
				incrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				incrpf.setChdrcoy(rs.getString("CHDRCOY"));
				incrpf.setChdrnum(rs.getString("CHDRNUM"));
				incrpf.setLife(rs.getString("LIFE"));
				incrpf.setJlife(rs.getString("JLIFE"));
				incrpf.setCoverage(rs.getString("COVERAGE"));
				incrpf.setRider(rs.getString("RIDER"));
				incrpf.setPlnsfx(rs.getInt("PLNSFX"));
				incrpf.setEffdate(rs.getInt("EFFDATE"));
				incrpf.setTranno(rs.getInt("TRANNO"));
				incrpf.setValidflag(rs.getString("VALIDFLAG"));
				incrpf.setStatcode(rs.getString("STATCODE"));
				incrpf.setPstatcode(rs.getString("PSTATCODE"));
				incrpf.setCrrcd(rs.getInt("CRRCD"));
				incrpf.setCrtable(rs.getString("CRTABLE"));
				incrpf.setOrigInst(rs.getBigDecimal("ORIGINST"));
				incrpf.setLastInst(rs.getBigDecimal("LASTINST"));
				incrpf.setNewinst(rs.getBigDecimal("NEWINST"));
				incrpf.setOrigSum(rs.getBigDecimal("ORIGSUM"));
				incrpf.setLastSum(rs.getBigDecimal("LASTSUM"));
				incrpf.setNewsum(rs.getBigDecimal("NEWSUM"));
				incrpf.setAnniversaryMethod(rs.getString("ANNVRY"));
				incrpf.setBasicCommMeth(rs.getString("BASCMETH"));
				incrpf.setPctinc(rs.getInt("PCTINC"));
				incrpf.setRefusalFlag(rs.getString("REFFLAG"));
				incrpf.setTransactionDate(rs.getInt("TRDT"));
				incrpf.setTransactionTime(rs.getInt("TRTM"));
				incrpf.setUser(rs.getInt("USER_T"));
				incrpf.setTermid(rs.getString("TERMID"));
				incrpf.setBascpy(rs.getString("BASCPY"));
				incrpf.setCeaseInd(rs.getString("CEASEIND"));
				incrpf.setRnwcpy(rs.getString("RNWCPY"));
				incrpf.setSrvcpy(rs.getString("SRVCPY"));
				incrpf.setZboriginst(rs.getBigDecimal("ZBORIGINST"));
				incrpf.setZblastinst(rs.getBigDecimal("ZBLASTINST"));
				incrpf.setZbnewinst(rs.getBigDecimal("ZBNEWINST"));
				incrpf.setZloriginst(rs.getBigDecimal("ZLORIGINST"));
				incrpf.setZllastinst(rs.getBigDecimal("ZLLASTINST"));
				incrpf.setZlnewinst(rs.getBigDecimal("ZLNEWINST"));
				incrpf.setUserProfile(rs.getString("USRPRF"));
				incrpf.setJobName(rs.getString("JOBNM"));
				incrpf.setDatime(rs.getTimestamp("DATIME"));
				result.add(incrpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchBfrqpfRecord()", e);
			throw new SQLRuntimeException(e);
		} 
		return result;
	}
	
	private final String SQLFORDeleteRecordByUniqueNumber = "delete from Incrpf where UNIQUE_NUMBER in";
	@Override
	public int deleteRecordByUniqueNumber(List<Long> ids) {
		if (ids == null || ids.size() == 0) {
			return 0;
		}
		
		PreparedStatement pre = null;
		try (Connection con = getConnection()) {
			String sql = SQLFORDeleteRecordByUniqueNumber + " (";
			for (int i = 0; i < ids.size(); i++) {
				sql += "?";
				if ( i < ids.size() - 1) {
					sql += ",";
				}
			}
			sql += ")";
			pre = con.prepareStatement(sql);
			for (int i = 0; i < ids.size(); i++) {
				pre.setLong(i + 1, ids.get(i));
			}
			smartFileCodeSupport.updateSessionContextInfo(con, AppVars.getInstance().getLoggedOnUser());
			return pre.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("searchBfrqpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(pre, null);
		}
	}

}
