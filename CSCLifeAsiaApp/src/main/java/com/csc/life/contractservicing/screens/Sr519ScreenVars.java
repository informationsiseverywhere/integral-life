package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR519
 * @version 1.0 generated on 30/08/09 07:16
 * @author Quipoz
 */
public class Sr519ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(363);
	public FixedLengthStringData dataFields = new FixedLengthStringData(107).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbage = DD.anbage.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData dob = DD.dob.copyToZonedDecimal().isAPartOf(dataFields,11);
	public FixedLengthStringData dummy = DD.dummy.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData lifesel = DD.lifesel.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData occup = DD.occup.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData pursuits = new FixedLengthStringData(12).isAPartOf(dataFields, 85);
	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(2, 6, pursuits, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(pursuits, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(filler,0);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(filler,6);
	public FixedLengthStringData relation = DD.relation.copy().isAPartOf(dataFields,97);
	public FixedLengthStringData rollit = DD.rollit.copy().isAPartOf(dataFields,99);
	public FixedLengthStringData selection = DD.selection.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData smoking = DD.smoking.copy().isAPartOf(dataFields,105);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 107);
	public FixedLengthStringData anbageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData dobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dummyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifeselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData occupErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData pursuitsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] pursuitErr = FLSArrayPartOfStructure(2, 4, pursuitsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(pursuitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData pursuit02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData rollitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData selectionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData sexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData smokingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 171);
	public FixedLengthStringData[] anbageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] dobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dummyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifeselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] occupOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData pursuitsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] pursuitOut = FLSArrayPartOfStructure(2, 12, pursuitsOut, 0);
	public FixedLengthStringData[][] pursuitO = FLSDArrayPartOfArrayStructure(12, 1, pursuitOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(pursuitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pursuit01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] pursuit02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] rollitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] selectionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] sexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dobDisp = new FixedLengthStringData(10);

	public LongData Sr519screenWritten = new LongData(0);
	public LongData Sr519protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr519ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(jlifeOut,new String[] {null, null, null, "51",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sexOut,new String[] {"02","13","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dobOut,new String[] {"03","13","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dummyOut,new String[] {null, null, null, "54",null, null, null, null, null, null, null, null});
		fieldIndMap.put(anbageOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectionOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(smokingOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occupOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pursuit01Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pursuit02Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(relationOut,new String[] {"12","52","-12","52",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rollitOut,new String[] {null, null, null, "53",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, life, jlife, lifesel, lifename, sex, dob, dummy, anbage, selection, smoking, occup, pursuit01, pursuit02, relation, rollit};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, jlifeOut, lifeselOut, lifenameOut, sexOut, dobOut, dummyOut, anbageOut, selectionOut, smokingOut, occupOut, pursuit01Out, pursuit02Out, relationOut, rollitOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, jlifeErr, lifeselErr, lifenameErr, sexErr, dobErr, dummyErr, anbageErr, selectionErr, smokingErr, occupErr, pursuit01Err, pursuit02Err, relationErr, rollitErr};
		screenDateFields = new BaseData[] {dob};
		screenDateErrFields = new BaseData[] {dobErr};
		screenDateDispFields = new BaseData[] {dobDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr519screen.class;
		protectRecord = Sr519protect.class;
	}

}
