package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50mscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 4;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 14, 3, 40}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50mScreenVars sv = (Sr50mScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr50mscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr50mscreensfl, 
			sv.Sr50mscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr50mScreenVars sv = (Sr50mScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr50mscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr50mScreenVars sv = (Sr50mScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr50mscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr50mscreensflWritten.gt(0))
		{
			sv.sr50mscreensfl.setCurrentIndex(0);
			sv.Sr50mscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr50mScreenVars sv = (Sr50mScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr50mscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr50mScreenVars screenVars = (Sr50mScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.datakey.setFieldName("datakey");
				screenVars.pstatdate.setFieldName("pstatdate");
				screenVars.crtable.setFieldName("crtable");
				screenVars.entity.setFieldName("entity");
				screenVars.sumins.setFieldName("sumins");
				screenVars.waiverprem.setFieldName("waiverprem");
				screenVars.activeInd.setFieldName("activeInd");
				screenVars.zrwvflg01.setFieldName("zrwvflg01");
				screenVars.zrwvflg02.setFieldName("zrwvflg02");
				screenVars.zrwvflg03.setFieldName("zrwvflg03");
				screenVars.zrwvflg04.setFieldName("zrwvflg04");
				screenVars.workAreaData.setFieldName("workAreaData");
				screenVars.basprm.setFieldName("basprm");
				screenVars.zloprm.setFieldName("zloprm");
				screenVars.zgrsamt.setFieldName("zgrsamt");
				screenVars.zstpduty.setFieldName("zstpduty");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.datakey.set(dm.getField("datakey"));
			screenVars.pstatdate.set(dm.getField("pstatdate"));
			screenVars.crtable.set(dm.getField("crtable"));
			screenVars.entity.set(dm.getField("entity"));
			screenVars.sumins.set(dm.getField("sumins"));
			screenVars.waiverprem.set(dm.getField("waiverprem"));
			screenVars.activeInd.set(dm.getField("activeInd"));
			screenVars.zrwvflg01.set(dm.getField("zrwvflg01"));
			screenVars.zrwvflg02.set(dm.getField("zrwvflg02"));
			screenVars.zrwvflg03.set(dm.getField("zrwvflg03"));
			screenVars.zrwvflg04.set(dm.getField("zrwvflg04"));
			screenVars.workAreaData.set(dm.getField("workAreaData"));
			screenVars.basprm.set(dm.getField("basprm"));
			screenVars.zloprm.set(dm.getField("zloprm"));
			screenVars.zgrsamt.set(dm.getField("zgrsamt"));
			screenVars.zstpduty.set(dm.getField("zstpduty"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr50mScreenVars screenVars = (Sr50mScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.datakey.setFieldName("datakey");
				screenVars.pstatdate.setFieldName("pstatdate");
				screenVars.crtable.setFieldName("crtable");
				screenVars.entity.setFieldName("entity");
				screenVars.sumins.setFieldName("sumins");
				screenVars.waiverprem.setFieldName("waiverprem");
				screenVars.activeInd.setFieldName("activeInd");
				screenVars.zrwvflg01.setFieldName("zrwvflg01");
				screenVars.zrwvflg02.setFieldName("zrwvflg02");
				screenVars.zrwvflg03.setFieldName("zrwvflg03");
				screenVars.zrwvflg04.setFieldName("zrwvflg04");
				screenVars.workAreaData.setFieldName("workAreaData");
				screenVars.basprm.setFieldName("basprm");
				screenVars.zloprm.setFieldName("zloprm");
				screenVars.zgrsamt.setFieldName("zgrsamt");
				screenVars.zstpduty.setFieldName("zstpduty");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("datakey").set(screenVars.datakey);
			dm.getField("pstatdate").set(screenVars.pstatdate);
			dm.getField("crtable").set(screenVars.crtable);
			dm.getField("entity").set(screenVars.entity);
			dm.getField("sumins").set(screenVars.sumins);
			dm.getField("waiverprem").set(screenVars.waiverprem);
			dm.getField("activeInd").set(screenVars.activeInd);
			dm.getField("zrwvflg01").set(screenVars.zrwvflg01);
			dm.getField("zrwvflg02").set(screenVars.zrwvflg02);
			dm.getField("zrwvflg03").set(screenVars.zrwvflg03);
			dm.getField("zrwvflg04").set(screenVars.zrwvflg04);
			dm.getField("workAreaData").set(screenVars.workAreaData);
			dm.getField("basprm").set(screenVars.basprm);
			dm.getField("zloprm").set(screenVars.zloprm);
			dm.getField("zgrsamt").set(screenVars.zgrsamt);
			dm.getField("zstpduty").set(screenVars.zstpduty);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr50mscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr50mScreenVars screenVars = (Sr50mScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.datakey.clearFormatting();
		screenVars.pstatdate.clearFormatting();
		screenVars.crtable.clearFormatting();
		screenVars.entity.clearFormatting();
		screenVars.sumins.clearFormatting();
		screenVars.waiverprem.clearFormatting();
		screenVars.activeInd.clearFormatting();
		screenVars.zrwvflg01.clearFormatting();
		screenVars.zrwvflg02.clearFormatting();
		screenVars.zrwvflg03.clearFormatting();
		screenVars.zrwvflg04.clearFormatting();
		screenVars.workAreaData.clearFormatting();
		screenVars.basprm.clearFormatting();
		screenVars.zloprm.clearFormatting();
		screenVars.zgrsamt.clearFormatting();
		screenVars.zstpduty.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr50mScreenVars screenVars = (Sr50mScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.datakey.setClassString("");
		screenVars.pstatdate.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.entity.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.waiverprem.setClassString("");
		screenVars.activeInd.setClassString("");
		screenVars.zrwvflg01.setClassString("");
		screenVars.zrwvflg02.setClassString("");
		screenVars.zrwvflg03.setClassString("");
		screenVars.zrwvflg04.setClassString("");
		screenVars.workAreaData.setClassString("");
		screenVars.basprm.setClassString("");
		screenVars.zloprm.setClassString("");
		screenVars.zgrsamt.setClassString("");
		screenVars.zstpduty.setClassString("");
	}

/**
 * Clear all the variables in Sr50mscreensfl
 */
	public static void clear(VarModel pv) {
		Sr50mScreenVars screenVars = (Sr50mScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.datakey.clear();
		screenVars.pstatdate.clear();
		screenVars.crtable.clear();
		screenVars.entity.clear();
		screenVars.sumins.clear();
		screenVars.waiverprem.clear();
		screenVars.activeInd.clear();
		screenVars.zrwvflg01.clear();
		screenVars.zrwvflg02.clear();
		screenVars.zrwvflg03.clear();
		screenVars.zrwvflg04.clear();
		screenVars.workAreaData.clear();
		screenVars.basprm.clear();
		screenVars.zloprm.clear();
		screenVars.zgrsamt.clear();
		screenVars.zstpduty.clear();
	}
}
