/*
 * File: Acomcalc.java
 * Date: 29 August 2009 20:11:16
 * Author: Quipoz Limited
 * 
 * Class transformed from ACOMCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgcmdmnTableDAM;
import com.csc.life.agents.dataaccess.AgcmdmyTableDAM;
import com.csc.life.agents.dataaccess.AgcmsqnTableDAM;
import com.csc.life.agents.dataaccess.AgcmsqyTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.T5647rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;

import com.csc.life.contractservicing.dataaccess.AgcmbcrTableDAM;
import com.csc.life.contractservicing.dataaccess.AglfmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrrevTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Acomcalrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.flexiblepremium.procedures.Fpcommod;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        COMPONENT CHANGE COMMISSION PROCESSING
*        --------------------------------------
*
* Overview.
* ---------
*
* This subroutine performs all commission processing that used
*  to be done in the Component Change AT module P5132AT. It will
*  process all AGCM Agent Commission records for the contract
*  components being altered and post ACMV Accounting Movement
*  records where necessary.
*
* Processing.
* -----------
*
*  Commission details are represented by  AGCM  records.  These
*  hold  the  details of the premium and its Paid-to-Date. When
*  a premium varies over  the  life  of  a  contract  new  AGCM
*  records  will  be  created  that represent the new increased
*  portion. These will have their own  Risk  Commencement  Date
*  so  that  the system can calculate the commission due on the
*  separate portions. When a decrease  has  occurred  the  AGCM
*  records  will  be  'deactivated' and if necessary split into
*  two; one part  holding  the  still  active  portion  of  the
*  premium  the  other showing the now 'inactive' portion. Over
*  the life of a  contract  then  there  may  be  several  AGCM
*  records  representing the current amount of premium and also
*  several, now 'inactive',  representing  the  oldest  highest
*  value  of the premium. These AGCM records will have sequence
*  numbers; number one will be the original and  oldest  record
*  representing  the  lowest value the premium ever had and the
*  highest sequence number will be a  portion  of  the  premium
*  that  represents  the  highest  value  that the premium ever
*  reached.
*
*  Where a decrease has  occurred  the  AGCM  records  will  be
*  de-activated from the old highest first.
*
*  Where  an  increase  has  occurred  the  old  inactive  AGCM
*  records will be  activated  from  the  old  lowest  sequence
*  number  first.  Where  a  record  is  not  being  completely
*  activated or de-activated it is split into two.
*
*  Increases:
*  ----------
*
*  Where an increase has occurred the  increased  portion  will
*  be  represented by a new AGCM record that has a Paid-to-Date
*  equal to the effective date of the component change.  If  an
*  increase  is  being  made where a previous decrease had been
*  in  effect  the  old,  'inactive',  AGCM  records  will   be
*  activated  first and then, if they are insufficient to cover
*  the new increase, a new AGCM  will  be  written.  Where  the
*  increase  is  less  than the old decrease the old 'inactive'
*  AGCM record may have to be split into two; the  first,  with
*  the  lower  sequence number representing the active portion,
*  and  the   second,   with   the   higher   sequence   number
*  representing  the  still  inactive  portion  of the previous
*  premium level.
*
*  When an AGCM  record  is  re-activated  after  a  period  of
*  inactivity  the  'dead'  period  between  when  it  was last
*  de-activated and the date it is being re-activated  must  be
*  'removed'  in  order  that the commission processing will be
*  able to work with a continuous period during which the  AGCM
*  was  active.  This  will  mean  giving  the  AGCM a new, and
*  notional  Risk  Commencement  Date  by  advancing  the   old
*  Paid-to-date  to the current Paid-to-Date, which will be the
*  effective  date   of   the   transaction.   The   old   Risk
*  Commencement  Date  must  be  advanced  by  the same amount,
*  thereby giving the AGCM record a  continuous  period  during
*  which  the  system  will  deem  it to have been active, thus
*  making it easier to calculate commission.
*
*  Decreases:
*  ----------
*
*  Where a decrease has occurred the decreased portion will  be
*  represented  by de-activating existing AGCM records from the
*  highest 'active' sequence downwards. Where the  decrease  is
*  less  than a whole AGCM record the record will be split into
*  two, the higher sequence  number  holding  the  de-activated
*  portion  and  the  lower  sequence  number  holding  the now
*  active portion.
*
*  The  following  example  should  serve  to  illustrate  this
*  procedure.  Including  the  contract  issue  there  are four
*  changes to the policy: issue,  first  increase,  a  decrease
*  below  the  original  amount  and  an  increase to an amount
*  above the recent highest level.
*
*      Risk Commencement     Premium
*            Date
*      -----------------     -------
*
*
*      1.   1/1/88             100
*
*
*
*      2.   1/1/89             150       (increase)
*      3.   1/7/89              80       (decrease)
*      4.   1/1/90             200       (increase).
*
*  The AGCM records will be as follows:
*
*          Seq.  R.C.D.  Premium  Dormant flag  Paid-to-Date
*          -------------------------------------------------
*
*      1.   1    1/1/88    100       'N'            1/1/88
*
*      2.   2    1/1/89     50       'N'           1/1/89
*           1    1/1/88    100       'N'           1/1/89
*
*      3.   3    1/1/89     50       'Y'           1/7/89
*           2    1/1/88     20       'Y'           1/7/89
*           1    1/1/88     80       'N'           1/7/89
*
*      4.   4    1/1/90     50       'N'           1/1/90
*           3    1/7/89     50       'N'           1/1/90
*           2    1/7/88     20       'N'           1/1/90
*           1    1/1/88     80       'N'           1/1/90
*
*  At position #2 a new AGCM was created  to  account  for  the
*  increased  portion  of  50  pounds  with a Risk Commencement
*  Date of 1/1/89, one year after the original R.C.D.
*
*  At position #3 that AGCM is now de-activated by setting  its
*  DORMANT-FLAG to 'Y'. The  original  AGCM  is  split into two
*  records, the higher  sequence  number,  (#2),  being  of  20
*  pounds  and  inactive,  and  the lower sequence number, (#1)
*  being for the remaining 80 pounds which is still active.
*
*  At  position  #4  the  two   inactive   AGCM   records   are
*  re-activated  and  their Risk Commencement Dates advanced by
*  the same amount as  their  Paid-To-Dates,  and  as  the  new
*  premium  is  50  pounds  greater  than the old highest total
*  premium a new AGCM record is created,  (#4)  which  reflects
*  the remainder of the increase of 50 pounds.
*
*  Note:  When  a  commission split was for more than one agent
*  the system will have  created  multiple  AGCM  records  that
*  account  for  the premium or portion of the premium, one for
*  each agent in the split. The premium will  have  been  split
*  across  the  AGCM records in the proportion specified in the
*  commission split. These AGCM records  will  all  be  at  the
*  same  'level',  ie.  they  will  all share the same sequence
*  number.
*
*  In the example above there is only one agent for all of  the
*  different  stages  and  therefore no more that one record at
*  each level with the same sequence number. If there had  been
*  a  commission  split  then  at  any one level there would be
*  more than one commission record. This split will never  vary
*  for  that  portion  of the premium and when AGCM records are
*  deactivated and reactivated, or when they have to  be  split
*  because  the  total  premium is greater than the increase or
*  decrease  then  the  proportions  of  the  split   must   be
*  maintained.  The  following example will serve to illustrate
*  this point:
*
*  The original premium was $50 and there was  only  one  agent
*  so  only  one AGCM record was written with a sequence number
*  of 1. When an increase came into effect a year  later  there
*  was  a  60/40  split  between  agent  #1  and  agent #2. The
*  premium was $100 and so two AGCM records were written,  both
*  with sequence number 2.
*
*      Seq.  R.C.D.  Premium  Dormant flag  Paid-to-Date Agent
*
*
*      -------------------------------------------------------
*
*       2    1/1/89    $60       'N'           1/1/89       1
*       2    1/1/89    $40       'N'           1/1/89       2
*       1    1/1/88    $50       'N'           1/1/89       1
*
*  A  year  later a decrease came into effect which reduced the
*  total premium by $50.  In  accordance  with  the  rules  the
*  program  had to de-activate from the highest sequence number
*  downwards, in this case number 2. At each level the  program
*  has  to  read  all  the  AGCM records with the same sequence
*  number in order to calculate the total premium at any  given
*  level,  in  this case the two records with a sequence number
*  of 2 give a total premium portion of $100. The $50  decrease
*  is  less  than  this total. Therefore these two records will
*  both have to be split whilst retaining  the  original  60/40
*  split.  Therefore  two  new  records  will be created with a
*  sequence number  of  2  representing  the  now  re-activated
*  portion  of  the  premium,  and two new ones will be created
*  with a sequence number of 3 representing the still  inactive
*  portion  of  the  premium. The following represents this new
*  set up.
*
*      Seq.  R.C.D.  Premium  Dormant flag  Paid-to-Date Agent
*      -------------------------------------------------------
*
*
*       3    1/1/89    $30       'Y'           1/1/90       1
*       3    1/1/89    $20       'Y'           1/1/90       2
*       2    1/1/89    $30       'N'           1/1/90       1
*       2    1/1/89    $20       'N'           1/1/90       2
*       1    1/1/88    $50       'N'           1/1/90       1
*
*  So when calculating the amount of the  premium  at  any  one
*  'level'  when  de-activating  or  re-activating AGCM records
*  the program  must  read  all  AGCM  records  with  the  same
*  sequence  number  to find the total at that level so that it
*  may be de-activated or re-activated as required.
*
*  New Commission Agent: When a  previously  de-activated  AGCM
*  is  being  re-activated  then  the  existing  agent  details
*  remain.  However  if  a  new  AGCM  is  being  written  that
*  accounts  for  a  portion  of  the  premium that is over and
*  above the previous highest level the  user  may  have  input
*  details  of  a new commission split. These will be held on a
*  transaction record PCDT, on a table of up to ten agents  and
*  their respective percentage shares of the commission.
*
*  If  a  new  AGCM  is  being  created  and a PCDT transaction
*  record exists for that component then the  commission  agent
*  on  that  AGCM  must  be taken from there. When this happens
*  there may be more than one agent in the split.  If  this  is
*  so  then  multiple  AGCM  records  must be created - one for
*  each agent in the split - and  the  amount  of  the  premium
*  divided  up  amongst  the AGCM records in the proportions of
*  the specified split. These AGCM records will  all  have  the
*  same sequence number, thus tieing them permanently together.
*
*  When  processing  has  completed  for  a  component the PCDT
*  record may be deleted, as it  was  only  used  for  the  new
*  increased portion of the premium.
*
*
*  Clawback Processing.
*  --------------------
*
*
*  Clawback  processing  will  be  necessary  if a decrease has
*  occurred and the agent's earned  income  is  less  than  his
*  paid income at the effective date of the transaction.
*
*  Check  the  AGCM  that  is being deactivated. If the paid is
*  more than the earned on that AGCM then  Commission  Clawback
*  is  necessary.  If  this  is the case then the corresponding
*  AGCM must also have its Paid field set to the value  of  its
*  Earned field.
*
*  If  clawback  is  necessary then write two ACMV records with
*  the details as shown in the Notes  section  at  the  end  of
*  this specification.
*
*
*  AGCM Record:
*  ------------
*
*  The key will contain the component  key  plus  the  relevant
*  agent number. Other relevant fields will be set as follows:
*
*  . AGCM-TRANNO            -  the new TRANNO
*  . AGCM-EFDATE            -  the transaction effective date
*  . AGCM-ANNPREM           -  the increase of the new premium
*                              over the old.
*  . AGCM-BASIC-COMM-METH   -  Code from table T5687, passed in
*                              the ACOMCALREC linkage record
*  . AGCM-INITCOM           -  calculated by the commission
*                              calculation method.
*  . AGCM-BASCPY            -  Basic Commission Payment Method
*                              from T5687, passed in the
*                              ACOMCALREC linkage record
*  . AGCM-COMPAY            -  zero
*  . AGCM-COMERN            -  zero
*  . AGCM-SRVCPY            -  Service Commission Payment
*                              Method from T5687, if blank from
*                              AGNT, if blank there use spaces.
*  . AGCM-SCMDUE            -  zero
*  . AGCM-SCMEARN           -  zero
*  . AGCM-RNWCPY            -  Renewal Commission Payment
*                              Method from T5687, if blank from
*                              AGNT, if blank there use spaces.
*                              this is passed in the ACOMCALREC
*                              linkage record.
*  . AGCM-RNLCDUE           -  zero
*  . AGCM-RNLCEARN          -  zero
*  . AGCM-AGENT-CLASS       -  from AGNT
*  . AGCM-TERMID            -  Term Id. passed in Linkage
*  . AGCM-TRANSACTION-DATE  -  Current System Date
*  . AGCM-TRANSACTION-TIME  -  Current System Time
*  . AGCM-USER              -  User Id. passed in linkage
*  . AGCM-VALIDFLAG         -  '1'
*  . AGCM-CURRFROM          -  Current System Date
*  . AGCM-CURRTO            -  all 9's
*
*  The   commission   calculation  subroutine  will  called  to
*  calculate  AGCM-INITCOM.  The  correct  subroutine  will  be
*  obtained  from T5647 which is read with the Basic Commission
*  Payment Method from T5687 which is passed in the ACOMCALREC
*  linkage record.
*
*  The Commission Calculation subroutine must  be  called  with
*  the  current  age  of  the  life assured at component change
*  effective date, the Effective Date itself, the  Premium  and
*  the Premium Cessation Date.
*
*  ACMV Record:
*  ------------
*
*  Two  ACMV records will be created for each deactivation; one
*  for  Commission   Clawback   for   agents,   and   one   for
*  Re-crediting  Agent  Commission  Advance.  The  details  for
*  these will be held on T5645, accessed by  program  Id.  Line
*  #1  will  give  the  details for the first posting for which
*  the amount on the ACMV will be negative  and  line  #2  will
*  give  the  details  for  the  second  posting  for which the
*  amount on the ACMV will be positive.
*
*  The subroutine LIFACMV  will  be  called  to  add  the  ACMV
*  records.
*
*  The  key  will  be  the  batch transaction key passed in the
*  linkage  section.  The  remaining  fields  will  be  set  as
*  follows:
*
*  . ACMV-RLDGCOY           -  Contract Company
*  . ACMV-SACSCODE          -  from T5645
*  . ACMV-RLDGACCT          -  Agent number
*  . ACMV-ORIGCURR          -  Contract Currency
*  . ACMV-SACSTYP           -  from T5645
*  . ACMV-RDOCNUM           -  Contract Number
*  . ACMV-TRANNO            -  The new TRANNO
*  . ACMV-JRNSEQ            -  blank
*  . ACMV-ORIGAMT           -  amount of clawback in Contract
*                              Currency - negative for the 1st.
*                              ACMV and positive for the second
*                              (Earned minus Paid).
*  . ACMV-CRATE             -  Currency Conversion Rate
*  . ACMV-ACCTAMT           -  ORIGAMT * CRATE
*  . ACMV-GENLCOY           -  FSU Company
*  . ACMV-GLCODE            -  from T5645
*  . ACMV-GLSIGN            -  from T5645
*  . ACMV-POSTYEAR          -  from Batch Transaction details
*  . ACMV-POSTMONTH         -    "    "        "         "
*  . ACMV-EFFDATE           -  Effective Date of change.
*  . ACMV-RCAMT             -
*  . ACMV-TRANSACTION-DATE  -  Current System Date
*  . ACMV-TRANSACTION-TIME  -  Current System Time
*  . ACMV-USER              -  User Id. passed in linkage
*  . ACMV-TERMID            -  Term Id. passed in linkage
*
* Tables used.
* ------------
*
* T5645 - Transaction Accounting Rules
* T5647 - Basic Commission Methods
*
*****************************************************************
* </pre>
*/
public class Acomcalc extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Acomcalc.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected final String wsaaSubr = "ACOMCALC";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaAddPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaLessPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaSplitPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCalcPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCommPaid = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaSq1TotPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaSq2TotPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNewTotpremVf1 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNewTotpremVf2 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaRunPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaSplitFactor = new PackedDecimalData(18, 8);
	private PackedDecimalData wsaaSplitFactor2 = new PackedDecimalData(18, 8);
	private PackedDecimalData wsaaOldCompay = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaOldComern = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaOldInitcom = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaClawbackAmt = new PackedDecimalData(13, 2).init(0);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaIcommtot = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaCompay = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaComern = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-T5645 */
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (15, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaIfOvrdCommEnd = new FixedLengthStringData(1);
	private Validator wsaaEndOvrdComm = new Validator(wsaaIfOvrdCommEnd, "Y");

	private FixedLengthStringData wsaaIfSplitReqd = new FixedLengthStringData(1);
	private Validator wsaaSplitNotReqd = new Validator(wsaaIfSplitReqd, "N");
	private Validator wsaaSplitReqd = new Validator(wsaaIfSplitReqd, "Y");

	private FixedLengthStringData wsaaIncreaseSplit = new FixedLengthStringData(1).init("N");
	private Validator noIncreaseSplitReqd = new Validator(wsaaIncreaseSplit, "N");
	private Validator increaseSplitReqd = new Validator(wsaaIncreaseSplit, "Y");
	private PackedDecimalData wsaaSplitPlusOne = new PackedDecimalData(2, 0);

	private FixedLengthStringData wsaaSqDataKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaSqChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqDataKey, 0);
	private FixedLengthStringData wsaaSqChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaSqDataKey, 1);
	private FixedLengthStringData wsaaSqLife = new FixedLengthStringData(2).isAPartOf(wsaaSqDataKey, 9);
	private FixedLengthStringData wsaaSqCoverage = new FixedLengthStringData(2).isAPartOf(wsaaSqDataKey, 11);
	private FixedLengthStringData wsaaSqRider = new FixedLengthStringData(2).isAPartOf(wsaaSqDataKey, 13);
	private PackedDecimalData wsaaSqPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaSqDataKey, 15);
	private PackedDecimalData wsaaSqSeqno = new PackedDecimalData(2, 0).isAPartOf(wsaaSqDataKey, 18);
	private FixedLengthStringData wsaaSqDormantFlag = new FixedLengthStringData(1).isAPartOf(wsaaSqDataKey, 20);

	private FixedLengthStringData wsaaProcessCompleteFlag = new FixedLengthStringData(1);
	private Validator processComplete = new Validator(wsaaProcessCompleteFlag, "Y");

	private FixedLengthStringData wsaaEndProcessFlag = new FixedLengthStringData(1);
	private Validator processExit = new Validator(wsaaEndProcessFlag, "Y");

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");

	private FixedLengthStringData wsaaInitCom = new FixedLengthStringData(1);
	private Validator initCom = new Validator(wsaaInitCom, "Y");
	private PackedDecimalData wsaaErnDiff = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPayDiff = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaTargFrom = new PackedDecimalData(8, 0);
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5647 = "T5647";
	private static final String t5644 = "T5644";
	private static final String th605 = "TH605";
		/* FORMATS */
	private static final String agcmdmnrec = "AGCMDMNREC";
	private static final String agcmdmyrec = "AGCMDMYREC";
	private static final String agcmsqnrec = "AGCMSQNREC";
	private static final String agcmsqyrec = "AGCMSQYREC";
	private static final String itemrec = "ITEMREC   ";
	private static final String agcmbcrrec = "AGCMBCRREC";
	private AgcmbcrTableDAM agcmbcrIO = new AgcmbcrTableDAM();
	private AgcmdmnTableDAM agcmdmnIO = new AgcmdmnTableDAM();
	private AgcmdmyTableDAM agcmdmyIO = new AgcmdmyTableDAM();
	private AgcmsqnTableDAM agcmsqnIO = new AgcmsqnTableDAM();
	private AgcmsqyTableDAM agcmsqyIO = new AgcmsqyTableDAM();
	private AglfmjaTableDAM aglfmjaIO = new AglfmjaTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrrevTableDAM covrrevIO = new CovrrevTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	protected Varcom varcom = new Varcom();
	private T5644rec t5644rec = new T5644rec();
	private T5645rec t5645rec = new T5645rec();
	private T5647rec t5647rec = new T5647rec();
	private Th605rec th605rec = new Th605rec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	protected Syserrrec syserrrec = new Syserrrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Acomcalrec acomcalrec = new Acomcalrec();
	private boolean gstOnCommFlag = false;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seqLvlTot2120, 
		nextAgcm2150, 
		exit2190, 
		exit2290, 
		next2320, 
		exit2390, 
		seqLvlTot2420, 
		nextAgcm2450, 
		exit2490, 
		exit3090, 
		nextAgcm3120, 
		exit3190, 
		exit3255, 
		writeVldfg23420, 
		pass3430, 
		validflagTo23560, 
		pass3570, 
		writr3570, 
		exit3590, 
		exit3690, 
		exit5090, 
		nextAgcm5120, 
		exit5190, 
		writePara5470, 
		rewriteVldfg15480, 
		writeVldfg25520, 
		pass6120, 
		exit6190, 
		exit6290
	}

	public Acomcalc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		acomcalrec.acomcalRec = convertAndSetParam(acomcalrec.acomcalRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		gstOnCommFlag = FeaConfg.isFeatureExist(acomcalrec.company.toString(), "CTISS007", appVars, "IT"); //PINNACLE-1380
		syserrrec.subrname.set(wsaaSubr);
		acomcalrec.statuz.set(varcom.oK);
		if (isNE(acomcalrec.fpcoTargPrem, 0)) {
			wsaaFlexiblePremium.set("Y");
			datcon2rec.frequency.set("01");
			datcon2rec.intDate1.set(acomcalrec.fpcoCurrto);
			datcon2rec.freqFactor.set(-1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				systemError9000();
			}
			wsaaTargFrom.set(datcon2rec.intDate2);
		}
		readTh6051300();
		commissionProcess2000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void readTh6051300()
	{
		start1310();
	}

protected void start1310()
	{
		/* Read TH605 to see whether system is "Override based on Agent    */
		/* Details"                                                        */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(acomcalrec.company);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(acomcalrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			systemError9000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void commissionProcess2000()
	{
		setupKey2010();
		readAgentComm2010();
	}

protected void setupKey2010()
	{
		wsaaJrnseq.set(ZERO);
		agcmdmnIO.setDataArea(SPACES);
		agcmdmnIO.setChdrcoy(acomcalrec.company);
		agcmdmnIO.setChdrnum(acomcalrec.chdrnum);
		agcmdmnIO.setAgntnum(ZERO);
		agcmdmnIO.setLife(acomcalrec.covrLife);
		agcmdmnIO.setCoverage(acomcalrec.covrCoverage);
		agcmdmnIO.setRider(acomcalrec.covrRider);
		agcmdmnIO.setPlanSuffix(acomcalrec.covrPlanSuffix);
		agcmdmnIO.setFunction(varcom.begn);
	}

protected void readAgentComm2010()
	{
		if (isLTE(acomcalrec.covtAnnprem, acomcalrec.covrAnnprem)) {
			compute(wsaaLessPrem, 2).set(sub(acomcalrec.covrAnnprem, acomcalrec.covtAnnprem));
			splitReqd2100();
			resequenceSeqno2200();
			wsaaSqDataKey.set(SPACES);
			wsaaSqChdrcoy.set(acomcalrec.company);
			wsaaSqChdrnum.set(acomcalrec.chdrnum);
			wsaaSqLife.set(acomcalrec.covtLife);
			wsaaSqCoverage.set(acomcalrec.covtCoverage);
			wsaaSqRider.set(acomcalrec.covtRider);
			wsaaSqPlanSuffix.set(acomcalrec.covtPlanSuffix);
			wsaaSqSeqno.set(99);
			wsaaProcessCompleteFlag.set("N");
			while ( !(processComplete.isTrue())) {
				reducePrem3000();
			}
			
			updateRemaining2500();
		}
		else {
			/*        SUBTRACT ACOM-COVR-ANNPREM                               */
			/*                                   FROM ACOM-COVT-ANNPREM        */
			/*                                   GIVING WSAA-ADD-PREM          */
			/* To obtain WSAA-ADD-PREM we accumulate all anual premiums     */
			/* with valid flag 1 and not dormant (2400-REDUCE-PREM) and     */
			/* then we subtract the result from the requested new anual     */
			/* premium.                                                     */
			wsaaAddPrem.set(ZERO);
			reducePrem2400();
			compute(wsaaAddPrem, 2).set(sub(acomcalrec.covtAnnprem, wsaaAddPrem));
			wsaaProcessCompleteFlag.set("N");
			while ( !(processComplete.isTrue())) {
				increasePrem5000();
			}
			
			updateRemaining2500();
			if (isGT(wsaaAddPrem, ZERO)) {
				increasePrem6000();
			}
		}
	}

protected void splitReqd2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupKey2110();
				case seqLvlTot2120: 
					seqLvlTot2120();
				case nextAgcm2150: 
					nextAgcm2150();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupKey2110()
	{
		wsaaSplitPrem.set(wsaaLessPrem);
		wsaaIfSplitReqd.set("Y");
		agcmsqnIO.setDataArea(SPACES);
		agcmsqnIO.setChdrcoy(acomcalrec.company);
		agcmsqnIO.setChdrnum(acomcalrec.chdrnum);
		agcmsqnIO.setLife(acomcalrec.covtLife);
		agcmsqnIO.setCoverage(acomcalrec.covtCoverage);
		agcmsqnIO.setRider(acomcalrec.covtRider);
		agcmsqnIO.setPlanSuffix(acomcalrec.covtPlanSuffix);
		agcmsqnIO.setSeqno(99);
		agcmsqnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmsqnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmsqnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void seqLvlTot2120()
	{
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqnIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqnIO.getChdrnum())
		|| isNE(acomcalrec.covrLife, agcmsqnIO.getLife())
		|| isNE(acomcalrec.covrCoverage, agcmsqnIO.getCoverage())
		|| isNE(acomcalrec.covrRider, agcmsqnIO.getRider())
		|| isNE(acomcalrec.covrPlanSuffix, agcmsqnIO.getPlanSuffix())
		|| isEQ(agcmsqnIO.getStatuz(), varcom.endp)) {
			agcmsqnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.nextAgcm2150);
		}
		if (isEQ(agcmsqnIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.nextAgcm2150);
		}
		compute(wsaaSplitPrem, 2).set(sub(wsaaSplitPrem, agcmsqnIO.getAnnprem()));
		if (isEQ(wsaaSplitPrem, ZERO)) {
			wsaaIfSplitReqd.set("N");
			goTo(GotoLabel.exit2190);
		}
	}

protected void nextAgcm2150()
	{
		agcmsqnIO.setFunction(varcom.nextr);
		goTo(GotoLabel.seqLvlTot2120);
	}

protected void resequenceSeqno2200()
	{
		try {
			setupKey2210();
			readAgentComm2210();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void setupKey2210()
	{
		if (!wsaaSplitReqd.isTrue()) {
			goTo(GotoLabel.exit2290);
		}
		agcmdmyIO.setDataArea(SPACES);
		agcmdmyIO.setStatuz(SPACES);
		agcmdmyIO.setChdrcoy(acomcalrec.company);
		agcmdmyIO.setChdrnum(acomcalrec.chdrnum);
		agcmdmyIO.setLife(acomcalrec.covrLife);
		agcmdmyIO.setCoverage(acomcalrec.covrCoverage);
		agcmdmyIO.setRider(acomcalrec.covrRider);
		agcmdmyIO.setPlanSuffix(acomcalrec.covrPlanSuffix);
		agcmdmyIO.setSeqno(99);
		/* MOVE BEGNH                  TO AGCMDMY-FUNCTION.             */
		agcmdmyIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmdmyIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmdmyIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void readAgentComm2210()
	{
		while ( !(isEQ(agcmdmyIO.getStatuz(), varcom.endp))) {
			resequenceVflg22300();
		}
		
	}

protected void resequenceVflg22300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call2310();
				case next2320: 
					next2320();
				case exit2390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call2310()
	{
		SmartFileCode.execute(appVars, agcmdmyIO);
		if (isNE(agcmdmyIO.getStatuz(), varcom.oK)
		&& isNE(agcmdmyIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmdmyIO.getParams());
			syserrrec.statuz.set(agcmdmyIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmdmyIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmdmyIO.getChdrnum())
		|| isNE(acomcalrec.covtLife, agcmdmyIO.getLife())
		|| isNE(acomcalrec.covtCoverage, agcmdmyIO.getCoverage())
		|| isNE(acomcalrec.covtRider, agcmdmyIO.getRider())
		|| isNE(acomcalrec.covtPlanSuffix, agcmdmyIO.getPlanSuffix())
		|| isEQ(agcmdmyIO.getStatuz(), varcom.endp)) {
			agcmdmyIO.setStatuz(varcom.endp);
		}
		if (isEQ(agcmdmyIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2390);
		}
		/* IF WSAA-SPLIT-REQD                                           */
		/*     MOVE DELET              TO AGCMDMY-FUNCTION              */
		/* ELSE                                                         */
		/*     MOVE REWRT              TO AGCMDMY-FUNCTION.             */
		agcmdmyIO.setValidflag("2");
		agcmdmyIO.setCurrto(acomcalrec.payrPtdate);
		if (flexiblePremium.isTrue()) {
			agcmdmyIO.setCurrto(wsaaTargFrom);
		}
		/* MOVE REWRT                  TO AGCMDMY-FUNCTION.     <LA3993>*/
		/*                                                      <LA3993>*/
		agcmdmyIO.setFunction(varcom.writd);
		agcmdmyIO.setFormat(agcmdmyrec);
		SmartFileCode.execute(appVars, agcmdmyIO);
		if (isNE(agcmdmyIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdmyIO.getParams());
			syserrrec.statuz.set(agcmdmyIO.getStatuz());
			systemError9000();
		}
		if (wsaaSplitReqd.isTrue()) {
			setPrecision(agcmdmyIO.getSeqno(), 0);
			agcmdmyIO.setSeqno(add(agcmdmyIO.getSeqno(), 1));
			agcmdmyIO.setTermid(acomcalrec.termid);
			agcmdmyIO.setTransactionDate(acomcalrec.transactionDate);
			agcmdmyIO.setTransactionTime(acomcalrec.transactionTime);
			agcmdmyIO.setUser(acomcalrec.user);
			agcmdmyIO.setValidflag("1");
			agcmdmyIO.setCurrto(varcom.vrcmMaxDate);
			agcmdmyIO.setTranno(acomcalrec.tranno);
			agcmdmyIO.setFunction(varcom.writr);
		}
		else {
			goTo(GotoLabel.next2320);
		}
		SmartFileCode.execute(appVars, agcmdmyIO);
		if (isNE(agcmdmyIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdmyIO.getParams());
			syserrrec.statuz.set(agcmdmyIO.getStatuz());
			systemError9000();
		}
	}

protected void next2320()
	{
		agcmdmyIO.setFunction(varcom.nextr);
	}

protected void reducePrem2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupKey2401();
				case seqLvlTot2420: 
					seqLvlTot2420();
				case nextAgcm2450: 
					nextAgcm2450();
				case exit2490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupKey2401()
	{
		agcmsqnIO.setDataArea(SPACES);
		agcmsqnIO.setChdrcoy(acomcalrec.company);
		agcmsqnIO.setChdrnum(acomcalrec.chdrnum);
		agcmsqnIO.setLife(acomcalrec.covtLife);
		agcmsqnIO.setCoverage(acomcalrec.covtCoverage);
		agcmsqnIO.setRider(acomcalrec.covtRider);
		agcmsqnIO.setPlanSuffix(acomcalrec.covtPlanSuffix);
		agcmsqnIO.setSeqno(99);
		agcmsqnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmsqnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmsqnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void seqLvlTot2420()
	{
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqnIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqnIO.getChdrnum())
		|| isNE(acomcalrec.covrLife, agcmsqnIO.getLife())
		|| isNE(acomcalrec.covrCoverage, agcmsqnIO.getCoverage())
		|| isNE(acomcalrec.covrRider, agcmsqnIO.getRider())
		|| isNE(acomcalrec.covrPlanSuffix, agcmsqnIO.getPlanSuffix())
		|| isEQ(agcmsqnIO.getStatuz(), varcom.endp)) {
			agcmsqnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2490);
		}
		if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.nextAgcm2450);
		}
		if (isEQ(agcmsqnIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.nextAgcm2450);
		}
		compute(wsaaAddPrem, 2).set(add(wsaaAddPrem, agcmsqnIO.getAnnprem()));
	}

protected void nextAgcm2450()
	{
		agcmsqnIO.setFunction(varcom.nextr);
		goTo(GotoLabel.seqLvlTot2420);
	}

protected void updateRemaining2500()
	{
		begn2510();
	}

protected void begn2510()
	{
		/* Update all remaining validflag '1' AGCMs which were not      */
		/* altered by the transaction and which do not relate to a      */
		/* single premium to ensure that all validflag '1' records      */
		/* have the same transaction number.  This is necessary to      */
		/* allow reversal processing to function correctly.             */
		agcmbcrIO.setDataArea(SPACES);
		agcmbcrIO.setChdrcoy(acomcalrec.company);
		agcmbcrIO.setChdrnum(acomcalrec.chdrnum);
		agcmbcrIO.setLife(acomcalrec.covtLife);
		agcmbcrIO.setCoverage(acomcalrec.covtCoverage);
		agcmbcrIO.setRider(acomcalrec.covtRider);
		agcmbcrIO.setPlanSuffix(acomcalrec.covtPlanSuffix);
		setPrecision(agcmbcrIO.getTranno(), 0);
		agcmbcrIO.setTranno(sub(acomcalrec.tranno, 1));
		agcmbcrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbcrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmbcrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		agcmbcrIO.setFormat(agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)
		&& isNE(agcmbcrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError9000();
		}
		if (isNE(agcmbcrIO.getChdrcoy(), acomcalrec.company)
		|| isNE(agcmbcrIO.getChdrnum(), acomcalrec.chdrnum)
		|| isNE(agcmbcrIO.getLife(), acomcalrec.covrLife)
		|| isNE(agcmbcrIO.getCoverage(), acomcalrec.covrCoverage)
		|| isNE(agcmbcrIO.getRider(), acomcalrec.covrRider)
		|| isNE(agcmbcrIO.getPlanSuffix(), acomcalrec.covrPlanSuffix)) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmbcrIO.getStatuz(), varcom.endp))) {
			updateAgcms2550();
		}
		
	}

protected void updateAgcms2550()
	{
		check2560();
		next2570();
	}

protected void check2560()
	{
		if (isNE(agcmbcrIO.getValidflag(), "1")
		|| isEQ(agcmbcrIO.getPtdate(), ZERO)) {
			return ;
		}
		/* Update the AGCM record as validflag '2'.                     */
		agcmbcrIO.setValidflag("2");
		agcmbcrIO.setCurrto(acomcalrec.payrPtdate);
		if (flexiblePremium.isTrue()) {
			agcmbcrIO.setCurrto(wsaaTargFrom);
		}
		agcmbcrIO.setFunction(varcom.writd);
		agcmbcrIO.setFormat(agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError9000();
		}
		/* Write a new validflag '1' AGCM record with the new           */
		/* transaction number.                                          */
		agcmbcrIO.setValidflag("1");
		agcmbcrIO.setTranno(acomcalrec.tranno);
		agcmbcrIO.setCurrto(varcom.vrcmMaxDate);
		agcmbcrIO.setFunction(varcom.writr);
		agcmbcrIO.setFormat(agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError9000();
		}
	}

protected void next2570()
	{
		/* Look for the next record to update.                          */
		agcmbcrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)
		&& isNE(agcmbcrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError9000();
		}
		if (isNE(agcmbcrIO.getChdrcoy(), acomcalrec.company)
		|| isNE(agcmbcrIO.getChdrnum(), acomcalrec.chdrnum)
		|| isNE(agcmbcrIO.getCoverage(), acomcalrec.covrCoverage)
		|| isNE(agcmbcrIO.getLife(), acomcalrec.covrLife)
		|| isNE(agcmbcrIO.getRider(), acomcalrec.covrRider)
		|| isNE(agcmbcrIO.getPlanSuffix(), acomcalrec.covrPlanSuffix)) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void reducePrem3000()
	{
		try {
			setupKey3010();
			seqLvlTot3020();
			seqLvlTot3030();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void setupKey3010()
	{
		if (isLTE(wsaaLessPrem, ZERO)) {
			wsaaProcessCompleteFlag.set("Y");
			agcmsqnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3090);
		}
		wsaaSq1TotPrem.set(ZERO);
		agcmsqnIO.setDataArea(SPACES);
		agcmsqnIO.setStatuz(SPACES);
		wsaaSqSeqno.subtract(1);
		wsaaSqDormantFlag.set(SPACES);
		agcmsqnIO.setDataKey(wsaaSqDataKey);
		agcmsqnIO.setFunction(varcom.begn);
	}

protected void seqLvlTot3020()
	{
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqnIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqnIO.getChdrnum())
		|| isNE(acomcalrec.covrLife, agcmsqnIO.getLife())
		|| isNE(acomcalrec.covrCoverage, agcmsqnIO.getCoverage())
		|| isNE(acomcalrec.covrRider, agcmsqnIO.getRider())
		|| isNE(acomcalrec.covrPlanSuffix, agcmsqnIO.getPlanSuffix())
		|| isEQ(agcmsqnIO.getStatuz(), varcom.endp)) {
			agcmsqnIO.setStatuz(varcom.endp);
			wsaaProcessCompleteFlag.set("Y");
			goTo(GotoLabel.exit3090);
		}
		/* ADD AGCMSQN-ANNPREM         TO WSAA-SQ1-TOT-PREM.            */
		if (isNE(agcmsqnIO.getPtdate(), ZERO)) {
			wsaaSq1TotPrem.add(agcmsqnIO.getAnnprem());
		}
		wsaaSqDataKey.set(agcmsqnIO.getDataKey());
		agcmsqnIO.setFunction(varcom.nextr);
		while ( !(isEQ(agcmsqnIO.getStatuz(), varcom.endp))) {
			seqnoLevelTot3100();
		}
		
		if (isEQ(wsaaSq1TotPrem, ZERO)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void seqLvlTot3030()
	{
		agcmsqnIO.setStatuz(varcom.oK);
		agcmsqnIO.setDataKey(wsaaSqDataKey);
		/* MOVE BEGNH                  TO AGCMSQN-FUNCTION.             */
		agcmsqnIO.setFunction(varcom.begn);
		agcmsqnIO.setFormat(agcmsqnrec);
		wsaaNewTotpremVf2.set(ZERO);
		wsaaInitCom.set("N");
		while ( !(isEQ(agcmsqnIO.getStatuz(), varcom.endp))) {
			updateAgcm3200();
		}
		
		if (flexiblePremium.isTrue()
		&& initCom.isTrue()) {
			recalcFpInitcom3250();
		}
	}

protected void seqnoLevelTot3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call3110();
				case nextAgcm3120: 
					nextAgcm3120();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call3110()
	{
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqnIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqnIO.getChdrnum())
		|| isNE(acomcalrec.covtLife, agcmsqnIO.getLife())
		|| isNE(acomcalrec.covtCoverage, agcmsqnIO.getCoverage())
		|| isNE(acomcalrec.covtRider, agcmsqnIO.getRider())
		|| isNE(acomcalrec.covtPlanSuffix, agcmsqnIO.getPlanSuffix())
		|| isNE(wsaaSqSeqno, agcmsqnIO.getSeqno())
		|| isEQ(agcmsqnIO.getStatuz(), varcom.endp)) {
			agcmsqnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.nextAgcm3120);
		}
		if (isEQ(agcmsqnIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.nextAgcm3120);
		}
		wsaaSq1TotPrem.add(agcmsqnIO.getAnnprem());
	}

protected void nextAgcm3120()
	{
		agcmsqnIO.setFunction(varcom.nextr);
	}

protected void updateAgcm3200()
	{
		call3210();
	}

protected void call3210()
	{
		if (isLTE(wsaaLessPrem, ZERO)) {
			agcmsqnIO.setStatuz(varcom.endp);
			return ;
		}
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqnIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqnIO.getChdrnum())
		|| isNE(acomcalrec.covtLife, agcmsqnIO.getLife())
		|| isNE(acomcalrec.covtCoverage, agcmsqnIO.getCoverage())
		|| isNE(acomcalrec.covtRider, agcmsqnIO.getRider())
		|| isNE(acomcalrec.covtPlanSuffix, agcmsqnIO.getPlanSuffix())
		|| isNE(wsaaSqSeqno, agcmsqnIO.getSeqno())
		|| isEQ(agcmsqnIO.getStatuz(), varcom.endp)) {
			wsaaLessPrem.set(wsaaRunPrem);
			if (isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
				/* REWRT to prevent locked recs causing ABEND in subsequent prgs.  */
				/*     MOVE REWRT                  TO AGCMSQN-FUNCTION  <A05017>*/
				agcmsqnIO.setFunction(varcom.writd);
				agcmsqnIO.setFormat(agcmsqnrec);
				SmartFileCode.execute(appVars, agcmsqnIO);
				if (isNE(agcmsqnIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(agcmsqnIO.getParams());
					syserrrec.statuz.set(agcmsqnIO.getStatuz());
					systemError9000();
				}
				else {
					agcmsqnIO.setStatuz(varcom.endp);
				}
			}
		}
		if (isEQ(agcmsqnIO.getStatuz(), varcom.endp)) {
			return ;
		}
		agcmsqnIO.setFunction(varcom.nextr);
		if (isEQ(agcmsqnIO.getPtdate(), ZERO)) {
			return ;
		}
		if (isEQ(agcmsqnIO.getTranno(), acomcalrec.tranno)) {
			return ;
		}
		compute(wsaaSplitFactor, 9).setRounded(div(agcmsqnIO.getAnnprem(), wsaaSq1TotPrem));
		compute(wsaaCalcPrem, 9).setRounded(mult(wsaaLessPrem, wsaaSplitFactor));
		if (isLT(wsaaCalcPrem, agcmsqnIO.getAnnprem())) {
			validflagTo13300();
			validflagTo23400();
		}
		else {
			validflagTo23500();
		}
		agcmsqnIO.setFunction(varcom.nextr);
	}

protected void recalcFpInitcom3250()
	{
		call3250();
	}

	/**
	* <pre>
	******************************                            <D9604> 
	*                                                         <D9604> 
	*  For Flexible Premiums where the initial commission was <D9604> 
	*  not all paid or earned, we must re-read all the updated<D9604> 
	*  AGCMs for the sequence number as we have at the moment <D9604> 
	*  'artificially' set the earned and paid to the original <D9604> 
	*  values. We must now update them with the correct       <D9604> 
	*  initial commission by calling the payment subroutine.  <D9604> 
	*  If we had done it earlier the subroutine would have    <D9604> 
	*  returned the wrong values as it works out total AGCM an<D9604> 
	*  premiums per sequence number - the AGCMs would not all <D9604> 
	*  been updated, therefore the results would have been inc<D9604> 
	* </pre>
	*/
protected void call3250()
	{
		/*                                                         <D9604> */
		agcmsqnIO.setStatuz(varcom.oK);
		agcmsqnIO.setDataKey(wsaaSqDataKey);
		agcmsqnIO.setFunction(varcom.begn);
		agcmsqnIO.setFormat(agcmsqnrec);
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqnIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqnIO.getChdrnum())
		|| isNE(acomcalrec.covtLife, agcmsqnIO.getLife())
		|| isNE(acomcalrec.covtCoverage, agcmsqnIO.getCoverage())
		|| isNE(acomcalrec.covtRider, agcmsqnIO.getRider())
		|| isNE(acomcalrec.covtPlanSuffix, agcmsqnIO.getPlanSuffix())
		|| isNE(wsaaSqSeqno, agcmsqnIO.getSeqno())
		|| isEQ(agcmsqnIO.getStatuz(), varcom.endp)) {
			agcmsqnIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmsqnIO.getStatuz(), varcom.endp))) {
			callFlexPayMeth3255();
		}
		
	}

	/**
	* <pre>
	*                                                         <D9604> 
	* </pre>
	*/
protected void callFlexPayMeth3255()
	{
		try {
			start3256();
			nextAgcm3255();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

	/**
	* <pre>
	********************************                          <D9604> 
	* </pre>
	*/
protected void start3256()
	{
		/*  For Flexible Premiums, we must recall the commission   <D9604> */
		/*  Payment routine passed in the linkage, to find the diff<D9604> */
		/*  between what has been paid and earned and what should b<D9604> */
		/*  and earned.  ACMV entries must be created for these    <D9604> */
		/*  differences.                                           <D9604> */
		commonClnk8000();
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(acomcalrec.company);
		itemIO.setItemtabl(t5644);
		if (isEQ(acomcalrec.bascpy, SPACES)) {
			agcmsqnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3255);
		}
		itemIO.setItemitem(acomcalrec.bascpy);
		comlinkrec.method.set(acomcalrec.bascpy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(t5644rec.compysubr, SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			if (isNE(comlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				syserrrec.statuz.set(comlinkrec.statuz);
				databaseError9500();
			}
		}
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		/*  The amount returned will be the actual amount payable. <D9604> */
		/*  Any difference between the pro-rata earned and paid    <D9604> */
		/*  will result in a clawback or payment.                  <D9604> */
		/*                                                         <D9604> */
		compute(wsaaErnDiff, 2).set(sub(comlinkrec.erndamt, agcmsqnIO.getComern()));
		compute(wsaaPayDiff, 2).set(sub(comlinkrec.payamnt, agcmsqnIO.getCompay()));
		clawbackOrPay8100();
		agcmsqnIO.setCompay(comlinkrec.payamnt);
		agcmsqnIO.setComern(comlinkrec.erndamt);
		agcmsqnIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
	}

protected void nextAgcm3255()
	{
		agcmsqnIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqnIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqnIO.getChdrnum())
		|| isNE(acomcalrec.covtLife, agcmsqnIO.getLife())
		|| isNE(acomcalrec.covtCoverage, agcmsqnIO.getCoverage())
		|| isNE(acomcalrec.covtRider, agcmsqnIO.getRider())
		|| isNE(acomcalrec.covtPlanSuffix, agcmsqnIO.getPlanSuffix())
		|| isNE(wsaaSqSeqno, agcmsqnIO.getSeqno())
		|| isEQ(agcmsqnIO.getStatuz(), varcom.endp)) {
			agcmsqnIO.setStatuz(varcom.endp);
		}
	}

protected void validflagTo13300()
	{
		setupKey3310();
		rewriteVldfg13320();
	}

protected void setupKey3310()
	{
		/* Write a validflag '2' record prior to the changes.           */
		agcmsqnIO.setValidflag("2");
		agcmsqnIO.setCurrto(acomcalrec.payrPtdate);
		if (flexiblePremium.isTrue()) {
			agcmsqnIO.setCurrto(wsaaTargFrom);
		}
		agcmsqnIO.setFunction(varcom.writd);
		agcmsqnIO.setFormat(agcmsqnrec);
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			syserrrec.params.set(agcmsqnIO.getParams());
			systemError9000();
		}
		agcmsqnIO.setValidflag("1");
		agcmsqnIO.setCurrto(varcom.vrcmMaxDate);
		agcmsqnIO.setTermid(acomcalrec.termid);
		agcmsqnIO.setTransactionDate(acomcalrec.transactionDate);
		agcmsqnIO.setTransactionTime(acomcalrec.transactionTime);
		agcmsqnIO.setUser(acomcalrec.user);
		agcmsqnIO.setTranno(acomcalrec.tranno);
		if (flexiblePremium.isTrue()) {
			agcmsqnIO.setCurrfrom(wsaaTargFrom);
		}
		else {
			agcmsqnIO.setCurrfrom(acomcalrec.payrBtdate);
		}
		/*    MOVE ACOM-PAYR-BTDATE       TO AGCMSQN-CURRFROM.     <D9604> */
		agcmsqnIO.setPtdate(acomcalrec.payrPtdate);
		wsaaAnnprem.set(agcmsqnIO.getAnnprem());
		wsaaOldComern.set(agcmsqnIO.getComern());
		wsaaOldCompay.set(agcmsqnIO.getCompay());
		wsaaOldInitcom.set(agcmsqnIO.getInitcom());
		setPrecision(agcmsqnIO.getAnnprem(), 2);
		agcmsqnIO.setAnnprem(sub(agcmsqnIO.getAnnprem(), wsaaCalcPrem));
		compute(wsaaSplitFactor2, 9).setRounded(div(agcmsqnIO.getAnnprem(), wsaaAnnprem));
		if (flexiblePremium.isTrue()) {
			if (isNE(agcmsqnIO.getComern(), agcmsqnIO.getInitcom())
			|| isNE(agcmsqnIO.getCompay(), agcmsqnIO.getInitcom())) {
				wsaaInitCom.set("Y");
			}
		}
		setPrecision(agcmsqnIO.getComern(), 9);
		agcmsqnIO.setComern(mult(agcmsqnIO.getComern(), wsaaSplitFactor2), true);
		setPrecision(agcmsqnIO.getCompay(), 9);
		agcmsqnIO.setCompay(mult(agcmsqnIO.getCompay(), wsaaSplitFactor2), true);
		/*    Initial Commission should be calculated the same as the*/
		/*    Paid and Earned fields.*/
		setPrecision(agcmsqnIO.getInitcom(), 9);
		agcmsqnIO.setInitcom(mult(agcmsqnIO.getInitcom(), wsaaSplitFactor2), true);
	}

protected void rewriteVldfg13320()
	{
		/* MOVE WRITD                  TO AGCMSQN-FUNCTION.     <A05940>*/
		/* MOVE REWRT                  TO AGCMSQN-FUNCTION.             */
		agcmsqnIO.setFunction(varcom.writr);
		agcmsqnIO.setFormat(agcmsqnrec);
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		/*EXIT*/
	}

protected void validflagTo23400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupKey3410();
					writePara3415();
				case writeVldfg23420: 
					writeVldfg23420();
				case pass3430: 
					pass3430();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupKey3410()
	{
		setPrecision(agcmsqnIO.getComern(), 2);
		agcmsqnIO.setComern(sub(wsaaOldComern, agcmsqnIO.getComern()));
		setPrecision(agcmsqnIO.getCompay(), 2);
		agcmsqnIO.setCompay(sub(wsaaOldCompay, agcmsqnIO.getCompay()));
		setPrecision(agcmsqnIO.getInitcom(), 2);
		agcmsqnIO.setInitcom(sub(wsaaOldInitcom, agcmsqnIO.getInitcom()));
		agcmsqnIO.setAnnprem(wsaaCalcPrem);
		agcmsqnIO.setValidflag("1");
		setPrecision(agcmsqnIO.getSeqno(), 0);
		agcmsqnIO.setSeqno(add(agcmsqnIO.getSeqno(), 1));
		agcmsqnIO.setTranno(acomcalrec.tranno);
		agcmsqnIO.setCurrfrom(acomcalrec.payrBtdate);
		if (flexiblePremium.isTrue()) {
			agcmsqnIO.setCurrfrom(wsaaTargFrom);
		}
		agcmsqnIO.setPtdate(acomcalrec.payrPtdate);
		agcmsqnIO.setTransactionDate(acomcalrec.transactionDate);
		agcmsqnIO.setTransactionTime(acomcalrec.transactionTime);
		/* Set up Dormant Flag*/
		agcmsqnIO.setDormantFlag("Y");
	}

protected void writePara3415()
	{
		agcmsqnIO.setFormat(agcmsqnrec);
		agcmsqnIO.setFunction(varcom.writr);
		if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.writeVldfg23420);
		}
		wsaaNewTotpremVf2.add(agcmsqnIO.getAnnprem());
		/* This is the new decreased premium being totaled for each*/
		/* sequence number(wsaa-new-totprem-vf2).Agcmsq1-annprem was*/
		/* incorrectly used , this caused a loop if you had more*/
		/* than 1 agent with the same sequence number.*/
		compute(wsaaRunPrem, 2).set(sub(wsaaLessPrem, wsaaNewTotpremVf2));
	}

protected void writeVldfg23420()
	{
		agcmdmnIO.setFunction(agcmsqnIO.getFunction());
		agcmdmnIO.setStatuz(agcmsqnIO.getStatuz());
		agcmdmnIO.setGenDate(agcmsqnIO.getGenDate());
		agcmdmnIO.setGenTime(agcmsqnIO.getGenTime());
		agcmdmnIO.setVn(agcmsqnIO.getVn());
		agcmdmnIO.setFormat(agcmdmnrec);
		agcmdmnIO.setChdrcoy(agcmsqnIO.getChdrcoy());
		agcmdmnIO.setChdrnum(agcmsqnIO.getChdrnum());
		agcmdmnIO.setLife(agcmsqnIO.getLife());
		agcmdmnIO.setCoverage(agcmsqnIO.getCoverage());
		agcmdmnIO.setRider(agcmsqnIO.getRider());
		agcmdmnIO.setPlanSuffix(agcmsqnIO.getPlanSuffix());
		agcmdmnIO.setSeqno(agcmsqnIO.getSeqno());
		agcmdmnIO.setTranno(agcmsqnIO.getTranno());
		agcmdmnIO.setAgntnum(agcmsqnIO.getAgntnum());
		agcmdmnIO.setEfdate(agcmsqnIO.getEfdate());
		agcmdmnIO.setAnnprem(agcmsqnIO.getAnnprem());
		agcmdmnIO.setBasicCommMeth(agcmsqnIO.getBasicCommMeth());
		agcmdmnIO.setInitcom(agcmsqnIO.getInitcom());
		agcmdmnIO.setBascpy(agcmsqnIO.getBascpy());
		agcmdmnIO.setCompay(agcmsqnIO.getCompay());
		agcmdmnIO.setComern(agcmsqnIO.getComern());
		agcmdmnIO.setSrvcpy(agcmsqnIO.getSrvcpy());
		agcmdmnIO.setScmdue(agcmsqnIO.getScmdue());
		agcmdmnIO.setScmearn(agcmsqnIO.getScmearn());
		agcmdmnIO.setRnwcpy(agcmsqnIO.getRnwcpy());
		agcmdmnIO.setRnlcdue(agcmsqnIO.getRnlcdue());
		agcmdmnIO.setRnlcearn(agcmsqnIO.getRnlcearn());
		agcmdmnIO.setAgentClass(agcmsqnIO.getAgentClass());
		agcmdmnIO.setTermid(agcmsqnIO.getTermid());
		agcmdmnIO.setTransactionDate(acomcalrec.transactionDate);
		agcmdmnIO.setTransactionTime(acomcalrec.transactionTime);
		agcmdmnIO.setUser(agcmsqnIO.getUser());
		agcmdmnIO.setValidflag(agcmsqnIO.getValidflag());
		agcmdmnIO.setCurrfrom(agcmsqnIO.getCurrfrom());
		agcmdmnIO.setCurrto(agcmsqnIO.getCurrto());
		agcmdmnIO.setPtdate(agcmsqnIO.getPtdate());
		agcmdmnIO.setOvrdcat(agcmsqnIO.getOvrdcat());
		agcmdmnIO.setCedagent(agcmsqnIO.getCedagent());
		/* Set up Dormant Flag*/
		agcmdmnIO.setDormantFlag(agcmsqnIO.getDormantFlag());
		//PINNACLE-1380 START
		if(gstOnCommFlag) {
			agcmdmnIO.setInitCommGst(agcmsqnIO.getInitCommGst());
		}
		//PINNACLE-1380 END
		if (flexiblePremium.isTrue()) {
			if (isNE(agcmsqnIO.getComern(), agcmsqnIO.getInitcom())
			|| isNE(agcmsqnIO.getCompay(), agcmsqnIO.getInitcom())) {
				recalcComm3450();
				agcmdmnIO.setComern(agcmsqnIO.getComern());
				agcmdmnIO.setCompay(agcmsqnIO.getComern());
				goTo(GotoLabel.pass3430);
			}
		}
		if (isLT(agcmsqnIO.getComern(), agcmsqnIO.getCompay())) {
			createCommonAcmv3700();
			compute(wsaaClawbackAmt, 2).set(sub(agcmsqnIO.getCompay(), agcmsqnIO.getComern()));
			if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
				ovrdCommClawback3900();
			}
			else {
				commClawback3800();
			}
			agcmdmnIO.setCompay(agcmsqnIO.getComern());
		}
	}

protected void pass3430()
	{
		SmartFileCode.execute(appVars, agcmdmnIO);
		if (isNE(agcmdmnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdmnIO.getParams());
			syserrrec.statuz.set(agcmdmnIO.getStatuz());
			systemError9000();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	******************************                            <D9604> 
	* </pre>
	*/
protected void recalcComm3450()
	{
		start3450();
	}

	/**
	* <pre>
	******************************                            <D9604> 
	*                                                         <D9604> 
	*  For Flexible Premiums where the initial commission was <D9604> 
	*  not all paid or earned, we recalulate the earned by    <D9604> 
	*  calling the flex. prem component modify subroutine     <D9604> 
	*  'FPCOMMOD'.                                            <D9604> 
	*  The amount returned by the subroutine is the amount    <D9604> 
	*  earned and payable. Clawbacks or payments will be due o<D9604> 
	*  any deviation from the calculated pro-rata amount.     <D9604> 
	* </pre>
	*/
protected void start3450()
	{
		commonClnk8000();
		callProgram(Fpcommod.class, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			databaseError9500();
		}
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		/*  The returned earned amount is the amount to pay/ earn  <D9604> */
		/*  The difference between the current earned and this valu<D9604> */
		/*  is the clawback/payment earned amount.                 <D9604> */
		/*  The difference between the compay and the returned earn<D9604> */
		/*  is the clawback/payment paid amount.                   <D9604> */
		compute(wsaaErnDiff, 2).set(sub(comlinkrec.erndamt, agcmsqnIO.getComern()));
		compute(wsaaPayDiff, 2).set(sub(comlinkrec.erndamt, agcmsqnIO.getCompay()));
		clawbackOrPay8100();
		agcmsqnIO.setCompay(comlinkrec.erndamt);
		agcmsqnIO.setComern(comlinkrec.erndamt);
	}

protected void validflagTo23500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3500();
				case validflagTo23560: 
					validflagTo23560();
				case pass3570: 
					pass3570();
				case writr3570: 
					writr3570();
				case exit3590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3500()
	{
		/* Write a validflag '2' record prior to the changes            */
		agcmsqnIO.setValidflag("2");
		agcmsqnIO.setCurrto(acomcalrec.payrPtdate);
		if (flexiblePremium.isTrue()) {
			agcmsqnIO.setCurrto(wsaaTargFrom);
		}
		agcmsqnIO.setFunction(varcom.writd);
		agcmsqnIO.setFormat(agcmsqnrec);
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			syserrrec.params.set(agcmsqnIO.getParams());
			systemError9000();
		}
		agcmsqnIO.setValidflag("1");
		agcmsqnIO.setCurrto(varcom.vrcmMaxDate);
		agcmsqnIO.setTermid(acomcalrec.termid);
		agcmsqnIO.setTransactionDate(acomcalrec.transactionDate);
		agcmsqnIO.setTransactionTime(acomcalrec.transactionTime);
		agcmsqnIO.setUser(acomcalrec.user);
		if (flexiblePremium.isTrue()) {
			agcmsqnIO.setCurrfrom(wsaaTargFrom);
		}
		else {
			agcmsqnIO.setCurrfrom(acomcalrec.payrBtdate);
		}
		/*    MOVE ACOM-PAYR-BTDATE       TO AGCMSQN-CURRFROM.             */
		agcmsqnIO.setTranno(acomcalrec.tranno);
		/* IF  WSAA-SPLIT-REQD                                          */
		/*     MOVE DELET              TO AGCMSQN-FUNCTION              */
		/* ELSE                                                         */
		/*     MOVE REWRT              TO AGCMSQN-FUNCTION.             */
		agcmsqnIO.setFormat(agcmsqnrec);
		/* Set up Dormant Flag*/
		/* MOVE 'Y'                    TO AGCMSQN-DORMANT-FLAG.         */
		/* We change REWRITE for WRITD to prevent STATUS 21             */
		/* when trying to update the record after changing one          */
		/* of the keys stored (AGCMSGN-DORMANT-FLAG)                    */
		/* We also avoid STATUS 21 for DELET statement by only          */
		/* moving 'Y' to DORMANT-FLAG when the record needs to          */
		/* be rewritten.                                                */
		if (wsaaSplitReqd.isTrue()) {
			/*     MOVE READH              TO AGCMSQN-FUNCTION      <A05940>*/
			/*                                                      <A05940>*/
			/*     CALL 'AGCMSQNIO'        USING AGCMSQN-PARAMS     <A05940>*/
			/*                                                      <A05940>*/
			/*     IF AGCMSQN-STATUZ           NOT = O-K            <A05940>*/
			/*        MOVE AGCMSQN-PARAMS      TO SYSR-PARAMS       <A05940>*/
			/*        MOVE AGCMSQN-STATUZ      TO SYSR-STATUZ       <A05940>*/
			/*        PERFORM 9000-SYSTEM-ERROR                     <A05940>*/
			/*     END-IF                                           <A05940>*/
			/*                                                      <A05940>*/
			/*     MOVE DELET              TO AGCMSQN-FUNCTION      <A05940>*/
			/*NEXT_SENTENCE*/
		}
		else {
			/*     MOVE WRITD              TO AGCMSQN-FUNCTION      <A05940>*/
			agcmsqnIO.setFunction(varcom.writr);
			agcmsqnIO.setDormantFlag("Y");
		}
		if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.validflagTo23560);
		}
		wsaaNewTotpremVf2.add(agcmsqnIO.getAnnprem());
		/* This is the new decreasced premium being totaled for each*/
		/* sequence number(wsaa-new-totprem-vf2).*/
		compute(wsaaRunPrem, 2).set(sub(wsaaLessPrem, wsaaNewTotpremVf2));
	}

protected void validflagTo23560()
	{
		/* Move the update/delete of the AGCM after the clawback        */
		/* processing so that the AGCM is updated with the commission   */
		/* paid equal to commission earned even if no splitting of the  */
		/* AGCM is required.                                            */
		/* CALL 'AGCMSQNIO'            USING AGCMSQN-PARAMS.            */
		/* IF AGCMSQN-STATUZ           NOT = O-K                        */
		/*    MOVE AGCMSQN-PARAMS      TO SYSR-PARAMS                   */
		/*    MOVE AGCMSQN-STATUZ      TO SYSR-STATUZ                   */
		/*    PERFORM 9000-SYSTEM-ERROR.                                */
		if (flexiblePremium.isTrue()) {
			if (isNE(agcmsqnIO.getComern(), agcmsqnIO.getInitcom())
			|| isNE(agcmsqnIO.getCompay(), agcmsqnIO.getInitcom())) {
				recalcComm3450();
				goTo(GotoLabel.pass3570);
			}
		}
		if (isLT(agcmsqnIO.getComern(), agcmsqnIO.getCompay())) {
			compute(wsaaClawbackAmt, 2).set(sub(agcmsqnIO.getCompay(), agcmsqnIO.getComern()));
			createCommonAcmv3700();
			if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
				ovrdCommClawback3900();
			}
			else {
				commClawback3800();
			}
			agcmsqnIO.setCompay(agcmsqnIO.getComern());
		}
	}

protected void pass3570()
	{
		if (wsaaSplitReqd.isTrue()) {
			goTo(GotoLabel.writr3570);
		}
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (!wsaaSplitReqd.isTrue()) {
			goTo(GotoLabel.exit3590);
		}
	}

protected void writr3570()
	{
		agcmsqnIO.setFormat(agcmsqnrec);
		agcmsqnIO.setFunction(varcom.writr);
		setPrecision(agcmsqnIO.getSeqno(), 0);
		agcmsqnIO.setSeqno(add(agcmsqnIO.getSeqno(), 1));
		agcmdmnIO.setFunction(agcmsqnIO.getFunction());
		agcmdmnIO.setStatuz(agcmsqnIO.getStatuz());
		agcmdmnIO.setGenDate(agcmsqnIO.getGenDate());
		agcmdmnIO.setGenTime(agcmsqnIO.getGenTime());
		agcmdmnIO.setVn(agcmsqnIO.getVn());
		agcmdmnIO.setFormat(agcmdmnrec);
		agcmdmnIO.setChdrcoy(agcmsqnIO.getChdrcoy());
		agcmdmnIO.setChdrnum(agcmsqnIO.getChdrnum());
		agcmdmnIO.setLife(agcmsqnIO.getLife());
		agcmdmnIO.setCoverage(agcmsqnIO.getCoverage());
		agcmdmnIO.setRider(agcmsqnIO.getRider());
		agcmdmnIO.setPlanSuffix(agcmsqnIO.getPlanSuffix());
		agcmdmnIO.setSeqno(agcmsqnIO.getSeqno());
		agcmdmnIO.setTranno(agcmsqnIO.getTranno());
		agcmdmnIO.setAgntnum(agcmsqnIO.getAgntnum());
		agcmdmnIO.setEfdate(agcmsqnIO.getEfdate());
		agcmdmnIO.setAnnprem(agcmsqnIO.getAnnprem());
		agcmdmnIO.setBasicCommMeth(agcmsqnIO.getBasicCommMeth());
		agcmdmnIO.setInitcom(agcmsqnIO.getInitcom());
		agcmdmnIO.setBascpy(agcmsqnIO.getBascpy());
		agcmdmnIO.setCompay(agcmsqnIO.getCompay());
		agcmdmnIO.setComern(agcmsqnIO.getComern());
		agcmdmnIO.setSrvcpy(agcmsqnIO.getSrvcpy());
		agcmdmnIO.setScmdue(agcmsqnIO.getScmdue());
		agcmdmnIO.setScmearn(agcmsqnIO.getScmearn());
		agcmdmnIO.setRnwcpy(agcmsqnIO.getRnwcpy());
		agcmdmnIO.setRnlcdue(agcmsqnIO.getRnlcdue());
		agcmdmnIO.setRnlcearn(agcmsqnIO.getRnlcearn());
		agcmdmnIO.setAgentClass(agcmsqnIO.getAgentClass());
		agcmdmnIO.setTermid(agcmsqnIO.getTermid());
		agcmdmnIO.setTransactionDate(acomcalrec.transactionDate);
		agcmdmnIO.setTransactionTime(acomcalrec.transactionTime);
		agcmdmnIO.setUser(agcmsqnIO.getUser());
		agcmdmnIO.setValidflag(agcmsqnIO.getValidflag());
		agcmdmnIO.setCurrfrom(agcmsqnIO.getCurrfrom());
		agcmdmnIO.setCurrto(agcmsqnIO.getCurrto());
		agcmdmnIO.setPtdate(agcmsqnIO.getPtdate());
		agcmdmnIO.setOvrdcat(agcmsqnIO.getOvrdcat());
		agcmdmnIO.setCedagent(agcmsqnIO.getCedagent());
		/* Set up Dormant Flag*/
		agcmsqnIO.setDormantFlag("Y");
		agcmdmnIO.setDormantFlag(agcmsqnIO.getDormantFlag());
		//PINNACLE-1380 START
		if(gstOnCommFlag) {
			agcmdmnIO.setInitCommGst(agcmsqnIO.getInitCommGst());
		}
		//PINNACLE-1380 END
		SmartFileCode.execute(appVars, agcmdmnIO);
		if (isNE(agcmdmnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdmnIO.getParams());
			syserrrec.statuz.set(agcmdmnIO.getStatuz());
			systemError9000();
		}
	}

protected void initialCommCalc3600()
	{
		try {
			initLinkage3610();
			t5647Table3620();
			subrCall3630();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initLinkage3610()
	{
		comlinkrec.chdrcoy.set(acomcalrec.company);
		comlinkrec.chdrnum.set(acomcalrec.chdrnum);
		comlinkrec.life.set(acomcalrec.covrLife);
		comlinkrec.coverage.set(acomcalrec.covrCoverage);
		comlinkrec.effdate.set(acomcalrec.payrPtdate);
		if (flexiblePremium.isTrue()) {
			comlinkrec.effdate.set(wsaaTargFrom);
		}
		comlinkrec.rider.set(acomcalrec.covrRider);
		comlinkrec.planSuffix.set(acomcalrec.covrPlanSuffix);
		comlinkrec.agent.set(agcmsqnIO.getAgntnum());
		comlinkrec.jlife.set(acomcalrec.covrJlife);
		comlinkrec.crtable.set(acomcalrec.covrCrtable);
		comlinkrec.agentClass.set(agcmsqnIO.getAgentClass());
		comlinkrec.icommtot.set(0);
		comlinkrec.icommpd.set(0);
		comlinkrec.icommernd.set(0);
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		comlinkrec.annprem.set(agcmsqnIO.getAnnprem());
		comlinkrec.instprem.set(agcmsqnIO.getAnnprem());
		//PINNACLE-1380 START
		if(gstOnCommFlag) {
		if(isNE(acomcalrec.covtCommPrem,0.00)) {
			comlinkrec.commprem.set(acomcalrec.covtCommPrem);
		}
		if(isNE(acomcalrec.covrCommPrem,0.00)) {
			comlinkrec.commprem.set(acomcalrec.covrCommPrem);
		}
		}
		//PINNACLE-1380 END
		comlinkrec.billfreq.set(acomcalrec.payrBillfreq);
		comlinkrec.billfreq.set(acomcalrec.payrBillfreq);
		comlinkrec.targetPrem.set(acomcalrec.fpcoTargPrem);
		comlinkrec.currto.set(acomcalrec.fpcoCurrto);
	}

protected void t5647Table3620()
	{
		if (isEQ(acomcalrec.basicCommMeth, SPACES)) {
			goTo(GotoLabel.exit3690);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(acomcalrec.company);
		itemIO.setItemtabl(t5647);
		itemIO.setItemitem(acomcalrec.basicCommMeth);
		agcmsqnIO.setBasicCommMeth(acomcalrec.basicCommMeth);
		agcmsqyIO.setBasicCommMeth(acomcalrec.basicCommMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
	}

protected void subrCall3630()
	{
		comlinkrec.method.set(acomcalrec.basicCommMeth);
		comlinkrec.language.set(acomcalrec.atmdLanguage);
		if (isNE(t5647rec.commsubr, SPACES)) {
			callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
		}
		if (isEQ(comlinkrec.statuz, varcom.bomb)) {
			systemError9000();
		}
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		a000CallRounding();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		/*    PERFORM 9000-SYSTEM-ERROR.                                */
		/*  Check for Single Premium by examination of Single Premium   */
		/*  Indicator passed in the linkage. If so, read T5644 to get   */
		/*  the Commission PAYMENT subroutine and call this.            */
		if (isEQ(acomcalrec.singlePremInd, "Y")) {
			itemIO.setDataArea(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(acomcalrec.company);
			itemIO.setItemtabl(t5644);
			if (isNE(acomcalrec.bascpy, SPACES)) {
				itemIO.setItemitem(acomcalrec.bascpy);
				comlinkrec.method.set(acomcalrec.bascpy);
				itemIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, itemIO);
				if (isNE(itemIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(itemIO.getParams());
					syserrrec.statuz.set(itemIO.getStatuz());
					databaseError9500();
				}
				t5644rec.t5644Rec.set(itemIO.getGenarea());
				if (isNE(t5644rec.compysubr, SPACES)) {
					callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
					if (isNE(comlinkrec.statuz, varcom.oK)) {
						syserrrec.params.set(comlinkrec.clnkallRec);
						syserrrec.statuz.set(comlinkrec.statuz);
						databaseError9500();
					}
				}
			}
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
	}

protected void createCommonAcmv3700()
	{
		t5645Page23705();
		accountingRules3710();
		ledgerAccounting3720();
	}

protected void t5645Page23705()
	{
		/*  Read 2nd page of T5645 entries first.                       */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5645);
		itemIO.setItemcoy(acomcalrec.company);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq("01");
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*  Store the entries from page 2.                                 */
		for (wsaaSub1.set(1); !(isGT(wsaaSub1, 15)); wsaaSub1.add(1)){
			wsaaT5645Cnttot[wsaaSub1.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsaaT5645Glmap[wsaaSub1.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub1.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub1.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub1.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
	}

protected void accountingRules3710()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(acomcalrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(acomcalrec.company);
		descIO.setDesctabl(t5645);
		descIO.setLanguage(acomcalrec.atmdLanguage);
		descIO.setDescitem(wsaaSubr);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError9500();
		}
	}

protected void ledgerAccounting3720()
	{
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(acomcalrec.atmdBatchKey);
		lifacmvrec.transactionTime.set(acomcalrec.transactionTime);
		lifacmvrec.transactionDate.set(acomcalrec.transactionDate);
		lifacmvrec.termid.set(acomcalrec.termid);
		lifacmvrec.user.set(acomcalrec.user);
		lifacmvrec.rdocnum.set(acomcalrec.chdrnum);
		lifacmvrec.rldgcoy.set(acomcalrec.company);
		lifacmvrec.origcurr.set(acomcalrec.payrCntcurr);
		lifacmvrec.tranref.set(acomcalrec.tranno);
		lifacmvrec.tranno.set(acomcalrec.tranno);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(acomcalrec.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		/*  If this is a Single Premium case, use the current from date */
		/*  on the coverage (held in ACOM-CHDR-CURRFROM) rather than    */
		/*  the contract commencement date.                             */
		if (isEQ(acomcalrec.singlePremInd, "Y")) {
			lifacmvrec.effdate.set(acomcalrec.chdrCurrfrom);
		}
		else {
			lifacmvrec.effdate.set(acomcalrec.chdrOccdate);
		}
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
	}

protected void commClawback3800()
	{
		para3800();
	}

protected void para3800()
	{
		lifacmvrec.origamt.set(wsaaClawbackAmt);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		/* if this is a single premium contract, use the single         */
		/* premium account set in T5645                                 */
		if (isEQ(acomcalrec.singlePremInd, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[2]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[2]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[2]);
			lifacmvrec.glsign.set(wsaaT5645Sign[2]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[2]);
		}
		lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		/*    MOVE ACOM-CHDRNUM           TO LIFA-RLDGACCT.                */
		lifacmvrec.rldgacct.set(agcmsqnIO.getAgntnum());
		wsaaRldgChdrnum.set(acomcalrec.chdrnum);
		wsaaRldgLife.set(acomcalrec.covrLife);
		wsaaRldgCoverage.set(acomcalrec.covrCoverage);
		wsaaRldgRider.set(acomcalrec.covrRider);
		wsaaPlan.set(acomcalrec.covrPlanSuffix);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		writeAcmv4000();
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(lifacmvrec.origamt);
			zorlnkrec.clawback.set("Y");
			callZorcompy6300();
		}
		if (isEQ(acomcalrec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaClawbackAmt);
			lifacmvrec.sacscode.set(t5645rec.sacscode11);
			lifacmvrec.sacstyp.set(t5645rec.sacstype11);
			lifacmvrec.glcode.set(t5645rec.glmap11);
			lifacmvrec.glsign.set(t5645rec.sign11);
			lifacmvrec.contot.set(t5645rec.cnttot11);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
		}
		else {
			lifacmvrec.origamt.set(wsaaClawbackAmt);
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		writeAcmv4000();
	}

protected void ovrdCommClawback3900()
	{
		para3900();
	}

protected void para3900()
	{
		lifacmvrec.origamt.set(wsaaClawbackAmt);
		lifacmvrec.sacscode.set(t5645rec.sacscode09);
		lifacmvrec.sacstyp.set(t5645rec.sacstype09);
		lifacmvrec.glcode.set(t5645rec.glmap09);
		lifacmvrec.glsign.set(t5645rec.sign09);
		lifacmvrec.contot.set(t5645rec.cnttot09);
		/*    MOVE ACOM-CHDRNUM           TO LIFA-RLDGACCT.                */
		lifacmvrec.rldgacct.set(agcmsqnIO.getAgntnum());
		lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		writeAcmv4000();
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(lifacmvrec.origamt);
			zorlnkrec.clawback.set("Y");
			callZorcompy6300();
		}
		if (isEQ(acomcalrec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaClawbackAmt);
			lifacmvrec.sacscode.set(t5645rec.sacscode12);
			lifacmvrec.sacstyp.set(t5645rec.sacstype12);
			lifacmvrec.glcode.set(t5645rec.glmap12);
			lifacmvrec.glsign.set(t5645rec.sign12);
			lifacmvrec.contot.set(t5645rec.cnttot12);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
		}
		else {
			lifacmvrec.origamt.set(wsaaClawbackAmt);
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		writeAcmv4000();
	}

protected void writeAcmv4000()
	{
		/*PARA*/
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(acomcalrec.company, lifacmvrec.rldgcoy)
		|| isNE(acomcalrec.chdrnum, lifacmvrec.rdocnum)
		|| isNE(acomcalrec.tranno, lifacmvrec.tranno)) {
			return ;
		}
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError9000();
		}
		/*EXIT*/
	}

protected void increasePrem5000()
	{
		try {
			setupKey5010();
			seqLvlTot5020();
			seqLvlTot5030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void setupKey5010()
	{
		if (isLTE(wsaaAddPrem, ZERO)) {
			wsaaProcessCompleteFlag.set("Y");
			goTo(GotoLabel.exit5090);
		}
		wsaaSq2TotPrem.set(ZERO);
		agcmsqyIO.setDataArea(SPACES);
		agcmsqyIO.setStatuz(SPACES);
		agcmsqyIO.setChdrcoy(acomcalrec.company);
		agcmsqyIO.setChdrnum(acomcalrec.chdrnum);
		agcmsqyIO.setLife(acomcalrec.covrLife);
		agcmsqyIO.setCoverage(acomcalrec.covrCoverage);
		agcmsqyIO.setRider(acomcalrec.covrRider);
		agcmsqyIO.setPlanSuffix(acomcalrec.covrPlanSuffix);
		agcmsqyIO.setSeqno(ZERO);
		agcmsqyIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmsqyIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmsqyIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");

	}

protected void seqLvlTot5020()
	{
		SmartFileCode.execute(appVars, agcmsqyIO);
		if (isNE(agcmsqyIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqyIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqyIO.getParams());
			syserrrec.statuz.set(agcmsqyIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqyIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqyIO.getChdrnum())
		|| isNE(acomcalrec.covrLife, agcmsqyIO.getLife())
		|| isNE(acomcalrec.covrCoverage, agcmsqyIO.getCoverage())
		|| isNE(acomcalrec.covrRider, agcmsqyIO.getRider())
		|| isNE(acomcalrec.covrPlanSuffix, agcmsqyIO.getPlanSuffix())
		|| isEQ(agcmsqyIO.getStatuz(), varcom.endp)) {
			agcmsqyIO.setStatuz(varcom.endp);
			wsaaProcessCompleteFlag.set("Y");
			goTo(GotoLabel.exit5090);
		}
		/* ADD AGCMSQY-ANNPREM         TO WSAA-SQ2-TOT-PREM.            */
		if (isNE(agcmsqyIO.getPtdate(), ZERO)) {
			wsaaSq2TotPrem.add(agcmsqyIO.getAnnprem());
		}
		wsaaSqDataKey.set(agcmsqyIO.getDataKey());
		agcmsqyIO.setFunction(varcom.nextr);
		while ( !(isEQ(agcmsqyIO.getStatuz(), varcom.endp))) {
			seqnoLevelTot5100();
		}
		
	}

protected void seqLvlTot5030()
	{
		/* If the total premium for this sequence number is greater than   */
		/* the remaining premium increase then this set of dormant records */
		/* must be split into two sets.  Before that happens, resequence   */
		/* the later dormant records to make room for the new set.         */
		if (isGT(wsaaSq2TotPrem, wsaaAddPrem)) {
			increaseSplitReqd.setTrue();
			compute(wsaaSplitPlusOne, 0).set(add(1, wsaaSqSeqno));
			resequenceDormantRecs5600();
		}
		agcmsqyIO.setStatuz(varcom.oK);
		agcmsqyIO.setDataKey(wsaaSqDataKey);
		/* MOVE BEGNH                  TO AGCMSQY-FUNCTION.             */
		agcmsqyIO.setFunction(varcom.begn);
		agcmsqyIO.setFormat(agcmsqyrec);
		wsaaNewTotpremVf1.set(ZERO);
		while ( !(isEQ(agcmsqyIO.getStatuz(), varcom.endp))) {
			updateAgcm5200();
		}
		
		if (increaseSplitReqd.isTrue()) {
			wsaaProcessCompleteFlag.set("Y");
		}
	}

protected void seqnoLevelTot5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call5110();
				case nextAgcm5120: 
					nextAgcm5120();
				case exit5190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call5110()
	{
		SmartFileCode.execute(appVars, agcmsqyIO);
		if (isNE(agcmsqyIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqyIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqyIO.getParams());
			syserrrec.statuz.set(agcmsqyIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqyIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqyIO.getChdrnum())
		|| isNE(acomcalrec.covrLife, agcmsqyIO.getLife())
		|| isNE(acomcalrec.covrCoverage, agcmsqyIO.getCoverage())
		|| isNE(acomcalrec.covrRider, agcmsqyIO.getRider())
		|| isNE(acomcalrec.covrPlanSuffix, agcmsqyIO.getPlanSuffix())
		|| isNE(wsaaSqSeqno, agcmsqyIO.getSeqno())
		|| isEQ(agcmsqyIO.getStatuz(), varcom.endp)) {
			agcmsqyIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5190);
		}
		if (isEQ(agcmsqyIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.nextAgcm5120);
		}
		if (isEQ(agcmsqyIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.nextAgcm5120);
		}
		wsaaSq2TotPrem.add(agcmsqyIO.getAnnprem());
	}

protected void nextAgcm5120()
	{
		agcmsqyIO.setFunction(varcom.nextr);
	}

protected void updateAgcm5200()
	{
		call5210();
	}

protected void call5210()
	{
		SmartFileCode.execute(appVars, agcmsqyIO);
		if (isNE(agcmsqyIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqyIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqyIO.getParams());
			syserrrec.statuz.set(agcmsqyIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqyIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqyIO.getChdrnum())
		|| isNE(acomcalrec.covrLife, agcmsqyIO.getLife())
		|| isNE(acomcalrec.covrCoverage, agcmsqyIO.getCoverage())
		|| isNE(acomcalrec.covrRider, agcmsqyIO.getRider())
		|| isNE(acomcalrec.covrPlanSuffix, agcmsqyIO.getPlanSuffix())
		|| isNE(wsaaSqSeqno, agcmsqyIO.getSeqno())
		|| isEQ(agcmsqyIO.getStatuz(), varcom.endp)) {
			wsaaAddPrem.set(wsaaRunPrem);
			agcmsqyIO.setStatuz(varcom.endp);
			return ;
		}
		agcmsqyIO.setFunction(varcom.nextr);
		compute(wsaaSplitFactor, 9).setRounded(div(agcmsqyIO.getAnnprem(), wsaaSq2TotPrem));
		compute(wsaaCalcPrem, 9).setRounded(mult(wsaaAddPrem, wsaaSplitFactor));
		/* IF AGCMSQY-ANNPREM          NOT > WSAA-CALC-PREM             */
		if (noIncreaseSplitReqd.isTrue()) {
			validflagTo15300();
		}
		else {
			wsaaAnnprem.set(agcmsqyIO.getAnnprem());
			/*    SUBTRACT WSAA-CALC-PREM  FROM AGCMSQY-ANNPREM             */
			/*                             GIVING AGCMSQY-ANNPREM           */
			validflagTo25500();
			agcmsqyIO.setAnnprem(wsaaCalcPrem);
			validflagTo15400();
		}
		agcmsqyIO.setFunction(varcom.nextr);
	}

protected void validflagTo15300()
	{
		setupKey5310();
		rewriteVldfg15320();
	}

protected void setupKey5310()
	{
		/* Write a validflag '2' record prior to the changes.           */
		agcmsqyIO.setValidflag("2");
		agcmsqyIO.setCurrto(acomcalrec.payrPtdate);
		if (flexiblePremium.isTrue()) {
			agcmsqyIO.setCurrto(wsaaTargFrom);
		}
		agcmsqyIO.setFunction(varcom.writd);
		agcmsqyIO.setFormat(agcmsqyrec);
		SmartFileCode.execute(appVars, agcmsqyIO);
		if (isNE(agcmsqyIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agcmsqyIO.getStatuz());
			syserrrec.params.set(agcmsqyIO.getParams());
			systemError9000();
		}
		agcmsqyIO.setValidflag("1");
		agcmsqyIO.setCurrto(varcom.vrcmMaxDate);
		agcmsqyIO.setTermid(acomcalrec.termid);
		agcmsqyIO.setTransactionDate(acomcalrec.transactionDate);
		agcmsqyIO.setTransactionTime(acomcalrec.transactionTime);
		agcmsqyIO.setUser(acomcalrec.user);
		agcmsqyIO.setValidflag("1");
		agcmsqyIO.setTranno(acomcalrec.tranno);
		datcon3rec.intDate1.set(agcmsqyIO.getPtdate());
		datcon3rec.intDate2.set(agcmsqyIO.getEfdate());
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		datcon2rec.freqFactor.set(datcon3rec.freqFactor);
		datcon2rec.frequency.set("12");
		datcon2rec.intDate1.set(acomcalrec.payrPtdate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError9000();
		}
		agcmsqyIO.setEfdate(datcon2rec.intDate2);
		agcmsqyIO.setCurrfrom(acomcalrec.chdrCurrfrom);
		agcmsqyIO.setPtdate(acomcalrec.payrPtdate);
		/* Prior to the recalculation call, set up agent and annual     */
		/* premium fields on the AGCMSQN as these are used in the       */
		/* linkage for commission calculation.                          */
		agcmsqnIO.setAgntnum(agcmsqyIO.getAgntnum());
		agcmsqnIO.setAgentClass(agcmsqyIO.getAgentClass());
		agcmsqnIO.setAnnprem(agcmsqyIO.getAnnprem());
		if (flexiblePremium.isTrue()) {
			agcmsqnIO.setEfdate(wsaaTargFrom);
			agcmsqnIO.setCurrfrom(wsaaTargFrom);
		}
		/* Set up Dormant Flag*/
		agcmsqyIO.setFormat(agcmsqyrec);
		if (isEQ(agcmsqyIO.getOvrdcat(), "O")) {
			return ;
		}
		/* Call the commission calculation routine.  If the initial     */
		/* commission amount calculated is less than the earned         */
		/* commission on the AGCM, set the initial commission to the    */
		/* earned initial commission to stop initial earned commission  */
		/* from being clawed back.  Otherwise, move the calculated      */
		/* initial commission to the AGCM record.                       */
		initialCommCalc3600();
		if (isLT(comlinkrec.icommtot, agcmsqyIO.getComern())) {
			agcmsqyIO.setInitcom(agcmsqyIO.getComern());
		}
		else {
			agcmsqyIO.setInitcom(comlinkrec.icommtot);
		}
		/* Wsaa-new-totprem-vf1 is the running total for the premiums of*/
		/* the new valid flag 1 records (calculated for each seq. num.).*/
		wsaaNewTotpremVf1.add(agcmsqyIO.getAnnprem());
		compute(wsaaRunPrem, 2).set(sub(wsaaAddPrem, wsaaNewTotpremVf1));
	}

protected void rewriteVldfg15320()
	{
		/* We have replaced DELET + REWRITE for WRITD for updating      */
		/* to dormant flag 'Y'.                                         */
		/* We need to do so because we have replaced previous BEGNH     */
		/* for a BEGN.                                                  */
		/* MOVE DELET                  TO AGCMSQY-FUNCTION.             */
		/* MOVE WRITD                  TO AGCMSQY-FUNCTION.     <A05940>*/
		agcmsqyIO.setFunction(varcom.writr);
		agcmsqyIO.setDormantFlag("N");
		SmartFileCode.execute(appVars, agcmsqyIO);
		if (isNE(agcmsqyIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmsqyIO.getParams());
			syserrrec.statuz.set(agcmsqyIO.getStatuz());
			systemError9000();
		}
		/*EXIT*/
	}

protected void validflagTo15400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupKey5400();
				case writePara5470: 
					writePara5470();
				case rewriteVldfg15480: 
					rewriteVldfg15480();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupKey5400()
	{
		agcmsqyIO.setTermid(acomcalrec.termid);
		agcmsqyIO.setTransactionDate(acomcalrec.transactionDate);
		agcmsqyIO.setTransactionTime(acomcalrec.transactionTime);
		agcmsqyIO.setUser(acomcalrec.user);
		agcmsqyIO.setValidflag("1");
		agcmsqyIO.setTranno(acomcalrec.tranno);
		datcon3rec.intDate1.set(agcmsqyIO.getPtdate());
		datcon3rec.intDate2.set(agcmsqyIO.getEfdate());
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		datcon2rec.freqFactor.set(datcon3rec.freqFactor);
		datcon2rec.frequency.set("12");
		datcon2rec.intDate1.set(acomcalrec.payrPtdate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError9000();
		}
		agcmsqyIO.setEfdate(datcon2rec.intDate2);
		agcmsqyIO.setCurrfrom(acomcalrec.chdrCurrfrom);
		agcmsqyIO.setPtdate(acomcalrec.payrPtdate);
		agcmsqnIO.setAgntnum(agcmsqyIO.getAgntnum());
		agcmsqnIO.setAgentClass(agcmsqyIO.getAgentClass());
		agcmsqnIO.setAnnprem(agcmsqyIO.getAnnprem());
		if (flexiblePremium.isTrue()) {
			agcmsqyIO.setEfdate(wsaaTargFrom);
			agcmsqyIO.setCurrfrom(wsaaTargFrom);
		}
		/* Set up Dormant Flag*/
		agcmsqyIO.setDormantFlag("N");
		if (isEQ(agcmsqyIO.getOvrdcat(), "O")) {
			setPrecision(agcmsqyIO.getInitcom(), 2);
			agcmsqyIO.setInitcom(sub(wsaaOldInitcom, agcmsqyIO.getInitcom()));
			goTo(GotoLabel.writePara5470);
		}
		initialCommCalc3600();
		agcmsqyIO.setInitcom(comlinkrec.icommtot);
	}

protected void writePara5470()
	{
		/* We change REWRITE for WRITD to prevent STATUS 21             */
		/* when trying to update the record after changing one          */
		/* of the keys stored (AGCMSGN-DORMANT-FLAG)                    */
		/* MOVE REWRT                  TO AGCMSQY-FUNCTION.             */
		/* MOVE WRITD                  TO AGCMSQY-FUNCTION.     <A05940>*/
		agcmsqyIO.setFunction(varcom.writr);
		agcmsqyIO.setFormat(agcmsqyrec);
		if (isEQ(agcmsqyIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.rewriteVldfg15480);
		}
		/* Wsaa-new-totprem-vf1 is the running total for the premiums of*/
		/* the new valid flag 1 records (calculated for each seq. num.).*/
		wsaaNewTotpremVf1.add(agcmsqyIO.getAnnprem());
		compute(wsaaRunPrem, 2).set(sub(wsaaAddPrem, wsaaNewTotpremVf1));
	}

protected void rewriteVldfg15480()
	{
		SmartFileCode.execute(appVars, agcmsqyIO);
		if (isNE(agcmsqyIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmsqyIO.getParams());
			syserrrec.statuz.set(agcmsqyIO.getStatuz());
			systemError9000();
		}
		/*EXIT*/
	}

protected void validflagTo25500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupKey5510();
				case writeVldfg25520: 
					writeVldfg25520();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupKey5510()
	{
		/* Write a validflag '2' record prior to the changes.           */
		agcmsqyIO.setValidflag("2");
		agcmsqyIO.setCurrto(acomcalrec.payrPtdate);
		if (flexiblePremium.isTrue()) {
			agcmsqyIO.setCurrto(wsaaTargFrom);
		}
		agcmsqyIO.setFunction(varcom.writd);
		agcmsqyIO.setFormat(agcmsqyrec);
		SmartFileCode.execute(appVars, agcmsqyIO);
		if (isNE(agcmsqyIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agcmsqyIO.getStatuz());
			syserrrec.params.set(agcmsqyIO.getParams());
			systemError9000();
		}
		setPrecision(agcmsqyIO.getAnnprem(), 2);
		agcmsqyIO.setAnnprem(sub(agcmsqyIO.getAnnprem(), wsaaCalcPrem));
		agcmsqyIO.setCurrto(varcom.vrcmMaxDate);
		agcmsqyIO.setValidflag("1");
		/* Set up Dormant Flag*/
		agcmsqyIO.setDormantFlag("Y");
		/* Do not add seq. num here as VALID-FLAG-TO-2 is completed first*/
		/* and then rewriting the the valid flag 1 records(these sould hav*/
		/* have the same seq. num. as before).  The new valid flag 2*/
		/* records which are being created here have there seq. num*/
		/* increased.*/
		agcmsqyIO.setTranno(acomcalrec.tranno);
		agcmsqnIO.setAgntnum(agcmsqyIO.getAgntnum());
		agcmsqnIO.setAgentClass(agcmsqyIO.getAgentClass());
		agcmsqnIO.setAnnprem(agcmsqyIO.getAnnprem());
		wsaaOldInitcom.set(agcmsqyIO.getInitcom());
		compute(wsaaSplitFactor2, 9).setRounded(div(agcmsqyIO.getAnnprem(), wsaaAnnprem));
		if (isEQ(agcmsqyIO.getOvrdcat(), "O")) {
			setPrecision(agcmsqyIO.getInitcom(), 9);
			agcmsqyIO.setInitcom(mult(agcmsqyIO.getInitcom(), wsaaSplitFactor2), true);
			goTo(GotoLabel.writeVldfg25520);
		}
		initialCommCalc3600();
		agcmsqyIO.setInitcom(comlinkrec.icommtot);
	}

protected void writeVldfg25520()
	{
		agcmsqyIO.setFormat(agcmsqyrec);
		agcmsqyIO.setFunction(varcom.writr);
		agcmdmyIO.setFunction(agcmsqyIO.getFunction());
		agcmdmyIO.setStatuz(agcmsqyIO.getStatuz());
		agcmdmyIO.setGenDate(agcmsqyIO.getGenDate());
		agcmdmyIO.setGenTime(agcmsqyIO.getGenTime());
		agcmdmyIO.setVn(agcmsqyIO.getVn());
		agcmdmyIO.setFormat(agcmdmyrec);
		agcmdmyIO.setChdrcoy(agcmsqyIO.getChdrcoy());
		agcmdmyIO.setChdrnum(agcmsqyIO.getChdrnum());
		agcmdmyIO.setLife(agcmsqyIO.getLife());
		agcmdmyIO.setCoverage(agcmsqyIO.getCoverage());
		agcmdmyIO.setRider(agcmsqyIO.getRider());
		agcmdmyIO.setPlanSuffix(agcmsqyIO.getPlanSuffix());
		agcmdmyIO.setSeqno(agcmsqyIO.getSeqno());
		setPrecision(agcmdmyIO.getSeqno(), 0);
		agcmdmyIO.setSeqno(add(agcmdmyIO.getSeqno(), 1));
		agcmdmyIO.setTranno(agcmsqyIO.getTranno());
		agcmdmyIO.setAgntnum(agcmsqyIO.getAgntnum());
		agcmdmyIO.setEfdate(agcmsqyIO.getEfdate());
		agcmdmyIO.setAnnprem(agcmsqyIO.getAnnprem());
		agcmdmyIO.setBasicCommMeth(agcmsqyIO.getBasicCommMeth());
		agcmdmyIO.setInitcom(agcmsqyIO.getInitcom());
		agcmdmyIO.setBascpy(agcmsqyIO.getBascpy());
		agcmdmyIO.setCompay(agcmsqyIO.getCompay());
		agcmdmyIO.setComern(agcmsqyIO.getComern());
		agcmdmyIO.setSrvcpy(agcmsqyIO.getSrvcpy());
		agcmdmyIO.setScmdue(agcmsqyIO.getScmdue());
		agcmdmyIO.setScmearn(agcmsqyIO.getScmearn());
		agcmdmyIO.setRnwcpy(agcmsqyIO.getRnwcpy());
		agcmdmyIO.setRnlcdue(agcmsqyIO.getRnlcdue());
		agcmdmyIO.setRnlcearn(agcmsqyIO.getRnlcearn());
		agcmdmyIO.setAgentClass(agcmsqyIO.getAgentClass());
		agcmdmyIO.setTermid(agcmsqyIO.getTermid());
		agcmdmyIO.setTransactionDate(agcmsqyIO.getTransactionDate());
		agcmdmyIO.setTransactionTime(agcmsqyIO.getTransactionTime());
		agcmdmyIO.setUser(agcmsqyIO.getUser());
		agcmdmyIO.setValidflag(agcmsqyIO.getValidflag());
		agcmdmyIO.setCurrfrom(agcmsqyIO.getCurrfrom());
		agcmdmyIO.setCurrto(agcmsqyIO.getCurrto());
		agcmdmyIO.setPtdate(agcmsqyIO.getPtdate());
		agcmdmyIO.setOvrdcat(agcmsqyIO.getOvrdcat());
		agcmdmyIO.setCedagent(agcmsqyIO.getCedagent());
		/* Set up Dormant Flag*/
		agcmdmyIO.setDormantFlag(agcmsqyIO.getDormantFlag());
		SmartFileCode.execute(appVars, agcmdmyIO);
		if (isNE(agcmdmyIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdmyIO.getParams());
			syserrrec.statuz.set(agcmdmyIO.getStatuz());
			systemError9000();
		}
	}

protected void resequenceDormantRecs5600()
	{
		setupKey5610();
		readAgentComm5620();
	}

protected void setupKey5610()
	{
		agcmdmyIO.setDataArea(SPACES);
		agcmdmyIO.setStatuz(SPACES);
		agcmdmyIO.setChdrcoy(acomcalrec.company);
		agcmdmyIO.setChdrnum(acomcalrec.chdrnum);
		agcmdmyIO.setLife(acomcalrec.covrLife);
		agcmdmyIO.setCoverage(acomcalrec.covrCoverage);
		agcmdmyIO.setRider(acomcalrec.covrRider);
		agcmdmyIO.setPlanSuffix(acomcalrec.covrPlanSuffix);
		agcmdmyIO.setSeqno(99);
		agcmdmyIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, agcmdmyIO);
		if (isNE(agcmdmyIO.getStatuz(), varcom.oK)
		&& isNE(agcmdmyIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmdmyIO.getParams());
			syserrrec.statuz.set(agcmdmyIO.getStatuz());
			systemError9000();
		}
	}

protected void readAgentComm5620()
	{
		while ( !(isLT(agcmdmyIO.getSeqno(), wsaaSplitPlusOne)
		|| isNE(acomcalrec.company, agcmdmyIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmdmyIO.getChdrnum())
		|| isNE(acomcalrec.covtLife, agcmdmyIO.getLife())
		|| isNE(acomcalrec.covtCoverage, agcmdmyIO.getCoverage())
		|| isNE(acomcalrec.covtRider, agcmdmyIO.getRider())
		|| isNE(acomcalrec.covtPlanSuffix, agcmdmyIO.getPlanSuffix())
		|| isEQ(agcmdmyIO.getStatuz(), varcom.endp))) {
			resequence5700();
		}
		
		/*EXIT*/
	}

protected void resequence5700()
	{
		call5710();
		next5720();
	}

protected void call5710()
	{
		agcmdmyIO.setValidflag("2");
		agcmdmyIO.setCurrto(acomcalrec.payrPtdate);
		if (flexiblePremium.isTrue()) {
			agcmdmyIO.setCurrto(wsaaTargFrom);
		}
		agcmdmyIO.setFunction(varcom.writd);
		agcmdmyIO.setFormat(agcmdmyrec);
		SmartFileCode.execute(appVars, agcmdmyIO);
		if (isNE(agcmdmyIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agcmdmyIO.getStatuz());
			syserrrec.params.set(agcmdmyIO.getParams());
			systemError9000();
		}
		agcmdmyIO.setValidflag("1");
		agcmdmyIO.setCurrto(varcom.vrcmMaxDate);
		setPrecision(agcmdmyIO.getSeqno(), 0);
		agcmdmyIO.setSeqno(add(agcmdmyIO.getSeqno(), 1));
		agcmdmyIO.setTermid(acomcalrec.termid);
		agcmdmyIO.setTransactionDate(acomcalrec.transactionDate);
		agcmdmyIO.setTransactionTime(acomcalrec.transactionTime);
		agcmdmyIO.setUser(acomcalrec.user);
		agcmdmyIO.setTranno(acomcalrec.tranno);
		/* MOVE WRITD                  TO AGCMDMY-FUNCTION      <A05940>*/
		agcmdmyIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmdmyIO);
		if (isNE(agcmdmyIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdmyIO.getParams());
			syserrrec.statuz.set(agcmdmyIO.getStatuz());
			systemError9000();
		}
	}

protected void next5720()
	{
		agcmdmyIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmdmyIO);
		if (isNE(agcmdmyIO.getStatuz(), varcom.oK)
		&& isNE(agcmdmyIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmdmyIO.getParams());
			syserrrec.statuz.set(agcmdmyIO.getStatuz());
			systemError9000();
		}
		/*EXIT*/
	}

protected void increasePrem6000()
	{
		setupKey6010();
		callCommSplit6020();
		newAgcmsq1Level6030();
	}

protected void setupKey6010()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setChdrcoy(acomcalrec.company);
		pcdtmjaIO.setChdrnum(acomcalrec.chdrnum);
		pcdtmjaIO.setPlanSuffix(acomcalrec.covtPlanSuffix);
		pcdtmjaIO.setLife(acomcalrec.covtLife);
		pcdtmjaIO.setCoverage(acomcalrec.covtCoverage);
		pcdtmjaIO.setRider(acomcalrec.covtRider);
		pcdtmjaIO.setTranno(acomcalrec.tranno);
		pcdtmjaIO.setFunction(varcom.readr);
	}

protected void callCommSplit6020()
	{
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			syserrrec.statuz.set(pcdtmjaIO.getStatuz());
			databaseError9500();
		}
	}

	/**
	* <pre>
	****     PERFORM 9000-SYSTEM-ERROR.
	* </pre>
	*/
protected void newAgcmsq1Level6030()
	{
		agcmsqnIO.setDataArea(SPACES);
		agcmsqnIO.setChdrcoy(acomcalrec.company);
		agcmsqnIO.setChdrnum(acomcalrec.chdrnum);
		agcmsqnIO.setLife(acomcalrec.covtLife);
		agcmsqnIO.setCoverage(acomcalrec.covtCoverage);
		agcmsqnIO.setRider(acomcalrec.covtRider);
		agcmsqnIO.setPlanSuffix(acomcalrec.covtPlanSuffix);
		agcmsqnIO.setSeqno(99);
		agcmsqnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmsqnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmsqnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, agcmsqnIO);
		if (isNE(agcmsqnIO.getStatuz(), varcom.oK)
		&& isNE(agcmsqnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmsqnIO.getParams());
			syserrrec.statuz.set(agcmsqnIO.getStatuz());
			systemError9000();
		}
		if (isNE(acomcalrec.company, agcmsqnIO.getChdrcoy())
		|| isNE(acomcalrec.chdrnum, agcmsqnIO.getChdrnum())
		|| isNE(acomcalrec.covtLife, agcmsqnIO.getLife())
		|| isNE(acomcalrec.covtCoverage, agcmsqnIO.getCoverage())
		|| isNE(acomcalrec.covtRider, agcmsqnIO.getRider())
		|| isNE(acomcalrec.covtPlanSuffix, agcmsqnIO.getPlanSuffix())) {
			agcmsqnIO.setChdrcoy(acomcalrec.company);
			agcmsqnIO.setChdrnum(acomcalrec.chdrnum);
			agcmsqnIO.setLife(acomcalrec.covtLife);
			agcmsqnIO.setCoverage(acomcalrec.covtCoverage);
			agcmsqnIO.setRider(acomcalrec.covtRider);
			agcmsqnIO.setPlanSuffix(acomcalrec.covtPlanSuffix);
			agcmsqnIO.setSeqno(0);
		}
		setPrecision(agcmsqnIO.getSeqno(), 0);
		agcmsqnIO.setSeqno(add(agcmsqnIO.getSeqno(), 1));
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.oK)) {
			wsaaSub.set(1);
			wsaaEndProcessFlag.set("N");
			while ( !(processExit.isTrue()
			|| isGT(wsaaSub, 10))) {
				pcdtSplit6100();
			}
			
		}
	}

protected void pcdtSplit6100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call6110();
				case pass6120: 
					pass6120();
					a120WriteVldfg1();
				case exit6190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call6110()
	{
		if (isEQ(pcdtmjaIO.getAgntnum(wsaaSub), SPACES)) {
			wsaaEndProcessFlag.set("Y");
			goTo(GotoLabel.exit6190);
		}
		aglfmjaIO.setAgntcoy(pcdtmjaIO.getChdrcoy());
		aglfmjaIO.setAgntnum(pcdtmjaIO.getAgntnum(wsaaSub));
		aglfmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfmjaIO);
		if (isNE(aglfmjaIO.getStatuz(), varcom.oK)
		&& isNE(aglfmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglfmjaIO.getParams());
			syserrrec.statuz.set(aglfmjaIO.getStatuz());
			systemError9000();
		}
		if (isEQ(aglfmjaIO.getStatuz(), varcom.mrnf)) {
			agcmsqnIO.setAgentClass(SPACES);
			aglfmjaIO.setReportag(SPACES);
		}
		agcmsqnIO.setBasicCommMeth(acomcalrec.basicCommMeth);
		/* Dont use Agent methods for flexible premium contracts  *        */
		/* Also, if no AGLF record found use values passed        *        */
		if (flexiblePremium.isTrue()
		|| isEQ(aglfmjaIO.getStatuz(), varcom.mrnf)) {
			agcmsqnIO.setBascpy(acomcalrec.bascpy);
			agcmsqnIO.setSrvcpy(acomcalrec.srvcpy);
			agcmsqnIO.setRnwcpy(acomcalrec.rnwcpy);
			goTo(GotoLabel.pass6120);
		}
		if (isEQ(acomcalrec.bascpy, SPACES)) {
			agcmsqnIO.setBascpy(aglfmjaIO.getBcmtab());
		}
		else {
			agcmsqnIO.setBascpy(acomcalrec.bascpy);
		}
		if (isNE(aglfmjaIO.getScmtab(), SPACES)) {
			agcmsqnIO.setSrvcpy(aglfmjaIO.getScmtab());
		}
		else {
			agcmsqnIO.setSrvcpy(acomcalrec.srvcpy);
		}
		if (isNE(aglfmjaIO.getRcmtab(), SPACES)) {
			agcmsqnIO.setRnwcpy(aglfmjaIO.getRcmtab());
		}
		else {
			agcmsqnIO.setRnwcpy(acomcalrec.rnwcpy);
		}
	}

	/**
	* <pre>
	*    MOVE AGLFMJA-AGENT-CLASS    TO AGCMSQN-AGENT-CLASS.  <D9607> 
	* </pre>
	*/
protected void pass6120()
	{
		if (isNE(aglfmjaIO.getStatuz(), varcom.mrnf)) {
			agcmsqnIO.setAgentClass(aglfmjaIO.getAgentClass());
		}
		setPrecision(agcmsqnIO.getAnnprem(), 2);
		agcmsqnIO.setAnnprem((div((mult(wsaaAddPrem, pcdtmjaIO.getSplitc(wsaaSub))), 100)));
		zrdecplrec.amountIn.set(agcmsqnIO.getAnnprem());
		a000CallRounding();
		agcmsqnIO.setAnnprem(zrdecplrec.amountOut);
		agcmsqnIO.setCedagent(SPACES);
		agcmsqnIO.setOvrdcat("B");
		agcmsqnIO.setAgntnum(pcdtmjaIO.getAgntnum(wsaaSub));
		agcmsqnIO.setValidflag("1");
		agcmsqnIO.setTranno(acomcalrec.tranno);
		/* MOVE ACOM-PAYR-PTDATE       TO AGCMSQN-EFDATE.               */
		agcmsqnIO.setCurrto(varcom.vrcmMaxDate);
		/* MOVE ACOM-PAYR-PTDATE       TO AGCMSQN-CURRFROM.             */
		if (isEQ(acomcalrec.singlePremInd, "Y")) {
			agcmsqnIO.setCurrfrom(acomcalrec.chdrCurrfrom);
			agcmsqnIO.setEfdate(acomcalrec.chdrCurrfrom);
		}
		else {
			/*     MOVE ACOM-PAYR-PTDATE   TO AGCMSQN-CURRFROM      <D9604> */
			/*                                AGCMSQN-EFDATE        <D9604> */
			if (!flexiblePremium.isTrue()) {
				agcmsqnIO.setCurrfrom(acomcalrec.payrPtdate);
				agcmsqnIO.setEfdate(acomcalrec.payrPtdate);
			}
			else {
				agcmsqnIO.setEfdate(wsaaTargFrom);
				agcmsqnIO.setCurrfrom(wsaaTargFrom);
			}
		}
		agcmsqnIO.setCompay(ZERO);
		agcmsqnIO.setComern(ZERO);
		agcmsqnIO.setScmdue(ZERO);
		agcmsqnIO.setScmearn(ZERO);
		agcmsqnIO.setRnlcdue(ZERO);
		agcmsqnIO.setTransactionDate(ZERO);
		agcmsqnIO.setTransactionTime(ZERO);
		agcmsqnIO.setTermid(ZERO);
		agcmsqnIO.setUser(ZERO);
		agcmsqnIO.setRnlcearn(ZERO);
		wsaaCommPaid.set(0);
		agcmsqnIO.setPtdate(acomcalrec.payrPtdate);
		/* Set up Dormant Flag*/
		agcmsqnIO.setDormantFlag("N");
		initialCommCalc3600();
		agcmsqnIO.setCompay(comlinkrec.payamnt);
		agcmsqnIO.setComern(comlinkrec.erndamt);
		agcmsqnIO.setInitcom(comlinkrec.icommtot);
		//PINNACLE-1380 START
		if(gstOnCommFlag) {
		agcmsqnIO.setInitCommGst(comlinkrec.gstAmount);
		}
		//PINNACLE-1380 END
		compute(wsaaCommPaid, 3).setRounded(sub(comlinkrec.payamnt, comlinkrec.erndamt));
		agcmsqnIO.setFormat(agcmsqnrec);
		agcmsqnIO.setFunction(varcom.writr);
	}

protected void a120WriteVldfg1()
	{
		agcmdmnIO.setFunction(agcmsqnIO.getFunction());
		agcmdmnIO.setStatuz(agcmsqnIO.getStatuz());
		agcmdmnIO.setGenDate(agcmsqnIO.getGenDate());
		agcmdmnIO.setGenTime(agcmsqnIO.getGenTime());
		agcmdmnIO.setVn(agcmsqnIO.getVn());
		agcmdmnIO.setFormat(agcmdmnrec);
		agcmdmnIO.setChdrcoy(agcmsqnIO.getChdrcoy());
		agcmdmnIO.setChdrnum(agcmsqnIO.getChdrnum());
		agcmdmnIO.setLife(agcmsqnIO.getLife());
		agcmdmnIO.setCoverage(agcmsqnIO.getCoverage());
		agcmdmnIO.setRider(agcmsqnIO.getRider());
		agcmdmnIO.setPlanSuffix(agcmsqnIO.getPlanSuffix());
		agcmdmnIO.setSeqno(agcmsqnIO.getSeqno());
		agcmdmnIO.setTranno(agcmsqnIO.getTranno());
		agcmdmnIO.setAgntnum(agcmsqnIO.getAgntnum());
		agcmdmnIO.setEfdate(agcmsqnIO.getEfdate());
		agcmdmnIO.setAnnprem(agcmsqnIO.getAnnprem());
		agcmdmnIO.setBasicCommMeth(agcmsqnIO.getBasicCommMeth());
		agcmdmnIO.setInitcom(agcmsqnIO.getInitcom());
		agcmdmnIO.setBascpy(agcmsqnIO.getBascpy());
		agcmdmnIO.setCompay(agcmsqnIO.getCompay());
		agcmdmnIO.setComern(agcmsqnIO.getComern());
		agcmdmnIO.setSrvcpy(agcmsqnIO.getSrvcpy());
		agcmdmnIO.setScmdue(agcmsqnIO.getScmdue());
		agcmdmnIO.setScmearn(agcmsqnIO.getScmearn());
		agcmdmnIO.setRnwcpy(agcmsqnIO.getRnwcpy());
		agcmdmnIO.setRnlcdue(agcmsqnIO.getRnlcdue());
		agcmdmnIO.setRnlcearn(agcmsqnIO.getRnlcearn());
		agcmdmnIO.setAgentClass(agcmsqnIO.getAgentClass());
		agcmdmnIO.setTermid(acomcalrec.termid);
		agcmdmnIO.setTransactionDate(acomcalrec.transactionDate);
		agcmdmnIO.setTransactionTime(acomcalrec.transactionTime);
		agcmdmnIO.setUser(acomcalrec.user);
		agcmdmnIO.setValidflag(agcmsqnIO.getValidflag());
		agcmdmnIO.setCurrfrom(agcmsqnIO.getCurrfrom());
		agcmdmnIO.setCurrto(agcmsqnIO.getCurrto());
		agcmdmnIO.setPtdate(agcmsqnIO.getPtdate());
		agcmdmnIO.setCedagent(agcmsqnIO.getCedagent());
		agcmdmnIO.setOvrdcat(agcmsqnIO.getOvrdcat());
		/* Set up Dormant Flag*/
		agcmdmnIO.setDormantFlag(agcmsqnIO.getDormantFlag());
		//PINNACLE-1380 START
		if(gstOnCommFlag) {
			agcmdmnIO.setInitCommGst(agcmsqnIO.getInitCommGst());
		}
		//PINNACLE-1380 END
		SmartFileCode.execute(appVars, agcmdmnIO);
		if (isNE(agcmdmnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdmnIO.getParams());
			syserrrec.statuz.set(agcmdmnIO.getStatuz());
			systemError9000();
		}
		/* post inital paid for the agcm records created*/
		createCommonAcmv3700();
		if (isNE(agcmdmnIO.getCompay(), ZERO)) {
			lifacmvrec.origamt.set(agcmdmnIO.getCompay());
			/*       MOVE ACOM-CHDRNUM           TO LIFA-RLDGACCT              */
			lifacmvrec.rldgacct.set(agcmdmnIO.getAgntnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
			/* if this is a single premium contract, use the single         */
			/* premium account set in T5645                                 */
			if (isEQ(acomcalrec.singlePremInd, "Y")) {
				lifacmvrec.sacscode.set(wsaaT5645Sacscode[3]);
				lifacmvrec.sacstyp.set(wsaaT5645Sacstype[3]);
				lifacmvrec.glcode.set(wsaaT5645Glmap[3]);
				lifacmvrec.glsign.set(wsaaT5645Sign[3]);
				lifacmvrec.contot.set(wsaaT5645Cnttot[3]);
			}
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			/*       PERFORM 4000-WRITE-ACMV.                                  */
			writeAcmv4000();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(lifacmvrec.origamt);
				zorlnkrec.clawback.set("Y");
				callZorcompy6300();
			}
		}
		/* post inital earned for the agcm records created*/
		/* IF AGCMDMN-COMERN              NOT = ZEROES                  */
		/*    MOVE AGCMDMN-COMERN         TO LIFA-ORIGAMT               */
		/*    MOVE AGCMDMN-CHDRNUM        TO WSAA-RLDG-CHDRNUM          */
		/*    MOVE AGCMDMN-LIFE           TO WSAA-RLDG-LIFE             */
		/*    MOVE AGCMDMN-COVERAGE       TO WSAA-RLDG-COVERAGE         */
		/*    MOVE AGCMDMN-RIDER          TO WSAA-RLDG-RIDER            */
		/*    MOVE AGCMDMN-PLAN-SUFFIX    TO WSAA-PLAN                  */
		/*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX      */
		/*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT              */
		/*    MOVE ACOM-CNTTYPE           TO LIFA-SUBSTITUTE-CODE(1)    */
		/*    MOVE ACOM-COVR-CRTABLE      TO LIFA-SUBSTITUTE-CODE(6)    */
		/*    MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE              */
		/*    MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP               */
		/*    MOVE T5645-GLMAP-04         TO LIFA-GLCODE                */
		/*    MOVE T5645-SIGN-04          TO LIFA-GLSIGN                */
		/*    MOVE T5645-CNTTOT-04        TO LIFA-CONTOT                */
		/*    PERFORM 4000-WRITE-ACMV.                                  */
		/*  A different posting will be made dependent upon whether     */
		/*  This contract type uses Component or Contract Level         */
		/*  Accounting.                                                 */
		if (isNE(agcmdmnIO.getComern(), ZERO)) {
			if (isEQ(acomcalrec.comlvlacc, "Y")) {
				wsaaRldgChdrnum.set(agcmdmnIO.getChdrnum());
				wsaaRldgLife.set(agcmdmnIO.getLife());
				wsaaRldgCoverage.set(agcmdmnIO.getCoverage());
				wsaaRldgRider.set(agcmdmnIO.getRider());
				wsaaPlan.set(agcmdmnIO.getPlanSuffix());
				wsaaRldgPlanSuffix.set(wsaaPlansuff);
				lifacmvrec.rldgacct.set(wsaaRldgacct);
				lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
				lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
				lifacmvrec.sacscode.set(t5645rec.sacscode13);
				lifacmvrec.sacstyp.set(t5645rec.sacstype13);
				lifacmvrec.glcode.set(t5645rec.glmap13);
				lifacmvrec.glsign.set(t5645rec.sign13);
				lifacmvrec.contot.set(t5645rec.cnttot13);
			}
			else {
				lifacmvrec.sacscode.set(t5645rec.sacscode04);
				lifacmvrec.sacstyp.set(t5645rec.sacstype04);
				lifacmvrec.glcode.set(t5645rec.glmap04);
				lifacmvrec.glsign.set(t5645rec.sign04);
				lifacmvrec.contot.set(t5645rec.cnttot04);
				lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
				lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
			lifacmvrec.origamt.set(agcmdmnIO.getComern());
			writeAcmv4000();
		}
		/* IF WSAA-COMM-PAID              NOT = ZEROES                  */
		/*    MOVE WSAA-COMM-PAID         TO LIFA-ORIGAMT               */
		/*    MOVE AGCMDMN-CHDRNUM        TO WSAA-RLDG-CHDRNUM          */
		/*    MOVE AGCMDMN-LIFE           TO WSAA-RLDG-LIFE             */
		/*    MOVE AGCMDMN-COVERAGE       TO WSAA-RLDG-COVERAGE         */
		/*    MOVE AGCMDMN-RIDER          TO WSAA-RLDG-RIDER            */
		/*    MOVE AGCMDMN-PLAN-SUFFIX    TO WSAA-PLAN                  */
		/*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX      */
		/*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT              */
		/*    MOVE ACOM-CNTTYPE           TO LIFA-SUBSTITUTE-CODE(1)    */
		/*    MOVE ACOM-COVR-CRTABLE      TO LIFA-SUBSTITUTE-CODE(6)    */
		/*    MOVE T5645-SACSCODE-05      TO LIFA-SACSCODE              */
		/*    MOVE T5645-SACSTYPE-05      TO LIFA-SACSTYP               */
		/*    MOVE T5645-GLMAP-05         TO LIFA-GLCODE                */
		/*    MOVE T5645-SIGN-05          TO LIFA-GLSIGN                */
		/*    MOVE T5645-CNTTOT-05        TO LIFA-CONTOT                */
		/*    PERFORM 4000-WRITE-ACMV.                                  */
		/*  A different posting will be made dependent upon whether     */
		/*  This contract type uses Component or Contract Level         */
		/*  Accounting.                                                 */
		if (isNE(wsaaCommPaid, ZERO)) {
			if (isEQ(acomcalrec.comlvlacc, "Y")) {
				wsaaRldgChdrnum.set(agcmdmnIO.getChdrnum());
				wsaaRldgLife.set(agcmdmnIO.getLife());
				wsaaRldgCoverage.set(agcmdmnIO.getCoverage());
				wsaaRldgRider.set(agcmdmnIO.getRider());
				wsaaPlan.set(agcmdmnIO.getPlanSuffix());
				wsaaRldgPlanSuffix.set(wsaaPlansuff);
				lifacmvrec.rldgacct.set(wsaaRldgacct);
				lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
				lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
				lifacmvrec.sacscode.set(t5645rec.sacscode14);
				lifacmvrec.sacstyp.set(t5645rec.sacstype14);
				lifacmvrec.glcode.set(t5645rec.glmap14);
				lifacmvrec.glsign.set(t5645rec.sign14);
				lifacmvrec.contot.set(t5645rec.cnttot14);
			}
			else {
				lifacmvrec.sacscode.set(t5645rec.sacscode05);
				lifacmvrec.sacstyp.set(t5645rec.sacstype05);
				lifacmvrec.glcode.set(t5645rec.glmap05);
				lifacmvrec.glsign.set(t5645rec.sign05);
				lifacmvrec.contot.set(t5645rec.cnttot05);
				lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
				lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
			lifacmvrec.origamt.set(wsaaCommPaid);
			writeAcmv4000();
		}
		/* overide commission processing (addition of agcm records).*/
		wsaaIfOvrdCommEnd.set("N");
		if (isNE(aglfmjaIO.getReportag(), SPACES)) {
			wsaaFirstTime = "Y";
			while ( !(wsaaEndOvrdComm.isTrue())) {
				overrideComm6200();
			}
			
		}
		wsaaSub.add(1);
	}

protected void overrideComm6200()
	{
		try {
			para6200();
			writeVldfg16220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para6200()
	{
		if (isEQ(wsaaIfOvrdCommEnd, "Y")) {
			goTo(GotoLabel.exit6290);
		}
		wsaaCommPaid.set(0);
		agcmsqnIO.setCedagent(agcmsqnIO.getAgntnum());
		comlinkrec.agent.set(aglfmjaIO.getReportag());
		agcmsqnIO.setAgntnum(aglfmjaIO.getReportag());
		initialCommCalc3600();
		if (isEQ(wsaaFirstTime, "N")) {
			comlinkrec.icommtot.set(wsaaIcommtot);
			comlinkrec.payamnt.set(wsaaCompay);
			comlinkrec.erndamt.set(wsaaComern);
		}
		setPrecision(agcmsqnIO.getInitcom(), 3);
		agcmsqnIO.setInitcom(mult(comlinkrec.icommtot, (div(aglfmjaIO.getOvcpc(), 100))), true);
		zrdecplrec.amountIn.set(agcmsqnIO.getInitcom());
		a000CallRounding();
		agcmsqnIO.setInitcom(zrdecplrec.amountOut);
		/*  Calculate override commission paid and earned by dividing   */
		/*  by override percentage for this agent.                      */
		setPrecision(agcmsqnIO.getCompay(), 3);
		agcmsqnIO.setCompay(mult(comlinkrec.payamnt, (div(aglfmjaIO.getOvcpc(), 100))), true);
		setPrecision(agcmsqnIO.getComern(), 3);
		agcmsqnIO.setComern(mult(comlinkrec.erndamt, (div(aglfmjaIO.getOvcpc(), 100))), true);
		wsaaIcommtot.set(agcmsqnIO.getInitcom());
		wsaaCompay.set(agcmsqnIO.getCompay());
		wsaaComern.set(agcmsqnIO.getComern());
		wsaaFirstTime = "N";
		zrdecplrec.amountIn.set(agcmsqnIO.getCompay());
		a000CallRounding();
		agcmsqnIO.setCompay(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(agcmsqnIO.getComern());
		a000CallRounding();
		agcmsqnIO.setComern(zrdecplrec.amountOut);
		/* MOVE CLNK-PAYAMNT              TO AGCMSQN-COMPAY.            */
		/* MOVE CLNK-ERNDAMT              TO AGCMSQN-COMERN.            */
		compute(wsaaCommPaid, 3).setRounded(sub(comlinkrec.payamnt, comlinkrec.erndamt));
		agcmsqnIO.setOvrdcat("O");
		agcmsqnIO.setBasicCommMeth(acomcalrec.basicCommMeth);
		if (isEQ(acomcalrec.bascpy, SPACES)) {
			agcmsqnIO.setBascpy(aglfmjaIO.getBcmtab());
		}
		else {
			agcmsqnIO.setBascpy(acomcalrec.bascpy);
		}
		agcmsqnIO.setSrvcpy(SPACES);
		agcmsqnIO.setRnwcpy(SPACES);
		setPrecision(agcmsqnIO.getAnnprem(), 2);
		agcmsqnIO.setAnnprem((div((mult(wsaaAddPrem, pcdtmjaIO.getSplitc(wsaaSub))), 100)));
		zrdecplrec.amountIn.set(agcmsqnIO.getAnnprem());
		a000CallRounding();
		agcmsqnIO.setAnnprem(zrdecplrec.amountOut);
		agcmsqnIO.setValidflag("1");
		agcmsqnIO.setTranno(acomcalrec.tranno);
		/* MOVE ACOM-PAYR-PTDATE       TO AGCMSQN-EFDATE.               */
		agcmsqnIO.setCurrto(varcom.vrcmMaxDate);
		/* MOVE ACOM-PAYR-PTDATE       TO AGCMSQN-CURRFROM.             */
		if (isEQ(acomcalrec.singlePremInd, "Y")) {
			agcmsqnIO.setCurrfrom(acomcalrec.chdrCurrfrom);
			agcmsqnIO.setEfdate(acomcalrec.chdrCurrfrom);
		}
		else {
			if (!flexiblePremium.isTrue()) {
				/*        MOVE ACOM-PAYR-PTDATE   TO AGCMSQN-CURRFROM      <D9604> */
				/*                                   AGCMSQN-EFDATE        <D9604> */
				agcmsqnIO.setCurrfrom(acomcalrec.payrPtdate);
				agcmsqnIO.setEfdate(acomcalrec.payrPtdate);
			}
			else {
				agcmsqnIO.setCurrfrom(wsaaTargFrom);
				agcmsqnIO.setEfdate(wsaaTargFrom);
			}
		}
		agcmsqnIO.setScmdue(ZERO);
		agcmsqnIO.setScmearn(ZERO);
		agcmsqnIO.setRnlcdue(ZERO);
		agcmsqnIO.setTransactionDate(ZERO);
		agcmsqnIO.setTransactionTime(ZERO);
		agcmsqnIO.setTermid(ZERO);
		agcmsqnIO.setUser(ZERO);
		agcmsqnIO.setRnlcearn(ZERO);
		agcmsqnIO.setPtdate(acomcalrec.payrPtdate);
		/* Set up Dormant Flag*/
		agcmsqnIO.setDormantFlag("N");
		//PINNACLE-1380 START
		if(gstOnCommFlag) {
			agcmsqnIO.setInitCommGst(comlinkrec.gstAmount);
			}
		//PINNACLE-1380 END
		agcmsqnIO.setFormat(agcmsqnrec);
		agcmsqnIO.setFunction(varcom.writr);
	}

protected void writeVldfg16220()
	{
		agcmdmnIO.setFunction(agcmsqnIO.getFunction());
		agcmdmnIO.setStatuz(agcmsqnIO.getStatuz());
		agcmdmnIO.setGenDate(agcmsqnIO.getGenDate());
		agcmdmnIO.setGenTime(agcmsqnIO.getGenTime());
		agcmdmnIO.setVn(agcmsqnIO.getVn());
		agcmdmnIO.setFormat(agcmdmnrec);
		agcmdmnIO.setChdrcoy(agcmsqnIO.getChdrcoy());
		agcmdmnIO.setChdrnum(agcmsqnIO.getChdrnum());
		agcmdmnIO.setLife(agcmsqnIO.getLife());
		agcmdmnIO.setCoverage(agcmsqnIO.getCoverage());
		agcmdmnIO.setRider(agcmsqnIO.getRider());
		agcmdmnIO.setPlanSuffix(agcmsqnIO.getPlanSuffix());
		agcmdmnIO.setSeqno(agcmsqnIO.getSeqno());
		agcmdmnIO.setTranno(agcmsqnIO.getTranno());
		agcmdmnIO.setAgntnum(agcmsqnIO.getAgntnum());
		agcmdmnIO.setEfdate(agcmsqnIO.getEfdate());
		agcmdmnIO.setAnnprem(agcmsqnIO.getAnnprem());
		agcmdmnIO.setBasicCommMeth(agcmsqnIO.getBasicCommMeth());
		agcmdmnIO.setInitcom(agcmsqnIO.getInitcom());
		agcmdmnIO.setBascpy(agcmsqnIO.getBascpy());
		agcmdmnIO.setCompay(agcmsqnIO.getCompay());
		agcmdmnIO.setComern(agcmsqnIO.getComern());
		agcmdmnIO.setSrvcpy(agcmsqnIO.getSrvcpy());
		agcmdmnIO.setScmdue(agcmsqnIO.getScmdue());
		agcmdmnIO.setScmearn(agcmsqnIO.getScmearn());
		agcmdmnIO.setRnwcpy(agcmsqnIO.getRnwcpy());
		agcmdmnIO.setRnlcdue(agcmsqnIO.getRnlcdue());
		agcmdmnIO.setRnlcearn(agcmsqnIO.getRnlcearn());
		agcmdmnIO.setAgentClass(agcmsqnIO.getAgentClass());
		agcmdmnIO.setTermid(acomcalrec.termid);
		agcmdmnIO.setTransactionDate(acomcalrec.transactionDate);
		agcmdmnIO.setTransactionTime(acomcalrec.transactionTime);
		agcmdmnIO.setUser(acomcalrec.user);
		agcmdmnIO.setValidflag(agcmsqnIO.getValidflag());
		agcmdmnIO.setCurrfrom(agcmsqnIO.getCurrfrom());
		agcmdmnIO.setCurrto(agcmsqnIO.getCurrto());
		agcmdmnIO.setPtdate(agcmsqnIO.getPtdate());
		agcmdmnIO.setCedagent(agcmsqnIO.getCedagent());
		agcmdmnIO.setOvrdcat(agcmsqnIO.getOvrdcat());
		if (flexiblePremium.isTrue()) {
			agcmdmnIO.setEfdate(wsaaTargFrom);
			agcmdmnIO.setCurrfrom(wsaaTargFrom);
		}
		/* Set up Dormant Flag*/
		agcmdmnIO.setDormantFlag(agcmsqnIO.getDormantFlag());
		//PINNACLE-1380 START
		if(gstOnCommFlag) {
			agcmdmnIO.setInitCommGst(agcmsqnIO.getInitCommGst());
		}
		//PINNACLE-1380 END
		SmartFileCode.execute(appVars, agcmdmnIO);
		if (isNE(agcmdmnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdmnIO.getParams());
			syserrrec.statuz.set(agcmdmnIO.getStatuz());
			systemError9000();
		}
		/* post inital paid for the agcm records created*/
		createCommonAcmv3700();
		if (isNE(agcmdmnIO.getCompay(), ZERO)) {
			lifacmvrec.origamt.set(agcmdmnIO.getCompay());
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			lifacmvrec.tranref.set(agcmdmnIO.getCedagent());
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
		}
		/* post inital earned for the agcm records created*/
		/* IF AGCMDMN-COMERN              NOT = ZEROES                  */
		/*    MOVE AGCMDMN-COMERN         TO LIFA-ORIGAMT               */
		/*    MOVE AGCMDMN-CHDRNUM        TO WSAA-RLDG-CHDRNUM          */
		/*    MOVE AGCMDMN-LIFE           TO WSAA-RLDG-LIFE             */
		/*    MOVE AGCMDMN-COVERAGE       TO WSAA-RLDG-COVERAGE         */
		/*    MOVE AGCMDMN-RIDER          TO WSAA-RLDG-RIDER            */
		/*    MOVE AGCMDMN-PLAN-SUFFIX    TO WSAA-PLAN                  */
		/*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX      */
		/*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT              */
		/*    MOVE ACOM-CNTTYPE           TO LIFA-SUBSTITUTE-CODE(1)    */
		/*    MOVE ACOM-COVR-CRTABLE      TO LIFA-SUBSTITUTE-CODE(6)    */
		/*    MOVE T5645-SACSCODE-07      TO LIFA-SACSCODE              */
		/*    MOVE T5645-SACSTYPE-07      TO LIFA-SACSTYP               */
		/*    MOVE T5645-GLMAP-07         TO LIFA-GLCODE                */
		/*    MOVE T5645-SIGN-07          TO LIFA-GLSIGN                */
		/*    MOVE T5645-CNTTOT-07        TO LIFA-CONTOT                */
		/*    MOVE AGCMDMN-CEDAGENT       TO LIFA-TRANREF               */
		/*    PERFORM 4000-WRITE-ACMV.                                  */
		/*  A different posting will be made dependent upon whether     */
		/*  This contract type uses Component or Contract Level         */
		/*  Accounting.                                                 */
		if (isNE(agcmdmnIO.getComern(), ZERO)) {
			if (isEQ(acomcalrec.comlvlacc, "Y")) {
				wsaaRldgChdrnum.set(agcmdmnIO.getChdrnum());
				wsaaRldgLife.set(agcmdmnIO.getLife());
				wsaaRldgCoverage.set(agcmdmnIO.getCoverage());
				wsaaRldgRider.set(agcmdmnIO.getRider());
				wsaaPlan.set(agcmdmnIO.getPlanSuffix());
				wsaaRldgPlanSuffix.set(wsaaPlansuff);
				lifacmvrec.rldgacct.set(wsaaRldgacct);
				lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
				lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
				lifacmvrec.sacscode.set(t5645rec.sacscode15);
				lifacmvrec.sacstyp.set(t5645rec.sacstype15);
				lifacmvrec.glcode.set(t5645rec.glmap15);
				lifacmvrec.glsign.set(t5645rec.sign15);
				lifacmvrec.contot.set(t5645rec.cnttot15);
			}
			else {
				lifacmvrec.sacscode.set(t5645rec.sacscode07);
				lifacmvrec.sacstyp.set(t5645rec.sacstype07);
				lifacmvrec.glcode.set(t5645rec.glmap07);
				lifacmvrec.glsign.set(t5645rec.sign07);
				lifacmvrec.contot.set(t5645rec.cnttot07);
				lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
				lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
			lifacmvrec.tranref.set(agcmdmnIO.getCedagent());
			lifacmvrec.origamt.set(agcmdmnIO.getComern());
			writeAcmv4000();
		}
		/* IF WSAA-COMM-PAID              NOT = ZEROES                  */
		/*    MOVE AGCMDMN-COMERN         TO LIFA-ORIGAMT               */
		/*    MOVE AGCMDMN-CHDRNUM        TO WSAA-RLDG-CHDRNUM          */
		/*    MOVE AGCMDMN-LIFE           TO WSAA-RLDG-LIFE             */
		/*    MOVE AGCMDMN-COVERAGE       TO WSAA-RLDG-COVERAGE         */
		/*    MOVE AGCMDMN-RIDER          TO WSAA-RLDG-RIDER            */
		/*    MOVE AGCMDMN-PLAN-SUFFIX    TO WSAA-PLAN                  */
		/*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX      */
		/*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT              */
		/*    MOVE ACOM-CNTTYPE           TO LIFA-SUBSTITUTE-CODE(1)    */
		/*    MOVE ACOM-COVR-CRTABLE      TO LIFA-SUBSTITUTE-CODE(6)    */
		/*    MOVE T5645-SACSCODE-08      TO LIFA-SACSCODE              */
		/*    MOVE T5645-SACSTYPE-08      TO LIFA-SACSTYP               */
		/*    MOVE T5645-GLMAP-08         TO LIFA-GLCODE                */
		/*    MOVE T5645-SIGN-08          TO LIFA-GLSIGN                */
		/*    MOVE T5645-CNTTOT-08        TO LIFA-CONTOT                */
		/*    MOVE AGCMDMN-CEDAGENT       TO LIFA-TRANREF               */
		/*    PERFORM 4000-WRITE-ACMV.                                  */
		/*  A different posting will be made dependent upon whether     */
		/*  This contract type uses Component or Contract Level         */
		/*  Accounting.                                                 */
		if (isNE(wsaaCommPaid, ZERO)) {
			if (isEQ(acomcalrec.comlvlacc, "Y")) {
				wsaaRldgChdrnum.set(agcmdmnIO.getChdrnum());
				wsaaRldgLife.set(agcmdmnIO.getLife());
				wsaaRldgCoverage.set(agcmdmnIO.getCoverage());
				wsaaRldgRider.set(agcmdmnIO.getRider());
				wsaaPlan.set(agcmdmnIO.getPlanSuffix());
				wsaaRldgPlanSuffix.set(wsaaPlansuff);
				lifacmvrec.rldgacct.set(wsaaRldgacct);
				lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
				lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
				lifacmvrec.sacscode.set(wsaaT5645Sacscode[1]);
				lifacmvrec.sacstyp.set(wsaaT5645Sacstype[1]);
				lifacmvrec.glcode.set(wsaaT5645Glmap[1]);
				lifacmvrec.glsign.set(wsaaT5645Sign[1]);
				lifacmvrec.contot.set(wsaaT5645Cnttot[1]);
			}
			else {
				lifacmvrec.sacscode.set(t5645rec.sacscode08);
				lifacmvrec.sacstyp.set(t5645rec.sacstype08);
				lifacmvrec.glcode.set(t5645rec.glmap08);
				lifacmvrec.glsign.set(t5645rec.sign08);
				lifacmvrec.contot.set(t5645rec.cnttot08);
				lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
				lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
			lifacmvrec.tranref.set(agcmdmnIO.getCedagent());
			lifacmvrec.origamt.set(wsaaCommPaid);
			writeAcmv4000();
		}
		/* if the AGLF record has a override agent then repeat*/
		/* add another agcm record (repeate this process until*/
		/* there are no override agents for a override agent).........*/
		aglfmjaIO.setAgntcoy(acomcalrec.chdrAgntcoy);
		aglfmjaIO.setAgntnum(aglfmjaIO.getReportag());
		aglfmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfmjaIO);
		if (isNE(aglfmjaIO.getStatuz(), varcom.oK)
		&& isNE(aglfmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglfmjaIO.getParams());
			syserrrec.statuz.set(aglfmjaIO.getStatuz());
			systemError9000();
		}
		if (isEQ(aglfmjaIO.getStatuz(), varcom.mrnf)) {
			wsaaIfOvrdCommEnd.set("Y");
		}
		if (isEQ(aglfmjaIO.getReportag(), SPACES)) {
			wsaaIfOvrdCommEnd.set("Y");
			return ;
		}
	}

protected void callZorcompy6300()
	{
		start6310();
	}

protected void start6310()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.effdate.set(acomcalrec.chdrOccdate);
		zorlnkrec.ptdate.set(agcmsqnIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(agcmsqnIO.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set("Y");
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			systemError9000();
		}
	}

protected void commonClnk8000()
	{
		start8000();
	}

	/**
	* <pre>
	********************************                          <D9604> 
	* </pre>
	*/
protected void start8000()
	{
		comlinkrec.chdrcoy.set(agcmsqnIO.getChdrcoy());
		comlinkrec.chdrnum.set(agcmsqnIO.getChdrnum());
		comlinkrec.life.set(agcmsqnIO.getLife());
		comlinkrec.coverage.set(agcmsqnIO.getCoverage());
		comlinkrec.effdate.set(agcmsqnIO.getEfdate());
		comlinkrec.rider.set(agcmsqnIO.getRider());
		comlinkrec.planSuffix.set(agcmsqnIO.getPlanSuffix());
		comlinkrec.agent.set(agcmsqnIO.getAgntnum());
		comlinkrec.jlife.set(acomcalrec.covrJlife);
		comlinkrec.crtable.set(acomcalrec.covrCrtable);
		comlinkrec.agentClass.set(agcmsqnIO.getAgentClass());
		comlinkrec.icommtot.set(agcmsqnIO.getInitcom());
		comlinkrec.icommpd.set(0);
		comlinkrec.payamnt.set(0);
		comlinkrec.icommernd.set(0);
		comlinkrec.erndamt.set(0);
		comlinkrec.annprem.set(agcmsqnIO.getAnnprem());
		comlinkrec.instprem.set(0);
		comlinkrec.billfreq.set(acomcalrec.payrBillfreq);
		comlinkrec.targetPrem.set(acomcalrec.fpcoTargPrem);
		comlinkrec.currto.set(acomcalrec.fpcoCurrto);
		comlinkrec.seqno.set(agcmsqnIO.getSeqno());
		comlinkrec.method.set(agcmsqnIO.getBascpy());
		comlinkrec.ptdate.set(0);
	}

protected void clawbackOrPay8100()
	{
		start8100();
	}

	/**
	* <pre>
	********************************                          <D9604> 
	* </pre>
	*/
protected void start8100()
	{
		/*                                                         <D9604> */
		/* for flexible premiums, if there is any commission to rev<D9604> */
		/* or pay (either earnings or payment) it will be accounted<D9604> */
		/* here.                                                   <D9604> */
		/*                                                         <D9604> */
		if (isEQ(wsaaErnDiff, ZERO)
		&& isEQ(wsaaPayDiff, ZERO)) {
			return ;
		}
		createCommonAcmv3700();
		if (isLT(wsaaPayDiff, 0)) {
			compute(wsaaClawbackAmt, 2).set(mult(wsaaPayDiff, -1));
			if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
				ovrdCommClawback3900();
			}
			else {
				commClawback3800();
			}
		}
		else {
			if (isGT(wsaaPayDiff, 0)) {
				if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
					ovrdAgentUnderPaid8200();
				}
				else {
					agentUnderPaid8300();
				}
			}
		}
		/*  A negative WSAA-ERN-DIFF means agent earned too much - <D9604> */
		/*  positive means too little. If zero then just right !   <D9604> */
		/*  (so no ACMV)                                           <D9604> */
		if (isLT(wsaaErnDiff, 0)) {
			compute(wsaaClawbackAmt, 2).set(mult(wsaaErnDiff, -1));
			if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
				ovrdEarnClawback8400();
			}
			else {
				earnClawback8500();
			}
		}
		else {
			if (isGT(wsaaErnDiff, 0)) {
				if (isEQ(agcmsqnIO.getOvrdcat(), "O")) {
					ovrdAgentUnderEarn8600();
				}
				else {
					agentUnderEarn8700();
				}
			}
		}
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void ovrdAgentUnderPaid8200()
	{
		para8200();
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void para8200()
	{
		/*                                                         <D9604> */
		lifacmvrec.origamt.set(wsaaPayDiff);
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.contot.set(t5645rec.cnttot06);
		lifacmvrec.rldgacct.set(agcmsqnIO.getAgntnum());
		lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		writeAcmv4000();
		if (isEQ(acomcalrec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaPayDiff);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[1]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[1]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[1]);
			lifacmvrec.glsign.set(wsaaT5645Sign[1]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[1]);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
		}
		else {
			lifacmvrec.origamt.set(wsaaPayDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		writeAcmv4000();
	}

protected void agentUnderPaid8300()
	{
		start8300();
	}

	/**
	* <pre>
	******************************                            <D9604> 
	* </pre>
	*/
protected void start8300()
	{
		lifacmvrec.origamt.set(wsaaPayDiff);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(agcmsqnIO.getAgntnum());
		wsaaRldgChdrnum.set(acomcalrec.chdrnum);
		wsaaRldgLife.set(acomcalrec.covrLife);
		wsaaRldgCoverage.set(acomcalrec.covrCoverage);
		wsaaRldgRider.set(acomcalrec.covrRider);
		wsaaPlan.set(acomcalrec.covrPlanSuffix);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		writeAcmv4000();
		if (isEQ(acomcalrec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaPayDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode14);
			lifacmvrec.sacstyp.set(t5645rec.sacstype14);
			lifacmvrec.glcode.set(t5645rec.glmap14);
			lifacmvrec.glsign.set(t5645rec.sign14);
			lifacmvrec.contot.set(t5645rec.cnttot14);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
		}
		else {
			lifacmvrec.origamt.set(wsaaPayDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		writeAcmv4000();
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void ovrdEarnClawback8400()
	{
		para8400();
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void para8400()
	{
		/*                                                         <D9604> */
		if (isEQ(acomcalrec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[3]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[3]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[3]);
			lifacmvrec.glsign.set(wsaaT5645Sign[3]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[3]);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
			writeAcmv4000();
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[1]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[1]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[1]);
			lifacmvrec.glsign.set(wsaaT5645Sign[1]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[1]);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
			writeAcmv4000();
		}
		else {
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[5]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[5]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[5]);
			lifacmvrec.glsign.set(wsaaT5645Sign[5]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[5]);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
		}
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void earnClawback8500()
	{
		para8500();
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void para8500()
	{
		/*                                                         <D9604> */
		if (isEQ(acomcalrec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaClawbackAmt);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[2]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[2]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[2]);
			lifacmvrec.glsign.set(wsaaT5645Sign[2]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[2]);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
			writeAcmv4000();
			lifacmvrec.origamt.set(wsaaClawbackAmt);
			lifacmvrec.sacscode.set(t5645rec.sacscode14);
			lifacmvrec.sacstyp.set(t5645rec.sacstype14);
			lifacmvrec.glcode.set(t5645rec.glmap14);
			lifacmvrec.glsign.set(t5645rec.sign14);
			lifacmvrec.contot.set(t5645rec.cnttot14);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
			writeAcmv4000();
		}
		else {
			lifacmvrec.origamt.set(wsaaClawbackAmt);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[4]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[4]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[4]);
			lifacmvrec.glsign.set(wsaaT5645Sign[4]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[4]);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
			lifacmvrec.origamt.set(wsaaClawbackAmt);
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
		}
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void ovrdAgentUnderEarn8600()
	{
		para8600();
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void para8600()
	{
		/*                                                         <D9604> */
		if (isEQ(acomcalrec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode12);
			lifacmvrec.sacstyp.set(t5645rec.sacstype12);
			lifacmvrec.glcode.set(t5645rec.glmap12);
			lifacmvrec.glsign.set(t5645rec.sign12);
			lifacmvrec.contot.set(t5645rec.cnttot12);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
			writeAcmv4000();
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode15);
			lifacmvrec.sacstyp.set(t5645rec.sacstype15);
			lifacmvrec.glcode.set(t5645rec.glmap15);
			lifacmvrec.glsign.set(t5645rec.sign15);
			lifacmvrec.contot.set(t5645rec.cnttot15);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
			writeAcmv4000();
		}
		else {
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			lifacmvrec.contot.set(t5645rec.cnttot07);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
		}
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void agentUnderEarn8700()
	{
		para8700();
	}

	/**
	* <pre>
	*********************************                         <D9604> 
	* </pre>
	*/
protected void para8700()
	{
		/*                                                         <D9604> */
		if (isEQ(acomcalrec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode13);
			lifacmvrec.sacstyp.set(t5645rec.sacstype13);
			lifacmvrec.glcode.set(t5645rec.glmap13);
			lifacmvrec.glsign.set(t5645rec.sign13);
			lifacmvrec.contot.set(t5645rec.cnttot13);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
			writeAcmv4000();
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode11);
			lifacmvrec.sacstyp.set(t5645rec.sacstype11);
			lifacmvrec.glcode.set(t5645rec.glmap11);
			lifacmvrec.glsign.set(t5645rec.sign11);
			lifacmvrec.contot.set(t5645rec.cnttot11);
			wsaaRldgChdrnum.set(acomcalrec.chdrnum);
			wsaaRldgLife.set(acomcalrec.covrLife);
			wsaaRldgCoverage.set(acomcalrec.covrCoverage);
			wsaaRldgRider.set(acomcalrec.covrRider);
			wsaaPlan.set(acomcalrec.covrPlanSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(acomcalrec.covrCrtable);
			writeAcmv4000();
		}
		else {
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
			lifacmvrec.origamt.set(wsaaErnDiff);
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
			lifacmvrec.rldgacct.set(acomcalrec.chdrnum);
			lifacmvrec.substituteCode[1].set(acomcalrec.cnttype);
			lifacmvrec.substituteCode[6].set(SPACES);
			writeAcmv4000();
		}
	}

protected void systemError9000()
	{
		start9000();
		exit9490();
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		acomcalrec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		start9500();
		exit9990();
	}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		acomcalrec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(acomcalrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(acomcalrec.payrCntcurr);
		zrdecplrec.batctrcde.set(acomcalrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError9000();
		}
		/*A090-EXIT*/
	}
}