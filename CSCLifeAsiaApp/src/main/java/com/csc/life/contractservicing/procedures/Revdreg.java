/*
 * File: Revdreg.java
 * Date: 30 August 2009 2:07:17
 * Author: Quipoz Limited
 * 
 * Class transformed from REVDREG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrrwinTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.LiferevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrclrTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
* This is a new Subroutine which forms part of the 9405 Annuities
* Development. It is the final part of the processing required
* to reverse the registration of the death of a Life Assured on
* a Contract.
*
* It will be called from REVGENAT as is standard for all reversal
* processing.
*
* If any COVR records were created as part of the registration
* of death processing, these will be deleted, and the previous
* record reinstated.
*
* The LIFE and CLNT records similarly need to Delete the Valid-
* Flag '2' and to reinstate the Valid-Flag '1' records to how
* they were before the Forward Transaction.
*
* The CLMH record, created by the forward transaction should be
* deleted.
*
* Table T5671 will then be read in order to find and call any
* Generic Subroutines, for example to reverse the processing of
* regular payment records on Annuity cases.
*
*
* INPUTS AND OUTPUTS.
*
* INPUTS.
*    COVRCLR
*    LIFEREV
*    CLTS
*    CLMHCLM
*    T5671
*    ITEM
*
* OUTPUTS.
*    LIFE
*    COVR
*    CLTS
*
*
*****************************************************************
* </pre>
*/
public class Revdreg extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVDREG";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private static final String t5671 = "T5671";
		/* FORMATS */
	private static final String covrclrrec = "COVRCLRREC";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String clrrwinrec = "CLRRWINREC";
	private static final String liferevrec = "LIFEREVREC";
	private static final String cltsrec = "CLTSREC";
	private static final String covrrec = "COVRREC";
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	private ClrrwinTableDAM clrrwinIO = new ClrrwinTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrclrTableDAM covrclrIO = new CovrclrTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private LiferevTableDAM liferevIO = new LiferevTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Greversrec greversrec = new Greversrec();
	private Reverserec reverserec = new Reverserec();
	private WsaaStorageAreasAndSubsInner wsaaStorageAreasAndSubsInner = new WsaaStorageAreasAndSubsInner();

	public Revdreg() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		starts1000();
	}

protected void starts1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		initialize(wsaaStorageAreasAndSubsInner.wsaaStorageAreasAndSubs);
		/* Call CLMHCLM to get Transaction number (TRANNO) for use*/
		/* throughout the program.*/
		reverserec.statuz.set(varcom.oK);
		clmhclmIO.setParams(SPACES);
		clmhclmIO.setChdrcoy(reverserec.company);
		clmhclmIO.setChdrnum(reverserec.chdrnum);
		clmhclmIO.setFormat(clmhclmrec);
		clmhclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
		/* After the READH on the CLMHCLM we do a BEGN on the LIFE*/
		/* so as to obtain the CLNTNUM which is subsequently moved*/
		/* to Working Storage for use in later processing.*/
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(clmhclmIO.getChdrcoy());
		lifeIO.setChdrnum(clmhclmIO.getChdrnum());
		lifeIO.setLife(clmhclmIO.getLife());
		lifeIO.setJlife(clmhclmIO.getJlife());
		lifeIO.setCurrfrom(99999999);
		lifeIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		lifeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "JLIFE"); 
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(), varcom.oK))
		&& (isNE(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if (isNE(clmhclmIO.getChdrcoy(), lifeIO.getChdrcoy())
		|| isNE(clmhclmIO.getChdrnum(), lifeIO.getChdrnum())
		|| isNE(clmhclmIO.getLife(), lifeIO.getLife())
		|| isNE(clmhclmIO.getJlife(), lifeIO.getJlife())
		|| isNE(clmhclmIO.getChdrnum(), lifeIO.getChdrnum())
		|| isEQ(lifeIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		wsaaStorageAreasAndSubsInner.wsaaLifcnum.set(lifeIO.getLifcnum());
		/* Store and increment tranno. This will be used in*/
		/* selecting records and in updating them.*/
		wsaaStorageAreasAndSubsInner.wsaaKeyChdrcoy.set(clmhclmIO.getChdrcoy());
		wsaaStorageAreasAndSubsInner.wsaaKeyChdrnum.set(clmhclmIO.getChdrnum());
		wsaaStorageAreasAndSubsInner.wsaaJlife.set(clmhclmIO.getJlife());
		wsaaStorageAreasAndSubsInner.wsaaOldTranno.set(clmhclmIO.getTranno());
		compute(wsaaStorageAreasAndSubsInner.wsaaNewTranno, 0).set(add(1, wsaaStorageAreasAndSubsInner.wsaaOldTranno));
	}

protected void process2000()
	{
		/*STARTS*/
		reverseClaimHeader2100();
		while ( !(isEQ(covrclrIO.getStatuz(), varcom.endp))) {
			reverseCoverage2200();
		}
		
		while ( !(isEQ(liferevIO.getStatuz(), varcom.endp))) {
			reverseLives2300();
		}
		
		updateClts2400();
		/*EXIT*/
	}

protected void reverseClaimHeader2100()
	{
		/*STARTS*/
		/* Delete Record*/
		clmhclmIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void reverseCoverage2200()
	{
		starts2200();
	}

protected void starts2200()
	{
		/* NB: The BEGNH is done within the iteration, this is done*/
		/*     because the first record found should be a valid flag*/
		/*     1, and once deleted the next record found by the BEGNH*/
		/*     should be THE NEXT valid flag 1.*/
		covrclrIO.setParams(SPACES);
		covrclrIO.setChdrnum(wsaaStorageAreasAndSubsInner.wsaaKeyChdrnum);
		covrclrIO.setChdrcoy(wsaaStorageAreasAndSubsInner.wsaaKeyChdrcoy);
		covrclrIO.setTranno(wsaaStorageAreasAndSubsInner.wsaaOldTranno);
		covrclrIO.setFormat(covrclrrec);
		/* MOVE BEGNH                  TO COVRCLR-FUNCTION.             */
		covrclrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrclrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrclrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrclrIO);
		if ((isNE(covrclrIO.getStatuz(), varcom.oK))
		&& (isNE(covrclrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrclrIO.getParams());
			fatalError600();
		}
		/* If no record is found all the COVRCLRs have been reversed*/
		/* so drop out of the iteration.*/
		if ((isNE(covrclrIO.getChdrnum(), wsaaStorageAreasAndSubsInner.wsaaKeyChdrnum))
		|| (isNE(covrclrIO.getChdrcoy(), wsaaStorageAreasAndSubsInner.wsaaKeyChdrcoy))
		|| (isNE(covrclrIO.getValidflag(), "1"))
		|| (isEQ(covrclrIO.getStatuz(), varcom.endp))
		|| (isEQ(covrclrIO.getTranno(), wsaaStorageAreasAndSubsInner.wsaaNewTranno))) {
			covrclrIO.setStatuz(varcom.endp);
			return ;
		}
		/* Store the statcode of the contract*/
		wsaaStorageAreasAndSubsInner.wsaaChdrcoy.set(covrclrIO.getChdrcoy());
		wsaaStorageAreasAndSubsInner.wsaaChdrnum.set(covrclrIO.getChdrnum());
		wsaaStorageAreasAndSubsInner.wsaaLife.set(covrclrIO.getLife());
		wsaaStorageAreasAndSubsInner.wsaaCoverage.set(covrclrIO.getCoverage());
		wsaaStorageAreasAndSubsInner.wsaaRider.set(covrclrIO.getRider());
		wsaaStorageAreasAndSubsInner.wsaaPlanSuffix.set(covrclrIO.getPlanSuffix());
		/* Delete the valid flag 1 record*/
		/* MOVE DELET                  TO COVRCLR-FUNCTION.             */
		covrclrIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, covrclrIO);
		if (isNE(covrclrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrclrIO.getParams());
			fatalError600();
		}
		/* Perform a BEGNH on the COVR logical view using the key*/
		/* store to find the valid flag 2 record*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(wsaaStorageAreasAndSubsInner.wsaaChdrcoy);
		covrIO.setChdrnum(wsaaStorageAreasAndSubsInner.wsaaChdrnum);
		covrIO.setLife(wsaaStorageAreasAndSubsInner.wsaaLife);
		covrIO.setCoverage(wsaaStorageAreasAndSubsInner.wsaaCoverage);
		covrIO.setRider(wsaaStorageAreasAndSubsInner.wsaaRider);
		covrIO.setPlanSuffix(wsaaStorageAreasAndSubsInner.wsaaPlanSuffix);
		covrIO.setFormat(covrrec);
		/* MOVE BEGNH                  TO COVR-FUNCTION.                */
		covrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(), varcom.oK))
		&& (isNE(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		/* Unlike the check for the Valid flag 1 record, if no record*/
		/* is found there must be an error. This is because for every*/
		/* valid flag 1 record there must be a Valid flag 2 record.*/
		if ((isNE(covrIO.getChdrcoy(), wsaaStorageAreasAndSubsInner.wsaaKeyChdrcoy))
		|| (isNE(covrIO.getChdrnum(), wsaaStorageAreasAndSubsInner.wsaaKeyChdrnum))
		|| (isNE(covrIO.getValidflag(), "2"))
		|| (isEQ(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		/* The Generic routines are now performed. The section*/
		/* calls T5671 and performs the subroutines held on it.*/
		genericProcessing2210();
		if (isNE(reverserec.statuz, varcom.oK)) {
			syserrrec.params.set(reverserec.reverseRec);
			fatalError600();
		}
		/* Update the valid flag 2 record*/
		covrIO.setValidflag("1");
		covrIO.setTranno(wsaaStorageAreasAndSubsInner.wsaaNewTranno);
		covrIO.setCurrto(varcom.maxdate);
		/* MOVE REWRT                  TO COVR-FUNCTION.                */
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
	}

protected void genericProcessing2210()
	{
		starts2210();
	}

protected void starts2210()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaStorageAreasAndSubsInner.wsaaChdrcoy);
		itemIO.setItemtabl(t5671);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(reverserec.oldBatctrcde, SPACES);
		stringVariable1.addExpression(covrIO.getCrtable(), SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		initialize(greversrec.reverseRec);
		greversrec.effdate.set(varcom.vrcmMaxDate);
		greversrec.transDate.set(varcom.vrcmMaxDate);
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.life.set(covrIO.getLife());
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.effdate.set(reverserec.ptrneff);
		greversrec.language.set(reverserec.language);
		greversrec.statuz.set(varcom.oK);
		/* Perform each of the subroutines (if found) on T5671:*/
		for (wsaaStorageAreasAndSubsInner.wsaaSub.set(1); !(isGT(wsaaStorageAreasAndSubsInner.wsaaSub, 4)); wsaaStorageAreasAndSubsInner.wsaaSub.add(1)){
			if (isNE(t5671rec.trevsub[wsaaStorageAreasAndSubsInner.wsaaSub.toInt()], SPACES)) {
				callProgram(t5671rec.trevsub[wsaaStorageAreasAndSubsInner.wsaaSub.toInt()], greversrec.reverseRec);
				if (isNE(greversrec.statuz, varcom.oK)) {
					syserrrec.statuz.set(greversrec.statuz);
					fatalError600();
				}
			}
		}
	}

protected void reverseLives2300()
	{
		starts2300();
	}

protected void starts2300()
	{
		/* NB: The BEGNH is done within the iteration, this is done*/
		/*     because the first record found should be a valid flag*/
		/*     2. NB: LIFEREV HAS A DESCENDING KEY.*/
		liferevIO.setParams(SPACES);
		liferevIO.setChdrnum(wsaaStorageAreasAndSubsInner.wsaaKeyChdrnum);
		liferevIO.setChdrcoy(wsaaStorageAreasAndSubsInner.wsaaKeyChdrcoy);
		liferevIO.setTranno(wsaaStorageAreasAndSubsInner.wsaaOldTranno);
		liferevIO.setFormat(liferevrec);
		/* MOVE BEGNH                  TO LIFEREV-FUNCTION.             */
		liferevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		liferevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		liferevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, liferevIO);
		if ((isNE(liferevIO.getStatuz(), varcom.oK))
		&& (isNE(liferevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(liferevIO.getParams());
			fatalError600();
		}
		/* If no record is found all the LIFEREVs have been reversed*/
		/* so drop out of the iteration.*/
		/*    Note the check on TRANNO. anything other than the REVE-*/
		/* TRANNO means that the process has gone on to process another*/
		/* life.*/
		if ((isNE(liferevIO.getChdrnum(), wsaaStorageAreasAndSubsInner.wsaaKeyChdrnum))
		|| (isNE(liferevIO.getChdrcoy(), wsaaStorageAreasAndSubsInner.wsaaKeyChdrcoy))
		|| (isNE(liferevIO.getValidflag(), "1"))
		|| (isNE(liferevIO.getJlife(), wsaaStorageAreasAndSubsInner.wsaaJlife))
		|| (isNE(liferevIO.getTranno(), reverserec.tranno))
		|| (isEQ(liferevIO.getStatuz(), varcom.endp))) {
			liferevIO.setStatuz(varcom.endp);
			return ;
		}
		/* Store the statcode of the contract*/
		wsaaStorageAreasAndSubsInner.wsaaChdrcoy.set(liferevIO.getChdrcoy());
		wsaaStorageAreasAndSubsInner.wsaaChdrnum.set(liferevIO.getChdrnum());
		wsaaStorageAreasAndSubsInner.wsaaLife.set(liferevIO.getLife());
		wsaaStorageAreasAndSubsInner.wsaaJlife.set(liferevIO.getJlife());
		/* Delete the valid flag 1 record*/
		/* MOVE DELET                  TO LIFEREV-FUNCTION.             */
		liferevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, liferevIO);
		if (isNE(liferevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(liferevIO.getParams());
			fatalError600();
		}
		/* Perform a BEGNH on the LIFE using the key*/
		/* store to find the valid flag 2 record*/
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(wsaaStorageAreasAndSubsInner.wsaaChdrcoy);
		lifeIO.setChdrnum(wsaaStorageAreasAndSubsInner.wsaaChdrnum);
		lifeIO.setLife(wsaaStorageAreasAndSubsInner.wsaaLife);
		lifeIO.setJlife(wsaaStorageAreasAndSubsInner.wsaaJlife);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		/* MOVE BEGNH                  TO LIFE-FUNCTION.                */
		lifeIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(), varcom.oK))
		&& (isNE(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		/* Unlike the check for the Valid flag 1 record, if no record*/
		/* is found there must be an error. This is because for every*/
		/* valid flag 1 record there must be a Valid flag 2 record.*/
		if ((isNE(lifeIO.getChdrcoy(), wsaaStorageAreasAndSubsInner.wsaaKeyChdrcoy))
		|| (isNE(lifeIO.getChdrnum(), wsaaStorageAreasAndSubsInner.wsaaKeyChdrnum))
		|| (isNE(lifeIO.getValidflag(), "2"))
		|| (isEQ(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		/* Update the valid flag 2 record*/
		lifeIO.setValidflag("1");
		lifeIO.setTranno(wsaaStorageAreasAndSubsInner.wsaaNewTranno);
		lifeIO.setCurrto(varcom.maxdate);
		/* MOVE REWRT                  TO LIFE-FUNCTION.                */
		lifeIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
	}

protected void updateClts2400()
	{
		starts2400();
	}

protected void starts2400()
	{
		clrrwinIO.setParams(SPACES);
		clrrwinIO.setForecoy(lifeIO.getChdrcoy());
		clrrwinIO.setClntnum(lifeIO.getLifcnum());
		clrrwinIO.setClrrrole(SPACES);
		clrrwinIO.setFormat(clrrwinrec);
		clrrwinIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, clrrwinIO);
		if ((isNE(clrrwinIO.getStatuz(), varcom.oK))
		&& (isNE(clrrwinIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(clrrwinIO.getParams());
			fatalError600();
		}
		if (isNE(clrrwinIO.getClntnum(), wsaaStorageAreasAndSubsInner.wsaaLifcnum)) {
			clrrwinIO.setParams(clrrwinIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntcoy(clrrwinIO.getClntcoy());
		cltsIO.setClntnum(wsaaStorageAreasAndSubsInner.wsaaLifcnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*                                                      <V76F10>*/
		/* MOVE CLTSREC                TO CLTS-FORMAT.          <V76F10>*/
		/* MOVE '2'                    TO CLTS-VALIDFLAG.       <V76F10>*/
		/* MOVE REWRT                  TO CLTS-FUNCTION.        <V76F10>*/
		/*                                                      <V76F10>*/
		/* CALL 'CLTSIO'               USING CLTS-PARAMS.       <V76F10>*/
		/*                                                      <V76F10>*/
		/* IF CLTS-STATUZ           NOT = O-K                   <V76F10>*/
		/*    MOVE CLTS-PARAMS         TO SYSR-PARAMS           <V76F10>*/
		/*    PERFORM 600-FATAL-ERROR                           <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		cltsIO.setCltdod(varcom.vrcmMaxDate);
		/* MOVE REWRT                  TO CLTS-FUNCTION.                */
		/* MOVE '1'                    TO CLTS-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO CLTS-FUNCTION.        <V76F10>*/
		cltsIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		fatalErrors600();
		errorProg600();
	}

protected void fatalErrors600()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg600()
	{
		reverserec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure WSAA-STORAGE-AREAS-AND-SUBS--INNER
 */
private static final class WsaaStorageAreasAndSubsInner { 

	private FixedLengthStringData wsaaStorageAreasAndSubs = new FixedLengthStringData(44);
	private FixedLengthStringData wsaaKeyChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaStorageAreasAndSubs, 0);
	private FixedLengthStringData wsaaKeyChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaStorageAreasAndSubs, 1);
	private PackedDecimalData wsaaOldTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaStorageAreasAndSubs, 9);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaStorageAreasAndSubs, 12);
	private PackedDecimalData wsaaSub = new PackedDecimalData(1, 0).isAPartOf(wsaaStorageAreasAndSubs, 15).setUnsigned();
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaStorageAreasAndSubs, 16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaStorageAreasAndSubs, 17);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaStorageAreasAndSubs, 25);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaStorageAreasAndSubs, 27);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaStorageAreasAndSubs, 29);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaStorageAreasAndSubs, 31);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).isAPartOf(wsaaStorageAreasAndSubs, 34);
	private FixedLengthStringData wsaaLifcnum = new FixedLengthStringData(8).isAPartOf(wsaaStorageAreasAndSubs, 36);
}
}
