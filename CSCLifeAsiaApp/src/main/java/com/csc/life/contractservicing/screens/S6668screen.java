package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6668screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6668ScreenVars sv = (S6668ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6668screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6668ScreenVars screenVars = (S6668ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.jownnum.setClassString("");
		screenVars.jownername.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.billday.setClassString("");
		screenVars.billcdDisp.setClassString("");
		screenVars.mopdesc.setClassString("");
		screenVars.freqdesc.setClassString("");
		screenVars.grpind.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.payind.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.aiind.setClassString("");
	}

/**
 * Clear all the variables in S6668screen
 */
	public static void clear(VarModel pv) {
		S6668ScreenVars screenVars = (S6668ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.jownnum.clear();
		screenVars.jownername.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.billfreq.clear();
		screenVars.billday.clear();
		screenVars.billcdDisp.clear();
		screenVars.billcd.clear();
		screenVars.mopdesc.clear();
		screenVars.freqdesc.clear();
		screenVars.grpind.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.mop.clear();
		screenVars.payind.clear();
		screenVars.ddind.clear();
		screenVars.aiind.clear();
	}
}
