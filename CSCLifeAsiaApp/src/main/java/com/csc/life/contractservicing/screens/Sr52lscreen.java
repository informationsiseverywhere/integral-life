package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr52lscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52lScreenVars sv = (Sr52lScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52lscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52lScreenVars screenVars = (Sr52lScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.transcode.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.batctrcde.setClassString("");
		screenVars.reserveUnitsDateDisp.setClassString("");
		screenVars.erordsc.setClassString("");
		screenVars.amnt.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.dtldesc01.setClassString("");
		screenVars.medfee01.setClassString("");
		screenVars.medamt01.setClassString("");
		screenVars.dtldesc02.setClassString("");
		screenVars.admfee01.setClassString("");
		screenVars.admamt01.setClassString("");
		screenVars.dtldesc03.setClassString("");
		screenVars.sacscode01.setClassString("");
		screenVars.sacstypw01.setClassString("");
		screenVars.zdesc01.setClassString("");
		screenVars.adjamt01.setClassString("");
		screenVars.dtldesc04.setClassString("");
		screenVars.tdeduct.setClassString("");
		screenVars.dtldesc05.setClassString("");
		screenVars.premi.setClassString("");
		screenVars.dtldesc06.setClassString("");
		screenVars.medfee02.setClassString("");
		screenVars.medamt02.setClassString("");
		screenVars.dtldesc07.setClassString("");
		screenVars.admfee02.setClassString("");
		screenVars.admamt02.setClassString("");
		screenVars.dtldesc08.setClassString("");
		screenVars.sacscode02.setClassString("");
		screenVars.sacstypw02.setClassString("");
		screenVars.zdesc02.setClassString("");
		screenVars.adjamt02.setClassString("");
		screenVars.dtldesc09.setClassString("");
		screenVars.ztotamt.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.resndesc.setClassString("");
	}

/**
 * Clear all the variables in Sr52lscreen
 */
	public static void clear(VarModel pv) {
		Sr52lScreenVars screenVars = (Sr52lScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.transcode.clear();
		screenVars.chdrnum.clear();
		screenVars.ctypedes.clear();
		screenVars.batctrcde.clear();
		screenVars.reserveUnitsDateDisp.clear();
		screenVars.reserveUnitsDate.clear();
		screenVars.erordsc.clear();
		screenVars.amnt.clear();
		screenVars.billcurr.clear();
		screenVars.dtldesc01.clear();
		screenVars.medfee01.clear();
		screenVars.medamt01.clear();
		screenVars.dtldesc02.clear();
		screenVars.admfee01.clear();
		screenVars.admamt01.clear();
		screenVars.dtldesc03.clear();
		screenVars.sacscode01.clear();
		screenVars.sacstypw01.clear();
		screenVars.zdesc01.clear();
		screenVars.adjamt01.clear();
		screenVars.dtldesc04.clear();
		screenVars.tdeduct.clear();
		screenVars.dtldesc05.clear();
		screenVars.premi.clear();
		screenVars.dtldesc06.clear();
		screenVars.medfee02.clear();
		screenVars.medamt02.clear();
		screenVars.dtldesc07.clear();
		screenVars.admfee02.clear();
		screenVars.admamt02.clear();
		screenVars.dtldesc08.clear();
		screenVars.sacscode02.clear();
		screenVars.sacstypw02.clear();
		screenVars.zdesc02.clear();
		screenVars.adjamt02.clear();
		screenVars.dtldesc09.clear();
		screenVars.ztotamt.clear();
		screenVars.reasoncd.clear();
		screenVars.resndesc.clear();
	}
}
