package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5080
 * @version 1.0 generated on 30/08/09 06:33
 * @author Quipoz
 */
public class S5080ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(403);
	public FixedLengthStringData dataFields = new FixedLengthStringData(227).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData despname = DD.despname.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData despsel = DD.despsel.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,153);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,163);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,167);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 227);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData despnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData despselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 271);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] despnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] despselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5080screenWritten = new LongData(0);
	public LongData S5080protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5080ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(despselOut,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, rstate, pstate, cownnum, ownername, despsel, despname, reasoncd, resndesc};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, rstateOut, pstateOut, cownnumOut, ownernameOut, despselOut, despnameOut, reasoncdOut, resndescOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, rstateErr, pstateErr, cownnumErr, ownernameErr, despselErr, despnameErr, reasoncdErr, resndescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5080screen.class;
		protectRecord = S5080protect.class;
	}

}
