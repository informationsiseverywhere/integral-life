/*
 * File: Pt523at.java
 * Date: 30 August 2009 2:01:02
 * Author: Quipoz Limited
 *
 * Class transformed from PT523AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*  POLICY DEBT REGISTER   -   AT MODULE
*  ------------------------------------
*
*  This AT module is called by the Policy Debt Register
*  program PT523  and the Contract number with Trans. no are
*  pass in the AT parameters area as the "primary key".
*
*  AT PROCESSING
*
*  Check the return status  codes  for  each  of  the
*  following records being updated.
*
*  CONTRACT HEADER RECORD
*
*  Read the  Contract Header record (CHDRLNB) for the relevant
*  contract.  Then, updates trans. no to CHDRLNB for the
*  corresponding record.
*
*  Only  post  when  the amount  is greater  than zero.  Call
*  LIFACMV  ("cash"   posting  subroutine)  to  post  to  the
*  correct  account. The  posting  required is defined in the
*  appropriate line no.  on  the  T5645  table entry.  Set up
*  and pass the linkage area as follows:
*
*  Post to the sub-account (01) or (02) see below:
*  Use the  amount  on  the  sub-account with the sign from
*  T5645.
*
*  If it is amount from interest rate
*  then post to the sub-account (01).
*
*  If it is amount without interest rate
*  then post to the sub-account (02).
*
*  Amount between interest or without interest can be identified
*  by considering the Batch Trans Code (otherwise by action code).
*
*  Then post to the sub-account (03) as the above counterpart.
*
*  The accounting entry will be the following :
*
*  Dr.   LP  CI or CN            999,999.99
*     Cr.   LP  OR                  999,999.99
*
*  GENERAL HOUSE-KEEPING
*
*  1) Update the  batch  header  by  calling  BATCUP with a
*     function of WRITS and the following parameters:
*
*  - transaction count equals 1
*  - all other amounts zero
*  - batch key from AT linkage
*
*  3) Release contract SFTLOCK.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pt523at extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PT523AT");
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);
	private ZonedDecimalData wsaaPrimaryTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrimaryKey, 9).setUnsigned();
	private FixedLengthStringData wsaaPrimaryAction = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 14);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaTransArea, 16);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 19);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
		/* TABLES */
	private String t5645 = "T5645";
		/* FORMATS */
	private String chdrlnbrec = "CHDRLNBREC";
	private String tpoldbtrec = "TPOLDBTREC";
	private String ptrnrec = "PTRNREC";
	private Atmodrec atmodrec = new Atmodrec();
	private Batcuprec batcuprec = new Batcuprec();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
		/*Policy Debt*/
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		xxxxErrorProg,
		exit2490
	}

	public Pt523at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		housekeepingTerminate3000();
		/*EXIT*/
		exitProgram();
	}

protected void xxxxFatalError()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					xxxxFatalErrors();
				}
				case xxxxErrorProg: {
					xxxxErrorProg();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.xxxxErrorProg);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		getTranAcctRules1200();
		callDatcons1300();
		readContractHeader1400();
		/*EXIT*/
	}

protected void getTranAcctRules1200()
	{
		read1201();
	}

protected void read1201()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("PT523");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(atmodrec.company);
		descIO.setDesctabl(t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("PT523");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			xxxxFatalError();
		}
	}

protected void callDatcons1300()
	{
		/*DATCON1*/
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

protected void readContractHeader1400()
	{
		/*READ-CONTRACT-HEADER*/
		chdrlnbIO.setRecKeyData(SPACES);
		chdrlnbIO.setChdrcoy(wsaaPrimaryChdrcoy);
		chdrlnbIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void process2000()
	{
		/*PROCESS*/
		processContractHeader2100();
		postPolDebt2400();
		writePtrn2500();
		/*EXIT*/
	}

protected void processContractHeader2100()
	{
		/*PROCESS-CONTRACT-HEADER*/
		setPrecision(chdrlnbIO.getTranno(), 0);
		chdrlnbIO.setTranno(add(chdrlnbIO.getTranno(),1));
		chdrlnbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void postPolDebt2400()
	{
		try {
			go2401();
		}
		catch (GOTOException e){
		}
	}

protected void go2401()
	{
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrlnbIO.getChdrnum());
		tpoldbtIO.setTranno(wsaaPrimaryTranno);
		tpoldbtIO.setFormat(tpoldbtrec);
		tpoldbtIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			xxxxFatalError();
		}
		initialize(lifacmvrec.lifacmvRec);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranno.set(wsaaPrimaryTranno);
		if (isEQ(wsaaPrimaryAction,"A")) {
			wsaaI.set(1);
		}
		else {
			wsaaI.set(2);
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaI.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaI.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaI.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaI.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaI.toInt()]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.origamt.set(tpoldbtIO.getTdbtamt());
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			goTo(GotoLabel.exit2490);
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(wsaaPrimaryKey);
		lifacmvrec.trandesc.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(descIO.getShortdesc().toString());
		stringVariable1.append(" ");
		stringVariable1.append(tpoldbtIO.getTdbtdesc().toString());
		lifacmvrec.trandesc.setLeft(stringVariable1.toString());
		lifacmvrec.effdate.set(tpoldbtIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(tpoldbtIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		initialize(lifacmvrec.lifacmvRec);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranno.set(wsaaPrimaryTranno);
		wsaaI.set(3);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaI.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaI.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaI.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaI.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaI.toInt()]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.origamt.set(tpoldbtIO.getTdbtamt());
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			goTo(GotoLabel.exit2490);
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(wsaaPrimaryKey);
		lifacmvrec.trandesc.set(SPACES);
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(descIO.getShortdesc().toString());
		stringVariable2.append(" ");
		stringVariable2.append(tpoldbtIO.getTdbtdesc().toString());
		lifacmvrec.trandesc.setLeft(stringVariable2.toString());
		lifacmvrec.effdate.set(tpoldbtIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(tpoldbtIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void writePtrn2500()
	{
		start2500();
	}

protected void start2500()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(wsaaPrimaryTranno);
		ptrnIO.setPtrneff(tpoldbtIO.getEffdate());
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void housekeepingTerminate3000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		batchHeader3200();
		releaseSoftlock3300();
		/*EXIT*/
	}

protected void batchHeader3200()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void releaseSoftlock3300()
	{
		releaseSoftlock3301();
	}

protected void releaseSoftlock3301()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}
}
