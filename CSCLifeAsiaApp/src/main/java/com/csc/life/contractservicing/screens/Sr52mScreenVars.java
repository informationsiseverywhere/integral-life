package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52M
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr52mScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(263);
	public FixedLengthStringData dataFields = new FixedLengthStringData(71).isAPartOf(dataArea, 0);
	public ZonedDecimalData admfee = DD.admfee.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData admpcs = new FixedLengthStringData(6).isAPartOf(dataFields, 12);
	public ZonedDecimalData[] admpc = ZDArrayPartOfStructure(2, 3, 0, admpcs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(admpcs, 0, FILLER_REDEFINE);
	public ZonedDecimalData admpc01 = DD.admpc.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData admpc02 = DD.admpc.copyToZonedDecimal().isAPartOf(filler,3);
	public FixedLengthStringData basind = DD.basind.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData fieldEditCode = DD.edit.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData medpcs = new FixedLengthStringData(6).isAPartOf(dataFields, 59);
	public ZonedDecimalData[] medpc = ZDArrayPartOfStructure(2, 3, 0, medpcs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(medpcs, 0, FILLER_REDEFINE);
	public ZonedDecimalData medpc01 = DD.medpc.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData medpc02 = DD.medpc.copyToZonedDecimal().isAPartOf(filler1,3);
	public FixedLengthStringData ratebas = DD.ratebas.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 71);
	public FixedLengthStringData admfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData admpcsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] admpcErr = FLSArrayPartOfStructure(2, 4, admpcsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(admpcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData admpc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData admpc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData basindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData editErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData medpcsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] medpcErr = FLSArrayPartOfStructure(2, 4, medpcsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(medpcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData medpc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData medpc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData ratebasErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 119);
	public FixedLengthStringData[] admfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData admpcsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] admpcOut = FLSArrayPartOfStructure(2, 12, admpcsOut, 0);
	public FixedLengthStringData[][] admpcO = FLSDArrayPartOfArrayStructure(12, 1, admpcOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(admpcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] admpc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] admpc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] basindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] editOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData medpcsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] medpcOut = FLSArrayPartOfStructure(2, 12, medpcsOut, 0);
	public FixedLengthStringData[][] medpcO = FLSDArrayPartOfArrayStructure(12, 1, medpcOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(medpcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] medpc01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] medpc02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] ratebasOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr52mscreenWritten = new LongData(0);
	public LongData Sr52mprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52mScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ratebasOut,new String[] {"59",null, "-59",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(basindOut,new String[] {"60",null, "-60",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(editOut,new String[] {"61",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(medpc01Out,new String[] {"62",null, "-62",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(medpc02Out,new String[] {"63",null, "-63",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(admfeeOut,new String[] {"64",null, "-64",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(admpc01Out,new String[] {"65",null, "-65",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(admpc02Out,new String[] {"66",null, "-66",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, ratebas, basind, fieldEditCode, medpc01, medpc02, admfee, admpc01, admpc02};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, ratebasOut, basindOut, editOut, medpc01Out, medpc02Out, admfeeOut, admpc01Out, admpc02Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, ratebasErr, basindErr, editErr, medpc01Err, medpc02Err, admfeeErr, admpc01Err, admpc02Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52mscreen.class;
		protectRecord = Sr52mprotect.class;
	}

}
