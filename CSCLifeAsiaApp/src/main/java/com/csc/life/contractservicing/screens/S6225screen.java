package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6225screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 8, 1, 52}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6225ScreenVars sv = (S6225ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6225screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6225ScreenVars screenVars = (S6225ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.despnum.setClassString("");
		screenVars.despname.setClassString("");
		screenVars.cltaddr01.setClassString("");
		screenVars.cltaddr02.setClassString("");
		screenVars.cltaddr03.setClassString("");
		screenVars.cltaddr04.setClassString("");
		screenVars.cltaddr05.setClassString("");
		screenVars.cltpcode.setClassString("");
	}

/**
 * Clear all the variables in S6225screen
 */
	public static void clear(VarModel pv) {
		S6225ScreenVars screenVars = (S6225ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.despnum.clear();
		screenVars.despname.clear();
		screenVars.cltaddr01.clear();
		screenVars.cltaddr02.clear();
		screenVars.cltaddr03.clear();
		screenVars.cltaddr04.clear();
		screenVars.cltaddr05.clear();
		screenVars.cltpcode.clear();
	}
}
