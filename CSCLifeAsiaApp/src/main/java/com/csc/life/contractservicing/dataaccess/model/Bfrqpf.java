/******************************************************************************
 * File Name 		: Bfrqpf.java
 * Author			: smalchi2
 * Creation Date	: 04 January 2017
 * Project			: Integral Life
 * Description		: The Model Class for BFRQPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

public class Bfrqpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "BFRQPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String cnttype;
	private String currency;
	private String freq;
	private String billfreq;
	private BigDecimal instpramt;
	private BigDecimal instprem;
	private Integer effdate;
	private String validflag;
	private String usrprf;
	private String jobnm;
	private Date datime;

	// Constructor
	public Bfrqpf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getCnttype(){
		return this.cnttype;
	}
	public String getCurrency(){
		return this.currency;
	}
	public String getFreq(){
		return this.freq;
	}
	public String getBillfreq(){
		return this.billfreq;
	}
	public BigDecimal getInstpramt(){
		return this.instpramt;
	}
	public BigDecimal getInstprem(){
		return this.instprem;
	}
	public Integer getEffdate(){
		return this.effdate;
	}
	public String getValidflag(){
		return this.validflag;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setCnttype( String cnttype ){
		 this.cnttype = cnttype;
	}
	public void setCurrency( String currency ){
		 this.currency = currency;
	}
	public void setFreq( String freq ){
		 this.freq = freq;
	}
	public void setBillfreq( String billfreq ){
		 this.billfreq = billfreq;
	}
	public void setInstpramt( BigDecimal instpramt ){
		 this.instpramt = instpramt;
	}
	public void setInstprem( BigDecimal instprem ){
		 this.instprem = instprem;
	}
	public void setEffdate( Integer effdate ){
		 this.effdate = effdate;
	}
	public void setValidflag( String validflag ){
		 this.validflag = validflag;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("CNTTYPE:		");
		output.append(getCnttype());
		output.append("\r\n");
		output.append("CURRENCY:		");
		output.append(getCurrency());
		output.append("\r\n");
		output.append("FREQ:		");
		output.append(getFreq());
		output.append("\r\n");
		output.append("BILLFREQ:		");
		output.append(getBillfreq());
		output.append("\r\n");
		output.append("INSTPRAMT:		");
		output.append(getInstpramt());
		output.append("\r\n");
		output.append("INSTPREM:		");
		output.append(getInstprem());
		output.append("\r\n");
		output.append("EFFDATE:		");
		output.append(getEffdate());
		output.append("\r\n");
		output.append("VALIDFLAG:		");
		output.append(getValidflag());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
