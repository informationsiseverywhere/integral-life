package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52D
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr52dScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(151);
	public FixedLengthStringData dataFields = new FixedLengthStringData(55).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData txcode = DD.txcode.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData txsubr = DD.txsubr.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 55);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData txcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData txsubrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 79);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] txcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] txsubrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr52dscreenWritten = new LongData(0);
	public LongData Sr52dprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52dScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, txcode, txsubr};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, txcodeOut, txsubrOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, txcodeErr, txsubrErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52dscreen.class;
		protectRecord = Sr52dprotect.class;
	}

}
