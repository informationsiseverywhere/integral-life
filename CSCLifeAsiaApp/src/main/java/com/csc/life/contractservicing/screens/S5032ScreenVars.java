package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5032
 * @version 1.0 generated on 30/08/09 06:31
 * @author Quipoz
 */
public class S5032ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(43);
	public FixedLengthStringData dataFields = new FixedLengthStringData(11).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 11);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 19);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5032screenWritten = new LongData(0);
	public LongData S5032protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5032ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {action, chdrsel};
		screenOutFields = new BaseData[][] {actionOut, chdrselOut};
		screenErrFields = new BaseData[] {actionErr, chdrselErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S5032screen.class;
		protectRecord = S5032protect.class;
	}

}
