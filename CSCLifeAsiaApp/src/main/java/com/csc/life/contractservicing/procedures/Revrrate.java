/*
 * File: Revrrate.java
 * Date: 30 August 2009 2:11:31
 * Author: Quipoz Limited
 * 
 * Class transformed from REVRRATE.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrvsTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrrevTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.PayrlifDAO;
import com.csc.life.newbusiness.dataaccess.model.Payrpf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*              RE-RATE REVERSAL SUBROUTINE.
*
*OVERVIEW.
*
*  This subroutine will be called  from  the  Reversal Routines
*  Table,  T6661,  when  reversing  the effects of Re-rate on a
*  re-rateable component within a contract.
*
*  It  will reverse the updates to the following files, changed
*  during Re-rate processing.
*
*
*CONTRACT HEADER (CHDR)
*
*       Delete the valid flag '1' record created during Re-rate
*       and reinstate the valid flag '2' record.
*
*PAYER (PAYR)
*
*       Delete the valid flag '1' record created during Re-rate
*       and reinstate the valid flag '2' record.
*
*COVERAGE (COVR)
*
*       Delete any valid flag '1' records created during Re-rate
*       and reinstate the valid flag '2' records.
*
*INCREASE (INCR)
*
*       Delete any valid flag '1' records created during Re-rate
*       and reinstate the valid flag '2' records.
*
*AGENT COMMISSION (AGCM)
*
*       Delete any valid flag '1' records created during Re-rate
*       and reinstate the valid flag '2' records.
*
*GENERIC SUBROUTINES
*
*       Table T5671  will be  read and any reversal subroutines
*       found will also be called.
*
*****************************************************************
*
* </pre>
*/
public class Revrrate extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "REVRRATE";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaBatchkey = new FixedLengthStringData(65);
	private FixedLengthStringData wsaaContkey = new FixedLengthStringData(19).isAPartOf(wsaaBatchkey, 0);
	private FixedLengthStringData wsaaBatcpfx = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 0);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaContkey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaContkey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaContkey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaContkey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaContkey, 14);
	private FixedLengthStringData wsaaOldBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatchkey, 19);
		/* TABLES */
	private String t5671 = "T5671";
		/* FORMATS */
	private String chdrmjarec = "CHDRMJAREC";
	private String covrenqrec = "COVRENQREC";
	private String covrrec = "COVRREC";
	private String itemrec = "ITEMREC";
	private String payrrevrec = "PAYRREVREC";
	private String incrselrec = "INCRSELREC";
	private String agcmrvsrec = "AGCMRVSREC";
	private String agcmdbcrec = "AGCMREC";
		/*Agent Commission Dets- Billing Change*/
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
		/*Logical File for Reversals*/
	private AgcmrvsTableDAM agcmrvsIO = new AgcmrvsTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Greversrec greversrec = new Greversrec();
		/*Automatic Increase Refusal Select File*/
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Alter from Inception view of PAYR*/
	private PayrrevTableDAM payrrevIO = new PayrrevTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private PayrlifDAO payrlifDAO	 = getApplicationContext().getBean("payrlifDAO", PayrlifDAO.class);
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit4349, 
		exit4590, 
		errorProg610
	}

	public Revrrate() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set("TDAY");
		datcon1rec.intDate.set(ZERO);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		processChdr2000();
		processPayr3000();
		processCovrs4000();
		processAgcms5000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processChdr2000()
	{
		begnh2010();
	}

protected void begnh2010()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(),"1")
		|| isEQ(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(),"2")
		|| isEQ(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		chdrmjaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void processPayr3000()
	{
		begnh3010();
	}

protected void begnh3010()
	{
	
	
	List<Payrpf> payrpfs = payrlifDAO.getUniqueAndFlag(reverserec.company.toString().trim(),
			reverserec.chdrnum.toString().trim());
		/*if (payrpfs.size() != 2) {
			syserrrec.statuz.set(Varcom.bomb);
			syserrrec.params.set(reverserec.reverseRec);
			databaseError99500();
		}*/
		int payNewCount = 0;
		int payNOldCount = 0;
		for (Payrpf payrpf : payrpfs) {
			if ("1".equals(payrpf.getValidflag().trim())) {
				payNewCount++;
				if (payNewCount > 1) {
					syserrrec.statuz.set(Varcom.bomb);
					syserrrec.params.set(reverserec.reverseRec);
					fatalError600();
					break;
				}
				boolean res = payrlifDAO.deletByUnqiue(payrpf.getUnique_number());
				if (!res) {
					syserrrec.statuz.set(Varcom.bomb);
					syserrrec.params.set(reverserec.reverseRec);
					fatalError600();
					break;
				}
				continue;
			}
			if ("2".equals(payrpf.getValidflag().trim()) && payNewCount > 0) {
				/*payNOldCount++;
				if (payNOldCount > 1) {
					syserrrec.statuz.set(Varcom.bomb);
					syserrrec.params.set(reverserec.reverseRec);
					databaseError99500();
					break;
				}*/
				boolean res = payrlifDAO.updateValidflag(payrpf.getUnique_number(), "1");
				if (!res) {
					syserrrec.statuz.set(Varcom.bomb);
					syserrrec.params.set(reverserec.reverseRec);
					fatalError600();
					// break;
				}
				break;
			}
		}
		/*
		 * 
		 */
	/*	payrrevIO.setParams(SPACES);
		payrrevIO.setChdrcoy(reverserec.company);
		payrrevIO.setChdrnum(reverserec.chdrnum);
		payrrevIO.setTranno(reverserec.tranno);
		payrrevIO.setPayrseqno(1);
		payrrevIO.setFormat(payrrevrec);
		payrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(),varcom.oK)
		&& isNE(payrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(payrrevIO.getChdrcoy(),reverserec.company)
		|| isNE(payrrevIO.getChdrnum(),reverserec.chdrnum)
		 || isNE(payrrevIO.getValidflag(),"1")
		|| isEQ(payrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			fatalError600();
		}
		payrrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			fatalError600();
		}
		payrrevIO.setFunction(varcom.nextp);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(),varcom.oK)
		&& isNE(payrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(payrrevIO.getChdrcoy(),reverserec.company)
		|| isNE(payrrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(payrrevIO.getValidflag(),"2")
		|| isEQ(payrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			fatalError600();
		}
		payrrevIO.setValidflag("1");
		payrrevIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			fatalError600();
		}*/
	
	
	}

protected void processCovrs4000()
	{
		begn4010();
	}

protected void begn4010()
	{
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setLife(ZERO);
		covrenqIO.setCoverage(ZERO);
		covrenqIO.setRider(ZERO);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),reverserec.company)
		|| isNE(covrenqIO.getChdrnum(),reverserec.chdrnum)) {
			covrenqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			reverseCovrs4100();
		}
		
	}

protected void reverseCovrs4100()
	{
		check4110();
	}

protected void check4110()
	{
		if (isEQ(covrenqIO.getTranno(),reverserec.tranno)) {
			deleteAndReinstate4200();
			genericProcessing4300();
			checkIncr4500();
		}
		covrenqIO.setFunction(varcom.nextr);
		covrenqIO.setFormat(covrenqrec);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),reverserec.company)
		|| isNE(covrenqIO.getChdrnum(),reverserec.chdrnum)) {
			covrenqIO.setStatuz(varcom.endp);
		}
	}

protected void deleteAndReinstate4200()
	{
		begnh4210();
	}

protected void begnh4210()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(covrenqIO.getChdrcoy());
		covrIO.setChdrnum(covrenqIO.getChdrnum());
		covrIO.setLife(covrenqIO.getLife());
		covrIO.setCoverage(covrenqIO.getCoverage());
		covrIO.setRider(covrenqIO.getRider());
		covrIO.setPlanSuffix(covrenqIO.getPlanSuffix());
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(),covrenqIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),covrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(),covrenqIO.getLife())
		|| isNE(covrIO.getCoverage(),covrenqIO.getCoverage())
		|| isNE(covrIO.getRider(),covrenqIO.getRider())
		|| isNE(covrIO.getPlanSuffix(),covrenqIO.getPlanSuffix())) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		covrIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		reinstateCovr4400();
	}

protected void genericProcessing4300()
	{
		try {
			readr4310();
		}
		catch (GOTOException e){
		}
	}

protected void readr4310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			goTo(GotoLabel.exit4349);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		wsaaBatchkey.set(reverserec.batchkey);
		wsaaOldBatctrcde.set(reverserec.oldBatctrcde);
		greversrec.batckey.set(wsaaBatchkey);
		greversrec.effdate.set(datcon1rec.intDate);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callTrevsubs4350();
		}
	}

protected void callTrevsubs4350()
	{
		/*CALL*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz,varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void reinstateCovr4400()
	{
		nextr4410();
	}

protected void nextr4410()
	{
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(),covrenqIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),covrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(),covrenqIO.getLife())
		|| isNE(covrIO.getCoverage(),covrenqIO.getCoverage())
		|| isNE(covrIO.getRider(),covrenqIO.getRider())
		|| isNE(covrIO.getPlanSuffix(),covrenqIO.getPlanSuffix())) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void checkIncr4500()
	{
		try {
			readh4510();
		}
		catch (GOTOException e){
		}
	}

protected void readh4510()
	{
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(covrenqIO.getChdrcoy());
		incrselIO.setChdrnum(covrenqIO.getChdrnum());
		incrselIO.setLife(covrenqIO.getLife());
		incrselIO.setCoverage(covrenqIO.getCoverage());
		incrselIO.setRider(covrenqIO.getRider());
		incrselIO.setPlanSuffix(covrenqIO.getPlanSuffix());
		incrselIO.setTranno(reverserec.tranno);
		incrselIO.setFunction(varcom.readh);
		incrselIO.setFormat(incrselrec);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)
		&& isNE(incrselIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			fatalError600();
		}
		if (isEQ(incrselIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit4590);
		}
		incrselIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			fatalError600();
		}
	}

protected void processAgcms5000()
	{
		begn5010();
	}

protected void begn5010()
	{
		agcmrvsIO.setParams(SPACES);
		agcmrvsIO.setChdrcoy(reverserec.company);
		agcmrvsIO.setChdrnum(reverserec.chdrnum);
		agcmrvsIO.setTranno(reverserec.tranno);
		agcmrvsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrvsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		agcmrvsIO.setFormat(agcmrvsrec);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(),varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			fatalError600();
		}
		if (isNE(agcmrvsIO.getChdrcoy(),reverserec.company)
		|| isNE(agcmrvsIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(agcmrvsIO.getTranno(),reverserec.tranno)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmrvsIO.getStatuz(),varcom.endp))) {
			deleteReinstateAgcms5100();
		}
		
	}

protected void deleteReinstateAgcms5100()
	{
		begnh5110();
	}

protected void begnh5110()
	{
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFormat(agcmdbcrec);
		agcmdbcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			fatalError600();
		}
		if (isNE(agcmdbcIO.getChdrcoy(),agcmrvsIO.getChdrcoy())
		|| isNE(agcmdbcIO.getChdrnum(),agcmrvsIO.getChdrnum())
		|| isNE(agcmdbcIO.getAgntnum(),agcmrvsIO.getAgntnum())
		|| isNE(agcmdbcIO.getLife(),agcmrvsIO.getLife())
		|| isNE(agcmdbcIO.getCoverage(),agcmrvsIO.getCoverage())
		|| isNE(agcmdbcIO.getRider(),agcmrvsIO.getRider())
		|| isNE(agcmdbcIO.getPlanSuffix(),agcmrvsIO.getPlanSuffix())
		|| isNE(agcmdbcIO.getSeqno(),agcmrvsIO.getSeqno())) {
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			syserrrec.params.set(agcmdbcIO.getParams());
			fatalError600();
		}
		agcmdbcIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			fatalError600();
		}
		agcmdbcIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			fatalError600();
		}
		if (isNE(agcmdbcIO.getChdrcoy(),agcmrvsIO.getChdrcoy())
		|| isNE(agcmdbcIO.getChdrnum(),agcmrvsIO.getChdrnum())
		|| isNE(agcmdbcIO.getAgntnum(),agcmrvsIO.getAgntnum())
		|| isNE(agcmdbcIO.getLife(),agcmrvsIO.getLife())
		|| isNE(agcmdbcIO.getCoverage(),agcmrvsIO.getCoverage())
		|| isNE(agcmdbcIO.getRider(),agcmrvsIO.getRider())
		|| isNE(agcmdbcIO.getPlanSuffix(),agcmrvsIO.getPlanSuffix())
		|| isNE(agcmdbcIO.getSeqno(),agcmrvsIO.getSeqno())) {
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			syserrrec.params.set(agcmdbcIO.getParams());
			fatalError600();
		}
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writd);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			fatalError600();
		}
		agcmrvsIO.setFunction(varcom.nextr);
		agcmrvsIO.setFormat(agcmrvsrec);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(),varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			fatalError600();
		}
		if (isNE(agcmrvsIO.getChdrcoy(),reverserec.company)
		|| isNE(agcmrvsIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(agcmrvsIO.getTranno(),reverserec.tranno)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		reverserec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
