package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr51kscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 21, 20}; 
	public static int maxRecords = 16;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 21, 10, 73}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51kScreenVars sv = (Sr51kScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr51kscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr51kscreensfl, 
			sv.Sr51kscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr51kScreenVars sv = (Sr51kScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr51kscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr51kScreenVars sv = (Sr51kScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr51kscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr51kscreensflWritten.gt(0))
		{
			sv.sr51kscreensfl.setCurrentIndex(0);
			sv.Sr51kscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr51kScreenVars sv = (Sr51kScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr51kscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr51kScreenVars screenVars = (Sr51kScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.erordsc.setFieldName("erordsc");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.payrseqno.setFieldName("payrseqno");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.erordsc.set(dm.getField("erordsc"));
			screenVars.life.set(dm.getField("life"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.payrseqno.set(dm.getField("payrseqno"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr51kScreenVars screenVars = (Sr51kScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.erordsc.setFieldName("erordsc");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.payrseqno.setFieldName("payrseqno");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("erordsc").set(screenVars.erordsc);
			dm.getField("life").set(screenVars.life);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("payrseqno").set(screenVars.payrseqno);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr51kscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr51kScreenVars screenVars = (Sr51kScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.erordsc.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.payrseqno.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr51kScreenVars screenVars = (Sr51kScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.erordsc.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.payrseqno.setClassString("");
	}

/**
 * Clear all the variables in Sr51kscreensfl
 */
	public static void clear(VarModel pv) {
		Sr51kScreenVars screenVars = (Sr51kScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.erordsc.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.payrseqno.clear();
	}
}
