package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sr60t
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class Sr60tScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(200);
	public FixedLengthStringData dataFields = new FixedLengthStringData(136).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnum = DD.ownerSel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData ownername = DD.ownernamez.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData schmno = DD.schemekey.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData schmnme = DD.schemeName.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 136);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);	
	public FixedLengthStringData schmnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData schmnmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 152);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] schmnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] schmnmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(241);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(78).isAPartOf(subfileArea, 0);
	public FixedLengthStringData select = DD.sel.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData clntnumSfl = DD.ownerSel.copy().isAPartOf(subfileFields,9);
	public FixedLengthStringData schmnoSfl = DD.schemekey.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData cnttype = DD.cnttyp.copy().isAPartOf(subfileFields,25);
	public ZonedDecimalData taxPeriodFrom = DD.datefrm.copyToZonedDecimal().isAPartOf(subfileFields,28);
	public ZonedDecimalData taxPeriodTo = DD.dateto.copyToZonedDecimal().isAPartOf(subfileFields,36);
	public FixedLengthStringData statcode = DD.statcde.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(subfileFields,46);
	public ZonedDecimalData totamnt = DD.acntamt.copyToZonedDecimal().isAPartOf(subfileFields,48);
	public FixedLengthStringData dflag = DD.statcde.copy().isAPartOf(subfileFields,65);
	public FixedLengthStringData schdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData scnttype = DD.cnttyp.copy().isAPartOf(subfileFields,75);

	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 78);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData clntnumSflErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData schmnoSflErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData taxPeriodFromErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData taxPeriodToErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData statcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData totamntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 118);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] clntnumSflOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] schmnoSflOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);	
	public FixedLengthStringData[] taxPeriodFromOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] taxPeriodToOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] statcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 238);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	public FixedLengthStringData taxPeriodFromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData taxPeriodToDisp = new FixedLengthStringData(10);
	public LongData Sr60tscreensflWritten = new LongData(0);
	public LongData Sr60tscreenctlWritten = new LongData(0);
	public LongData Sr60tscreenWritten = new LongData(0);
	public LongData Sr60tprotectWritten = new LongData(0);
	public GeneralTable sr60tscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr60tscreensfl;
	}

	public Sr60tScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"02","03", "-02",null, null, null, null, null, null, null, null, null});

		
		screenSflFields = new BaseData[] {select, chdrnum, clntnumSfl, schmnoSfl, cnttype, taxPeriodFrom, taxPeriodTo,statcode, pstatcode, totamnt};
		screenSflOutFields = new BaseData[][] {selectOut, chdrnumOut, clntnumSflOut, schmnoSflOut, cnttypeOut, taxPeriodFromOut, taxPeriodToOut, statcodeOut, pstatcodeOut,totamntOut};
		screenSflErrFields = new BaseData[] {selectErr, chdrnumErr, clntnumSflErr, schmnoSflErr, cnttypeErr, taxPeriodFromErr, taxPeriodToErr, statcodeErr, pstatcodeErr,totamntErr};
		screenSflDateFields = new BaseData[] {taxPeriodFrom, taxPeriodTo};
		screenSflDateErrFields = new BaseData[] {taxPeriodFromErr, taxPeriodToErr};
		screenSflDateDispFields = new BaseData[] {taxPeriodFromDisp, taxPeriodToDisp};

		screenFields = new BaseData[] {clntnum, ownername,schmno,schmnme};
		screenOutFields = new BaseData[][] {clntnumOut, ownernameOut,schmnoOut,schmnmeOut};
		screenErrFields = new BaseData[] {clntnumErr, ownernameErr,schmnoErr,schmnmeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr60tscreen.class;
		screenSflRecord = Sr60tscreensfl.class;
		screenCtlRecord = Sr60tscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr60tprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr60tscreenctl.lrec.pageSubfile);
	}
}
