/*
 * File: Annyout.java
 * Date: 29 August 2009 20:14:42
 * Author: Quipoz Limited
 * 
 * Class transformed from ANNYOUT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.recordstructures.Genoutrec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnnylnbTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* OVERVIEW
*
* This is  a  new  subroutine  which  forms  part  of  the  9405
* Annuities Development.  It will be called from T5671 by BRKOUT
* processing.
*
* If  a  transaction  causes  Break Out to occur this subroutine
* will Break Out the  ANNY  records  in  the  same  way  as  the
* coverage records to which they correspond are broken out.  The
* only  field which needs to be broken down in proportion is the
* Capital Content field.
*
*
*****************************************************************
* </pre>
*/
public class Annyout extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ANNYOUT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FLAGS-TOTALS-COUNTS-ETC */
	private PackedDecimalData wsaaOriginalCapcont = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBreakoutCapcont = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalBreakCapcont = new PackedDecimalData(17, 2);
		/* TEMPORARY-STORAGE-AREAS */
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
		/* FORMATS */
	private static final String annyrec = "ANNYREC";
	private static final String annylnbrec = "ANNYLNBREC";
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
		/*Annuity Details Life New Business*/
	private AnnylnbTableDAM annylnbIO = new AnnylnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Genoutrec genoutrec = new Genoutrec();

	public Annyout() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		genoutrec.outRec = convertAndSetParam(genoutrec.outRec, parmArray, 0);
		try {
			control0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control0000()
	{
		/*PARA*/
		initialize1000();
		/* Process the ANNY file:*/
		while ( !(isEQ(annyIO.getStatuz(),varcom.endp))) {
			writeBreakoutAnny2000();
		}
		
		/*EXIT*/
		exitProgram();
	}

protected void initialize1000()
	{
		para1001();
	}

protected void para1001()
	{
		genoutrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		/* initialize the  working storage used in the*/
		/* breakout calculation*/
		wsaaOriginalCapcont.set(ZERO);
		wsaaBreakoutCapcont.set(ZERO);
		wsaaTotalBreakCapcont.set(ZERO);
		/* FIRST read of the ANNY file. NB: the ANNY physical drives*/
		/* the Iteration. But within the iteration the ANNYLNB logical*/
		/* is written to. This ensures that when the ANNY is written /*/
		/* rewritten later, the position hasn't been lost.*/
		annyIO.setDataArea(SPACES);
		annyIO.setChdrnum(genoutrec.chdrnum);
		annyIO.setChdrcoy(genoutrec.chdrcoy);
		annyIO.setPlanSuffix(ZERO);
		annyIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
		if (isNE(annyIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(annyIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(annyIO.getPlanSuffix(),ZERO)
		|| isEQ(annyIO.getStatuz(),varcom.endp)) {
			exitProgram();
		}
	}

protected void writeBreakoutAnny2000()
	{
			para2000();
		}

protected void para2000()
	{
		/* initialize the  working storage used in the*/
		/* breakout calculation*/
		wsaaOriginalCapcont.set(ZERO);
		wsaaBreakoutCapcont.set(ZERO);
		wsaaTotalBreakCapcont.set(ZERO);
		wsaaOriginalCapcont.set(annyIO.getCapcont());
		for (wsaaPlanSuffix.set(genoutrec.oldSummary); !(isEQ(wsaaPlanSuffix,genoutrec.newSummary)); wsaaPlanSuffix.add(-1)){
			writeAnnylnbRecord2100();
		}
		calcNewSummaryRecAnny2500();
		/* This NEXTR is outside the iteration since the condition*/
		/* 'ANNY-PLAN-SUFFIX = 0' will always be true unless the*/
		/* next one is read.*/
		annyIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, annyIO);
		if ((isNE(annyIO.getStatuz(),varcom.oK))
		&& (isNE(annyIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(),varcom.endp)) {
			return ;
			/****     GO TO 2909-EXIT                                           */
		}
		while ( !(isEQ(annyIO.getPlanSuffix(),ZERO)
		|| isEQ(annyIO.getStatuz(),varcom.endp))) {
			getNextRecord2900();
		}
		
	}

protected void writeAnnylnbRecord2100()
	{
		/*PARA*/
		if (isEQ(wsaaOriginalCapcont,ZERO)) {
			wsaaOriginalCapcont.set(ZERO);
		}
		else {
			compute(wsaaBreakoutCapcont, 3).setRounded(div(wsaaOriginalCapcont,genoutrec.oldSummary));
			zrdecplrec.amountIn.set(wsaaBreakoutCapcont);
			a000CallRounding();
			wsaaBreakoutCapcont.set(zrdecplrec.amountOut);
			compute(wsaaTotalBreakCapcont, 3).setRounded(add(wsaaTotalBreakCapcont,wsaaBreakoutCapcont));
		}
		annylnbIO.setDataArea(SPACES);
		annylnbIO.setCapcont(wsaaBreakoutCapcont);
		writeBreakoutAnny2110();
		/*EXIT*/
	}

protected void writeBreakoutAnny2110()
	{
		write2110();
	}

protected void write2110()
	{
		/* Move the ANNY values to the ANNYLNB logical and write a*/
		/* new record, corrosponding with a new broken out record:*/
		annylnbIO.setChdrcoy(annyIO.getChdrcoy());
		annylnbIO.setChdrnum(annyIO.getChdrnum());
		annylnbIO.setLife(annyIO.getLife());
		annylnbIO.setCoverage(annyIO.getCoverage());
		annylnbIO.setRider(annyIO.getRider());
		annylnbIO.setPlanSuffix(wsaaPlanSuffix);
		annylnbIO.setGuarperd(annyIO.getGuarperd());
		annylnbIO.setFreqann(annyIO.getFreqann());
		annylnbIO.setArrears(annyIO.getArrears());
		annylnbIO.setAdvance(annyIO.getAdvance());
		annylnbIO.setDthpercn(annyIO.getDthpercn());
		annylnbIO.setDthperco(annyIO.getDthperco());
		annylnbIO.setIntanny(annyIO.getIntanny());
		annylnbIO.setWithprop(annyIO.getWithprop());
		annylnbIO.setWithoprop(annyIO.getWithoprop());
		annylnbIO.setPpind(annyIO.getPpind());
		annylnbIO.setNomlife(annyIO.getNomlife());
		annylnbIO.setValidflag(annyIO.getValidflag());
		annylnbIO.setTranno(annyIO.getTranno());
		annylnbIO.setFunction(varcom.writr);
		annylnbIO.setFormat(annylnbrec);
		SmartFileCode.execute(appVars, annylnbIO);
		if (isNE(annylnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annylnbIO.getParams());
			fatalError600();
		}
	}

protected void calcNewSummaryRecAnny2500()
	{
		read2500();
	}

protected void read2500()
	{
		/* If GEN-NEW-SUMMARY is 1 or 0 will want to delete*/
		/* summary ANNY.*/
		/*   NB: Use ANNY since this is still set on the Original*/
		/* read (i.e. Un-brokenout one). Cannot use ANNYLNB as the*/
		/* interviening writes have selcted a different record.*/
		if (isLT(genoutrec.newSummary,2)) {
			annyIO.setFunction(varcom.delet);
			annyIO.setFormat(annyrec);
			SmartFileCode.execute(appVars, annyIO);
			if (isNE(annyIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(annyIO.getParams());
				fatalError600();
			}
		}
		/* For the summary, we are interested in the remaining*/
		/* CAPCONT after all the parts have been broken out.*/
		/* For the case of the last broken out record (where*/
		/* GEN-NEW-SUMMARY < 2) this represents is used as well*/
		/* since when the ORIGINAL-CAPCONT was divided up there*/
		/* might be some small rounding errors, so the program will*/
		/* componsate for these by moving the remainder in.*/
		/* If GEN-NEW-SUMMARY is 1 then we have broken out the*/
		/* last record, otherwise we rewrite the summary:*/
		if (isLT(genoutrec.newSummary,2)) {
			wsaaPlanSuffix.set(1);
			annylnbIO.setDataArea(SPACES);
			setPrecision(annylnbIO.getCapcont(), 3);
			annylnbIO.setCapcont(sub(wsaaOriginalCapcont,wsaaTotalBreakCapcont), true);
			writeBreakoutAnny2110();
		}
		else {
			setPrecision(annyIO.getCapcont(), 3);
			annyIO.setCapcont(sub(wsaaOriginalCapcont,wsaaTotalBreakCapcont), true);
			annyIO.setFormat(annyrec);
			annyIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, annyIO);
			if (isNE(annyIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(annyIO.getParams());
				fatalError600();
			}
		}
	}

protected void getNextRecord2900()
	{
			gets2900();
		}

protected void gets2900()
	{
		/* The reason an iteration is required is that since there*/
		/* have been REWRT's on ANNYLNB the NEXTR 'pointer' has been*/
		/* altered and it will read the wrong record. The record we*/
		/* require is the one where PLAN-SUFFIX = 0, i.e. the next*/
		/* one:*/
		annyIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, annyIO);
		if ((isNE(annyIO.getStatuz(),varcom.oK))
		&& (isNE(annyIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(annyIO.getParams());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(),varcom.endp)) {
			return ;
		}
		/* A rewrite is neccesary to releas the BEGNH:*/
		if (isNE(annyIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(annyIO.getChdrcoy(),genoutrec.chdrcoy)) {
			annyIO.setFunction(varcom.rewrt);
			annyIO.setFormat(annyrec);
			SmartFileCode.execute(appVars, annyIO);
			if (isNE(annyIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(annyIO.getParams());
				fatalError600();
			}
			else {
				annyIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isNE(annyIO.getChdrnum(),genoutrec.chdrnum)
		|| isNE(annyIO.getChdrcoy(),genoutrec.chdrcoy)
		|| isNE(annyIO.getPlanSuffix(),ZERO)) {
			annyIO.setFunction(varcom.rewrt);
			annyIO.setFormat(annyrec);
			SmartFileCode.execute(appVars, annyIO);
			if (isNE(annyIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(annyIO.getParams());
				fatalError600();
			}
		}
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(genoutrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(genoutrec.cntcurr);
		zrdecplrec.batctrcde.set(genoutrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A090-EXIT*/
	}

protected void fatalError600()
	{
					error600();
					exit602();
				}

protected void error600()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		genoutrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
