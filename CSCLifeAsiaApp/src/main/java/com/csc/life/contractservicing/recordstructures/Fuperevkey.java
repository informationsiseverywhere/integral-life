package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:48
 * Description:
 * Copybook name: FUPEREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fuperevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fuperevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData fuperevKey = new FixedLengthStringData(256).isAPartOf(fuperevFileKey, 0, REDEFINE);
  	public FixedLengthStringData fuperevChdrcoy = new FixedLengthStringData(1).isAPartOf(fuperevKey, 0);
  	public FixedLengthStringData fuperevChdrnum = new FixedLengthStringData(8).isAPartOf(fuperevKey, 1);
  	public PackedDecimalData fuperevTranno = new PackedDecimalData(5, 0).isAPartOf(fuperevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(fuperevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fuperevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fuperevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}