package com.csc.life.contractservicing.dataaccess;

import com.csc.life.terminationclaims.dataaccess.RegppfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RegprevTableDAM.java
 * Date: Sun, 30 Aug 2009 03:45:57
 * Class transformed from REGPREV.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RegprevTableDAM extends RegppfTableDAM {

	public RegprevTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("REGPREV");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", RGPYNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "PLNSFX, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "RGPYNUM, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "SACSCODE, " +
		            "SACSTYPE, " +
		            "GLACT, " +
		            "DEBCRED, " +
		            "DESTKEY, " +
		            "PAYCLT, " +
		            "RGPYMOP, " +
		            "REGPAYFREQ, " +
		            "CURRCD, " +
		            "PYMT, " +
		            "PRCNT, " +
		            "TOTAMNT, " +
		            "RGPYSTAT, " +
		            "PAYREASON, " +
		            "CLAIMEVD, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "CRTDATE, " +
		            "APRVDATE, " +
		            "FPAYDATE, " +
		            "NPAYDATE, " +
		            "REVDTE, " +
		            "LPAYDATE, " +
		            "EPAYDATE, " +
		            "ANVDATE, " +
		            "CANCELDATE, " +
		            "RGPYTYPE, " +
		            "CRTABLE, " +
		            "CERTDATE, " +
		            "TERMID, " +
		            "CLAMPARTY, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "PAYCOY, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "RGPYNUM ASC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "RGPYNUM DESC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               planSuffix,
                               life,
                               coverage,
                               rider,
                               rgpynum,
                               validflag,
                               tranno,
                               sacscode,
                               sacstype,
                               glact,
                               debcred,
                               destkey,
                               payclt,
                               rgpymop,
                               regpayfreq,
                               currcd,
                               pymt,
                               prcnt,
                               totamnt,
                               rgpystat,
                               payreason,
                               claimevd,
                               bankkey,
                               bankacckey,
                               crtdate,
                               aprvdate,
                               firstPaydate,
                               nextPaydate,
                               revdte,
                               lastPaydate,
                               finalPaydate,
                               anvdate,
                               cancelDate,
                               rgpytype,
                               crtable,
                               certdate,
                               termid,
                               clamparty,
                               transactionDate,
                               transactionTime,
                               user,
                               paycoy,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(43);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getRgpynum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, rgpynum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller40.setInternal(life.toInternal());
	nonKeyFiller50.setInternal(coverage.toInternal());
	nonKeyFiller60.setInternal(rider.toInternal());
	nonKeyFiller70.setInternal(rgpynum.toInternal());
	nonKeyFiller90.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(269);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getPlanSuffix().toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getValidflag().toInternal()
					+ nonKeyFiller90.toInternal()
					+ getSacscode().toInternal()
					+ getSacstype().toInternal()
					+ getGlact().toInternal()
					+ getDebcred().toInternal()
					+ getDestkey().toInternal()
					+ getPayclt().toInternal()
					+ getRgpymop().toInternal()
					+ getRegpayfreq().toInternal()
					+ getCurrcd().toInternal()
					+ getPymt().toInternal()
					+ getPrcnt().toInternal()
					+ getTotamnt().toInternal()
					+ getRgpystat().toInternal()
					+ getPayreason().toInternal()
					+ getClaimevd().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getCrtdate().toInternal()
					+ getAprvdate().toInternal()
					+ getFirstPaydate().toInternal()
					+ getNextPaydate().toInternal()
					+ getRevdte().toInternal()
					+ getLastPaydate().toInternal()
					+ getFinalPaydate().toInternal()
					+ getAnvdate().toInternal()
					+ getCancelDate().toInternal()
					+ getRgpytype().toInternal()
					+ getCrtable().toInternal()
					+ getCertdate().toInternal()
					+ getTermid().toInternal()
					+ getClamparty().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getPaycoy().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstype);
			what = ExternalData.chop(what, glact);
			what = ExternalData.chop(what, debcred);
			what = ExternalData.chop(what, destkey);
			what = ExternalData.chop(what, payclt);
			what = ExternalData.chop(what, rgpymop);
			what = ExternalData.chop(what, regpayfreq);
			what = ExternalData.chop(what, currcd);
			what = ExternalData.chop(what, pymt);
			what = ExternalData.chop(what, prcnt);
			what = ExternalData.chop(what, totamnt);
			what = ExternalData.chop(what, rgpystat);
			what = ExternalData.chop(what, payreason);
			what = ExternalData.chop(what, claimevd);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, crtdate);
			what = ExternalData.chop(what, aprvdate);
			what = ExternalData.chop(what, firstPaydate);
			what = ExternalData.chop(what, nextPaydate);
			what = ExternalData.chop(what, revdte);
			what = ExternalData.chop(what, lastPaydate);
			what = ExternalData.chop(what, finalPaydate);
			what = ExternalData.chop(what, anvdate);
			what = ExternalData.chop(what, cancelDate);
			what = ExternalData.chop(what, rgpytype);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, certdate);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, clamparty);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, paycoy);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getRgpynum() {
		return rgpynum;
	}
	public void setRgpynum(Object what) {
		setRgpynum(what, false);
	}
	public void setRgpynum(Object what, boolean rounded) {
		if (rounded)
			rgpynum.setRounded(what);
		else
			rgpynum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getSacstype() {
		return sacstype;
	}
	public void setSacstype(Object what) {
		sacstype.set(what);
	}	
	public FixedLengthStringData getGlact() {
		return glact;
	}
	public void setGlact(Object what) {
		glact.set(what);
	}	
	public FixedLengthStringData getDebcred() {
		return debcred;
	}
	public void setDebcred(Object what) {
		debcred.set(what);
	}	
	public FixedLengthStringData getDestkey() {
		return destkey;
	}
	public void setDestkey(Object what) {
		destkey.set(what);
	}	
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	public void setPayclt(Object what) {
		payclt.set(what);
	}	
	public FixedLengthStringData getRgpymop() {
		return rgpymop;
	}
	public void setRgpymop(Object what) {
		rgpymop.set(what);
	}	
	public FixedLengthStringData getRegpayfreq() {
		return regpayfreq;
	}
	public void setRegpayfreq(Object what) {
		regpayfreq.set(what);
	}	
	public FixedLengthStringData getCurrcd() {
		return currcd;
	}
	public void setCurrcd(Object what) {
		currcd.set(what);
	}	
	public PackedDecimalData getPymt() {
		return pymt;
	}
	public void setPymt(Object what) {
		setPymt(what, false);
	}
	public void setPymt(Object what, boolean rounded) {
		if (rounded)
			pymt.setRounded(what);
		else
			pymt.set(what);
	}	
	public PackedDecimalData getPrcnt() {
		return prcnt;
	}
	public void setPrcnt(Object what) {
		setPrcnt(what, false);
	}
	public void setPrcnt(Object what, boolean rounded) {
		if (rounded)
			prcnt.setRounded(what);
		else
			prcnt.set(what);
	}	
	public PackedDecimalData getTotamnt() {
		return totamnt;
	}
	public void setTotamnt(Object what) {
		setTotamnt(what, false);
	}
	public void setTotamnt(Object what, boolean rounded) {
		if (rounded)
			totamnt.setRounded(what);
		else
			totamnt.set(what);
	}	
	public FixedLengthStringData getRgpystat() {
		return rgpystat;
	}
	public void setRgpystat(Object what) {
		rgpystat.set(what);
	}	
	public FixedLengthStringData getPayreason() {
		return payreason;
	}
	public void setPayreason(Object what) {
		payreason.set(what);
	}	
	public FixedLengthStringData getClaimevd() {
		return claimevd;
	}
	public void setClaimevd(Object what) {
		claimevd.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public PackedDecimalData getCrtdate() {
		return crtdate;
	}
	public void setCrtdate(Object what) {
		setCrtdate(what, false);
	}
	public void setCrtdate(Object what, boolean rounded) {
		if (rounded)
			crtdate.setRounded(what);
		else
			crtdate.set(what);
	}	
	public PackedDecimalData getAprvdate() {
		return aprvdate;
	}
	public void setAprvdate(Object what) {
		setAprvdate(what, false);
	}
	public void setAprvdate(Object what, boolean rounded) {
		if (rounded)
			aprvdate.setRounded(what);
		else
			aprvdate.set(what);
	}	
	public PackedDecimalData getFirstPaydate() {
		return firstPaydate;
	}
	public void setFirstPaydate(Object what) {
		setFirstPaydate(what, false);
	}
	public void setFirstPaydate(Object what, boolean rounded) {
		if (rounded)
			firstPaydate.setRounded(what);
		else
			firstPaydate.set(what);
	}	
	public PackedDecimalData getNextPaydate() {
		return nextPaydate;
	}
	public void setNextPaydate(Object what) {
		setNextPaydate(what, false);
	}
	public void setNextPaydate(Object what, boolean rounded) {
		if (rounded)
			nextPaydate.setRounded(what);
		else
			nextPaydate.set(what);
	}	
	public PackedDecimalData getRevdte() {
		return revdte;
	}
	public void setRevdte(Object what) {
		setRevdte(what, false);
	}
	public void setRevdte(Object what, boolean rounded) {
		if (rounded)
			revdte.setRounded(what);
		else
			revdte.set(what);
	}	
	public PackedDecimalData getLastPaydate() {
		return lastPaydate;
	}
	public void setLastPaydate(Object what) {
		setLastPaydate(what, false);
	}
	public void setLastPaydate(Object what, boolean rounded) {
		if (rounded)
			lastPaydate.setRounded(what);
		else
			lastPaydate.set(what);
	}	
	public PackedDecimalData getFinalPaydate() {
		return finalPaydate;
	}
	public void setFinalPaydate(Object what) {
		setFinalPaydate(what, false);
	}
	public void setFinalPaydate(Object what, boolean rounded) {
		if (rounded)
			finalPaydate.setRounded(what);
		else
			finalPaydate.set(what);
	}	
	public PackedDecimalData getAnvdate() {
		return anvdate;
	}
	public void setAnvdate(Object what) {
		setAnvdate(what, false);
	}
	public void setAnvdate(Object what, boolean rounded) {
		if (rounded)
			anvdate.setRounded(what);
		else
			anvdate.set(what);
	}	
	public PackedDecimalData getCancelDate() {
		return cancelDate;
	}
	public void setCancelDate(Object what) {
		setCancelDate(what, false);
	}
	public void setCancelDate(Object what, boolean rounded) {
		if (rounded)
			cancelDate.setRounded(what);
		else
			cancelDate.set(what);
	}	
	public FixedLengthStringData getRgpytype() {
		return rgpytype;
	}
	public void setRgpytype(Object what) {
		rgpytype.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getCertdate() {
		return certdate;
	}
	public void setCertdate(Object what) {
		setCertdate(what, false);
	}
	public void setCertdate(Object what, boolean rounded) {
		if (rounded)
			certdate.setRounded(what);
		else
			certdate.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public FixedLengthStringData getClamparty() {
		return clamparty;
	}
	public void setClamparty(Object what) {
		clamparty.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getPaycoy() {
		return paycoy;
	}
	public void setPaycoy(Object what) {
		paycoy.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		rgpynum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		planSuffix.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		validflag.clear();
		nonKeyFiller90.clear();
		sacscode.clear();
		sacstype.clear();
		glact.clear();
		debcred.clear();
		destkey.clear();
		payclt.clear();
		rgpymop.clear();
		regpayfreq.clear();
		currcd.clear();
		pymt.clear();
		prcnt.clear();
		totamnt.clear();
		rgpystat.clear();
		payreason.clear();
		claimevd.clear();
		bankkey.clear();
		bankacckey.clear();
		crtdate.clear();
		aprvdate.clear();
		firstPaydate.clear();
		nextPaydate.clear();
		revdte.clear();
		lastPaydate.clear();
		finalPaydate.clear();
		anvdate.clear();
		cancelDate.clear();
		rgpytype.clear();
		crtable.clear();
		certdate.clear();
		termid.clear();
		clamparty.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		paycoy.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}