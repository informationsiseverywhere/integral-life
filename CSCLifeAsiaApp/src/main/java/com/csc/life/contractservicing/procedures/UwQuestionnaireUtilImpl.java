package com.csc.life.contractservicing.procedures;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.dip.jvpms.web.VPMScaller;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.life.underwriting.tablestructures.T6770rec;
import com.csc.life.underwriting.tablestructures.T6772rec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.AppVars;

public class UwQuestionnaireUtilImpl implements UwQuestionnaireUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(UwoccupationUtilImpl.class);
	private static final String UWQUESTIONNAIRE = "UWQUESTIONNAIRE";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, Itempf> t5448Map;
	private Map<String, Itempf> th618Map;
	private T6770rec t6770rec = new T6770rec();
	private T6772rec t6772rec = new T6772rec();
	private void initSmartable(String company){
		t5448Map = itempfDAO.getItemMap("IT", company, "T5448");
		th618Map = itempfDAO.getItemMap("IT", company, "TH618");
	}
	@Override
	public UWQuestionnaireRec getUWMessage(UWQuestionnaireRec uwQuestionnaireRec, String language,String company,String question,String crtable,String reasondCode){
		initSmartable(company);
		ExternalisedRules er = new ExternalisedRules();
		List<String> riskClass = new ArrayList<String>();
		List<String> splTermList = new ArrayList<String>();
		if (AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(UWQUESTIONNAIRE)) {
			FixedLengthStringData[] lrkcls = getRiskClass(uwQuestionnaireRec.getContractType(), crtable);
			for (FixedLengthStringData rkcls : lrkcls) {
				if (rkcls != null && !rkcls.toString().trim().isEmpty()) {
					riskClass.add(rkcls.toString());
				}
			}
			if(!reasondCode.equals("") && question.equals("")){
				splTermList.add(reasondCode);
			}else{
				FixedLengthStringData[] splTerms = getSplTerms(company,language, question);
//				for (FixedLengthStringData splTerm : splTerms) {
//					if (splTerm != null && !splTerm.toString().trim().isEmpty()) {
//						splTermList.add(splTerm.toString());
//					}
//				}
				splTermList = getsplTerm(splTerms);
			}
			return callVpms(uwQuestionnaireRec,riskClass,splTermList,crtable);

		}
		return null;
	}
	private List<String> getsplTerm(FixedLengthStringData[] splTerms){
		List<String> splTermList = new ArrayList<String>();
		for (FixedLengthStringData splTerm : splTerms) {
			if (splTerm != null && !splTerm.toString().trim().isEmpty()) {
				splTermList.add(splTerm.toString());
			}
		}
		return splTermList;
	}
	private UWQuestionnaireRec callVpms(UWQuestionnaireRec uwQuestionnaireRec,List<String> riskClass,
			List<String> splTermList,String crtable){
		UWQuestionnaireRec calcUWrec = new UWQuestionnaireRec();
		calcUWrec.setContractType(uwQuestionnaireRec.getContractType());
		calcUWrec.setTransEffdate(uwQuestionnaireRec.getTransEffdate());
		if(!riskClass.isEmpty() && !splTermList.isEmpty() && riskClass.size() < splTermList.size()){
			for(int i = 0 ; i<(splTermList.size() - riskClass.size()); i++){
				riskClass.add(riskClass.get(0));
			}
		}
		calcUWrec.setRiskClass(riskClass);
		calcUWrec.setSplTerm(splTermList);
		calcUWrec.setCovgCode(crtable);
		vpmsCall(UWQUESTIONNAIRE, calcUWrec);
		return calcUWrec;
	}
	private FixedLengthStringData[] getRiskClass(String cnttype, String crtable) {
		String t5488item = cnttype + crtable;
		if (t5448Map != null && t5448Map.containsKey(t5488item)) {
			Itempf t5488 = t5448Map.get(t5488item);
			T5448rec t5448Rec = new T5448rec();
			t5448Rec.t5448Rec.set(StringUtil.rawToString(t5488.getGenarea()));
			String rsktyp = t5448Rec.rrsktyp.toString();
			if (th618Map != null && th618Map.containsKey(rsktyp)) {
				Itempf th618 = th618Map.get(rsktyp);
				Th618rec th618rec = new Th618rec();
				th618rec.th618Rec.set(StringUtil.rawToString(th618.getGenarea()));
				return th618rec.lrkcls;
			}
		}
		return new FixedLengthStringData[0];
	}
	
	private FixedLengthStringData[] getSplTerms(String company,String language,String question) {
		String t6770Item = language.trim().concat(question);
		List<Itempf> t6770List = itempfDAO.readItempf("IT",company,"T6770",t6770Item);
		if (!t6770List.isEmpty()) {
			t6770rec.t6770Rec.set(StringUtil.rawToString(t6770List.get(0).getGenarea()));
			List<Itempf> t6772List = itempfDAO.readItempf("IT",company,"T6772",t6770rec.undwrule.toString().trim());
			if (!t6772List.isEmpty()) {
				t6772rec.t6772Rec.set(StringUtil.rawToString(t6772List.get(0).getGenarea()));
				return t6772rec.opcda;
			}
		}
		return new FixedLengthStringData[0];
	} 
	
	private int vpmsCall(String whatToCallName, UWQuestionnaireRec calcUWrec) {

		LOGGER.info("CALLING VPMS FOR ={}", whatToCallName);
		VPMScaller caller = new VPMScaller(0, whatToCallName, calcUWrec);
		caller.mapRequestToBean(calcUWrec, 0);
		int status = caller.callVpms();
		LOGGER.info("VPMS RETURN STATUS={}", status);
		if (status == 0) {
			caller.mapResponseToBean(calcUWrec, 0);
		}
		if (status == 1) {
			caller.getErrorCode(calcUWrec);
		}
		return (status);
	}
	private ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}
}
