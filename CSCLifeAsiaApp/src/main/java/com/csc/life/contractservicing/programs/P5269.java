/*
 * File: P5269.java
 * Date: 30 August 2009 0:23:11
 * Author: Quipoz Limited
 * 
 * Class transformed from P5269.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.procedures.Trcdechk;
import com.csc.life.contractservicing.screens.S5269ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tranchkrec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.FlupclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmdpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.life.terminationclaims.dataaccess.dao.CrsvpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* Claims Reversal
*
*  This transaction, Claims Reversal is selected from the
*  Claims sub-menu S6319/P6319.
*
*     Initialise
*     ----------
*
*  Read the  Contract  header  (function  RETRV)  and  read
*  the relevant data necessary for obtaining the status
*  description, the Owner, CCD, Paid-to-date and the
*  Bill-to-date.
*
*  Read the Claim Header record (CLMHCLM) for this contract.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the life assured and joint-life details (if any)
*  do the following;-
*
*  - Obtain  the  life  details  using LIFECLM,(READR) (for
*    this  contract  number, using the life number/joint
*    life  number  from the CLMHCLM record).  Format the
*    name accordingly.
*
*  - Obtain  the  joint-life  details using LIFECLM (READR)
*    (for   this   contract   number,   using  the  life
*    number/joint  life number from the CHLMCLM record).
*    Format the name accordingly.
*
*  - Indicate the 'dead' life by a highlighted 'asterisk'.
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Format Name
*
*  Read the  client  details  record  and  use the relevant
*  copybook in order to format the required names.
*
*  Build the Coverage/Rider review details.
*
*  Read the  Claim  details  record  (CLMDCLM) and output a
*  detail line,  for each record for this contract, to
*  the subfile and display the entries.
*
*  Sum the  Estimated  values  and  the  Actual amounts and
*  display them on the screen.
*
*  From the  Claim  header display the following details on
*  the screen:
*
*                - date of death
*                - effective date
*                - cause of death
*                - follow-ups
*                - adjustment reason code
*                - policy loan amount
*                - other adjustments amount
*                - currency
*                - adjustment reason code
*                - adjustment reason description
*
*  The above details are returned  to  the user and  if  he
*  wishes to continue and perform the Reversal  he  presses
*  Enter, otherwise he opts for  KILL  to  exit  from  this
*  transaction.
*
*     Validation
*     ----------
*
*  If  KILL  was  entered,  then  skip the remainder of the
*  validation and exit from the program.
*
*  No validation occurs, skip this section.
*
*     Updating
*     --------
*
*      If the KILL  function  key  was  pressed, skip the
*      updating and exit from the program.
*
*  Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
*  Call the AT  submission module to submit P5269AT, passing
*  the Contract  number, as the 'primary  key',  this
*  performs  the Claims Reversal transaction.
*
*  Next Program
*  ------------
*
*  For KILL or 'Enter', add 1 to the program pointer and exit.
*
*
*  Modifications
*  -------------
*
*  Rewrite P5269 with the above processing included.
*
*  Include the AT module P5269AT.
*
*  Include  the  following logical views (access only the
*  fields required).
*
*            - CHDRCLM
*            - LIFECLM
*            - COVRCLM
*            - CLMHCLM
*            - CLMDCLM
*
*****************************************************************
* </pre>
*/
public class P5269 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5269");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaDeadLife = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaRegisterTranCode = new FixedLengthStringData(4);
	private String wsaaTranError = "";
		/* ERRORS */
	private static final String f910 = "F910";
	private static final String e304 = "E304";
	private static final String h093 = "H093";
	private static final String i034 = "I034";
	private BinaryData wsaaEstMatValue = new BinaryData(17, 2).init(ZERO);
	private BinaryData wsaaActvalue = new BinaryData(17, 2).init(ZERO);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t6661 = "T6661";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String cltsrec = "CLTSREC";
	private static final String tpoldbtrec = "TPOLDBTREC";
	protected ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FlupclmTableDAM flupclmIO = new FlupclmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Gensswrec gensswrec = new Gensswrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Tranchkrec tranchkrec = new Tranchkrec();
	private T6661rec t6661rec = new T6661rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5269ScreenVars sv = getLScreenVars();//ScreenProgram.getScreenVars( S5269ScreenVars.class);
	private WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	protected Cattpf cattpf = new Cattpf();	
	//ILJ-49 End
	private CrsvpfDAO crsvpfDAO = getApplicationContext().getBean("crsvpfDAO", CrsvpfDAO.class);
	boolean CMDTH010Permission = false;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1030, 
		exit1090, 
	}

	public P5269() {
		super();
		screenVars = sv;
		new ScreenModel("S5269", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}
protected S5269ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(S5269ScreenVars.class);
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case continue1030: 
					continue1030();
					readPtrn1050();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", "CMDTH010", appVars, "IT");//ILJ-383
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*    Initialse this calculation field as it is used *             */
		/*    within the program.                            *             */
		wsaaEstMatValue.set(ZERO);
		wsaaActvalue.set(ZERO);
		wsaaTranError = "N";
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S5269", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Dummy field initilisation for prototype version.*/
		sv.clamamt.set(ZERO);
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.actvalue.set(ZERO);
		sv.estMatValue.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.dtofdeath.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		chdrclmIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}
		sv.occdate.set(chdrclmIO.getOccdate());
		sv.chdrnum.set(chdrclmIO.getChdrnum());
		sv.cnttype.set(chdrclmIO.getCnttype());
		descIO.setDescitem(chdrclmIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrclmIO.getCownnum());
		cltsIO.setClntnum(chdrclmIO.getCownnum());
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrclmIO.getBtdate());
		sv.ptdate.set(chdrclmIO.getPtdate());
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrclmIO.getStatcode());
		findDesc1300();
		/*ILIFE-965 start sgadkari*/
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getLongdesc());
		}
		/*ILIFE-965 end*/
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrclmIO.getPstatcode());
		findDesc1300();
		/*ILIFE-965 start sgadkari*/
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getLongdesc());
		}
		/*ILIFE-965 end*/
		else {
			sv.pstate.fill("?");
		}
		/*MOVE SPACES                 TO CLMHCLM-DATA-AREA.            */
		clmhclmIO.setParams(SPACES);
		clmhclmIO.setChdrcoy(chdrclmIO.getChdrcoy());
		clmhclmIO.setChdrnum(chdrclmIO.getChdrnum());
		clmhclmIO.setFunction("READR");
		clmhclmIO.setFormat(clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
		sv.dtofdeath.set(clmhclmIO.getDtofdeath());
		sv.effdate.set(clmhclmIO.getEffdate());
		sv.causeofdth.set(clmhclmIO.getCauseofdth());
		sv.reasoncd.set(clmhclmIO.getReasoncd());
		sv.resndesc.set(clmhclmIO.getResndesc());
		sv.policyloan.set(clmhclmIO.getPolicyloan());
		sv.zrcshamt.set(clmhclmIO.getZrcshamt());
		sv.otheradjst.set(clmhclmIO.getOtheradjst());
		sv.currcd.set(clmhclmIO.getCurrcd());
		if(!CMDTH010Permission) {
			sv.claimnumberOut[varcom.nd.toInt()].set("Y");
		}
		if(CMDTH010Permission) {
			cattpf = cattpfDAO.selectRecords(chdrclmIO.getChdrcoy().toString(),chdrclmIO.getChdrnum().toString());
			if(null == cattpf){
				syserrrec.params.set(chdrclmIO.getChdrcoy().toString()+ chdrclmIO.getChdrnum().toString());
				fatalError600();	
			}
			else{
				sv.claimnumber.set(cattpf.getClaim());
			}
		}
		/*    Read the first life as the life retrieved above*/
		/*    may have been a joint life.*/
		wsaaJlife.set(clmhclmIO.getJlife());
		/*MOVE SPACES                 TO LIFECLM-DATA-AREA.            */
		lifeclmIO.setParams(SPACES);
		lifeclmIO.setChdrcoy(clmhclmIO.getChdrcoy());
		lifeclmIO.setChdrnum(clmhclmIO.getChdrnum());
		lifeclmIO.setLife(clmhclmIO.getLife());
		lifeclmIO.setJlife("00");
		lifeclmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeclmIO);
		if (isNE(lifeclmIO.getStatuz(), varcom.oK)
		&& isNE(lifeclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company, lifeclmIO.getChdrcoy())
		|| isNE(chdrclmIO.getChdrnum(), lifeclmIO.getChdrnum())
		|| isNE(lifeclmIO.getJlife(), "00")
		&& isNE(lifeclmIO.getJlife(), "  ")
		|| isEQ(lifeclmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		/* Move the last record read to the screen and get the*/
		/* description.*/
		sv.lifcnum.set(lifeclmIO.getLifcnum());
		wsaaDeadLife.set(lifeclmIO.getLifcnum());
		cltsIO.setClntnum(lifeclmIO.getLifcnum());
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		/*    look for joint life.*/
		lifeclmIO.setJlife("01");
		lifeclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeclmIO);
		if ((isNE(lifeclmIO.getStatuz(), varcom.mrnf))
		&& (isNE(lifeclmIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeclmIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
		}
		sv.jlifcnum.set(lifeclmIO.getLifcnum());
		cltsIO.setClntnum(lifeclmIO.getLifcnum());
		if (isEQ(clmhclmIO.getJlife(), "01")) {
			wsaaDeadLife.set(lifeclmIO.getLifcnum());
		}
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.jlinsnameErr.set(e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void continue1030()
	{
		if (isEQ(wsaaJlife, "01")) {
			sv.astrsk.set("*");
		}
		else {
			sv.asterisk.set("*");
		}
		/*  find each coverage or rider attached to the death*/
		/*  by reading the CLMDCLM file*/
		/*MOVE SPACES                 TO CLMDCLM-DATA-AREA.            */
		clmdclmIO.setParams(SPACES);
		clmdclmIO.setChdrcoy(chdrclmIO.getChdrcoy());
		clmdclmIO.setChdrnum(chdrclmIO.getChdrnum());
		clmdclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clmdclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(clmdclmIO.getStatuz(),varcom.endp))) {
			processClmdclm1500();
		}
		
		sv.estimateTotalValue.set(wsaaEstMatValue);
		getPolicyDebt1900();
		compute(sv.clamamt, 2).set(add(add(sub(add(wsaaActvalue,clmhclmIO.getOtheradjst()),sv.tdbtamt),clmhclmIO.getZrcshamt()),sv.policyloan));
		checkFollowups1800();
		/*  Read T6661 to get the foward transaction code for death claim  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaRegisterTranCode.set(t6661rec.trcode);
		/* Check that the last active transaction was a Death Claim        */
		/* Register.                                                       */
		/* PTRNREV will return only those records with Valid Flag '1'      */
		/* and in descending order of TRANNO.                              */
		ptrnrevIO.setDataArea(SPACES);
		ptrnrevIO.setChdrcoy(chdrclmIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrclmIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void readPtrn1050()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		//ILIFE-5935 changes started
		if (isEQ(ptrnrevIO.getStatuz(), varcom.endp)) {
			return;
		}
		//ILIFE-5935 changes ended
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		/* There should be at least one PTRN for this contract             */
		if ((isNE(chdrclmIO.getChdrnum(), ptrnrevIO.getChdrnum()))
		|| (isNE(chdrclmIO.getChdrcoy(), ptrnrevIO.getChdrcoy()))) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		/* If the last transaction was a Death Claim Register, Go To       */
		/* Exit.                                                           */
		if (isEQ(ptrnrevIO.getBatctrcde(), wsaaRegisterTranCode)) {
			return ;
		}
		/* Call the subroutine to check the Transaction Code               */
		/* against what is on T6661                                        */
		tranchkrec.codeCheckRec.set(SPACES);
		tranchkrec.tcdeStatuz.set(varcom.oK);
		tranchkrec.tcdeTranCode.set(ptrnrevIO.getBatctrcde());
		tranchkrec.tcdeCompany.set(wsspcomn.company);
		callProgram(Trcdechk.class, tranchkrec.codeCheckRec);
		if ((isNE(tranchkrec.tcdeStatuz, varcom.oK))
		&& (isNE(tranchkrec.tcdeStatuz, varcom.mrnf))) {
			syserrrec.params.set(tranchkrec.codeCheckRec);
			fatalError600();
		}
		if (isEQ(tranchkrec.tcdeStatuz, varcom.oK)) {
			ptrnrevIO.setFunction(varcom.nextr);
			readPtrn1050();
			return ;
		}
		if (isEQ(tranchkrec.tcdeStatuz, varcom.mrnf)) {
			wsaaTranError = "Y";
		}
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1400()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void processClmdclm1500()
	{
		read1510();
	}

protected void read1510()
	{
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.endp)
		&& isNE(clmdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrclmIO.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(chdrclmIO.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			clmdclmIO.setStatuz(varcom.endp);
		}
		else {
			sv.coverage.set(clmdclmIO.getCoverage());
			sv.rider.set(clmdclmIO.getRider());
			/*       MOVE CLMDCLM-CRTABLE      TO S5269-CRTABLE                */
			sv.crtable.set(clmdclmIO.getVirtualFund());
			sv.shortds.set(clmdclmIO.getShortds());
			sv.liencd.set(clmdclmIO.getLiencd());
			/*     MOVE CLMDCLM-RIIND        TO S5269-RIIND                  */
			sv.cnstcur.set(clmdclmIO.getCnstcur());
			sv.estMatValue.set(clmdclmIO.getEstMatValue());
			sv.actvalue.set(clmdclmIO.getActvalue());
			wsaaEstMatValue.add(clmdclmIO.getEstMatValue());
			wsaaActvalue.add(clmdclmIO.getActvalue());
			scrnparams.function.set(varcom.sadd);
			processScreen("S5269", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			else {
				clmdclmIO.setFunction(varcom.nextr);
			}
		}
	}

protected void checkFollowups1800()
	{
		go1801();
	}

protected void go1801()
	{
		/*MOVE SPACES                TO FLUPCLM-DATA-KEY.             */
		flupclmIO.setParams(SPACES);
		flupclmIO.setChdrcoy(clmhclmIO.getChdrcoy());
		flupclmIO.setChdrnum(clmhclmIO.getChdrnum());
		flupclmIO.setFupno(ZERO);
		flupclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		flupclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		flupclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, flupclmIO);
		if ((isNE(flupclmIO.getStatuz(), varcom.oK))
		&& (isNE(flupclmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		if ((isNE(flupclmIO.getChdrcoy(), clmhclmIO.getChdrcoy()))
		|| (isNE(flupclmIO.getChdrnum(), clmhclmIO.getChdrnum()))
		|| (isEQ(flupclmIO.getStatuz(), varcom.endp))) {
			sv.fupflg.set(SPACES);
		}
		else {
			/*  their are some followups*/
			sv.fupflg.set("+");
		}
	}

protected void getPolicyDebt1900()
	{
		start1900();
		read1900();
	}

protected void start1900()
	{
		sv.tdbtamt.set(ZERO);
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrclmIO.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrclmIO.getChdrnum());
		tpoldbtIO.setTranno(ZERO);
		tpoldbtIO.setFormat(tpoldbtrec);
		tpoldbtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		tpoldbtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		tpoldbtIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void read1900()
	{
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		&& isNE(tpoldbtIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			fatalError600();
		}
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		|| isNE(tpoldbtIO.getChdrcoy(), chdrclmIO.getChdrcoy())
		|| isNE(tpoldbtIO.getChdrnum(), chdrclmIO.getChdrnum())) {
			return ;
		}
		sv.tdbtamt.add(tpoldbtIO.getTdbtamt());
		tpoldbtIO.setFunction(varcom.nextr);
		read1900();
		return ;
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S5269IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5269-DATA-AREA                         */
		/*                         S5269-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*    If 'CALC' requested then set a srceen error so the screen    */
		/*    is re-displayed.                                             */
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/* If errors were set in the 1000 section they must remain set     */
		/* here, i.e. the user must KILL to get out.                       */
		if (isEQ(wsaaTranError, "Y")) {
			sv.chdrnumErr.set(i034);
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		/**    No Further Validation Required.*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		if (isEQ(sv.fupflg, "X")) {
			return ;
		}
		/*  Update the Client Details File to  remove the Date of Death.   */
		updateClient3600();
		/*  Delete claim reserve record ILJ-383 starts*/
		if(CMDTH010Permission){
		  deleteClmReserve3610();
		}
		
		/*  transfer the contract soft lock to AT*/
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrclmIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		/*MOVE 'P5269AT'            TO  ATRT-MODULE.                   */
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO  ATRT-REQ-TERM.                 */
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrclmIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransAreaInner.wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransAreaInner.wsaaTranDate.set(varcom.vrcmDate);
		wsaaTransAreaInner.wsaaTranTime.set(varcom.vrcmTime);
		wsaaTransAreaInner.wsaaUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO WSAA-TERMID.                    */
		wsaaTransAreaInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransAreaInner.wsaaTranCode.set(ptrnrevIO.getBatctrcde());
		wsaaTransAreaInner.wsaaTodate.set(ptrnrevIO.getPtrneff());
		/*MOVE WSKY-BATC-BATCTRCDE  TO WSAA-TRAN-CODE.            <005>*/
		wsaaTransAreaInner.wsaaTranNum.set(ptrnrevIO.getTranno());
		wsaaTransAreaInner.wsaaPlnsfx.set(ZERO);
		wsaaTransAreaInner.wsaaSuppressTo.set(ZERO);
		wsaaTransAreaInner.wsaaSupflag.set("N");
		wsaaTransAreaInner.wsaaCfiafiTranno.set(ZERO);
		wsaaTransAreaInner.wsaaCfiafiTranCode.set(SPACES);
		wsaaTransAreaInner.wsaaBbldat.set(ZERO);
		atreqrec.transArea.set(wsaaTransAreaInner.wsaaTransArea);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void updateClient3600()
	{
		update3600();
	}

protected void update3600()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		/*     MOVE CHDRCLM-COWNNUM       TO CLTS-CLNTNUM.                 */
		cltsIO.setClntnum(wsaaDeadLife);
		/*   Read and hold the record for updating.                        */
		cltsIO.setFunction(varcom.readh);
		getClientDetails3700();
		/* IF CLTS-STATUZ               = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO CLTS-VALIDFLAG        <V76F10>*/
		/*    MOVE CLTSREC             TO CLTS-FORMAT           <V76F10>*/
		/*    MOVE REWRT               TO CLTS-FUNCTION         <V76F10>*/
		/*                                                      <V76F10>*/
		/*    CALL 'CLTSIO'         USING CLTS-PARAMS           <V76F10>*/
		/*    IF CLTS-STATUZ        NOT = O-K                   <V76F10>*/
		/*       MOVE CLTS-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*       MOVE CLTS-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/*    END-IF                                            <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		/*   Re-initialise the Date of Death.                              */
		cltsIO.setCltdod(varcom.vrcmMaxDate);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		/*     MOVE CHDRCLM-COWNNUM     TO CLTS-CLNTNUM.                   */
		cltsIO.setClntnum(wsaaDeadLife);
		cltsIO.setCltstat("AC");
		/*   Rewrite the record without the Date of Death.                 */
		/*  MOVE REWRT               TO CLTS-FUNCTION.          <V73F02>*/
		/*  MOVE '1'                 TO CLTS-VALIDFLAG.         <V76F10>*/
		/*  MOVE WRITR               TO CLTS-FUNCTION.          <V76F10>*/
		cltsIO.setFunction(varcom.rewrt);
		getClientDetails3700();
	}

protected void deleteClmReserve3610(){
	List<Crsvpf> crsvpf = crsvpfDAO.getCrsvpfRecord(chdrclmIO.getChdrnum().toString(), "L", "1", "T668");
	if(!crsvpf.isEmpty()){
		crsvpfDAO.deleteCrsvpfRecord(crsvpf.get(0).getUniqueNumber());
		crsvpf.clear();
		crsvpf = crsvpfDAO.getCrsvpfRecord(chdrclmIO.getChdrnum().toString(), "L", "2", "TASO");
		if(!crsvpf.isEmpty()){
			crsvpf.get(0).setTrdt(wsaaToday.toInt());
			crsvpfDAO.updateValidFlgCrsvRecord(crsvpf.get(0),"1");
		}
	}
 }
 //ILJ-383 ends
protected void getClientDetails3700()
	{
		/*PARA*/
		/*  Call to I-O Module.                                            */
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		/*  If first time into this section (stack action blank)*/
		/*  save next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		if (isEQ(sv.fupflg, "X")) {
			gensswrec.function.set("A");
			sv.fupflg.set("?");
			callGenssw4300();
			return ;
		}
		/*   No more selected (or none)*/
		/*      - restore stack form wsaa to wssp*/
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		/*  If current stack action is * then re-display screen*/
		/*     (in this case, some other option(s) were requested)*/
		/*  Otherwise continue as normal.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		callSubroutine4310();
	}

protected void callSubroutine4310()
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void loadProgram4400()
	{
		/*RESTORE1*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT1*/
	}
/*
 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
 */
private static final class WsaaTransAreaInner { 

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
}
}
