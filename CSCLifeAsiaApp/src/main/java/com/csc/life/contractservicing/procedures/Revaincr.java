/*
 * File: Revaincr.java
 * Date: 30 August 2009 2:05:14
 * Author: Quipoz Limited
 * 
 * Class transformed from REVAINCR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.PayrlifDAO;
import com.csc.life.newbusiness.dataaccess.model.Payrpf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE ACTUAL INCREASE.
*        ------------------------
*
* Processing.
* -----------
*
* The process for adding or modifying  components via Automatic
* Increases uses  the  same principles as  online Component Add
* or Modify.
*
* To  this  end,  this  subroutine  determines  which  Reversal
* subroutine to  call as defined in T6658.  Both will be called
* with  the REVERSEREC  linkage.  The two routines that will be
* called  are  REVINCRA and REVINCRM,  one  for modify  and the
* other for add. Generic reversal processing  will  be  catered
* for in these subroutines.
*
* In  addition to calling  this  subroutine the INCR  record(s)
* that  were  updated  by  the   forward  transaction  must  be
* reinstated.
*
*****************************************************************
* </pre>
*/
public class Revaincr extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "REVAINCR";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String h036 = "H036";
		/* TABLES */
	private String t6658 = "T6658";
	private String chdrmjarec = "CHDRMJAREC";
	private String payrlifrec = "PAYRLIFREC";
	private String incrrec = "INCRREC   ";
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Automatic Increase File*/
	private IncrTableDAM incrIO = new IncrTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Payr logical file*/
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private T6658rec t6658rec = new T6658rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	
	private PayrlifDAO payrlifDAO	 = getApplicationContext().getBean("payrlifDAO", PayrlifDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		next2255, 
		exit3090, 
		exit99490, 
		exit99990
	}

	public Revaincr() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		processIncr2000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processIncr2000()
	{
		try {
			start2000();
		}
		catch (GOTOException e){
		}
	}

protected void start2000()
	{
		incrIO.setParams(SPACES);
		incrIO.setChdrcoy(reverserec.company);
		incrIO.setChdrnum(reverserec.chdrnum);
		incrIO.setPlanSuffix(ZERO);
		incrIO.setFormat(incrrec);
		incrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		callIncrio2100();
		if (isEQ(incrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2090);
		}
		processChdrs4000();
		processPayrs5000();
		while ( !(isEQ(incrIO.getStatuz(),varcom.endp))) {
			updateIncrs2200();
		}
		
	}

protected void callIncrio2100()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(),varcom.oK)
		&& isNE(incrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrIO.getParams());
			syserrrec.statuz.set(incrIO.getStatuz());
			databaseError99500();
		}
		if (isNE(incrIO.getChdrcoy(),reverserec.company)
		|| isNE(incrIO.getChdrnum(),reverserec.chdrnum)) {
			incrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void updateIncrs2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2200();
				}
				case next2255: {
					next2255();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2200()
	{
		if (isEQ(incrIO.getRefusalFlag(),SPACES)) {
			goTo(GotoLabel.next2255);
		}
		processComponents3000();
		/*REWRT*/
		incrIO.setFunction(varcom.readh);
		callIncrio2100();
		incrIO.setValidflag("1");
		incrIO.setFunction(varcom.rewrt);
		callIncrio2100();
	}

protected void next2255()
	{
		incrIO.setFunction(varcom.nextr);
		callIncrio2100();
		/*EXIT*/
	}

protected void processComponents3000()
	{
		try {
			start3000();
		}
		catch (GOTOException e){
		}
	}

protected void start3000()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItmfrm(reverserec.effdate1);
		itdmIO.setItemtabl(t6658);
		itdmIO.setItemitem(incrIO.getAnniversaryMethod());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			databaseError99500();
		}
		if (isNE(itdmIO.getItemcoy(),reverserec.company)
		|| isNE(itdmIO.getItemtabl(),t6658)
		|| isNE(itdmIO.getItemitem(),incrIO.getAnniversaryMethod())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(reverserec.company);
			itdmIO.setItemtabl(t6658);
			itdmIO.setItemitem(incrIO.getAnniversaryMethod());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h036);
			systemError99000();
		}
		t6658rec.t6658Rec.set(itdmIO.getGenarea());
		if (isEQ(t6658rec.trevsub,SPACES)) {
			goTo(GotoLabel.exit3090);
		}
		reverserec.life.set(incrIO.getLife());
		reverserec.coverage.set(incrIO.getCoverage());
		reverserec.rider.set(incrIO.getRider());
		reverserec.planSuffix.set(incrIO.getPlanSuffix());
		callProgram(t6658rec.trevsub, reverserec.reverseRec);
		if (isNE(reverserec.statuz,varcom.oK)) {
			syserrrec.statuz.set(reverserec.statuz);
			syserrrec.params.set(reverserec.reverseRec);
			databaseError99500();
		}
	}

protected void processChdrs4000()
	{
		start4000();
	}

protected void start4000()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(),"1")
		|| isEQ(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		chdrmjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		chdrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(),"2")
		|| isEQ(chdrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
	}

protected void processPayrs5000()
	{
		start5000();
	}

protected void start5000()
	{
	List<Payrpf> payrpfs = payrlifDAO.getUniqueAndFlag(reverserec.company.toString().trim(),
		reverserec.chdrnum.toString().trim());
	/*if (payrpfs.size() != 2) {
		syserrrec.statuz.set(Varcom.bomb);
		syserrrec.params.set(reverserec.reverseRec);
		databaseError99500();
	}*/
	int payNewCount = 0;
	int payNOldCount = 0;
	for (Payrpf payrpf : payrpfs) {
		if ("1".equals(payrpf.getValidflag().trim())) {
			payNewCount++;
			if (payNewCount > 1) {
				syserrrec.statuz.set(Varcom.bomb);
				syserrrec.params.set(reverserec.reverseRec);
				databaseError99500();
				break;
			}
			boolean res = payrlifDAO.deletByUnqiue(payrpf.getUnique_number());
			if (!res) {
				syserrrec.statuz.set(Varcom.bomb);
				syserrrec.params.set(reverserec.reverseRec);
				databaseError99500();
				break;
			}
			continue;
		}
		if ("2".equals(payrpf.getValidflag().trim()) && payNewCount > 0) {
			/*payNOldCount++;
			if (payNOldCount > 1) {
				syserrrec.statuz.set(Varcom.bomb);
				syserrrec.params.set(reverserec.reverseRec);
				databaseError99500();
				break;
			}*/
			boolean res = payrlifDAO.updateValidflag(payrpf.getUnique_number(), "1");
			if (!res) {
				syserrrec.statuz.set(Varcom.bomb);
				syserrrec.params.set(reverserec.reverseRec);
				databaseError99500();
				// break;
			}
			break;
		}
	}
	/*
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setValidflag(1);
		payrlifIO.setPayrseqno(ZERO);
		payrlifIO.setFormat(payrlifrec);
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "VALIDFLAG");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)
		&& isNE(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		if (isNE(payrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(payrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(),"1")
		|| isEQ(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		payrlifIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		payrlifIO.setValidflag(2);
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "VALIDFLAG");
		payrlifIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)
		&& isNE(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		if (isNE(payrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(payrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(),"2")) {
//		|| isEQ(payrlifIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		payrlifIO.setValidflag("1");
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		*/
	}

protected void systemError99000()
	{
		try {
			start99000();
		}
		catch (GOTOException e){
		}
		finally{
			exit99490();
		}
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError99500()
	{
		try {
			start99500();
		}
		catch (GOTOException e){
		}
		finally{
			exit99990();
		}
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99990);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
