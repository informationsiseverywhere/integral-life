package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.LhdrpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Lhdrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LhdrpfDAOImpl extends BaseDAOImpl<Lhdrpf> implements LhdrpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(LhdrpfDAOImpl.class);

	@Override
	public void insertLhdrpfRecord(Lhdrpf lhdrpf) {
		StringBuilder sqlInsert = new StringBuilder();
		sqlInsert.append("INSERT INTO LHDRPF(CHDRCOY,CHDRNUM,LOANTYPE,LOANCURR,LOANORIGAM,LOANSTDATE,TERMID,USER_T,TRDT,TRTM)");
		sqlInsert.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
		PreparedStatement psRplnpfInsert = getPrepareStatement(sqlInsert.toString());
		try {
			psRplnpfInsert.setString(1, lhdrpf.getChdrcoy());
			psRplnpfInsert.setString(2, lhdrpf.getChdrnum());
			psRplnpfInsert.setString(3, lhdrpf.getLoantype());
			psRplnpfInsert.setString(4, lhdrpf.getLoancurr());
			psRplnpfInsert.setBigDecimal(5, lhdrpf.getLoanorigam());
			psRplnpfInsert.setInt(6, lhdrpf.getLoanstdate());			
			psRplnpfInsert.setString(7, lhdrpf.getTermid());
			psRplnpfInsert.setInt(8, lhdrpf.getUser_T());			
			psRplnpfInsert.setInt(9, lhdrpf.getTrdt());
			psRplnpfInsert.setInt(10, lhdrpf.getTrtm());			
			executeUpdate(psRplnpfInsert);
		} catch (SQLException e) {
			LOGGER.error("insertLhdrpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psRplnpfInsert, null);
		}
	}
	
	
}
