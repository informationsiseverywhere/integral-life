/*
 * File: Pr51r.java
 * Date: December 3, 2013 3:25:54 AM ICT
 * Author: CSC
 * 
 * Class transformed from PR51R.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PrmhactTableDAM;
import com.csc.life.contractservicing.dataaccess.PrmhenqTableDAM;
import com.csc.life.contractservicing.screens.Sr51rScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.life.productdefinition.tablestructures.Tr51prec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*          Permium Holiday Submenu
*          -----------------------
*
*   This is Premium Holiday Submenun program. This acreen (SR51R)  llows
*   user to select a Contract and an Action.
*
* Validation.
* -----------
*  - The Contract must exist.
*  - The Contract must have a valid status for transaction that is
*    being selected - check this via the table T5679.
*  - For Action:
*    - A: Contract type must be valid for TR51P
* </pre>
*/
public class Pr51r extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR51R");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(1);
	private Validator trcdeMatch = new Validator(wsaaTrcde, "Y");
	private Validator trcdeNotMatch = new Validator(wsaaTrcde, "N");
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String tr51p = "TR51P";
	private static final String ta524 = "TA524";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String itemrec = "ITEMREC";
	private static final String prmhenqrec = "PRMHENQREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PrmhactTableDAM prmhactIO = new PrmhactTableDAM();
	private PrmhenqTableDAM prmhenqIO = new PrmhenqTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Tr51prec tr51prec = new Tr51prec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sr51rScreenVars sv = ScreenProgram.getScreenVars( Sr51rScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private boolean prmhldtrad = false;
	private Ta524rec ta524rec = new Ta524rec();
	private Itempf itempf;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2990, 
		exit3090
	}

	public Pr51r() {
		super();
		screenVars = sv;
		new ScreenModel("Sr51r", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		validateKey12210();
	}

protected void validateKey12210()
	{
		/*    Validate Contract with correct branch, contract statii*/
		/*    for selected action.*/
		if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(errorsInner.g667);
			return ;
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.f259);
			return ;
		}
		if (isNE(chdrmjaIO.getCntbranch(), wsspcomn.branch)) {
			sv.chdrselErr.set(errorsInner.e455);
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		if (isEQ(sv.action, "A")
		&& isNE(chdrmjaIO.getPtdate(), chdrmjaIO.getBtdate())) {
			sv.chdrselErr.set("H013");
			return ;
		}
		//ILIFE-8179
		prmhldtrad = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString(), "CSOTH010", appVars, "IT");	
		if(prmhldtrad) {
			datcon1rec.function.set("TDAY");
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
			itempf = null;
			itempf = itempfDAO.findItemByDate("IT", chdrmjaIO.getChdrcoy().toString(), ta524, chdrmjaIO.getCnttype().toString(), wsaaToday.toString(), "1");
			if(itempf != null) {
				ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			else {
				ta524rec.ta524Rec.set(SPACES);
			}
		}
		else {
			ta524rec.ta524Rec.set(SPACES);
		}
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr51p);
			itemIO.setItemitem(chdrmjaIO.getCnttype());
			itemIO.setFunction("READR");
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				if(!prmhldtrad)
					sv.chdrselErr.set(errorsInner.rlbs);
				else
					sv.chdrselErr.set(errorsInner.rrrh);
				return ;
			}
			tr51prec.tr51pRec.set(itemIO.getGenarea());
		}
		/*   Validate Contract Status Code*/
		checkStatus2400();
		trcdeNotMatch.setTrue();
		/*    Verify the Contract Risk Statuz*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| trcdeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(chdrmjaIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				trcdeMatch.setTrue();
			}
		}
		if (trcdeNotMatch.isTrue()) {
			sv.chdrselErr.set(errorsInner.h137);
			return ;
		}
		/*    Verify the Contract Premium Statuz*/
	
		readStatusTable2410 (subprogrec.transcd );
		trcdeNotMatch.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| trcdeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(chdrmjaIO.getPstatcode(), t5679rec.cnPremStat[wsaaSub.toInt()])) {
				trcdeMatch.setTrue();
			}
		}
		if (trcdeNotMatch.isTrue()) {
			sv.chdrselErr.set(errorsInner.h137);
			return ;
		}
		if (isEQ(sv.action, "B")) {
			checkPrmhenq2400();
		}
	}

protected void checkStatus2400()
{
	readStatusTable2410(subprogrec.transcd);
	lookForStat();
	
}


protected void  lookForStat() {
	
	
	wsaaSub.set(ZERO);
	lookForStat2500();
	
}


private void readStatusTable2410(FixedLengthStringData contitem) {
	itemIO.setParams(SPACES);
	itemIO.setItempfx(smtpfxcpy.item);
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setItemtabl(t5679);
	itemIO.setItemitem(contitem.toString());
	itemIO.setItemseq(SPACES);
	itemIO.setFormat(itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(itemIO.getStatuz());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
		sv.chdrselErr.set(errorsInner.f321);
		return ;
	}
	t5679rec.t5679Rec.set(itemIO.getGenarea());
	
	
}


protected void lookForStat2500()
	{  
		
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			if (isNE(t5679rec.contitem,SPACES)) {
				readStatusTable2410(t5679rec.contitem);
				
			}else{
				sv.chdrselErr.set(errorsInner.e767);
				wsspcomn.edterror.set("Y");
			}
		
			
		}
		else {
			if (isNE(chdrmjaIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				//goTo(GotoLabel.search2510);
				lookForStat2500();
			}
		}
		/*EXIT*/
	}

protected void checkPrmhenq2400()
	{
		start2410();
	}

protected void start2410()
	{
		prmhenqIO.setParams(SPACES);
		prmhenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		prmhenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		prmhenqIO.setFormat(prmhenqrec);
		prmhenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, prmhenqIO);
		if (isNE(prmhenqIO.getStatuz(), varcom.oK)
		&& isNE(prmhenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(prmhenqIO.getParams());
			syserrrec.statuz.set(prmhenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(prmhenqIO.getStatuz(), varcom.oK)) {
			sv.chdrselErr.set(errorsInner.g957);
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
			goTo(GotoLabel.exit2990);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateWssp3010();
			setUp3500();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details                                      **/
		/*  Perform KEEPS and SFTLOCKs here that required by the next progr*/
		chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getParams());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void setUp3500()
	{
		if (isEQ(sv.action, "A")){
			wsspcomn.flag.set("P");
		}
		else if (isEQ(sv.action, "B")){
			wsspcomn.flag.set("I");
		}
		if (isNE(wsspcomn.flag, "I")) {
			softlockContract3200();
		}
		/*BATCHING*/
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void softlockContract3200()
	{
		softlockContract3210();
	}

protected void softlockContract3210()
	{
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.entity.set(sv.chdrsel);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
	private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData g667 = new FixedLengthStringData(4).init("G667");
	private FixedLengthStringData g957 = new FixedLengthStringData(4).init("G957");
	private FixedLengthStringData h137 = new FixedLengthStringData(4).init("H137");
	private FixedLengthStringData rlbs = new FixedLengthStringData(4).init("RLBS");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData rrrh = new FixedLengthStringData(4).init("RRRH");	//ILIFE-8179
}
}
