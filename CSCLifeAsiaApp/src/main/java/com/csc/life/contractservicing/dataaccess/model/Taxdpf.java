package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

import com.csc.fsu.general.dataaccess.model.Ptrnpf;

public class Taxdpf {
	private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int plansfx;
	private int effdate;
	private String tranref;
	private int instfrom;
	private int instto;
	private int billcd;
	private int tranno;
	private BigDecimal baseamt;
	private String trantype;
	private BigDecimal taxamt01;
	private BigDecimal taxamt02;
	private BigDecimal taxamt03;
	private String txabsind01;
	private String txabsind02;
	private String txabsind03;
	private String txtype01;
	private String txtype02;
	private String txtype03;
	private String postflg;
	private String usrprf;
	private String jobnm;
	private Date datime;
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlansfx() {
		return plansfx;
	}
	public void setPlansfx(int plansfx) {
		this.plansfx = plansfx;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getTranref() {
		return tranref;
	}
	public void setTranref(String tranref) {
		this.tranref = tranref;
	}
	public int getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(int instfrom) {
		this.instfrom = instfrom;
	}
	public int getInstto() {
		return instto;
	}
	public void setInstto(int instto) {
		this.instto = instto;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getBaseamt() {
		return baseamt;
	}
	public void setBaseamt(BigDecimal baseamt) {
		this.baseamt = baseamt;
	}
	public String getTrantype() {
		return trantype;
	}
	public void setTrantype(String trantype) {
		this.trantype = trantype;
	}
	public BigDecimal getTaxamt01() {
		return taxamt01;
	}
	public void setTaxamt01(BigDecimal taxamt01) {
		this.taxamt01 = taxamt01;
	}
	public BigDecimal getTaxamt02() {
		return taxamt02;
	}
	public void setTaxamt02(BigDecimal taxamt02) {
		this.taxamt02 = taxamt02;
	}
	public BigDecimal getTaxamt03() {
		return taxamt03;
	}
	public void setTaxamt03(BigDecimal taxamt03) {
		this.taxamt03 = taxamt03;
	}
	public String getTxabsind01() {
		return txabsind01;
	}
	public void setTxabsind01(String txabsind01) {
		this.txabsind01 = txabsind01;
	}
	public String getTxabsind02() {
		return txabsind02;
	}
	public void setTxabsind02(String txabsind02) {
		this.txabsind02 = txabsind02;
	}
	public String getTxabsind03() {
		return txabsind03;
	}
	public void setTxabsind03(String txabsind03) {
		this.txabsind03 = txabsind03;
	}
	public String getTxtype01() {
		return txtype01;
	}
	public void setTxtype01(String txtype01) {
		this.txtype01 = txtype01;
	}
	public String getTxtype02() {
		return txtype02;
	}
	public void setTxtype02(String txtype02) {
		this.txtype02 = txtype02;
	}
	public String getTxtype03() {
		return txtype03;
	}
	public void setTxtype03(String txtype03) {
		this.txtype03 = txtype03;
	}
	public String getPostflg() {
		return postflg;
	}
	public void setPostflg(String postflg) {
		this.postflg = postflg;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {

	}
	
	public Taxdpf() {
	}
	public Taxdpf(Taxdpf taxdpf){
		this.unique_number=taxdpf.unique_number;
		this.chdrcoy=taxdpf.chdrcoy;
		this.chdrnum=taxdpf.chdrnum;
		this.life=taxdpf.life;
    	this.coverage=taxdpf.coverage;
    	this.rider=taxdpf.rider;
		this.plansfx=taxdpf.plansfx;
		this.effdate=taxdpf.effdate;
		this.tranref=taxdpf.tranref;
		this.instfrom=taxdpf.instfrom;
		this.instto=taxdpf.instto;
		this.billcd=taxdpf.billcd;
		this.tranno=taxdpf.tranno;
		this.baseamt=taxdpf.baseamt;
		this.trantype=taxdpf.trantype;
		this.taxamt01=taxdpf.taxamt01;
		this.taxamt02=taxdpf.taxamt02;
		this.taxamt03=taxdpf.taxamt03;
		this.txabsind01=taxdpf.txabsind01;
		this.txabsind02=taxdpf.txabsind02;
		this.txabsind03=taxdpf.txabsind03;
		this.txtype01=taxdpf.txtype01;
		this.txtype02=taxdpf.txtype02;
		this.txtype03=taxdpf.txtype03;
		this.postflg=taxdpf.postflg;
		this.usrprf=taxdpf.usrprf;
		this.jobnm=taxdpf.jobnm;
		this.datime=taxdpf.datime;
		
	}

}
