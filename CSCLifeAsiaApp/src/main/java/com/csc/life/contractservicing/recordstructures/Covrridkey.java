package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:25
 * Description:
 * Copybook name: COVRRIDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrridkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrridFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrridKey = new FixedLengthStringData(64).isAPartOf(covrridFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrridChdrcoy = new FixedLengthStringData(1).isAPartOf(covrridKey, 0);
  	public FixedLengthStringData covrridChdrnum = new FixedLengthStringData(8).isAPartOf(covrridKey, 1);
  	public FixedLengthStringData covrridLife = new FixedLengthStringData(2).isAPartOf(covrridKey, 9);
  	public FixedLengthStringData covrridCoverage = new FixedLengthStringData(2).isAPartOf(covrridKey, 11);
  	public FixedLengthStringData covrridCrtable = new FixedLengthStringData(4).isAPartOf(covrridKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covrridKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrridFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrridFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}