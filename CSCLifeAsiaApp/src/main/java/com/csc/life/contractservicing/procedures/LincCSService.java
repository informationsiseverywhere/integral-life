/**
 * 
 */
package com.csc.life.contractservicing.procedures;

import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;

/**
 * Service for LINC processing via Contract Servicing Transactions.
 * 
 * @author gsaluja2
 *
 */
public interface LincCSService {
	/**
	 * <pre>
	 * Handles request from Component Add Proposal Transaction.
	 * If TJL06 criteria is met then
	 * 		1. If record exists in LINCPF and does not exist in SLNCPF
	 * 				a new record is inserted in SLNCPF.
	 * 		3. If a record does not exist in LINCPF and SLNCPF both
	 * 				a new record is inserted in LINCPF and SLNCPF
	 * 		If Life Assured's Age is less than the Juvenile Age specified
	 * 		in TJL06 then Expected date of adding rider in LINCPF is set 
	 * 		as ptrn-eff date.
	 * 		If Life Assured's Age is less than the Juvenile Age specified
	 * 		in TJL06 then Actual date of adding rider in LINCPF is set 
	 * 		as ptrn-eff date.
	 * </pre>
	 * 
	 * @param lincpfHelper
	 * @param tjl47rec
	 */
	public void addComponentProposal(LincpfHelper lincpfHelper, Tjl47rec tjl47rec);
	/**
	 * <pre>
	 * Handles request from Component Add Approval Transaction.
	 * If TJL06 criteria is met then and if record exists in LINCPF 
	 * and does not exist in SLNCPF then a new record is inserted 
	 * in SLNCPF.
	 * </pre>
	 * 
	 * @param lincpfHelper
	 * @param tjl47rec
	 */
	public void addComponentApproval(LincpfHelper lincpfHelper, Tjl47rec tjl47rec);
	/**
	 * <pre>
	 * Handles request from Component Modify Proposal Transaction.
	 * If TJL06 criteria is met then
	 * 		1. If record exists in LINCPF and does not exist in SLNCPF
	 * 				a new record is inserted in SLNCPF.
	 * 		3. If a record does not exist in LINCPF and SLNCPF both
	 * 				a new record is inserted in LINCPF and SLNCPF
	 * 		Whenever a new record is inserted a follow up SLP is generated.
	 * </pre>
	 * 
	 * @param lincpfHelper
	 * @param tjl47rec
	 */
	public void modifyComponentProposal(LincpfHelper lincpfHelper, Tjl47rec tjl47rec);
	/**
	 * <pre>
	 * Handles request from Component Modify Approval Transaction.
	 * 1. If a record exists in LINCPF and TJL06 Criteria is met
	 * 		If there is an increase in Sum Insured after component modify
	 * 		then, insert data to SLNCPF.
	 * 		If there is a decrease in Sum Insured after component modify
	 * 		then, insert data to SLNCPF.
	 * 	2. If a record exists in LINCPF and TJL06 Criteria is NOT met
	 * 		then, update date terminated field in LINCPF.
	 * </pre>
	 * 
	 * @param lincpfHelper
	 * @param tjl47rec
	 */
	public void modifyComponentApproval(LincpfHelper lincpfHelper, Tjl47rec tjl47rec);
	/**
	 * <pre>
	 * Handles request from Death Claim and Lapse Transactions.
	 * </pre>
	 * 
	 * @param lincpfHelper
	 * @param tjl47rec
	 */
	public void generalProcessing(LincpfHelper lincpfHelper, Tjl47rec tjl47rec);
}
