/*
 * File: Revbbut.java
 * Date: 30 August 2009 2:05:34
 * Author: Quipoz Limited
 * 
 * Class transformed from REVBBUT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.contractservicing.dataaccess.CovrbbrTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrrevTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrsttrnTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        BENEFIT BILLING REVERSAL - UNIT TRANSACTIONS ONLY
*        -------------------------------------------------
*
* Overview.
* ---------
*
* In order to ensure that, after the reversal of Benefit Billing,
*  (by the subroutine REVBBAC), the original contract situation is
*  recreated, it is also necessary to reverse any Unit Linked
*  transactions which were created when the Coverage debt was
*  recovered. This will ensure that the correct Unit prices are
*  used and that the Office obtains a correct picture in the
*  accounts of the effects of such a reversal.
* Thus, this subroutine will reverse any UTRNs from the forward
*  transaction which match on the key specified...
*  the key being Component, Transaction Number and Transaction
*  Code.
*  It will also amend the Coverage Debt which is held on the
*   Coverage record in the COVR file ( using COVRBBR logical ).
*
* Processing.
* -----------
*
* This subroutine is called from the Full contract Reversal AT
*  module REVGENAT via an entry in T6661. All parameters required
*  by this subroutine are passed in the REVERSEREC copybook.
*
*  Read through the UTRN records ( using UTRNREV logical )
*
*    FOR each UTRNREV record that matches on the key of Company,
*        Contract number and Transaction Number (passed in the
*        REVERSEREC copybook)
*
*        Note value UTRNREV-CONTRACT-AMOUNT
*
*        Check the Feedback Indicator on the UTRNREV record:
*
*        If Feedback-indicator = SPACES
*            DELETe UTRN record
*
*        Else
*
*            Post an UTRN for the opposite Contract Amount
*             just retrieved on the UTRNREV record.
*             ( multiply UTRNREV-CONTRACT-AMOUNT by -1,
*               multiply UTRNREV-FUND-AMOUNT by -1,
*               multiply UTRNREV-NOF-DUNITS by -1,
*               multiply UTRNREV-NOF-UNITS by -1 )
*
*        End-if
*
*        Read & lock coverage record on coverage (COVRBBR) file
*         in preparation for updating - the COVR key is
*         determined from the fields on the UTRNREV record
*
*        Subtract the value UTRNREV-CONTRACT-AMOUNT from the
*         Coverage debt field, COVRBBR-COVERAGE-DEBT.
*
*        REWRiTe coverage (COVRBBR) record
*
*    Read NEXT UTRNREV record & repeat above
*
* Tables used.
* ------------
*
* T6647 - Unit Linked Contract Details
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Include processing for interest bearing funds.                      *
* Reverse any HITR records created by the forward transaction.        *
*                                                                     *
*****************************************************************
* </pre>
*/
public class Revbbut extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVBBUT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaContractAmount = new PackedDecimalData(13, 2);
	private FixedLengthStringData wsaaCurrentCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCurrentLife = new FixedLengthStringData(2).init(SPACES);
		/*01  WSAA-T6647-KEY.                                              */
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
		/* FORMATS */
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String covrbbrrec = "COVRBBRREC";
	private static final String utrnrevrec = "UTRNREVREC";
	private static final String hitrrevrec = "HITRREVREC";
	private static final String zrsttrnrec = "ZRSTTRNREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrbbrTableDAM covrbbrIO = new CovrbbrTableDAM();
	private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
	private ZrsttrnTableDAM zrsttrnIO = new ZrsttrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Reverserec reverserec = new Reverserec();

	public Revbbut() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaContractAmount.set(ZERO);
		wsaaCurrentLife.set(SPACES);
		wsaaCurrentCoverage.set(SPACES);
		wsaaChdrcoy.set(SPACES);
		wsaaChdrnum.set(SPACES);
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		initialise2000();
		processUtrns3000();
		processHitrs3200();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		/*START*/
		/* read contract to get contract type*/
		readContract2100();
		/*EXIT*/
	}

protected void readContract2100()
	{
		/*START*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(reverserec.company);
		chdrenqIO.setChdrnum(reverserec.chdrnum);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			databaseError9500();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*2200-READ-T6647 SECTION.                                         
	*2200-START.                                                      
	* Read table T6647 to get processing sequence number              
	**** MOVE SPACES                 TO ITDM-PARAMS.                  
	**** MOVE 'IT'                   TO ITDM-ITEMPFX.                 
	**** MOVE T6647                  TO ITDM-ITEMTABL.                
	**** MOVE REVE-COMPANY           TO ITDM-ITEMCOY.                 
	**** MOVE REVE-BATCTRCDE         TO WSAA-TRANSCODE.               
	**** MOVE CHDRENQ-CNTTYPE        TO WSAA-CNTTYPE.                 
	**** MOVE WSAA-T6647-KEY         TO ITDM-ITEMITEM.                
	**** MOVE REVE-EFFDATE-1         TO ITDM-ITMFRM.                  
	**** MOVE BEGN                   TO ITDM-FUNCTION.                
	**** MOVE ITDMREC                TO ITDM-FORMAT.                  
	**** CALL 'ITDMIO'               USING ITDM-PARAMS.               
	**** IF ITDM-STATUZ              NOT = O-K                        
	****    AND ITDM-STATUZ          NOT = ENDP                       
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   
	****     MOVE ITDM-STATUZ        TO SYSR-STATUZ                   
	****     PERFORM 9500-DATABASE-ERROR                              
	**** END-IF.                                                      
	**** IF REVE-COMPANY             NOT = ITDM-ITEMCOY               
	****    OR WSAA-T6647-KEY        NOT = ITDM-ITEMITEM              
	****    OR T6647                 NOT = ITDM-ITEMTABL              
	****    OR ITDM-STATUZ           = ENDP                           
	****     MOVE WSAA-T6647-KEY     TO ITDM-ITEMITEM                 
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   
	****     MOVE H115               TO SYSR-STATUZ                   
	****     PERFORM 9500-DATABASE-ERROR                              
	**** END-IF.                                                      
	**** MOVE ITDM-GENAREA           TO T6647-T6647-REC.              
	*2290-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void processUtrns3000()
	{
			start3000();
		}

protected void start3000()
	{
		utrnrevIO.setParams(SPACES);
		utrnrevIO.setChdrcoy(reverserec.company);
		utrnrevIO.setChdrnum(reverserec.chdrnum);
		utrnrevIO.setPlanSuffix(ZERO);
		utrnrevIO.setTranno(reverserec.tranno);
		utrnrevIO.setFormat(utrnrevrec);
		utrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(),varcom.oK)
		&& isNE(utrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(utrnrevIO.getChdrcoy(),reverserec.company)
		|| isNE(utrnrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(utrnrevIO.getTranno(),reverserec.tranno)
		|| isEQ(utrnrevIO.getStatuz(),varcom.endp)) {
			return ;
		}
		while ( !(isEQ(utrnrevIO.getStatuz(),varcom.endp))) {
			reverseUtrns3100();
		}
		
	}

protected void reverseUtrns3100()
	{
			start3100();
		}

protected void start3100()
	{
		/* note Coverage number - we are going to process all UTRNs*/
		/*  and everytime the Covergae changes, update the Coverage*/
		/*  debt field on the corresponding COVRBBR Coverage record*/
		wsaaCurrentCoverage.set(utrnrevIO.getCoverage());
		wsaaCurrentLife.set(utrnrevIO.getLife());
		/* note contract amount value first so we can take it off*/
		/*  the coverage debt on the COVRBBR coverage record*/
		wsaaContractAmount.add(utrnrevIO.getContractAmount());
		/* check feedback indicator to see if UTRN has been processed*/
		/*  or not and either delete or post reverse UTRN*/
		if (isEQ(utrnrevIO.getFeedbackInd(),SPACES)) {
			/*        delete UTRNREV record*/
			utrnrevIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, utrnrevIO);
			if (isNE(utrnrevIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(utrnrevIO.getParams());
				syserrrec.statuz.set(utrnrevIO.getStatuz());
				databaseError9500();
			}
			utrnrevIO.setFunction(varcom.delet);
		}
		else {
			/*        Reverse ZRST whose XTRANNO = utrn tranno                 */
			a100ProcessZrst();
			/*        write an opposite UTRNREV record*/
			utrnrevIO.setFeedbackInd(SPACES);
			utrnrevIO.setTriggerModule(SPACES);
			utrnrevIO.setTriggerKey(SPACES);
			utrnrevIO.setSurrenderPercent(ZERO);
			setPrecision(utrnrevIO.getContractAmount(), 2);
			utrnrevIO.setContractAmount(mult(utrnrevIO.getContractAmount(),-1));
			setPrecision(utrnrevIO.getFundAmount(), 2);
			utrnrevIO.setFundAmount(mult(utrnrevIO.getFundAmount(),-1));
			setPrecision(utrnrevIO.getNofDunits(), 5);
			utrnrevIO.setNofDunits(mult(utrnrevIO.getNofDunits(),-1));
			setPrecision(utrnrevIO.getNofUnits(), 5);
			utrnrevIO.setNofUnits(mult(utrnrevIO.getNofUnits(),-1));
			setPrecision(utrnrevIO.getProcSeqNo(), 0);
			utrnrevIO.setProcSeqNo(mult(utrnrevIO.getProcSeqNo(),-1));
			/*     MOVE T6647-PROC-SEQ-NO   TO UTRNREV-PROC-SEQ-NO          */
			utrnrevIO.setTransactionDate(reverserec.transDate);
			utrnrevIO.setTransactionTime(reverserec.transTime);
			utrnrevIO.setTranno(reverserec.newTranno);
			utrnrevIO.setTermid(reverserec.termid);
			utrnrevIO.setUser(reverserec.user);
			wsaaBatckey.set(reverserec.batchkey);
			utrnrevIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			utrnrevIO.setBatccoy(wsaaBatckey.batcBatccoy);
			utrnrevIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			utrnrevIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			utrnrevIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			utrnrevIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			utrnrevIO.setUstmno(ZERO);
			utrnrevIO.setFunction(varcom.writr);
		}
		/*    store key UTRN details in case no more UTRNs to process*/
		/*    for current Coverage*/
		wsaaChdrcoy.set(utrnrevIO.getChdrcoy());
		wsaaChdrnum.set(utrnrevIO.getChdrnum());
		wsaaLife.set(utrnrevIO.getLife());
		wsaaCoverage.set(utrnrevIO.getCoverage());
		wsaaPlanSuffix.set(utrnrevIO.getPlanSuffix());
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			databaseError9500();
		}
		/* get next UTRN to process*/
		utrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(),varcom.oK)
		&& isNE(utrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnrevIO.getParams());
			systemError9000();
		}
		if (isNE(utrnrevIO.getChdrcoy(),reverserec.company)
		|| isNE(utrnrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(utrnrevIO.getTranno(),reverserec.tranno)
		|| isEQ(utrnrevIO.getStatuz(),varcom.endp)) {
			/*       No more UTRNs to process, but don't forget to update*/
			/*        Coverage record*/
			updateCoverage4000();
			wsaaContractAmount.set(ZERO);
			utrnrevIO.setStatuz(varcom.endp);
			return ;
		}
		/* IF coverage we are processing has changed, then go and amend*/
		/*  the Coverage debt before processing UTRNs for the next*/
		/*  Coverage.*/
		if (isNE(utrnrevIO.getCoverage(),wsaaCurrentCoverage)
		|| isNE(utrnrevIO.getLife(),wsaaCurrentLife)) {
			updateCoverage4000();
			wsaaContractAmount.set(ZERO);
		}
	}

protected void processHitrs3200()
	{
			start3210();
		}

protected void start3210()
	{
		hitrrevIO.setParams(SPACES);
		hitrrevIO.setChdrcoy(reverserec.company);
		hitrrevIO.setChdrnum(reverserec.chdrnum);
		hitrrevIO.setPlanSuffix(ZERO);
		hitrrevIO.setTranno(reverserec.tranno);
		hitrrevIO.setFormat(hitrrevrec);
		hitrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)
		&& isNE(hitrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			databaseError9500();
		}
		if (isNE(hitrrevIO.getChdrcoy(),reverserec.company)
		|| isNE(hitrrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(hitrrevIO.getTranno(),reverserec.tranno)
		|| isEQ(hitrrevIO.getStatuz(),varcom.endp)) {
			return ;
		}
		while ( !(isEQ(hitrrevIO.getStatuz(),varcom.endp))) {
			reverseHitrs3300();
		}
		
	}

protected void reverseHitrs3300()
	{
			start3310();
		}

protected void start3310()
	{
		/* note Coverage number - we are going to process all UTRNs        */
		/*  and everytime the Covergae changes, update the Coverage        */
		/*  debt field on the corresponding COVRBBR Coverage record        */
		wsaaCurrentCoverage.set(hitrrevIO.getCoverage());
		wsaaCurrentLife.set(hitrrevIO.getLife());
		/* note contract amount value first so we can take it off          */
		/*  the coverage debt on the COVRBBR coverage record               */
		wsaaContractAmount.add(hitrrevIO.getContractAmount());
		/* check feedback indicator to see if HITR has been processed      */
		/*  or not and either delete or post reverse HITR                  */
		if (isEQ(hitrrevIO.getFeedbackInd(),SPACES)) {
			/* Delete HITRREV record                                           */
			hitrrevIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hitrrevIO);
			if (isNE(hitrrevIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hitrrevIO.getParams());
				databaseError9500();
			}
			hitrrevIO.setFunction(varcom.delet);
		}
		else {
			utrnrevIO.setLife(hitrrevIO.getLife());
			utrnrevIO.setCoverage(hitrrevIO.getCoverage());
			utrnrevIO.setTranno(hitrrevIO.getTranno());
			a100ProcessZrst();
			/* Write an opposite HITRREV record                                */
			hitrrevIO.setFeedbackInd(SPACES);
			hitrrevIO.setTriggerModule(SPACES);
			hitrrevIO.setTriggerKey(SPACES);
			hitrrevIO.setSurrenderPercent(ZERO);
			setPrecision(hitrrevIO.getContractAmount(), 2);
			hitrrevIO.setContractAmount(mult(hitrrevIO.getContractAmount(),-1));
			setPrecision(hitrrevIO.getFundAmount(), 2);
			hitrrevIO.setFundAmount(mult(hitrrevIO.getFundAmount(),-1));
			setPrecision(hitrrevIO.getProcSeqNo(), 0);
			hitrrevIO.setProcSeqNo(mult(hitrrevIO.getProcSeqNo(),-1));
			hitrrevIO.setTranno(reverserec.newTranno);
			wsaaBatckey.set(reverserec.batchkey);
			hitrrevIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			hitrrevIO.setBatccoy(wsaaBatckey.batcBatccoy);
			hitrrevIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			hitrrevIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			hitrrevIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			hitrrevIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			hitrrevIO.setUstmno(ZERO);
			hitrrevIO.setZintrate(ZERO);
			hitrrevIO.setZlstintdte(varcom.vrcmMaxDate);
			hitrrevIO.setZinteffdt(varcom.vrcmMaxDate);
			hitrrevIO.setZintappind(SPACES);
			hitrrevIO.setFunction(varcom.writr);
		}
		/*    Store key HITR details in case no more HITRs to process      */
		/*    for current Coverage                                         */
		wsaaChdrcoy.set(hitrrevIO.getChdrcoy());
		wsaaChdrnum.set(hitrrevIO.getChdrnum());
		wsaaLife.set(hitrrevIO.getLife());
		wsaaCoverage.set(hitrrevIO.getCoverage());
		wsaaPlanSuffix.set(hitrrevIO.getPlanSuffix());
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrrevIO.getParams());
			databaseError9500();
		}
		/* Get next HITR to process                                        */
		hitrrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)
		&& isNE(hitrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			systemError9000();
		}
		if (isNE(hitrrevIO.getChdrcoy(),reverserec.company)
		|| isNE(hitrrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(hitrrevIO.getTranno(),reverserec.tranno)
		|| isEQ(hitrrevIO.getStatuz(),varcom.endp)) {
			/*    No more HITRs to process, but don't forget to update         */
			/*    Coverage record                                              */
			updateCoverage4000();
			wsaaContractAmount.set(ZERO);
			hitrrevIO.setStatuz(varcom.endp);
			return ;
		}
		/* IF coverage we are processing has changed, then go and amend    */
		/*  the Coverage debt before processing HITRs for the next         */
		/*  Coverage.                                                      */
		if (isNE(hitrrevIO.getCoverage(),wsaaCurrentCoverage)
		|| isNE(hitrrevIO.getLife(),wsaaCurrentLife)) {
			updateCoverage4000();
			wsaaContractAmount.set(ZERO);
		}
	}

protected void updateCoverage4000()
	{
		start4000();
	}

protected void start4000()
	{
		/* Read-lock the COVRBBR record for updating*/
		covrbbrIO.setParams(SPACES);
		covrbbrIO.setFunction(varcom.readh);
		covrbbrIO.setFormat(covrbbrrec);
		/* set up key for reading COVR*/
		covrbbrIO.setChdrcoy(wsaaChdrcoy);
		covrbbrIO.setChdrnum(wsaaChdrnum);
		covrbbrIO.setLife(wsaaLife);
		covrbbrIO.setCoverage(wsaaCoverage);
		covrbbrIO.setPlanSuffix(wsaaPlanSuffix);
		/* the RIDER is set to zeros, because the debt is held against*/
		/*  the Coverage.*/
		covrbbrIO.setRider(ZERO);
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			databaseError9500();
		}
		/* calculate new revised Coverage debt value*/
		setPrecision(covrbbrIO.getCoverageDebt(), 2);
		covrbbrIO.setCoverageDebt(sub(covrbbrIO.getCoverageDebt(),wsaaContractAmount));
		/*        rewrite amended COVRBBR coverage record*/
		/*MOVE REVE-NEW-TRANNO        TO COVRBBR-TRANNO.               */
		/*MOVE REVE-TRANS-DATE        TO COVRBBR-TRANSACTION-DATE.     */
		/*MOVE REVE-TRANS-TIME        TO COVRBBR-TRANSACTION-TIME.     */
		/*MOVE REVE-USER              TO COVRBBR-USER.                 */
		/*MOVE REVE-TERMID            TO COVRBBR-TERMID.               */
		covrbbrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrbbrIO);
		if (isNE(covrbbrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrbbrIO.getParams());
			syserrrec.statuz.set(covrbbrIO.getStatuz());
			databaseError9500();
		}
	}

protected void a100ProcessZrst()
	{
			a100Start();
		}

protected void a100Start()
	{
		zrsttrnIO.setParams(SPACES);
		zrsttrnIO.setChdrcoy(reverserec.company);
		zrsttrnIO.setChdrnum(reverserec.chdrnum);
		zrsttrnIO.setLife(utrnrevIO.getLife());
		zrsttrnIO.setCoverage(utrnrevIO.getCoverage());
		zrsttrnIO.setXtranno(utrnrevIO.getTranno());
		zrsttrnIO.setFormat(zrsttrnrec);
		/* MOVE BEGNH                  TO ZRSTTRN-FUNCTION.    <V4LAQR> */
		zrsttrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		zrsttrnIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, zrsttrnIO);
		if (isNE(zrsttrnIO.getStatuz(),varcom.oK)
		&& isNE(zrsttrnIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zrsttrnIO.getStatuz());
			syserrrec.params.set(zrsttrnIO.getParams());
			databaseError9500();
		}
		if (isEQ(zrsttrnIO.getStatuz(),varcom.endp)
		|| isNE(zrsttrnIO.getChdrcoy(),reverserec.company)
		|| isNE(zrsttrnIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(zrsttrnIO.getLife(),utrnrevIO.getLife())
		|| isNE(zrsttrnIO.getCoverage(),utrnrevIO.getCoverage())
		|| isNE(zrsttrnIO.getXtranno(),utrnrevIO.getTranno())) {
			/*    MOVE REWRT               TO ZRSTTRN-FUNCTION      <V4LAQR>*/
			/*       MOVE WRITD               TO ZRSTTRN-FUNCTION      <LA4995>*/
			/*       CALL 'ZRSTTRNIO'         USING ZRSTTRN-PARAMS     <LA4995>*/
			/*                                                         <LA4995>*/
			/*       IF ZRSTTRN-STATUZ        NOT = O-K                <LA4995>*/
			/*          MOVE ZRSTTRN-STATUZ      TO SYSR-STATUZ        <LA4995>*/
			/*          MOVE ZRSTTRN-PARAMS      TO SYSR-PARAMS        <LA4995>*/
			/*          PERFORM 9500-DATABASE-ERROR                    <LA4995>*/
			/*       END-IF                                            <LA4995>*/
			return ;
		}
		while ( !(isEQ(zrsttrnIO.getStatuz(),varcom.endp))) {
			a200RewrtZrst();
		}
		
	}

protected void a200RewrtZrst()
	{
		a200Start();
	}

protected void a200Start()
	{
		zrsttrnIO.setFeedbackInd(SPACES);
		/* MOVE REWRT               TO ZRSTTRN-FUNCTION         <V4LAQR>*/
		zrsttrnIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, zrsttrnIO);
		if (isNE(zrsttrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zrsttrnIO.getStatuz());
			syserrrec.params.set(zrsttrnIO.getParams());
			databaseError9500();
		}
		zrsttrnIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, zrsttrnIO);
		if (isNE(zrsttrnIO.getStatuz(),varcom.oK)
		&& isNE(zrsttrnIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zrsttrnIO.getStatuz());
			syserrrec.params.set(zrsttrnIO.getParams());
			databaseError9500();
		}
		if (isEQ(zrsttrnIO.getStatuz(),varcom.endp)
		|| isNE(zrsttrnIO.getChdrcoy(),reverserec.company)
		|| isNE(zrsttrnIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(zrsttrnIO.getLife(),utrnrevIO.getLife())
		|| isNE(zrsttrnIO.getCoverage(),utrnrevIO.getCoverage())
		|| isNE(zrsttrnIO.getXtranno(),utrnrevIO.getTranno())) {
			zrsttrnIO.setStatuz(varcom.endp);
		}
	}

protected void systemError9000()
	{
			start9000();
			exit9490();
		}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
			start9500();
			exit9990();
		}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
