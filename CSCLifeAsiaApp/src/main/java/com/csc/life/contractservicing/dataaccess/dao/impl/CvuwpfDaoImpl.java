package com.csc.life.contractservicing.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.CvuwpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Cvuwpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CvuwpfDaoImpl extends BaseDAOImpl<Cvuwpf> implements CvuwpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CvuwpfDaoImpl.class);	
	@Override
	public Cvuwpf getCvuwpfRecord(String company, String chdrnum) {
		StringBuilder sb = new StringBuilder();
		//IJTI-306 START		
		sb.append(" SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,HUWDCDTE,EFFDATE,JOBNM,USRPRF,DATIME ")
				.append(" FROM CVUWPF  WHERE CHDRCOY = ? AND CHDRNUM = ?")
				.append("  ORDER BY CHDRCOY, CHDRNUM");
		ResultSet rs = null;		
		Cvuwpf cvuwpf = null;
		PreparedStatement statement = null;
		try{
			statement = getPrepareStatement(sb.toString());
			statement.setString(1, company);
			statement.setString(2, chdrnum);
			rs = statement.executeQuery();
			//IJTI-306 END
			while(rs.next()){
				cvuwpf = new Cvuwpf();
				cvuwpf.setUnique_Number(rs.getLong("UNIQUE_NUMBER"));
				cvuwpf.setUsrprf(rs.getString("USRPRF"));
				cvuwpf.setChdrcoy(rs.getString("CHDRCOY"));
				cvuwpf.setChdrnum(rs.getString("CHDRNUM"));
				cvuwpf.setHuwdcdte(rs.getInt("HUWDCDTE"));
				cvuwpf.setEffdate(rs.getInt("EFFDATE"));
				cvuwpf.setJobnm(rs.getString("JOBNM"));
				cvuwpf.setDatime(rs.getDate("DATIME"));
			}
		}catch (SQLException e) {
			LOGGER.error("getCvuwpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(statement, rs);			
		}
		   return cvuwpf;
	}

}
