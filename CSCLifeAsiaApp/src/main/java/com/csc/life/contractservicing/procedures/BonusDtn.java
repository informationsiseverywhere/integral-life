package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.life.contractservicing.recordstructures.TaxDeductionRec;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.regularprocessing.dataaccess.dao.BonspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Bonspf;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.quipoz.framework.util.QPUtilities;
/**
 * <pre>
 * REMARKS
 * 				BONUS DEDUCTION SUBROUTINE
 * --------------------------------------------------------------------------------------
 * This subroutine is called by Contributions Tax Processing batch job. This subroutine
 * takes in COVR list,  T5645 List, Tax deduction rec and batcdorrec passed as parameters
 * from calling batch program.
 * 1. Read CHDR data passed by calling batch program.
 * 2. Retrieve T5645 data being passed by calling batch program.
 * 3. Retrieve COVR data passed by calling program and calculate total risk premium.
 * 4. Fetch the details from BONSPF for particular policy, read only primary record for
 * 	  policy level tax processing rather than coverage level tax processing.
 * 5. Deduct tax form bonus, set BONSPF record with valid flag 2 and write a new
 *    record with new bonus value.
 * </pre>
 * 
 * @author gsaluja2
 * @version 1.0
 * @since 05 February 2019
 *
 */

public class BonusDtn extends TaxComputeDtn{
	/*	Subroutine params	*/
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final String SPACES = "";
	private static final String wsaaSubr = "BONUSDTN";
	
	/*	Rec's Used	*/
	private Syserrrec syserrrec = new Syserrrec();
	private TaxDeductionRec taxdrec = new TaxDeductionRec();
	//private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5645rec t5645rec = new T5645rec();

	/*	DAO Used	*/
	private BonspfDAO bonspfDAO = getApplicationContext().getBean("bonspfDAO", BonspfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	
	/*	Objects Used	*/
	private Chdrpf chdrpf;
	private Bonspf bonspf;
	private Covrpf covrpf;
	private Varcom varcom = new Varcom();
	
	/*	Lists Used	*/
	private List<Bonspf> bonspfList;
	
	/*	Variables Used	*/
	private BigDecimal wsaaBonusVal;
	
	private boolean itemFound = false;
	private static final String h134 = "H134";
	
	private String trandesc;
	
	@Override
	public void mainline(Object... parmArray){
		trandesc = (String) parmArray [4];
		covrpf = (Covrpf) convertAndSetParam(covrpf, parmArray, 3);
		t5645rec.t5645Rec = convertAndSetParam(t5645rec.t5645Rec, parmArray, 2);
		taxdrec = (TaxDeductionRec) convertAndSetParam(taxdrec, parmArray, 1);
		batcdorrec.batcdorRec = convertAndSetParam(batcdorrec.batcdorRec, parmArray, 0);
		try{
			startSubr();
		}catch(COBOLExitProgramException e) {
			/*	EXIT	*/
		}
	}
	
	protected void startSubr() {
		taxdrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		initialise();
		exitProgram();
	}
	
	protected void initialise() {
		chdrpf = new Chdrpf();
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);		
		readBonusHoldings();
		updatePostings();
	}
	

//	protected void setT5645Rec(){
//		if(t5645Item != null) {			
//				t5645rec.t5645Rec.set(StringUtil.rawToString(t5645Item.getGenarea()));
//				itemFound = true;					
//			}
//		
//		if (!itemFound) {
//			syserrrec.params.set(t5645rec.t5645Rec);
//			syserrrec.statuz.set(h134);
//			fatalError();
//		}
//	}
	
	protected void readBonusHoldings() {
		bonspf = bonspfDAO.readBonspf(taxdrec.getChdrcoy(), taxdrec.getChdrnum(), "1");
		wsaaBonusVal = BigDecimal.ZERO;
		if(bonspf != null) {
			wsaaBonusVal = wsaaBonusVal.add(bonspf.getTotalBonus());
		}
	}
	
	protected void updatePostings() {
		deductTaxFromBonus();
		postAcmv(batcdorrec,taxdrec,varcom,t5645rec,trandesc,chdrpf,covrpf);
	}
	
	
	
	protected void deductTaxFromBonus() {
		wsaaBonusVal = wsaaBonusVal.subtract(taxdrec.getTotalTaxAmt());
		bonspfList = new ArrayList<>();
		if(bonspf != null) {
			bonspf.setValidflag("2");
			bonspf.setCurrto(taxdrec.getEffdate());
			bonspfList.add(bonspf);
			bonspf.setTermid(varcom.vrcmTermid.toString());
			bonspf.setUser(varcom.vrcmUser.toInt());
			varcom.vrcmTime.set(getCobolTime());
			varcom.vrcmDate.set(getCobolDate());
			bonspf.setTransactionDate(varcom.vrcmDate.toInt());
			bonspf.setTransactionTime(varcom.vrcmTime.toInt());
			bonspfDAO.updateBonspf(bonspfList);
			Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(1);
			datcon2Pojo.setIntDate1(Integer.toString(bonspf.getCurrto()));
			datcon2Pojo.setFrequency("DY");
			datcon2Utils.calDatcon2(datcon2Pojo);
			if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
				syserrrec.statuz.set(datcon2Pojo.getStatuz());
				syserrrec.params.set(datcon2Pojo.toString());
				fatalError();
			}
			bonspf.setCurrfrom(Integer.parseInt(datcon2Pojo.getIntDate2()));
		}
		else {
			bonspf = new Bonspf();
			bonspf.setCurrfrom(taxdrec.getEffdate());
			bonspf.setBonPayLastYr(BigDecimal.ZERO);
		}
		bonspf.setChdrcoy(taxdrec.getChdrcoy());
		bonspf.setChdrnum(taxdrec.getChdrnum());
		bonspf.setLife(covrpf.getLife());
		bonspf.setCoverage(covrpf.getCoverage());
		bonspf.setRider(covrpf.getRider());
		bonspf.setPlanSuffix(covrpf.getPlanSuffix());
		bonspf.setValidflag("1");
		bonspf.setCurrto(varcom.vrcmMaxDate.toInt());
		bonspf.setBonusCalcMeth(SPACES);			
		if(taxdrec.getTotalTaxAmt().compareTo(taxdrec.getTotalTaxAmt().negate()) == 0)
			bonspf.setBonPayThisYr(taxdrec.getTotalTaxAmt());
		else
			bonspf.setBonPayThisYr(taxdrec.getTotalTaxAmt().negate());
		bonspf.setTermid(varcom.vrcmTermid.toString());
		bonspf.setUser(varcom.vrcmUser.toInt());
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		bonspf.setTransactionDate(varcom.vrcmDate.toInt());
		bonspf.setTransactionTime(varcom.vrcmTime.toInt());
		bonspf.setTotalBonus(wsaaBonusVal);
		bonspfList.add(bonspf);
		bonspfDAO.insertBonspf(bonspfList);
	}
	
	@Override
	public  void dtnPostings(T5645rec t5645rec, Covrpf covrpf,TaxDeductionRec taxdrec,Lifacmvrec lifacmvrec,int wsaaJrnseq)
	{
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.rldgacct.set(taxdrec.getChdrnum()+covrpf.getLife()+covrpf.getCoverage()+covrpf.getRider()+new DecimalFormat("00").format(covrpf.getPlanSuffix()));
		callLifacmv(lifacmvrec,++wsaaJrnseq);
	}
	
}
