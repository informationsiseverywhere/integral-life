/*
 * File: P5098.java
 * Date: 30 August 2009 0:06:49
 * Author: Quipoz Limited
 * 
 * Class transformed from P5098.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.BjrnenqTableDAM;
import com.csc.life.contractservicing.dataaccess.BjrnmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.screens.S5098ScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
*                    BONUS JOURNALS
*                    ==============
*
*  The Bonus Journals Transaction is used for Traditional Type
*  components to update either the Amount of Bonus Held against
*  a component or to update the Bonus Declaration Date.
*
*  This program is called generically via T5671 and is called
*  subsequent to P6350 (policy select) and P6351 (component
*  select) having run.
*
*  An adjustment to one or both of the fields mentioned above
*  is permissible and this program is run for any single
*  policy/component combination.
*
*  PROCESSING.
*  ----------
*
*  INITIALISE.
*  ----------
*
*  - Retrieve the Contract Header from the CHDRMJA storage area.
*
*  - Retrieve the Coverage from the COVRMJA storage area.
*
*  - Release the Coverage.
*
*  - Load the Screen details as follows :
*
*    - Effective Date from WSSP-EFFDATE
*    - Contract Header Num from CHDRMJA
*    - Number of policies from CHDRMJA
*    - Contract Currency from CHDRMJA
*    - Paid to date from CHDRMJA
*    - Contract Type from CHDRMJA
*    - Life Number from COVRMJA
*    - Coverage Number from COVRMJA
*    - Rider Number from COVRMJA
*    - Coverage/Rider type from COVRMJA
*    - Current Bonus Declaration Date from COVRMJA
*    - Contract Type Description from T5688
*    - Life Assured Number from LIFEMJA
*    - Life Assured Name from LIFEMJA
*    - Coverage/Rider type description from T5687
*    - Plan Suffix from CHDRMJA or COVRMJA depending
*      on whether processing 'notional' policy or not
*    - Instalment Premium from COVRMJA
*    If no BJRN already exists
*    - Current Accrued Reversionary Bonus from ACBL
*        - Read T5645 to get SACSCODE and SACSTYPE to use
*          as the key to ACBL
*        - Multiply the figure by -1 as it will be held as
*          a negative number
*    If BJRN already exists
*    - Current Accrued Reversionary Bonus from latest BJRN
*
*    - Divide Balance by Number of Summarised Policies
*      if processing a notional policy
*
*  SEND / RECEIVE SCREEN.
*  ---------------------
*
*  - Validate Screen input
*
*      - Adjusted Bonus Amount must be greater than zero
*
*      - Adjusted Bonus Declaration Date must be greater than
*        Coverage/Rider Contract Risk Commencement Date
*
*      - At least one of the fields must be changed otherwise
*        processing will not be allowed to continue
*
*  - Pressing CALC wil redisplay the screen, pressing ENTER
*    will continue the processing.
*
*  UPDATE.
*  -------
*
*  - Write a Bonus Journal Record
*
*  WHERE NEXT.
*  ----------
*
*  - Add 1 to program pointer
*
*****************************************************************
* </pre>
*/
public class P5098 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5098");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaAcblKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcblChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAcblKey, 0);
	private FixedLengthStringData wsaaAcblLife = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 8);
	private FixedLengthStringData wsaaAcblCoverage = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 10);
	private FixedLengthStringData wsaaAcblRider = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 12);
	private FixedLengthStringData wsaaAcblPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 14);
		/* WSAA-HOLD-VALUES */
	private ZonedDecimalData wsaaHoldArbAdjust = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaHoldBdd = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaDataChangeFlag = new FixedLengthStringData(1).init("N");
	private Validator dataHasChanged = new Validator(wsaaDataChangeFlag, "Y");
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaPlanSuffFlag = new FixedLengthStringData(1);
	private Validator summarised = new Validator(wsaaPlanSuffFlag, "1");
	private Validator notional = new Validator(wsaaPlanSuffFlag, "2");
	private Validator nonNotional = new Validator(wsaaPlanSuffFlag, "3");
	private Validator singlePolicyPlan = new Validator(wsaaPlanSuffFlag, "4");

	private FixedLengthStringData wsaaBjrnFlag = new FixedLengthStringData(1).init("N");
	private Validator noPreviousBjrn = new Validator(wsaaBjrnFlag, "N");
	private Validator previousBjrn = new Validator(wsaaBjrnFlag, "Y");
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private String wsaaValidStatus = "";
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).setUnsigned();
	private static final String wsaaErrorIndicators = "";
	private PackedDecimalData wsaaTempValue = new PackedDecimalData(17, 2);
		/* ERRORS */
	private static final String h134 = "H134";
	private static final String e304 = "E304";
	private static final String t046 = "T046";
	private static final String f309 = "F309";
	private static final String t043 = "T043";
	private static final String t056 = "T056";
	private static final String t057 = "T057";
	private static final String t058 = "T058";
	private static final String rfik = "RFIK";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t5679 = "T5679";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String acblrec = "ACBLREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String bjrnmjarec = "BJRNMJAREC";
	private static final String bjrnenqrec = "BJRNENQREC";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private BjrnenqTableDAM bjrnenqIO = new BjrnenqTableDAM();
	private BjrnmjaTableDAM bjrnmjaIO = new BjrnmjaTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5098ScreenVars sv = ScreenProgram.getScreenVars( S5098ScreenVars.class);
	//ILIFE-8175
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public P5098() {
		super();
		screenVars = sv;
		new ScreenModel("S5098", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		/*  Is control returning from another program further down*/
		/*  the stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		/*  Retrieve contract header information.*/
		//ILIFE-8175
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		compute(wsaaTranno, 0).set(add(1, chdrpf.getTranno()));
		/*  Retrieve coverage information.*/
		//ILIFE-8175
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		  Release coverage information.
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		covrpfDAO.deleteCacheObject(covrpf);
		/*  Set Plan Suffix accordingly.*/
		/*      1. Summarised Policy*/
		/*      2. Notional Policy*/
		/*      3. Non-Notional Policy*/
		/*      4. Single Policy Plan*/
		if (isEQ(covrpf.getPlanSuffix(), 0)) {
			if (isEQ(chdrpf.getPolinc(), 1)) {
				wsaaPlanSuffFlag.set("4");
			}
			else {
				wsaaPlanSuffFlag.set("1");
			}
		}
		else {
			if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
				wsaaPlanSuffFlag.set("2");
			}
			else {
				wsaaPlanSuffFlag.set("3");
			}
		}
		/*  Load the screen details.*/
		sv.effdate.set(wsspsmart.effdate);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.numpols.set(chdrpf.getPolinc());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.crtable.set(covrpf.getCrtable());
		sv.bonusDecDate.set(covrpf.getUnitStatementDate());
		/*  Read T5688 description to get Contract Type*/
		/*  description.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.cntdesc.fill("?");
		}
		else {
			sv.cntdesc.set(descIO.getLongdesc());
		}
		/*  Read T5687 description to get Coverage/Rider type*/
		/*  description.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.crtabled.fill("?");
		}
		else {
			sv.crtabled.set(descIO.getLongdesc());
		}
		/*  Read T5679 to validate component status.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/*  Check risk status.*/
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			if (isEQ(covrpf.getRider(), ZERO)) {
				if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
					if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
						wsaaValidStatus = "Y";
					}
				}
			}
			else {
				if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
					if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
						wsaaValidStatus = "Y";
					}
				}
			}
		}
		if (isEQ(wsaaValidStatus, "N")) {
			sv.chdrnumErr.set(t056);
		}
		/*  If a valid risk status was found, then now check premium*/
		/*  status.*/
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			if (isEQ(covrpf.getRider(), ZERO)) {
				if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
					if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
						wsaaValidStatus = "Y";
					}
				}
			}
			else {
				if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
					if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
						wsaaValidStatus = "Y";
					}
				}
			}
		}
		if (isEQ(wsaaValidStatus, "N")) {
			sv.chdrnumErr.set(t057);
		}
		/*  Find the associated contract Life details.*/
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrnum(chdrpf.getChdrnum());
		lifemjaIO.setChdrcoy(chdrpf.getChdrcoy());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/*  Move the the life number to the screen and read the clients*/
		/*  file for the name.*/
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		/*  Look up the contract details of the client owner (CLTS) and*/
		/*  format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		/*  Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.lifenameErr.set(e304);
			sv.lifename.set(SPACES);
		}
		else {
			plainname();
			sv.lifename.set(wsspcomn.longconfname);
		}
		/*  Set screen Plan Suffix.*/
		if (summarised.isTrue()) {
			sv.planSuffix.set(ZERO);
			sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		}
		else {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		/*  If a Notional Policy then breakout the Instalment Premium*/
		/*  for the number of summarised policies within the Plan.*/
		if (notional.isTrue()) {
			compute(wsaaTempValue, 3).setRounded(div(covrpf.getInstprem(), chdrpf.getPolsum()));
			if (isEQ(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
				compute(wsaaTempValue, 3).setRounded(sub(covrpf.getInstprem(), (mult(wsaaTempValue, (sub(chdrpf.getPolsum(), 1))))));
			}
			zrdecplrec.amountIn.set(wsaaTempValue);
			callRounding5000();
			wsaaTempValue.set(zrdecplrec.amountOut);
			sv.instprem.set(wsaaTempValue);
		}
		else {
			sv.instprem.set(covrpf.getInstprem());
		}
		/*  Check to see if a Bonus Journal already exists for this*/
		/*  Component for this Transaction  Number.*/
		/*  If so then the Adjusted Balance should be taken*/
		/*  from there rather than from the ACBL and the Adjusted*/
		/*  Bonus Declaration Date should be taken from there also.*/
		wsaaBjrnFlag.set("N");
		bjrnenqIO.setParams(SPACES);
		bjrnenqIO.setChdrcoy(chdrpf.getChdrcoy());
		bjrnenqIO.setChdrnum(chdrpf.getChdrnum());
		bjrnenqIO.setTranno(wsaaTranno);
		bjrnenqIO.setLife(covrpf.getLife());
		bjrnenqIO.setCoverage(covrpf.getCoverage());
		bjrnenqIO.setRider(covrpf.getRider());
		bjrnenqIO.setPlanSuffix(covrpf.getPlanSuffix());
		bjrnenqIO.setFunction(varcom.readr);
		bjrnenqIO.setFormat(bjrnenqrec);
		SmartFileCode.execute(appVars, bjrnenqIO);
		if (isNE(bjrnenqIO.getStatuz(), varcom.oK)
		&& isNE(bjrnenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(bjrnenqIO.getParams());
			syserrrec.statuz.set(bjrnenqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(bjrnenqIO.getStatuz(), varcom.mrnf)) {
			readAcbl1200();
			sv.bonusDecDate.set(covrpf.getUnitStatementDate());
		}
		else {
			sv.bonusValue.set(bjrnenqIO.getAdjustedArb());
			sv.bonusDecDate.set(bjrnenqIO.getAdjBonusDecDate());
			wsaaBjrnFlag.set("Y");
		}
		/*  Initialise the remaining fields.*/
		sv.adjustedArb.set(ZERO);
		sv.arbAdjustAmt.set(ZERO);
		wsaaHoldArbAdjust.set(ZERO);
		sv.adjBonusDecDate.set(varcom.vrcmMaxDate);
		wsaaHoldBdd.set(varcom.vrcmMaxDate);
		wsaaDataChangeFlag.set("N");
		/*  If any errors were found, store for later use.*/
		sv.errorIndicators.set(wsaaErrorIndicators);
	}

protected void readAcbl1200()
	{
		start1200();
	}

protected void start1200()
	{
		/*  Read ACBL file to get the Accrued Reversionary Bonus.*/
		/*  First read T5645 to get SACSTYPE and SACSCODE to be used*/
		/*  as the key to reading ACBL file.*/
		/*  Check to see if processing a notional policy.*/
		if (notional.isTrue()) {
			wsaaPlan.set(ZERO);
		}
		else {
			wsaaPlan.set(covrpf.getPlanSuffix());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(h134);
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*  Read Account Balance file.*/
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(chdrpf.getChdrcoy());
		acblIO.setSacscode(t5645rec.sacscode01);
		wsaaAcblChdrnum.set(chdrpf.getChdrnum());
		wsaaAcblLife.set(covrpf.getLife());
		wsaaAcblCoverage.set(covrpf.getCoverage());
		wsaaAcblRider.set(covrpf.getRider());
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(chdrpf.getCntcurr());
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			acblIO.setSacscurbal(0);
		}
		if (notional.isTrue()) {
			compute(wsaaTempValue, 3).setRounded(div(acblIO.getSacscurbal(), chdrpf.getPolsum()));
			if (isEQ(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
				compute(wsaaTempValue, 3).setRounded(sub(acblIO.getSacscurbal(), (mult(wsaaTempValue, (sub(chdrpf.getPolsum(), 1))))));
			}
			zrdecplrec.amountIn.set(wsaaTempValue);
			callRounding5000();
			wsaaTempValue.set(zrdecplrec.amountOut);
			sv.bonusValue.set(wsaaTempValue);
		}
		else {
			sv.bonusValue.set(acblIO.getSacscurbal());
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*  Is control returning from another program further down         */
		/*  the stack.                                                     */
		wsaaDataChangeFlag.set("N");
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			start2000();
			validateScreen2020();
			checkForErrors2030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start2000()
	{
		/*    CALL 'S5098IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5098-DATA-AREA.                        */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		/*  Pull in any errors which were encountered in the 1000 section.*/
		sv.errorIndicators.set(wsaaErrorIndicators);
		wsspcomn.edterror.set(varcom.oK);
		/*  If terminating processing go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2099-EXIT.                                         */
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2020()
	{
		/*  If ARB Adjustment has been entered, update the total,*/
		/*  validate the new total, and set the 'data has changed' flag.*/
		if (isNE(sv.arbAdjustAmt, ZERO)) {
			zrdecplrec.amountIn.set(sv.arbAdjustAmt);
			callRounding5000();
			if (isNE(zrdecplrec.amountOut, sv.arbAdjustAmt)) {
				sv.arbadjamtErr.set(rfik);
			}
		}
		compute(sv.adjustedArb, 2).set(add(sv.arbAdjustAmt, sv.bonusValue));
		if (isLT(sv.adjustedArb, 0)) {
			sv.arbadjamtErr.set(t046);
		}
		if (isNE(sv.arbAdjustAmt, wsaaHoldArbAdjust)) {
			wsaaDataChangeFlag.set("Y");
		}
		/*  If New Bonus Declaration Date is not already in error,*/
		/*  validate, store the new value and set 'data has changed'*/
		/*  flag.*/
		if (isEQ(sv.adjbondateErr, SPACES)) {
			if (isLT(sv.adjBonusDecDate, covrpf.getCrrcd())
			&& isNE(sv.adjBonusDecDate, varcom.vrcmMaxDate)) {
				sv.adjbondateErr.set(t058);
			}
		}
		if (isEQ(sv.adjbondateErr, SPACES)) {
			if (isGT(sv.adjBonusDecDate, covrpf.getRiskCessDate())
			&& isNE(sv.adjBonusDecDate, varcom.vrcmMaxDate)) {
				sv.adjbondateErr.set(t043);
			}
		}
		if (isNE(sv.adjBonusDecDate, wsaaHoldBdd)) {
			wsaaDataChangeFlag.set("Y");
		}
		if (!dataHasChanged.isTrue()
		&& noPreviousBjrn.isTrue()) {
			sv.adjbondateErr.set(f309);
			sv.arbadjamtErr.set(f309);
			sv.adjustedArb.set(0);
		}
		/*  If F9 pressed then redisplay the screen.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2030()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		start3000();
	}

protected void start3000()
	{
		/*  Is control returning from another program further down*/
		/*  the stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		/*  If terminating processing go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/*  If nothing was changed, then do not write a BJRN. This is*/
		/*  only permissible when a BJRN was still there and unprocessed.*/
		if (!dataHasChanged.isTrue()) {
			return ;
		}
		bjrnmjaIO.setDataArea(SPACES);
		bjrnmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		bjrnmjaIO.setChdrnum(chdrpf.getChdrnum());
		bjrnmjaIO.setTranno(wsaaTranno);
		bjrnmjaIO.setLife(covrpf.getLife());
		bjrnmjaIO.setCoverage(covrpf.getCoverage());
		bjrnmjaIO.setRider(covrpf.getRider());
		bjrnmjaIO.setPlanSuffix(sv.planSuffix);
		bjrnmjaIO.setEffdate(sv.effdate);
		bjrnmjaIO.setBonusValue(sv.bonusValue);
		bjrnmjaIO.setArbAdjustAmt(sv.arbAdjustAmt);
		bjrnmjaIO.setAdjustedArb(sv.adjustedArb);
		bjrnmjaIO.setBonusDecDate(sv.bonusDecDate);
		if (isNE(sv.adjBonusDecDate, wsaaHoldBdd)) {
			bjrnmjaIO.setAdjBonusDecDate(sv.adjBonusDecDate);
		}
		else {
			bjrnmjaIO.setAdjBonusDecDate(sv.bonusDecDate);
		}
		bjrnmjaIO.setCrtable(covrpf.getCrtable());
		bjrnmjaIO.setCnttype(chdrpf.getCnttype());
		bjrnmjaIO.setValidflag("1");
		bjrnmjaIO.setTermid(varcom.vrcmTermid);
		bjrnmjaIO.setUser(varcom.vrcmUser);
		bjrnmjaIO.setTransactionDate(varcom.vrcmDate);
		bjrnmjaIO.setTransactionTime(varcom.vrcmTime);
		bjrnmjaIO.setFunction(varcom.writr);
		bjrnmjaIO.setFormat(bjrnmjarec);
		SmartFileCode.execute(appVars, bjrnmjaIO);
		if (isNE(bjrnmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bjrnmjaIO.getParams());
			syserrrec.statuz.set(bjrnmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*START*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.cntcurr);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
}
