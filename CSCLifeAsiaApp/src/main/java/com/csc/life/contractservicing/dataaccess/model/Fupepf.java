package com.csc.life.contractservicing.dataaccess.model;

import java.util.Date;

public class Fupepf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private int fupno;
	private String clamnum;
	private int tranno;
	private String docseq;
	private String message;
	private String userProfile;
	private String jobName;
	private Date datime;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getFupno() {
		return fupno;
	}
	public void setFupno(int fupno) {
		this.fupno = fupno;
	}
	public String getClamnum() {
		return clamnum;
	}
	public void setClamnum(String clamnum) {
		this.clamnum = clamnum;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getDocseq() {
		return docseq;
	}
	public void setDocseq(String docseq) {
		this.docseq = docseq;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	//IJTI-461 START
	public Date getDatime() {
		if (this.datime==null){
			return null;
		} else {
			return (Date) this.datime.clone();
		}
	} //IJTI-461 END
	public void setDatime(Date datime) {
		this.datime = (Date) datime.clone(); //IJTI-460
	}
	
}
