/*
 * File: P5147at.java
 * Date: 30 August 2009 0:14:37
 * Author: Quipoz Limited
 *
 * Class transformed from P5147AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal; //ILIFE-8786
import java.util.ArrayList;
import java.util.List;

import com.csc.diary.procedures.Dryproces;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO; //ILIFE-8786
import com.csc.fsu.accounting.dataaccess.model.Acblpf; //ILIFE-8786
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO; //ILIFE-8786
import com.csc.fsu.clients.dataaccess.model.Clrfpf; //ILIFE-8786
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO; //ILIFE-8786
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO; //ILIFE-8786
import com.csc.fsu.general.dataaccess.dao.impl.ChdrpfDAOImpl;
import com.csc.fsu.general.dataaccess.model.Payrpf; //ILIFE-8786
import com.csc.fsu.general.dataaccess.model.Ptrnpf; //ILIFE-8786
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo; //ILIFE-8786
import com.csc.fsu.general.procedures.ZrdecplcUtils; //ILIFE-8786
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5647rec;
import com.csc.life.agents.tablestructures.Tr695rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.PcdtpfDAO; //ILIFE-8786
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Pcdtpf; //ILIFE-8786
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO; //ILIFE-8786
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf; //ILIFE-8786
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO; //ILIFE-8786
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO; //ILIFE-8786
import com.csc.life.productdefinition.dataaccess.model.Covrpf; //ILIFE-8786
import com.csc.life.productdefinition.dataaccess.model.Hitrpf; //ILIFE-8786
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.procedures.Vpuubbl;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.procedures.Vpxubbl;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;

import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
//ILIFE-8786 start
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.ZctnpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.ZptnpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.regularprocessing.dataaccess.model.Zctnpf;
import com.csc.life.regularprocessing.dataaccess.model.Zptnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO; 
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO; 
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf; 
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
 //ILIFE-8786 end
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.procedures.SftlockUtil; //ILIFE-8786
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.SftlockRecBean; //ILIFE-8786
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*AUTHOR.
*
*REMARKS
*
*       SINGLE PREMIUM TOP-UP - AT MODULE
*       ---------------------------------
*
*  This program will be run under AT and will perform the
*  general functions for the finalisation of Single Premium
*  Top-Up.
*  The program will carry out the following functions:
*
*  1.  Increment the TRANNO field on the Contract Header.
*
*  2.  Write a PTRN record to mark the transaction.
*
*  3.  Call the Breakout routine if required.
*
*  4.  Create the AGCM records to show the commission split.
*
*  5.  Create the ACMV records for the General Ledger.
*
*  Processing:
*
*  Contract Header.
*
*  The ATMOD-PRIMARY-KEY will contain the Company and Contract
*  number of the contract being processed. Use these to
*  perform a READH on CHDRMJA. Increment the TRANNO field by 1
*  and rewrite the Contract Header.
*
*  PTRN Record.
*
*  Write a PTRN record with the new TRANNO.
*
*  See the Notes section at the end of this specification for
*  a description of the field contents.
*
*  Breakout Processing.
*
*  A physical breakout will be required if any UTRN record
*  exists for the transaction that has a Plan Suffix that is
*  less than or equal to CHDRMJA-POLSUM. All the UNLT records
*  will have to be read to determine whether or not this is
*  necessary.
*
*  Use the Contract Company, Contract Number and the new
*  TRANNO to read all the UTRN records that match on this
*  portion of the key. Make a note of the lowest
*  UTRN-PLAN-SUFFIX. At the end, if this is not greater than
*  CHDRMJA-POLSUM then the subroutine BRKOUT must be called.
*  Set the OLD-SUMMARY to the value of CHDRMJA-POLSUM and the
*  NEW-SUMMARY to the value of the lowest UTRN-PLAN-SUFFIX,
*  then call 'BRKOUT'.
*
*  Commission Processing.
*
*  Commission details are represented by AGCM records. These
*  hold the details of the premium and its Paid-to-Date.
*  If a new AGCM is being created and a PCDT transaction
*  record exists for that component then the commission agent
*  on that AGCM must be taken from there. When this happens
*  there may be more than one agent in the split. If this is
*  so then multiple AGCM records must be created - one for
*  each agent in the split - and the amount of the premium
*  divided up amongst the AGCM records in the proportions of
*  the specified split. These AGCM records will all have the
*  same sequence number, AGCM-SEQNO, thus tieing them
*  permanently together.
*
*  When processing has completed for a component the PCDT
*  record may be deleted, as it was only used for the new
*  increased portion of the premium.
*
*  Write two ACMV records with the details as shown in the
*  Notes section at the end of this specification. The
*  subroutine LIFACMV will be called in order to write these
*  ACMV records.
*
*  Notes.
*
*  Tables:
*  T5645 - Transaction Accounting Rules       Key: Program Id.
*  T5671 - Coverage/Rider Switching           Key: Tran Code||CRTABLE
*  T5687 - General Coverage/Rider Details     Key: CRTABLE
*
*  PTRN Record:
*  . PTRN-BATCTRCDE         -  Transaction Code
*  . PTRN-CHDRCOY           -  CHDRMJA-CHDRCOY
*  . PTRN-CHDRNUM           -  CHDRMJA-CHDRNUM
*  . PTRN-TRANNO            -  CHDRMJA-TRANNO
*  . PTRN-TRANSACTION-DATE  -  Current Date
*  . PTRN-TRANSACTION-TIME  -  Current Time
*  . PTRN-PTRNEFF           -  Current Date
*  . PTRN-TERMID            -  Passed in Linkage
*  . PTRN-USER              -  Passed in Linkage.
*
*  AGCM Record:
*
*  The key will contain the component key plus the relevant
*  agent number. Other relevant fields will be set as follows:
*
*  . AGCM-TRANNO            -  the new TRANNO
*  . AGCM-EFDATE            -  the transaction effective date
*  . AGCM-SINGP             -  the increase of the new premium
*                              over the old.
*  . AGCM-BASIC-COMM-METH   -  Code from table T5687
*  . AGCM-INITCOM           -  calculated by the commission
*                              calculation method.
*  . AGCM-BASCPY            -  Basic Commission Payment Method
*                              from T5687, if blank from AGNT,
*                              if blank there - error.
*  . AGCM-COMPAY            -  zero
*  . AGCM-COMERN            -  zero
*  . AGCM-SRVCPY            -  Service Commission Payment
*                              Method from T5687, if blank from
*                              AGNT, if blank there use spaces.
*  . AGCM-SCMDUE            -  zero
*  . AGCM-SCMEARN           -  zero
*  . AGCM-RNWCPY            -  Renewal Commission Payment
*                              Method from T5687, if blank from
*                              AGNT, if blank there use spaces.
*  . AGCM-RNLCDUE           -  zero
*  . AGCM-RNLCEARN          -  zero
*  . AGCM-AGENT-CLASS       -  from AGNT
*  . AGCM-TERMID            -  Term Id. passed in Linkage
*  . AGCM-TRANSACTION-DATE  -  Current System Date
*  . AGCM-TRANSACTION-TIME  -  Current System Time
*  . AGCM-USER              -  User Id. passed in linkage
*  . AGCM-VALIDFLAG         -  '1'
*  . AGCM-CURRFROM          -  Current System Date
*  . AGCM-CURRTO            -  all 9's
*
*  The commission calculation subroutine will called to
*  calculate AGCM-INITCOM. The correct subroutine will be
*  obtained from T5647 which is read with the Basic Commission
*  Payment Method from T5687.
*
*  The Commission Calculation subroutine must be called with
*  the current age of the life assured at component change
*  effective date, the Effective Date itself, the Premium and
*  the Premium Cessation Date.
*
*  ACMV Record:
*
*  Two ACMV records will be created for each deactivation; one
*  for Commission Clawback for agents, and one for
*  Re-crediting Agent Commission Advance. The details for
*  these will be held on T5645, accessed by program Id. Line
*  #1 will give the details for the first posting for which
*  the amount on the ACMV will be negative and line #2 will
*  give the details for the second posting for which the
*  amount on the ACMV will be positive.
*
*  Another ACMV record will be required for PREMIUMS, this will
*  be in position '4' in T5645.
*
*  Note: the subroutine LIFACMV will be called to add these
*  records, call with a function of PSTW so that ACBL records are
*  created in the subroutine.
*
*  The key will be the batch transaction key passed in the
*  linkage section. The remaining fields will be set as
*  follows:
*
*  . ACMV-RLDGCOY           -  Contract Company
*  . ACMV-SACSCODE          -  from T5645
*  . ACMV-RLDGACCT          -  Agent number
*  . ACMV-ORIGCURR          -  Contract Currency
*  . ACMV-SACSTYP           -  from T5645
*  . ACMV-RDOCNUM           -  Contract Number
*  . ACMV-TRANNO            -  The new TRANNO
*  . ACMV-JRNSEQ            -  blank
*  . ACMV-ORIGAMT           -  amount of clawback in Contract
*                              Currency - negative for the 1st.
*                              ACMV and positive for the second
*                              (Earned minus Paid).
*  . ACMV-TRANREF           -
*  . ACMV-CRATE             -  Currency Conversion Rate
*  . ACMV-ACCTAMT           -  ORIGAMT * CRATE
*  . ACMV-GENLCOY           -  FSU Company
*  . ACMV-GENLCUR           -  ?
*  . ACMV-GLCODE            -  from T5645
*  . ACMV-GLSIGN            -  from T5645
*  . ACMV-POSTYEAR          -  from Batch Transaction details
*  . ACMV-POSTMONTH         -    "    "        "         "
*  . ACMV-EFFDATE           -  Effective Date of change.
*  . ACMV-RCAMT             -
*  . ACMV-FRCDATE           -  ?
*  . ACMV-INTEXTIND         -  ?
*  . ACMV-TRANSACTION-DATE  -  Current System Date
*  . ACMV-TRANSACTION-TIME  -  Current System Time
*  . ACMV-USER              -  User Id. passed in linkage
*  . ACMV-TERMID            -  Term Id. passed in linkage
*
*  RTRN Record:
*
*  Policy Suspense will need to be reduced by the Single Premium
*  Top-Up.  To  do  this call LIFRTRN which will create an RTRN;
*  the same fields as for ACMV will be required.
*  This has to be called for only position '3' in T5645. No ACMV
*  is required for this number '3' entry.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are COVRSTS and COVRSTA.
* COVRSTS allow only validflag 1 records, and COVRSTA
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through its life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as its item name, the transaction code and
* the contract status of a contract, e.g. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, e.g. AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to COVRSTA records, and
* current categories refer to COVRSTS records. AGCMSTS
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the AGCMSTS, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out up to and including the TRANNOR number.
*
******************Enhancements for Life Asia 1.0****************
*
* Referring to enhancement in P5144 where on performing single
* premium top up, the suspense currency entered by the user
* should be considered in updating COVR and its GL entries.
*
*
*****************************************************
* </pre>
*/
public class P5147at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5147AT");

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	protected PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData wsaaPremCcy = new FixedLengthStringData(3).isAPartOf(wsaaTransactionRec, 21);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 24);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	protected FixedLengthStringData wsaaSbmaction = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 8);

	private FixedLengthStringData wsaaT5687Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5687Table = new FixedLengthStringData(4).isAPartOf(wsaaT5687Item, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaT5687Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5645Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5645Prog = new FixedLengthStringData(5).isAPartOf(wsaaT5645Item, 0);
	private FixedLengthStringData filler3 = new FixedLengthStringData(3).isAPartOf(wsaaT5645Item, 5, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5647Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5647Cmth = new FixedLengthStringData(4).isAPartOf(wsaaT5647Item, 0);
	private FixedLengthStringData filler4 = new FixedLengthStringData(4).isAPartOf(wsaaT5647Item, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private PackedDecimalData wsaaLowestPlnsfx = new PackedDecimalData(4, 0);
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	protected PackedDecimalData wsaaNetPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalSuspense = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaOrigNetPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotPremTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotNonInvestTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotCd = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0);

	protected FixedLengthStringData wsbbPayrkey = new FixedLengthStringData(9);
	protected FixedLengthStringData wsbbChdrnum = new FixedLengthStringData(8).isAPartOf(wsbbPayrkey, 0);
	protected FixedLengthStringData wsbbPayrseqno = new FixedLengthStringData(1).isAPartOf(wsbbPayrkey, 8);
	protected ZonedDecimalData wsaaTday = new ZonedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(4, 0).init(ZERO);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaAgcmseq = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaIfOvrdCommEnd = new FixedLengthStringData(1);
	private Validator wsaaEndOvrdComm = new Validator(wsaaIfOvrdCommEnd, "Y");

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	protected FixedLengthStringData wsaaBasicCommMeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBascpy = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaBastcmth = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaBastcpy = new FixedLengthStringData(4);

	protected FixedLengthStringData wsaaTr695Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaTr695Coverage = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 0);
	protected FixedLengthStringData wsaaTr695Rider = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 4);
	protected ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	protected ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler5, 4).setUnsigned();
	protected ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler5, 6).setUnsigned();
	//ILIFE-3332 AT jobs Bomb_Single premium with Top Up_SA
	private FixedLengthStringData wsaaPremMeth= new FixedLengthStringData(4);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String f294 = "F294";
	private static final String e366 = "E366";
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	protected Syserrrec syserrrec = new Syserrrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	protected Varcom varcom = new Varcom();
	private Batcuprec batcuprec = new Batcuprec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Itemkey wsaaItemkey = new Itemkey();
	protected T5687rec t5687rec = new T5687rec();
	private T5645rec t5645rec = new T5645rec();
	private T5647rec t5647rec = new T5647rec();
	protected T5688rec t5688rec = new T5688rec();
	protected T6687rec t6687rec = new T6687rec();
	protected Comlinkrec comlinkrec = new Comlinkrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	protected Tr695rec tr695rec = new Tr695rec();
	protected Prasrec prasrec = new Prasrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	protected Ubblallpar ubblallpar = new Ubblallpar();
	protected Datcon4rec datcon4rec = new Datcon4rec();
	protected T5534rec t5534rec = new T5534rec();
	protected Premiumrec premiumrec = new Premiumrec();
	private T5675rec t5675rec = new T5675rec();
	private T7508rec t7508rec = new T7508rec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	private Th605rec th605rec = new Th605rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Getdescrec getdescrec = new Getdescrec();

	protected Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	protected FormatsInner formatsInner = new FormatsInner();
	protected TablesInner tablesInner = new TablesInner();
	private ExternalisedRules er = new ExternalisedRules();
	//ILIFE-3997-STARTS
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAOImpl.class);
	//ILIFE-3997-ENDS
	
	private Taxdpf taxdbilpf = new Taxdpf();
	private TaxdpfDAO taxdbilpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private List<Taxdpf> taxdbilpfList = null;
	private List<Taxdpf> updateTaxdList = null;
 //ILIFE-8786 start	
	private UtrnpfDAO utrnpfDAO	 = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	private List<Zrstpf> zrstnudInsertList  =  new ArrayList<>();
	private List<Zrstpf> zrstUpdateList =  new ArrayList<>();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private Clrfpf clrfpf = new Clrfpf();
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	protected Payrpf payrpf=new Payrpf();
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private Ptrnpf ptrnpf = new Ptrnpf();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ZctnpfDAO zctnpfDAO =  getApplicationContext().getBean("zctnpfDAO", ZctnpfDAO.class);
	private List<Zctnpf> insertZctnpfList = new ArrayList<>();
	private List<Zptnpf> insertZptnpfList = new ArrayList<>();
	private ZptnpfDAO zptnpfDAO = getApplicationContext().getBean("zptnpfDAO", ZptnpfDAO.class);
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private SftlockUtil sftlockUtil = new SftlockUtil();
	private Agcmpf agcmpf = new Agcmpf();
	private AgcmpfDAO agcmpfDAO =  getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
	private List<Agcmpf> agcmpfListInst = new ArrayList<>();
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
	private Aglfpf aglfpf  = new Aglfpf();
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private PcdtpfDAO pcdtpfDAO = getApplicationContext().getBean("pcdtpfDAO",PcdtpfDAO.class);
	private Pcdtpf pcdtmja = new Pcdtpf();
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	List<Long> deleteRecords = new ArrayList<>();
 //ILIFE-8786 end
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		pcdt5010,
		loop5015,
		exit5090,
		exit7190,
		next8570,
		exit8590
	}

	public P5147at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		mainline0001();
		exit0009();
	}

protected void mainline0001()
	{
		comlinkrec.currto.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		comlinkrec.language.set(atmodrec.language);
		/* Get the today date                                              */
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			xxxxFatalError();
		}
		wsaaTday.set(datcon1rec.intDate);
		updateChdr1000();
		writePtrn2000();
		writeBatcup3000();
		breakout4000();
		createAgcm5000();
		//ILIFE-3997-STARTS
		Chdrpf chdrpf = null;
		chdrpf = chdrpfDAO.getchdrRecord(ptrnpf.getChdrcoy(),ptrnpf.getChdrnum()); //ILIFE-8786
		if(chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
			writeNLGRec();
		//ILIFE-3997-ENDS
		dryProcessing8600();
		/*    PERFORM 9000-RELEASE-SOFTLOCK.                               */
		b000BenefitBilling();
		a000Statistics();
		releaseSoftlock9000();
	}

protected void exit0009()
	{
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void updateChdr1000()
	{
		readh1010();
		rewrt1020();
	}

protected void readh1010()
	{
		/*    Read & hold CHDRMJA record*/
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransactionRec.set(atmodrec.transArea);
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaChdrnum);
		chdrmjaIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			xxxxFatalError();
		}
	}

protected void rewrt1020()
	{
		/*    Update CHDRMJA header with TRANNO + 1*/
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void writePtrn2000()
	{
		/*    Writes a transaction record to PTRN file*/
 		//ILIFE-8786 start
		ptrnpf = new Ptrnpf();
		ptrnpf.setChdrpfx(chdrmjaIO.getChdrpfx().toString());
		ptrnpf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrmjaIO.getChdrnum().toString());
		ptrnpf.setTranno(chdrmjaIO.getTranno().toInt());
		ptrnpf.setUserT(wsaaUser.toInt());
		ptrnpf.setTermid(wsaaTermid.toString());
		/* MOVE TDAY                   TO DTC1-FUNCTION.                */
		/* CALL 'DATCON1'              USING DTC1-DATCON1-REC.          */
		/* MOVE DTC1-INT-DATE          TO WSAA-TODAY.                   */
		/* MOVE WSAA-TODAY             TO PTRN-PTRNEFF.                 */
		ptrnpf.setPtrneff(wsaaEffdate.toInt());
		/* MOVE WSAA-EFFDATE           TO PTRN-DATESUB.         <LA5184>*/
		ptrnpf.setDatesub(wsaaTday.toInt());
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(atmodrec.company.toString());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		wsaaBatckey.set(atmodrec.batchKey);
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setValidflag("1");
		ptrnpfDAO.insertPtrnPF(ptrnpf);
		
		 //ILIFE-8786 end
	}

protected void writeBatcup3000()
	{
		batcup3010();
	}

protected void batcup3010()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		batcuprec.batccoy.set(wsaaBatckey.batcBatccoy);
		batcuprec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		batcuprec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Call the BRKOUT subroutine
	* </pre>
	*/
protected void breakout4000()
	{
		go4010();
	}

protected void go4010()
	{
		/*  A physical breakout will be required if any UTRN records*/
		/*  exist for the transaction that has a Plan Suffix that is*/
		/*  less than or equal to CHDRMJA-POLSUM. All the UTRN records*/
		/*  will have to be read to determine whether or not this is*/
		/*  necessary.*/
		readUtrn7000();
		readHitr7100();
		if (isGT(wsaaLowestPlnsfx, chdrmjaIO.getPolsum())) {
			return ;
		}
		wsaaTransactionRec.set(atmodrec.transArea);
		brkoutrec.brkOldSummary.set(chdrmjaIO.getPolsum());
		compute(brkoutrec.brkNewSummary, 0).set(sub(wsaaLowestPlnsfx, 1));
		brkoutrec.brkChdrnum.set(atmodrec.primaryKey);
		brkoutrec.brkChdrcoy.set(atmodrec.company);
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(SPACES);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, "****")) {
			syserrrec.params.set(brkoutrec.brkStatuz);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Create AGCM records
	* </pre>
	*/
protected void createAgcm5000()
	{
		go5010();
	}

protected void go5010()
	{
		/* First READ T5688 to see whether contract type requires          */
		/*  Component Level Accounting.                                    */
		readTableT56885200();
		/* If a new AGCM is being created and a PCDT transaction record*/
		/* exists for that component then the commission agent on that*/
		/* AGCM must be taken from there. When this happens there may be*/
		/* more than one agent in the split. If this is so then multiple*/
		/* AGCM records must be created - one for each agent in the split-*/
		/* and the amount of the premium divided up amongst the AGCM*/
		/* in the proportions of the specified split. These AGCM records*/
		/* will all have the same sequence number.*/
		wsaaJrnseq.set(0);
 //ILIFE-8786 start
		/**
		* <pre>
		* Read and check to see if any PCDT records exist.
		* </pre>
		*/
		List<Pcdtpf>pcdtpfList = pcdtpfDAO.getPcdtmja(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString(), chdrmjaIO.getTranno().toInt());
		for(Pcdtpf pcdtpf:pcdtpfList) {
			pcdtmja=pcdtpf;
			getNewAgcmSeqno5900();
			loop5015();
	 //ILIFE-8786 end		
		}
			pcdtpfDAO.deletePcdtmjaRecord(deleteRecords);
	}
protected void loop5015()
	{
		/*  Read TH605 to judge if Bonus Workbench Extraction used         */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(atmodrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/*    IF ITEM-STATUZ           NOT = O-K AND MRNF          <V70L01>*/
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		wsaaIndex.add(1);
		if (isGT(wsaaIndex, 10)
		|| isEQ(pcdtmja.getAgntnum(wsaaIndex), SPACES)) {
			calculateNetPremium8200();
			b200GetPremTax();
			calculateTolerance8300();
			/* Bonus Workbench *                                               */
			if (isEQ(th605rec.bonusInd, "Y")) {
				c100PremiumHistory();
			}
			callLifrtrnLifacmv8500();
			b300PostTax();
			/**
			* <pre>
			* Delete  PCDT record after creation of AGCM
			* </pre>
			*/
			deleteRecords.add(pcdtmja.getUniqueNumber());
			return;
		}
		//ILIFE-8786 start
		aglfpf = aglfpfDAO.searchAglflnb(chdrmjaIO.getAgntcoy().toString(), pcdtmja.getAgntnum(wsaaIndex));
		if (aglfpf == null) {
			aglfpf = new Aglfpf();
			aglfpf.setReportag(SPACES.stringValue());
		}
		addAgcm5020();
		//ILIFE-8786 end
	}

	/**
	* <pre>
	*    MOVE  READR                 TO  AGCM-FUNCTION.
	*    CALL 'AGCMIO'               USING AGCM-PARAMS.
	*    IF CHDRMJA-CHDRCOY          NOT = AGCM-CHDRCOY OR
	*       CHDRMJA-CHDRNUM          NOT = AGCM-CHDRNUM
	*       GO TO 5090-EXIT.
	*    IF AGCM-STATUZ              = MRNF
	*       GO TO 5020-ADD-AGCM.
	*    IF AGCM-STATUZ              NOT = O-K
	*       MOVE AGCM-PARAMS         TO SYSR-PARAMS
	*       PERFORM XXXX-FATAL-ERROR.
	*    GO TO 5030-UPD-AGCM.
	* </pre>
	*/
protected void addAgcm5020()
	{
 		//ILIFE-8786 start
		agcmpf.setChdrcoy(pcdtmja.getChdrcoy());
		agcmpf.setChdrnum(pcdtmja.getChdrnum());
		agcmpf.setTranno(pcdtmja.getTranno());
		agcmpf.setPlnsfx(pcdtmja.getPlnsfx());
		agcmpf.setLife(pcdtmja.getLife());
		agcmpf.setCoverage(pcdtmja.getCoverage());
		agcmpf.setRider(pcdtmja.getRider());
		agcmpf.setAgntnum(pcdtmja.getAgntnum(wsaaIndex));
		agcmpf.setEfdate(wsaaEffdate.toInt());
		agcmpf.setAnnprem(pcdtmja.getInstprem());
		readT56878000();
		if (isNE(pcdtmja.getRider(), "00")) {
			readTr6958050();
		}
		/* IF T5687-BASIC-COMM-METH    =   SPACES                       */
		/* IF T5687-BASTCMTH           =   SPACES                       */
		if (isEQ(wsaaBastcmth, SPACES)) {
			loop5015(); //ILIFE-8786
		}
		/*MOVE T5687-BASIC-COMM-METH  TO  AGCM-BASIC-COMM-METH.        */
		/* MOVE T5687-BASTCMTH         TO  AGCM-BASIC-COMM-METH.        */
		agcmpf.setBascmeth(wsaaBastcmth.toString());
		agcmpf.setInitcom(BigDecimal.ZERO);
		calcInitComm5600();
		comlinkrec.agent.set(pcdtmja.getAgntnum(wsaaIndex)); //ILIFE-8786
		/* MOVE 'LINJ'                 TO CLNK-FUNCTION.           <004>*/
		callT56475650();
		/*    COMPUTE AGCM-INITCOM = CLNK-ICOMMTOT *                       */
		/*                        PCDTMJA-SPLITC (WSAA-INDEX) / 100.       */
		setPrecision(agcmpf.getInitcom(), 3);
		agcmpf.setInitcom((div(mult(comlinkrec.icommtot, pcdtmja.getSplitc(wsaaIndex)), 100)).getbigdata()); //ILIFE-8786
		/* IF  T5687-BASCPY = SPACE                                     */
		/* IF  WSAA-BASCPY  = SPACE                             <A06695>*/
		if (isEQ(wsaaBastcpy, SPACES)) {
			agcmpf.setBascpy(aglfpf.getBcmtab());
		}
		else {
			/*     MOVE T5687-BASCPY           TO  AGCM-BASCPY.             */
			/*     MOVE WSAA-BASCPY            TO  AGCM-BASCPY.     <A06695>*/
			agcmpf.setBascpy(wsaaBastcpy.toString());
		}
		agcmpf.setCompay(agcmpf.getInitcom());
		agcmpf.setComern(agcmpf.getInitcom());
		/* MOVE T5687-SRVCPY           TO  AGCM-SRVCPY.                 */
		agcmpf.setSrvcpy(SPACES.stringValue());
		agcmpf.setScmdue(BigDecimal.ZERO);
		agcmpf.setScmearn(BigDecimal.ZERO);
		/* MOVE T5687-RNWCPY           TO  AGCM-RNWCPY.                 */
		agcmpf.setRnwcpy(SPACES.stringValue());
		agcmpf.setRnlcdue(BigDecimal.ZERO);
		agcmpf.setRnlcearn(BigDecimal.ZERO);
		/* MOVE SPACES                 TO  AGCM-AGENT-CLASS.            */
		agcmpf.setAgcls(aglfpf.getAgentClass());
		agcmpf.setTermid(wsaaTermid.toString());
		agcmpf.setTrdt(wsaaTransactionDate.toInt());
		agcmpf.setTrtm(wsaaTransactionTime.toInt());
		agcmpf.setUserT(wsaaUser.toInt());
		agcmpf.setValidflag("1");
		agcmpf.setCurrfrom(chdrmjaIO.getCurrfrom().toInt());
		/*    MOVE 99999999               TO  AGCM-CURRTO                  */
		/*                                    AGCM-PTDATE.                 */
		agcmpf.setCurrto(99999999);
		/* MOVE WSAA-EFFDATE           TO  AGCM-PTDATE.         <A05940>*/
		agcmpf.setPtdate(0);
		/*    MOVE ZEROES                 TO  AGCM-SEQNO.             <008>*/
		agcmpf.setSeqno(wsaaAgcmseq.toInt());
		agcmpf.setCedagent(SPACES.stringValue());
		agcmpf.setOvrdcat("B");
		agcmpfListInst.add(agcmpf);
		agcmpfDAO.insertAgcmpfList(agcmpfListInst);
		agcmpfListInst.clear();
		 //ILIFE-8786 end
		/* Bonus Workbench *                                               */
		if (isEQ(th605rec.bonusInd, "Y")) {
			c200CommissionHistory();
		}
		/* Post Initial Paid.                                              */
		createAcmv6000();
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(aglfpf.getAgntnum()); //ILIFE-8786
		lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec.glcode.set(t5645rec.glmap[1]);
		lifacmvrec.glsign.set(t5645rec.sign[1]);
		 //ILIFE-8786 start
		covrpf = covrpfDAO.getcovrincrRecord(pcdtmja.getChdrcoy(),pcdtmja.getChdrnum(),pcdtmja.getLife(),
				pcdtmja.getCoverage(),pcdtmja.getRider(),pcdtmja.getPlnsfx()); 
		if(covrpf==null) {
			syserrrec.params.set(pcdtmja.getChdrcoy().concat(pcdtmja.getChdrnum()));
			syserrrec.statuz.set(varcom.mrnf);
			xxxxFatalError();
		}
		 //ILIFE-8786 end
		lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable()); //ILIFE-8786
		wsaaPlan.set(pcdtmja.getPlnsfx()); //ILIFE-8786
		wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum()); //ILIFE-8786
		wsaaRldgLife.set(pcdtmja.getLife()); //ILIFE-8786
		wsaaRldgCoverage.set(pcdtmja.getCoverage()); //ILIFE-8786
		wsaaRldgRider.set(pcdtmja.getRider()); //ILIFE-8786
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		writeAcmv6050();
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmpf.getAnnprem()); //ILIFE-8786
			zorlnkrec.effdate.set(agcmpf.getEfdate()); //ILIFE-8786
			b100CallZorcompy();
		}
		/* Post Initial Earned.                                            */
		/* Check for Component level accounting & act accordingly          */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			/*        Set up new RLDGACCT field for Component Level Accounting */
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(pcdtmja.getPlnsfx()); //ILIFE-8786
			wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum());
			wsaaRldgLife.set(pcdtmja.getLife()); //ILIFE-8786
			wsaaRldgCoverage.set(pcdtmja.getCoverage()); //ILIFE-8786
			wsaaRldgRider.set(pcdtmja.getRider()); //ILIFE-8786
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/*       Read the coverage file to get the Coverage/Rider table to */
			/*        put into LIFA-SUBSTITUTE-CODE ( 6 ).                     */
			//ILIFE-8786 
			if(covrpf!=null) {
				lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
			}
			lifacmvrec.sacscode.set(t5645rec.sacscode[8]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[8]);
			lifacmvrec.glcode.set(t5645rec.glmap[8]);
			lifacmvrec.glsign.set(t5645rec.sign[8]);
		}
		else {
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode[2]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[2]);
			lifacmvrec.glcode.set(t5645rec.glmap[2]);
			lifacmvrec.glsign.set(t5645rec.sign[2]);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		/* MOVE T5645-SACSCODE (2)     TO LIFA-SACSCODE.           <015>*/
		/* MOVE T5645-SACSTYPE (2)     TO LIFA-SACSTYP.            <015>*/
		/* MOVE T5645-GLMAP (2)        TO LIFA-GLCODE.             <015>*/
		/* MOVE T5645-SIGN (2)         TO LIFA-GLSIGN.             <015>*/
		writeAcmv6050();
		wsaaIfOvrdCommEnd.set("N");
		if (isNE(aglfpf.getReportag(), SPACES)) {
			while ( !(wsaaEndOvrdComm.isTrue())) {
				overrideComm5800();
			}

		}
		loop5015(); //ILIFE-8786
	}

protected void readTableT56885200()
	{
		read5210();
	}

protected void read5210()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrmjaIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			xxxxFatalError();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}
	/**
	* <pre>
	* Calculate AGCM-INITCOM. The correct subroutine will be
	* obtained from T5647 which is read with the read with the
	* Basic Commission CALC Method from T5687.
	* </pre>
	*/
protected void calcInitComm5600()
	{
		calc5600();
	}

protected void calc5600()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5647);
		wsaaT5647Item.set(SPACES);
		/* MOVE T5687-BASTCMTH         TO WSAA-T5647-CMTH.              */
		wsaaT5647Cmth.set(wsaaBastcmth);
		itemIO.setItemitem(wsaaT5647Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
		/* MOVE SPACES                  TO CLNK-FUNCTION.               */
		comlinkrec.function.set(SPACES);
		comlinkrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		comlinkrec.chdrnum.set(chdrmjaIO.getChdrnum());
		comlinkrec.life.set(pcdtmja.getLife()); //ILIFE-8786
		comlinkrec.coverage.set(pcdtmja.getCoverage()); //ILIFE-8786
		comlinkrec.rider.set(pcdtmja.getRider()); //ILIFE-8786
		comlinkrec.planSuffix.set(pcdtmja.getPlnsfx()); //ILIFE-8786
		/*MOVE WSAA-EFFDATE            TO CLNK-OCCDATE.*/
		comlinkrec.effdate.set(wsaaEffdate);
		/*MOVE T5687-BASTCPY           TO CLNK-METHOD.                 */
		/* MOVE T5687-BASTCMTH          TO CLNK-METHOD.                 */
		comlinkrec.method.set(wsaaBastcmth);
		comlinkrec.agentClass.set(aglfpf.getAgentClass()); //ILIFE-8786
		comlinkrec.crtable.set(wsaaCrtable);
		comlinkrec.annprem.set(pcdtmja.getInstprem()); //ILIFE-8786
		/* MOVE PCDTMJA-AGNTNUM (WSAA-INDEX)   TO  CLNK-AGENT.*/
		comlinkrec.ptdate.set(0);
	}

protected void callT56475650()
	{
		/*CALL*/
		callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
		/* IF CLNK-STATUZ              NOT = O-K                        */
		if (isNE(comlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

	
	/**
	* <pre>
	* New Section, where the Commission is re-calculated for the
	* selling agent and the Override Percentage is applied.
	* </pre>
	*/
protected void overrideComm5800()
	{
		para5810();
	}

protected void para5810()
	{
		if (isEQ(wsaaIfOvrdCommEnd, "Y")) {
			return ;
		}
 		//ILIFE-8786 start
		agcmpf.setCedagent(agcmpf.getAgntnum());
		comlinkrec.agent.set(aglfpf.getReportag());
		agcmpf.setAgntnum(aglfpf.getReportag());
		/* MOVE 'LINJO'                TO CLNK-FUNCTION.           <004>*/
		callT56475650();
		setPrecision(agcmpf.getInitcom(), 3);
		agcmpf.setInitcom((div(mult(div(mult(comlinkrec.icommtot, pcdtmja.getSplitc(wsaaIndex)), 100), aglfpf.getOvcpc()), 100)).getbigdata()); //ILIFE-8786
		agcmpf.setCompay(agcmpf.getInitcom());
		agcmpf.setComern(agcmpf.getInitcom());
		agcmpf.setOvrdcat("O");
		/* MOVE AGLFLNB-BCMTAB         TO  AGCM-BASCPY.            <005>*/
		agcmpf.setEfdate(wsaaEffdate.toInt());
		agcmpf.setAnnprem(pcdtmja.getInstprem()); //ILIFE-8786
		readT56878000();
		if (isNE(pcdtmja.getRider(), "00")) {
			readTr6958050();
		}
		/* IF T5687-BASTCPY            = SPACES                 <V42004>*/
		if (isEQ(wsaaBastcpy, SPACES)) {
			agcmpf.setBascpy(aglfpf.getBcmtab());
		}
		else {
			/*     MOVE T5687-BASTCPY      TO AGCM-BASCPY           <V42004>*/
			agcmpf.setBascpy(wsaaBastcpy.toString());
		}
		/* MOVE T5687-BASIC-COMM-METH  TO  AGCM-BASIC-COMM-METH.        */
		agcmpf.setBascmeth(wsaaBasicCommMeth.toString());
		agcmpf.setSrvcpy(SPACES.stringValue());
		agcmpf.setScmdue(BigDecimal.ZERO);
		agcmpf.setScmearn(BigDecimal.ZERO);
		agcmpf.setRnwcpy(SPACES.stringValue());
		agcmpf.setRnlcdue(BigDecimal.ZERO);
		agcmpf.setRnlcearn(BigDecimal.ZERO);
		/* MOVE SPACES                 TO  AGCM-AGENT-CLASS.            */
		agcmpf.setAgcls(aglfpf.getAgentClass());
		agcmpf.setTermid(wsaaTermid.toString());
		agcmpf.setTrdt(wsaaTransactionDate.toInt());
		agcmpf.setTrtm(wsaaTransactionTime.toInt());
		agcmpf.setUserT(wsaaUser.toInt());
		agcmpf.setValidflag("1");
		agcmpf.setCurrfrom(chdrmjaIO.getCurrfrom().toInt());
		/*    MOVE 99999999               TO  AGCM-CURRTO                  */
		/*                                    AGCM-PTDATE.                 */
		agcmpf.setCurrto(99999999);
		/* MOVE WSAA-EFFDATE           TO  AGCM-PTDATE.         <A05940>*/
		agcmpf.setPtdate(0);
		/*    MOVE ZEROES                 TO  AGCM-SEQNO.             <008>*/
		agcmpf.setSeqno(wsaaAgcmseq.toInt());
		agcmpfListInst.add(agcmpf);
		agcmpfDAO.insertAgcmpfList(agcmpfListInst);
		agcmpfListInst.clear();
		 //ILIFE-8786 end
		/* Post Override Paid.                                             */
		createAcmv6000();
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(aglfpf.getReportag()); //ILIFE-8786
		lifacmvrec.tranref.set(agcmpf.getCedagent()); //ILIFE-8786
		lifacmvrec.sacscode.set(t5645rec.sacscode[5]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[5]);
		lifacmvrec.glcode.set(t5645rec.glmap[5]);
		lifacmvrec.glsign.set(t5645rec.sign[5]);
 //ILIFE-8786 
		covrpf = covrpfDAO.getcovrincrRecord(pcdtmja.getChdrcoy(),pcdtmja.getChdrnum(),pcdtmja.getLife(),
				pcdtmja.getCoverage(),pcdtmja.getRider(),pcdtmja.getPlnsfx());
		if(covrpf!=null) {
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		}
		
		writeAcmv6050();
		/* Post Override Earned.                                           */
		/* Check for Component level accounting & act accordingly          */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			/*        Set up new RLDGACCT field for Component Level Accounting */
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(pcdtmja.getPlnsfx());
			wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum());
			wsaaRldgLife.set(pcdtmja.getLife());
			wsaaRldgCoverage.set(pcdtmja.getCoverage());
			wsaaRldgRider.set(pcdtmja.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/*       Read the coverage file to get the Coverage/Rider table to */
			/*        put into LIFA-SUBSTITUTE-CODE ( 6 ).                     */
			
			if(covrpf!=null) {
				lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
			}
			
			
			lifacmvrec.sacscode.set(t5645rec.sacscode[10]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[10]);
			lifacmvrec.glcode.set(t5645rec.glmap[10]);
			lifacmvrec.glsign.set(t5645rec.sign[10]);
		}
		else {
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode[6]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[6]);
			lifacmvrec.glcode.set(t5645rec.glmap[6]);
			lifacmvrec.glsign.set(t5645rec.sign[6]);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		/* MOVE T5645-SACSCODE (6)     TO LIFA-SACSCODE.           <015>*/
		/* MOVE T5645-SACSTYPE (6)     TO LIFA-SACSTYP.            <015>*/
		/* MOVE T5645-GLMAP (6)        TO LIFA-GLCODE.             <015>*/
		/* MOVE T5645-SIGN (6)         TO LIFA-GLSIGN.             <015>*/
		writeAcmv6050();
		/* if the AGLF record has a override agent then repeat             */
		/* add another agcm record (repeate this process until             */
		/* there are no override agents for a override agent).........     */
 		//ILIFE-8786 start
		aglfpf = aglfpfDAO.searchAglflnb(chdrmjaIO.getAgntcoy().toString(), aglfpf.getReportag());
		if (aglfpf != null) {
			if (isEQ(aglfpf.getReportag(), SPACES)) {
				wsaaIfOvrdCommEnd.set("Y");
			}
		}else {
			wsaaIfOvrdCommEnd.set("Y");
		}
		 //ILIFE-8786 end
	}

protected void getNewAgcmSeqno5900()
	{
	/**
	* <pre>
	*Get the last AGCM sequence number and increment it by 1 to
	*obtain the new AGCM sequence number.
	* </pre>
	*/
		wsaaAgcmseq.set(ZERO);
		 //ILIFE-8786 start
		Agcmpf a = new Agcmpf();
		a.setChdrcoy(pcdtmja.getChdrcoy());
		a.setChdrnum(pcdtmja.getChdrnum());
		a.setLife(pcdtmja.getLife());
		a.setCoverage(pcdtmja.getCoverage());
		a.setRider(pcdtmja.getRider());
		a.setPlnsfx(pcdtmja.getPlnsfx());
		a.setSeqno(0);
		agcmpfDAO.searchAgcmpfRecord(a);
		compute(wsaaAgcmseq, 0).set(add(1, a.getSeqno()));
		 //ILIFE-8786 end
	}
	/**
	* <pre>
	* Create ACMV records via call to LIFACMV
	* </pre>
	*/
protected void createAcmv6000()
	{
		go6010();
	}

protected void go6010()
	{
		/* MOVE ZERO                   TO WSAA-INDEX1.                  */
		/*6020-LOOP.                                                       */
		/*    ADD 1                       TO WSAA-INDEX1.                  */
		/*    IF WSAA-INDEX1              > 2                              */
		/*    GO TO 6090-EXIT.                                          */
		lifacmvrec.function.set("PSTW");
		wsaaBatckey.set(atmodrec.batchKey);
		lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		readT56458100();
		lifacmvrec.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec.tranno.set(chdrmjaIO.getTranno());
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		/*    MOVE ZEROES                 TO  LIFA-JRNSEQ.*/
		lifacmvrec.rldgcoy.set(wsaaBatckey.batcBatccoy);
		/* MOVE T5645-SACSCODE (WSAA-INDEX1)   TO  LIFA-SACSCODE.       */
		/* The following lines have been commented out during change 013   */
		/*  because they are redundant and seem to have been since         */
		/*  change 004.                                                    */
		/* IF WSAA-INDEX1 = 1                                      <015>*/
		/* MOVE PCDTMJA-AGNTNUM (WSAA-INDEX)                            */
		/*                         TO  LIFA-RLDGACCT.                   */
		/* IF WSAA-INDEX1 = 2                                      <015>*/
		/* MOVE CHDRMJA-CHDRNUM                                         */
		/*                         TO  LIFA-RLDGACCT.                   */
		/* End of commenting out at change 013                             */
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		/* MOVE T5645-SACSTYPE (WSAA-INDEX1)  TO  LIFA-SACSTYP.*/
		lifacmvrec.origamt.set(agcmpf.getInitcom()); //ILIFE-8786
		lifacmvrec.tranref.set(chdrmjaIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(ZERO);
		compute(lifacmvrec.acctamt, 10).setRounded(mult(lifacmvrec.origamt, lifacmvrec.crate));
		lifacmvrec.genlcoy.set(atmodrec.company);
		/*    MOVE CHDRMJA-CNTCURR        TO  LIFA-GENLCUR.                */
		lifacmvrec.genlcur.set(SPACES);
		/* MOVE T5645-GLMAP (WSAA-INDEX1)  TO  LIFA-GLCODE.             */
		/* MOVE T5645-SIGN (WSAA-INDEX1) TO  LIFA-GLSIGN.               */
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.effdate.set(wsaaEffdate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.termid.set(wsaaTermid);
		/* Set up LIFA-SUBSTITUTE-CODE No1 - contract type.                */
		lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
	}

protected void writeAcmv6050()
	{
		/*PARA*/
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(chdrmjaIO.getChdrcoy(), lifacmvrec.rldgcoy)
		|| isNE(chdrmjaIO.getChdrnum(), lifacmvrec.rdocnum)
		|| isNE(chdrmjaIO.getTranno(), lifacmvrec.tranno)) {
			return ;
		}
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void readUtrn7000()
	{
		wsaaCrtable.set(SPACES);
		//ILIFE-8786 start
		wsaaLowestPlnsfx.set(9999);
		List<Utrnpf> utrnList = utrnpfDAO.searchUtrnRecord(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
		if(!(utrnList.isEmpty())) {
			for(Utrnpf utrn: utrnList) {
				if (isNE(chdrmjaIO.getTranno(), utrn.getTranno())) {
					continue;
				}
				wsaaCrtable.set(utrn.getCrtable());
				if (isEQ(utrn.getPlanSuffix(), ZERO)) {
					continue;
				}
			
				if (isLT(utrn.getPlanSuffix(), wsaaLowestPlnsfx)) {
					wsaaLowestPlnsfx.set(utrn.getPlanSuffix());
		 //ILIFE-8786 end
				}
			}
		}
	}

protected void readHitr7100()
	{
 //ILIFE-8786 start
		if (isNE(wsaaCrtable, SPACES)) {
			return;
		}
		wsaaLowestPlnsfx.set(9999);
		List<Hitrpf> hitrList = hitrpfDAO.searchHitrRecord(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
		if(!(hitrList.isEmpty())) {
			for(Hitrpf hitrpf:hitrList) {
				if (isNE(chdrmjaIO.getTranno(), hitrpf.getTranno())) {
					continue ;
				}
				wsaaCrtable.set(hitrpf.getCrtable());
				if (isEQ(hitrpf.getPlanSuffix(), ZERO)) {
					continue;
				}
				if (isLT(hitrpf.getPlanSuffix(), wsaaLowestPlnsfx)) {
					wsaaLowestPlnsfx.set(hitrpf.getPlanSuffix());
				}
			}
		}
 //ILIFE-8786 end
	}
protected void readT56878000()
	{
		t56878010();
	}

protected void t56878010()
	{
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItmfrm(wsaaEffdate);
		wsaaT5687Item.set(SPACES);
		wsaaT5687Table.set(wsaaCrtable);
		itdmIO.setItemitem(wsaaT5687Item);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), wsaaT5687Item)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT5687Item);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		/*  IF  ITDM-STATUZ              NOT = MRNF                      */
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		wsaaBasicCommMeth.set(t5687rec.basicCommMeth);
		wsaaBastcmth.set(t5687rec.bastcmth);
		wsaaBastcpy.set(t5687rec.bastcpy);
	}

protected void readTr6958050()
	{
		read8051();
	}

protected void read8051()
	{
 	//ILIFE-8786 start
		covrpf = new Covrpf();
		covrpf = covrpfDAO.getcovrincrRecord(pcdtmja.getChdrcoy(),pcdtmja.getChdrnum(),pcdtmja.getLife(),
				pcdtmja.getCoverage(),"00",pcdtmja.getPlnsfx());
		wsaaTr695Coverage.set(covrpf.getCrtable());
 	//ILIFE-8786 end
		wsaaTr695Rider.set(wsaaCrtable);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.tr695);
		itdmIO.setItemitem(wsaaTr695Key);
		itdmIO.setItmfrm(wsaaEffdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr695)
		|| isNE(itdmIO.getItemitem(), wsaaTr695Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaTr695Rider.set("****");
			itdmIO.setDataArea(SPACES);
			itdmIO.setItemtabl(tablesInner.tr695);
			itdmIO.setItemitem(wsaaTr695Key);
			itdmIO.setItemcoy(atmodrec.company);
			itdmIO.setItmfrm(wsaaEffdate);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				xxxxFatalError();
			}
			if (isNE(itdmIO.getItemcoy(), atmodrec.company)
			|| isNE(itdmIO.getItemtabl(), tablesInner.tr695)
			|| isNE(itdmIO.getItemitem(), wsaaTr695Key)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				return ;
			}
		}
		tr695rec.tr695Rec.set(itdmIO.getGenarea());
		wsaaBasicCommMeth.set(tr695rec.basicCommMeth);
		wsaaBastcmth.set(tr695rec.bastcmth);
		wsaaBastcpy.set(tr695rec.bastcpy);
	}

protected void readT56458100()
	{
		t56458110();
	}

protected void t56458110()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		wsaaT5645Item.set(SPACES);
		wsaaT5645Prog.set("P5147");
		itemIO.setItemitem(wsaaT5645Item);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
		/* MOVE 'IT'                   TO DESC-DESCPFX.                 */
		/* MOVE CHDRMJA-CHDRCOY        TO DESC-DESCCOY.                 */
		/* MOVE T5645                  TO DESC-DESCTABL.                */
		/* MOVE ATMD-LANGUAGE          TO DESC-LANGUAGE.                */
		/* MOVE 'P5147'                TO DESC-DESCITEM                 */
		/* MOVE READR                  TO DESC-FUNCTION.                */
		/* CALL 'DESCIO' USING DESC-PARAMS.                             */
		/* IF DESC-STATUZ              NOT = O-K                        */
		/*     MOVE DESC-PARAMS        TO SYSR-PARAMS                   */
		/*     PERFORM XXXX-FATAL-ERROR.                                */
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			xxxxFatalError();
		}
	}

protected void calculateNetPremium8200()
	{
		para8200();
	}

protected void para8200()
	{
		if (isEQ(wsaaSbmaction, "A")) {
			premiumrec.sumin.set(ZERO);
		}
		else {
			calculateSa8400();
		}
		prasrec.taxrelamt.set(ZERO);
		/* Read the coverage file to get the payer sequence number.        */
		/* Coverage now updated to reflect addition of Single              */
		/* Premium Top-up on this component.                               */
		/* End current Coverage & write new Coverage details using the     */
		/* Premium Top-up effective date.                                  */
 		//ILIFE-8786 start
		covrpf.setValidflag("2");
		covrpf.setCurrto(wsaaEffdate.toInt());
		covrpfDAO.updateCovrValidAndCurrtoFlag(covrpf);
		covrpf.setCurrfrom(wsaaEffdate.toInt());
		covrpf.setCurrto(varcom.vrcmMaxDate.toInt());
		covrpf.setTranno(chdrmjaIO.getTranno().toInt());
		covrpf.setValidflag("1");
		covrpf.setTransactionDate(wsaaTransactionDate.toInt());
		covrpf.setTransactionTime(wsaaTransactionTime.toInt());
		/* ADD   PCDTMJA-INSTPREM      TO COVRMJA-SINGP.        <LA5076>*/
		if (isNE(wsaaSbmaction, "A")) {
			setPrecision(covrpf.getSingp(), 2);
			covrpf.setSingp(add(covrpf.getSingp(), pcdtmja.getInstprem()).getbigdata());
		}
		setPrecision(covrpf.getSumins(), 2);
		covrpf.setSumins(add(covrpf.getSumins(), premiumrec.sumin).getbigdata());
		List<Covrpf> data = new ArrayList<>();
		data.add(covrpf);
		covrpfDAO.insertCovrpfList(data);
		/*  Read the PAYR file                                             */
		payrpf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		payrpf.setChdrnum(chdrmjaIO.getChdrnum().toString());
		payrpf.setPayrseqno(covrpf.getPayrseqno());
		payrpf.setValidflag("1");	
		List<Payrpf> payrpfList = payrpfDAO.readPayrData(payrpf);
		if (payrpfList == null || payrpfList.isEmpty()) {
			syserrrec.params.set(chdrmjaIO.getChdrnum().concat(covrpf.getPayrseqno()));
			xxxxFatalError();
		}
		
		/*  Read the client role file to get the payer number.             */
		clrfpf = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrmjaIO.getChdrpfx().toString(), chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1)), "PY");
		if (clrfpf == null) {
			syserrrec.params.set(chdrmjaIO.getChdrcoy().concat(chdrmjaIO.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1))));
			xxxxFatalError();
		}
		//ILIFE-8786 end
		/* Look up the tax relief method on T5688.                         */
		readTableT56885200();
		/* MOVE SPACES                 TO ITDM-DATA-KEY.           <015>*/
		/* MOVE ATMD-COMPANY           TO ITDM-ITEMCOY.            <015>*/
		/* MOVE T5688                  TO ITDM-ITEMTABL.           <015>*/
		/* MOVE CHDRMJA-CNTTYPE        TO ITDM-ITEMITEM.           <015>*/
		/* MOVE CHDRMJA-OCCDATE        TO ITDM-ITMFRM.             <015>*/
		/* MOVE BEGN                   TO ITDM-FUNCTION.           <015>*/
		/* CALL 'ITDMIO' USING ITDM-PARAMS.                        <015>*/
		/* IF ITDM-ITEMCOY             NOT = ATMD-COMPANY          <015>*/
		/*  OR ITDM-ITEMTABL           NOT = T5688                 <015>*/
		/*  OR ITDM-ITEMITEM           NOT = CHDRMJA-CNTTYPE       <015>*/
		/*  OR ITDM-STATUZ             NOT = O-K                   <015>*/
		/*    MOVE ITDM-PARAMS         TO SYSR-PARAMS              <015>*/
		/*    PERFORM XXXX-FATAL-ERROR                             <015>*/
		/* ELSE                                                    <015>*/
		/*    MOVE ITDM-GENAREA        TO T5688-T5688-REC.         <015>*/
		/* Look up the tax relief subroutine on T6687.                     */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t6687);
		itemIO.setItemitem(t5688rec.taxrelmth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t6687rec.t6687Rec.set(itemIO.getGenarea());
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		/* If the tax relief method is not spaces calculate the tax        */
		/* relief amount and deduct it from the premium.                   */
		if (isNE(t5688rec.taxrelmth, SPACES)) {
			prasrec.clntnum.set(clrfpf.getClntnum()); //ILIFE-8786
			prasrec.clntcoy.set(clrfpf.getClntcoy()); //ILIFE-8786
			prasrec.incomeSeqNo.set(payrpf.getIncomeSeqNo()); //ILIFE-8786
			prasrec.cnttype.set(chdrmjaIO.getCnttype());
			prasrec.effdate.set(wsaaEffdate);
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			prasrec.company.set(chdrmjaIO.getChdrcoy());
			prasrec.grossprem.set(pcdtmja.getInstprem()); //ILIFE-8786
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				xxxxFatalError();
			}
		}
		
		zrdecplcPojo.setAmountIn(prasrec.taxrelamt.getbigdata());  //ILIFE-8786
		zrdecplcPojo.setCurrency(chdrmjaIO.getCntcurr().toString()); //ILIFE-8786
		d000CallRounding();
		prasrec.taxrelamt.set(zrdecplcPojo.getAmountOut()); //ILIFE-8786
		compute(wsaaNetPremium, 2).set(sub(pcdtmja.getInstprem(), prasrec.taxrelamt)); //ILIFE-8786
	}

	/**
	* <pre>
	* CALCULATE TOLERANCE IF INSUFFICIENT SUSPENSE             *
	* </pre>
	*/
protected void calculateTolerance8300()
	{
		read8310();
	}

	/**
	* <pre>
	* Read T5645 & ACBL file for Suspense details.
	* </pre>
	*/
protected void read8310()
	{
		readT56458100();
 		//ILIFE-8786 start
		Acblpf acblpf = acblpfDAO.getAcblpfRecord(atmodrec.company.toString(), t5645rec.sacscode03.toString(), wsaaChdrnum.toString(),t5645rec.sacstype03.toString(),wsaaPremCcy.toString());
				
		compute(wsaaTotalSuspense, 2).set(mult(acblpf.getSacscurbal(), -1));
		 //ILIFE-8786 end
		/* Convert the Single Premium if it has not been made in           */
		/* the Contract currency.                                          */
		wsaaOrigNetPrem.set(wsaaNetPremium);
		wsaaNetPremium.add(wsaaTotPremTax);
		if (isNE(wsaaPremCcy, chdrmjaIO.getCntcurr())) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.rateUsed.set(ZERO);
			conlinkrec.cashdate.set(wsaaEffdate);
			conlinkrec.currIn.set(chdrmjaIO.getCntcurr());
			conlinkrec.currOut.set(wsaaPremCcy);
			conlinkrec.amountIn.set(wsaaNetPremium);
			conlinkrec.company.set(atmodrec.company);
			conlinkrec.function.set("SURR");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(conlinkrec.clnk002Rec);
				syserrrec.statuz.set(conlinkrec.statuz);
				xxxxFatalError();
			}
			if (isNE(conlinkrec.amountOut, 0)) {
				zrdecplcPojo.setCurrency(conlinkrec.currOut.toString()); //ILIFE-8786
				zrdecplcPojo.setAmountIn(conlinkrec.amountOut.getbigdata()); //ILIFE-8786
				zrdecplcPojo.setCurrency(wsaaPremCcy.toString()); //ILIFE-8786
				d000CallRounding();
				conlinkrec.amountOut.set(zrdecplcPojo.getAmountOut()); //ILIFE-8786
			}
			wsaaNetPremium.set(conlinkrec.amountOut);
		}
		/* Calculate the Tolerance if insufficient Suspense.               */
		wsaaTolerance.set(ZERO);
		if (isGT(wsaaNetPremium, wsaaTotalSuspense)) {
			compute(wsaaTolerance, 2).set(sub(wsaaNetPremium, wsaaTotalSuspense));
		}
	}

protected void calculateSa8400()
	{
		calSuminAssured8400();
	}

protected void calSuminAssured8400()
	{
		/*  Obtain the general Coverage/Rider details (including Premium   */
		/*  Calculation Method details from T5687 using the coverage/      */
		/*  rider code as part of the key (COVRMJA-CRTABLE).               */
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrpf.getCrtable()); //ILIFE-8786
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrpf.getCrtable()) //ILIFE-8786
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
			itdmIO.setItemtabl(tablesInner.t5687);
			itdmIO.setItemitem(covrpf.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e366);
			xxxxFatalError();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItempfx("IT");
		if (isEQ(covrpf.getJlife(), SPACES)) {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
		}
		/*    IF ITEM-ITEMITEM             = SPACES                        */
		/*       IF WSKY-BATC-BATCTRCDE      = 'TA69'                      */
		/*          MOVE WSAA-TOPUP-PREM-METH TO ITEM-ITEMITEM             */
		/*       ELSE                                                      */
		/*          GO TO 8400-EXIT                                        */
		/*       END-IF                                                    */
		/*    END-IF.                                                      */
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		/*ILIFE-3332 AT jobs Bomb_Single premium with Top Up_SA Start*/
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			//premiumrec.premMethod.set(itemIO.getItemitem());
			wsaaPremMeth.set(itemIO.getItemitem());
		}
		/*ILIFE-3332 end*/
		/* ILIFE-3142 End*/
		t5675rec.t5675Rec.set(itemIO.getGenarea());
		premiumrec.premiumRec.set(SPACES);
		premiumrec.function.set("TOPU");
		premiumrec.crtable.set(covrpf.getCrtable()); //ILIFE-8786
		premiumrec.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(chdrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrpf.getLife()); //ILIFE-8786
		premiumrec.lifeJlife.set(covrpf.getJlife()); //ILIFE-8786
		premiumrec.covrCoverage.set(covrpf.getCoverage()); //ILIFE-8786
		premiumrec.covrRider.set(covrpf.getRider()); //ILIFE-8786
		premiumrec.plnsfx.set(covrpf.getPlanSuffix()); //ILIFE-8786
		premiumrec.lage.set(covrpf.getAnbAtCcd()); //ILIFE-8786
		premiumrec.lsex.set(covrpf.getSex()); //ILIFE-8786
		premiumrec.jlage.set(covrpf.getAnbAtCcd()); //ILIFE-8786
		premiumrec.jlsex.set(covrpf.getSex()); //ILIFE-8786
		premiumrec.effectdt.set(chdrmjaIO.getOccdate());
		premiumrec.termdate.set(covrpf.getPremCessDate()); //ILIFE-8786
		/* COMPUTE CPRM-DURATION       = DTC3-FREQ-FACTOR + .99999.     */
		premiumrec.duration.set(ZERO);
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.currcode.set(chdrmjaIO.getCntcurr());
		premiumrec.sumin.set(ZERO);
		premiumrec.mortcls.set(covrpf.getMortcls()); //ILIFE-8786
		premiumrec.billfreq.set(chdrmjaIO.getBillfreq());
		premiumrec.mop.set(chdrmjaIO.getBillchnl());
		premiumrec.ratingdate.set(chdrmjaIO.getOccdate());
		premiumrec.reRateDate.set(chdrmjaIO.getOccdate());
		premiumrec.calcPrem.set(pcdtmja.getInstprem()); //ILIFE-8786
		premiumrec.language.set(atmodrec.language);
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.inputPrevPrem.set(ZERO);//ILIFE-8164
		//****Ticket #VE-155/263 TRM Calculations - Premium calculation start
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()) && er.isExternalized(premiumrec.cnttype.toString(), premiumrec.crtable.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			/*ILIFE-3332 AT jobs Bomb_Single premium with Top Up_SA start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			premiumrec.premMethod.set(wsaaPremMeth);
			premiumrec.calcBasPrem.set(pcdtmja.getInstprem()); //ILIFE-8786
			/*ILIFE-3332 end*/

			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/	
		//****Ticket #VE-155/263 TRM Calculations - Premium calculation end
		if (isNE(premiumrec.statuz, "****")) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			xxxxFatalError();
		}
	}

protected void callLifrtrnLifacmv8500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					loop8520();
				case next8570:
					next8570();
				case exit8590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loop8520()
	{
		/* ADD   +1                    TO WSAA-INDEX1.                  */
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.substituteCode[1].set(SPACES);
		wsaaBatckey.set(atmodrec.batchKey);
		lifrtrnrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifrtrnrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifrtrnrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifrtrnrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifrtrnrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifrtrnrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		readT56458100();
		lifrtrnrec.rdocnum.set(chdrmjaIO.getChdrnum());
		lifrtrnrec.tranno.set(chdrmjaIO.getTranno());
		wsaaJrnseq.add(1);
		lifrtrnrec.jrnseq.set(wsaaJrnseq);
		/*    MOVE ZEROES                 TO  LIFR-JRNSEQ.*/
		lifrtrnrec.rldgcoy.set(wsaaBatckey.batcBatccoy);
		/* MOVE T5645-SACSCODE (WSAA-INDEX1)   TO  LIFR-SACSCODE.       */
		lifrtrnrec.rldgacct.set(chdrmjaIO.getChdrnum());
		/* MOVE CHDRMJA-CNTCURR        TO  LIFR-ORIGCURR.               */
		lifrtrnrec.origcurr.set(wsaaPremCcy);
		/* MOVE T5645-SACSTYPE (WSAA-INDEX1)  TO  LIFR-SACSTYP.         */
		/* MOVE PCDTMJA-INSTPREM       TO  LIFR-ORIGAMT.                */
		lifrtrnrec.origamt.set(wsaaNetPremium);
		lifrtrnrec.tranref.set(chdrmjaIO.getTranno());
		lifrtrnrec.trandesc.set(descIO.getLongdesc());
		lifrtrnrec.crate.set(ZERO);
		compute(lifrtrnrec.acctamt, 9).set(mult(lifrtrnrec.origamt, lifrtrnrec.crate));
		lifrtrnrec.genlcoy.set(atmodrec.company);
		/*    MOVE CHDRMJA-CNTCURR        TO  LIFR-GENLCUR.                */
		lifrtrnrec.genlcur.set(SPACES);
		/* MOVE T5645-GLMAP (WSAA-INDEX1)  TO  LIFR-GLCODE.             */
		/* MOVE T5645-SIGN (WSAA-INDEX1) TO  LIFR-GLSIGN.               */
		/* Lets post with proper subscripts like the rest of the program   */
		/* rather than a variable which is only actively used in this      */
		/* section.                                                        */
		lifrtrnrec.sacscode.set(t5645rec.sacscode[3]);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype[3]);
		lifrtrnrec.glcode.set(t5645rec.glmap[3]);
		lifrtrnrec.glsign.set(t5645rec.sign[3]);
		lifrtrnrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifrtrnrec.contot.set(ZERO);
		lifrtrnrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifrtrnrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifrtrnrec.effdate.set(wsaaEffdate);
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.frcdate.set(99999999);
		lifrtrnrec.transactionDate.set(wsaaTransactionDate);
		lifrtrnrec.transactionTime.set(wsaaTransactionTime);
		lifrtrnrec.user.set(wsaaUser);
		lifrtrnrec.termid.set(wsaaTermid);
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(chdrmjaIO.getChdrcoy(), lifrtrnrec.rldgcoy)
		|| isNE(chdrmjaIO.getChdrnum(), lifrtrnrec.rdocnum)
		|| isNE(chdrmjaIO.getTranno(), lifrtrnrec.tranno)) {
			goTo(GotoLabel.exit8590);
		}
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			xxxxFatalError();
		}
		/*    ADD   +1                    TO WSAA-INDEX1.                  */
		lifacmvrec.function.set("PSTW");
		wsaaBatckey.set(atmodrec.batchKey);
		lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		readT56458100();
		lifacmvrec.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec.tranno.set(chdrmjaIO.getTranno());
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		/*   MOVE ZEROES                 TO  LIFA-JRNSEQ.*/
		lifacmvrec.rldgcoy.set(wsaaBatckey.batcBatccoy);
		/* MOVE T5645-SACSCODE (WSAA-INDEX1)   TO  LIFA-SACSCODE.       */
		/* MOVE CHDRMJA-CHDRNUM        TO  LIFA-RLDGACCT.               */
		/* Check for Component level accounting & act accordingly          */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			/*        Set up new RLDGACCT field for Component Level Accounting */
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(pcdtmja.getPlnsfx()); //ILIFE-8786
			wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum());
			wsaaRldgLife.set(pcdtmja.getLife()); //ILIFE-8786
			wsaaRldgCoverage.set(pcdtmja.getCoverage()); //ILIFE-8786
			wsaaRldgRider.set(pcdtmja.getRider()); //ILIFE-8786
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable()); //ILIFE-8786
			lifacmvrec.sacscode.set(t5645rec.sacscode[9]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[9]);
			lifacmvrec.glcode.set(t5645rec.glmap[9]);
			lifacmvrec.glsign.set(t5645rec.sign[9]);
		}
		else {
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode[4]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[4]);
			lifacmvrec.glcode.set(t5645rec.glmap[4]);
			lifacmvrec.glsign.set(t5645rec.sign[4]);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		/* MOVE T5645-SACSTYPE (WSAA-INDEX1)  TO  LIFA-SACSTYP.         */
		lifacmvrec.origamt.set(pcdtmja.getInstprem()); //ILIFE-8786
		lifacmvrec.tranref.set(chdrmjaIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(ZERO);
		compute(lifacmvrec.acctamt, 9).set(mult(lifacmvrec.origamt, lifacmvrec.crate));
		lifacmvrec.genlcoy.set(atmodrec.company);
		/*    MOVE CHDRMJA-CNTCURR        TO  LIFA-GENLCUR.                */
		lifacmvrec.genlcur.set(SPACES);
		/* MOVE T5645-GLMAP (WSAA-INDEX1)  TO  LIFA-GLCODE.             */
		/* MOVE T5645-SIGN (WSAA-INDEX1) TO  LIFA-GLSIGN.               */
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.effdate.set(wsaaEffdate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.termid.set(wsaaTermid);
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			goTo(GotoLabel.next8570);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), lifacmvrec.rldgcoy)
		|| isNE(chdrmjaIO.getChdrnum(), lifacmvrec.rdocnum)
		|| isNE(chdrmjaIO.getTranno(), lifacmvrec.tranno)) {
			goTo(GotoLabel.exit8590);
		}
	}

protected void next8570()
	{
		/* Post the tax relif amount                                       */
		lifacmvrec.rldgacct.set(SPACES);
		/*  MOVE PRAS-INREVNUM         TO LIFA-RLDGACCT.         <A06541>*/
		lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
		lifacmvrec.origamt.set(prasrec.taxrelamt);
		lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		if (isNE(lifacmvrec.origamt, 0)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode[7]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[7]);
			lifacmvrec.glcode.set(t5645rec.glmap[7]);
			lifacmvrec.glsign.set(t5645rec.sign[7]);
			lifacmvrec.contot.set(t5645rec.cnttot[7]);
			lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			/*    MOVE T5645-SACSCODE (07) TO LIFA-SACSCODE            <015>*/
			/*    MOVE T5645-SACSTYPE (07) TO LIFA-SACSTYP             <015>*/
			/*    MOVE T5645-GLMAP (07)    TO LIFA-GLCODE              <015>*/
			/*    MOVE T5645-SIGN (07)     TO LIFA-GLSIGN              <015>*/
			/*    MOVE T5645-CNTTOT (07)   TO LIFA-CONTOT              <015>*/
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				xxxxFatalError();
			}
		}
		/* IF LIFA-STATUZ              NOT = O-K                   <015>*/
		/*    MOVE LIFA-LIFACMV-REC    TO SYSR-PARAMS              <015>*/
		/*    PERFORM XXXX-FATAL-ERROR.                            <015>*/
		/* Reset RLDGACCT prior to postings to Contract level.             */
		lifacmvrec.rldgacct.set(chdrmjaIO.getChdrnum());
		/* Post Tolerance in Single Premium currency if there are          */
		/* insufficient funds in Suspense.                                 */
		if (isNE(wsaaTolerance, ZERO)) {
			lifacmvrec.origamt.set(wsaaTolerance);
			lifacmvrec.origcurr.set(wsaaPremCcy);
			lifacmvrec.sacscode.set(t5645rec.sacscode11);
			lifacmvrec.sacstyp.set(t5645rec.sacstype11);
			lifacmvrec.glcode.set(t5645rec.glmap11);
			lifacmvrec.glsign.set(t5645rec.sign11);
			lifacmvrec.contot.set(t5645rec.cnttot11);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				xxxxFatalError();
			}
		}
		/* If the Premium currency differs from the Contract currency      */
		/* post the Premium in both the premium & contract currencies.     */
		if (isNE(chdrmjaIO.getCntcurr(), wsaaPremCcy)) {
			lifacmvrec.origcurr.set(wsaaPremCcy);
			lifacmvrec.origamt.set(wsaaNetPremium);
			lifacmvrec.contot.set(t5645rec.cnttot[12]);
			lifacmvrec.glcode.set(t5645rec.glmap[12]);
			lifacmvrec.sacscode.set(t5645rec.sacscode[12]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[12]);
			lifacmvrec.glsign.set(t5645rec.sign[12]);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				xxxxFatalError();
			}
			lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
			compute(lifacmvrec.origamt, 2).set(sub(pcdtmja.getInstprem(), prasrec.taxrelamt)); //ILIFE-8786
			lifacmvrec.contot.set(t5645rec.cnttot[13]);
			lifacmvrec.glcode.set(t5645rec.glmap[13]);
			lifacmvrec.sacscode.set(t5645rec.sacscode[13]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[13]);
			lifacmvrec.glsign.set(t5645rec.sign[13]);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				xxxxFatalError();
			}
		}
	}

//ILIFE-3997-STARTS
protected void writeNLGRec() {
	nlgcalcrec.chdrcoy.set (ptrnpf.getChdrcoy()); //ILIFE-8786
	nlgcalcrec.chdrnum.set(ptrnpf.getChdrnum()); //ILIFE-8786
	nlgcalcrec.tranno.set(ptrnpf.getTranno()); //ILIFE-8786
	nlgcalcrec.effdate.set(datcon1rec.intDate);
	nlgcalcrec.occdate.set(ptrnpf.getPtrneff()); //ILIFE-8786
	nlgcalcrec.frmdate.set(chdrmjaIO.getOccdate());
	nlgcalcrec.todate.set(varcom.vrcmMaxDate);
	nlgcalcrec.batcactyr.set(ptrnpf.getBatcactyr()); //ILIFE-8786
	nlgcalcrec.batcactmn.set(ptrnpf.getBatcactmn()); //ILIFE-8786
	nlgcalcrec.batctrcde.set(ptrnpf.getBatctrcde()); //ILIFE-8786
	nlgcalcrec.inputAmt.set(pcdtmja.getInstprem());
	nlgcalcrec.language.set(atmodrec.language);
	nlgcalcrec.cnttype.set(chdrmjaIO.getCnttype());
	nlgcalcrec.fsuco.set(wsaaFsuco);
	nlgcalcrec.function.set("TOPUP");
	callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec) ;
	if (isNE(nlgcalcrec.status, varcom.oK)) {
		syserrrec.statuz.set(batcuprec.statuz);
		xxxxFatalError();
	}
}
//ILIFE-3997-ENDS
protected void dryProcessing8600()
	{
		start8610();
	}

protected void start8610()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088700();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088700();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		/* MOVE TDAY                   TO DTC1-FUNCTION.        <LA5184>*/
		/* CALL 'DATCON1'              USING DTC1-DATCON1-REC.  <LA5184>*/
		/* MOVE DTC1-INT-DATE          TO WSAA-TODAY.           <LA5184>*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		/* MOVE WSAA-TODAY             TO DRYP-RUN-DATE.        <LA5184>*/
		drypDryprcRecInner.drypRunDate.set(wsaaTday);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		/* MOVE WSAA-TODAY             TO DRYP-EFFECTIVE-DATE.  <LA5184>*/
		drypDryprcRecInner.drypEffectiveDate.set(wsaaTday);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrmjaIO.getStatementDate());
		//ILPI-145
//		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75088700()
	{
		start8710();
	}

protected void start8710()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock9000()
	{
		releaseSoftlock9010();
	}

protected void releaseSoftlock9010()
	{
 		//ILIFE-8786 start
		sftlockRecBean.setFunction("UNLK");
		sftlockRecBean.setCompany(atmodrec.company.toString());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(wsaaChdrnum.toString());
		sftlockRecBean.setTransaction(atmodrec.batchKey.toString());
		sftlockRecBean.setStatuz(SPACES.stringValue());
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				xxxxFatalError();
 		//ILIFE-8786 end
		}
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.minorChg.set("Y");
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void b000BenefitBilling()
	{
		b010BenefitBilling();
	}

protected void b010BenefitBilling()
	{
		if (isEQ(t5687rec.bbmeth, SPACES)) {
			return ;
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5534);
		itemIO.setItemitem(t5687rec.bbmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5534rec.t5534Rec.set(itemIO.getGenarea());
		if (isEQ(t5534rec.subprog, SPACES)) {
			return ;
		}
		ubblallpar.ubblallRec.set(SPACES);
		ubblallpar.chdrChdrcoy.set(chdrmjaIO.getChdrcoy());
		ubblallpar.chdrChdrnum.set(chdrmjaIO.getChdrnum());
		ubblallpar.lifeLife.set(covrpf.getLife()); //ILIFE-8786
		ubblallpar.lifeJlife.set(covrpf.getJlife()); //ILIFE-8786
		ubblallpar.covrCoverage.set(covrpf.getCoverage()); //ILIFE-8786
		ubblallpar.covrRider.set(covrpf.getRider()); //ILIFE-8786
		ubblallpar.planSuffix.set(covrpf.getPlanSuffix()); //ILIFE-8786
		ubblallpar.billfreq.set(t5534rec.unitFreq);
		ubblallpar.cntcurr.set(chdrmjaIO.getCntcurr());
		ubblallpar.cnttype.set(chdrmjaIO.getCnttype());
		ubblallpar.tranno.set(chdrmjaIO.getTranno());
		ubblallpar.effdate.set(wsaaEffdate);
		ubblallpar.premMeth.set(t5534rec.premmeth);
		ubblallpar.jlifePremMeth.set(t5534rec.jlPremMeth);
		ubblallpar.sumins.set(covrpf.getSumins()); //ILIFE-8786
		/* MOVE WSAA-NET-PREMIUM       TO UBBL-SINGP.                   */
		ubblallpar.singp.set(wsaaOrigNetPrem);
		ubblallpar.premCessDate.set(covrpf.getPremCessDate()); //ILIFE-8786
		ubblallpar.crtable.set(covrpf.getCrtable()); //ILIFE-8786
		ubblallpar.billchnl.set(chdrmjaIO.getBillchnl());
		ubblallpar.mortcls.set(covrpf.getMortcls()); //ILIFE-8786
		ubblallpar.svMethod.set(t5534rec.svMethod);
		ubblallpar.language.set(atmodrec.language);
		ubblallpar.user.set(wsaaUser);
		ubblallpar.batccoy.set(wsaaBatckey.batcBatccoy);
		ubblallpar.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ubblallpar.batcactyr.set(wsaaBatckey.batcBatcactyr);
		ubblallpar.batcactmn.set(wsaaBatckey.batcBatcactmn);
		ubblallpar.batctrcde.set(wsaaBatckey.batcBatctrcde);
		ubblallpar.batch.set(wsaaBatckey.batcBatcbatch);
		ubblallpar.tranno.set(chdrmjaIO.getTranno());
		ubblallpar.adfeemth.set(t5534rec.adfeemth);
		ubblallpar.function.set("TOPUP");
		ubblallpar.polsum.set(chdrmjaIO.getPolsum());
		ubblallpar.ptdate.set(chdrmjaIO.getPtdate());
		ubblallpar.polinc.set(chdrmjaIO.getPolinc());
		ubblallpar.occdate.set(chdrmjaIO.getOccdate());
		ubblallpar.chdrRegister.set(chdrmjaIO.getRegister());
		/* Get the next benefit billing date.                              */
		/*    MOVE SPACE                  TO DTC2-DATCON2-REC.     <LA4351>*/
		/*    MOVE T5534-UNIT-FREQ        TO DTC2-FREQUENCY.       <LA4351>*/
		/*    MOVE 1                      TO DTC2-FREQ-FACTOR.     <LA4351>*/
		/*    MOVE COVRMJA-CRRCD          TO DTC2-INT-DATE-1.      <LA4351>*/
		/*    MOVE 0                      TO DTC2-INT-DATE-2.      <LA4351>*/
		/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC.  <LA4351>*/
		/*    IF DTC2-STATUZ          NOT = O-K                    <LA4351>*/
		/*       MOVE DTC2-DATCON2-REC    TO SYSR-PARAMS           <LA4351>*/
		/*       MOVE DTC2-STATUZ         TO SYSR-STATUZ           <LA4351>*/
		/*       PERFORM XXXX-FATAL-ERROR                          <LA4351>*/
		/*    END-IF.                                              <LA4351>*/
		/*    MOVE DTC2-INT-DATE-2        TO COVRMJA-BEN-BILL-DATE.<LA4351>*/
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.frequency.set(t5534rec.unitFreq);
		datcon4rec.freqFactor.set(1);
		datcon4rec.intDate1.set(covrpf.getCrrcd()); //ILIFE-8786
		wsaaOccdate.set(covrpf.getCrrcd()); //ILIFE-8786
		datcon4rec.billdayNum.set(wsaaOccDd);
		datcon4rec.billmonthNum.set(wsaaOccMm);
		datcon4rec.intDate2.set(0);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		covrpf.setBenBillDate(datcon4rec.intDate2.toInt()); //ILIFE-8786
		/*IVE-795 RUL Product - Benefit Billing Calculation started*/
		//callProgram(t5534rec.subprog, ubblallpar.ubblallRec);
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5534rec.subprog.toString()) && er.isExternalized(ubblallpar.cnttype.toString(), ubblallpar.crtable.toString()))) 
		{
			callProgramX(t5534rec.subprog, ubblallpar.ubblallRec); //ILIFE-8320
		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxubblrec vpxubblrec = new Vpxubblrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);		
			//ubblallpar.sumins.format(ubblallpar.sumins.toString());
			vpxubblrec.function.set("INIT");
			callProgram(Vpxubbl.class, vpmcalcrec.vpmcalcRec,vpxubblrec);	
			ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
			vpmfmtrec.initialize();
			vpmfmtrec.date01.set(ubblallpar.effdate);
			callProgram(t5534rec.subprog, vpmfmtrec,ubblallpar.ubblallRec,vpxubblrec);
			callProgram(Vpuubbl.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
			if(isEQ(ubblallpar.statuz,SPACES))
				ubblallpar.statuz.set(Varcom.oK);
		}
		
		/*IVE-795 RUL Product - Benefit Billing Calculation end*/
		if (isNE(ubblallpar.statuz, varcom.oK)) {
			syserrrec.params.set(ubblallpar.ubblallRec);
			syserrrec.statuz.set(ubblallpar.statuz);
			xxxxFatalError();
		}
	}

protected void b100CallZorcompy()
	{
		b110Start();
	}

protected void b110Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(chdrmjaIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(chdrmjaIO.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			xxxxFatalError();
		}
	}

protected void b200GetPremTax()
	{		
		wsaaTotPremTax.set(ZERO);	
		
		taxdbilpf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		taxdbilpf.setChdrnum(chdrmjaIO.getChdrnum().toString());
		taxdbilpf.setLife(covrpf.getLife()); //ILIFE-8786
		taxdbilpf.setCoverage(covrpf.getCoverage()); //ILIFE-8786
		taxdbilpf.setRider(covrpf.getRider()); //ILIFE-8786
		taxdbilpf.setPlansfx(0);
		taxdbilpf.setTranno(chdrmjaIO.getTranno().toInt());
		taxdbilpf.setTrantype("PREM");
		taxdbilpf.setInstfrom(wsaaEffdate.toInt());
		taxdbilpfList = taxdbilpfDAO.searchTaxdpfRecord(taxdbilpf);
		if(!taxdbilpfList.isEmpty())
			for (Taxdpf taxdbilpf1 : taxdbilpfList) {
				/* Read TAXD records                                               */
				if (isEQ(taxdbilpf1.getPostflg(), SPACES)) {			
							
					if (isNE(taxdbilpf1.getTxabsind01(), "Y")) {
						wsaaTotPremTax.add(taxdbilpf1.getTaxamt01().doubleValue());
					}
					if (isNE(taxdbilpf1.getTxabsind02(), "Y")) {
						wsaaTotPremTax.add(taxdbilpf1.getTaxamt02().doubleValue());
					}
				}
			}
	}

protected void b300PostTax()
	{
		/* Read table TR52D using CHDRMJA-REGISTER as key                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrmjaIO.getRegister());
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItemcoy(atmodrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setItempfx("IT");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		taxdbilpf = new Taxdpf();
		taxdbilpf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		taxdbilpf.setChdrnum(chdrmjaIO.getChdrnum().toString());
		taxdbilpf.setLife(covrpf.getLife()); //ILIFE-8786
		taxdbilpf.setCoverage(covrpf.getCoverage()); //ILIFE-8786
		taxdbilpf.setRider(covrpf.getRider()); //ILIFE-8786
		taxdbilpf.setPlansfx(0);
		taxdbilpf.setTranno(chdrmjaIO.getTranno().toInt());
		taxdbilpf.setInstfrom(wsaaEffdate.toInt());
		taxdbilpfList = taxdbilpfDAO.searchTaxdpfRecord(taxdbilpf);
		if(!taxdbilpfList.isEmpty()) {
			updateTaxdList = new ArrayList<>();
			for (Taxdpf taxdbilpf_loc : taxdbilpfList) {				
				taxdbilpf = new Taxdpf(taxdbilpf_loc);
				b500PostTaxdbil();                                             				
			}
			taxdbilpfDAO.updateTaxdpf(updateTaxdList);
		}
		}


protected void b500PostTaxdbil()
	{
				
		if (isEQ(taxdbilpf.getPostflg(), SPACES)) {			
			/* Call tax subroutine                                             */
			txcalcrec.function.set("POST");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
			txcalcrec.life.set(covrpf.getLife()); //ILIFE-8786
			txcalcrec.coverage.set(covrpf.getCoverage()); //ILIFE-8786
			txcalcrec.rider.set(covrpf.getRider()); //ILIFE-8786
			txcalcrec.planSuffix.set(covrpf.getPlanSuffix()); //ILIFE-8786
			txcalcrec.crtable.set(covrpf.getCrtable()); //ILIFE-8786
			txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
			txcalcrec.register.set(chdrmjaIO.getRegister());
			txcalcrec.taxrule.set(subString(taxdbilpf.getTranref(), 1, 8));
			txcalcrec.rateItem.set(subString(taxdbilpf.getTranref(), 9, 8));
			txcalcrec.effdate.set(taxdbilpf.getEffdate());
			txcalcrec.amountIn.set(taxdbilpf.getBaseamt());
			txcalcrec.taxType[1].set(taxdbilpf.getTxtype01());
			txcalcrec.taxType[2].set(taxdbilpf.getTxtype02());
			txcalcrec.taxAmt[1].set(taxdbilpf.getTaxamt01());
			txcalcrec.taxAmt[2].set(taxdbilpf.getTaxamt02());
			txcalcrec.taxAbsorb[1].set(taxdbilpf.getTxabsind01());
			txcalcrec.taxAbsorb[2].set(taxdbilpf.getTxabsind02());
			txcalcrec.tranno.set(chdrmjaIO.getTranno());
			txcalcrec.transType.set(taxdbilpf.getTrantype());
			txcalcrec.jrnseq.set(wsaaJrnseq);
			txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
			txcalcrec.batckey.set(wsaaBatckey);
			txcalcrec.language.set(atmodrec.language);
			txcalcrec.txcode.set(tr52drec.txcode);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				xxxxFatalError();
			}
			wsaaJrnseq.set(txcalcrec.jrnseq);
			if (isEQ(taxdbilpf.getTrantype(), "NINV")) {
				b600ProcessNonInvestTax();
			}
			/* Update TAXD record                                              */
			/*                                                         <LA4758>*/
			/*                                                         <LA4758>*/
			/*     IF TXCL-STATUZ             NOT = O-K                <LA4758>*/
			/*        MOVE TXCL-LINK-REC      TO SYSR-PARAMS           <LA4758>*/
			/*        MOVE TXCL-STATUZ        TO SYSR-STATUZ           <LA4758>*/
			/*        PERFORM XXXX-FATAL-ERROR                         <LA4758>*/
			/*     END-IF.                                             <LA4758>*/
			
			
			updateTaxdList.add(taxdbilpf);			
			
		}
	}

protected void b600ProcessNonInvestTax()
	{
		
		wsaaTotNonInvestTax.set(ZERO);
		if (isNE(taxdbilpf.getTxabsind01(), "Y")) {
			wsaaTotNonInvestTax.add(taxdbilpf.getTaxamt01().doubleValue());
		}
		if (isNE(taxdbilpf.getTxabsind02(), "Y")) {
			wsaaTotNonInvestTax.add(taxdbilpf.getTaxamt02().doubleValue());
		}
		if (isEQ(wsaaTotNonInvestTax, ZERO)) {
			return ;
		}
		/* Get total coverage debt from ZRST.                              */
 		//ILIFE-8786 start
		List<Zrstpf> zrstnudList = zrstpfDAO.searchZrstnudRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getLife(), covrpf.getCoverage(),covrpf.getRider(), SPACES.stringValue());
		if(!(zrstnudList.isEmpty())) {
			for(Zrstpf zrstnudpf : zrstnudList) {
				wsaaTotCd.add(zrstnudpf.getZramount01().doubleValue());
			}
		}

		wsaaTotCd.add(wsaaTotNonInvestTax);
		if (isNE(taxdbilpf.getTxabsind01(), "Y")
		&& isNE(taxdbilpf.getTaxamt01(), ZERO)) {
			Zrstpf zrstnud = new Zrstpf();
			zrstnud.setSacstyp(taxdbilpf.getTxtype01());
			zrstnud.setZramount01(taxdbilpf.getTaxamt01());
			zrstnud.setZramount02(wsaaTotCd.getbigdata());
			b800WriteZrstNinv(zrstnud);
		}
		if (isNE(taxdbilpf.getTxabsind02(), "Y")
		&& isNE(taxdbilpf.getTaxamt02(), ZERO)) {
			Zrstpf zrstnud = new Zrstpf();
			zrstnud.setSacstyp(taxdbilpf.getTxtype02());
			zrstnud.setZramount01(taxdbilpf.getTaxamt02());
			zrstnud.setZramount02(wsaaTotCd.getbigdata());
			b800WriteZrstNinv(zrstnud);
		}
		/* Update ZRST record                                              */
		if(!(zrstnudList.isEmpty())) {
			for(Zrstpf z : zrstnudList) {
				Zrstpf zrstpf = new Zrstpf(z);
				zrstpf.setUniqueNumber(z.getUniqueNumber());
				//after this a begin was there which reads the records based on unique number but list already has all the records and reads each record
				zrstpf.setZramount02(wsaaTotCd.getbigdata());
				zrstUpdateList.add(zrstpf);	
			}
			if (!zrstUpdateList.isEmpty()) 
				zrstpfDAO.updateZrststm(zrstUpdateList);
		}
		//ILIFE-8786 end
		/* Post coverage debt                                              */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(atmodrec.company);
		wsaaItemkey.itemItemtabl.set(tablesInner.t1688);
		wsaaItemkey.itemItemitem.set(wsaaBatckey.batcBatctrcde);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set("E");
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account    */
		/* movements.                                                      */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec.jrnseq.set(txcalcrec.jrnseq);
		lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.rldgcoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.genlcoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.origcurr.set(chdrmjaIO.getCntcurr());
		lifacmvrec.origamt.set(wsaaTotNonInvestTax);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.tranno.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.tranno.set(chdrmjaIO.getTranno());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.effdate.set(wsaaEffdate);
		/* Set up TRANREF to contain the Full component key of the         */
		/* Coverage/Rider that is Creating the debt so that if a           */
		/* reversal is required at a later date, the correct component     */
		/* can be identified.                                              */
		lifacmvrec.tranref.set(taxdbilpf.getTranref());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.trandesc.set(getdescrec.longdesc);
		/* Check for Component Level accounting and act accordingly        */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode15);
			lifacmvrec.sacstyp.set(t5645rec.sacstype15);
			lifacmvrec.glsign.set(t5645rec.sign15);
			lifacmvrec.glcode.set(t5645rec.glmap15);
			lifacmvrec.contot.set(t5645rec.cnttot15);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum());
			wsaaPlan.set(covrpf.getPlanSuffix()); //ILIFE-8786
			wsaaRldgLife.set(covrpf.getLife()); //ILIFE-8786
			wsaaRldgCoverage.set(covrpf.getCoverage()); //ILIFE-8786
			/* Post ACMV against the Coverage, not Rider since the debt is also*/
			/* set against Coverage.                                           */
			wsaaRldgRider.set("00");
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			/* Set correct substitution code                                   */
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable()); //ILIFE-8786
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode14);
			lifacmvrec.sacstyp.set(t5645rec.sacstype14);
			lifacmvrec.glsign.set(t5645rec.sign14);
			lifacmvrec.glcode.set(t5645rec.glmap14);
			lifacmvrec.contot.set(t5645rec.cnttot14);
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(covrpf.getChdrnum()); //ILIFE-8786
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifacmvrec.transactionDate.set(wsaaEffdate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			xxxxFatalError();
		}
		b900aUpdateCovrmja();
	}
 //ILIFE-8786 start
protected void b800WriteZrstNinv(Zrstpf zrstnud)
	{
		zrstnud.setChdrcoy(covrpf.getChdrcoy());
		zrstnud.setChdrnum(covrpf.getChdrnum());
		zrstnud.setLife(covrpf.getLife());
		zrstnud.setJlife(SPACES.stringValue());
		zrstnud.setCoverage(covrpf.getCoverage());
		zrstnud.setRider(covrpf.getRider());
		zrstnud.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		zrstnud.setTrandate(wsaaEffdate.toInt());
		zrstnud.setTranno(chdrmjaIO.getTranno().toInt());
		wsaaSeqno.add(1);
		zrstnud.setXtranno(0);
		zrstnud.setSeqno(wsaaSeqno.toInt());
		zrstnud.setUstmno(0);
		zrstnud.setFeedbackInd(SPACES.stringValue());
		zrstnudInsertList.clear();
		zrstnudInsertList.add(zrstnud);
		zrstpfDAO.insertZrstpfRecord(zrstnudInsertList);
		 //ILIFE-8786 end
	}

protected void b900aUpdateCovrmja()
	{
		b910aStart();
	}

protected void b910aStart()
	{
		/* Update COVRMJA to reflect addition of coverage debt.            */
 //ILIFE-8786 start
		covrpf = covrpfDAO.getcovrincrRecord(taxdbilpf.getChdrcoy(),taxdbilpf.getChdrnum(),taxdbilpf.getLife(),
				taxdbilpf.getCoverage(),taxdbilpf.getRider(),taxdbilpf.getPlansfx());
		
		setPrecision(covrpf.getCoverageDebt(), 2);
		covrpf.setCoverageDebt(add(covrpf.getCoverageDebt(), wsaaTotNonInvestTax).getbigdata());
		
		if(!covrpfDAO.updateCrdebt(covrpf)) {
			syserrrec.params.set(covrpf.getChdrnum());
			xxxxFatalError();
 //ILIFE-8786 end
		}
	}

protected void c100PremiumHistory()
	{
		c110Init();
	}

protected void c110Init()
	{
		//ILIFE-8786 start
		if (isNE(pcdtmja.getInstprem(), 0)) {
			Zptnpf zptnIO = new Zptnpf();
			zptnIO.setChdrcoy(pcdtmja.getChdrcoy());
			zptnIO.setChdrnum(pcdtmja.getChdrnum());
			zptnIO.setLife(pcdtmja.getLife());
			zptnIO.setCoverage(pcdtmja.getCoverage());
			zptnIO.setRider(pcdtmja.getRider());
			zptnIO.setTranno(chdrmjaIO.getTranno().toInt());
			zptnIO.setOrigamt(pcdtmja.getInstprem());
			zptnIO.setTransCode(wsaaBatckey.batcBatctrcde.toString());
			zptnIO.setEffdate(wsaaEffdate.toInt());
			zptnIO.setBillcd(wsaaEffdate.toInt());
			zptnIO.setInstfrom(wsaaEffdate.toInt());
			zptnIO.setInstto(wsaaEffdate.toInt());
			zptnIO.setTrandate(wsaaTday.toInt());
			zptnIO.setZprflg("S");
			insertZptnpfList.add(zptnIO);
			zptnpfDAO.insertZptnpf(insertZptnpfList);
 			//ILIFE-8786 end
			
		}
	}

protected void c200CommissionHistory()
	{
		c210Init();
	}

protected void c210Init()
	{
 		//ILIFE-8786 start
		if (isNE(agcmpf.getInitcom(), ZERO)) {
			Zctnpf zctnpf = new Zctnpf();
			zctnpf.setAgntcoy(aglfpf.getAgntcoy());
			zctnpf.setAgntnum(aglfpf.getAgntnum());
			zctnpf.setCommAmt(agcmpf.getInitcom());
			zctnpf.setPremium(agcmpf.getAnnprem());
			zctnpf.setSplitBcomm(pcdtmja.getSplitc(wsaaIndex));
			zctnpf.setZprflg("S");
			zctnpf.setChdrcoy(pcdtmja.getChdrcoy());
			zctnpf.setChdrnum(pcdtmja.getChdrnum());
			zctnpf.setLife(pcdtmja.getLife());
			zctnpf.setCoverage(pcdtmja.getCoverage());
			zctnpf.setRider(pcdtmja.getRider());
			zctnpf.setTranno(chdrmjaIO.getTranno().toInt());
			zctnpf.setTransCode(wsaaBatckey.batcBatctrcde.toString());
			zctnpf.setEffdate(wsaaEffdate.toInt());
			zctnpf.setTrandate(wsaaTday.toInt());
			insertZctnpfList.add(zctnpf);
			zctnpfDAO.insertZctnpf(insertZctnpfList);
 //ILIFE-8786 end
		}
	}

protected void d000CallRounding()
	{
		/*D100-CALL*/
 //ILIFE-8786
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setCompany(atmodrec.company.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			xxxxFatalError();
 //ILIFE-8786 end
		}
		/*D900-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5647 = new FixedLengthStringData(5).init("T5647");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6687 = new FixedLengthStringData(5).init("T6687");
	private FixedLengthStringData t5534 = new FixedLengthStringData(5).init("T5534");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData tr695 = new FixedLengthStringData(5).init("TR695");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData th605 = new FixedLengthStringData(5).init("TH605");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
