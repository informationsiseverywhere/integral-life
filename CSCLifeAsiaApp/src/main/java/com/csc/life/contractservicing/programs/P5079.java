/*
 * File: P5079.java
 * Date: 30 August 2009 0:04:17
 * Author: Quipoz Limited
 *
 * Class transformed from P5079.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.impl.ClntpfDAOImpl;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.AsgnmnaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.dataaccess.TashTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.AglflnbpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.AsgnpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.TashpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Aglflnbpf;
import com.csc.life.contractservicing.dataaccess.model.Asgnpf;
import com.csc.life.contractservicing.dataaccess.model.Covrpf;
import com.csc.life.contractservicing.dataaccess.model.Tashpf;
import com.csc.life.contractservicing.screens.S5079ScreenVars;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*                    CONTRACT ASSIGNMENT
*                    P5079 - Contract Assignee
* Overview :
*     An assignee is a Company/Bank/Individual who is related
* to the contract  by some declaration of interest.  They are
* therefore  entitled  to  any  information  related  to  the
* Contract entitling them to  Schedule Copies etc. An example
* may be a Loan of money by the  bank  to  pay  for  the life
* insurance or perhaps a trustee.
*
*     This program contains a multiple  input  subfile  which
* can hold any amount of assignees dated  on  a  from  and to
* basis.   Assignees   can  be  Deleted/Modified/Added,   all
* assignees are attatched to the client by client roles and a
* detail line is contained  against  the  contract  for  each
* assignee (ASGNPF).
*
* Processing :
*
* 1000-Initialise -
* ~~~~~~~~~~~~~~~Call DATCON1 to get today's Date. Initialise
* the Screen fields with a SCLR function, then  retrieve  the
* Contract Header Details (CHDRMNA) with a function of RETRV.
* We can then start loading the screen header details.
*
*      Header - Read table T5688  with  the  Contract  header
* Contract Type to find the Contract  Type  description, also
* read the Tables T3588 and T3623 for the Premium/Risk Status
* descriptions respectively.  Tables T3588 and T3623 are read
* with the Contract header PSTATCODE  and STATCODE's.   Other
* description to look up will be for  the  Owner  name, Payor
* name and the Agent name.  All the information  required  to
* read the above names is  held  on the CHDRMNA logical file.
* Taking each one in turn READR the CLTS logical for the name
* and format it  by  calling  Plainname.  Once all the screen
* fields have been ascertained, move them to the  screen.  We
* must now load the Subfile for all attached assignees.
*
*     Subfile -  Load  the  first  page  into the subfile, by
* reading through the Assignee File with  a  logical  view of
* ASGNMNA and a function of BEGN. We must however signify the
* first page by holding a first page flag in working storage,
* this will be used when ROLU or ROLD is in action.  The BEGN
* will  be  on  the  CHDRMNA-CHDRNUM (Contract Number) and we
* perform a LOOP section 6 times for each line of the subfile.
*
*     Each loop through will load 1 assignee  until we have a
* Subfile Screen full. This may mean outputting blank lines.
*
*     Firstly read the assignee file storing  the key for the
* first record loaded into the page as this will be used  for
* Pageing Down and Up.  If  the File key changes then set the
* subfile record to blanks  and  a flag to signify that there
* are no more assignees to load. On a successful read we will
* attempt  to  find  the  assignee's name by reading the CLTS
* file with the assignee number.  Again, if found, format the
* name.  We  can  now  set  up  the  subfile  record with the
* assignee details.
*
* Note  that  we  will  store the assignee number in a hidden
* field, to check for modifications.  Finally, load  the next
* available sequence number to a hidden sequence number. This
* will control the order in which records were created on the
* file. Each time a successful record is read,  we will check
* its sequence number against a working storage field to find
* the highest number in order to assign new records correctly.
* One last point is a hidden field called UPDTEFLAG  which is
* set with an 'N' if an assignee  record  is  loaded  to  the
* subfile and a ' ' if a blank line  is  inserted.  This flag
* determines  what action  is  to be taken in the record when
* the Assignee file is updated.  The  2000 section will check
* for any changes and  set  the  flag  appropriately, see the
* 2000-section for more detail.
*
*     Whether we have a  blank  record or an assignee to load
* we will perform a subfile add  on  the  screen (SADD) which
* will place the detail line on the screen.
*
*     The above will be performed until we  have  loaded that
* first initial screen.
*
*  2000-Validation -
*  ~~~~~~~~~~~~~~~~  Call the screen to  be  displayed,  then
* check for screen status equal to KILL or CALC. If KILL then
* exit, but if CALC move 'Y' to WSSP-EDTERROR  to  cause  the
* screen to be called at the start of the 2000-section again.
* Next  check  for  a  request  to  ROLD.  This must cause an
* error if the First page flag set in  the  Subfile  load  is
* equal to 'Y',  signifying  we are at the start of the file.
* We must also flag the fact that ROLD  or ROLU was requested
* for use in the 4000-section where  any such request will be
* handled.  This  is  because  we must update the file before
* loading the next/previous page, otherwise we would lose all
* the information entered.
*
*     The main part of the validation  section  is  to action
* any changes made to the  subfile  records,  hence  we  will
* validate all subfile records until the end of the screen is
* reached. Each record is validated as follows :
*
*     - Perform a sequential read next change to find
*       any altered records (SRNCH).
*
*     - If the Subfile record is blank and the hidden
*       update flag is 'N' then we will reset the flag
*       to 'D' as this record was previously loaded from
*       the ASGN file and is now to be deleted.
*
*     - If the fields are blank and the update flag is
*       blank move an 'X' to the update flag to signify
*       the record was blank on the subfile load and is
*       still blank now.
*
*     - If the assignee number is not blank then read
*       the CLTS file for the assignee's name. This is
*       prevalant on an option of CALC after altering
*       the assignee's number.
*
*     - Validate that the date TO is not prior to the
*       date FROM.
*
*     - If we have got this far and the update flag is
*       still 'N' or ' ' we can reset the flag to 'U'
*       for Update and 'A' for add. 'N' means we loaded
*       a record from the ASGN file initially and ' '
*       means we have just added details to an initial
*       blank subfile line.  If, however, the update flag
*       is 'N' and the hidden Assignee number is different
*       from the one on the screen, then the assignee has
*       been overtyped so delete the overtyped record
*       having set the flag to 'D' and then reset it to
*       'A' to add the new record.
*
*     Lastly we must Update the subfile record with a
* function of SUPD to pass the newly changed update flags and
* screen changes into force.
*
* 3000-Update    -
* ~~~~~~~~~~~
* SCRN-STATUZ = KILL then exit, otherwise update the assignee
* file for each record in the subfile, i.e. 6 times.
* For each record :   Call the screen with an SREAD to read
*                     the record next check to see if the
*                     record has been updated/added/deleted.
*
*                     * No updates i.e. flag = 'N' or ' '
*                       exit the section.
*
*                     * Set up the Key details of the
*                       ASGNMNA file for the following
*                       updates.
*
*                     * If the record is to be deleted
*                       then READH the ASGN record then
*                       DELET the held record and finally
*                       rewrite the client Role as with
*                       a USED-TO-BE flag set to U by
*                       CALLing CLTRELN with a function
*                       of 'REM '.
*                       To CALL the CLTRELN subroutine
*                       use the following key details.
*
*                            - CLNTPFX = 'CN'
*                            - CLNTCOY = WSSP-FSUCO
*                            - CLNTNUM = Hidden assignee No
*                            - CLRRROLE= 'NE'
*                            - FOREPFX = 'CH'
*                            - FORECOY = CHDRCOY
*                            - FORENUM = CHDRNUM
*
*                     * If the record is to be added then
*                       add 1 to the stored seqno and
*                       move it to the file, move all
*                       details from the screen record to
*                       the file and WRITR the rec.
*                       We must also add a Client Role
*                       record with parameters as above,
*                       but the CLNTNUM field will
*                       contain the screen assignee
*                       number.
*
*                     * If we have got this far then we
*                       must be updating the record.
*                       Simply read the record and hold
*                       it READH and move the screen
*                       details to the file and rewrite
*                       the record REWRT.
*
* 4000-Next     -
* ~~~~~~~~~
*     This section will handle either a  ROLU, ROLD or EXIT
* request.
*
* ROLU :  Set the first page flag off as we are loading the
* next page of data. set the file function to read the next
* record then clear the Subfile for the load. We must now load
* the screen with another 6 records by performing the same
* process as in the 1000-section see above.
*
* ROLD :  If Rolldown requested move the key of the first
* record in the last subfile page to the key of the file and
* BEGIN in order to position the file correctly. We will read
* backwards through the file in order to find the previous 6
* records. To do this however we must read seven records
* backwards in order to determine if we are at the first
* record of the file, as this will mean we must set flags for
* validation. If at any time during the NEXTP reads we hit the
* beginning of the file and have not yet filled the subfile,
* we must load forwards as in the above 1000-section subfile
* load. Note that we can never load the subfile from the
* bottom up as the IO  RRN cannot be manipulated. We must find
* our file position and load forward on a clean subfile.
*
* EXIT :  If we are finished processing then we must move
* the program name to WSSP-NEXTPROG in order to re-establish
* the switching order, add 1 to the program pointer but before
* exiting we must perform certain updates to the PTRN file
* (Policy Transactions), BATCUP (Batch Update)and finally UNLK
* the Softlock contract record. When complete exit.
*
* GENERAL HOUSEKEEPING
*
*       - Write a PTRN record (if updating the CHDR):
*
*           - contract key from contract header
*           - transaction number from WSSP
*           - transaction effective date from VRCM-DATE
*           - batch key information WSSP batchkey
*
*      - Update the header by calling BATCUP with a function of
*           SPACES and the following parameters:
*
*           - transaction count equals 1
*           - all other amounts equal to zero
*           - batch key from WSSP
*
*      FILES USED     - AGLFLNBSKM
*                       ASGNMNASKM
*                       CLTSSKM
*                       CLRRSKM
*                       CHDRMNASKM
*                       DESCSKM
*                       LIFELNBSKM
*                       PTRNSKM
*     TABLES - T3623, T3588, T5688.
*
*
*****************************************************************
* </pre>
*/
public class P5079 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5079");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*----First page is set when at the start of the ASGN file.*/
	private String wsaaFirstPage = "";
		/*----RRN used to read back up the ASGN file one screens worth.*/
	private PackedDecimalData wsaaRrn = new PackedDecimalData(2, 0);
	private boolean subFileNoRecord = false;
		/*----SEQNO used to load the file in order of entry.*/
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(3, 0);

		/*----The following work area is to hold the ASGN key of the
		----first subfile record for ROLLDOWN processing.*/
	private FixedLengthStringData wsaaFirstAsgnKey = new FixedLengthStringData(63);
	private FixedLengthStringData wsaaAsgnChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFirstAsgnKey, 0);
	private FixedLengthStringData wsaaAsgnChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstAsgnKey, 1);
	private FixedLengthStringData wsaaAsgnAsgnnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstAsgnKey, 9).init(SPACES);
	private PackedDecimalData wsaaAsgnSeqno = new PackedDecimalData(3, 0).isAPartOf(wsaaFirstAsgnKey, 17);
	private FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(wsaaFirstAsgnKey, 19, FILLER).init(SPACES);

		/* WSAA-ROLL-FLGS */
	private FixedLengthStringData wsaaRolu = new FixedLengthStringData(1);
	private Validator rollup = new Validator(wsaaRolu, "Y");

	private FixedLengthStringData wsaaRold = new FixedLengthStringData(1);
	private Validator rolldown = new Validator(wsaaRold, "Y");

		/*----Flag to signify the store of the first ASGN record loaded
		----at subfile RRN 1. Used for ROLD request.*/
	private FixedLengthStringData wsaaFirstSubfileRec = new FixedLengthStringData(1);
	private Validator firstSubfileRec = new Validator(wsaaFirstSubfileRec, "Y");

		/*----Flag to signify no more records to load from ASGNMNA.*/
	private FixedLengthStringData wsaaNoMoreAssignees = new FixedLengthStringData(1);
	private Validator asgnmnaEof = new Validator(wsaaNoMoreAssignees, "Y");

		/*----Batch transaction code hold field.*/
	private FixedLengthStringData wsaaBatcKey = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaBatcBatctrcde = new FixedLengthStringData(5).isAPartOf(wsaaBatcKey, 17);
		/*----Current system date.*/
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private String wsaaUpdated = "N";

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 1).setUnsigned();

		/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
		/* ERRORS */
	private static final String e017 = "E017";
	private static final String e304 = "E304";
	private static final String f498 = "F498";
	private static final String h359 = "H359";
	private static final String e058 = "E058";
	private static final String h366 = "H366";
	private static final String e032 = "E032";
	private static final String f782 = "F782";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5688 = "T5688";
	private static final String tr384 = "TR384";
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private AsgnmnaTableDAM asgnmnaIO = new AsgnmnaTableDAM();
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	//private CltsTableDAM cltsIO = new CltsTableDAM(); ILB-487
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	//private DescTableDAM descIO = new DescTableDAM(); ILB-487
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TashTableDAM tashIO = new TashTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	protected S5079ScreenVars sv = ScreenProgram.getScreenVars( S5079ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAOP6351",CovrpfDAO.class);
	private AglflnbpfDAO aglflnbpfDAO = getApplicationContext().getBean("aglflnbpfDAOP5079",AglflnbpfDAO.class);
	private AsgnpfDAO asgnpfDAO = getApplicationContext().getBean("asgnpfDAOP5079",AsgnpfDAO.class);
	private TashpfDAO tashpfDAO = getApplicationContext().getBean("tashpfDAOP5079",TashpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private int insertCount = 0;
	
	//ILB- 487 
	private Iterator itr= null; 
	private ClntpfDAO clntPfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntPf = new Clntpf();
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		exit2190,
		exit2290,
		exit3190,
		sftlockFile4330,
		exit5090,
		readTr3846020
	}

	public P5079() {
		super();
		screenVars = sv;
		new ScreenModel("S5079", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntPf.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntPf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntPf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntPf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntPf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntPf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntPf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntPf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(clntPf.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntPf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntPf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntPf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntPf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntPf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntPf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntPf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntPf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		contractHeader1020();
		readCovrlnb1025();
		contractDefinition1030();
		loadScreenFields1040();
		contractStatii1050();
		formatNames1060();
		loadSubfile1070();
	}

protected void initialise1010()
	{
		/* Get todays system date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaUpdated = "N";
		/* Store batch key.*/
		wsaaBatckey.set(wsspcomn.batchkey);
		/* Intialise the Screen Subfile.*/
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		sv.subfileArea.set(SPACES);
		sv.commfrom.set(varcom.vrcmMaxDate);
		sv.commto.set(varcom.vrcmMaxDate);
		sv.hseqno.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5079", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void contractHeader1020()
	{
		/* Retrieve contract header information.*/
		chdrmnaIO.setFunction(varcom.retrv);
		/* Retrieve contract header information.*/
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.cnttype.set(chdrmnaIO.getCnttype());
	}

	/**
	* <pre>
	* Read main coverage details.
	* </pre>
	*/
protected void readCovrlnb1025(){
	List<Covrpf> covrpfs = covrpfDAO.getCovrlnbPF(chdrmnaIO.getChdrcoy().toString().trim(), 
			chdrmnaIO.getChdrnum().toString().trim(), "01", "01", "00", ZERO.intValue());
	if (covrpfs.isEmpty()) {
		syserrrec.params.set(new FixedLengthStringData(chdrmnaIO.getChdrcoy().toString().trim() + 
				chdrmnaIO.getChdrnum().toString().trim()+ "01"+ "01"+ "00"+ ZERO.intValue()));
		syserrrec.statuz.set(Varcom.mrnf);
		fatalError600();
	} else {
		covrpf = covrpfs.get(0);
	}
		/*covrlnbIO.setDataKey(SPACES);
		covrlnbIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		covrlnbIO.setChdrnum(chdrmnaIO.getChdrnum());
		covrlnbIO.setLife("01");
		covrlnbIO.setCoverage("01");
		covrlnbIO.setRider("00");
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		covrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)
		|| isNE(covrlnbIO.getValidflag(), "1")) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			fatalError600();
		}*/
	}

protected void contractDefinition1030()
	{
		/* Read the contract type description.*/
	    //modified for ILB-487
		
		descpf.setDescpfx("IT");
		descpf.setDesccoy(wsspcomn.company.toString());
		descpf.setDesctabl(t5688);
		descpf.setDescitem(chdrmnaIO.getCnttype().toString());
		descpf.setLanguage(wsspcomn.language.toString());
		descpf= descDAO.getdescData(descpf.getDescpfx(),descpf.getDesctabl(), descpf.getDescitem(), descpf.getDesccoy(), descpf.getLanguage());
		if (null==descpf) {
			sv.ctypdesc.fill("?");
		}
		else {
			sv.ctypdesc.set(descpf.getLongdesc());
		}
	}

protected void loadScreenFields1040()
	{
		/* Load all fields from the Contract Header to the Screen*/
		sv.cownnum.set(chdrmnaIO.getCownnum());
		sv.payrnum.set(chdrmnaIO.getPayrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		sv.billfreq.set(chdrmnaIO.getBillfreq());
		sv.mop.set(chdrmnaIO.getBillchnl());
		sv.agntnum.set(chdrmnaIO.getAgntnum());
	}

protected void contractStatii1050()
	{   //modified for ILB-487
		/* Look up premium status*/
		
		descpf.setDescpfx("IT");
		descpf.setDesccoy(wsspcomn.company.toString());
		descpf.setDesctabl(t3588);
		descpf.setDescitem(chdrmnaIO.getPstatcode().toString());
		descpf.setLanguage(wsspcomn.language.toString());
		
		descpf= descDAO.getdescData(descpf.getDescpfx(),descpf.getDesctabl(), descpf.getDescitem(), descpf.getDesccoy(), descpf.getLanguage());
		if (null==descpf) {
			sv.pstate.fill("?");
		}
		else {
			sv.pstate.set(descpf.getShortdesc());
		}
		
		/*if (isEQ(descpf.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}*/
		/*  Look up Risk status*/
		
		descpf.setDescpfx("IT");
		descpf.setDesccoy(wsspcomn.company.toString());
		descpf.setDesctabl(t3623);
		descpf.setDescitem(chdrmnaIO.getStatcode().toString());
		descpf.setLanguage(wsspcomn.language.toString());
		descpf= descDAO.getdescData(descpf.getDescpfx(),descpf.getDesctabl(), descpf.getDescitem(), descpf.getDesccoy(), descpf.getLanguage());
		if (null==descpf) {
			sv.rstate.fill("?");
		}
		else {
			sv.rstate.set(descpf.getShortdesc());
		}
		
		
	}

protected void formatNames1060()
	{
		/*   Look up all other descriptions*/
		/*       - owner name*/
		/*       - payor name*/
		/*       - agent name*/
		if (isNE(sv.cownnum, SPACES)) {
			formatClientName1100();
		}
		if (isNE(sv.payrnum, SPACES)) {
			formatPayorName1200();
		}
		if (isNE(sv.agntnum, SPACES)) {
			formatAgentName1300();
		}
	}

protected void loadSubfile1070()
	{
		/* Load the first page into the subfile.*/
		wsaaNoMoreAssignees.set("N");
		wsaaFirstSubfileRec.set("Y");
		wsaaSeqno.set(ZERO);
		/* Read the Assignee file with a logical view of ASGNMNA.*/
		/* This file will contain all the Assignees against a contract*/
		/*    the Key will be Company, Contract number, and Sequence*/
		/*    number which is used to load the file in the order in*/
		/*    which it was entered.*/
		/* 5000-section is a commom area used to load the 6 lines of the*/
		/*    subfile. It will be used within the 4000-section for ROLD*/
		/*    and ROLU requests.*/
		List<Asgnpf> asgnpfs = asgnpfDAO.getAsgnpfByChdrNum(chdrmnaIO.getChdrcoy().toString().trim(),
				chdrmnaIO.getChdrnum().toString().trim(), "");
		List<Tashpf> tashpfs = new ArrayList<>();
		for (Asgnpf asgnpf : asgnpfs) {
			// process in assigneeRead5010
			/* On the first record store the key for use in ROLD request.*/
			if (firstSubfileRec.isTrue()) {
				wsaaFirstSubfileRec.set("N");
				if ("".equals(asgnpf.getAsgnnum().trim())) {
					wsaaFirstAsgnKey.set(asgnpf.getChdrcoy()+asgnpf.getChdrnum()+asgnpf.getSeqno()+asgnpf.getAsgnnum());
				}
			}
			// process in asgnToScreen5030
			/* Read the Assignee name and format it if the Assignee number is*/
			/*    not blank.*/
			if (!"".equals(asgnpf.getAsgnnum().trim())) {
				//cltsIO.setDataArea(SPACES);
				clntPf.setClntnum(asgnpf.getAsgnnum());
				x100AssigneeName();
			}
			/* Set up the Screen subfile fields.*/
			sv.asgnsel.set(asgnpf.getAsgnnum());
			sv.hasgnnum.set(asgnpf.getAsgnnum());
			sv.reasoncd.set(asgnpf.getReasoncd());
			sv.commfrom.set(asgnpf.getCommfrom());
			sv.commto.set(asgnpf.getCommto());
			sv.hseqno.set(asgnpf.getSeqno());
			if (isGT(asgnpf.getSeqno(), wsaaSeqno)) {
				wsaaSeqno.set(asgnpf.getSeqno());
			}
			//ILIFE-830
			// process in writeTashFile3800();
			Tashpf tashpf = new Tashpf();
			tashpf.setChdrcoy(chdrmnaIO.getChdrcoy().toString());
			tashpf.setChdrnum(chdrmnaIO.getChdrnum().toString());
			tashpf.setTranno(chdrmnaIO.getTranno().toInt());
			tashpf.setValidflag("1");
			tashpf.setAsgnpfx("CN");
			tashpf.setAsgnnum(sv.hasgnnum.toString());
			tashpf.setSeqno(sv.hseqno.toInt());
			tashpf.setCommfrom(asgnpf.getCommfrom());
			tashpf.setCommto(asgnpf.getCommto());
			tashpf.setTrancde(wsaaBatcBatctrcde.toString());
			tashpf.setUsrprf(this.appVars.getUser());
			tashpf.setJobnm(this.appVars.getJobnumber());
			tashpfs.add(tashpf);
			
			/*tashIO.setFormat(formatsInner.tashrec);
			tashIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, tashIO);
			if (isNE(tashIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(tashIO.getStatuz());
				syserrrec.params.set(tashIO.getParams());
				fatalError600();
			}*/
			// end of process in writeTashFile3800();
			//ILIFE-830
			sv.updteflag.set("N");
			// end of process in asgnToScreen5030
			// process in addSubfileRecord5040
			/* Add the record to the Subfile.*/
			addSubfileRecord5040();
			ldSfl1070CustomerSpecific1();
		}
			insertCount = this.tashpfDAO.insertTashsByList(tashpfs);
		if (insertCount != asgnpfs.size()) {
			syserrrec.statuz.set(Varcom.bomb);
			syserrrec.params.set(chdrmnaIO.getChdrnum());
			fatalError600();
		}
		// process in endOfFile5020
		/* On change of key clear that subfile record and set flag to*/
		/*    signify no more Assignee records.*/
		wsaaNoMoreAssignees.set("Y");
		sv.subfileFields.set(SPACES);
		sv.commfrom.set(varcom.vrcmMaxDate);
		sv.commto.set(varcom.vrcmMaxDate);
		sv.hseqno.set(ZERO);
		//addSubfileRecord5040();
		ldSfl1070CustomerSpecific2();		
		// end of process in endOfFile5020
		
		//ILIFE-6386 start
		/*This part will initiate the subfile;
		assigneeRead5010() read the data from asgnmnaIO. */
		/*
		asgnmnaIO.setDataArea(SPACES);
		asgnmnaIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		asgnmnaIO.setChdrnum(chdrmnaIO.getChdrnum());
		asgnmnaIO.setSeqno(ZERO);
		asgnmnaIO.setFunction(varcom.begn);
		for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
			loadSubfile5000();
		}*/
		//ILIFE-6386 end
		/* Set the screen to signify more records can be loaded. Note that*/
		/*    this may NOT really be the case, but we must trick the*/
		/*    the system to enable us to add records.*/
		/* Set the screen to signify it is the first page of data. We can*/
		/*    then Validate against a ROLD request when there are no recs*/
		/*    to load.*/
		scrnparams.subfileMore.set("Y");
		wsaaFirstPage = "Y";
	}

protected void ldSfl1070CustomerSpecific1(){
	
}
protected void ldSfl1070CustomerSpecific2(){
	
	//ilife-7496 starts 
	//add blanc subfiles to enable addition of new rows in UI
	//this will keep six records on page one
	//preventing Subfiles to throw fulld exception
	for (int i =0; i+insertCount<6;i++){ 
	 
		addSubfileRecord5040();
	}
}	

protected void formatClientName1100()
	{
		readClientRecord1110();
	}

protected void readClientRecord1110()
	{
		/* Read the Clients name and format it.*/
		/*cltsIO.setDataArea(SPACES);
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) { ILB-487*/
	
	clntPf.setClntnum(sv.cownnum.toString());
	clntPf.setClntcoy(wsspcomn.fsuco.toString());
	clntPf.setClntpfx("CN");
	/*List<Clntpf> clntpfList= clntPfDAO.queryReadr(clntPf.getClntpfx(), clntPf.getClntcoy(), clntPf.getClntnum());
	clntPf=clntpfList.get(0);
	if (null==clntpfList) {
		fatalError600();
	}
	if (clntpfList.isEmpty() || isNE(clntPf.getValidflag(), 1)) {
			sv.cownnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}*/
	
	clntPf = clntPfDAO.searchClientRecord("CN", wsspcomn.fsuco.toString(), sv.cownnum.toString(),"N");
	
	if (null==clntPf) {
		sv.cownnumErr.set(e304);
		sv.ownername.set(SPACES);
	}
	else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	} 

protected void formatPayorName1200()
	{
		readClientRecord1210();
	}

protected void readClientRecord1210()
	{
		/* Read the Payors name and format it.*/
		/*cltsIO.setDataArea(SPACES);
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.payrnumErr.set(e304);
			sv.payorname.set(SPACES);
		} ILB-487*/
	
		clntPf.setClntnum(sv.payrnum.toString());
		clntPf.setClntcoy(wsspcomn.fsuco.toString());
		clntPf.setClntpfx("CN");
		/*List<Clntpf> clntpfList= clntPfDAO.queryReadr(clntPf.getClntpfx(), clntPf.getClntcoy(), clntPf.getClntnum());
		clntPf=clntpfList.get(0);
		if (null==clntpfList) {
			fatalError600();
		}
		if (clntpfList.isEmpty() || isNE(clntPf.getValidflag(), 1)) {
			sv.payrnumErr.set(e304);
			sv.payorname.set(SPACES);
		}
			else {
				plainname();
				sv.payorname.set(wsspcomn.longconfname);
			}*/
		clntPf = clntPfDAO.searchClientRecord("CN", wsspcomn.fsuco.toString(), sv.payrnum.toString(),"N");
		
		if (null==clntPf) {
			sv.payrnumErr.set(e304);
			sv.payorname.set(SPACES);
		}
		else {
				plainname();
				sv.payorname.set(wsspcomn.longconfname);
			}
	}

	protected void formatAgentName1300() {
		Aglflnbpf aglflnbpf = aglflnbpfDAO.getAgentInformByNum(sv.agntnum.toString().trim(), wsspcomn.company.toString().trim());
//		readAgent1310();
//		readAgentName1320();
		
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(aglflnbpf.getClttype())) {
			wsspcomn.longconfname.set(SPACES);
			/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
			/*        CLTS-GIVNAME         DELIMITED '  '                   */
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(aglflnbpf.getLsurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(aglflnbpf.getLgivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		if (isNE(clntPf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(aglflnbpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(aglflnbpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(aglflnbpf.getSurname());
		}
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void readAgent1310()
	{
		/* Read the Agents name and format it.*/
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntnum);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)
		&& isNE(aglflnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
	}

protected void readAgentName1320()
	{
	//	cltsIO.setDataArea(SPACES);
		clntPf.setClntnum(aglflnbIO.getClntnum().toString());
		clntPf.setClntcoy(wsspcomn.fsuco.toString());
		clntPf.setClntpfx("CN");
		/*cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		} ILB-487*/
		/*List<Clntpf> clntpfList= clntPfDAO.queryReadr(clntPf.getClntpfx(), clntPf.getClntcoy(), clntPf.getClntnum());
		clntPf=clntpfList.get(0);
		if (null==clntpfList) {
			fatalError600();
		}*/
		clntPf = clntPfDAO.searchClientRecord("CN", wsspcomn.fsuco.toString(), aglflnbIO.getClntnum().toString(),"N");
		
		if (null==clntPf) {
			syserrrec.params.set(clntPf);
			fatalError600();
		}
		
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}
	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Call the screen to display the information.                     */
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2020();
			checkForErrors2030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5079IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                S5079-DATA-AREA                  */
		/*                                S5079-SUBFILE-AREA.              */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2020()
	{
		/* If terminating processing go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/* If roll down requested and display shows first page then error.*/
		if (isEQ(scrnparams.statuz, varcom.rold)
		&& isEQ(wsaaFirstPage, "Y")) {
			scrnparams.errorCode.set(f498);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Set flag to signify ROLD requested.*/
		if (isEQ(scrnparams.statuz, varcom.rold)) {
			wsaaRold.set("Y");
		}
		/* If roll up requested and no more assignees to be displayed then*/
		/*    error.*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaRolu.set("Y");
		}
	}

protected void checkForErrors2030()
	{	
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE-SUBFILE*/
		/* Validate all enteries in the subfile.*/
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2100();
		}
	}

protected void validateSubfile2100()
	{
		try {			
			readNextModifiedRecord2110();
			updateErrorIndicators2130();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readNextModifiedRecord2110()
	{
		/* Read down the subfile to find the next modified record.*/
		scrnparams.function.set(varcom.srnch);		
		processScreen("S5079", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		/*FIELD-VALIDATIONS*/
		/* Validate changes to any of the subfile fields.*/
		fieldValidations2200();
	}

protected void updateErrorIndicators2130()
	{
		/* SUPD is performed on each record NOT just when errors occur,*/
		/*   it is used to update the S5079-updteflag for processing in*/
		/*   the 3000 section.*/
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5079", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void fieldValidations2200()
	{
		try {
			delete2210();
			validate2240();
			updateOrAdd2270();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void delete2210()
	{
		/* Check if fields have been blanked out i.e. deleted.*/
		if (isEQ(sv.asgnsel, SPACES)
		&& isEQ(sv.reasoncd, SPACES)
		&& isEQ(sv.commfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.commto, varcom.vrcmMaxDate)
		&& isEQ(sv.updteflag, "N")) {
			sv.updteflag.set("D");
			wsaaUpdated = "Y";
			goTo(GotoLabel.exit2290);
		}
		/*BLANK*/
		/* Fields have been blanked out for a new record*/
		if (isEQ(sv.asgnsel, SPACES)
		&& isEQ(sv.reasoncd, SPACES)
		&& isEQ(sv.commfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.commto, varcom.vrcmMaxDate)
		&& isNE(sv.hasgnnum, SPACES)
		&& isEQ(sv.updteflag, " ")) {
			sv.updteflag.set("X");
			wsaaUpdated = "Y";
			goTo(GotoLabel.exit2290);
		}
		/* If ASGNSEL is spaces, then throw an error                       */
		if (isEQ(sv.asgnsel, SPACES)
		&& (isNE(sv.reasoncd, SPACES)
			|| isNE(sv.commfrom, varcom.vrcmMaxDate)
			|| isNE(sv.commto, varcom.vrcmMaxDate))) {
			sv.asgnselErr.set(h366);
		}
	}

protected void validate2240()
	{
		/* Read the Assignee name if the assignee number is not spaces.*/
		if (isNE(sv.asgnsel, SPACES)) {
			//cltsIO.setDataArea(SPACES);
			clntPf.setClntnum(sv.asgnsel.toString());
			x100AssigneeName();
		}
		/* Make Reason Code mandatory                                      */
		if (isEQ(sv.reasoncd, SPACES)
		&& (isNE(sv.asgnsel, SPACES)
			|| isNE(sv.commfrom, varcom.vrcmMaxDate)
			|| isNE(sv.commto, varcom.vrcmMaxDate))) {
			sv.reasoncdErr.set(h366);
		}
		/* If the Commence from date is before the Contract Commencement   */
		/* date then error.                                                */
		if (isLT(sv.commfrom, sv.occdate)) {
			sv.commfromErr.set(h359);
		}
		/* Assignment 'To Date' to be defaulted to Risk Cessation Date     */
		if (isEQ(sv.commto, varcom.vrcmMaxDate)) {
			if (isNE(sv.asgnsel, SPACES)
			|| isNE(sv.reasoncd, SPACES)
			|| isNE(sv.commfrom, varcom.vrcmMaxDate)) {
				// sv.commto.set(covrlnbIO.getRiskCessDate());
				sv.commto.set(covrpf.getRcesDte());
			}
		}
		else {
			// if (isGT(sv.commto, covrlnbIO.getRiskCessDate())) {
			if (isGT(sv.commto, covrpf.getRcesDte())) {
				sv.commtoErr.set(e032);
			}
		}
		/* If the Commence from date is after the Commence to date then*/
		/*    error.*/
		if (isGT(sv.commfrom, sv.commto)) {
			sv.commtoErr.set(e017);
		}
	}

protected void updateOrAdd2270()
	{
		/* The subfile record has been changed so update the hidden*/
		/*    flag. If it is 'N' then a record exists already so update*/
		/*    else if the flag is ' ' then we are adding a new record.*/
		/* If the update flag is 'N' and the hidden assignee number is     */
		/* different from the screen assignee number then the record       */
		/* has been overwritten, so first delete the old record.  The      */
		/* new one will be inserted following the deletion.                */
		if (isEQ(sv.updteflag, "N")) {
			if (isNE(sv.hasgnnum, sv.asgnsel)) {
				sv.updteflag.set("D");
			}
			else {
				sv.updteflag.set("U");
			}
		}
		/*MOVE 'U'                 TO S5079-UPDTEFLAG.              */
		if (isEQ(sv.updteflag, SPACES)
		&& isNE(sv.asgnsel, SPACES)
		&& isNE(sv.reasoncd, SPACES)
		&& isNE(sv.commfrom, varcom.vrcmMaxDate)) {
			sv.updteflag.set("A");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* If termination of processing then go to exit, no updates reqd.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/* Perform updates for all subfile records up to a maximum of*/
		/*    one page.*/
		// for jave there is no limit for one page, so use the 'mrf' to check whether there is still record
//		for (wsaaRrn.set(1); !(isGT(wsaaRrn, 6)); wsaaRrn.add(1)){
		subFileNoRecord = false;
		for (wsaaRrn.set(1); !(isGT(wsaaRrn, 99)); wsaaRrn.add(1)){
			updateAssignees3100();
			if (subFileNoRecord) {
				break;
			}
		}
		if (isNE(wsaaUpdated, "N")) {
			writeLetter6000();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void updateAssignees3100()
	{
		try {
			// readSubfile3110();
			/* Get the subfile record using the relative record number.*/
			scrnparams.subfileRrn.set(wsaaRrn);
			scrnparams.function.set(varcom.sread);
			processScreen("S5079", sv);
			if (isEQ(scrnparams.statuz, Varcom.mrnf)) {
				subFileNoRecord = true;
				return;
			}
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			/*NO-UPDATES*/
			/* The subfile record has not been changed.*/
			if (isEQ(sv.updteflag, SPACES)
			|| isEQ(sv.updteflag, "N")) {
				return;
			}
			else {
				wsaaUpdated = "Y";
			}
			
			//setAsgnKey3130();
			/* Set up key for updating or writing an Assignee.*/
			asgnmnaIO.setDataArea(SPACES);
			asgnmnaIO.setChdrcoy(chdrmnaIO.getChdrcoy());
			asgnmnaIO.setChdrnum(chdrmnaIO.getChdrnum());
			asgnmnaIO.setAsgnnum(sv.hasgnnum);
			asgnmnaIO.setSeqno(sv.hseqno);
			
			//deleteAssignee3140();
			/* If the update is a delete.*/
			if (isEQ(sv.updteflag, "D")) {
				asgnmnaIO.setFunction(varcom.readh);
				asgnmnaFile3300();
				asgnmnaIO.setFormat(formatsInner.asgnmnarec);
				asgnmnaIO.setFunction(varcom.delet);
				asgnmnaFile3300();
				deleteClientRole3400();
				//ILIFE-830
				//writeTashFile3800();
				//ILIFE-830
				if (isEQ(sv.asgnsel, SPACES)) {
					return;
				}
				else {
					sv.updteflag.set("A");
				}
			}
			
			//addAssignee3150();
			/* If the update is an addition.*/
			if (isEQ(sv.updteflag, "A")) {
				addAssignee3200();		
				return;
			}
			
			updateAssignee3160();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readSubfile3110()
	{
		/* Get the subfile record using the relative record number.*/
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S5079", sv);
		if (isEQ(scrnparams.statuz, Varcom.mrnf)) {
			subFileNoRecord = true;
			goTo(GotoLabel.exit3190);
		}
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*NO-UPDATES*/
		/* The subfile record has not been changed.*/
		if (isEQ(sv.updteflag, SPACES)
		|| isEQ(sv.updteflag, "N")) {
			goTo(GotoLabel.exit3190);
		}
		else {
			wsaaUpdated = "Y";
		}
	}

	/**
	* <pre>
	***     GO TO 3190-EXIT.
	* </pre>
	*/
protected void setAsgnKey3130()
	{
		/* Set up key for updating or writing an Assignee.*/
		asgnmnaIO.setDataArea(SPACES);
		asgnmnaIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		asgnmnaIO.setChdrnum(chdrmnaIO.getChdrnum());
		asgnmnaIO.setAsgnnum(sv.hasgnnum);
		asgnmnaIO.setSeqno(sv.hseqno);
	}

protected void deleteAssignee3140()
	{
		/* If the update is a delete.*/
		if (isEQ(sv.updteflag, "D")) {
			asgnmnaIO.setFunction(varcom.readh);
			asgnmnaFile3300();
			asgnmnaIO.setFormat(formatsInner.asgnmnarec);
			asgnmnaIO.setFunction(varcom.delet);
			asgnmnaFile3300();
			deleteClientRole3400();
			//ILIFE-830
			//writeTashFile3800();
			//ILIFE-830
			if (isEQ(sv.asgnsel, SPACES)) {
				goTo(GotoLabel.exit3190);
			}
			else {
				sv.updteflag.set("A");
			}
		}
	}

	/**
	* <pre>
	********GO TO 3190-EXIT.
	* </pre>
	*/
protected void addAssignee3150()
	{
		/* If the update is an addition.*/
		if (isEQ(sv.updteflag, "A")) {
			addAssignee3200();		
			goTo(GotoLabel.exit3190);
		}
	}

protected void updateAssignee3160()
	{
		/* If the update is an update.*/
		if (isEQ(sv.updteflag, "U")) {
			asgnmnaIO.setFunction(varcom.readh);
			asgnmnaFile3300();
		}
		writeTashFile3800();
		asgnmnaIO.setAsgnnum(sv.asgnsel);
		asgnmnaIO.setReasoncd(sv.reasoncd);
		asgnmnaIO.setCommfrom(sv.commfrom);
		asgnmnaIO.setCommto(sv.commto);
		/* MOVE VRCM-TERM              TO ASGNMNA-TERMID.               */
		asgnmnaIO.setTermid(varcom.vrcmTermid);
		asgnmnaIO.setUser(varcom.vrcmUser);
		asgnmnaIO.setTransactionDate(varcom.vrcmDate);
		asgnmnaIO.setTransactionTime(varcom.vrcmTime);
		asgnmnaIO.setFormat(formatsInner.asgnmnarec);
		asgnmnaIO.setFunction(varcom.rewrt);
		asgnmnaFile3300();
	}

protected void addAssignee3200()
	{
		asgnmnaFile3210();
		addRole3220();
	}

protected void asgnmnaFile3210()
	{
		/* Set up all fields for the creation of an Assignee record.*/
		asgnmnaIO.setAsgnnum(sv.asgnsel);
		asgnmnaIO.setReasoncd(sv.reasoncd);
		asgnmnaIO.setCommfrom(sv.commfrom);
		asgnmnaIO.setCommto(sv.commto);
		asgnmnaIO.setAsgnpfx("CN");
		asgnmnaIO.setTranno(chdrmnaIO.getTranno());
		wsaaSeqno.add(1);
		asgnmnaIO.setSeqno(wsaaSeqno);
		/* MOVE VRCM-TERM              TO ASGNMNA-TERMID.               */
		asgnmnaIO.setTermid(varcom.vrcmTermid);
		asgnmnaIO.setUser(varcom.vrcmUser);
		asgnmnaIO.setTransactionDate(varcom.vrcmDate);
		asgnmnaIO.setTransactionTime(varcom.vrcmTime);
		asgnmnaIO.setFormat(formatsInner.asgnmnarec);
		asgnmnaIO.setFunction(varcom.writr);
		asgnmnaFile3300();
	}

protected void addRole3220()
	{
		/* Create a new Client Role for the Assignee.*/
		/*    MOVE SPACES                 TO CLRR-PARAMS.                  */
		/*    MOVE 'NE'                   TO CLRR-CLRRROLE.                */
		/*    MOVE 'CH'                   TO CLRR-FOREPFX.                 */
		/*    MOVE CHDRMNA-CHDRCOY        TO CLRR-FORECOY.                 */
		/*    MOVE CHDRMNA-CHDRNUM        TO CLRR-FORENUM.                 */
		/*    MOVE 'CN'                   TO CLRR-CLNTPFX.                 */
		/*    MOVE WSSP-FSUCO             TO CLRR-CLNTCOY.                 */
		/*    MOVE S5079-ASGNSEL          TO CLRR-CLNTNUM.                 */
		/*    MOVE CLRRREC                TO CLRR-FORMAT.                  */
		/*    MOVE WRITR                  TO CLRR-FUNCTION.                */
		/*    CALL 'CLRRIO'  USING CLRR-PARAMS.                            */
		/*    IF CLRR-STATUZ              NOT = O-K                        */
		/*                            AND NOT = DUPR                       */
		/*       MOVE CLRR-PARAMS         TO SYSR-PARAMS                   */
		/*       MOVE CLRR-STATUZ         TO SYSR-STATUZ                   */
		/*       PERFORM 600-FATAL-ERROR.                                  */
		initialize(cltrelnrec.cltrelnRec);
		cltrelnrec.clrrrole.set("NE");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.asgnsel);
		cltrelnrec.function.set("ADD ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)
		&& isNE(cltrelnrec.statuz, "F962")) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

protected void asgnmnaFile3300()
	{
		/*ASGNMNA-FILE*/
		/* Call the Assignee file.*/
		if (isEQ(sv.updteflag, "X")) {
			return ;
		}
		SmartFileCode.execute(appVars, asgnmnaIO);
		if (isNE(asgnmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(asgnmnaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void deleteClientRole3400()
	{
		removeRole3410();
	}

protected void removeRole3410()
	{
		/* Call CLTRELN which will read the Client Role record and rewrite*/
		/* it with a USED-TO-BE flag set to "U".                           */
		initialize(cltrelnrec.cltrelnRec);
		cltrelnrec.clrrrole.set("NE");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(chdrmnaIO.getChdrcoy());
		cltrelnrec.forenum.set(chdrmnaIO.getChdrnum());
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.hasgnnum);
		cltrelnrec.function.set("REM ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)
		&& isNE(cltrelnrec.statuz, "F968")) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

protected void writeTashFile3800()
	{
		start3810();
	}

protected void start3810()
	{
		tashIO.setParams(SPACES);
		tashIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		tashIO.setChdrnum(chdrmnaIO.getChdrnum());
		tashIO.setTranno(chdrmnaIO.getTranno());
		tashIO.setValidflag("1");
		tashIO.setAsgnpfx("CN");
		tashIO.setAsgnnum(sv.hasgnnum);
		tashIO.setSeqno(sv.hseqno);
		tashIO.setCommfrom(asgnmnaIO.getCommfrom());
		tashIO.setCommto(asgnmnaIO.getCommto());
		tashIO.setTrancde(wsaaBatcBatctrcde);
		tashIO.setFormat(formatsInner.tashrec);
		tashIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, tashIO);
		if (isNE(tashIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(tashIO.getStatuz());
			syserrrec.params.set(tashIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		/* If roll up requested set first page flag off and the First*/
		/*   subfile record flag ON, as we are about to load a new page.*/
		/* Move NEXTR to read the next GN record. Clear the subfile for*/
		/*   loading and read in the next 6 lines.*/
		if (rollup.isTrue()) {
			wsaaFirstPage = "N";
			wsaaFirstSubfileRec.set("Y");
			asgnmnaIO.setFunction(varcom.nextr);
			clearSubfile4200();
			for (int loopVar2 = 0; !(loopVar2 == 6); loopVar2 += 1){
				loadSubfile5000();
			}
		}
		/* If roll down requested move the key of the first record in the*/
		/*   last subfile page to the key of the file and BEGIN in order*/
		/*   to position the file correctly.*/
		/* We will read backwards through the file in order to find the*/
		/*   previous 6 records.*/
		if (rolldown.isTrue()) {
			asgnmnaIO.setParams(SPACES);
			asgnmnaIO.setDataKey(wsaaFirstAsgnKey);
			asgnmnaIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, asgnmnaIO);
			if (isNE(asgnmnaIO.getStatuz(), varcom.oK)
			&& isNE(asgnmnaIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(asgnmnaIO.getParams());
				fatalError600();
			}
			else {
				/* Set the count to position itself 7 records back. This will*/
				/*   determine if we are loading the first page again. i.e.*/
				/*   If the end of the file is hit on the seventh NEXTP then we*/
				/*   are at the start of the file.*/
				wsaaRrn.set(7);
				while ( !(isEQ(asgnmnaIO.getStatuz(), varcom.endp)
				|| isNE(asgnmnaIO.getChdrcoy(), chdrmnaIO.getChdrcoy())
				|| isNE(asgnmnaIO.getChdrnum(), chdrmnaIO.getChdrnum())
				|| isEQ(wsaaRrn, 0))) {
					roldSubfileLoad4100();
				}

			}
		}
		/* If we have requested a page backwards (ROLD) then we perform*/
		/*   the load of the previous 6 records. Note that The position*/
		/*   of the file will either be 7 records back with a function*/
		/*   of NEXTP or positioned at the start of the file with a BEGN.*/
		if (rolldown.isTrue()
		&& isEQ(wsaaRrn, 0)) {
			wsaaFirstSubfileRec.set("Y");
			clearSubfile4200();
			for (int loopVar3 = 0; !(loopVar3 == 6); loopVar3 += 1){
				loadSubfile5000();
			}
		}
		/* Clear the Roll up and Roll down flags for future use.*/
		/* Redisplay the screen.*/
		if (rollup.isTrue()
		|| rolldown.isTrue()) {
			wsaaRolu.set("N");
			wsaaRold.set("N");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			/* Perform updates for this transaction.*/
			/* Exit to the next program in the stack.*/
			finalUpdates4300();
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
		}
	}

protected void roldSubfileLoad4100()
	{
		roldSubfileLoad4110();
	}

protected void roldSubfileLoad4110()
	{
		/* Read the previous Assignee records until a key change or the*/
		/*   section has been performed 7 times.*/
		asgnmnaIO.setFunction(varcom.nextp);
		SmartFileCode.execute(appVars, asgnmnaIO);
		if (isNE(asgnmnaIO.getStatuz(), varcom.oK)
		&& isNE(asgnmnaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(asgnmnaIO.getParams());
			fatalError600();
		}
		/* If key changes then we will have to clear the subfile and start*/
		/*   again but at the start of the file this time with a BEGN.*/
		/* This means we are now loading the first page of the whole*/
		/*   subfile. 5000-section will load each line for us.*/
		if (isNE(asgnmnaIO.getChdrcoy(), chdrmnaIO.getChdrcoy())
		|| isNE(asgnmnaIO.getChdrnum(), chdrmnaIO.getChdrnum())
		|| isEQ(asgnmnaIO.getStatuz(), varcom.endp)) {
			asgnmnaIO.setParams(SPACES);
			clearSubfile4200();
			asgnmnaIO.setDataKey(wsaaFirstAsgnKey);
			asgnmnaIO.setFunction(varcom.begn);
			wsaaFirstPage = "Y";
			wsaaRrn.set(0);
			return ;
		}
		/* We will move The ASGN key to store on each record as it will*/
		/*    be required if the end of file is reached.*/
		/* Decrease the count until we are 7 records back up the file.*/
		/* Assumeing we can count back 7 records, we will move a function*/
		/*    of NEXTR to read the next record forward in the 5000-section*/
		/* Remember we read 7 records backwards to find out if we will hit*/
		/*    the beginning of the file.*/
		wsaaFirstAsgnKey.set(asgnmnaIO.getDataKey());
		wsaaRrn.subtract(1);
		asgnmnaIO.setFunction(varcom.nextr);
	}

protected void clearSubfile4200()
	{
		/*CLEAR-SUBFILE*/
		/* Initialise the Screen subfile.*/
		sv.subfileArea.set(SPACES);
		sv.commfrom.set(varcom.vrcmMaxDate);
		sv.commto.set(varcom.vrcmMaxDate);
		sv.hseqno.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5079", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void finalUpdates4300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					finalUpdatesPara4300();
					ptrnFile4310();
					callBldenrl4315();
					bcupFile4320();
				case sftlockFile4330:
					sftlockFile4330();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void finalUpdatesPara4300()
	{
		/* Check if updating is required.                                  */
		if (isEQ(wsaaUpdated, "N")) {
			goTo(GotoLabel.sftlockFile4330);
		}
		/*UPDATE-CHDR*/
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
	}

protected void ptrnFile4310()
	{
		/* Write a PTRN record.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		wsaaBatcKey.set(wsspcomn.batchkey);
		ptrnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmnaIO.getChdrnum());
		ptrnIO.setTranno(chdrmnaIO.getTranno());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		/* MOVE VRCM-TERM              TO PTRN-TERMID.                  */
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void callBldenrl4315()
	{
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrmnaIO.getChdrpfx());
		bldenrlrec.company.set(chdrmnaIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrmnaIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
	}

protected void bcupFile4320()
	{
		/* Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsspcomn.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.statuz.set(ZERO);
		batcuprec.function.set(SPACES);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
	}

protected void sftlockFile4330()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     Load subfile
	* </pre>
	*/
protected void loadSubfile5000()
	{
		try {
			assigneeRead5010();
			endOfFile5020();
			asgnToScreen5030();
			addSubfileRecord5040();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void assigneeRead5010()
	{
		/* Read the ASGN file for all Assignees associated with the*/
		/*    Contract. This is performed 6 times once for each line of*/
		/*    the subfile.*/
		SmartFileCode.execute(appVars, asgnmnaIO);
		if (isNE(asgnmnaIO.getStatuz(), varcom.oK)
		&& isNE(asgnmnaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(asgnmnaIO.getParams());
			fatalError600();
		}
		/* On the first record store the key for use in ROLD request.*/
		if (firstSubfileRec.isTrue()) {
			wsaaFirstSubfileRec.set("N");
			if (isNE(asgnmnaIO.getAsgnnum(), SPACES)) {
				wsaaFirstAsgnKey.set(asgnmnaIO.getRecKeyData());
			}
		}
		asgnmnaIO.setFunction(varcom.nextr);
	}

protected void endOfFile5020()
	{
		/* On change of key clear that subfile record and set flag to*/
		/*    signify no more Assignee records.*/
		if (isNE(asgnmnaIO.getChdrcoy(), chdrmnaIO.getChdrcoy())
		|| isNE(asgnmnaIO.getChdrnum(), chdrmnaIO.getChdrnum())
		|| isEQ(asgnmnaIO.getStatuz(), varcom.endp)) {
			wsaaNoMoreAssignees.set("Y");
			sv.subfileFields.set(SPACES);
			sv.commfrom.set(varcom.vrcmMaxDate);
			sv.commto.set(varcom.vrcmMaxDate);
			sv.hseqno.set(ZERO);
			addSubfileRecord5040();
			goTo(GotoLabel.exit5090);
		}
	}

protected void asgnToScreen5030()
	{
		/* Read the Assignee name and format it if the Assignee number is*/
		/*    not blank.*/
		if (isNE(asgnmnaIO.getAsgnnum(), SPACES)) {
			//cltsIO.setDataArea(SPACES);
			clntPf.setClntnum(asgnmnaIO.getAsgnnum().toString());
			x100AssigneeName();
		}
		/* Set up the Screen subfile fields.*/
		sv.asgnsel.set(asgnmnaIO.getAsgnnum());
		sv.hasgnnum.set(asgnmnaIO.getAsgnnum());
		sv.reasoncd.set(asgnmnaIO.getReasoncd());
		sv.commfrom.set(asgnmnaIO.getCommfrom());
		sv.commto.set(asgnmnaIO.getCommto());
		sv.hseqno.set(asgnmnaIO.getSeqno());
		if (isGT(asgnmnaIO.getSeqno(), wsaaSeqno)) {
			wsaaSeqno.set(asgnmnaIO.getSeqno());
		}
		//ILIFE-830
		writeTashFile3800();
		//ILIFE-830
		sv.updteflag.set("N");
	}

protected void addSubfileRecord5040()
	{
		/* Add the record to the Subfile.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5079", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void x100AssigneeName()
	{
		x110LoadScreen();
	}

protected void x110LoadScreen()
	{
		/* Read the clients file for the Assignee name.*/
		/*cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) { ILB-487*/
	
	clntPf.setClntcoy(wsspcomn.fsuco.toString());
	clntPf.setClntpfx("CN");
	/*List<Clntpf> clntpfList= clntPfDAO.queryReadr(clntPf.getClntpfx(), clntPf.getClntcoy(), clntPf.getClntnum());
	clntPf=clntpfList.get(0);
	if (null==clntpfList) {
		fatalError600();
	}
	if (clntpfList.isEmpty() || isNE(clntPf.getValidflag(), 1)) {
			sv.assigneeName.set(SPACES);
			sv.asgnselErr.set(e058);
		}*/
		clntPf = clntPfDAO.searchClientRecord("CN", wsspcomn.fsuco.toString(),clntPf.getClntnum().replaceAll("\\s","") ,"N");
		
		if (null==clntPf) {
			sv.asgnselErr.set(e058);
			sv.assigneeName.set(SPACES);
		}
		else {
			plainname();
			/*       Check Death Date for assignee < Orig Comm Date            */
			if (isNE(clntPf.getCltdod(), varcom.vrcmMaxDate)
			&& isGT(chdrmnaIO.getOccdate(), clntPf.getCltdod())) {
				sv.asgnselErr.set(f782);
			}
			sv.assigneeName.set(wsspcomn.longconfname);
		}
	}

protected void writeLetter6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start6010();
				case readTr3846020:
					readTr3846020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6010()
	{
		/* Read Table T6634 for get letter-type.                           */
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
	}

	/**
	* <pre>
	*6020-READ-T6634.                                         <PCPPRT>
	* </pre>
	*/
protected void readTr3846020()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634        TO ITEM-ITEMITEM.        <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* If record not found then read again using generic key.          */
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)
			&& isNE(wsaaItemCnttype, "***")) {
				wsaaItemCnttype.set("***");
				/*     GO TO 6020-READ-T6634                            <PCPPRT>*/
				goTo(GotoLabel.readTr3846020);
			}
			else {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF T6634-LETTER-TYPE        = SPACES                 <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*   Get Endorsement no.                                       */
		/* MOVE 'NEXT '                TO ALNO-FUNCTION.        <PCPPRT>*/
		/* MOVE 'EN'                   TO ALNO-PREFIX.          <PCPPRT>*/
		/* MOVE WSKY-BATC-BATCBRN      TO ALNO-GENKEY.          <PCPPRT>*/
		/* MOVE WSSP-COMPANY           TO ALNO-COMPANY.         <PCPPRT>*/
		/*                                                      <PCPPRT>*/
		/* CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                 <PCPPRT>*/
		/*                                                      <PCPPRT>*/
		/* IF ALNO-STATUZ              NOT = O-K                <PCPPRT>*/
		/*    MOVE ALNO-STATUZ         TO SYSR-STATUZ           <PCPPRT>*/
		/*    MOVE 'ALOCNO'            TO SYSR-PARAMS           <PCPPRT>*/
		/*    PERFORM 600-FATAL-ERROR.                          <PCPPRT>*/
		/*   Get set-up parameter for call 'HLETRQS'                   */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		/* MOVE 'EN'                   TO LETRQST-RDOCPFX.      <PCPPRT>*/
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		/* MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.      <PCPPRT>*/
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.branch.set(wsspcomn.branch);
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(wsspcomn.language);
		wsaaOkeyTranno.set(chdrmnaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.function.set("ADD");
		/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData asgnmnarec = new FixedLengthStringData(10).init("ASGNMNAREC");
	private FixedLengthStringData chdrmnarec = new FixedLengthStringData(10).init("CHDRMNAREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData tashrec = new FixedLengthStringData(10).init("TASHREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
}
}
