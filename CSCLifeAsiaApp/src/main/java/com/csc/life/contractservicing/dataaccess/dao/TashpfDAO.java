package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;

import com.csc.life.contractservicing.dataaccess.model.Tashpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface TashpfDAO extends BaseDAO<Tashpf> {

	public int insertTashsByList(List<Tashpf> tashpfs);
}
