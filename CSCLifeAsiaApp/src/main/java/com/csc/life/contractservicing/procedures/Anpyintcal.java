/*
 * File: Intcalc.java
 * Date: 29 August 2009 22:57:20
 * Author: Quipoz Limited
 * 
 * Class transformed from INTCALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.pow;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.math.BigDecimal;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.AcmvlonTableDAM;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.recordstructures.Loandtarec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*           INTEREST CALCULATION SUBROUTINE.
*           --------------------------------
* Overview.
* ---------
*
* This Subroutine is called to calculate Interest due on a given
*  Annuity Payment between 2 specified dates passed in the INTCALC parameter
*  area. The interest is calculated in the Loan Currency and
*  returned in the Loan currency to the calling program.
*
*
*Given:
*      Loan no, Company, contract no, interest from date,
*      interest to date, loan prin at last capn date,
*      last loan capn date, loan commencement date, contract type
*
*Gives:
*      interest amount
*
*Functionality.
*--------------
*Read T6633 Loan methods table using the loan commencement date
* in the ITMFRM parameter.
*
*Check if there is a Fixed rate period or not....
*                                  - T6633-GTDFIXPRD = Y or N
*
*If there is,
*     check whether the interest to date is inside this period.
*If there is & it is,
*            use T6633-INT-RATE from this T6633 record.
*If there isn't OR it is outside,
*            REREAD T6633 using effective date as ITMFRM.
*
*We now have an interest rate.
*                                            (last cap loan amt)
*1) Calculate interest for period on the given loan principal
*    amount at last capitalisation date....
*    where the 'period' is between the from & to dates specified
*
*    Loan principal * T6633-int-rate * ( date diff / 365 )
*
*2) Check whether Loan principal has changed since the interest
*    from date ......
*
*  Now look for any changes to the Loan Principal subaccount made
*   since the interest from date and calculate any interest
*   accrued over the period from when the change to the principal
*   was made up to today ( effective date ) - it may be an
*   increase or a decrease.....
*   Do a BEGN on ACMV file - using the ACMVLON logical file,
*    the Loan principal ACMVS are to be processed......
*
*    The logical view ACMVLON is used to read the ACMV file so
*     that it can be read with the Rldgacct field set as the
*     Contract number//Loan number
*
*    Get 1st ACMVLON record
*
*        IF acmv effdate > interest to date,
*           ignore ACMVLON and do NEXTR
*
*        IF acmv effdate < interest from date,
*           ignore ACMVLON and do NEXTR
*        ENDIF
*
*        set datcon3 date 1 to acmv-effdate
*        set datcon3 date 2 to interest to date
*
*        Call DATCON3 to no days between 2 dates
*
*        Calculate interest on ACMVLON amount over above No days
*
*        Interest =
*          ACMVLON-amount * T6633int-rate * (datcon3 days / 365)
*
*        Make sure interest in the the Loan currency
*
*        Add Interest to running total
*
*        Get NEXTR ACMVLON record
*
*
* TABLES USED
* -----------
*
* T6633   -   Loan Interest Rules
* T5645   -   Transaction Accounting Rules
*
* NOTE.                                                  !!!!!
*------
*      ALL VALUES CALCULATED & RETURNED FROM THIS PROGRAM ARE
*       IN LOAN CURRENCY.
*          ----
*
*
*
*****************************************************************
* </pre>
*/
public class Anpyintcal extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "APINTCAL";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaLoanPrinInt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalInterest = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInterest1 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInterest2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaLoanCurrVal = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSign = new FixedLengthStringData(1);

	
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3);
	

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private int wsaaMaxDate = 99999999;
		/* ERRORS */
	private String e723 = "E723";
		/* TABLES */
	private String td5h7 = "TD5H7";
	private String t5645 = "T5645";
		/* FORMATS */
	private String acmvlonrec = "ACMVLONREC";
	private String itdmrec = "ITEMREC";
	private String itemrec = "ITEMREC";
		/*Loans ACMV enquiry logical file*/
	private AcmvlonTableDAM acmvlonIO = new AcmvlonTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Td5h7rec td5h7Fromrec = new Td5h7rec();
	private Td5h7rec td5h7Torec = new Td5h7rec();
	private Varcom varcom = new Varcom();

	private ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private List<Acmvpf> acmvpfList;
	private Acmvpf acmvpf = null;
	private ExternalisedRules er = new ExternalisedRules();
	private ZonedDecimalData intRateFrom = new ZonedDecimalData(8, 5);
	private ZonedDecimalData intRateTo = new ZonedDecimalData(8, 5);
	private int days1 = 0;
	private int days2 = 0;
	private static int totDays = 365;
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		readNextAcmv2270, 
		exit2290, 
		exit99490, 
		exit99590
	}

	public Anpyintcal() {
		super();
	}

public void mainline(Object... parmArray)
	{
		annypyintcalcrec.intcalcRec = convertAndSetParam(annypyintcalcrec.intcalcRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		control100();
		exit190();
	}

protected void control100()
	{
		initialise1000();
		mainProcessing2000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		try {
			start1000();
		}
		catch (GOTOException e){
		}
	}

protected void start1000()
	{
		wsaaSacscode.set(SPACES);
		wsaaSacstype.set(SPACES);
		wsaaSign.set(SPACES);
		annypyintcalcrec.statuz.set(varcom.oK);
		readT5645();	
		days1 = 0;
		days2 = 0;
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemtabl(td5h7);
		itempf.setItemcoy(annypyintcalcrec.chdrcoy.toString());
		itempf.setItemitem(annypyintcalcrec.cnttype.toString());
		itempf.setItmfrm(annypyintcalcrec.interestFrom.getbigdata());
		itempf.setItmto(annypyintcalcrec.interestFrom.getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
				td5h7Fromrec.td5h7Rec.set(StringUtil.rawToString(it.getGenarea()));
				itempf.setItmto(it.getItmto());
			}
		} else {
			return;
		}
		intRateFrom.set(td5h7Fromrec.intrate);
		//days1++;
		calculateDays(annypyintcalcrec.interestFrom.getbigdata(),1);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(annypyintcalcrec.interestFrom.getbigdata());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(days1);
		callDatcon21200();
		if(isLT(datcon2rec.intDate2,annypyintcalcrec.interestTo)) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemtabl(td5h7);
			itempf.setItemcoy(annypyintcalcrec.chdrcoy.toString());
			itempf.setItemitem(annypyintcalcrec.cnttype.toString());
			itempf.setItmfrm(annypyintcalcrec.interestTo.getbigdata());
			itempf.setItmto(annypyintcalcrec.interestTo.getbigdata());
			itempfList = itempfDAO.findByItemDates(itempf);
			if(itempfList.size() > 0) {
				for (Itempf it : itempfList) {			
					td5h7Torec.td5h7Rec.set(StringUtil.rawToString(it.getGenarea()));
					itempf.setItmto(it.getItmto());
				}
			} else {
				return;
			}
			intRateTo.set(td5h7Torec.intrate);
			BigDecimal date = datcon2rec.intDate2.getbigdata();
			datcon2rec.datcon2Rec.set(SPACES);		
			calculateDays(date,2);
		}
		
		
	}

protected void calculateDays(BigDecimal date1,int days)
 {	//IBPLIFE-12546		
    if(isLTE(date1, itempf.getItmto()) && isLTE(date1, annypyintcalcrec.interestTo)) {
    	datcon3rec.datcon3Rec.set(SPACES);
    	
	  if(isLT(itempf.getItmto(), annypyintcalcrec.interestTo)) {
		datcon3rec.intDate2.set(itempf.getItmto());
	  }else {
		datcon3rec.intDate2.set(annypyintcalcrec.interestTo);
	 }
	
		datcon3rec.intDate1.set(date1);
		datcon3rec.frequency.set("DY");
		callDatcon32400();
		
		if(days == 1){		
			days1 = datcon3rec.freqFactor.toInt();

		}			
		else {
			days2 = datcon3rec.freqFactor.toInt();
		}
    }		
	
 }

protected void callDatcon21200()
 {
		/*START*/
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError99000();
		}
		/*EXIT*/
 }

protected void readT5645()
	{		
	
		itempf = new Itempf();
	
		itempf.setItempfx("IT");
		itempf.setItemcoy(annypyintcalcrec.chdrcoy.toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaSubr.trim());/* IJTI-1523 */
		itempf.setItemseq(null);
		itempf = itemDao.getItemRecordByItemkey(itempf);
	
		if (itempf == null) {
			syserrrec.params.set("IT".concat(annypyintcalcrec.chdrcoy.toString()).concat("TR386").concat(wsaaSubr));/* IJTI-1523 */
			databaseError99500();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
		
	}

protected void mainProcessing2000()
	{
		
		lastCapnAmtInt2100();	
		
		if(isEQ(annypyintcalcrec.transaction, "ANNUITY"))
		{
		acmvpfList = acmvpfDAO.loadDataByBatch(wsaaRldgacct.toString(), t5645rec.sacscode01.toString(), t5645rec.sacstype01.toString(), annypyintcalcrec.lastCaplsnDate.toInt(), annypyintcalcrec.chdrcoy.toString());
		}
		else if (isEQ(annypyintcalcrec.transaction, "ENDOWMENT"))
		{
			acmvpfList = acmvpfDAO.loadDataByBatch(wsaaRldgacct.toString(), t5645rec.sacscode02.toString(), t5645rec.sacstype02.toString(), annypyintcalcrec.lastCaplsnDate.toInt(), annypyintcalcrec.chdrcoy.toString());
		}
	
		if(acmvpfList != null && acmvpfList.size() > 0)
		{
			for(Acmvpf acmv : acmvpfList)
			{
				
				datcon3rec.datcon3Rec.set(SPACES);
				if (isLT(acmv.getEffdate(),annypyintcalcrec.interestFrom)) {
					datcon3rec.intDate1.set(annypyintcalcrec.interestFrom);
				}
				else {
					datcon3rec.intDate1.set(acmv.getEffdate());
				}
				datcon3rec.intDate2.set(annypyintcalcrec.interestTo);
				datcon3rec.frequency.set("DY");
				callDatcon32400();
				wsaaInterest1.set(ZERO);
				wsaaInterest2.set(ZERO);
				wsaaLoanCurrVal.set(ZERO);
				if (isNE(annypyintcalcrec.currency,acmv.getOrigcurr())) {
					currencyConvert2300();
				}
				else {
					wsaaLoanCurrVal.set(acmv.getOrigamt());
				}               
				
				compute(wsaaInterest1, 6).setRounded(mult(wsaaLoanCurrVal, sub((pow(((add(1,div(intRateFrom, 100))).toDouble()),div(days1, 365))),1)));				
				if(days2 > 0)
					compute(wsaaInterest1, 6).setRounded(mult(wsaaLoanCurrVal, sub((pow(((add(1,div(intRateTo, 100))).toDouble()),div(days2, 365))),1)));
				
				if(isEQ(annypyintcalcrec.transaction, "ANNUITY"))
				{
					if (isEQ(acmv.getGlsign(), t5645rec.sign01)) {
						wsaaTotalInterest.add(wsaaInterest1);
						wsaaTotalInterest.add(wsaaInterest2);
					} else {
						wsaaTotalInterest.subtract(wsaaInterest1);
						wsaaTotalInterest.subtract(wsaaInterest2);
					}
				}
				else if (isEQ(annypyintcalcrec.transaction, "ENDOWMENT"))
				{
					if (isEQ(acmv.getGlsign(), t5645rec.sign02)) {
						wsaaTotalInterest.add(wsaaInterest1);
						wsaaTotalInterest.add(wsaaInterest2);
					} else {
						wsaaTotalInterest.subtract(wsaaInterest1);
						wsaaTotalInterest.subtract(wsaaInterest2);
					}
				}
				
			}
		}
		
		annypyintcalcrec.interestAmount.set(wsaaTotalInterest);
	}
protected void lastCapnAmtInt2100()
	{
		/*START*/
		/*datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(annypyintcalcrec.interestFrom);
		datcon3rec.intDate2.set(annypyintcalcrec.interestTo);
		datcon3rec.frequency.set("DY");
		callDatcon32400();*/
		wsaaTotalInterest.set(ZERO);
		wsaaLoanPrinInt.set(ZERO);
		//compute(wsaaLoanPrinInt, 6).setRounded(mult(mult(annypyintcalcrec.loanorigam,(div(t6633rec.intRate,100))),(div(datcon3rec.freqFactor,365))));
		compute(wsaaLoanPrinInt, 6).setRounded(mult(annypyintcalcrec.annPaymt, sub((pow(((add(1,div(intRateFrom, 100))).toDouble()),div(days1, 365))),1)));
		wsaaTotalInterest.add(wsaaLoanPrinInt);
		
		if(days2 > 0) {
			wsaaLoanPrinInt.set(ZERO);		
			compute(wsaaLoanPrinInt, 6).setRounded(mult(annypyintcalcrec.annPaymt, sub((pow(((add(1,div(intRateTo, 100))).toDouble()),div(days2, 365))),1)));
			wsaaTotalInterest.add(wsaaLoanPrinInt);
		}
		/*EXIT*/
	}

protected void currencyConvert2300()
	{
		start2300();
	}

protected void start2300()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountIn.set(acmvlonIO.getOrigamt());
		conlinkrec.statuz.set(SPACES);
		conlinkrec.function.set("CVRT");
		conlinkrec.currIn.set(acmvlonIO.getOrigcurr());
		conlinkrec.cashdate.set(wsaaMaxDate);
		conlinkrec.currOut.set(annypyintcalcrec.currency);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(annypyintcalcrec.chdrcoy);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			systemError99000();
		}
		else {
			wsaaLoanCurrVal.set(conlinkrec.amountOut);
		}
	}

protected void callDatcon32400()
	{
		/*START*/
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError99000();
		}
		/*EXIT*/
	}

protected void systemError99000()
	{
		try {
			start99000();
		}
		catch (GOTOException e){
		}
		finally{
			exit99490();
		}
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		annypyintcalcrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		try {
			start99500();
		}
		catch (GOTOException e){
		}
		finally{
			exit99590();
		}
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		annypyintcalcrec.statuz.set(varcom.bomb);
		exit190();
	}
}
