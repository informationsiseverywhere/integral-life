package com.csc.life.contractservicing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CwfddelTableDAM.java
 * Date: Sun, 30 Aug 2009 03:36:20
 * Class transformed from CWFDDEL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CwfddelTableDAM extends CwfdpfTableDAM {

	public CwfddelTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CWFDDEL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANCD"
		             + ", EFDATE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANCD, " +
		            "TRANNO, " +
		            "EFDATE, " +
		            "PRCSEQNBR, " +
		            "BSCHEDNAM, " +
		            "BSJOBUSER, " +
		            "BSJOBJOB, " +
		            "BSJOBNO, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANCD ASC, " +
		            "EFDATE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANCD DESC, " +
		            "EFDATE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               trancd,
                               tranno,
                               efdate,
                               prcSeqNbr,
                               scheduleName,
                               jobnameUser,
                               jobnameJob,
                               jobnameNumber,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTrancd().toInternal()
					+ getEfdate().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, trancd);
			what = ExternalData.chop(what, efdate);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(trancd.toInternal());
	nonKeyFiller5.setInternal(efdate.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(104);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ getTranno().toInternal()
					+ nonKeyFiller5.toInternal()
					+ getPrcSeqNbr().toInternal()
					+ getScheduleName().toInternal()
					+ getJobnameUser().toInternal()
					+ getJobnameJob().toInternal()
					+ getJobnameNumber().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, prcSeqNbr);
			what = ExternalData.chop(what, scheduleName);
			what = ExternalData.chop(what, jobnameUser);
			what = ExternalData.chop(what, jobnameJob);
			what = ExternalData.chop(what, jobnameNumber);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getTrancd() {
		return trancd;
	}
	public void setTrancd(Object what) {
		trancd.set(what);
	}
	public PackedDecimalData getEfdate() {
		return efdate;
	}
	public void setEfdate(Object what) {
		setEfdate(what, false);
	}
	public void setEfdate(Object what, boolean rounded) {
		if (rounded)
			efdate.setRounded(what);
		else
			efdate.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getPrcSeqNbr() {
		return prcSeqNbr;
	}
	public void setPrcSeqNbr(Object what) {
		setPrcSeqNbr(what, false);
	}
	public void setPrcSeqNbr(Object what, boolean rounded) {
		if (rounded)
			prcSeqNbr.setRounded(what);
		else
			prcSeqNbr.set(what);
	}	
	public FixedLengthStringData getScheduleName() {
		return scheduleName;
	}
	public void setScheduleName(Object what) {
		scheduleName.set(what);
	}	
	public FixedLengthStringData getJobnameUser() {
		return jobnameUser;
	}
	public void setJobnameUser(Object what) {
		jobnameUser.set(what);
	}	
	public FixedLengthStringData getJobnameJob() {
		return jobnameJob;
	}
	public void setJobnameJob(Object what) {
		jobnameJob.set(what);
	}	
	public PackedDecimalData getJobnameNumber() {
		return jobnameNumber;
	}
	public void setJobnameNumber(Object what) {
		setJobnameNumber(what, false);
	}
	public void setJobnameNumber(Object what, boolean rounded) {
		if (rounded)
			jobnameNumber.setRounded(what);
		else
			jobnameNumber.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		trancd.clear();
		efdate.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		tranno.clear();
		nonKeyFiller5.clear();
		prcSeqNbr.clear();
		scheduleName.clear();
		jobnameUser.clear();
		jobnameJob.clear();
		jobnameNumber.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}