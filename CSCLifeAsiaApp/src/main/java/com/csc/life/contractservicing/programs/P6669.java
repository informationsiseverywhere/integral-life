/*
 * File: P6669.java
 * Date: 30 August 2009 0:50:15
 * Author: Quipoz Limited
 * 
 * Class transformed from P6669.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.GrpsTableDAM;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.screens.S6669ScreenVars;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  S6669/P6669 - Group Membership Details
*  --------------------------------------
*  Partial window displaying the group number, description  and
*  optionally   the  group  membership  number  via  which  the
*  contract is to be billed.
*
*  Billing changes that involve group billing can be either:
*     i) A contract that is within group billing which is now
*        being removed.
*    ii) A contract that is now being changed to be part of group
*        billing.
*   iii) A contract that is a member of one group is changed to
*        be a member of another group.
*
*  Initialise
*  ----------
*  Retrieve the necessary details using the RETRV  command  and
*  the CHDRMJA logical view.
*  The details are obtained as follows:-
*       Group number, GRUPKEY  - from the contract header,
*       Member number, MEMBSEL - from the contract header,
*       Group name, GRUPNAME   - from the Group master file
*                                (GRPS).
*
*  Validation
*  ----------
*  If EXIT is pressed, then skip the validation.
*  Windowing  on  the  group number field allows new details to
*  be created (automatically). This is invoked by pressing  the
*  Window Fkey and controlled externally to this program.
*  Validate  that  the  group number entered exists on the GRPS
*  file. If not found then display an error message E714.  This
*  file  has  an indicator (MEMBREQ), to say whether the member
*  number is mandatory or  not.  If  it  is  required  and  not
*  entered,  then  display  error  message  E978.  If it is not
*  required and a group number is entered, then  display  error
*  message E979.
*  Validate billing method against Table T3620.
*  If group billing is selected the group number entered has the
*  same billing currency as that of the contract.
*  If   REFRESH  is pressed then re-display  the screen by
*     setting WSSP-EDTERROR to 'Y'.
*
*  Updating
*  --------
*  If EXIT  is pressed, then skip the updating.
*  Update the contract header as follows:-
*        - if the group number has changed (blank is also
*          valid), then set the GRUPNUM on the contract header
*          to this new value,
*        - if the member number has changed (blank is also
*          valid), then set the MEMBSEL on the contract header
*          to this value,
*        - update the CHDR record using the KEEPS function.
*
*  Next Program
*  ------------
*  Add 1 to the program pointer and exit from the program.
*
*
*****************************************************************
* </pre>
*/
public class P6669 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6669");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaGroupKey = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaGrpcoy = new FixedLengthStringData(1).isAPartOf(wsaaGroupKey, 0);
	private FixedLengthStringData wsaaGrpnum = new FixedLengthStringData(8).isAPartOf(wsaaGroupKey, 1);

	private FixedLengthStringData wsaaOldGroupKey = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaOldGrpcoy = new FixedLengthStringData(1).isAPartOf(wsaaOldGroupKey, 0);
	private FixedLengthStringData wsaaOldGrpnum = new FixedLengthStringData(8).isAPartOf(wsaaOldGroupKey, 1);
	private FixedLengthStringData wsaaOldMembsel = new FixedLengthStringData(10);
	private static final String e714 = "E714";
	private static final String e978 = "E978";
	private static final String e979 = "E979";
	private static final String e679 = "E679";
	private static final String t086 = "T086";
		/* TABLES */
	private static final String t3620 = "T3620";
	private static final String payrrec = "PAYRREC   ";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private GrpsTableDAM grpsIO = new GrpsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private T3620rec t3620rec = new T3620rec();
	private Wssplife wssplife = new Wssplife();
	private S6669ScreenVars sv = ScreenProgram.getScreenVars( S6669ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public P6669() {
		super();
		screenVars = sv;
		new ScreenModel("S6669", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/*    Retrieve the payer details.                                  */
		payrIO.setDataArea(SPACES);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Set the screen fields*/
		/* MOVE CHDRMJA-GRUPKEY        TO WSAA-OLD-GROUP-KEY.           */
		/* MOVE WSAA-OLD-GRPNUM        TO S6669-GRUPNUM.                */
		/* MOVE CHDRMJA-MEMBSEL        TO S6669-MEMBSEL.                */
		wsaaOldGrpcoy.set(payrIO.getGrupcoy());
		wsaaOldGrpnum.set(payrIO.getGrupnum());
		sv.grupnum.set(payrIO.getGrupnum());
		wsaaOldMembsel.set(payrIO.getMembsel());
		sv.membsel.set(payrIO.getMembsel());
		sv.mplnum.set(chdrmjaIO.getMplnum());
		/* Get Group Name (GRUPNAME) from the group master file (GRPS) and*/
		/* move to screen*/
		grpsIO.setParams(SPACES);
		grpsIO.setGrupnum(sv.grupnum);
		grpsIO.setGrupcoy(wsspcomn.company);
		grpsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, grpsIO);
		if ((isNE(grpsIO.getStatuz(), varcom.oK))
		&& (isNE(grpsIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(grpsIO.getParams());
			fatalError600();
		}
		/* If the group number is not found, then display error message*/
		/* E714*/
		if ((isNE(sv.grupnum, SPACES))
		&& (isEQ(grpsIO.getStatuz(), varcom.mrnf))) {
			sv.grupnumErr.set(e714);
		}
		else {
			sv.grupname.set(grpsIO.getGrupname());
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6669IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S6669-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		/* If EXIT is pressed, then skip the validation*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/* Check that the group number entered on the screen exists*/
		/* on the GRPS file*/
		grpsIO.setGrupnum(sv.grupnum);
		grpsIO.setGrupcoy(wsspcomn.company);
		grpsIO.setFunction("READR");
		SmartFileCode.execute(appVars, grpsIO);
		if ((isNE(grpsIO.getStatuz(), varcom.oK))
		&& (isNE(grpsIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(grpsIO.getParams());
			fatalError600();
		}
		/* Check Group Number entered is not a deleted Group, if           */
		/* so display error message - 'Cannot attach to del Grp'.          */
		if (isEQ(grpsIO.getValidflag(), "2")) {
			sv.grupnumErr.set(t086);
			goTo(GotoLabel.checkForErrors2080);
		}
		/* If the group number entered on the screen is not found,*/
		/* then display error message E714*/
		if (isEQ(grpsIO.getStatuz(), varcom.mrnf)) {
			sv.grupnumErr.set(e714);
			goTo(GotoLabel.checkForErrors2080);
		}
		/* If MEMBREQ NOT = SPACES, i.e. the member number is mandatory, an*/
		/* the member number has not been entered - display error*/
		/* message E978*/
		/* IF (GRPS-MEMBREQ            = 'Y') AND                       */
		if ((isNE(grpsIO.getMembreq(), SPACES))
		&& (isEQ(sv.membsel, SPACES))) {
			sv.membselErr.set(e978);
			goTo(GotoLabel.checkForErrors2080);
		}
		/* If MEMBREQ = SPACES, i.e. the member number is not mandatory,*/
		/* and the member number has been entered - display error*/
		/* message E979*/
		/* IF (GRPS-MEMBREQ        NOT = 'Y') AND                       */
		if ((isEQ(grpsIO.getMembreq(), SPACES))
		&& (isNE(sv.membsel, SPACES))) {
			sv.membselErr.set(e979);
			goTo(GotoLabel.checkForErrors2080);
		}
		/* IF GRPS-MEMBREQ         NOT = 'Y'                            */
		/*    GO TO 2080-CHECK-FOR-ERRORS.                              */
		/*    Validate Members Method of Payment against Table T3620       */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t3620);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(payrIO.getBillchnl());
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
		/*    Check that if group billing is selected that the             */
		/*    billing currency on the group is the same on the             */
		/*    contract.                                                    */
		/*    Otherwise display error - 'Currency Invalid'.                */
		if (isEQ(payrIO.getBillchnl(), "G")) {
			if (isNE(grpsIO.getCurrcode(), payrIO.getBillcurr())) {
				sv.grupnumErr.set(e679);
			}
		}
		/* If member number is required - check that it is not in use*/
		/* by another contract*/
		/* Use CHDRGRP*/
		/* MOVE SPACES                 TO CHDRGRP-DATA-AREA.            */
		/* MOVE CHDRMJA-CHDRCOY        TO CHDRGRP-CHDRCOY.              */
		/* MOVE WSSP-COMPANY           TO WSAA-GRPCOY.                  */
		/* MOVE S6669-GRUPNUM          TO WSAA-GRPNUM.                  */
		/* MOVE WSAA-GROUP-KEY         TO CHDRGRP-GRUPKEY.              */
		/* MOVE S6669-MEMBSEL          TO CHDRGRP-MEMBSEL.              */
		/* MOVE CHDRGRPREC             TO CHDRGRP-FORMAT.               */
		/* MOVE READR                  TO CHDRGRP-FUNCTION.             */
		/* CALL 'CHDRGRPIO'            USING CHDRGRP-PARAMS.            */
		/* IF (CHDRGRP-STATUZ      NOT = O-K) AND                       */
		/*    (CHDRGRP-STATUZ      NOT = MRNF)                          */
		/*    MOVE CHDRGRP-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* IF CHDRGRP-STATUZ           = O-K                            */
		/*    MOVE EV04                TO S6669-MEMBSEL-ERR             */
		/*    GO TO 2080-CHECK-FOR-ERRORS.                              */
		/* If F9 is pressed, redisplay the screen*/
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/* If EXIT is pressed, then skip the updating*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*  Update the contract header*/
		wsaaGrpcoy.set(wsspcomn.company);
		wsaaGrpnum.set(sv.grupnum);
		/* If the group number has changed, then set the GRUPNUM on*/
		/* the contract header to this new value*/
		if (isNE(wsaaOldGrpnum, sv.grupnum)) {
			chdrmjaIO.setGrupkey(wsaaGroupKey);
		}
		else {
			chdrmjaIO.setGrupkey(wsaaOldGroupKey);
		}
		/* If the member number has changed, then set the MEMBSEL on*/
		/* the contract header to this value*/
		if (isNE(wsaaOldMembsel, sv.membsel)) {
			chdrmjaIO.setMembsel(sv.membsel);
		}
		else {
			chdrmjaIO.setMembsel(wsaaOldMembsel);
		}
		/* Update the CHDR record using the KEEPS function*/
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if ((isNE(chdrmjaIO.getStatuz(), varcom.oK))
		&& (isNE(chdrmjaIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/* Update the group key on the payer record.                       */
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		payrIO.setGrupcoy(wsspcomn.company);
		payrIO.setGrupnum(sv.grupnum);
		payrIO.setMembsel(sv.membsel);
		payrIO.setBillnet(grpsIO.getBillnet());
		payrIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
