package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RskppfDAO extends BaseDAO<Rskppf>{
	public List<Rskppf> fetchRecord(String chdrcoy, String chdrnum, int date);
	public boolean insertRecords(List<Rskppf> rskppfList);
	public boolean updateRecords(List<Rskppf> rskppfList);
	public Map<String, List<Rskppf>> searchRskppf(String coy, List<String> chdrnumList);
	public boolean checkCoverage(String chdrcoy, String chdrnum, String life, String coverage, String rider);
	public void updateRiskPremium(Map<String, List<Rskppf>> covrpfMapList);
	public List<Rskppf> fetchRecordReversal(String chdrcoy, String chdrnum, int date);
	public boolean updateRiskPrem(List<Rskppf> rskppfList);
	public List<Rskppf> fetchRskppfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,int date);
	public void reverseContract(String chdrcoy, String chdrnum);
	public Map<String,List<Rskppf>> searchRskpMap(String coy, List<String> chdrnumList);
}

