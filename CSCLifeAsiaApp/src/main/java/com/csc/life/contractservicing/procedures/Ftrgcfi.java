/*
 * File: Ftrgcfi.java
 * Date: December 3, 2013 2:28:53 AM ICT
 * Author: CSC
 * 
 * Class transformed from FTRGCFI.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.tablestructures.T3698rec;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.FcfiTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrclmTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrrnlTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnextTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        FTRGCFI - Freelook Cancellation Trigger module
*
*    This new unit deal trigger module is created to update the
*    relevant sub-account.
*
*    It will check whether Actual value + Suspense is greater than
*    the refund charges, if any.
*    If there is any shortfall, then the auto adjustment will be
*    conducted automatically base on the client's deduction amount 
*    The adjustment will be conducted in the following order:
*    - FCFI-ADJAMT02, if any
*    - FCFI-ADMAMT02
*    - FCFI-MEDAMT02, if any
*
*    The actual deduction then will post accordingly.
*
*    Only when there is refund value, program will transfer all th 
*    cancellation refund amounts from cancellation outstanding
*    account into the payment suspense:
*
*    DR  LC/CO (proceeds from online part)
*    DR  LE/CO (proceeds from unit deal part)
*
*    CR  LP/PS (payment suspense)
*
****************************************************************** ****
* </pre>
*/
public class Ftrgcfi extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "FTRGCFI";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUtrnHitr = "";
	private FixedLengthStringData wsaaT5645Desc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMedamt = new PackedDecimalData(12, 2);
	private PackedDecimalData wsaaAdmamt = new PackedDecimalData(12, 2);
	private PackedDecimalData wsaaAdjamt = new PackedDecimalData(12, 2);
	private PackedDecimalData wsaaShortfall = new PackedDecimalData(12, 2);
	private PackedDecimalData wsaaTotDeduct = new PackedDecimalData(12, 2);
	private PackedDecimalData wsaaPayOut = new PackedDecimalData(12, 2);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaT3698Key = new FixedLengthStringData(6);
		/* WSAA-T3698-GL */
	private FixedLengthStringData wsaaT3698Sacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT3698Sacstyp = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaT3698Contot = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaT3698Glcode = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaT3698Glsign = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaFound = new FixedLengthStringData(1);
	private Validator found = new Validator(wsaaFound, "Y");
	private Validator notFound = new Validator(wsaaFound, "N");
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t1688 = "T1688";
	private static final String t3698 = "T3698";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FcfiTableDAM fcfiIO = new FcfiTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitrclmTableDAM hitrclmIO = new HitrclmTableDAM();
	private HitrrnlTableDAM hitrrnlIO = new HitrrnlTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrnextTableDAM utrnextIO = new UtrnextTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T3698rec t3698rec = new T3698rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Udtrigrec udtrigrec = new Udtrigrec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		process070, 
		exit090, 
		deductionPosting3030, 
		call3220, 
		errorProg9020
	}

	public Ftrgcfi() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin010();
				case process070: 
					process070();
				case exit090: 
					exit090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin010()
	{
		udtrigrec.statuz.set(varcom.oK);
		wsaaMedamt.set(ZERO);
		wsaaAdmamt.set(ZERO);
		wsaaAdjamt.set(ZERO);
		wsaaShortfall.set(ZERO);
		wsaaTotDeduct.set(ZERO);
		wsaaPayOut.set(ZERO);
		wsaaT3698Contot.set(ZERO);
		wsaaToday.set(varcom.vrcmMaxDate);
		updateFcfi500();
		checkUtrnHitr800();
		if (isEQ(wsaaUtrnHitr, "Y")) {
			goTo(GotoLabel.exit090);
		}
		compute(wsaaPayOut, 2).set(sub(add(fcfiIO.getSusamt(), fcfiIO.getActval()), wsaaTotDeduct));
		if (isGTE(wsaaPayOut, 0)) {
			goTo(GotoLabel.process070);
		}
		else {
			wsaaPayOut.set(ZERO);
		}
		compute(wsaaShortfall, 2).set(sub(sub(wsaaTotDeduct, fcfiIO.getSusamt()), fcfiIO.getActval()));
		if (isLTE(wsaaShortfall, 0)) {
			goTo(GotoLabel.process070);
		}
		/*  If deduction amount is more than the refund amount, then we nee*/
		/*  to adjust the deduction by the following sequence accordingly:*/
		/*  - Adjuest amount, if any, and > 0*/
		/*  - Admin Charge, if any*/
		/*  - Medical fee, if any*/
		if (isGT(wsaaAdjamt, 0)) {
			if (isGT(wsaaAdjamt, wsaaShortfall)) {
				compute(wsaaAdjamt, 2).set(sub(wsaaAdjamt, wsaaShortfall));
				wsaaShortfall.set(ZERO);
				goTo(GotoLabel.process070);
			}
			else {
				compute(wsaaShortfall, 2).set(sub(wsaaShortfall, wsaaAdjamt));
				wsaaAdjamt.set(ZERO);
			}
		}
		if (isGT(wsaaAdmamt, 0)) {
			if (isGT(wsaaAdmamt, wsaaShortfall)) {
				compute(wsaaAdmamt, 2).set(sub(wsaaAdmamt, wsaaShortfall));
				wsaaShortfall.set(ZERO);
				goTo(GotoLabel.process070);
			}
			else {
				compute(wsaaShortfall, 2).set(sub(wsaaShortfall, wsaaAdmamt));
				wsaaAdmamt.set(ZERO);
			}
		}
		if (isGT(wsaaMedamt, 0)) {
			if (isGT(wsaaMedamt, wsaaShortfall)) {
				compute(wsaaAdmamt, 2).set(sub(wsaaMedamt, wsaaShortfall));
				wsaaShortfall.set(ZERO);
				goTo(GotoLabel.process070);
			}
			else {
				compute(wsaaShortfall, 2).set(sub(wsaaShortfall, wsaaMedamt));
				wsaaMedamt.set(ZERO);
			}
		}
	}

protected void process070()
	{
		initialize1000();
		openBatch2000();
		readT16882100();
		checkT36982300();
		postRefund3000();
		updateFcfi3800();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void updateFcfi500()
	{
		begin501();
	}

protected void begin501()
	{
		utrnIO.setParams(SPACES);
		utrnIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setTranno(udtrigrec.tranno);
		utrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)
		&& isNE(utrnIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(utrnIO.getStatuz(), varcom.mrnf)) {
			hitrclmIO.setParams(SPACES);
			hitrclmIO.setChdrcoy(udtrigrec.chdrcoy);
			hitrclmIO.setChdrnum(udtrigrec.chdrnum);
			hitrclmIO.setLife(udtrigrec.life);
			hitrclmIO.setCoverage(udtrigrec.coverage);
			hitrclmIO.setRider(udtrigrec.rider);
			hitrclmIO.setPlanSuffix(udtrigrec.planSuffix);
			hitrclmIO.setZintbfnd(udtrigrec.unitVirtualFund);
			hitrclmIO.setTranno(udtrigrec.tranno);
			hitrclmIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, hitrclmIO);
			if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(hitrclmIO.getParams());
				syserrrec.statuz.set(hitrclmIO.getStatuz());
				fatalError9000();
			}
			hitrFcfi510();
		}
		else {
			utrnFcfi520();
		}
		wsaaMedamt.set(fcfiIO.getMedamt02());
		wsaaAdmamt.set(fcfiIO.getAdmamt02());
		wsaaAdjamt.set(fcfiIO.getAdjamt02());
		compute(wsaaTotDeduct, 2).set(add(add(wsaaMedamt, wsaaAdmamt), wsaaAdjamt));
	}

protected void hitrFcfi510()
	{
		begin511();
	}

protected void begin511()
	{
		if (isNE(hitrclmIO.getContractAmount(), ZERO)) {
			fcfiIO.setParams(SPACES);
			fcfiIO.setChdrcoy(udtrigrec.chdrcoy);
			fcfiIO.setChdrnum(udtrigrec.chdrnum);
			fcfiIO.setTranno(udtrigrec.tranno);
			fcfiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, fcfiIO);
			if (isNE(fcfiIO.getStatuz(), varcom.oK)
			&& isNE(fcfiIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(fcfiIO.getParams());
				syserrrec.statuz.set(fcfiIO.getStatuz());
				fatalError9000();
			}
			compute(wsaaAmount, 2).set(mult(hitrclmIO.getContractAmount(), (-1)));
			setPrecision(fcfiIO.getActval(), 2);
			fcfiIO.setActval(add(fcfiIO.getActval(), wsaaAmount));
			fcfiIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, fcfiIO);
			if (isNE(fcfiIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fcfiIO.getParams());
				syserrrec.statuz.set(fcfiIO.getStatuz());
				fatalError9000();
			}
		}
	}

protected void utrnFcfi520()
	{
		begin521();
	}

protected void begin521()
	{
		if (isNE(utrnIO.getContractAmount(), ZERO)) {
			fcfiIO.setParams(SPACES);
			fcfiIO.setChdrcoy(udtrigrec.chdrcoy);
			fcfiIO.setChdrnum(udtrigrec.chdrnum);
			fcfiIO.setTranno(udtrigrec.tranno);
			fcfiIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, fcfiIO);
			if (isNE(fcfiIO.getStatuz(), varcom.oK)
			&& isNE(fcfiIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(fcfiIO.getParams());
				syserrrec.statuz.set(fcfiIO.getStatuz());
				fatalError9000();
			}
			compute(wsaaAmount, 2).set(mult(utrnIO.getContractAmount(), (-1)));
			setPrecision(fcfiIO.getActval(), 2);
			fcfiIO.setActval(add(fcfiIO.getActval(), wsaaAmount));
			fcfiIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, fcfiIO);
			if (isNE(fcfiIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fcfiIO.getParams());
				syserrrec.statuz.set(fcfiIO.getStatuz());
				fatalError9000();
			}
		}
	}

protected void checkUtrnHitr800()
	{
		begin801();
	}

protected void begin801()
	{
		/* Check whether there is un-processed UTRN or HITR.*/
		wsaaUtrnHitr = "N";
		utrnextIO.setParams(SPACES);
		utrnextIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnextIO.setChdrnum(udtrigrec.chdrnum);
		utrnextIO.setProcSeqNo(ZERO);
		utrnextIO.setFormat(formatsInner.utrnextrec);
		utrnextIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, utrnextIO);
		if (isNE(utrnextIO.getStatuz(), varcom.oK)
		&& isNE(utrnextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrnextIO.getParams());
			syserrrec.statuz.set(utrnextIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(utrnextIO.getStatuz(), varcom.oK)
		&& isEQ(utrnextIO.getChdrcoy(), udtrigrec.chdrcoy)
		&& isEQ(utrnextIO.getChdrnum(), udtrigrec.chdrnum)) {
			wsaaUtrnHitr = "Y";
			return ;
		}
		hitrrnlIO.setParams(SPACES);
		hitrrnlIO.setChdrcoy(udtrigrec.chdrcoy);
		hitrrnlIO.setChdrnum(udtrigrec.chdrnum);
		hitrrnlIO.setFormat(formatsInner.hitrrnlrec);
		hitrrnlIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitrrnlIO);
		if (isNE(hitrrnlIO.getStatuz(), varcom.oK)
		&& isNE(hitrrnlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitrrnlIO.getParams());
			syserrrec.statuz.set(hitrrnlIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(hitrrnlIO.getStatuz(), varcom.oK)
		&& isEQ(hitrrnlIO.getChdrcoy(), udtrigrec.chdrcoy)
		&& isEQ(hitrrnlIO.getChdrnum(), udtrigrec.chdrnum)) {
			wsaaUtrnHitr = "Y";
		}
	}

protected void initialize1000()
	{
		/*BEGIN*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		readChdrlnb1200();
		readT56451300();
		readT56881400();
		readFcfi1500();
		/*EXIT*/
	}

protected void readChdrlnb1200()
	{
		/*BEGIN*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(udtrigrec.chdrcoy);
		chdrlnbIO.setChdrnum(udtrigrec.chdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void readT56451300()
	{
		begin1310();
	}

protected void begin1310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(udtrigrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(udtrigrec.chdrcoy);
		descIO.setLanguage(udtrigrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			/* Nothing to do. */
		}
	}

protected void readT56881400()
	{
		begin1410();
	}

protected void begin1410()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(udtrigrec.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), udtrigrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
			return ;
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void readFcfi1500()
	{
		/*BEGIN*/
		fcfiIO.setParams(SPACES);
		fcfiIO.setChdrcoy(udtrigrec.chdrcoy);
		fcfiIO.setChdrnum(udtrigrec.chdrnum);
		fcfiIO.setTranno(udtrigrec.tranno);
		fcfiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fcfiIO);
		if (isNE(fcfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fcfiIO.getParams());
			syserrrec.statuz.set(fcfiIO.getStatuz());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void openBatch2000()
	{
		begin2010();
	}

protected void begin2010()
	{
		wsaaBatckey.set(udtrigrec.batchkey);
		utrnIO.setParams(SPACES);
		utrnIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setTranno(udtrigrec.tranno);
		utrnIO.setFunction("READR");
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)
		&& isNE(utrnIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(utrnIO.getStatuz(), varcom.mrnf)) {
			hitrIO.setParams(SPACES);
			hitrIO.setChdrcoy(udtrigrec.chdrcoy);
			hitrIO.setChdrnum(udtrigrec.chdrnum);
			hitrIO.setLife(udtrigrec.life);
			hitrIO.setCoverage(udtrigrec.coverage);
			hitrIO.setRider(udtrigrec.rider);
			hitrIO.setPlanSuffix(udtrigrec.planSuffix);
			hitrIO.setZintbfnd(udtrigrec.unitVirtualFund);
			hitrIO.setTranno(udtrigrec.tranno);
			hitrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, hitrIO);
			if (isNE(hitrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(hitrIO.getParams());
				syserrrec.statuz.set(hitrIO.getStatuz());
				fatalError9000();
			}
			wsaaBatckey.batcBatctrcde.set(hitrIO.getBatctrcde());
		}
		else {
			wsaaBatckey.batcBatctrcde.set(utrnIO.getBatctrcde());
		}
		/* Open a financial batching.*/
		batcdorrec.batcdorRec.set(SPACES);
		batcdorrec.function.set("AUTO");
		batcdorrec.batchkey.set(wsaaBatckey);
		batcdorrec.prefix.set("BA");
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		wsaaBatckey.set(batcdorrec.batchkey);
	}

protected void readT16882100()
	{
		begin2110();
	}

protected void begin2110()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(udtrigrec.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(udtrigrec.language);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError9000();
		}
		wsaaDesc.set(descIO.getLongdesc());
	}

protected void checkT36982300()
	{
		start2310();
	}

protected void start2310()
	{
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(udtrigrec.chdrcoy);
		itemIO.setItemtabl(t3698);
		wsaaT3698Key.set(SPACES);
		wsaaT3698Key.setSub1String(1, 4, wsaaBatckey.batcBatctrcde);
		wsaaT3698Key.setSub1String(5, 2, fcfiIO.getSacscode02());
		itemIO.setItemitem(wsaaT3698Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		else {
			t3698rec.t3698Rec.set(itemIO.getGenarea());
		}
		wsaaFound.set("N");
		for (ix.set(1); !(isEQ(t3698rec.sacstyp[ix.toInt()], SPACES)
		|| found.isTrue()); ix.add(1)){
			if (isEQ(t3698rec.sacstyp[ix.toInt()], fcfiIO.getSacstyp02())) {
				wsaaFound.set("Y");
				wsaaT3698Sacscode.set(fcfiIO.getSacscode02());
				wsaaT3698Sacstyp.set(t3698rec.sacstyp[ix.toInt()]);
				wsaaT3698Contot.set(t3698rec.cnttot[ix.toInt()]);
				wsaaT3698Glcode.set(t3698rec.glmap[ix.toInt()]);
				wsaaT3698Glsign.set(t3698rec.sign[ix.toInt()]);
			}
		}
		if (notFound.isTrue()) {
			return ;
		}
	}

protected void postRefund3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin3010();
				case deductionPosting3030: 
					deductionPosting3030();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3010()
	{
		wsaaJrnseq.set(0);
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.tranno.set(fcfiIO.getTranno());
		lifacmvrec.genlcoy.set(fcfiIO.getChdrcoy());
		lifacmvrec.rldgcoy.set(fcfiIO.getChdrcoy());
		lifacmvrec.rldgacct.set(fcfiIO.getChdrnum());
		lifacmvrec.rdocnum.set(fcfiIO.getChdrnum());
		lifacmvrec.tranref.set(fcfiIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.trandesc.set(wsaaDesc);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.user.set(udtrigrec.user);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		lifacmvrec.transactionTime.set(getCobolTime());
		lifacmvrec.transactionDate.set(getCobolDate());
		if (isLTE(wsaaPayOut, 0)) {
			goTo(GotoLabel.deductionPosting3030);
		}
		/* Transfer the cancellation outstanding proceeds from online,(and)*/
		/* unit deal by debiting LC/CO*/
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		//MIBT-266 STARTS
		//if (isEQ(t5688rec.comlvlacc, "N")) {
		if (isEQ(t5688rec.comlvlacc, " ")) {
//			MIBT-266 ENDS
			compute(lifacmvrec.origamt, 2).set(add(fcfiIO.getSusamt(), fcfiIO.getActval()));
		}
		else {
			lifacmvrec.origamt.set(fcfiIO.getSusamt());
		}
		wsaaIx.set(1);
		callLifacmv3500();
		/* Transfer ther cancellation outstanding proceeds from unit deal*/
		/* part by debiting LE/CO*/
		//if (isEQ(t5688rec.comlvlacc, "N")) {//COMMENTED MIBT-266
				if (isEQ(t5688rec.comlvlacc, " ")) {
			goTo(GotoLabel.deductionPosting3030);
		}
		reverseLeCoPostings3200();
	}

protected void deductionPosting3030()
	{
		/*  Credit LP/ME to clawback medical bill expense from client:*/
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(wsaaMedamt);
		wsaaIx.set(4);
		callLifacmv3500();
		/*  Credit LP/CG to clawback administration charges from client:*/
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(wsaaAdmamt);
		wsaaIx.set(5);
		callLifacmv3500();
		/*  Credit LP/xx (depending on sub account type entered) to clawbac*/
		/*  other charges from client:*/
		if (isNE(wsaaAdjamt, ZERO)) {
			lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
			lifacmvrec.origamt.set(wsaaAdjamt);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.sacscode.set(wsaaT3698Sacscode);
			lifacmvrec.sacstyp.set(wsaaT3698Sacstyp);
			lifacmvrec.contot.set(wsaaT3698Contot);
			lifacmvrec.glcode.set(wsaaT3698Glcode);
			lifacmvrec.glsign.set(wsaaT3698Glsign);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError9000();
			}
		}
		/* Post the total refund payable to payment suspense account by*/
		/* crediting LP/PS*/
		lifacmvrec.origamt.set(wsaaPayOut);
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.user.set(udtrigrec.user);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		lifacmvrec.tranref.set(SPACES);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.tranref.set(fcfiIO.getChdrnum());
		lifacmvrec.rldgacct.set(fcfiIO.getChdrnum());
		wsaaIx.set(3);
		callLifacmv3500();
	}

protected void reverseLeCoPostings3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin3210();
				case call3220: 
					call3220();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3210()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(fcfiIO.getChdrcoy());
		acmvrevIO.setRdocnum(fcfiIO.getChdrnum());
		acmvrevIO.setTranno(fcfiIO.getTranno());
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
	}

protected void call3220()
	{
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError9000();
		}
		if (isNE(fcfiIO.getChdrcoy(), acmvrevIO.getRldgcoy())
		|| isNE(fcfiIO.getChdrnum(), acmvrevIO.getRdocnum())
		|| isNE(fcfiIO.getTranno(), acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(acmvrevIO.getSacscode(), t5645rec.sacscode[2])
		&& isEQ(acmvrevIO.getSacstyp(), t5645rec.sacstype[2])
		&& isNE(acmvrevIO.getBatctrcde(), wsaaBatckey.batcBatctrcde)) {
			wsaaJrnseq.add(1);
			lifacmvrec.tranref.set(acmvrevIO.getTranref());
			lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
			lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
			lifacmvrec.glcode.set(acmvrevIO.getGlcode());
			lifacmvrec.glsign.set(acmvrevIO.getGlsign());
			lifacmvrec.trandesc.set(wsaaDesc);
			lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
			lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), (-1)));
			compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), (-1)));
			lifacmvrec.crate.set(acmvrevIO.getCrate());
			lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
			lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError9000();
			}
		}
		acmvrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call3220);
	}

protected void callLifacmv3500()
	{
		begin3510();
	}

protected void begin3510()
	{
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.genlcur.set(SPACES);
		if (isEQ(lifacmvrec.origamt, 0)) {
			return ;
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaIx.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaIx.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaIx.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaIx.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaIx.toInt()]);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError9000();
		}
	}

protected void updateFcfi3800()
	{
		/*BEGIN*/
		fcfiIO.setMedamt02(wsaaMedamt);
		fcfiIO.setAdmamt02(wsaaAdmamt);
		fcfiIO.setAdjamt02(wsaaAdjamt);
		fcfiIO.setZtotamt(wsaaPayOut);
		fcfiIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fcfiIO);
		if (isNE(fcfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fcfiIO.getParams());
			syserrrec.statuz.set(fcfiIO.getStatuz());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fatalError9010();
				case errorProg9020: 
					errorProg9020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError9010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg9020);
		}
		syserrrec.subrname.set(wsaaSubr);
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9020()
	{
		udtrigrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit9090()
	{
		exitProgram();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hitrrnlrec = new FixedLengthStringData(10).init("HITRRNLREC");
	private FixedLengthStringData utrnextrec = new FixedLengthStringData(10).init("UTRNEXTREC");
}
}
