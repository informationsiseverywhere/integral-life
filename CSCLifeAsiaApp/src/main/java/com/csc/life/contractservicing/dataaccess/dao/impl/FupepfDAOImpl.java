package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.FupepfDAO;
import com.csc.life.contractservicing.dataaccess.model.Fupepf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class FupepfDAOImpl extends BaseDAOImpl<Fupepf> implements FupepfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(FupepfDAOImpl.class);
	
	public List<Fupepf> readRecord(String chdrcoy, String chdrnum, int fupno){
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"select * From FUPEPF where CHDRCOY=? and CHDRNUM=? and FUPNO=? order by DOCSEQ ASC");
		List<Fupepf> list = new ArrayList<>();
		Fupepf fupepfRec = null;
		
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		  ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setInt(3, fupno);
			ResultSet rs = null;
				rs = ps.executeQuery();
			    while (rs.next()) {		
			    fupepfRec = new Fupepf();
			    fupepfRec.setMessage(rs.getString("MESSAGE"));
			    list.add(fupepfRec);
			    }
		}
			catch(SQLException e)
			{
				LOGGER.error(" FupepfDAOImpl.readRecord", e);//IJTI-1561
				throw new SQLRuntimeException(e);	
			}
			return list;
	}
	public void deleteRecords(String chdrcoy, String chdrnum, int fupno){
		StringBuilder sql_fupepf_delete = new StringBuilder("");
		sql_fupepf_delete.append("DELETE FROM FUPEPF  ");
		sql_fupepf_delete.append("WHERE CHDRCOY='"+chdrcoy+"'");
		sql_fupepf_delete.append("AND CHDRNUM='"+chdrnum+"'");
		sql_fupepf_delete.append("AND FUPNO='"+fupno+"'");
		
		PreparedStatement preparedStmt = getPrepareStatement(sql_fupepf_delete.toString());
        int count;
        try{
        	count = preparedStmt.executeUpdate();
        }catch (SQLException e) {
            LOGGER.error("deleteRecords()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
        	close(preparedStmt,null);
        }
	}
	public void insertRecord(Fupepf fupepf){
		if(this.isCheckDuplicatedFupepf(fupepf)){
			return;
		}
		StringBuilder sql_fupepf_insert = new StringBuilder();
		sql_fupepf_insert.append("INSERT INTO FUPEPF");
		sql_fupepf_insert.append("(CHDRCOY,CHDRNUM,FUPNO,CLAMNUM,TRANNO,DOCSEQ,MESSAGE,USRPRF,JOBNM,DATIME)");
		sql_fupepf_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?)");
		PreparedStatement psInsert = getPrepareStatement(sql_fupepf_insert.toString());
		try
		{
				psInsert.setString(1, fupepf.getChdrcoy());
				psInsert.setString(2, fupepf.getChdrnum());
				psInsert.setInt(3, fupepf.getFupno());
				psInsert.setString(4, fupepf.getClamnum());
				psInsert.setInt(5, fupepf.getTranno());
				psInsert.setString(6, fupepf.getDocseq());
				psInsert.setString(7, fupepf.getMessage());
				psInsert.setString(8, this.getUsrprf());
				psInsert.setString(9, this.getJobnm());
				psInsert.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
				psInsert.addBatch();
				int[] rowCount=psInsert.executeBatch();	
		}
		catch (SQLException e) {
            LOGGER.error("insertExclpfRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psInsert, null);
        }
	}
	
	public List<Fupepf> readRecord(Fupepf fupepf){
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"SELECT MESSAGE FROM FUPEPF WHERE CHDRCOY=? AND CHDRNUM=? AND FUPNO=?  AND TRANNO = ? AND CLAMNUM = ? ORDER BY DOCSEQ ASC");
		List<Fupepf> list = new ArrayList<>();
		Fupepf fupepfRec = null;
		
		try(PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString())) {		  	
		  	ps.setString(1, fupepf.getChdrcoy());
			ps.setString(2, fupepf.getChdrnum());
			ps.setInt(3, fupepf.getFupno());
			ps.setInt(4, fupepf.getTranno());
			ps.setString(5, fupepf.getClamnum());
			try(ResultSet rs = ps.executeQuery()){
				while (rs.next()) {		
					fupepfRec = new Fupepf();
					fupepfRec.setMessage(rs.getString("MESSAGE"));
				    list.add(fupepfRec);
				}
			}
		}
		catch(SQLException e){				
				throw new SQLRuntimeException(e);	
		}
			return list;
	}
	private boolean isCheckDuplicatedFupepf(Fupepf fupepf){
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"SELECT COUNT(*) FROM FUPEPF WHERE CHDRCOY=? AND CHDRNUM=? AND FUPNO=?  AND TRANNO = ? AND DOCSEQ = ? ");
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int count = 0;
		try {
			stmt = getPrepareStatement(sqlSchemeSelect1.toString()); 
			stmt.setString(1, fupepf.getChdrcoy());
			stmt.setString(2, fupepf.getChdrnum());
			stmt.setInt(3, fupepf.getFupno());
			stmt.setInt(4, fupepf.getTranno());
			stmt.setString(5, fupepf.getDocseq());

			rs = stmt.executeQuery();

			while (rs.next()) {
				count = rs.getInt(1);

			}
		} catch (SQLException e) {
			LOGGER.error("isCheckDuplicatedFupepf", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}
		if (count > 0) {
			return true;

		}
		return false;
	}
}

