/*
 * File: P6257.java
 * Date: 30 August 2009 0:39:48
 * Author: Quipoz Limited
 * 
 * Class transformed from P6257.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.screens.S6257ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcchk;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This is a subsystem to be called after 'WINDBACK' and allows
* the user to advance forward to a specified point in a
* controlled on-line manner.
*
* Check the contract status against table T5679.
*
*****************************************************************
* </pre>
*/
public class P6257 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6257");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* Program variables.*/
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).setUnsigned();
	private String wsaaValidStatus = "";
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e132 = "E132";
	private static final String e717 = "E717";
	private static final String f259 = "F259";
	private static final String g667 = "G667";
	private static final String f910 = "F910";
	private static final String h142 = "H142";
	private static final String h085 = "H085";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcchkrec batcchkrec = new Batcchkrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S6257ScreenVars sv = ScreenProgram.getScreenVars( S6257ScreenVars.class);
	
	private ItemDAO itemDao =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = new Itempf();



	public P6257() {
		super();
		screenVars = sv;
		new ScreenModel("S6257", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		wsaaValidStatus = "N";
		varcom.vrcmTranid.set(wsspcomn.tranid);
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
	    wsspcomn.edterror.set(varcom.oK);
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			validate2200();
			return ;
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			validate2200();
			return ;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		
		if (isEQ(scrnparams.statuz, "BACH")) {
			if (isNE(subprogrec.bchrqd, "Y")) {
				sv.actionErr.set(e073);
				wsspcomn.edterror.set("Y");
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*        GO TO 2900-EXIT.                                         */
				return ;
			}
			bcbprogrec.transcd.set(subprogrec.transcd);		
			bcbprogrec.company.set(wsspcomn.company);
			callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
			if (isNE(bcbprogrec.statuz, varcom.oK)) {
				sv.actionErr.set(bcbprogrec.statuz);
				wsspcomn.edterror.set("Y");
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*        GO TO 2900-EXIT.                                         */
				return ;
			}
			wsspcomn.secProg[1].set(bcbprogrec.nxtprog1);
			wsspcomn.secProg[2].set(bcbprogrec.nxtprog2);
			wsspcomn.secProg[3].set(bcbprogrec.nxtprog3);
			wsspcomn.secProg[4].set(bcbprogrec.nxtprog4);
		}
		
		validate2200();
		return ;
	  
	}


protected void validate2200()
	{
		/*    Validate fields*/
		if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(g667);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/* Read the contract header details.*/
		chdrlifIO.setParams(SPACES);
		/* MOVE WSSP-FSUCO             TO CHDRLIF-CHDRCOY.              */
		chdrlifIO.setChdrcoy(wsspcomn.company);
		chdrlifIO.setChdrnum(sv.chdrsel);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)
		&& isNE(chdrlifIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlifIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(f259);
			wsspcomn.edterror.set("Y");
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  Check if SINSTAMT05 is less than zero, this will indicate that */
		/*  a premium waiver is in force, and that Paid to Date advance is */
		/*  not allowed                                                    */
		if (isLT(chdrlifIO.getSinstamt05(), ZERO)) {
			sv.chdrselErr.set(h142);
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(chdrlifIO.getBillfreq(), "00")) {
			/*      MOVE U062                TO S6257-CHDRSEL-ERR            */
			sv.chdrselErr.set(h085);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/* Read the valid statii from table T5679.*/
	
		
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(subprogrec.transcd.toString());
		
		itempf = itemDao.getItempfRecord(itempf);
		
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(t5679).concat(subprogrec.transcd.toString()));
			fatalError600();
		} else { //IJTI-462
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea())); //IJTI-462
		} //IJTI-462
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			headerStatuzCheck2910();
		}
		if (isNE(wsaaValidStatus, "Y")) {
			sv.chdrselErr.set(e717);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			return ;
		}
		/* Softlock the contract header record.                            */
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(f910);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    GO TO 2900-EXIT.                                             */
		return ;
	}


	/**
	* <pre>
	*     CHECK THE CONTRACT HEADER STATUS AGAINST T5679.
	* </pre>
	*/
protected void headerStatuzCheck2910()
	{
		/*HEADER-STATUZ-CHECK*/
		/*IF T5679-CN-RISK-STAT(WSAA-INDEX) NOT = SPACE                */
		/*    IF (T5679-CN-RISK-STAT(WSAA-INDEX) = CHDRLIF-STATCODE)   */
		/*   AND (T5679-CN-PREM-STAT(WSAA-INDEX) = CHDRLIF-PSTATCODE)  */
		/*        MOVE 13             TO WSAA-INDEX                    */
		/*        MOVE 'Y'            TO WSAA-VALID-STATUS             */
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*        GO TO 2990-EXIT.                                     */
		/*        GO TO 2090-EXIT.                                     */
		/*ADD 1                       TO WSAA-INDEX.                   */
		if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()], chdrlifIO.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()], chdrlifIO.getPstatcode())) {
					wsaaValidStatus = "Y";
				}
			}
		}
		/*EXIT1*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		update3001("DEFAULT");			
			
	}

protected void update3001(String nextMethod){

	switch (nextMethod) {
	  case "DEFAULT": 
		updateWssp3100();
		updateBatchControl3200();
		openNewBatch3300();
	  case "continue3400": 
		continue3400();
	  case "exit3900": 
	 }
}

protected void updateWssp3100()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		/*  Update WSSP Key details*/
		/*  Keep the contract header record.*/
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)
		&& isNE(chdrlifIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/* Softlock the contract header record.*/
		/*    MOVE 'LOCK'                 TO SFTL-FUNCTION.*/
		/*    MOVE WSSP-COMPANY           TO SFTL-COMPANY.*/
		/*    MOVE 'CH'                   TO SFTL-ENTTYP.*/
		/*    MOVE S6257-CHDRSEL          TO SFTL-ENTITY.*/
		/*    MOVE SUBP-TRANSCD           TO SFTL-TRANSACTION.*/
		/*    MOVE VRCM-USER              TO SFTL-USER.*/
		/*    CALL 'SFTLOCK' USING SFTL-SFTLOCK-REC.*/
		/*    IF SFTL-STATUZ              NOT = O-K*/
		/*                            AND NOT = 'LOCK'*/
		/*       MOVE SFTL-STATUZ         TO SYSR-STATUZ*/
		/*       PERFORM 600-FATAL-ERROR.*/
		/*    IF SFTL-STATUZ              = 'LOCK'*/
		/*       MOVE F910                TO S6257-CHDRSEL-ERR.*/
		if (isEQ(scrnparams.statuz, "BACH")) {
			update3001("exit3900");
		}
		if (isNE(subprogrec.bchrqd, "Y")) {
			update3001("exit3900");
		}
	}

protected void updateBatchControl3200()
	{
		batcchkrec.batcchkRec.set(SPACES);
		batcchkrec.function.set("CHECK");
		batcchkrec.batchkey.set(wsspcomn.batchkey);
		batcchkrec.tranid.set(wsspcomn.tranid);
		callProgram(Batcchk.class, batcchkrec.batcchkRec);
		if (isEQ(batcchkrec.statuz, varcom.oK)) {
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			update3001("exit3900");
		}
		if (isEQ(batcchkrec.statuz, varcom.dupr)) {
			sv.actionErr.set(e132);
			wsspcomn.edterror.set("Y");
			update3001("exit3900");
		}
		if (isEQ(batcchkrec.statuz, "INAC")) {
			batcdorrec.function.set("ACTIV");
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			update3001("continue3400");
		}
		if (isNE(batcchkrec.statuz, varcom.mrnf)) {
			sv.actionErr.set(batcchkrec.statuz);
			wsspcomn.edterror.set("Y");
			update3001("exit3900");
		}
	}

protected void openNewBatch3300()
	{
		batcdorrec.function.set("AUTO");
	}

protected void continue3400()
	{
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
			wsspcomn.edterror.set("Y");
			rollback();
			return ;
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.set(1);
		/*EXIT*/
	}
}
