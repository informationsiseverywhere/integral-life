/*
 * File: Pr51j.java
 * Date: 30 August 2009 1:36:38
 * Author: Quipoz Limited
 * 
 * Class transformed from PR51J.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;//ILIFE-8096
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.screens.Sr51jScreenVars;
import com.csc.life.productdefinition.procedures.Vlpdrule;
import com.csc.life.productdefinition.recordstructures.Vlpdrulrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Errmesg;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILIFE-8096
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The program will process cross check validation by call
* subroutine 'VLPDRULE'
*   - If has any error then add all error in subfile
*     and not allow to press enter to process next program
*   - If no error then pass through program to call switching
*     program in T1675
*
*
***********************************************************************
* </pre>
*/
public class Pr51j extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR51J");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaError = "";
	private PackedDecimalData wsaaIxc = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaIyc = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaProdError = new FixedLengthStringData(24);
	private FixedLengthStringData wsaaProdtyp = new FixedLengthStringData(4).isAPartOf(wsaaProdError, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaProdError, 4, FILLER).init(" ");
	private FixedLengthStringData wsaaProdErr = new FixedLengthStringData(19).isAPartOf(wsaaProdError, 5);

	private FixedLengthStringData wsaaSecProgs = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(8, 5, wsaaSecProgs, 0);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private int wsaaErrCount = 0;
	private String wsaaAddChg = "";
		/* ERRORS */
	private String rlbm = "RLBM";
		/* TABLES */
	private String t5688 = "T5688";
		/* FORMATS */
	private String chdrmjarec = "CHDRMJAREC";
	private String covtmjarec = "COVTMJAREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Errmesgrec errmesgrec = new Errmesgrec();
	private Gensswrec gensswrec = new Gensswrec();
	private Vlpdrulrec vlpdrulrec = new Vlpdrulrec();
	private Batckey wsaaBatckey = new Batckey();
	private Sr51jScreenVars sv = ScreenProgram.getScreenVars( Sr51jScreenVars.class);
	
	//ILIFE-8096 start
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private FormatsInner formatsInner = new FormatsInner();
	//ILIFE-8096 end
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit, 
		exit3090, 
		nextProgram4080
	}

	public Pr51j() {
		super();
		screenVars = sv;
		new ScreenModel("Sr51j", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		clearSubfile1100();
		retrvChdrmja1200();
		readCovtmja1600();
		if (isEQ(wsaaAddChg,"N")) {
			goTo(GotoLabel.exit1090);
		}
		sv.chdrnum.set(chdrpf.getChdrnum());//ILIFE-8096
		sv.cnttype.set(chdrpf.getCnttype());//ILIFE-8096
		readT56881500();
		validateProduct1300();
	}

protected void clearSubfile1100()
	{
		begin1110();
	}

protected void begin1110()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaError = "N";
		wsaaAddChg = "Y";
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR51J", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvChdrmja1200()
	{
		/*BEGIN*/
//ILIFE-8096 start
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			chdrmjaIO.setFormat(formatsInner.chdrmjarec);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
//ILIFE-8096 end
		}
		/*EXIT*/
	}

protected void validateProduct1300()
	{
		begin1310();
	}

protected void begin1310()
	{
		initialize(vlpdrulrec.validRec);
		vlpdrulrec.function.set("ADCH");
		vlpdrulrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
//ILIFE-8096 start
		vlpdrulrec.chdrcoy.set(chdrpf.getChdrcoy());
		vlpdrulrec.chdrnum.set(chdrpf.getChdrnum());
		vlpdrulrec.cnttype.set(chdrpf.getCnttype());
		vlpdrulrec.srcebus.set(chdrpf.getSrcebus());
		vlpdrulrec.effdate.set(chdrpf.getPtdate());
		vlpdrulrec.cownnum.set(chdrpf.getCownnum());
		vlpdrulrec.billfreq.set(chdrpf.getBillfreq());
//ILIFE-8096 end
		vlpdrulrec.language.set(wsspcomn.language);
		callProgram(Vlpdrule.class, vlpdrulrec.validRec);
		if (isNE(vlpdrulrec.statuz,varcom.oK)) {
			syserrrec.subrname.set("VLPDRULE");
			syserrrec.params.set(vlpdrulrec.validRec);
			syserrrec.statuz.set(vlpdrulrec.statuz);
			fatalError600();
		}
		for (wsaaIxc.set(1); !(isGT(wsaaIxc,50)); wsaaIxc.add(1)){
			if (isEQ(vlpdrulrec.errLife[wsaaIxc.toInt()],SPACES)
			&& isEQ(vlpdrulrec.errCnttype[wsaaIxc.toInt()],SPACES)) {
				wsaaIxc.set(51);
			}
			else {
				for (wsaaIyc.set(1); !(isGT(wsaaIyc,20)); wsaaIyc.add(1)){
					if (isEQ(vlpdrulrec.errCode[wsaaIxc.toInt()][wsaaIyc.toInt()],SPACES)) {
						wsaaIyc.set(21);
					}
					else {
						errmesgrec.errmesgRec.set(SPACES);
						errmesgrec.eror.set(vlpdrulrec.errCode[wsaaIxc.toInt()][wsaaIyc.toInt()]);
						productErrorMessage1400();
					}
				}
			}
		}
	}

protected void productErrorMessage1400()
	{
		error1410();
	}

protected void error1410()
	{
		wsaaError = "Y";
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaProg);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.set(SPACES);
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		sv.life.set(vlpdrulrec.errLife[wsaaIxc.toInt()]);
		sv.jlife.set(vlpdrulrec.errJlife[wsaaIxc.toInt()]);
		sv.coverage.set(vlpdrulrec.errCoverage[wsaaIxc.toInt()]);
		sv.rider.set(vlpdrulrec.errRider[wsaaIxc.toInt()]);
		sv.payrseqno.set(ZERO);
		if (isEQ(vlpdrulrec.errDet[wsaaIxc.toInt()][wsaaIyc.toInt()],SPACES)) {
			sv.erordsc.set(errmesgrec.errmesg[1]);
		}
		else {
			wsaaProdtyp.set(vlpdrulrec.errDet[wsaaIxc.toInt()][wsaaIyc.toInt()]);
			wsaaProdErr.set(errmesgrec.errmesg[1]);
			sv.erordsc.set(wsaaProdError);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR51J", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readT56881500()
	{
		begin1510();
	}

protected void begin1510()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrpf.getChdrcoy());//ILIFE-8096
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());//ILIFE-8096
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
	}

protected void readCovtmja1600()
	{
		begin1610();
	}

protected void begin1610()
	{
		covtmjaIO.setParams(SPACES);
		covtmjaIO.setChdrcoy(chdrpf.getChdrcoy());//ILIFE-8096
		covtmjaIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-8096
		covtmjaIO.setPlanSuffix(ZERO);
		covtmjaIO.setFormat(covtmjarec);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(),covtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),covtmjaIO.getChdrnum())) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			wsaaAddChg = "N";
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		|| isEQ(wsaaAddChg,"N")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsaaError,"N")) {
			wsspcomn.sectionno.set("3000");
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		if (isNE(wsaaError,"N")) {
			sv.chdrnumErr.set(rlbm);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit3090);
		}
//ILIFE-8096 start
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
//ILIFE-8096 end
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
					callGenssw4070();
				}
				case nextProgram4080: {
					nextProgram4080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
			goTo(GotoLabel.nextProgram4080);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
	}

protected void callGenssw4070()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("A");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			goTo(GotoLabel.nextProgram4080);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	}

protected void nextProgram4080()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}
//ILIFE-8096 start
private static final class FormatsInner { 
	/* FORMATS */
private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
}
//ILIFE-8096 end
}
