package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5150
 * @version 1.0 generated on 30/08/09 06:36
 * @author Quipoz
 */
public class S5150ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(659);
	public FixedLengthStringData dataFields = new FixedLengthStringData(307).isAPartOf(dataArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,55);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,73);
	public ZonedDecimalData cntinst = DD.cntinst.copyToZonedDecimal().isAPartOf(dataFields,76);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,134);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,135);
	public ZonedDecimalData osbal = DD.osbal.copyToZonedDecimal().isAPartOf(dataFields,143);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,160);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,207);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,254);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,262);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,272);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,280);
	public FixedLengthStringData supflag = DD.supflag.copy().isAPartOf(dataFields,290);
	public ZonedDecimalData suppressTo = DD.suppto.copyToZonedDecimal().isAPartOf(dataFields,291);
	public ZonedDecimalData todate = DD.todate.copyToZonedDecimal().isAPartOf(dataFields,299);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(88).isAPartOf(dataArea, 307);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntinstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData osbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData supflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData supptoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData todateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(264).isAPartOf(dataArea, 395);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntinstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] osbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] supflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] supptoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] todateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData suppressToDisp = new FixedLengthStringData(10);
	public FixedLengthStringData todateDisp = new FixedLengthStringData(10);
	
	public LongData S5150screenWritten = new LongData(0);
	public LongData S5150protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5150ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cnttypeOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rstateOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pstateOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payrnumOut,new String[] {null, null, null, "01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payornameOut,new String[] {null, null, null, "01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreqOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mopOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(todateOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(supflagOut,new String[] {"11","19", "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(supptoOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(osbalOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, rstate, pstate, cownnum, ownername, payrnum, payorname, agntnum, agentname, occdate, ptdate, cntcurr, btdate, billfreq, mop, cntinst, todate, supflag, suppressTo, osbal};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, rstateOut, pstateOut, cownnumOut, ownernameOut, payrnumOut, payornameOut, agntnumOut, agentnameOut, occdateOut, ptdateOut, cntcurrOut, btdateOut, billfreqOut, mopOut, cntinstOut, todateOut, supflagOut, supptoOut, osbalOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, rstateErr, pstateErr, cownnumErr, ownernameErr, payrnumErr, payornameErr, agntnumErr, agentnameErr, occdateErr, ptdateErr, cntcurrErr, btdateErr, billfreqErr, mopErr, cntinstErr, todateErr, supflagErr, supptoErr, osbalErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, todate, suppressTo};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, todateErr, supptoErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, todateDisp, suppressToDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5150screen.class;
		protectRecord = S5150protect.class;
	}

}
