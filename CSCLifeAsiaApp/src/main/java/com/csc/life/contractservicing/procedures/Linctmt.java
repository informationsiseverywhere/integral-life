package com.csc.life.contractservicing.procedures;

import java.math.BigDecimal;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.newbusiness.dataaccess.dao.LincpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.SlncpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Lincpf;
import com.csc.life.newbusiness.dataaccess.model.Slncpf;
import com.csc.life.newbusiness.procedures.LincCommonsUtil;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;

/**
 * <pre>
 * This subroutine writes data to LINCPF and SLNCPF based on comparisons 
 * done with values present in TJL06.
 * 
 * Case 1:
 * If Life Assured's age is less than the age specified in Juvenile Age
 * field of TJL06 and the sum insured on this policy for components present
 * in TJL06 is greater than or equal to the Minimum Sum Assured for Juvenile
 * field in TJL06 
 * OR
 * Case 2:
 * If Life Assured's age is greater than the age specified in Juvenile Age
 * field of TJL06 and the sum insured on this policy for components present
 * in TJL06 is greater than or equal to the Minimum Sum Assured field in TJL06
 * then  in both Case 1 and Case 2 create a record in LINCPF and SLNCPF.
 * 
 * There are two type of components for this requirement.
 * 1. Hospitalization Benefit Component
 * 2. Death Benefit component
 * 
 * If there are multiple Death Benefit Components or Hospital Benefit Components
 * on a policy such as TEN1 and TRM1 then their cumulative Sum Insured will be 
 * sent to LINC.
 *  
 * This subroutine emphasizes on Contract Servicing Transactions.
 * 
 * </pre>
 * @author gsaluja2
 * 
 * */

public class Linctmt implements LincCSService{
	
	@Autowired
	private LincCommonsUtil lincCommonsUtil;

	@Autowired
	private LincpfDAO lincpfDAO;
	
	@Autowired
	private SlncpfDAO slncpfDAO;
	
	@Autowired
	private CovrpfDAO covrpfDAO;

	private Lincpf lincpf;
	
	private Slncpf slncpfRecord;
	
	private LincpfHelper lincpfHelper;
	
	private boolean lincExists;
	
	private boolean isJuvenile;
	
	private boolean updateSlncData;
	
	/**
	 * If record exists in LINCPF and Life Assured's age is less
	 * than 15 years OR If record does NOT exist in LINCPF and
	 * Life Assured's current age is less than 15 years, this is 
	 * set as TRUE.
	 */
	private boolean condition1;
	/**
	 * If record exists in LINCPF and Life Assured's age is NOT 
	 * less than 15 years OR If record does NOT exist in LINCPF 
	 * and Life Assured's current age is NOT less than 15 years, 
	 * this is set as TRUE.
	 */
	private boolean condition2;
	/**
	 * If no rider is added after issue (ZADRDDTE IS 0) and 
	 * TRANSACTION DATE - OCCDATE from CHDRPF is less than the 
	 * duration from contract date field in TJL06 then this is 
	 * set as TRUE. (tempCondition1)
	 */
	private boolean condition3;
	/**
	 * If a rider is added after issue (ZADRDDTE IS NOT 0) and 
	 * TRANSACTION DATE - ZADRDDTE from LINCPF is less than the 
	 * duration from contract date field in TJL06 then this is 
	 * set as TRUE. (tempCondition2)
	 */
	private boolean condition4;
	/**
	 * <pre>
	 * Condition 3: If no rider is added after issue (ZADRDDTE IS 0) 
	 * and TRANSACTION DATE - OCCDATE from CHDRPF is less than the 
	 * duration from contract date field in TJL06 then this is 
	 * set as TRUE. (tempCondition1)
	 * 
	 * Condition 4: If a rider is added after issue (ZADRDDTE IS NOT 0) 
	 * and TRANSACTION DATE - ZADRDDTE from LINCPF is less than the 
	 * duration from contract date field in TJL06 then this is 
	 * set as TRUE. (tempCondition2)
	 * 
	 * Condition 5: ORING of Condition 3 AND Condition 4.
	 * </pre>
	 */
	private boolean condition5;
	/**
	 * <pre>
	 * If TRANSACTION DATE - OCCDATE from CHDRPF is greater than the 
	 * duration from contract date field in TJL06 then this is
	 * set as TRUE. (!tempCondition1 - negation of tempCondition1)
	 * OR 
	 * TRANSACTION DATE - ZADRDDTE from LINCPF is greater than the 
	 * duration from contract date field in TJL06 then this is 
	 * set as TRUE. (!tempCondition2 - negation of tempCondition2)
	 * </pre>
	 */
	private boolean condition6;
	/**
	 * If daily hospitalization benefit of a modified component 
	 * is greater than the amount stored in ZOTHSBNF of LINCPF
	 * then this is set as TRUE. 
	 */
	private boolean condition7;
	/**
	 * If death sum assured of a modified component is greater 
	 * than the amount stored in ZCSUMINS of LINCPF then this is 
	 * set as TRUE.
	 */
	private boolean condition8;
	/**
	 * <pre>
	 * Condition 7: If daily hospitalization benefit of a modified 
	 * component is greater than the amount stored in ZOTHSBNF of 
	 * LINCPF then this is set as TRUE.
	 * 
	 * Condition 8: If death sum assured of a modified component is 
	 * greater than the amount stored in ZCSUMINS of LINCPF then this 
	 * is set as TRUE.
	 * 
	 * Condition 9: ORING of Condition 6 AND Condition 7.
	 * </pre>
	 */
	private boolean condition9;
	/**
	 * If daily hospitalization benefit of a modified component 
	 * is lesser than the amount stored in ZOTHSBNF of LINCPF
	 * then this is set as TRUE. 
	 */
	private boolean condition10;
	/**
	 * If death sum assured of a modified component is lesser 
	 * than the amount stored in ZCSUMINS of LINCPF then this is 
	 * set as TRUE.
	 */
	private boolean condition11;
	/**
	 * <pre>
	 * Condition 10: If daily hospitalization benefit of a modified 
	 * component is lesser than the amount stored in ZOTHSBNF of 
	 * LINCPF then this is set as TRUE.
	 * 
	 * Condition 11: If death sum assured of a modified component is 
	 * lesser than the amount stored in ZCSUMINS of LINCPF then this 
	 * is set as TRUE.
	 * 
	 * Condition 12: ORING of Condition 10 AND Condition 11.
	 * </pre>
	 */
	private boolean condition12;
	
	private boolean setDateTerminated;
	
	private static final String ZERO = "0";
	
	private int zadrddte;
	
	private int zexdrddte;
	
	public Linctmt(){
		//does nothing
	}
	
	public void performLincProcess(Tjl47rec tjl47rec, int effdate) {
		zexdrddte = 0;
		updateSlncData = false;
		lincpf = lincpfDAO.readLincpfData(lincpfHelper.getChdrnum());
		if(lincpf!=null)
			lincExists = true;
		else {
			lincExists = false;
			lincpf = new Lincpf();
		}
		slncpfRecord = slncpfDAO.readSlncpfData(lincpfHelper.getChdrnum());
		if(slncpfRecord != null)
			updateSlncData = true;
		zadrddte = lincpf.getZadrddte();
		getCoverageDetails(effdate);
		commonProcessing(tjl47rec);
	}
	
	public void commonProcessing(Tjl47rec tjl47rec) {
		isJuvenile = false;
		initConditions();
		if(lincpfHelper.getLifeage() < lincpfHelper.getJuvenileage())
			isJuvenile = true;
		BigDecimal termDiff = lincCommonsUtil.getTermDiff(lincpfHelper.getOccdate(), lincpfHelper.getTransactionDate());
		BigDecimal termDiffAddedRider = lincCommonsUtil.getTermDiff(lincpf.getZadrddte(), lincpfHelper.getTransactionDate());
		boolean tempCondition1 = termDiff.compareTo(new BigDecimal(lincpfHelper.getCdateduration())) < 0;
		boolean tempCondition2 = termDiffAddedRider.compareTo(new BigDecimal(lincpfHelper.getCdateduration())) < 0;
		resetConditions1(tempCondition1, tempCondition2);
		resetConditions2(tempCondition1, tempCondition1);
		if(!setDateTerminated) {
			lincpf.setZreclass(tjl47rec.rectype01.toString());
			lincpf.setZmodtype(tjl47rec.chgtype01.toString());
		}
	}
	
	public void initConditions() {
		condition1 = false;
		condition2 = false;
		condition3 = false;
		condition4 = false;
		condition5 = false;
		condition6 = false;
		condition7 = false;
		condition8 = false;
		condition9 = false;
		condition10 = false;
		condition11 = false;
		condition12 = false;
	}
	
	public void getCoverageDetails(int effDate) {
		Map<String, BigDecimal> crtableSuminsMap = null;
		crtableSuminsMap = covrpfDAO.componentChangeCvg(lincpfHelper.getChdrnum());
		lincCommonsUtil.validateLifeAssuredAge(lincpfHelper, effDate);
		if(lincpfHelper.getLifeage() != 0)
			lincCommonsUtil.checkCriteria(crtableSuminsMap, lincpfHelper);
	}
	
	/**
	 * 
	 */
	public void resetConditions1(boolean tempCondition1, boolean tempCondition2) {
		condition1 = (lincExists && isJuvenile) || (!lincExists && isJuvenile);
		condition2 = (lincExists && !isJuvenile) || (!lincExists && !isJuvenile);
		condition3 = lincpf.getZadrddte() == 0 && tempCondition1;
		condition4 = lincpf.getZadrddte() != 0 && tempCondition2;
		condition5 = condition3 || condition4;
	}
	
	public void resetConditions2(boolean tempCondition1, boolean tempCondition2) {
		condition6 = !tempCondition1 || !tempCondition2;
		condition7 = lincpfHelper.getHosbensSI().compareTo(lincpf.getZothsbnf()) > 0;
		condition8 = lincpfHelper.getSumInsured().compareTo(lincpf.getZcsumins()) > 0;
		condition9 = condition7 || condition8;
		condition10 = lincpfHelper.getHosbensSI().compareTo(lincpf.getZothsbnf()) < 0;
		condition11 = lincpfHelper.getSumInsured().compareTo(lincpf.getZcsumins()) < 0;
		condition12 = condition10 || condition11;
	}
	
	@Override
	public void generalProcessing(LincpfHelper lincpfHelper, Tjl47rec tjl47rec) {
		this.lincpfHelper = lincpfHelper;
		setDateTerminated = true;
		performLincProcess(tjl47rec, this.lincpfHelper.getTransactionDate());
		if(lincExists && lincpf.getDtetrmntd() == 0) {
			if(condition1 || (condition2 && condition5)) {
				setLincDeleteData(tjl47rec.rectype01.toString().trim(), tjl47rec.chgtype01.toString().trim());
			}
			else if(condition6) {
				setDateTerminated();
			}
		}
	}

	@Override
	public void addComponentProposal(LincpfHelper lincpfHelper, Tjl47rec tjl47rec) {
		this.lincpfHelper = lincpfHelper;
		setDateTerminated = false;
		performLincProcess(tjl47rec, lincpfHelper.getPtrnEff());
		if(this.lincpfHelper.isCritSatisfied()) {
			if(condition1) {
				zexdrddte = this.lincpfHelper.getPtrnEff();
				setAddModProposalData();
				lincCommonsUtil.generateFollowUp(lincpfHelper);
			}
			else if (condition2 && condition5) {
				setAddModProposalData();
				lincCommonsUtil.generateFollowUp(lincpfHelper);
			}
		}
	}

	@Override
	public void addComponentApproval(LincpfHelper lincpfHelper, Tjl47rec tjl47rec) {
		this.lincpfHelper = lincpfHelper;
		setDateTerminated = false;
		performLincProcess(tjl47rec, lincpfHelper.getPtrnEff());
		if(this.lincpfHelper.isCritSatisfied()) {
			if(condition1) {
				zadrddte = this.lincpfHelper.getPtrnEff();
				setAddModApprovalData();
			}
			else if (condition2 && condition5) {
				setAddModApprovalData();
			}
		}
	}

	@Override
	public void modifyComponentProposal(LincpfHelper lincpfHelper, Tjl47rec tjl47rec) {
		this.lincpfHelper = lincpfHelper;
		setDateTerminated = false;
		boolean tempCondition;
		performLincProcess(tjl47rec, lincpfHelper.getPtrnEff());
		if(this.lincpfHelper.isCritSatisfied()) {
			tempCondition = condition2 && condition5;
			if((condition1 || tempCondition) && condition9) {
				setAddModProposalData();
				lincCommonsUtil.generateFollowUp(this.lincpfHelper);
			}
		}
	}

	@Override
	public void modifyComponentApproval(LincpfHelper lincpfHelper, Tjl47rec tjl47rec) {
		this.lincpfHelper = lincpfHelper;
		setDateTerminated = true;
		boolean tempCondition;
		performLincProcess(tjl47rec, lincpfHelper.getPtrnEff());
		if(this.lincpfHelper.isCritSatisfied()) {
			tempCondition = condition2 && condition5;
			if("T6A8".equals(lincpf.getBatctrcde())) {
				lincpf.setZreclass(tjl47rec.rectype01.toString());
				lincpf.setZmodtype(tjl47rec.chgtype01.toString());
				setAddModApprovalData();
			}
			else if ((condition1 || tempCondition) && condition12) {
				lincpf.setZreclass(tjl47rec.rectype02.toString());
				lincpf.setZmodtype(tjl47rec.chgtype02.toString());
				setAddModApprovalData();
			}
		}
		else if(condition6 && condition12) {
			setDateTerminated();
		}
		else if(condition12) {
			setLincDeleteData(tjl47rec.rectype03.toString().trim(), tjl47rec.chgtype03.toString().trim());
		}
	}
		
	public void setDateTerminated() {
		lincpf.setBatctrcde(lincpfHelper.getTranscd());
		lincpf.setDtetrmntd(lincpfHelper.getTransactionDate());
		lincpf.setUsrprf(lincpfHelper.getUsrprf().toUpperCase());
		lincpf.setJobnm(lincpfHelper.getJobnm().toUpperCase());
		lincpfDAO.updateLincData(lincpf);
	}
	
	public void setLincDeleteData(String dataClass, String changeType) {
		lincpf.setZreclass(dataClass);
		lincpf.setZmodtype(changeType);
		lincCommonsUtil.setupCommonLinc(lincpf, lincpfHelper);
		lincCommonsUtil.setupLincDates(lincpf, lincpfHelper.getOccdate(), 0, 
				lincpfHelper.getTransactionDate(), 0, zadrddte, zexdrddte);
		lincpf.setZrevdate(lincpfHelper.getPtrnEff());
		performDatabaseOperations();
	}
	
	public void setAddModProposalData() {
		if(lincpf.getDtetrmntd() != 0)
			lincpf.setKlinckey(StringUtils.leftPad(StringUtils.EMPTY, 12, ZERO));
		lincCommonsUtil.setupCommonLinc(lincpf, lincpfHelper);
		lincCommonsUtil.setupLincDates(lincpf, lincpfHelper.getOccdate(), 0, 0, 
				lincpfHelper.getPtrnEff(), zadrddte, zexdrddte);
		lincpf.setZothsbnf(lincpfHelper.getHosbensSI());
		lincpf.setZcsumins(lincpfHelper.getSumInsured());
		lincpf.setSenddate(0);
		lincpf.setRecvdate(0);
		lincpf.setZlincdte(0);
		performDatabaseOperations();
	}
	
	public void setAddModApprovalData() {
		lincCommonsUtil.setupCommonLinc(lincpf, lincpfHelper);
		lincCommonsUtil.setupLincDates(lincpf, lincpfHelper.getOccdate(), 0, 0, 0, zadrddte, zexdrddte);
		lincpf.setZrevdate(lincpfHelper.getPtrnEff());
		lincpf.setZothsbnf(lincpfHelper.getHosbensSI());
		lincpf.setZcsumins(lincpfHelper.getSumInsured());
		performDatabaseOperations();
	}
	
	public void performDatabaseOperations() {
		if(!lincExists) {
			lincpf.setKlinckey(StringUtils.leftPad(StringUtils.EMPTY, 12, ZERO));
			lincpf = lincCommonsUtil.setupLincpf(lincpfHelper, lincpf);
			lincpfDAO.insertLincData(lincpf);
		}
		else
			lincpfDAO.updateLincData(lincpf);
		slncpfRecord = lincCommonsUtil.setupSlncpf(lincpf, slncpfRecord);
		if(!updateSlncData)
			slncpfDAO.insertSlncData(slncpfRecord);
		else
			slncpfDAO.updateSlncData(slncpfRecord);
	}
}