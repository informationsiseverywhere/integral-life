/*
 * File: Zrrevlif.java
 * Date: 30 August 2009 2:56:26
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRREVLIF.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*            REVERSE ADD ANOTHER LIFE TRANSACTION.
*
* This module reverses the add another life transaction. It is
* called from the full contract reversal transaction.
* The proceesing is as follows :
*
* Initialise
* ----------
*
* Set up the reversal parameters in working storage.
*
* Main Processing
* ---------------
*
* Delete the life
*
*   - Read the life records using LIFEMJA and find the record
*     where LIFEMJA-TRANNO is equal to the REVE-TRANNO.
*
*   - Call CLTRELN with the function of delete to process any
*     role records.
*
*   - Delete life record using LIFEMJA.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Zrrevlif extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zrrevlif.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("ZREVLIFE");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private static final String lifemjarec = "LIFEMJAREC";

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaForenum, 8).init(SPACES);
	private FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(wsaaForenum, 10, FILLER).init(SPACES);
	private static final String wsaaFsuco = "9";
		/* WSAA-DELETE-KEY */
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Reverserec reverserec = new Reverserec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readLifeRecord2210
	}

	public Zrrevlif() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control0000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void control0000()
	{
		/*START*/

		initialise1000();
		deleteLife2000();
		callBldenrl3000();
		/*EXIT*/

		exitProgram();
	}

protected void initialise1000()
	{
		/*INITIALISE*/

		wsaaChdrChdrnum.set(reverserec.chdrnum);
		wsaaChdrChdrcoy.set(reverserec.company);
	}

protected void deleteLife2000()
	{
		/*DELETE-LIFE*/
		/*  - delete life*/
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsaaChdrChdrcoy);
		lifemjaIO.setChdrnum(wsaaChdrChdrnum);
		lifemjaIO.setLife("00");
		lifemjaIO.setJlife("00");
		/* MOVE BEGNH                  TO LIFEMJA-FUNCTION.             */
		lifemjaIO.setFunction(varcom.begn);
		lifeDeletion2200();
		/*EXIT*/
	}

protected void lifeDeletion2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case readLifeRecord2210: 
					readLifeRecord2210();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readLifeRecord2210()
	{
		/*  - read  the life details using LIFEMJA.*/
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		/*  - skip deletion if optional joint life is not there.*/
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			databaseError99500();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(lifemjaIO.getChdrcoy(),reverserec.company)
		|| isNE(lifemjaIO.getChdrnum(),reverserec.chdrnum)) {
			return ;
		}
		if (isNE(lifemjaIO.getTranno(),reverserec.tranno)) {
			lifemjaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readLifeRecord2210);
		}
		cltrelnrec.data.set(SPACES);
		cltrelnrec.clrrrole.set("LF");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(lifemjaIO.getChdrcoy());
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(lifemjaIO.getChdrnum());
		wsaaLife.set(lifemjaIO.getLife());
		cltrelnrec.forenum.set(wsaaForenum);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsaaFsuco);
		cltrelnrec.clntnum.set(lifemjaIO.getLifcnum());
		cltrelnrec.function.set("DEL  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			databaseError99500();
		}
		/* delete life record previously read*/
		/*  MOVE DELET                 TO LIFEMJA-FUNCTION.             */
		lifemjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			syserrrec.params.set(lifemjaIO.getParams());
			databaseError99500();
		}
	}

protected void callBldenrl3000()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(fsupfxcpy.chdr);
		bldenrlrec.company.set(reverserec.company);
		bldenrlrec.uentity.set(reverserec.chdrnum);
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			databaseError99500();
		}
		/*EXIT*/
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
	}

protected void databaseError99500()
	{
			start99500();
			exit99990();
		}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		/****  GO TO 0000-EXIT.                                             */
	}
}
