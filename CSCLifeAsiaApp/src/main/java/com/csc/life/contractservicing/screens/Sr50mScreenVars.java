package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR50M
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50mScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(702);
	public FixedLengthStringData dataFields = new FixedLengthStringData(334).isAPartOf(dataArea, 0);
	public ZonedDecimalData cntfee = DD.cntfee.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData grsprm = DD.grsprm.copyToZonedDecimal().isAPartOf(dataFields,17);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,28);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,45);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,47);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,73);
	public FixedLengthStringData cnttyp = DD.cnttyp.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,87);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,117);
	public FixedLengthStringData freqdesc = DD.freqdesc.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,155);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,202);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,210);
	public FixedLengthStringData mopdesc = DD.mopdesc.copy().isAPartOf(dataFields,211);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,241);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,249);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,296);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,306);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,314);
	public ZonedDecimalData stampduty = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,317);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(92).isAPartOf(dataArea, 334);
	public FixedLengthStringData cntfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData grsprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cnttypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData freqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData mopdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData stampdutyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(276).isAPartOf(dataArea, 426);
	public FixedLengthStringData[] cntfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] grsprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cnttypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] freqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] mopdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[]stampdutyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);

	//ILIFE-2002 starts by slakkala
	public FixedLengthStringData subfileArea = new FixedLengthStringData(2637);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(2378).isAPartOf(subfileArea, 0);
	//ILIFE-2002 ends
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData basprm = DD.basprm.copyToZonedDecimal().isAPartOf(subfileFields,1);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(subfileFields,14);
	public FixedLengthStringData datakey = DD.datakey.copy().isAPartOf(subfileFields,18);
	//ILIFE-2002 starts by slakkala
	public FixedLengthStringData entity = DD.entity1.copy().isAPartOf(subfileFields,274);
	public ZonedDecimalData pstatdate = DD.pstdat.copyToZonedDecimal().isAPartOf(subfileFields,306);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(subfileFields,314);
	public FixedLengthStringData waiverprem = DD.waiverprem.copy().isAPartOf(subfileFields,331);
	public FixedLengthStringData workAreaData = DD.workarea.copy().isAPartOf(subfileFields,332);
	public ZonedDecimalData zgrsamt = DD.zgrsamt.copyToZonedDecimal().isAPartOf(subfileFields,2332);
	public ZonedDecimalData zloprm = DD.zloprm.copyToZonedDecimal().isAPartOf(subfileFields,2344);
	public FixedLengthStringData zrwvflgs = new FixedLengthStringData(4).isAPartOf(subfileFields, 2357);
	//ILIFE-2002 ends
	public FixedLengthStringData[] zrwvflg = FLSArrayPartOfStructure(4, 1, zrwvflgs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(zrwvflgs, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrwvflg01 = DD.zrwvflg.copy().isAPartOf(filler,0);
	public FixedLengthStringData zrwvflg02 = DD.zrwvflg.copy().isAPartOf(filler,1);
	public FixedLengthStringData zrwvflg03 = DD.zrwvflg.copy().isAPartOf(filler,2);
	public FixedLengthStringData zrwvflg04 = DD.zrwvflg.copy().isAPartOf(filler,3);
	public ZonedDecimalData zstpduty = DD.zstpduty.copyToZonedDecimal().isAPartOf(subfileFields, 2361);
	//ILIFE-2002 starts by slakkala
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(64).isAPartOf(subfileArea, 2378);
	//ILIFE-2002 ends
	public FixedLengthStringData actindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData basprmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData datakeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData entityErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData pstdatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData waiverpremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData workareaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData zgrsamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData zloprmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData zrwvflgsErr = new FixedLengthStringData(16).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData[] zrwvflgErr = FLSArrayPartOfStructure(4, 4, zrwvflgsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(zrwvflgsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrwvflg01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData zrwvflg02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData zrwvflg03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData zrwvflg04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData zstpdutyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	//ILIFE-2002 starts by slakkala
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(195).isAPartOf(subfileArea, 2442);
	//ILIFE-2002 ends
	public FixedLengthStringData[] actindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] basprmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] datakeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] entityOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] pstdatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] waiverpremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] workareaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] zgrsamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] zloprmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData zrwvflgsOut = new FixedLengthStringData(48).isAPartOf(outputSubfile, 132);
	public FixedLengthStringData[] zrwvflgOut = FLSArrayPartOfStructure(4, 12, zrwvflgsOut, 0);
	public FixedLengthStringData[][] zrwvflgO = FLSDArrayPartOfArrayStructure(12, 1, zrwvflgOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(zrwvflgsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrwvflg01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] zrwvflg02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] zrwvflg03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] zrwvflg04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] zstpdutyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	//ILIFE-2002 starts by slakkala
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 192);
	//ILIFE-2002 ends
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr50mscreensflWritten = new LongData(0);
	public LongData Sr50mscreenctlWritten = new LongData(0);
	public LongData Sr50mscreenWritten = new LongData(0);
	public LongData Sr50mprotectWritten = new LongData(0);
	public GeneralTable sr50mscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr50mscreensfl;
	}

	public Sr50mScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(stampdutyOut,new String[] {null, null, null, "01", null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {datakey, pstatdate, crtable, entity, sumins, waiverprem, activeInd, zrwvflg01, zrwvflg02, zrwvflg03, zrwvflg04, workAreaData, basprm, zloprm, zgrsamt};
		screenSflOutFields = new BaseData[][] {datakeyOut, pstdatOut, crtableOut, entityOut, suminsOut, waiverpremOut, actindOut, zrwvflg01Out, zrwvflg02Out, zrwvflg03Out, zrwvflg04Out, workareaOut, basprmOut, zloprmOut, zgrsamtOut};
		screenSflErrFields = new BaseData[] {datakeyErr, pstdatErr, crtableErr, entityErr, suminsErr, waiverpremErr, actindErr, zrwvflg01Err, zrwvflg02Err, zrwvflg03Err, zrwvflg04Err, workareaErr, basprmErr, zloprmErr, zgrsamtErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttyp, ctypedes, cntcurr, chdrstatus, occdate, cownnum, lifenum, billfreq, mop, premstatus, ptdate, ownername, lifename, freqdesc, mopdesc, register, btdate, effdate, grsprm, cntfee, instPrem, stampduty};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypOut, ctypedesOut, cntcurrOut, chdrstatusOut, occdateOut, cownnumOut, lifenumOut, billfreqOut, mopOut, premstatusOut, ptdateOut, ownernameOut, lifenameOut, freqdescOut, mopdescOut, registerOut, btdateOut, effdateOut, grsprmOut, cntfeeOut, instprmOut, stampdutyOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypErr, ctypedesErr, cntcurrErr, chdrstatusErr, occdateErr, cownnumErr, lifenumErr, billfreqErr, mopErr, premstatusErr, ptdateErr, ownernameErr, lifenameErr, freqdescErr, mopdescErr, registerErr, btdateErr, effdateErr, grsprmErr, cntfeeErr, instprmErr, stampdutyErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp};
		
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr50mscreen.class;
		screenSflRecord = Sr50mscreensfl.class;
		screenCtlRecord = Sr50mscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr50mprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr50mscreenctl.lrec.pageSubfile);
	}
}
