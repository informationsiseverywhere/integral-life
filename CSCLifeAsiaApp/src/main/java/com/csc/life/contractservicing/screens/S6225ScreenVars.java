package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6225
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6225ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(193+DD.cltaddr.length*5);//pmujavadiya
	public FixedLengthStringData dataFields = new FixedLengthStringData(65+DD.cltaddr.length*5).isAPartOf(dataArea, 0);//pmujavadiya//ILIFE-3222
	public FixedLengthStringData cltaddrs = new FixedLengthStringData(DD.cltaddr.length*5).isAPartOf(dataFields, 0);//pmujavadiya
	public FixedLengthStringData[] cltaddr = FLSArrayPartOfStructure(5, DD.cltaddr.length, cltaddrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(cltaddrs.length()).isAPartOf(cltaddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01 = DD.cltaddr.copy().isAPartOf(filler,0);
	public FixedLengthStringData cltaddr02 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*1);
	public FixedLengthStringData cltaddr03 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*2);
	public FixedLengthStringData cltaddr04 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*3);
	public FixedLengthStringData cltaddr05 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*4);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(dataFields,DD.cltaddr.length*5);//pmujavadiya
	public FixedLengthStringData despname = DD.despname.copy().isAPartOf(dataFields,10+DD.cltaddr.length*5);
	public FixedLengthStringData despnum = DD.despnum.copy().isAPartOf(dataFields,57+DD.cltaddr.length*5);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 65+DD.cltaddr.length*5);//ILIFE-3222
	public FixedLengthStringData cltaddrsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] cltaddrErr = FLSArrayPartOfStructure(5, 4, cltaddrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(cltaddrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData cltaddr02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData cltaddr03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData cltaddr04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData cltaddr05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData cltpcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData despnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData despnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 97+DD.cltaddr.length*5);//ILIFE-3222
	public FixedLengthStringData cltaddrsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] cltaddrOut = FLSArrayPartOfStructure(5, 12, cltaddrsOut, 0);
	public FixedLengthStringData[][] cltaddrO = FLSDArrayPartOfArrayStructure(12, 1, cltaddrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(cltaddrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cltaddr01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] cltaddr02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] cltaddr03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] cltaddr04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] cltaddr05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] cltpcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] despnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] despnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6225screenWritten = new LongData(0);
	public LongData S6225windowWritten = new LongData(0);
	public LongData S6225hideWritten = new LongData(0);
	public LongData S6225protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6225ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(despnumOut,new String[] {"01","50","-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {despnum, despname, cltaddr01, cltaddr02, cltaddr03, cltaddr04, cltaddr05, cltpcode};
		screenOutFields = new BaseData[][] {despnumOut, despnameOut, cltaddr01Out, cltaddr02Out, cltaddr03Out, cltaddr04Out, cltaddr05Out, cltpcodeOut};
		screenErrFields = new BaseData[] {despnumErr, despnameErr, cltaddr01Err, cltaddr02Err, cltaddr03Err, cltaddr04Err, cltaddr05Err, cltpcodeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6225screen.class;
		protectRecord = S6225protect.class;
		hideRecord = S6225hide.class;
	}

}
