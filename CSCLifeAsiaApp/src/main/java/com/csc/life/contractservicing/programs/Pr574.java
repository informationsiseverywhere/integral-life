/*
 * File: Pr574.java
 * Date: 30 August 2009 1:44:09
 * Author: Quipoz Limited
 * 
 * Class transformed from PR574.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO; //ILB-456
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILB-456
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.screens.Sr574ScreenVars;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.procedures.Csncalc;
import com.csc.life.reassurance.procedures.Rasaltr;
import com.csc.life.reassurance.recordstructures.Csncalcrec;
import com.csc.life.reassurance.recordstructures.Rasaltrrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Facultative Reassurance Confirm Screen.
* (Clone from P5466 for Component Change V4)
*
* Overview
* ========
*
* This program forms part of the Reassurance Development.
* It takes the place of the existing Program P5132.
*
* It is a scrolling selection subfile. Switching after component
* selection is controlled by OPTSWCH.
*
* Processing
* ==========
*
* 1000-INITIALISE.
*
* Bypass this section if returning from a previous selection.
* Clear the subfile.
* Intialise Call to OPTSWCH.
* Retrieve CHDRMJA.
* Set up screen header details.
* Load all associated coverages and riders to the subfile.
*
* 2000-SCREEN-EDIT.
*
* Bypass this section if returning from a previous selection.
* Display the screen.
* Validate the subfile.
* Redisplay the screen if any errors exist.
*
* 3000-UPDATE.
*
* No updating required.
*
* 4000-WHERE-NEXT.
*
* Find selected components, if any.
* Once a selection is found, store the COVRMJA for use in
* the next program and call OPTSWCH to load the program stack
* and call the next program.
* If no selection is found, return to the submenu.
*
*****************************************************************
* </pre>
*/
public class Pr574 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR574");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatus = "";
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaStoreLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaValidStatcode = new FixedLengthStringData(1).init("N");
	private Validator validStatcode = new Validator(wsaaValidStatcode, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1).init("N");
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");

	private FixedLengthStringData wsaaRacdExit = new FixedLengthStringData(1);
	private Validator wsaaRacdEof = new Validator(wsaaRacdExit, "Y");

	private FixedLengthStringData wsaaNoFacl = new FixedLengthStringData(1);
	private Validator noFacl = new Validator(wsaaNoFacl, "Y");

	private FixedLengthStringData wsaaCompSelected = new FixedLengthStringData(1);
	private Validator compSelected = new Validator(wsaaCompSelected, "Y");

	private FixedLengthStringData wsaaResetProgramStack = new FixedLengthStringData(1);
	private Validator resetProgramStack = new Validator(wsaaResetProgramStack, "Y");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator addComp = new Validator(wsaaFlag, "A");
	private Validator modifyComp = new Validator(wsaaFlag, "M");
	private Validator compEnquiry = new Validator(wsaaFlag, "I");
	private Validator addApp = new Validator(wsaaFlag, "P");
	private Validator modApp = new Validator(wsaaFlag, "S");
	private Validator revComp = new Validator(wsaaFlag, "R");
	private Validator addLife = new Validator(wsaaFlag, "D");

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 24);
	private FixedLengthStringData filler3 = new FixedLengthStringData(175).isAPartOf(wsaaTransactionRec, 25, FILLER).init(SPACES);

	private FixedLengthStringData wsaaAtSubmitFlag = new FixedLengthStringData(1).init(SPACES);
	private Validator atSubmissionReqd = new Validator(wsaaAtSubmitFlag, "Y");

		/* WSAA-MESSAGE-AREA */
	private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaMessage = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 0).init("AT request submitted for ");
	private FixedLengthStringData wsaaMsgnum = new FixedLengthStringData(8).isAPartOf(wsaaMsgarea, 25);
		/* ERRORS */
	private static final String r067 = "R067";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5679 = "T5679";
	private static final String t5681 = "T5681";
	private static final String t5682 = "T5682";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
		/* FORMATS */
	private static final String covrmjarec = "COVRMJAREC";
	private static final String covtmjarec = "COVTMJAREC";
	private static final String lifeenqrec = "LIFEENQREC";
	private static final String racdlnbrec = "RACDLNBREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
//	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Atreqrec atreqrec = new Atreqrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Optswchrec optswchrec = new Optswchrec();
	private T5679rec t5679rec = new T5679rec();
	private Rasaltrrec rasaltrrec = new Rasaltrrec();
	private Csncalcrec csncalcrec = new Csncalcrec();
	private Wssplife wssplife = new Wssplife();
	private Sr574ScreenVars sv = ScreenProgram.getScreenVars( Sr574ScreenVars.class);
	//ILB-456 starts
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private Covrpf covrpf = new Covrpf();
	private int covrpfCount = 0;
	private List<Covtpf> covtpfList;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private Covtpf covtpf=new Covtpf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private Lifepf lifepf=new Lifepf();
	private List<Lifepf> lifepfList;
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private Racdpf racdpf=new Racdpf();
	private List<Racdpf> racdpfList;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		skipInit1080, 
		exit1090, 
		next1180, 
		exit1190, 
		next1680, 
		exit1690, 
		exit2090, 
		updateErrorIndicators2120
	}

	public Pr574() {
		super();
		screenVars = sv;
		new ScreenModel("Sr574", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					headerDetail1030();
					readCovt1040();
					readLifeDetails1050();
					jointLifeDetails1060();
					contractTypeStatus1070();
					subfileLoad1080();
				case skipInit1080: 
					skipInit1080();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaFlag.set(wsspcomn.flag);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR574", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Read CHDRMJA (RETRV) to obtain the contract header*/
		/* information.*/
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
	}

protected void headerDetail1030()
	{
		/* Move Contract details to the screen header.*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
	}

protected void readCovt1040()
	{
		wsaaAtSubmitFlag.set("N");
		//performance improvement -- Anjali
		/* Check if AT module is to be called*/

		covtpfList = covtpfDAO.searchCovtRecordByCoyNumDescUniquNo(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());

		if(covtpfList.isEmpty()){
			wsaaAtSubmitFlag.set("N");
			wsaaNoFacl.set("Y");
			goTo(GotoLabel.exit1090);
		}
		else {
			wsaaAtSubmitFlag.set("Y");
			wsaaEffdate.set(covtpfList.get(0).getEffdate());
		}		
	}

protected void readLifeDetails1050()
	{
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/


		lifepfList = lifepfDAO.getOccRecordJLife(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum(),"01","00");
		if(lifepfList.isEmpty()){
			syserrrec.params.set("chdrpf.getChdrcoy()"+"chdrpf.getChdrnum()");
			fatalError600();
		}
		else
			lifepf = lifepfList.get(0);	
		/* Get the client name.*/
		cltsIO.setParams(SPACES);
		sv.lifenum.set(lifepf.getLifcnum());
		cltsIO.setClntnum(lifepf.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/* Format the Name in Plain format.*/
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jointLifeDetails1060()
	{
		/* Check for the existence of Joint Life details.*/
		if(lifepfList.size() > 1 && lifepfList.get(1).getJlife() == "01"){
			lifepf = lifepfList.get(1);  
			sv.jlife.set(lifepf.getLifcnum());
			cltsIO.setClntnum(lifepf.getLifcnum());
			/*                                WSAA-L2-CLNTNUM               */
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
			/***** ELSE                                                         */
			/*****    MOVE SPACES              TO WSAA-L2-CLNTNUM               */
		}
	}

protected void contractTypeStatus1070()
	{
		/* Obtain the Contract Type description from T5688.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/* Obtain the Contract Status description from T3623.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.set(SPACES);
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.set(SPACES);
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
	}

protected void subfileLoad1080()
	{
		/* Call RASALTR to create RACD records. If a statuz of 'FACL'*/
		/* is returned, facultative RACD records have been created so*/
		/* load these into the subfile. Note that treaty RACD records*/
		/* are NOT loaded into the subfile and therefore are never*/
		/* displayed. The reason for this is that the user is only*/
		/* allowed to modify facultative records. Treaty records are*/
		/* created from arrangements which are table driven and should*/
		/* not be altered. This only applies to Component Modify, as with*/
		/* Component Add, CSNCALC should be called directly, to calculate*/
		/* Cessions from scratch.*/
		sv.hflagOut[varcom.nd.toInt()].set("N");
		wsaaNoFacl.set("N");
		wsaaStatuz.set(varcom.oK);
		/*    IF ADD-COMP                                                  */
		
		lifepfList = lifepfDAO.searchLifeRecordByChdrNum(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
		if(lifepfList.isEmpty()){
			syserrrec.params.set(chdrpf.getChdrcoy().toString() + chdrpf.getChdrnum());
			fatalError600();
		}
		
		if (addComp.isTrue()
		|| addApp.isTrue()) {

			wsaaLife.set(SPACES);

			for(Lifepf l :lifepfList){
				lifepf = l;
				callCsncalc1600();
			}
			
		}
		else {
			for(Covtpf c :covtpfList){
				covtpf = c;
				callRasaltr1500();
			}
			
		}
		if (isEQ(wsaaStatuz,varcom.oK)) {
			sv.hflagOut[varcom.nd.toInt()].set("Y");
			wsaaNoFacl.set("Y");
			goTo(GotoLabel.exit1090);
		}
		if (!noFacl.isTrue()
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		&& firstTime.isTrue()) {
			wsaaResetProgramStack.set("Y");
			wsaaFirstTime.set("N");
			goTo(GotoLabel.skipInit1080);
		}
		else {
			wsaaResetProgramStack.set("N");
			wsaaFirstTime.set("Y");
		}
		/* Intialise Call to OPTSWCH.*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}

protected void skipInit1080()
	{
		/* Add to the subfile all coverages and riders which have*/
		/* facultative reassurance associated with them.*/

		racdpfList = racdpfDAO.searchRacdRecord(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
		//performance improvement -- Anjali
		wsaaStoreLife.set(SPACES);
		wsaaStoreCoverage.set(SPACES);
		wsaaStoreRider.set(SPACES);
		/* List all the coverages.*/

		for(Racdpf r: racdpfList){
			racdpf = r;
			listCoverages1100();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void listCoverages1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFirstCoverage1110();
				case next1180: 
					next1180();
				case exit1190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFirstCoverage1110()
	{
		if (isNE(racdpf.getCestype(),"2")) {
			goTo(GotoLabel.next1180);
		}
		if (isEQ(racdpf.getLife(),wsaaStoreLife)
				&& isEQ(racdpf.getCoverage(),wsaaStoreCoverage)
				&& isEQ(racdpf.getRider(),wsaaStoreRider)) {
			goTo(GotoLabel.next1180);
		}
		else {
			wsaaStoreLife.set(racdpf.getLife());
			wsaaStoreCoverage.set(racdpf.getCoverage());
			wsaaStoreRider.set(racdpf.getRider());
		}
		/*    IF ADD-COMP                                                  */
		if (addComp.isTrue()
		|| addApp.isTrue()) {
			readCovt1200();
		}
		else {
			readCovr1300();
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR574", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void next1180()
	{
		//
	}

protected void readCovt1200()
	{
		covt1201();
	}

protected void covt1201()
	{

		covtpf = covtpfDAO.getCovtpfData(racdpf.getChdrcoy(),racdpf.getChdrnum(),racdpf.getLife(),
				racdpf.getCoverage(),racdpf.getRider(),racdpf.getPlanSuffix()).get(0);

		if(null == covtpf){
			syserrrec.params.set("racdlnbIO.getChdrcoy() "+" racdlnbIO.getChdrnum()");
			fatalError600();
		}
		/* If COVTMJA-RIDER equal to '00' then we are Processing a Cover*/
		/* Else we are processing Riders.*/
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		wsaaCrtable.set(covtpf.getCrtable());
		if (isEQ(covtpf.getRider(),ZERO)) {
			covtCoverageToScreen1250();
		}
		else {
			covtRiderToScreen1260();
		}
	}

protected void covtCoverageToScreen1250()
	{
		/*COVTAGE*/
		sv.cmpntnum.set(covtpf.getCoverage());
		wsaaCoverage.set(covtpf.getCrtable());
		sv.component.set(wsaaCovrComponent);
		getDescription1370();
		/* Store the COVTMJA key for later use in screen Hidden Fields.*/
		sv.hlifeno.set(covtpf.getLife());
		sv.hsuffix.set(covtpf.getPlnsfx());
		sv.hcoverage.set(covtpf.getCoverage());
		sv.hrider.set(covtpf.getRider());
		/*EXIT*/
	}

protected void covtRiderToScreen1260()
	{
		/*RIDER*/
		/* Set up and write the current Rider details.*/
		sv.cmpntnum.set(covtpf.getRider());
		wsaaRider.set(covtpf.getCrtable());
		sv.component.set(wsaaRidrComponent);
		getDescription1370();
		/* Store the COVRMJA key for later use in screen Hidden Fields.*/
		sv.hlifeno.set(covtpf.getLife());
		sv.hsuffix.set(covtpf.getPlnsfx());
		sv.hcoverage.set(covtpf.getCoverage());
		sv.hrider.set(covtpf.getRider());
		/*EXIT*/
	}

protected void readCovr1300()
	{
		covr1301();
	}

protected void covr1301()
	{
		//covrmjaIO.setParams(SPACES);
		covrpf.setChdrcoy(racdpf.getChdrcoy());
		covrpf.setChdrnum(racdpf.getChdrnum());
		covrpf.setLife(racdpf.getLife());
		covrpf.setCoverage(racdpf.getCoverage());
		covrpf.setRider(racdpf.getRider());
		covrpf.setPlanSuffix(racdpf.getPlanSuffix());
		covrpf=covrpfDAO.getCovrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),"1");
		if (covrpf == null) {
			fatalError600();
		}
		/*covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		/* If COVRMJA-RIDER equal to '00' then we are Processing a Cover*/
		/* Else we are processing Riders.*/
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		wsaaCrtable.set(covrpf.getCrtable());
		if (isEQ(covrpf.getRider(),ZERO)) {
			covrCoverageToScreen1350();
		}
		else {
			covrRiderToScreen1360();
		}
	}

protected void covrCoverageToScreen1350()
	{
		/*COVRAGE*/
		sv.cmpntnum.set(covrpf.getCoverage());
		wsaaCoverage.set(covrpf.getCrtable());
		sv.component.set(wsaaCovrComponent);
		getDescription1370();
		covrStatusDescs1380();
		validateComponent1390();
		/* Store the COVRMJA key for later use in screen Hidden Fields.*/
		sv.hlifeno.set(covrpf.getLife());
		sv.hsuffix.set(covrpf.getPlanSuffix());
		sv.hcoverage.set(covrpf.getCoverage());
		sv.hrider.set(covrpf.getRider());
		/*EXIT*/
	}

protected void covrRiderToScreen1360()
	{
		/*RIDER-SUBFILE*/
		/* Set up and write the current Rider details.*/
		sv.cmpntnum.set(covrpf.getRider());
		wsaaRider.set(covrpf.getCrtable());
		sv.component.set(wsaaRidrComponent);
		getDescription1370();
		covrStatusDescs1380();
		validateComponent1390();
		/* Store the COVRMJA key for later use in screen Hidden Fields.*/
		sv.hlifeno.set(covrpf.getLife());
		sv.hsuffix.set(covrpf.getPlanSuffix());
		sv.hcoverage.set(covrpf.getCoverage());
		sv.hrider.set(covrpf.getRider());
		/*EXIT*/
	}

protected void getDescription1370()
	{
		para1371();
	}

protected void para1371()
	{
		/* Read the contract definition description from table T5687 for*/
		/* the contract held on the client header record.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaCrtable);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.deit.fill(SPACES);
		}
		else {
			sv.deit.set(descIO.getLongdesc());
		}
	}

protected void covrStatusDescs1380()
	{
		para1381();
	}

protected void para1381()
	{
		/* Use the Risk Status and Premium Status codes to read T5681 and*/
		/* T5682 and obtain the short descriptions.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5681);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrpf.getPstatcode());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstatcode.set(SPACES);
		}
		else {
			sv.pstatcode.set(descIO.getShortdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5682);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(covrpf.getStatcode());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.statcode.fill(SPACES);
		}
		else {
			sv.statcode.set(descIO.getShortdesc());
		}
	}

protected void validateComponent1390()
	{
		component1391();
	}

protected void component1391()
	{
		/* Read the valid statii from table T5679.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		headerStatuzCheck1400();
		if (isNE(wsaaValidStatus,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void headerStatuzCheck1400()
	{
		headerStatuzCheck1401();
	}

protected void headerStatuzCheck1401()
	{
		/* Validate the coverage status against T5679.*/
		wsaaValidStatcode.set("N");
		wsaaValidPstatcode.set("N");
		if (isEQ(covrpf.getRider(),SPACES)
		|| isEQ(covrpf.getRider(),"00")) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| validStatcode.isTrue()); wsaaIndex.add(1)){
				if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())) {
					wsaaValidStatcode.set("Y");
					for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
					|| validPstatcode.isTrue()); wsaaIndex.add(1)){
						if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrpf.getPstatcode())) {
							wsaaValidPstatcode.set("Y");
						}
					}
				}
			}
		}
		/* Validate the rider status against T5679.*/
		if (isNE(covrpf.getRider(),SPACES)
		&& isNE(covrpf.getRider(),"00")) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())) {
					wsaaValidStatcode.set("Y");
					for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
					|| validPstatcode.isTrue()); wsaaIndex.add(1)){
						if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],covrpf.getPstatcode())) {
							wsaaValidPstatcode.set("Y");
						}
					}
				}
			}
		}
		if (validStatcode.isTrue()
		&& validPstatcode.isTrue()) {
			wsaaValidStatus = "Y";
		}
	}

protected void callRasaltr1500()
	{
		rasaltr1510();
	}

protected void rasaltr1510()
	{
		readLife1550();
		rasaltrrec.rasaltrRec.set(SPACES);
		rasaltrrec.function.set("ALTR");
		rasaltrrec.chdrcoy.set(chdrpf.getChdrcoy());
		rasaltrrec.chdrnum.set(chdrpf.getChdrnum());
		rasaltrrec.life.set(covtpf.getLife());
		rasaltrrec.coverage.set(covtpf.getCoverage());
		rasaltrrec.rider.set(covtpf.getRider());
		rasaltrrec.planSuffix.set(covtpf.getPlnsfx());
		rasaltrrec.polsum.set(chdrpf.getPolsum());
		rasaltrrec.effdate.set(chdrpf.getPtdate());
		rasaltrrec.batckey.set(wsaaBatckey);
		compute(rasaltrrec.tranno, 0).set(add(chdrpf.getTranno(),1));
		rasaltrrec.l1Clntnum.set(wsaaL1Clntnum);
		rasaltrrec.l2Clntnum.set(wsaaL2Clntnum);
		rasaltrrec.fsuco.set(wsspcomn.fsuco);
		rasaltrrec.cnttype.set(chdrpf.getCnttype());
		rasaltrrec.cntcurr.set(chdrpf.getCntcurr());
		rasaltrrec.crtable.set(covtpf.getCrtable());
		rasaltrrec.newSumins.set(covtpf.getSumins());
		rasaltrrec.oldSumins.set(ZERO);
		rasaltrrec.language.set(wsspcomn.language);
		rasaltrrec.crrcd.set(covtpf.getEffdate());
		callProgram(Rasaltr.class, rasaltrrec.rasaltrRec);
		if (isNE(rasaltrrec.statuz,varcom.oK)
		&& isNE(rasaltrrec.statuz,"FACL")) {
			syserrrec.statuz.set(rasaltrrec.statuz);
			syserrrec.params.set(rasaltrrec.rasaltrRec);
			fatalError600();
		}
		if (isNE(rasaltrrec.statuz,varcom.oK)) {
			wsaaStatuz.set(rasaltrrec.statuz);
		}

	}

protected void readLife1550()
	{
		life1551();
	}

protected void life1551()
	{
		/* Obtain the Life Assured and Joint Life, if one exists.          */


		for(Lifepf l:lifepfList){
			if(l.getLife() == covtpf.getLife() && l.getJlife() == "00"){
				wsaaL1Clntnum.set(l.getLifcnum());
		}

			if(l.getLife() == covtpf.getLife() && l.getJlife() == "01"){
				wsaaL2Clntnum.set(l.getLifcnum());
		}
		else {
			wsaaL2Clntnum.set(SPACES);
			}
		}
	}

protected void callCsncalc1600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					csncalc1610();
				case next1680: 
					next1680();
				case exit1690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void csncalc1610()
	{
		if (isEQ(lifepf.getLife(),wsaaLife)) {
			goTo(GotoLabel.next1680);
		}
		wsaaLife.set(lifepf.getLife());
		csncalcrec.csncalcRec.set(SPACES);
		csncalcrec.function.set("COVT");
		csncalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		csncalcrec.chdrnum.set(chdrpf.getChdrnum());
		/* MOVE '01'                   TO CSNC-LIFE.                    */
		csncalcrec.life.set(lifepf.getLife());
		csncalcrec.cnttype.set(chdrpf.getCnttype());
		csncalcrec.currency.set(chdrpf.getCntcurr());
		csncalcrec.fsuco.set(wsspcomn.fsuco);
		csncalcrec.language.set(wsspcomn.language);
		csncalcrec.incrAmt.set(ZERO);
		csncalcrec.effdate.set(chdrpf.getPtdate());
		csncalcrec.tranno.set(chdrpf.getTranno());
		csncalcrec.planSuffix.set(ZERO);
		csncalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Csncalc.class, csncalcrec.csncalcRec);
		if (isNE(csncalcrec.statuz,varcom.oK)
		&& isNE(csncalcrec.statuz,"FACL")) {
			syserrrec.statuz.set(csncalcrec.statuz);
			syserrrec.params.set(csncalcrec.csncalcRec);
			fatalError600();
		}
		if (isNE(csncalcrec.statuz,varcom.oK)) {
			wsaaStatuz.set(csncalcrec.statuz);
		}
	}

protected void next1680()
	{
		//lifeenqIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		if (noFacl.isTrue()
		|| resetProgramStack.isTrue()
		|| !atSubmissionReqd.isTrue()) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		else {
			scrnparams.function.set(varcom.init);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SR574IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      SR574-DATA-AREA            */
		/*                                      SR574-SUBFILE-AREA.        */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (noFacl.isTrue()) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR574", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		wsaaCompSelected.set("N");
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		if (!compSelected.isTrue()) {
			scrnparams.errorCode.set(r067);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateSelect2110();
				case updateErrorIndicators2120: 
					updateErrorIndicators2120();
					readNextModifiedRecord2130();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSelect2110()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		/* Validation of the OPTSWCH function. The 'CHCK' function*/
		/* validates the selections made on the Calling Screen.*/
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
		}
	}

protected void updateErrorIndicators2120()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(sv.select,SPACES)) {
				wsaaCompSelected.set("Y");
			}
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR574", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2130()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SR574", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* Bypass this section if returning from a previous use of this*/
		/* program.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (noFacl.isTrue()) {
			if (atSubmissionReqd.isTrue()) {
				sftlckCallAt3100();
			}
			else {
				rlseSftlock3200();
			}
		}
		/*EXIT*/
	}

protected void sftlckCallAt3100()
	{
		softlock3110();
	}

protected void softlock3110()
	{
		/* Softlock contract for AT request.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("TOAT");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/*  Call the AT module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("PR574AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		atreqrec.primaryKey.set(chdrpf.getChdrnum());
		wsaaPolsum.set(chdrpf.getPolsum());
		/* MOVE COVTMJA-EFFDATE      TO WSAA-EFFDATE.          <CAS1.0> */
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
		wsaaMsgnum.set(chdrpf.getChdrnum());
		wsspcomn.msgarea.set(wsaaMsgarea);
	}

protected void rlseSftlock3200()
	{
		unlockContract3210();
	}

protected void unlockContract3210()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
					nextProgram4010();
					optswch4080();
				}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (noFacl.isTrue()
		|| resetProgramStack.isTrue()
		|| !atSubmissionReqd.isTrue()) {
			return ;
		}
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.sstrt);
			screenio4100();
		}
		while ( !(isNE(sv.select,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			scrnparams.statuz.set(varcom.oK);
			scrnparams.function.set(varcom.srdn);
			screenio4100();
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			return ;
		}
		/* If End of File call OPTSWCH a final time to reset the stack.*/
		/* Initialise the Selection Type and Option Number fields before*/
		/* call.*/
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			optswchrec.optsSelType.set(SPACES);
			optswchrec.optsSelOptno.set(ZERO);
			return ;
		}
		if (isEQ(sv.select,"1")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelCode.set(SPACES);
			sv.select.set(SPACES);
			covtmjaIO.setParams(SPACES);
			covtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
			covtmjaIO.setChdrnum(chdrpf.getChdrnum());
			covtmjaIO.setLife(sv.hlifeno);
			covtmjaIO.setPlanSuffix(sv.hsuffix);
			covtmjaIO.setCoverage(sv.hcoverage);
			covtmjaIO.setRider(sv.hrider);
			covtmjaIO.setFunction(varcom.reads);
			covtmjaIO.setFormat(covtmjarec);
			SmartFileCode.execute(appVars, covtmjaIO);
			if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covtmjaIO.getParams());
				fatalError600();
			}
			scrnparams.function.set(varcom.supd);
			screenio4100();
		}
	}

protected void optswch4080()
	{
		programStack4200();
		if (isEQ(optswchrec.optsStatuz,varcom.endp)
		|| resetProgramStack.isTrue()) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			wsaaResetProgramStack.set("N");
			wsaaFirstTime.set("Y");
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		/*EXIT*/
	}

protected void screenio4100()
	{
		/*CALL*/
		processScreen("SR574", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,scrnparams.statuz)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void programStack4200()
	{
		/*STCK*/
		/* The 'STCK' function saves the original switching stack and*/
		/* replaces it with the stack specified on T1661.*/
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*EXIT*/
	}
}