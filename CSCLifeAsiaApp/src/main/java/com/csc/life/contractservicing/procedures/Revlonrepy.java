/*
 * File: Revploan.java
 * Date: 30 August 2009 2:10:48
 * Author: Quipoz Limited
 * 
 * Class transformed from REVPLOAN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.contractservicing.dataaccess.dao.RpdetpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.RplnpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rpdetpf;
import com.csc.life.contractservicing.dataaccess.model.Rplnpf;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*         REVERSE LOAN REPAYMENT PROPOPSAL 
*         --------------------------------
*
* This subroutine is called to provide reversal of a Loan repayment 
*  done against a contract and loan.
* The subroutine requires an entry on T6661.
*
* The subroutine will read each ACMVREV associated with the Loan
*  we want to Reverse, multiply the amount on the ACMVREV by -1
*  and write another ACMV with the new value - thus cancelling
*  out the posting.
* Having done the 'Reverse Postings' for the Loan, the subroutine
*  will then delete the LOAN record from the LOANPF file.
*
* TABLES USED
* -----------
*
*  T1688  -  Transaction Codes
*
*****************************************************************
* </pre>
*/
public class Revlonrepy extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVPLOAN";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);

	
		/* TABLES */
	private static final String t1688 = "T1688";
	private Descpf descpf =null;
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Reverserec reverserec = new Reverserec();
	private DescDAO descDao = getApplicationContext().getBean("descDAO", DescDAO.class);
	private RplnpfDAO rplnpfDAO = getApplicationContext().getBean("rplnpfDAO", RplnpfDAO.class);
	private RpdetpfDAO rpdetpfDAO = getApplicationContext().getBean("rpdetpfDAO", RpdetpfDAO.class);
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	private LoanpfDAO loanpfDAO = getApplicationContext().getBean("loanpfDAO", LoanpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Rplnpf rplnpf= new Rplnpf();
	private Rpdetpf rpdetpf= new Rpdetpf();
	private Acmvpf acmvpf= new Acmvpf();
	private Loanpf loanpf = null;
	private Loanpf loanobj = null;
	private List<Chdrpf> chdrpflist = null;
	private Chdrpf chdrobj = null;
	private List<Acmvpf> acmvpflist= new ArrayList<Acmvpf>();
	private List<Loanpf> loanpflist= new ArrayList<Loanpf>();
	private String pendingStatus="P";
	public Revlonrepy() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		reverserec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaRldgacct.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		acmvpflist = acmvpfDAO.getAcmvPtrnRecords(reverserec.company.toString(),reverserec.chdrnum.toString(),reverserec.tranno.toInt());
		for (Acmvpf acmvobj : acmvpflist){
			if (isEQ(acmvobj.getBatctrcde(),reverserec.oldBatctrcde)) {
				/* Post opposite ACMV record.*/
				postRevAcmvs3000(acmvobj);
				/*  get the Loan number so we can delete the Loan record*/
				wsaaRldgacct.set(acmvobj.getRldgacct());
				if(isEQ(acmvobj.getSacstyp(),"AO") || isEQ(acmvobj.getSacstyp(),"LO"))
						deleteLoanRepayRcd();
				
			/*	acmvrevIO.setFunction(varcom.nextr);*/
			}
		}
		
		modifyRplnpfRecord();
		modifyRpdetpfRecord();
		processChdr6000();
	}

protected void exit1090()
	{
		exitProgram();
	}


protected void postRevAcmvs3000(Acmvpf acmvobj)
	{		
		/* Now post the reversal ACMV record*/
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(reverserec.company);
		lifacmvrec.rldgcoy.set(reverserec.company);
		lifacmvrec.genlcoy.set(reverserec.company);
		lifacmvrec.batcactyr.set(reverserec.batcactyr);
		lifacmvrec.batctrcde.set(reverserec.batctrcde);
		lifacmvrec.batcactmn.set(reverserec.batcactmn);
		lifacmvrec.batcbatch.set(reverserec.batcbatch);
		lifacmvrec.batcbrn.set(reverserec.batcbrn);
		lifacmvrec.rdocnum.set(reverserec.chdrnum);
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvobj.getSacscode());
		lifacmvrec.sacstyp.set(acmvobj.getSacstyp());
		lifacmvrec.glcode.set(acmvobj.getGlcode());
		lifacmvrec.glsign.set(acmvobj.getGlsign());
		lifacmvrec.jrnseq.set(acmvobj.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvobj.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvobj.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvobj.getRldgacct());
		lifacmvrec.origcurr.set(acmvobj.getOrigcurr());
		compute(lifacmvrec.origamt, 2).set(mult(acmvobj.getOrigamt(),-1));
		compute(lifacmvrec.acctamt, 2).set(mult(acmvobj.getAcctamt(),-1));
		/* MULTIPLY ACMVREV-RCAMT      BY -1 GIVING LIFA-RCAMT.         */
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(reverserec.company);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvobj.getTranref());
		getDescription3100();
		/* MOVE REVE-EFFDATE-1         TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvobj.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(acmvobj.getTermid());
		lifacmvrec.user.set(acmvobj.getUsrprf());
		/* MOVE ACMVREV-TRANSACTION-DATE TO LIFA-TRANSACTION-TIME.      */
		/* MOVE ACMVREV-TRANSACTION-TIME TO LIFA-TRANSACTION-DATE.      */
		lifacmvrec.transactionDate.set(acmvobj.getTrdt());
		lifacmvrec.transactionTime.set(acmvobj.getTrtm());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError9500();
		}
	}

protected void getDescription3100()
	{
		descpf = descDao.getdescData("IT", t1688, reverserec.batctrcde.toString(),reverserec.company.toString(),reverserec.language.toString());
		if(descpf!=null)
		{
			lifacmvrec.trandesc.set(descpf.getLongdesc());
		} else {
			lifacmvrec.trandesc.set(SPACES);
		}	
	}

protected void deleteLoanRepayRcd()
	{		
		
	    loanpflist = loanpfDAO.loadValidflagRecord(reverserec.company.toString(),reverserec.chdrnum.toString(),wsaaRldgLoanno.toString(),"1");
		if(!loanpflist.isEmpty()){
			loanpf =new Loanpf();
			loanpf = loanpflist.get(0);
			if(isEQ(loanpf.getFtranno(),reverserec.tranno)){
				loanpfDAO.deleteLoanpfRecord(loanpf);
			}
		}
		else{
			syserrrec.params.set(reverserec.company.toString().concat("Loanpf"));
			databaseError9500();
		}
	  loanpflist = loanpfDAO.loadValidflagRecord(reverserec.company.toString(), reverserec.chdrnum.toString(), wsaaRldgLoanno.toString(),"2");
	  if(!loanpflist.isEmpty()){
		  	loanobj =new Loanpf();
		  	loanobj = loanpflist.get(0);
		  	loanobj.setValidflag("1");
		  	if(isEQ(loanobj.getLtranno(),reverserec.tranno))
		  		loanpfDAO.updateLoanpftranno(loanobj);
	  }
	}

protected void processChdr6000()
{
	
	/*  Read and delete current CHDR record.                        */
	chdrobj = chdrpfDAO.getChdrpf(reverserec.company.toString(), reverserec.chdrnum.toString(), reverserec.tranno.toInt());
	if(chdrobj != null){
		if(isEQ(chdrobj.getValidflag(),"1")){
			chdrpfDAO.deleteChdrByTranno(chdrobj);
		}
	}
	chdrpflist = chdrpfDAO.getChdrpfByChdrnumList("CH",reverserec.company.toString(), reverserec.chdrnum.toString(),"2");
	 if(!chdrpflist.isEmpty()){
		 chdrobj =new Chdrpf();
		 chdrobj = chdrpflist.get(0);
		 chdrobj.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrpfDAO.updateChdrValidflag(chdrobj,"1");
	}
}  
protected void modifyRplnpfRecord(){
	rplnpf.setChdrnum(reverserec.chdrnum.toString());
	rplnpf.setAprvdate(99999999);
	rplnpf.setStatus(pendingStatus);
	rplnpf.setTranno(reverserec.tranno.toInt());
	rplnpfDAO.updateRploantrannoRecord(rplnpf);
	
}
protected void modifyRpdetpfRecord(){
	rpdetpf.setChdrnum(reverserec.chdrnum.toString());
	rpdetpf.setApprovaldate(99999999);
	rpdetpf.setStatus("");
	rpdetpf.setRepayamt(0);
	rpdetpf.setRepaystatus(" ");
	rpdetpf.setTranno(reverserec.tranno.toInt());
	rpdetpfDAO.updateRpdetpftrannoRecord(rpdetpf);
	
}
protected void systemError9000()
	{
			start9000();
			exit9490();
		}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
			start9500();
			exit9990();
		}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
