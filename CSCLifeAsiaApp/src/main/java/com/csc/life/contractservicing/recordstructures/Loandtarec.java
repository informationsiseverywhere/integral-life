package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: 16 nov,2017
 * Description:
 * Copybook name: LOANDTAREC
 *
 *#ILIFE-5744 LIFE VPMS Externalization - Code promotion for Policy Loans Calculation for TEN product
 *
 */
public class Loandtarec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData loandtarec = new FixedLengthStringData(218);
  	
  	public ZonedDecimalData loanNumber = new ZonedDecimalData(3, 0).isAPartOf(loandtarec, 0).setUnsigned();
  	public FixedLengthStringData loanCurrency = new FixedLengthStringData(3).isAPartOf(loandtarec, 3);
	public FixedLengthStringData nofloan = new FixedLengthStringData(1).isAPartOf(loandtarec, 6);

	public FixedLengthStringData loanorigam = new FixedLengthStringData(50).isAPartOf(loandtarec, 7);
	public PackedDecimalData loanorigam01 = new PackedDecimalData(9,0).isAPartOf(loanorigam, 0);
	public PackedDecimalData loanorigam02 = new PackedDecimalData(9,0).isAPartOf(loanorigam,5 );
	public PackedDecimalData loanorigam03 = new PackedDecimalData(9,0).isAPartOf(loanorigam,10 );
	public PackedDecimalData loanorigam04 = new PackedDecimalData(9,0).isAPartOf(loanorigam,15 );
	public PackedDecimalData loanorigam05 = new PackedDecimalData(9,0).isAPartOf(loanorigam,20 );
	public PackedDecimalData loanorigam06 = new PackedDecimalData(9,0).isAPartOf(loanorigam,25 );
	public PackedDecimalData loanorigam07 = new PackedDecimalData(9,0).isAPartOf(loanorigam,30 );
	public PackedDecimalData loanorigam08 = new PackedDecimalData(9,0).isAPartOf(loanorigam,35 );
	public PackedDecimalData loanorigam09 = new PackedDecimalData(9,0).isAPartOf(loanorigam,40);
	public PackedDecimalData loanorigam10 = new PackedDecimalData(9,0).isAPartOf(loanorigam,45);
	

	
	public FixedLengthStringData loanStartDate = new FixedLengthStringData(50).isAPartOf(loandtarec,57);
	public PackedDecimalData loanStartDate01 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,0);
	public PackedDecimalData loanStartDate02 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,5);
	public PackedDecimalData loanStartDate03 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,10);
	public PackedDecimalData loanStartDate04 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,15);
	public PackedDecimalData loanStartDate05 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,20);
	public PackedDecimalData loanStartDate06 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,25);
	public PackedDecimalData loanStartDate07 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,30);
	public PackedDecimalData loanStartDate08 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,35);
	public PackedDecimalData loanStartDate09 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,40);
	public PackedDecimalData loanStartDate10 = new PackedDecimalData(8,0).isAPartOf(loanStartDate,45);
    
	public FixedLengthStringData loanType = new FixedLengthStringData(1).isAPartOf(loandtarec,107);

    
	
	public FixedLengthStringData interestFrom = new FixedLengthStringData(50).isAPartOf(loandtarec,108);
	public PackedDecimalData interestFrom01 = new PackedDecimalData(8,0).isAPartOf(interestFrom,0);
	public PackedDecimalData interestFrom02 = new PackedDecimalData(8,0).isAPartOf(interestFrom,5);
	public PackedDecimalData interestFrom03 = new PackedDecimalData(8,0).isAPartOf(interestFrom,10);
	public PackedDecimalData interestFrom04 = new PackedDecimalData(8,0).isAPartOf(interestFrom,15);
	public PackedDecimalData interestFrom05 = new PackedDecimalData(8,0).isAPartOf(interestFrom,20);
	public PackedDecimalData interestFrom06 = new PackedDecimalData(8,0).isAPartOf(interestFrom,25);
	public PackedDecimalData interestFrom07 = new PackedDecimalData(8,0).isAPartOf(interestFrom,30);
	public PackedDecimalData interestFrom08 = new PackedDecimalData(8,0).isAPartOf(interestFrom,35);
	public PackedDecimalData interestFrom09 = new PackedDecimalData(8,0).isAPartOf(interestFrom,40);
	public PackedDecimalData interestFrom10 = new PackedDecimalData(8,0).isAPartOf(interestFrom,45);
	
	public FixedLengthStringData interestTo = new FixedLengthStringData(50).isAPartOf(loandtarec,158);
	public PackedDecimalData interestTo01 = new PackedDecimalData(8,0).isAPartOf(interestTo,0);
	public PackedDecimalData interestTo02 = new PackedDecimalData(8,0).isAPartOf(interestTo,5);
	public PackedDecimalData interestTo03 = new PackedDecimalData(8,0).isAPartOf(interestTo,10);
	public PackedDecimalData interestTo04 = new PackedDecimalData(8,0).isAPartOf(interestTo,15);
	public PackedDecimalData interestTo05 = new PackedDecimalData(8,0).isAPartOf(interestTo,20);
	public PackedDecimalData interestTo06 = new PackedDecimalData(8,0).isAPartOf(interestTo,25);
	public PackedDecimalData interestTo07 = new PackedDecimalData(8,0).isAPartOf(interestTo,30);
	public PackedDecimalData interestTo08 = new PackedDecimalData(8,0).isAPartOf(interestTo,35);
	public PackedDecimalData interestTo09 = new PackedDecimalData(8,0).isAPartOf(interestTo,40);
	public PackedDecimalData interestTo10 = new PackedDecimalData(8,0).isAPartOf(interestTo,45);
	
	public FixedLengthStringData loanSign = new FixedLengthStringData(10).isAPartOf(loandtarec,208);
	public FixedLengthStringData loanSign01 = new FixedLengthStringData(1).isAPartOf(loanSign, 0);
  	public FixedLengthStringData loanSign02 = new FixedLengthStringData(1).isAPartOf(loanSign, 1);
  	public FixedLengthStringData loanSign03 = new FixedLengthStringData(1).isAPartOf(loanSign, 2);
  	public FixedLengthStringData loanSign04 = new FixedLengthStringData(1).isAPartOf(loanSign, 3);
  	public FixedLengthStringData loanSign05 = new FixedLengthStringData(1).isAPartOf(loanSign, 4);
  	public FixedLengthStringData loanSign06 = new FixedLengthStringData(1).isAPartOf(loanSign, 5);
  	public FixedLengthStringData loanSign07 = new FixedLengthStringData(1).isAPartOf(loanSign, 6);
  	public FixedLengthStringData loanSign08 = new FixedLengthStringData(1).isAPartOf(loanSign, 7);
  	public FixedLengthStringData loanSign09 = new FixedLengthStringData(1).isAPartOf(loanSign, 8);
  	public FixedLengthStringData loanSign10 = new FixedLengthStringData(1).isAPartOf(loanSign, 9);
	
	
	public void initialize() {
		COBOLFunctions.initialize(loandtarec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			loandtarec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}