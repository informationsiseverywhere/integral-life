
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.contractservicing.screens.Sd5ijScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
/**
 * 
 * MULTIPLE OWNER SCREEN
 *
 */

public class Pd5ij extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5IJ");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private static final String rrgz = "RRGZ";
	private static final String rg43 = "RG43";
	private static final String f782 = "F782";
	private static final String e304 = "E304";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Clntpf clntpf= new Clntpf();
	private Chdrpf chdrpf=null;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	boolean isUpdate=false;
	boolean isDelete=false;
	List<String> clrrToDeleteList = null;
	List<String> clrrToUpdateList = null;
	private Batckey wsaaBatckey = new Batckey();
	private Sd5ijScreenVars sv = ScreenProgram.getScreenVars( Sd5ijScreenVars.class);
	private static final Logger LOGGER = LoggerFactory.getLogger(Pd5ij.class);


	public Pd5ij() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5ij", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}
	@Override
	public void mainline(Object... parmArray)
		{
			sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
			scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
			wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
			wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
			try {
				super.mainline();
			}
			catch (Exception e) {
				LOGGER.error("Exception",e);
			}
		}
	@Override
	public void processBo(Object... parmArray) {
			sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
			scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
			wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
			wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
	
			try {
				processBoMainline(sv, sv.dataArea, parmArray);
			} catch (Exception e) {
				LOGGER.error("Exception",e);
			}
		}
	@Override
	protected void initialise1000()
		{
			initialise();
			checkInquiry();
		}
	
	protected void initialise()
		{
		
		wsaaBatckey.set(wsspcomn.batchkey);
		if(isEQ(wsspcomn.flag,"I") && isNE(wsaaBatckey.batcBatctrcde,"T509")) {
			chdrpf= new Chdrpf();
			chdrpf = chdrpfDAO.getCacheObject(chdrpf);//ALS - 4563
			if(chdrpf !=null)
			{
				sv.owner1.set(chdrpf.getCownnum());
				sv.owner2.set(chdrpf.getCownnum2());
				sv.owner3.set(chdrpf.getCownnum3());
				sv.owner4.set(chdrpf.getCownnum4());
				sv.owner5.set(chdrpf.getCownnum5());
			}	
		}
		else{			
			retrieveChdrlnbRecord();
		}	
		readClntpf();
	}
	
	protected void retrieveChdrlnbRecord()
	{
		sv.dataArea.set(SPACES);
		chdrlnbIO.setFunction(Varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFunction(Varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}			
		sv.owner1.set(wsspcomn.chdrCownnum);
		sv.owner2.set(chdrlnbIO.getCownnum2());
		sv.owner3.set(chdrlnbIO.getCownnum3());
		sv.owner4.set(chdrlnbIO.getCownnum4());
		sv.owner5.set(chdrlnbIO.getCownnum5());
	}

	protected void readClntpf() {
		clntpf = new Clntpf();
		for (int i = 1; i <= sv.owner.length - 1; i++) {
			if (isNE(sv.owner[i], SPACES)) {
				clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.owner[i].toString());
				if (clntpf == null) {
					sv.ownerErr[i].set(e304);
					sv.ownname[i].set(SPACES);
					wsspcomn.edterror.set("Y");
				} else {
					if (clntpf != null) {
						plainname(clntpf);
						sv.ownname[i].set(wsspcomn.longconfname.toString());
						if(i>1) {
							validateClient(clntpf, i);
						}
					} else {
						sv.ownname[i].set(SPACES);
					}
				}
			}else {
				sv.ownname[i].set(SPACES);
			} 	
		}
	}
	protected  void validateClient(Clntpf clntpf,int i)
	{
		//Check for Dead Client
		if (isNE(chdrlnbIO.getOccdate(), varcom.vrcmMaxDate) && isLT(clntpf.getCltdod(), chdrlnbIO.getOccdate())) {
			sv.ownerErr[i].set(f782);
		}	
		//Validate for Personal client
		if(clntpf!=null && isEQ(clntpf.getClttype(),"C"))
		{
			sv.ownerErr[i].set(rg43);
		}	
	}
	protected void largename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname(clntpf);
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

	protected void plainname(Clntpf clntpf)
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname(clntpf);
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}
	protected void corpname(Clntpf clntpf)
	{
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
	}

	protected void checkInquiry()
	{
		//In inquiry mode all fields are protected(non-editable)
		if (isEQ(wsspcomn.flag,"I")) {
			sv.owner2Out[Varcom.pr.toInt()].set("Y");
			sv.owner3Out[Varcom.pr.toInt()].set("Y");
			sv.owner4Out[Varcom.pr.toInt()].set("Y");
			sv.owner5Out[Varcom.pr.toInt()].set("Y");
			sv.owner2Out[Varcom.pr.toInt()].set("Y");
			sv.ownname2Out[Varcom.pr.toInt()].set("Y");
			sv.ownname3Out[Varcom.pr.toInt()].set("Y");
			sv.ownname4Out[Varcom.pr.toInt()].set("Y");
			sv.ownname5Out[Varcom.pr.toInt()].set("Y");
		}	
	}
	@Override
	protected void screenEdit2000()
	{
		try {
			screenIo();
			validate();
		}
		catch (Exception e){
			LOGGER.error("Exception",e);
		}
	}
	protected void screenIo()
	{
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz,Varcom.kill)) {
			return;
		}
		if (isEQ(scrnparams.statuz,Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}
	protected void validate()
	{				
		//Validation for Personal Client
		readClntpf();
		//Validation for Duplicate Client
		checkDuplicateClient();
	}
	protected void checkDuplicateClient()
	{
		if(isNE(sv.owner2,SPACES) &&( sv.owner2.toString().equals(sv.owner1.toString()) || sv.owner2.toString().equals(sv.owner3.toString()) || sv.owner2.toString().equals(sv.owner4.toString()) || sv.owner2.toString().equals(sv.owner5.toString())))
			sv.owner2Err.set(rrgz);
		else if(isNE(sv.owner3,SPACES) &&(sv.owner3.toString().equals(sv.owner1.toString()) || sv.owner3.toString().equals(sv.owner2.toString()) || sv.owner3.toString().equals(sv.owner4.toString()) || sv.owner3.toString().equals(sv.owner5.toString())))
			sv.owner3Err.set(rrgz);
		else if(isNE(sv.owner4,SPACES) &&(sv.owner4.toString().equals(sv.owner1.toString()) || sv.owner4.toString().equals(sv.owner2.toString()) || sv.owner4.toString().equals(sv.owner3.toString()) || sv.owner4.toString().equals(sv.owner5.toString())))
			sv.owner4Err.set(rrgz);
		else if(isNE(sv.owner5,SPACES) &&(sv.owner5.toString().equals(sv.owner1.toString()) || sv.owner5.toString().equals(sv.owner2.toString()) || sv.owner5.toString().equals(sv.owner3.toString()) || sv.owner5.toString().equals(sv.owner4.toString())))
			sv.owner5Err.set(rrgz);
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
	@Override
	protected void update3000()
	{
		try {
			updateDatabase();
			if (isEQ(wsspcomn.flag,"I"))//ALS - 4563
				{
				return;
				}
			contractHeader();
		}
		catch (Exception e){
			LOGGER.error("Exception",e);
		}
	}	
	protected void updateDatabase()
	{
		if (isEQ(scrnparams.statuz,Varcom.kill)) {
			return;
		}
		if (isEQ(wsspcomn.flag,"I")) {
			return;
		}
		cltrelnrec.cltrelnRec.set(SPACES);
	}	
	protected void contractHeader()
	{
		updateClrrpf();
		chdrlnbIO.setCownum2(sv.owner2);
		chdrlnbIO.setCownum3(sv.owner3);
		chdrlnbIO.setCownum4(sv.owner4);
		chdrlnbIO.setCownum5(sv.owner5);
		chdrlnbIO.setFunction(Varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}	 
	}
	protected void updateClrrpf()
	{
		if (isNE(sv.owner2, chdrlnbIO.getCownnum2())) {
			performAction(sv.owner2.toString(),chdrlnbIO.getCownnum2().toString());
		}
		if(isNE(sv.owner3, chdrlnbIO.getCownnum3()))
		{
			performAction(sv.owner3.toString(),chdrlnbIO.getCownnum3().toString());
		}
		if(isNE(sv.owner4, chdrlnbIO.getCownnum4()))
		{
			performAction(sv.owner4.toString(),chdrlnbIO.getCownnum4().toString());
		}
		if(isNE(sv.owner5, chdrlnbIO.getCownnum5()))
		{
			performAction(sv.owner5.toString(),chdrlnbIO.getCownnum5().toString());
		}
		
		//Set Common values
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clrrrole.set("OW");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(wsspcomn.company);
		cltrelnrec.forenum.set(wsspcomn.chdrChdrnum);
			
		//If any owner is changed, delete the earlier owner's entry from CLRRPF
		if(isDelete) 
			callDelCltreln(clrrToDeleteList);
		//If any owner newly added, add this new entry in CLRRPF
		if(isUpdate)
			callAddCltreln(clrrToUpdateList);		
	}
	protected void performAction(String input1, String input2)
	{
		if(clrrToDeleteList == null)
			clrrToDeleteList = new ArrayList<>();
		if(clrrToUpdateList == null)
			clrrToUpdateList = new ArrayList<>();
		if(isEQ(wsspcomn.flag ,"M") && isNE(input2,SPACES) )
		{
			clrrToDeleteList.add(input2.trim());
			isDelete=true;
		}
		if(isNE(input1,SPACES))
		{
			clrrToUpdateList.add(input1.trim());
			isUpdate = true;
		}
	}
	protected void callDelCltreln(List<String> clrrToDeleteList)
	{
		for(String clntnum: clrrToDeleteList) {
			
			cltrelnrec.clntnum.set(clntnum);
			cltrelnrec.function.set("DEL  ");
			callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
			if (isNE(cltrelnrec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(cltrelnrec.statuz);
				fatalError600();
			}
		}
		isDelete=false;
	}
	protected void callAddCltreln(List<String> clrrToUpdateList)
	{	
		for(String clntnum: clrrToUpdateList) {	
			
			cltrelnrec.clntnum.set(clntnum);			
			cltrelnrec.function.set("ADD");			
			callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
			if (isNE(cltrelnrec.statuz,Varcom.oK)) {
				syserrrec.statuz.set(cltrelnrec.statuz);
				fatalError600();
			}
		}	
		isUpdate=false;
	}
	@Override
	protected void whereNext4000()
	{
			wsspcomn.programPtr.add(1);
			scrnparams.function.set("HIDEW");
	}
	@Override
	protected void preScreenEdit() {
		return;
	}
}
