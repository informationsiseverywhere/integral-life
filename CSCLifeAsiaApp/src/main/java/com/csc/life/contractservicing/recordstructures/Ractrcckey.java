package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:25
 * Description:
 * Copybook name: RACTRCCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ractrcckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ractrccFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ractrccKey = new FixedLengthStringData(64).isAPartOf(ractrccFileKey, 0, REDEFINE);
  	public FixedLengthStringData ractrccChdrcoy = new FixedLengthStringData(1).isAPartOf(ractrccKey, 0);
  	public FixedLengthStringData ractrccChdrnum = new FixedLengthStringData(8).isAPartOf(ractrccKey, 1);
  	public FixedLengthStringData ractrccLife = new FixedLengthStringData(2).isAPartOf(ractrccKey, 9);
  	public FixedLengthStringData ractrccCoverage = new FixedLengthStringData(2).isAPartOf(ractrccKey, 11);
  	public FixedLengthStringData ractrccRider = new FixedLengthStringData(2).isAPartOf(ractrccKey, 13);
  	public FixedLengthStringData ractrccRasnum = new FixedLengthStringData(8).isAPartOf(ractrccKey, 15);
  	public FixedLengthStringData ractrccRatype = new FixedLengthStringData(4).isAPartOf(ractrccKey, 23);
  	public FixedLengthStringData ractrccValidflag = new FixedLengthStringData(1).isAPartOf(ractrccKey, 27);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(ractrccKey, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ractrccFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ractrccFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}