package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FcfipfTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:44
 * Class transformed from FCFIPF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FcfipfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 143;
	public FixedLengthStringData fcfirec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData fcfipfRecord = fcfirec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(fcfirec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(fcfirec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(fcfirec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(fcfirec);
	public FixedLengthStringData ratebas = DD.ratebas.copy().isAPartOf(fcfirec);
	public PackedDecimalData medamt01 = DD.medamt.copy().isAPartOf(fcfirec);
	public PackedDecimalData medamt02 = DD.medamt.copy().isAPartOf(fcfirec);
	public PackedDecimalData admamt01 = DD.admamt.copy().isAPartOf(fcfirec);
	public PackedDecimalData admamt02 = DD.admamt.copy().isAPartOf(fcfirec);
	public FixedLengthStringData sacscode01 = DD.sacscode.copy().isAPartOf(fcfirec);
	public FixedLengthStringData sacscode02 = DD.sacscode.copy().isAPartOf(fcfirec);
	public FixedLengthStringData sacstyp01 = DD.sacstyp.copy().isAPartOf(fcfirec);
	public FixedLengthStringData sacstyp02 = DD.sacstyp.copy().isAPartOf(fcfirec);
	public PackedDecimalData adjamt01 = DD.adjamt.copy().isAPartOf(fcfirec);
	public PackedDecimalData adjamt02 = DD.adjamt.copy().isAPartOf(fcfirec);
	public PackedDecimalData susamt = DD.susamt.copy().isAPartOf(fcfirec);
	public PackedDecimalData reserveUnitsDate = DD.rundte.copy().isAPartOf(fcfirec);
	public PackedDecimalData actval = DD.actval.copy().isAPartOf(fcfirec);
	public PackedDecimalData ztotamt = DD.ztotamt.copy().isAPartOf(fcfirec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(fcfirec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(fcfirec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(fcfirec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public FcfipfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for FcfipfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public FcfipfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for FcfipfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public FcfipfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for FcfipfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public FcfipfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("FCFIPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"EFFDATE, " +
							"RATEBAS, " +
							"MEDAMT01, " +
							"MEDAMT02, " +
							"ADMAMT01, " +
							"ADMAMT02, " +
							"SACSCODE01, " +
							"SACSCODE02, " +
							"SACSTYP01, " +
							"SACSTYP02, " +
							"ADJAMT01, " +
							"ADJAMT02, " +
							"SUSAMT, " +
							"RUNDTE, " +
							"ACTVAL, " +
							"ZTOTAMT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     effdate,
                                     ratebas,
                                     medamt01,
                                     medamt02,
                                     admamt01,
                                     admamt02,
                                     sacscode01,
                                     sacscode02,
                                     sacstyp01,
                                     sacstyp02,
                                     adjamt01,
                                     adjamt02,
                                     susamt,
                                     reserveUnitsDate,
                                     actval,
                                     ztotamt,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		effdate.clear();
  		ratebas.clear();
  		medamt01.clear();
  		medamt02.clear();
  		admamt01.clear();
  		admamt02.clear();
  		sacscode01.clear();
  		sacscode02.clear();
  		sacstyp01.clear();
  		sacstyp02.clear();
  		adjamt01.clear();
  		adjamt02.clear();
  		susamt.clear();
  		reserveUnitsDate.clear();
  		actval.clear();
  		ztotamt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getFcfirec() {
  		return fcfirec;
	}

	public FixedLengthStringData getFcfipfRecord() {
  		return fcfipfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setFcfirec(what);
	}

	public void setFcfirec(Object what) {
  		this.fcfirec.set(what);
	}

	public void setFcfipfRecord(Object what) {
  		this.fcfipfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(fcfirec.getLength());
		result.set(fcfirec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}