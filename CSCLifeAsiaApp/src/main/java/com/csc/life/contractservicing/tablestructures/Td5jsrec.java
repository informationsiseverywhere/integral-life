package com.csc.life.contractservicing.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Td5jsrec extends ExternalData{
	public FixedLengthStringData td5jsRec = new FixedLengthStringData(1);
	public FixedLengthStringData taxCalcReqd = new FixedLengthStringData(1).isAPartOf(td5jsRec, 0);

	@Override
	public void initialize() {
		COBOLFunctions.initialize(td5jsRec);
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			td5jsRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
}
