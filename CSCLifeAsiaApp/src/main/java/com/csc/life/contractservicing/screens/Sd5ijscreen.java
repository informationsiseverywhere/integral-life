package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5ijscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 8, 1, 52}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5ijScreenVars sv = (Sd5ijScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5ijscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5ijScreenVars screenVars = (Sd5ijScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.owner1.setClassString("");
		screenVars.owner2.setClassString("");
		screenVars.owner3.setClassString("");
		screenVars.owner4.setClassString("");
		screenVars.owner5.setClassString("");
		screenVars.ownname1.setClassString("");
		screenVars.ownname2.setClassString("");
		screenVars.ownname3.setClassString("");
		screenVars.ownname4.setClassString("");
		screenVars.ownname5.setClassString("");
	}


	public static void clear(VarModel pv) {
		Sd5ijScreenVars screenVars = (Sd5ijScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.owner1.clear();
		screenVars.owner2.clear();
		screenVars.owner3.clear();
		screenVars.owner4.clear();
		screenVars.owner5.clear();
		screenVars.ownname1.clear();
		screenVars.ownname2.clear();
		screenVars.ownname3.clear();
		screenVars.ownname4.clear();
		screenVars.ownname5.clear();
	}
}
