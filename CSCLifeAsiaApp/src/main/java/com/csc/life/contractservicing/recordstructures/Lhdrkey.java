package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:04
 * Description:
 * Copybook name: LHDRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lhdrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lhdrFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lhdrKey = new FixedLengthStringData(256).isAPartOf(lhdrFileKey, 0, REDEFINE);
  	public FixedLengthStringData lhdrChdrcoy = new FixedLengthStringData(1).isAPartOf(lhdrKey, 0);
  	public FixedLengthStringData lhdrChdrnum = new FixedLengthStringData(8).isAPartOf(lhdrKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(lhdrKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lhdrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lhdrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}