/*
 * File: PtX10.java
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.general.tablestructures.T3678rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.MandmjaTableDAM;
import com.csc.life.contractservicing.screens.Sr57hScreenVars;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr57h extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR57H");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);

	private FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 0);
	private FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 30);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private static final String wsaaBillchnl = "D";
		/* ERRORS */
	private static final String e756 = "E756";
	private static final String f373 = "F373";
	private static final String f955 = "F955";
	private static final String g600 = "G600";
	private static final String h926 = "H926";
	private static final String h928 = "H928";
	private static final String i011 = "I011";
	private static final String i014 = "I014";
	private static final String i020 = "I020";
		/* TABLES */
	private static final String t3678 = "T3678";
	private static final String clrfrec = "CLRFREC";
	
	private BabrTableDAM babrIO = new BabrTableDAM();
	/* TSD 321 Phase 2 Starts */
	private final ClrrTableDAM clrrIO = new ClrrTableDAM();
	private final CltsTableDAM cltsIO = new CltsTableDAM();
	private static final FixedLengthStringData cltsrec = new FixedLengthStringData(7).init("CLTSREC");
	private static final FixedLengthStringData e655 = new FixedLengthStringData(4).init("E655");
	private static final FixedLengthStringData zm02 = new FixedLengthStringData(4).init("ZM02");
	private static final FixedLengthStringData zm03 = new FixedLengthStringData(4).init("ZM03");
	private static final String wsaaPayer = "PY";
	/* TSD 321 Phase 2 Ends */
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
		/*Client Roles by Foreign Key*/
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Mandate Details*/
	private MandmjaTableDAM mandmjaIO = new MandmjaTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
	private T3678rec t3678rec = new T3678rec();
	private Wssplife wssplife = new Wssplife();
	private Sr57hScreenVars sv = ScreenProgram.getScreenVars( Sr57hScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public Pr57h() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57h", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	@Override
	protected void initialise1000()	{
		initialise1010();
	}

	protected void initialise1010()	{
		wsspcomn.clonekey.set("PR57H");
		sv.dataArea.set(SPACES);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* TSD 321 Phase 2 Starts */
		//sv.payrnum.set(chdrmjaIO.getCownnum());
		//sv.numsel.set(chdrmjaIO.getCownnum());
		sv.payrnum.set(chdrmjaIO.getPayclt());
		sv.numsel.set(chdrmjaIO.getPayclt());
		if (isEQ(chdrmjaIO.getPayclt(), SPACES)) {
			sv.payrnum.set(chdrmjaIO.getCownnum());
			sv.numsel.set(chdrmjaIO.getCownnum());
		}
		cltsIO.setClntpfx(chdrmjaIO.getCownpfx());
		cltsIO.setClntcoy(chdrmjaIO.getCowncoy());
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
		   syserrrec.params.set(cltsIO.getParams());
		   syserrrec.statuz.set(cltsIO.getStatuz());
		   fatalError600();
		}
		plainname();
		sv.payorname.set(wsspcomn.longconfname);
		/* TSD 321 Phase 2 Ends */
		wsaaClntkey.set(SPACES);
		if (isNE(chdrmjaIO.getCownnum(),SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			
			stringVariable1.addExpression(chdrmjaIO.getCownpfx());
			stringVariable1.addExpression(chdrmjaIO.getCowncoy());
			stringVariable1.addExpression(chdrmjaIO.getCownnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		sv.currcode.set(SPACES);
		sv.facthous.set(SPACES);
		sv.billcd.set(payrIO.getBillcd());
		if (isNE(chdrmjaIO.getZmandref(), SPACES)) {
			sv.mandref.set(chdrmjaIO.getZmandref());
		} else {
			sv.mandref.set(SPACES);
			return;
		}
		if (isEQ(chdrmjaIO.getZmandref(), SPACES)) {
			return;
		}
		mandmjaIO.setParams(SPACES);
		mandmjaIO.setPayrcoy(wsspcomn.fsuco);
		mandmjaIO.setPayrnum(sv.payrnum);
		mandmjaIO.setMandref(chdrmjaIO.getZmandref());
		mandmjaIO.setFunction(varcom.readr);
		mandmjaIO.setFormat("MANDMJAREC");
		SmartFileCode.execute(appVars, mandmjaIO);
		if (isNE(mandmjaIO.getStatuz(),varcom.oK)
		&& isNE(mandmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(mandmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(mandmjaIO.getStatuz(),varcom.mrnf)) {
			sv.mandrefErr.set("H929");
			return ;
		}
		clblIO.setParams(SPACES);
		clblIO.setBankkey(mandmjaIO.getBankkey());
		sv.bankkey.set(mandmjaIO.getBankkey());
		clblIO.setBankacckey(mandmjaIO.getBankacckey());
		sv.bankacckey.set(mandmjaIO.getBankacckey());
		clblIO.setClntcoy(chdrmjaIO.getCowncoy());
		clblIO.setClntnum(chdrmjaIO.getCownnum());
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getCownnum(),clblIO.getClntnum())) {
			sv.payrnumErr.set(f373);
			return ;
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.facthous.set(SPACES);
			sv.bankkey.set(SPACES);
			sv.bankacckey.set(SPACES);
			return ;
		}
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		sv.facthous.set(clblIO.getFacthous());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t3678);
		itemIO.setItemitem(mandmjaIO.getMandstat());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3678rec.t3678Rec.set(itemIO.getGenarea());
		if (isEQ(t3678rec.gonogoflg,"N")) {
			sv.mandrefErr.set(i014);
			return ;
		}
		if (isGT(clblIO.getCurrfrom(),mandmjaIO.getEffdate())) {
			sv.bankacckeyErr.set(i020);
			return ;
		}
		if (isNE(clblIO.getCurrto(),ZERO)) {
			if (isLT(clblIO.getCurrto(),mandmjaIO.getEffdate())) {
				sv.bankacckeyErr.set(i020);
				return ;
			}
		}
	}
	
	/* TSD 321 Phase 2 Starts */
	protected void plainname() {
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/

	}
	
	protected void corpname() {
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/

	}
	/* TSD 321 Phase 2 Ends */

	@Override
	protected void preScreenEdit()	{
		/*PRE-START*/
		wsspcomn.clntkey.set(SPACES);
		return ;
		/*PRE-EXIT*/
	}
	
	@Override
	protected void screenEdit2000()	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz,Varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		
		/* TSD 321 Phase 2 Starts*/
		//Check Feature Configuration for MTL321 and proceed to do this feature only if this feature is set to true
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(Varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),Varcom.oK))
		&& (isNE(cltsIO.getStatuz(),Varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),Varcom.mrnf)) {
			sv.payrnumErr.set(e655);
			wsspcomn.edterror.set("Y");
		sv.payorname.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
		plainname();
		sv.payorname.set(wsspcomn.longconfname);
		/* TSD 321 Phase 2 Ends*/
		
		if (isEQ(sv.mandref,SPACES)) {
			sv.mandrefErr.set(h926);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.mandref,SPACES)) {
			if (isNE(sv.mandref,NUMERIC)) {
				sv.mandrefErr.set(i011);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		mandmjaIO.setParams(SPACES);
		mandmjaIO.setPayrcoy(wsspcomn.fsuco);
		mandmjaIO.setPayrnum(sv.payrnum);
		mandmjaIO.setMandref(sv.mandref);
		mandmjaIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, mandmjaIO);
		if (isNE(mandmjaIO.getStatuz(),Varcom.oK)
		&& isNE(mandmjaIO.getStatuz(),Varcom.mrnf)) {
			syserrrec.params.set(mandmjaIO.getParams());
			fatalError600();
		}
		/* TSD 321 Phase 2 Start */
		if (isEQ(mandmjaIO.getStatuz(),Varcom.mrnf) || isEQ(mandmjaIO.getCrcind(), "C") ) {
			sv.mandrefErr.set(zm02);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* TSD 321 Phase 2 Ends */
		
		/* Move bank account details to corresponding fields*/
		sv.bankkey.set(mandmjaIO.getBankkey());
		sv.bankacckey.set(mandmjaIO.getBankacckey());
	
		/* TSD 321 Phase 2 Start */
		//Check Feature Configuration for MTL321 and proceed to do this feature only if this feature is set to true
		//validate client role
		validateClientRole2010();
		/* TSD 321 Phase 2 Ends */
	}
	
	/* TSD 321 Phase 2 Start */
	protected void validateClientRole2010() {
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)
		&& isNE(clrrIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			syserrrec.params.set(clrrIO.getParams());
			
			fatalError600();
		}
		if (isNE(clrrIO.getStatuz(), varcom.oK)
		|| isNE(clrrIO.getClntpfx(), "CN")
		|| isNE(clrrIO.getClntcoy(), wsspcomn.fsuco)
		|| isNE(clrrIO.getClntnum(), sv.payrnum)) {
			clrrIO.setStatuz(varcom.endp);
			return ;
		}
		
		if (isEQ(clrrIO.getStatuz(), varcom.oK)
		&& isEQ(clrrIO.getClntpfx(), "CN")
		&& isEQ(clrrIO.getClntcoy(), wsspcomn.fsuco)
		&& isEQ(clrrIO.getClntnum(), sv.payrnum)
		&& (isEQ(clrrIO.getClrrrole(), wsaaPayer)
			|| isEQ(clrrIO.getClrrrole(), "LA")	
			|| isEQ(clrrIO.getClrrrole(), "OW")
				)) {
			scrnparams.errorCode.set(zm03);
		}
	}
	/* TSD 321 Phase 2 Ends */

	protected void validate2020() {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t3678);
		itemIO.setItemitem(mandmjaIO.getMandstat());
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3678rec.t3678Rec.set(itemIO.getGenarea());
		if (isEQ(t3678rec.gonogoflg,"N")) {
			sv.mandrefErr.set(i014);
			goTo(GotoLabel.checkForErrors2080);
		}
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(e756);
			goTo(GotoLabel.checkForErrors2080);
		}
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)
		&& isNE(clblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankacckeyErr.set(g600);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isGT(clblIO.getCurrfrom(),mandmjaIO.getEffdate())) {
			sv.bankacckeyErr.set(i020);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(clblIO.getCurrto(),ZERO)) {
			if (isLT(clblIO.getCurrto(),mandmjaIO.getEffdate())) {
				sv.bankacckeyErr.set(i020);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.bankacckeyErr.set(f373);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(chdrmjaIO.getBillcurr(),clblIO.getCurrcode())) {
			sv.bankacckeyErr.set(f955);
			goTo(GotoLabel.checkForErrors2080);
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
	}

	protected void checkForErrors2080()	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	@Override
	protected void update3000()	{
		updateDatabase3010();
	}

	protected void updateDatabase3010()	{
		if (isNE(wsaaClntkey,SPACES)) {
			wsspcomn.clntkey.set(wsaaClntkey);
		}
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
			return ;
		}
		chdrmjaIO.setZmandref(sv.mandref);
		/* TSD 321 Phase 2 Starts */
		chdrmjaIO.setPayclt(sv.payrnum);
		/* TSD 321 Phase 2 Ends */
		chdrmjaIO.setFunction(Varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),Varcom.oK)) {
			chdrmjaIO.setParams(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setZmandref(sv.mandref);
		payrIO.setFunction(Varcom.keeps);
		SmartFileCode.execute(appVars, payrIO);
		if ((isNE(payrIO.getStatuz(),Varcom.oK))
		&& (isNE(payrIO.getStatuz(),Varcom.mrnf))) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

	@Override
	protected void whereNext4000()	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		wsspcomn.clonekey.set(SPACES);
		/*EXIT*/
	}
}
