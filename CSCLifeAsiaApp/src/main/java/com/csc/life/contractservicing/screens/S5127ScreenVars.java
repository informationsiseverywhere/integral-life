package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5127
 * @version 1.0 generated on 30/08/09 06:35
 * @author Quipoz
 */
public class S5127ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());//BRD-306 887
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,48);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,51);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,182);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,183);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,187);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,188);
	public ZonedDecimalData premCessDate = DD.pcesdte.copyToZonedDecimal().isAPartOf(dataFields,189);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,197);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,200);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,203);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,207);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,215);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,218);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,221);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,223);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,224);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,225);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,227);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,231);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,246);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,263);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,264);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,277);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,294);
	/*BRD-306 START */
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 311);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 314);
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 344);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 361);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 378);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 395);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 412);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,429);
	public ZonedDecimalData linstamt = DD.linstamt.copyToZonedDecimal().isAPartOf(dataFields,437);
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,454);
	/*BRD-306 END */
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,455);//BRD-NBP-011
	public FixedLengthStringData exclind = DD.optextind.copy().isAPartOf(dataFields,458);
	public FixedLengthStringData tpdtype = DD.tpdtype.copy().isAPartOf(dataFields,459); //ILIFE-7118
	public ZonedDecimalData cashvalarer = DD.cashvalare.copyToZonedDecimal().isAPartOf(dataFields,464);//ICIL-299
	public FixedLengthStringData lnkgind=DD.jpjlnkind.copy().isAPartOf(dataFields,481);//ILIFE-7752
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,482);
	public FixedLengthStringData lnkgno = DD.lnkgno.copy().isAPartOf(dataFields,499);//ILIFE-8248
	public FixedLengthStringData lnkgsubrefno = DD.lnkgsubrefno.copy().isAPartOf(dataFields,549);//ILIFE-8248
	/*IBPLIFE-2137 Start*/
	public FixedLengthStringData trancd = DD.trancd.copy().isAPartOf(dataFields,599);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(dataFields,603);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(dataFields,633);
	public ZonedDecimalData effdatex = DD.effdatex.copyToZonedDecimal().isAPartOf(dataFields,634);
	public FixedLengthStringData covrprpse = DD.covrprpse.copy().isAPartOf(dataFields,642);
	/*IBPLIFE-2137 End*/
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData pcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	/*BRD-306 START */
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData linstamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	/*BRD-306 END */
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);//BRD-NBP-011
	public FixedLengthStringData exclindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData tpdtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192); //ILIFE-7118
	public FixedLengthStringData cashvalarerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);//ICIL-299
	public FixedLengthStringData lnkgindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);//ILIFE-7752
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData lnkgnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);//ILIFE-8248
	public FixedLengthStringData lnkgsubrefnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);//ILIFE-8248
	/*IBPLIFE-2137 Start*/
	public FixedLengthStringData trancdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData trandescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData effdatexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData covrprpseErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	/*IBPLIFE-2137 End*/
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] pcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	/*BRD-306 START */
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] linstamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	/*BRD-306 END */
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);//BRD-NBP-011
	public FixedLengthStringData[] exclindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] tpdtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576); //ILIFE-7118
	public FixedLengthStringData[] cashvalarerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);//ICIL-299
	public FixedLengthStringData[] lnkgindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600); //ILIFE-7752
	public FixedLengthStringData[]zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] lnkgnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);//ILIFE-8248
	public FixedLengthStringData[]  lnkgsubrefnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);//ILIFE-8248
	/*IBPLIFE-2137 Start*/
	public FixedLengthStringData[] trancdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] trandescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData[] effdatexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[] covrprpseOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	/*IBPLIFE-2137 End*/
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	/*BRD-306 START */
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	/*BRD-306 END */

	public FixedLengthStringData premCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdatexDisp = new FixedLengthStringData(10);//IBPLIFE-2137
	public FixedLengthStringData nbprp126lag = new FixedLengthStringData(1);//IBPLIFE-2137

	public LongData S5127screenWritten = new LongData(0);
	public LongData S5127protectWritten = new LongData(0);	

	public boolean hasSubfile() {
		return false;
	}


	public S5127ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminOut,new String[] {"14","02","-14","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcessageOut,new String[] {"15","03","-15","88", null, null, null, null, null, null, null, null});//ILJ-47
		fieldIndMap.put(rcesstrmOut,new String[] {"16","04","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesdteOut,new String[] {"29","28","-29","89", null, null, null, null, null, null, null, null});//ILJ-47
		fieldIndMap.put(pcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"19","06","-19","05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"20","08","-20","07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"21","09","-21","09",null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"22","31","-22","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"23","26","-23","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"33","34","-33","34",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {"36","37","-36","38",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zlinstpremOut,new String[] {null, null, null, "32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, "74", null, "42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"43","42","-43","42",null, null, null, null, null, null, null, null});
		/*BRD-306 START */
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		/*ILIFE-7730 starts*/
		/*fieldIndMap.put(linstamtOut,new String[] {"22","31","-22","32",null, null, null, null, null, null, null, null});*/
		fieldIndMap.put(linstamtOut,new String[] {null,"75",null,null,null, null, null, null, null, null, null, null});
		/*ILIFE-7730 ends*/
		/*BRD-306 END */
		fieldIndMap.put(prmbasisOut,new String[] {"58", "59", "-58", "57", null, null, null, null, null, null, null, null});
		//ILIFE-3399-STARTS
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "55", null, null, null, null, null, null, null, null});
		//ILIFE-3399-ENDS
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(exclindOut,new String[] {null, null, null, "73", null, null, null, null, null, null, null, null});
		fieldIndMap.put(tpdtypeOut,new String[] {null, "78", null, "77", null, null, null, null, null, null, null, null}); //ILIFE-7118
		fieldIndMap.put(lnkgindOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});//ILIFE-7752
		/*
		 * fwang3 ICIL-560
		 */
		fieldIndMap.put(cashvalarerOut,new String[] {null, null, null, "79", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "80", null, null, null, null, null, null, null, null});
		fieldIndMap.put(lnkgnoOut,new String[] {null, "60", null, "62", null, null, null, null, null, null, null, null}); //ILIFE-8248
		fieldIndMap.put(lnkgsubrefnoOut,new String[] {null,"61", null, "63", null, null, null, null, null, null, null, null});//ILIFE-8248
	
		fieldIndMap.put(covrprpseOut,new String[] {"82","85","-82",null,null, null, null, null, null, null, null, null});//IBPLIFE-2137
		
		/*screenFields = new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifcnum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, numpols, planSuffix, sumin, currcd, riskCessAge, riskCessTerm, riskCessDate, premCessAge, premCessTerm, premCessDate, mortcls, liencd, optextind, instPrem, comind, select, pbind, bappmeth, zlinstprem, taxamt, taxind,loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt,linstamt,prmbasis,dialdownoption,exclind,tpdtype,cashvalarer,lnkgind,zstpduty01};
		screenOutFields = new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, numpolsOut, plnsfxOut, suminOut, currcdOut, rcessageOut, rcesstrmOut, rcesdteOut, pcessageOut, pcesstrmOut, pcesdteOut, mortclsOut, liencdOut, optextindOut, instprmOut, comindOut, selectOut, pbindOut, bappmethOut, zlinstpremOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut,linstamtOut,prmbasisOut,dialdownoptionOut,exclindOut,tpdtypeOut,cashvalarerOut,lnkgindOut,zstpduty01Out};
		screenErrFields = new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, numpolsErr, plnsfxErr, suminErr, currcdErr, rcessageErr, rcesstrmErr, rcesdteErr, pcessageErr, pcesstrmErr, pcesdteErr, mortclsErr, liencdErr, optextindErr, instprmErr, comindErr, selectErr, pbindErr, bappmethErr, zlinstpremErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr,linstamtErr,prmbasisErr,dialdownoptionErr,exclindErr,tpdtypeErr,cashvalarerErr,lnkgindErr,zstpduty01Err};
		screenDateFields = new BaseData[] {riskCessDate, premCessDate,effdate};
		screenDateErrFields = new BaseData[] {rcesdteErr, pcesdteErr,effdateErr};
		screenDateDispFields = new BaseData[] {riskCessDateDisp, premCessDateDisp,effdateDisp};
*/
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields =getscreenDateDispFields();
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5127screen.class;
		protectRecord = S5127protect.class;
	}
	public int getDataAreaSize()
	{
		return 1686;
	}
	public int getDataFieldsSize()
	{
		return 742;
	}
	public int getErrorIndicatorSize()
	{
		return 236;
	}
	public int getOutputFieldSize()
	{
		return 708;
	}
	public BaseData[] getscreenFields(){
		return new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifcnum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, numpols, planSuffix, sumin, currcd, riskCessAge, riskCessTerm, riskCessDate, premCessAge, premCessTerm, premCessDate, mortcls, liencd, optextind, instPrem, comind, select, pbind, bappmeth, zlinstprem, taxamt, taxind,loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt,linstamt,prmbasis,dialdownoption,exclind,tpdtype,cashvalarer,lnkgind,zstpduty01,lnkgno,lnkgsubrefno,
				trancd, trandesc, effdatex, validflag, covrprpse};//ILIFE-8248
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, numpolsOut, plnsfxOut, suminOut, currcdOut, rcessageOut, rcesstrmOut, rcesdteOut, pcessageOut, pcesstrmOut, pcesdteOut, mortclsOut, liencdOut, optextindOut, instprmOut, comindOut, selectOut, pbindOut, bappmethOut, zlinstpremOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut,linstamtOut,prmbasisOut,dialdownoptionOut,exclindOut,tpdtypeOut,cashvalarerOut,lnkgindOut,zstpduty01Out,lnkgnoOut,lnkgsubrefnoOut,
			trancdOut, trandescOut, effdatexOut, validflagOut, covrprpseOut};//ILIFE-8248
	}
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, numpolsErr, plnsfxErr, suminErr, currcdErr, rcessageErr, rcesstrmErr, rcesdteErr, pcessageErr, pcesstrmErr, pcesdteErr, mortclsErr, liencdErr, optextindErr, instprmErr, comindErr, selectErr, pbindErr, bappmethErr, zlinstpremErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr,linstamtErr,prmbasisErr,dialdownoptionErr,exclindErr,tpdtypeErr,cashvalarerErr,lnkgindErr,zstpduty01Err,lnkgnoErr,lnkgsubrefnoErr,
				trancdErr, trandescErr, effdatexErr, validflagErr, covrprpseErr};//ILIFE-8248
	}
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {riskCessDate, premCessDate,effdate, effdatex};
	}
	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {riskCessDateDisp, premCessDateDisp,effdateDisp, effdatexDisp};
	}
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {rcesdteErr, pcesdteErr,effdateErr, effdatexErr};
	}

}
