package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.life.contractservicing.screens.Sd5jsScreenVars;
import com.csc.life.contractservicing.tablestructures.Td5jsrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pd5js extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pd5js");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Td5jsrec td5jsrec = new Td5jsrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sd5jsScreenVars sv = ScreenProgram.getScreenVars( Sd5jsScreenVars.class);
	
	public Pd5js() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5js", AppVars.getInstance(), sv);
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray){
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			/*	EXIT	*/
		}
	}
	
	@Override
	protected void initialise1000(){
		initialise1010();
		readRecord();
		moveToScreen();
	}
	
	protected void initialise1010() {
		sv.dataArea.set(SPACES);
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}
	
	protected void readRecord() {
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
	
	protected void moveToScreen() {
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.itmfrm.set(itemIO.getItmfrm());
		sv.itmto.set(itemIO.getItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		td5jsrec.td5jsRec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			sv.taxCalcReqd.set(td5jsrec.taxCalcReqd);
		}
	}
	
	protected void preScreenEdit() {
		try {
			preStart();
		}
		catch (GOTOException e){
			/*	EXIT	*/
		}
	}
	
	protected void preStart(){
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
	}
	
	@Override
	protected void screenEdit2000() {
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")) {
			exitScreenEdit();
		}
		exitScreenEdit();
	}
	
	protected void exitScreenEdit() {
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	@Override
	protected void update3000() {
		if (isEQ(wsspcomn.flag,"I")) {
			return;
		}
		validateUpdate();
		if(isEQ(wsaaUpdateFlag,"Y")) {
			fetchRecord();
			updateRecord();
		}
	}
	
	protected void validateUpdate() {
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		if(isNE(sv.taxCalcReqd, td5jsrec.taxCalcReqd)) {
			wsaaUpdateFlag = "Y";
			td5jsrec.taxCalcReqd.set(sv.taxCalcReqd);
		}
	}
	
	protected void fetchRecord() {
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}
	
	protected void updateRecord() {
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(td5jsrec.td5jsRec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}
	
	@Override
	protected void whereNext4000()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}
}
