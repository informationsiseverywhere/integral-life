/*
 * File: Pr50k.java
 * Date: 30 August 2009 1:31:43
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50K.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.tablestructures.T3583rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.screens.Sr50kScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ZlifelcTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Life Assured Details Changes
*
* This screen shows the life assured fields which allow to do
* alteration. (Change of gender, DOB, smoking flag & occupation code)
* Based on the Life Assured's client number entered, system
* will display the existing client details in both the Old &
* New columns. Changes will allow to be make under "New" column
* only.
*
* 1000 Section:
* =============
* Retrieve(RETRV) client info keeps in previous screen &
* populate into both Old & New column:
* Title/Calutation: CLTS-SALUTL
* Sex             : CLTS-CLTSEX
* Date of Birth   : CLTS-CLTDOB
* Occupation Code : CLTS-OCCPCODE
* Occupation Class: CLTS-STATCODE
*
* Get latest smoking flag from ZLIFELC (File format: *LIFO)
* with key ZLIFELC-CHDRCOY & ZLIFELC-LIFCNUM.
*
* 2000 Section:
* =============
* Validate all fields & all must be mandatory.
*
* Validate Sex Code againts Title/Salutation:
* IF  T3583-CLTSEX NOT = BLANK
* AND T3583-CLTSEX NOT = SR50K-CLTSEX-02
*     Display error msg "Salut Sex not = Clnt Sex"
*
* Validate DOB:
* IF SR50K-CLTDOB-02 = VRCM-MAX-DATE
*    Display error msg "Invalide Date"
*
* IF SR50K-CLTDOB-02 > Today's Date
*    Display error msg "DATE > TODAY"
*
* IF SR50K-ERROR-INDICATORS   = SPACES
*    Keeps ZLIFELC for next screen to proceed.
*
* If did not change anything for LIFE, not allow to go to next screen
*
* 3000 Section:
* =============
* Release client info keeps previously & direct update client
* from New column into client file.
*
* 4000 Section:
* =============
* Get (READ) the updated client info & KEEPS for next pgm.
*
***********************************************************************
* </pre>
*/
public class Pr50k extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50K");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
		/* ERRORS */
	private static final String h366 = "H366";
	private static final String g983 = "G983";
	private static final String g984 = "G984";
	private static final String f073 = "F073";
	private static final String f992 = "F992";
	private static final String rlas = "RLAS";
		/* TABLES */
	private static final String t3644 = "T3644";
	private static final String t3583 = "T3583";
		/* FORMATS */
	private static final String cltsrec = "CLTSREC";
	private static final String zlifelcrec = "ZLIFELCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ZlifelcTableDAM zlifelcIO = new ZlifelcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T3583rec t3583rec = new T3583rec();
	private Sr50kScreenVars sv = ScreenProgram.getScreenVars( Sr50kScreenVars.class);
	private boolean isOccOptional = false;
	
	
	public Pr50k() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50k", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.cltdob01.set(varcom.maxdate);
		sv.cltdob02.set(varcom.maxdate);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		isOccOptional = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSOTH017", appVars, "IT");
		cltsIO.setParams(SPACES);
		cltsIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(cltsIO.getClntnum());
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		sv.salutl01.set(cltsIO.getSalutl());
		sv.cltsex01.set(cltsIO.getCltsex());
		sv.cltdob01.set(cltsIO.getCltdob());
		sv.occpcode01.set(cltsIO.getOccpcode());
		sv.statcode01.set(cltsIO.getStatcode());
		if (isEQ(wsspcomn.sbmaction,"A")) {
			sv.salutl02.set(cltsIO.getSalutl());
			sv.cltsex02.set(cltsIO.getCltsex());
			sv.cltdob02.set(cltsIO.getCltdob());
			sv.occpcode02.set(cltsIO.getOccpcode());
			sv.statcode02.set(cltsIO.getStatcode());
		}
		zlifelcIO.setDataKey(SPACES);
		zlifelcIO.setParams(SPACES);
		zlifelcIO.setChdrcoy(wsspcomn.company);
		zlifelcIO.setLifcnum(cltsIO.getClntnum());
		zlifelcIO.setFormat(zlifelcrec);
		zlifelcIO.setFunction(varcom.readr);
		if (isEQ(wsspcomn.sbmaction,"B")) {
			zlifelcIO.setFunction(varcom.begn);
		}
		SmartFileCode.execute(appVars, zlifelcIO);
		if (isEQ(zlifelcIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		if (isNE(zlifelcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zlifelcIO.getParams());
			syserrrec.statuz.set(zlifelcIO.getStatuz());
			fatalError600();
		}
		if (isEQ(wsspcomn.sbmaction,"B")
		&& isNE(zlifelcIO.getChdrnum(),wsspcomn.chdrChdrnum)) {
			while ( !(isEQ(zlifelcIO.getStatuz(),varcom.endp)
			|| isEQ(zlifelcIO.getChdrnum(),wsspcomn.chdrChdrnum))) {
				zlifelcIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, zlifelcIO);
				if (isNE(zlifelcIO.getStatuz(),varcom.oK)
				&& isNE(zlifelcIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(zlifelcIO.getParams());
					syserrrec.statuz.set(zlifelcIO.getStatuz());
					fatalError600();
				}
				if (isNE(zlifelcIO.getLifcnum(),cltsIO.getClntnum())) {
					zlifelcIO.setStatuz(varcom.endp);
				}
			}
			
		}
		sv.smoking01.set(zlifelcIO.getSmoking());
		sv.smoking02.set(zlifelcIO.getSmoking());
		if (isEQ(wsspcomn.sbmaction,"B")) {
			sv.salutl02.set(cltsIO.getSalutl());
			sv.cltsex02.set(zlifelcIO.getCltsex());
			sv.cltdob02.set(zlifelcIO.getCltdob());
			sv.occpcode02.set(zlifelcIO.getOccup());
			sv.statcode02.set(cltsIO.getStatcode());
//			ILIFE-795
//			if (isEQ(sv.salutl01,sv.salutl02)) {
//				sv.salutl02Out[varcom.pr.toInt()].set("Y");
//			}
//			if (isEQ(sv.cltsex01,sv.cltsex02)) {
//				sv.cltsex02Out[varcom.pr.toInt()].set("Y");
//			}
//			if (isEQ(sv.cltdob01,sv.cltdob02)) {
//				sv.cltdob02Out[varcom.pr.toInt()].set("Y");
//			}
//			if (isEQ(sv.occpcode01,sv.occpcode02)) {
//				sv.occpcode02Out[varcom.pr.toInt()].set("Y");
//			}
//			if (isEQ(sv.statcode01,sv.statcode02)) {
//				sv.statcode02Out[varcom.pr.toInt()].set("Y");
//			}
//			if (isEQ(sv.smoking01,sv.smoking02)
//			&& isNE(sv.smoking01,SPACES)) {
//				sv.smoking02Out[varcom.pr.toInt()].set("Y");
//			}
			
		}
		
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
					screenIo2010();
					validate2020();
					checkForErrors2080();
				}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(sv.salutl02,SPACES)) {
			sv.salutl02Err.set(h366);
		}
		if (isEQ(sv.cltsex02,SPACES)) {
			sv.cltsex02Err.set(h366);
		}
		if (isEQ(sv.cltdob02,SPACES)) {
			sv.cltdob02Err.set(h366);
		}
		if (isEQ(sv.occpcode02,SPACES) && !isOccOptional) {
			sv.occpcode02Err.set(h366);
		}
		if (isEQ(sv.statcode02,SPACES) && !isOccOptional) {
			sv.statcode02Err.set(h366);
		}
		if (isEQ(sv.smoking02,SPACES)) {
			sv.smoking02Err.set(h366);
		}
		if (isNE(sv.salutl02,SPACES)) {
			itemIO.setDataKey(SPACES);
			t3583rec.t3583Rec.set(SPACES);
			itemIO.setItemtabl(t3583);
			itemIO.setItemitem(sv.salutl02);
			readItem2100();
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				sv.salutl02Err.set(g984);
			}
			if (isEQ(itemIO.getStatuz(),varcom.oK)) {
				t3583rec.t3583Rec.set(itemIO.getGenarea());
			}
		}
		if (isNE(t3583rec.cltsex,SPACES)
		&& isNE(sv.cltsex02,SPACES)
		&& isNE(sv.cltsex02Err,"G979")) {
			if (isNE(t3583rec.cltsex,sv.cltsex02)) {
				sv.salutl02Err.set(g983);
				sv.cltsex02Err.set(g983);
			}
		}
		if (isNE(sv.cltdob02,SPACES)) {
			if (isEQ(sv.cltdob02,varcom.vrcmMaxDate)) {
				sv.cltdob02Err.set(h366);
				return ;
			}
			if (isGT(sv.cltdob02,wsaaToday)) {
				sv.cltdob02Err.set(f073);
			}
		}
		if (isNE(sv.occpcode02,SPACES)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItemtabl(t3644);
			itemIO.setItemitem(sv.occpcode02);
			readItem2100();
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				sv.occpcode02Err.set(f992);
			}
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.sbmaction,"A")
		&& isEQ(sv.cltsex01,sv.cltsex02)
		&& isEQ(sv.cltdob01,sv.cltdob02)
		&& isEQ(sv.occpcode01,sv.occpcode02)
		&& isEQ(sv.statcode01,sv.statcode02)
		&& isEQ(sv.smoking01,sv.smoking02)) {
			sv.cltsex02Err.set(rlas);
			sv.cltdob02Err.set(rlas);
			sv.occpcode02Err.set(rlas);
			sv.statcode02Err.set(rlas);
			sv.smoking02Err.set(rlas);
		}
		if (isEQ(wsspcomn.sbmaction,"B")
		&& isEQ(sv.salutl02Out[varcom.pr.toInt()],"Y")
		&& isEQ(sv.cltsex02Out[varcom.pr.toInt()],"Y")
		&& isEQ(sv.cltdob02Out[varcom.pr.toInt()],"Y")
		&& isEQ(sv.occpcode02Out[varcom.pr.toInt()],"Y")
		&& isEQ(sv.statcode02Out[varcom.pr.toInt()],"Y")
		&& isEQ(sv.smoking02Out[varcom.pr.toInt()], "Y")
		&& isEQ(sv.smoking01, sv.smoking02)) {
			sv.cltsex02Err.set(rlas);
			sv.cltdob02Err.set(rlas);
			sv.occpcode02Err.set(rlas);
			sv.statcode02Err.set(rlas);
			sv.smoking02Err.set(rlas);
		}
//	ILIFE-795
//		if (isEQ(wsspcomn.sbmaction,"B")) {
//			if (isNE(sv.cltsex01,sv.cltsex02)) {
//				sv.cltsex02Err.set(rlas);
//			}
//			if (isNE(sv.cltdob01,sv.cltdob02)) {
//				sv.cltdob02Err.set(rlas);
//			}
//			if (isNE(sv.occpcode01,sv.occpcode02)) {
//				sv.occpcode02Err.set(rlas);
//			}
//			if (isNE(sv.statcode01,sv.statcode02)) {
//				sv.statcode02Err.set(rlas);
//			}
//			if (isNE(sv.smoking01, sv.smoking02)
//			&& isNE(sv.smoking01, SPACES)) {
//				sv.smoking02Err.set(rlas);
//			}
//		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.errorIndicators,SPACES)) {
			keepsZlifelc2200();
		}
		/*EXIT*/
	}

protected void readItem2100()
	{
		/*START*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*EXIT1*/
	}

protected void keepsZlifelc2200()
	{
		start2210();
	}

protected void start2210()
	{
		zlifelcIO.setCltsex(sv.cltsex02);
		zlifelcIO.setCltdob(sv.cltdob02);
		zlifelcIO.setOccup(sv.occpcode02);
		zlifelcIO.setStatcode(sv.statcode02);
		zlifelcIO.setSmoking(sv.smoking02);
		zlifelcIO.setFunction(varcom.keeps);
		zlifelcIO.setFormat(zlifelcrec);
		SmartFileCode.execute(appVars, zlifelcIO);
		if (isNE(zlifelcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zlifelcIO.getParams());
			fatalError600();
		}
	}

protected void update3000()
	{
			rlseCltn3010();
			updateCltn3020();
		}

protected void rlseCltn3010()
	{
		cltsIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
	}

protected void updateCltn3020()
	{
    //PINNACLE-2643	
     if (isNE(wsspcomn.sbmaction,"A")) {
	    return ;
      }     
//	    ILIFE-1241 ends
		cltsIO.setParams(SPACES);
		cltsIO.setStatuz(varcom.oK);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.lifcnum);
		cltsIO.setFunction(varcom.readh);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.salutl01,sv.salutl02)
		&& isEQ(sv.cltsex01,sv.cltsex02)
		&& isEQ(sv.cltdob01,sv.cltdob02)
		&& isEQ(sv.occpcode01,sv.occpcode02)
		&& isEQ(sv.statcode01,sv.statcode02)) {
			return ;
		}
		//PINNACLE-2643
		cltsIO.setSalutl(sv.salutl02);
		cltsIO.setCltsex(sv.cltsex02);
		cltsIO.setCltdob(sv.cltdob02);
		cltsIO.setOccpcode(sv.occpcode02);
		cltsIO.setStatcode(sv.statcode02);  
		cltsIO.setFunction(varcom.rewrt);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		readClts4010();
		nextProgram4020();
	}

protected void readClts4010()
	{
		cltsIO.setParams(SPACES);
		cltsIO.setStatuz(varcom.oK);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.lifcnum);
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		cltsIO.setFunction(varcom.keeps);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
