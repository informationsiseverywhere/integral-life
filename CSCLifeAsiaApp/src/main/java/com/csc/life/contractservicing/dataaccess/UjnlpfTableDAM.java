package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UjnlpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:42
 * Class transformed from UJNLPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UjnlpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 295;
	public FixedLengthStringData ujnlrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ujnlpfRecord = ujnlrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ujnlrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(ujnlrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(ujnlrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(ujnlrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(ujnlrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(ujnlrec);
	public PackedDecimalData jobnoPrice = DD.jctljnumpr.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData unitSubAccount = DD.unitsa.copy().isAPartOf(ujnlrec);
	public PackedDecimalData strpdate = DD.strpdate.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData nowDeferInd = DD.ndfind.copy().isAPartOf(ujnlrec);
	public PackedDecimalData nofUnits = DD.nofunt.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData unitType = DD.unityp.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(ujnlrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(ujnlrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(ujnlrec);
	public PackedDecimalData priceDateUsed = DD.pricedt.copy().isAPartOf(ujnlrec);
	public PackedDecimalData moniesDate = DD.moniesdt.copy().isAPartOf(ujnlrec);
	public PackedDecimalData priceUsed = DD.priceused.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(ujnlrec);
	public PackedDecimalData nofDunits = DD.nofdunt.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData feedbackInd = DD.fdbkind.copy().isAPartOf(ujnlrec);
	public PackedDecimalData unitBarePrice = DD.ubrepr.copy().isAPartOf(ujnlrec);
	public PackedDecimalData inciNum = DD.incinum.copy().isAPartOf(ujnlrec);
	public PackedDecimalData inciPerd01 = DD.inciperd.copy().isAPartOf(ujnlrec);
	public PackedDecimalData inciPerd02 = DD.inciperd.copy().isAPartOf(ujnlrec);
	public PackedDecimalData inciprm01 = DD.inciprm.copy().isAPartOf(ujnlrec);
	public PackedDecimalData inciprm02 = DD.inciprm.copy().isAPartOf(ujnlrec);
	public PackedDecimalData ustmno = DD.ustmno.copy().isAPartOf(ujnlrec);
	public PackedDecimalData contractAmount = DD.cntamnt.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData fundCurrency = DD.fndcurr.copy().isAPartOf(ujnlrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(ujnlrec);
	public PackedDecimalData fundRate = DD.fundrate.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData genlcde = DD.genlcde.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData contractType = DD.contyp.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData triggerModule = DD.triger.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData triggerKey = DD.trigky.copy().isAPartOf(ujnlrec);
	public PackedDecimalData procSeqNo = DD.prcseq.copy().isAPartOf(ujnlrec);
	public PackedDecimalData svp = DD.svp.copy().isAPartOf(ujnlrec);
	public PackedDecimalData discountFactor = DD.discfa.copy().isAPartOf(ujnlrec);
	public PackedDecimalData surrenderPercent = DD.persur.copy().isAPartOf(ujnlrec);
	public PackedDecimalData crComDate = DD.crcdte.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData canInitUnitInd = DD.ciuind.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData switchIndicator = DD.swchind.copy().isAPartOf(ujnlrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ujnlrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ujnlrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UjnlpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UjnlpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UjnlpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UjnlpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UjnlpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UjnlpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UjnlpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UJNLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"SEQNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"JCTLJNUMPR, " +
							"VRTFND, " +
							"UNITSA, " +
							"STRPDATE, " +
							"NDFIND, " +
							"NOFUNT, " +
							"UNITYP, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"PRICEDT, " +
							"MONIESDT, " +
							"PRICEUSED, " +
							"CRTABLE, " +
							"CNTCURR, " +
							"NOFDUNT, " +
							"FDBKIND, " +
							"UBREPR, " +
							"INCINUM, " +
							"INCIPERD01, " +
							"INCIPERD02, " +
							"INCIPRM01, " +
							"INCIPRM02, " +
							"USTMNO, " +
							"CNTAMNT, " +
							"FNDCURR, " +
							"FUNDAMNT, " +
							"FUNDRATE, " +
							"SACSCODE, " +
							"SACSTYP, " +
							"GENLCDE, " +
							"CONTYP, " +
							"TRIGER, " +
							"TRIGKY, " +
							"PRCSEQ, " +
							"SVP, " +
							"DISCFA, " +
							"PERSUR, " +
							"CRCDTE, " +
							"CIUIND, " +
							"SWCHIND, " +
							"TRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     seqno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     jobnoPrice,
                                     unitVirtualFund,
                                     unitSubAccount,
                                     strpdate,
                                     nowDeferInd,
                                     nofUnits,
                                     unitType,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     priceDateUsed,
                                     moniesDate,
                                     priceUsed,
                                     crtable,
                                     cntcurr,
                                     nofDunits,
                                     feedbackInd,
                                     unitBarePrice,
                                     inciNum,
                                     inciPerd01,
                                     inciPerd02,
                                     inciprm01,
                                     inciprm02,
                                     ustmno,
                                     contractAmount,
                                     fundCurrency,
                                     fundAmount,
                                     fundRate,
                                     sacscode,
                                     sacstyp,
                                     genlcde,
                                     contractType,
                                     triggerModule,
                                     triggerKey,
                                     procSeqNo,
                                     svp,
                                     discountFactor,
                                     surrenderPercent,
                                     crComDate,
                                     canInitUnitInd,
                                     switchIndicator,
                                     tranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		seqno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		jobnoPrice.clear();
  		unitVirtualFund.clear();
  		unitSubAccount.clear();
  		strpdate.clear();
  		nowDeferInd.clear();
  		nofUnits.clear();
  		unitType.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		priceDateUsed.clear();
  		moniesDate.clear();
  		priceUsed.clear();
  		crtable.clear();
  		cntcurr.clear();
  		nofDunits.clear();
  		feedbackInd.clear();
  		unitBarePrice.clear();
  		inciNum.clear();
  		inciPerd01.clear();
  		inciPerd02.clear();
  		inciprm01.clear();
  		inciprm02.clear();
  		ustmno.clear();
  		contractAmount.clear();
  		fundCurrency.clear();
  		fundAmount.clear();
  		fundRate.clear();
  		sacscode.clear();
  		sacstyp.clear();
  		genlcde.clear();
  		contractType.clear();
  		triggerModule.clear();
  		triggerKey.clear();
  		procSeqNo.clear();
  		svp.clear();
  		discountFactor.clear();
  		surrenderPercent.clear();
  		crComDate.clear();
  		canInitUnitInd.clear();
  		switchIndicator.clear();
  		tranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUjnlrec() {
  		return ujnlrec;
	}

	public FixedLengthStringData getUjnlpfRecord() {
  		return ujnlpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUjnlrec(what);
	}

	public void setUjnlrec(Object what) {
  		this.ujnlrec.set(what);
	}

	public void setUjnlpfRecord(Object what) {
  		this.ujnlpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ujnlrec.getLength());
		result.set(ujnlrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}