package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Id;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Yctxpf implements Cloneable {
	@Id
	private long unique_Number;
	private String chdrcoy;
	private String chdrnum;
	private String cnttype;
	private String validFlag;
	private int staxdt;
	private int etaxdt;
	private int tranno;
	private String batctrcde;
	private BigDecimal totalTaxAmt;
	private BigDecimal rpoffset;
	private int effDate;
	private String notrcvd;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private static final Logger LOGGER = LoggerFactory.getLogger(Yctxpf.class);
	
	public long getUniqueNumber() {
		return unique_Number;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.unique_Number = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	public int getStaxdt() {
		return staxdt;
	}
	public void setStaxdt(int staxdt) {
		this.staxdt = staxdt;
	}
	public int getEtaxdt() {
		return etaxdt;
	}
	public void setEtaxdt(int etaxdt) {
		this.etaxdt = etaxdt;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public BigDecimal getTotalTaxAmt() {
		return totalTaxAmt;
	}
	public void setTotalTaxAmt(BigDecimal totalTaxAmt) {
		this.totalTaxAmt = totalTaxAmt;
	}
	public BigDecimal getRpoffset() {
		return rpoffset;
	}
	public void setRpoffset(BigDecimal rpoffset) {
		this.rpoffset = rpoffset;
	}
	public int getEffDate() {
		return effDate;
	}
	public void setEffDate(int effDate) {
		this.effDate = effDate;
	}
	public String getNotrcvd() {
		return notrcvd;
	}
	public void setNotrcvd(String notrcvd) {
		this.notrcvd = notrcvd;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Timestamp getDatime() {
		return datime;
	}
	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}	
	
	public Yctxpf copyObject() {
		try {
			return (Yctxpf) super.clone();
		}
		catch(CloneNotSupportedException e) {
			LOGGER.error("Unable to copy object", e);
		}
		return new Yctxpf();
	}
}
