/*
 * File: Ph5mpat.java
 * Date: 30 August 2009 0:40:10
 * Author: Quipoz Limited
 *
 * Class transformed from Ph5mpAT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.financials.dataaccess.dao.RbnkpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Rbnkpf;
import com.csc.fsu.financials.procedures.Addrtrn;
import com.csc.fsu.financials.tablestructures.T3676rec;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.BextpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Bextpf;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Billreq1;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Addrtrnrec;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Billreqrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3678rec;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.general.tablestructures.Tr29srec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdbilTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrcpy2;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*REMARKS.
*
*   PAY-TO-DATE ADVANCE
*
*   This is a subsystem to be called after a WINDBACK. It allows
*   the user to advance forward to a specified point, make
*   alterations, advance forward again and make alterations,
*   before being passed through the renewals sub-system to bring
*   the contract up-to-date.
*
*  Almost a direct copy of the first three batch programs
*  within renewals.
*                    Re-Rate.
*                    Billing.
*                    Collection.
*
*  AT MODULE
*
*
*    Perform
*            Perform Re-Rate
*            Perform Billing
*            Perform Collection
*
*    Until frequency number of times
*       or the amount in suspense does not cover billing.
*
*   ! Within billing a bill to the punter should not be
*     created.
*     But, the suspense account should be checked to see
*     whether enough money exists to pay the bill - if
*     not then processing should stop.
*
*  Re-rate of installment Premiums.
*
*  This is to recalculate any contract premiums due for revision
*  prior to billing. Lead times (billing in
*  advance periods) are established and generic subroutines are
*  called to recalculate premiums and components are rescheduled
*  for re-rating again if applicable.
*
*  The program flow is as follows:-
*
*              If WSAA-PREM-DIFF not = 0
*              (Total of difference of new and old premiums)
*                  Update CHDRLIF with CHDRLIF-VALIDFLAG = 2.
*
*         Read a coverage .
*         Validate the coverage/rider status against T5679.
*         If invalid status read the next related COVR.
*
*         Once found calculate
*              WSAA-BILL-DATE = Bill to date.
*
*          If COVR-RRTDAT > WSAA-BILL-DATE
*              Goto read next COVR record.
*
*          If COVR-PCESDTE = COVR-RRTDAT(Cessation = Re-rate)
*              Goto next section(Update COVR).
*
*          Read T5687 for Premium calculation method and Re-calculate
*          frequency.
*
*          If no method found
*              Goto next section(Update COVR).
*
*          Read LIFERNL to see if there is a joint life.
*          (Joint life existed implies joint life contract)
*          If joint life indicator not = 'N' and
*             joint life existed
*              Use joint life premium calculation method to access
*              T5675 to get the Calculation subroutine
*          Else
*              Use single life premium calculation method to access
*              T5675 to get the Calculation subroutine.
*
*          Call calculation subroutine.
*
*          Recalculate, reschedule next re-rating dates and update  OVR.
*
*          Read and hold COVR record.
*
*          If COVR-PCESDTE = COVR-RRTDAT(Cessation = Re-rate)
*              Move 'PU' to PSTATCODE and calculate
*              WSAA-PREM-DIFF = installment premium * -1
*              Rewrite COVR record.
*
*          Set VALIDFLAG to '2' and rewrite with the current to da e
*          set to (WSAA-BILL-DATE - 1)
*
*          Set up new COVR record details.
*
*          Set VALIDFLAG to '1', add 1 to TRANNO and update the
*          current from date to WSAA-BILL-DATE and current to date
*          as VRCM-MAX-DATE.
*
*          Read LEXTBRR(Option/extra loading) for any non-expired
*          records so that rescheduling can be done if required.
*
*          If T5687-RECALC-FREQ not = 0
*              Calculate WSAA-RATE-FROM as re-rate from date
*              incremented by this frequency.
*              If this new date is less than any current LEXT
*              expired date
*                  Move this new date to COVR-RERATE-DATE and
*                  COVR-RERATE-FROM-DATE
*              Else
*                  Move the LEXT expired date to COVR-RERATE-DATE.
*
*          Adjust the calculated premium for plan polices.
*
*          Calculate WSAA-INST-PREM = CPRM-CALC-PREM / POLINC.
*          If plan suffix = 0
*              Calculate WSAA-INST-PREM = WSAA-INST-PREM * POLSUM.
*          Calculate WSAA-PREM-DIFF = WSAA-PREM-DIFF +
*                                     WSAA-INST-PREM - COVR-SINGP
*          (where COVR-SINGP  is the old coverage/rider installment
*           premium)
*
*          Set COVR-SINGP to WSAA-INST-PREM.
*          Write this new COVR record.
*
*       If WSAA-PREM-DIFF not = 0
*       (Total of the differences of new and old premiums)
*           Update CHDRLIF with CHDRLIF-VALIDFLAG = 2.
*
*       End of Re-Rate.
*
*  Billing.
*  -------
*  This bills  any premiums due for a contract. It also produces
*  installment records for outstanding payments.
*
* Read  the transaction status codes from T5679.
*
* Calculate WSAA-BILL-DATE as the effective date. Move
* CHDR-BTDATE to WSAA-BTDATE.
*
*
* Read SACSPF with logical  view  SACSRNL  using sub-account and
* key from T5645 line  '01'  to  get  suspense  balance.  Using
* sub-account code as the item  access  T3695  to check the sign
* for this account, if  negative multiply the suspense value by
* -1. Move the suspense balance amount to WSAA-SUSP-AVAIL.
*
* NO MEDIA RECORDS SHOULD BE PRODUCED !.
* ONLY adjust the amount of suspense premium available.
*
* Zero the WSAA-TOT-AMOUNT and perform Create LINS records.
*
*
* 3.) Create LINS records.
*
*
* Firstly check if  the contract record we are dealing with has
* the correct premium  for  the installment date; if not we will
 * </pre>
 */
public class Ph5mpat extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Ph5mp");
	protected PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaStoreEffdate = new ZonedDecimalData(8, 0).init(0);

	private String wsaaAllCovExp = "Y";
	private String wsaaAllRidExp = "Y";

	private Varcom varcom = new Varcom();
	private ZonedDecimalData wsaaRerateDate = new ZonedDecimalData(8, 0).init(0);

	/* WSAA-COVERAGE-PREMIUMS */
	private FixedLengthStringData[] wsaaLifeLevel = FLSInittedArray(10, 3366);
	private FixedLengthStringData[][] wsaaCoverageLevel = FLSDArrayPartOfArrayStructure(99, 34, wsaaLifeLevel, 0);
	private ZonedDecimalData[][] wsaaCovrInstprem = ZDArrayPartOfArrayStructure(17, 2, wsaaCoverageLevel, 0);
	private ZonedDecimalData[][] wsaaCovrSingp = ZDArrayPartOfArrayStructure(17, 2, wsaaCoverageLevel, 17);
	private ZonedDecimalData wsaaC = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaL = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCoverageNum = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaLifeNum = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaNetBillamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaNetCbillamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBillDate = new ZonedDecimalData(8, 0).init(0);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init(SPACES);
	private String wsaaNotEnoughCash = "N";

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);

	private PackedDecimalData wsaaCurrptdt = new PackedDecimalData(8, 0);

	/* AT Request storage. */
	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	protected FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(192 );
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 5);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 9);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 13);
	private PackedDecimalData wsaaAdvptdat = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 17);
	private PackedDecimalData wsaaNoOfFreqs = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 22).setUnsigned();
	private PackedDecimalData wsaaUnitEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 26);
	private PackedDecimalData wsaaApaRequired = new PackedDecimalData(15, 2).isAPartOf(wsaaTransArea, 31);
	private FixedLengthStringData wsaaNewBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 46);
	private PackedDecimalData wsaaProDaysPrem = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 48);
	private PackedDecimalData wsaaProDaysStpduty = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 65);
	private PackedDecimalData wsaaProDaysTotPrem = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 82); 
	private PackedDecimalData wsaaProDaysFee = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 99 ); 
	private PackedDecimalData wsaaTaxRelAmt = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 116 );
	private FixedLengthStringData wsaaChdrpf = new FixedLengthStringData(37).isAPartOf(wsaaTransArea, 133 );
	private FixedLengthStringData wsaaChdrpfGroupkey = new FixedLengthStringData(12).isAPartOf(wsaaChdrpf, 0);
	private FixedLengthStringData wsaaChdrpfMandref = new FixedLengthStringData(5).isAPartOf(wsaaChdrpf, 12);
	private FixedLengthStringData wsaaChdrpfMembsel = new FixedLengthStringData(10).isAPartOf(wsaaChdrpf, 17);
	private FixedLengthStringData wsaaChdrpfPayrnum = new FixedLengthStringData(8).isAPartOf(wsaaChdrpf, 27);
	private FixedLengthStringData wsaaChdrpfBillchnl = new FixedLengthStringData(2).isAPartOf(wsaaChdrpf, 35);
	private FixedLengthStringData wsaaPayrpf = new FixedLengthStringData(12).isAPartOf(wsaaTransArea, 170 );
	private FixedLengthStringData wsaaPayrpfGrupcoy = new FixedLengthStringData(1).isAPartOf(wsaaPayrpf, 0 );
	private FixedLengthStringData wsaaPayrpfIncomeSeqNo = new FixedLengthStringData(2).isAPartOf(wsaaPayrpf, 1 );
	private FixedLengthStringData wsaaPayrpfBillday = new FixedLengthStringData(2).isAPartOf(wsaaPayrpf, 3 );
	private FixedLengthStringData wsaaPayrpfBillmonth = new FixedLengthStringData(2).isAPartOf(wsaaPayrpf, 5) ;
	private PackedDecimalData wsaaPayrpfOrigBillcd = new PackedDecimalData(8, 0).isAPartOf(wsaaPayrpf, 7 );
	private FixedLengthStringData filler2 = new FixedLengthStringData(10).isAPartOf(wsaaTransArea, 182, FILLER).init(SPACES);

	private FixedLengthStringData wsaaAtmdBatchKey = new FixedLengthStringData(19);
	private FixedLengthStringData atrtBatchKey = new FixedLengthStringData(19).isAPartOf(wsaaAtmdBatchKey, 0);
	private FixedLengthStringData atrtPrefix = new FixedLengthStringData(2).isAPartOf(atrtBatchKey, 0);
	private FixedLengthStringData atrtCompany = new FixedLengthStringData(1).isAPartOf(atrtBatchKey, 2);
	protected FixedLengthStringData atrtBranch = new FixedLengthStringData(2).isAPartOf(atrtBatchKey, 3);
	private FixedLengthStringData atrtAcctPerd = new FixedLengthStringData(5).isAPartOf(atrtBatchKey, 5);
	protected PackedDecimalData atrtAcctYear = new PackedDecimalData(4, 0).isAPartOf(atrtAcctPerd, 0);
	protected PackedDecimalData atrtAcctMonth = new PackedDecimalData(2, 0).isAPartOf(atrtAcctPerd, 3);
	protected FixedLengthStringData atrtTransCode = new FixedLengthStringData(4).isAPartOf(atrtBatchKey, 10);
	protected FixedLengthStringData atrtBatch = new FixedLengthStringData(5).isAPartOf(atrtBatchKey, 14);
	private PackedDecimalData wsaaCurrentPayuptoDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBtdate = new PackedDecimalData(8, 0);
	protected PackedDecimalData wsaaBillcd = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNextdt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaSuspAvail = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotAmount = new PackedDecimalData(17, 2);
	protected String wsaaOldPayr = "";
	protected FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30);
	/* WSAA-BILLFREQ-VAR */
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaItemCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);
	/* WSAA-DATES */
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaTerm, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTermLeftInteger = new ZonedDecimalData(6, 0).isAPartOf(filler4, 0).setUnsigned();
	private ZonedDecimalData wsaaTermLeftRemain = new ZonedDecimalData(5, 0).isAPartOf(filler4, 6).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTermRan = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaBascpySubprog = new FixedLengthStringData(7).init(SPACES);
	private FixedLengthStringData wsaaSrvcpySubprog = new FixedLengthStringData(7).init(SPACES);
	private FixedLengthStringData wsaaRnwcpySubprog = new FixedLengthStringData(7).init(SPACES);
	/* WSAA-COMM-TOTALS */
	private ZonedDecimalData wsaaBascpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaBascpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaBascpyPay = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSrvcpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSrvcpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRnwcpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaRnwcpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyDue = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyErn = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOvrdBascpyPay = new ZonedDecimalData(17, 2).init(0);

	protected FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	protected FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	protected FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	protected FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	protected FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	protected FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	/* WSAA-PLAN-SUFF */
	protected ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	protected ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	/*
	 * WSAA-T5645 03 WSAA-STORED-T5645 OCCURS 4. 03 WSAA-STORED-T5645 OCCURS 5.
	 * <018>
	 */
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray(30, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
	/* WSAA-T5645-32TH-VALUE */
	private ZonedDecimalData wsaaT5645Cnttot32 = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaT5645Glmap32 = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaT5645Sacscode32 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sacstype32 = new FixedLengthStringData(2);
	private static final String wsaaT5645Sign32 = "";
	/* WSAA-T5645-33TH-VALUE */
	private ZonedDecimalData wsaaT5645Cnttot33 = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaT5645Glmap33 = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaT5645Sacscode33 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sacstype33 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sign33 = new FixedLengthStringData(1);
	/* WSAA-T5645-34TH-VALUE */
	private ZonedDecimalData wsaaT5645Cnttot34 = new ZonedDecimalData(2, 0);
	private FixedLengthStringData wsaaT5645Glmap34 = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaT5645Sacscode34 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sacstype34 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT5645Sign34 = new FixedLengthStringData(1);
	
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTol2 = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaLinsBtdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEomDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaEomDateFmt = new FixedLengthStringData(8).isAPartOf(wsaaEomDate, 0, REDEFINE);
	private ZonedDecimalData wsaaEomYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaEomDateFmt, 0).setUnsigned();
	private FixedLengthStringData wsaaEomYearFmt = new FixedLengthStringData(4).isAPartOf(wsaaEomYear, 0, REDEFINE);
	private ZonedDecimalData wsaaCenturyYear = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomYearFmt, 2).setUnsigned();
	private ZonedDecimalData wsaaEomMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomDateFmt, 4).setUnsigned();
	private ZonedDecimalData wsaaEomDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEomDateFmt, 6).setUnsigned();
	private static final String wsaaDaysInMonthNorm = "312831303130313130313031";
	private static final String wsaaDaysInMonthLeap = "312931303130313130313031";
	private FixedLengthStringData wsaaDaysInMonth = new FixedLengthStringData(24);

	private FixedLengthStringData wsaaDaysInMonthRed = new FixedLengthStringData(24).isAPartOf(wsaaDaysInMonth, 0,
			REDEFINE);
	private ZonedDecimalData[] wsaaMonthDays = ZDArrayPartOfStructure(12, 2, 0, wsaaDaysInMonthRed, 0, UNSIGNED_TRUE);
	private String wsaaLeapYear = "";
	private String wsaaEndOfMonth = "";
	private FixedLengthStringData wsaaEom1stDate = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(11, 5);

	private FixedLengthStringData wsaaFreqFactorFmt = new FixedLengthStringData(11).isAPartOf(wsaaFreqFactor, 0,
			REDEFINE);
	private ZonedDecimalData wsaaFreqDecimal = new ZonedDecimalData(5, 5).isAPartOf(wsaaFreqFactorFmt, 6);
	private PackedDecimalData wsaaResult = new PackedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaPtdate = new PackedDecimalData(8, 0);
	private String wsaaAgtTerminatedFlag = "";
	private String wsaaToleranceFlag = "";

	/* <DRYAPL> */
	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private FixedLengthStringData wsaaBillfq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfq, 0, REDEFINE).setUnsigned();
	private static final int wsaaAgcmIxSize = 100;
	private ZonedDecimalData wsaaAgcmIx = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIa = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private static final int wsaaAgcmIySize = 500;
	private ZonedDecimalData wsaaAgcmIy = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIb = new ZonedDecimalData(3, 0).init(0).setUnsigned();

	/* WSAA-AGCM-SUMMARY */
	private FixedLengthStringData[] wsaaAgcmRec = FLSInittedArray(100, 8515);
	private FixedLengthStringData[] wsaaAgcmChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaAgcmRec, 0);
	private FixedLengthStringData[] wsaaAgcmChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAgcmRec, 1);
	private FixedLengthStringData[] wsaaAgcmLife = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 9);
	private FixedLengthStringData[] wsaaAgcmCoverage = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 11);
	private FixedLengthStringData[] wsaaAgcmRider = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 13);
	private FixedLengthStringData[][] wsaaAgcmPremRec = FLSDArrayPartOfArrayStructure(500, 17, wsaaAgcmRec, 15);
	private ZonedDecimalData[][] wsaaAgcmSeqno = ZDArrayPartOfArrayStructure(3, 0, wsaaAgcmPremRec, 0, UNSIGNED_TRUE);
	private PackedDecimalData[][] wsaaAgcmEfdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgcmPremRec, 3, UNSIGNED_TRUE);
	private PackedDecimalData[][] wsaaAgcmAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 8);
	private PackedDecimalData wsaaAgcmPremium = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaAgcmFound = new FixedLengthStringData(1);
	private Validator agcmFound = new Validator(wsaaAgcmFound, "Y");

	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator singPrem = new Validator(wsaaPremType, "S");
	private Validator firstPrem = new Validator(wsaaPremType, "I");
	private Validator renPrem = new Validator(wsaaPremType, "R");
	/* ERRORS */
	private static final String e308 = "E308";
	private static final String f294 = "F294";
	private static final String g029 = "G029";
	private static final String e103 = "E103";
	private static final String f109 = "F109";
	private static final String nfnd = "NFND";
	private static final String h791 = "H791";
	
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private TaxdbilTableDAM taxdbilIO = new TaxdbilTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private Rdockey wsaaRdockey = new Rdockey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itdmkey wsaaItdmkey = new Itdmkey();
	private Syserrrec syserrrec = new Syserrrec();
	private Vflagcpy vflagcpy = new Vflagcpy();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private T3629rec t3629rec = new T3629rec();
	private T3695rec t3695rec = new T3695rec();
	private T5679rec t5679rec = new T5679rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T5645rec t5645rec = new T5645rec();
	private T5644rec t5644rec = new T5644rec();
	private T5671rec t5671rec = new T5671rec();
	private T6647rec t6647rec = new T6647rec();
	private T5688rec t5688rec = new T5688rec();
	private T6687rec t6687rec = new T6687rec();
	private T5667rec t5667rec = new T5667rec();
	private T6659rec t6659rec = new T6659rec();
	private Th605rec th605rec = new Th605rec();
	private T7508rec t7508rec = new T7508rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private T5729rec t5729rec = new T5729rec();
	private T6654rec t6654rec = new T6654rec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Billreqrec billreqrec = new Billreqrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private ZonedDecimalData wsaaFpcoFreqFactor = new ZonedDecimalData(11, 5);
	private PackedDecimalData wsaaT5729Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaMinOverdue = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private RertpfDAO rertpfDAO = getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	private AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);

	
	private String strT3629BankCode;
	private Itempf itempf;
	private Descpf descpf;
	private Chdrpf chdrpf;
	private Clrrpf clrrpf;
	private Payrpf payrpf;
	private Ptrnpf ptrnpf;
	private Clrfpf clrfpf;
	private Covrpf covrpf;
	private List<Covrpf> updateCovrList = new ArrayList<>();
	private List<Covrpf> insertCovrList = new ArrayList<>();
	private List<Racdpf> updateRacdList = new ArrayList<>();
	private List<Agcmpf> agcmList = new ArrayList<>();
	private List<Agcmpf> updateAgcmList = new ArrayList<>();
	private List<Agcmpf> insertAgcmList = new ArrayList<>();
	private List<Rertpf> rertList = new ArrayList<>();
	private Annypf annypf;

	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private T5674rec t5674rec = new T5674rec();
	private boolean isProcesFreqChange;
	private boolean isFirstCovr = true;
	private Iterator<Covrpf> iterator;
	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private boolean stampDutyflag;
	private boolean lnkgFlag;
	private boolean billChgFlag;
	private int intT6654Leaddays;
	
	private PackedDecimalData wsaaLastRerateDate = new PackedDecimalData(8, 0);
	private boolean rerateChgFlag;
	private ZonedDecimalData wsaaRerateEffDate = new ZonedDecimalData(8, 0).setUnsigned();
	
	
	/* TABLES */
	private static final String t3678 = "T3678";
	private static final String t1693 = "T1693";
	private static final String t3676 = "T3676";
	private static final String t5674 = "T5674";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t5679 = "T5679";
	private static final String t1688 = "T1688";
	private static final String tr52d = "TR52D";
	private static final String t5687 = "T5687";
	private static final String t5675 = "T5675";
	private static final String t5667 = "T5667";
	private static final String t3695 = "T3695";
	private static final String t3629 = "T3629";
	private static final String t3688 = "T3688";
	private static final String t5644 = "T5644";
	private static final String t5671 = "T5671";
	private static final String t6647 = "T6647";
	private static final String t6659 = "T6659";
	private static final String th605 = "TH605";
	private static final String t7508 = "T7508";
	private static final String tr52e = "TR52E";
	private static final String t5729 = "T5729";
	private static final String t6654 = "T6654";
	
	private static final String IT = "IT";
	private static final String itemCoy = "ITEMCOY";
	private static final String itemTabl = "ITEMTABL";
	private static final String itemItem = "ITEMITEM";
	
	private IntegerData ix = new IntegerData();
	private IntegerData iz = new IntegerData();
	private IntegerData iy = new IntegerData();
	private IntegerData iw = new IntegerData();
	
	/* WSAA-T3688-TABLE */
	private FixedLengthStringData[] wsaaT3688Entries = FLSInittedArray (1000, 34);
	private FixedLengthStringData[] wsaaT3688Key = FLSDArrayPartOfArrayStructure(2, wsaaT3688Entries, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3688Bankcode = FLSDArrayPartOfArrayStructure(2, wsaaT3688Key, 0);
	private FixedLengthStringData[] wsaaT3688Data = FLSDArrayPartOfArrayStructure(32, wsaaT3688Entries, 2);
	private FixedLengthStringData[] wsaaT3688Glmap01 = FLSDArrayPartOfArrayStructure(14, wsaaT3688Data, 0);
	private FixedLengthStringData[] wsaaT3688Sign01 = FLSDArrayPartOfArrayStructure(1, wsaaT3688Data, 14);
	private FixedLengthStringData[] wsaaT3688Currcode = FLSDArrayPartOfArrayStructure(3, wsaaT3688Data, 15);
	private ZonedDecimalData[] wsaaT3688Cdcontot = ZDArrayPartOfArrayStructure(2, 0, wsaaT3688Data, 18);
	private FixedLengthStringData[] wsaaT3688Bankacckey = FLSDArrayPartOfArrayStructure(10, wsaaT3688Data, 20);
	private FixedLengthStringData[] wsaaT3688Bbalpfx = FLSDArrayPartOfArrayStructure(2, wsaaT3688Data, 30);
	
	/* WSAA-T3678-TABLE */
	private FixedLengthStringData[] wsaaT3678Entries = FLSInittedArray (1000, 5);
	private FixedLengthStringData[] wsaaT3678Key = FLSDArrayPartOfArrayStructure(2, wsaaT3678Entries, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3678Mandstat = FLSDArrayPartOfArrayStructure(2, wsaaT3678Key, 0);
	private FixedLengthStringData[] wsaaT3678Data = FLSDArrayPartOfArrayStructure(3, wsaaT3678Entries, 2);
	private FixedLengthStringData[] wsaaT3678Gonogoflg = FLSDArrayPartOfArrayStructure(1, wsaaT3678Data, 0);
	private FixedLengthStringData[] wsaaT3678Bacstrcode = FLSDArrayPartOfArrayStructure(2, wsaaT3678Data, 1);
	
	/* WSAA-T3629-TABLE */
	private FixedLengthStringData[] wsaaT3629Entries = FLSInittedArray (1500, 385);
	private FixedLengthStringData[] wsaaT3629Key = FLSDArrayPartOfArrayStructure(8, wsaaT3629Entries, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3629Genarea = FLSDArrayPartOfArrayStructure(377, wsaaT3629Entries, 8);
	private FixedLengthStringData wsaaT3629Test = new FixedLengthStringData(8);
	
	private ZonedDecimalData wsaaJrnseqRtrn = new ZonedDecimalData(4, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaTranseq = new FixedLengthStringData(4).isAPartOf(wsaaJrnseqRtrn, 0, REDEFINE);
	
	private BextpfDAO bextpfDAO =  getApplicationContext().getBean("bextpfDAO", BextpfDAO.class);
	private RbnkpfDAO rbnkpfDAO = getApplicationContext().getBean("rbnkpfDAO", RbnkpfDAO.class);
	
	private Bextpf bextpf;
	
	private Alocnorec alocnorec = new Alocnorec(); 
	private Addrtrnrec addrtrnrec = new Addrtrnrec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private T3688rec t3688rec = new T3688rec();
	private T3678rec t3678rec = new T3678rec();
	private T3676rec t3676rec = new T3676rec();
	private Tr29srec tr29srec = new Tr29srec();
	
	private DescTableDAM descIO = new DescTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(12, 7).init(0).setUnsigned();
	private PackedDecimalData iu = new PackedDecimalData(5, 0).init(0);
	private Dbcstrcpy2 dbcstrcpy2 = new Dbcstrcpy2();
	private static final int wsaaMaxT3676Transcd = 16;
	private ZonedDecimalData wsaaContot = new ZonedDecimalData(2, 0).init(0);
	
	private boolean gstOnCommFlag = false;
    private PackedDecimalData wsaaCompErnInitCommGst = new PackedDecimalData(17, 2);
    private PackedDecimalData wsaaCompErnRnwlCommGst = new PackedDecimalData(17, 2);
    private ZonedDecimalData wsaaRnwcpyPay = new ZonedDecimalData(17, 2);
    private FixedLengthStringData wsaaReporttoAgnt = new FixedLengthStringData(8);
    private List<Acblpf> acblpfInsertAll = new ArrayList<Acblpf>();
    protected  AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
    private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
    private Clntpf clntObj = null;
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, tableLoop1020, loopCheck1030, skipInitializeAgcm2303, callCovr2304, skipBonusWorkbench2304, continue2305, callAgcmbchio2307, servComm2309, renlComm2310, endComm2311, exit2312, exit2329, fee2332, tolerance2333, stampDuty2334, spare2335, taxRelief2336, checkTax2337, bypassDate2340, initialCommEarned2353, initialCommPaid2354, servCommDue2355, servCommEarned2356, rnwCommDue2357, rnwCommEarned2358, rnwCommPaid ,initialOvrdCommDue2359, initialOvrdCommEarned2359, initialOvrdCommPaid2359, b220Call, b280Next, b290Exit, b320Loop, b380Next, b390Exit, reRead6010
	}

	public Ph5mpat() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void mainline0000() {
		/* MAINLINE */
		initialise1000();
		reRateBillCollect2000();
		processFreqChange3000();
		generalHousekeeping4000();
		/* EXIT */
		exitProgram();
	}

	/**
	 * <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	 * </pre>
	 */
	protected void xxxxFatalError() {
		xxxxFatalErrors();
		xxx1ErrorProg();
	}

	protected void xxxxFatalErrors() {
		if (isEQ(syserrrec.statuz, Varcom.bomb)) {
			return;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void xxx1ErrorProg() {
		/* AT */
		atmodrec.statuz.set(Varcom.bomb);
		/* XXXX-EXIT */
		exitProgram();
	}

	protected void initialise1000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case tableLoop1020:
					tableLoop1020();
				case loopCheck1030:
					loopCheck1030();
					transDescription1040();
					updateApa1050();
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void initialise1010() {
		wsaaFirstTime.set("Y");
		comlinkrec.currto.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaAtmdBatchKey.set(atmodrec.batchKey);
		wsaaCompany.set(atmodrec.company);
		/* Read the contract header record. */

		stampDutyflag = FeaConfg.isFeatureExist(wsaaCompany.toString(), "NBPROP01", appVars, IT);
		lnkgFlag = FeaConfg.isFeatureExist(wsaaCompany.toString(), "NBPRP055", appVars, IT);
		gstOnCommFlag = FeaConfg.isFeatureExist(wsaaCompany.toString(), "CTISS007", appVars, "IT");
		
		setUpHeadingCompany1020();
		setUpHeadingDates1040();
		
		chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsaaCompany.toString(), wsaaChdrnum.toString());
		if (chdrpf == null) {
			syserrrec.params.set(wsaaCompany.toString().concat(wsaaChdrnum.toString()));
			xxxxFatalError();
		}
		chdrpf.setGrupkey(wsaaChdrpfGroupkey.toString());
		chdrpf.setMandref(wsaaChdrpfMandref.toString());
		chdrpf.setMembsel(wsaaChdrpfMembsel.toString());
		chdrpf.setPayrnum(wsaaChdrpfPayrnum.toString());
		chdrpf.setBillchnl(wsaaChdrpfBillchnl.toString());
		
		Chdrpf updateChdrpf = new Chdrpf();
		updateChdrpf.setUniqueNumber(chdrpf.getUniqueNumber());

		ptrnpf = new Ptrnpf();
		ptrnpf.setPtrneff(chdrpf.getBtdate());

		payrpf = populatePayrpf1011();
		if (payrpf == null) {
			syserrrec.params.set(wsaaCompany.toString().concat(wsaaChdrnum.toString()));
			xxxxFatalError();
		}
		payrpf.setGrupnum(wsaaChdrpfGroupkey.toString());
		payrpf.setGrupcoy(wsaaPayrpfGrupcoy.toString());
		payrpf.setMembsel(wsaaChdrpfMembsel.toString());
		payrpf.setMandref(wsaaChdrpfMandref.toString());
		if(isNE(wsaaPayrpfIncomeSeqNo,SPACES)) {
			payrpf.setIncomeSeqNo(Integer.parseInt(wsaaPayrpfIncomeSeqNo.toString()));
		} else {
			payrpf.setIncomeSeqNo(0);
		}
		payrpf.setBillchnl(wsaaChdrpfBillchnl.toString());
		payrpf.setOrgbillcd(wsaaPayrpfOrigBillcd.toInt());
		
		Payrpf updatePayrpf = new Payrpf();
		updatePayrpf.setUniqueNumber(payrpf.getUniqueNumber());
		wsaaCurrptdt.set(payrpf.getPtdate());
		updateChdrpf.setCurrto(wsaaCurrptdt.toInt());
		
		rertList = rertpfDAO.getRertpfList(wsaaCompany.toString(), wsaaChdrnum.toString(),"1",0);
		
		clrrpf = clrrpfDAO.getClrfRecord(chdrpf.getChdrpfx(), payrpf.getChdrcoy(),(payrpf.getChdrnum() + payrpf.getPayrseqno()).substring(0, 9), "PY");
		
		/* Read the client role file to get the payer number. */
		clrfpf = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrpf.getChdrpfx(), payrpf.getChdrcoy(),
				payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1)),
				"PY");
		if (clrfpf == null) {
			syserrrec.statuz.set(Varcom.mrnf);
			syserrrec.params.set(payrpf.getChdrcoy().concat(
					payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1))));
			xxxxFatalError();
		}
		wsaaIndex.set(0);
		wsaaPremDiff.set(0);
		compute(wsaaTranno, 0).set(add(chdrpf.getTranno(), 1));
		/* Calculate the first date to pay-to date. */
		/* MOVE SPACES TO DTC2-FUNCTION. */
		/* MOVE CHDRLIF-BTDATE TO DTC2-INT-DATE-1. */
		/* MOVE CHDRLIF-BILLFREQ TO DTC2-FREQUENCY. */
		/* MOVE PAYR-BTDATE TO DTC2-INT-DATE-1. <018> */
		/* MOVE PAYR-BILLFREQ TO DTC2-FREQUENCY. <018> */
		/* MOVE 1 TO DTC2-FREQ-FACTOR. */
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC. */
		/* IF DTC2-STATUZ NOT = O-K */
		/* MOVE DTC2-DATCON2-REC TO SYSR-PARAMS */
		/* PERFORM XXXX-FATAL-ERROR. */
		/* MOVE DTC2-INT-DATE-2 TO WSAA-CURRENT-PAYUPTO-DATE. */
		/* Read the Premium Billing file to find first billing date. */

		Linspf linspf = linspfDAO.getLinsRecordByCoyAndNumAndflag(payrpf.getChdrcoy(), payrpf.getChdrnum(),
				payrpf.getValidflag());
		// performance improvement -- Anjali

		/* Call DATCON3 to work out if the first premium was a */
		/* pro-rate premium. */
		if (linspf != null) {
			wsaaLinsBtdate.set(linspf.getInstfrom());
		} else {
			wsaaLinsBtdate.set(payrpf.getBtdate());
		}
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(chdrpf.getOccdate());
		datcon3rec.intDate2.set(wsaaLinsBtdate);
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			xxxxFatalError();
		}
		wsaaFreqFactor.set(datcon3rec.freqFactor);
		/* If the original commence date and the first billing */
		/* date are a whole billing frequency apart, use the risk */
		/* commence dates day for future billing, otherwise use */
		/* the first billing dates day for the first renewal, and */
		/* there after the first LINS install from dates day. */
		/* The last two dates are moved previously, before calling */
		/* DATCON3. */
		if (isEQ(wsaaFreqDecimal, ZERO)) {
			wsaaLinsBtdate.set(chdrpf.getOccdate());
		}
		/* Check to see if the commence date is at the end of the month. */
		wsaaEndOfMonth = "N";
		wsaaEomDate.set(chdrpf.getOccdate());
		checkEndOfMonth9000();
		wsaaEom1stDate.set(wsaaEndOfMonth);
		/* Check to see if the 1ST bill date is at the end of the month, */
		/* and if the month is February. */
		wsaaEndOfMonth = "N";
		wsaaEomDate.set(wsaaLinsBtdate);
		if (isEQ(wsaaEomMonth, 2)) {
			checkEndOfMonth9000();
		}
		/* If the commence date was not at the end of the month, and */
		/* the 1st billing date was in February and at the end of the */
		/* month, set the billing day to the original commence day. */
		if (isEQ(wsaaEndOfMonth, "Y") && isEQ(wsaaEom1stDate, "N")) {
			wsaaLinsBtdate.set(chdrpf.getOccdate());
		}
		wsaaCurrentPayuptoDate.set(wsaaAdvptdat);
		/* Get todays date. */
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Read the contract definition on T5688. */
		List<Itempf> list = itemDao.getItdmByFrmdate(wsaaCompany.toString(), t5688, chdrpf.getCnttype(),
				chdrpf.getOccdate());

		if (list == null || list.isEmpty()) {
			/* MOVE ITDM-STATUZ TO SYSR-STATUZ */
			syserrrec.params.set(wsaaCompany.toString().concat(t5688).concat(chdrpf.getCnttype()));
			syserrrec.statuz.set(e308);
			xxxxFatalError();
		} else {
			t5688rec.t5688Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
		}
		
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/

		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(t5674);
		itempf.setItemitem(t5688rec.feemeth.toString());
		itempf = itemDao. getItempfRecord(itempf);

		t5674rec.t5674Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		wsaaAllCovExp = "Y";
		wsaaAllRidExp = "Y";
		wsaaRerateDate.set(varcom.vrcmMaxDate);
		/* Initialize all the commission totals. */
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaJrnseq.set(0);
		wsaaJrnseqRtrn.set(0);
		wsaaRnwcpyDue.set(0);
		wsaaCompErnInitCommGst.set(0);
		wsaaCompErnRnwlCommGst.set(0);
		wsaaRnwcpyPay.set(0);
		/* Read T5679 for validation of status of future COVR's. */
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(atrtTransCode.toString());
		itempf = itemDao.getItempfRecord(itempf);
		if (itempf == null) {
			syserrrec.params.set(wsaaCompany.toString().concat(t5679).concat(atrtTransCode.toString()));
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		wsaaNotEnoughCash = "N";
		/* Read T5645 to get the financial accounting rules */
		/* MOVE 1 TO WSAA-FREQUENCY. */
		/* Read T5645 for accounting details. This call will read the 2nd */
		/* page of the table item(this item has more than one page of */
		/* accounting details). This first page will be read in individual */
		/* section. The details read here will be stored away in working */
		/* storage and to be used later on when calling UNITALOC(from */
		/* T5671). */
		readT5645Page34200();
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq("01");
		itempf = itemDao.getItempfRecordBySeq(itempf);

		if (itempf != null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));//third call 
		} else {
			t5645rec.t5645Rec.set(SPACES);
		}

		wsaaSub1.set(1);
		wsaaSub2.set(1);
		List<Chdrpf> chdrUpdatList = new ArrayList<>();
		chdrUpdatList.add(updateChdrpf);
		chdrpfDAO.updateInvalidChdrRecord(chdrUpdatList);
		List<Payrpf> payrUpdatList = new ArrayList<>();
		payrUpdatList.add(updatePayrpf);
		payrpfDAO.updatePayrRecord(payrUpdatList, wsaaCurrptdt.toInt());
		readCovrpf();
		covrpfDAO.updateCovrRecord(updateCovrList, wsaaCurrptdt.toInt());
		covrpfDAO.insertCovrpfList(insertCovrList);
		racdpfDAO.updateRacdPendcsttoProrateflagBulk(updateRacdList);
		agcmpfDAO.updateInvalidRecord(updateAgcmList);
		agcmpfDAO.insertAgcmpfList(insertAgcmList);
	}
	
	protected void covrRiskStatus5110(Covrpf covr) {
		wsaaValidStatus.set("N");
		if (isEQ(covr.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getStatcode(), t5679rec.covRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		} else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getStatcode(), t5679rec.ridRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
	}
	
	protected void covrPremStatus5120(Covrpf covr) {
		wsaaValidStatus.set("N");
		if (isEQ(covr.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getPstatcode(), t5679rec.covPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		} else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covr.getPstatcode(), t5679rec.ridPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
	}

	protected void readCovrpf() {
		Covrpf covr;
		List<Covrpf> covrpflist = covrpfDAO.getCovrsurByComAndNum(wsaaCompany.toString(), wsaaChdrnum.toString());
		if (!covrpflist.isEmpty()) {
			for (Covrpf c : covrpflist) {
				covrRiskStatus5110(c);
				if (isNE(wsaaValidStatus, "Y")) {
					continue;
				}
				/*covrPremStatus5120(c);
				if (isNE(wsaaValidStatus, "Y")) {
					continue;
					//For now it is not required in future if we need it we can do premium status check
				}*/
				covr = new Covrpf();
				covr.setUniqueNumber(c.getUniqueNumber());
				covr.setValidflag("2");
				covr.setCurrto(wsaaCurrptdt.toInt());
				updateCovrList.add(covr);
				c.setTranno(chdrpf.getTranno() + 1);
				c.setCurrfrom(wsaaCurrptdt.toInt());
				insertCovrList.add(c);
				readRacdData(c);
				readAgcmData(c);
			}
		}
	}

	protected void readRacdData(Covrpf c) {
		List<Racdpf> racdList = racdpfDAO.searchRacdstRecord(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(),
				c.getRider(), c.getPlanSuffix(), "1");
		for (Racdpf racd : racdList) {
			racd.setTranno(wsaaTranno.toInt());
			racd.setPendcstto(wsaaAdvptdat.toInt());
			racd.setProrateflag("Y");
			updateRacdList.add(racd);
		}
	}

	protected void readAgcmData(Covrpf c) {
		agcmList = agcmpfDAO.searchAgcmstRecord(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(),
				c.getRider(), c.getPlanSuffix(), "1");
		Agcmpf agcmpf = new Agcmpf();
		for (Agcmpf agcm : agcmList) {
			agcmpf.setUniqueNumber(agcm.getUniqueNumber());
			agcmpf.setValidflag("2");
			agcmpf.setCurrto(wsaaCurrptdt.toInt());
			updateAgcmList.add(agcmpf);
			agcm.setCurrfrom(wsaaCurrptdt.toInt());
			agcm.setTranno(chdrpf.getTranno() + 1);
			insertAgcmList.add(agcm);
		}
	}

	protected void tableLoop1020() {
		if (isEQ(t5645rec.sacscode[wsaaSub2.toInt()], SPACES) && isEQ(t5645rec.sacstype[wsaaSub2.toInt()], SPACES)
				&& isEQ(t5645rec.glmap[wsaaSub2.toInt()], SPACES)) {
			wsaaSub2.set(16);
			goTo(GotoLabel.loopCheck1030);
		}
		wsaaT5645Cnttot[wsaaSub1.toInt()].set(t5645rec.cnttot[wsaaSub2.toInt()]);
		wsaaT5645Glmap[wsaaSub1.toInt()].set(t5645rec.glmap[wsaaSub2.toInt()]);
		wsaaT5645Sacscode[wsaaSub1.toInt()].set(t5645rec.sacscode[wsaaSub2.toInt()]);
		wsaaT5645Sacstype[wsaaSub1.toInt()].set(t5645rec.sacstype[wsaaSub2.toInt()]);
		wsaaT5645Sign[wsaaSub1.toInt()].set(t5645rec.sign[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
		
	}

	protected void loopCheck1030() {
		if (isLT(wsaaSub2, 16)) {
			goTo(GotoLabel.tableLoop1020);
		}
		if(isEQ(wsaaSub1, 16)) {
			itempf = new Itempf();
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsaaCompany.toString());
			itempf.setItemtabl(t5645);
			itempf.setItemitem(wsaaProg.toString());
			itempf.setItemseq("02");
			itempf = itemDao.getItempfRecordBySeq(itempf);//second call

			if (itempf != null) {
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				wsaaSub2.set(1);
				goTo(GotoLabel.tableLoop1020);
			} else {
				t5645rec.t5645Rec.set(SPACES);
			}
			
		}
	}

	/**
	 * <pre>
	*    MOVE T5645-CNTTOT-01        TO WSAA-T5645-CNTTOT (01). <013>>
	*    MOVE T5645-SACSCODE-01      TO WSAA-T5645-SACSCODE (01).<013>
	*    MOVE T5645-SACSTYPE-01      TO WSAA-T5645-SACSTYPE (01).<013>
	*    MOVE T5645-GLMAP-01         TO WSAA-T5645-GLMAP (01).  <013>>
	*    MOVE T5645-CNTTOT-02        TO WSAA-T5645-CNTTOT (02). <013>>
	*    MOVE T5645-SACSCODE-02      TO WSAA-T5645-SACSCODE (02).<013>
	*    MOVE T5645-SACSTYPE-02      TO WSAA-T5645-SACSTYPE (02).<013>
	*    MOVE T5645-GLMAP-02         TO WSAA-T5645-GLMAP (02).   <013>
	*    MOVE T5645-SIGN-02          TO WSAA-T5645-SIGN (02).    <013>
	*    MOVE T5645-CNTTOT-03        TO WSAA-T5645-CNTTOT (03).  <013>
	*    MOVE T5645-SACSCODE-03      TO WSAA-T5645-SACSCODE (03).<013>
	*    MOVE T5645-SACSTYPE-03      TO WSAA-T5645-SACSTYPE (03).<013>
	*    MOVE T5645-GLMAP-03         TO WSAA-T5645-GLMAP (03).   <013>
	*    MOVE T5645-SIGN-03          TO WSAA-T5645-SIGN (03).    <013>
	*    MOVE T5645-CNTTOT-04        TO WSAA-T5645-CNTTOT (04).  <013>
	*    MOVE T5645-SACSCODE-04      TO WSAA-T5645-SACSCODE (04).<013>
	*    MOVE T5645-SACSTYPE-04      TO WSAA-T5645-SACSTYPE (04).<013>
	*    MOVE T5645-GLMAP-04         TO WSAA-T5645-GLMAP (04).   <013>
	*    MOVE T5645-SIGN-04          TO WSAA-T5645-SIGN (04).    <013>
	*    MOVE T5645-CNTTOT-05        TO WSAA-T5645-CNTTOT (05).  <018>
	*    MOVE T5645-SACSCODE-05      TO WSAA-T5645-SACSCODE (05).<018>
	*    MOVE T5645-SACSTYPE-05      TO WSAA-T5645-SACSTYPE (05).<018>
	*    MOVE T5645-GLMAP-05         TO WSAA-T5645-GLMAP (05).   <018>
	*    MOVE T5645-SIGN-05          TO WSAA-T5645-SIGN (05).    <018>
	*****MOVE T5645-SACSCODE-01      TO WSAA-T5645-SACSCODE-16.
	*****MOVE T5645-SACSTYPE-01      TO WSAA-T5645-SACSTYPE-16.
	*****MOVE T5645-GLMAP-01         TO WSAA-T5645-GLMAP-16.
	*                                                         <V4L001>
	 * </pre>
	 */
	protected void transDescription1040() {
		/* <V4L001> */
		/* Get transaction description. Clone from 2300-COLLECTION section. */
		/* <V4L001> */
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), t1688, atrtTransCode.toString(),
				wsaaCompany.toString(), atmodrec.language.toString());
		if (descpf == null) {
			descpf = new Descpf();
			descpf.setLongdesc("");
		}
		wsaaTransDesc.set(descpf.getLongdesc());
	}

	/**
	 * <pre>
	*                                                         <V4L001>
	 * </pre>
	 */
	protected void updateApa1050() {
		/* <V4L001> */
		/* This paragraph is required before the 2200-BILLING so that */
		/* suspense will have enough money for the amount due. If there */
		/* has extract suspense for the amount due then NEXT SENTENCE. If */
		/* Advance Premium Deposit amount (APA) is required to pay the amount due */
		/* then move DELT to withdraw money from APA and put into suspense. */
		/* Otherwise, take the extra money from suspense and try to post it */
		/* into APA. <V4L001> */
		/* <V4L001> */
		initialize(rlpdlonrec.rec);
		if (isEQ(wsaaApaRequired, ZERO)) {
			/* NEXT_SENTENCE */
		} else {
			if (isGT(wsaaApaRequired, ZERO)) {
				rlpdlonrec.function.set(Varcom.delt);
				rlpdlonrec.prmdepst.set(wsaaApaRequired);
			} else {
				rlpdlonrec.function.set(Varcom.insr);
				compute(wsaaApaRequired, 2).set(mult(wsaaApaRequired, (-1)));
				rlpdlonrec.prmdepst.set(wsaaApaRequired);
			}
			updateApa1100();
		}
		/* READ TR52D. */
		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(chdrpf.getChdrcoy().toString());
		itempf.setItemtabl(tr52d);
		itempf.setItemitem(atrtTransCode.toString());
		itempf = itemDao.getItempfRecord(itempf);

		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx(IT);
			itempf.setItemcoy(chdrpf.getChdrcoy().toString());
			itempf.setItemtabl(tr52d);
			itempf.setItemitem("***");
			itempf = itemDao.getItempfRecord(itempf);
			if (itempf == null) {
				syserrrec.statuz.set(Varcom.mrnf);
				syserrrec.params
						.set(IT.concat(chdrpf.getChdrcoy().toString()).concat(tr52d).concat("***"));
				xxxxFatalError();
			}
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

	/**
	 * <pre>
	*                                                         <V4L001>
	 * </pre>
	 */
	protected void updateApa1100() {

		rlpdlonrec.pstw.set("PSTW");
		rlpdlonrec.chdrcoy.set(chdrpf.getChdrcoy());
		rlpdlonrec.chdrnum.set(chdrpf.getChdrnum());
		wsaaRdockey.rdocRdocpfx.set("CH");
		wsaaRdockey.rdocRdoccoy.set(chdrpf.getChdrcoy());
		wsaaRdockey.rdocRdocnum.set(chdrpf.getChdrnum());
		wsaaRdockey.rdocTranseq.set(wsaaJrnseq);
		rlpdlonrec.doctkey.set(wsaaRdockey);
		rlpdlonrec.effdate.set(chdrpf.getOccdate());
		rlpdlonrec.currency.set(payrpf.getBillcurr());
		rlpdlonrec.tranno.set(wsaaTranno);
		rlpdlonrec.transeq.set(wsaaJrnseq);
		rlpdlonrec.longdesc.set(wsaaTransDesc);
		rlpdlonrec.language.set(atmodrec.language);
		rlpdlonrec.batchkey.set(atmodrec.batchKey);
		rlpdlonrec.authCode.set(atrtTransCode);
		rlpdlonrec.time.set(wsaaTransactionTime);
		rlpdlonrec.date_var.set(wsaaTransactionDate);
		rlpdlonrec.user.set(wsaaUser);
		rlpdlonrec.termid.set(wsaaTermid);
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if (isNE(rlpdlonrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(rlpdlonrec.statuz);
			syserrrec.params.set(rlpdlonrec.rec);
			xxxxFatalError();
		}
		wsaaJrnseq.set(rlpdlonrec.transeq);
	}

	protected void reRateBillCollect2000() {
		/* Set COVR parameters so that we start at beginning of COVeRages */
		/* associated with the specified Contract Number. */
		billing2200();
		if (isEQ(wsaaNotEnoughCash, "Y")) {
			produceBextRecord();
			//create receipt
			createReceipts();
			updateBextpf();
		}
		collection2300();
		updateCovrStatus2370();
	}



	protected void validateCovrStatus2110() {
		/* VALIDATE-COVR-STATUS */
		/* IF T5679-COV-RISK-STAT(WSAA-INDEX) NOT = SPACE */
		/* IF (T5679-COV-RISK-STAT(WSAA-INDEX) = COVR-STATCODE) AND */
		/* (T5679-COV-PREM-STAT(WSAA-INDEX) = COVR-PSTATCODE) */
		/* MOVE 13 TO WSAA-INDEX */
		/* MOVE 'Y' TO WSAA-VALID-STATUS */
		/* GO TO 2119-EXIT. */
		/* ADD 1 TO WSAA-INDEX. */
		if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12) || isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)) {
				if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getPstatcode())) {
					wsaaValidStatus.set("Y");
				}
			}
		}
		/* EXIT */
	}

	protected void validateRiderStatus2120() {
		/* VALIDATE-RIDER-STATUS */
		/* IF T5679-RID-RISK-STAT(WSAA-INDEX) NOT = SPACE */
		/* IF (T5679-RID-RISK-STAT(WSAA-INDEX) = COVR-STATCODE) AND */
		/* (T5679-RID-PREM-STAT(WSAA-INDEX) = COVR-PSTATCODE) */
		/* MOVE 13 TO WSAA-INDEX */
		/* MOVE 'Y' TO WSAA-VALID-STATUS */
		/* GO TO 2129-EXIT. */
		/* ADD 1 TO WSAA-INDEX. */
		if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12) || isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)) {
				if (isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrpf.getPstatcode())) {
					wsaaValidStatus.set("Y");
				}
			}
		}
		/* EXIT */
	}

	/**
	 * <pre>
	* Billing.
	 * </pre>
	 */
	protected void billing2200() {
		/* Read T5645 for suspense balance. */
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T3695 to check the sign. */
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/* MOVE CHDRLIF-BTDATE TO WSAA-BTDATE. */
		wsaaBtdate.set(payrpf.getBtdate());
		wsaaBillcd.set(payrpf.getBillcd());
		wsaaNextdt.set(payrpf.getNextdate());
		/* Read ACBL */
		acblIO.setParams(SPACES);
		acblIO.setRldgacct(chdrpf.getChdrnum());
		acblIO.setRldgcoy(chdrpf.getChdrcoy());
		/* MOVE CHDRLIF-CNTCURR TO ACBL-ORIGCURR. <022> */
		acblIO.setOrigcurr(chdrpf.getBillcurr());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(Varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(), Varcom.oK)) && (isNE(acblIO.getStatuz(), Varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			xxxxFatalError();
		}
		/* Multiply the balance by the sign from T3695. */
		if (isEQ(acblIO.getStatuz(), Varcom.mrnf)) {
			wsaaSuspAvail.set(0);
		} else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspAvail, 2).set(mult(acblIO.getSacscurbal(), -1));
			} else {
				wsaaSuspAvail.set(acblIO.getSacscurbal());
			}
		}
		/* MOVE SPACE TO SACSRNL-PARAMS. */
		/* MOVE CHDRLIF-CHDRNUM TO SACSRNL-CHDRNUM. */
		/* MOVE CHDRLIF-CHDRCOY TO SACSRNL-CHDRCOY. */
		/* MOVE CHDRLIF-CNTCURR TO SACSRNL-CNTCURR. */
		/* MOVE T5645-SACSCODE-01 TO SACSRNL-SACSCODE. */
		/* MOVE T5645-SACSTYPE-01 TO SACSRNL-SACSTYP. */
		/* MOVE READR TO SACSRNL-FUNCTION. */
		/* MOVE SACSRNLREC TO SACSRNL-FORMAT. */
		/* CALL 'SACSRNLIO' USING SACSRNL-PARAMS. */
		/* IF (SACSRNL-STATUZ NOT = O-K) AND */
		/* (SACSRNL-STATUZ NOT = MRNF) */
		/* MOVE SACSRNL-PARAMS TO SYSR-PARAMS */
		/* PERFORM XXXX-FATAL-ERROR. */
		/* Multiply the balance by the sign from T3695. */
		/* IF SACSRNL-STATUZ = MRNF */
		/* MOVE 0 TO WSAA-SUSP-AVAIL */
		/* ELSE */
		/* IF T3695-SIGN = '-' */
		/* MULTIPLY SACSRNL-SACSCURBAL BY -1 */
		/* GIVING WSAA-SUSP-AVAIL */
		/* ELSE */
		/* MOVE SACSRNL-SACSCURBAL TO WSAA-SUSP-AVAIL. */
		/* IF WSAA-SUSP-AVAIL NOT > 0 < */
		/* MOVE 'Y' TO WSAA-NOT-ENOUGH-CASH < */
		/* GO TO 2209-EXIT. < */
		/* Create a LINS record for each new payment. */
		wsaaTotAmount.set(0);
		produceLinsRecord2210();
		/* IF WSAA-SUSP-AVAIL < LINSRNL-CBILLAMT */
		/* MOVE 'Y' TO WSAA-NOT-ENOUGH-CASH */
		/* GO TO 2209-EXIT. */
		if (isLT(wsaaSuspAvail, wsaaNetCbillamt)) {
			/* Read the latest premium tolerance allowed. */
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(atrtCompany);
			itemIO.setItemtabl(t5667);
			wsbbT5667Trancd.set(atrtTransCode);
			wsbbT5667Curr.set(payrpf.getBillcurr());
			itemIO.setItemitem(wsbbT5667Key);
			itemIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				xxxxFatalError();
			}
			if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
				t5667rec.t5667Rec.set(SPACES);
			} else {
				t5667rec.t5667Rec.set(itemIO.getGenarea());
			}
			/*-Check the tolerance amount against T5667 table entry read       */
			/* above. */
			for (wsaaSub.set(1); true; wsaaSub.add(1)) {
				if (isEQ(payrpf.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
					compute(wsaaTol, 3).setRounded(div((mult(wsaaNetCbillamt, t5667rec.prmtol[wsaaSub.toInt()])), 100));
					zrdecplrec.amountIn.set(wsaaTol);
					c000CallRounding();
					wsaaTol.set(zrdecplrec.amountOut);
					if (isGT(wsaaTol, t5667rec.maxAmount[wsaaSub.toInt()])) {
						wsaaTol.set(t5667rec.maxAmount[wsaaSub.toInt()]);
					}
					wsaaTol2.set(0);
					if (isGT(t5667rec.maxamt[wsaaSub.toInt()], 0) && (isEQ(wsaaAgtTerminatedFlag, "N")
							|| (isEQ(wsaaAgtTerminatedFlag, "Y") && isEQ(t5667rec.sfind, "2")))) {
						compute(wsaaTol2, 3).setRounded(div((mult(wsaaNetCbillamt, t5667rec.prmtoln[wsaaSub.toInt()])), 100));
						zrdecplrec.amountIn.set(wsaaTol2);
						c000CallRounding();
						wsaaTol2.set(zrdecplrec.amountOut);
						if (isGT(wsaaTol2, t5667rec.maxamt[wsaaSub.toInt()])) {
							wsaaTol2.set(t5667rec.maxamt[wsaaSub.toInt()]);
						}
					}
				}
				if (isEQ(wsaaSub, 11)) {
					break;
				}
			}
			if ((setPrecision(wsaaSuspAvail, 2) && isLT(wsaaSuspAvail, (sub(wsaaNetCbillamt, wsaaTol))))) {
				if ((setPrecision(wsaaSuspAvail, 2) && isLT(wsaaSuspAvail, (sub(wsaaNetCbillamt, wsaaTol2))))) {
					wsaaNotEnoughCash = "Y";
				} else {
					setPrecision(linsrnlIO.getInstamt03(), 2);
					linsrnlIO.setInstamt03(sub(wsaaNetCbillamt, wsaaSuspAvail));
					wsaaToleranceFlag = "2";
				}
			} else {
				setPrecision(linsrnlIO.getInstamt03(), 2);
				linsrnlIO.setInstamt03(sub(wsaaNetCbillamt, wsaaSuspAvail));
				wsaaToleranceFlag = "1";
			}
		}
	}

	protected void produceLinsRecord2210() {
		produceLinsRecord2211();
		otherFields2215();
	}

	protected void produceLinsRecord2211() {
		payrpf.setBtdate(wsaaAdvptdat.toInt());
		chdrpf.setBtdate(wsaaAdvptdat.toInt());

		if(isEQ(wsaaPayrpfBillday,SPACES)) {
			payrpf.setBillcd(this.wsaaAdvptdat.toInt());
			chdrpf.setBillcd(this.wsaaAdvptdat.toInt());
			
			if(isNE(payrpf.getBillday(),SPACES)) {
				getCollectiondate(wsaaAdvptdat.toString(), payrpf.getBillday());
				payrpf.setOrgbillcd(datcon2rec.intDate2.toInt());
				}else {
					payrpf.setOrgbillcd(wsaaAdvptdat.toInt());
				}
		}
		else {
				if(isEQ(wsaaPayrpfOrigBillcd,0)) {
					getCollectiondate(wsaaAdvptdat.toString(), wsaaPayrpfBillday.toString());
					payrpf.setBillcd(datcon2rec.intDate2.toInt());
					chdrpf.setBillcd(datcon2rec.intDate2.toInt());
				}
				else {
					payrpf.setBillcd(wsaaAdvptdat.toInt());
					chdrpf.setBillcd(wsaaAdvptdat.toInt());
				}
		}
		if(isEQ(wsaaPayrpfOrigBillcd,0)) {
			payrpf.setBillday(wsaaPayrpfBillday.toString());
		}
		payrpf.setBillmonth(String.valueOf(payrpf.getBillcd()).substring(4, 6));


		calculateLeadDays();

		/* Subtract the lead days from the billed to date to calculate */
		/* the next billing extract date for the PAYR record. */
		compute(datcon2rec.freqFactor, 0).set((sub(ZERO, intT6654Leaddays)));
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(payrpf.getBillcd());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}

		payrpf.setNextdate(datcon2rec.intDate2.toInt()); // Need to check
		/* Store the waiver amount and the contract fee and calculate */

		/* IF CHDRLIF-BTDATE > CHDRLIF-SINSTTO */
		/* MOVE T5679-SET-CN-PREM-STAT TO CHDRLIF-PSTATCODE */
		/* MOVE WSAA-BTDATE TO CHDRLIF-BTDATE */
		/* GO TO 2219-EXIT. */
		/* <018> */
		/* IF PAYR-BTDATE > CHDRLIF-SINSTTO <018> */
		/* MOVE T5679-SET-CN-PREM-STAT TO PAYR-PSTATCODE <018> */
		/* MOVE WSAA-BTDATE TO PAYR-BTDATE <018> */
		/* GO TO 2219-EXIT. <018> */
		/* <018> */
		/* Look up the tax relief subroutine on T6687. */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(IT);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl("T6687");
		itemIO.setItemitem(t5688rec.taxrelmth);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.oK)) {
			t6687rec.t6687Rec.set(itemIO.getGenarea());
		} else {
			t6687rec.t6687Rec.set(SPACES);
		}

		compute(wsaaNetBillamt, 2).set(wsaaProDaysPrem);

		/* Convert the net bill amount if the contract currency */
		/* is not the same as billing currency. */
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.currIn.set(payrpf.getCntcurr());
		conlinkrec.currOut.set(payrpf.getBillcurr());
		conlinkrec.amountIn.set(wsaaNetBillamt);
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		/* MOVE WSAA-TRANSACTION-DATE TO CLNK-CASHDATE. */
		conlinkrec.cashdate.set(datcon1rec.intDate);
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(wsaaCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			conlinkrec.amountOut.set(0);
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		c000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		if (isEQ(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			wsaaNetCbillamt.set(wsaaNetBillamt);
		} else {
			wsaaNetCbillamt.set(conlinkrec.amountOut);
		}
		/* Set up LINS record.(LINS holds installment amounts in contract */
		/* currency and the total installment in billing currency.) */

		linsrnlIO.setParams(SPACES);
		linsrnlIO.setBillcd(ZERO);
		linsrnlIO.setInstamt01(wsaaProDaysPrem);
		linsrnlIO.setInstamt02(wsaaProDaysFee);
		linsrnlIO.setInstamt03(payrpf.getSinstamt03());
		linsrnlIO.setInstamt04(payrpf.getSinstamt04());
		linsrnlIO.setInstamt05(payrpf.getSinstamt05());
		linsrnlIO.setInstamt06(add(linsrnlIO.getInstamt01(), linsrnlIO.getInstamt02(), linsrnlIO.getInstamt03(),
				linsrnlIO.getInstamt04(), linsrnlIO.getInstamt05()));
		/* Convert contract amount(total) to billing amount if contract */
		/* currency is not the same as billing currency. */
		/* IF CHDRLIF-CNTCURR = CHDRLIF-BILLCURR */
		if (isEQ(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			linsrnlIO.setCbillamt(linsrnlIO.getInstamt06());
			return;
		}
		conlinkrec.clnk002Rec.set(SPACES);
		/* MOVE CHDRLIF-CNTCURR TO CLNK-CURR-IN. */
		/* MOVE CHDRLIF-BILLCURR TO CLNK-CURR-OUT. */
		conlinkrec.currIn.set(payrpf.getCntcurr());
		conlinkrec.currOut.set(payrpf.getBillcurr());
		conlinkrec.amountIn.set(linsrnlIO.getInstamt06());
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		/* MOVE WSAA-TRANSACTION-DATE TO CLNK-CASHDATE. */
		conlinkrec.cashdate.set(datcon1rec.intDate);
		/* MOVE 'CVRT' TO CLNK-FUNCTION */
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(wsaaCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			conlinkrec.amountOut.set(0);
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		c000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		linsrnlIO.setCbillamt(conlinkrec.amountOut);
	}

	protected void getCollectiondate(String todaydate, String billday) {
		String dayFrom = todaydate.substring(6, 8);
		if(isLT(billday,dayFrom)) {
			
			datcon2rec.intDate1.set(todaydate);
		    datcon2rec.intDate2.set(ZERO);
		    datcon2rec.freqFactor.set("01");
		    datcon2rec.frequency.set("12");
		    callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		    if (isNE(datcon2rec.statuz, Varcom.oK)) {
		        syserrrec.statuz.set(datcon2rec.statuz);
		        xxxxFatalError();
		    }
			compute(datcon2rec.freqFactor, 0).set(sub(billday,dayFrom));
			
		    datcon2rec.intDate1.set(datcon2rec.intDate2);
		    datcon2rec.intDate2.set(ZERO);
		    datcon2rec.frequency.set("DY");
		    callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		    if (isNE(datcon2rec.statuz, Varcom.oK)) {
		        syserrrec.statuz.set(datcon2rec.statuz);
		        xxxxFatalError();
		    }
	}
	else {
			compute(datcon2rec.freqFactor, 0).set(sub(billday,dayFrom));
			
		    datcon2rec.intDate1.set(todaydate);
		    datcon2rec.intDate2.set(ZERO);
		    datcon2rec.freqFactor.set(datcon2rec.freqFactor);
		    datcon2rec.frequency.set("DY");
		    callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		    if (isNE(datcon2rec.statuz, Varcom.oK)) {
		        syserrrec.statuz.set(datcon2rec.statuz);
		        xxxxFatalError();
		    }

		}
	}
	protected void calculateLeadDays() {
		FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
		FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
		FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);

		boolean BTPRO028Permission = FeaConfg.isFeatureExist("2", "BTPRO028", appVars, IT);
		if (BTPRO028Permission) {
			String key = payrpf.getBillchnl().trim() + chdrpf.getCnttype().trim() + payrpf.getBillfreq().trim();
			if (!readT6654(key)) {
				key = payrpf.getBillchnl().trim().concat(chdrpf.getCnttype().trim()).concat("**");
				if (!readT6654(key)) {
					key = payrpf.getBillchnl().trim().concat("*****");
					if (!readT6654(key)) {
						syserrrec.params.set(t6654);
						syserrrec.statuz.set(Varcom.mrnf);
						xxxxFatalError();
					}
				}
			}
		} else {
			wsaaT6654Item.set(SPACES);
			wsaaT6654Billchnl.set(payrpf.getBillchnl());
			wsaaT6654Cnttype.set(chdrpf.getCnttype());
			List<Itempf> itemList = itemDao.getAllitemsbyCurrency(IT, payrpf.getChdrcoy(), t6654,
					wsaaT6654Item.toString());
			if (itemList == null || itemList.isEmpty()) {
				syserrrec.params.set(atrtTransCode);
				syserrrec.statuz.set(Varcom.mrnf);
				xxxxFatalError();
			}

			if (itemList != null && !itemList.isEmpty()) {
				t6654rec.t6654Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
				return;
			}
			wsaaT6654Cnttype.set("***");
			itemList = itemDao.getAllitemsbyCurrency(IT, payrpf.getChdrcoy(), t6654, wsaaT6654Item.toString());
			if (itemList == null || itemList.isEmpty()) {
				syserrrec.params.set(t6654);
				syserrrec.statuz.set(Varcom.mrnf);
				xxxxFatalError();
				return;
			}
			t6654rec.t6654Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		}
		intT6654Leaddays = t6654rec.leadDays.toInt();
	}

	protected boolean readT6654(String key) {
		itempf = new Itempf();
		ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

		itempf.setItempfx(IT);
		itempf.setItemcoy(payrpf.getChdrcoy());
		itempf.setItemtabl(t6654);
		itempf.setItemitem(key);
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null != itempf) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			return true;
		}
		return false;
	}

	/**
	 * <pre>
	* We now have the record we want.
	 * </pre>
	 */

	protected void otherFields2215() {
		linsrnlIO.setChdrnum(chdrpf.getChdrnum());
		linsrnlIO.setChdrcoy(chdrpf.getChdrcoy());
		linsrnlIO.setBranch(atrtBranch);
		linsrnlIO.setInstfrom(wsaaBtdate);
		/* MOVE CHDRLIF-CNTCURR TO LINSRNL-CNTCURR. */
		/* MOVE CHDRLIF-BILLCURR TO LINSRNL-BILLCURR. */
		/* MOVE CHDRLIF-BTDATE TO LINSRNL-INSTTO. */
		/* MOVE CHDRLIF-BILLFREQ TO LINSRNL-INSTFREQ. */
		linsrnlIO.setCntcurr(payrpf.getCntcurr());
		linsrnlIO.setBillcurr(payrpf.getBillcurr());
		linsrnlIO.setInstto(wsaaAdvptdat);
		linsrnlIO.setInstfreq(payrpf.getBillfreq());
		/* MOVE '0' TO LINSRNL-PAYFLAG. */
		linsrnlIO.setPayflag("O");
		linsrnlIO.setTranscode(atrtTransCode);
		linsrnlIO.setInstjctl(atrtBatchKey);
		linsrnlIO.setValidflag(vflagcpy.inForceVal);
		/* MOVE CHDRLIF-BILLCHNL TO LINSRNL-BILLCHNL. */
		linsrnlIO.setBillchnl(payrpf.getBillchnl());
		linsrnlIO.setPayrseqno(payrpf.getPayrseqno());
		if (isNE(wsaaTaxRelAmt, 0)) {
			linsrnlIO.setTaxrelmth(t5688rec.taxrelmth);
		}
		linsrnlIO.setBillcd(datcon1rec.intDate);
		linsrnlIO.setAcctmeth(chdrpf.getAcctmeth());
		linsrnlIO.setProraterec("Y");
	}

	/**
	 * <pre>
	 * Collection
	 * </pre>
	 */
	protected void collection2300() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					collection2301();
					processLinsRecord2302();
					ledgerAccountingUpdates2303();
				case skipInitializeAgcm2303:
					skipInitializeAgcm2303();
				case callCovr2304:
					callCovr2304();
				case skipBonusWorkbench2304:
					skipBonusWorkbench2304();
				case continue2305:
					continue2305();
				case callAgcmbchio2307:
					callAgcmbchio2307();
					basicComm2308();
				case servComm2309:
					servComm2309();
				case renlComm2310:
					renlComm2310();
				case endComm2311:
					endComm2311();
				case exit2312:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void collection2301() {
		/* Read T5645 for suspense balance. */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsaaCompany);
		wsaaItemkey.itemItemtabl.set(t5645);
		wsaaItemkey.itemItemitem.set(wsaaProg);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T3695 to check the sign. */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsaaCompany);
		wsaaItemkey.itemItemtabl.set(t3695);
		wsaaItemkey.itemItemitem.set(t5645rec.sacstype01);
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/* Get transaction description. */
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), t1688, atrtTransCode.toString(),
				wsaaCompany.toString(), atmodrec.language.toString());

		if (descpf == null) {
			descpf = new Descpf();
			descpf.setLongdesc(" ");
		}
		wsaaTransDesc.set(descpf.getLongdesc());
	}

	protected void processLinsRecord2302() {
		/* Now we know we have enough money in suspense to cover the */
		/* installment. LINS need to be updated. */
		/* Update LINSRNL with PAYFLAG set to 'P' to denote the */
		/* transaction has been paid. */
		linsrnlIO.setPayflag("P");
		linsrnlIO.setFunction(Varcom.writr);
		linsrnlIO.setFormat(formatsInner.linsrnlrec);
		SmartFileCode.execute(appVars, linsrnlIO);
		if ((isNE(linsrnlIO.getStatuz(), Varcom.oK))) {
			syserrrec.params.set(linsrnlIO.getParams());
			xxxxFatalError();
		}
		updateLinsForOtherFee();
		/* Update certain fields in CHDRLIF to denote the installment has */
		/* been paid. Most importantly is the outstanding amount to be paid */
		/* this field should less the payment received. */
		/* THE ACTUAL RECORD MUST BE UPDATED AT THIS POINT, BECAUSE, */
		/* THE COMMISSION ROUTINES CALLED RE-READ THE CONTRACT HEADER */
		/* TO GET THE BILLING DATE ! */
		chdrpf.setPtdate(linsrnlIO.getInstto().toInt());
		chdrpf.setInstjctl(atrtBatchKey.toString());
		chdrpf.setInstfrom(linsrnlIO.getInstfrom().toInt());
		chdrpf.setInstto(linsrnlIO.getInstto().toInt());
		/* ADD 1 TO CHDRLIF-TRANNO. */
		chdrpf.setTranno(wsaaTranno.toInt());
		/* this is the wrong place to update CHDR */
		/* MOVE LINSRNL-INSTAMT01 TO CHDRLIF-SINSTAMT01. <025> */
		/* MOVE LINSRNL-INSTAMT02 TO CHDRLIF-SINSTAMT02. <025> */
		/* MOVE LINSRNL-INSTAMT03 TO CHDRLIF-SINSTAMT03. <025> */
		/* MOVE LINSRNL-INSTAMT04 TO CHDRLIF-SINSTAMT04. <025> */
		/* MOVE LINSRNL-INSTAMT05 TO CHDRLIF-SINSTAMT05. <025> */
		/* MOVE LINSRNL-INSTAMT06 TO CHDRLIF-SINSTAMT06. <025> */
		setPrecision(chdrpf.getInsttot01(), 2);
		chdrpf.setInsttot01(chdrpf.getInsttot01().add(new BigDecimal(linsrnlIO.getInstamt01().toString())));
		setPrecision(chdrpf.getInsttot02(), 2);
		chdrpf.setInsttot02(chdrpf.getInsttot02().add(new BigDecimal(linsrnlIO.getInstamt02().toString())));
		setPrecision(chdrpf.getInsttot03(), 2);
		chdrpf.setInsttot03(chdrpf.getInsttot03().add(new BigDecimal(linsrnlIO.getInstamt03().toString())));
		setPrecision(chdrpf.getInsttot04(), 2);
		chdrpf.setInsttot04(chdrpf.getInsttot04().add(new BigDecimal(linsrnlIO.getInstamt04().toString())));
		setPrecision(chdrpf.getInsttot05(), 2);
		chdrpf.setInsttot05(chdrpf.getInsttot05().add(new BigDecimal(linsrnlIO.getInstamt05().toString())));
		setPrecision(chdrpf.getInsttot06(), 2);
		chdrpf.setInsttot06(chdrpf.getInsttot06().add(new BigDecimal(linsrnlIO.getInstamt06().toString())));
		/* COMPUTE CHDRLIF-OUTSTAMT = CHDRLIF-OUTSTAMT - */
		/* LINSRNL-INSTAMT06. */
		chdrpf.setOutstamt(BigDecimal.ZERO);
		/* Rewrite the Contract Header Record CHDRLIF. */

		/*
		/* Update the PTDATE on the PAYR record. */
		/* firstly, store the old PTDATE for us to use when doing */
		/* commission suppression testing. */
		wsaaPtdate.set(payrpf.getPtdate());
		payrpf.setPtdate(linsrnlIO.getInstto().toInt());
		payrpf.setOutstamt(BigDecimal.ZERO);
	}

	protected void ledgerAccountingUpdates2303() {
		/* Once the transaction has been paid out from the suspense, the */
		/* suspense account need to be updated(less the amount paid). */
		updateSuspenseAccount2320();
		/* Update individual ledger accounts(INSTAMT01 to INSTAMT05). */
		updateIndividualLedger2330();
		/* Generic component processing. */
		/* INSTAMT06 is the actual life premium which to be invested. */
		/* MOVE LINSRNL-INSTAMT06 TO WSAA-TOT-AMT. 008 */
		/* Initialize coverage premium table. */
		wsaaC.set(1);
		wsaaL.set(1);
		while (!(isGT(wsaaL, 10))) {
			initialiseCovrPrem8000();
		}

		/* Find the total premium for each coverage and load them into a */
		/* table so that they can be referenced before calling UNITALOC. */
		/* This amount is required for enhanced allocation. */
		covrpf = new Covrpf();
		findCovrTotPrem8100();
		readTh6052345();
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.skipInitializeAgcm2303);
		}
		/* Initialize the AGCM arrays */
		b100InitializeArrays();
		wsaaBillfq.set(payrpf.getBillfreq());
		wsaaAgcmIx.set(0);
		covrpf = new Covrpf();
	}

	protected void skipInitializeAgcm2303() {
		/* Read in all the relevant COVR records which made up the */
		/* installment. Although we are not updating COVR, we need the */
		/* precise key(life, joint life, coverage, rider, plan suffix) from */
		/* it to call the generic subroutine to process unit linked */
		/* investment. */
		covrpf = new Covrpf();
		List<Covrpf> covrpflist = covrpfDAO.getCovrByComAndNumAllFlagRecord(linsrnlIO.getChdrcoy().toString(),
				linsrnlIO.getChdrnum().toString());
		if (covrpflist == null || covrpflist.isEmpty()) {
			goTo(GotoLabel.exit2312);
		} else {
			iterator = covrpflist.iterator();
		}
	}

	/**
	 * <pre>
	*    MOVE 0                      TO WSAA-JRNSEQ.          <V4L001>
	 * </pre>
	 */
	protected void callCovr2304() {
		if (iterator == null || !iterator.hasNext()) {
			goTo(GotoLabel.exit2312);
		}
		covrpf = iterator.next();

		/* Check valid flag 2 on COVR */
		/* IF COVR-VALIDFLAG = '2' */
		/* MOVE NEXTR TO COVR-FUNCTION */
		/* GO TO 2304-CALL-COVR. */
		wsaaValidStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12) || isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)) {
			if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
				wsaaValidStatus.set("Y");
			}
		}
		if (isNE(wsaaValidStatus, "Y")) {
			callCovr2304();
			return;
		}
		wsaaValidStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12) || isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)) {
			if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrpf.getPstatcode())) {
				wsaaValidStatus.set("Y");
			}
		}
		if (isNE(wsaaValidStatus, "Y")) {
			callCovr2304();
			return;
		}
		/* Read table T5687 to see if this is a single premium */
		/* Component. If it is then no need to generate Suspense */
		/* Postings. */
		readT568711100(covrpf);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			goTo(GotoLabel.exit2312);
		}
		/* IF (LINSRNL-INSTFROM > COVR-CURRTO) OR <044> */
		if ((isGTE(linsrnlIO.getInstfrom(), covrpf.getCurrto()))
				|| (isLT(linsrnlIO.getInstfrom(), covrpf.getCurrfrom()))) {
			callCovr2304();
			return;
		}
		/* Only process coverage for the selected payer. */
		/* IF COVR-PAYRSEQNO NOT = PAYR-PAYRSEQNO */
		/* MOVE NEXTR TO COVR-FUNCTION */
		/* GO TO 2304-CALL-COVR. */
		/* IF (LINSRNL-INSTFROM > COVR-CURRTO) OR */
		/* (LINSRNL-INSTFROM < COVR-CURRFROM) */
		/* MOVE NEXTR TO COVR-FUNCTION */
		/* GO TO 2304-CALL-COVR. */
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.skipBonusWorkbench2304);
		}
		
		callPmexSubroutine(covrpf);
		
		if (iterator.hasNext()) {
			isFirstCovr = false;
		} else {
			isFirstCovr = true;
		}
		/* Bonus Workbench * */
		if (isNE(covrpf.getInstprem(), ZERO)) {
			wsaaAgcmIx.add(1);
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrpf.getChdrcoy());
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrpf.getChdrnum());
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrpf.getLife());
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrpf.getCoverage());
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrpf.getRider());
			b200PremiumHistory();
			b300WriteArrays();
		} else {
			wsaaAgcmIx.add(1);
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrpf.getChdrcoy());
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrpf.getChdrnum());
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrpf.getLife());
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrpf.getCoverage());
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrpf.getRider());
			b200PremiumHistory();
		}
	}

	protected void callPmexSubroutine(Covrpf covrpf) {
		premiumrec.premiumRec.set(SPACES);
		initialize(premiumrec.premiumRec);
		premiumrec.updateRequired.set("N");
		premiumrec.proratPremCalcFlag.set("N");
		if (isFirstCovr) {
			premiumrec.firstCovr.set("Y");
		} else {
			premiumrec.firstCovr.set("N");
			premiumrec.lifeLife.set(covrpf.getLife());
			premiumrec.covrCoverage.set(covrpf.getCoverage());
			premiumrec.covrRider.set(covrpf.getRider());
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			if (isNE(premiumrec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(premiumrec.statuz);
				xxxxFatalError();
			}
			return;
		}
		premiumrec.batcBatctrcde.set(atrtTransCode);
		premiumrec.riskPrem.set(ZERO);
		premiumrec.language.set(atmodrec.language);
		if (isProcesFreqChange) {
			premiumrec.billfreq.set(wsaaNewBillfreq);
		} else {
			premiumrec.bilfrmdt.set(wsaaBtdate);
			premiumrec.biltodt.set(wsaaAdvptdat);
			premiumrec.billfreq.set(payrpf.getBillfreq());
		}
		readT5675();
		if (AppVars.getInstance().getAppConfig().isVpmsEnable()) {
			premiumrec.premMethod.set(t5687rec.premmeth);
		}
		if (premiumrec.premMethod.toString().trim().equals("PMEX")) {
			premiumrec.setPmexCall.set("Y");
			premiumrec.cownnum.set(chdrpf.getCownnum());
			premiumrec.occdate.set(chdrpf.getOccdate());
		}
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(chdrpf.getChdrnum());
		premiumrec.cnttype.set(chdrpf.getCnttype());
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(t5687rec.rtrnwfreq, ZERO)) {
			traceLastRerateDate5500(covrpf);
			premiumrec.effectdt.set(wsaaLastRerateDate);
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
		} else {
			premiumrec.ratingdate.set(covrpf.getCrrcd());
			premiumrec.reRateDate.set(covrpf.getCrrcd());
		}
		if (rerateChgFlag) { // to get the re-rate premiums
			premiumrec.effectdt.set(wsaaRerateEffDate);
			premiumrec.ratingdate.set(wsaaRerateEffDate);
			premiumrec.reRateDate.set(wsaaRerateEffDate);
		}
		premiumrec.mop.set(payrpf.getBillchnl());
		premiumrec.commissionPrem.set(ZERO);
		premiumrec.zstpduty01.set(ZERO);
		premiumrec.zstpduty02.set(ZERO);
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.lifeLife.set(covrpf.getLife());
		if (("PMEX".equals(t5675rec.premsubr.toString().trim())) && (lnkgFlag)) {
			if (null == covrpf.getLnkgsubrefno() || covrpf.getLnkgsubrefno().trim().isEmpty()) {
				premiumrec.lnkgSubRefNo.set(SPACE);
			} else {
				premiumrec.lnkgSubRefNo.set(covrpf.getLnkgsubrefno().trim());
			}
			if (null == covrpf.getLnkgno() || covrpf.getLnkgno().trim().isEmpty()) {
				premiumrec.linkcov.set(SPACE);
			} else {
				LinkageInfoService linkgService = new LinkageInfoService();
				FixedLengthStringData linkgCov = new FixedLengthStringData(
						linkgService.getLinkageInfo(covrpf.getLnkgno().trim()));
				premiumrec.linkcov.set(linkgCov);
			}
		}
		premiumrec.lifeJlife.set(covrpf.getJlife());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.covrRider.set(covrpf.getRider());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		Lifepf lifepf = lifepfDAO.getLifeEnqRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "00");
		premiumrec.lsex.set(lifepf.getCltsex());
		calculateAnb3260(lifepf);
		premiumrec.lage.set(wsaaAnb);
		Clntpf clntpf = clntpfDAO.searchClntRecord("CN", wsaaFsuCoy.toString(), lifepf.getLifcnum());
		if (stampDutyflag && clntpf.getClntStateCd() != null) {
			premiumrec.rstate01.set(clntpf.getClntStateCd().substring(3).trim());
		}
		Lifepf jlife = lifepfDAO.getLifelnbRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");
		if (jlife != null) {
			premiumrec.jlsex.set(jlife.getCltsex());
			calculateAnb3260(jlife);
			premiumrec.jlage.set(wsaaAnb);
		} else {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			xxxxFatalError();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(covrpf.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			xxxxFatalError();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.riskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(covrpf.getPremCurrency());
		if (isEQ(covrpf.getPlanSuffix(), ZERO) && isNE(chdrpf.getPolinc(), 1)) {
			compute(premiumrec.sumin, 3).setRounded(div(covrpf.getSumins(), chdrpf.getPolsum()));
		} else {
			premiumrec.sumin.set(covrpf.getSumins());
		}
		compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin, chdrpf.getPolinc()));
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.calcPrem.set(covrpf.getInstprem());
		premiumrec.calcBasPrem.set(covrpf.getZbinstprem());
		premiumrec.calcLoaPrem.set(covrpf.getZlinstprem());
		getAnny3280(covrpf);
		premiumrec.commTaxInd.set("Y");
		premiumrec.prevSumIns.set(ZERO);
		premiumrec.inputPrevPrem.set(ZERO);
		com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao = DAOFactory.getClntpfDAO();
		com.csc.smart400framework.dataaccess.model.Clntpf clnt = clntDao.getClientByClntnum(lifepf.getLifcnum());
		if (null != clnt && null != clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()) {
			premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
		} else {
			premiumrec.stateAtIncep.set(clntpf.getClntStateCd());
		}
		premiumrec.validind.set("2");
		getRcvdpf(covrpf);
		callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		if (isNE(premiumrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			xxxxFatalError();
		}
	}
	
	protected void processFreqChange3000() {
		PackedDecimalData policylevelsinst01 = new PackedDecimalData(17, 2).init(ZERO); 
		List<Rertpf> updateRertList = new ArrayList<>();
		List<Rertpf> insertRertList = new ArrayList<>();
		List<Rertpf> insertRertList1 = new ArrayList<>();
		isProcesFreqChange = true;
		isFirstCovr = true;
		if (!updateCovrList.isEmpty()) {
			updateCovrList.clear();
		}
		updateAgcmList = new ArrayList<>();
		List<Covrpf> covrpflist = covrpfDAO.getCovrmjaByComAndNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		
		if (!covrpflist.isEmpty()) {
			for (int i = 0; i < covrpflist.size(); i++) {
				Covrpf c = covrpflist.get(i);
				covrRiskStatus5110(c);
				if (isNE(wsaaValidStatus, "Y")) {
					continue;
				}
				/*covrPremStatus5120(c);
				if (isNE(wsaaValidStatus, "Y")) {
					continue;
					//For now it is not required in future if we need it we can do premium status check
				}*/
				callPmexSubroutine(c);
				if (stampDutyflag) {
					compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
					c.setZstpduty01(premiumrec.zstpduty01.getbigdata());
				}
				c.setInstprem(premiumrec.calcPrem.getbigdata());
				policylevelsinst01.add(premiumrec.calcPrem);
				c.setZbinstprem(premiumrec.calcBasPrem.getbigdata());
				c.setZlinstprem(premiumrec.calcBasPrem.getbigdata());
				c.setCommprem(premiumrec.commissionPrem.getbigdata());
				c.setCurrfrom(wsaaCurrptdt.toInt());
				updateCovrList.add(c);
				agcmList = agcmpfDAO.searchAgcmstRecord(c.getChdrcoy(), c.getChdrnum(), c.getLife(), c.getCoverage(), c.getRider(), c.getPlanSuffix(), "1");
				if(!agcmList.isEmpty()) {
					for (Agcmpf agcm : agcmList) {
						if(isNE(agcm.getDormflag() , 'Y')) {
							agcm.setAnnprem(mult(c.getInstprem(), wsaaNewBillfreq).getbigdata());
							updateAgcmList.add(agcm);
						}
					}
				}
				if (!rertList.isEmpty()) {
					for (Rertpf r : rertList) {
						if (r.getLife().equals(c.getLife()) && r.getCoverage().equals(c.getCoverage()) && r.getRider().equals(c.getRider())) {
							Rertpf rert = new Rertpf(r);
							rert.setValidflag("2");
							updateRertList.add(rert);
							if (stampDutyflag) {
								r.setLastzstpduty(premiumrec.zstpduty01.getbigdata());
							}
							r.setLastInst(premiumrec.calcPrem.getbigdata());
							r.setZblastinst(premiumrec.calcBasPrem.getbigdata());
							r.setZllastinst(premiumrec.calcBasPrem.getbigdata());
							insertRertList.add(r);
						}
					}
				}
				if (i + 1 < covrpflist.size()) {
					isFirstCovr = false;
				} else {
					isFirstCovr = true;
				}
			}
			isFirstCovr = true;
			if (!insertRertList.isEmpty()) {
				for (int i = 0; i < covrpflist.size(); i++) {
				Covrpf c = covrpflist.get(i);
					for (Rertpf r : insertRertList) {
						if (r.getLife().equals(c.getLife()) && r.getCoverage().equals(c.getCoverage())
								&& r.getRider().equals(c.getRider())) {
							rerateChgFlag = true;
							wsaaRerateEffDate.set(r.getEffdate());
							callPmexSubroutine(c);
							r.setTranno(wsaaTranno.toInt());
							if (stampDutyflag) {
								r.setNewzstpduty(premiumrec.zstpduty01.getbigdata());
							}
							r.setNewinst(premiumrec.calcPrem.getbigdata());
							r.setZbnewinst(premiumrec.calcBasPrem.getbigdata());
							r.setZlnewinst(premiumrec.calcLoaPrem.getbigdata());
							insertRertList1.add(r);
							rerateChgFlag = false;
						}
					}
					if (i + 1 < covrpflist.size()) {
						isFirstCovr = false;
					} else {
						isFirstCovr = true;
					}
				}
			}
		}
		
		chdrpf.setTranno(wsaaTranno.toInt());
		chdrpf.setCurrfrom(wsaaCurrptdt.toInt());
		chdrpf.setBillfreq(wsaaNewBillfreq.toString());
		// if applicable we need to add contract fee, tolerance and others
		chdrpf.setOutstamt(BigDecimal.ZERO);
		chdrpf.setSinstfrom(wsaaCurrptdt.toInt());
		chdrpf.setSinstto(varcom.vrcmMaxDate.toInt());
		chdrpf.setSinstamt01(policylevelsinst01.getbigdata());
		calculateContractFee();
		chdrpf.setSinstamt06(chdrpf.getSinstamt01().add(chdrpf.getSinstamt02()).add(chdrpf.getSinstamt03()).add(chdrpf.getSinstamt04()).add(chdrpf.getSinstamt05()));
	
		// if applicable we need to set tolerance and others
		payrpf.setEffdate(wsaaAdvptdat.toInt());
		payrpf.setTranno(wsaaTranno.toInt());
		//Setting values changed in billing change previous screen
		payrpf.setBillfreq(wsaaNewBillfreq.toString());
		payrpf.setValidflag("1");
		payrpf.setSinstamt01(policylevelsinst01.getbigdata());
		payrpf.setSinstamt06(chdrpf.getSinstamt06());
		List<Payrpf> payrpfInsList = new ArrayList<>();
		payrpfInsList.add(payrpf);

		if (!updateRertList.isEmpty()) {
			rertpfDAO.updateRertList(updateRertList);
		}
		if (!insertRertList1.isEmpty()) {
			rertpfDAO.insertRertList(insertRertList1);
		}
		chdrpfDAO.insertChdrBilling(chdrpf);
		payrpfDAO.insertPayrpfList(payrpfInsList);
		covrpfDAO.updateRerateData(updateCovrList);
		agcmpfDAO.updateAgcmRecord(updateAgcmList);

		isProcesFreqChange = false;
	}
	
	protected void traceLastRerateDate5500(Covrpf covr) {
		if (billChgFlag) {
			start5510ForBilltmpChg(covr);
		} else {
			start5510(covr);
		}
	}

	protected void start5510(Covrpf covr) {
		wsaaLastRerateDate.set(covr.getRerateDate());
		datcon4rec.billday.set(payrpf.getDuedd());
		datcon4rec.billmonth.set(payrpf.getDuemm());
		compute(datcon4rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));
		datcon4rec.frequency.set(t5687rec.rtrnwfreq);
		while (!(isLTE(wsaaLastRerateDate, wsaaPtdate))) {
			datcon4rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, Varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				xxxxFatalError();
			}
			wsaaLastRerateDate.set(datcon4rec.intDate2);
		}

		if (isLT(wsaaLastRerateDate, covr.getCrrcd())) {
			wsaaLastRerateDate.set(covr.getCrrcd());
		}
	}

	protected void start5510ForBilltmpChg(Covrpf covr) {
		wsaaLastRerateDate.set(covr.getRerateDate());

		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.frequency.set(t5687rec.rtrnwfreq);
		compute(datcon2rec.freqFactor, 0).set(sub(ZERO, t5687rec.rtrnwfreq));

		while (!(isLTE(wsaaLastRerateDate, wsaaPtdate))) {
			datcon2rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, Varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				xxxxFatalError();
			}
			wsaaLastRerateDate.set(datcon2rec.intDate2);
		}

		if (isLT(wsaaLastRerateDate, covr.getCrrcd())) {
			wsaaLastRerateDate.set(covr.getCrrcd());
		}
	}

	protected void readT5675() {
		List<Itempf> t5675ItempfList = itemDao.getAllItemitem(IT, atmodrec.company.toString(), t5675,
				t5687rec.premmeth.toString());
		if (t5675ItempfList.isEmpty()) {
			t5675rec.t5675Rec.set(SPACES);
			syserrrec.params.set(IT.concat(atmodrec.company.toString()).concat(t5675)
					.concat(t5687rec.premmeth.toString()));
			xxxxFatalError();
		} else {
			t5675rec.t5675Rec.set(StringUtil.rawToString(t5675ItempfList.get(0).getGenarea()));
		}
	}
	protected void calculateContractFee() {
		if (isNE(t5674rec.commsubr, SPACES)) {
			mgfeelrec.mgfeelRec.set(SPACES);
			mgfeelrec.effdate.set(ZERO);
			mgfeelrec.mgfee.set(ZERO);
			mgfeelrec.cnttype.set(chdrpf.getCnttype());
			/* MOVE CHDRLNB-BILLFREQ       TO MGFL-BILLFREQ.                */
			mgfeelrec.billfreq.set(payrpf.getBillfreq());
			mgfeelrec.bilfrmdt.set(chdrpf.getPtdate());
			mgfeelrec.biltodt.set(wsaaAdvptdat);
			mgfeelrec.policyRCD.set(chdrpf.getOccdate());
			mgfeelrec.cntcurr.set(chdrpf.getCntcurr());
			mgfeelrec.company.set(atmodrec.company.toString());
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
			if (isNE(mgfeelrec.statuz, Varcom.oK)
					&& isNE(mgfeelrec.statuz, Varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				xxxxFatalError();
			}
			zrdecplrec.amountIn.set(mgfeelrec.mgfee);
			zrdecplrec.currency.set(chdrpf.getCntcurr());
			c000CallRounding();
			mgfeelrec.mgfee.set(zrdecplrec.amountOut);
		} else {
			mgfeelrec.mgfee.set(0);
		}
		chdrpf.setSinstamt02(mgfeelrec.mgfee.getbigdata());
		payrpf.setSinstamt02(mgfeelrec.mgfee.getbigdata());

	}
	protected void calculateAnb3260(Lifepf lifepf) {
		wsaaAnb.set(ZERO);
		AgecalcPojo agecalcPojo = new AgecalcPojo();
		agecalcPojo.setFunction("CALCP");
		agecalcPojo.setLanguage(atmodrec.language.toString());
		agecalcPojo.setCnttype(chdrpf.getCnttype());
		agecalcPojo.setIntDate1(Integer.toString(lifepf.getCltdob()));
		agecalcPojo.setIntDate2(premiumrec.effectdt.toString());
		agecalcPojo.setCompany(wsaaFsuCoy.toString());
		agecalcUtils.calcAge(agecalcPojo);
		if (isNE(agecalcPojo.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(agecalcPojo.toString());
			syserrrec.statuz.set(agecalcPojo.getStatuz());
			xxxxFatalError();
		}
		wsaaAnb.set(agecalcPojo.getAgerating());
	}

	protected void getAnny3280(Covrpf covrpf) {
		annypf = annypfDAO.getAnnyRecordByAnnyKey(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(),
				covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlanSuffix());
		if (annypf != null) {
			premiumrec.freqann.set(annypf.getFreqann());
			premiumrec.advance.set(annypf.getAdvance());
			premiumrec.arrears.set(annypf.getArrears());
			premiumrec.guarperd.set(annypf.getGuarperd());
			premiumrec.intanny.set(annypf.getIntanny());
			premiumrec.capcont.set(annypf.getCapcont());
			premiumrec.withprop.set(annypf.getWithprop());
			premiumrec.withoprop.set(annypf.getWithoprop());
			premiumrec.ppind.set(annypf.getPpind());
			premiumrec.nomlife.set(annypf.getNomlife());
			premiumrec.dthpercn.set(annypf.getDthpercn());
			premiumrec.dthperco.set(annypf.getDthperco());
		} else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

	protected void getRcvdpf(Covrpf covrpf) {
		boolean dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", appVars, IT);
		boolean incomeProtectionflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP02", appVars, IT);
		boolean premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", appVars, IT);
		if (incomeProtectionflag || premiumflag || dialdownFlag) {
			Rcvdpf rcvdPFObject = new Rcvdpf();
			rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
			rcvdPFObject.setChdrnum(covrpf.getChdrnum());
			rcvdPFObject.setLife(covrpf.getLife());
			rcvdPFObject.setCoverage(covrpf.getCoverage());
			rcvdPFObject.setRider(covrpf.getRider());
			rcvdPFObject.setCrtable(covrpf.getCrtable());
			rcvdPFObject = rcvdDAO.readRcvdpf(rcvdPFObject);
			premiumrec.bentrm.set(rcvdPFObject.getBentrm());
			if (rcvdPFObject.getPrmbasis() != null && isEQ("S", rcvdPFObject.getPrmbasis())) {
				premiumrec.prmbasis.set("Y");
			} else {
				premiumrec.prmbasis.set("");
			}
			premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
			premiumrec.occpcode.set("");
			premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
			premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
		}
	}

	protected void skipBonusWorkbench2304() {
		/* COMPUTE WSAA-TOT-AMT = WSAA-TOT-AMT - COVR-INSTPREM. 008 */
		if (isNE(t5688rec.comlvlacc, "Y") || isEQ(premiumrec.calcPrem, ZERO)) {
			goTo(GotoLabel.continue2305);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account */
		/* movements. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[6]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[6]);
		lifacmvrec.glsign.set(wsaaT5645Sign[6]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[6]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[6]);
		wsaaRldgChdrnum.set(covrpf.getChdrnum());
		wsaaRldgLife.set(covrpf.getLife());
		wsaaRldgCoverage.set(covrpf.getCoverage());
		wsaaRldgRider.set(covrpf.getRider());
		wsaaPlan.set(covrpf.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		/* MOVE LINSRNL-INSTAMT01 TO LIFA-ORIGAMT. */
		lifacmvrec.origamt.set(premiumrec.calcPrem);
		if (stampDutyflag) {
			compute(lifacmvrec.origamt, 3).set(add(lifacmvrec.origamt, premiumrec.zstpduty01));
		}
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <021> */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		 if(stampDutyflag) {
			 postCompStampduty();
		 }
	}

	/**
	 * <pre>
	* Set up LIFACMV parameters and call 'LIFACMV' to post account
	* movements.
	*    IF T5688-COMLVLACC          NOT = 'Y'
	*       GO TO                    2305-CONTINUE.
	*    IF PRAS-TAXRELAMT           NOT > 0
	*       GO TO                    2305-CONTINUE.
	*    MOVE SPACE                  TO LIFA-LIFACMV-REC.
	*    MOVE WSAA-CHDRNUM           TO LIFA-RDOCNUM.
	*    MOVE 0                      TO LIFA-JRNSEQ.
	*    MOVE WSAA-COMPANY           TO LIFA-BATCCOY
	*                                   LIFA-RLDGCOY
	*                                   LIFA-GENLCOY.
	*    MOVE ATRT-ACCT-YEAR         TO LIFA-BATCACTYR.
	*    MOVE ATRT-TRANS-CODE        TO LIFA-BATCTRCDE.
	*    MOVE ATRT-ACCT-MONTH        TO LIFA-BATCACTMN.
	*    MOVE ATRT-BATCH             TO LIFA-BATCBATCH.
	*    MOVE ATRT-BRANCH            TO LIFA-BATCBRN.
	*    MOVE WSAA-T5645-SACSCODE(14) TO LIFA-SACSCODE.
	*    MOVE WSAA-T5645-SACSTYPE(14) TO LIFA-SACSTYP.
	*    MOVE WSAA-T5645-SIGN(14)     TO LIFA-GLSIGN.
	*    MOVE WSAA-T5645-GLMAP(14)    TO LIFA-GLCODE.
	*    MOVE WSAA-T5645-CNTTOT(14)   TO LIFA-CONTOT.
	*    MOVE CHDRLIF-CNTCURR        TO LIFA-ORIGCURR.
	*    MOVE PRAS-TAXRELAMT         TO LIFA-ORIGAMT.
	*    MOVE 0                      TO LIFA-RCAMT
	*                                   LIFA-CRATE
	*                                   LIFA-ACCTAMT.
	*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANNO.
	*    MOVE WSAA-TRANNO            TO LIFA-TRANNO.
	*    MOVE VRCM-MAX-DATE          TO LIFA-FRCDATE.
	*    MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.
	*    MOVE LINSRNL-CHDRNUM        TO LIFA-TRANREF.
	*    MOVE WSAA-USER              TO LIFA-USER.
	*    MOVE CHDRLIF-CNTTYPE        TO LIFA-SUBSTITUTE-CODE(1).
	*    MOVE COVR-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).
	*    MOVE WSAA-TRANS-DESC        TO LIFA-TRANDESC.
	*    MOVE PRAS-INREVNUM          TO WSAA-RLDG-CHDRNUM.
	*    MOVE COVR-LIFE              TO WSAA-RLDG-LIFE.
	*    MOVE COVR-COVERAGE          TO WSAA-RLDG-COVERAGE.
	*    MOVE COVR-RIDER             TO WSAA-RLDG-RIDER.
	*    MOVE COVR-PLAN-SUFFIX       TO WSAA-PLAN.
	*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.
	*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.
	*    MOVE WSAA-TRANSACTION-DATE  TO LIFA-TRANSACTION-DATE.
	*    MOVE WSAA-TRANSACTION-TIME  TO LIFA-TRANSACTION-TIME.
	*    MOVE 'PSTW'                 TO LIFA-FUNCTION.
	*    CALL 'LIFACMV' USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ NOT = O-K
	*        MOVE LIFA-LIFACMV-REC   TO SYSR-PARAMS
	*        PERFORM XXXX-FATAL-ERROR.
	 * </pre>
	 */
	
	protected void postCompStampduty() {
		wsaaRldgChdrnum.set(covrpf.getChdrnum());
        wsaaRldgLife.set(covrpf.getLife());
        wsaaRldgCoverage.set(covrpf.getCoverage());
        wsaaRldgRider.set(covrpf.getRider());
        wsaaPlan.set(covrpf.getPlanSuffix());
        wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.origamt.set(premiumrec.zstpduty01);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		Clntpf clntpf = clntpfDAO.searchClntRecord("CN", wsaaFsuCoy.toString(), chdrpf.getCownnum());
        if(stampDutyflag) {
	    	itemIO.clearRecKeyData();
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(wsaaFsuCoy.toString());
			itemIO.setItemtabl("TR2EN");
			itemIO.setItemitem(clntpf.getClntStateCd());
			itemIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK)) {
				lifacmvrec.substituteCode[4].set("01");
			}
			else {
				lifacmvrec.substituteCode[4].set(premiumrec.rstate01);
	        }        
        }
        
        lifacmvrec.jrnseq.add(1);
        lifacmvrec.sacscode.set(wsaaT5645Sacscode33);
        lifacmvrec.sacstyp.set(wsaaT5645Sacstype33);
        lifacmvrec.glsign.set(wsaaT5645Sign33);
        lifacmvrec.glcode.set(wsaaT5645Glmap33);
        lifacmvrec.contot.set(wsaaT5645Cnttot33);
		
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}	
		lifacmvrec.rldgacct.set(linsrnlIO.getChdrnum());
		lifacmvrec.jrnseq.add(1);
        lifacmvrec.sacscode.set(wsaaT5645Sacscode34);
        lifacmvrec.sacstyp.set(wsaaT5645Sacstype34);
        lifacmvrec.glsign.set(wsaaT5645Sign34);
        lifacmvrec.glcode.set(wsaaT5645Glmap34);
        lifacmvrec.contot.set(wsaaT5645Cnttot34);
        callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		
		
	}
	protected void continue2305() {
		/* Call the generic processing for each relevant COVR record read. */
		genericProcessing2340();
		/* COMMISSION-UPDATING */
		/* MOVE 0 TO WSAA-JRNSEQ. */
		agcmbchIO.setDataArea(SPACES);
		/* MOVE LINSRNL-CHDRNUM TO AGCMBCH-CHDRNUM. <LA2108> */
		agcmbchIO.setChdrnum(covrpf.getChdrnum());
		/* MOVE LINSRNL-CHDRCOY TO AGCMBCH-CHDRCOY. <LA2108> */
		agcmbchIO.setChdrcoy(covrpf.getChdrcoy());
		agcmbchIO.setLife(covrpf.getLife());
		agcmbchIO.setCoverage(covrpf.getCoverage());
		agcmbchIO.setRider(covrpf.getRider());
		agcmbchIO.setPlanSuffix(covrpf.getPlanSuffix());
		agcmbchIO.setFunction(Varcom.begnh);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
	}

	protected void callAgcmbchio2307() {
		SmartFileCode.execute(appVars, agcmbchIO);
		if ((isNE(agcmbchIO.getStatuz(), Varcom.oK)) && (isNE(agcmbchIO.getStatuz(), Varcom.endp))) {
			syserrrec.params.set(agcmbchIO.getParams());
			xxxxFatalError();
		}
		
		if (gstOnCommFlag) {
			Aglfpf aglfpf = aglfpfDAO.searchAglfRecord(agcmbchIO.getChdrcoy().toString(), agcmbchIO.getAgntnum().toString());
			if (null != aglfpf && null != aglfpf.getReportag() && isNE(aglfpf.getReportag(), SPACES)) {
				wsaaReporttoAgnt.set(aglfpf.getReportag());
			} else {
				wsaaReporttoAgnt.set(SPACES);
			}
		}
		
		/* IF AGCMBCH-CURRFROM = ZEROES <V4L001> */
		/* OR AGCMBCH-EFDATE = ZEROS <V4L001> */
		/* OR AGCMBCH-CURRTO = ZEROS <V4L001> */
		/* CONTINUE <V4L001> */
		/* END-IF. <V4L001> */
		if (isEQ(agcmbchIO.getStatuz(), Varcom.endp)) {
			goTo(GotoLabel.callCovr2304);
		}
		if (isNE(agcmbchIO.getChdrnum(), covrpf.getChdrnum()) || isNE(agcmbchIO.getChdrcoy(), covrpf.getChdrcoy())
				|| isNE(agcmbchIO.getLife(), covrpf.getLife()) || isNE(agcmbchIO.getCoverage(), covrpf.getCoverage())
				|| isNE(agcmbchIO.getRider(), covrpf.getRider())
				|| isNE(agcmbchIO.getPlanSuffix(), covrpf.getPlanSuffix())) {
			goTo(GotoLabel.callCovr2304);
		}
		/* GO TO 2312-EXIT. <044> */
		/* Do not process Single Premium AGCMs */
		/* (paid-to date = zeroes). */
		if (isEQ(agcmbchIO.getPtdate(), ZERO) || isEQ(agcmbchIO.getDormantFlag(), "Y")) {
			/* MOVE REWRT TO AGCMBCH-FUNCTION <LA4018> */
			/* MOVE AGCMBCHREC TO AGCMBCH-FORMAT <LA4018> */
			/* CALL 'AGCMBCHIO' USING AGCMBCH-PARAMS <LA4018> */
			/* IF AGCMBCH-STATUZ NOT = O-K <LA4018> */
			/* MOVE AGCMBCH-STATUZ TO SYSR-STATUZ <LA4018> */
			/* MOVE AGCMBCH-PARAMS TO SYSR-PARAMS <LA4018> */
			/* PERFORM XXXX-FATAL-ERROR <LA4018> */
			/* END-IF <LA4018> */
			agcmbchIO.setFunction(Varcom.nextr);
			callAgcmbchio2307();
			return;
		}
		/* IF (AGCMBCH-CHDRNUM NOT = LINSRNL-CHDRNUM) OR <LA2108> */
		/* (AGCMBCH-CHDRCOY NOT = LINSRNL-CHDRCOY) <LA2108> */
		/* MOVE REWRT TO AGCMBCH-FUNCTION <LA2108> */
		/* CALL 'AGCMBCHIO' USING AGCMBCH-PARAMS <LA2108> */
		/* IF AGCMBCH-STATUZ NOT = O-K <LA2108> */
		/* MOVE AGCMBCH-PARAMS TO SYSR-PARAMS <LA2108> */
		/* PERFORM XXXX-FATAL-ERROR */
		/* ELSE */
		/* GO TO 2312-EXIT. */
		/* PERFORM 2360-GET-CRTABLE. */
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyDue.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
		comlinkrec.clnkallRec.set(SPACES);
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		wsaaRnwcpyDue.set(0);
		wsaaCompErnInitCommGst.set(0);
		wsaaCompErnRnwlCommGst.set(0);
		if(gstOnCommFlag){
			comlinkrec.gstAmount.set(ZERO);
		}
		/* MOVE 0 TO CLNK-ERNDAMT. */
		if (isNE(chdrpf.getBillfreq(), NUMERIC)) {
			chdrpf.setBillfreq("01");
		}
		wsaaBillfreq.set(chdrpf.getBillfreq());
		comlinkrec.cnttype.set(chdrpf.getCnttype());
		comlinkrec.billfreq.set(chdrpf.getBillfreq());
		comlinkrec.instprem.set(premiumrec.calcPrem);
		if (stampDutyflag) {
			compute(comlinkrec.instprem, 3).set(add(comlinkrec.instprem, premiumrec.zstpduty01));
		}
		/* MOVE LINSRNL-CHDRNUM TO CLNK-CHDRNUM. */
		comlinkrec.chdrnum.set(agcmbchIO.getChdrnum());
		/* MOVE LINSRNL-CHDRCOY TO CLNK-CHDRCOY. */
		comlinkrec.chdrcoy.set(agcmbchIO.getChdrcoy());
		comlinkrec.life.set(agcmbchIO.getLife());
		comlinkrec.agent.set(agcmbchIO.getAgntnum());
		comlinkrec.coverage.set(agcmbchIO.getCoverage());
		comlinkrec.annprem.set(agcmbchIO.getAnnprem());
		comlinkrec.rider.set(agcmbchIO.getRider());
		comlinkrec.planSuffix.set(agcmbchIO.getPlanSuffix());
		compute(comlinkrec.targetPrem, 3).setRounded(mult(covrpf.getInstprem(), wsaaBillfreq));
		comlinkrec.commprem.set(premiumrec.commissionPrem);
		comlinkrec.seqno.set(agcmbchIO.getSeqno());
		comlinkrec.currto.set(agcmbchIO.getPtdate());
		comlinkrec.crtable.set(covrpf.getCrtable());
		/* MOVE CHDRLIF-OCCDATE TO CLNK-OCCDATE. */
		/* MOVE AGCMBCH-EFDATE TO CLNK-OCCDATE. */
		comlinkrec.effdate.set(agcmbchIO.getEfdate());
		comlinkrec.agentClass.set(agcmbchIO.getAgentClass());
		comlinkrec.icommtot.set(agcmbchIO.getInitcom());
		comlinkrec.icommpd.set(agcmbchIO.getCompay());
		comlinkrec.icommernd.set(agcmbchIO.getComern());
		comlinkrec.ptdate.set(0);
		comlinkrec.language.set(atmodrec.language);
		/* Read T5644 to get generic subroutine for basic commission. */
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set(smtpfxcpy.item);
		wsaaItemkey.itemItemcoy.set(wsaaCompany);
		wsaaItemkey.itemItemtabl.set(t5644);
		wsaaItemkey.itemItemitem.set(agcmbchIO.getBascpy());
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), Varcom.oK)) && (isNE(itemIO.getStatuz(), Varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		wsaaBascpySubprog.set(t5644rec.compysubr);
		/* Read T5644 to get generic subroutine for servicing commission. */
		wsaaItemkey.itemItemitem.set(agcmbchIO.getSrvcpy());
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), Varcom.oK)) && (isNE(itemIO.getStatuz(), Varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		wsaaSrvcpySubprog.set(t5644rec.compysubr);
		/* Read T5644 to get generic subroutine for renewal commission. */
		wsaaItemkey.itemItemitem.set(agcmbchIO.getRnwcpy());
		itemIO.setDataKey(wsaaItemkey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), Varcom.oK)) && (isNE(itemIO.getStatuz(), Varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		wsaaRnwcpySubprog.set(t5644rec.compysubr);
	}

	protected void basicComm2308() {
		if (isEQ(wsaaBascpySubprog, SPACES)) {
			goTo(GotoLabel.servComm2309);
		}
		comlinkrec.method.set(agcmbchIO.getBascpy());
		/* Read ZRAP to get ZRORCODE, this one is valid only for Override */
		/* Agent */
		/* PERFORM A100-READ-ZRAP */
		callProgram(wsaaBascpySubprog, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, Varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		c000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		c000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		if (isEQ(agcmbchIO.getOvrdcat(), "O")) {
			wsaaOvrdBascpyDue.add(comlinkrec.payamnt);
			wsaaOvrdBascpyErn.add(comlinkrec.erndamt);
			compute(wsaaOvrdBascpyPay, 2).set(add(wsaaOvrdBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
		} else {
			//wsaaBascpyDue.add(comlinkrec.payamnt);
			wsaaBascpyDue.set(comlinkrec.erndamt);
			wsaaBascpyErn.set(comlinkrec.erndamt);
			if(gstOnCommFlag && agcmbchIO.getInitCommGst()!=null) {
				compute(wsaaCompErnInitCommGst, 3).setRounded(add(wsaaCompErnInitCommGst, comlinkrec.gstAmount));    			   			
				agcmbchIO.setInitCommGst(add(agcmbchIO.getInitCommGst(), wsaaCompErnInitCommGst));
            	compute(wsaaBascpyPay, 2).setRounded(add(wsaaBascpyPay,(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt)))); 
			}else{
            	compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay, (sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));
            }
			//compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			/***** IF WSAA-COMPAY-KEPT = ZEROES */
			/***** MOVE AGCMBCH-AGNTNUM TO WSAA-AGENT-KEPT <LA2108> */
			/***** MOVE CLNK-PAYAMNT TO WSAA-COMPAY-KEPT */
			/***** END-IF */
		}
		setPrecision(agcmbchIO.getCompay(), 2);
		agcmbchIO.setCompay(add(agcmbchIO.getCompay(), comlinkrec.payamnt));
		setPrecision(agcmbchIO.getComern(), 2);
		agcmbchIO.setComern(add(agcmbchIO.getComern(), comlinkrec.erndamt));
		/* ADD CLNK-PAYAMNT TO WSAA-BASCPY-DUE */
		/* AGCMBCH-COMPAY. <LA2108> */
		/* ADD CLNK-ERNDAMT TO WSAA-BASCPY-ERN */
		/* AGCMBCH-COMERN. <LA2108> */
		/* COMPUTE WSAA-BASCPY-PAY = WSAA-BASCPY-PAY + */
		/* (CLNK-PAYAMNT - CLNK-ERNDAMT). */
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.servComm2309);
		}
		/* Bonus Workbench * */
		/* Initial commission */
		if (isEQ(agcmbchIO.getOvrdcat(), "B") && isNE(comlinkrec.payamnt, ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmbchIO.getChdrcoy());
			zctnIO.setAgntnum(agcmbchIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			b400LocatePremium();
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(div(wsaaAgcmPremium, wsaaBillfq9), true);
			zrdecplrec.amountIn.set(zctnIO.getPremium());
			c000CallRounding();
			zctnIO.setPremium(zrdecplrec.amountOut);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmbchIO.getAnnprem(), 100), wsaaAgcmPremium), true);
			zctnIO.setZprflg("I");
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(wsaaTranno);
			zctnIO.setTransCode(atrtTransCode);
			zctnIO.setEffdate(linsrnlIO.getInstfrom());
			zctnIO.setTrandate(datcon1rec.intDate);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(Varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(), Varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				xxxxFatalError();
			}
		}
	}

	protected void servComm2309() {
		if (isEQ(wsaaSrvcpySubprog, SPACES)) {
			goTo(GotoLabel.renlComm2310);
		}
		comlinkrec.method.set(agcmbchIO.getSrvcpy());
		callProgram(wsaaSrvcpySubprog, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, Varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		c000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		c000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		wsaaSrvcpyDue.add(comlinkrec.payamnt);
		setPrecision(agcmbchIO.getScmdue(), 2);
		agcmbchIO.setScmdue(add(agcmbchIO.getScmdue(), comlinkrec.payamnt));
		wsaaSrvcpyErn.add(comlinkrec.erndamt);
		setPrecision(agcmbchIO.getScmearn(), 2);
		agcmbchIO.setScmearn(add(agcmbchIO.getScmearn(), comlinkrec.erndamt));
	}

	protected void renlComm2310() {
		/* IF WSAA-RNWCPY-SUBPROG = SPACE */
		/* GO TO 2311-END-COMM. */
		if (isEQ(wsaaRnwcpySubprog, SPACES) || (isEQ(chdrpf.getRnwlsupr(), "Y")
				&& isGTE(wsaaPtdate, chdrpf.getRnwlspfrom()) && isLTE(wsaaPtdate, chdrpf.getRnwlspto()))) {
			goTo(GotoLabel.endComm2311);
		}
		comlinkrec.method.set(agcmbchIO.getRnwcpy());
		callProgram(wsaaRnwcpySubprog, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, Varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		c000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		c000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);	
				
		if(gstOnCommFlag) {
			wsaaRnwcpyDue.set(comlinkrec.erndamt); 
		} else {
			wsaaRnwcpyDue.set(comlinkrec.payamnt);
		}
		 wsaaRnwcpyErn.set(comlinkrec.erndamt);
		 setPrecision(agcmbchIO.getRnlcdue(), 2);
		 agcmbchIO.setRnlcdue(add(agcmbchIO.getRnlcdue(), comlinkrec.payamnt));
		 setPrecision(agcmbchIO.getRnlcearn(), 2);
		 agcmbchIO.setRnlcearn(add(agcmbchIO.getRnlcearn(), comlinkrec.erndamt));
		 
		 if(gstOnCommFlag && agcmbchIO.getRnwlCommGst()!=null) {
         	compute(wsaaCompErnRnwlCommGst, 3).setRounded(add(wsaaCompErnRnwlCommGst, comlinkrec.gstAmount));    					
         	agcmbchIO.setRnwlCommGst(add(agcmbchIO.getRnwlCommGst(), wsaaCompErnRnwlCommGst));
         	compute(wsaaRnwcpyPay, 2).setRounded(add(wsaaRnwcpyPay,(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));
         }
		
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.endComm2311);
		}
		/* Bonus Workbench * */
		/* Initial commission */
		if (isEQ(agcmbchIO.getOvrdcat(), "B") && isNE(comlinkrec.payamnt, ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmbchIO.getChdrcoy());
			zctnIO.setAgntnum(agcmbchIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			b400LocatePremium();
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(div(wsaaAgcmPremium, wsaaBillfq9), true);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmbchIO.getAnnprem(), 100), wsaaAgcmPremium), true);
			zctnIO.setZprflg("R");
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(wsaaTranno);
			zctnIO.setTransCode(atrtTransCode);
			zctnIO.setEffdate(linsrnlIO.getInstfrom());
			zctnIO.setTrandate(datcon1rec.intDate);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(Varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(), Varcom.oK)) {
				syserrrec.statuz.set(zctnIO.getStatuz());
				syserrrec.params.set(zctnIO.getParams());
				xxxxFatalError();
			}
		}
	}

	protected void endComm2311() {
		datcon2rec.frequency.set(chdrpf.getBillfreq());
		datcon2rec.intDate1.set(agcmbchIO.getPtdate());
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		fpcoIO.setTargfrom(datcon2rec.intDate2);
		agcmbchIO.setPtdate(chdrpf.getPtdate());
		agcmbchIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(wsaaBascpyDue, 0)) || (isNE(wsaaBascpyPay, 0)) || (isNE(wsaaBascpyErn, 0)) || (isNE(wsaaSrvcpyDue, 0))
				|| (isNE(wsaaSrvcpyErn, 0)) || (isNE(wsaaRnwcpyDue, 0)) || (isNE(wsaaRnwcpyErn, 0))
				|| (isNE(wsaaOvrdBascpyPay, 0)) || (isNE(wsaaOvrdBascpyDue, 0)) || (isNE(wsaaOvrdBascpyErn, 0))) {
			readTh6052345();
			updateCommLedger2350();
		}
		agcmbchIO.setFunction(Varcom.nextr);
		readTableT57291150();
		if (flexiblePremiumContract.isTrue()) {

			fpcoIO.setFormat(formatsInner.fpcorec);
			fpcoIO.setFunction(Varcom.readh);
			readFpco7200();
			if (isGTE(fpcoIO.getBilledInPeriod(), fpcoIO.getTargetPremium())) {
				fpcoIO.setActiveInd("N");
			}
			fpcoIO.setAnnProcessInd("Y");
			fpcoIO.setFunction(Varcom.rewrt);
			SmartFileCode.execute(appVars, fpcoIO);
			if (isNE(fpcoIO.getStatuz(), Varcom.oK)) {
				syserrrec.params.set(fpcoIO.getParams());
				xxxxFatalError();
			}
			writeFpco7000();
		}
		goTo(GotoLabel.callAgcmbchio2307);
	}

	protected void readFpco7200() {
		fpcoIO.setChdrcoy(chdrpf.getChdrcoy());
		fpcoIO.setChdrnum(chdrpf.getChdrnum());
		fpcoIO.setLife(covrpf.getLife());
		fpcoIO.setJlife(covrpf.getJlife());
		fpcoIO.setCoverage(covrpf.getCoverage());
		fpcoIO.setRider(covrpf.getRider());
		fpcoIO.setPlanSuffix(covrpf.getPlanSuffix());
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			xxxxFatalError();
		}
	}

	protected void readTableT57291150() {

		notFlexiblePremiumContract.setTrue();
		/* Read T5729. <D9604> */
		itdmIO.setStatuz(Varcom.oK);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK) && isNE(itdmIO.getStatuz(), Varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrpf.getChdrcoy(), SPACES);
			stringVariable1.addExpression(t5729, SPACES);
			stringVariable1.addExpression(chdrpf.getCnttype(), SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(), Varcom.endp) || isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
				|| isNE(itdmIO.getItemtabl(), t5729) || isNE(itdmIO.getItemitem(), chdrpf.getCnttype())
				|| isGT(itdmIO.getItmfrm(), chdrpf.getOccdate()) || isLT(itdmIO.getItmto(), chdrpf.getOccdate())) {
			return;
		}
		t5729rec.t5729Rec.set(itdmIO.getGenarea());
		flexiblePremiumContract.setTrue();
	}

	protected void writeFpco7000() {

		fpcoIO.setChdrcoy(SPACES);
		fpcoIO.setChdrnum(SPACES);
		fpcoIO.setLife(SPACES);
		fpcoIO.setCoverage(SPACES);
		fpcoIO.setRider(SPACES);
		fpcoIO.setActiveInd(SPACES);
		fpcoIO.setPlanSuffix(ZERO);
		fpcoIO.setCurrfrom(ZERO);
		fpcoIO.setTargfrom(ZERO);
		fpcoIO.setValidflag("1");
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(wsaaCurrentPayuptoDate);
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		fpcoIO.setCurrfrom(datcon2rec.intDate2);
		fpcoIO.setTargfrom(datcon2rec.intDate2);
		fpcoIO.setEffdate(datcon2rec.intDate2);
		fpcoIO.setCurrto(varcom.vrcmMaxDate);
		fpcoIO.setTargto(wsaaCurrentPayuptoDate);
		fpcoIO.setAnnivProcDate(wsaaCurrentPayuptoDate);
		fpcoIO.setActiveInd("Y");
		setPrecision(fpcoIO.getTargetPremium(), 2);
		fpcoIO.setTargetPremium(mult(covrpf.getInstprem(), payrpf.getBillfreq()));
		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(chdrpf.getOccdate());
		datcon3rec.intDate2.set(fpcoIO.getTargto());
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			xxxxFatalError();
		}
		wsaaFpcoFreqFactor.set(datcon3rec.freqFactor);

		if (isEQ(wsaaFpcoFreqFactor, ZERO)) {
			fpcoIO.setPremRecPer(ZERO);
			fpcoIO.setBilledInPeriod(ZERO);
		} else {
			setPrecision(fpcoIO.getPremRecPer(), 2);
			fpcoIO.setPremRecPer(mult(covrpf.getInstprem(), wsaaFpcoFreqFactor));
			setPrecision(fpcoIO.getBilledInPeriod(), 2);
			fpcoIO.setBilledInPeriod(mult(covrpf.getInstprem(), wsaaFpcoFreqFactor));
		}
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 6)
				|| isEQ(t5729rec.frqcy[wsaaT5729Sub.toInt()], payrpf.getBillfreq())); wsaaT5729Sub.add(1)) {
			/* CONTINUE_STMT */
		}
		wsaaMinOverdue.set(ZERO);
		if (isEQ(wsaaT5729Sub, 1)) {
			wsaaMinOverdue.set(t5729rec.overdueMina01);
		} else if (isEQ(wsaaT5729Sub, 2)) {
			wsaaMinOverdue.set(t5729rec.overdueMinb01);
		} else if (isEQ(wsaaT5729Sub, 3)) {
			wsaaMinOverdue.set(t5729rec.overdueMinc01);
		} else if (isEQ(wsaaT5729Sub, 4)) {
			wsaaMinOverdue.set(t5729rec.overdueMind01);
		} else if (isEQ(wsaaT5729Sub, 5)) {
			wsaaMinOverdue.set(t5729rec.overdueMine01);
		} else if (isEQ(wsaaT5729Sub, 6)) {
			wsaaMinOverdue.set(t5729rec.overdueMinf01);
		} else {
			syserrrec.params.set(t5729);
			xxxxFatalError();
		}
		fpcoIO.setMinOverduePer(wsaaMinOverdue);
		setPrecision(fpcoIO.getOverdueMin(), 3);
		fpcoIO.setOverdueMin(mult(div(wsaaMinOverdue, 100), (mult(covrpf.getInstprem(), wsaaFpcoFreqFactor))), true);
		zrdecplrec.amountIn.set(fpcoIO.getOverdueMin());
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		c000CallRounding();
		fpcoIO.setOverdueMin(zrdecplrec.amountOut);
		fpcoIO.setAnnProcessInd(SPACES);
		fpcoIO.setTranno(chdrpf.getTranno());
		fpcoIO.setFormat(formatsInner.fpcorec);
		fpcoIO.setFunction(Varcom.writr);
		readFpco7200();
	}

	protected void updateSuspenseAccount2320() {
		try {
			updateSuspenseAccount2321();
			conversionControl2322();
		} catch (GOTOException e) {
			/* Expected exception for control flow purposes. */
		}
	}

	protected void updateSuspenseAccount2321() {
		/* Reduce amount in suspense account. This is to be done by */
		/* calling the subroutine LIFRTRN. */
		/* Set up LIFRTRN parameters and call 'LIFRTRN' to update the */
		/* ledger. */
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifrtrnrec.rdocnum.set(linsrnlIO.getChdrnum());
		lifrtrnrec.jrnseq.set(0);
		lifrtrnrec.batccoy.set(wsaaCompany);
		lifrtrnrec.rldgcoy.set(wsaaCompany);
		lifrtrnrec.genlcoy.set(wsaaCompany);
		lifrtrnrec.batcactyr.set(atrtAcctYear);
		lifrtrnrec.batctrcde.set(atrtTransCode);
		lifrtrnrec.batcactmn.set(atrtAcctMonth);
		lifrtrnrec.batcbatch.set(atrtBatch);
		lifrtrnrec.batcbrn.set(atrtBranch);
		lifrtrnrec.contot.set(t5645rec.cnttot01);
		lifrtrnrec.sacscode.set(t5645rec.sacscode01);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype01);
		lifrtrnrec.glsign.set(t5645rec.sign01);
		lifrtrnrec.glcode.set(t5645rec.glmap01);
		/* MOVE CHDRLIF-TRANNO TO LIFR-TRANNO. */
		lifrtrnrec.tranno.set(wsaaTranno);
		lifrtrnrec.origcurr.set(linsrnlIO.getBillcurr());
		/* MOVE LINSRNL-CBILLAMT TO LIFR-ORIGAMT. */
		/* MOVE WSAA-NET-CBILLAMT TO LIFR-ORIGAMT. <018> */
		compute(lifrtrnrec.origamt, 2).set(sub(wsaaNetCbillamt, linsrnlIO.getInstamt03()));
		lifrtrnrec.rcamt.set(0);
		lifrtrnrec.crate.set(0);
		lifrtrnrec.acctamt.set(0);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFR-EFFDATE. */
		lifrtrnrec.effdate.set(linsrnlIO.getInstfrom());
		lifrtrnrec.tranref.set(linsrnlIO.getChdrnum());
		lifrtrnrec.user.set(wsaaUser);
		lifrtrnrec.substituteCode[1].set(chdrpf.getCnttype());
		lifrtrnrec.trandesc.set(wsaaTransDesc);
		lifrtrnrec.rldgacct.set(chdrpf.getChdrnum());
		lifrtrnrec.transactionDate.set(wsaaTransactionDate);
		lifrtrnrec.transactionTime.set(wsaaTransactionTime);
		lifrtrnrec.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			xxxxFatalError();
		}
		if (isEQ(linsrnlIO.getCntcurr(), linsrnlIO.getBillcurr())) {
			goTo(GotoLabel.exit2329);
		}
	}

	protected void conversionControl2322() {
		/* Call LIFACMV to write 2 ACMV records for the two currencies. */
		/* Contract currency. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(linsrnlIO.getChdrnum());
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.tranno.set(chdrpf.getTranno());
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.origcurr.set(linsrnlIO.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt06());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. 021> */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(linsrnlIO.getChdrnum());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Billing currency. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <021> */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		wsaaRldgChdrnum.set(covrpf.getChdrnum()); //need to check
		wsaaRldgLife.set(covrpf.getLife());
		wsaaRldgCoverage.set(covrpf.getCoverage());
		wsaaRldgRider.set(covrpf.getRider());
		wsaaPlan.set(covrpf.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.contot.set(t5645rec.cnttot15);
		lifacmvrec.sacscode.set(t5645rec.sacscode15);
		lifacmvrec.sacstyp.set(t5645rec.sacstype15);
		lifacmvrec.glsign.set(t5645rec.sign15);
		lifacmvrec.glcode.set(t5645rec.glmap15);
		lifacmvrec.origcurr.set(linsrnlIO.getBillcurr());
		lifacmvrec.origamt.set(linsrnlIO.getCbillamt());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void updateIndividualLedger2330() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					premium2331();
				case fee2332:
					fee2332();
				case tolerance2333:
					tolerance2333();
				case stampDuty2334:
					stampDuty2334();
				case spare2335:
					spare2335();
				case taxRelief2336:
					taxRelief2336();
				case checkTax2337:
					checkTax2337();
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void premium2331() {
		/* Update ledger and accounting details for the installment */
		/* 'premium'. */
		/* If the installment premium has been updated i.e. not = zero */
		/* move to the line record. */
		/* IF WSAA-INST-PREM NOT = ZERO <025> */
		/* MOVE WSAA-INST-PREM TO LINSRNL-INSTAMT01. <025> */
		if (isLTE(linsrnlIO.getInstamt01(), 0)) {
			goTo(GotoLabel.fee2332);
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			goTo(GotoLabel.fee2332);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account */
		/* movements. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt01());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <021> */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void fee2332() {
		/* Update ledger and accounting details for the installment */
		/* 'Fee'. */
		if (isLTE(linsrnlIO.getInstamt02(), 0)) {
			goTo(GotoLabel.tolerance2333);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account */
		/* movements. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt02());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(linsrnlIO.getChdrnum());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(linsrnlIO.getChdrnum());
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void tolerance2333() {
		/* Update ledger and accounting details for the installment */
		/* 'Tolerance'. */
		if (isLTE(linsrnlIO.getInstamt03(), 0)) {
			goTo(GotoLabel.stampDuty2334);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account */
		/* movements. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		/* MOVE T5645-SACSCODE-05 TO LIFA-SACSCODE. */
		/* MOVE T5645-SACSTYPE-05 TO LIFA-SACSTYP. */
		/* MOVE T5645-SIGN-05 TO LIFA-GLSIGN. */
		/* MOVE T5645-GLMAP-05 TO LIFA-GLCODE. */
		/* MOVE T5645-CNTTOT-05 TO LIFA-CONTOT. */
		if (isEQ(wsaaToleranceFlag, "1")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		} else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode32);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype32);
			lifacmvrec.glsign.set(wsaaT5645Sign32);
			lifacmvrec.glcode.set(wsaaT5645Glmap32);
			lifacmvrec.contot.set(wsaaT5645Cnttot32);
			lifacmvrec.rldgacct.set(chdrpf.getAgntnum());
		}
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt03());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE WSAA-CHDRNUM TO LIFA-RLDGACCT. <V42013> */
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Override Commission */
		if (isEQ(th605rec.indic, "Y") && isNE(wsaaToleranceFlag, "1")) {
			zorlnkrec.effdate.set(lifacmvrec.effdate);
			zorlnkrec.annprem.set(lifacmvrec.origamt);
			b500CallZorcompy();
		}
	}

	protected void stampDuty2334() {
		/* Update ledger and accounting details for the installment */
		/* 'Stamp Duty'. */
		if (isLTE(linsrnlIO.getInstamt04(), 0)) {
			goTo(GotoLabel.spare2335);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account */
		/* movements. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.contot.set(t5645rec.cnttot06);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt04());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(linsrnlIO.getChdrnum());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void spare2335() {
		/* Update ledger and accounting details for the installment */
		/* 'Spare'. */
		if (isLTE(linsrnlIO.getInstamt05(), 0)) {
			/* GO TO 2339-EXIT. */
			goTo(GotoLabel.taxRelief2336);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account */
		/* movements. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(t5645rec.sacscode07);
		lifacmvrec.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec.glsign.set(t5645rec.sign07);
		lifacmvrec.glcode.set(t5645rec.glmap07);
		lifacmvrec.contot.set(t5645rec.cnttot07);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(linsrnlIO.getInstamt05());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(wsaaChdrnum);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void taxRelief2336() {
		/* Update ledger and accounting details for the installment */
		/* 'Tax Relief'. */
		/* IF T5688-COMLVLACC = 'Y' <021> */
		/* GO TO 2339-EXIT. <021> */
		if (isLTE(wsaaTaxRelAmt, 0)) {
			/* GO TO 2339-EXIT. <V74L01> */
			goTo(GotoLabel.checkTax2337);
		}
		/* Set up LIFACMV parameters and call 'LIFACMV' to post account */
		/* movements. */
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[5]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[5]);
		lifacmvrec.glsign.set(wsaaT5645Sign[5]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[5]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[5]);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.origamt.set(wsaaTaxRelAmt);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <021> */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(linsrnlIO.getChdrnum());
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE PRAS-INREVNUM TO LIFA-RLDGACCT. <A06541> */
		lifacmvrec.rldgacct.set(chdrpf.getChdrnum());
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void checkTax2337() {
		/* IF WSAA-TAX NOT = ZERO */
		b700PostTax();
		/* EXIT */
	}

	protected void genericProcessing2340() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readT56712341();
				case bypassDate2340:
					bypassDate2340();
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void readT56712341() {
		/* Read T5671 to for the generic subroutines. */
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(t5671);
		wsaaItemCrtable.set(covrpf.getCrtable());
		wsaaItemTranCode.set(atrtTransCode);
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), Varcom.oK)) && (isNE(itemIO.getStatuz(), Varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		rnlallrec.rnlallRec.set(SPACES);
		rnlallrec.company.set(covrpf.getChdrcoy());
		rnlallrec.chdrnum.set(covrpf.getChdrnum());
		rnlallrec.life.set(covrpf.getLife());
		wsaaLifeNum.set(covrpf.getLife());
		rnlallrec.coverage.set(covrpf.getCoverage());
		wsaaCoverageNum.set(covrpf.getCoverage());
		rnlallrec.rider.set(covrpf.getRider());
		rnlallrec.planSuffix.set(covrpf.getPlanSuffix());
		/* MOVE CHDRLIF-BILLCD TO RNLA-BILLCD. */
		rnlallrec.billcd.set(linsrnlIO.getBillcd());
		rnlallrec.duedate.set(linsrnlIO.getInstfrom());
		rnlallrec.user.set(wsaaUser);
		rnlallrec.crdate.set(covrpf.getCrrcd());
		rnlallrec.crtable.set(covrpf.getCrtable());
		if (isNE(wsaaProDaysTotPrem, covrpf.getInstprem())) {
			rnlallrec.totrecd.set(wsaaProDaysTotPrem);
			rnlallrec.covrInstprem.set(wsaaProDaysTotPrem);
		} else {
			rnlallrec.totrecd.set(ZERO);
			rnlallrec.covrInstprem.set(wsaaCovrInstprem[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()]);
		}
		rnlallrec.language.set(atmodrec.language);
		/* If an effective date has been passed, then use this ! */
		/* IF WSAA-UNIT-EFFDATE NOT = VRCM-MAX-DATE */
		/* AND NOT = 0 */
		/* MOVE WSAA-UNIT-EFFDATE TO RNLA-EFFDATE */
		/* GO TO 2348-BYPASS-DATE. */
		/* Read T6647 to determine the sort of date to be used as */
		/* the effective date for unit allocation. */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsaaCompany);
		itdmIO.setItemtabl(t6647);
		itdmIO.setItmfrm(chdrpf.getPtdate());
		wsaaT6647Batctrcde.set(atrtTransCode);
		wsaaT6647Cnttype.set(chdrpf.getCnttype());
		itdmIO.setItemitem(wsaaT6647Item);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(Varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch(itemCoy, itemTabl, itemItem);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), Varcom.endp)) && (isNE(itdmIO.getStatuz(), Varcom.oK))) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if ((isNE(itdmIO.getItemcoy(), wsaaCompany)) || (isNE(itdmIO.getItemtabl(), t6647))
				|| (isNE(itdmIO.getItemitem(), wsaaT6647Item)) || (isEQ(itdmIO.getStatuz(), Varcom.endp))) {
			t6647rec.t6647Rec.set(SPACES);
			t6647rec.procSeqNo.set(0);
		} else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
		if (isNE(t6647rec.unitStatMethod, SPACES)) {
			a300ReadT6659();
		}
		if (isNE(wsaaUnitEffdate, varcom.vrcmMaxDate) && isNE(wsaaUnitEffdate, 0)) {
			rnlallrec.effdate.set(wsaaUnitEffdate);
			/* GO TO 2348-BYPASS-DATE. */
			goTo(GotoLabel.bypassDate2340);
		}
		rnlallrec.effdate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode, "DD")) {
			rnlallrec.effdate.set(covrpf.getCrrcd());
		}
		if (isEQ(t6647rec.efdcode, "LO") && isGT(covrpf.getCrrcd(), datcon1rec.intDate)) {
			rnlallrec.effdate.set(covrpf.getCrrcd());
		}
	}

	protected void bypassDate2340() {
		/* MOVE CHDRLIF-TRANNO TO RNLA-TRANNO. */
		rnlallrec.tranno.set(wsaaTranno);
		/* Use entries 15(invested premium) and 16(non-invested premium). */
		/* RNLALLREC has been extended to cater for the extra linkage. */
		/* MOVE T5645-SACSCODE-03 TO RNLA-SACSCODE. */
		/* MOVE T5645-SACSTYPE-03 TO RNLA-SACSTYP. */
		/* MOVE T5645-GLMAP-03 TO RNLA-GENLCDE. */
		/* MOVE T5645-SACSCODE-15 TO RNLA-SACSCODE. <002> */
		/* MOVE T5645-SACSTYPE-15 TO RNLA-SACSTYP. <002> */
		/* MOVE T5645-GLMAP-15 TO RNLA-GENLCDE. <002> */
		/* MOVE WSAA-T5645-SACSCODE-16 TO RNLA-SACSCODE-02. <013> */
		/* MOVE WSAA-T5645-SACSTYPE-16 TO RNLA-SACSTYP-02. <013> */
		/* MOVE WSAA-T5645-GLMAP-16 TO RNLA-GENLCDE-02. <013> */
		/* MOVE WSAA-T5645-SACSCODE (01) TO RNLA-SACSCODE-02. <013> */
		/* MOVE WSAA-T5645-SACSTYPE (01) TO RNLA-SACSTYP-02. <013> */
		/* MOVE WSAA-T5645-GLMAP (01) TO RNLA-GENLCDE-02. <013> */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[11]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[11]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[11]);
		} else {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[1]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[1]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[1]);
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[12]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[12]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[12]);
		} else {
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[2]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[2]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[2]);
		}
		rnlallrec.cntcurr.set(chdrpf.getCntcurr());
		rnlallrec.cnttype.set(chdrpf.getCnttype());
		rnlallrec.batcactyr.set(atrtAcctYear);
		rnlallrec.batctrcde.set(atrtTransCode);
		rnlallrec.batcactmn.set(atrtAcctMonth);
		rnlallrec.batcbatch.set(atrtBatch);
		rnlallrec.batcbrn.set(atrtBranch);
		rnlallrec.batccoy.set(wsaaCompany);
		rnlallrec.billfreq.set(chdrpf.getBillfreq());
		rnlallrec.moniesDate.set(rnlallrec.effdate);
		/* Calculate the term left to run. It is needed in the generic */
		/* processing routine to work out the initial unit discount factor. */
		datcon3rec.intDate1.set(wsaaCurrentPayuptoDate);
		datcon3rec.intDate2.set(covrpf.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaTerm.set(datcon3rec.freqFactor);
		if (isNE(wsaaTermLeftRemain, 0)) {
			wsaaTerm.add(1);
		}
		rnlallrec.termLeftToRun.set(wsaaTermLeftInteger);
		/* Calculate age next birthday at present. */
		/* Age next birthday = (Age next birthday at contract commencement */
		/* date) + (Term ran so far) */
		compute(wsaaTermRan, 0).set(sub(covrpf.getPremCessTerm(), wsaaTermLeftInteger));
		compute(rnlallrec.anbAtCcd, 0).set(add(covrpf.getAnbAtCcd(), wsaaTermRan));
		rnlallrec.anbAtCcd.set(covrpf.getAnbAtCcd());
		/* Get the total premium from the temporary table for the coverage. */
		/* Call each non blank table entry. The table entry consists the */
		/* required generic subroutine. */
		if(isNE(t5671rec.subprog01, SPACES) && isEQ(wsaaFirstTime, "Y")) {
			callProgram(t5671rec.subprog01, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz, Varcom.oK)) {
				syserrrec.params.set(rnlallrec.rnlallRec);
				xxxxFatalError();
			}
			wsaaFirstTime.set("N");
		}
		if (isNE(t5671rec.subprog02, SPACES)) {
			callProgram(t5671rec.subprog02, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz, Varcom.oK)) {
				syserrrec.params.set(rnlallrec.rnlallRec);
				xxxxFatalError();
			}
		}
		if (isNE(t5671rec.subprog03, SPACES)) {
			callProgram(t5671rec.subprog03, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz, Varcom.oK)) {
				syserrrec.params.set(rnlallrec.rnlallRec);
				xxxxFatalError();
			}
		}
		if (isNE(t5671rec.subprog04, SPACES)) {
			callProgram(t5671rec.subprog04, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz, Varcom.oK)) {
				syserrrec.params.set(rnlallrec.rnlallRec);
				xxxxFatalError();
			}
		}
	}

	protected void readTh6052345() {
		itempf = new Itempf();
		itempf.setItempfx(IT);
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(th605);
		itempf.setItemitem(wsaaCompany.toString());
		itempf = itemDao.getItempfRecord(itempf);

		if (itempf == null) {
			syserrrec.statuz.set(Varcom.mrnf);
			syserrrec.params.set(wsaaCompany.toString().concat(th605).concat(wsaaCompany.toString()));
			xxxxFatalError();
		} else {
			th605rec.th605Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

	protected void updateCommLedger2350() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateCommLedger2351();
					initialCommDue2352();
				case initialCommEarned2353:
					initialCommEarned2353();
				case initialCommPaid2354:
					initialCommPaid2354();
				case servCommDue2355:
					servCommDue2355();
				case servCommEarned2356:
					servCommEarned2356();
				case rnwCommDue2357:
					rnwCommDue2357();
				case rnwCommEarned2358:
					rnwCommEarned2358();
				case rnwCommPaid:
					rnwCommPaid();
				case initialOvrdCommDue2359:
					initialOvrdCommDue2359();
				case initialOvrdCommEarned2359:
					initialOvrdCommEarned2359();
				case initialOvrdCommPaid2359:
					initialOvrdCommPaid2359();
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	 * <pre>
	* This is to done by calling the subroutine LIFACMV for each type
	* of commission.
	 * </pre>
	 */
	protected void updateCommLedger2351() {
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rdocnum.set(wsaaChdrnum);
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.batccoy.set(wsaaCompany);
		lifacmvrec.rldgcoy.set(wsaaCompany);
		lifacmvrec.genlcoy.set(wsaaCompany);
		lifacmvrec.batcactyr.set(atrtAcctYear);
		lifacmvrec.batctrcde.set(atrtTransCode);
		lifacmvrec.batcactmn.set(atrtAcctMonth);
		lifacmvrec.batcbatch.set(atrtBatch);
		lifacmvrec.batcbrn.set(atrtBranch);
		lifacmvrec.origcurr.set(chdrpf.getCntcurr());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		/* MOVE CHDRLIF-TRANNO TO LIFA-TRANNO. */
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. */
		/* MOVE LINSRNL-INSTFROM TO LIFA-EFFDATE. <LA3426> */
		/* MOVE DTC1-INT-DATE TO LIFA-EFFDATE. <LA3426> */
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
	}

	protected void initialCommDue2352() {
		if (isEQ(wsaaBascpyDue, 0)) {
			goTo(GotoLabel.initialCommEarned2353);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origamt.set(wsaaBascpyDue);
		lifacmvrec.sacscode.set(t5645rec.sacscode08);
		lifacmvrec.sacstyp.set(t5645rec.sacstype08);
		lifacmvrec.glsign.set(t5645rec.sign08);
		lifacmvrec.glcode.set(t5645rec.glmap08);
		if (isEQ(th605rec.indic, "Y")) {
			wsaaStoreEffdate.set(lifacmvrec.effdate);
			lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		}
		lifacmvrec.contot.set(t5645rec.cnttot08);
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
		wsaaRldgLife.set(agcmbchIO.getLife());
		wsaaRldgCoverage.set(agcmbchIO.getCoverage());
		wsaaRldgRider.set(agcmbchIO.getRider());
		wsaaPlan.set(agcmbchIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		if (isEQ(th605rec.indic, "Y")) {
			lifacmvrec.effdate.set(wsaaStoreEffdate);
		}
		/* Override Commission */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
			zorlnkrec.effdate.set(lifacmvrec.effdate);
			b500CallZorcompy();
			if (gstOnCommFlag  && isNE(zorlnkrec.gstAmount,0)) {  
				gstPostings(zorlnkrec.gstAmount, wsaaReporttoAgnt, 21); 
			}
		}
	}

	protected void initialCommEarned2353() {
		if (isEQ(wsaaBascpyErn, 0)) {
			goTo(GotoLabel.initialCommPaid2354);
		}
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaBascpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[7]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[7]);
			lifacmvrec.glsign.set(wsaaT5645Sign[7]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[7]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[7]);
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		} else {
			lifacmvrec.sacscode.set(t5645rec.sacscode09);
			lifacmvrec.sacstyp.set(t5645rec.sacstype09);
			lifacmvrec.glsign.set(t5645rec.sign09);
			lifacmvrec.glcode.set(t5645rec.glmap09);
			lifacmvrec.contot.set(t5645rec.cnttot09);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaBascpyErn);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		if(gstOnCommFlag && isNE(wsaaCompErnInitCommGst,0)) { 
			gstPostings(wsaaCompErnInitCommGst,wsaaRldgacct, 20); 
		}
	}

	protected void initialCommPaid2354() {
		if (isEQ(wsaaBascpyPay, 0)) {
			goTo(GotoLabel.servCommDue2355);
		}
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			if(gstOnCommFlag) {
				lifacmvrec.sacscode.set(t5645rec.sacscode08);
				lifacmvrec.sacstyp.set(t5645rec.sacstype08);
				lifacmvrec.glsign.set(t5645rec.sign08);
				lifacmvrec.glcode.set(t5645rec.glmap08);
				lifacmvrec.contot.set(t5645rec.cnttot08);
				lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			}
			else {
				lifacmvrec.sacscode.set(wsaaT5645Sacscode[8]);
				lifacmvrec.sacstyp.set(wsaaT5645Sacstype[8]);
				lifacmvrec.glsign.set(wsaaT5645Sign[8]);
				lifacmvrec.glcode.set(wsaaT5645Glmap[8]);
				lifacmvrec.contot.set(wsaaT5645Cnttot[8]);
				wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
				wsaaRldgLife.set(agcmbchIO.getLife());
				wsaaRldgCoverage.set(agcmbchIO.getCoverage());
				wsaaRldgRider.set(agcmbchIO.getRider());
				wsaaPlan.set(agcmbchIO.getPlanSuffix());
				wsaaRldgPlanSuffix.set(wsaaPlansuff);
				lifacmvrec.rldgacct.set(wsaaRldgacct);
				lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
				lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
			}
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaBascpyPay);
		} else {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaBascpyPay);
			lifacmvrec.sacscode.set(t5645rec.sacscode10);
			lifacmvrec.sacstyp.set(t5645rec.sacstype10);
			lifacmvrec.glsign.set(t5645rec.sign10);
			lifacmvrec.glcode.set(t5645rec.glmap10);
			lifacmvrec.contot.set(t5645rec.cnttot10);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void servCommDue2355() {
		if (isEQ(wsaaSrvcpyDue, 0)) {
			goTo(GotoLabel.servCommEarned2356);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origamt.set(wsaaSrvcpyDue);
		lifacmvrec.sacscode.set(t5645rec.sacscode11);
		lifacmvrec.sacstyp.set(t5645rec.sacstype11);
		lifacmvrec.glsign.set(t5645rec.sign11);
		lifacmvrec.glcode.set(t5645rec.glmap11);
		lifacmvrec.contot.set(t5645rec.cnttot11);
		if (isEQ(th605rec.indic, "Y")) {
			wsaaStoreEffdate.set(lifacmvrec.effdate);
			lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		}
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.tranref.set(wsaaChdrnum);
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Override Commission */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
			zorlnkrec.effdate.set(agcmbchIO.getEfdate());
			zorlnkrec.effdate.set(lifacmvrec.effdate);
			b500CallZorcompy();
		}
	}

	protected void servCommEarned2356() {
		if (isEQ(wsaaSrvcpyErn, 0)) {
			goTo(GotoLabel.rnwCommDue2357);
		}
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaSrvcpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[9]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[9]);
			lifacmvrec.glsign.set(wsaaT5645Sign[9]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[9]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[9]);
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		} else {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaSrvcpyErn);
			lifacmvrec.sacscode.set(t5645rec.sacscode12);
			lifacmvrec.sacstyp.set(t5645rec.sacstype12);
			lifacmvrec.glsign.set(t5645rec.sign12);
			lifacmvrec.glcode.set(t5645rec.glmap12);
			lifacmvrec.contot.set(t5645rec.cnttot12);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		/* MOVE AGCMBCH-AGNTNUM TO LIFA-TRANREF. */
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void rnwCommDue2357() {
		if (isEQ(wsaaRnwcpyDue, 0)) {
			goTo(GotoLabel.rnwCommEarned2358);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origamt.set(wsaaRnwcpyDue);
		lifacmvrec.sacscode.set(t5645rec.sacscode13);
		lifacmvrec.sacstyp.set(t5645rec.sacstype13);
		lifacmvrec.glsign.set(t5645rec.sign13);
		lifacmvrec.glcode.set(t5645rec.glmap13);
		lifacmvrec.contot.set(t5645rec.cnttot13);
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.tranref.set(wsaaChdrnum);
		if (isEQ(th605rec.indic, "Y")) {
			wsaaStoreEffdate.set(lifacmvrec.effdate);
			lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		}
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		lifacmvrec.function.set("PSTW");
		wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
		wsaaRldgLife.set(agcmbchIO.getLife());
		wsaaRldgCoverage.set(agcmbchIO.getCoverage());
		wsaaRldgRider.set(agcmbchIO.getRider());
		wsaaPlan.set(agcmbchIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Override Commission */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
			zorlnkrec.effdate.set(lifacmvrec.effdate);
			b500CallZorcompy();
			if(gstOnCommFlag && isNE(zorlnkrec.gstAmount,0)) { 
 				gstPostings(zorlnkrec.gstAmount,wsaaReporttoAgnt, 21);  
 			}    
		}
	}

	protected void rnwCommEarned2358() {
		if (isEQ(wsaaRnwcpyErn, 0)) {  
			goTo(GotoLabel.rnwCommPaid);
		}
		/* GO TO 2359-EXIT. */
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			/* MOVE WSAA-RNWCPY-DUE TO LIFA-ORIGAMT */
			lifacmvrec.origamt.set(wsaaRnwcpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[10]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[10]);
			lifacmvrec.glsign.set(wsaaT5645Sign[10]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[10]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[10]);
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		} else {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			/* MOVE WSAA-RNWCPY-DUE TO LIFA-ORIGAMT */
			lifacmvrec.origamt.set(wsaaRnwcpyErn);
			lifacmvrec.sacscode.set(t5645rec.sacscode14);
			lifacmvrec.sacstyp.set(t5645rec.sacstype14);
			lifacmvrec.glsign.set(t5645rec.sign14);
			lifacmvrec.glcode.set(t5645rec.glmap14);
			lifacmvrec.contot.set(t5645rec.cnttot14);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(wsaaChdrnum);
		}
		/* MOVE AGCMBCH-AGNTNUM TO LIFA-TRANREF. */
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		if(gstOnCommFlag && isNE(wsaaCompErnRnwlCommGst,0)) { 
			gstPostings(wsaaCompErnRnwlCommGst, agcmbchIO.getAgntnum(), 22); 
		}
		
	}
	
	protected void rnwCommPaid() { 
		if (isEQ(wsaaRnwcpyPay, 0)) {  
			goTo(GotoLabel.initialOvrdCommDue2359);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);	
         lifacmvrec.origamt.set(wsaaRnwcpyPay);
            /* ADD +1 TO LIFA-JRNSEQ */
            if (isEQ(t5688rec.comlvlacc, "Y")) {
            	if(gstOnCommFlag) {
            		lifacmvrec.sacscode.set(t5645rec.sacscode13);
        			lifacmvrec.sacstyp.set(t5645rec.sacstype13);
        			lifacmvrec.glsign.set(t5645rec.sign13);
        			lifacmvrec.glcode.set(t5645rec.glmap13);
        			lifacmvrec.contot.set(t5645rec.cnttot13);
            		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
            	} else {
	                lifacmvrec.sacscode.set(wsaaT5645Sacscode[10]);
            	    lifacmvrec.sacstyp.set(wsaaT5645Sacstype[10]);
            	    lifacmvrec.glsign.set(wsaaT5645Sign[10]);
            	    lifacmvrec.glcode.set(wsaaT5645Glmap[10]);
            	    lifacmvrec.contot.set(wsaaT5645Cnttot[10]);
	                wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
	    			wsaaRldgLife.set(agcmbchIO.getLife());
	    			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
	    			wsaaRldgRider.set(agcmbchIO.getRider());
	    			wsaaPlan.set(agcmbchIO.getPlanSuffix());
	    			wsaaRldgPlanSuffix.set(wsaaPlansuff);
	    			lifacmvrec.rldgacct.set(wsaaRldgacct);
	    			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
	    			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
            	}
            } else {
                lifacmvrec.sacscode.set(t5645rec.sacscode14);
    			lifacmvrec.sacstyp.set(t5645rec.sacstype14);
    			lifacmvrec.glsign.set(t5645rec.sign14);
    			lifacmvrec.glcode.set(t5645rec.glmap14);
    			lifacmvrec.contot.set(t5645rec.cnttot14);
    			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
    			lifacmvrec.substituteCode[6].set(SPACES);
    			lifacmvrec.rldgacct.set(wsaaChdrnum);
            }
            lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
            lifacmvrec.function.set("PSTW");
    		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
    		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
    			syserrrec.params.set(lifacmvrec.lifacmvRec);
    			xxxxFatalError();
    		}
	}
	
	

	protected void initialOvrdCommDue2359() {
		if (isEQ(wsaaOvrdBascpyDue, 0)) {
			goTo(GotoLabel.initialOvrdCommEarned2359);
		}
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.origamt.set(wsaaOvrdBascpyDue);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[15]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[15]);
		lifacmvrec.glsign.set(wsaaT5645Sign[15]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[15]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[15]);
		lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
		lifacmvrec.tranref.set(agcmbchIO.getCedagent());
		lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void initialOvrdCommEarned2359() {
		if (isEQ(wsaaOvrdBascpyErn, 0)) {
			goTo(GotoLabel.initialOvrdCommPaid2359);
		}
		/* <013> */
		wsaaJrnseq.add(1);
		/* MOVE WSAA-JRNSEQ TO LIFA-JRNSEQ. <013> */
		/* MOVE WSAA-OVRD-BASCPY-ERN TO LIFA-ORIGAMT. <013> */
		/* MOVE WSAA-T5645-SACSCODE (03) TO LIFA-SACSCODE. <013> */
		/* MOVE WSAA-T5645-SACSTYPE (03) TO LIFA-SACSTYP. <013> */
		/* MOVE WSAA-T5645-SIGN (03) TO LIFA-GLSIGN. <013> */
		/* MOVE WSAA-T5645-GLMAP (03) TO LIFA-GLCODE. <013> */
		/* MOVE WSAA-T5645-CNTTOT (03) TO LIFA-CONTOT. <013> */
		/* MOVE AGCMBCH-AGNTNUM TO LIFA-TRANREF. */
		/* MOVE AGCMBCH-CEDAGENT TO LIFA-RLDGACCT. */
		/* MOVE AGCMBCH-CHDRNUM TO WSAA-RLDG-CHDRNUM */
		/* MOVE AGCMBCH-LIFE TO WSAA-RLDG-LIFE. */
		/* MOVE AGCMBCH-COVERAGE TO WSAA-RLDG-COVERAGE. */
		/* MOVE AGCMBCH-RIDER TO WSAA-RLDG-RIDER. */
		/* MOVE AGCMBCH-PLAN-SUFFIX TO WSAA-PLAN. */
		/* MOVE WSAA-PLANSUFF TO WSAA-RLDG-PLAN-SUFFIX. <013> */
		/* MOVE WSAA-RLDGACCT TO LIFA-RLDGACCT. <013> */
		/* MOVE CHDRLIF-CNTTYPE TO LIFA-SUBSTITUTE-CODE(1). <013> */
		/* MOVE COVR-CRTABLE TO LIFA-SUBSTITUTE-CODE(6). <013> */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[14]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[14]);
			lifacmvrec.glsign.set(wsaaT5645Sign[14]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[14]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[14]);
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		} else {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[3]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[3]);
			lifacmvrec.glsign.set(wsaaT5645Sign[3]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[3]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[3]);
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			lifacmvrec.rldgacct.set(agcmbchIO.getChdrnum());
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void initialOvrdCommPaid2359() {
		if (isEQ(wsaaOvrdBascpyPay, 0)) {
			return;
		}
		wsaaJrnseq.add(1);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
			/* MOVE WSAA-T5645-SACSCODE(13) TO LIFA-SACSCODE <021> */
			/* MOVE WSAA-T5645-SACSTYPE(13) TO LIFA-SACSTYP <021> */
			/* MOVE WSAA-T5645-SIGN(13) TO LIFA-GLSIGN <021> */
			/* MOVE WSAA-T5645-GLMAP(13) TO LIFA-GLCODE <021> */
			/* MOVE WSAA-T5645-CNTTOT(13) TO LIFA-CONTOT <021> */
			/* MOVE WSAA-T5645-SACSCODE(14) TO LIFA-SACSCODE <A06168> */
			/* MOVE WSAA-T5645-SACSTYPE(14) TO LIFA-SACSTYP <A06168> */
			/* MOVE WSAA-T5645-SIGN(14) TO LIFA-GLSIGN <A06168> */
			/* MOVE WSAA-T5645-GLMAP(14) TO LIFA-GLCODE <A06168> */
			/* MOVE WSAA-T5645-CNTTOT(14) TO LIFA-CONTOT <A06168> */
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[13]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[13]);
			lifacmvrec.glsign.set(wsaaT5645Sign[13]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[13]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[13]);
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		} else {
			lifacmvrec.substituteCode[1].set(chdrpf.getCnttype());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
			/* MOVE WSAA-T5645-SACSCODE(4) TO LIFA-SACSCODE <013> */
			/* MOVE WSAA-T5645-SACSTYPE(4) TO LIFA-SACSTYP <013> */
			/* MOVE WSAA-T5645-SIGN(4) TO LIFA-GLSIGN <013> */
			/* MOVE WSAA-T5645-GLMAP(4) TO LIFA-GLCODE <013> */
			/* MOVE WSAA-T5645-CNTTOT(4) TO LIFA-CONTOT <013> */
			/* MOVE WSAA-T5645-SACSCODE(3) TO LIFA-SACSCODE <A06168> */
			/* MOVE WSAA-T5645-SACSTYPE(3) TO LIFA-SACSTYP <A06168> */
			/* MOVE WSAA-T5645-SIGN(3) TO LIFA-GLSIGN <A06168> */
			/* MOVE WSAA-T5645-GLMAP(3) TO LIFA-GLCODE <A06168> */
			/* MOVE WSAA-T5645-CNTTOT(3) TO LIFA-CONTOT <A06168> */
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[4]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[4]);
			lifacmvrec.glsign.set(wsaaT5645Sign[4]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[4]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[4]);
			lifacmvrec.rldgacct.set(agcmbchIO.getCedagent());
		}
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}
	
	protected void gstPostings(PackedDecimalData origamt, FixedLengthStringData agntNum, int gstSub) {	
		lifacmvrec.origamt.set(origamt);
		/* ADD +1 TO LIFA-JRNSEQ */	
	    if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.rldgacct.set(agntNum);
	        lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
	    }
	    lifacmvrec.tranref.set(agntNum);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[gstSub]);
	    lifacmvrec.sacstyp.set(wsaaT5645Sacstype[gstSub]);
	    lifacmvrec.glsign.set(wsaaT5645Sign[gstSub]);
	    lifacmvrec.glcode.set(wsaaT5645Glmap[gstSub]);
	    lifacmvrec.contot.set(wsaaT5645Cnttot[gstSub]);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	protected void updateCovrStatus2370() {

		List<Covrpf> covrpflist = covrpfDAO.getCovrByComAndNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());

		if (covrpflist != null && !covrpflist.isEmpty()) {

			for (int i = 0; i < covrpflist.size(); i++) {
				covrpf = covrpflist.get(i);
				readT568711100(covrpf);
				if (isEQ(t5687rec.singlePremInd, "Y")) {
					continue;
				}
				/* Validate the coverage status against T5679. */
				wsaaValidStatus.set("N");

				if ((isNE(covrpf.getCoverage(), SPACES))
						&& ((isEQ(covrpf.getRider(), SPACES)) || (isEQ(covrpf.getRider(), "00")))) {
					for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12) || isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)) {
						validateCovrStatus2110();
					}
				}
				/* Validate the new rider status against T5679. */
				if ((isNE(covrpf.getCoverage(), SPACES))
						&& ((isNE(covrpf.getRider(), SPACES)) && (isNE(covrpf.getRider(), "00")))) {
					for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12) || isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)) {
						validateRiderStatus2120();
					}
				}
				if (isEQ(wsaaValidStatus, "N")) {
					continue;
				}

				String wsaaUpdateFlag = "N";
				if (isNE(t5687rec.rtrnwfreq, ZERO) && (isNE(covrpf.getPremCessDate(), covrpf.getRerateDate()))) {
					continue;
				}

				if (isGTE(chdrpf.getPtdate(), covrpf.getPremCessDate())) {
					if ((isEQ(covrpf.getRider(), "00")) || (isEQ(covrpf.getRider(), "  "))) {
						if (isNE(t5679rec.setCovPremStat, SPACES)) {
							covrpf.setPstatcode(t5679rec.setCovPremStat.toString());
							wsaaUpdateFlag = "Y";
						}
					} else {
						if (isNE(t5679rec.setRidPremStat, SPACES)) {
							covrpf.setPstatcode(t5679rec.setRidPremStat.toString());
							wsaaUpdateFlag = "Y";
						}
					}
				}

				if (isNE(wsaaUpdateFlag, "Y")) {
					continue;
				}
				if (!covrpfDAO.updateCovrPstatcode(covrpf)) {
					syserrrec.params.set(covrpf.getChdrnum());
					xxxxFatalError();
				}
			}
		}
	}

	protected void generalHousekeeping4000() {
		/* Write PTRN record. */
		/* MOVE SPACE TO PTRN-PARAMS. */
		ptrnpf.setChdrcoy(wsaaCompany.toString());
		ptrnpf.setChdrnum(wsaaChdrnum.toString());
		/* MOVE CHDRLIF-TRANNO TO PTRN-TRANNO. */
		ptrnpf.setTranno(wsaaTranno.toInt());
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		/* MOVE DTC1-INT-DATE TO PTRN-PTRNEFF. */
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		ptrnpf.setUserT(wsaaUser.toInt());
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(wsaaCompany.toString());
		ptrnpf.setBatcbrn(atrtBranch.toString());
		ptrnpf.setBatcactyr(atrtAcctYear.toInt());
		ptrnpf.setBatctrcde(atrtTransCode.toString());
		ptrnpf.setBatcactmn(atrtAcctMonth.toInt());
		ptrnpf.setBatcbatch(atrtBatch.toString());
		ptrnpf.setValidflag(" ");
		List<Ptrnpf> ptrnList = new ArrayList<>();
		ptrnList.add(ptrnpf);
		boolean result = ptrnpfDAO.insertPtrnPF(ptrnList);

		if (!result) {
			syserrrec.params.set(ptrnpf.getChdrcoy().concat(ptrnpf.getChdrnum()));
			xxxxFatalError();
		}
		/* Update batch header. */
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set(atrtPrefix);
		batcuprec.batccoy.set(wsaaCompany);
		batcuprec.batcbrn.set(atrtBranch);
		batcuprec.batcactyr.set(atrtAcctYear);
		batcuprec.batctrcde.set(atrtTransCode);
		batcuprec.batcactmn.set(atrtAcctMonth);
		batcuprec.batcbatch.set(atrtBatch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, Varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/* DIARY PROCESSING.... */
		/* Call the section for Diary Maintenance. */
		dryProcessing12000();
		/* RELEASE SOFTLOCK.... */
		/* Release the soft lock on the contract. */
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(wsaaChdrnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

	protected void readT5645Page34200() {

		/* If more values are added onto the 3rd screen, then a more */
		/* method should be used. (such as looping). But for now, this */
		/* will suffice. */
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsaaCompany.toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq("02");
		itempf = itemDao.getItempfRecordBySeq(itempf);//second call

		if (itempf != null) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		} else {
			t5645rec.t5645Rec.set(SPACES);
		}

		if (isEQ(t5645rec.sacscode[2], SPACES) && isEQ(t5645rec.sacstype[2], SPACES)
				&& isEQ(t5645rec.glmap[2], SPACES)) {
			return;
		}
		wsaaT5645Cnttot32.set(t5645rec.cnttot[2]);
		wsaaT5645Glmap32.set(t5645rec.glmap[2]);
		wsaaT5645Sacscode32.set(t5645rec.sacscode[2]);
		wsaaT5645Sacstype32.set(t5645rec.sacstype[2]);
		
		if (isEQ(t5645rec.sacscode[3], SPACES) && isEQ(t5645rec.sacstype[3], SPACES)
				&& isEQ(t5645rec.glmap[3], SPACES)) {
			return;
		}
		wsaaT5645Cnttot33.set(t5645rec.cnttot[3]);
		wsaaT5645Glmap33.set(t5645rec.glmap[3]);
		wsaaT5645Sacscode33.set(t5645rec.sacscode[3]);
		wsaaT5645Sacstype33.set(t5645rec.sacstype[3]);
		wsaaT5645Sign33.set(t5645rec.sign[3]);
		
		if (isEQ(t5645rec.sacscode[4], SPACES) && isEQ(t5645rec.sacstype[4], SPACES)
				&& isEQ(t5645rec.glmap[4], SPACES)) {
			return;
		}
		wsaaT5645Cnttot34.set(t5645rec.cnttot[4]);
		wsaaT5645Glmap34.set(t5645rec.glmap[4]);
		wsaaT5645Sacscode34.set(t5645rec.sacscode[4]);
		wsaaT5645Sacstype34.set(t5645rec.sacstype[4]);
		wsaaT5645Sign34.set(t5645rec.sign[4]);
	}

	protected void cessationProcess5700() {
		/* Read the Coverage Rider file. */
		if (isEQ(covrpf.getRider(), "00") || isEQ(covrpf.getRider(), "  ")) {
			coverageStatus7000();
		} else {
			riderStatus6000();
		}

	}

	protected void riderStatus6000() {
		if (isEQ(wsaaAllRidExp, "Y") && isGT(covrpf.getPremCessDate(), wsaaBillDate)) {
			wsaaAllRidExp = "N";
		}
	}

	protected void coverageStatus7000() {
		if (isEQ(wsaaAllCovExp, "Y") && isGT(covrpf.getPremCessDate(), wsaaBillDate)) {
			wsaaAllCovExp = "N";
		}
	}

	protected void initialiseCovrPrem8000() {
		/* PARA */
		wsaaCovrInstprem[wsaaL.toInt()][wsaaC.toInt()].set(0);
		wsaaC.add(1);
		if (isGT(wsaaC, 99)) {
			wsaaL.add(1);
			wsaaC.set(1);
		}
		/* EXIT */
	}

	protected void findCovrTotPrem8100() {
		List<Covrpf> covrlist = covrpfDAO.getCovrByComAndNumAllFlagRecord(linsrnlIO.getChdrcoy().toString(),
				linsrnlIO.getChdrnum().toString());
		if (covrlist == null || covrlist.isEmpty())
			return;
		for (int i = 0; i < covrlist.size(); i++) {
			covrpf = covrlist.get(i);
			if ((isGTE(linsrnlIO.getInstfrom(), covrpf.getCurrto()))
					|| (isLT(linsrnlIO.getInstfrom(), covrpf.getCurrfrom()))) {
				continue;
			}
			if (isEQ(covrpf.getRider(), "00") || isEQ(covrpf.getRider(), "  ")) {
				wsaaCoverageNum.set(covrpf.getCoverage());
				wsaaLifeNum.set(covrpf.getLife());
				wsaaCovrInstprem[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()].add(covrpf.getInstprem().doubleValue());
			}
		}

	}

	protected void checkEndOfMonth9000() {
		/* PARA */
		wsaaEndOfMonth = "N";
		wsaaLeapYear = "N";
		checkLeapYear10000();
		if (isEQ(wsaaLeapYear, "Y")) {
			wsaaDaysInMonth.set(wsaaDaysInMonthLeap);
		} else {
			wsaaDaysInMonth.set(wsaaDaysInMonthNorm);
		}
		if (isGTE(wsaaEomDay, wsaaMonthDays[wsaaEomMonth.toInt()])) {
			wsaaEndOfMonth = "Y";
		}
		/* EXIT */
	}

	protected void checkLeapYear10000() {
		/* CHK-LEAP-YEAR */
		/* If the year is the beginning of the century */
		/* Divide the year by 400 and if result is an integer value, */
		/* this indicates that this is a Century leap year. */
		if (isEQ(wsaaCenturyYear, ZERO)) {
			compute(wsaaResult, 0).set(div(wsaaEomYear, 400));
			wsaaResult.multiply(400);
			if (isEQ(wsaaResult, wsaaEomYear)) {
				wsaaLeapYear = "Y";
			}
			return;
		}
		/* Divide year by 4 and if result is an integer value, this */
		/* indicates a leap year. */
		compute(wsaaResult, 0).set(div(wsaaEomYear, 4));
		wsaaResult.multiply(4);
		if (isEQ(wsaaResult, wsaaEomYear)) {
			wsaaLeapYear = "Y";
		}
		/* EXIT */
	}

	protected void a300ReadT6659() {
		a301Read();
		a350CheckT6659Details();
	}

	protected void a301Read() {
		itdmIO.setItemcoy(wsaaCompany);
		itdmIO.setItemtabl(t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(wsaaUnitEffdate);
		itdmIO.setFunction(Varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch(itemCoy, itemTabl, itemItem);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK) && isNE(itdmIO.getStatuz(), Varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(t6647rec.unitStatMethod, itdmIO.getItemitem()) || isNE(wsaaCompany, itdmIO.getItemcoy())
				|| isNE(itdmIO.getItemtabl(), t6659) || isEQ(itdmIO.getStatuz(), Varcom.endp)) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g029);
			xxxxFatalError();
		} else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

	protected void a350CheckT6659Details() {
		/* Check the details and call the generic subroutine from T6659. */
		if (isEQ(t6659rec.subprog, SPACES) || isNE(t6659rec.annOrPayInd, "P") || isNE(t6659rec.osUtrnInd, "Y")) {
			return;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(wsaaCompany);
		annprocrec.chdrnum.set(wsaaChdrnum);
		annprocrec.effdate.set(wsaaUnitEffdate);
		annprocrec.batchkey.set(atrtBatchKey);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			xxxxFatalError();
		}
	}

	protected void readT568711100(Covrpf covrpf) {

		/* Access the table to establish if this is a single premium */
		/* component. */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setFunction(Varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch(itemCoy, itemTabl, itemItem);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK) && isNE(itdmIO.getStatuz(), Varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company) || isNE(itdmIO.getItemtabl(), t5687)
				|| isNE(itdmIO.getItemitem(), covrpf.getCrtable()) || isEQ(itdmIO.getStatuz(), Varcom.endp)) {
			syserrrec.params.set(covrpf.getCrtable());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		} else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

	protected void dryProcessing12000() {
		start12010();
	}

	protected void start12010() {
		/* This section will determine if the DIARY system is present */
		/* If so, the appropriate parameters are filled and the */
		/* diary processor is called. */
		wsaaT7508Cnttype.set(chdrpf.getCnttype());
		wsaaT7508Batctrcde.set(atrtTransCode);
		readT750813000();
		/* If item not found try other types of contract. */
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT750813000();
		}
		/* If item not found no Diary Update Processing is Required. */
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			return;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(atrtCompany);
		drypDryprcRecInner.drypBranch.set(atrtBranch);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(atrtBatchKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrpf.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuCoy);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters. */
		drypDryprcRecInner.drypTranno.set(chdrpf.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
		drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, Varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

	protected void readT750813000() {
		start13010();
	}

	protected void start13010() {
		itempf = new Itempf();

		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(IT);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

	protected void b100InitializeArrays() {
		/* B110-INIT */
		for (wsaaAgcmIx.set(1); !(isGT(wsaaAgcmIx, wsaaAgcmIxSize)); wsaaAgcmIx.add(1)) {
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(SPACES);
			for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy, wsaaAgcmIySize)); wsaaAgcmIy.add(1)) {
				wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
			}
		}
		/* B190-EXIT */
	}

	protected void b200PremiumHistory() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b210Init();
				case b220Call:
					b220Call();
					b230Summary();
				case b280Next:
					b280Next();
				case b290Exit:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void b210Init() {
		/* Read the AGCM for differentiating the First & Renewal Year */
		/* Premium */
		agcmseqIO.setDataArea(SPACES);
		agcmseqIO.setChdrcoy(covrpf.getChdrcoy());
		agcmseqIO.setChdrnum(covrpf.getChdrnum());
		agcmseqIO.setLife(covrpf.getLife());
		agcmseqIO.setCoverage(covrpf.getCoverage());
		agcmseqIO.setRider(covrpf.getRider());
		agcmseqIO.setPlanSuffix(covrpf.getPlanSuffix());
		agcmseqIO.setSeqno(ZERO);
		agcmseqIO.setFormat(formatsInner.agcmseqrec);
		agcmseqIO.setFunction(Varcom.begn);
		// performance improvement -- Anjali
		agcmseqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmseqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
	}

	protected void b220Call() {
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(), Varcom.oK) && isNE(agcmseqIO.getStatuz(), Varcom.endp)) {
			syserrrec.statuz.set(agcmseqIO.getStatuz());
			syserrrec.params.set(agcmseqIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(agcmseqIO.getStatuz(), Varcom.endp) || isNE(agcmseqIO.getChdrcoy(), covrpf.getChdrcoy())
				|| isNE(agcmseqIO.getChdrnum(), covrpf.getChdrnum()) || isNE(agcmseqIO.getLife(), covrpf.getLife())
				|| isNE(agcmseqIO.getCoverage(), covrpf.getCoverage())
				|| isNE(agcmseqIO.getRider(), covrpf.getRider())) {
			agcmseqIO.setStatuz(Varcom.endp);
			goTo(GotoLabel.b290Exit);
		}
		/* Skip those irrelevant records */
		if (isNE(agcmseqIO.getOvrdcat(), "B") || isEQ(agcmseqIO.getPtdate(), ZERO) || isEQ(agcmseqIO.getEfdate(), ZERO)
				|| isEQ(agcmseqIO.getEfdate(), varcom.vrcmMaxDate)) {
			goTo(GotoLabel.b280Next);
		}
	}

	protected void b230Summary() {
		wsaaAgcmFound.set("N");
		for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy, wsaaAgcmIySize)
				|| isEQ(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO) || agcmFound.isTrue()); wsaaAgcmIy
						.add(1)) {
			if (isEQ(agcmseqIO.getSeqno(), wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()])) {
				wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].add(agcmseqIO.getAnnprem());
				agcmFound.setTrue();
			}
		}
		if (!agcmFound.isTrue()) {
			if (isGT(wsaaAgcmIy, wsaaAgcmIySize)) {
				syserrrec.statuz.set(e103);
				xxxxFatalError();
			}
			wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getSeqno());
			wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getEfdate());
			wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getAnnprem());
		}
	}

	protected void b280Next() {
		agcmseqIO.setFunction(Varcom.nextr);
		goTo(GotoLabel.b220Call);
	}

	protected void b300WriteArrays() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b310Init();
				case b320Loop:
					b320Loop();
					b330Writ();
				case b380Next:
					b380Next();
				case b390Exit:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void b310Init() {
		wsaaAgcmIy.set(1);
	}

	protected void b320Loop() {
		if (isEQ(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)) {
			goTo(GotoLabel.b390Exit);
		} else {
			if (isGT(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], linsrnlIO.getInstfrom())) {
				goTo(GotoLabel.b380Next);
			}
		}
	}

	protected void b330Writ() {
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]);
		datcon3rec.intDate2.set(linsrnlIO.getInstfrom());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			xxxxFatalError();
		}
		if (isGTE(datcon3rec.freqFactor, 1)) {
			renPrem.setTrue();
		} else {
			firstPrem.setTrue();
		}
		if (isNE(wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(covrpf.getChdrcoy());
			zptnIO.setChdrnum(covrpf.getChdrnum());
			zptnIO.setLife(covrpf.getLife());
			zptnIO.setCoverage(covrpf.getCoverage());
			zptnIO.setRider(covrpf.getRider());
			zptnIO.setTranno(wsaaTranno);
			setPrecision(zptnIO.getOrigamt(), 3);
			zptnIO.setOrigamt(div(wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], wsaaBillfq9), true); //need to check
			zrdecplrec.amountIn.set(zptnIO.getOrigamt());
			c000CallRounding();
			zptnIO.setOrigamt(zrdecplrec.amountOut);
			zptnIO.setTransCode(atrtTransCode);
			zptnIO.setEffdate(linsrnlIO.getInstfrom());
			zptnIO.setInstfrom(linsrnlIO.getInstfrom());
			zptnIO.setBillcd(linsrnlIO.getBillcd());
			zptnIO.setInstto(linsrnlIO.getInstto());
			zptnIO.setTrandate(datcon1rec.intDate);
			zptnIO.setZprflg(wsaaPremType);
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(Varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(), Varcom.oK)) {
				syserrrec.statuz.set(zptnIO.getStatuz());
				syserrrec.params.set(zptnIO.getParams());
				xxxxFatalError();
			}
		}
	}

	protected void b380Next() {
		wsaaAgcmIy.add(1);
		goTo(GotoLabel.b320Loop);
	}

	protected void b400LocatePremium() {
		/* B410-INIT */
		wsaaAgcmFound.set("N");
		wsaaAgcmPremium.set(ZERO);
		for (wsaaAgcmIa.set(1); !(isGT(wsaaAgcmIa, wsaaAgcmIxSize) || agcmFound.isTrue()); wsaaAgcmIa.add(1)) {
			if (isEQ(agcmbchIO.getChdrcoy(), wsaaAgcmChdrcoy[wsaaAgcmIa.toInt()])
					&& isEQ(agcmbchIO.getChdrnum(), wsaaAgcmChdrnum[wsaaAgcmIa.toInt()])
					&& isEQ(agcmbchIO.getLife(), wsaaAgcmLife[wsaaAgcmIa.toInt()])
					&& isEQ(agcmbchIO.getCoverage(), wsaaAgcmCoverage[wsaaAgcmIa.toInt()])
					&& isEQ(agcmbchIO.getRider(), wsaaAgcmRider[wsaaAgcmIa.toInt()])) {
				for (wsaaAgcmIb.set(1); !(isGT(wsaaAgcmIb, wsaaAgcmIySize) || agcmFound.isTrue()); wsaaAgcmIb.add(1)) {
					if (isEQ(agcmbchIO.getSeqno(), wsaaAgcmSeqno[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()])) {
						agcmFound.setTrue();
						wsaaAgcmPremium.set(wsaaAgcmAnnprem[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()]);
					}
				}
			}
		}
		/* B490-EXIT */
	}

	protected void b500CallZorcompy() {
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(chdrpf.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(chdrpf.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(atmodrec.batchKey);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			xxxxFatalError();
		}
	}

	protected void b700PostTax() {
		/* B700-START */
		/* Read TAXD records */
		taxdbilIO.setStatuz(Varcom.oK);
		taxdbilIO.setChdrcoy(chdrpf.getChdrcoy());
		taxdbilIO.setChdrnum(chdrpf.getChdrnum());
		taxdbilIO.setInstfrom(linsrnlIO.getInstfrom());
		taxdbilIO.setLife(SPACES);
		taxdbilIO.setCoverage(SPACES);
		taxdbilIO.setRider(SPACES);
		taxdbilIO.setTrantype(SPACES);
		taxdbilIO.setPlansfx(ZERO);
		taxdbilIO.setFunction(Varcom.begn);
		taxdbilIO.setFormat(formatsInner.taxdbilrec);
		while (!(isEQ(taxdbilIO.getStatuz(), Varcom.endp))) {
			b800ProcessTax();
		}

		/* B700-EXIT */
	}

	protected void b800ProcessTax() {

		/* Read TAXD records */
		SmartFileCode.execute(appVars, taxdbilIO);
		if (isNE(taxdbilIO.getStatuz(), Varcom.oK) && isNE(taxdbilIO.getStatuz(), Varcom.endp)) {
			syserrrec.statuz.set(taxdbilIO.getStatuz());
			syserrrec.params.set(taxdbilIO.getParams());
			xxxxFatalError();
		}
		if (isNE(taxdbilIO.getChdrcoy(), chdrpf.getChdrcoy()) || isNE(taxdbilIO.getChdrnum(), chdrpf.getChdrnum())
				|| isEQ(taxdbilIO.getStatuz(), Varcom.endp)) {
			taxdbilIO.setStatuz(Varcom.endp);
			return;
		}
		if (isNE(taxdbilIO.getChdrcoy(), chdrpf.getChdrcoy()) || isNE(taxdbilIO.getChdrnum(), chdrpf.getChdrnum())
				|| isNE(taxdbilIO.getEffdate(), linsrnlIO.getInstfrom()) || isNE(taxdbilIO.getPostflg(), SPACES)) {
			taxdbilIO.setFunction(Varcom.nextr);
			return;
		}
		/* Read table TR52E */
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tr52e);
		itdmIO.setItemitem(subString(taxdbilIO.getTranref(), 1, 8));
		itdmIO.setItmfrm(taxdbilIO.getEffdate());
		itdmIO.setFunction(Varcom.begn);
		wsaaItdmkey.set(itdmIO.getDataKey());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK) && isNE(itdmIO.getStatuz(), Varcom.endp)
				|| isNE(subString(taxdbilIO.getTranref(), 1, 8), itdmIO.getItemitem())) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(), Varcom.endp) || isNE(itdmIO.getItemtabl(), tr52e)
				|| isNE(subString(taxdbilIO.getTranref(), 1, 8), itdmIO.getItemitem())) {
			itdmIO.setDataKey(wsaaItdmkey);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f109);
			xxxxFatalError();
		}
		tr52erec.tr52eRec.set(itdmIO.getGenarea());
		/* Call tax subroutine */
		txcalcrec.function.set("POST");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(taxdbilIO.getChdrcoy());
		txcalcrec.chdrnum.set(taxdbilIO.getChdrnum());
		txcalcrec.life.set(taxdbilIO.getLife());
		txcalcrec.coverage.set(taxdbilIO.getCoverage());
		txcalcrec.rider.set(taxdbilIO.getRider());
		txcalcrec.planSuffix.set(taxdbilIO.getPlansfx());
		if (isNE(taxdbilIO.getCoverage(), SPACES)) {
			covrmjaIO.setRecKeyData(SPACES);
			covrmjaIO.setChdrcoy(chdrpf.getChdrcoy());
			covrmjaIO.setChdrnum(chdrpf.getChdrnum());
			covrmjaIO.setLife(taxdbilIO.getLife());
			covrmjaIO.setCoverage(taxdbilIO.getCoverage());
			covrmjaIO.setRider(taxdbilIO.getRider());
			covrmjaIO.setPlanSuffix(taxdbilIO.getPlansfx());
			covrmjaIO.setFunction(Varcom.readr);
			covrmjaIO.setStatuz(Varcom.oK);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), Varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				xxxxFatalError();
			}
			txcalcrec.crtable.set(covrmjaIO.getCrtable());
			txcalcrec.cntTaxInd.set("N");
		} else {
			txcalcrec.crtable.set(SPACES);
			txcalcrec.cntTaxInd.set("Y");
		}
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.taxrule.set(subString(taxdbilIO.getTranref(), 1, 8));
		txcalcrec.rateItem.set(subString(taxdbilIO.getTranref(), 9, 8));
		txcalcrec.amountIn.set(taxdbilIO.getBaseamt());
		txcalcrec.effdate.set(taxdbilIO.getEffdate());
		txcalcrec.taxType[1].set(taxdbilIO.getTxtype01());
		txcalcrec.taxType[2].set(taxdbilIO.getTxtype02());
		txcalcrec.taxAmt[1].set(taxdbilIO.getTaxamt01());
		txcalcrec.taxAmt[2].set(taxdbilIO.getTaxamt02());
		txcalcrec.taxAbsorb[1].set(taxdbilIO.getTxabsind01());
		txcalcrec.taxAbsorb[2].set(taxdbilIO.getTxabsind02());
		txcalcrec.batckey.set(atmodrec.batchKey);
		txcalcrec.language.set(atmodrec.language);
		txcalcrec.jrnseq.set(wsaaJrnseq);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		txcalcrec.tranno.set(taxdbilIO.getTranno());
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			xxxxFatalError();
		}
		wsaaJrnseq.set(txcalcrec.jrnseq);
		/* Update TAXD record */
		/* IF TXCL-STATUZ NOT = O-K <LA4758> */
		/* MOVE TXCL-LINK-REC TO SYSR-PARAMS <LA4758> */
		/* MOVE TXCL-STATUZ TO SYSR-STATUZ <LA4758> */
		/* PERFORM XXXX-FATAL-ERROR <LA4758> */
		/* END-IF. <LA4758> */
		taxdIO.setRrn(taxdbilIO.getRrn());
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(Varcom.readd);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), Varcom.oK)) {
			syserrrec.statuz.set(taxdIO.getStatuz());
			syserrrec.params.set(taxdIO.getParams());
			xxxxFatalError();
		}
		taxdIO.setPostflg("P");
		taxdIO.setFunction(Varcom.writd);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), Varcom.oK)) {
			syserrrec.statuz.set(taxdIO.getStatuz());
			syserrrec.params.set(taxdIO.getParams());
			xxxxFatalError();
		}
		/* Read next TAXD */
		taxdbilIO.setFunction(Varcom.nextr);
	}

	protected void c000CallRounding() {
		/* C100-CALL */
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(atmodrec.company);
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.currency.set(chdrpf.getBillcurr());
		zrdecplrec.batctrcde.set(atrtTransCode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			xxxxFatalError();
		}
		/* C900-EXIT */
	}
	
	protected void produceBextRecord(){
		Mandpf mandpf = mandpfDAO.searchMandpfRecordData(clrfpf.getClntcoy(),clrfpf.getClntnum(),payrpf.getMandref(),"VM1DTA.MANDPF");
		if(mandpf!=null) {
			Clbapf clbapf = clbapfDAO.searchClbapfRecordData(mandpf.getBankkey(), mandpf.getBankacckey(), 
					chdrpf.getCowncoy().toString(), chdrpf.getCownnum(),chdrpf.getCownpfx());
			billreqrec.bankkey.set(mandpf.getBankkey());
			billreqrec.bankacckey.set(mandpf.getBankacckey());
			billreqrec.facthous.set(clbapf.getFacthous());
			billreqrec.mandstat.set(mandpf.getMandstat());
		} else {
			billreqrec.bankkey.set(SPACES);
			billreqrec.bankacckey.set(SPACES);
			billreqrec.facthous.set(SPACES);
			billreqrec.mandstat.set(SPACES);
		}
		callBillreq();
	}
	
	protected void callBillreq(){
		/* Search the T3629 array to obtain the required bank code.*/
		readT3629();
		billreqrec.bankcode.set(strT3629BankCode);
		billreqrec.user.set(0);
		billreqrec.contot01.set(0);
		billreqrec.contot02.set(0);
		billreqrec.instjctl.set(SPACES);
		billreqrec.payflag.set(SPACES);
		billreqrec.bilflag.set(SPACES);
		billreqrec.outflag.set(SPACES);
		billreqrec.supflag.set("N");
		billreqrec.company.set(wsaaCompany);
		billreqrec.branch.set(atrtBranch);
		billreqrec.language.set(atmodrec.language);
		billreqrec.effdate.set(wsaaUnitEffdate);
		billreqrec.acctyear.set(atrtAcctYear);
		billreqrec.acctmonth.set(atrtAcctMonth);
		billreqrec.trancode.set(atrtTransCode);
		billreqrec.batch.set(atrtBatch);
		billreqrec.fsuco.set(wsaaFsuCoy);
		billreqrec.modeInd.set("BATCH");
		billreqrec.termid.set(SPACES);
		billreqrec.time.set(ZERO);
		billreqrec.date_var.set(ZERO);
		billreqrec.tranno.set(wsaaTranno);
		billreqrec.chdrpfx.set(chdrpf.getChdrpfx());
		billreqrec.chdrcoy.set(payrpf.getChdrcoy());
		billreqrec.chdrnum.set(payrpf.getChdrnum());
		billreqrec.servunit.set(chdrpf.getServunit());
		billreqrec.cnttype.set(chdrpf.getCnttype());
		billreqrec.occdate.set(chdrpf.getOccdate());
		billreqrec.ccdate.set(chdrpf.getCcdate());
		billreqrec.instcchnl.set(chdrpf.getCollchnl());
		billreqrec.cownpfx.set(chdrpf.getCownpfx());
		billreqrec.cowncoy.set(chdrpf.getCowncoy());
		billreqrec.cownnum.set(chdrpf.getCownnum());
		billreqrec.cntbranch.set(chdrpf.getCntbranch());
		billreqrec.agntpfx.set(chdrpf.getAgntpfx());
		billreqrec.agntcoy.set(chdrpf.getAgntcoy());
		billreqrec.agntnum.set(chdrpf.getAgntnum());
		billreqrec.cntcurr.set(payrpf.getCntcurr());
		billreqrec.billcurr.set(payrpf.getBillcurr());
		billreqrec.ptdate.set(payrpf.getPtdate());
		billreqrec.instto.set(wsaaAdvptdat);
		billreqrec.instbchnl.set(chdrpf.getBillchnl());
		billreqrec.billchnl.set(chdrpf.getBillchnl());
		billreqrec.instfreq.set(payrpf.getBillfreq());//Need to check
		billreqrec.grpscoy.set(payrpf.getGrupcoy());
		billreqrec.grpsnum.set(payrpf.getGrupnum());
		billreqrec.membsel.set(payrpf.getMembsel());
		billreqrec.mandref.set(payrpf.getMandref());
		billreqrec.nextdate.set(payrpf.getNextdate());
		billreqrec.payrpfx.set(clrrpf.getClntpfx());
		billreqrec.payrcoy.set(clrrpf.getClntcoy());
		billreqrec.payrnum.set(clrrpf.getClntnum());
		
		billreqrec.instfrom.set(wsaaBtdate);
		billreqrec.btdate.set(wsaaBtdate);
		billreqrec.duedate.set(wsaaBtdate);
		billreqrec.billcd.set(wsaaBillcd);
		billreqrec.billdate.set(datcon1rec.intDate);
		billreqrec.nextdate.set(wsaaNextdt);
		
		billreqrec.sacscode01.set(t5645rec.sacscode01);
		billreqrec.sacstype01.set(t5645rec.sacstype01);
		billreqrec.glmap01.set(t5645rec.glmap01);
		billreqrec.glsign01.set(t5645rec.sign01);
		
		billreqrec.sacscode02.set(t5645rec.sacscode02);
		billreqrec.sacstype02.set(t5645rec.sacstype02);
		billreqrec.glmap02.set(t5645rec.glmap02);
		billreqrec.glsign02.set(t5645rec.sign02);
		/* If BILLREQ1 needs to know the PAYER NAME then it will be looked*/
		/* up using the PAYRPFX*/
		billreqrec.payername.set(SPACES);
		
		if (isNE(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			currNotEqualBillcurr(linsrnlIO.getInstamt01());
			billreqrec.instamt01.set(conlinkrec.amountOut);
			currNotEqualBillcurr(linsrnlIO.getInstamt02());
			billreqrec.instamt02.set(conlinkrec.amountOut);
			currNotEqualBillcurr(linsrnlIO.getInstamt03());
			billreqrec.instamt03.set(conlinkrec.amountOut);
			currNotEqualBillcurr(linsrnlIO.getInstamt04());
			billreqrec.instamt04.set(conlinkrec.amountOut);
			currNotEqualBillcurr(linsrnlIO.getInstamt05());
			billreqrec.instamt05.set(conlinkrec.amountOut);
		} else {
			billreqrec.instamt01.set(linsrnlIO.getInstamt01());
			billreqrec.instamt02.set(linsrnlIO.getInstamt02());
			billreqrec.instamt03.set(linsrnlIO.getInstamt03());
			billreqrec.instamt04.set(linsrnlIO.getInstamt04());
			billreqrec.instamt05.set(linsrnlIO.getInstamt05());
		}
		billreqrec.instamt06.set(linsrnlIO.getInstamt06());
		billreqrec.instamt07.set(ZERO);
		billreqrec.instamt08.set(ZERO);
		billreqrec.instamt09.set(ZERO);
		billreqrec.instamt10.set(ZERO);
		billreqrec.instamt11.set(ZERO);
		billreqrec.instamt12.set(ZERO);
		billreqrec.instamt13.set(ZERO);
		billreqrec.instamt14.set(ZERO);
		billreqrec.instamt15.set(ZERO);
		
		callProgram(Billreq1.class, billreqrec.billreqRec);
		if (isNE(billreqrec.statuz, Varcom.oK)) {
			syserrrec.params.set(billreqrec.billreqRec);
			syserrrec.statuz.set(billreqrec.statuz);
			xxxxFatalError();
		}
	}
	
	protected void currNotEqualBillcurr(PackedDecimalData instamt) {
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.currIn.set(payrpf.getCntcurr());
		conlinkrec.currOut.set(payrpf.getBillcurr());
		conlinkrec.amountIn.set(instamt);
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		conlinkrec.cashdate.set(datcon1rec.intDate);
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(wsaaCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			conlinkrec.amountOut.set(0);
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		c000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}
	
	protected void readT3629() {
		String keyItemitem = payrpf.getBillcurr().trim();
		boolean itemFound = false;
		Map<String, List<Itempf>> t3629ListMap = itemDao.loadSmartTable(IT, wsaaCompany.toString(), t3629);
		if (t3629ListMap.containsKey(keyItemitem)){	
			List<Itempf> itempfList = t3629ListMap.get(keyItemitem);
			Iterator<Itempf> itr = itempfList.iterator();
			String strEffDate = datcon1rec.intDate.toString();
			while (itr.hasNext()) {
				itempf = new Itempf();
				itempf = itr.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						strT3629BankCode  = t3629rec.bankcode.toString();
						itemFound = true;
					}
				}else{
					t3629rec.t3629Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					strT3629BankCode  = t3629rec.bankcode.toString();
					itemFound = true;					
				}				
			}		
		}	
		if (!itemFound) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(t3629);
			stringVariable1.addExpression(payrpf.getBillcurr());
			stringVariable1.setStringInto(syserrrec.params);
			xxxxFatalError();		
		}
	}
	
	// 1834 changes start
	protected void updateBextpf() {
		bextpf.setRdocnum(alocnorec.alocNo.toString());
		bextpfDAO.updateBextpfRdocnum(bextpf);
	}
	
	protected void createReceipts() {
		/*
		 1. get bext where tranno
		 2. postcashtosuspense() -
 			a) generate receipt 
				i) generate aloc no
				ii) create rtrn pstw
 
		3. summary processing
			i) add rtrn npstw
			ii) add rbnk 
		*/
		
		readBextpf();
		postCashToSuspense();
		summaryProcessing();
	}

	protected void readBextpf() {
		 bextpf= bextpfDAO.getMapOfBextpfByTranno(payrpf.getChdrnum(),payrpf.getChdrcoy(),wsaaTranno.toInt());
	}
	
	protected void postCashToSuspense() {

		/*    Using the BEXT Bankcode find the currency code associated*/
		/*    with this bankcode. The vast majority of instalations will*/
		/*    only have currency per Factoring House. However as banking*/
		/*    systems improve this could quite easily change.*/
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3688Entries);
		as1.setIndices(iy);
		as1.addSearchKey(wsaaT3688Key, bextpf.getBankcode(), true);
		if (as1.binarySearch()) {
			wsaaContot.set(wsaaT3688Cdcontot[iy.toInt()]);
		}
		else {
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(wsaaCompany);
			itemIO.setItemtabl(t3688);
			itemIO.setItemitem(bextpf.getBankcode());
			itemIO.setFunction(Varcom.readr);
			itemIO.setStatuz(nfnd);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		/*   The BEXT records amounts are in billing currency. Thus we need*/
		/*    to find the accounting currency when we post the ACMVs and*/
		/*    RTRNs.*/
		wsaaT3629Test.set(wsaaT3688Currcode[iy.toInt()]);
		ArraySearch as2 = ArraySearch.getInstance(wsaaT3629Entries);
		as2.setIndices(iw);
		as2.addSearchKey(wsaaT3629Key, wsaaT3629Test, true);
		if (as2.binarySearch()) {
			t3629rec.t3629Rec.set(wsaaT3629Genarea[iw.toInt()]);
		}
		else {
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(wsaaCompany);
			itemIO.setItemtabl(t3629);
			itemIO.setItemitem(wsaaT3629Test);
			itemIO.setFunction(Varcom.readr);
			itemIO.setStatuz(nfnd);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		
		alocateNo();
		createPolicyRtrn();
	}
	
	protected void alocateNo() {
		/* Allocate next automatic Receipt Number.                         */
		alocnorec.function.set("NEXT ");
		alocnorec.prefix.set("CA");
		/* MOVE LAST-BANKCODE          TO ALNO-GENKEY.         <PAX908> 
		  MOVE WSAA-T3688-BBALPFX (IY)   TO ALNO-GENKEY.       <FA1491>
		---SEARCH THE ARRAY FOR THE CORRECT GENKEY  */                     
			ArraySearch as1 = ArraySearch.getInstance(wsaaT3688Entries);
			as1.setIndices(iy);
			as1.addSearchKey(wsaaT3688Key, bextpf.getBankcode(), true);
			if (as1.binarySearch()) {
				alocnorec.genkey.set(wsaaT3688Bbalpfx[iy.toInt()]);
				if (isEQ(alocnorec.genkey, SPACES)) {
					alocnorec.genkey.set(bextpf.getBankcode());
				}
			}
			else {
				itemIO.setItempfx(IT);
				itemIO.setItemcoy(wsaaCompany);
				itemIO.setItemtabl(t3688);
				itemIO.setItemitem(bextpf.getBankcode());
				itemIO.setFunction(Varcom.readr);
				itemIO.setStatuz(nfnd);
				syserrrec.params.set(itemIO.getParams());
				xxxxFatalError();
			}
		alocnorec.company.set(wsaaCompany);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isNE(alocnorec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(alocnorec.statuz);
			xxxxFatalError();
		}
	}
	
	
	
	protected void createPolicyRtrn() {
		initialize(addrtrnrec.addrtrnRec);
		addrtrnrec.function.set("PSTW");
		addrtrnrec.mode.set("BATCH");
		addrtrnrec.tranno.set(ZERO);
		addrtrnrec.genlpfx.set("GE");
		addrtrnrec.batckey.set(atmodrec.batchKey);
		addrtrnrec.trandesc.set(descIO.getLongdesc());
		wsaaJrnseqRtrn.add(1); //initialize to 0
		addrtrnrec.transeq.set(wsaaTranseq);
		addrtrnrec.rdocpfx.set("CA");
		addrtrnrec.rdoccoy.set(wsaaCompany);
		addrtrnrec.rdocnum.set(alocnorec.alocNo);
		addrtrnrec.rldgpfx.set(bextpf.getChdrpfx());
		addrtrnrec.rldgcoy.set(bextpf.getChdrcoy());
		addrtrnrec.rldgacct.set(bextpf.getChdrnum());
		addrtrnrec.sacscode.set(bextpf.getSacscode());
		addrtrnrec.sacstyp.set(bextpf.getSacstyp());
		addrtrnrec.effdate.set(datcon1rec.intDate);
		addrtrnrec.chdrpfx.set(bextpf.getChdrpfx());
		addrtrnrec.chdrcoy.set(bextpf.getChdrcoy());
		addrtrnrec.chdrnum.set(bextpf.getChdrnum());
		addrtrnrec.contot.set(wsaaContot);
		addrtrnrec.origccy.set(wsaaT3688Currcode[iy.toInt()]);
		addrtrnrec.origamt.set(bextpf.getInstamt06());
		if (isEQ(wsaaT3688Sign01[iy.toInt()], "+")) {
			addrtrnrec.glsign.set("-");
		}
		else {
			addrtrnrec.glsign.set("+");
		}
		addrtrnrec.bankcode.set(bextpf.getBankcode());
		addrtrnrec.genlcoy.set(bextpf.getChdrcoy());
		addrtrnrec.genlcur.set(t3629rec.ledgcurr);
		addrtrnrec.glcode.set(bextpf.getGlmap());
		/* REMOVE THIS CHECK -                                             */
		/* Check to see if either currency is Euro Compliant and the       */
		/* amount needs to be converted using VIAEURO instead of the       */
		/* rate on T3629.                                                  */
		/*    IF  ADDR-ORIGCCY            = ADDR-GENLCUR                   */
		/*        MOVE 1                  TO ADDR-CURRRATE                 */
		/*        MOVE ADDR-ORIGAMT       TO ADDR-ACCTAMT                  */
		/*    ELSE                                                         */
		/*        MOVE SPACES           TO CLNK-CLNK002-REC                */
		/*        MOVE 'LEDG'           TO CLNK-FUNCTION                   */
		/*        MOVE ADDR-ORIGCCY     TO CLNK-CURR-IN                    */
		/*        MOVE ADDR-GENLCUR     TO CLNK-CURR-OUT                   */
		/*        MOVE ADDR-ORIGAMT     TO CLNK-AMOUNT-IN                  */
		/*        MOVE ADDR-EFFDATE     TO CLNK-CASHDATE                   */
		/*        MOVE BSPR-COMPANY     TO CLNK-COMPANY                    */
		/*        CALL 'LEDCVRT'        USING CLNK-CLNK002-REC             */
		/*        IF CLNK-STATUZ     NOT = O-K                             */
		/*            AND             NOT = 'NECM'                         */
		/*             MOVE CLNK-STATUZ TO SYSR-STATUZ                     */
		/*             PERFORM 600-FATAL-ERROR                             */
		/*        END-IF                                                   */
		/*        IF CLNK-STATUZ         = O-K                             */
		/*            MOVE CLNK-RATE-USED TO ADDR-CURRRATE                 */
		/*            MOVE CLNK-AMOUNT-OUT TO ADDR-ACCTAMT                 */
		/*        ELSE                                                     */
		/*            MOVE ADDR-ORIGCCY TO WSAA-T3629-TEST                 */
		/*            PERFORM 6000-GET-FOREX-RATE                          */
		/*            MOVE WSAA-NOMINAL-RATE TO ADDR-CURRRATE              */
		/*            COMPUTE ADDR-ACCTAMT ROUNDED =                       */
		/*                  ADDR-ORIGAMT * ADDR-CURRRATE                   */
		/*        END-IF                                                   */
		/*    END-IF.                                                      */
		/* REVERT THE OLD LOGIC                                            */
		if (isEQ(addrtrnrec.origccy, addrtrnrec.genlcur)) {
			addrtrnrec.currrate.set(1);
			addrtrnrec.acctamt.set(addrtrnrec.origamt);
		}
		else {
			wsaaT3629Test.set(addrtrnrec.origccy);
			getForexRate6000();
			addrtrnrec.currrate.set(wsaaNominalRate);
			compute(addrtrnrec.acctamt, 10).setRounded(mult(addrtrnrec.origamt, addrtrnrec.currrate));
		}
		addrtrnrec.accpfx.set(bextpf.getAgntpfx());
		addrtrnrec.acccoy.set(bextpf.getAgntcoy());
		addrtrnrec.accno.set(bextpf.getAgntnum());
		readAgnt6500();
		
		addrtrnrec.chdrstcda.set(SPACES);
		addrtrnrec.chdrstcdb.set(SPACES);
		addrtrnrec.chdrstcdc.set(SPACES);
		addrtrnrec.chdrstcdd.set(SPACES);
		addrtrnrec.chdrstcde.set(SPACES);
			
		callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
		if (isNE(addrtrnrec.statuz, Varcom.oK)) {
			syserrrec.params.set(addrtrnrec.addrtrnRec);
			syserrrec.statuz.set(addrtrnrec.statuz);
			xxxxFatalError();
		}
	}
	
	protected void getForexRate6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case reRead6010: 
					reRead6010();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void reRead6010()
	{
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3629Entries);
		as1.setIndices(iw);
		as1.addSearchKey(wsaaT3629Key, wsaaT3629Test, true);
		if (as1.binarySearch()) {
			/*NEXT_SENTENCE*/
		}
		else {
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(wsaaCompany);
			itemIO.setItemtabl(t3629);
			itemIO.setItemitem(wsaaT3629Test);
			itemIO.setFunction(Varcom.readr);
			itemIO.setStatuz(nfnd);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3629rec.t3629Rec.set(wsaaT3629Genarea[iw.toInt()]);
		wsaaNominalRate.set(0);
		for (iu.set(1); !(isGT(iu, 7)
		|| isNE(wsaaNominalRate, ZERO)); iu.add(1)){
			if (isGTE(addrtrnrec.effdate, t3629rec.frmdate[iu.toInt()])
			&& isLTE(addrtrnrec.effdate, t3629rec.todate[iu.toInt()])) {
				wsaaNominalRate.set(t3629rec.scrate[iu.toInt()]);
			}
		}
		if (isEQ(wsaaNominalRate, ZERO)) {
			wsaaT3629Test.set(t3629rec.contitem);
			goTo(GotoLabel.reRead6010);
		}
	}
	
	protected void readAgnt6500()
	{
		para6500();
	}

	protected void para6500()
	{
		agntIO.setDataArea(SPACES);
		agntIO.setAgntpfx(bextpf.getAgntpfx());
		agntIO.setAgntcoy(bextpf.getAgntcoy());
		agntIO.setAgntnum(bextpf.getAgntnum());
		agntIO.setFormat(formatsInner.agntrec);
		agntIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), Varcom.oK)
		&& isNE(agntIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(agntIO.getStatuz(), Varcom.oK)) {
			addrtrnrec.acctyp.set(agntIO.getAgtype());
		}
		else {
			addrtrnrec.acctyp.set(SPACES);
		}
	}
	
	protected void summaryProcessing() {
		getT36885080();
		getT36295090();
		
		writeRtrn();
		writeRbnk5400();
	}
	
	protected void getT36885080()
	{
		t36885080();
	}

protected void t36885080()
	{
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3688Entries);
		as1.setIndices(iy);
		as1.addSearchKey(wsaaT3688Key, bextpf.getBankcode(), true);
		if (as1.binarySearch()) {
			/*NEXT_SENTENCE*/
		}
		else {
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(wsaaCompany);
			itemIO.setItemtabl(t3688);
			itemIO.setItemitem(bextpf.getBankcode());
			itemIO.setFunction(Varcom.readr);
			itemIO.setStatuz(nfnd);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
	}

	protected void getT36295090()
	{
		t36295090();
	}

	protected void t36295090()
	{
		wsaaT3629Test.set(wsaaT3688Currcode[iy.toInt()]);
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3629Entries);
		as1.setIndices(iw);
		as1.addSearchKey(wsaaT3629Key, wsaaT3629Test, true);
		if (as1.binarySearch()) {
			t3629rec.t3629Rec.set(wsaaT3629Genarea[iw.toInt()]);
		}
		else {
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(wsaaCompany);
			itemIO.setItemtabl(t3629);
			itemIO.setItemitem(wsaaT3629Test);
			itemIO.setFunction(Varcom.readr);
			itemIO.setStatuz(nfnd);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
	}
	
	protected void writeRtrn()
	{
		
		/* This paragraph creates an RTRN record. The RTRN is effectively*/
		/* the same as the RTRN/ACMV header created from an online cash*/
		/* receipt. The header is the summary of all dissections which*/
		/* have been created as each BEXT has been processed in the 3000*/
		/* section.*/
		initialize(addrtrnrec.addrtrnRec);
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3688Entries);
		as1.setIndices(iy);
		as1.addSearchKey(wsaaT3688Key, bextpf.getBankcode(), true);
		if (as1.binarySearch()) {
			/*NEXT_SENTENCE*/
		}
		else {
			itemIO.setItempfx(IT);
			itemIO.setItemcoy(wsaaCompany);
			itemIO.setItemtabl(t3688);
			itemIO.setItemitem(bextpf.getBankcode());
			itemIO.setFunction(Varcom.readr);
			itemIO.setStatuz(nfnd);
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		addrtrnrec.tranno.set(0);
		addrtrnrec.transeq.set(ZERO);
		addrtrnrec.sacstyp.set("3");
		addrtrnrec.batckey.set(atmodrec.batchKey);
		/* MOVE DESC-LONGDESC          TO ADDR-TRANDESC.                */
		addrtrnrec.effdate.set(datcon1rec.intDate);
		addrtrnrec.chdrnum.set(bextpf.getChdrnum());
		addrtrnrec.chdrpfx.set(bextpf.getChdrpfx());
		addrtrnrec.chdrcoy.set(bextpf.getChdrcoy());
		addrtrnrec.bankcode.set(bextpf.getBankcode());
		addrtrnrec.origamt.set(bextpf.getInstamt06());
		addrtrnrec.origccy.set(wsaaT3688Currcode[iy.toInt()]);
		addrtrnrec.glsign.set(wsaaT3688Sign01[iy.toInt()]);
		addrtrnrec.contot.set(wsaaT3688Cdcontot[iy.toInt()]);
		
		addrtrnrec.rldgacct.set(bextpf.getPayrnum());
		
		addrtrnrec.rldgcoy.set(bextpf.getPayrcoy());
		addrtrnrec.rldgpfx.set("CN");
		addrtrnrec.sacscode.set("CN");
		/* get payee name                                                  */
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(addrtrnrec.sacscode);
		namadrsrec.clntCompany.set(wsaaFsuCoy);
		namadrsrec.clntNumber.set(addrtrnrec.rldgacct);
		namadrsrec.language.set(atmodrec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, Varcom.oK)
		&& isNE(namadrsrec.statuz, Varcom.mrnf)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			xxxxFatalError();
		}
		if (isNE(namadrsrec.statuz, Varcom.oK)) {
			addrtrnrec.trandesc.fill("?");
		}
		else {
			addrtrnrec.trandesc.set(namadrsrec.name);
			dbcstrcpy2.dbcsInputString.set(namadrsrec.name);
			dbcstrcpy2.dbcsOutputLength.set(30);
			a4000CallDbcstrnc();
			addrtrnrec.trandesc.set(dbcstrcpy2.dbcsOutputString);
		}
		/* Allocate next automatic Receipt Number.*/
		/* MOVE 'NEXT '                TO ALNO-FUNCTION.                */
		/* MOVE 'CA'                   TO ALNO-PREFIX.                  */
		/* MOVE LAST-BANKCODE          TO ALNO-GENKEY.                  */
		/* MOVE BPRD-COMPANY           TO ALNO-COMPANY.                 */
		/* CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                         */
		/* IF ALNO-STATUZ              NOT = O-K                        */
		/*    MOVE ALNO-STATUZ         TO SYSR-STATUZ                   */
		/*    PERFORM 600-FATAL-ERROR                                   */
		/* END-IF.                                                      */
		addrtrnrec.rdocnum.set(alocnorec.alocNo);
		addrtrnrec.rdocpfx.set("CA");
		addrtrnrec.rdoccoy.set(wsaaCompany);
		addrtrnrec.genlpfx.set("GE");
		addrtrnrec.genlcoy.set(wsaaCompany);
		addrtrnrec.glcode.set(wsaaT3688Glmap01[iy.toInt()]);
		addrtrnrec.genlcur.set(t3629rec.ledgcurr);
		/* Do not try to convert the original amount into the*/
		/* accounting currency if the two differ.  Simply move the*/
		/* accumulated amount in accounting currency into the ACCTAMT*/
		/* field.*/
		/* REMOVE THE CHECK -                                              */
		/* Check to see if either currency is Euro Compliant and the       */
		/* amount needs to be converted using VIAEURO instead of the       */
		/* rate on T3629.                                                  */
		/*    IF  ADDR-ORIGCCY            = ADDR-GENLCUR                   */
		/*        MOVE 1                  TO ADDR-CURRRATE                 */
		/*        MOVE ADDR-ORIGAMT       TO ADDR-ACCTAMT                  */
		/*    ELSE                                                         */
		/*        MOVE SPACES           TO CLNK-CLNK002-REC                */
		/*        MOVE 'LEDG'           TO CLNK-FUNCTION                   */
		/*        MOVE ADDR-ORIGCCY     TO CLNK-CURR-IN                    */
		/*        MOVE ADDR-GENLCUR     TO CLNK-CURR-OUT                   */
		/*        MOVE LAST-DD-TOTAL-ACCTCUR TO CLNK-AMOUNT-IN             */
		/*        MOVE ADDR-EFFDATE     TO CLNK-CASHDATE                   */
		/*        MOVE BSPR-COMPANY     TO CLNK-COMPANY                    */
		/*        CALL 'LEDCVRT'        USING CLNK-CLNK002-REC             */
		/*        IF CLNK-STATUZ     NOT = O-K                             */
		/*            AND             NOT = 'NECM'                         */
		/*             MOVE CLNK-STATUZ TO SYSR-STATUZ                     */
		/*             PERFORM 600-FATAL-ERROR                             */
		/*        END-IF                                                   */
		/*        IF CLNK-STATUZ         = O-K                             */
		/*            MOVE CLNK-RATE-USED TO ADDR-CURRRATE                 */
		/*            MOVE LAST-DD-TOTAL-ACCTCUR                           */
		/*                              TO ADDR-ACCTAMT                    */
		/*        ELSE                                                     */
		/*            PERFORM 6000-GET-FOREX-RATE                          */
		/*            MOVE WSAA-NOMINAL-RATE TO ADDR-CURRRATE              */
		/*            MOVE LAST-DD-TOTAL-ACCTCUR                           */
		/*                              TO ADDR-ACCTAMT                    */
		/*        END-IF                                                   */
		/*    END-IF.                                                      */
		/* REVERT THE OLD LOGIC*/
		if (isEQ(addrtrnrec.origccy, addrtrnrec.genlcur)) {
			addrtrnrec.currrate.set(1);
			addrtrnrec.acctamt.set(addrtrnrec.origamt);
		}
		else {
			getForexRate6000();
			addrtrnrec.currrate.set(wsaaNominalRate);
			addrtrnrec.acctamt.set(bextpf.getInstamt06());
		}
		addrtrnrec.function.set("NPSTW");
		callProgram(Addrtrn.class, addrtrnrec.addrtrnRec);
		if (isNE(addrtrnrec.statuz, Varcom.oK)) {
			syserrrec.params.set(addrtrnrec.addrtrnRec);
			syserrrec.statuz.set(addrtrnrec.statuz);
			xxxxFatalError();
		}
	}
	
	protected void a4000CallDbcstrnc()
	{
		/*A4100-START*/
		dbcstrcpy2.dbcsStatuz.set(SPACES);
		dbcsTrnc2(dbcstrcpy2.rec);
		if (isNE(dbcstrcpy2.dbcsStatuz, Varcom.oK)) {
			dbcstrcpy2.dbcsOutputString.set(SPACES);
		}
		/*A4900-EXIT*/
	}
	
	protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx(IT);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(atmodrec.language);
		descIO.setFunction(Varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			xxxxFatalError();
		}
	}
	
	protected void setUpHeadingDates1040() //call in initialize
	{
		/*    Load T3688 into working storage for fast access.*/
		itemIO.setItempfx(IT);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(t3688);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(Varcom.begn);
		itemIO.setStatuz(Varcom.oK);
		iy.set(1);
		while ( !(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT36881100();
		}
		
		/*    Load T3629 into working storage for fast access.*/
		itemIO.setItempfx(IT);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(Varcom.begn);
		itemIO.setStatuz(Varcom.oK);
		iw.set(1);
		while ( !(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT36291200();
		}
		
		/*    Load T3678 into working storage for fast access.*/
		itemIO.setItempfx(IT);
		itemIO.setItemcoy(wsaaFsuCoy);
		itemIO.setItemtabl(t3678);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(Varcom.begn);
		itemIO.setStatuz(Varcom.oK);
		iz.set(1);
		while ( !(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT36781300();
		}
		
	}
	
	protected void loadT36881100()
	{
		loadT36881110();
	}

protected void loadT36881110()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		itemIO.setFunction(Varcom.nextr);
		if (isNE(itemIO.getItemcoy(), wsaaCompany)
		|| isNE(itemIO.getItemtabl(), t3688)
		|| isEQ(itemIO.getStatuz(), Varcom.endp)) {
			itemIO.setStatuz(Varcom.endp);
			return ;
		}
		/* IF IY > 25                                                   */
		if (isGT(iy, 1000)) { //ILIFE-2628 fixed--Array size increased from 100
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t3688);
			xxxxFatalError();
		}
		t3688rec.t3688Rec.set(itemIO.getGenarea());
		wsaaT3688Bankcode[iy.toInt()].set(itemIO.getItemitem());
		
		wsaaT3688Glmap01[iy.toInt()].set(t3688rec.glmap01);
		wsaaT3688Sign01[iy.toInt()].set(t3688rec.sign01);
		
		/*    MOVE T3688-GLMAP-01         TO WSAA-T3688-GLMAP-01   (IY).   */
		/*    MOVE T3688-SIGN-01          TO WSAA-T3688-SIGN-01    (IY).   */
		/* MOVE T3688-CURRCODE         TO WSAA-T3688-CURRCODE   (IY).   */
		wsaaT3688Currcode[iy.toInt()].set(t3688rec.bcurr);
		wsaaT3688Cdcontot[iy.toInt()].set(t3688rec.cdcontot);
		wsaaT3688Bankacckey[iy.toInt()].set(t3688rec.bankacckey);
		wsaaT3688Bbalpfx[iy.toInt()].set(t3688rec.bbalpfx);
		iy.add(1);
	}

protected void loadT36291200()
	{
		loadT36291210();
	}

protected void loadT36291210()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		itemIO.setFunction(Varcom.nextr);
		if (isNE(itemIO.getItemcoy(), wsaaCompany)
		|| isNE(itemIO.getItemtabl(), t3629)
		|| isEQ(itemIO.getStatuz(), Varcom.endp)) {
			itemIO.setStatuz(Varcom.endp);
			return ;
		}
		/* IF IW > 150                                                  */
		if (isGT(iw, 1500)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t3629);
			xxxxFatalError();
		}
		wsaaT3629Key[iw.toInt()].set(itemIO.getItemitem());
		wsaaT3629Genarea[iw.toInt()].set(itemIO.getGenarea());
		iw.add(1);
	}

protected void loadT36781300()
	{
		loadT36781310();
	}

protected void loadT36781310()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		itemIO.setFunction(Varcom.nextr);
		if (isNE(itemIO.getItemcoy(), wsaaFsuCoy)
		|| isNE(itemIO.getItemtabl(), t3678)
		|| isEQ(itemIO.getStatuz(), Varcom.endp)) {
			itemIO.setStatuz(Varcom.endp);
			return ;
		}
		if (isGT(iz, 1000)) { //ILIFE-2628 fixed--Array size increased from 25
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t3678);
			xxxxFatalError();
		}
		t3678rec.t3678Rec.set(itemIO.getGenarea());
		wsaaT3678Mandstat[iz.toInt()].set(itemIO.getItemitem());
		wsaaT3678Gonogoflg[iz.toInt()].set(t3678rec.gonogoflg);
		wsaaT3678Bacstrcode[iz.toInt()].set(t3678rec.bacstrcode);
		iz.add(1);
	}
	
protected void writeRbnk5400()
{
	writeRbnkpfRecord();
}

protected void writeRbnkpfRecord() {
	
    Rbnkpf rbnkpf = new Rbnkpf();
	rbnkpf.setCrdtcard(bextpf.getBankacckey());
	rbnkpf.setCrcardmod(SPACE);
	rbnkpf.setCcmid(SPACE);
	rbnkpf.setCctid(SPACE);
	rbnkpf.setMchntid(SPACE);
	rbnkpf.setTrmnlid(SPACE);
	rbnkpf.setCrcardtype(SPACE);
	rbnkpf.setAuthid(SPACE);
	rbnkpf.setCrcname(SPACE);
	rbnkpf.setCrcardexpy(ZERO.intValue());
	rbnkpf.setCrcardexpm(ZERO.intValue());
	rbnkpf.setCrcardchg(BigDecimal.ZERO);
	rbnkpf.setRcptstat(SPACE);
	rbnkpf.setHlrsncd(SPACE);
	rbnkpf.setCnrsncd(SPACE);
	rbnkpf.setPostdteflg(SPACE);
	rbnkpf.setInsrefno(SPACE);
	rbnkpf.setAuthdat(varcom.vrcmMaxDate.toString());
	rbnkpf.setRealdat(varcom.vrcmMaxDate.toString());
	rbnkpf.setDepdate(varcom.vrcmMaxDate.toString());
	rbnkpf.setDishonno(ZERO.intValue());
	rbnkpf.setTrdt(wsaaTransactionDate.toInt());
	rbnkpf.setTrtm(wsaaTransactionTime.toString());
	rbnkpf.setUser_t(wsaaUser.toInt());
	rbnkpf.setCrtdate(wsaaTransactionDate.toString());
	rbnkpf.setSeqnbr(1);
	rbnkpf.setRdocpfx("CA");
	rbnkpf.setRdoccoy(wsaaCompany.toString());
	rbnkpf.setRdocnum(alocnorec.alocNo.toString());
	rbnkpf.setPaytype("3");
	
	
	if (isEQ(tr29srec.appflag, "Y"))
	{
		/*--Deposit Date is set to Business Date                           */
		rbnkpf.setDepdate(wsaaTransactionDate.toString());
		/*--The receipt Status setup in T3676 only applicable for receipt  */
		/*--creation, corresponding transaction must be setup for each     */
		/*--payment mode.                                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaCompany);
		itemIO.setItemtabl(t3676);
		itemIO.setItemitem(rbnkpf.getPaytype());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.oK)) {
			t3676rec.t3676Rec.set(itemIO.getGenarea());
		}
		else {
			t3676rec.t3676Rec.set(SPACES);
		}
		rbnkpf.setRcptstat(SPACE);
		for (ix.set(1); !(isGT(ix, wsaaMaxT3676Transcd)
		|| isNE(rbnkpf.getRcptstat(), SPACES)); ix.add(1)){
			if (isEQ(atrtTransCode, t3676rec.transcd[ix.toInt()])) {
				rbnkpf.setRcptstat(t3676rec.rcptst[ix.toInt()].toString());
			}
		}
	}
	rbnkpf.setDocorigamt(java.math.BigDecimal.valueOf (bextpf.getInstamt06()));
	rbnkpf.setOrigccy(addrtrnrec.origccy.toString());
	rbnkpf.setAcctccy(t3629rec.ledgcurr.toString());
	rbnkpf.setScrate(addrtrnrec.currrate.getbigdata());
	rbnkpf.setDocacctamt(addrtrnrec.acctamt.getbigdata());
	rbnkpf.setZchqtyp(SPACE);
	rbnkpf.setChqnum(SPACE);
	rbnkpf.setBankdesc01(SPACE);
	rbnkpf.setRcptrev(SPACE);
	rbnkpf.setBankkey(SPACE);
	rbnkpf.setTchqdate(varcom.vrcmMaxDate.toString());
	rbnkpf.setUsrprf(rbnkpfDAO.getUsrprf());
	rbnkpf.setJobnm(rbnkpfDAO.getJobnm());
	rbnkpf.setDatime(rbnkpfDAO.getDatime().toString());
	rbnkpfDAO.insert(rbnkpf);
}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		/* FORMATS */
		private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
		private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
		private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
		private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
		private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
		private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
		private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
		private FixedLengthStringData agcmseqrec = new FixedLengthStringData(10).init("AGCMSEQREC");
		private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
		private FixedLengthStringData taxdbilrec = new FixedLengthStringData(10).init("TAXDBILREC");
		private FixedLengthStringData fpcorec = new FixedLengthStringData(10).init("FPCOREC");
		private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
		private FixedLengthStringData agntrec = new FixedLengthStringData(10).init("AGNTREC");
	}

	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {

		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0,
				REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
		private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
		private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
		private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	}

	protected Payrpf populatePayrpf1011() {
		return payrpfDAO.getPayrRecordByCoyAndNumAndSeq(wsaaCompany.toString(), wsaaChdrnum.toString());
	}

	protected void updateLinsForOtherFee() {
		//if any
	}
}
