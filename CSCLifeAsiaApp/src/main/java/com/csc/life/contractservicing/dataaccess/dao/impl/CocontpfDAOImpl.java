package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.CocontpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Cocontpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CocontpfDAOImpl extends BaseDAOImpl<Cocontpf> implements CocontpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(CocontpfDAOImpl.class);
	
	public Cocontpf checkUnProcessed(String chdrcoy,String chdrnum){
		Cocontpf cocont = null;
		PreparedStatement psCoctSelect = null;
		ResultSet rs = null;
		StringBuilder sqlCovrSelectBuilder = new StringBuilder();
		sqlCovrSelectBuilder.append("SELECT * FROM COCONTPF WHERE CHDRCOY = ? AND CHDRNUM = ?");
		psCoctSelect = getPrepareStatement(sqlCovrSelectBuilder.toString());
		try {
			psCoctSelect.setString(1, chdrcoy);
			psCoctSelect.setString(2, chdrnum.trim());
			rs = psCoctSelect.executeQuery();
			
			while(rs.next()) {
				cocont = new Cocontpf();
				cocont.setUniqueNumber(rs.getLong(1));
				cocont.setChdrcoy(rs.getString(2));
				cocont.setChdrnum(rs.getString(3));
				cocont.setDatercvd(rs.getString(4));
				cocont.setCocontamnt(rs.getBigDecimal(5));
				cocont.setLiscamnt(rs.getBigDecimal(6));
				cocont.setPrcdflag(rs.getString(7));
			}
		}
			catch (SQLException e) {
				LOGGER.error("checkUnProcessed()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psCoctSelect, rs);
			}
		
		
		return cocont;
	}
	
	public boolean updateProcessedflag(String chdrcoy,String chdrnum, String procflag) {
		StringBuilder updateSql = new StringBuilder();
		updateSql.append(" UPDATE COCONTPF SET PRCDFLAG=? WHERE CHDRCOY=? AND CHDRNUM = ? ");
		PreparedStatement ps = getPrepareStatement(updateSql.toString());
		boolean isUpdated = false;
		try {
			ps.setString(1, procflag);
			ps.setString(2, chdrcoy);
			ps.setString(3, chdrnum);
			ps.executeUpdate();
			isUpdated = true;
		}
		catch (SQLException e) {
			LOGGER.error("updateCocontpf ProcessedFlag()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		return isUpdated;
	}
	
	public Map<String, Cocontpf> fetchAllCocont(String chdrcoy,String chdrnumFrm, String chdrnumTo) {
		Cocontpf cocont = null;
		Map<String, Cocontpf> cocontListMap = new HashMap<>();
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM COCONTPF ");
		if(!chdrnumFrm.trim().isEmpty() && !chdrnumTo.trim().isEmpty())
			sb.append(" WHERE CHDRNUM BETWEEN ? AND ? ");
		else if(chdrnumFrm.trim().isEmpty() && !chdrnumTo.trim().isEmpty())
			sb.append(" WHERE CHDRNUM <= ? ");
		else if(!chdrnumFrm.trim().isEmpty() && chdrnumTo.trim().isEmpty())
			sb.append(" WHERE CHDRNUM >= ? ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
        try {
            //ps.setString(1, chdrcoy);
            if(!chdrnumFrm.trim().isEmpty() && !chdrnumTo.trim().isEmpty()) {
            	ps.setString(1, chdrnumFrm);
	            ps.setString(2, chdrnumTo);
            }
            else if(chdrnumFrm.trim().isEmpty() && !chdrnumTo.trim().isEmpty()) {
            	ps.setString(1, chdrnumTo);
            }
            else if(!chdrnumFrm.trim().isEmpty() && chdrnumTo.trim().isEmpty()) {
            	ps.setString(1, chdrnumFrm);
            }
            rs = ps.executeQuery();
		
			while(rs.next()) {
				cocont = new Cocontpf();
				cocont.setUniqueNumber(rs.getLong(1));
				cocont.setChdrcoy(rs.getString(2));
				cocont.setChdrnum(rs.getString(3));
				cocont.setDatercvd(rs.getString(4));
				cocont.setCocontamnt(rs.getBigDecimal(5));
				cocont.setLiscamnt(rs.getBigDecimal(6));
				cocont.setPrcdflag(rs.getString(7));
				 if (!cocontListMap.containsKey(cocont.getChdrnum())) {
	                    cocontListMap.put(cocont.getChdrnum(), cocont);
	                }
			}
		}
			catch (SQLException e) {
				LOGGER.error("checkUnProcessed()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);
			}
		
		
		return cocontListMap;
	
        }

}
