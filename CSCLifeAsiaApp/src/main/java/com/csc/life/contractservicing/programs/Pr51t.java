/*
 * File: Pr51t.java
 * Date: December 3, 2013 3:26:08 AM ICT
 * Author: CSC
 * 
 * Class transformed from PR51T.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PrmhenqTableDAM;
import com.csc.life.contractservicing.screens.Sr51tScreenVars;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Premium Holiday Enquiry.
* ========================
*
*  This screen is used to enquire on the Premium Holidy details.
*
*  Premium Holiday Start and End Dates indicate the period the
*  contract's Premium Holiday.
*
*   Req Type indicates the mode the premium holiday is triggered:
*    'C' - Premium Holiday is requested by the client, and process d
*          through the on-line function
*    'A' - Premium Holiday is processed through the batch overdue, wherei
*          the no-forfeiture option is set to Premium Holiday (NF1 )
*
*   Status for premium holiday can be any of the following:
*
*          A - Active Premium Holiday
*          T - Terminated or Completed
*
*
****************************************************************** ****
* </pre>
*/
public class Pr51t extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR51T");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSeqnbr = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaDescFields = new FixedLengthStringData(13);
	private FixedLengthStringData wsaaDescTable = new FixedLengthStringData(5).isAPartOf(wsaaDescFields, 0);
	private FixedLengthStringData wsaaDescItem = new FixedLengthStringData(8).isAPartOf(wsaaDescFields, 5);
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String descrec = "DESCREC";
	private static final String payrrec = "PAYRREC";
	private static final String prmhenqrec = "PRMHENQREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PrmhenqTableDAM prmhenqIO = new PrmhenqTableDAM();
	private Sr51tScreenVars sv = ScreenProgram.getScreenVars( Sr51tScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1280, 
		exit1290
	}

	public Pr51t() {
		super();
		screenVars = sv;
		new ScreenModel("Sr51t", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaSeqnbr.set(0);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		/* Retrieve contract fields from I/O module*/
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		/* Read the PAYR file*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		scrnparams.function.set(varcom.sclr);
		a100CallSr51tio();
		scrnparams.subfileRrn.set(1);
		/*    Set screen fields*/
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.register.set(chdrmjaIO.getRegister());
		sv.cntcurr.set(payrIO.getCntcurr());
		/* Get contract type description from T5688.*/
		wsaaDescFields.set(SPACES);
		wsaaDescTable.set(t5688);
		wsaaDescItem.set(chdrmjaIO.getCnttype());
		a200CallDescio();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		/* Get Premium status description from T3588.*/
		wsaaDescFields.set(SPACES);
		wsaaDescTable.set(t3588);
		wsaaDescItem.set(chdrmjaIO.getPstatcode());
		a200CallDescio();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/* Get Risk status description from T3623.*/
		wsaaDescFields.set(SPACES);
		wsaaDescTable.set(t3623);
		wsaaDescItem.set(chdrmjaIO.getStatcode());
		a200CallDescio();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/* Load data to sub-file.*/
		prmhenqIO.setParams(SPACES);
		prmhenqIO.setStatuz(varcom.oK);
		prmhenqIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		prmhenqIO.setChdrnum(chdrmjaIO.getChdrnum());
		prmhenqIO.setFrmdate(varcom.vrcmMaxDate);
		prmhenqIO.setFormat(prmhenqrec);
		prmhenqIO.setFunction(varcom.begn);
		scrnparams.subfileRrn.set(1);
		while ( !(isEQ(prmhenqIO.getStatuz(), varcom.endp))) {
			loadSubfile1200();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin1210();
				case next1280: 
					next1280();
				case exit1290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1210()
	{
		SmartFileCode.execute(appVars, prmhenqIO);
		if (isNE(prmhenqIO.getStatuz(), varcom.oK)
		&& isNE(prmhenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(prmhenqIO.getParams());
			syserrrec.statuz.set(prmhenqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(prmhenqIO.getStatuz(), varcom.endp)
		|| isNE(prmhenqIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(prmhenqIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			prmhenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		if (isEQ(prmhenqIO.getFrmdate(), varcom.vrcmMaxDate)
		|| isEQ(prmhenqIO.getTodate(), varcom.vrcmMaxDate)) {
			goTo(GotoLabel.next1280);
		}
		sv.frmdate.set(prmhenqIO.getFrmdate());
		sv.todate.set(prmhenqIO.getTodate());
		sv.logtype.set(prmhenqIO.getLogtype());
		sv.apind.set(prmhenqIO.getApind());
		sv.activeInd.set(prmhenqIO.getActiveInd());
		sv.effdate.set(prmhenqIO.getEffdate());
		if (isEQ(prmhenqIO.getCrtuser(), SPACES)) {
			sv.crtuser.set(prmhenqIO.getUserProfile());
		}
		else {
			sv.crtuser.set(prmhenqIO.getCrtuser());
		}
		sv.validflag.set(prmhenqIO.getValidflag());
		wsaaSeqnbr.add(1);
		sv.seqnbr.set(wsaaSeqnbr);
		sv.seqnbr.set(prmhenqIO.getTranno());
		scrnparams.function.set(varcom.sadd);
		a100CallSr51tio();
	}

protected void next1280()
	{
		prmhenqIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a100CallSr51tio()
	{
		/*A101-BEGIN*/
		processScreen("SR51T", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*A109-EXIT*/
	}

protected void a200CallDescio()
	{
		a201Begin();
	}

protected void a201Begin()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaDescTable);
		descIO.setDescitem(wsaaDescItem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
}
