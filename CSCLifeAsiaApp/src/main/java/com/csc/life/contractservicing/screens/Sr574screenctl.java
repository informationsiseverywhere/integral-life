package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr574screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 15, 24, 16, 1, 2, 12, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr574screensfl";
		lrec.subfileClass = Sr574screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 13;
		lrec.pageSubfile = 12;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 9, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr574ScreenVars sv = (Sr574ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr574screenctlWritten, sv.Sr574screensflWritten, av, sv.sr574screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr574ScreenVars screenVars = (Sr574ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.hflag.setClassString("");
	}

/**
 * Clear all the variables in Sr574screenctl
 */
	public static void clear(VarModel pv) {
		Sr574ScreenVars screenVars = (Sr574ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.hflag.clear();
	}
}
