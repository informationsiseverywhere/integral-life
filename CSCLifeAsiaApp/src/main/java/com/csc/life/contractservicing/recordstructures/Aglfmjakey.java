package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:07
 * Description:
 * Copybook name: AGLFMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Aglfmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData aglfmjaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData aglfmjaKey = new FixedLengthStringData(256).isAPartOf(aglfmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData aglfmjaAgntcoy = new FixedLengthStringData(1).isAPartOf(aglfmjaKey, 0);
  	public FixedLengthStringData aglfmjaAgntnum = new FixedLengthStringData(8).isAPartOf(aglfmjaKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(aglfmjaKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(aglfmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		aglfmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}