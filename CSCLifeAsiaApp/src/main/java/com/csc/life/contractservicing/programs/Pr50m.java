/*
 * File: Pr50m.java
 * Date: 30 August 2009 1:32:07
 * Author: Quipoz Limited
 *
 * Class transformed from PR50M.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Covrmjakey;
import com.csc.life.contractservicing.screens.Sr50mScreenVars;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.ZlifelcTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th611rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.recordstructures.Pr676cpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Life Assured Changes
* - Since change of gender, DOB, smoking flag & occupation code will
*   not impact the sum assured of the contract. Hence, no need to write
*   record into INCR (Automatic Increase File).
*
* This screen is for Life Assured change processing. It
* displays the revised premium amount based on the changes
* made on the life assured details screen in SR50K.
* All fields in the screen will be protected, no modification
* will be allowed. Once enter being press, record will be updated.
*
* 1000 Section
* ============
* Retrieve(RETRV) CHDRMJA, PAYR info KEEPS in previous
* screen & populate neccesary fields into screen.
*
* 3000 Section
* ============
* 1.Update CHDR.
* -Release contract info keeps previously.
* -Get(READH) the respective contract from CHDRMJA & update
*  (REWRT) valid flag to 2.
* -Then, add(WRITR) new rec into CHDR with info below:
*  ADD  1                      TO CHDRMJA-TRANNO.
*  MOVE SR50M-PTDATE           TO CHDRMJA-CURRFROM.
*  MOVE VRCM-MAX-DATE          TO CHDRMJA-CURRTO.
*  MOVE SR50M-MOP              TO CHDRMJA-BILLCHNL.
*  MOVE SR50M-BILLFREQ         TO CHDRMJA-BILLFREQ.
*  MOVE SR50M-GRSPRM           TO CHDRMJA-SINSTAMT01.
*  MOVE WSAA-CONT-FEE          TO CHDRMJA-SINSTAMT02.
*  MOVE '1'                    TO CHDRMJA-VALIDFLAG.
*
*  COMPUTE CHDRMJA-SINSTAMT06
*        = CHDRMJA-SINSTAMT01
*        + CHDRMJA-SINSTAMT02
*        + CHDRMJA-SINSTAMT03
*        + CHDRMJA-SINSTAMT04
*        + CHDRMJA-SINSTAMT05
*  END-COMPUTE.
*
* 2.Update PAYR.
* -Release payor info keeps previously.
* -Get(READH) the payor from PAYR & update(REWRT) valid flag to 2.
* -Then, add(WRITR) new rec into PAYR with info below:
*  MOVE CHDRMJA-TRANNO         TO PAYR-TRANNO.
*  MOVE WSAA-TODAY             TO PAYR-TRANSACTION-DATE.
*  MOVE VRCM-USER              TO PAYR-USER.
*  MOVE SR50M-PTDATE           TO PAYR-EFFDATE.
*  MOVE SR50M-MOP              TO PAYR-BILLCHNL.
*  MOVE SR50M-BILLFREQ         TO PAYR-BILLFREQ.
*  MOVE SR50M-GRSPRM           TO PAYR-SINSTAMT01.
*  MOVE WSAA-CONT-FEE          TO PAYR-SINSTAMT02.
*  MOVE '1'                    TO PAYR-VALIDFLAG
*
*  COMPUTE PAYR-SINSTAMT06
*        = PAYR-SINSTAMT01
*        + PAYR-SINSTAMT02
*        + PAYR-SINSTAMT03
*        + PAYR-SINSTAMT04
*        + PAYR-SINSTAMT05
*  END-COMPUTE.
*
* 3.Update COVR & AGCM.
* -Perform looping to Update each COMPONENT.
* -Get(READH) the respective coverage from COVRMJA & update
*  (REWRT) valid flag to 2.
* -Using keys COVRMJA-CHDRCOY, COVRMJA-CHDRNUM, COVRMJA-LIFE
*  to get the respective updated sex code & age nxt birthday at
*  commencement date from LIFE file.
*  MOVE LIFEMJA-ANB-AT-CCD     TO COVRMJA-ANB-AT-CCD.
*  MOVE LIFEMJA-CLTSEX         TO COVRMJA-SEX.
* -Then, add(WRITR) new rec into COVR with info below:
*  MOVE '1'                    TO COVRMJA-VALIDFLAG.
*  MOVE SR50M-PTDATE           TO COVRMJA-CURRFROM.
*  MOVE VRCM-MAX-DATE          TO COVRMJA-CURRTO.
*  MOVE CHDRMJA-TRANNO         TO COVRMJA-TRANNO.
*  MOVE VRCM-USER              TO COVRMJA-USER.
*  MOVE WSAA-TODAY             TO COVRMJA-TRANSACTION-DATE.
*  MOVE SR50M-ZGRSAMT          TO COVRMJA-INSTPREM.
*  MOVE SR50M-BASPRM           TO COVRMJA-ZBINSTPREM.
*  MOVE SR50M-ZLOPRM           TO COVRMJA-ZLINSTPREM.
*  IF SR50M-WAIVERPREM         = 'Y'
*     MOVE SR50M-SUMINS        TO COVRMJA-SUMINS
*  END-IF.
*
* -Get(READH) the contract commission from AGCMBCH & update
*  (REWRT) valid flag to 2.
* -Then, add(WRITR) new rec into AGCM with info below:
*  MOVE SR50M-BILLFREQ         TO WSAA-BILLFREQ.
*  COMPUTE AGCMBCH-ANNPREM
*          = SR50M-ZGRSAMT * WSAA-BILLFREQ9
*  END-COMPUTE.
*
*  MOVE '1'                    TO AGCMBCH-VALIDFLAG.
*  MOVE CHDRMJA-TRANNO         TO AGCMBCH-TRANNO.
*  MOVE SR50M-PTDATE           TO AGCMBCH-CURRFROM.
*  MOVE VRCM-MAX-DATE          TO AGCMBCH-CURRTO.
*  MOVE VRCM-USER              TO AGCMBCH-USER.
*  MOVE WSAA-TODAY             TO AGCMBCH-TRANSACTION-DATE.
*  ACCEPT AGCMBCH-TRANSACTION-TIME FROM TIME.
*
* 4.Update PTRN.
* -Add new txn record with info below:
*  MOVE WSAA-BATCKEY           TO PTRN-DATA-KEY.
*  MOVE CHDRMJA-CHDRPFX        TO PTRN-CHDRPFX.
*  MOVE CHDRMJA-CHDRCOY        TO PTRN-CHDRCOY.
*  MOVE CHDRMJA-RECODE         TO PTRN-RECODE.
*  MOVE CHDRMJA-CHDRNUM        TO PTRN-CHDRNUM.
*  MOVE CHDRMJA-TRANNO         TO PTRN-TRANNO.
*  MOVE SR50M-PTDATE           TO PTRN-PTRNEFF.
*  MOVE WSAA-TODAY             TO PTRN-DATESUB.
*  MOVE WSAA-TODAY             TO PTRN-TRANSACTION-DATE.
*  ACCEPT PTRN-TRANSACTION-TIME   FROM TIME.
*  MOVE VRCM-USER              TO PTRN-USER.
*  MOVE VRCM-TERMID            TO PTRN-TERMID.
*  MOVE '1'                    TO PTRN-VALIDFLAG.
*
* 5.Print Letter
* -Get letter group from TR384.
*
* 6.Statistic
* -Call 'LIFSTTR'(AGENT/GOVERNMENT STATISTICS TRANSACTION
*  SUBROUTINE).
*
* 7.Release Contract softlock
* -Release contract softlock.
*
***********************************************************************
* </pre>
*/
public class Pr50m extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50M");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private PackedDecimalData iz = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaBillfreqx = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, REDEFINE);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreqx, 0).setUnsigned();

	private FixedLengthStringData wsaaCtables = new FixedLengthStringData(2000);
	private FixedLengthStringData[] wsaaCtable = FLSArrayPartOfStructure(500, 4, wsaaCtables, 0);
	private static final int wsaaZldcMax = 100;

	private FixedLengthStringData wsaaCompPrems = new FixedLengthStringData(1800);
	private FixedLengthStringData[] wsaaCompPrem = FLSArrayPartOfStructure(100, 18, wsaaCompPrems, 0);
	private PackedDecimalData[] wsaaCompBprem = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 0);
	private PackedDecimalData[] wsaaCompLprem = PDArrayPartOfArrayStructure(17, 2, wsaaCompPrem, 9);
	private FixedLengthStringData wsaaTableToRead = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaItemToRead = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPremiumMethod = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");

	private FixedLengthStringData wsaaRegularUnitLink = new FixedLengthStringData(1);
	private Validator regularUnitLink = new Validator(wsaaRegularUnitLink, "Y");

	private FixedLengthStringData wsaaJointLife = new FixedLengthStringData(1);
	private Validator jointLife = new Validator(wsaaJointLife, "Y");

	private FixedLengthStringData wsaaRerateComp = new FixedLengthStringData(1);
	private Validator rerateComp = new Validator(wsaaRerateComp, "Y");

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator statusValid = new Validator(wsaaValidStatus, "Y");
	private Validator statusNotValid = new Validator(wsaaValidStatus, "N");

	private FixedLengthStringData wsaaWaiveIt = new FixedLengthStringData(1);
	private Validator waiveIt = new Validator(wsaaWaiveIt, "Y");
	private PackedDecimalData wsaaLastRerateDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaZrwvflg01 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg02 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg03 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZrwvflg04 = new FixedLengthStringData(1);
	private PackedDecimalData wsaaAnnualPremium = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWopCompRrn = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaContFee = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWaiverSumins = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaJlDob = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaJlAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaJlSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaMlDob = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaMlAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaMlSex = new FixedLengthStringData(1);
	// ILIFE -3302  [Not able to do Change Client and Life Assured Detail]
	private FixedLengthStringData wsaaPremMeth= new FixedLengthStringData(4);
	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkey1 = new FixedLengthStringData(1).isAPartOf(wsaaOkeys, 0);
	private ZonedDecimalData wsaaPrevTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOkeys, 1).setUnsigned();
	private FixedLengthStringData wsaaRqstBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 6);
	private static final String hbnf = "HBNF";
		/* ERRORS */
	private static final String rl11 = "RL11";
	private static final String rl12 = "RL12";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*AGT COMM DETAILS FOR BILLING TRANSACTION*/
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Hospital Benefit File Logical*/
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private ZlifelcTableDAM zlifelcIO = new ZlifelcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Covrmjakey wsaaCovrmjakey = new Covrmjakey();
	private Covrmjakey wsaaWopCovrmjakey = new Covrmjakey();
	private Covrmjakey wsaaMainCovrmjakey = new Covrmjakey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Premiumrec premiumrec = new Premiumrec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private T5679rec t5679rec = new T5679rec();
	private Th611rec th611rec = new Th611rec();
	private T5687rec t5687rec = new T5687rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T5675rec t5675rec = new T5675rec();
	private T5688rec t5688rec = new T5688rec();
	private T5674rec t5674rec = new T5674rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Pr676cpy pr676cpy = new Pr676cpy();
	private Sr50mScreenVars sv = ScreenProgram.getScreenVars( Sr50mScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private MrtapfDAO mrtapfDAO = getApplicationContext().getBean("mrtapfDAO", MrtapfDAO.class);
	private Mrtapf mrtaIO;
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
  	private Hpadpf hpadIO;
  	private static final String  RISKPREM_FEATURE_ID="NBPRP094";//ILIFE-7845
  	private PackedDecimalData riskprm = new PackedDecimalData(17, 2);//ILIFE-7845
  	private PackedDecimalData wsaaZstpduty01 = new PackedDecimalData(17,2);
  	private boolean stampDutyflag = false;
  	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
  	private Rcvdpf rcvdPFObject= new Rcvdpf();
  	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private boolean dialdownFlag = false;
	private List<String> zclstate;
	private Iterator<String> zclstateItr;
	/*ILIFE-8248 start*/
  	private boolean lnkgFlag = false;
	/*ILIFE-8248 end*/
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;//ILIFE-8502
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao;	//ILIFE-8502
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1590,
		readSfl3302,
		exit3309,
		readAgcm3322,
		nextAgcm3328,
		exit3329,
		tr384Found3830,
		exit3890,
		readCovrmja5020,
		exit5190,
		readContitem5220,
		noMoreTr5175280,
		exit5290,
		exit5390,
		readSfl7120,
		nextSfl7180,
		exit7190,
		exit7290,
		exit7390,
		readSfl8020,
		totalDue8080
	}

	public Pr50m() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50m", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}



	/**
	* <pre>
	* NAME FORMATTING ROUTINE.
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("4000");
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End 
		sv.zgrsamt.set(ZERO);
		sv.grsprm.set(ZERO);
		sv.cntfee.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.occdate.set(varcom.maxdate);
		sv.ptdate.set(varcom.maxdate);
		sv.btdate.set(varcom.maxdate);
		sv.effdate.set(varcom.maxdate);
		wsaaBatckey.set(wsspcomn.batchkey);
		premiumrec.riskPrem.set(ZERO);//ILIFE-7845
		if(stampDutyflag) {
			wsaaZstpduty01.set(ZERO);
		}
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* Get Contract information.*/
		chdrmjaIO.setRecKeyData(SPACES);
		chdrmjaIO.setRecNonKeyData(SPACES);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		/* Get Payer information.*/
		payrIO.setRecKeyData(SPACES);
		payrIO.setRecNonKeyData(SPACES);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Initialize neccessary fields for age calculation.*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
		agecalcrec.company.set(wsspcomn.fsuco);
		/* Output to the screen.*/
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttyp.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.register.set(chdrmjaIO.getRegister());
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.ptdate.set(payrIO.getPtdate());
		sv.btdate.set(payrIO.getBtdate());
		sv.effdate.set(payrIO.getBtdate());
		sv.billfreq.set(payrIO.getBillfreq());
		wsaaBillfreq.set(payrIO.getBillfreq());
		sv.mop.set(payrIO.getBillchnl());
		/* Get Contract Owner.*/
		cltsIO.setClntpfx(chdrmjaIO.getCownpfx());
		cltsIO.setClntcoy(chdrmjaIO.getCowncoy());
		cltsIO.setClntnum(chdrmjaIO.getCownnum());
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		sv.cownnum.set(chdrmjaIO.getCownnum());
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		/* Get Assured.*/
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		sv.lifenum.set(lifemjaIO.getLifcnum());
		/*    MOVE LIFEMJA-SMOKING        TO WSAA-SMOKING.                 */
		stampDutyflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(stampDutyflag) {
			sv.zstpduty.set(ZERO);
		}

		/* ILIFE-8248 start */
		lnkgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(), "NBPRP055", appVars, "IT");
		/* ILIFE-8248 end */
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		getDesc1100();
		readT56791200();
		readT57291300();
		readTh6111500();
		if(stampDutyflag) {
			zclstate = new ArrayList<>();
		}
		loadSubfile5000();
		calcContFee6000();
		calcWopPremium7000();
		if(stampDutyflag)
			sv.stampduty.set(wsaaZstpduty01);
		calcDuePremium8000();
		checkMaxminPrem8100();
		scrnparams.subfileRrn.set(1);
	}

protected void getDesc1100()
	{
		start1110();
	}

protected void start1110()
	{
		/* Get Contract Status Description*/
		wsaaTableToRead.set(tablesInner.t3623);
		wsaaItemToRead.set(chdrmjaIO.getStatcode());
		readDesc1100a();
		sv.chdrstatus.set(descIO.getShortdesc());
		/* Get Premium Status Description*/
		wsaaTableToRead.set(tablesInner.t3588);
		wsaaItemToRead.set(payrIO.getPstatcode());
		readDesc1100a();
		sv.premstatus.set(descIO.getShortdesc());
		/* Get Contract Type Description*/
		wsaaTableToRead.set(tablesInner.t5688);
		wsaaItemToRead.set(sv.cnttyp);
		readDesc1100a();
		sv.ctypedes.set(descIO.getLongdesc());
		/* Get Billing Freq Description*/
		wsaaTableToRead.set(tablesInner.t3590);
		wsaaItemToRead.set(sv.billfreq);
		readDesc1100a();
		sv.freqdesc.set(descIO.getLongdesc());
		/* Get Billing Method Description*/
		wsaaTableToRead.set(tablesInner.t3620);
		wsaaItemToRead.set(sv.mop);
		readDesc1100a();
		sv.mopdesc.set(descIO.getLongdesc());
	}

protected void readDesc1100a()
	{
		start1110a();
	}

protected void start1110a()
	{
		descIO.setRecKeyData(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaTableToRead);
		descIO.setDescitem(wsaaItemToRead);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
	}

protected void readT56791200()
	{
		start1210();
	}

protected void start1210()
	{
		/* Routine to get Status Reqs by Transaction.*/
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readT57291300()
	{
		start1310();
	}

protected void start1310()
	{
		/* Routine to get Flexible Premium Variances.*/
		wsaaFlexiblePremium.set("N");
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5729);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemcoy(),wsspcomn.company)
		&& isEQ(itdmIO.getItemtabl(), tablesInner.t5729)
		&& isEQ(itdmIO.getItemitem(),chdrmjaIO.getCnttype())) {
			wsaaFlexiblePremium.set("Y");
		}
	}

protected void readTh6111500()
	{
		try {
			start1510();
			readCatchAll1520();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start1510()
	{
		/* Routine to get Minimum/Maximum Modal Premium*/
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.th611);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.getItemitem().setSub1String(4, 3, payrIO.getCntcurr());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemcoy(),chdrmjaIO.getChdrcoy())
		&& isEQ(itdmIO.getItemtabl(), tablesInner.th611)
		&& isEQ(subString(itdmIO.getItemitem(), 1, 3),chdrmjaIO.getCnttype())
		&& isEQ(subString(itdmIO.getItemitem(), 4, 3),payrIO.getCntcurr())) {
			th611rec.th611Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.exit1590);
		}
	}

protected void readCatchAll1520()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.th611);
		itdmIO.setItemitem("***");
		itdmIO.getItemitem().setSub1String(4, 3, payrIO.getCntcurr());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemcoy(),chdrmjaIO.getChdrcoy())
		&& isEQ(itdmIO.getItemtabl(), tablesInner.th611)
		&& isEQ(subString(itdmIO.getItemitem(), 1, 3),"***")
		&& isEQ(subString(itdmIO.getItemitem(), 4, 3),payrIO.getCntcurr())) {
			th611rec.th611Rec.set(itdmIO.getGenarea());
			return ;
		}
		syserrrec.statuz.set(rl12);
		fatalError600();
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*VALIDATE-SCREEN*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		callSr50mio5900a();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}

		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		/*VALIDATION*/
		/**    Validate subfile fields*/
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		callSr50mio5900();
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srnch);
		callSr50mio5900a();
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		processChdr3100();
		processPayr3200();
		processComponent3300();
		createPtrn3400();
		printLetter3800();
		statistic3600();
		releaseSoftlock3700();
		/*EXIT*/
	}

protected void processChdr3100()
	{
		rlseChdr3110();
		readChdr3120();
		setValidflag23130();
		validflag13140();
	}

protected void rlseChdr3110()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void readChdr3120()
	{
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void setValidflag23130()
	{
		wsaaPrevTranno.set(chdrmjaIO.getTranno());
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(sv.ptdate);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13140()
	{
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		chdrmjaIO.setCurrfrom(sv.ptdate);
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		chdrmjaIO.setBillchnl(sv.mop);
		chdrmjaIO.setBillfreq(sv.billfreq);
		chdrmjaIO.setSinstamt01(sv.grsprm);
		chdrmjaIO.setSinstamt02(wsaaContFee);
		chdrmjaIO.setValidflag("1");
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(),chdrmjaIO.getSinstamt02()),chdrmjaIO.getSinstamt03()),chdrmjaIO.getSinstamt04()),chdrmjaIO.getSinstamt05()));
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void processPayr3200()
	{
		rlsePayr3210();
		readPayr3220();
		setValidflag23230();
		setValidflag13240();
	}

protected void rlsePayr3210()
	{
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void readPayr3220()
	{
		payrIO.setFunction(varcom.readh);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void setValidflag23230()
	{
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void setValidflag13240()
	{
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setTransactionDate(wsaaToday);
		payrIO.setUser(varcom.vrcmUser);
		payrIO.setEffdate(sv.ptdate);
		payrIO.setBillchnl(sv.mop);
		payrIO.setBillfreq(sv.billfreq);
		if(stampDutyflag) {
			payrIO.setSinstamt01(add(sv.grsprm,sv.stampduty));
		}
		else {
			payrIO.setSinstamt01(sv.grsprm);
		}
		payrIO.setSinstamt02(wsaaContFee);
		payrIO.setValidflag("1");
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(),payrIO.getSinstamt02()),payrIO.getSinstamt03()),payrIO.getSinstamt04()),payrIO.getSinstamt05()));
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void processComponent3300()
	{
		if(stampDutyflag) {
			zclstateItr = zclstate.iterator();
		}
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					startSfl3301();
				case readSfl3302:
					readSfl3302();
				case exit3309:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void startSfl3301()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl3302()
	{
		callSr50mio5900a();
		if (isNE(scrnparams.statuz,varcom.oK)) {
			goTo(GotoLabel.exit3309);
		}
		/*PROCESS-SFL*/
		processCovr3310();
		processAgcm3320();
		/*NEXT-SFL*/
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl3302);
	}

protected void processCovr3310()
	{
		readCovrmja3311();
		setValidflag23312();
		setValidflag13313();
	}

protected void readCovrmja3311()
	{
		covrmjaIO.setDataKey(sv.datakey);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void setValidflag23312()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(sv.ptdate);
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void setValidflag13313()
	{
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(sv.ptdate);
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(chdrmjaIO.getTranno()); //PINNACLE-2658
		covrmjaIO.setUser(varcom.vrcmUser);
		covrmjaIO.setTransactionDate(wsaaToday);
		covrmjaIO.setInstprem(sv.zgrsamt);
		covrmjaIO.setZbinstprem(sv.basprm);
		covrmjaIO.setZlinstprem(sv.zloprm);
		covrmjaIO.setRiskprem(riskprm);//ILIFE-7845
		if(stampDutyflag) {
			covrmjaIO.setZstpduty01(sv.zstpduty);
			covrmjaIO.setZclstate(zclstateItr.next());
			covrmjaIO.setInstprem(add(sv.zgrsamt, sv.zstpduty));
		}
		if (isEQ(sv.waiverprem,"Y")) {
			covrmjaIO.setSumins(sv.sumins);
		}
		/* Get Updated Age Next & Sex from LIFE*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setAnbAtCcd(lifemjaIO.getAnbAtCcd());
		covrmjaIO.setSex(lifemjaIO.getCltsex());
		covrmjaIO.setMortcls(lifemjaIO.getSmoking());//ILIFE-8412
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void processAgcm3320()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begnAgcm3321();
				case readAgcm3322:
					readAgcm3322();
					setValidflag23323();
					setValidflag13324();
				case nextAgcm3328:
					nextAgcm3328();
				case exit3329:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnAgcm3321()
	{
		agcmbchIO.setRecKeyData(SPACES);
		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
		agcmbchIO.setLife(covrmjaIO.getLife());
		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
		agcmbchIO.setRider(covrmjaIO.getRider());
		agcmbchIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmbchIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
	}

protected void readAgcm3322()
	{
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)
		&& isNE(agcmbchIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			fatalError600();
		}
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)
		|| isNE(agcmbchIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(agcmbchIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(agcmbchIO.getLife(),covrmjaIO.getLife())
		|| isNE(agcmbchIO.getCoverage(),covrmjaIO.getCoverage())
		|| isNE(agcmbchIO.getRider(),covrmjaIO.getRider())
		|| isNE(agcmbchIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())) {
			goTo(GotoLabel.exit3329);
		}
		if (isEQ(agcmbchIO.getPtdate(),ZERO)) {
			goTo(GotoLabel.nextAgcm3328);
		}
		if (isEQ(agcmbchIO.getTranno(),chdrmjaIO.getTranno())) {
			goTo(GotoLabel.nextAgcm3328);
		}
	}

protected void setValidflag23323()
	{
		agcmbchIO.setCurrto(sv.ptdate);
		agcmbchIO.setValidflag("2");
		agcmbchIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			fatalError600();
		}
	}

protected void setValidflag13324()
	{
		/*    MOVE SR50M-BILLFREQ         TO WSAA-BILLFREQ.*/
		setPrecision(agcmbchIO.getAnnprem(), 2);
		agcmbchIO.setAnnprem(mult(sv.zgrsamt,wsaaBillfreq9));
		agcmbchIO.setValidflag("1");
		agcmbchIO.setTranno(chdrmjaIO.getTranno());
		agcmbchIO.setCurrfrom(sv.ptdate);
		agcmbchIO.setCurrto(varcom.vrcmMaxDate);
		agcmbchIO.setUser(varcom.vrcmUser);
		/*agcmbchIO.setTransactionDate(wsaaToday);
		agcmbchIO.setTransactionTime(getCobolTime());
		*/agcmbchIO.setTransactionTime(varcom.vrcmTime.toInt());
		agcmbchIO.setTransactionDate(varcom.vrcmDate.toInt());
		agcmbchIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmbchIO.getParams());
			syserrrec.statuz.set(agcmbchIO.getStatuz());
			fatalError600();
		}
	}

protected void nextAgcm3328()
	{
		agcmbchIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readAgcm3322);
	}

protected void createPtrn3400()
	{
		start3410();
	}

protected void start3410()
	{
		ptrnIO.setRecKeyData(SPACES);
		ptrnIO.setRecNonKeyData(SPACES);
		ptrnIO.setDataKey(wsaaBatckey);
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setRecode(chdrmjaIO.getRecode());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(sv.ptdate);
		ptrnIO.setDatesub(wsaaToday);
		/*	ptrnIO.setTransactionDate(wsaaToday);
		ptrnIO.setTransactionTime(getCobolTime());
		*/ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setCrtuser(wsspcomn.userid); //IJS-523
		ptrnIO.setValidflag("1");
		ptrnIO.setTransactionTime(varcom.vrcmTime.toInt());
		ptrnIO.setTransactionDate(varcom.vrcmDate.toInt());
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void statistic3600()
	{
		start3610();
	}

protected void start3610()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void releaseSoftlock3700()
	{
		rlseChdr3710();
	}

protected void rlseChdr3710()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.fsuco);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void printLetter3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readTr384FullKey3810();
					readTr384CatchAll3820();
				case tr384Found3830:
					tr384Found3830();
					letterRequest3860();
				case exit3890:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readTr384FullKey3810()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.getItemitem().setSub1String(4, 4, wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.tr384Found3830);
		}
	}

protected void readTr384CatchAll3820()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem("***");
		itemIO.getItemitem().setSub1String(4, 4, wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.tr384Found3830);
		}
		else {
			goTo(GotoLabel.exit3890);
		}
	}

protected void tr384Found3830()
	{
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType,SPACES)) {
			goTo(GotoLabel.exit3890);
		}
	}

protected void letterRequest3860()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		wsaaOkey1.set(wsspcomn.language);
		wsaaRqstBillfreq.set(sv.billfreq);
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.despnum.set(chdrmjaIO.getDespnum());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadSubfile5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					clrSfl5010();
				case readCovrmja5020:
					readCovrmja5020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void clrSfl5010()
	{
		scrnparams.function.set(varcom.sclr);
		callSr50mio5900();
		covrmjaIO.setRecKeyData(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void readCovrmja5020()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setFunction(varcom.nextr);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		|| isNE(covrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(covrmjaIO.getChdrcoy(),chdrmjaIO.getChdrcoy())) {
			return ;
		}
		statusCheck5100();
		if (statusNotValid.isTrue()) {
			goTo(GotoLabel.readCovrmja5020);
		}
		sv.initialiseSubfileArea();
		initialize(sv.subfileFields);
		sv.sumins.set(ZERO);
		sv.basprm.set(ZERO);
		sv.zloprm.set(ZERO);
		sv.zgrsamt.set(ZERO);
		if(stampDutyflag)
			sv.zstpduty.set(ZERO);
		riskprm.set(ZERO);//ILIFE-7845
		sv.datakey.set(covrmjaIO.getDataKey());
		sv.crtable.set(covrmjaIO.getCrtable());
		wsaaTableToRead.set(tablesInner.t5687);
		wsaaItemToRead.set(covrmjaIO.getCrtable());
		readDesc1100a();
		sv.entity.set(descIO.getLongdesc());
		sv.pstatdate.set(covrmjaIO.getPremCessDate());
		if (!flexiblePremium.isTrue()) {
			readTr5175200();
			if (isNE(sv.waiverprem,"Y")) {
				sv.sumins.set(covrmjaIO.getSumins());
				calcPremium5300();
			}
		}
		else {
			calcFlexiblePremium5400();
		}
		scrnparams.function.set(varcom.sadd);
		callSr50mio5900();
		goTo(GotoLabel.readCovrmja5020);
	}

protected void statusCheck5100()
	{
		try {
			covrRiskStatus5110();
			covrPremStatus5120();
		}
		catch (GOTOException e){
		}
	}

protected void covrRiskStatus5110()
	{
		wsaaValidStatus.set("N");
		if (isEQ(covrmjaIO.getRider(),"00")) {
			for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
				if (isEQ(covrmjaIO.getStatcode(),t5679rec.covRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
				if (isEQ(covrmjaIO.getStatcode(),t5679rec.ridRiskStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		if (statusNotValid.isTrue()) {
			goTo(GotoLabel.exit5190);
		}
	}

protected void covrPremStatus5120()
	{
		wsaaValidStatus.set("N");
		if (isEQ(covrmjaIO.getRider(),"00")) {
			for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(),t5679rec.covPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(),t5679rec.ridPremStat[ix.toInt()])) {
					wsaaValidStatus.set("Y");
					ix.set(13);
				}
			}
		}
		if (statusNotValid.isTrue()) {
			return ;
		}
	}

protected void readTr5175200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begnItdm5210();
				case readContitem5220:
					readContitem5220();
				case noMoreTr5175280:
					noMoreTr5175280();
				case exit5290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnItdm5210()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())) {
			goTo(GotoLabel.exit5290);
		}
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		sv.waiverprem.set("Y");
		sv.activeInd.set("Y");
		sv.zrwvflg01.set(tr517rec.zrwvflg01);
		sv.zrwvflg02.set(tr517rec.zrwvflg02);
		sv.zrwvflg03.set(tr517rec.zrwvflg03);
		sv.zrwvflg04.set(tr517rec.zrwvflg04);
		iy.set(ZERO);
		wsaaCtables.set(SPACES);
		for (ix.set(1); !(isGT(ix,50)
		|| isGTE(iy,500)); ix.add(1)){
			if (isNE(tr517rec.ctable[ix.toInt()],SPACES)) {
				iy.add(1);
				wsaaCtable[iy.toInt()].set(tr517rec.ctable[ix.toInt()]);
			}
		}
		if (isEQ(tr517rec.contitem,SPACES)) {
			goTo(GotoLabel.noMoreTr5175280);
		}
	}

protected void readContitem5220()
	{
		itdmIO.setItemitem(tr517rec.contitem);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(),tr517rec.contitem)) {
			goTo(GotoLabel.noMoreTr5175280);
		}
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		for (ix.set(1); !(isGT(ix,50)
		|| isGTE(iy,500)); ix.add(1)){
			if (isNE(tr517rec.ctable[ix.toInt()],SPACES)) {
				iy.add(1);
				wsaaCtable[iy.toInt()].set(tr517rec.ctable[ix.toInt()]);
			}
		}
		if (isEQ(tr517rec.contitem,SPACES)) {
			goTo(GotoLabel.noMoreTr5175280);
		}
		else {
			goTo(GotoLabel.readContitem5220);
		}
	}

protected void noMoreTr5175280()
	{
		sv.workAreaData.set(wsaaCtables);
	}

protected void calcPremium5300()
	{
		try {
			lifeAndJointLife5310();
			readT56875320();
			readT56755330();
			initPremiumrec5350();
		}
		catch (GOTOException e){
		}
	}

protected void lifeAndJointLife5310()
	{
		lifeAndJointLife5700();
	}

protected void readT56875320()
	{
		readT56879000();
		if (isNE(t5687rec.bbmeth,SPACES)) {
			wsaaRegularUnitLink.set("Y");
		}
		else {
			wsaaRegularUnitLink.set("N");
		}
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod,SPACES)) {
			goTo(GotoLabel.exit5390);
		}
		if (isNE(t5687rec.rtrnwfreq,ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
	}

protected void readT56755330()
	{
		readT56759100();
	}

protected void initPremiumrec5350()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.mop.set(sv.mop);
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(lifemjaIO.getSmoking()); //PINNACLE-2565
		/*    MOVE WSAA-MORTCLS           TO CPRM-MORTCLS.                 */
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.sumin.set(covrmjaIO.getSumins());
		premiumrec.calcPrem.set(covrmjaIO.getInstprem());
		premiumrec.calcBasPrem.set(covrmjaIO.getInstprem());//ILIFE-7504
		if (rerateComp.isTrue()) {
			traceLastRerateDate5500();
			premiumrec.effectdt.set(wsaaLastRerateDate);
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastRerateDate);
			calcAge5600();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5600();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(wsaaMlAnbAtCcd);
			if (jointLife.isTrue()) {
				premiumrec.jlage.set(wsaaJlAnbAtCcd);
			}
		}
		premiumrec.lsex.set(wsaaMlSex);
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		annyIO.setRecKeyData(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFormat(formatsInner.annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.language.set(wsspcomn.language);
		/* Get Hospital Benefit.*/
		initialize(pr676cpy.parmRecord);
		hbnfIO.setRecKeyData(SPACES);
		hbnfIO.setChdrcoy(covrmjaIO.getChdrcoy());
		hbnfIO.setChdrnum(covrmjaIO.getChdrnum());
		hbnfIO.setLife(covrmjaIO.getLife());
		hbnfIO.setCoverage(covrmjaIO.getCoverage());
		hbnfIO.setRider(covrmjaIO.getRider());
		hbnfIO.setFormat(formatsInner.hbnfrec);
		hbnfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isEQ(hbnfIO.getStatuz(),varcom.oK)) {
			/*       MOVE HBNF-ZPLNCDE        TO CPRM-ZPLNCDE*/
			premiumrec.benpln.set(hbnfIO.getBenpln());
			pr676cpy.mortcls.set(hbnfIO.getMortcls());
			pr676cpy.livesno.set(hbnfIO.getLivesno());
			pr676cpy.benpln.set(hbnfIO.getBenpln());
			pr676cpy.waiverCrtable.set(hbnfIO.getCrtable());
		}
		else {
			/*       MOVE SPACES              TO CPRM-ZPLNCDE*/
			premiumrec.benpln.set(SPACES);
		}
		compute(wsaaAnnualPremium, 2).set(mult(covrmjaIO.getInstprem(),wsaaBillfreq9));
		/*  Calc Premium Base on the freq.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.billfreq.set(sv.billfreq);
		wsaaBillfreq.set(premiumrec.billfreq);
		if (regularUnitLink.isTrue()) {
			compute(premiumrec.calcPrem, 2).set(div(wsaaAnnualPremium,wsaaBillfreq9));
		}
		/*Start TMLII-1966*/
		/*if (isNE(covrmjaIO.getCrtable(),hbnf)) {
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else {
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
		}*/
		//ILIFE-8248
		if (("PMEX".equals(t5675rec.premsubr.toString().trim()))&& (lnkgFlag == true)){
		checkLinkage();
		}
		//ILIFE-8248
		if (isEQ(covrmjaIO.getCrtable(),hbnf)
			|| (isEQ(hbnfIO.getStatuz(),varcom.oK)
			&& isEQ(covrmjaIO.getCrtable(),hbnfIO.getCrtable()))) {
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
			{
		    	callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);			}
			else
			{	
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				// ILIFE-3302 [Not able to do Change Client and Life Assured Detail]
				premiumrec.premMethod.set(wsaaPremMeth);
				/* ILIFE_3489 starts */
				premiumrec.liveno.set(pr676cpy.livesno);
				premiumrec.livesno.set(pr676cpy.livesno);
				premiumrec.indic.set(pr676cpy.waiverprem);
				premiumrec.percent.set(ZERO);
				premiumrec.ccode.set(pr676cpy.waiverCrtable);
				/* ILIFE-3489 ends */
				//ILIFE-2465 Sr676_Hospital & Surgical Benefit
				callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
			}
		    /*Ticket #IVE-792 - End*/
		}
		else {

		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
			/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())|| t5675rec.premsubr.toString().trim().equals("PMEX")))// ILIFE-7584
			{
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			}
			else
			{	
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				vpxlextrec.function.set("INIT");
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
				Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
				vpxchdrrec.function.set("INIT");
				callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
				premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
				Vpxacblrec vpxacblrec=new Vpxacblrec();
				callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
				//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
				//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
				// ILIFE - 3302 [Not able to do Change Client and Life Assured Detail] 
				premiumrec.premMethod.set(wsaaPremMeth);
				// ILIFE-7584
				premiumrec.cownnum.set(SPACES);
				premiumrec.occdate.set(ZERO);				
				if("PMEX".equals(t5675rec.premsubr.toString().trim())){
					premiumrec.setPmexCall.set("Y");
					premiumrec.batcBatctrcde.set(wsaaBatckey.batcBatctrcde);
					premiumrec.inputPrevPrem.set(ZERO);//ILIFE-8537
					premiumrec.validflag.set("Y");
					premiumrec.updateRequired.set("N");
					premiumrec.cownnum.set(chdrmjaIO.getCownnum());
					premiumrec.occdate.set(chdrmjaIO.getOccdate());
					dialdownFlag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
					incomeProtectionflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
					premiumflag = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
					if(incomeProtectionflag || premiumflag || dialdownFlag){
						setRcvdData();
						if(rcvdPFObject!=null){
							if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("")){
								premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
							}
							if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("")){
								premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
							}
							if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("")){
								premiumrec.bentrm.set(rcvdPFObject.getBentrm());
							}
							if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("")){
								if(("S").equals(rcvdPFObject.getPrmbasis()))
									premiumrec.prmbasis.set("Y");
								else
									premiumrec.prmbasis.set(SPACES);
							}
							if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
								if(rcvdPFObject.getDialdownoption().startsWith("0")) {
									premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption().substring(1));
								}
								else {
									premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
								}
							}
						}
					}
						premiumrec.tpdtype.set(covrmjaIO.getTpdtype().charAt(5));
					
				}
				// ILIFE-7584
				if("RPRMRDT".equals(t5675rec.premsubr.toString().trim()) || "LCP1".equals(covrmjaIO.getCrtable().toString().trim())){//ILIFE-8537
					hpadRead100();
					m900CheckRedtrm();
					vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
					compute(vpxlextrec.tempprat, 3).setRounded(mult(mrtaIO.getPrat(),10));
					vpxlextrec.prat.set(vpxlextrec.tempprat);
					compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
					vpxlextrec.coverc.set(vpxlextrec.temptoc);
					compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
					vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
					vpxlextrec.hpropdte.set(hpadIO.getHpropdte());
				}
				//ILIFE-8502-starts
				premiumrec.commTaxInd.set("Y");
				premiumrec.prevSumIns.set(ZERO);
				premiumrec.mortcls.set(covrmjaIO.getMortcls());
				clntDao = DAOFactory.getClntpfDAO();
				clnt=clntDao.getClientByClntnum(cltsIO.getClntnum().toString());
				if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
					premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
				}else{
					premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
				}
				//ILIFE-8502-end
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			}
		    /*Ticket #IVE-792 - End*/
			//****Ticket #ILIFE-2005 end
			
		}
			/* End TMLII-1966*/
			 
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		premiumrec.riskPrem.set(ZERO);//ILIFE-7845
		calcRiskPrem();//ILIFE-7845
		sv.zgrsamt.set(premiumrec.calcPrem);
		sv.basprm.set(premiumrec.calcBasPrem);
		sv.zloprm.set(premiumrec.calcLoaPrem);
		riskprm.set(premiumrec.riskPrem);//ILIFE-7845 
		if(stampDutyflag) {
			wsaaZstpduty01.add(premiumrec.zstpduty01);
			sv.zstpduty.set(premiumrec.zstpduty01);
			zclstate.add(premiumrec.rstate01.toString());
		}
		else {
			sv.stampdutyOut[varcom.nd.toInt()].set("Y");
		}
	}

	// ILIFE-8248 - Start
	protected void checkLinkage() {

		if (null == covrmjaIO.getLnkgsubrefno() || covrmjaIO.getLnkgsubrefno().trim().isEmpty() ) {
			premiumrec.lnkgSubRefNo.set(SPACE);
		} else {
			premiumrec.lnkgSubRefNo.set(covrmjaIO.getLnkgsubrefno().toString().trim());
		}

		if (null == covrmjaIO.getLnkgno() || covrmjaIO.getLnkgno().trim().isEmpty()) {
			premiumrec.linkcov.set(SPACE);
		} else {
				LinkageInfoService linkgService = new LinkageInfoService();
				FixedLengthStringData linkgCov = new FixedLengthStringData(
						linkgService.getLinkageInfo(covrmjaIO.getLnkgno().toString().trim()));
				premiumrec.linkcov.set(linkgCov);
		}

	}

	// ILIFE-8248 - ends
protected void m900CheckRedtrm()
{
	/*M900-START*/
	mrtaIO=mrtapfDAO.getMrtaRecord(covrmjaIO.getChdrcoy().toString(), covrmjaIO.getChdrnum().toString());
		/*M900-EXIT*/
}

protected void hpadRead100()
{
	/*HPAD*/
	/* READ OFF HPAD FILE TO RETRIEVE THE PROPOSE DATE (HPAD-PROPDTE)*/
	/* USED TO ACCESS PREMIUM RATE FOR TABLE TH617*/
	
	hpadIO=hpadpfDAO.getHpadData(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
	/*EXIT*/
}
protected void calcFlexiblePremium5400()
	{
		/*START*/
		compute(sv.zgrsamt, 2).set(mult(covrmjaIO.getInstprem(),wsaaBillfreq9));
		/*EXIT*/
	}

protected void traceLastRerateDate5500()
	{
		start5510();
	}

protected void start5510()
	{
		wsaaLastRerateDate.set(covrmjaIO.getRerateDate());
		datcon4rec.billday.set(payrIO.getDuedd());
		datcon4rec.billmonth.set(payrIO.getDuemm());
		compute(datcon4rec.freqFactor, 0).set(sub(ZERO,t5687rec.rtrnwfreq));
		datcon4rec.frequency.set("01");
		while ( !(isLTE(wsaaLastRerateDate,sv.ptdate))) {
			datcon4rec.intDate1.set(wsaaLastRerateDate);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastRerateDate.set(datcon4rec.intDate2);
		}

		if (isLT(wsaaLastRerateDate,covrmjaIO.getCrrcd())) {
			wsaaLastRerateDate.set(covrmjaIO.getCrrcd());
		}
	}

protected void calcAge5600()
	{
		/*START*/
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			syserrrec.params.set(agecalcrec.agecalcRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void lifeAndJointLife5700()
	{
		start5710();
	}

protected void start5710()
	{
		/* Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		wsaaMlDob.set(lifemjaIO.getCltdob());
		wsaaMlSex.set(lifemjaIO.getCltsex());
		wsaaMlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		/*Get Mortality Class from CLTS*/
		cltsIO.setParams(SPACES);
		cltsIO.setStatuz(varcom.oK);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    MOVE CLTS-STATCODE          TO WSAA-MORTCLS.*/
		/*    MOVE WSAA-SMOKING           TO WSAA-MORTCLS.                 */
		/* Joint Life.*/
		lifemjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.oK)) {
			wsaaJointLife.set("Y");
			wsaaJlSex.set(lifemjaIO.getCltdob());
			wsaaJlDob.set(lifemjaIO.getCltsex());
			wsaaJlAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		else {
			wsaaJointLife.set("N");
			wsaaJlSex.set(SPACES);
			wsaaJlDob.set(ZERO);
			wsaaJlAnbAtCcd.set(ZERO);
		}
	}

protected void callSr50mio5900()
	{
		/*START*/
		processScreen("SR50M", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callSr50mio5900a()
	{
		/*A-START*/
		processScreen("SR50M", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*A-EXIT*/
	}

protected void callSr50mio5900b()
	{
		/*B-START*/
		processScreen("SR50M", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*B-EXIT*/
	}

protected void calcContFee6000()
	{
			start6010();
		}

protected void start6010()
	{
		/* Get Contract Processing Rules.*/
		wsaaContFee.set(ZERO);
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		if (isEQ(t5688rec.feemeth,SPACES)) {
			return ;
		}
		/* Get Contract Management Fee Subrountine & Calc the fees based*/
		/* billing freq.*/
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		if (isEQ(t5674rec.commsubr,SPACES)) {
			return ;
		}
		initialize(mgfeelrec.mgfeelRec);
		mgfeelrec.cnttype.set(chdrmjaIO.getCnttype());
		mgfeelrec.effdate.set(chdrmjaIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrmjaIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		/*    MOVE CHDRMJA-CHDRNUM        TO MGFL-CHDRNUM.*/
		mgfeelrec.billfreq.set(sv.billfreq);
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)
		&& isNE(mgfeelrec.statuz,varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		wsaaContFee.set(mgfeelrec.mgfee);
	}

protected void calcWopPremium7000()
	{
		findWopComp7010();
	}

protected void findWopComp7010()
	{
		/* WSAA-ZRWVFLG-03: Waive Policy Fee*/
		wsaaWopCompRrn.set(ZERO);
		scrnparams.statuz.set(varcom.oK);
		for (iz.set(1); !(isNE(scrnparams.statuz,varcom.oK)); iz.add(1)){
			scrnparams.subfileRrn.set(iz);
			scrnparams.function.set(varcom.sread);
			callSr50mio5900b();
			if (isEQ(scrnparams.statuz,varcom.oK)) {
				if (isEQ(sv.activeInd,"Y")
				&& isEQ(sv.sumins,ZERO)) {
					wsaaWopCompRrn.set(iz);
					sumWopSumins7100();
					if (isEQ(wsaaZrwvflg03,"Y")) {
						wsaaWaiverSumins.add(wsaaContFee);
					}
					wopPremium7300();
					scrnparams.statuz.set(varcom.oK);
				}
			}
		}
	}

protected void sumWopSumins7100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start7110();
				case readSfl7120:
					readSfl7120();
					checkWaiveAll7130();
				case nextSfl7180:
					nextSfl7180();
				case exit7190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7110()
	{
		/* -ZRWVFLG-01: Waive itself*/
		/* -ZRWVFLG-02: Waive all life*/
		/* -ZRWVFLG-03: Waive Policy Fee*/
		/* -ZRWVFLG-04: Accelerated Crisis Waiver*/
		wsaaWaiverSumins.set(ZERO);
		wsaaWopCovrmjakey.set(sv.datakey);
		wsaaCtables.set(sv.workAreaData);
		wsaaZrwvflg02.set(sv.zrwvflg02);
		wsaaZrwvflg03.set(sv.zrwvflg03);
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl7120()
	{
		callSr50mio5900a();
		if (isNE(scrnparams.statuz,varcom.oK)) {
			goTo(GotoLabel.exit7190);
		}
		if (isEQ(scrnparams.subfileRrn,wsaaWopCompRrn)) {
			goTo(GotoLabel.nextSfl7180);
		}
	}

protected void checkWaiveAll7130()
	{
		wsaaCovrmjakey.set(sv.datakey);
		if (isNE(wsaaZrwvflg02,"Y")) {
			if (isNE(wsaaCovrmjakey.covrmjaLife,wsaaWopCovrmjakey.covrmjaLife)) {
				goTo(GotoLabel.nextSfl7180);
			}
		}
		wsaaWaiveIt.set("N");
		for (ix.set(1); !(isGT(ix,500)
		|| waiveIt.isTrue()
		|| isEQ(wsaaCtable[ix.toInt()],SPACES)); ix.add(1)){
			if (isEQ(sv.crtable,wsaaCtable[ix.toInt()])) {
				wsaaWaiveIt.set("Y");
			}
		}
		/* Not waive, skip.*/
		if (!waiveIt.isTrue()) {
			goTo(GotoLabel.nextSfl7180);
		}
		/* None Accelerated Crisis Waiver, just add the premium as WOP*/
		/* sum assured.*/
		if (isNE(tr517rec.zrwvflg04,"Y")) {
			wsaaWaiverSumins.add(sv.zgrsamt);
			goTo(GotoLabel.nextSfl7180);
		}
		/* Accelerated Crisis Waiver, .....*/
		if (isNE(wsaaCovrmjakey.covrmjaLife,wsaaWopCovrmjakey.covrmjaLife)) {
			goTo(GotoLabel.nextSfl7180);
		}
		if (isNE(wsaaCovrmjakey.covrmjaCoverage,wsaaWopCovrmjakey.covrmjaCoverage)) {
			goTo(GotoLabel.exit7190);
		}
		if (isEQ(wsaaCovrmjakey.covrmjaRider,"00")) {
			wsaaMainCovrmjakey.set(sv.datakey);
			wsaaWaiverSumins.set(sv.sumins);
		}
		else {
			wsaaWaiverSumins.subtract(sv.sumins);
			calcBenefitAmount7200();
		}
	}

protected void nextSfl7180()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl7120);
	}

protected void calcBenefitAmount7200()
	{
		try {
			readMainCoverage7210();
			readT56877230();
			initPremiumrec7250();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readMainCoverage7210()
	{
		covrmjaIO.setDataKey(wsaaMainCovrmjakey);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		/*LIFE-AND-JOINT-LIFE*/
		lifeAndJointLife5700();
	}

protected void readT56877230()
	{
		readT56879000();
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod,SPACES)) {
			goTo(GotoLabel.exit7290);
		}
		if (isNE(t5687rec.rtrnwfreq,ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
		/*READ-T5675*/
		readT56759100();
	}

protected void initPremiumrec7250()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.mop.set(sv.mop);
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		/*    MOVE WSAA-MORTCLS           TO CPRM-MORTCLS.                 */
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		if (rerateComp.isTrue()) {
			traceLastRerateDate5500();
			premiumrec.effectdt.set(wsaaLastRerateDate);
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastRerateDate);
			calcAge5600();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5600();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.jlage.set(wsaaJlAnbAtCcd);
			}
		}
		premiumrec.lsex.set(wsaaMlSex);
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Get Annuity Details.*/
		annyIO.setRecKeyData(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFormat(formatsInner.annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.language.set(wsspcomn.language);
		/*  Calc Premium Base on Billing Freq.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins);
		premiumrec.billfreq.set(sv.billfreq);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{	
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			// ILIFE-3302 [Not able to do Change Client and Life Assured Detail]
			premiumrec.premMethod.set(wsaaPremMeth);
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		//****Ticket #ILIFE-2005 end
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		premiumrec.riskPrem.set(ZERO);//ILIFE-7845
		calcRiskPrem();//ILIFE-7845
		wsaaWaiverSumins.set(premiumrec.calcPrem);
	}

protected void wopPremium7300()
	{
		try {
			readWopCoverage7310();
			sreadForWopCoverage7320();
			readT56877340();
			readT56757350();
			initPremiumrec7360();
			supdForWopCoverage7370();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readWopCoverage7310()
	{
		covrmjaIO.setDataKey(wsaaWopCovrmjakey);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void sreadForWopCoverage7320()
	{
		scrnparams.subfileRrn.set(wsaaWopCompRrn);
		scrnparams.function.set(varcom.sread);
		callSr50mio5900();
		/*LIFE-AND-JOINT-LIFE*/
		lifeAndJointLife5700();
	}

protected void readT56877340()
	{
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (jointLife.isTrue()) {
			wsaaPremiumMethod.set(t5687rec.jlPremMeth);
		}
		else {
			wsaaPremiumMethod.set(t5687rec.premmeth);
		}
		if (isEQ(wsaaPremiumMethod,SPACES)) {
			goTo(GotoLabel.exit7390);
		}
		if (isNE(t5687rec.rtrnwfreq,ZERO)) {
			wsaaRerateComp.set("Y");
		}
		else {
			wsaaRerateComp.set("N");
		}
	}

protected void readT56757350()
	{
		readT56759100();
	}

protected void initPremiumrec7360()
	{
		initialize(premiumrec.premiumRec);
		premiumrec.function.set("CALC");
		premiumrec.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrmjaIO.getChdrnum());
		premiumrec.lifeLife.set(covrmjaIO.getLife());
		premiumrec.covrCoverage.set(covrmjaIO.getCoverage());
		premiumrec.covrRider.set(covrmjaIO.getRider());
		premiumrec.plnsfx.set(covrmjaIO.getPlanSuffix());
		premiumrec.mop.set(sv.mop);
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.mortcls.set(covrmjaIO.getMortcls());
		/*    MOVE WSAA-MORTCLS           TO CPRM-MORTCLS.                 */
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.effectdt.set(covrmjaIO.getCrrcd());
		premiumrec.termdate.set(covrmjaIO.getPremCessDate());
		premiumrec.calcPrem.set(covrmjaIO.getInstprem());
		if (rerateComp.isTrue()) {
			traceLastRerateDate5500();
			premiumrec.effectdt.set(wsaaLastRerateDate);
			premiumrec.ratingdate.set(wsaaLastRerateDate);
			premiumrec.reRateDate.set(wsaaLastRerateDate);
		}
		else {
			premiumrec.ratingdate.set(covrmjaIO.getCrrcd());
			premiumrec.reRateDate.set(covrmjaIO.getCrrcd());
		}
		premiumrec.jlage.set(ZERO);
		if (rerateComp.isTrue()) {
			agecalcrec.intDate1.set(wsaaMlDob);
			agecalcrec.intDate2.set(wsaaLastRerateDate);
			calcAge5600();
			premiumrec.lage.set(agecalcrec.agerating);
			if (jointLife.isTrue()) {
				agecalcrec.intDate1.set(wsaaJlDob);
				calcAge5600();
				premiumrec.jlage.set(agecalcrec.agerating);
			}
		}
		else {
			premiumrec.lage.set(covrmjaIO.getAnbAtCcd());
			if (jointLife.isTrue()) {
				premiumrec.jlage.set(wsaaJlAnbAtCcd);
			}
		}
		premiumrec.lsex.set(wsaaMlSex);
		if (jointLife.isTrue()) {
			premiumrec.lifeJlife.set("01");
			premiumrec.jlsex.set(wsaaJlSex);
		}
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/* Get Annuity Details.*/
		annyIO.setRecKeyData(SPACES);
		annyIO.setDataKey(covrmjaIO.getDataKey());
		annyIO.setFormat(formatsInner.annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		}
		else {
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		premiumrec.riskCessTerm.set(covrmjaIO.getRiskCessTerm());
		premiumrec.benCessTerm.set(covrmjaIO.getBenCessTerm());
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.language.set(wsspcomn.language);
		/*  Calc Premium Based On Freq.*/
		/*  Call Premium Calc Subr.*/
		premiumrec.sumin.set(wsaaWaiverSumins);
		premiumrec.billfreq.set(sv.billfreq);
		//ILIFE-8502-starts
		premiumrec.setPmexCall.set("Y");
		premiumrec.commTaxInd.set("Y");
		premiumrec.prevSumIns.set(ZERO);
		clntDao = DAOFactory.getClntpfDAO();
		clnt=clntDao.getClientByClntnum(cltsIO.getClntnum().toString());
		if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
			premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
		}else{
			premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
		}
		//ILIFE-8502-end

		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())|| t5675rec.premsubr.toString().trim().equals("PMEX")))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			// ILIFE-3302 [Not able to do Change Client and Life Assured Detail] 
			premiumrec.premMethod.set(wsaaPremMeth);
			premiumrec.validflag.set("Y"); //ILIFE-8502
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		//****Ticket #ILIFE-2005 end
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		premiumrec.riskPrem.set(ZERO);//ILIFE-7845
		calcRiskPrem();//ILIFE-7845
		sv.sumins.set(wsaaWaiverSumins);
		sv.zgrsamt.set(premiumrec.calcPrem);
		sv.basprm.set(premiumrec.calcBasPrem);
		sv.zloprm.set(premiumrec.calcLoaPrem);
		riskprm.set(premiumrec.riskPrem);//ILIFE-7845
		sv.activeInd.set("N");
		if(stampDutyflag) {
			sv.zstpduty.set(premiumrec.zstpduty01);
			wsaaZstpduty01.add(premiumrec.zstpduty01);
			zclstate.add(premiumrec.rstate01.toString());
		}
	}

protected void supdForWopCoverage7370()
	{
		scrnparams.function.set(varcom.supd);
		callSr50mio5900();
	}

protected void calcDuePremium8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					init8010();
				case readSfl8020:
					readSfl8020();
					nextSfl8070();
				case totalDue8080:
					totalDue8080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init8010()
	{
		sv.grsprm.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.cntfee.set(ZERO);
		/*    INITIALIZE                     ZLDC-PARAM-REC*/
		/*                                   WSAA-COMP-PREMS.*/
		iy.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl8020()
	{
		callSr50mio5900a();
		if (isNE(scrnparams.statuz,varcom.oK)) {
			goTo(GotoLabel.totalDue8080);
		}
		/*CALC*/
		sv.grsprm.add(sv.zgrsamt);
		iy.add(1);
		wsaaCovrmjakey.set(sv.datakey);
		if (isLTE(iy,wsaaZldcMax)) {
			/* Nothing to do. */
		}
	}

protected void nextSfl8070()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl8020);
	}

protected void totalDue8080()
	{
		sv.cntfee.set(wsaaContFee);
		compute(sv.instPrem, 2).set(add(sv.grsprm,sv.cntfee,sv.stampduty));
		/*EXIT*/
	}

protected void checkMaxminPrem8100()
	{
		/*START*/
		for (ix.set(1); !(isGT(ix,8)); ix.add(1)){
			if (isEQ(th611rec.frequency[ix.toInt()],sv.billfreq)) {
				if (isLT(sv.instPrem,th611rec.cmin[ix.toInt()])
				|| isGT(sv.instPrem,th611rec.cmax[ix.toInt()])) {
					sv.instprmErr.set(rl11);
				}
			}
		}
		/*EXIT*/
	}

protected void readT56879000()
	{
		start9010();
	}

protected void start9010()
	{
		/* Get General Coverage/Rider Details*/
		initialize(itdmIO.getItemrecKeyData());
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

protected void readT56759100()
	{
		readT56759110();
	}

protected void readT56759110()
	{
		initialize(itemIO.getRecKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5675);
		itemIO.setItemitem(wsaaPremiumMethod);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		//if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		//{
			//premiumrec.premMethod.set(itemIO.getItemitem());
		//}
		/* ILIFE-3142 End*/
		// ILIFE - 3302 [Not able to do Change Client and Life Assured Detail] 
		wsaaPremMeth.set(itemIO.getItemitem());
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

//ILIFE-7845 : Start
protected void calcRiskPrem(){
	boolean riskPremflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), RISKPREM_FEATURE_ID, appVars, "IT");
	if(riskPremflag){
		premiumrec.riskPrem.set(ZERO);
		premiumrec.cnttype.set(chdrmjaIO.getCnttype());
		premiumrec.crtable.set(covrmjaIO.getCrtable());
		premiumrec.calcTotPrem.set(add(premiumrec.zstpduty01, premiumrec.calcPrem));
		if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
		{
			callProgram("RISKPREMIUM", premiumrec.premiumRec);
			covrmjaIO.setRiskprem(premiumrec.riskPrem);
		}
	}				
}

protected void setRcvdData() {
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covrmjaIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covrmjaIO.getChdrnum().toString());
	rcvdPFObject.setLife(covrmjaIO.getLife().toString());
	rcvdPFObject.setCoverage(covrmjaIO.getCoverage().toString());
	rcvdPFObject.setRider(covrmjaIO.getRider().toString());
	rcvdPFObject.setCrtable(covrmjaIO.getCrtable().toString());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		premiumrec.dialdownoption.set(100);
}
//ILIFE-7845 : End
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3620 = new FixedLengthStringData(5).init("T3620");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData th611 = new FixedLengthStringData(5).init("TH611");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData lifemjarec = new FixedLengthStringData(10).init("LIFEMJAREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData annyrec = new FixedLengthStringData(10).init("ANNYREC");
	private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC");
	private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
}
}