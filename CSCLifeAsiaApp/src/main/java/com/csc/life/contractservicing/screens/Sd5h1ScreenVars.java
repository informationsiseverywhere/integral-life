
package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datadictionarydatatype.PackedDDObj;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sd5h1ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(1077);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(517).isAPartOf(dataArea, 0);	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);	
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,64);	
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,130);	
	public ZonedDecimalData susbalnce = DD.sacscurbal.copyToZonedDecimal().isAPartOf(dataFields,177);	
	public ZonedDecimalData repaymentamt = DD.zhpleint.copyToZonedDecimal().isAPartOf(dataFields,194);	
	public ZonedDecimalData totalplprncpl = DD.zhpleamt.copyToZonedDecimal().isAPartOf(dataFields,211);
	public ZonedDecimalData totalplint = DD.zhpleint.copyToZonedDecimal().isAPartOf(dataFields,228);
	public ZonedDecimalData totalaplprncpl = DD.zhpleamt.copyToZonedDecimal().isAPartOf(dataFields,245);
	public ZonedDecimalData totalaplint = DD.zhpleint.copyToZonedDecimal().isAPartOf(dataFields,262);
	public ZonedDecimalData remngplprncpl = DD.zhpleamt.copyToZonedDecimal().isAPartOf(dataFields,279);
	public ZonedDecimalData remngplint = DD.zhpleint.copyToZonedDecimal().isAPartOf(dataFields,296);
	public ZonedDecimalData remngaplprncpl = DD.zhpleamt.copyToZonedDecimal().isAPartOf(dataFields,313);
	public ZonedDecimalData remngaplint = DD.zhpleint.copyToZonedDecimal().isAPartOf(dataFields,330);
	public FixedLengthStringData repymop = DD.repymop.copy().isAPartOf(dataFields,347);	
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,348);	
	public FixedLengthStringData bnkbrndesc = DD.branchdesc.copy().isAPartOf(dataFields,358);	
	public FixedLengthStringData bankclntname= DD.lifename.copy().isAPartOf(dataFields,388);
	
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,435);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,443);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,459);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,467);
	public FixedLengthStringData crcind = DD.crcind.copy().isAPartOf(dataFields,475);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,476);
	public FixedLengthStringData ansrepy = DD.ansrepy.copy().isAPartOf(dataFields,477);
	public FixedLengthStringData refcode = DD.refcode.copy().isAPartOf(dataFields,478);
	public FixedLengthStringData descn = DD.descn.copy().isAPartOf(dataFields,479);	
	public FixedLengthStringData rpcurr = DD.currcd.copy().isAPartOf(dataFields,514);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(140).isAPartOf(dataArea, 517);		
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData susbalnceErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData repaymentamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData totalplprncplErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData totalplintErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData totalaplprncplErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData totalaplintErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData remngplprncplErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData remngplintErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData remngaplprncplErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData remngaplintErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData repymopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData bnkbrndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData bankclntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);	
	
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);	
	public FixedLengthStringData crcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData ansrepyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData refcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData descnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData rpcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(420).isAPartOf(dataArea, 657);	
	public FixedLengthStringData[] chdrnumOut =FLSArrayPartOfStructure(12, 1, outputIndicators, 0);	
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] susbalnceOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] repaymentamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] totalplprncplOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] totalplintOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] totalaplprncplOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] totalaplintOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] remngplprncplOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] remngplintOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] remngaplprncplOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] remngaplintOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] repymopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] bnkbrndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] bankclntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
		
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);	
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);		
	public FixedLengthStringData[] crcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] ansrepyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] refcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] descnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] rpcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(279);
	
	public FixedLengthStringData subfileFields = new FixedLengthStringData(84).isAPartOf(subfileArea, 0);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData hacrint = DD.hacrint.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public ZonedDecimalData hcurbal = DD.hcurbal.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public ZonedDecimalData hpltot = DD.hpltot.copyToZonedDecimal().isAPartOf(subfileFields,18);
	public ZonedDecimalData hpndint = DD.hpndint.copyToZonedDecimal().isAPartOf(subfileFields,27);
	public ZonedDecimalData hprincipal = (new PackedDDObj(17, 2)).copyToZonedDecimal().isAPartOf(subfileFields,34);
	public ZonedDecimalData loanNumber = DD.loannumber.copyToZonedDecimal().isAPartOf(subfileFields,51);	
	public ZonedDecimalData loanstdate = DD.loanstdate.copyToZonedDecimal().isAPartOf(subfileFields,53);
	public FixedLengthStringData loanType = DD.loantype.copy().isAPartOf(subfileFields,61);
	public ZonedDecimalData intrstpercentage = DD.intpersntg.copyToZonedDecimal().isAPartOf(subfileFields,62);
	public ZonedDecimalData repayment = DD.zhpleint.copyToZonedDecimal().isAPartOf(subfileFields,66);
	public FixedLengthStringData repaymentstatus =DD.repmntstat.copy().isAPartOf(subfileFields,83);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 84);

	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData hacrintErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData hcurbalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hpltotErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hpndintErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hprincipalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData loanNumberErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData loanstdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData loantypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData intrstpercentageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData repaymentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData repaymentstatusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(144).isAPartOf(subfileArea, 132);
	
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] hacrintOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] hcurbalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hpltotOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hpndintOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hprincipalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] loanNumberOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] loanstdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] loantypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] intrstpercentageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] repaymentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] repaymentstatusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 276);
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);	
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	//public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData loanstdateDisp = new FixedLengthStringData(10);

	public LongData Sd5h1screensflWritten = new LongData(0);
	public LongData Sd5h1screenctlWritten = new LongData(0);
	public LongData Sd5h1screenWritten = new LongData(0);
	public LongData Sd5h1protectWritten = new LongData(0);
	public GeneralTable sd5h1screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sd5h1screensfl;
	}

	public Sd5h1ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(currcdOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(repymopOut,new String[] {"02","03", "-02","04", null, null, null, null, null, null, null, null});
		fieldIndMap.put(repaymentamtOut,new String[] {"05","06", "-05","07", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankkeyOut,new String[] {"08","09", "-08","10", null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcindOut,new String[] {"12","11", "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"13","11", "-13",null, null, null, null, null, null, null, null, null});
		
		screenSflFields = new BaseData[] {loanNumber, loanType, loanstdate, cntcurr, hprincipal, hcurbal, hacrint, hpndint, hpltot, intrstpercentage,repayment,repaymentstatus};
		screenSflOutFields = new BaseData[][] {loanNumberOut, loantypeOut, loanstdateOut, cntcurrOut, hprincipalOut, hcurbalOut, hacrintOut, hpndintOut, hpltotOut, intrstpercentageOut,repaymentOut,repaymentstatusOut};
		screenSflErrFields = new BaseData[] {loanNumberErr, loantypeErr, loanstdateErr, cntcurrErr, hprincipalErr, hcurbalErr, hacrintErr, hpndintErr, hpltotErr, intrstpercentageErr,repaymentErr,repaymentstatusErr};
		screenSflDateFields = new BaseData[] {loanstdate};
		screenSflDateErrFields = new BaseData[] {loanstdateErr};
		screenSflDateDispFields = new BaseData[] {loanstdateDisp};

		screenFields = new BaseData[] {chdrnum,cnttype,ctypedes,chdrstatus,premstatus,currcd,register,cownnum,ownername,lifenum,lifename,susbalnce,repaymentamt,totalplprncpl,totalplint,totalaplprncpl,totalaplint,remngplprncpl,remngplint,remngaplprncpl,remngaplint,repymop,bankkey,bnkbrndesc,bankclntname,ptdate,effdate,btdate,currfrom,crcind,ddind,ansrepy,refcode,descn,rpcurr};
		screenOutFields = new BaseData[][] {chdrnumOut,cnttypeOut,ctypedesOut,chdrstatusOut,premstatusOut,currcdOut,registerOut,cownnumOut,ownernameOut,lifenumOut,lifenameOut,susbalnceOut,repaymentamtOut,totalplprncplOut,totalplintOut,totalaplprncplOut,totalaplintOut,remngplprncplOut,remngplintOut,remngaplprncplOut,remngaplintOut,repymopOut,bankkeyOut,bnkbrndescOut,bankclntnameOut,ptdateOut,effdateOut,btdateOut,currfromOut,crcindOut,ddindOut,ansrepyOut,refcodeOut,descnOut,rpcurrOut};
		screenErrFields = new BaseData[] {chdrnumErr,cnttypeErr,ctypedesErr,chdrstatusErr,premstatusErr,currcdErr,registerErr,cownnumErr,ownernameErr,lifenumErr,lifenameErr,susbalnceErr,repaymentamtErr,totalplprncplErr,totalplintErr,totalaplprncplErr,totalaplintErr,remngplprncplErr,remngplintErr,remngaplprncplErr,remngaplintErr,repymopErr,bankkeyErr,bnkbrndescErr,bankclntnameErr,ptdateErr,effdateErr,btdateErr,currfromErr,crcindErr,ddindErr,ansrepyErr,refcodeErr,descnErr,rpcurrErr};
		screenDateFields = new BaseData[] {ptdate,effdate,btdate,currfrom};
		screenDateErrFields = new BaseData[] {ptdateErr,effdateErr,btdateErr,currfromErr};
		screenDateDispFields = new BaseData[] {ptdateDisp,effdateDisp,btdateDisp,currfromDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5h1Screen.class;
		screenSflRecord = Sd5h1screensfl.class;
		screenCtlRecord = Sd5h1screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5h1protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5h1screenctl.lrec.pageSubfile);
	}
}
