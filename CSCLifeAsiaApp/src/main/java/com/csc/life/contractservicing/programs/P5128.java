/*
 * File: P5128.java
 * Date: 30 August 2009 0:11:07
 * Author: Quipoz Limited
 * 
 * Class transformed from P5128.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
//ILIFE-8096 start
import java.util.List;
import java.util.Map;

//import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
//ILIFE-8096 end
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtcovTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtridTableDAM;import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.screens.S5128ScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;//ILIFE-8096
import com.csc.life.productdefinition.dataaccess.model.Covrpf;//ILIFE-8096
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5673rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
//ILIFE-8096 start
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
//ILIFE-8096 end
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
//ILIFE-8096 start
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
//ILIFE-8096 end
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                       COMPONENT SELECTION
*
*                       ===================
* Initialise
* ----------
*
* Skip  this  section  if  returning from an optional selection
* (current stack position action flag = '*').
*
* Clear the subfile ready for loading.
*
* Read  CHDRMJA  (RETRV) in order to obtain the contract header
* information  and  read  LIFEMJA  (RETRV) for the life assured
* details.
*
* Read  the  contract definition description from T5688 for the
* contract type held on CHDRMJA.
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*       - using the life  detaiLs  passed (retrieved above from
*         LIFEMJA, joint life number '00').  Look  up the  name
*         from  the  client  details  (CLTS)  and  format as  a
*         "confirmation name".
*
*       - read the joint life details using LIFEMJA (joint life
*         number '01').  If  found, look up  the name  from the
*         client details (CLTS) and  format as  a "confirmation
*         name".
*
* This  program displays component parts for either a life or a
* coverage.  To determine which version is required, attempt to
* retrieve the current coverage transaction (from COVRMJA).  If
* there  is no COVRMJA record to retrieve (MRNF return status),
* coverages  and  riders  for the entire contract are required.
* Otherwise, just riders for the current coverage are required.
*
* Read  the  contract  structure  table  (T5673),  accessed  by
* contract  type  (from  CHDRMJA) and effective on the original
* inception  date  of  the contract. When processing this table
* item,  it  can  continue  onto  additional  (unlimited) table
* items.  These  should  be  read in turn during the processing
* described below.  The  original  inception  date must also be
* used to provide the correct version of each item read.
*
* THE CASE OF ALL COVERAGES AND RIDERS
*
* Taking one coverage at  a  time, one record is written to the
* subfile for the  coverage,  with one for each rider attaching
* to it. The coverage record is written with a blank rider code
* and the rider with a blank coverage code. In the table, there
* is  space for six riders against each coverage. It there is a
* requirement for more than six riders on a coverage, they will
* be  continued  on  the next line.  In this case, the coverage
* will  be  left  blank  to  indicate  a continuation.  NB, the
* coverage  on the last line of a table item could be continued
* onto  another  item.  Look  up  the  long  description of the
* coverage/rider   from  the  general  coverage  details  table
* (T5687) for each subfile record written.
*
* It is possible that the proposal has already been set up when
* a user  requests  this  screen.  Read  the  current  coverage
* transaction   records   for   the  coverage  being  processed
* (COVRCOV,   keyed   company,   contract-no,   coverage  type,
* coverage-no; select  rider-no  = 0). Accumulate the number of
* DIFFERENT coverages  of  the same type already stored. (There
* could be multiple  records  with  the  same  key.  Ignore the
* duplicates.) Whenever  a  transaction is read for a different
* coverage, keep  the  number  of the highest transaction read.
* This is needed  to  calculate  the next coverage number to be
* allocated. (The  coverage  number  needed  is the highest one
* used by any coverage  type.) If there is already at least one
* coverage transaction, put  a  '+'  in  the  select field.  If
* there is already the maximum allowable (as defined in T5673),
* protect the select field so that no more can be requested. If
* the coverage is  mandatory  (this is indicated by an "R", for
* required, in  the  coverage required indicator) and there are
* no transactions for  the  coverage on file, "select" the line
* by moving "X" in to the select field and protect it.
*
* When adding  rider details to  subfile, they may be mandatory
* (indicated in a similar way to mandatory coverages above). In
* this case, "select" the  line  by moving "X" in to the select
* field and protect  it.  -  note  that rider selection will be
* ignored if the coverage is not selected.
*
* Details of all  coverages  and  riders  for the contract type
* should be loaded in this case.
*
* THE CASE OF ALL RIDERS FOR ONE COVERAGE ONLY
*
* In this case, a COVRMJA record will have been retrieved. This
* will hold details of  the  coverage to which riders are to be
* added. Write a coverage  record to the subfile, with a '+' in
* the selection field,  and with this field protected. (Look up
* the description as above.)
*
* Search through  the  contract structure table entries (T5673)
* for the applicable  coverage  code. This will give the riders
* attachable to this  coverage  in this case. Write a record to
* the subfile for each applicable rider.
*
* It is possible that  the riders have already been set up when
* a  user   requests   this  screen.  Read  the  current  rider
* transaction record  for  the  rider being processed (COVRRID,
* keyed  company, contract-no, coverage-no, rider type;  select
* rider-no *ne 0).  If  there  is already a record, this is the
* maximum allowable, so  put  a  '+'  in  the  select field and
* protect it so that no more can be requested.  If the rider is
* mandatory (this is  indicated by an "R", for required, in the
* rider required indicator) and there is no transaction for the
* rider on file, "select"  the  line  by  moving  "X" in to the
* select field and protect it.
*
* Whenever a transaction  is  read  for a different rider, keep
* the number of  the  highest transaction read.  This is needed
* to calculate the next rider number to be allocated.
*
* In all cases, load  all pages required in the subfile and set
* the subfile more indicator to no.
*
* Validation
* ----------
*
* Skip  this  section  if  returning from an optional selection
* (current stack position action flag = '*').
*
* Read  all   modified  subfile  records.  The  only  allowable
* selections are  '+',  'X'  and  blank.  Ignore inconsistences
* between riders being selected for coverages which have not.
*
* Updating
* --------
*
* This program performs no updating.
*
* Next program
* ------------
*
* If returning from  a  selection (action action is '*'), check
* that  if  the  current  component  was  mandatory,  that  his
* actually been entered.  (Use  a  hidden  field in the subfile
* record when loading in  the 1000 section to indicate if it is
* a required  section?)  Read the current coverage/ transaction
* record again (COVRMJA,  the  key  was  set up by this program
* previously.) If  there  is  no  transaction for the mandatory
* component, KEEPS the  current  key  again and exit.
*
*
*
*
*
* If not returning  from  a  component (stack action is blank),
* save the next four  programs currently on the stack. Read the
* first record from  the  subfile. If this is not selected ('X'
* in select field), read  the  next  one  and  so  on,  until a
* selected record is  found,  or  the  end  of  the  subfile is
* reached.
*
* For the case  when  all  coverages  and riders are displayed,
* keep track of whether  a  coverage  is  selected or not. If a
* coverage is not  selected,  skip  all its riders even if they
* are selected.  If  a  coverage  is  selected, re-set the next
* rider sequence number to zero.
*
* For the case  when  only  the  riders  for  one  coverage are
* displayed, the coverage cannot be selected. Process the rider
* selected even though the coverage has not.
*
* Once the end  of  the  subfile  has been reached, restore the
* previously saved  four  programs, blank out the stack action,
* add one to the pointer and exit.
*
* If a subfile  record  has been selected, look up the programs
* required to  be  processed  from  the coverage/rider programs
* table (T5671  -  accessed  by transaction number concatenated
* with coverage/rider code from the subfile record). Move these
* four programs into  the  program  stack  and  set the current
* stack action to '*'  (so  that the system will return to this
* program to process the next one).
*
* Set up the  key details of the coverage/rider to be processed
* (in COVRMJA using  the KEEPS function) by the called programs
* as follows:
*
*       Company - WSSP company
*       Contract no - from CHDRMJA
*       Life number - from LIFEMJA
*       Coverage number -  from COVRMJA if read in the 1000
*            section  (i.e.  adding  to  coverage case), or
*            sequentially from the number calculated in the
*            1000  section for each coverage selected (i.e.
*            adding to life case)
*       Rider number -  '00'  if  this  is  a  coverage, or
*            sequentially from the number calculated above
*
* Add one to  the  program  pointer  and  exit  to  process the
* required generic component.
*
******************Enhancements for Life Asia 1.0****************
*
* In order to allow components to be selected on a specific life,
* a new indicatore has been introduced to T5673. This indicator
* specifies whether the component can be selected for just the
* primary life, or just the subsequent lives or for all lives.
* This module uses the indicator T5673-ZRLIFIND to determine
* which component may be selected. The processing is as follows :
*
* - For each component found on T5673, check the T5673-ZRLIFIND :
*
*   - If we are processing a primary life and the T5673-ZRLIFIND
*     contains an 'S' for that component, then don't set up the
*     component details for display.
*
*   - If we are processing subsequent lives and T5673-ZRLIFIND
*     contains an 'P' for that component, then don't set up the
*     component details for display.
*
*****************************************************************
* </pre>
*/
public class P5128 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5128");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub3 = new PackedDecimalData(2, 0).init(1);
		/*    WSAA-SUB4 and WSAA-SUB5 relate to Program pointers and
		    counts for the handling of the program stack.*/
	private PackedDecimalData wsaaSub4 = new PackedDecimalData(2, 0).init(ZERO);
	private PackedDecimalData wsaaSub5 = new PackedDecimalData(2, 0).init(ZERO);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData sub3 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData ctSub = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData rdSub = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaCcount = new PackedDecimalData(2, 0).init(ZERO);
	private String wsaaNoSelect = "N";
	private String wsaaLifeorcov = "N";
	private FixedLengthStringData wsaaPrevCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRetrvLife = new FixedLengthStringData(2).init(SPACES);
	private final int wsaaMaxIndex = 90;

	private FixedLengthStringData wsaaIfCovrSelected = new FixedLengthStringData(1).init("N");
	private Validator wsaaCovrSelected = new Validator(wsaaIfCovrSelected, "Y");

	private FixedLengthStringData wsaaRiderRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaRider = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiderRider, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaRiderRider, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaRiderR = new FixedLengthStringData(2).isAPartOf(filler, 0);

	private FixedLengthStringData wsaaCoverCover = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaCoverage = new ZonedDecimalData(2, 0).isAPartOf(wsaaCoverCover, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaCoverCover, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaCoverageR = new FixedLengthStringData(2).isAPartOf(filler1, 0);
		/* WSAA-RIDER-NO-TABLE */
	private ZonedDecimalData[] wsaaRidrTab = ZDInittedArray(99, 2, 0, UNSIGNED_TRUE);
	private BinaryData wsaaRidrSub = new BinaryData(3, 0).init(0);
	
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaNextCovridno = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaNextCovno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 0);
	private FixedLengthStringData wsaaNextRidno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 2);

	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaNextCovridno, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaNextCovnoR = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextRidnoR = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2).setUnsigned();
	

	private FixedLengthStringData wsaaLineType = new FixedLengthStringData(1);
	private Validator wsaaLcover = new Validator(wsaaLineType, "1");
	private Validator wsaaCcover = new Validator(wsaaLineType, "2");

	private FixedLengthStringData wsaaCoverFlag = new FixedLengthStringData(1);
	private Validator wsaaCover = new Validator(wsaaCoverFlag, "Y");

	private FixedLengthStringData wsaaExitFlag = new FixedLengthStringData(1);
	private Validator wsaaExit = new Validator(wsaaExitFlag, "Y");

	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");

	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");

	private FixedLengthStringData wsaaProgramSave = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(4, 5, wsaaProgramSave, 0);

	private FixedLengthStringData wsaaConcatName = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrancode = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 4);
	private FixedLengthStringData wsaaLifeKey = new FixedLengthStringData(64);
		/* ERRORS */
	private static final String e005 = "E005";
	private static final String g433 = "G433";
	private static final String h999 = "H999";
	private static final String h248 = "H248";
	private static final String h249 = "H249";
	private static final String f321 = "F321";
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t5673 = "T5673";
	private static final String t5679 = "T5679";
	//ILIFE-1403 START by nnazeer
	private String t5688 = "T5688";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	//ILIFE-1403 END
	
	private IntegerData ctIndex = new IntegerData();
	private IntegerData rdIndex = new IntegerData();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
//ILIFE-8096 start
//	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage Comp Logical View - Major Alts*/
//	private CovrcovTableDAM covrcovIO = new CovrcovTableDAM();
		/*Coverage/Rider details - Major Alts*/
//	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Rider Component Logical View - Major Alt*/
//	private CovrridTableDAM covrridIO = new CovrridTableDAM();
		/*New business transactions - coverages on*/
//	private CovtcovTableDAM covtcovIO = new CovtcovTableDAM();
		/*New business transactions - riders only*/
//	private CovtridTableDAM covtridIO = new CovtridTableDAM();
//ILIFE-8096 end
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5673rec t5673rec = new T5673rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private Wssplife wssplife = new Wssplife();
	private S5128ScreenVars sv = ScreenProgram.getScreenVars( S5128ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT5673StoredTableInner wsaaT5673StoredTableInner = new WsaaT5673StoredTableInner();
//ILIFE-8096 start
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Map<String,Descpf> t5687Map =  new HashMap<String, Descpf>();
	private Map<String,Descpf> t5688Map =  new HashMap<String, Descpf>();
	private Map<String,Descpf> t3623Map =  new HashMap<String, Descpf>();
	private Map<String,Descpf> t3588Map =  new HashMap<String, Descpf>();
	private Map<String, List<Itempf>> t5671ListMap = new HashMap<String, List<Itempf>>();	
	private Map<String, List<Itempf>> t5679ListMap = new HashMap<String, List<Itempf>>();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private List<Covtpf> covtList = new ArrayList<Covtpf>();
	private List<Covtpf> covtcovList = new ArrayList<Covtpf>();
	private List<Covrpf> covrcovList = new ArrayList<Covrpf>();
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private Covtpf covtrid = null;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Clntpf clntpf = null;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
//ILIFE-8096 end
	private List<Covrpf> covrpfLifeList = new ArrayList<Covrpf>();
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1190, 
		subfileLoad1210, 
		exit1490, 
		selectCheck2060, 
		exit2090, 
		exit4190, 
		exit4290, 
		exit4490, 
		exit4590, 
		readProgramTable4610, 
		exit4690, 
		exit4790
	}

	public P5128() {
		super();
		screenVars = sv;
		new ScreenModel("S5128", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

//ILIFE-8096 start
protected void largename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}
//ILIFE-8096 end
protected void initialise1000()
	{
		try {
			initSmartTable(); //ILIFE-8096
			skipOnFlag1010();
			initialiseSubfile1020();
			retreiveHeader1030();
			readContractLongdesc1040();
			retreiveLife1050();
			lifeOrJlife1060();
			coverRiderTable1070();
			retrieveCurrCoverage1080();
			contractTypeStatus1070(); //ILIFE-1403 START by nnazeer
		}
		catch (GOTOException e){
		}
	}
//ILIFE-8096 start
protected void initSmartTable(){
	String coy = wsspcomn.company.toString();
	t5679ListMap = itemDAO.loadSmartTable("IT", coy, "T5679");
	t5671ListMap = itemDAO.loadSmartTable("IT", coy, "T5671");	
	t5687Map = descDAO.getItems("IT", coy, "T5687", wsspcomn.language.toString());
	t5688Map = descDAO.getItems("IT", coy, "T5688", wsspcomn.language.toString());
	t3623Map = descDAO.getItems("IT", coy, "T3623", wsspcomn.language.toString());
	t3588Map = descDAO.getItems("IT", coy, "T3588", wsspcomn.language.toString());
}
//ILIFE-8096 end
protected void skipOnFlag1010()
	{
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsaaBatckey.set(wsspcomn.batchkey);
		/*INIT-RIDR-TABLE*/
		for (wsaaRidrSub.set(1); !(isGT(wsaaRidrSub,99)); wsaaRidrSub.add(1)){
			wsaaRidrTab[wsaaRidrSub.toInt()].set(0);//ILIFE-8096
		}
	}

protected void initialiseSubfile1020()
	{
		wsaaSub1.set(1);
		wsaaSub2.set(1);
		wsaaSub4.set(1);
		wsaaSub5.set(1);
		wsaaLineType.set(ZERO);
		scrnparams.subfileMore.set("N");
		wsaaCoverFlag.set("N");
		wsaaExitFlag.set("N");
		wsaaSelectFlag.set("N");
		wsaaNoSelect = "N";
		wsaaImmexitFlag.set("N");
		wsaaProgramSave.set(SPACES);
		wsaaConcatName.set(SPACES);
		wsaaNextCovno.set("00");
		wsaaNextRidno.set("00");
		wsaaSub1.set(1);
		wsaaSub2.set(1);
		wsaaCcount.set(ZERO);
		wsaaCoverage.set(ZERO);
		wsaaRider.set(ZERO);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaT5673StoredTableInner.wsaaT5673StoredTable.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void retreiveHeader1030()
	{
//ILIFE-8096 start	
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
			&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/

		sv.cntcurr.set(chdrpf.getCntcurr());    //ILIFE-1403 START by nnazeer
		sv.register.set(chdrpf.getReg());  //ILIFE-1403 START by nnazeer
		sv.numpols.set(chdrpf.getPolinc());    //ILIFE-1403 START by nnazeer
//ILIFE-8096 end
	}

protected void readContractLongdesc1040()
	{
//ILIFE-8096 start	
	String longDesc = "";
	if(t5688Map != null){
		if(t5688Map.get(chdrpf.getCnttype()) != null){
			longDesc = t5688Map.get(chdrpf.getCnttype()).getLongdesc();
		}else{
			longDesc = "??????????????????????????????";
		}
	}
	sv.chdrnum.set(chdrpf.getChdrnum());  
	sv.cnttype.set(chdrpf.getCnttype());  
	sv.ctypedes.set(longDesc);
//ILIFE-8096 end
	}

protected void retreiveLife1050()
	{
		lifemjaIO.setFunction(varcom.retrv);
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(chdrpf.getChdrnum()); //ILIFE-8096
		lifeDetails1100();
		wsaaRetrvLife.set(lifemjaIO.getLife());
	}

protected void lifeOrJlife1060()
	{
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		lifeDetails1100();
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemtabl(t5673);
		itdmIO.setItemitem(chdrpf.getCnttype()); //ILIFE-809
	}

protected void coverRiderTable1070()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(chdrpf.getOccdate()); //ILIFE-809
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5673)
		|| (isNE(itdmIO.getItemitem(),chdrpf.getCnttype())
		&& isNE(itdmIO.getItemitem(),t5673rec.gitem))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.chdrnumErr.set(h999);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1090);
		}
		else {
			t5673rec.t5673Rec.set(itdmIO.getGenarea());
		}
		ctSub.set(0);
		rdSub.set(0);
		sub3.set(0);
		for (sub1.set(1); !(isGT(sub1,8)); sub1.add(1)){
			a1100StoreT5673Table();
		}
		readT5679(); 
 //ILIFE-8096
		}
 //ILIFE-8096 start
protected void readT5679() {
	String keyItemitem = wsaaBatckey.batcBatctrcde.toString();
                                                   
	if (t5679ListMap.containsKey(keyItemitem)) {
		List<Itempf> itempfList = t5679ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
                                
			Itempf itempf = iterator.next();
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			break;
		}
	}
}
 //ILIFE-8096 end

protected void retrieveCurrCoverage1080()
	{
		wsaaNextCovnoR.set(0);
		wsaaNextRidnoR.set(0);
		wsaaPrevCoverage.set(SPACES);
 //ILIFE-8096 start
		covrpfList = covrpfDAO.searchCovrmjaByChdrnumCoy(chdrpf.getChdrnum(), chdrpf.getChdrcoy().toString());
		ctIndex.set(1);
		Iterator<Covrpf> icovr = covrpfList.iterator(); 
		boolean breakFlag = false;
		while (isLTE(ctIndex,wsaaMaxIndex) && !breakFlag) {
		breakFlag = a1200LoadSubfileFromCovr(icovr);
 //ILIFE-8096 end
		}
		//commented for Ticket ILIFE-996 by smalchi2 STARTS
		//Uncommented for Ticket ILIFE-2181 by snayeni STARTS
		for (ctIndex.set(1); !(isGT(ctIndex,wsaaMaxIndex)
		|| isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], SPACES)); ctIndex.add(1)){
			a1500LoadSubfileFromT5673();
		}
		//Uncommented for Ticket ILIFE-2181 by snayeni END
		//ENDS
		Covrpf tempCovrpf = new Covrpf();
		tempCovrpf.setChdrnum(chdrpf.getChdrnum());
		tempCovrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		tempCovrpf.setLife(lifemjaIO.getLife().toString());
		covrpfLifeList = covrpfDAO.selectCoverage(tempCovrpf);
		if(covrpfLifeList.isEmpty()) {
			wsaaNextCovno.set(0);
		}
		for(Covrpf c : covrpfLifeList) {
			wsaaNextCovno.set(c.getCoverage());
		}
	}

//ILIFE-1403 START by nnazeer
protected void contractTypeStatus1070()
{
 //ILIFE-8096 start
	if(t5688Map != null){
		if(t5688Map.get(chdrpf.getCnttype()) != null){
			sv.ctypedes.set(t5688Map.get(chdrpf.getCnttype()).getLongdesc());
		}else{
			sv.ctypedes.fill("?");
		}
	}
	if(t3623Map != null){
		if(t3623Map.get(chdrpf.getStatcode()) != null){
			sv.chdrstatus.set(t3623Map.get(chdrpf.getStatcode()).getShortdesc());
		}else{
			sv.chdrstatus.fill("?");
		}
	}
	if(t3588Map != null){
		if(t3588Map.get(chdrpf.getPstcde()) != null){
			sv.premstatus.set(t3588Map.get(chdrpf.getPstcde()).getShortdesc());
		}else{
			sv.premstatus.fill("?");
		}
 //ILIFE-8096 end
	}
}



protected void lifeDetails1100()
	{
		try {
			readLife1110();
			retreiveClientDetails1120();
			readLifeJlife1140();
		}
		catch (GOTOException e){
		}
	}

protected void readLife1110()
	{
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1190);
		}
		if (isNE(lifemjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(lifemjaIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			lifemjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
	}

protected void retreiveClientDetails1120()
	{
 //ILIFE-8096 start
	clntpf = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),lifemjaIO.getLifcnum().toString());
	plainname();
 //ILIFE-8096 end	
	}

protected void readLifeJlife1140()
	{
		if (isEQ(lifemjaIO.getJlife(),"00")
		|| isEQ(lifemjaIO.getJlife(), "  ")) {
			lifeToScreen1150();
		}
		else {
			jlifeToScreen1160();
		}
		goTo(GotoLabel.exit1190);
	}

protected void lifeToScreen1150()
	{
		sv.life.set(lifemjaIO.getLife());
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		sv.linsname.set(wsspcomn.longconfname);
		sv.jlife.set(SPACES);
	}

protected void jlifeToScreen1160()
	{
		sv.jlife.set(lifemjaIO.getJlife());
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void a1100StoreT5673Table()
	{
 //ILIFE-8096 start
	if (isNE(t5673rec.ctable[sub1.toInt()],SPACES)) {
		ctSub.add(1);
		rdSub.set(1);
		wsaaT5673StoredTableInner.wsaaZrlifind[ctSub.toInt()].set(t5673rec.zrlifind[sub1.toInt()]);
		wsaaT5673StoredTableInner.wsaaCreq[ctSub.toInt()].set(t5673rec.creq[sub1.toInt()]);
		wsaaT5673StoredTableInner.wsaaCtable[ctSub.toInt()].set(t5673rec.ctable[sub1.toInt()]);
		/*      MOVE T5673-CTMAXCOV (SUB1) TO WSAA-CTMAXCOV (CT-SUB).   */
		wsaaT5673StoredTableInner.wsaaCtmaxcov[ctSub.toInt()].set(t5673rec.ctmaxcov[sub1.toInt()]);
		wsaaT5673StoredTableInner.wsaaNoOfCovs[ctSub.toInt()].set(ZERO);
	}
	for (sub2.set(1); !(isGT(sub2,6)); sub2.add(1)){
		sub3.add(1);
		if (isEQ(t5673rec.rtable[sub3.toInt()],SPACES)) {
			continue;
		}
		wsaaT5673StoredTableInner.wsaaRreq[ctSub.toInt()][rdSub.toInt()].set(t5673rec.rreq[sub3.toInt()]);
		wsaaT5673StoredTableInner.wsaaRtable[ctSub.toInt()][rdSub.toInt()].set(t5673rec.rtable[sub3.toInt()]);
		rdSub.add(1);
	}
	
	if (isEQ(sub1,8)) {
		if (isNE(t5673rec.gitem,SPACES)) {
			itdmIO.setStatuz(varcom.oK);
			itdmIO.setItemitem(t5673rec.gitem);
			itdmIO.setFunction("BEGN");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
			if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
			|| isNE(itdmIO.getItemtabl(),t5673)
			|| isNE(itdmIO.getItemitem(),t5673rec.gitem)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				sv.chdrnumErr.set(h999);
				wsspcomn.edterror.set("Y");
			}
			t5673rec.t5673Rec.set(itdmIO.getGenarea());
			sub1.set(0);
			sub3.set(0);
		}
	}
	}


protected boolean a1200LoadSubfileFromCovr(Iterator<Covrpf> icovrmjaIO)
	{
		boolean breakFlag = false;
		//Covrpf covrmjaIO = null;
		if (!icovrmjaIO.hasNext()){
			breakFlag = true;
		}else{
			covrpf = icovrmjaIO.next();
		}
		if(breakFlag || (isNE(covrpf.getCoverage(),wsaaPrevCoverage)
				&& isNE(wsaaPrevCoverage, "  ")) ){
			for (ctIndex.set(1); !(isGT(ctIndex,wsaaMaxIndex)
			|| isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], SPACES)); ctIndex.add(1)){
				a1300CoverToScreen(covrpf);
			}
			if (breakFlag) {
				ctIndex.set(99);
				return true;
			}
		}
		wsaaRidrSub.set(covrpf.getCoverage());
		if (isGT(covrpf.getRider(),wsaaRidrTab[wsaaRidrSub.toInt()])) {
			wsaaRidrTab[wsaaRidrSub.toInt()].set(covrpf.getRider());
		}
		if (isGT(covrpf.getCoverage(),wsaaNextCovno)) {
			wsaaNextCovno.set(covrpf.getCoverage());
		}
		if (isEQ(covrpf.getCoverage(),wsaaPrevCoverage)) {
			for (rdIndex.set(1); !(isGT(rdIndex,wsaaMaxIndex)
			|| isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], SPACES)); rdIndex.add(1)){
				if (isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], covrpf.getCrtable())) {
					wsaaT5673StoredTableInner.wsaaRtableUsed[ctIndex.toInt()][rdIndex.toInt()].set("Y");
					wsaaT5673StoredTableInner.wsaaRtableRisk[ctIndex.toInt()][rdIndex.toInt()].set(covrpf.getStatcode());
					rdIndex.set(99);
				}
			}
		}
		else {
			ctIndex.set(1);
			 searchlabel1:
			{
				for (; isLT(ctIndex, wsaaT5673StoredTableInner.wsaaT5673Info.length); ctIndex.add(1)){
					if (isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], covrpf.getCrtable())) {
						wsaaT5673StoredTableInner.wsaaCtableUsed[ctIndex.toInt()].set("Y");
						break searchlabel1;
					}
				}
				sv.crcodeErr.set(h249);
				return false;
			}
		}
		/*     GO TO A1290-EXIT.                                        */
		wsaaPrevCoverage.set(covrpf.getCoverage());
		return false;
	}
 //ILIFE-8096 end
 //ILIFE-8096 start
protected void a1300CoverToScreen(Covrpf covrmjaIO)
	{
			//a1310CoverToLine();
		if(isNE(wsaaRetrvLife,covrmjaIO.getLife())) { //ILIFE-8365
		return;
		}
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcodeOut[varcom.nd.toInt()].set(SPACES);
		/*   If the coverage appears on the contract already, we           */
		/*   display it and keep a coverage count to compare               */
		/*   against the maximum later.<                                   */
		if (isEQ(wsaaT5673StoredTableInner.wsaaCtableUsed[ctIndex.toInt()], "Y")) {
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaT5673StoredTableInner.wsaaNoOfCovs[ctIndex.toInt()].add(1);
		}
		else {
			return ;
		}
		String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]) != null){
				longDesc = t5687Map.get(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}
		
		
		sv.crcode.set(covrmjaIO.getCrtable());
		sv.crcode.set(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]);
		sv.longdesc.set(longDesc);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		for (rdIndex.set(1); !(isGT(rdIndex,wsaaMaxIndex)
		|| isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], SPACES)); rdIndex.add(1)){
			a1400AsocRiderToScreen(covrmjaIO);
		}
		wsaaT5673StoredTableInner.wsaaCtableUsed[ctIndex.toInt()].set(SPACES);
		}

/*protected void a1310CoverToLine()
	{
	sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcodeOut[varcom.nd.toInt()].set(SPACES);
		   If the coverage appears on the contract already, we           
		   display it and keep a coverage count to compare               
		   against the maximum later.<                                   
		if (isEQ(wsaaT5673StoredTableInner.wsaaCtableUsed[ctIndex.toInt()], "Y")) {
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaT5673StoredTableInner.wsaaNoOfCovs[ctIndex.toInt()].add(1);
		}
		else {
			return ;
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5687");
		descIO.setDescitem(covrmjaIO.getCrtable());
		descIO.setDescitem(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.crcode.set(covrmjaIO.getCrtable());
		sv.crcode.set(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]);
		sv.longdesc.set(descIO.getLongdesc());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		for (rdIndex.set(1); !(isGT(rdIndex,wsaaMaxIndex)
		|| isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], SPACES)); rdIndex.add(1)){
			a1400AsocRiderToScreen();
		}
		wsaaT5673StoredTableInner.wsaaCtableUsed[ctIndex.toInt()].set(SPACES);
	}*/
 //ILIFE-8096 end

 //ILIFE-8096 start
protected void a1400AsocRiderToScreen(Covrpf covrmjaIO)
	{
		sv.rtable.set(SPACES);
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcode.set(covrmjaIO.getCoverage());
		sv.crcode.set(wsaaPrevCoverage);
		sv.crcodeOut[varcom.nd.toInt()].set("Y");
		sv.rtable.set(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]);
		if (isEQ(wsaaT5673StoredTableInner.wsaaRtableUsed[ctIndex.toInt()][rdIndex.toInt()], "Y")) {
			wsaaSub3.set(1);
			while ( !((isGT(wsaaSub3,12)
			|| (isEQ(t5679rec.ridRiskStat[wsaaSub3.toInt()], wsaaT5673StoredTableInner.wsaaRtableRisk[ctIndex.toInt()][rdIndex.toInt()]))))) {
				wsaaSub3.add(1);
			}
			
			if (isGT(wsaaSub3,12)) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.select.set("+");
				sv.selectOut[varcom.pr.toInt()].set("Y");
				wsaaT5673StoredTableInner.wsaaRtableUsed[ctIndex.toInt()][rdIndex.toInt()].set(SPACES);
			}
		}
		String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]) != null){
				longDesc = t5687Map.get(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}
		sv.longdesc.set(longDesc);
 //ILIFE-8096 end		scrnparams.function.set(varcom.sadd);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void a1500LoadSubfileFromT5673()
	{
		/*A1510-START*/
		if (isEQ(wsaaT5673StoredTableInner.wsaaZrlifind[ctIndex.toInt()], "P")
		&& isNE(wsaaRetrvLife,"01")) {
			return ;
		}
		if (isEQ(wsaaT5673StoredTableInner.wsaaZrlifind[ctIndex.toInt()], "S")
		&& isEQ(wsaaRetrvLife,"01")) {
			return ;
		}
		
		
		/**
		 *  For ticket ILIFE-4290
		 *  Skip display of coverage which already on cover
		 */
				
		/*for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
			if (isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()],t5673rec.ctable[wsaaSub1.toInt()] )
			&& isLT(wsaaT5673StoredTableInner.wsaaCtmaxcov[ctIndex.toInt()], 100)) {
				return ;
			}
		}*/
		
		if(covrpf.getCoverage().trim().equals(wsaaT5673StoredTableInner.wsaaCtmaxcov[ctIndex.toInt()].toString().trim())){/* IJTI-1523 */
			return;
		}	
				
		a1600TableCoverToScreen();
		for (rdIndex.set(1); !(isGT(rdIndex,wsaaMaxIndex)
		|| isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], SPACES)); rdIndex.add(1)){
			a1700AsocRiderToScreen();
		}
	}

protected void a1600TableCoverToScreen()
/*	{
		a1610CoverToLine();
	}

protected void a1610CoverToLine()*/
	{
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcodeOut[varcom.nd.toInt()].set(SPACES);
 //ILIFE-8096 start
		String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]) != null){
				longDesc = t5687Map.get(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}
		sv.crcode.set(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]);
		sv.rtable.set(SPACES);
		sv.longdesc.set(longDesc);
 //ILIFE-8096 end
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void a1700AsocRiderToScreen()
	{
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcodeOut[varcom.nd.toInt()].set(SPACES);
		sv.rtable.set(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]);
		/*    Reference Rider Long description  and output details to the*/
		/*    Screen.*/
 //ILIFE-8096 start
		String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]) != null){
				longDesc = t5687Map.get(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}
		sv.longdesc.set(longDesc);
 //ILIFE-8096 end		scrnparams.function.set(varcom.sadd);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case subfileLoad1210: 
					subfileLoad1210();
				case exit1490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void subfileLoad1210()
	{
 //ILIFE-8096 start
		/*if (isEQ(covrmjaIO.getStatuz(),varcom.oK)) {
			coverageTrans1410();
		}*/
		if(covrpf != null){
			coverageTrans1410();
 //ILIFE-8096 end
		}
		else {
			lifeTrans1310();
		}
		if (isGT(wsaaSub1,8)
		&& isGT(wsaaSub2,wsaaMaxIndex)) {
			if (isNE(t5673rec.gitem,SPACES)) {
				itdmIO.setItemitem(t5673rec.gitem);
				wsaaSub1.set(1);
				wsaaSub2.set(1);
				scrnparams.subfileMore.set("Y");
				coverRiderTable1070();
				if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
					goTo(GotoLabel.subfileLoad1210);
				}
				else {
					scrnparams.subfileMore.set("N");
				}
			}
			else {
				scrnparams.subfileMore.set("N");
			}
		}
		goTo(GotoLabel.exit1490);
	}

protected void lifeTrans1310()
	{
		if (wsaaLcover.isTrue()) {
			checkCover1330();
			wsaaLineType.set("3");
		}
		else {
			for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
				riderFile1370();
			}
			wsaaLineType.set("1");
		}
		if (isLTE(wsaaSub1,8)
		&& isLTE(wsaaSub2,wsaaMaxIndex)) {
			endSubfile1320();
		}
	}

protected void endSubfile1320()
	{
 //ILIFE-8096 start
	if (isEQ(t5673rec.ctable[wsaaSub1.toInt()],SPACES)
			&& isEQ(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
				scrnparams.subfileMore.set("N");
			}
	else {
			scrnparams.subfileMore.set("Y");
		}
 //ILIFE-8096 end
	}

protected void checkCover1330()
	{
	wsaaExitFlag.set("N");
	wsaaNoSelect = "N";
	if (isNE(t5673rec.ctable[wsaaSub1.toInt()],SPACES)) {
		wsaaCcount.set(ZERO);
 //ILIFE-8096 start
		covrpfList = covrpfDAO.getCovrcovData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), lifemjaIO.getLife().toString(), t5673rec.ctable[wsaaSub1.toInt()].toString());
		if(covrpfList != null && covrpfList.size() >0){
			for(int i=0; i < covrpfList.size() || !(wsaaExit.isTrue()); i++ ){
				if(covrpfList.get(i) == null){
					fatalError600();
				}
				if(i == covrpfList.size()){
					checkCovtcov1360();
				} else {
					if (isEQ(covrpfList.get(i).getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
						wsaaCcount.add(1);
					}
				}
				if (isGT(covrpfList.get(i).getCoverage(),wsaaNextCovno)) {
					wsaaNextCovno.set(covrpfList.get(i).getCoverage());
				}
			}
		}
		coverValidation1350();
		coverToScreen1600();
	}
	wsaaSub1.add(1);
	}

/*protected void checkCovrcov1340()
	{
		SmartFileCode.execute(appVars, covrcovIO);
		if (isNE(covrcovIO.getStatuz(),varcom.oK)
		&& isNE(covrcovIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrcovIO.getParams());
			fatalError600();
		}
		if (isNE(covrcovIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(covrcovIO.getLife(),lifemjaIO.getLife())
		|| isEQ(covrcovIO.getStatuz(),varcom.endp)) {
			covtcovIO.setDataKey(SPACES);
			covtcovIO.setChdrcoy(chdrmjaIO.getChdrcoy());
			covtcovIO.setChdrnum(chdrmjaIO.getChdrnum());
			covtcovIO.setLife(lifemjaIO.getLife());
			covtcovIO.setCrtable(t5673rec.ctable[wsaaSub1.toInt()]);
			covtcovIO.setFunction("BEGN");
			while ( !(wsaaExit.isTrue())) {
				checkCovtcov1360();
			}
			
		}
		else {
			if (isEQ(covrcovIO.getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
				wsaaCcount.add(1);
			}
		}
		if (isGT(covrcovIO.getCoverage(),wsaaNextCovno)) {
			wsaaNextCovno.set(covrcovIO.getCoverage());
		}
		covrcovIO.setFunction(varcom.nextr);
		
	}
*/
 //ILIFE-8096 end
protected void coverValidation1350()
	{
		if (isGT(wsaaCcount,ZERO)) {
			sv.select.set("+");
		}
		if (isEQ(t5673rec.ctmaxcov[wsaaSub1.toInt()],wsaaCcount)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaNoSelect = "Y";
		}
		if (isEQ(t5673rec.creq[wsaaSub1.toInt()],"R")
		&& isLT(wsaaCcount,t5673rec.ctmaxcov[wsaaSub1.toInt()])
		&& isNE(sv.select,"+")) {
			sv.select.set("X");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.hrequired.set("Y");
		}
	}

protected void checkCovtcov1360()
	{
 //ILIFE-8096 start
	covtcovList = covtpfDAO.getCovtcovData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), lifemjaIO.getLife().toString(), t5673rec.ctable[wsaaSub1.toInt()].toString());/* IJTI-1523 */
	if(covtcovList != null && covtcovList.size() > 0){
		for(int i=0; i < covtcovList.size() || !(wsaaExit.isTrue()); i++ ){
			if(covtcovList.get(i) == null && i != covtcovList.size()){
				fatalError600();
			}
			if(i == covtcovList.size()){
				wsaaExitFlag.set("Y");
			}else{
				if (isEQ(covtcovList.get(i).getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
					wsaaCcount.add(1);
				}
			}
			if (isGT(covtcovList.get(i).getCoverage(),wsaaNextCovno)) {
				wsaaNextCovno.set(covtcovList.get(i).getCoverage());
			}
		}
		}
	  }
 //ILIFE-8096 end	}

protected void riderFile1370()
	{
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		if (isNE(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
			riderToScreen1700();
		}
		wsaaSub2.add(1);
	}

protected void coverageTrans1410()
	{
		if (wsaaCcover.isTrue()) {
			for (wsaaSub1.set(1); !(wsaaExit.isTrue()); wsaaSub1.add(1)){
				findCover1420();
			}
		}
		else {
			riderInfo1430();
		}
	}

protected void findCover1420()
	{
		wsaaCoverageR.set(covrpf.getCoverage()); //ILIFE-8096
		wsaaNextRidno.set(ZERO);
		if (isEQ(wsaaNoSelect,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(covrpf.getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
			sv.select.set("+");
			wsaaExitFlag.set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			coverToScreen1600();
			wsaaLineType.set("3");
		}
		else {
			wsaaSub2.add(6);
		}
		if (isGT(wsaaSub1,8)) {
			wsaaExitFlag.set("Y");
		}
	}

protected void riderInfo1430()
	{
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		if (isEQ(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
			scrnparams.subfileMore.set("N");
			wsaaLineType.set("2");
		}
		else {
			scrnparams.subfileMore.set("Y");
			riderCheck1440();
			riderToScreen1700();
		}
		wsaaSub2.add(1);
	}

protected void riderCheck1440()
	{
 //ILIFE-8096 start
		covrpf = covrpfDAO.getCovrridData(wsspcomn.company.toString(), chdrpf.getChdrnum(), lifemjaIO.getLife().toString(), 
				covrpf.getCoverage(),t5673rec.rtable[wsaaSub2.toInt()].toString());/* IJTI-1523 */
		if (covrpf != null) {
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaRidrSub.set(covrpf.getCoverage());
			if (isGT(covrpf.getRider(),wsaaRidrTab[wsaaRidrSub.toInt()])) {
				wsaaRidrTab[wsaaRidrSub.toInt()].set(covrpf.getRider());
			}
		}
		else {
			covtRiderCheck1450();
		}
		 //ILIFE-8096 end
	}

protected void covtRiderCheck1450()
	{
 //ILIFE-8096 start
		covtrid = covtpfDAO.getCovtridData(wsspcomn.company.toString(), chdrpf.getChdrnum(), lifemjaIO.getLife().toString(), 
				covrpf.getCoverage(),t5673rec.rtable[wsaaSub2.toInt()].toString());/* IJTI-1523 */
		if (covtrid != null) {
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaRidrSub.set(covtrid.getCoverage());
			if (isGT(covtrid.getRider(),wsaaRidrTab[wsaaRidrSub.toInt()])) {
				wsaaRidrTab[wsaaRidrSub.toInt()].set(covtrid.getRider());
 //ILIFE-8096 end
			}
		}
	}

protected void coverToScreen1600()
	{
		coverToLine1610();
	}

protected void coverToLine1610()
	{
 //ILIFE-8096 start
		String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(t5673rec.ctable[wsaaSub1.toInt()]) != null){
				longDesc = t5687Map.get(t5673rec.ctable[wsaaSub1.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}
		sv.crcode.set(t5673rec.ctable[wsaaSub1.toInt()]);
		sv.longdesc.set(longDesc);
 //ILIFE-8096 end
		scrnparams.function.set(varcom.sadd);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void riderToScreen1700()
	{
		riderToLine1710();
	}

protected void riderToLine1710()
	{
		sv.rtable.set(SPACES);
		if (isEQ(t5673rec.rreq[wsaaSub2.toInt()],"R")) {
			sv.select.set("X");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.hrequired.set("Y");
		}
		sv.crcode.set(SPACES);
		sv.rtable.set(t5673rec.rtable[wsaaSub2.toInt()]);
		/*    Reference Rider Long description  and output details to the*/
		/*    Screen.*/
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5687");
 //ILIFE-8096 start
		String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(t5673rec.rtable[wsaaSub2.toInt()]) != null){
				longDesc = t5687Map.get(t5673rec.rtable[wsaaSub2.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}
		sv.longdesc.set(longDesc);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

/*protected void callCovrmjaio1800()
	{
		START
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(),wsaaRetrvLife)
		|| isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		EXIT
	}*/
 //ILIFE-8096 end
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    Skip this section  if returning  from an optional selection  */
		/*    (current stack position action flag = '*').                  */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateSubfile2050();
				case selectCheck2060: 
					selectCheck2060();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*ROLL-UP*/
		/*    Check if another page has been requested.*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)
		|| isEQ(scrnparams.statuz,varcom.rold)) {
			validateSubfile2050(); //ILIFE-8096
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSubfile2050()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2090);
		}
		wsaaIfCovrSelected.set("N");
	}

protected void selectCheck2060()
	{
		/*    Validate  Selection being  one  of the following conditions.*/
		/*    IF Valid then go to 4000-next-program.*/
		if (isEQ(sv.select, " ")
		|| isEQ(sv.select,"+")
		|| isEQ(sv.select,"X")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.selectErr.set(e005);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.select,"X")) {
			if (isEQ(sv.rtable,SPACES)) {
				wsaaIfCovrSelected.set("Y");
			}
		}
		if (isNE(sv.crcode,SPACES)) {
			ctIndex.set(1);
			 searchlabel1:
			{
				for (; isLT(ctIndex, wsaaT5673StoredTableInner.wsaaT5673Info.length); ctIndex.add(1)){
					if (isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], sv.crcode)) {
						if (isEQ(wsaaT5673StoredTableInner.wsaaNoOfCovs[ctIndex.toInt()], wsaaT5673StoredTableInner.wsaaCtmaxcov[ctIndex.toInt()])) {
							sv.selectErr.set(h248);
							wsspcomn.edterror.set("Y");
						}
						break searchlabel1;
					}
				}
				/*CONTINUE_STMT*/
			}
		}
		if (isEQ(sv.select,"X")) {
			if (isNE(sv.rtable,SPACES)
			&& isEQ(sv.crcode,SPACES)) {
				if (!wsaaCovrSelected.isTrue()) {
					sv.selectErr.set(e005);
					wsspcomn.edterror.set("Y");
				}
			}
		}
		if (isNE(wsspcomn.edterror,varcom.oK)) {
			scrnparams.function.set(varcom.supd);
			processScreen("S5128", sv);
		}
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srnch);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.selectCheck2060);
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		return ;
		/*EXIT*/
	}

protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			initSelection4100();
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.function.set(varcom.srdn);
			if (isEQ(sv.hrequired,"Y")) {
				verifyComponent4200();
			}
		}
		if (wsaaImmExit.isTrue()) {
			return ;
		}
		verifyNextSelect4300();
		if (wsaaImmExit.isTrue()) {
			return ;
		}
		coverRiderProgs4500();
		readProgramTables4600();
		screenUpdate4800();
	}

protected void initSelection4100()
	{
 //ILIFE-8096 start
		/*try {
			saveNextProgs4110();
		}
		catch (GOTOException e){
		}*/
	compute(wsaaSub4, 0).set(add(1,wsspcomn.programPtr));
	wsaaSub5.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
		/*    This loop will load the next four  programs from WSSP Stack*/
		/*    into a Working Storage Save area.*/
		wsaaSecProg[wsaaSub5.toInt()].set(wsspcomn.secProg[wsaaSub4.toInt()]);
		wsaaSub4.add(1);
		wsaaSub5.add(1);
	}
	/*    First Read of the Subfile for a Selection.*/
	scrnparams.function.set(varcom.sstrt);
	}

/*protected void saveNextProgs4110()
	{
		compute(wsaaSub4, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub5.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loop14120();
		}
		scrnparams.function.set(varcom.sstrt);
		goTo(GotoLabel.exit4190);
	}*/

/*protected void loop14120()
	{
		wsaaSecProg[wsaaSub5.toInt()].set(wsspcomn.secProg[wsaaSub4.toInt()]);
		wsaaSub4.add(1);
		wsaaSub5.add(1);
	}*/

protected void verifyComponent4200()
	{
	wsaaExitFlag.set("N");
	wsaaImmexitFlag.set("N");
	covrpfList = covrpfDAO.searchValidCovrpfRecord(wsspcomn.company.toString(),chdrpf.getChdrnum(),lifemjaIO.getLife().toString(),wsaaCoverageR.toString(),wsaaRiderR.toString());

	/*    IF the transaction has not been created then Keep the*/
	/*       transaction key and pass an error message to the*/
	/*       previous program, then exit.*/
	if (covrpfList == null || covrpfList.isEmpty()) {
		wsspcomn.msgarea.set(" MANDATORY TRANSACTION - PLEASE COMPLETE.");
		wsaaImmexitFlag.set("Y");
		readProgramTables4600();
	}
	wsaaExitFlag.set("N");
	/*    IF the mandatory Selection was not created then exit*/
	/*       to the previous program.*/
	/*    ELSE if OK then find next selection from Subfile.*/
	goTo(GotoLabel.exit4290);
	}

/*protected void verifyReqTrans4210()
	{
		wsaaExitFlag.set("N");
		wsaaImmexitFlag.set("N");
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(lifemjaIO.getLife());
		covrmjaIO.setCoverage(wsaaCoverageR);
		covrmjaIO.setRider(wsaaRiderR);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE");
		while ( !(wsaaExit.isTrue())) {
			verifyTrans4220();
		}
		
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			wsspcomn.msgarea.set(" MANDATORY TRANSACTION - PLEASE COMPLETE.");
			wsaaImmexitFlag.set("Y");
			readProgramTables4600();
		}
		
	}*/

/*protected void verifyTrans4220()
	{
	SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covrmjaIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(),lifemjaIO.getLife())
		|| isNE(covrmjaIO.getCoverage(),wsaaCoverageR)
		|| isNE(covrmjaIO.getRider(),wsaaRiderR)
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			wsaaExitFlag.set("Y");
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.oK)) {
			wsaaExitFlag.set("Y");
		}
		covrmjaIO.setFunction(varcom.nextr);
	}*/
 //ILIFE-8096 end
protected void verifyNextSelect4300()
	{
		/*VERIFY*/
		wsaaSelectFlag.set("N");
		wsaaImmexitFlag.set("N");
		while ( !(wsaaSelection.isTrue()
		|| wsaaImmExit.isTrue())) {
			next4400();
		}
		
		/*EXIT*/
	}

protected void next4400()
	{
		try {
			nextRec4410();
			ifSubfileEndp4420();
		}
		catch (GOTOException e){
		}
	}

protected void nextRec4410()
	{
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4420()
	{
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			reloadProgsWssp4430();
			return; //ILIFE-8096
		}
		if (isEQ(sv.rtable,SPACES)) {
			wsaaCoverFlag.set("N");
		}
		if (isEQ(sv.select,"X")) {
			wsaaSelectFlag.set("Y");
			if (isEQ(sv.rtable,SPACES)) {
				wsaaCoverFlag.set("Y");
			}
		}
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.exit4490);
	}

protected void reloadProgsWssp4430()
	{
		wsaaImmexitFlag.set("Y");
		compute(wsaaSub4, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub5.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
 //ILIFE-8096 start
			/*    Re-load the Saved four programs from Working Storage to*/
			/*    the WSSP Program Stack.*/
			wsspcomn.secProg[wsaaSub4.toInt()].set(wsaaSecProg[wsaaSub5.toInt()]);
			wsaaSub4.add(1);
			wsaaSub5.add(1);
		}
		/*    Set action flag to ' ' in order that the program will*/
		/*    resume initial stack order and go to the next program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.programPtr.add(1);
	}

/*protected void loop34440()
	{
		wsspcomn.secProg[wsaaSub4.toInt()].set(wsaaSecProg[wsaaSub5.toInt()]);
		wsaaSub4.add(1);
		wsaaSub5.add(1);
	}*/ //ILIFE-8096 end
protected void coverRiderProgs4500()
	{
		try {
			initCovrmja4510();
			coverOrRider4520();
		}
		catch (GOTOException e){
		}
	}

protected void initCovrmja4510()
	{
 //ILIFE-8096 start
	  //covrmjaIO.setDataArea(SPACES);
		covrpf.setPlanSuffix(0);
		covrpf.setPayrseqno(1);
		covrpf.setCrInstamt01(new BigDecimal(0));
		covrpf.setCrInstamt02(new BigDecimal(0));
		covrpf.setCrInstamt03(new BigDecimal(0));
		covrpf.setCrInstamt04(new BigDecimal(0));
		covrpf.setCrInstamt05(new BigDecimal(0));
		covrpf.setEstMatValue01(new BigDecimal(0));
		covrpf.setEstMatValue02(new BigDecimal(0));
		covrpf.setEstMatDate01(0);
		covrpf.setEstMatDate02(0);
		covrpf.setEstMatInt01(new BigDecimal(0));
		covrpf.setEstMatInt02(new BigDecimal(0));
		covrpf.setTranno(0);
		covrpf.setCurrfrom(0);
		covrpf.setCurrto(0);
		covrpf.setCrrcd(0);
		covrpf.setAnbAtCcd(0);
		covrpf.setRiskCessDate(0);
		covrpf.setPremCessDate(0);
		covrpf.setBenCessDate(0);
		covrpf.setNextActDate(0);
		covrpf.setReserveUnitsDate(0);
		covrpf.setConvertInitialUnits(0);
		covrpf.setReviewProcessing(0);
		covrpf.setUnitStatementDate(0);
		covrpf.setCpiDate(0);
		covrpf.setInitUnitCancDate(0);
		covrpf.setExtraAllocDate(0);
		covrpf.setInitUnitIncrsDate(0);
		covrpf.setRiskCessAge(0);
		covrpf.setPremCessAge(0);
		covrpf.setBenCessAge(0);
		covrpf.setRiskCessTerm(0);
		covrpf.setPremCessTerm(0);
		covrpf.setBenCessTerm(0);
		covrpf.setSumins(new BigDecimal(0));
		covrpf.setVarSumInsured(new BigDecimal(0));
		covrpf.setDeferPerdAmt(new BigDecimal(0));
		covrpf.setTotMthlyBenefit(new BigDecimal(0));
		covrpf.setSingp(new BigDecimal(0));
		covrpf.setInstprem(new BigDecimal(0));
		covrpf.setStatSumins(new BigDecimal(0));
		covrpf.setRtrnyrs(0);
		covrpf.setPremCessAgeMth(0);
		covrpf.setPremCessAgeDay(0);
		covrpf.setPremCessTermMth(0);
		covrpf.setPremCessTermDay(0);
		covrpf.setRiskCessAgeMth(0);
		covrpf.setRiskCessAgeDay(0);
		covrpf.setRiskCessTermMth(0);
		covrpf.setRiskCessTermDay(0);
		covrpf.setRerateDate(0);
		covrpf.setRerateFromDate(0);
		covrpf.setBenBillDate(0);
		covrpf.setCoverageDebt(new BigDecimal(0));
		covrpf.setAnnivProcDate(0);
		covrpf.setTransactionDate(varcom.vrcmDate.toInt());
		covrpf.setTransactionTime(varcom.vrcmTime.toInt());
		covrpf.setUser(varcom.vrcmUser.toInt());
		covrpf.setZstpduty01(BigDecimal.ZERO);	//ILIFE-8359
		covrpf.setLnkgno(""); //ILIFE-8248
		covrpf.setLnkgsubrefno(""); //ILIFE-8248
 //ILIFE-8096 end
	}

protected void coverOrRider4520()
	{
		if (isEQ(sv.rtable,SPACES)) {
			coverNextProg4530();
		}
		if (isNE(sv.rtable,SPACES)) {
			riderNextProg4540();
		}
		/*    Keep the key of the COVRMJA transaction file for the next*/
		/*       program.*/
		keepsCovrmja4550();
	}

protected void coverNextProg4530()
	{
		if (wsaaCover.isTrue()) {
			wsaaCrtable.set(sv.crcode);
			wsaaTrancode.set(wsaaBatckey.batcBatctrcde);
		}
		wsaaNextCovnoR.add(1); //ILIFE-8445
		wsaaCoverage.set(wsaaNextCovnoR);
		covrpf.setCoverage(wsaaNextCovnoR.toString()); //ILIFE-8096
		wsaaRider.set(ZERO);
		covrpf.setRider(wsaaRider.toString()); //ILIFE-8264
	}

protected void riderNextProg4540()
	{
		wsaaCrtable.set(sv.rtable);
		wsaaTrancode.set(wsaaBatckey.batcBatctrcde);
		if (isNE(sv.crcode,SPACES)) {
			covrpf.setCoverage(sv.crcode.toString()); //ILIFE-8096
		}
		else {
			covrpf.setCoverage(wsaaCoverage.toString()); //ILIFE-8096
		}
		wsaaRidrSub.set(covrpf.getCoverage()); //ILIFE-8096
		compute(wsaaNextRidnoR, 0).set(add(1,wsaaRidrTab[wsaaRidrSub.toInt()]));
		wsaaRidrTab[wsaaRidrSub.toInt()].set(wsaaNextRidno);
		covrpf.setRider(wsaaNextRidno.toString()); //ILIFE-8096
	}

protected void keepsCovrmja4550()
	{
		 //ILIFE-8096 start
		covrpf.setCrtable(wsaaCrtable.toString());
		covrpf.setChdrcoy(wsspcomn.company.toString());
		covrpf.setChdrnum(chdrpf.getChdrnum());
		covrpf.setLife(lifemjaIO.getLife().toString());
		covrpfDAO.setCacheObject(covrpf);
 //ILIFE-8096 end	
		}

protected void readProgramTables4600()
	{
 //ILIFE-8096 start
		if(readProgramTable4610()){
			loadProgsToWssp4620();
		}
	}

protected boolean readProgramTable4610()
	{
		
		wsspcomn.nextprog.set(wsaaProg);
		boolean itemFound = false;
		String keyItemitem = wsaaConcatName.toString();
	                                                
		if (t5671ListMap.containsKey(keyItemitem)) {
			List<Itempf> itempfList = t5671ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
	                             
				Itempf itempf = iterator.next();
				t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
				break;
			}
		}else{
			syserrrec.params.set(t5671rec.t5671Rec);
			fatalError600();
		}
		if(!itemFound) {
			if (isEQ(wsaaCrtable,"****")) {
				sv.selectErr.set(g433);
				wsspcomn.edterror.set("Y");
				wsspcomn.nextprog.set(scrnparams.scrname);
				/*          PERFORM 4700-PLAN-RELOAD                               */
				return false;
	
			}
			else {
				wsaaCrtable.set("****");
	                                     
			}
		}
		return true;
	}

protected void loadProgsToWssp4620()
	{
	wsaaSub4.set(1);
	compute(wsaaSub5, 0).set(add(1,wsspcomn.programPtr));
	for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
		/*    This loop will load four programs from table T5671 to*/
		/*    the WSSP stack.*/
		wsspcomn.secProg[wsaaSub5.toInt()].set(t5671rec.pgm[wsaaSub4.toInt()]);
		wsaaSub4.add(1);
		wsaaSub5.add(1);
	}
	/*    Reset the Action to '*' signifying action desired to*/
	/*    the next program.*/
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
	}

/*protected void loop24630()
	{
		wsspcomn.secProg[wsaaSub5.toInt()].set(t5671rec.pgm[wsaaSub4.toInt()]);
		wsaaSub4.add(1);
		wsaaSub5.add(1);
	}*/
 //ILIFE-8096 end 
protected void planReload4700()
	{
		try {
			reloadProgsWssp4710();
		}
		catch (GOTOException e){
		}
	}

protected void reloadProgsWssp4710()
	{
		compute(wsaaSub1, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 4); loopVar5 += 1){
			loop34720();
		}
		/* Set action flag to ' ' in order that the program will resume    */
		/* initial stack order and go to the next program.                 */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		goTo(GotoLabel.exit4790);
	}

protected void loop34720()
	{
		wsspcomn.secProg[wsaaSub1.toInt()].set(wsaaSecProg[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
	}

protected void screenUpdate4800()
	{
		/*UPDATE-SCREEN*/
		scrnparams.function.set(varcom.supd);
		processScreen("S5128", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-T5673-STORED-TABLE--INNER
 */
private static final class WsaaT5673StoredTableInner { 

		/*   The array is amended to cater for continuation items.         
		   A maximum of 48 coverages seems sufficient.                   
		   48 coverages not sufficient change to 90.            <LFA1065>*/
	private FixedLengthStringData wsaaT5673StoredTable = new FixedLengthStringData(65790);
		/* 03  WSAA-T5673-INFO OCCURS 8 TIMES                           
		 03  WSAA-T5673-INFO OCCURS 48 TIMES                 <LFA1065>*/
	private FixedLengthStringData[] wsaaT5673Info = FLSArrayPartOfStructure(90, 731, wsaaT5673StoredTable, 0);
	private FixedLengthStringData[] wsaaZrlifind = FLSDArrayPartOfArrayStructure(1, wsaaT5673Info, 0);
	private FixedLengthStringData[] wsaaCreq = FLSDArrayPartOfArrayStructure(1, wsaaT5673Info, 1);
	private FixedLengthStringData[] wsaaCtable = FLSDArrayPartOfArrayStructure(4, wsaaT5673Info, 2);
	private ZonedDecimalData[] wsaaCtmaxcov = ZDArrayPartOfArrayStructure(2, 0, wsaaT5673Info, 6);
	private ZonedDecimalData[] wsaaNoOfCovs = ZDArrayPartOfArrayStructure(2, 0, wsaaT5673Info, 8);
	private FixedLengthStringData[] wsaaCtableUsed = FLSDArrayPartOfArrayStructure(1, wsaaT5673Info, 10);
		/*     10 WSAA-T5673-RIDR-INFO OCCURS 48 TIMES                  */
	private FixedLengthStringData[][] wsaaT5673RidrInfo = FLSDArrayPartOfArrayStructure(90, 8, wsaaT5673Info, 11);
	private FixedLengthStringData[][] wsaaRreq = FLSDArrayPartOfArrayStructure(1, wsaaT5673RidrInfo, 0);
	private FixedLengthStringData[][] wsaaRtable = FLSDArrayPartOfArrayStructure(4, wsaaT5673RidrInfo, 1);
	private FixedLengthStringData[][] wsaaRtableUsed = FLSDArrayPartOfArrayStructure(1, wsaaT5673RidrInfo, 5);
	private FixedLengthStringData[][] wsaaRtableRisk = FLSDArrayPartOfArrayStructure(2, wsaaT5673RidrInfo, 6);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
}
}
