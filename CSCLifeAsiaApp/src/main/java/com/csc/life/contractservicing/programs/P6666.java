/*
 * File: P6666.java
 * Date: 30 August 2009 0:49:15
 * Author: Quipoz Limited
 * 
 * Class transformed from P6666.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.screens.S6666ScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS
*
*               P6666 - Billing Change Submenu
*               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Overview
*~~~~~~~~
*
*This screen is to show the user what the system can  do  and
*what the user is allowed to do for billing changes.
*This is part of contract alterations.
*
*Table Entries
*~~~~~~~~~~~~~
*
*Create the following items in T1688:(already created)
*
*    S522 - Billing Change Submenu
*    T522 - Billing Change Transaction
*
*Create a continuation item in T1691 with the following:
*
*    item: EP5048A
*
*    item details:   Action = A
*                    Submenu Program = P6666
*                    Next action = A
*                    Transaction code = S522
*                    Description = Billing Changes
*                    MENU code = L
*
*Modify the item EP5048 with the following:
*
*    Continuation item = EP5048A
*
*Create the item P6666 in T1690 with the following:
*
*    Action = A
*    Key1 = Y
*    Key2 = Y
*    Key3 = ' '
*    NEXTPROG1 = P6668
*    NEXTPROG2 = NEXTPROG3 = NEXTPROG4 = space
*    Transaction code = T522
*    Batch required = Y
*
*
*S6666
*~~~~~
*
*Design a screen to have the followings:-
*
*     Billing Change Submenu          S6666 01
*
*
*          A - Billing Change
*
*
*
*      Contract No: BBBBBBBBBB
*
*           Action: B
*
*where  contract  number  is  the field CHDRNUM and action is
*the field ACTION, both should have the attributes RI HI  and
*colour  read  for  error, also an indicator attached to each
*of the field.
*
*Compile the screen with type *SUB
*
*
*P6666
*~~~~~
*
*We  should  now  have  a  program(P6666)  generated  by  the
*system(successful compilation of S6666).
*
*
*1000-Initialisation Section
*~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Read  T5679 with the transaction code(from WSSP-BATCHKEY) as
*the item  to  get  all  the  valid  status  codes  for  this
*particular transaction.
*
*Note: You have to set up the entry (ie T522) in T5679.
*
*
*2000-Screen-Edit Section
*~~~~~~~~~~~~~~~~~~~~~~~~
*
*Normal validations required for key1, key2 and key3.
*Use  the  captured  contract  number  to  read  the contract
*header file(CHDRMJA) to check if the  contract  is  a  valid
*contract.
*
*If no record found
*    Display message 'Contract not found'
*    Exit section.
*
*Check the risk and premium statii against those on T5679.
*
*If   they   are  not  on  T5679(invalid  contract  for  this
*transaction)
*    Display message 'Invalid contract'
*    Exit this section.
*
*If validflag not = '1'
*    Display message 'Invalid contract'
*    Exit this section.
*
*If    Bill-to-Date(BTDATE)    is    not    the    same    as
*Paid-to-Date(PTDATE)
*    Display message 'BTDATE not = PTDATE'
*    Exit this section.
*
*
*3000-Update Section
*~~~~~~~~~~~~~~~~~~~
*
*Set WSSP-FLAG to S6666-ACTION.
*
*Soft  lock  the  contract by calling 'SFTLOCK' with function
*'LOCK'.
*
*Call 'CHDRMJAIO' with function 'KEEPS' to store  a  copy  of
*the  contract  header  record in the working storage so that
*it can be retrieved by the subsequent programs.
*
*4000-Where-Next Section
*~~~~~~~~~~~~~~~~~~~~~~~
*
*No additional codings required.
*
*****************************************************************
* </pre>
*/
public class P6666 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6666");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaStat = new FixedLengthStringData(1);
	private Validator wsaaStatOk = new Validator(wsaaStat, "Y");
	private Validator wsaaStatInvalid = new Validator(wsaaStat, "N");
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e186 = "E186";
	private static final String e374 = "E374";
	private static final String f321 = "F321";
	private static final String f910 = "F910";
	private static final String h945 = "H945";
	private static final String h946 = "H946";
	private static final String rrgf = "RRGF";
	private FixedLengthStringData h947 = new FixedLengthStringData(4).init("H947");
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String payrrec = "PAYRREC";
		/* Dummy user wssp. Replace with required version.*/
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private S6666ScreenVars sv = ScreenProgram.getScreenVars( S6666ScreenVars.class);
	
	boolean csbil003Permission = false;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2290, 
		exit12090, 
		release3010, 
		exit3090
	}

	public P6666() {
		super();
		screenVars = sv;
		new ScreenModel("S6666", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
		}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{	
		csbil003Permission = FeaConfg.isFeatureExist("2", "CSBIL003", appVars, "IT");   //ICIL-298
		wsspcomn.bchaction.set(" ");
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if ((isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth))
		|| (isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear))) {
			scrnparams.errorCode.set(e070);
		}
		/* Check against table*/
		subprogrec.action.set(wsspcomn.sbmaction);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			return ;
		}
		/* Read T5679 with the transaction code as the item to get*/
		/* all the valid status codes for this particular*/
		/* transaction*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.actionErr.set(f321);
			return ;
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6666IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S6666-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		try {
			validateKey12210();
			checkChdrmja2215();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateKey12210()
	{
		/* Contract number must be entered                                 */
		if (isEQ(sv.chdrsel,SPACES)) {
			sv.chdrselErr.set(e186);
			/*     GO TO 2080-CHECK-FOR-ERRORS.                              */
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(subprogrec.key1,"Y")) {
			if (isEQ(sv.chdrsel,SPACES)) {
				sv.chdrselErr.set(e186);
				goTo(GotoLabel.exit2290);
			}
		}

		

		if (isEQ(subprogrec.key1,"N")) {
			if (isNE(sv.chdrsel,SPACES)) {
				sv.chdrselErr.set(e374);
				goTo(GotoLabel.exit2290);
			}
		}
	}
/*ICIL-298 starts*/
protected void getT5688Data(String cnttype, Integer currfrom){
	ItemDAO itempfDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
    List<Itempf> itempfList = itempfDao.getItdmByFrmdate(wsspcomn.company.toString(),"T5688",cnttype,currfrom);   
    T5688rec t5688rec = new T5688rec();
	if (itempfList == null) {
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5688").concat(cnttype));
		fatalError600();
	}
	else{
		t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
				if(isNE(t5688rec.bfreqallw,SPACES)){
					sv.chdrselErr.set(rrgf);
					wsspcomn.edterror.set("Y");
				}

		}
}
/*ICIL-298 ends*/
protected void checkChdrmja2215()
	{
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if ((isNE(chdrmjaIO.getStatuz(),varcom.oK))
		&& (isNE(chdrmjaIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/* No record found*/
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			sv.chdrselErr.set(h945);
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		/*ICIL-298 starts*/
		if(csbil003Permission)
		{
		if (isEQ(sv.action,"B"))
			getT5688Data(chdrmjaIO.getCnttype().toString(),chdrmjaIO.getCurrfrom().toInt());
		}
		/*ICIL-298 ends*/
		/*                                                         <LA1167>*/
		/* If Billing change, validate that the same contract does <LA1167>*/
		/* exist in FPDF with valid flag equal '1'.                <LA1167>*/
		/*                                                         <LA1167>*/
		/*     IF  S6666-ACTION           = 'A'                    <LA1167>*/
		/*         MOVE CHDRMJA-CHDRPFX  TO FPDF-CHDRPFX           <LA1167>*/
		/*         MOVE CHDRMJA-CHDRCOY  TO FPDF-CHDRCOY           <LA1167>*/
		/*         MOVE CHDRMJA-CHDRNUM  TO FPDF-CHDRNUM           <LA1167>*/
		/*         MOVE FPDFREC          TO FPDF-FORMAT            <LA1167>*/
		/*         MOVE READR            TO FPDF-FUNCTION          <LA1167>*/
		/*                                                         <LA1167>*/
		/*         CALL 'FPDFIO'      USING FPDF-PARAMS            <LA1167>*/
		/*                                                         <LA1167>*/
		/*         IF  FPDF-STATUZ        = O-K                    <LA1167>*/
		/*             MOVE RLA0         TO S6666-CHDRSEL-ERR      <LA1167>*/
		/*             MOVE 'Y'          TO WSSP-EDTERROR          <LA1167>*/
		/*         END-IF                                          <LA1167>*/
		/*     END-IF.                                             <LA1167>*/
		/* Read the PAYR file                                              */
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Check if the current Bill-To-Date, (CHDRMJA-BTDATE) is equal*/
		/* to the Contract Header From Date, (CURRFROM).*/
		/*  IF (CHDRMJA-BTDATE          = CHDRMJA-CURRFROM)              */
		/*     MOVE F041                TO S6666-CHDRSEL-ERR             */
		/*     MOVE F040                TO SCRN-ERROR-CODE               */
		/*     GO TO 2290-EXIT                                           */
		/*  END-IF.                                                      */
		/* Check the risk and premium statii against those*/
		/* on T5679*/
		wsaaStat.set("N");
		wsaaSub.set(ZERO);
		while ( !(isGT(wsaaSub,12)
		|| wsaaStatOk.isTrue())) {
			wsaaSub.add(1);
			if (isGT(wsaaSub,12)) {
				sv.chdrselErr.set(h946);
				return ;
			}
			if (isEQ(chdrmjaIO.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaStat.set("Y");
			}
		}
		
		wsaaStat.set("N");
		wsaaSub.set(ZERO);
		while ( !(isGT(wsaaSub,12)
		|| wsaaStatOk.isTrue())) {
			wsaaSub.add(1);
			if (isGT(wsaaSub,12)) {
				sv.chdrselErr.set(h946);
				return ;
			}
			if (isEQ(chdrmjaIO.getPstatcode(),t5679rec.cnPremStat[wsaaSub.toInt()])) {
				wsaaStat.set("Y");
			}
		}
		
		/* Valid flag not = '1'*/
		if (isNE(chdrmjaIO.getValidflag(),"1")) {
			sv.chdrselErr.set(h946);
			return ;
		}
		
		/* Bill-to-Date not the same as Paid-to-Date*/
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(subprogrec.key2,"Y")) {
			/*CONTINUE_STMT*/
		}
		else {
			if (isNE(chdrmjaIO.getBtdate(),chdrmjaIO.getPtdate())) {
				sv.chdrselErr.set(h947);
				return ;
			}
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			/*      MOVE E073               TO S6666-ACTION-ERR.             */
			sv.actionErr.set(e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2990-EXIT.                                          */
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2990-EXIT.                                          */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case release3010: 
					release3010();
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsspcomn.bchaction.set("B");
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/* Set WSSP-FLAG to S6666-ACTION*/
		wsspcomn.flag.set(sv.action);
		/* Soft lock contract.*/
		if (isEQ(wsspcomn.flag,"B")) {
			goTo(GotoLabel.release3010);
		}
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz,varcom.oK))
		&& (isNE(sftlockrec.statuz,"LOCK"))) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void release3010()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(wsspcomn.company);
		chdrenqIO.setChdrnum(sv.chdrsel);
		chdrenqIO.setFunction(varcom.reads);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.rlse);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrsel);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readh);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.keeps);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void batching3080()
	{
		if ((isEQ(subprogrec.bchrqd,"Y"))
		&& (isEQ(sv.errorIndicators,SPACES))) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
