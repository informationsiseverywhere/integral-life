package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:36
 * Description:
 * Copybook name: CVUWKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cvuwkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData cvuwFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData cvuwKey = new FixedLengthStringData(64).isAPartOf(cvuwFileKey, 0, REDEFINE);
  	public FixedLengthStringData cvuwChdrcoy = new FixedLengthStringData(1).isAPartOf(cvuwKey, 0);
  	public FixedLengthStringData cvuwChdrnum = new FixedLengthStringData(8).isAPartOf(cvuwKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(cvuwKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(cvuwFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		cvuwFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}