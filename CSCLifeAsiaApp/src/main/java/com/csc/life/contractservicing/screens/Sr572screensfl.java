package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr572screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 16;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 12}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 22, 7, 72}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr572ScreenVars sv = (Sr572ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr572screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr572screensfl, 
			sv.Sr572screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr572ScreenVars sv = (Sr572ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr572screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr572ScreenVars sv = (Sr572ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr572screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr572screensflWritten.gt(0))
		{
			sv.sr572screensfl.setCurrentIndex(0);
			sv.Sr572screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr572ScreenVars sv = (Sr572ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr572screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr572ScreenVars screenVars = (Sr572ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rreq.setFieldName("rreq");
				screenVars.clcode.setFieldName("clcode");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.indic.setFieldName("indic");
				screenVars.select.setFieldName("select");
				screenVars.crcode.setFieldName("crcode");
				screenVars.rtable.setFieldName("rtable");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.hrequired.setFieldName("hrequired");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.rreq.set(dm.getField("rreq"));
			screenVars.clcode.set(dm.getField("clcode"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.indic.set(dm.getField("indic"));
			screenVars.select.set(dm.getField("select"));
			screenVars.crcode.set(dm.getField("crcode"));
			screenVars.rtable.set(dm.getField("rtable"));
			screenVars.longdesc.set(dm.getField("longdesc"));
			screenVars.hrequired.set(dm.getField("hrequired"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr572ScreenVars screenVars = (Sr572ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.rreq.setFieldName("rreq");
				screenVars.clcode.setFieldName("clcode");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.indic.setFieldName("indic");
				screenVars.select.setFieldName("select");
				screenVars.crcode.setFieldName("crcode");
				screenVars.rtable.setFieldName("rtable");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.hrequired.setFieldName("hrequired");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("rreq").set(screenVars.rreq);
			dm.getField("clcode").set(screenVars.clcode);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("indic").set(screenVars.indic);
			dm.getField("select").set(screenVars.select);
			dm.getField("crcode").set(screenVars.crcode);
			dm.getField("rtable").set(screenVars.rtable);
			dm.getField("longdesc").set(screenVars.longdesc);
			dm.getField("hrequired").set(screenVars.hrequired);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr572screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr572ScreenVars screenVars = (Sr572ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.rreq.clearFormatting();
		screenVars.clcode.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.indic.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.crcode.clearFormatting();
		screenVars.rtable.clearFormatting();
		screenVars.longdesc.clearFormatting();
		screenVars.hrequired.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr572ScreenVars screenVars = (Sr572ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.rreq.setClassString("");
		screenVars.clcode.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.select.setClassString("");
		screenVars.crcode.setClassString("");
		screenVars.rtable.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.hrequired.setClassString("");
	}

/**
 * Clear all the variables in Sr572screensfl
 */
	public static void clear(VarModel pv) {
		Sr572ScreenVars screenVars = (Sr572ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.rreq.clear();
		screenVars.clcode.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.indic.clear();
		screenVars.select.clear();
		screenVars.crcode.clear();
		screenVars.rtable.clear();
		screenVars.longdesc.clear();
		screenVars.hrequired.clear();
	}
}
