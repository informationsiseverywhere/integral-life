/*
 * File: P6758.java
 * Date: 30 August 2009 0:56:16
 * Author: Quipoz Limited
 * 
 * Class transformed from P6758.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.CwfdregTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnwfdTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnwfdtTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*                     Cancel Windforward
*                     ------------------
*
*   This is a non screen program which is accessed from the
*   Cancel Windforward screen or following a cancel windforward
*   selection on the Windforward Registration screen.
*
*   The program deletes all of the windforward records for a
*   contract when accessed from the Cancel Windforward screen, or
*   all of the windforward records for a Transaction when
*   accessed from the Windforward Registration screen.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6758 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6758");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String cwfdregrec = "CWFDREGREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Windforward file by Tranno*/
	private CwfdregTableDAM cwfdregIO = new CwfdregTableDAM();
		/*PTRN Logical File for windforward*/
	private PtrnwfdtTableDAM ptrnwfdtIO = new PtrnwfdtTableDAM();
	private Wssplife wssplife = new Wssplife();

	public P6758() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		chdrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(wsspcomn.flag,"M")) {
			ptrnwfdtIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, ptrnwfdtIO);
			if (isNE(ptrnwfdtIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(ptrnwfdtIO.getStatuz());
				syserrrec.params.set(ptrnwfdtIO.getParams());
				fatalError600();
			}
			ptrnwfdtIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, ptrnwfdtIO);
			if (isNE(ptrnwfdtIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(ptrnwfdtIO.getStatuz());
				syserrrec.params.set(ptrnwfdtIO.getParams());
				fatalError600();
			}
		}
	}

protected void screenEdit2000()
	{
		/*START*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		cwfdregIO.setDataArea(SPACES);
		cwfdregIO.setChdrcoy(chdrenqIO.getChdrcoy());
		cwfdregIO.setChdrnum(chdrenqIO.getChdrnum());
		if (isEQ(wsspcomn.flag,"D")) {
			cwfdregIO.setTranno(ZERO);
		}
		if (isEQ(wsspcomn.flag,"M")) {
			cwfdregIO.setTranno(ptrnwfdtIO.getTranno());
		}
		cwfdregIO.setFunction(varcom.begnh);
		cwfdregIO.setFormat(cwfdregrec);
		SmartFileCode.execute(appVars, cwfdregIO);
		if (isNE(cwfdregIO.getStatuz(),varcom.oK)
		&& isNE(cwfdregIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(cwfdregIO.getStatuz());
			syserrrec.params.set(cwfdregIO.getParams());
			fatalError600();
		}
		if (isNE(cwfdregIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(cwfdregIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			cwfdregIO.setStatuz(varcom.endp);
		}
		if (isEQ(wsspcomn.flag,"D")) {
			while ( !(isEQ(cwfdregIO.getStatuz(),varcom.endp))) {
				delete3100();
			}
			
		}
		if (isEQ(wsspcomn.flag,"M")) {
			while ( !(isEQ(cwfdregIO.getStatuz(),varcom.endp)
			|| isNE(cwfdregIO.getTranno(),ptrnwfdtIO.getTranno()))) {
				delete3100();
			}
			
		}
	}

protected void delete3100()
	{
		para3110();
	}

protected void para3110()
	{
		cwfdregIO.setFunction(varcom.delet);
		cwfdregIO.setFormat(cwfdregrec);
		SmartFileCode.execute(appVars, cwfdregIO);
		if (isNE(cwfdregIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cwfdregIO.getStatuz());
			syserrrec.params.set(cwfdregIO.getParams());
			fatalError600();
		}
		cwfdregIO.setFunction(varcom.nextr);
		cwfdregIO.setFormat(cwfdregrec);
		SmartFileCode.execute(appVars, cwfdregIO);
		if (isNE(cwfdregIO.getStatuz(),varcom.oK)
		&& isNE(cwfdregIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(cwfdregIO.getStatuz());
			syserrrec.params.set(cwfdregIO.getParams());
			fatalError600();
		}
		if (isNE(cwfdregIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(cwfdregIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			cwfdregIO.setStatuz(varcom.endp);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		chdrenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
