package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:30
 * Description:
 * Copybook name: TXCALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Txcalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData linkRec = new FixedLengthStringData(212);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(linkRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(linkRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(linkRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(linkRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(linkRec, 18);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(linkRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(linkRec, 22);
  	public ZonedDecimalData planSuffix = new ZonedDecimalData(2, 0).isAPartOf(linkRec, 24).setUnsigned();
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(linkRec, 26);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(linkRec, 30);
  	public FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(linkRec, 33);
  	public FixedLengthStringData taxrule = new FixedLengthStringData(8).isAPartOf(linkRec, 36);
  	public FixedLengthStringData rateItem = new FixedLengthStringData(8).isAPartOf(linkRec, 44);
  	public FixedLengthStringData cntTaxInd = new FixedLengthStringData(1).isAPartOf(linkRec, 52);
  	public ZonedDecimalData amountIn = new ZonedDecimalData(17, 2).isAPartOf(linkRec, 53);
  	public FixedLengthStringData transType = new FixedLengthStringData(4).isAPartOf(linkRec, 70);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(linkRec, 74);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(linkRec, 82);
  	public ZonedDecimalData jrnseq = new ZonedDecimalData(3, 0).isAPartOf(linkRec, 85).setUnsigned();
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(linkRec, 88);
  	public FixedLengthStringData ccy = new FixedLengthStringData(3).isAPartOf(linkRec, 152);
  	public FixedLengthStringData output = new FixedLengthStringData(44).isAPartOf(linkRec, 155);
  	public FixedLengthStringData[] taxType = FLSArrayPartOfStructure(2, 2, output, 0);
  	public ZonedDecimalData[] taxAmt = ZDArrayPartOfStructure(2, 17, 2, output, 4);
  	public FixedLengthStringData[] taxAbsorb = FLSArrayPartOfStructure(2, 1, output, 38);
  	public FixedLengthStringData[] taxSacstyp = FLSArrayPartOfStructure(2, 2, output, 40);
  	public FixedLengthStringData txcode = new FixedLengthStringData(1).isAPartOf(linkRec, 199);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(linkRec, 200);

  	// IVE-868 LIFE Service Tax Calculation Rework- Integration with latest PA compatible models
  	public FixedLengthStringData taxType01 = new FixedLengthStringData(2).isAPartOf(output, 0);
  	public FixedLengthStringData taxType02 = new FixedLengthStringData(2).isAPartOf(output, 2);
  	/*ILIFE-2749 S6378 - DAN - Difference in taxes on pre issue validation screen Start */
  	public ZonedDecimalData taxAmt01 = new ZonedDecimalData(17,2).isAPartOf(output, 4);
  	public ZonedDecimalData taxAmt02 = new ZonedDecimalData(17,2).isAPartOf(output, 21);
  	/*ILIFE-2749 End */
  	public FixedLengthStringData taxAbsorb01 = new FixedLengthStringData(1).isAPartOf(output, 38);
  	public FixedLengthStringData taxAbsorb02 = new FixedLengthStringData(1).isAPartOf(output, 39);
  	public FixedLengthStringData taxSacstyp01 = new FixedLengthStringData(2).isAPartOf(output, 40);
  	public FixedLengthStringData taxSacstyp02 = new FixedLengthStringData(2).isAPartOf(output, 42);
  	//ILIFE-6006
  	public FixedLengthStringData item = new FixedLengthStringData(3).isAPartOf(linkRec, 201);
  	public FixedLengthStringData vpmtaxrule = new FixedLengthStringData(8).isAPartOf(linkRec, 204);
  	//ILIFE-6006 end


	public void initialize() {
		COBOLFunctions.initialize(linkRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linkRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}