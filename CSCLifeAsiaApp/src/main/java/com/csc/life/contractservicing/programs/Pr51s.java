/*
 * File: Pr51s.java
 * Date: December 3, 2013 3:25:58 AM ICT
 * Author: CSC
 *
 * Class transformed from PR51S.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.screens.Sr51sScreenVars;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.life.productdefinition.tablestructures.Tr51prec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.regularprocessing.tablestructures.T5399rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.procedures.Calcfee;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.terminationclaims.tablestructures.Th584rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*          Permium Holiday Processing
*          --------------------------
*
*   This is Premium Holiday Process program to turn valid inforce/ remium
*   paying Investment Linked Policy into inforce/premium holiday s atus,
*   if product is set up on Premium Holiday Rules table, TR51P.
*
*   It is contract level process, so component selection is not re uired.
*   Screen will list down all the components, for eligible compone t,
*   a 'Y' will be displayed under Process indicator, and program w ll
*   process each valid component according to the premium holiday  rocess
*   rule set up on TH584.
*
*   Main Coverage has to be valid, and eligible for the premium ho iday
*   process, if there is any error, then the whole transaction wil  be
*   stopped.
*
*Initialise
*----------
*  The  details of the contract being worked with will have been
*  stored  in  the  CHDRMJA I/O module. Retrieve the details and
*  set up the header portion of the screen.
*
*  -  Preload Frequently Used Tables
*  -  Calculate in-force duration
*  -  Prepare Header section
*  -  Prepare Subfile section
*    -  Evaluate the component Risk/Premium status through T5679 c ecking
*    -  Validate Premium Holiday Process rule through TH584 checki g
*    -  Determine the Premium Holiday rule applicable for each com onent
*       Through TH584 checking
*    -  Write a subfile line for each component, for eligible comp nent
*       Process flag set 'Y', otherwise is 'N', if any error has b en
*       detected, error code is also displayed.
*
*Validate.
*--------
*  - No change in screen, read table T6647 & T5399 for later use
*  - Validate the Policy in-force duration against TR51P
*  - Validate sub-file
*
*Updating
*--------
*
*  Process component:
*  - If process flag is 'N', skip update this component
*  - For eligible (process flag is 'Y'), set up OVERDUE linkage,
*    perform the corresponding subroutine for premium holiday proc ss
*    - If detect 'SKIP' status, display error and stop the whole p ocess
*    - otherwise perform till end of all the component
*  - Update CHDR, with TRANNO + 1.
*  - Update PTRN.
*  - Update PAYR.
*  - Perform Diary process
*  - Write letter request control record
*  - Release Soft Lock.
*
****************************************************************** ****
* </pre>
*/
public class Pr51s extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR51S");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTableKey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTableBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaTableKey, 0);
	private FixedLengthStringData wsaaTableCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTableKey, 4);

	private FixedLengthStringData wsaaTh584Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTh584Znfopt = new FixedLengthStringData(3).isAPartOf(wsaaTh584Key, 0);
	private FixedLengthStringData wsaaTh584Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh584Key, 3);

	private FixedLengthStringData wsaaCovrStatusArray = new FixedLengthStringData(192);
	private FixedLengthStringData[] wsaaCovrRec = FLSArrayPartOfStructure(48, 4, wsaaCovrStatusArray, 0);
	private FixedLengthStringData[] wsaaCovrRist = FLSDArrayPartOfArrayStructure(2, wsaaCovrRec, 0);
	private FixedLengthStringData[] wsaaCovrPrst = FLSDArrayPartOfArrayStructure(2, wsaaCovrRec, 2);

	private FixedLengthStringData wsaaCovrRskMatch = new FixedLengthStringData(1).init("N");
	private Validator covrRskMatch = new Validator(wsaaCovrRskMatch, "Y");

	private FixedLengthStringData wsaaCovrPrmMatch = new FixedLengthStringData(1).init("N");
	private Validator covrPrmMatch = new Validator(wsaaCovrPrmMatch, "Y");
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaIfMonths = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaValidComponent = new FixedLengthStringData(1);
	private Validator validComponent = new Validator(wsaaValidComponent, "Y");

	private FixedLengthStringData wsaaProcessFlag = new FixedLengthStringData(1);
	private Validator noProcess = new Validator(wsaaProcessFlag, "N");
	private String wsaaT6597Found = "";
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String itdmrec = "ITEMREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String ptrnrec = "PTRNREC";
	private IntegerData wsaaCovrIx = new IntegerData();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Itdmkey wsaaItdmKey = new Itdmkey();
	private Batckey wsaaBatckey = new Batckey();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private T5399rec t5399rec = new T5399rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6597rec t6597rec = new T6597rec();
	private T6647rec t6647rec = new T6647rec();
	private Th584rec th584rec = new Th584rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Tr51prec tr51prec = new Tr51prec();
	private T7508rec t7508rec = new T7508rec();
	private Wssplife wssplife = new Wssplife();
	private Sr51sScreenVars sv = ScreenProgram.getScreenVars( Sr51sScreenVars.class);
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private ErrorsInner errorsInner = new ErrorsInner();
	private TablesInner tablesInner = new TablesInner();
	private boolean prmhldtrad = false;
	private Ta524rec ta524rec = new Ta524rec();
	private Itempf itempf;
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private ZonedDecimalData wsaaPrevBtDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaBillcd = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaNextdate = new ZonedDecimalData(8, 0);
	private Datcon4rec datcon4rec = new Datcon4rec();
	private T6654rec t6654rec = new T6654rec();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		addToSfl1270,
		exit1290,
		exit1390,
		exit2690,
		readSfl3120,
		nextSfl3180,
		exit3190,
		validflag23250,
		exit3290
	}

	public Pr51s() {
		super();
		screenVars = sv;
		new ScreenModel("Sr51s", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(cntDteFlag)	{
					sv.iljCntDteFlag.set("Y");
				} else {
					sv.iljCntDteFlag.set("N");
				}
		//ILJ-49 End
		sv.subfileArea.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaTime.set(getCobolTime());
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/* Get Screen Title*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.acdes.set(descIO.getLongdesc());
		}
		else {
			sv.acdes.fill("?");
		}
		/* Initalisation the Subfile*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SR51S", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Initilise screen fileds*/
		sv.btdate.set(varcom.maxdate);
		sv.occdate.set(varcom.maxdate);
		sv.ptdate.set(varcom.maxdate);
		/*    Set screen fields*/
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.cownnum.set(chdrmjaIO.getCownnum());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.occdate.set(chdrmjaIO.getOccdate());
		sv.billfreq.set(chdrmjaIO.getBillfreq());
		sv.mop.set(chdrmjaIO.getBillchnl());
		sv.register.set(chdrmjaIO.getRegister());
		/* Get Contract Descriptions*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		/* Get Contract Status Descriptions*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		else {
			sv.chdrstatus.fill("?");
		}
		/* Get Premium Status Descriptions*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.premstatus.set(descIO.getShortdesc());
		}
		else {
			sv.premstatus.fill("?");
		}
		/*  Read T5679*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(errorsInner.f321);
			wsspcomn.edterror.set("Y");
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		/*  Read TR51P*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr51p);
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.chdrnumErr.set(errorsInner.rlbs);
			wsspcomn.edterror.set("Y");
		}
		else {
			tr51prec.tr51pRec.set(itemIO.getGenarea());
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(chdrmjaIO.getCownpfx());
		namadrsrec.clntCompany.set(chdrmjaIO.getCowncoy());
		namadrsrec.clntNumber.set(chdrmjaIO.getCownnum());
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMF");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.ownername.set(namadrsrec.name);
		/*  Get Life name*/
		lifelnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(chdrmjaIO.getCownpfx());
		namadrsrec.clntCompany.set(chdrmjaIO.getCowncoy());
		namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMF");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.lifenum.set(lifelnbIO.getLifcnum());
		sv.lifename.set(namadrsrec.name);
		/* Calculate in-force duration*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon3rec.intDate2.set(chdrmjaIO.getPtdate());
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaIfMonths.set(datcon3rec.freqFactor);
		/* Retrieve the PAYR  record.*/
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Load coverages*/
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setValidflag(1); //ILIFE-7804
		covrmjaIO.setFunction(varcom.begn);
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT); //ILIFE-7804
		covrmjaIO.setFitKeysSearch("CHDRNUM","CHDRCOY","VALIDFLAG"); //ILIFE-7804
		//ILIFE-8179		
		prmhldtrad = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString(), "CSOTH010", appVars, "IT");
		if(prmhldtrad) {
			/*  Read TA524*/
			itempf = null;
			itempf = itempfDAO.findItemByDate("IT", chdrmjaIO.getChdrcoy().toString(), tablesInner.ta524.toString(), chdrmjaIO.getCnttype().toString(), wsaaToday.toString(), "1");
			if(itempf != null) {
				ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				sv.chdrnumErr.set(SPACES);
				wsspcomn.edterror.set(SPACES);
			}
			else {
				ta524rec.ta524Rec.set(SPACES);
				if(isEQ(tr51prec.tr51pRec, SPACES)) {
					sv.chdrnumErr.set(errorsInner.rrrh);
					wsspcomn.edterror.set("Y");
				}
			}
			itempf = null;
			if (BTPRO028Permission) {
				itempf = itempfDAO.findItemByItdm(chdrmjaIO.getChdrcoy().toString(), tablesInner.t6654.toString(), payrIO.getBillchnl().toString().trim()+chdrmjaIO.getCnttype().toString().trim()+payrIO.getBillfreq().toString().trim());	
			}
			else {
			itempf = itempfDAO.findItemByItdm(chdrmjaIO.getChdrcoy().toString(), tablesInner.t6654.toString(), payrIO.getBillchnl().toString().trim()+chdrmjaIO.getCnttype().toString());
			}
			if(itempf != null) {
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			if(isNE(ta524rec.ta524Rec, SPACES) && ("P").equals(wsspcomn.flag.toString()))
				getBtPtdate();
		}
		else {
			ta524rec.ta524Rec.set(SPACES);
		}
		/*    Perform section to load first page of subfile*/
		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			loadSubfile1200();
		}

		scrnparams.subfileRrn.set(1);
	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1210();
					covrmjaProcess1220();
					checkTh5841250();
					checkT65971260();
				case addToSfl1270:
					addToSfl1270();
				case exit1290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1210()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
	}

protected void covrmjaProcess1220()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.erorErr.set(errorsInner.e300);
			sv.eror.set(errorsInner.e300);
			wsspcomn.edterror.set("Y");
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
			sv.pumeth.set(t5687rec.pumeth);
		}
		sv.subfileArea.set(SPACES);
		sv.sumins.set(ZERO);
		sv.instprem.set(ZERO);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrmjaIO.getCrtable());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.crtabled.set(descIO.getLongdesc());
		}
		else {
			sv.crtabled.fill("?");
		}
		sv.life.set(covrmjaIO.getLife());
		sv.jlife.set(covrmjaIO.getJlife());
		sv.coverage.set(covrmjaIO.getCoverage());
		sv.rider.set(covrmjaIO.getRider());
		sv.statcde.set(covrmjaIO.getStatcode());
		sv.pstatcode.set(covrmjaIO.getPstatcode());
		sv.crtable.set(covrmjaIO.getCrtable());
		sv.sumins.set(covrmjaIO.getSumins());
		sv.instprem.set(covrmjaIO.getInstprem());
		sv.planSuffix.set(covrmjaIO.getPlanSuffix());
		componentValidityCheck1300();
		if (validComponent.isTrue()) {
			sv.sel.set("Y");
		}
		else {
			sv.sel.set("N");
			if (isEQ(covrmjaIO.getLife(), "01")
			&& isEQ(covrmjaIO.getCoverage(), "01")
			&& isEQ(covrmjaIO.getRider(), "00")) {
				sv.erorErr.set(errorsInner.rgh5);
				sv.eror.set(errorsInner.rgh5);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.addToSfl1270);
			}
			else {
				sv.nonForfeitMethod.set(SPACES);
				goTo(GotoLabel.addToSfl1270);
			}
		}
	}

protected void checkTh5841250()
	{
		itemIO.setItemcoy(covrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.th584);
		//ILIFE-8179
		if (prmhldtrad && isNE(ta524rec.ta524Rec, SPACES))
			 wsaaTh584Znfopt.set(ta524rec.znfopt);
		else
			wsaaTh584Znfopt.set(tr51prec.znfopt); 
		wsaaTh584Crtable.set(covrmjaIO.getCrtable());
		itemIO.setItemitem(wsaaTh584Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.erorErr.set(errorsInner.hl38);
			sv.eror.set(errorsInner.hl38);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.addToSfl1270);
		}
		th584rec.th584Rec.set(itemIO.getGenarea());
		sv.nonForfeitMethod.set(th584rec.nonForfeitMethod);
	}

protected void checkT65971260()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t6597);
		itdmIO.setItmfrm(chdrmjaIO.getPtdate());
		itdmIO.setItemitem(th584rec.nonForfeitMethod);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);
		wsaaItdmKey.set(itdmIO.getDataKey());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemitem(), th584rec.nonForfeitMethod)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isGT(itdmIO.getItmfrm(), chdrmjaIO.getPtdate())) {
			itdmIO.setDataKey(wsaaItdmKey);
			sv.erorErr.set(errorsInner.g381);
			sv.eror.set(errorsInner.g381);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.addToSfl1270);
		}
		t6597rec.t6597Rec.set(itdmIO.getGenarea());
		wsaaT6597Found = "N";
		for (ix.set(1); !(isGT(ix, 3)
		|| isEQ(wsaaT6597Found, "Y")); ix.add(1)){
			//ILAE-63 Issue9 START by dpuhawan
			//if (isLTE(wsaaIfMonths, t6597rec.durmnth[ix.toInt()])) {
			if (isLT(wsaaIfMonths, t6597rec.durmnth[ix.toInt()])) {
			//ILAE-63 Issue9 END
				sv.premsubr01.set(t6597rec.premsubr[ix.toInt()]);
				sv.crstat01.set(t6597rec.crstat[ix.toInt()]);
				sv.cpstat01.set(t6597rec.cpstat[ix.toInt()]);
				sv.premsubr04.set(t6597rec.premsubr04);
				sv.crstat04.set(t6597rec.crstat04);
				sv.cpstat04.set(t6597rec.cpstat04);
				wsaaT6597Found = "Y";
			}
		}
		if (isNE(wsaaT6597Found, "Y")
		|| isEQ(sv.premsubr01, SPACES)
		|| isEQ(sv.crstat01, SPACES)
		|| isEQ(sv.cpstat01, SPACES)
		|| isEQ(sv.premsubr04, SPACES)
		|| isEQ(sv.crstat04, SPACES)
		|| isEQ(sv.cpstat04, SPACES)) {
			sv.erorErr.set(errorsInner.g381);
			sv.eror.set(errorsInner.g381);
			wsspcomn.edterror.set("Y");
		}
	}

protected void addToSfl1270()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("SR51S", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*NEXT-COVRMJA*/
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void componentValidityCheck1300()
	{
		try {
			covrRiskStatus1310();
			covrPremStatus1330();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void covrRiskStatus1310()
	{
		wsaaValidComponent.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.covRiskStat[ix.toInt()])) {
					wsaaValidComponent.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getStatcode(), t5679rec.ridRiskStat[ix.toInt()])) {
					wsaaValidComponent.set("Y");
					ix.set(13);
				}
			}
		}
		if (!validComponent.isTrue()) {
			goTo(GotoLabel.exit1390);
		}
	}

protected void covrPremStatus1330()
	{
		wsaaValidComponent.set("N");
		if (isEQ(covrmjaIO.getRider(), "00")) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.covPremStat[ix.toInt()])) {
					wsaaValidComponent.set("Y");
					ix.set(13);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(covrmjaIO.getPstatcode(), t5679rec.ridPremStat[ix.toInt()])) {
					wsaaValidComponent.set("Y");
					ix.set(13);
				}
			}
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		checkForErrors2050();
	}

protected void screenIo2010()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		readTables2100();
		//ILIFE-8179
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES)) {
			if (isLT(wsaaIfMonths, ta524rec.minmthif)) {
				sv.chdrnumErr.set(errorsInner.h010);
			}
		}
		else {
			if (isLT(wsaaIfMonths, tr51prec.minmthif)) {
				sv.chdrnumErr.set(errorsInner.h010);
			}
		}
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}

	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void readTables2100()
	{
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES))
			t66472110();	//ILIFE-8179
		t53992150();
	}

protected void t66472110()
	{
		/*    Read the Unit Linked Contract details table*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTableBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaTableCnttype.set(chdrmjaIO.getCnttype());
		itdmIO.setItemitem(wsaaTableKey);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6647)
		|| isNE(itdmIO.getItemitem(), wsaaTableKey)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.cnttypeErr.set(errorsInner.h115);
			wsspcomn.edterror.set("Y");
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void t53992150()
	{
		/*    Read the Contract Risk/Prem Stat Codes*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5399);
		wsaaTableBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaTableCnttype.set(chdrmjaIO.getCnttype());
		itemIO.setItemitem(wsaaTableKey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getItemcoy(), wsspcomn.company)
		|| isNE(itemIO.getItemtabl(), tablesInner.t5399)
		|| isNE(itemIO.getItemitem(), wsaaTableKey)
		|| isEQ(itemIO.getStatuz(), varcom.endp)) {
			sv.cnttypeErr.set(errorsInner.rlbu);
			wsspcomn.edterror.set("Y");
		}
		else {
			t5399rec.t5399Rec.set(itemIO.getGenarea());
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void validateSubfile2600()
	{
		try {
			validation2610();
			subfileError2670();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validation2610()
	{
		processScreen("SR51S", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit2690);
		}
		if (isNE(sv.eror, SPACES)) {
			sv.erorErr.set(sv.eror);
		}
	}

protected void subfileError2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR51S", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*NEXT-SFL*/
		scrnparams.function.set(varcom.srdn);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
		checkForErrors3020();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		wsaaProcessFlag.set("Y");
		initialize(wsaaCovrStatusArray);
		wsaaCovrIx.set(1);
		processComponent3100();
	}

protected void checkForErrors3020()
	{
		if (isNE(wsspcomn.edterror, varcom.oK)) {
			wsspcomn.sectionno.set("2000");
			wsspcomn.edterror.set("Y");
			return ;
		}
		updateChdrmja3400();
		updatePtrn3500();
		processPayr3600();
		printLetter3700();
		statistic3800();
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES))
			dryProcessing8000();	//ILIFE-8179
		releaseSoftlock3900();
	}

protected void processComponent3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					startSfl3110();
				case readSfl3120:
					readSfl3120();
					checkForErrors3150();
				case nextSfl3180:
					nextSfl3180();
				case exit3190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void startSfl3110()
	{
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl3120()
	{
		processScreen("SR51S", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit3190);
		}
		if (isNE(sv.sel, "Y")) {
			loadCovrStatuses5000();
			goTo(GotoLabel.nextSfl3180);
		}
		/*PROCESS-SFL*/
		processCovr3200();
	}

protected void checkForErrors3150()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3190);
		}
	}

protected void nextSfl3180()
	{
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl3120);
	}

protected void processCovr3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readCovrmja3210();
				case validflag23250:
					validflag23250();
					validflag13270();
				case exit3290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCovrmja3210()
	{
		covrmjaIO.setFunction(varcom.rlse);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(sv.chdrnum);
		covrmjaIO.setLife(sv.life);
		covrmjaIO.setCoverage(sv.coverage);
		covrmjaIO.setRider(sv.rider);
		covrmjaIO.setPlanSuffix(sv.planSuffix);
		covrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		setupOverdueRec3300();
		ovrduerec.planSuffix.set(covrmjaIO.getPlanSuffix());
		ovrduerec.instprem.set(covrmjaIO.getInstprem());
		ovrduerec.sumins.set(covrmjaIO.getSumins());
		ovrduerec.crrcd.set(covrmjaIO.getCrrcd());
		ovrduerec.premCessDate.set(covrmjaIO.getPremCessDate());
		ovrduerec.riskCessDate.set(covrmjaIO.getRiskCessDate());
		ovrduerec.newInstprem.set(covrmjaIO.getInstprem());
		ovrduerec.function.set("OLPT");
		callProgram(sv.premsubr01, ovrduerec.ovrdueRec);
		if (isEQ(ovrduerec.statuz, "BOMB")) {
			syserrrec.subrname.set(sv.premsubr01);
			syserrrec.params.set(ovrduerec.ovrdueRec);
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
		if (isEQ(ovrduerec.statuz, varcom.oK)) {
			ovrduerec.statcode.set(sv.crstat01);
			ovrduerec.pstatcode.set(sv.cpstat01);
			goTo(GotoLabel.validflag23250);
		}
		/*  When detect 'OMIT' status, do not update COVRMJA record*/
		if (isEQ(ovrduerec.statuz, "OMIT")) {
			goTo(GotoLabel.exit3290);
		}
		/*  When detect 'SKIP' status, stop the transaction for further pro*/
		if (isEQ(ovrduerec.statuz, "SKIP")) {
			sv.erorErr.set("E961");
			sv.eror.set("E961");
			wsspcomn.edterror.set("Y");
			scrnparams.function.set(varcom.supd);
			processScreen("SR51S", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			goTo(GotoLabel.exit3290);
		}
		if (isEQ(ovrduerec.statuz, "LAPS")) {
			callProgram(sv.premsubr04, ovrduerec.ovrdueRec);
			if (isEQ(ovrduerec.statuz, varcom.oK)) {
				ovrduerec.statcode.set(sv.crstat04);
				ovrduerec.pstatcode.set(sv.cpstat04);
			}
			else {
				syserrrec.subrname.set(sv.premsubr04);
				syserrrec.params.set(ovrduerec.ovrdueRec);
				syserrrec.statuz.set(ovrduerec.statuz);
				fatalError600();
			}
		}
	}

protected void validflag23250()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(chdrmjaIO.getPtdate());
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13270()
	{
		covrmjaIO.setValidflag("1");
		covrmjaIO.setStatcode(ovrduerec.statcode);
		covrmjaIO.setPstatcode(ovrduerec.pstatcode);
		if (isNE(ovrduerec.newRiskCessDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newRiskCessDate, ZERO)) {
			covrmjaIO.setRiskCessDate(ovrduerec.newRiskCessDate);
		}
		if (isNE(ovrduerec.newPremCessDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newPremCessDate, ZERO)) {
			covrmjaIO.setPremCessDate(ovrduerec.newPremCessDate);
		}
		if (isNE(ovrduerec.newRerateDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newRerateDate, ZERO)) {
			covrmjaIO.setRerateDate(ovrduerec.newRerateDate);
		}
		if (isNE(ovrduerec.newSumins, ZERO)) {
			covrmjaIO.setSumins(ovrduerec.newSumins);
			covrmjaIO.setVarSumInsured(ZERO);
		}
		else {
			if (isGT(covrmjaIO.getVarSumInsured(), ZERO)) {
				setPrecision(covrmjaIO.getVarSumInsured(), 2);
				covrmjaIO.setVarSumInsured(sub(covrmjaIO.getVarSumInsured(), covrmjaIO.getSumins()));
			}
			covrmjaIO.setSumins(ZERO);
		}
		if (isNE(ovrduerec.newCrrcd, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newCrrcd, ZERO)) {
			covrmjaIO.setCrrcd(ovrduerec.newCrrcd);
		}
		if (isNE(ovrduerec.newSingp, ZERO)) {
			covrmjaIO.setSingp(ovrduerec.newSingp);
		}
		if (isNE(ovrduerec.newAnb, ZERO)) {
			covrmjaIO.setAnbAtCcd(ovrduerec.newAnb);
		}
		covrmjaIO.setInstprem(ovrduerec.newInstprem);
		if (isNE(ovrduerec.newPua, ZERO)) {
			setPrecision(covrmjaIO.getVarSumInsured(), 2);
			covrmjaIO.setVarSumInsured(add(covrmjaIO.getVarSumInsured(), ovrduerec.newPua));
		}
		if (isNE(ovrduerec.newBonusDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newBonusDate, ZERO)) {
			covrmjaIO.setUnitStatementDate(ovrduerec.newBonusDate);
		}
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setCurrfrom(chdrmjaIO.getPtdate());
		setPrecision(covrmjaIO.getTranno(), 0);
		covrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		covrmjaIO.setReinstated("N");
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		loadCovrStatuses5000();
	}

protected void setupOverdueRec3300()
	{
		start3310();
	}

protected void start3310()
	{
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.language.set(wsspcomn.language);
		ovrduerec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		ovrduerec.chdrnum.set(chdrmjaIO.getChdrnum());
		ovrduerec.life.set(covrmjaIO.getLife());
		ovrduerec.coverage.set(covrmjaIO.getCoverage());
		ovrduerec.rider.set(covrmjaIO.getRider());
		ovrduerec.planSuffix.set(ZERO);
		compute(ovrduerec.tranno, 0).set(add(1, chdrmjaIO.getTranno()));
		ovrduerec.cntcurr.set(payrIO.getCntcurr());
		ovrduerec.effdate.set(wsaaToday);
		ovrduerec.outstamt.set(payrIO.getOutstamt());
		ovrduerec.ptdate.set(chdrmjaIO.getPtdate());
		ovrduerec.btdate.set(chdrmjaIO.getBtdate());
		ovrduerec.agntnum.set(chdrmjaIO.getAgntnum());
		ovrduerec.cownnum.set(chdrmjaIO.getCownnum());
		ovrduerec.trancode.set(wsaaBatckey.batcBatctrcde);
		ovrduerec.acctyear.set(wsaaBatckey.batcBatcactyr);
		ovrduerec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		ovrduerec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ovrduerec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ovrduerec.user.set(payrIO.getUser());
		ovrduerec.company.set(chdrmjaIO.getCowncoy());
		ovrduerec.tranDate.set(wsaaToday);
		ovrduerec.tranTime.set(wsaaTime);
		ovrduerec.termid.set(varcom.vrcmTermid);
		ovrduerec.crtable.set(covrmjaIO.getCrtable());
		ovrduerec.pumeth.set(sv.pumeth);
		ovrduerec.billfreq.set(payrIO.getBillfreq());
		ovrduerec.instprem.set(ZERO);
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.sumins.set(ZERO);
		ovrduerec.crrcd.set(ZERO);
		ovrduerec.newCrrcd.set(ZERO);
		ovrduerec.newSingp.set(ZERO);
		ovrduerec.newAnb.set(ZERO);
		ovrduerec.newPua.set(ZERO);
		ovrduerec.newInstprem.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.premCessDate.set(ZERO);
		ovrduerec.cnttype.set(chdrmjaIO.getCnttype());
		ovrduerec.occdate.set(chdrmjaIO.getOccdate());
		ovrduerec.polsum.set(chdrmjaIO.getPolsum());
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES)) {	//ILIFE-8179
			ovrduerec.aloind.set(t6647rec.aloind);
			ovrduerec.efdcode.set(t6647rec.efdcode);
			ovrduerec.procSeqNo.set(t6647rec.procSeqNo);
		}
		/*    Call the CALCFEE subroutine to calculate OVRD-PUPFEE.*/
		callProgram(Calcfee.class, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, varcom.oK)) {
			syserrrec.params.set(ovrduerec.ovrdueRec);
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
		ovrduerec.newRiskCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newPremCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newBonusDate.set(varcom.vrcmMaxDate);
		ovrduerec.newRerateDate.set(varcom.vrcmMaxDate);
		ovrduerec.cvRefund.set(ZERO);
		ovrduerec.surrenderValue.set(ZERO);
		ovrduerec.etiYears.set(ZERO);
		ovrduerec.etiDays.set(ZERO);
	}

protected void updateChdrmja3400()
	{
		start3410();
	}

protected void start3410()
	{
		chdrmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setValidflag("1");
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		wsaaCovrRskMatch.set("N");
		for (ix.set(1); !(isGT(ix, 12)
		|| covrRskMatch.isTrue()); ix.add(1)){
			readRist5100();
		}
		/*  Use T5679 as a 'catch all' if the T5399 hierarchy does not*/
		/*  contain the coverage risk.*/
		if (!covrRskMatch.isTrue()) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}
		/*  Perform the same processing as above with the premium statcode*/
		wsaaCovrPrmMatch.set("N");
		for (ix.set(1); !(isGT(ix, 12)
		|| covrPrmMatch.isTrue()); ix.add(1)){
			readPrst5200();
		}
		if (!covrPrmMatch.isTrue()) {
			chdrmjaIO.setPstatcode(t5679rec.setCnPremStat);
		}
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && ("P").equals(wsspcomn.flag.toString())) {	//ILIFE-8179
			chdrmjaIO.setBtdate(sv.btdate);
			chdrmjaIO.setPtdate(sv.ptdate);
			chdrmjaIO.setBillcd(wsaaBillcd);
		}
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void updatePtrn3500()
	{
		start3510();
	}

protected void start3510()
	{
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setValidflag("1");
		if(!prmhldtrad || isEQ(ta524rec.ta524Rec, SPACES))	//ILIFE-8179
			ptrnIO.setPtrneff(chdrmjaIO.getBtdate());
		else
			ptrnIO.setPtrneff(ovrduerec.ptdate);
		ptrnIO.setTermid(SPACES);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);
		ptrnIO.setTransactionDate(getCobolDate());
		//ptrnIO.setTransactionTime(getCobolTime());
		ptrnIO.setTransactionTime(varcom.vrcmTime.toInt());
		//ptrnIO.setTransactionDate(varcom.vrcmDate.toInt());
		ptrnIO.setUser(999999);
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void processPayr3600()
	{
		readPayr3610();
		validflag23620();
		validflag13630();
	}

protected void readPayr3610()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag23620()
	{
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13630()
	{
		payrIO.setValidflag("1");
		payrIO.setPstatcode(chdrmjaIO.getPstatcode());
		payrIO.setTranno(chdrmjaIO.getTranno());
		if(prmhldtrad && isNE(ta524rec.ta524Rec, SPACES) && ("P").equals(wsspcomn.flag.toString())) {	//ILIFE-8179
			payrIO.setBtdate(chdrmjaIO.getBtdate());
			payrIO.setPtdate(chdrmjaIO.getPtdate());
			payrIO.setEffdate(wsaaPrevBtDate);
			payrIO.setBillcd(wsaaBillcd);
			payrIO.setNextdate(wsaaNextdate);
		}
		else
			payrIO.setEffdate(chdrmjaIO.getBtdate());
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void printLetter3700()
	{
		start3710();
	}

protected void start3710()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***");
			stringVariable2.addExpression(wsaaBatckey.batcBatctrcde);
			stringVariable2.setStringInto(itemIO.getItemitem());
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				itemIO.setGenarea(SPACES);
			}
			else {
				tr384rec.tr384Rec.set(itemIO.getGenarea());
			}
		}
		else {
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrmjaIO.getChdrpfx());
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdoccoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.despnum.set(chdrmjaIO.getDespnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		letcokcpy.recCode.set("LD");
		letcokcpy.ldDate.set(wsaaToday);
		letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(letrqstrec.statuz);
			syserrrec.params.set(letrqstrec.params);
			fatalError600();
		}
	}

protected void statistic3800()
	{
		start3810();
	}

protected void start3810()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void releaseSoftlock3900()
	{
		start3910();
	}

protected void start3910()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadCovrStatuses5000()
	{
		/*START*/
		/*  If this COVR's statuses are to be loaded, ensure we have not*/
		/*  exceeded the number of occurences in WSAA-COVR-REC.*/
		if (isGT(wsaaCovrIx, 48)) {
			syserrrec.statuz.set(errorsInner.h791);
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		wsaaCovrPrst[wsaaCovrIx.toInt()].set(covrmjaIO.getPstatcode());
		wsaaCovrRist[wsaaCovrIx.toInt()].set(covrmjaIO.getStatcode());
		wsaaCovrIx.add(1);
		/*EXIT*/
	}

protected void readRist5100()
	{
		/*START*/
		if (isEQ(t5399rec.covRiskStat[ix.toInt()], "  ")) {
			return ;
		}
		wsaaCovrIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)){
				if (isEQ(t5399rec.covRiskStat[ix.toInt()], wsaaCovrRist[wsaaCovrIx.toInt()])) {
					chdrmjaIO.setStatcode(t5399rec.setCnRiskStat[ix.toInt()]);
					wsaaCovrRskMatch.set("Y");
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
		/*EXIT*/
	}

protected void readPrst5200()
	{
		/*START*/
		if (isEQ(t5399rec.covPremStat[ix.toInt()], "  ")) {
			return ;
		}
		wsaaCovrIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)){
				if (isEQ(t5399rec.covPremStat[ix.toInt()], wsaaCovrPrst[wsaaCovrIx.toInt()])) {
					chdrmjaIO.setPstatcode(t5399rec.setCnPremStat[ix.toInt()]);
					wsaaCovrPrmMatch.set("Y");
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
		/*EXIT*/
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present*/
		/* If so, the appropriate parameters are filled and the*/
		/* diary processor is called.*/
		wsaaTableCnttype.set(chdrmjaIO.getCnttype());
		wsaaTableBatctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaTableCnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(wsaaToday);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(wsaaToday);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.*/
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(varcom.vrcmMaxDate);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaTableKey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}
	//ILIFE-8179
	protected void getBtPtdate() {
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(ta524rec.maxphprd.toInt());
		datcon2Pojo.setIntDate1(sv.ptdate.toString());
		datcon2Pojo.setFrequency("12");
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		sv.ptdate.set(datcon2Pojo.getIntDate2());
		datcon2Pojo.setIntDate1(sv.btdate.toString());
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		wsaaPrevBtDate.set(sv.btdate);
		sv.btdate.set(datcon2Pojo.getIntDate2());
		datcon2Pojo.setFreqFactor(ta524rec.maxphprd.toInt());
		datcon2Pojo.setIntDate1(payrIO.getBillcd().toString());
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		wsaaBillcd.set(datcon2Pojo.getIntDate2());
		datcon2Pojo.setFreqFactor(t6654rec.leadDays.toInt()*(-1));
		datcon2Pojo.setIntDate1(wsaaBillcd.toString());
		datcon2Pojo.setFrequency("DY");
		datcon2Utils.calDatcon2(datcon2Pojo);
		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			syserrrec.statuz.set(datcon2Pojo.getStatuz());
			syserrrec.params.set(datcon2Pojo.toString());
			fatalError600();
		}
		wsaaNextdate.set(datcon2Pojo.getIntDate2());
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
	private FixedLengthStringData e300 = new FixedLengthStringData(4).init("E300");
	private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
	private FixedLengthStringData g381 = new FixedLengthStringData(4).init("G381");
	private FixedLengthStringData h010 = new FixedLengthStringData(4).init("H010");
	private FixedLengthStringData h115 = new FixedLengthStringData(4).init("H115");
	private FixedLengthStringData h791 = new FixedLengthStringData(4).init("H791");
	private FixedLengthStringData hl38 = new FixedLengthStringData(4).init("HL38");
	private FixedLengthStringData rlbs = new FixedLengthStringData(4).init("RLBS");
	private FixedLengthStringData rgh5 = new FixedLengthStringData(4).init("RGH5");
	private FixedLengthStringData rlbu = new FixedLengthStringData(4).init("RLBU");
	private FixedLengthStringData rrrh = new FixedLengthStringData(4).init("RRRH");	//ILIFE-8179
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t5399 = new FixedLengthStringData(5).init("T5399");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6597 = new FixedLengthStringData(5).init("T6597");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData th584 = new FixedLengthStringData(5).init("TH584");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData tr51p = new FixedLengthStringData(5).init("TR51P");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData ta524 = new FixedLengthStringData(5).init("TA524");	//ILIFE-8179
	private FixedLengthStringData t6654 = new FixedLengthStringData(5).init("T6654");	//ILIFE-8179
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
