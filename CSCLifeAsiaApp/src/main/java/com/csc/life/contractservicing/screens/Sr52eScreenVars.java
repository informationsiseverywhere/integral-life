package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52E
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr52eScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(414);
	public FixedLengthStringData dataFields = new FixedLengthStringData(78).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData taxinds = new FixedLengthStringData(13).isAPartOf(dataFields, 60);
	public FixedLengthStringData[] taxind = FLSArrayPartOfStructure(13, 1, taxinds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(taxinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData taxind01 = DD.taxind.copy().isAPartOf(filler,0);
	public FixedLengthStringData taxind02 = DD.taxind.copy().isAPartOf(filler,1);
	public FixedLengthStringData taxind03 = DD.taxind.copy().isAPartOf(filler,2);
	public FixedLengthStringData taxind04 = DD.taxind.copy().isAPartOf(filler,3);
	public FixedLengthStringData taxind05 = DD.taxind.copy().isAPartOf(filler,4);
	public FixedLengthStringData taxind06 = DD.taxind.copy().isAPartOf(filler,5);
	public FixedLengthStringData taxind07 = DD.taxind.copy().isAPartOf(filler,6);
	public FixedLengthStringData taxind08 = DD.taxind.copy().isAPartOf(filler,7);
	public FixedLengthStringData taxind09 = DD.taxind.copy().isAPartOf(filler,8);
	public FixedLengthStringData taxind10 = DD.taxind.copy().isAPartOf(filler,9);
	public FixedLengthStringData taxind11 = DD.taxind.copy().isAPartOf(filler,10);
	public FixedLengthStringData taxind12 = DD.taxind.copy().isAPartOf(filler,11);
	public FixedLengthStringData taxind13 = DD.taxind.copy().isAPartOf(filler,12);//ALS-4706
	public FixedLengthStringData txitem = DD.txitem.copy().isAPartOf(dataFields,73);
	public FixedLengthStringData zbastyp = DD.zbastyp.copy().isAPartOf(dataFields,77);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 78);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData taxindsErr = new FixedLengthStringData(52).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] taxindErr = FLSArrayPartOfStructure(12, 4, taxindsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(52).isAPartOf(taxindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData taxind01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData taxind02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData taxind03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData taxind04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData taxind05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData taxind06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData taxind07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData taxind08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData taxind09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData taxind10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData taxind11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData taxind12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData taxind13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);//ALS-4706
	public FixedLengthStringData txitemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData zbastypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 162);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData taxindsOut = new FixedLengthStringData(156).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 12, taxindsOut, 0);
	public FixedLengthStringData[][] taxindO = FLSDArrayPartOfArrayStructure(12, 1, taxindOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(156).isAPartOf(taxindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] taxind01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] taxind02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] taxind03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] taxind04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] taxind05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] taxind06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] taxind07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] taxind08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] taxind09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] taxind10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] taxind11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] taxind12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] taxind13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);//ALS-4706
	public FixedLengthStringData[] txitemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] zbastypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr52escreenWritten = new LongData(0);
	public LongData Sr52eprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52eScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(taxind13Out,new String[] {null, null, null,"01",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, taxind05, taxind01, taxind02, taxind03, taxind04, taxind06, taxind08, taxind10, taxind07, taxind09, taxind11, txitem, zbastyp, taxind12,taxind13};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, taxind05Out, taxind01Out, taxind02Out, taxind03Out, taxind04Out, taxind06Out, taxind08Out, taxind10Out, taxind07Out, taxind09Out, taxind11Out, txitemOut, zbastypOut, taxind12Out,taxind13Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, taxind05Err, taxind01Err, taxind02Err, taxind03Err, taxind04Err, taxind06Err, taxind08Err, taxind10Err, taxind07Err, taxind09Err, taxind11Err, txitemErr, zbastypErr, taxind12Err,taxind13Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52escreen.class;
		protectRecord = Sr52eprotect.class;
	}

}
