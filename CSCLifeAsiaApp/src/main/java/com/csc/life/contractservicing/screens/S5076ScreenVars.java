package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5076
 * @version 1.0 generated on 30/08/09 06:33
 * @author Quipoz
 */
public class S5076ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(195+DD.cltaddr.length*5); //Starts ILIFE-3212
	public FixedLengthStringData dataFields = new FixedLengthStringData(67+DD.cltaddr.length*5).isAPartOf(dataArea, 0);
	public FixedLengthStringData cltaddrs = new FixedLengthStringData(DD.cltaddr.length*5).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] cltaddr = FLSArrayPartOfStructure(5, DD.cltaddr.length, cltaddrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(cltaddrs.length()).isAPartOf(cltaddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01 = DD.cltaddr.copy().isAPartOf(filler,0);
	public FixedLengthStringData cltaddr02 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*1);
	public FixedLengthStringData cltaddr03 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*2);
	public FixedLengthStringData cltaddr04 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*3);
	public FixedLengthStringData cltaddr05 = DD.cltaddr.copy().isAPartOf(filler,DD.cltaddr.length*4);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(dataFields,DD.cltaddr.length*5);
	public FixedLengthStringData jownname = DD.jownname.copy().isAPartOf(dataFields,10+DD.cltaddr.length*5);
	public FixedLengthStringData jownsel = DD.jownsel.copy().isAPartOf(dataFields,57+DD.cltaddr.length*5);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 67+DD.cltaddr.length*5); //End ILIFE-3212
	public FixedLengthStringData cltaddrsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] cltaddrErr = FLSArrayPartOfStructure(5, 4, cltaddrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(cltaddrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData cltaddr02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData cltaddr03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData cltaddr04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData cltaddr05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData cltpcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jownnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jownselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 99+DD.cltaddr.length*5); //ILIFE-3212
	public FixedLengthStringData cltaddrsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] cltaddrOut = FLSArrayPartOfStructure(5, 12, cltaddrsOut, 0);
	public FixedLengthStringData[][] cltaddrO = FLSDArrayPartOfArrayStructure(12, 1, cltaddrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(cltaddrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cltaddr01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] cltaddr02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] cltaddr03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] cltaddr04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] cltaddr05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] cltpcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jownnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jownselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5076screenWritten = new LongData(0);
	public LongData S5076windowWritten = new LongData(0);
	public LongData S5076hideWritten = new LongData(0);
	public LongData S5076protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5076ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(jownselOut,new String[] {"01","50","-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {jownsel, jownname, cltaddr01, cltaddr02, cltaddr03, cltaddr04, cltpcode, cltaddr05};
		screenOutFields = new BaseData[][] {jownselOut, jownnameOut, cltaddr01Out, cltaddr02Out, cltaddr03Out, cltaddr04Out, cltpcodeOut, cltaddr05Out};
		screenErrFields = new BaseData[] {jownselErr, jownnameErr, cltaddr01Err, cltaddr02Err, cltaddr03Err, cltaddr04Err, cltpcodeErr, cltaddr05Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5076screen.class;
		protectRecord = S5076protect.class;
		hideRecord = S5076hide.class;
	}

}
