package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:56
 * Description:
 * Copybook name: TOTLOANREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Totloanrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData totloanRec = new FixedLengthStringData(104);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(totloanRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(totloanRec, 5);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(totloanRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(totloanRec, 10);
  	public PackedDecimalData principal = new PackedDecimalData(17, 2).isAPartOf(totloanRec, 18);
  	public PackedDecimalData interest = new PackedDecimalData(17, 2).isAPartOf(totloanRec, 27);
  	public ZonedDecimalData loanCount = new ZonedDecimalData(2, 0).isAPartOf(totloanRec, 36).setUnsigned();
  	public PackedDecimalData effectiveDate = new PackedDecimalData(8, 0).isAPartOf(totloanRec, 38);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(22).isAPartOf(totloanRec, 43);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(totloanRec, 65);
  	public FixedLengthStringData tranid = new FixedLengthStringData(16).isAPartOf(totloanRec, 68);
  	public FixedLengthStringData tranTerm = new FixedLengthStringData(4).isAPartOf(tranid, 0);
  	public PackedDecimalData tranDate = new PackedDecimalData(6, 0).isAPartOf(tranid, 4);
  	public PackedDecimalData tranTime = new PackedDecimalData(6, 0).isAPartOf(tranid, 8);
  	public PackedDecimalData tranUser = new PackedDecimalData(6, 0).isAPartOf(tranid, 12);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(totloanRec, 84);
  	public FixedLengthStringData postFlag = new FixedLengthStringData(1).isAPartOf(totloanRec, 85);
  	public FixedLengthStringData filler = new FixedLengthStringData(18).isAPartOf(totloanRec, 86, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(totloanRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		totloanRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}