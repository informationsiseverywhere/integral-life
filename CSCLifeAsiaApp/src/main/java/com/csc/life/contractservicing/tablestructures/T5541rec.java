package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:39
 * Description:
 * Copybook name: T5541REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5541rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5541Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData freqcys = new FixedLengthStringData(24).isAPartOf(t5541Rec, 0);
  	public FixedLengthStringData[] freqcy = FLSArrayPartOfStructure(12, 2, freqcys, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(freqcys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freqcy01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData freqcy02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData freqcy03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData freqcy04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData freqcy05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData freqcy06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData freqcy07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData freqcy08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData freqcy09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData freqcy10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData freqcy11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData freqcy12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData lfacts = new FixedLengthStringData(60).isAPartOf(t5541Rec, 24);
  	public ZonedDecimalData[] lfact = ZDArrayPartOfStructure(12, 5, 4, lfacts, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(lfacts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData lfact01 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 0);
  	public ZonedDecimalData lfact02 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 5);
  	public ZonedDecimalData lfact03 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 10);
  	public ZonedDecimalData lfact04 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 15);
  	public ZonedDecimalData lfact05 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 20);
  	public ZonedDecimalData lfact06 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 25);
  	public ZonedDecimalData lfact07 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 30);
  	public ZonedDecimalData lfact08 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 35);
  	public ZonedDecimalData lfact09 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 40);
  	public ZonedDecimalData lfact10 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 45);
  	public ZonedDecimalData lfact11 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 50);
  	public ZonedDecimalData lfact12 = new ZonedDecimalData(5, 4).isAPartOf(filler1, 55);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(416).isAPartOf(t5541Rec, 84, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5541Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5541Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}