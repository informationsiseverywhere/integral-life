package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FupepfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:55
 * Class transformed from FUPEPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FupepfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 150;
	public FixedLengthStringData fuperec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData fupepfRecord = fuperec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(fuperec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(fuperec);
	public PackedDecimalData fupno = DD.fupno.copy().isAPartOf(fuperec);
	public FixedLengthStringData clamnum = DD.clamnum.copy().isAPartOf(fuperec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(fuperec);
	public FixedLengthStringData docseq = DD.docseq.copy().isAPartOf(fuperec);
	public FixedLengthStringData message = DD.message.copy().isAPartOf(fuperec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(fuperec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(fuperec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(fuperec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public FupepfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for FupepfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public FupepfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for FupepfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public FupepfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for FupepfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public FupepfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("FUPEPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"FUPNO, " +
							"CLAMNUM, " +
							"TRANNO, " +
							"DOCSEQ, " +
							"MESSAGE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     fupno,
                                     clamnum,
                                     tranno,
                                     docseq,
                                     message,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		fupno.clear();
  		clamnum.clear();
  		tranno.clear();
  		docseq.clear();
  		message.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getFuperec() {
  		return fuperec;
	}

	public FixedLengthStringData getFupepfRecord() {
  		return fupepfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setFuperec(what);
	}

	public void setFuperec(Object what) {
  		this.fuperec.set(what);
	}

	public void setFupepfRecord(Object what) {
  		this.fupepfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(fuperec.getLength());
		result.set(fuperec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}