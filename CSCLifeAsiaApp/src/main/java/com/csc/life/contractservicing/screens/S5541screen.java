package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5541screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5541ScreenVars sv = (S5541ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5541screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5541ScreenVars screenVars = (S5541ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.lfact01.setClassString("");
		screenVars.lfact02.setClassString("");
		screenVars.lfact03.setClassString("");
		screenVars.lfact04.setClassString("");
		screenVars.lfact05.setClassString("");
		screenVars.lfact06.setClassString("");
		screenVars.lfact07.setClassString("");
		screenVars.lfact08.setClassString("");
		screenVars.lfact09.setClassString("");
		screenVars.lfact10.setClassString("");
		screenVars.lfact11.setClassString("");
		screenVars.lfact12.setClassString("");
		screenVars.freqcy01.setClassString("");
		screenVars.freqcy02.setClassString("");
		screenVars.freqcy03.setClassString("");
		screenVars.freqcy04.setClassString("");
		screenVars.freqcy05.setClassString("");
		screenVars.freqcy06.setClassString("");
		screenVars.freqcy07.setClassString("");
		screenVars.freqcy08.setClassString("");
		screenVars.freqcy09.setClassString("");
		screenVars.freqcy10.setClassString("");
		screenVars.freqcy11.setClassString("");
		screenVars.freqcy12.setClassString("");
	}

/**
 * Clear all the variables in S5541screen
 */
	public static void clear(VarModel pv) {
		S5541ScreenVars screenVars = (S5541ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.lfact01.clear();
		screenVars.lfact02.clear();
		screenVars.lfact03.clear();
		screenVars.lfact04.clear();
		screenVars.lfact05.clear();
		screenVars.lfact06.clear();
		screenVars.lfact07.clear();
		screenVars.lfact08.clear();
		screenVars.lfact09.clear();
		screenVars.lfact10.clear();
		screenVars.lfact11.clear();
		screenVars.lfact12.clear();
		screenVars.freqcy01.clear();
		screenVars.freqcy02.clear();
		screenVars.freqcy03.clear();
		screenVars.freqcy04.clear();
		screenVars.freqcy05.clear();
		screenVars.freqcy06.clear();
		screenVars.freqcy07.clear();
		screenVars.freqcy08.clear();
		screenVars.freqcy09.clear();
		screenVars.freqcy10.clear();
		screenVars.freqcy11.clear();
		screenVars.freqcy12.clear();
	}
}
