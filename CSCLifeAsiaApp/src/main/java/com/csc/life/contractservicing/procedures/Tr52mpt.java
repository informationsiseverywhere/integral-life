/*
 * File: Tr52mpt.java
 * Date: December 3, 2013 3:59:32 AM ICT
 * Author: CSC
 * 
 * Class transformed from TR52MPT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.contractservicing.tablestructures.Tr52mrec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR52M.
*
*
*
****************************************************************** ****
* </pre>
*/
public class Tr52mpt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr52mrec tr52mrec = new Tr52mrec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr52mpt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr52mrec.tr52mRec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(tr52mrec.ratebas);
		generalCopyLinesInner.fieldNo006.set(tr52mrec.basind);
		generalCopyLinesInner.fieldNo007.set(tr52mrec.fieldEditCode);
		generalCopyLinesInner.fieldNo008.set(tr52mrec.medpc01);
		generalCopyLinesInner.fieldNo009.set(tr52mrec.medpc02);
		generalCopyLinesInner.fieldNo010.set(tr52mrec.admfee);
		generalCopyLinesInner.fieldNo011.set(tr52mrec.admpc01);
		generalCopyLinesInner.fieldNo012.set(tr52mrec.admpc02);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine001, 26, FILLER).init("Freelook Cancellation Rule                   SR52M");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(68);
	private FixedLengthStringData filler7 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine003, 0, FILLER).init("    Premium Refund Basis............:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine003, 38);
	private FixedLengthStringData filler8 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine003, 39, FILLER).init("    ( P - Refund full premium");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(62);
	private FixedLengthStringData filler9 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine004, 43, FILLER).init("( U - Refund unit )");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(59);
	private FixedLengthStringData filler11 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine005, 9, FILLER).init("If Refund Unit, NAV Basis..:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 38);
	private FixedLengthStringData filler13 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine005, 39, FILLER).init("    ( 1 - Issue date");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(66);
	private FixedLengthStringData filler14 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine006, 43, FILLER).init("( 2 - Cancellation date");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(61);
	private FixedLengthStringData filler16 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine007, 43, FILLER).init("( 3 - Input date )");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(48);
	private FixedLengthStringData filler18 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine008, 6, FILLER).init("Editable......................:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 38);
	private FixedLengthStringData filler20 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 39, FILLER).init("    (Y/N)");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(26);
	private FixedLengthStringData filler21 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine009, 0, FILLER).init("    Medical Fee Clawback :");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(36);
	private FixedLengthStringData filler22 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine010, 6, FILLER).init("( Refer Medical Bill payment )");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(41);
	private FixedLengthStringData filler24 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler25 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine011, 6, FILLER).init("Percentage from Agent.........:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 38).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(41);
	private FixedLengthStringData filler26 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler27 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine012, 6, FILLER).init("Percentage from Assured.......:");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 38).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(37);
	private FixedLengthStringData filler28 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine013, 0, FILLER).init("    Administration Charges Clawback :");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(52);
	private FixedLengthStringData filler29 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine014, 6, FILLER).init("Admin.Charges.................:");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine014, 38).setPattern("ZZZZZZZZZZ.ZZ-");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(41);
	private FixedLengthStringData filler31 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler32 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine015, 6, FILLER).init("Percentage from Agent.........:");
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 38).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(41);
	private FixedLengthStringData filler33 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler34 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine016, 6, FILLER).init("Percentage from Assured.......:");
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 38).setPattern("ZZZ");
}
}
