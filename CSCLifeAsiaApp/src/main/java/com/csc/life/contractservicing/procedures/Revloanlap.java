/*
 * File: Zrevlaps.java
 * Date: 30 August 2009 2:56:02
 * Author: Quipoz Limited
 *
 * Class transformed from ZREVLAPS.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.recordstructures.Chdrlifkey;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

/**
 * <pre>
 *REMARKS
 *
 *
 *(c) Copyright Continuum Corporation Ltd.  1986....1995.
 *    All rights reserved.  Continuum Confidential.
 *
 *  REVLOANLAP - Auto Lapse/Paid-up Reversal (Clone from REVLAPS)
 *  -----------------------------------------------------------
 *
 *  This subroutine is called by the Full Contract Reversal program
 *  P5155 via Table T6661. The parameter passed to this subroutine
 *  is contained in the REVERSEREC copybook.
 *
 *	According from the BSD-ICIL-LIFE-POS032, there is no change to the accounting movement.
 *	The lapse processing will only reverse the previous contract billing and previous overdue processing and change the Contract Risk Status
 *	So we don't need the ACMVs. Only do the contract status reversal.
 *  Because the subroutine : Overdue processing(Overbill.java) is basically works in the same way as the full Contract Reversal AT module, REVGENAT.
 *  So we don't need add extra methods to reverse the contract billing.
 *
 *  The following transactions are reversed :
 *
 *  - CHDR
 *
 *       READH on  the  Contract  Header record (CHDRLIF) for the
 *            relevant contract.
 *       DELET this record.
 *       NEXTR on  the  Contract  Header record (CHDRLIF) for the
 *            same contract. This should read a record with
 *            valid flag of '2'. If not, then this is an error.
 *       REWRT this   Contract   Header  record  (CHDRLIF)  after
 *            altering  the  valid  flag from '2' to '1'.
 *
 *  - COVRs
 *
 *       Only reverse COVR records whose TRANNO is equal to the
 *           Transaction number (of the PTRN) being reversed.
 *       READH on  the  COVR  records for the relevant contract.
 *       DELET this record.
 *       NEXTR on  the  COVR  file should read a record with valid
 *            flag  of  '2'. If not, then this is an error.
 *       REWRT this  COVR  record after altering the valid flag
 *            from  '2'  to  '1' . This will reinstate the
 *            coverage prior to the LAPSE/PUP.
 *
 * All of these records are reversed when REVGENAT, the AT module,
 *  calls the statistical subroutine LIFSTTR.
 *
 * NO processing is therefore required in this subroutine.
 *
 *
 ****************************************************************
 * </pre>
 */
public class Revloanlap extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVLOANLAP";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);

	/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t1688 = "T1688";
	/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String covrrec = "COVRREC";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
//	private CovrTableDAM covrIO = new CovrTableDAM();
//	private DescTableDAM descIO = new DescTableDAM();
//	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Reverserec reverserec = new Reverserec();

	public Revloanlap() {
		super();
	}

    private ChdrpfDAO chdrpfDao = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Chdrpf chdrpf = null;
	private List<Chdrpf> chdrpflist = null;
    private Descpf descpf = null;
    private Itempf itempf = null;
	private Covrpf covrpf = null;
	private List<Covrpf> covrpfList = null;
	List<Covrpf> covrpfUpdateList = new ArrayList<>();
	List<Covrpf> covrpfDeleteList = new ArrayList<>();

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

	protected void initialise1000()
	{
		initialise1001();
	}

	protected void initialise1001()
	{
		syserrrec.subrname.set(wsaaSubr);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
        descpf=descDAO.getdescData("IT", t1688 ,reverserec.batctrcde.toString().trim(), reverserec.company.toString(), reverserec.language.toString());
        if (descpf == null) {
            wsaaRevTrandesc.set(SPACES);
        } else {
            wsaaRevTrandesc.set(descpf.getLongdesc());
        }
	}

	protected void process2000()
	{
		/*PROCESS*/
		processChdrs2100();
		processCovrs3000();
		/*EXIT*/
	}

	protected void processChdrs2100()
	{
		start2101();
	}

	protected void start2101()
	{
		/*  delete valid flag 1 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getValidflag(),1)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		chdrlifIO.setFunction(varcom.delet);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*    Select the next record which will be the valid flag 2 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		/* MOVE BEGNH                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrlifIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrcoy(),reverserec.company)
				|| isNE(chdrlifIO.getChdrnum(),reverserec.chdrnum)
				|| isNE(chdrlifIO.getValidflag(),2)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.writd);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}
	protected void processCovrs3000()
	{
		covers3001();
	}

	protected void covers3001()
	{
		Boolean updateflag = true;
		Integer tranNoTemp = 0;
		covrpfList = covrpfDAO.getCovrpfByTranno(reverserec.company.toString(),reverserec.chdrnum.toString());
		if(covrpfList != null && covrpfList.size() > 0)
		{
			for(Covrpf covr : covrpfList) {
				/*    If this component has a TRANNO equal to the TRANNO we are*/
				/*    reversing, then it has been Lapsed/Paid-up and must now be*/
				/*    reversed.  If not, then find the next component.*/
				if (isEQ(covr.getValidflag(),"1") && isEQ(covr.getTranno(), reverserec.tranno)) {
					covrpfDeleteList.add(covr);
				}
				if(isEQ(covr.getValidflag(),"2") && updateflag && isNE(covr.getTranno(), reverserec.tranno)){
					if(tranNoTemp == 0){
						covrpfUpdateList.add(covr);
						tranNoTemp = covr.getTranno();
					}else if(isEQ(tranNoTemp,covr.getTranno())){
						covrpfUpdateList.add(covr);
					}else{
						updateflag = false;
					}
				}
			}
		}
		if(covrpfDeleteList !=null && covrpfUpdateList != null){
			/* delete the lapsed one */
			covrpfDAO.deleteCovrpfRecord(covrpfDeleteList);
			/*change the latest tranno validflag to 1*/
			covrpfDAO.updateCovrValidFlagTo1(covrpfUpdateList);
		}
		//processGenericSubr3300();
	}

	protected void processGenericSubr3300()
	{
		readGenericTable3301();
	}

	protected void loadT56714100() {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(reverserec.company.toString());
		itempf.setItemtabl(t5671);
		itempf.setItemitem(wsaaT5671Item.toString());
		itempf = itempfDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			xxxxFatalError();
		}
		t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

	protected void readGenericTable3301()
	{
		if(covrpfUpdateList != null && covrpfUpdateList.size() > 0){
			for(Covrpf covr : covrpfUpdateList) {
				wsaaT5671Trancode.set(reverserec.oldBatctrcde);
				wsaaT5671Crtable.set(covr.getCrtable());
				loadT56714100();
				greversrec.chdrcoy.set(covr.getChdrcoy());
				greversrec.chdrnum.set(covr.getChdrnum());
				greversrec.tranno.set(reverserec.tranno);
				/* MOVE ZEROES                 TO GREV-PLAN-SUFFIX.*/
				greversrec.planSuffix.set(covr.getPlanSuffix());
				greversrec.life.set(covr.getLife());
				greversrec.coverage.set(covr.getCoverage());
				greversrec.rider.set(covr.getRider());
				greversrec.batckey.set(reverserec.batchkey);
				greversrec.termid.set(reverserec.termid);
				greversrec.user.set(reverserec.user);
				greversrec.newTranno.set(reverserec.newTranno);
				greversrec.transDate.set(reverserec.transDate);
				greversrec.transTime.set(reverserec.transTime);
				greversrec.language.set(reverserec.language);
				for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
					callSubprog3400();
				}
			}
		}
	}

	protected void callSubprog3400()
	{
		/*GO*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
		}
		if (isNE(greversrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(greversrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}
	protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorBomb();
	}

	protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void xxxxErrorBomb()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
}
