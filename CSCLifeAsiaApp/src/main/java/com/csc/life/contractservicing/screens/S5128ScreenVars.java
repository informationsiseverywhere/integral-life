package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5128
 * @version 1.0 generated on 30/08/09 06:35
 * @author Quipoz
 */
public class S5128ScreenVars extends SmartVarModel { 

    //ILIFE-1403 START by nnazeer
	public FixedLengthStringData dataArea = new FixedLengthStringData(478);
	public FixedLengthStringData dataFields = new FixedLengthStringData(206).isAPartOf(dataArea, 0); //ILIFE-1403 END
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData lifeorcov = DD.lifeorcov.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,109);
	 //ILIFE-1403 START by nnazeer
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,156);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,159);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,179);
	public FixedLengthStringData entity = DD.entity.copy().isAPartOf(dataFields,182);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,198);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,202);
	//ILIFE-1403 END
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 206);  //ILIFE-1403 START by nnazeer
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifeorcovErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	 //ILIFE-1403 START by nnazeer
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData entityErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	//ILIFE-1403 END
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 274); //ILIFE-1403 START by nnazeer
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifeorcovOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	 //ILIFE-1403 START by nnazeer
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] entityOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	//ILIFE-1403 END

	public FixedLengthStringData subfileArea = new FixedLengthStringData(122);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(40).isAPartOf(subfileArea, 0);
	public FixedLengthStringData crcode = DD.crcode.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData hrequired = DD.hrequired.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(subfileFields,5);
	public FixedLengthStringData rtable = DD.rtable.copy().isAPartOf(subfileFields,35);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,39);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20).isAPartOf(subfileArea, 40);
	public FixedLengthStringData crcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData hrequiredErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData rtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 60);
	public FixedLengthStringData[] crcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] hrequiredOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] rtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 120);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5128screensflWritten = new LongData(0);
	public LongData S5128screenctlWritten = new LongData(0);
	public LongData S5128screenWritten = new LongData(0);
	public LongData S5128protectWritten = new LongData(0);
	public GeneralTable s5128screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5128screensfl;
	}

	public S5128ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"12","01","-12",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, crcode, rtable, longdesc, hrequired};
		screenSflOutFields = new BaseData[][] {selectOut, crcodeOut, rtableOut, longdescOut, hrequiredOut};
		screenSflErrFields = new BaseData[] {selectErr, crcodeErr, rtableErr, longdescErr, hrequiredErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, life, lifcnum, linsname, jlife, jlifcnum, jlinsname, lifeorcov,register,chdrstatus,premstatus,cntcurr,entity,numpols,planSuffix}; //ILIFE-1403 START by nnazeer
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, lifeOut, lifcnumOut, linsnameOut, jlifeOut, jlifcnumOut, jlinsnameOut, lifeorcovOut,registerOut,chdrstatusOut,premstatusOut,cntcurrOut,entityOut,numpolsOut,plnsfxOut}; //ILIFE-1403 START by nnazeer
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, lifeErr, lifcnumErr, linsnameErr, jlifeErr, jlifcnumErr, jlinsnameErr, lifeorcovErr,registerErr,chdrstatusErr,premstatusErr,cntcurrErr,entityErr,numpolsErr,plnsfxErr}; //ILIFE-1403 START by nnazeer
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5128screen.class;
		screenSflRecord = S5128screensfl.class;
		screenCtlRecord = S5128screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5128protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5128screenctl.lrec.pageSubfile);
	}
}
