/*
 * File: Pd5lm.java
 * Date: 30 August 2009 0:37:43
 * Author: Quipoz Limited
 * 
 * Class transformed from Pd5lm.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;


import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.contractservicing.dataaccess.dao.YctxpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Yctxpf;
import com.csc.life.contractservicing.procedures.BonusDtn;
import com.csc.life.contractservicing.procedures.TaxComputeDtn;
import com.csc.life.contractservicing.procedures.UnitDtn;
import com.csc.life.contractservicing.recordstructures.TaxDeductionRec;
import com.csc.life.contractservicing.screens.Sd5lmScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.Tr59xrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pd5lm extends ScreenProgCS {
	public static final String ROUTINE = QPUtilities.getThisClass();	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5LM");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sd5lmScreenVars sv = ScreenProgram.getScreenVars(Sd5lmScreenVars.class);
	private Wssplife wssplife = new Wssplife();
	private DescpfDAO descpfDAO = DAOFactory.getDescpfDAO();
	private Zctxpf zctxpf = null;
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);	
	private TaxDeductionRec taxDeductionRec;
	private Yctxpf yctxpf;
	private YctxpfDAO yctxpfDAO = getApplicationContext().getBean("yctxpfDAO", YctxpfDAO.class);
	private ZonedDecimalData total = new ZonedDecimalData(17,2); 
	private Tr59xrec tr59xrec = new Tr59xrec(); 
	private Batckey batcKey = new Batckey();
	private Datcon1rec datcon1 = new Datcon1rec(); 
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Lifacmvrec lifacmvrec = new Lifacmvrec();	
	private String trdesc;
	private Covrpf covrpf;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Itempf itempf ;
	private ItemDAO itemDAO= getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);	
	private Chdrpf chdrpf = null;
	private boolean fieldUpdated;	
	private Map<String, List<Covrpf>> covrpfMap;
	private T5645rec t5645rec = new T5645rec(); 
	
	public Pd5lm() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5lm", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	@Override
	protected void initialise1000() {	

		syserrrec.subrname.set(wsaaProg);
		fieldUpdated = false;
		sv.dataArea.initialise();
		total.set(ZERO);
		sv.chdrnum.set(wsspcomn.chdrChdrnum);
		sv.cnttype.set(wsspcomn.chdrCnttype);
		Descpf descpf = descpfDAO.getItemTblItemByLang("IT", "T5688", wsspcomn.chdrCnttype.value(),
				wsspcomn.language.toString(), wsspcomn.company.toString());
		Smtpfxcpy smtpfxcpy = new Smtpfxcpy();	
		if (descpf != null) {
			sv.cntypdesc.set(descpf.getLongdesc());
		} else {
			sv.cntypdesc.set("");
		}
		batcKey.set(wsspcomn.batchkey);
		descpf = descpfDAO.getItemTblItemByLang(smtpfxcpy.item.toString(), "T1688", batcKey.batcBatctrcde.trim(),
				wsspcomn.language.toString(), wsspcomn.company.toString());
		if (descpf != null) {
			trdesc = descpf.getLongdesc();
		} 
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "TR59X", wsspcomn.chdrCnttype.value());
		if(itempf != null)
		{
			tr59xrec.tr59xrec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		if(isNE(tr59xrec.liscpay,"Y"))
		{
			sv.liscPaymentOut[Varcom.hi.toInt()].set("Y");
		}
		if(isNE(tr59xrec.cocontpay,"Y"))
		{
			sv.coCopPaymentOut[Varcom.hi.toInt()].set("Y");
		}
		
		
		itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "T5645", wsaaProg.toString());
		if(itempf == null)
		{
			syserrrec.params.set(wsspcomn.company.toString()+ "T5645"+ wsaaProg.toString());
			syserrrec.function.set(Varcom.mrnf);
			syserrrec.statuz.set("H134");
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		Clntkey clntkey = new Clntkey();
		clntkey.set(wsspcomn.clntkey);	
		sv.lifenum.set(clntkey.clntClntnum);
		sv.lifedesc.set(wsspcomn.longconfname);
		zctxpf = new Zctxpf();
		zctxpf.setChdrnum(sv.chdrnum.toString());
		zctxpf.setDatefrm(wsspcomn.confirmationKey.toInt());
		zctxpf = zctxpfDAO.readRecordByDate(zctxpf);
		if (zctxpf != null) {
			setDefaultValues();
		}
		else
		{
			syserrrec.params.set("ZCTXPF"+sv.chdrnum.toString());
			syserrrec.statuz.set(Varcom.mrnf);
			fatalError600();
		}
		
		for(int i =0; i < sv.contrAmount.length;i++)
		{
			total.add(sv.contrAmount[i]);
		}		
		datcon1.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1.datcon1Rec);
		
		
		
		chdrpf = chdrpfDAO.getCacheObject(new Chdrpf());
	}
	
	protected void setDefaultValues()
	{
		sv.datefrm.set(zctxpf.getDatefrm());
		sv.dateto.set(zctxpf.getDateto());
		sv.contrAmount01.set(zctxpf.getZsgtamt());
		sv.contrPrcnt01.set(zctxpf.getZsgtpct());
		sv.contrAmount02.set(zctxpf.getZoeramt());
		sv.contrPrcnt02.set(zctxpf.getZoerpct());
		sv.contrAmount03.set(zctxpf.getZdedamt());
		sv.contrPrcnt03.set(zctxpf.getZdedpct());
		sv.contrAmount04.set(zctxpf.getZundamt());
		sv.contrPrcnt04.set(zctxpf.getZundpct());
		sv.contrAmount05.set(zctxpf.getZspsamt());
		sv.contrPrcnt05.set(zctxpf.getZspspct());
		sv.contrAmount06.set(zctxpf.getZslrysamt());
		sv.contrPrcnt06.set(zctxpf.getZslryspct());
		sv.coCopPayment.set(zctxpf.getCocontamnt() == null ? BigDecimal.ZERO : zctxpf.getCocontamnt());
		sv.liscPayment.set(zctxpf.getLiscamnt()  == null ? BigDecimal.ZERO : zctxpf.getLiscamnt());
		sv.totContrAmount.set(zctxpf.getAmount());
		//zctxpf.getCnttype()
		
		sv.revsgtamt.set(zctxpf.getZsgtamt());
		sv.revsgtpct.set(zctxpf.getZsgtpct());		
		sv.revoeramt.set(zctxpf.getZoeramt());
		sv.revoerpct.set(zctxpf.getZoerpct());
		sv.revdedamt.set(zctxpf.getZdedamt());
		sv.revdedpct.set(zctxpf.getZdedpct());
		sv.revundamt.set(zctxpf.getZundamt());
		sv.revundpct.set(zctxpf.getZundpct());
		sv.revspsamt.set(zctxpf.getZspsamt());
		sv.revspspct.set(zctxpf.getZspspct());
		sv.revslyamt.set(zctxpf.getZslrysamt());
		sv.revslypct.set(zctxpf.getZslryspct());
		sv.revTotContrAmount.set(zctxpf.getAmount());
		
		
		yctxpf = yctxpfDAO.fetchRecord(zctxpf.getChdrcoy(), zctxpf.getChdrnum(), zctxpf.getDatefrm());
		List<String> chdrnumList = new ArrayList<>();
		chdrnumList.add(wsspcomn.chdrChdrnum.trim());
		 covrpfMap = covrpfDAO.searchCovrMap(wsspcomn.chdrChdrcoy.toString(),chdrnumList);
		 BigDecimal riskPremium = getRiskPremium();
		if(yctxpf != null)
		{
			sv.contrTax.set(yctxpf.getTotalTaxAmt());
			sv.riskPremium.set(yctxpf.getRpoffset());
			sv.notRecvd.set(yctxpf.getNotrcvd());
		}
		else
		{
			yctxpf = new Yctxpf();
			yctxpf.setTranno(0);
			yctxpf.setTotalTaxAmt(BigDecimal.ZERO);
			yctxpf.setChdrnum(zctxpf.getChdrnum());
			yctxpf.setChdrcoy(zctxpf.getChdrcoy());
			yctxpf.setRpoffset(riskPremium);
			yctxpf.setStaxdt(zctxpf.getDatefrm());
			yctxpf.setEtaxdt(zctxpf.getDateto());				
			yctxpf.setEffDate(zctxpf.getEffdate());
			yctxpf.setCnttype(wsspcomn.chdrCnttype.toString());
			sv.contrTax.set(ZERO);
			sv.riskPremium.set(riskPremium);
			sv.notRecvd.set(SPACE);
		}
	}
	
	protected BigDecimal getRiskPremium() {
		BigDecimal wsaaTotRiskPrem = BigDecimal.ZERO;
		covrpf = new Covrpf();
		for(Covrpf covr: covrpfMap.get(wsspcomn.chdrChdrnum.trim())) {
			wsaaTotRiskPrem = wsaaTotRiskPrem.add(covr.getRiskprem());
			if(("01").equals(covr.getLife()) && ("01").equals(covr.getCoverage()) && ("00").equals(covr.getRider()))
				covrpf = covr;
		}
		return wsaaTotRiskPrem;
	}
	

	protected void preScreenEdit() {
		
		// not used
	}

	
	@Override
	protected void screenEdit2000() {

		/* Screen errors are now handled in the calling program. */
		/* PERFORM 200-SCREEN-ERRORS. */
		fieldUpdated = false;
		wsspcomn.edterror.set(Varcom.oK);
		/* VALIDATE-SCREEN */
		if (isEQ(scrnparams.statuz, "KILL")) {
			return;
		} else if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
 		validateIfFieldChanged();
		if(fieldUpdated){
		populateRevisedTotalContributionAmount();
			if (isNE(sv.totContrAmount, sv.revTotContrAmount)) {
				wsspcomn.edterror.set("Y");
				sv.totContrAmountErr.set("RRMV");
				return; 
			}
			
			if(isEQ(sv.errorIndicators,SPACE))
			{
				setRevisedTotalContributionTax();
			} 
			else
			{
				wsspcomn.edterror.set("Y");
			}
		}

	}

	public void validateIfFieldChanged()
	{	
		for(int i =1; i < sv.revContrAmount.length;i++)
		{
			if(isNE(sv.contrAmount[i],sv.revContrAmount[i]) || isNE(sv.contrPrcnt[i],sv.revContrPrcnt[i]))
			{
				fieldUpdated  = true;
				break;
			}
		}
	}
	
	public void populateRevisedTotalContributionAmount() {				
		sv.revTotContrAmount.set(ZERO);		
		ZonedDecimalData totprcnt = new ZonedDecimalData(5,2);
		for (int i = 1; i < sv.revContrAmount.length; i++) {
			validatePercentage(sv.revContrPrcnt[i],sv.revContrPrcntErr[i]);
			if(isNE(sv.revContrAmount[i],ZERO))
			{
				calculatePercentage(total,sv.revContrAmount[i],sv.revContrPrcnt[i]);
				validatePercentage(sv.revContrPrcnt[i],sv.revContrPrcntErr[i]);
			}
			if(isEQ(sv.revContrAmount[i],ZERO) &&  isNE(sv.revContrPrcnt[i],ZERO))
			{
				calculateAmount(total,sv.revContrAmount[i],sv.revContrPrcnt[i]);
			}
			sv.revTotContrAmount.add(sv.revContrAmount[i]);
			totprcnt.add(sv.revContrPrcnt[i]);
		}
		if(isGT(totprcnt,100))
		{
			sv.contrTaxErr.set("E631");
						
		}
		if(isNE(sv.revTotContrAmount,ZERO))
		{
			sv.revTotContrAmount.add(sv.coCopPayment);
			sv.revTotContrAmount.add(sv.liscPayment);
		}		
	}
	
	public void calculatePercentage(ZonedDecimalData totalAmount , ZonedDecimalData revContrAmount,ZonedDecimalData revContrPrcnt)
	{		
		BigDecimal num = new BigDecimal(100);
		BigDecimal pct=revContrAmount.getbigdata().multiply(num);
		revContrPrcnt.set(pct.divide(totalAmount.getbigdata(), 2, RoundingMode.HALF_UP));				
	}
	
	public void calculateAmount(ZonedDecimalData totalAmount , ZonedDecimalData revContrAmount,ZonedDecimalData revContrPrcnt)
	{
		BigDecimal num = new BigDecimal(100);
		BigDecimal pct=totalAmount.getbigdata().multiply(revContrPrcnt.getbigdata());
		revContrAmount.set(pct.divide(num, 2, RoundingMode.HALF_UP));
	}
	
	public void validatePercentage(ZonedDecimalData revContrPrcnt, FixedLengthStringData revContrPrcntErr)	
	{
		if(isLT(revContrPrcnt,0) || isGT(revContrPrcnt,100))
		{
			revContrPrcntErr.set("H370");				
		}
	}
	

	public void setRevisedTotalContributionTax() {
		sv.contrTax.set(ZERO);
		taxDeductionRec = new TaxDeductionRec();
		taxDeductionRec.setCnttype(sv.cnttype.toString());		
		taxDeductionRec.setDatefrm(sv.datefrm.toInt());
		taxDeductionRec.setDateto(sv.dateto.toInt());
		List<String> contributionAmt = new ArrayList<>();				
		List<String> contributionType = new ArrayList<>();	
		int count=0;
		for (int i = 1; i < sv.revContrAmount.length; i++) {
			if(isNE(sv.revContrAmount[i],ZERO))
			{
				contributionAmt.add(sv.revContrAmount[i].getbigdata().toString());
				contributionType.add(getContributionType(i));
				count++;
			}
		}
		taxDeductionRec.setChdrcoy(wsspcomn.chdrChdrcoy.toString());
		taxDeductionRec.setChdrnum(wsspcomn.chdrChdrnum.toString());
		taxDeductionRec.setEffdate(datcon1.intDate.toInt());
		taxDeductionRec.setCount(String.valueOf(count));		
		taxDeductionRec.setContributionType(contributionType);
		taxDeductionRec.setContributionAmt(contributionAmt);
		taxDeductionRec.setRiskPremium(sv.riskPremium.getbigdata());
		taxDeductionRec.setTotalTaxAmt(new BigDecimal(0));		
		callProgram("CONTTAX", taxDeductionRec);	
		if(isNE(taxDeductionRec.statuz,Varcom.oK))
			{
				syserrrec.params.set("CONTTAX "+ wsspcomn.chdrChdrnum);
				syserrrec.statuz.set(taxDeductionRec.statuz);
				fatalError600();
			}
		sv.contrTax.set(taxDeductionRec.getTotalTaxAmt());
	}

	private String getContributionType(int index)
	{
		String value = null;
		switch(index)
		{		
		case 1:
			value =  "EMPSG";
			break;
		case 2:
			value = "EMPOF";
			break;
		case 3:
			value = "MEMC";
			break;
		case 4:
			value = "MEMNC";
			break;
		case 5:
			value = "SPOUSE";
			break;
		case 6:
			value = "SALSC";		
			break;
		case 7:
			value = "COCNT";		
			break;
		case 8:
			value = "LISCP";		
			break;
		default:
			value="";
			fatalError600();
			break;
		}
		return value;
		
	}
	
	/**
	 * <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	@Override
	protected void update3000() {
		if (isEQ(scrnparams.statuz, "KILL") || !fieldUpdated) {
			return;
		}		
		if(isNE(sv.contrTax,yctxpf.getTotalTaxAmt()))
		{
			insertPTRNPF();
			callUpdateSubroutines();
			processYctxpf();
			removeCache();
		}
		rewriteZctxpf();				
	}

	/**
	 * <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	@Override
	protected void whereNext4000() {
		wsspcomn.sbmaction.set("A");
		wsspcomn.programPtr.add(1);
	}

	
	protected void callUpdateSubroutines() {			
		Batcdorrec batcdorrec = new Batcdorrec();		
		batcdorrec.batchkey.set(wsspcomn.batchkey);		
		taxDeductionRec.setTotalTaxAmt(taxDeductionRec.getTotalTaxAmt().subtract(yctxpf.getTotalTaxAmt()));
		taxDeductionRec.setNoticeCorrection(true);		
		if(("B").equals(tr59xrec.recoveryType.toString())) {			
			callProgram(BonusDtn.class, batcdorrec.batcdorRec, taxDeductionRec, t5645rec, covrpf,trdesc);
			if(isNE(taxDeductionRec.statuz, Varcom.oK)) {
				syserrrec.params.set("UnitDtn "+ wsspcomn.chdrChdrnum);
				syserrrec.statuz.set(taxDeductionRec.statuz);
				fatalError600();
			}			
		}
		else if (("U").equals(tr59xrec.recoveryType.toString())) {
			callProgram(UnitDtn.class, batcdorrec.batcdorRec, taxDeductionRec,t5645rec, covrpf,trdesc);
			if(isNE(taxDeductionRec.statuz, Varcom.oK)) {
				syserrrec.params.set("UnitDtn "+ wsspcomn.chdrChdrnum);
				syserrrec.statuz.set(taxDeductionRec.statuz);
				fatalError600();
			}			
		}
		else
		{
			TaxComputeDtn taxcompute = new TaxComputeDtn();
			taxcompute.setBaseModel(this.getBaseModel());
			taxcompute.setGlobalVars(appVars);
			taxcompute.setJTAWrapper(this.getJTAWrapper());
			taxcompute.postAcmv(batcdorrec,taxDeductionRec,varcom,t5645rec,trdesc,chdrpf,covrpf);
		}
	}
	
	public void insertPTRNPF()
	{
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(wsspcomn.company.toString());
		ptrnIO.setChdrnum(wsspcomn.chdrChdrnum.toString());
		ptrnIO.setTranno(chdrpf.getTranno()+1);
		ptrnIO.setPtrneff(datcon1.intDate.toInt());
		ptrnIO.setBatcpfx(batcKey.batcBatcpfx.toString());
		ptrnIO.setBatccoy(wsspcomn.company.toString());
		ptrnIO.setBatcbrn(batcKey.batcBatcbrn.toString());
		ptrnIO.setBatcactyr(batcKey.batcBatcactyr.toInt());
		ptrnIO.setBatcactmn(batcKey.batcBatcactmn.toInt());
		ptrnIO.setBatctrcde(batcKey.batcBatctrcde.toString());
		ptrnIO.setBatcbatch(batcKey.batcBatcbatch.toString());
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		ptrnIO.setTrdt(varcom.vrcmDate.toInt());
		ptrnIO.setUserT(varcom.vrcmUser.toInt());
		ptrnIO.setDatesub(datcon1.intDate.toInt());
		List<Ptrnpf> ptrnpf = new ArrayList<>();
		ptrnpf.add(ptrnIO);
		ptrnpfDAO.insertPtrnPF(ptrnpf);
	}
	
	public void processYctxpf()
	{
		if(yctxpf.getTranno() != 0)
		{
			yctxpf.setValidFlag("2");
			yctxpf.setUsrprf(yctxpfDAO.getUsrprf());
			yctxpf.setJobnm(yctxpfDAO.getJobnm());
			yctxpf.setDatime(yctxpfDAO.getDatime());		
			yctxpfDAO.updateByID(yctxpf);
		}
		yctxpf.setTotalTaxAmt(sv.contrTax.getbigdata());
		yctxpf.setValidFlag("1");
		yctxpf.setTranno(yctxpf.getTranno()+1);
		yctxpf.setBatctrcde(batcKey.batcBatctrcde.toString());
		yctxpf.setUsrprf(yctxpfDAO.getUsrprf());
		yctxpf.setJobnm(yctxpfDAO.getJobnm());
		yctxpf.setDatime(yctxpfDAO.getDatime());
		yctxpf.setNotrcvd("Y");
		List<Yctxpf> yctxpfList =  new ArrayList<>();
		yctxpfList.add(yctxpf);
		yctxpfDAO.insertRecord(yctxpfList);				
	}
	
	public void rewriteZctxpf()
	{
		zctxpf.setZsgtamt(sv.revsgtamt.getbigdata());
		zctxpf.setZsgtpct(sv.revsgtpct.getbigdata());		
		zctxpf.setZoeramt(sv.revoeramt.getbigdata());
		zctxpf.setZoerpct(sv.revoerpct.getbigdata());
		zctxpf.setZdedamt(sv.revdedamt.getbigdata());
		zctxpf.setZdedpct(sv.revdedpct.getbigdata());
		zctxpf.setZundamt(sv.revundamt.getbigdata());
		zctxpf.setZundpct(sv.revundpct.getbigdata());
		zctxpf.setZspsamt(sv.revspsamt.getbigdata());
		zctxpf.setZspspct(sv.revspspct.getbigdata());
		zctxpf.setZslrysamt(sv.revslyamt.getbigdata());
		zctxpf.setZslryspct(sv.revslypct.getbigdata());
		zctxpfDAO.updateRecord(zctxpf);
	}
	
	public void removeCache()
	{				
		List<Chdrpf> chdrpfUpdateList = new ArrayList<>();
		chdrpf.setTranno(chdrpf.getTranno()+1);
		chdrpfUpdateList.add(chdrpf);
		chdrpfDAO.updateChdrTrannoByUniqueNo(chdrpfUpdateList);
	}
	
			
}


