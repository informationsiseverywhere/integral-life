package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6663
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class S6663ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(194+DD.cltaddr.length*5);//pmujavadiya
	public FixedLengthStringData dataFields = new FixedLengthStringData(50+DD.cltaddr.length*5).isAPartOf(dataArea, 0);//pmujavadiya//ILIFE-3222
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData incomeSeqNo = DD.incseqno.copyToZonedDecimal().isAPartOf(dataFields,10);
	public FixedLengthStringData payraddrs = new FixedLengthStringData(DD.cltaddr.length*5).isAPartOf(dataFields, 12);//pmujavadiya
	public FixedLengthStringData[] payraddr = FLSArrayPartOfStructure(5, DD.cltaddr.length, payraddrs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(payraddrs.length()).isAPartOf(payraddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData payraddr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, 0);
	public FixedLengthStringData payraddr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, DD.cltaddr.length*1);
	public FixedLengthStringData payraddr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, DD.cltaddr.length*2);
	public FixedLengthStringData payraddr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, DD.cltaddr.length*3);
	public FixedLengthStringData payraddr5 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, DD.cltaddr.length*4);
	public FixedLengthStringData payrname = DD.payrname.copy().isAPartOf(dataFields,12+DD.cltaddr.length*5);//pmujavadiya
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,42+DD.cltaddr.length*5);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 50+DD.cltaddr.length*5);//ILIFE-3222
	public FixedLengthStringData cltpcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData incseqnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData payraddrsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] payraddrErr = FLSArrayPartOfStructure(5, 4, payraddrsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(payraddrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData payraddr1Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData payraddr2Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData payraddr3Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData payraddr4Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData payraddr5Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData payrnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 86+DD.cltaddr.length*5);//ILIFE-3222
	public FixedLengthStringData[] cltpcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] incseqnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData payraddrsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] payraddrOut = FLSArrayPartOfStructure(5, 12, payraddrsOut, 0);
	public FixedLengthStringData[][] payraddrO = FLSDArrayPartOfArrayStructure(12, 1, payraddrOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(payraddrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] payraddr1Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] payraddr2Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] payraddr3Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] payraddr4Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] payraddr5Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] payrnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6663screenWritten = new LongData(0);
	public LongData S6663windowWritten = new LongData(0);
	public LongData S6663protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6663ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(payrnumOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(incseqnoOut,new String[] {"02","10","-02","10",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {payrnum, payrname, incomeSeqNo, cltpcode};
		screenOutFields = new BaseData[][] {payrnumOut, payrnameOut, incseqnoOut, cltpcodeOut};
		screenErrFields = new BaseData[] {payrnumErr, payrnameErr, incseqnoErr, cltpcodeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6663screen.class;
		protectRecord = S6663protect.class;
	}

}
