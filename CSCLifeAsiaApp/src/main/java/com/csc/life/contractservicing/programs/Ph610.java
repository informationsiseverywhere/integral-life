/*
 * File: Ph610.java
 * Date: 30 August 2009 1:10:43
 * Author: Quipoz Limited
 *
 * Class transformed from PH610.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.recordstructures.Hcsdkey;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.Sh610ScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnenqTableDAM;
import com.csc.life.newbusiness.recordstructures.Covrtrbkey;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.newbusiness.tablestructures.Th535rec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                    DIVIDEND OPTION
*
* This program allows modification of Dividend Option
* of the contract. The Contract Header record file and Cash
* Dividend record file are updated.
*
* A "PTRN" record will be written.
*
* Initialise
* ----------
*
* The details of the contract being worked on will be stored in
* the CHDRMNA I/O module.  Retrieve the details.
* In order to format  the  required  names  the  client details
* record is read and  the  relevant copybook for formatting the
* names must be included in the program.
*
* Read the cash dividend details (HCSD) and format the name
* using the copybooks.
*
*
* Validation
* ----------
*
* If KILL is requested, then skip validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
*Updating
*--------
*
* Updating occurs  with  the  updating  of  the contract header
* record, and the creation of a PTRN record.
*
* If the 'KILL' function key was pressed, skip the updating.
*
* CONTRACT HEADER UPDATE
*
*      - if there were  no  changes,  then  do  not  update the
*           contract header record.
*
*      - if there is a cash div opt. screen entry, then replace
*           the dividend option with this new dividend option.
*
*      - rewrite (WRITS) the contract header record.
*
* GENERAL HOUSEKEEPING
*
*       - Write a PTRN record (if updating the CHDR):
*
*           - contract key from contract header
*           - transaction number from WSSP
*           - transaction effective date from VRCM-DATE
*           - batch key information WSSP batchkey
*
*      - Update the header by calling BATCUP with a function of
*           SPACES and the following parameters:
*
*           - transaction count equals 1
*           - all other amounts equal to zero
*           - batch key from WSSP
*
*      - Release contract SFTLOCK.
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class Ph610 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH610");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaZdivopt = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaLastTranno = new ZonedDecimalData(5, 0).init(0).setUnsigned();
	private String wsaaWriteLetter = "N";

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 1).setUnsigned();

	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
	private static final String e304 = "E304";
	private static final String h366 = "H366";
	private static final String rrps = "RRPS";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5688 = "T5688";
	private static final String t5500 = "T5500";
	private static final String th500 = "TH500";
	private static final String th535 = "TH535";
	private static final String tr384 = "TR384";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private ResnTableDAM resnIO = new ResnTableDAM();
	private ResnenqTableDAM resnenqIO = new ResnenqTableDAM();
	private Batckey wsaaBatcKey = new Batckey();
	private Covrtrbkey wsaaCovrtrbkey = new Covrtrbkey();
	private Hcsdkey wsaaHcsdkey = new Hcsdkey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Isuallrec isuallrec = new Isuallrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Th535rec th535rec = new Th535rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Wssplife wssplife = new Wssplife();
	private Sh610ScreenVars sv = ScreenProgram.getScreenVars( Sh610ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	//private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDao", AcblpfDAO.class);
	//private Acblpf acblpf = new Acblpf();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private List<Acblpf> acblenqListLCDE = null;
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private BigDecimal accAmtLCDE = new BigDecimal(0);
	private Itempf itempf = null;
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private static final String feaConfigDivNetPrem= "CSMIN006";
	private boolean divpremFlag = false;
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End
	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkForErrors2080,
		relsfl3070,
		x280LoopCovrtrb,
		x290Exit,
		readTr3846020
	}

	public Ph610() {
		super();
		screenVars = sv;
		new ScreenModel("Sh610", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
	}

protected void initialise1010()
	{
	//divtonetoffPrem = FeaConfg.isFeatureExist("2", "CSMIN006", appVars, "IT"); 
		wsaaBatcKey.set(wsspcomn.batchkey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		divpremFlag = FeaConfg.isFeatureExist("2", feaConfigDivNetPrem, appVars, "IT");

	}

protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(cntDteFlag)	{
					sv.iljCntDteFlag.set("Y");
				} else {
					sv.iljCntDteFlag.set("N");
				}
		//ILJ-49 End 
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		sv.occdate.set(chdrmnaIO.getOccdate());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		sv.cownnum.set(chdrmnaIO.getCownnum());
		sv.payrnum.set(chdrmnaIO.getPayrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		sv.billfreq.set(chdrmnaIO.getBillfreq());
		sv.mop.set(chdrmnaIO.getBillchnl());
		sv.agntnum.set(chdrmnaIO.getAgntnum());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmnaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		if (isNE(sv.cownnum,SPACES)) {
			formatClientName1700();
		}
		if (isNE(sv.payrnum,SPACES)) {
			formatPayorName1800();
		}
		if (isNE(sv.agntnum,SPACES)) {
			formatAgentName1900();
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		if (isEQ(chdrmnaIO.getAgntnum(),SPACES)) {
			sv.agntnum.set(SPACES);
		}
		else {
			sv.agntnum.set(chdrmnaIO.getAgntnum());
		}
		wsaaHcsdkey.set(SPACES);
		wsaaHcsdkey.hcsdChdrcoy.set(chdrmnaIO.getChdrcoy());
		wsaaHcsdkey.hcsdChdrnum.set(chdrmnaIO.getChdrnum());
		wsaaHcsdkey.hcsdPlanSuffix.set(0);
		hcsdIO.setDataKey(wsaaHcsdkey);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)
		|| isNE(hcsdIO.getChdrcoy(),wsaaHcsdkey.hcsdChdrcoy)
		|| isNE(hcsdIO.getChdrnum(),wsaaHcsdkey.hcsdChdrnum)) {
			syserrrec.params.set(hcsdIO.getParams());
			fatalError600();
		}
		else {
			sv.zdivopt.set(hcsdIO.getZdivopt());
			wsaaZdivopt.set(hcsdIO.getZdivopt());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(th500);
		descIO.setDescitem(sv.zdivopt);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.descrip.set(descIO.getLongdesc());
		}
		else {
			sv.descrip.set("?");
		}
		formatResnDetails5000();
	}

protected void formatClientName1700()
	{
		readClientRecord1710();
	}

protected void readClientRecord1710()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.cownnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void formatPayorName1800()
	{
		readClientRecord1810();
	}

protected void readClientRecord1810()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.payrnumErr.set(e304);
			sv.payorname.set(SPACES);
		}
		else {
			plainname();
			sv.payorname.set(wsspcomn.longconfname);
		}
	}

protected void formatAgentName1900()
	{
		readAgent1910();
	}

protected void readAgent1910()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntnum);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		sv.oragntnam.set(SPACES);
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.oragntnam.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					reascd2050();
					zdivopt2060();
				case checkForErrors2080:
					checkForErrors2080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(sv.zdivopt,SPACES)) {
			sv.zdivoptErr.set(h366);
		}
	
		
		if(divpremFlag) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("T5645");
			itempf.setItemitem(wsaaProg.toString());
		    itempf.setItemseq("  ");
			itempf = itemDao.getItemRecordByItemkey(itempf);
			
			if (itempf == null) {
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
				fatalError600();
			}
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			acblenqListLCDE = acblpfDAO.getAcblenqRecord(
					wsspcomn.company.toString(), StringUtils.rightPad(chdrmnaIO.getChdrnum().toString(), 16),t5645rec.sacscode[1].toString(),t5645rec.sacstype[1].toString());
			
			if (acblenqListLCDE != null && !acblenqListLCDE.isEmpty()) {
				accAmtLCDE = acblenqListLCDE.get(0).getSacscurbal();
				accAmtLCDE = new BigDecimal(ZERO.toString()).subtract(accAmtLCDE);
				if(isGT(accAmtLCDE,0))
				{
					sv.zdivoptErr.set(rrps);
					wsspcomn.edterror.set("Y");
				}
				
			}
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		/* Only read T5500 for the description if the user has not already */
		/* provided one.                                                   */
		if (isNE(sv.resndesc, SPACES)
		&& isNE(scrnparams.statuz, "CALC")) {
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void reascd2050()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5500);
		descIO.setDescitem(sv.reasoncd);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.resndesc.set(descIO.getLongdesc());
		}
		else {
			sv.resndesc.set("?");
		}
	}

protected void zdivopt2060()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(th500);
		descIO.setDescitem(sv.zdivopt);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.descrip.set(descIO.getLongdesc());
		}
		else {
			sv.descrip.set("?");
		}
	}

protected void checkForErrors2080()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateDatabase3010();
					writeReasncd3050();
				case relsfl3070:
					relsfl3070();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(sv.zdivopt,wsaaZdivopt)) {
			goTo(GotoLabel.relsfl3070);
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		chdrmnaIO.setFunction(varcom.keeps);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		x100UpdateHcsd();
		if (isEQ(wsaaWriteLetter,"Y")) {
			writeLetter6000();
		}
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		wsaaBatcKey.set(wsspcomn.batchkey);
		ptrnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmnaIO.getChdrnum());
		ptrnIO.setTranno(chdrmnaIO.getTranno());
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		/* Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsspcomn.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.statuz.set(ZERO);
		batcuprec.function.set(SPACES);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
	}

protected void writeReasncd3050()
	{
		if (isNE(sv.resndesc,SPACES)
		|| isNE(sv.reasoncd,SPACES)) {
			resnIO.setDataKey(SPACES);
			resnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
			resnIO.setChdrnum(chdrmnaIO.getChdrnum());
			resnIO.setTranno(chdrmnaIO.getTranno());
			resnIO.setReasoncd(sv.reasoncd);
			resnIO.setResndesc(sv.resndesc);
			resnIO.setTrancde(wsaaBatcKey.batcBatctrcde);
			resnIO.setTransactionDate(varcom.vrcmDate);
			resnIO.setTransactionTime(varcom.vrcmTime);
			resnIO.setUser(varcom.vrcmUser);
			resnIO.setFormat(formatsInner.resnrec);
			resnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, resnIO);
			if (isNE(resnIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(resnIO.getParams());
				fatalError600();
			}
		}
	}

protected void relsfl3070()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void formatResnDetails5000()
	{
		start5010();
	}

protected void start5010()
	{
		resnenqIO.setParams(SPACES);
		resnenqIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		resnenqIO.setChdrnum(chdrmnaIO.getChdrnum());
		resnenqIO.setTrancde(wsaaBatcKey.batcBatctrcde);
		resnenqIO.setTranno(999);
		resnenqIO.setFormat(formatsInner.resnenqrec);
		resnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		resnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		resnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANCDE");
		SmartFileCode.execute(appVars, resnenqIO);
		if (isNE(resnenqIO.getStatuz(),varcom.oK)
		&& isNE(resnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmnaIO.getChdrcoy(),resnenqIO.getChdrcoy())
		|| isNE(chdrmnaIO.getChdrnum(),resnenqIO.getChdrnum())
		|| isNE(wsaaBatcKey.batcBatctrcde,resnenqIO.getTrancde())
		|| isEQ(resnenqIO.getStatuz(),varcom.endp)) {
			sv.reasoncd.set(SPACES);
			sv.resndesc.set(SPACES);
		}
		else {
			sv.reasoncd.set(resnenqIO.getReasoncd());
			sv.resndesc.set(resnenqIO.getResndesc());
		}
	}

protected void x100UpdateHcsd()
	{
		x110Para();
	}

protected void x110Para()
	{
		wsaaWriteLetter = "N";
		initialize(isuallrec.isuallRec);
		isuallrec.planSuffix.set(0);
		isuallrec.freqFactor.set(0);
		isuallrec.batcactyr.set(0);
		isuallrec.batcactmn.set(0);
		isuallrec.transactionDate.set(0);
		isuallrec.transactionTime.set(0);
		isuallrec.user.set(0);
		isuallrec.covrInstprem.set(0);
		isuallrec.covrSingp.set(0);
		isuallrec.newTranno.set(0);
		isuallrec.effdate.set(0);
		isuallrec.batchkey.set(wsspcomn.batchkey);
		isuallrec.transactionDate.set(varcom.vrcmDate);
		isuallrec.transactionTime.set(varcom.vrcmTime);
		isuallrec.user.set(varcom.vrcmUser);
		isuallrec.termid.set(varcom.vrcmTermid);
		isuallrec.effdate.set(chdrmnaIO.getPtdate());
		isuallrec.newTranno.set(chdrmnaIO.getTranno());
		isuallrec.language.set(wsspcomn.language);
		y100FindOptchgSbr();
		th535rec.th535Rec.set(itemIO.getGenarea());
		wsaaCovrtrbkey.set(SPACES);
		wsaaCovrtrbkey.covrtrbChdrcoy.set(chdrmnaIO.getChdrcoy());
		wsaaCovrtrbkey.covrtrbChdrnum.set(chdrmnaIO.getChdrnum());
		wsaaCovrtrbkey.covrtrbPlanSuffix.set(0);
		covrtrbIO.setDataKey(wsaaCovrtrbkey);
		covrtrbIO.setFormat(formatsInner.covrtrbrec);
		covrtrbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		|| isNE(covrtrbIO.getChdrcoy(),wsaaCovrtrbkey.covrtrbChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),wsaaCovrtrbkey.covrtrbChdrnum)) {
			covrtrbIO.setParams(covrtrbIO.getParams());
			fatalError600();
		}
		while ( !(isEQ(covrtrbIO.getStatuz(),varcom.endp))) {
			x200ProcessCovrtrb();
		}

	}

protected void x200ProcessCovrtrb()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					x210Para();
				case x280LoopCovrtrb:
					x280LoopCovrtrb();
				case x290Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void x210Para()
	{
		if (isNE(covrtrbIO.getChdrcoy(),wsaaCovrtrbkey.covrtrbChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),wsaaCovrtrbkey.covrtrbChdrnum)) {
			covrtrbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.x290Exit);
		}
		else {
			if (isNE(covrtrbIO.getRider(),"00")) {
				goTo(GotoLabel.x280LoopCovrtrb);
			}
		}
		hcsdIO.setDataKey(covrtrbIO.getDataKey());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			fatalError600();
		}
		x300ProcessHcsd();
	}

protected void x280LoopCovrtrb()
	{
		covrtrbIO.setFormat(formatsInner.covrtrbrec);
		covrtrbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isEQ(covrtrbIO.getStatuz(),varcom.oK)
		|| isEQ(covrtrbIO.getStatuz(),"ENDP")) {
			/*NEXT_SENTENCE*/
		}
		else {
			syserrrec.params.set(covrtrbIO.getParams());
			fatalError600();
		}
	}

protected void x300ProcessHcsd()
	{
			x310Para();
		}

protected void x310Para()
	{
		wsaaLastTranno.set(hcsdIO.getTranno());
		hcsdIO.setValidflag("2");
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			fatalError600();
		}
		hcsdIO.setValidflag("1");
		hcsdIO.setTranno(chdrmnaIO.getTranno());
		hcsdIO.setEffdate(chdrmnaIO.getPtdate());
		hcsdIO.setZdivopt(sv.zdivopt);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			fatalError600();
		}
		wsaaWriteLetter = "Y";
		if (isEQ(th535rec.optionChange,SPACES)) {
			return ;
		}
		isuallrec.company.set(hcsdIO.getChdrcoy());
		isuallrec.chdrnum.set(hcsdIO.getChdrnum());
		isuallrec.life.set(hcsdIO.getLife());
		isuallrec.coverage.set(hcsdIO.getCoverage());
		isuallrec.rider.set(hcsdIO.getRider());
		isuallrec.planSuffix.set(hcsdIO.getPlanSuffix());
		callProgram(th535rec.optionChange, isuallrec.isuallRec);
		if (isNE(isuallrec.statuz,varcom.oK)) {
			syserrrec.params.set(isuallrec.isuallRec);
			fatalError600();
		}
	}

protected void y100FindOptchgSbr()
	{
		/*Y110-PARA*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmnaIO.getChdrcoy());
		itemIO.setItemtabl(th535);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaZdivopt);
		stringVariable1.addExpression(sv.zdivopt);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*Y190-EXIT*/
	}

protected void writeLetter6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start6010();
				case readTr3846020:
					readTr3846020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6010()
	{
		wsaaItemBatctrcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
	}

protected void readTr3846020()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)
			&& isNE(wsaaItemCnttype,"***")) {
				wsaaItemCnttype.set("***");
				goTo(GotoLabel.readTr3846020);
			}
			else {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType,SPACES)) {
			return ;
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.branch.set(chdrmnaIO.getCntbranch());
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(wsspcomn.language);
		wsaaOkeyTranno.set(wsaaLastTranno);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData resnrec = new FixedLengthStringData(10).init("RESNREC");
	private FixedLengthStringData chdrmnarec = new FixedLengthStringData(10).init("CHDRMNAREC");
	private FixedLengthStringData covrtrbrec = new FixedLengthStringData(10).init("COVRTRBREC");
	private FixedLengthStringData resnenqrec = new FixedLengthStringData(10).init("RESNENQREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
}
