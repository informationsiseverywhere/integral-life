package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:01
 * Description:
 * Copybook name: BUNLREVREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bunlrevrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData chgrevRec = new FixedLengthStringData(75);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(chgrevRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(chgrevRec, 5);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(chgrevRec, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(chgrevRec, 10);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(chgrevRec, 18);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(chgrevRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(chgrevRec, 22);
  	public ZonedDecimalData planSuffix = new ZonedDecimalData(4, 0).isAPartOf(chgrevRec, 24);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(chgrevRec, 28).setUnsigned();
  	public ZonedDecimalData oldTranno = new ZonedDecimalData(5, 0).isAPartOf(chgrevRec, 36).setUnsigned();
  	public ZonedDecimalData newTranno = new ZonedDecimalData(5, 0).isAPartOf(chgrevRec, 41).setUnsigned();
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(chgrevRec, 46);
  	public FixedLengthStringData filler = new FixedLengthStringData(25).isAPartOf(chgrevRec, 50, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chgrevRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chgrevRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}