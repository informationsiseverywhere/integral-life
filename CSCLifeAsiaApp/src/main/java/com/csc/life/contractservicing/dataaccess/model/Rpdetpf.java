package com.csc.life.contractservicing.dataaccess.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

public class Rpdetpf implements Serializable{

		private static final long serialVersionUID = 1L;
		
		@Id
		@Column(name = "UNIQUE_NUMBER")
		private long uniqueNumber;
		private int loannum;
		private String chdrnum;
		private String chdrcoy;
		private String chdrpfx;	
		private double repayamt;
		private String repaystatus;
		private int approvaldate;
		private String repaymethod;
		private int tranno;
		private String loantype;
		private int efftdate;
		private String loancurr;
		private int interest;
		private double prinamnt;
		private double curramnt;
		private double accrdint;
		private double pendint;
		private String status;
		
	
		
		public long getUniqueNumber() {
			return uniqueNumber;
		}
		public void setUniqueNumber(long uniqueNumber) {
			this.uniqueNumber = uniqueNumber;
		}
		public int getLoannum() {
			return loannum;
		}
		public void setLoannum(int loannum) {
			this.loannum = loannum;
		}	
		public String getChdrnum() {
			return chdrnum;
		}
		public void setChdrnum(String chdrnum) {
			this.chdrnum = chdrnum;
		}
		public String getChdrcoy() {
			return chdrcoy;
		}
		public void setChdrcoy(String chdrcoy) {
			this.chdrcoy = chdrcoy;
		}
		public String getChdrpfx() {
			return chdrpfx;
		}
		public void setChdrpfx(String chdrpfx) {
			this.chdrpfx = chdrpfx;
		}
	
		public double getRepayamt() {
			return repayamt;
		}
		public void setRepayamt(double repayamt) {
			this.repayamt = repayamt;
		}
		public String getRepaystatus() {
			return repaystatus;
		}
		public void setRepaystatus(String repaystatus) {
			this.repaystatus = repaystatus;
		}
		public int getApprovaldate() {
			return approvaldate;
		}
		public void setApprovaldate(int approvaldate) {
			this.approvaldate = approvaldate;
		}	
		public String getRepaymethod() {
			return repaymethod;
		}
		public void setRepaymethod(String repaymethod) {
			this.repaymethod = repaymethod;
		}	
		public int getTranno() {
			return tranno;
		}
		public void setTranno(int tranno) {
			this.tranno = tranno;
		}	
		public String getLoantype() {
			return loantype;
		}
		public void setLoantype(String loantype) {
			this.loantype = loantype;
		}	
		public int getEfftdate() {
			return efftdate;
		}
		public void setEfftdate(int efftdate) {
			this.efftdate = efftdate;
		}	
		public String getLoancurr() {
			return loancurr;
		}
		public void setLoancurr(String loancurr) {
			this.loancurr = loancurr;
		}
		public int getInterest() {
			return interest;
		}
		public void setInterest(int interest) {
			this.interest = interest;
		}	
		public double getPrinamnt() {
			return prinamnt;
		}
		public void setPrinamnt(double prinamnt) {
			this.prinamnt = prinamnt;
		}
		public double getCurramnt() {
			return curramnt;
		}
		public void setCurramnt(double curramnt) {
			this.curramnt = curramnt;
		}
		public double getAccrdint() {
			return accrdint;
		}
		public void setAccrdint(double accrdint) {
			this.accrdint = accrdint;
		}
		public double getPendint() {
			return pendint;
		}
		public void setPendint(double pendint) {
			this.pendint = pendint;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}

	}

