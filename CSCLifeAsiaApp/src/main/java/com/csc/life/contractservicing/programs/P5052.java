/*
 * File: P5052.java
 * Date: 30 August 2009 0:00:57
 * Author: Quipoz Limited
 * 
 * Class transformed from P5052.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.dao.LhdrpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Lhdrpf;
import com.csc.life.contractservicing.screens.S5052ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6632rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Tsdtcalrec;
import com.csc.life.productdefinition.tablestructures.Tt577rec;
import com.csc.life.productdefinition.tablestructures.Tt578rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
//import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;  ILIFE-5962

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*    POLICY LOAN REGISTRATION.
*    -------------------------
*
* This   transaction,  Policy Loans Registration is selected
*  from the Policy Loans submenu, S5051/P5051.
*
* Overview.
* ---------
* This program will calculate the Surrender value of the
*  contract being processed and display it on the screen S5052.
* The Surrender value is calculated at Coverage/Rider level and
*  the values are then summed together before display. It will
*  not worry about whether plans/policies are Broken out or not
*  - A plan suffix of '0000' is passed to the Surrender value
*  calculation subroutine(s) which then returns a value for the
*  whole Coverage/rider, regardless of how many Breakouts
*  have/have not taken place.
* The program then checks whether there are any existing Loans
*  associated with the contract and if there are any, it
*  calculates the current value of those Loans ( including
*  interest owed ) as at the effective date we are working on.
* The program then checks each Coverage/Rider on the Contract
*  to see whether it has a Loan method, and if it does, it uses
*  the Loan method to read T6632 the Loan Method table. This
*  table holds a percentage figure which is used to calculate
*  how much of the Surrender value of the Coverage/Rider is
*  allowed to be 'Lent' as a Policy Loan.
* Having done this for all of the Coverage/Riders on the
*  Contract, the Loan available value can be calculated
*  by subtracting the total of the existing Loans
*  from the total of the sum of the 'Loan allowed percentage'
*  values for all the Coverages/Riders.
* The Loan being registered can be taken in a different
*  currency to the Contract header currency. - when the
*  currency field on S5052 is changed, all values displayed
*  will be converted to the new currency.
*
* Initialise.
* -----------
*
* Read the  CHDRENQ contract  (function  RETRV)  and  read  the	
* relevant data necessary for obtaining the status description,
* the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
* and the Bill-to-date.
*
* Obtain the  necessary  descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Format the Ownername and the subsequent Life & Joint life
*  names by first reading the respective Client (CLTS) record
*  and use the CONFNAME procedure division copybook to format
*  the required names.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* The Life & Joint life Client numbers can be obtained from the
* first Coverage record read.
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*      - READR  the   life  details  using  LIFExxx  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*
*      - READR the  joint-life  details using LIFExxx (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
* COVERAGE/RIDER SURRENDER VALUES
*
* Loop around the COVRSUR file processing all records associated
*  with this contract:
*
*  Read T5687 for the Coverage/Rider type to get the Surrender
*   method,
*  Read T6598 for the surrender value calculation subroutine
*   using the Surrender method from T5687 retrieved above.
*  Call the T6598 surrender value calculation subroutine to get
*   the current surrender value of the component
*  Add this surrender value to the running total of Surr value.
*
* OUTSTANDING LOAN VALUES
*
* Call the 'TOTLOAN' subroutine to calculate the current value
*  of all existing ( if any ) loans held against the contract
*  we are processing.
* 'TOTLOAN' will return the total loan Principal and the total
*  loan Interest outstanding at the given effective date.
*
* LOAN AVAILABLE AMOUNT
*
* Read the table T6632 to get the maximum loan %age allowed
* i.e. the %age of the surrender value which can be borrowed
*
* Calculate the Loan available value :
*
*  Loan Avail = (Max loan Avail * T6632-%age) - Existing loans
*
*
*  DISPLAY ALL FIELDS ON SCREEN AND PROCEED TO VALIDATION
*
*
* Validate.
* ---------
*
* The Loan required val CANNOT be greater than Loan available val
*
* The Loan required val CANNOT be zero
*
* Validate chosen currency
* IF user changes currency loan is to be paid in, re-calculate
*  values on screen and redisplay S5052.
*
* Update.
* -------
*
* Write temporary Loan header LHDR record, containing
*  Company, Contract number, Loan currency, Loan amount and
*  Loan commencement date.
*
* SOFTLOCK contract header record for use in the AT
*
* Submit AT request
*
* Next Program.
* -------------
*    Add 1 to program pointer
*
* TABLES USED.
* ------------
*
* T3623 - Contract Risk statii
* T3588 - Contract Premium statii
* T5679 - Transaction Status requirements
* T5687 - General Coverage/Rider details
* T5688 - Contract Types
* T6632 - Loan Methods ( key T5687-LOANMETH )
* T6598 - Various Calculation methods
*
*****************************************************************
* </pre>
*/
public class P5052 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5052");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaOldCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaOldRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaOldPlnsfx = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRiskStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private PackedDecimalData wsaaSurrValTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaComponentTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaCompLoanVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaLoanAvailable = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldSurrVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldLoanAvail = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCashAmount = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOldCurrVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaNewCurrVal = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaPolsumCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaStamp = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler1 = new FixedLengthStringData(175).isAPartOf(wsaaTransactionRec, 25, FILLER).init(SPACES);

	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTt578Item1 = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 0);
	private FixedLengthStringData wsaaTt578Item2 = new FixedLengthStringData(4).isAPartOf(wsaaItemitem, 3);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);


		/* FORMATS */
		/* Dummy user wssp. Replace with required version.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Batckey wsaaBatckey = new Batckey();
	private Atreqrec atreqrec = new Atreqrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Tsdtcalrec tsdtcalrec = new Tsdtcalrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6632rec t6632rec = new T6632rec();
	private T6598rec t6598rec = new T6598rec();
	private Tt577rec tt577rec = new Tt577rec();
	private Tt578rec tt578rec = new Tt578rec();
	private S5052ScreenVars sv = ScreenProgram.getScreenVars( S5052ScreenVars.class);
	private ExternalisedRules er = new ExternalisedRules();
	
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);  //ILIFE-5962
	private Chdrpf chdrpf = new Chdrpf();  //ILIFE-5962
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	private CovrpfDAO covrpfdao =getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private LhdrpfDAO lhdrpfdao = getApplicationContext().getBean("lhdrpfDAO",LhdrpfDAO.class);
	List<Covrpf> covrSurList=null;
	private Lifepf lifeenqIO = new Lifepf();
	private Clntpf cltsIO = null;
	private Itempf itempf = null;
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public P5052() {
		super();
		screenVars = sv;
		new ScreenModel("S5052", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		wsaaOldCoverage.set(SPACES);
		wsaaOldRider.set(SPACES);
		wsaaStoredCurrency.set(SPACES);
		wsaaSurrValTot.set(ZERO);
		wsaaComponentTot.set(ZERO);
		wsaaCompLoanVal.set(ZERO);
		wsaaLoanAvailable.set(ZERO);
		wsaaHeldSurrVal.set(ZERO);
		wsaaHeldCurrLoans.set(ZERO);
		wsaaHeldLoanAvail.set(ZERO);
		wsaaOldPlnsfx.set(ZERO);
		wsaaPremStatus.set("N");
		wsaaRiskStatus.set("N");
		wsaaIndex.set(ZERO);
		wsaaPolsumCount.set(ZERO);
		
		/* START OF ILIFE-5962*/
		
		/*chdrenqIO.setParams(SPACES);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}*/
		
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);  
		
		if(chdrpf == null){
			fatalError600();
		}
		
		/*END OF ILIFE-5962*/
		
		sv.chdrnum.set(chdrpf.getChdrnum()); //ILIFE-5962
		sv.cnttype.set(chdrpf.getCnttype()); //ILIFE-5962
		sv.cntcurr.set(chdrpf.getCntcurr()); //ILIFE-5962
		sv.currcd.set(chdrpf.getCntcurr()); //ILIFE-5962
		sv.currfrom.set(chdrpf.getOccdate()); //ILIFE-5962
		sv.cownnum.set(chdrpf.getCownnum()); //ILIFE-5962
		sv.ptdate.set(chdrpf.getPtdate()); //ILIFE-5962
		sv.btdate.set(chdrpf.getBtdate()); //ILIFE-5962
		sv.loanvalue.set(ZERO);
		sv.loansum.set(ZERO);
		wsaaStamp.set(ZERO);
		sv.tplstmdty.set(ZERO);
		sv.netLceamt.set(ZERO);
		sv.effdate.set(wsspcomn.currfrom);
		getDescriptions1100();

		cltsIO = clntpfDAO.searchClntRecord("CN",chdrpf.getCowncoy().toString(),chdrpf.getCownnum());
		if (cltsIO == null
		|| isNE(cltsIO.getValidflag(),"1")) {
			sv.ownernameErr.set("E304");
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		readT56791350();
		getSurrValues1400();
		getCashAccounts1450();
		wsaaHeldLoanAvail.add(wsaaHeldCashAmount);
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy()); //ILIFE-5962
		totloanrec.chdrnum.set(sv.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.language.set(wsspcomn.language);
		totloanrec.function.set("LOAN");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal,totloanrec.interest));
		sv.loansum.set(wsaaHeldCurrLoans);
		sv.numberOfLoans.set(totloanrec.loanCount);
		wsaaStoredCurrency.set(chdrpf.getCntcurr()); //ILIFE-5962
		sv.surrval.set(wsaaHeldSurrVal);
		zrdecplrec.amountIn.set(sv.surrval);
		callRounding6000();
		sv.surrval.set(zrdecplrec.amountOut);
		wsaaHeldSurrVal.set(zrdecplrec.amountOut);
		/* subtract any existing loans from loan available value*/
		if (isGT(wsaaHeldLoanAvail,wsaaHeldCurrLoans)) {
			compute(wsaaHeldLoanAvail, 2).set(sub(wsaaHeldLoanAvail,wsaaHeldCurrLoans));
		}
		else {
			wsaaHeldLoanAvail.set(ZERO);
		}
		sv.loanallow.set(wsaaHeldLoanAvail);
	}

protected void getDescriptions1100(){
	Descpf descpf = null;
	descpf = descDAO.getdescData("IT", "T5688" ,chdrpf.getCnttype(),chdrpf.getChdrcoy().toString(), wsspcomn.language.toString());
	if (descpf != null) {
		sv.ctypedes.set(descpf.getLongdesc());
	}
	else {
		sv.ctypedes.fill("?");
	}
	descpf = descDAO.getdescData("IT", "T3623" ,chdrpf.getStatcode(),chdrpf.getChdrcoy().toString(), wsspcomn.language.toString());
	if (descpf != null) {
		sv.rstate.set(descpf.getLongdesc());
	}
	else {
		sv.rstate.fill("?");
	}
	descpf = descDAO.getdescData("IT", "T3588" ,chdrpf.getPstcde(),chdrpf.getChdrcoy().toString(), wsspcomn.language.toString());
	if (descpf != null) {
		sv.pstate.set(descpf.getLongdesc());
	}
	else {
		sv.pstate.fill("?");
	}
}


protected void readT56791350()
	{
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(chdrpf.getChdrcoy().toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
	    itempf = itemDAO.getItempfRecord(itempf);
		if (itempf == null) {
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void getSurrValues1400(){
	Covrpf covrsur=new Covrpf();
	covrsur.setChdrcoy(chdrpf.getChdrcoy().toString());
	covrsur.setChdrnum(chdrpf.getChdrnum());
	covrsur.setPlanSuffix(0);
	covrSurList=covrpfdao.selectCovrSurData(covrsur);
	
	if (covrSurList == null || covrSurList.isEmpty()) {
		return ;
	}
	lifeJlifeDetails1500();
	wsaaLoanAvailable.set(ZERO);
	for(Covrpf covrsurIO : covrSurList){
		processComponents1600(covrsurIO);
	}
	
	wsaaHeldSurrVal.set(wsaaSurrValTot);
	wsaaHeldLoanAvail.set(wsaaLoanAvailable);
}

protected void getCashAccounts1450()
	{
		start1451();
	}

protected void start1451()
	{
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy()); //ILIFE-5962
		totloanrec.chdrnum.set(sv.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("CASH");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCashAmount, 2).set(add(totloanrec.principal,totloanrec.interest));
		if (isLT(wsaaHeldCashAmount,0)) {
			compute(wsaaHeldCashAmount, 2).set(mult(wsaaHeldCashAmount,(-1)));
		}
		sv.zrcshamt.set(wsaaHeldCashAmount);
	}

protected void lifeJlifeDetails1500(){
	lifeenqIO.setChdrcoy(chdrpf.getChdrcoy().toString());
	lifeenqIO.setChdrnum(chdrpf.getChdrnum());
	lifeenqIO.setLife(covrSurList.get(0).getLife());
	lifeenqIO.setJlife("00");
	lifeenqIO.setValidflag("1");
	Map<String, Lifepf> lifepfMap =lifepfDAO.getLifeEnqData(lifeenqIO);
	String key = lifeenqIO.getChdrcoy().trim()+""+lifeenqIO.getChdrnum().trim()+""+lifeenqIO.getLife().trim()+""+lifeenqIO.getJlife().trim()+""+lifeenqIO.getValidflag().trim();
	if(!lifepfMap.containsKey(key)){
		fatalError600();
	}
	lifeenqIO= lifepfMap.get(key);
	sv.lifcnum.set(lifeenqIO.getLifcnum());
	cltsIO = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),lifeenqIO.getLifcnum().trim());
	if (cltsIO == null
	|| isNE(cltsIO.getValidflag(),"1")) {
		sv.linsnameErr.set("E304");
		sv.linsname.set(SPACES);
	}
	else {
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
	}
	lifeenqIO.setChdrcoy(chdrpf.getChdrcoy().toString());
	lifeenqIO.setChdrnum(chdrpf.getChdrnum());
	lifeenqIO.setLife(covrSurList.get(0).toString());
	lifeenqIO.setJlife("01");
	lifeenqIO.setValidflag("1");
	lifepfMap =lifepfDAO.getLifeEnqData(lifeenqIO);
	
	key = lifeenqIO.getChdrcoy().trim()+""+lifeenqIO.getChdrnum().trim()+""+lifeenqIO.getLife().trim()+""+lifeenqIO.getJlife().trim()+""+lifeenqIO.getValidflag().trim();
	if(!lifepfMap.containsKey(key)){
		sv.jlifcnum.set(SPACES);
		sv.jlinsname.set(SPACES);
		return ;
	}
	lifeenqIO= lifepfMap.get(key);
	sv.jlifcnum.set(lifeenqIO.getLifcnum());
	cltsIO = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),lifeenqIO.getLifcnum().trim());
	if (cltsIO == null
	|| isNE(cltsIO.getValidflag(),"1")) {
		sv.jlinsnameErr.set("E304");
		sv.jlinsname.set(SPACES);
	}
	else {
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}
}

protected void processComponents1600(Covrpf covrsurIO){
		wsaaOldCoverage.set(covrsurIO.getCoverage());
		wsaaOldRider.set(covrsurIO.getRider());
		wsaaOldPlnsfx.set(covrsurIO.getPlanSuffix());
		getSurrSubroutine1700(covrsurIO);
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		srcalcpy.polsum.set(chdrpf.getPolsum()); //ILIFE-5962
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.ptdate.set(chdrpf.getPtdate()); //ILIFE-5962
		srcalcpy.effdate.set(wsspcomn.currfrom);
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.language.set(wsspcomn.language);
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		srcalcpy.chdrCurr.set(chdrpf.getCntcurr()); //ILIFE-5962
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		if (isGT(covrsurIO.getInstprem(),ZERO)) {
			srcalcpy.singp.set(covrsurIO.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrsurIO.getSingp());
		}
		srcalcpy.billfreq.set(chdrpf.getBillfreq()); //ILIFE-5962
		srcalcpy.status.set(SPACES);
		wsaaComponentTot.set(ZERO);
		if(chdrpf.getPolinc() != null) { //ILIFE-5962
			if (isGT(chdrpf.getPolinc(),1)) { //ILIFE-5962
				if (isEQ(covrsurIO.getPlanSuffix(),ZERO)
				&& isNE(chdrpf.getPolsum(),chdrpf.getPolinc())) { //ILIFE-5962
					sumpolSvalCalc1650(covrsurIO);
				}
				else {
					while ( !(isEQ(srcalcpy.status,varcom.endp))) {
						calculateSurrValue1800(covrsurIO);
					}
					
				}
			}
			else {
				while ( !(isEQ(srcalcpy.status,varcom.endp))) {
					calculateSurrValue1800(covrsurIO);
				}
				
			}
		}
		if (isNE(t5687rec.loanmeth,SPACES)) {
			compute(wsaaCompLoanVal, 2).set(mult(wsaaComponentTot,t6632rec.maxpcnt));
			compute(wsaaCompLoanVal, 2).set(div(wsaaCompLoanVal,100));
			zrdecplrec.amountIn.set(wsaaCompLoanVal);
			callRounding6000();
			wsaaCompLoanVal.set(zrdecplrec.amountOut);
			wsaaLoanAvailable.add(wsaaCompLoanVal);
		}
		
		
	}

protected void sumpolSvalCalc1650(Covrpf covrsurIO)
	{
		/*START*/
		wsaaPolsumCount.set(1);
		srcalcpy.planSuffix.set(wsaaPolsumCount);
		srcalcpy.status.set(varcom.oK);
		while ( !(isEQ(srcalcpy.status,varcom.endp))) {
			calculateSurrValue1800(covrsurIO);
		}
		
		/*EXIT*/
	}

protected void getSurrSubroutine1700(Covrpf covrsurIO){
		List<Itempf> itempfList = itemDAO.getItdmByFrmdate(covrsurIO.getChdrcoy(),"T5687",covrsurIO.getCrtable(),covrsurIO.getCrrcd());

		if (itempfList.isEmpty()) {
			syserrrec.statuz.set("F294");
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		if (isNE(t5687rec.loanmeth,SPACES)) {
			itempfList = itemDAO.getItdmByFrmdate(covrsurIO.getChdrcoy(),"T6632",t5687rec.loanmeth.toString(),covrsurIO.getCrrcd());
			if (itempfList.isEmpty()) {
				syserrrec.statuz.set("G542");
				fatalError600();
			}
			else {
				t6632rec.t6632Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			}
		}
		else {
			t6632rec.t6632Rec.set(SPACES);
		}
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(covrsurIO.getChdrcoy());
		itempf.setItemtabl("T6598");
		itempf.setItemitem(t5687rec.svMethod.toString());
	    itempf = itemDAO.getItempfRecord(itempf);
		if (itempf == null) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void calculateSurrValue1800(Covrpf covrsurIO){
		if (isEQ(t6598rec.calcprog,SPACES)) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		checkCovrsurStatii1850(covrsurIO);
		if (isEQ(wsaaPremStatus,"N")
		|| isEQ(wsaaRiskStatus,"N")) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrpf);//VPMS call  ILIFE-5962
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}
				/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status,varcom.oK)
		&& isNE(srcalcpy.status,varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		callRounding6000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		/* Add values returned to our running Surrender value total*/
		wsaaSurrValTot.add(srcalcpy.actualVal);
		wsaaComponentTot.add(srcalcpy.actualVal);
		wsaaSurrValTot.add(srcalcpy.estimatedVal);
		wsaaComponentTot.add(srcalcpy.estimatedVal);
	}

protected void checkCovrsurStatii1850(Covrpf covrsurIO)
	{
		/*START*/
		wsaaPremStatus.set("N");
		wsaaRiskStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaPremStatus,"Y")); wsaaIndex.add(1)){
			premiumStatus1870(covrsurIO);
		}
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaRiskStatus,"Y")); wsaaIndex.add(1)){
			riskStatus1890(covrsurIO);
		}
		/*EXIT*/
	}

protected void premiumStatus1870(Covrpf covrsurIO)
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(),ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],covrsurIO.getPstatcode()))) {
					wsaaPremStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(),ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],covrsurIO.getPstatcode()))) {
					wsaaPremStatus.set("Y");
					return ;
				}
			}
		}
	}

protected void riskStatus1890(Covrpf covrsurIO)
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(),ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrsurIO.getStatcode()))) {
					wsaaRiskStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(),ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrsurIO.getStatcode()))) {
					wsaaRiskStatus.set("Y");
					return ;
				}
			}
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2020();
			checkPfkey2070();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2020()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.currcd,SPACES)) {
			sv.currcd.set(chdrpf.getCntcurr()); //ILIFE-5962
		}
		if (isNE(sv.currcd,wsaaStoredCurrency)) {
			changeCurrency2100();
			wsaaStoredCurrency.set(sv.currcd);
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(sv.currcd,sv.cntcurr)) {
				wsaaOldCurrVal.set(wsaaHeldLoanAvail);
				convert2200();
				sv.loanallow.set(wsaaNewCurrVal);
			}
		}
		if (isNE(sv.loanvalue, ZERO)) {
			zrdecplrec.amountIn.set(sv.loanvalue);
			callRounding6000();
			if (isNE(zrdecplrec.amountOut, sv.loanvalue)) {
				sv.loanvalueErr.set("RFIK");
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		/* Validate 'Loan reqd' value against 'Loan Available' value.*/
		if (isEQ(sv.currcd,sv.cntcurr)) {
			sv.loanallow.set(wsaaHeldLoanAvail);
		}
		if (isGT(sv.loanvalue,sv.loanallow)) {
			sv.loanvalueErr.set("E961");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.loanvalue,ZERO)) {
			sv.loanvalueErr.set("L001");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		stdutyRate2400();
		if (isNE(tt578rec.sd,0)
		&& isGTE(tt578rec.sd,sv.loanvalue)) {
			sv.loanvalueErr.set("TL02");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(tt578rec.sd,0)) {
			calStamp2500();
		}
		compute(sv.netLceamt, 2).set(sub(sv.loanvalue,sv.tplstmdty));
		if (isEQ(sv.currcd,sv.cntcurr)) {
			compute(sv.loanallow, 2).set(sub(wsaaHeldLoanAvail,sv.loanvalue));
		}
		else {
			compute(sv.loanallow, 2).set(sub(sv.loanallow,sv.loanvalue));
		}
	}

protected void checkPfkey2070()
	{
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void changeCurrency2100()
	{
			start2100();
		}

protected void start2100()
	{
		if (isEQ(sv.currcd,sv.cntcurr)) {
			sv.surrval.set(wsaaHeldSurrVal);
			sv.loansum.set(wsaaHeldCurrLoans);
			sv.loanallow.set(wsaaHeldLoanAvail);
			sv.zrcshamt.set(wsaaHeldCashAmount);
			return ;
		}
		wsaaOldCurrVal.set(wsaaHeldSurrVal);
		convert2200();
		sv.surrval.set(wsaaNewCurrVal);
		wsaaOldCurrVal.set(wsaaHeldCurrLoans);
		convert2200();
		sv.loansum.set(wsaaNewCurrVal);
		wsaaOldCurrVal.set(wsaaHeldLoanAvail);
		convert2200();
		sv.loanallow.set(wsaaNewCurrVal);
	}

protected void convert2200()
	{
			start2200();
		}

protected void start2200()
	{
		if (isEQ(wsaaOldCurrVal,ZERO)) {
			wsaaNewCurrVal.set(ZERO);
			return ;
		}
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountIn.set(wsaaOldCurrVal);
		conlinkrec.statuz.set(SPACES);
		conlinkrec.function.set("CVRT");
		conlinkrec.currIn.set(sv.cntcurr);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrpf.getChdrcoy()); //ILIFE-5962
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		else {
			wsaaNewCurrVal.set(conlinkrec.amountOut);
		}
		zrdecplrec.amountIn.set(wsaaNewCurrVal);
		callRounding6000();
		wsaaNewCurrVal.set(zrdecplrec.amountOut);
	}

protected void stdutyRate2400(){
	wsaaItemitem.set(SPACES);
	wsaaTt578Item1.set(chdrpf.getCntcurr()); //ILIFE-5962
	wsaaTt578Item2.set(wsaaBatckey.batcBatctrcde);
	List<Itempf> itempfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(),"TT578",wsaaItemitem.toString(),wsspcomn.currfrom.toInt());
	
	if (itempfList.isEmpty()) {
		tt578rec.sd.set(0);
	}
	else {
		tt578rec.tt578Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}
}

protected void calStamp2500(){
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("TT577");
		itempf.setItemitem(wsaaItemitem.toString());
	    itempf = itemDAO.getItempfRecord(itempf);
		
		if (itempf == null) {
			fatalError600();
		}
		tt577rec.tt577Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isEQ(tt577rec.subprog,SPACES)) {
			return ;
		}
		tsdtcalrec.tsdtcalRec.set(SPACES);
		tsdtcalrec.chdrcoy.set(chdrpf.getChdrcoy()); //ILIFE-5962
		tsdtcalrec.chdrnum.set(chdrpf.getChdrnum()); //ILIFE-5962
		tsdtcalrec.cntcurr.set(chdrpf.getCntcurr()); //ILIFE-5962
		tsdtcalrec.effdate.set(wsspcomn.currfrom);
		tsdtcalrec.trancde.set(wsaaBatckey.batcBatctrcde);
		tsdtcalrec.amount.set(sv.loanvalue);
		tsdtcalrec.sdutyRate.set(tt578rec.sd);
		callProgram(tt577rec.subprog, tsdtcalrec.tsdtcalRec);
		if (isNE(tsdtcalrec.statuz,varcom.oK)) {
			syserrrec.params.set(tsdtcalrec.tsdtcalRec);
			syserrrec.statuz.set(tsdtcalrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(tsdtcalrec.sdutyAmount);
		callRounding6000();
		tsdtcalrec.sdutyAmount.set(zrdecplrec.amountOut);
		sv.tplstmdty.set(tsdtcalrec.sdutyAmount);
	}

protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			return ;
		}
		writeLoanHeader3100();
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		submitAt3200();
	}

protected void writeLoanHeader3100(){
	Lhdrpf lhdrpf = new Lhdrpf();
	lhdrpf.setChdrnum(sv.chdrnum.toString());
	lhdrpf.setChdrcoy(wsspcomn.company.toString());
	lhdrpf.setLoanstdate(sv.effdate.toInt());
	lhdrpf.setLoanorigam(sv.loanvalue.getbigdata());
	lhdrpf.setLoancurr(sv.currcd.toString());
	lhdrpf.setLoantype("P");
	lhdrpf.setTermid(varcom.vrcmTermid.toString());
	lhdrpf.setUser_T(varcom.vrcmUser.toInt());
	lhdrpf.setTrdt(varcom.vrcmDate.toInt());
	lhdrpf.setTrtm(varcom.vrcmTime.toInt());
	lhdrpfdao.insertLhdrpfRecord(lhdrpf);
}

protected void submitAt3200()
	{
		start3200();
	}

protected void start3200()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(sv.chdrnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("TOAT");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5052AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.company.set(wsspcomn.company);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryKey.set(SPACES);
		wsaaPrimaryChdrnum.set(sv.chdrnum);
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaStamp.set(sv.tplstmdty);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callRounding6000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.currcd);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
}