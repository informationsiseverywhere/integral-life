/*
 * File: Genssw.java
 * Date: November 5, 2011 5:27:21 AM IST
 * Author: Quipoz Limited
 * 
 * Class transformed from GENSSW.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.recordstructures;


import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sat, 5 Nov 2011 06:58:12
 * Description:
 * Copybook name: ptempRec
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pd5h5rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData ptempRec = new FixedLengthStringData(66);
  	public FixedLengthStringData function = new FixedLengthStringData(1).isAPartOf(ptempRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(ptempRec, 1);
  	public FixedLengthStringData bnysel = new FixedLengthStringData(10).isAPartOf(ptempRec, 2);
  	public FixedLengthStringData paymmeth = new FixedLengthStringData(1).isAPartOf(ptempRec, 12);
  	public FixedLengthStringData bankkey = new FixedLengthStringData(10).isAPartOf(ptempRec, 13);
  	public FixedLengthStringData bankkeydesc = new FixedLengthStringData(20).isAPartOf(ptempRec, 23);
  	public FixedLengthStringData progIn = new FixedLengthStringData(5).isAPartOf(ptempRec, 43);
  	public FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(progIn, 0, FILLER);
  	public FixedLengthStringData progNo = new FixedLengthStringData(4).isAPartOf(progIn, 1);
  	public FixedLengthStringData transact = new FixedLengthStringData(4).isAPartOf(ptempRec, 48);
  	public FixedLengthStringData programsOut = new FixedLengthStringData(10).isAPartOf(ptempRec, 52);
  	public FixedLengthStringData[] progOut = FLSArrayPartOfStructure(2, 5, programsOut, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(10).isAPartOf(programsOut, 0, FILLER_REDEFINE);
  	public FixedLengthStringData progOut01 = new FixedLengthStringData(5).isAPartOf(filler1, 0);
  	public FixedLengthStringData progOut02 = new FixedLengthStringData(5).isAPartOf(filler1, 5);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(ptempRec, 62);


	public void initialize() {
		COBOLFunctions.initialize(ptempRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptempRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}