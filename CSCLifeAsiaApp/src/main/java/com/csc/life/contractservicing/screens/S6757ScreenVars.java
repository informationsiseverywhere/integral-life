package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6757
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6757ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(364);
	public FixedLengthStringData dataFields = new FixedLengthStringData(140).isAPartOf(dataArea, 0);
	public FixedLengthStringData jobnameJobs = new FixedLengthStringData(80).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] jobnameJob = FLSArrayPartOfStructure(8, 10, jobnameJobs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(80).isAPartOf(jobnameJobs, 0, FILLER_REDEFINE);
	public FixedLengthStringData jobnameJob01 = DD.bsjobjob.copy().isAPartOf(filler,0);
	public FixedLengthStringData jobnameJob02 = DD.bsjobjob.copy().isAPartOf(filler,10);
	public FixedLengthStringData jobnameJob03 = DD.bsjobjob.copy().isAPartOf(filler,20);
	public FixedLengthStringData jobnameJob04 = DD.bsjobjob.copy().isAPartOf(filler,30);
	public FixedLengthStringData jobnameJob05 = DD.bsjobjob.copy().isAPartOf(filler,40);
	public FixedLengthStringData jobnameJob06 = DD.bsjobjob.copy().isAPartOf(filler,50);
	public FixedLengthStringData jobnameJob07 = DD.bsjobjob.copy().isAPartOf(filler,60);
	public FixedLengthStringData jobnameJob08 = DD.bsjobjob.copy().isAPartOf(filler,70);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,81);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,89);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,97);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,105);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 140);
	public FixedLengthStringData bsjobjobsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] bsjobjobErr = FLSArrayPartOfStructure(8, 4, bsjobjobsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(bsjobjobsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData bsjobjob01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData bsjobjob02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData bsjobjob03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData bsjobjob04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData bsjobjob05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData bsjobjob06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData bsjobjob07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData bsjobjob08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 196);
	public FixedLengthStringData bsjobjobsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] bsjobjobOut = FLSArrayPartOfStructure(8, 12, bsjobjobsOut, 0);
	public FixedLengthStringData[][] bsjobjobO = FLSDArrayPartOfArrayStructure(12, 1, bsjobjobOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(96).isAPartOf(bsjobjobsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] bsjobjob01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] bsjobjob02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] bsjobjob03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] bsjobjob04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] bsjobjob05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] bsjobjob06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] bsjobjob07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] bsjobjob08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6757screenWritten = new LongData(0);
	public LongData S6757protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6757ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(bsjobjob01Out,new String[] {null, null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bsjobjob02Out,new String[] {null, null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bsjobjob03Out,new String[] {null, null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bsjobjob04Out,new String[] {null, null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bsjobjob05Out,new String[] {null, null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bsjobjob06Out,new String[] {null, null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bsjobjob07Out,new String[] {null, null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bsjobjob08Out,new String[] {null, null, "-08",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, jobnameJob01, jobnameJob02, jobnameJob03, jobnameJob04, jobnameJob05, jobnameJob06, jobnameJob07, jobnameJob08};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, bsjobjob01Out, bsjobjob02Out, bsjobjob03Out, bsjobjob04Out, bsjobjob05Out, bsjobjob06Out, bsjobjob07Out, bsjobjob08Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, bsjobjob01Err, bsjobjob02Err, bsjobjob03Err, bsjobjob04Err, bsjobjob05Err, bsjobjob06Err, bsjobjob07Err, bsjobjob08Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6757screen.class;
		protectRecord = S6757protect.class;
	}

}
