package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PrmhpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:08
 * Class transformed from PRMHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PrmhpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 94;
	public FixedLengthStringData prmhrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData prmhpfRecord = prmhrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(prmhrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(prmhrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(prmhrec);
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(prmhrec);
	public PackedDecimalData frmdate = DD.frmdate.copy().isAPartOf(prmhrec);
	public PackedDecimalData todate = DD.todate.copy().isAPartOf(prmhrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(prmhrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(prmhrec);
	public FixedLengthStringData apind = DD.apind.copy().isAPartOf(prmhrec);
	public FixedLengthStringData logtype = DD.logtype.copy().isAPartOf(prmhrec);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(prmhrec);
	public PackedDecimalData lastTranno = DD.ltranno.copy().isAPartOf(prmhrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(prmhrec);
	public PackedDecimalData zmthos = DD.zmthos.copy().isAPartOf(prmhrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(prmhrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(prmhrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(prmhrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PrmhpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PrmhpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PrmhpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PrmhpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PrmhpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PrmhpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PrmhpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PRMHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"VALIDFLAG, " +
							"ACTIND, " +
							"FRMDATE, " +
							"TODATE, " +
							"TRANNO, " +
							"EFFDATE, " +
							"APIND, " +
							"LOGTYPE, " +
							"CRTUSER, " +
							"LTRANNO, " +
							"BILLFREQ, " +
							"ZMTHOS, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     validflag,
                                     activeInd,
                                     frmdate,
                                     todate,
                                     tranno,
                                     effdate,
                                     apind,
                                     logtype,
                                     crtuser,
                                     lastTranno,
                                     billfreq,
                                     zmthos,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		validflag.clear();
  		activeInd.clear();
  		frmdate.clear();
  		todate.clear();
  		tranno.clear();
  		effdate.clear();
  		apind.clear();
  		logtype.clear();
  		crtuser.clear();
  		lastTranno.clear();
  		billfreq.clear();
  		zmthos.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPrmhrec() {
  		return prmhrec;
	}

	public FixedLengthStringData getPrmhpfRecord() {
  		return prmhpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPrmhrec(what);
	}

	public void setPrmhrec(Object what) {
  		this.prmhrec.set(what);
	}

	public void setPrmhpfRecord(Object what) {
  		this.prmhpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(prmhrec.getLength());
		result.set(prmhrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}