package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;

public class Covrpf{
    private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String validflag;
	private int tranno;
	private int currfrom;
	private int currto;
	private String statcode;
	private String pstatcode;
	private String statreasn;
	private int crrcd;
	private int anbAtCcd;
	private String sex;
	private String reptcd01;
	private String reptcd02;
	private String reptcd03;
	private String reptcd04;
	private String reptcd05;
	private String reptcd06;
	private BigDecimal crInstamt01;
	private BigDecimal crInstamt02;
	private BigDecimal crInstamt03;
	private BigDecimal crInstamt04;
	private BigDecimal crInstamt05;
	private String premCurrency;
	private String termid;
	private int transactionDate;
	private int transactionTime;
	private int user;
	private String statFund;
	private String statSect;
	private String statSubsect;
	private String crtable;
	private int rcesDte;
	private int pcesDte;
	private int bcesDte;
	private int nxtDte;
	private int rcesAge;
	private int pcesAge;
	private int bcesAge;
	private int rcesTrm;
	private int pcesTrm;
	private int bcesTrm;
	private BigDecimal sumins;
	private String sicurr;
	private BigDecimal varSumInsured;
	private String mortcls;
	private String liencd;
	private String ratingClass;
	private String indexationInd;
	private String bonusInd;
	private String deferPerdCode;
	private BigDecimal deferPerdAmt;
	private String deferPerdInd;
	private BigDecimal totMthlyBenefit;
	private BigDecimal estMatValue01;
	private BigDecimal estMatValue02;
	private int estMatDate01;
	private int estMatDate02;
	private BigDecimal estMatInt01;
	private BigDecimal estMatInt02;
	private String campaign;
	private BigDecimal statSumins;
	private int rtrnyrs;
	private String reserveUnitsInd;
	private int reserveUnitsDate;
	private String chargeOptionsInd;
	private String fundSplitPlan;
	private int premCessAgeMth;
	private int premCessAgeDay;
	private int premCessTermMth;
	private int premCessTermDay;
	private int riskCessAgeMth;
	private int riskCessAgeDay;
	private int riskCessTermMth;
	private int riskCessTermDay;
	private String jlLsInd;
	private BigDecimal instprem;
	private BigDecimal singp;
	private int rerateDate;
	private int rerateFromDate;
	private int benBillDate;
	private int annivProcDate;
	private int convertInitialUnits;
	private int reviewProcessing;
	private int unitStatementDate;
	private int cpiDate;
	private int initUnitCancDate;
	private int extraAllocDate;
	private int initUnitIncrsDate;
	private BigDecimal coverageDebt;
	private int payrseqno;
	private String bappmeth;
	private BigDecimal zbinstprem;
	private BigDecimal zlinstprem;
	private String userProfile;
	private String jobName;
	private String datime;
	private BigDecimal loadper;
	private BigDecimal rateadj; 	
	private BigDecimal fltmort;
	private BigDecimal premadj;
	private BigDecimal ageadj;
	private BigDecimal zstpduty01;
	private String tpdtype;// ILIFE-7584
	private String lnkgno;// ILIFE-7584
	private String lnkgsubrefno;
	private String lnkgind;
    private String singpremtype;//ILIFE-7805
	private BigDecimal riskprem;
	private String reinstated;//ILIFE-8509
	private BigDecimal prorateprem;
	private BigDecimal commPrem;  //IBPLIFE-5237
	
	public Covrpf() {
	}
	public Covrpf(Covrpf covrpf){
    	this.chdrcoy=covrpf.chdrcoy;
    	this.chdrnum=covrpf.chdrnum;
    	this.life=covrpf.life;
    	this.jlife=covrpf.jlife;
    	this.coverage=covrpf.coverage;
    	this.rider=covrpf.rider;
    	this.planSuffix=covrpf.planSuffix;
    	this.validflag=covrpf.validflag;
    	this.tranno=covrpf.tranno;
      	this.currfrom=covrpf.currfrom;
    	this.currto=covrpf.currto;
    	this.statcode=covrpf.statcode;
    	this.pstatcode=covrpf.pstatcode;
    	this.statreasn=covrpf.statreasn;
    	this.crrcd=covrpf.crrcd;
    	this.anbAtCcd=covrpf.anbAtCcd;
    	this.sex=covrpf.sex;
    	this.reptcd01=covrpf.reptcd01;
    	this.reptcd02=covrpf.reptcd02;
    	this.reptcd03=covrpf.reptcd03;
    	this.reptcd04=covrpf.reptcd04;
      	this.reptcd05=covrpf.reptcd05;
    	this.reptcd06=covrpf.reptcd06;
    	this.crInstamt01=covrpf.crInstamt01;
    	this.crInstamt02=covrpf.crInstamt02;
    	this.crInstamt03=covrpf.crInstamt03;
    	this.crInstamt04=covrpf.crInstamt04;
    	this.crInstamt05=covrpf.crInstamt05;
    	this.premCurrency=covrpf.premCurrency;
    	this.termid=covrpf.termid;
    	this.transactionDate=covrpf.transactionDate;
    	this.transactionTime=covrpf.transactionTime;
    	this.user=covrpf.user;
      	this.statFund=covrpf.statFund;
    	this.statSect=covrpf.statSect;
    	this.statSubsect=covrpf.statSubsect;
    	this.crtable=covrpf.crtable;
    	this.rcesDte=covrpf.rcesDte;
    	this.pcesDte=covrpf.pcesDte;
    	this.bcesDte=covrpf.bcesDte;
    	this.nxtDte=covrpf.nxtDte;
    	this.rcesAge=covrpf.rcesAge;
    	this.pcesAge=covrpf.pcesAge;
    	this.bcesAge=covrpf.bcesAge;
    	this.rcesTrm=covrpf.rcesTrm;
      	this.pcesTrm=covrpf.pcesTrm;
    	this.bcesTrm=covrpf.bcesTrm;
    	this.sumins=covrpf.sumins;
    	this.sicurr=covrpf.sicurr;
    	this.varSumInsured=covrpf.varSumInsured;
    	this.mortcls=covrpf.mortcls;
    	this.liencd=covrpf.liencd;
    	this.ratingClass=covrpf.ratingClass;
    	this.indexationInd=covrpf.indexationInd;
    	this.bonusInd=covrpf.bonusInd;
    	this.deferPerdCode=covrpf.deferPerdCode;
    	this.deferPerdAmt=covrpf.deferPerdAmt;
      	this.deferPerdInd=covrpf.deferPerdInd;
    	this.totMthlyBenefit=covrpf.totMthlyBenefit;
    	this.estMatValue01=covrpf.estMatValue01;
    	this.estMatValue02=covrpf.estMatValue02;
    	this.estMatDate01=covrpf.estMatDate01;
    	this.estMatDate02=covrpf.estMatDate02;
    	this.estMatInt01=covrpf.estMatInt01;
    	this.estMatInt02=covrpf.estMatInt02;
    	this.campaign=covrpf.campaign;
    	this.statSumins=covrpf.statSumins;
    	this.rtrnyrs=covrpf.rtrnyrs;
    	this.reserveUnitsInd=covrpf.reserveUnitsInd;
      	this.reserveUnitsDate=covrpf.reserveUnitsDate;
    	this.chargeOptionsInd=covrpf.chargeOptionsInd;
    	this.fundSplitPlan=covrpf.fundSplitPlan;
    	this.premCessAgeMth=covrpf.premCessAgeMth;
    	this.premCessAgeDay=covrpf.premCessAgeDay;
    	this.premCessTermMth=covrpf.premCessTermMth;
    	this.premCessTermDay=covrpf.premCessTermDay;
    	this.riskCessAgeMth=covrpf.riskCessAgeMth;
    	this.riskCessAgeDay=covrpf.riskCessAgeDay;
    	this.riskCessTermMth=covrpf.riskCessTermMth;
    	this.riskCessTermDay=covrpf.riskCessTermDay;
    	this.jlLsInd=covrpf.jlLsInd;
    	this.instprem=covrpf.instprem;
    	this.singp=covrpf.singp;
    	this.rerateDate=covrpf.rerateDate;
    	this.rerateFromDate=covrpf.rerateFromDate;
      	this.benBillDate=covrpf.benBillDate;
    	this.convertInitialUnits=covrpf.convertInitialUnits;
    	this.reviewProcessing=covrpf.reviewProcessing;
    	this.unitStatementDate=covrpf.unitStatementDate;
    	this.cpiDate=covrpf.cpiDate;
    	this.initUnitCancDate=covrpf.initUnitCancDate;
    	this.extraAllocDate=covrpf.extraAllocDate;
    	this.initUnitIncrsDate=covrpf.initUnitIncrsDate;
    	this.coverageDebt=covrpf.coverageDebt;
    	this.payrseqno=covrpf.payrseqno;
    	this.bappmeth=covrpf.bappmeth;
    	this.zbinstprem=covrpf.zbinstprem;
      	this.zlinstprem=covrpf.zlinstprem;
    	this.userProfile=covrpf.userProfile;
    	this.jobName=covrpf.jobName;
    	this.datime=covrpf.datime;
    	this.zstpduty01=covrpf.zstpduty01;
		this.tpdtype =covrpf.tpdtype;// ILIFE-7584
    	this.lnkgno =covrpf.lnkgno;// ILIFE-7584
    	this.lnkgsubrefno=covrpf.lnkgsubrefno;
    	this.lnkgind = covrpf.lnkgind;
        this.singpremtype=covrpf.singpremtype;//ILIFE-7805
		this.riskprem = covrpf.riskprem;
		this.reinstated=covrpf.reinstated;//ILIFE-8509
		this.prorateprem=covrpf.prorateprem;
    }
	public String getLnkgsubrefno() {
		return lnkgsubrefno;
	}
	public void setLnkgsubrefno(String lnkgsubrefno) {
		this.lnkgsubrefno = lnkgsubrefno;
	}
	// ILIFE-7584
    public String getLnkgno() {
		return lnkgno;
	}
	public void setLnkgno(String lnkgno) {
		this.lnkgno = lnkgno;
	}
	public String getTpdtype() {
		return tpdtype;
	}
	public void setTpdtype(String tpdtype) {
		this.tpdtype = tpdtype;
	}
// ILIFE-7584
    public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public String getJlife() {
        return jlife;
    }
    public void setJlife(String jlife) {
        this.jlife = jlife;
    }
    public String getCoverage() {
        return coverage;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public String getRider() {
        return rider;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public int getPlanSuffix() {
        return planSuffix;
    }
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    public String getValidflag() {
        return validflag;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public int getTranno() {
        return tranno;
    }
    public void setTranno(int tranno) {
        this.tranno = tranno;
    }
    public int getCurrfrom() {
        return currfrom;
    }
    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }
    public int getCurrto() {
        return currto;
    }
    public void setCurrto(int currto) {
        this.currto = currto;
    }
    public String getStatcode() {
        return statcode;
    }
    public void setStatcode(String statcode) {
        this.statcode = statcode;
    }
    public String getPstatcode() {
        return pstatcode;
    }
    public void setPstatcode(String pstatcode) {
        this.pstatcode = pstatcode;
    }
    public String getStatreasn() {
        return statreasn;
    }
    public void setStatreasn(String statreasn) {
        this.statreasn = statreasn;
    }
    public int getCrrcd() {
        return crrcd;
    }
    public void setCrrcd(int crrcd) {
        this.crrcd = crrcd;
    }
    public int getAnbAtCcd() {
        return anbAtCcd;
    }
    public void setAnbAtCcd(int anbAtCcd) {
        this.anbAtCcd = anbAtCcd;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
    public String getReptcd01() {
        return reptcd01;
    }
    public void setReptcd01(String reptcd01) {
        this.reptcd01 = reptcd01;
    }
    public String getReptcd02() {
        return reptcd02;
    }
    public void setReptcd02(String reptcd02) {
        this.reptcd02 = reptcd02;
    }
    public String getReptcd03() {
        return reptcd03;
    }
    public void setReptcd03(String reptcd03) {
        this.reptcd03 = reptcd03;
    }
    public String getReptcd04() {
        return reptcd04;
    }
    public void setReptcd04(String reptcd04) {
        this.reptcd04 = reptcd04;
    }
    public String getReptcd05() {
        return reptcd05;
    }
    public void setReptcd05(String reptcd05) {
        this.reptcd05 = reptcd05;
    }
    public String getReptcd06() {
        return reptcd06;
    }
    public void setReptcd06(String reptcd06) {
        this.reptcd06 = reptcd06;
    }
    public BigDecimal getCrInstamt01() {
        return crInstamt01;
    }
    public void setCrInstamt01(BigDecimal crInstamt01) {
        this.crInstamt01 = crInstamt01;
    }
    public BigDecimal getCrInstamt02() {
        return crInstamt02;
    }
    public void setCrInstamt02(BigDecimal crInstamt02) {
        this.crInstamt02 = crInstamt02;
    }
    public BigDecimal getCrInstamt03() {
        return crInstamt03;
    }
    public void setCrInstamt03(BigDecimal crInstamt03) {
        this.crInstamt03 = crInstamt03;
    }
    public BigDecimal getCrInstamt04() {
        return crInstamt04;
    }
    public void setCrInstamt04(BigDecimal crInstamt04) {
        this.crInstamt04 = crInstamt04;
    }
    public BigDecimal getCrInstamt05() {
        return crInstamt05;
    }
    public void setCrInstamt05(BigDecimal crInstamt05) {
        this.crInstamt05 = crInstamt05;
    }
    public String getPremCurrency() {
        return premCurrency;
    }
    public void setPremCurrency(String premCurrency) {
        this.premCurrency = premCurrency;
    }
    public String getTermid() {
        return termid;
    }
    public void setTermid(String termid) {
        this.termid = termid;
    }
    public int getTransactionDate() {
        return transactionDate;
    }
    public void setTransactionDate(int transactionDate) {
        this.transactionDate = transactionDate;
    }
    public int getTransactionTime() {
        return transactionTime;
    }
    public void setTransactionTime(int transactionTime) {
        this.transactionTime = transactionTime;
    }
    public int getUser() {
        return user;
    }
    public void setUser(int user) {
        this.user = user;
    }
    public String getStatFund() {
        return statFund;
    }
    public void setStatFund(String statFund) {
        this.statFund = statFund;
    }
    public String getStatSect() {
        return statSect;
    }
    public void setStatSect(String statSect) {
        this.statSect = statSect;
    }
    public String getStatSubsect() {
        return statSubsect;
    }
    public void setStatSubsect(String statSubsect) {
        this.statSubsect = statSubsect;
    }
    public String getCrtable() {
        return crtable;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    
    public BigDecimal getSumins() {
        return sumins;
    }
    public void setSumins(BigDecimal sumins) {
        this.sumins = sumins;
    }
    public String getSicurr() {
        return sicurr;
    }
    public void setSicurr(String sicurr) {
        this.sicurr = sicurr;
    }
    public BigDecimal getVarSumInsured() {
        return varSumInsured;
    }
    public void setVarSumInsured(BigDecimal varSumInsured) {
        this.varSumInsured = varSumInsured;
    }
    public String getMortcls() {
        return mortcls;
    }
    public void setMortcls(String mortcls) {
        this.mortcls = mortcls;
    }
    public String getLiencd() {
        return liencd;
    }
    public void setLiencd(String liencd) {
        this.liencd = liencd;
    }
    public String getRatingClass() {
        return ratingClass;
    }
    public void setRatingClass(String ratingClass) {
        this.ratingClass = ratingClass;
    }
    public String getIndexationInd() {
        return indexationInd;
    }
    public void setIndexationInd(String indexationInd) {
        this.indexationInd = indexationInd;
    }
    public String getBonusInd() {
        return bonusInd;
    }
    public void setBonusInd(String bonusInd) {
        this.bonusInd = bonusInd;
    }
    public String getDeferPerdCode() {
        return deferPerdCode;
    }
    public void setDeferPerdCode(String deferPerdCode) {
        this.deferPerdCode = deferPerdCode;
    }
    public BigDecimal getDeferPerdAmt() {
        return deferPerdAmt;
    }
    public void setDeferPerdAmt(BigDecimal deferPerdAmt) {
        this.deferPerdAmt = deferPerdAmt;
    }
    public String getDeferPerdInd() {
        return deferPerdInd;
    }
    public void setDeferPerdInd(String deferPerdInd) {
        this.deferPerdInd = deferPerdInd;
    }
    public int getRcesDte() {
		return rcesDte;
	}
	public void setRcesDte(int rcesDte) {
		this.rcesDte = rcesDte;
	}
	public int getPcesDte() {
		return pcesDte;
	}
	public void setPcesDte(int pcesDte) {
		this.pcesDte = pcesDte;
	}
	public int getBcesDte() {
		return bcesDte;
	}
	public void setBcesDte(int bcesDte) {
		this.bcesDte = bcesDte;
	}
	public int getNxtDte() {
		return nxtDte;
	}
	public void setNxtDte(int nxtDte) {
		this.nxtDte = nxtDte;
	}
	public int getRcesAge() {
		return rcesAge;
	}
	public void setRcesAge(int rcesAge) {
		this.rcesAge = rcesAge;
	}
	public int getPcesAge() {
		return pcesAge;
	}
	public void setPcesAge(int pcesAge) {
		this.pcesAge = pcesAge;
	}
	public int getBcesAge() {
		return bcesAge;
	}
	public void setBcesAge(int bcesAge) {
		this.bcesAge = bcesAge;
	}
	public int getRcesTrm() {
		return rcesTrm;
	}
	public void setRcesTrm(int rcesTrm) {
		this.rcesTrm = rcesTrm;
	}
	public int getPcesTrm() {
		return pcesTrm;
	}
	public void setPcesTrm(int pcesTrm) {
		this.pcesTrm = pcesTrm;
	}
	public int getBcesTrm() {
		return bcesTrm;
	}
	public void setBcesTrm(int bcesTrm) {
		this.bcesTrm = bcesTrm;
	}
	public BigDecimal getTotMthlyBenefit() {
        return totMthlyBenefit;
    }
    public void setTotMthlyBenefit(BigDecimal totMthlyBenefit) {
        this.totMthlyBenefit = totMthlyBenefit;
    }
    public BigDecimal getEstMatValue01() {
        return estMatValue01;
    }
    public void setEstMatValue01(BigDecimal estMatValue01) {
        this.estMatValue01 = estMatValue01;
    }
    public BigDecimal getEstMatValue02() {
        return estMatValue02;
    }
    public void setEstMatValue02(BigDecimal estMatValue02) {
        this.estMatValue02 = estMatValue02;
    }
    public int getEstMatDate01() {
        return estMatDate01;
    }
    public void setEstMatDate01(int estMatDate01) {
        this.estMatDate01 = estMatDate01;
    }
    public int getEstMatDate02() {
        return estMatDate02;
    }
    public void setEstMatDate02(int estMatDate02) {
        this.estMatDate02 = estMatDate02;
    }
    public BigDecimal getEstMatInt01() {
        return estMatInt01;
    }
    public void setEstMatInt01(BigDecimal estMatInt01) {
        this.estMatInt01 = estMatInt01;
    }
    public BigDecimal getEstMatInt02() {
        return estMatInt02;
    }
    public void setEstMatInt02(BigDecimal estMatInt02) {
        this.estMatInt02 = estMatInt02;
    }
    public String getCampaign() {
        return campaign;
    }
    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }
    public BigDecimal getStatSumins() {
        return statSumins;
    }
    public void setStatSumins(BigDecimal statSumins) {
        this.statSumins = statSumins;
    }
    public int getRtrnyrs() {
        return rtrnyrs;
    }
    public void setRtrnyrs(int rtrnyrs) {
        this.rtrnyrs = rtrnyrs;
    }
    public String getReserveUnitsInd() {
        return reserveUnitsInd;
    }
    public void setReserveUnitsInd(String reserveUnitsInd) {
        this.reserveUnitsInd = reserveUnitsInd;
    }
    public int getReserveUnitsDate() {
        return reserveUnitsDate;
    }
    public void setReserveUnitsDate(int reserveUnitsDate) {
        this.reserveUnitsDate = reserveUnitsDate;
    }
    public String getChargeOptionsInd() {
        return chargeOptionsInd;
    }
    public void setChargeOptionsInd(String chargeOptionsInd) {
        this.chargeOptionsInd = chargeOptionsInd;
    }
    public String getFundSplitPlan() {
        return fundSplitPlan;
    }
    public void setFundSplitPlan(String fundSplitPlan) {
        this.fundSplitPlan = fundSplitPlan;
    }
    public int getPremCessAgeMth() {
        return premCessAgeMth;
    }
    public void setPremCessAgeMth(int premCessAgeMth) {
        this.premCessAgeMth = premCessAgeMth;
    }
    public int getPremCessAgeDay() {
        return premCessAgeDay;
    }
    public void setPremCessAgeDay(int premCessAgeDay) {
        this.premCessAgeDay = premCessAgeDay;
    }
    public int getPremCessTermMth() {
        return premCessTermMth;
    }
    public void setPremCessTermMth(int premCessTermMth) {
        this.premCessTermMth = premCessTermMth;
    }
    public int getPremCessTermDay() {
        return premCessTermDay;
    }
    public void setPremCessTermDay(int premCessTermDay) {
        this.premCessTermDay = premCessTermDay;
    }
    public int getRiskCessAgeMth() {
        return riskCessAgeMth;
    }
    public void setRiskCessAgeMth(int riskCessAgeMth) {
        this.riskCessAgeMth = riskCessAgeMth;
    }
    public int getRiskCessAgeDay() {
        return riskCessAgeDay;
    }
    public void setRiskCessAgeDay(int riskCessAgeDay) {
        this.riskCessAgeDay = riskCessAgeDay;
    }
    public int getRiskCessTermMth() {
        return riskCessTermMth;
    }
    public void setRiskCessTermMth(int riskCessTermMth) {
        this.riskCessTermMth = riskCessTermMth;
    }
    public int getRiskCessTermDay() {
        return riskCessTermDay;
    }
    public void setRiskCessTermDay(int riskCessTermDay) {
        this.riskCessTermDay = riskCessTermDay;
    }
    public String getJlLsInd() {
        return jlLsInd;
    }
    public void setJlLsInd(String jlLsInd) {
        this.jlLsInd = jlLsInd;
    }
    public BigDecimal getInstprem() {
        return instprem;
    }
    public void setInstprem(BigDecimal instprem) {
        this.instprem = instprem;
    }
    public BigDecimal getSingp() {
        return singp;
    }
    public void setSingp(BigDecimal singp) {
        this.singp = singp;
    }
    public int getRerateDate() {
        return rerateDate;
    }
    public void setRerateDate(int rerateDate) {
        this.rerateDate = rerateDate;
    }
    public int getRerateFromDate() {
        return rerateFromDate;
    }
    public void setRerateFromDate(int rerateFromDate) {
        this.rerateFromDate = rerateFromDate;
    }
    public int getBenBillDate() {
        return benBillDate;
    }
    public void setBenBillDate(int benBillDate) {
        this.benBillDate = benBillDate;
    }
    public int getAnnivProcDate() {
        return annivProcDate;
    }
    public void setAnnivProcDate(int annivProcDate) {
        this.annivProcDate = annivProcDate;
    }
    public int getConvertInitialUnits() {
        return convertInitialUnits;
    }
    public void setConvertInitialUnits(int convertInitialUnits) {
        this.convertInitialUnits = convertInitialUnits;
    }
    public int getReviewProcessing() {
        return reviewProcessing;
    }
    public void setReviewProcessing(int reviewProcessing) {
        this.reviewProcessing = reviewProcessing;
    }
    public int getUnitStatementDate() {
        return unitStatementDate;
    }
    public void setUnitStatementDate(int unitStatementDate) {
        this.unitStatementDate = unitStatementDate;
    }
    public int getCpiDate() {
        return cpiDate;
    }
    public void setCpiDate(int cpiDate) {
        this.cpiDate = cpiDate;
    }
    public int getInitUnitCancDate() {
        return initUnitCancDate;
    }
    public void setInitUnitCancDate(int initUnitCancDate) {
        this.initUnitCancDate = initUnitCancDate;
    }
    public int getExtraAllocDate() {
        return extraAllocDate;
    }
    public void setExtraAllocDate(int extraAllocDate) {
        this.extraAllocDate = extraAllocDate;
    }
    public int getInitUnitIncrsDate() {
        return initUnitIncrsDate;
    }
    public void setInitUnitIncrsDate(int initUnitIncrsDate) {
        this.initUnitIncrsDate = initUnitIncrsDate;
    }
    public BigDecimal getCoverageDebt() {
        return coverageDebt;
    }
    public void setCoverageDebt(BigDecimal coverageDebt) {
        this.coverageDebt = coverageDebt;
    }
    public int getPayrseqno() {
        return payrseqno;
    }
    public void setPayrseqno(int payrseqno) {
        this.payrseqno = payrseqno;
    }
    public String getBappmeth() {
        return bappmeth;
    }
    public void setBappmeth(String bappmeth) {
        this.bappmeth = bappmeth;
    }
    public BigDecimal getZbinstprem() {
        return zbinstprem;
    }
    public void setZbinstprem(BigDecimal zbinstprem) {
        this.zbinstprem = zbinstprem;
    }
    public BigDecimal getZlinstprem() {
        return zlinstprem;
    }
    public void setZlinstprem(BigDecimal zlinstprem) {
        this.zlinstprem = zlinstprem;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
	public BigDecimal getLoadper() {
		return loadper;
	}
	public void setLoadper(BigDecimal loadper) {
		this.loadper = loadper;
	}
	public BigDecimal getRateadj() {
		return rateadj;
	}
	public void setRateadj(BigDecimal rateadj) {
		this.rateadj = rateadj;
	}
	public BigDecimal getFltmort() {
		return fltmort;
	}
	public void setFltmort(BigDecimal fltmort) {
		this.fltmort = fltmort;
	}
	public BigDecimal getPremadj() {
		return premadj;
	}
	public void setPremadj(BigDecimal premadj) {
		this.premadj = premadj;
	}
	public BigDecimal getAgeadj() {
		return ageadj;
	}
	public void setAgeadj(BigDecimal ageadj) {
		this.ageadj = ageadj;
	}
	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}
	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}
	public String getLnkgind() {
		return lnkgind;
	}
	public void setLnkgind(String lnkgind) {
		this.lnkgind=lnkgind;
	}
	/*ILIFE-7805:Starts*/
	public String getSingpremtype() {
		return singpremtype;
	}
	public void setSingpremtype(String singpremtype) {
		this.singpremtype = singpremtype;
	}  
  /*ILIFE-7805:Ends*/
  
	public BigDecimal getRiskprem() {
		return riskprem;
	}
	public void setRiskprem(BigDecimal riskprem) {
		this.riskprem = riskprem;
	}
	//ILIFE-8509
	public String getReinstated() {
		return reinstated;
	}
	public void setReinstated(String reinstated) {
		this.reinstated = reinstated;
	}	
	public BigDecimal getProrateprem() {
		return prorateprem;
	}
	public void setProrateprem(BigDecimal prorateprem) {
		this.prorateprem = prorateprem;
	}
	//IBPLIFE-5237
	public BigDecimal getCommPrem() {
		return commPrem;
	}
	public void setCommPrem(BigDecimal commPrem) {
		this.commPrem = commPrem;
	}	
}