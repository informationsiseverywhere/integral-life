package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:14
 * Description:
 * Copybook name: PTRNREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptrnrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptrnrevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData ptrnrevKey = new FixedLengthStringData(256).isAPartOf(ptrnrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptrnrevChdrcoy = new FixedLengthStringData(1).isAPartOf(ptrnrevKey, 0);
  	public FixedLengthStringData ptrnrevChdrnum = new FixedLengthStringData(8).isAPartOf(ptrnrevKey, 1);
  	public PackedDecimalData ptrnrevTranno = new PackedDecimalData(5, 0).isAPartOf(ptrnrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(ptrnrevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptrnrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptrnrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}