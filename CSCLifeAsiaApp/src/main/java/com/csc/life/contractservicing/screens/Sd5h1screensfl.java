package com.csc.life.contractservicing.screens;


import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.TableModel.Subfile.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5h1screensfl  extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static int maxRecords = 17;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {15, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5h1ScreenVars sv = (Sd5h1ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sd5h1screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sd5h1screensfl, 
			sv.Sd5h1screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sd5h1ScreenVars sv = (Sd5h1ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sd5h1screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sd5h1ScreenVars sv = (Sd5h1ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sd5h1screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sd5h1screensflWritten.gt(0))
		{
			sv.sd5h1screensfl.setCurrentIndex(0);
			sv.Sd5h1screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sd5h1ScreenVars sv = (Sd5h1ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sd5h1screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5h1ScreenVars screenVars = (Sd5h1ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.loanNumber.setFieldName("loanNumber");
				screenVars.loanType.setFieldName("loanType");
				screenVars.loanstdateDisp.setFieldName("loanstdateDisp");
				screenVars.loanstdate.setFieldName("loanstdate");				
				screenVars.cntcurr.setFieldName("cntcurr");
				screenVars.intrstpercentage.setFieldName("intrstpercentage");
				screenVars.hprincipal.setFieldName("hprincipal");
				screenVars.hcurbal.setFieldName("hcurbal");
				screenVars.hacrint.setFieldName("hacrint");
				screenVars.hpndint.setFieldName("hpndint");
				screenVars.hpltot.setFieldName("hpltot");
				screenVars.repayment.setFieldName("repayment");
				screenVars.repaymentstatus.setFieldName("repaymentstatus");
				
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.loanNumber.set(dm.getField("loanNumber"));
			screenVars.loanType.set(dm.getField("loanType"));
			screenVars.loanstdateDisp.set(dm.getField("loanstdateDisp"));
			screenVars.loanstdate.set(dm.getField("loanstdate"));			
			screenVars.cntcurr.set(dm.getField("cntcurr"));
			screenVars.intrstpercentage.set(dm.getField("intrstpercentage"));
			screenVars.hprincipal.set(dm.getField("hprincipal"));
			screenVars.hcurbal.set(dm.getField("hcurbal"));
			screenVars.hacrint.set(dm.getField("hacrint"));
			screenVars.hpndint.set(dm.getField("hpndint"));
			screenVars.hpltot.set(dm.getField("hpltot"));
			screenVars.repayment.set(dm.getField("repayment"));
			screenVars.repaymentstatus.set(dm.getField("repaymentstatus"));
			
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5h1ScreenVars screenVars = (Sd5h1ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.loanNumber.setFieldName("loanNumber");
				screenVars.loanType.setFieldName("loanType");
				screenVars.loanstdateDisp.setFieldName("loanstdateDisp");
				screenVars.loanstdate.setFieldName("loanstdate");				
				screenVars.cntcurr.setFieldName("cntcurr");
				screenVars.intrstpercentage.setFieldName("intrstpercentage");
				screenVars.hprincipal.setFieldName("hprincipal");
				screenVars.hcurbal.setFieldName("hcurbal");
				screenVars.hacrint.setFieldName("hacrint");
				screenVars.hpndint.setFieldName("hpndint");
				screenVars.hpltot.setFieldName("hpltot");
				screenVars.repayment.setFieldName("repayment");
				screenVars.repaymentstatus.setFieldName("repaymentstatus");
				
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("loanNumber").set(screenVars.loanNumber);
			dm.getField("loanType").set(screenVars.loanType);
			dm.getField("loanstdateDisp").set(screenVars.loanstdateDisp);
			dm.getField("loanstdate").set(screenVars.loanstdate);			
			dm.getField("cntcurr").set(screenVars.cntcurr);
			dm.getField("intrstpercentage").set(screenVars.intrstpercentage);
			dm.getField("hprincipal").set(screenVars.hprincipal);
			dm.getField("hcurbal").set(screenVars.hcurbal);
			dm.getField("hacrint").set(screenVars.hacrint);
			dm.getField("hpndint").set(screenVars.hpndint);
			dm.getField("hpltot").set(screenVars.hpltot);
			dm.getField("repayment").set(screenVars.repayment);
			dm.getField("repaymentstatus").set(screenVars.repaymentstatus);
			
			
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sd5h1screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sd5h1ScreenVars screenVars = (Sd5h1ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.loanNumber.clearFormatting();
		screenVars.loanType.clearFormatting();
		screenVars.loanstdateDisp.clearFormatting();
		screenVars.loanstdate.clearFormatting();
		screenVars.cntcurr.clearFormatting();
		screenVars.intrstpercentage.clearFormatting();
		screenVars.hprincipal.clearFormatting();
		screenVars.hcurbal.clearFormatting();
		screenVars.hacrint.clearFormatting();
		screenVars.hpndint.clearFormatting();
		screenVars.hpltot.clearFormatting();	
		screenVars.repayment.clearFormatting();
		screenVars.repaymentstatus.clearFormatting();
	
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sd5h1ScreenVars screenVars = (Sd5h1ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.loanNumber.setClassString("");
		screenVars.loanType.setClassString("");
		screenVars.loanstdateDisp.setClassString("");
		screenVars.loanstdate.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.intrstpercentage.setClassString("");
		screenVars.hprincipal.setClassString("");
		screenVars.hcurbal.setClassString("");
		screenVars.hacrint.setClassString("");
		screenVars.hpndint.setClassString("");
		screenVars.hpltot.setClassString("");	
		screenVars.repayment.setClassString("");	
		screenVars.repaymentstatus.setClassString("");			
		
	}

/**
 * Clear all the variables in Sd5h1screensfl
 */
	public static void clear(VarModel pv) {
		Sd5h1ScreenVars screenVars = (Sd5h1ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.loanNumber.clear();
		screenVars.loanType.clear();
		screenVars.loanstdateDisp.clear();
		screenVars.loanstdate.clear();
		screenVars.cntcurr.clear();
		screenVars.intrstpercentage.clear();
		screenVars.hprincipal.clear();
		screenVars.hcurbal.clear();
		screenVars.hacrint.clear();
		screenVars.hpndint.clear();
		screenVars.hpltot.clear();
		screenVars.repayment.clear();
		screenVars.repaymentstatus.clear();
				
	}
}
