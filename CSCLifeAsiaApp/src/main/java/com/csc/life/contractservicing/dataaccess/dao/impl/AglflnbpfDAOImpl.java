package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.csc.common.DD;
import com.csc.life.contractservicing.dataaccess.dao.AglflnbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Aglflnbpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AglflnbpfDAOImpl extends BaseDAOImpl<Aglflnbpf> implements AglflnbpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AglflnbpfDAOImpl.class);
	
	private static final String SQLForGetAgentInformByNum = "select t1.CLNTNUM as CLNTNUM, t1.AGNTBR as AGNTBR,t1.DTEAPP as DTEAPP,t1.DTETRM as DTETRM, "
			+ "t1.AGNTCOY as AGNTCOY,t1.AGNTNUM as AGNTNUM,t1.DTEEXP,t1.BCMTAB,t1.RCMTAB,t1.SCMTAB,t1.AGCLS,t1.REPORTAG,"
			+ "t1.OVCPC,t1.AGTYPE,t1.ARACDE,t1.ZRORCODE,t1.EFFDATE,t1.UNIQUE_NUMBER,t1.TLAGLICNO,"
			+ "t1.TLICEXPDT,t1.PRDAGENT,t1.AGCCQIND,t1.VALIDFLAG,t1.JOBNM,t1.USRPRF,t1.DATIME,"
			+ "t1.ZRECRUIT, t2.GIVNAME as GIVNAME, t2.SURNAME as SURNAME, t2.CLTTYPE as CLTTYPE, t2.lsurname as LSURNAME, t2.lgivname as LGIVNAME"
			+ " from Aglflnb t1, Clntpf t2 where t1.AGNTNUM = ? and t1.AGNTCOY = ?"
			+ " and t1.VALIDFLAG = '1' and t2.clntpfx = 'CN' and t1.clntnum = t2.clntnum";
	@Override
	public Aglflnbpf getAgentInformByNum(String agntNum, String compy) {
		Aglflnbpf aglflnbpf = new Aglflnbpf();
		try (PreparedStatement prep = getPrepareStatement(SQLForGetAgentInformByNum)) {
			prep.setString(1, agntNum);
			prep.setString(2, compy);
			ResultSet rs = prep.executeQuery();
			if (rs.next()) {
				BeanPropertyRowMapper<Aglflnbpf> rowMapper = BeanPropertyRowMapper.newInstance(Aglflnbpf.class);
				aglflnbpf = rowMapper.mapRow(rs, 1);
			}
		} catch (SQLException e) {
			LOGGER.error("getAgentInformByNum()", e);
			throw new SQLRuntimeException(e);
		}
		return aglflnbpf;
	}
	public Aglflnbpf getAglflnb(String agntNum, String compy) {
		Aglflnbpf aglflnbpf = null;
		StringBuilder sqlAglfSelect1 = new StringBuilder("SELECT * FROM AGLFLNB WHERE AGNTCOY=? AND AGNTNUM=? ");
		sqlAglfSelect1.append(" ORDER BY AGNTCOY ASC, AGNTNUM ASC, UNIQUE_NUMBER DESC");
		try (PreparedStatement prep = getPrepareStatement(sqlAglfSelect1.toString())) {
		/*	prep.setString(1, compy);
			prep.setString(2, agntNum);*/
			prep.setString(1, StringUtil.fillSpace(compy.trim(), DD.company.length)); 
			prep.setString(2, StringUtil.fillSpace(agntNum.trim(), DD.agntnum.length)); 
			ResultSet rs = prep.executeQuery();
			if (rs.next()) {
				BeanPropertyRowMapper<Aglflnbpf> rowMapper = BeanPropertyRowMapper.newInstance(Aglflnbpf.class);
				aglflnbpf = rowMapper.mapRow(rs, 1);
			}
		} catch (SQLException e) {
			LOGGER.error("getAgentInformByNum()", e);
			throw new SQLRuntimeException(e);
		}
		return aglflnbpf;
	}

}
