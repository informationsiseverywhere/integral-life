package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5151
 * @version 1.0 generated on 30/08/09 06:36
 * @author Quipoz
 */
public class S5151ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1040);//ILIFE-7805
	public FixedLengthStringData dataFields = new FixedLengthStringData(384).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,114);
	public ZonedDecimalData matage = DD.matage.copyToZonedDecimal().isAPartOf(dataFields,161);
	public ZonedDecimalData mattcess = DD.mattcess.copyToZonedDecimal().isAPartOf(dataFields,164);
	public ZonedDecimalData mattrm = DD.mattrm.copyToZonedDecimal().isAPartOf(dataFields,172);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,175);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,176);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,177);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,180);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,183);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,187);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,191);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,201);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,202);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,219);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,222);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,226);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,241);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,258);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,259);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,272);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,289);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 306);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 309);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,339);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,356);
	public FixedLengthStringData singpremtype = DD.sngprmtyp.copy().isAPartOf(dataFields,364);//ILIFE-7805
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,367);//ALS-7685
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(164).isAPartOf(dataArea, 384);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData matageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mattcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mattrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData instPremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData singpremtypeErr =new FixedLengthStringData(4).isAPartOf(errorIndicators,156);//ILIFE-7805
	public FixedLengthStringData zstpduty01Err =new FixedLengthStringData(4).isAPartOf(errorIndicators,160);//ALS-7685
	
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(478).isAPartOf(dataArea, 483);//ILIFE-7805
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(492).isAPartOf(dataArea, 544); //ILIFE-7805
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] matageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mattcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mattrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] instPremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] singpremtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);//ILIFE-7805
	public FixedLengthStringData[] zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);//ALS-7685
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData mattcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S5151screenWritten = new LongData(0);
	public LongData S5151protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5151ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminOut,new String[] {"03","01","-03","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(matageOut,new String[] {"14","13","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mattrmOut,new String[] {"16","15","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mattcessOut,new String[] {"17","18","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"07","06","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"09","08","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premcessOut,new String[] {"10","11","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"24","23","-24","43",null, null, null, null, null, null, null, null});
		fieldIndMap.put(singprmOut,new String[] {"26","25","-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"34","36","-34","35",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {"33",null, "-33","63",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"62","63","-26","63",null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {null, "61",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"41","60","-41","60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"32","31","-32","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"22","60","-22","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(instPremOut,new String[] {null,"64", null, null,null, null, null, null, null, null, null, null});
		fieldIndMap.put(singpremtypeOut,new String[] {"65","66","-65","67",null, null, null, null, null, null, null, null});//ILIFE-7805 
		fieldIndMap.put(zstpduty01Out,new String[] {"68","69","-70","71",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifenum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, polinc, planSuffix, sumin, currcd, matage, mattrm, mattcess, premCessAge, premCessTerm, premcess, zlinstprem, liencd, singlePremium, mortcls, taxamt, taxind, select, comind, optextind, cnttype, ctypedes, instPrem, singpremtype,zstpduty01};
		screenOutFields = new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifenumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, polincOut, plnsfxOut, suminOut, currcdOut, matageOut, mattrmOut, mattcessOut, pcessageOut, pcesstrmOut, premcessOut, zlinstpremOut, liencdOut, singprmOut, mortclsOut, taxamtOut, taxindOut, selectOut, comindOut, optextindOut, cnttypeOut, ctypedesOut, instPremOut, singpremtypeOut,zstpduty01Out};
		screenErrFields = new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifenumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, polincErr, plnsfxErr, suminErr, currcdErr, matageErr, mattrmErr, mattcessErr, pcessageErr, pcesstrmErr, premcessErr, zlinstpremErr, liencdErr, singprmErr, mortclsErr, taxamtErr, taxindErr, selectErr, comindErr, optextindErr, cnttypeErr, ctypedesErr, instPremErr, singpremtypeErr,zstpduty01Err};
		screenDateFields = new BaseData[] {mattcess, premcess, effdate};
		screenDateErrFields = new BaseData[] {mattcessErr, premcessErr, effdateErr};
		screenDateDispFields = new BaseData[] {mattcessDisp, premcessDisp, effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5151screen.class;
		protectRecord = S5151protect.class;
	}

}
