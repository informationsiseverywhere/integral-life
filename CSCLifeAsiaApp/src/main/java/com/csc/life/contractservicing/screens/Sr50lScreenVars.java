package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR50L
 * @version 1.0 generated on 30/08/09 07:15
 * @author Quipoz
 */
public class Sr50lScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(87);
	public FixedLengthStringData dataFields = new FixedLengthStringData(55).isAPartOf(dataArea, 0);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 55);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 63);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(202);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(40).isAPartOf(subfileArea, 0);
	public ZonedDecimalData anbage = DD.anbage.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData ccdate = DD.ccdate.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,11);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(subfileFields,19);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,20);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,22);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(subfileFields,24);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,32);
	public FixedLengthStringData status = DD.status.copy().isAPartOf(subfileFields,33);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,35);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 40);
	public FixedLengthStringData anbageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData ccdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData statusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 80);
	public FixedLengthStringData[] anbageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] ccdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] statusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 200);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData ccdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr50lscreensflWritten = new LongData(0);
	public LongData Sr50lscreenctlWritten = new LongData(0);
	public LongData Sr50lscreenWritten = new LongData(0);
	public LongData Sr50lprotectWritten = new LongData(0);
	public GeneralTable sr50lscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr50lscreensfl;
	}

	public Sr50lScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {life, jlife, anbage, tranno, ind, select, chdrnum, status, ccdate, ptdate};
		screenSflOutFields = new BaseData[][] {lifeOut, jlifeOut, anbageOut, trannoOut, indOut, selectOut, chdrnumOut, statusOut, ccdateOut, ptdateOut};
		screenSflErrFields = new BaseData[] {lifeErr, jlifeErr, anbageErr, trannoErr, indErr, selectErr, chdrnumErr, statusErr, ccdateErr, ptdateErr};
		screenSflDateFields = new BaseData[] {ccdate, ptdate};
		screenSflDateErrFields = new BaseData[] {ccdateErr, ptdateErr};
		screenSflDateDispFields = new BaseData[] {ccdateDisp, ptdateDisp};

		screenFields = new BaseData[] {lifcnum, lifename};
		screenOutFields = new BaseData[][] {lifcnumOut, lifenameOut};
		screenErrFields = new BaseData[] {lifcnumErr, lifenameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr50lscreen.class;
		screenSflRecord = Sr50lscreensfl.class;
		screenCtlRecord = Sr50lscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr50lprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr50lscreenctl.lrec.pageSubfile);
	}
}
