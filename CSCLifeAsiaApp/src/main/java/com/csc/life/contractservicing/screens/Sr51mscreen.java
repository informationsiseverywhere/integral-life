package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr51mscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {19, 23, 2, 63}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr51mScreenVars sv = (Sr51mScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr51mscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr51mScreenVars screenVars = (Sr51mScreenVars)pv;
		screenVars.optdsc.setClassString("");
		screenVars.optind.setClassString("");
		screenVars.xamt.setClassString("");
		screenVars.manadj.setClassString("");
		screenVars.totalfee.setClassString("");
		screenVars.cmax.setClassString("");
		screenVars.reqamt.setClassString("");
		screenVars.hprjptdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr51mscreen
 */
	public static void clear(VarModel pv) {
		Sr51mScreenVars screenVars = (Sr51mScreenVars) pv;
		screenVars.optdsc.clear();
		screenVars.optind.clear();
		screenVars.xamt.clear();
		screenVars.manadj.clear();
		screenVars.totalfee.clear();
		screenVars.cmax.clear();
		screenVars.reqamt.clear();
		screenVars.hprjptdateDisp.clear();
		screenVars.hprjptdate.clear();
	}
}
