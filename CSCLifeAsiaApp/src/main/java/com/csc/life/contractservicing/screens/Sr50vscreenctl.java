package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:48
 * @author Quipoz
 */
public class Sr50vscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr50vscreensfl";
		lrec.subfileClass = Sr50vscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 15;
		lrec.pageSubfile = 9;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 11, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr50vScreenVars sv = (Sr50vScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr50vscreenctlWritten, sv.Sr50vscreensflWritten, av, sv.sr50vscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr50vScreenVars screenVars = (Sr50vScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.jownnum.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.fupcode.setClassString("");
		screenVars.crtuser.setClassString("");
		screenVars.lstupuser.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.clntname01.setClassString("");
		screenVars.clntname02.setClassString("");
		screenVars.clntname03.setClassString("");
		screenVars.clntname04.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.name01.setClassString("");
		screenVars.name02.setClassString("");
		screenVars.crtdateDisp.setClassString("");
		screenVars.zlstupdtDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr50vscreenctl
 */
	public static void clear(VarModel pv) {
		Sr50vScreenVars screenVars = (Sr50vScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cownnum.clear();
		screenVars.jownnum.clear();
		screenVars.lifcnum.clear();
		screenVars.jlifcnum.clear();
		screenVars.fupcode.clear();
		screenVars.crtuser.clear();
		screenVars.lstupuser.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.clntname01.clear();
		screenVars.clntname02.clear();
		screenVars.clntname03.clear();
		screenVars.clntname04.clear();
		screenVars.longdesc.clear();
		screenVars.name01.clear();
		screenVars.name02.clear();
		screenVars.crtdateDisp.clear();
		screenVars.crtdate.clear();
		screenVars.zlstupdtDisp.clear();
		screenVars.zlstupdt.clear();
	}
}
