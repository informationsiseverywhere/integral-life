package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:35
 * Description:
 * Copybook name: MANDMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mandmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mandmjaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData mandmjaKey = new FixedLengthStringData(256).isAPartOf(mandmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData mandmjaPayrcoy = new FixedLengthStringData(1).isAPartOf(mandmjaKey, 0);
  	public FixedLengthStringData mandmjaPayrnum = new FixedLengthStringData(8).isAPartOf(mandmjaKey, 1);
  	public FixedLengthStringData mandmjaMandref = new FixedLengthStringData(5).isAPartOf(mandmjaKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(mandmjaKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mandmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mandmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}