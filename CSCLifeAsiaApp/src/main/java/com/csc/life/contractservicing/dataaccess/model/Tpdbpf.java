package com.csc.life.contractservicing.dataaccess.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Tpdbpf implements Serializable {
	
	private static final long serialVersionUID = -4362090257104787190L;
	
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	public String chdrcoy;
	public String chdrnum;
	public int tranno;
	public String cnttype;
	public BigDecimal effdate;
	public BigDecimal orgamnt;
	public BigDecimal fromdate;
	public BigDecimal todate;
	public String tdbtdesc;
	public BigDecimal tdbtrate;
	public BigDecimal tdbtamt;
	public String validflag;
	public String userProfile;
	public String jobName;
	public Date datime;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public BigDecimal getEffdate() {
		return effdate;
	}
	public void setEffdate(BigDecimal effdate) {
		this.effdate = effdate;
	}
	public BigDecimal getOrgamnt() {
		return orgamnt;
	}
	public void setOrgamnt(BigDecimal orgamnt) {
		this.orgamnt = orgamnt;
	}
	public BigDecimal getFromdate() {
		return fromdate;
	}
	public void setFromdate(BigDecimal fromdate) {
		this.fromdate = fromdate;
	}
	public BigDecimal getTodate() {
		return todate;
	}
	public void setTodate(BigDecimal todate) {
		this.todate = todate;
	}
	public String getTdbtdesc() {
		return tdbtdesc;
	}
	public void setTdbtdesc(String tdbtdesc) {
		this.tdbtdesc = tdbtdesc;
	}
	public BigDecimal getTdbtrate() {
		return tdbtrate;
	}
	public void setTdbtrate(BigDecimal tdbtrate) {
		this.tdbtrate = tdbtrate;
	}
	public BigDecimal getTdbtamt() {
		return tdbtamt;
	}
	public void setTdbtamt(BigDecimal tdbtamt) {
		this.tdbtamt = tdbtamt;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
}