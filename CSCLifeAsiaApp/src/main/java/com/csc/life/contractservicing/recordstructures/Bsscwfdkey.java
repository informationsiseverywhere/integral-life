package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:00
 * Description:
 * Copybook name: BSSCWFDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bsscwfdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bsscwfdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData bsscwfdKey = new FixedLengthStringData(64).isAPartOf(bsscwfdFileKey, 0, REDEFINE);
  	public FixedLengthStringData bsscwfdScheduleName = new FixedLengthStringData(10).isAPartOf(bsscwfdKey, 0);
  	public PackedDecimalData bsscwfdScheduleNumber = new PackedDecimalData(8, 0).isAPartOf(bsscwfdKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(bsscwfdKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bsscwfdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bsscwfdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}