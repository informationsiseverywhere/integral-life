/*
 * File: Revploan.java
 * Date: 30 August 2009 2:10:48
 * Author: Quipoz Limited
 * 
 * Class transformed from REVPLOAN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*         REVERSE POLICY LOAN REGISTRATION
*         --------------------------------
*
* This subroutine is called to provide reversal of a Loan taken
*  out against a contract.
* The subroutine requires an entry on T6661.
*
* The subroutine will read each ACMVREV associated with the Loan
*  we want to Reverse, multiply the amount on the ACMVREV by -1
*  and write another ACMV with the new value - thus cancelling
*  out the posting.
* Having done the 'Reverse Postings' for the Loan, the subroutine
*  will then delete the LOAN record from the LOANPF file.
*
* TABLES USED
* -----------
*
*  T1688  -  Transaction Codes
*
*****************************************************************
* </pre>
*/
public class Revploan extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVPLOAN";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLoanno = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* FORMATS */
	private static final String acmvrevrec = "ACMVREVSKM";
	private static final String descrec = "DESCREC   ";
	private static final String loanrec = "LOANREC   ";
	private static final String arcmrec = "ARCMREC";
		/* TABLES */
	private static final String t1688 = "T1688";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Reverserec reverserec = new Reverserec();

	public Revploan() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		reverserec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaRldgacct.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setBatctrcde(SPACES);
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvs2000();
		}
		
		processAcmvOptical2010();
		deleteLoanRecord4000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void reverseAcmvs2000()
	{
			start2000();
		}

protected void start2000()
	{
		/* Read through the ACMVs until we find the one we want to reverse*/
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall,"CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			databaseError9500();
		}
		if (isNE(acmvrevIO.getRldgcoy(), reverserec.company)
		|| isNE(acmvrevIO.getRdocnum(),reverserec.chdrnum)
		|| isNE(acmvrevIO.getTranno(),reverserec.tranno)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			acmvrevIO.setFunction(varcom.nextr);
			return ;
		}
		/* Post opposite ACMV record.*/
		postRevAcmvs3000();
		/*  get the Loan number so we can delete the Loan record*/
		wsaaRldgacct.set(acmvrevIO.getRldgacct());
		acmvrevIO.setFunction(varcom.nextr);
	}

protected void processAcmvOptical2010()
	{
		start2010();
	}

protected void start2010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError9500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs2000();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError9500();
			}
		}
	}

protected void postRevAcmvs3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Now post the reversal ACMV record*/
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(reverserec.company);
		lifacmvrec.rldgcoy.set(reverserec.company);
		lifacmvrec.genlcoy.set(reverserec.company);
		lifacmvrec.batcactyr.set(reverserec.batcactyr);
		lifacmvrec.batctrcde.set(reverserec.batctrcde);
		lifacmvrec.batcactmn.set(reverserec.batcactmn);
		lifacmvrec.batcbatch.set(reverserec.batcbatch);
		lifacmvrec.batcbrn.set(reverserec.batcbrn);
		lifacmvrec.rdocnum.set(reverserec.chdrnum);
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(),-1));
		/* MULTIPLY ACMVREV-RCAMT      BY -1 GIVING LIFA-RCAMT.         */
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(reverserec.company);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		descIO.setDescitem(acmvrevIO.getTranref());
		getDescription3100();
		/* MOVE REVE-EFFDATE-1         TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(acmvrevIO.getTermid());
		lifacmvrec.user.set(acmvrevIO.getUser());
		/* MOVE ACMVREV-TRANSACTION-DATE TO LIFA-TRANSACTION-TIME.      */
		/* MOVE ACMVREV-TRANSACTION-TIME TO LIFA-TRANSACTION-DATE.      */
		lifacmvrec.transactionDate.set(acmvrevIO.getTransactionDate());
		lifacmvrec.transactionTime.set(acmvrevIO.getTransactionTime());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError9500();
		}
	}

protected void getDescription3100()
	{
		start3100();
	}

protected void start3100()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setLanguage(reverserec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(descIO.getLongdesc());
		}
	}

protected void deleteLoanRecord4000()
	{
		start4000();
	}

protected void start4000()
	{
		loanIO.setDataArea(SPACES);
		loanIO.setChdrcoy(reverserec.company);
		loanIO.setChdrnum(reverserec.chdrnum);
		loanIO.setLoanNumber(wsaaRldgLoanno);
		loanIO.setFunction(varcom.readh);
		loanIO.setFormat(loanrec);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			databaseError9500();
		}
		/* found LOAN record so now delete it*/
		loanIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			databaseError9500();
		}
	}

protected void systemError9000()
	{
			start9000();
			exit9490();
		}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
			start9500();
			exit9990();
		}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
