package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CvuwpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:37
 * Class transformed from CVUWPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CvuwpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 65;
	public FixedLengthStringData cvuwrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData cvuwpfRecord = cvuwrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(cvuwrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(cvuwrec);
	public PackedDecimalData huwdcdte = DD.huwdcdte.copy().isAPartOf(cvuwrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(cvuwrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(cvuwrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(cvuwrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(cvuwrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CvuwpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CvuwpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CvuwpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CvuwpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CvuwpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CvuwpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CvuwpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CVUWPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"HUWDCDTE, " +
							"EFFDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     huwdcdte,
                                     effdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		huwdcdte.clear();
  		effdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCvuwrec() {
  		return cvuwrec;
	}

	public FixedLengthStringData getCvuwpfRecord() {
  		return cvuwpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCvuwrec(what);
	}

	public void setCvuwrec(Object what) {
  		this.cvuwrec.set(what);
	}

	public void setCvuwpfRecord(Object what) {
  		this.cvuwpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(cvuwrec.getLength());
		result.set(cvuwrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}