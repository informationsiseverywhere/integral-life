/*
 * File: Zrulrviss.java
 * Date: 30 August 2009 2:57:28
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRULRVISS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.life.contractservicing.dataaccess.IncirevTableDAM;
import com.csc.life.contractservicing.dataaccess.UlnkrevTableDAM;
import com.csc.life.contractservicing.dataaccess.UnltrevTableDAM;
import com.csc.life.contractservicing.dataaccess.ZrutnrvTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitdrevTableDAM;
import com.csc.life.interestbearing.dataaccess.Hitrrv1TableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* Generalised subroutine for reversal of INCIs, ULNKs, UTRNs & HITRs
* ------------------------------------------------------------------
* This subroutine is identical to GRVULISS except when
*  reversing UTRN's. The logical file ZRUTNRV is used as this
*  distinguishs UTRN's at component level and makes use of the
*  Processing Sequence Number to identify units allocated to
*  either a Coverage or Rider.
*
* Overview.
* ---------
*
* This subroutine is called via the generic processing table,
*  T5671, it is used when performing a 'Cancel from Inception'
*  or an 'Alter from Inception' on a Unit-linked contract. Its
*  purpose is to delete all INCI records associated with the
*  contract being processed, to Reverse all ULNK records,
*  using the earliest unique ULNKs to produce corresponding
*  UNLTs, then deleting all the ULNKs and to reverse all
*  processed UTRNs and delete any unprocessed UTRNs.
*
* For interest bearing funds, delate all HITD records.  To
*  reverse all processed HITRs and delete any unprocessed
*  HITRs.
*
* Processing.
* -----------
*
* This subroutine is called via an entry in T5671.
*  All parameters required by this subroutine are passed in
*   the GREVERSREC copybook.
* NOTE.
*    The records to be processed should be specified right down
*     to PLAN-SUFFIX level in the Linkage passed to this
*     subroutine.
*
***********************************************************************
* </pre>
*/
public class Zrulrviss extends COBOLConvCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zrulrviss.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "ZRULRVISS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaDoneFirstUlnk = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaSeqNo = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaSeqnbr = new PackedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4);
		/* WSAA-RIDER */
	private FixedLengthStringData wsaaRiderAlpha = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaRiderNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiderAlpha, 0, REDEFINE).setUnsigned();
	private FixedLengthStringData wsaaFund = new FixedLengthStringData(4);
	private String wsaaHitrWritten = "";
		/* ERRORS */
	private String f018 = "F018";
	private String h965 = "H965";
		/* TABLES */
	private String t6647 = "T6647";
		/* FORMATS */
	private String chdrenqrec = "CHDRENQREC";
	private String incirevrec = "INCIREVREC";
	private String ulnkrevrec = "ULNKREVREC";
	private String unltrevrec = "UNLTREVREC";
	private String zrutnrvrec = "ZRUTNRVREC";
	private String hitrrv1rec = "HITRRV1REC";
	private String hitdrevrec = "HITDREVREC";
	private String hitdrec = "HITDREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
		/*Interestr Bearing Fund Interest Details*/
	private HitdTableDAM hitdIO = new HitdTableDAM();
		/*I/B fund interest details Reversal*/
	private HitdrevTableDAM hitdrevIO = new HitdrevTableDAM();
		/*I/B Fund Transaction Reversals*/
	private Hitrrv1TableDAM hitrrv1IO = new Hitrrv1TableDAM();
		/*Individual Increase Details*/
	private IncirevTableDAM incirevIO = new IncirevTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Contract Enquiry - Transactions File.*/
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6647rec t6647rec = new T6647rec();
		/*CFI/AFI/Reversals view*/
	private UlnkrevTableDAM ulnkrevIO = new UlnkrevTableDAM();
		/*CFI/AFI/Reversals view*/
	private UnltrevTableDAM unltrevIO = new UnltrevTableDAM();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
		/*U/L Component Transaction Reversal*/
	private ZrutnrvTableDAM zrutnrvIO = new ZrutnrvTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit3190, 
		exit4190, 
		a190Exit, 
		a490Exit, 
		exit9490, 
		exit9990
	}

	public Zrulrviss() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		greversrec.statuz.set(varcom.oK);
		wsaaSub.set(ZERO);
		wsaaDoneFirstUlnk.set(SPACES);
		wsaaSeqnbr.add(1);
		processIncis2000();
		processUlnks3000();
		processUtrns4000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processIncis2000()
	{
		start2000();
	}

protected void start2000()
	{
		incirevIO.setParams(SPACES);
		incirevIO.setChdrcoy(greversrec.chdrcoy);
		incirevIO.setChdrnum(greversrec.chdrnum);
		incirevIO.setLife(greversrec.life);
		incirevIO.setCoverage(greversrec.coverage);
		incirevIO.setRider(greversrec.rider);
		incirevIO.setPlanSuffix(greversrec.planSuffix);
		incirevIO.setTranno(ZERO);
		incirevIO.setFormat(incirevrec);
		incirevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incirevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incirevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(incirevIO.getStatuz(),varcom.endp))) {
			deleteInci2100();
		}
		
	}

protected void deleteInci2100()
	{
		try {
			start2100();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void start2100()
	{
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)
		&& isNE(incirevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(incirevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		if (isNE(greversrec.chdrcoy,incirevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,incirevIO.getChdrnum())
		|| isNE(greversrec.life,incirevIO.getLife())
		|| isNE(greversrec.coverage,incirevIO.getCoverage())
		|| isNE(greversrec.rider,incirevIO.getRider())
		|| isNE(greversrec.planSuffix,incirevIO.getPlanSuffix())) {
			incirevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2190);
		}
		incirevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			systemError9000();
		}
		incirevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			systemError9000();
		}
		incirevIO.setFunction(varcom.nextr);
	}

protected void processUlnks3000()
	{
		start3000();
	}

protected void start3000()
	{
		ulnkrevIO.setParams(SPACES);
		ulnkrevIO.setChdrcoy(greversrec.chdrcoy);
		ulnkrevIO.setChdrnum(greversrec.chdrnum);
		ulnkrevIO.setLife(greversrec.life);
		ulnkrevIO.setCoverage(greversrec.coverage);
		ulnkrevIO.setRider(greversrec.rider);
		ulnkrevIO.setPlanSuffix(greversrec.planSuffix);
		ulnkrevIO.setTranno(ZERO);
		ulnkrevIO.setFormat(ulnkrevrec);
		ulnkrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ulnkrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ulnkrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(ulnkrevIO.getStatuz(),varcom.endp))) {
			ulnkLoop3100();
		}
		
	}

protected void ulnkLoop3100()
	{
		try {
			start3100();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void start3100()
	{
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)
		&& isNE(ulnkrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(ulnkrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3190);
		}
		if (isNE(greversrec.chdrcoy,ulnkrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,ulnkrevIO.getChdrnum())
		|| isNE(greversrec.life,ulnkrevIO.getLife())
		|| isNE(greversrec.coverage,ulnkrevIO.getCoverage())
		|| isNE(greversrec.rider,ulnkrevIO.getRider())
		|| isNE(greversrec.planSuffix,ulnkrevIO.getPlanSuffix())) {
			ulnkrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(wsaaDoneFirstUlnk,SPACES)) {
			if (isLTE(ulnkrevIO.getTranno(),greversrec.tranno)) {
				createUnlt3200();
			}
			wsaaDoneFirstUlnk.set("Y");
		}
		ulnkrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			systemError9000();
		}
		ulnkrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ulnkrevIO);
		if (isNE(ulnkrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ulnkrevIO.getParams());
			syserrrec.statuz.set(ulnkrevIO.getStatuz());
			systemError9000();
		}
		ulnkrevIO.setFunction(varcom.nextr);
	}

protected void createUnlt3200()
	{
		start3200();
	}

protected void start3200()
	{
		unltrevIO.setParams(SPACES);
		unltrevIO.setFormat(unltrevrec);
		unltrevIO.setFunction(varcom.writr);
		unltrevIO.setChdrcoy(ulnkrevIO.getChdrcoy());
		unltrevIO.setChdrnum(ulnkrevIO.getChdrnum());
		unltrevIO.setLife(ulnkrevIO.getLife());
		unltrevIO.setCoverage(ulnkrevIO.getCoverage());
		unltrevIO.setRider(ulnkrevIO.getRider());
		unltrevIO.setSeqnbr(wsaaSeqnbr);
		unltrevIO.setCurrfrom(ulnkrevIO.getCurrfrom());
		unltrevIO.setCurrto(varcom.vrcmMaxDate);
		unltrevIO.setTranno(greversrec.newTranno);
		unltrevIO.setPremTopupInd(SPACES);
		unltrevIO.setValidflag("1");
		unltrevIO.setPercOrAmntInd(ulnkrevIO.getPercOrAmntInd());
		if (isEQ(ulnkrevIO.getPlanSuffix(),ZERO)) {
			getPolsum3300();
			if (isGT(chdrenqIO.getPolinc(),1)) {
				unltrevIO.setNumapp(chdrenqIO.getPolsum());
			}
			else {
				unltrevIO.setNumapp(1);
			}
		}
		else {
			unltrevIO.setNumapp(1);
		}
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			unltrevIO.setUalfnd(wsaaSub, ulnkrevIO.getUalfnd(wsaaSub));
			unltrevIO.setUalprc(wsaaSub, ulnkrevIO.getUalprc(wsaaSub));
			unltrevIO.setUspcpr(wsaaSub, ulnkrevIO.getUspcpr(wsaaSub));
		}
		SmartFileCode.execute(appVars, unltrevIO);
		if (isNE(unltrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(unltrevIO.getParams());
			syserrrec.statuz.set(unltrevIO.getStatuz());
			systemError9000();
		}
	}

protected void getPolsum3300()
	{
		/*START*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setChdrcoy(ulnkrevIO.getChdrcoy());
		chdrenqIO.setChdrnum(ulnkrevIO.getChdrnum());
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.params.set(chdrenqIO.getStatuz());
			systemError9000();
		}
		/*EXIT*/
	}

protected void processUtrns4000()
	{
		start4000();
	}

protected void start4000()
	{
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(greversrec.chdrcoy);
		chdrenqIO.setChdrnum(greversrec.chdrnum);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			systemError9000();
		}
		ptrnenqIO.setDataArea(SPACES);
		ptrnenqIO.setChdrcoy(greversrec.chdrcoy);
		ptrnenqIO.setChdrnum(greversrec.chdrnum);
		ptrnenqIO.setTranno(greversrec.tranno);
		ptrnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		SmartFileCode.execute(appVars, ptrnenqIO);
		if ((isNE(ptrnenqIO.getStatuz(),varcom.oK))
		&& (isNE(ptrnenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ptrnenqIO.getParams());
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			systemError9000();
		}
		if ((isNE(ptrnenqIO.getChdrcoy(),greversrec.chdrcoy))
		|| (isNE(ptrnenqIO.getChdrnum(),greversrec.chdrnum))
		|| (isNE(ptrnenqIO.getTranno(),greversrec.tranno))
		|| (isEQ(ptrnenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(ptrnenqIO.getParams());
			syserrrec.statuz.set(f018);
			systemError9000();
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(greversrec.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaBatctrcde.set(ptrnenqIO.getBatctrcde());
		wsaaCnttype.set(chdrenqIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(ptrnenqIO.getPtrneff());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getItemcoy(),greversrec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(),t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h965);
			systemError9000();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		wsaaRiderAlpha.set(greversrec.rider);
		compute(wsaaSeqNo, 0).set(add(wsaaRiderNum,t6647rec.procSeqNo));
		zrutnrvIO.setParams(SPACES);
		zrutnrvIO.setChdrcoy(greversrec.chdrcoy);
		zrutnrvIO.setChdrnum(greversrec.chdrnum);
		zrutnrvIO.setLife(greversrec.life);
		zrutnrvIO.setCoverage(greversrec.coverage);
		zrutnrvIO.setPlanSuffix(ZERO);
		zrutnrvIO.setTranno(greversrec.tranno);
		zrutnrvIO.setProcSeqNo(wsaaSeqNo);
		zrutnrvIO.setUnitVirtualFund(SPACES);
		zrutnrvIO.setUnitType(SPACES);
		zrutnrvIO.setFunction(varcom.begn);
		zrutnrvIO.setFormat(zrutnrvrec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError9000();
		}
		wsaaBatckey.set(greversrec.batckey);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(greversrec.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(chdrenqIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getItemcoy(),greversrec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(),t6647))
		|| (isNE(itdmIO.getItemitem(),wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h965);
			systemError9000();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		while ( !(isEQ(zrutnrvIO.getStatuz(),varcom.endp))) {
			reverseUtrns4100();
		}
		
		hitrrv1IO.setParams(SPACES);
		hitrrv1IO.setChdrcoy(greversrec.chdrcoy);
		hitrrv1IO.setChdrnum(greversrec.chdrnum);
		hitrrv1IO.setLife(greversrec.life);
		hitrrv1IO.setCoverage(greversrec.coverage);
		hitrrv1IO.setPlanSuffix(ZERO);
		hitrrv1IO.setTranno(greversrec.tranno);
		hitrrv1IO.setProcSeqNo(wsaaSeqNo);
		hitrrv1IO.setZintbfnd(SPACES);
		hitrrv1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrrv1IO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrrv1IO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "TRANNO", "PRCSEQ");
		hitrrv1IO.setFormat(hitrrv1rec);
		wsaaFund.set(SPACES);
		while ( !(isEQ(hitrrv1IO.getStatuz(),varcom.endp))) {
			a100ReverseHitrs();
		}
		
		hitdrevIO.setParams(SPACES);
		hitdrevIO.setChdrcoy(greversrec.chdrcoy);
		hitdrevIO.setChdrnum(greversrec.chdrnum);
		hitdrevIO.setLife(greversrec.life);
		hitdrevIO.setCoverage(greversrec.coverage);
		hitdrevIO.setRider(greversrec.rider);
		hitdrevIO.setPlanSuffix(greversrec.planSuffix);
		hitdrevIO.setTranno(greversrec.tranno);
		hitdrevIO.setFormat(hitdrevrec);
		hitdrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitdrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(hitdrevIO.getStatuz(),varcom.endp))) {
			a400ReverseHitds();
		}
		
	}

protected void reverseUtrns4100()
	{
		try {
			start4100();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void start4100()
	{
		SmartFileCode.execute(appVars, zrutnrvIO);
		if ((isNE(zrutnrvIO.getStatuz(),varcom.oK))
		&& (isNE(zrutnrvIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			databaseError9500();
		}
		if ((isNE(greversrec.chdrcoy,zrutnrvIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,zrutnrvIO.getChdrnum()))
		|| (isNE(greversrec.life,zrutnrvIO.getLife()))
		|| (isNE(greversrec.coverage,zrutnrvIO.getCoverage()))
		|| (isNE(greversrec.tranno,zrutnrvIO.getTranno()))
		|| (isNE(wsaaSeqNo,zrutnrvIO.getProcSeqNo()))
		|| (isEQ(zrutnrvIO.getStatuz(),varcom.endp))) {
			zrutnrvIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit4190);
		}
		deleteRewriteUtrn4200();
		zrutnrvIO.setFunction(varcom.nextr);
	}

protected void deleteRewriteUtrn4200()
	{
		start4200();
	}

protected void start4200()
	{
		if (isEQ(zrutnrvIO.getFeedbackInd(),SPACES)) {
			zrutnrvIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, zrutnrvIO);
			if (isNE(zrutnrvIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(zrutnrvIO.getParams());
				syserrrec.statuz.set(zrutnrvIO.getStatuz());
				databaseError9500();
			}
			zrutnrvIO.setFunction(varcom.delet);
		}
		else {
			zrutnrvIO.setFeedbackInd(SPACES);
			zrutnrvIO.setTriggerModule(SPACES);
			zrutnrvIO.setTriggerKey(SPACES);
			zrutnrvIO.setSurrenderPercent(ZERO);
			setPrecision(zrutnrvIO.getContractAmount(), 2);
			zrutnrvIO.setContractAmount(mult(zrutnrvIO.getContractAmount(),-1));
			setPrecision(zrutnrvIO.getFundAmount(), 2);
			zrutnrvIO.setFundAmount(mult(zrutnrvIO.getFundAmount(),-1));
			setPrecision(zrutnrvIO.getNofDunits(), 5);
			zrutnrvIO.setNofDunits(mult(zrutnrvIO.getNofDunits(),-1));
			setPrecision(zrutnrvIO.getNofUnits(), 5);
			zrutnrvIO.setNofUnits(mult(zrutnrvIO.getNofUnits(),-1));
			setPrecision(zrutnrvIO.getInciprm01(), 2);
			zrutnrvIO.setInciprm01(mult(zrutnrvIO.getInciprm01(),-1));
			setPrecision(zrutnrvIO.getInciprm02(), 2);
			zrutnrvIO.setInciprm02(mult(zrutnrvIO.getInciprm02(),-1));
			zrutnrvIO.setProcSeqNo(t6647rec.procSeqNo);
			zrutnrvIO.setTransactionDate(greversrec.transDate);
			zrutnrvIO.setTransactionTime(greversrec.transTime);
			zrutnrvIO.setTranno(greversrec.newTranno);
			zrutnrvIO.setTermid(greversrec.termid);
			zrutnrvIO.setUser(greversrec.user);
			zrutnrvIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			zrutnrvIO.setBatccoy(wsaaBatckey.batcBatccoy);
			zrutnrvIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			zrutnrvIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			zrutnrvIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			zrutnrvIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			zrutnrvIO.setUstmno(ZERO);
			zrutnrvIO.setFunction(varcom.writr);
		}
		SmartFileCode.execute(appVars, zrutnrvIO);
		if (isNE(zrutnrvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zrutnrvIO.getParams());
			syserrrec.statuz.set(zrutnrvIO.getStatuz());
			databaseError9500();
		}
	}

protected void a100ReverseHitrs()
	{
		try {
			a110Start();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void a110Start()
	{
		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)
		&& isNE(hitrrv1IO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			databaseError9500();
		}
		if (isNE(greversrec.chdrcoy,hitrrv1IO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitrrv1IO.getChdrnum())
		|| isNE(greversrec.life,hitrrv1IO.getLife())
		|| isNE(greversrec.coverage,hitrrv1IO.getCoverage())
		|| isNE(greversrec.tranno,hitrrv1IO.getTranno())
		|| isNE(wsaaSeqNo,hitrrv1IO.getProcSeqNo())
		|| isEQ(hitrrv1IO.getStatuz(),varcom.endp)) {
			hitrrv1IO.setStatuz(varcom.endp);
			goTo(GotoLabel.a190Exit);
		}
		if (isNE(hitrrv1IO.getZintbfnd(),wsaaFund)) {
			wsaaFund.set(hitrrv1IO.getZintbfnd());
		}
		a200DeleteRewriteHitr();
		hitrrv1IO.setFunction(varcom.nextr);
	}

protected void a200DeleteRewriteHitr()
	{
		a200Start();
	}

protected void a200Start()
	{
		if (isEQ(hitrrv1IO.getFeedbackInd(),SPACES)) {
			hitrrv1IO.setFunction(varcom.deltd);
		}
		else {
			hitrrv1IO.setFeedbackInd(SPACES);
			hitrrv1IO.setTriggerModule(SPACES);
			hitrrv1IO.setTriggerKey(SPACES);
			hitrrv1IO.setSurrenderPercent(ZERO);
			setPrecision(hitrrv1IO.getContractAmount(), 2);
			hitrrv1IO.setContractAmount(mult(hitrrv1IO.getContractAmount(),-1));
			setPrecision(hitrrv1IO.getFundAmount(), 2);
			hitrrv1IO.setFundAmount(mult(hitrrv1IO.getFundAmount(),-1));
			setPrecision(hitrrv1IO.getInciprm01(), 2);
			hitrrv1IO.setInciprm01(mult(hitrrv1IO.getInciprm01(),-1));
			setPrecision(hitrrv1IO.getInciprm02(), 2);
			hitrrv1IO.setInciprm02(mult(hitrrv1IO.getInciprm02(),-1));
			setPrecision(hitrrv1IO.getProcSeqNo(), 0);
			hitrrv1IO.setProcSeqNo(mult(hitrrv1IO.getProcSeqNo(),-1));
			hitrrv1IO.setTranno(greversrec.newTranno);
			hitrrv1IO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			hitrrv1IO.setBatccoy(wsaaBatckey.batcBatccoy);
			hitrrv1IO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			hitrrv1IO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			hitrrv1IO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			hitrrv1IO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			hitrrv1IO.setUstmno(ZERO);
			hitrrv1IO.setEffdate(datcon1rec.intDate);
			hitrrv1IO.setContractAmount(0);
			hitrrv1IO.setFundAmount(0);
			hitrrv1IO.setFundRate(0);
			hitrrv1IO.setInciprm01(0);
			hitrrv1IO.setInciprm02(0);
			hitrrv1IO.setInciNum(0);
			hitrrv1IO.setInciPerd01(0);
			hitrrv1IO.setInciPerd02(0);
			hitrrv1IO.setSurrenderPercent(100);
			hitrrv1IO.setSvp(1);
			hitrrv1IO.setProcSeqNo(t6647rec.procSeqNo);
			hitrrv1IO.setFunction(varcom.writr);
		}
		hitrrv1IO.setFormat(hitrrv1rec);
		SmartFileCode.execute(appVars, hitrrv1IO);
		if (isNE(hitrrv1IO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrrv1IO.getParams());
			databaseError9500();
		}
	}

protected void a400ReverseHitds()
	{
		try {
			a410Hitd();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void a410Hitd()
	{
		SmartFileCode.execute(appVars, hitdrevIO);
		if (isNE(hitdrevIO.getStatuz(),varcom.oK)
		&& isNE(hitdrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitdrevIO.getParams());
			databaseError9500();
		}
		if (isNE(greversrec.chdrcoy,hitdrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitdrevIO.getChdrnum())
		|| isNE(greversrec.life,hitdrevIO.getLife())
		|| isNE(greversrec.coverage,hitdrevIO.getCoverage())
		|| isNE(greversrec.rider,hitdrevIO.getRider())
		|| isNE(greversrec.planSuffix,hitdrevIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hitdrevIO.getTranno())
		|| isEQ(hitdrevIO.getStatuz(),varcom.endp)) {
			hitdrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a490Exit);
		}
		hitdIO.setParams(SPACES);
		hitdIO.setRrn(hitdrevIO.getRrn());
		hitdIO.setFormat(hitdrec);
		hitdIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			databaseError9500();
		}
		hitdIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			databaseError9500();
		}
		hitdrevIO.setFunction(varcom.nextr);
	}

protected void systemError9000()
	{
		try {
			start9000();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
		finally{
			exit9490();
		}
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		try {
			start9500();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
		finally{
			exit9990();
		}
	}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9990);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}
}
