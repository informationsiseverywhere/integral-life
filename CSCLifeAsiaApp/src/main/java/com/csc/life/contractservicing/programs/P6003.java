/*
 * File: P6003.java
 * Date: 30 August 2009 0:36:18
 * Author: Quipoz Limited
 * 
 * Class transformed from P6003.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.screens.S6003ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*              REGULAR PAYMENT REVERSALS.
*              --------------------------
* This program is used to display the details of the Contract
*  that is going to have a Regular Payment Reversed.
*
* Initialisation.
* ---------------
*
* Read the Contract header (CHDRCLM) using the RETRV function to get
*  the relevant data necessary for obtaining the status description
*  and the agent details.
*
* Obtain the necessary descriptions for Contract Type, Contract
*  Risk Status, Contract Premium Status and Transaction Code (for
*  the screen title) by calling 'DESCIO'for the respective tables
*  and then display these descriptions on the screen S6003.
*
* Client name
*
*     Read the Client Details file to get the clients name
*
* Paid-to date
*
*     Read the PAYR file to get the Paid-to date
*          and the Billed-to Date
*
* Payer name
*
*     Read the Client Role file(CLRF) to get the client number of
*          the payer, then read the Client details file with
*          payer client number to get payer name
*
* Agent details
*
*     In order to read the agent file use the agent details
*          from the contract header (READR on the AGLFLNB file).
*
* Last 'Reversible Transaction'
* We must check the PTRN file to make sure that the most
* 'significant transaction' is a Regular Payment. If there
* are any other PTRNS which are more recent than the Regular
* Payment we are trying to reverse, then the user must
* reverse those transactions first before attempting a
* Regular Payment Reversal.
*
* Read PTRN file in reverse order, using PTRNREV logical view
* Now check if the PTRN record read is 'significant' and
* check if it is a Regular Payment. If the transaction is
* not 'significant' (e.g. a Minor Alteration) then read the
* next most recent PTRN and perform the same checks on it.
* Repeat this until a 'significant' PTRN is found. Check by
* looking in T6661, the reversals transaction table.
*
* However, whatever the last PTRN is, it should be displayed
*  on the screen S6003 so that if the transaction is not a
*  Regular Payment, the user can see what it is and then decide
*  to exit and take whatever action is necessary.
*
* Validation.
* -----------
*
* If the user pressed 'KILL', don't continue with rest of validation
*  and exit from program.
*
* If the user pressed Refresh, validate any data input on the
*  screen by the user.
*
*
* Updating.
* ---------
*
* If the user pressed 'KILL', don't continue with rest of section
*  and exit from program.
*
* Get this far, so the last transaction is a Regular Payment....
*
* Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
* Call the AT submission module to submit REVGENAT, passing the
*  contract number as the 'primary key', this performs the
*  Regular Payment Reversal.
*
*
*
* Tables Used.
* ------------
*
* T1688 - Transaction codes
* T3588 - Contract Premium Statii
* T3623 - Contract Risk Statii
* T5688 - Contract Structure
* T6661 - Transaction Reversal Parameters
*
*
*****************************************************************
* </pre>
*/
public class P6003 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6003");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaFwdRgpyCode = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaContinue = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(28).isAPartOf(wsaaPrimaryKey, 8, FILLER).init(SPACES);

	private FixedLengthStringData wsaaPayrKey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaPayrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrKey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrKey, 8);

	private FixedLengthStringData wsaaTransactionArea = new FixedLengthStringData(200);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransactionArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransactionArea, 45).setUnsigned();
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransactionArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransactionArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 62).setUnsigned();
	private FixedLengthStringData filler1 = new FixedLengthStringData(130).isAPartOf(wsaaTransactionArea, 70, FILLER).init(SPACES);
		/* ERRORS */
	private String f006 = "F006";
	private String f910 = "F910";
		/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String t6661 = "T6661";
	private String clrfrec = "CLRFREC   ";
	private String itemrec = "ITEMREC   ";
	private String payrrec = "PAYRREC   ";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Joind FSU and Life agent headers - new b*/
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private Atreqrec atreqrec = new Atreqrec();
		/*Claims Contract Header*/
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
		/*Client Roles by Foreign Key*/
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T6661rec t6661rec = new T6661rec();
	private Batckey wsaaBatckey = new Batckey();
	private S6003ScreenVars sv = ScreenProgram.getScreenVars( S6003ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		getDescription1580, 
		getNextPtrnRecord1850, 
		exit1890, 
		preExit, 
		exit2090, 
		exit2190, 
		getNextPtrnRecord2250, 
		exit2290, 
		exit3090
	}

	public P6003() {
		super();
		screenVars = sv;
		new ScreenModel("S6003", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End 
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaItem.set(SPACES);
		wsaaContinue.set(SPACES);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		chdrclmIO.setParams(SPACES);
		chdrclmIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(chdrclmIO.getChdrnum());
		sv.cnttype.set(chdrclmIO.getCnttype());
		sv.cntcurr.set(chdrclmIO.getCntcurr());
		sv.cownnum.set(chdrclmIO.getCownnum());
		sv.payrnum.set(chdrclmIO.getPayrnum());
		sv.agntnum.set(chdrclmIO.getAgntnum());
		sv.occdate.set(chdrclmIO.getOccdate());
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrclmIO.getChdrcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaFwdRgpyCode.set(t6661rec.trcode);
		clientDetails1100();
		agentDetails1200();
		payerDetails1300();
		tableDetails1400();
		checkLastPtrns1500();
		if (isNE(wsaaContinue,"Y")) {
			sv.batctrcdeErr.set(f006);
			wsspcomn.edterror.set("Y");
		}
	}

protected void clientDetails1100()
	{
		/*START*/
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(chdrclmIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		clientIo1600();
		sv.ownername.set(wsspcomn.longconfname);
		/*EXIT*/
	}

protected void agentDetails1200()
	{
		start1200();
	}

protected void start1200()
	{
		aglflnbIO.setParams(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(chdrclmIO.getAgntnum());
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(aglflnbIO.getStatuz(),varcom.mrnf)) {
			sv.agentname.set(SPACES);
		}
		else {
			cltsIO.setParams(SPACES);
			cltsIO.setClntnum(aglflnbIO.getClntnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			clientIo1600();
			sv.agentname.set(wsspcomn.longconfname);
		}
	}

protected void payerDetails1300()
	{
		start1300();
	}

protected void start1300()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrnum(chdrclmIO.getChdrnum());
		payrIO.setChdrcoy(chdrclmIO.getChdrcoy());
		payrIO.setPayrseqno(ZERO);
		payrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrclmIO.getChdrnum(),payrIO.getChdrnum())
		|| isNE(chdrclmIO.getChdrcoy(),payrIO.getChdrcoy())
		|| isEQ(payrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		sv.btdate.set(payrIO.getBtdate());
		sv.ptdate.set(payrIO.getPtdate());
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx(chdrclmIO.getChdrpfx());
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaPayrChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaPayrKey);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		clrfIO.setFormat(clrfrec);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(clrfIO.getClntnum());
		cltsIO.setClntcoy(clrfIO.getClntcoy());
		cltsIO.setClntpfx(clrfIO.getClntpfx());
		clientIo1600();
		sv.payrnum.set(clrfIO.getClntnum());
		sv.payorname.set(wsspcomn.longconfname);
	}

protected void tableDetails1400()
	{
		start1400();
	}

protected void start1400()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t5688);
		wsaaItem.set(chdrclmIO.getCnttype());
		getDescriptions1700();
		sv.ctypdesc.set(descIO.getLongdesc());
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3623);
		wsaaItem.set(chdrclmIO.getStatcode());
		getDescriptions1700();
		sv.rstate.set(descIO.getShortdesc());
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3588);
		wsaaItem.set(chdrclmIO.getPstatcode());
		getDescriptions1700();
		sv.pstate.set(descIO.getShortdesc());
	}

protected void checkLastPtrns1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1500();
				}
				case getDescription1580: {
					getDescription1580();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1500()
	{
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrclmIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrclmIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrclmIO.getChdrnum(),ptrnrevIO.getChdrnum())
		|| isNE(chdrclmIO.getChdrcoy(),ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(ptrnrevIO.getBatctrcde(),wsaaFwdRgpyCode)) {
			wsaaContinue.set("Y");
			goTo(GotoLabel.getDescription1580);
		}
		wsaaContinue.set(SPACES);
		while ( !(isNE(wsaaContinue,SPACES)) && isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			ptrnLoop1800();
		}
		
	}

protected void getDescription1580()
	{
		sv.batctrcde.set(ptrnrevIO.getBatctrcde());
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		wsaaItem.set(ptrnrevIO.getBatctrcde());
		getDescriptions1700();
		sv.longdesc.set(descIO.getLongdesc());
		/*EXIT*/
	}

protected void clientIo1600()
	{
		/*START*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			wsspcomn.longconfname.set(SPACES);
		}
		else {
			plainname();
		}
		/*EXIT*/
	}

protected void getDescriptions1700()
	{
		start1700();
	}

protected void start1700()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaItem);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setDescitem(wsaaItem);
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
	}

protected void ptrnLoop1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1800();
				}
				case getNextPtrnRecord1850: {
					getNextPtrnRecord1850();
				}
				case exit1890: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1800()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ptrnrevIO.getChdrcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setItemitem(ptrnrevIO.getBatctrcde());
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		if (isEQ(t6661rec.contRevFlag,"N")) {
			goTo(GotoLabel.getNextPtrnRecord1850);
		}
		if (isEQ(t6661rec.contRevFlag,"Y")
		|| isEQ(t6661rec.contRevFlag,SPACES)) {
			wsaaContinue.set("N");
			goTo(GotoLabel.exit1890);
		}
	}

protected void getNextPtrnRecord1850()
	{
		ptrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrclmIO.getChdrnum(),ptrnrevIO.getChdrnum())
		|| isNE(chdrclmIO.getChdrcoy(),ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(ptrnrevIO.getBatctrcde(),wsaaFwdRgpyCode)) {
			wsaaContinue.set("Y");
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			start2000();
		}
		catch (GOTOException e){
		}
	}

protected void start2000()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		revalidatePtrns2100();
		if (isNE(wsaaContinue,"Y")) {
			sv.batctrcdeErr.set(f006);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void revalidatePtrns2100()
	{
		try {
			start2100();
		}
		catch (GOTOException e){
		}
	}

protected void start2100()
	{
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrclmIO.getChdrcoy());
		ptrnrevIO.setChdrnum(sv.chdrnum);
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY");
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrclmIO.getChdrnum(),ptrnrevIO.getChdrnum())
		|| isNE(chdrclmIO.getChdrcoy(),ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(ptrnrevIO.getBatctrcde(),wsaaFwdRgpyCode)) {
			wsaaContinue.set("Y");
			goTo(GotoLabel.exit2190);
		}
		wsaaContinue.set(SPACES);
		while ( !(isNE(wsaaContinue,SPACES))) {
			ptrnLoop2200();
		}
		
		sv.batctrcde.set(ptrnrevIO.getBatctrcde());
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		wsaaItem.set(ptrnrevIO.getBatctrcde());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaItem);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setDescitem(wsaaItem);
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
		sv.longdesc.set(descIO.getLongdesc());
	}

protected void ptrnLoop2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2200();
				}
				case getNextPtrnRecord2250: {
					getNextPtrnRecord2250();
				}
				case exit2290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2200()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ptrnrevIO.getChdrcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setItemitem(ptrnrevIO.getBatctrcde());
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		if (isEQ(t6661rec.contRevFlag,"N")) {
			goTo(GotoLabel.getNextPtrnRecord2250);
		}
		if (isEQ(t6661rec.contRevFlag,"Y")
		|| isEQ(t6661rec.contRevFlag,SPACES)) {
			wsaaContinue.set("N");
			goTo(GotoLabel.exit2290);
		}
	}

protected void getNextPtrnRecord2250()
	{
		ptrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrclmIO.getChdrnum(),ptrnrevIO.getChdrnum())
		|| isNE(chdrclmIO.getChdrcoy(),ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(ptrnrevIO.getBatctrcde(),wsaaFwdRgpyCode)) {
			wsaaContinue.set("Y");
		}
	}

protected void update3000()
	{
		try {
			start3000();
		}
		catch (GOTOException e){
		}
	}

protected void start3000()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrclmIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaChdrnum.set(chdrclmIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTodate.set(wsaaToday);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTranDate.set(varcom.vrcmDate);
		wsaaTranTime.set(varcom.vrcmTime);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaSupflag.set("N");
		wsaaSuppressTo.set(varcom.vrcmMaxDate);
		wsaaPlnsfx.set(ZERO);
		wsaaTranNum.set(ptrnrevIO.getTranno());
		wsaaCfiafiTranCode.set(SPACES);
		wsaaCfiafiTranno.set(ZERO);
		wsaaBbldat.set(ZERO);
		atreqrec.transArea.set(wsaaTransactionArea);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*START*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
