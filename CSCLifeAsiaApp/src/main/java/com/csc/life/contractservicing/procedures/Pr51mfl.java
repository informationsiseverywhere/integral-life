/*
 * File: Pr51mfl.java
 * Date: 30 August 2009 1:38:00
 * Author: Quipoz Limited
 * 
 * Class transformed from PR51MFL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  This Subroutine is called from OPTSWCH table T1661.
*  It used to determine whether there is any Follow-Up records exist for
*  the component being enquired upon in PR51M.
*
*
*******************************************************************
*
* </pre>
*/
public class Pr51mfl extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PR51MFL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String fluprec = "FLUPREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Logical File for follow ups*/
	private FlupTableDAM flupIO = new FlupTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Pr51mfl() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		start010();
		exit090();
	}

protected void start010()
	{
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		flupIO.setParams(SPACES);
		flupIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		flupIO.setChdrnum(chdrmjaIO.getChdrnum());
		flupIO.setFupno(0);
		flupIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		flupIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		flupIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		flupIO.setFormat(fluprec);
		SmartFileCode.execute(appVars, flupIO);
		if (isNE(flupIO.getStatuz(),varcom.oK)
		&& isNE(flupIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(flupIO.getStatuz());
			syserrrec.params.set(flupIO.getParams());
			fatalError600();
		}
		if (isNE(flupIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(flupIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(flupIO.getStatuz(),varcom.endp)) {
			wsaaChkpStatuz.set("NFND");
		}
		else {
			wsaaChkpStatuz.set("DFND");
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
