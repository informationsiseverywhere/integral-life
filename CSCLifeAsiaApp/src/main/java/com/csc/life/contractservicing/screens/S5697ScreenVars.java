package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5697
 * @version 1.0 generated on 30/08/09 06:49
 * @author Quipoz
 */
public class S5697ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(505);
	public FixedLengthStringData dataFields = new FixedLengthStringData(297).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,156);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData sfcdesc = DD.sfcdesc.copy().isAPartOf(dataFields,177);
	public FixedLengthStringData confirm = DD.sfcdesd.copy().isAPartOf(dataFields,237);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 297);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData sfcdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData sfcdesdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 349);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] sfcdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] sfcdesdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(221);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(75).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,2);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(subfileFields,4);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData pstatdesc = DD.pstatdesc.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,40);
	public FixedLengthStringData rstatdesc = DD.rstatdesc.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,72);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(subfileFields,73);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 75);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData pstatdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData rstatdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData statcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 111);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] pstatdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] rstatdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] statcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 219);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5697screensflWritten = new LongData(0);
	public LongData S5697screenctlWritten = new LongData(0);
	public LongData S5697screenWritten = new LongData(0);
	public LongData S5697protectWritten = new LongData(0);
	public GeneralTable s5697screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5697screensfl;
	}

	public S5697ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01","04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(sfcdescOut,new String[] {null, null, null, "04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(sfcdesdOut,new String[] {null, null, null, "04",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {rider, coverage, life, select, planSuffix, statcode, rstatdesc, pstatcode, pstatdesc};
		screenSflOutFields = new BaseData[][] {riderOut, coverageOut, lifeOut, selectOut, plnsfxOut, statcodeOut, rstatdescOut, pstatcodeOut, pstatdescOut};
		screenSflErrFields = new BaseData[] {riderErr, coverageErr, lifeErr, selectErr, plnsfxErr, statcodeErr, rstatdescErr, pstatcodeErr, pstatdescErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, sfcdesc, confirm};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, sfcdescOut, sfcdesdOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, sfcdescErr, sfcdesdErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5697screen.class;
		screenSflRecord = S5697screensfl.class;
		screenCtlRecord = S5697screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5697protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5697screenctl.lrec.pageSubfile);
	}
}
