package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 17/02/2017 05:43
 * @author Quipoz
 */
public class Sr5a7screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {9, 17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr5a7ScreenVars sv = (Sr5a7ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr5a7screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr5a7ScreenVars screenVars = (Sr5a7ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.feddflag.setClassString("");
		screenVars.fpvflag.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		
}

/**
 * Clear all the variables in Sr5a7screen
 */
	public static void clear(VarModel pv) {
		Sr5a7ScreenVars screenVars = (Sr5a7ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.feddflag.clear();
		screenVars.fpvflag.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
			}
}
