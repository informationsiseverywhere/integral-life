/*
 * File: Annychg.java
 * Date: 29 August 2009 20:14:10
 * Author: Quipoz Limited
 * 
 * Class transformed from ANNYCHG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.annuities.dataaccess.Annyvf1TableDAM;
import com.csc.life.contractservicing.dataaccess.AnntmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* OVERVIEW
* ========
*
* This is  a  new  subroutine  which  forms  part  of  the  9405
* Annuities  Development.    It will be called from T5671 during
* the AT processing of component add or component modify.
* It will convert any ANNT records to ANNY records, setting  any
* existing ANNY records with the same key to valid flag '2'.
*
* If a new component is being added the new coverage record is
* also updated with the bonus indicator, the anniversary
* process date and the bonus declare date.
*
*****************************************************************
* </pre>
*/
public class Annychg extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaComponentAdd = new FixedLengthStringData(1);
		/* ERRORS */
	private String h147 = "H147";
	private String h148 = "H148";
	private String h149 = "H149";
		/* FORMATS */
	private String anntmjarec = "ANNTMJAREC";
	private String annyvf1rec = "ANNYVF1REC";
	private String covrmjarec = "COVRMJAREC";
		/* TABLES */
	private String t6625 = "T6625";
		/*Annuities Temporary Details Major Alts*/
	private AnntmjaTableDAM anntmjaIO = new AnntmjaTableDAM();
		/*Annuity Details Valid Flag 1 only*/
	private Annyvf1TableDAM annyvf1IO = new Annyvf1TableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Isuallrec isuallrec = new Isuallrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6625rec t6625rec = new T6625rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		noAnnt0001, 
		exit2009, 
		exit602
	}

	public Annychg() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					starts0000();
				}
				case noAnnt0001: {
					noAnnt0001();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void starts0000()
	{
		initialize1000();
		readAnnyvf12000();
		if (isEQ(anntmjaIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.noAnnt0001);
		}
		writeNewAnnyvf13000();
		deleteAnntmja4000();
	}

protected void noAnnt0001()
	{
		if (isEQ(wsaaComponentAdd,"Y")) {
			updateNewCoverage5000();
		}
		exitProgram();
	}

protected void initialize1000()
	{
		starts1000();
	}

protected void starts1000()
	{
		wsaaComponentAdd.set(SPACES);
		chdrmjaIO.setDataKey(SPACES);
		chdrmjaIO.setChdrcoy(isuallrec.company);
		chdrmjaIO.setChdrnum(isuallrec.chdrnum);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		anntmjaIO.setDataArea(SPACES);
		anntmjaIO.setChdrnum(isuallrec.chdrnum);
		anntmjaIO.setChdrcoy(isuallrec.company);
		anntmjaIO.setLife(isuallrec.life);
		anntmjaIO.setCoverage(isuallrec.coverage);
		anntmjaIO.setRider(isuallrec.rider);
		anntmjaIO.setPlanSuffix(isuallrec.planSuffix);
		anntmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, anntmjaIO);
		if (isNE(anntmjaIO.getStatuz(),varcom.oK)
		&& isNE(anntmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(anntmjaIO.getStatuz());
			syserrrec.params.set(anntmjaIO.getParams());
			fatalError600();
		}
	}

protected void readAnnyvf12000()
	{
		try {
			starts2000();
		}
		catch (GOTOException e){
		}
	}

protected void starts2000()
	{
		annyvf1IO.setDataArea(SPACES);
		annyvf1IO.setChdrnum(isuallrec.chdrnum);
		annyvf1IO.setChdrcoy(isuallrec.company);
		annyvf1IO.setLife(isuallrec.life);
		annyvf1IO.setCoverage(isuallrec.coverage);
		annyvf1IO.setRider(isuallrec.rider);
		annyvf1IO.setPlanSuffix(isuallrec.planSuffix);
		annyvf1IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, annyvf1IO);
		if (isNE(annyvf1IO.getStatuz(),varcom.oK)
		&& isNE(annyvf1IO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(annyvf1IO.getStatuz());
			syserrrec.params.set(annyvf1IO.getParams());
			fatalError600();
		}
		if (isEQ(annyvf1IO.getStatuz(),varcom.mrnf)) {
			wsaaComponentAdd.set("Y");
			goTo(GotoLabel.exit2009);
		}
		if (isEQ(annyvf1IO.getStatuz(),varcom.oK)
		&& isNE(anntmjaIO.getStatuz(),varcom.mrnf)) {
			annyvf1IO.setValidflag("2");
			annyvf1IO.setFormat(annyvf1rec);
			annyvf1IO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, annyvf1IO);
			if (isNE(annyvf1IO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(annyvf1IO.getStatuz());
				syserrrec.params.set(annyvf1IO.getParams());
				fatalError600();
			}
		}
	}

protected void writeNewAnnyvf13000()
	{
		starts3000();
	}

protected void starts3000()
	{
		annyvf1IO.setGuarperd(anntmjaIO.getGuarperd());
		annyvf1IO.setFreqann(anntmjaIO.getFreqann());
		annyvf1IO.setArrears(anntmjaIO.getArrears());
		annyvf1IO.setAdvance(anntmjaIO.getAdvance());
		annyvf1IO.setDthpercn(anntmjaIO.getDthpercn());
		annyvf1IO.setDthperco(anntmjaIO.getDthperco());
		annyvf1IO.setIntanny(anntmjaIO.getIntanny());
		annyvf1IO.setWithprop(anntmjaIO.getWithprop());
		annyvf1IO.setWithoprop(anntmjaIO.getWithoprop());
		annyvf1IO.setPpind(anntmjaIO.getPpind());
		annyvf1IO.setCapcont(anntmjaIO.getCapcont());
		annyvf1IO.setTranno(chdrmjaIO.getTranno());
		annyvf1IO.setNomlife(anntmjaIO.getNomlife());
		annyvf1IO.setUserProfile(anntmjaIO.getUserProfile());
		annyvf1IO.setJobName(anntmjaIO.getJobName());
		annyvf1IO.setDatime(anntmjaIO.getDatime());
		annyvf1IO.setValidflag("1");
		annyvf1IO.setFormat(annyvf1rec);
		annyvf1IO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, annyvf1IO);
		if (isNE(annyvf1IO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(annyvf1IO.getStatuz());
			syserrrec.params.set(annyvf1IO.getParams());
			fatalError600();
		}
	}

protected void deleteAnntmja4000()
	{
		/*STARTS*/
		anntmjaIO.setFormat(anntmjarec);
		anntmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, anntmjaIO);
		if (isNE(anntmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(anntmjaIO.getParams());
			syserrrec.statuz.set(anntmjaIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateNewCoverage5000()
	{
		/*STARTS*/
		readCovrRecord5100();
		updateFields5200();
		rewriteCovrRecord5300();
		/*EXIT*/
	}

protected void readCovrRecord5100()
	{
		starts5100();
	}

protected void starts5100()
	{
		covrmjaIO.setChdrcoy(isuallrec.company);
		covrmjaIO.setChdrnum(isuallrec.chdrnum);
		covrmjaIO.setLife(isuallrec.life);
		covrmjaIO.setCoverage(isuallrec.coverage);
		covrmjaIO.setRider(isuallrec.rider);
		covrmjaIO.setPlanSuffix(isuallrec.planSuffix);
		covrmjaIO.setFunction(varcom.readh);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void updateFields5200()
	{
		/*STARTS*/
		readT66255210();
		setBonusIndicator5220();
		annivProcessDate5230();
		bonusDeclareDate5240();
		/*EXIT*/
	}

protected void readT66255210()
	{
		start5210();
	}

protected void start5210()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getCrtable());
			syserrrec.statuz.set(h147);
			fatalError600();
		}
		else {
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
	}

protected void setBonusIndicator5220()
	{
		/*BONUS-INDICATOR*/
		if (isNE(t6625rec.bonalloc,SPACES)
		&& isNE(t6625rec.bonalloc,"P")
		&& isNE(t6625rec.bonalloc,"C")) {
			syserrrec.statuz.set(h148);
			fatalError600();
		}
		/*BONUS-METHOD*/
		if (isNE(t6625rec.bonalloc,SPACES)) {
			if (isEQ(t6625rec.revBonusMeth,SPACES)) {
				syserrrec.statuz.set(h149);
				fatalError600();
			}
		}
		/*SET-BONUS-INDICATOR*/
		covrmjaIO.setBonusInd(t6625rec.bonalloc);
		/*EXIT*/
	}

protected void annivProcessDate5230()
	{
		/*START*/
		covrmjaIO.setAnnivProcDate(99999999);
		datcon2rec.intDate1.set(covrmjaIO.getCrrcd());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		covrmjaIO.setAnnivProcDate(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void bonusDeclareDate5240()
	{
		/*STARTS*/
		if ((isEQ(covrmjaIO.getBonusInd(),"P"))) {
			covrmjaIO.setUnitStatementDate(covrmjaIO.getCrrcd());
		}
		else {
			covrmjaIO.setUnitStatementDate(99999999);
		}
		/*EXIT*/
	}

protected void rewriteCovrRecord5300()
	{
		start5300();
	}

protected void start5300()
	{
		covrmjaIO.setChdrcoy(isuallrec.company);
		covrmjaIO.setChdrnum(isuallrec.chdrnum);
		covrmjaIO.setLife(isuallrec.life);
		covrmjaIO.setCoverage(isuallrec.coverage);
		covrmjaIO.setRider(isuallrec.rider);
		covrmjaIO.setPlanSuffix(isuallrec.planSuffix);
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error600();
				}
				case exit602: {
					exit602();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit602);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		isuallrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
