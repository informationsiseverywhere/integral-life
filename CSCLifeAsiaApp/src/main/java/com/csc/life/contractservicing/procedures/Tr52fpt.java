/*
 * File: Tr52fpt.java
 * Date: December 3, 2013 3:59:29 AM ICT
 * Author: CSC
 * 
 * Class transformed from TR52FPT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.contractservicing.tablestructures.Tr52frec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR52F.
*
*
*
****************************************************************** ****
* </pre>
*/
public class Tr52fpt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr52frec tr52frec = new Tr52frec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr52fpt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr52frec.tr52fRec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(tr52frec.txtype01);
		generalCopyLinesInner.fieldNo008.set(tr52frec.shortds01);
		generalCopyLinesInner.fieldNo009.set(tr52frec.txtype02);
		generalCopyLinesInner.fieldNo010.set(tr52frec.shortds02);
		generalCopyLinesInner.fieldNo011.set(tr52frec.dtyamt01);
		generalCopyLinesInner.fieldNo012.set(tr52frec.txratea01);
		generalCopyLinesInner.fieldNo013.set(tr52frec.txfxamta1);
		generalCopyLinesInner.fieldNo014.set(tr52frec.txrateb01);
		generalCopyLinesInner.fieldNo015.set(tr52frec.txfxamtb1);
		generalCopyLinesInner.fieldNo016.set(tr52frec.dtyamt02);
		generalCopyLinesInner.fieldNo017.set(tr52frec.txratea02);
		generalCopyLinesInner.fieldNo018.set(tr52frec.txfxamta2);
		generalCopyLinesInner.fieldNo019.set(tr52frec.txrateb02);
		generalCopyLinesInner.fieldNo020.set(tr52frec.txfxamtb2);
		generalCopyLinesInner.fieldNo021.set(tr52frec.dtyamt03);
		generalCopyLinesInner.fieldNo022.set(tr52frec.txratea03);
		generalCopyLinesInner.fieldNo023.set(tr52frec.txfxamta3);
		generalCopyLinesInner.fieldNo024.set(tr52frec.txrateb03);
		generalCopyLinesInner.fieldNo025.set(tr52frec.txfxamtb3);
		generalCopyLinesInner.fieldNo026.set(tr52frec.dtyamt04);
		generalCopyLinesInner.fieldNo027.set(tr52frec.txratea04);
		generalCopyLinesInner.fieldNo028.set(tr52frec.txfxamta4);
		generalCopyLinesInner.fieldNo029.set(tr52frec.txrateb04);
		generalCopyLinesInner.fieldNo030.set(tr52frec.txfxamtb4);
		generalCopyLinesInner.fieldNo031.set(tr52frec.dtyamt05);
		generalCopyLinesInner.fieldNo032.set(tr52frec.txratea05);
		generalCopyLinesInner.fieldNo033.set(tr52frec.txfxamta5);
		generalCopyLinesInner.fieldNo034.set(tr52frec.txrateb05);
		generalCopyLinesInner.fieldNo035.set(tr52frec.txfxamtb5);
		generalCopyLinesInner.fieldNo036.set(tr52frec.txabsind01);
		generalCopyLinesInner.fieldNo037.set(tr52frec.txabsind02);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(39).isAPartOf(wsaaPrtLine001, 37, FILLER).init("Tax Rates Table                   SR52F");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(52);
	private FixedLengthStringData filler9 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Tax Type:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 18);
	private FixedLengthStringData filler10 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 21);
	private FixedLengthStringData filler11 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 39);
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 42);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(52);
	private FixedLengthStringData filler13 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine005, 0, FILLER).init("  Amount Up to    Rate   Fix Amt      Rate   Fix Amt");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(52);
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine006, 2).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine006, 25).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 38).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(52);
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine007, 2).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine007, 25).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 38).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine007, 45).setPattern("ZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(52);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine008, 2).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine008, 25).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 38).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(52);
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine009, 2).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine009, 25).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 38).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine009, 45).setPattern("ZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(52);
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine010, 2).setPattern("ZZZZZZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 13, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine010, 25).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 38).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(52);
	private FixedLengthStringData filler39 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler40 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 18, FILLER).init("Coy absorb?");
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 31);
	private FixedLengthStringData filler41 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler42 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 38, FILLER).init("Coy absorb?");
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 51);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(28);
	private FixedLengthStringData filler43 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" F1=Help  F3=ETit  F4=Prompt");
}
}
