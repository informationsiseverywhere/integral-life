/*
 * File: Nfsurr.java
 * Date: 29 August 2009 23:00:42
 * Author: Quipoz Limited
 * 
 * Class transformed from NFSURR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.recordstructures.Nfloanrec;
import com.csc.life.contractservicing.reports.R6244Report;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.CnfsTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;

/**
* <pre>
*
*REMARKS.
*
*        NON-FORFEITURES - SURRENDER SUBROUTINE.
*        ======================================
*
* Overview.
* ---------
*
* This program is to be called from Overdue processing, for
* Traditional contracts that are Overdue and in need of transfer
* to Non-forfeiture ( APL ), via T6654, and from the Loans
* Capitalisation batch job B5031.
* This subroutine will basically decide whether the contract
* being processed is to be allowed a Non-forfeiture Loan (APL).
* It will calculate the Current Surrender value of the Contract,
* total all outstanding Loans against the contract and then
* check the difference between the two. If the Surrender value
* is greater than the debt then processing for this contract
* can continue, otherwise this routine will write a line to a
* report saying that the Contract Surrender value can no longer
* support the debts against it.
*
* Processing.
* -----------
*
* Given: NFLOANREC copybook, containing.....
*          Company, Contract Number, Open-printer-flag,
*          Language, Branch, Effective date,
*
*
* Gives: Statuz:   'O-K'   ..... Policy can have an APL
*                  'NAPL'  ..... Policy cannot have an APL
*             anything else is an ERROR.
*
*
*    i) Calculate current Surrender value of Contract:
*       For each Coverage/Rider attached to the Contract, use
*       the Surrender method specified on T5687 to access the
*       table T6598 we get the Surrender value calculation
*       subroutine which can then be called to calculate the
*       S/Value of the Coverage/Rider being processed.
*       Total the Surrender values of all the Coverages/Riders
*       to get the Contract Surrender value.
*
*   ii) Call the 'TOTLOAN' subroutine with a Function of SPACES
*       to calculate the current total of any Loans + associated
*       interest owed which are associated with this contract
*       we are processing.
*
*  iii) Check the difference between the Surrender value of the
*       contract and the current Loan total:
*
*       If the Surrender value is > current Loan total,
*       Exit the subroutine with a status of OK.
*
*       If the Surrender value is <= current Loan total,
*       write a line to the R6244 Overdue Processing report,
*       detailing the contract number, Surrender value and
*       Current Loan total.
*       Exit the subroutine with a status of 'NAPL' i.e. no APL
*       allowed.
*
*
* TABLES USED
* -----------
*
*   T1692  -   Branch table
*   T1693  -   Company table
*   T5679  -   Transaction Status requirements
*   T5687  -   Coverage/Rider details table
*   T6598  -   Calculation methods table
*   T6633  -   Loan Interest Rules table
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Nfsurr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R6244Report printerFile = new R6244Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaOldCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaOldRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaOldPlnsfx = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRiskStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaTotalCash = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSurrValTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldSurrVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalDebt = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaYesToLoan = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrintALine = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaPageCount = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private String wsaaOpenPrintFlag = "N";
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaPolsumCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT6633Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6633Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Key, 0);
	private FixedLengthStringData wsaaT6633Type = new FixedLengthStringData(1).isAPartOf(wsaaT6633Key, 3);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT6633Key, 4, FILLER).init(SPACES);
	
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
		/* FORMATS */
	private static final String covrsurrec = "COVRSURREC";
	private static final String descrec = "DESCREC   ";
	private static final String itdmrec = "ITEMREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String cnfsrec = "CNFSREC   ";
		/* ERRORS */
	private static final String f294 = "F294";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
	private static final String t6633 = "T6633";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*   Main, standard page heading*/
	private FixedLengthStringData r6244H01 = new FixedLengthStringData(83);
	private FixedLengthStringData r6244h01O = new FixedLengthStringData(83).isAPartOf(r6244H01, 0);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(r6244h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r6244h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r6244h01O, 11);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r6244h01O, 41);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r6244h01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r6244h01O, 53);

		/*  Detail line*/
	private FixedLengthStringData r6244D01 = new FixedLengthStringData(67);
	private FixedLengthStringData r6244d01O = new FixedLengthStringData(67).isAPartOf(r6244D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r6244d01O, 0);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(r6244d01O, 8);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(r6244d01O, 11);
	private FixedLengthStringData rd01Totnox = new FixedLengthStringData(2).isAPartOf(r6244d01O, 14);
	private ZonedDecimalData rd01Surrval = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 16);
	private ZonedDecimalData rd01Loansum = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 33);
	private ZonedDecimalData rd01Outstamt = new ZonedDecimalData(17, 2).isAPartOf(r6244d01O, 50);
	private CnfsTableDAM cnfsIO = new CnfsTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6633rec t6633rec = new T6633rec();
	private Nfloanrec nfloanrec = new Nfloanrec();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ExternalisedRules er = new ExternalisedRules();
	public Nfsurr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		nfloanrec.nfloanRec = convertAndSetParam(nfloanrec.nfloanRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		start100();
		exit190();
	}

protected void start100()
	{
		initialise1000();
		if (isNE(wsaaYesToLoan, "Y")) {
			return ;
		}
		getSurrValues2000();
		sumLoans3000();
		surrvalLoansumCalc4000();
		if (isEQ(nfloanrec.statuz, "NAPL")) {
			writeCnfs5000();
		}
		if (isEQ(wsaaPrintALine, "Y")) {
			printReportLine10000();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		wsaaYesToLoan.set(SPACES);
		wsaaPrintALine.set(SPACES);
		wsaaSurrValTot.set(ZERO);
		wsaaHeldSurrVal.set(ZERO);
		wsaaOldCoverage.set(SPACES);
		wsaaOldRider.set(SPACES);
		wsaaHeldCurrLoans.set(ZERO);
		wsaaTotalDebt.set(ZERO);
		nfloanrec.statuz.set(varcom.oK);
		/* Open print file only if not already open*/
		if (isEQ(wsaaOpenPrintFlag, "N")) {
			setUpPrintFile1100();
		}
		/* check contract type has an entry in T6633 Loan Interest rules*/
		/*  table before we try to give contract an APL.*/
		readT66331200();
		/* if no T6633 record found, set status to NAPL and exit*/
		if (isEQ(t6633rec.t6633Rec, SPACES)) {
			wsaaYesToLoan.set("N");
			nfloanrec.statuz.set("NAPL");
		}
		else {
			wsaaYesToLoan.set("Y");
		}
		/* Read T5679 transaction statii table*/
		readT56791300();
	}

protected void setUpPrintFile1100()
	{
		start1100();
		setUpHeadingCompany1110();
		setUpHeadingBranch1120();
		setUpHeadingDates1130();
	}

protected void start1100()
	{
		/* this section will only be performed once whilst the subroutine*/
		/* itself may be called many times from the same program - we only*/
		/* need to set up the report header details & open the print file*/
		/* once*/
		printerFile.openOutput();
		wsaaOpenPrintFlag = "Y";
	}

protected void setUpHeadingCompany1110()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(nfloanrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(nfloanrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		rh01Company.set(nfloanrec.company);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1120()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(nfloanrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(nfloanrec.batcbrn);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(nfloanrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		rh01Branch.set(nfloanrec.batcbrn);
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1130()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(nfloanrec.effdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void readT66331200()
	{
		start1200();
	}

protected void start1200()
	{
		/* Read T6633 Loan interest rules to see if Contract type is*/
		/*  allowed to have a loan held against it.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(nfloanrec.chdrcoy);
		itdmIO.setItemtabl(t6633);
		/* MOVE NONF-CNTTYPE           TO ITDM-ITEMITEM.                */
		wsaaT6633Cnttype.set(nfloanrec.cnttype);
		wsaaT6633Type.set("A");
		itdmIO.setItemitem(wsaaT6633Key);
		itdmIO.setItmfrm(nfloanrec.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(), nfloanrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t6633)
		|| isNE(itdmIO.getItemitem(), wsaaT6633Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*       no loan rules for this contract type so we can't go APL*/
			t6633rec.t6633Rec.set(SPACES);
		}
		else {
			t6633rec.t6633Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT56791300()
	{
		start1300();
	}

protected void start1300()
	{
		/* Read T5679*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(nfloanrec.chdrcoy);
		itemIO.setItemtabl(t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(nfloanrec.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError99000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void getSurrValues2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* Start reading on Coverage file to see what components this*/
		/*  contract has ... if none, then we will just exit this section,*/
		/*  Otherwise we will calculate the total surrender value of the*/
		/*   contract.*/
		covrsurIO.setParams(SPACES);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setChdrcoy(nfloanrec.chdrcoy);
		covrsurIO.setChdrnum(nfloanrec.chdrnum);
		covrsurIO.setPlanSuffix(ZERO);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			systemError99000();
		}
		/* No Coverage/Riders on contract*/
		if (isNE(covrsurIO.getChdrcoy(), nfloanrec.chdrcoy)
		|| isNE(covrsurIO.getChdrnum(), nfloanrec.chdrnum)
		|| isEQ(covrsurIO.getStatuz(), varcom.endp)) {
			covrsurIO.setStatuz(varcom.endp);
			return ;
		}
		/* We have at least 1 Coverage/rider on the contract.*/
		/* Now process all components ( ie Coverage/Riders ) on contract*/
		wsaaLife.set(covrsurIO.getLife());
		while ( !(isEQ(covrsurIO.getStatuz(), varcom.endp))) {
			processComponents2100();
		}
		
		/* Store values calculated in the Contracts' own currency in case*/
		/*  user decides he wants the loan in a different currency later.*/
		wsaaHeldSurrVal.set(wsaaSurrValTot);
	}

protected void processComponents2100()
	{
		start2100();
	}

protected void start2100()
	{
		wsaaOldCoverage.set(covrsurIO.getCoverage());
		wsaaOldRider.set(covrsurIO.getRider());
		wsaaOldPlnsfx.set(covrsurIO.getPlanSuffix());
		/* Read T5687 for Surrender method of component being processed.*/
		/* IF Surrender method not spaces, Read T6598, the Calculation*/
		/*  methods table to get the name of the Surrender value*/
		/*  subroutine we are going to call.*/
		surrSubroutine2200();
		/* Set up linkage area to Call Surrender value calculation*/
		/*  subroutines.*/
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		srcalcpy.polsum.set(nfloanrec.polsum);
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.ptdate.set(nfloanrec.ptdate);
		srcalcpy.effdate.set(nfloanrec.effdate);
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.language.set(nfloanrec.language);
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		srcalcpy.chdrCurr.set(nfloanrec.cntcurr);
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		if (isGT(covrsurIO.getInstprem(), ZERO)) {
			srcalcpy.singp.set(covrsurIO.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrsurIO.getSingp());
		}
		srcalcpy.billfreq.set(nfloanrec.billfreq);
		srcalcpy.status.set(SPACES);
		/* Call T6598 Surrender value calculation subroutine....*/
		/* BUT FIRST..... We may have problems with contracts which are*/
		/* part broken out i.e. ...10 policies in the plan, only 5 are*/
		/* broken out and therefore there are still 5 summarised under*/
		/* a plan suffix of 0.... IF PRESERV ( the surrender val calc*/
		/* subroutine ) is given a plan suffix of zero, it thinks we*/
		/* want to do a Whole Plan Surrender value calculation when in*/
		/* fact in our part broken out case we want the Surrender value*/
		/* of the summarised policies and ALSO the surrender values of*/
		/* the individually broken out policies.*/
		/* So, when we have a part broken out case, we will pretend*/
		/* that the summarised record is broken out and call the*/
		/* Surrender value calculation subroutiens for each of the*/
		/* summarised policies within the plan.*/
		if (isEQ(covrsurIO.getPlanSuffix(), ZERO)
		&& isNE(nfloanrec.polsum, nfloanrec.polinc)
		&& isNE(nfloanrec.polinc, 1)) {
			for (wsaaPolsumCount.set(1); !(isGT(wsaaPolsumCount, nfloanrec.polsum)); wsaaPolsumCount.add(1)){
				sumpolSvalCalc2150();
			}
		}
		else {
			while ( !(isEQ(srcalcpy.status, varcom.endp))) {
				calculateSurrValue2300();
			}
			
		}
		/* Get next Component.*/
		while ( !(isNE(wsaaOldCoverage, covrsurIO.getCoverage())
		|| isNE(wsaaOldRider, covrsurIO.getRider())
		|| isNE(wsaaOldPlnsfx, covrsurIO.getPlanSuffix())
		|| isEQ(covrsurIO.getStatuz(), varcom.endp))) {
			getNextComponent2400();
		}
		
	}

protected void sumpolSvalCalc2150()
	{
		/*START*/
		/* Calculate the surrender value of a summarised policy in a*/
		/* part broken out plan*/
		srcalcpy.planSuffix.set(wsaaPolsumCount);
		srcalcpy.status.set(varcom.oK);
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			calculateSurrValue2300();
		}
		
		/*EXIT*/
	}

protected void surrSubroutine2200()
	{
		start2200();
	}

protected void start2200()
	{
		/* Read T5687*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(covrsurIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(), covrsurIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrsurIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(covrsurIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			systemError99000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/* Now read T6598 using T5687-SV-METHOD*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covrsurIO.getChdrcoy());
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError99000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
	}

protected void calculateSurrValue2300()
	{
		start2300();
	}

protected void start2300()
	{
		if (isEQ(t6598rec.calcprog, SPACES)) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		/* We have a subroutine to call so let's do it*/
		/* But first we must check that the chosen COVRSUR record has a*/
		/*  valid premium & risk status by checking in T5679*/
		checkCovrsurStatii2350();
		/* if either the Premium or Risk status of the Coverage is*/
		/*  invalid, then we won't calculate a Surrender value for it*/
		if (isEQ(wsaaPremStatus, "N")
		|| isEQ(wsaaRiskStatus, "N")) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);	
			
			chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
			chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}
		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			systemError99000();
		}
		/* Add values returned to our running Surrender value total*/
		/* MOVE SURC-ACTUAL-VAL         TO ZRDP-AMOUNT-IN.              */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO SURC-ACTUAL-VAL.             */
		if (isNE(srcalcpy.actualVal, 0)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				zrdecplrec.currency.set(nfloanrec.cntcurr);
			}
			else {
				zrdecplrec.currency.set(srcalcpy.currcode);
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			a000CallRounding();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
		}
		wsaaSurrValTot.add(srcalcpy.actualVal);
	}

protected void checkCovrsurStatii2350()
	{
		/*START*/
		wsaaPremStatus.set("N");
		wsaaRiskStatus.set("N");
		/* check Coverage premium status*/
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaPremStatus, "Y")); wsaaIndex.add(1)){
			premiumStatus2370();
		}
		/* check Coverage Risk status*/
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaRiskStatus, "Y")); wsaaIndex.add(1)){
			riskStatus2390();
		}
		/*EXIT*/
	}

protected void premiumStatus2370()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrsurIO.getPstatcode()))) {
					wsaaPremStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrsurIO.getPstatcode()))) {
					wsaaPremStatus.set("Y");
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void riskStatus2390()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrsurIO.getStatcode()))) {
					wsaaRiskStatus.set("Y");
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrsurIO.getStatcode()))) {
					wsaaRiskStatus.set("Y");
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void getNextComponent2400()
	{
		/*START*/
		/* Read the next coverage/rider record.*/
		covrsurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrsurIO.getChdrcoy(), nfloanrec.chdrcoy)
		|| isNE(covrsurIO.getChdrnum(), nfloanrec.chdrnum)
		|| isNE(covrsurIO.getLife(), wsaaLife)
		|| isEQ(covrsurIO.getStatuz(), varcom.endp)) {
			covrsurIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void sumLoans3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Having got the current surrender value of the contract and the*/
		/*  maximum loan amount available figures, we need to check on*/
		/*  whether there are any existing loans and if so what the value*/
		/*  of those loans is at this moment in time.*/
		/* NOTE..... values returned from TOTLOAN are in the Contract*/
		/*            currency.*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(nfloanrec.chdrcoy);
		totloanrec.chdrnum.set(nfloanrec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(nfloanrec.effdate);
		totloanrec.language.set(nfloanrec.language);
		/* Pass 'LOAN' as the Function to HRTOTLON to calculate only    */
		/* total Loans related to that contract.                        */
		totloanrec.function.set("LOAN");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError99000();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		/* Now, we should calculate the Total Cash that the Contract    */
		/* has by passing 'CASH' function to HRTOTLON.          <LA2103>*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(nfloanrec.chdrcoy);
		totloanrec.chdrnum.set(nfloanrec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(nfloanrec.effdate);
		totloanrec.function.set("CASH");
		totloanrec.language.set(nfloanrec.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError99000();
		}
		compute(wsaaTotalCash, 2).set(add(totloanrec.principal, totloanrec.interest));
	}

protected void surrvalLoansumCalc4000()
	{
		/*START*/
		compute(wsaaTotalDebt, 2).set(add(wsaaHeldCurrLoans, nfloanrec.outstamt));
		if ((setPrecision(wsaaTotalDebt, 2)
		&& isGT((add(wsaaHeldSurrVal, wsaaTotalCash)), wsaaTotalDebt))) {
			/* we can let the contract have an APL ( Automatic Premium Loan )*/
			wsaaPrintALine.set("N");
		}
		else {
			/* we cannot allow an APL for this contract because the Loan debt*/
			/*  is already higher than the Surrender value of the contract*/
			wsaaPrintALine.set("Y");
			nfloanrec.statuz.set("NAPL");
		}
		/*EXIT*/
	}

protected void writeCnfs5000()
	{
		cnfs5000();
	}

protected void cnfs5000()
	{
		cnfsIO.setDataArea(SPACES);
		cnfsIO.setChdrcoy(nfloanrec.chdrcoy);
		cnfsIO.setChdrnum(nfloanrec.chdrnum);
		cnfsIO.setValidflag("1");
		cnfsIO.setEffdate(nfloanrec.effdate);
		cnfsIO.setFunction(varcom.writr);
		cnfsIO.setFormat(cnfsrec);
		SmartFileCode.execute(appVars, cnfsIO);
		if (isNE(cnfsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cnfsIO.getParams());
			syserrrec.statuz.set(cnfsIO.getStatuz());
			databaseError99500();
		}
	}

protected void printReportLine10000()
	{
		writeDetail10050();
	}

protected void writeDetail10050()
	{
		/* If first page/overflow - write standard headings*/
		if (newPageReq.isTrue()) {
			printerFile.printR6244h01(r6244H01, indicArea);
			wsaaOverflow.set("N");
		}
		/* set up details line fields*/
		rd01Chdrnum.set(nfloanrec.chdrnum);
		rd01Cnttype.set(nfloanrec.cnttype);
		rd01Cntcurr.set(nfloanrec.cntcurr);
		rd01Surrval.set(wsaaHeldSurrVal);
		rd01Loansum.set(wsaaHeldCurrLoans);
		rd01Totnox.set(totloanrec.loanCount);
		rd01Outstamt.set(nfloanrec.outstamt);
		/*  Write detail, checking for page overflow*/
		printerFile.printR6244d01(r6244D01, indicArea);
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(nfloanrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(nfloanrec.cntcurr);
		zrdecplrec.batctrcde.set(nfloanrec.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError99000();
		}
		/*A900-EXIT*/
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		nfloanrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		start99500();
		exit99590();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		nfloanrec.statuz.set(varcom.bomb);
		exit190();
	}
}
