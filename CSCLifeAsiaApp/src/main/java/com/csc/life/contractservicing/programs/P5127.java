/*
 * File: P5127.java
 * Date: 30 August 2009 0:10:39
 * Author: Quipoz Limited
 * 
 * Class transformed from P5127.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
//ILB-456 start 
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.contractservicing.screens.S5127ScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgwTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.terminationclaims.dataaccess.dao.CovppfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Covppf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  P5127 - Component Change - Term Components.
*  -------------------------------------------
*  ----
*  This screen/program P5127 is  used  to  capture any  changes
*  to  the    coverage   and   rider  details  for  traditional
*  generic components.
*
*  Initialise.
*  -----------
*
*  Skip this section  if    returning    from    an    optional
*  selection (current stack position action flag = '*').
*
*  Read  CHDRLIF  (RETRV)  in    order  to  obtain the contract
*  header information.
*
*  The key for Coverage/rider  to    be    worked  on  will  be
*  available  from  COVR.   This is obtained by using the RETRV
*  function.
*
*  Firstly  we  need  to check  if  there  has  already    been
*  an   alteration   to   this     coverage/rider  within  this
*  transaction. If this  is   the  case  there  will    be    a
*  record      present     on    the  temporary  coverage/rider
*  transaction  file    COVTPF.    Read  this  file  using  the
*  logical  view    COVTMAJ  and  the  key  from  the retrieved
*  coverage/rider record. If  a  record  is  found  go  to  the
*  SET-UP-SCREEN section.
*
*  Compare  the  plan  suffix  of  the  retrieved  record  with
*  the contract header no of policies  in  plan    field.    If
*  the  plan  suffix  is '00' go to SET-UP-COVT.  If it is less
*  than or equal to this field the we need  to  'breakout'  the
*  amount fields as follows:
*
*  Divide the COVR sum assured  field  by  the CHDR no of
*                policies in plan.
*  Divide  the COVR premium  field  by  the  CHDR  no  of
*                policies in plan.
*
*  ******* SET-UP-COVT.
*
*  Move    the  appropriate fields  from  the  COVR  record  to
*  the COVTMAJ record.
*
*  ******* SET-UP-SCREEN.
*
*  Read  the  contract definition  details  from   T5688    for
*  the  contract  type  held on  CHDRLIF.  Access  the  version
*  of this item for the original commencement date of the risk.
*
*  Read  the  general  coverage/rider  details from  T5687  and
*  the  traditional/term    edit    rules  from  T5608  for the
*  coverage type held  on  COVTMAJ.  Access  the    version  of
*  these item for the original commencement date of the risk.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To  obtain the life assured  and joint-life details (if any)
*  do the following;-
*
*       - read the life  details  using  LIFEMAJ  (life  number
*       from  COVTMAJ,  joint  life number  '00').  Look up the
*       name from the client details (CLTS)  and  format  as  a
*       "confirmation name".
*
*       -  read  the  joint    life details using LIFEMAJ (life
*       number from COVTMAJ,  joint  life  number  '01').    If
*       found,  look  up  the  name  from  the  client  details
*       (CLTS) and format as a "confirmation name".
*
*  To determine which premium  calculation    method    to  use
*  decide  whether  or  not  it  is  a   Single  or  Joint-life
*  case  (it is a joint life case, the  joint    life    record
*  was  found  above). Then:
*
*       -  if  it  is a single-life case use, the single-method
*       from T5687. The age to be used  for validation will  be
*       the age of the main life.
*
*       -  if  the joint-life  indicator (from T5687) is blank,
*       and  if  it  is  a    Joint-life    case,    use    the
*       joint-method  from  T5687.  The  age  to  be  used  for
*       validation will be the age of the main life.
*
*       -  if the Joint-life  indicator  is  'N',   then    use
*       the  Single-method.    But,  if  there  is a joint-life
*       (this must be a rider to have got  this  far)    prompt
*       for  the  joint  life  indicator  to  determine   which
*       life  the rider is to attach to.  In all  other  cases,
*       the  joint  life  indicator should be non-displayed and
*       protected.  The  age  to  be used for  validation  will
*       be  the    age  of the main or joint life, depending on
*       the one selected.
*
*       - use the premium-method  selected  from   T5687,    if
*       not  blank, to access T5675.  This gives the subroutine
*       to use for the calculation.
*
*      COVERAGE/RIDER DETAILS
*
*  The fields to be displayed  on  the  screen  are  determined
*  from the entry in table T5608 read earlier as follows:
*
*       -  if  the  maximum and minimum sum insured amounts are
*       both zero, non-display  and  protect  the  sum  insured
*       amount.    If  the  minimum and maximum  are  both  the
*       same, display the  amount  protected.  Otherwise  allow
*       input.
*
*       -    if    all    the   valid   mortality  classes  are
*       blank, non-display and protect this   field.  If  there
*       is  only one mortality class, display  and  protect  it
*       (no validation will be required).
*
*       - if all the valid  lien codes are  blank,  non-display
*       and  protect this field. Otherwise, this is an optional
*       field.
*
*       - if the cessation  AGE  section  on  T5608  is  blank,
*       protect the two age related fields.
*
*       -    if  the  cessation    TERM  section  on  T5608  is
*       blank, protect the two term related fields.
*
*       - using the age next  birthday  (ANB  at   RCD)    from
*       the  applicable  life (see above), look up Issue Age on
*       the AGE and TERM sections.  If the  age fits into  only
*       one  "slot"  in  one  of these  sections,  and the risk
*       cessation limits are the same, default   and    protect
*       the    risk  cessation fields.  Also  do the  same  for
*       the  premium   cessation  details.      In  this  case,
*       also    calculate    the    risk  and premium cessation
*       dates.
*
*       -  please note that it  is  possible  for   the    risk
*       and  premium  cessation  dates    to   be  overwritten,
*       i.e.  even after they are calculated.
*
*      OPTIONS AND EXTRAS
*
*  If options and extras are  not   allowed    (as  defined  by
*  T5608) non-display and protect the fields.
*
*  Otherwise,  read  the  options  and  extras  details for the
*  current coverage/rider.   If any records  exist,    put    a
*  '+'    in   the Options/Extras indicator (to show that there
*  are some).
*
*  If options and extras  are  not  allowed,  then  non-display
*  and  protect  the  special  terms    narrative  clause  code
*  fields. If special terms are present,    then    obtain  the
*  clause  codes  from  the  coverage  record and output to the
*  screen.
*
* REASSURANCE
*
* If Reassurance is not allowed ( as defined by T5687 ), then
* non-display and protect the field ( S5127-RATYPIND ).
*
* Otherwise,  read  the  Reassurance  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the  Reassurance  indicator (to show that there are some).
*
*  Finally after setting up  all  of  the    screen  attributes
*  move over the relevant fields from COVTMAJ where applicable.
*
*  Validation.
*  -----------
*
*  Skip  this  section  if    returning    from    an  optional
*  selection (current stack position action flag = '*').
*
*  If CF11 (KILL) is requested then skip the validation.
*
*  Before the premium amount  is  calculated,  the screen  must
*  be  valid.    So  all  editing    is  completed  before  the
*  premium is calculated.
*
*  Table T5608 (previously read)  is    used    to  obtain  the
*  editing  rules.    Edit  the  screen according  to the rules
*  defined by the help. In particular:-
*
*       1) Check the  sum-assured,  if   applicable,    against
*       the limits.
*
*       2)  Check  the  consistency  of   the risk age and term
*       fields and premium age and term  fields.  Either,  risk
*       age  and  and  premium age must be used  or  risk  term
*       and premium term must  be  used.    They  must  not  be
*       combined.  Note that these only need validating if they
*       were not defaulted.
*
*       3) Mortality-Class, if the  mortality    class  appears
*       on  a  coverage/rider  screen  it is a compulsory field
*       because  it  will    be  used  in    calculating    the
*       premium    amount.    The  mortality class entered must
*       one  of the ones in the edit rules table.
*
*  Calculate the following:-
*
*       - risk cessation date
*
*       - premium cessation date
*
*       - note, the risk and premium  cessation  dates  may  be
*       overwritten by the user.
*
*  OPTIONS AND EXTRAS
*
*  If  options/extras  already  exist, there  will  be a '+' in
*  this field.  A request to access  the  details  is  made  by
*  entering  'X'.    No other values  (other  than  blank)  are
*  allowed.  If  options  and  extras  are  requested,  DO  NOT
*  CALCULATE THE PREMIUM THIS TIME AROUND.
*
* REASSURANCE
*
* If  Reassurance  already exists, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed.
*
*  PREMIUM CALCULATION.
*  --------------------
*
*  The    premium   amount is  required  on  all  products  and
*  all validating  must  be   successfully  completed    before
*  it    is  calculated.  If  there  is    no   premium  method
*  defined (i.e the relevant code  was  blank),    the  premium
*  amount  must  be  entered.  Otherwise,  it  is  optional and
*  always calculated.
*
*  To  calculate  it,    call    the    relevant    calculation
*  subroutine worked out above passing:
*
*       - 'CALC' for the function
*       - Coverage code (crtable)
*       - Company
*       - Contract number
*       - Life number
*       -  Joint life number (if the screen indicator is set to
*       'J' ,otherwise pass '00')
*       - Coverage number
*       - Rider number (0 for coverages)
*       - Effective date (Premium / Risk)
*       - Termination date (i.e. term cessation date)
*       - Currency
*       - Sum insured
*       - Calculation "Basis" (i.e. mortality code)
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium amount ("instalment")
*       - Return Status
*       - Rating Date (effective date of transaction)
*
*  Subroutine may look up:
*
*       - Life and Joint-life details
*       - Options/Extras
*
*  Having calculated it, the    entered    value,  if  any,  is
*  compared  with  it  to check that it  is  within  acceptable
*  limits of the automatically  calculated  figure.    If    it
*  is    less    than    the  amount    calculated  and  within
*  tolerance  then  the  manually entered  amount  is  allowed.
*  If    the    entered  value  exceeds the calculated one, the
*  calculated value is used.
*
*  To check the tolerance  amount,  read  the  tolerance  limit
*  from  T5667  (item  is  transaction  and  coverage  code, if
*  not found, item and '****'). Although this  is    a    dated
*  table, just read the latest one (using ITEM).
*
*      If 'CALC' was entered then re-display the screen.
*
*  Updating.
*  ---------
*
*  Updating  occurs  if  any of the  fields  on the screen have
*  been amended from the original COVTMAJ details.
*
*  If the 'KILL' function key was pressed skip the updating.
*
*  Update the COVTMAJ record with the details from  the  screen
*  and write/rewrite the record to the database.
*
*  IF no Coverage/rider details have been changed, but
*    Reassurance or Options/Extras details have been amended,
*    we force the program to produce a COVT record so that the
*    Component change AT module is executed, thus writing a PTRN
*    indicating that something has happened to the Coverage/Rider
*    details.
*
*  Next Program.
*  -------------
*
*  The    first  thing  to   consider   is   the   handling  of
*  an options/extras request. If  the    indicator  is  'X',  a
*  request  to  visit options and extras has been made. In this
*  case:
*
*       - change the options/extras request indicator to '?',
*
*       - save the next 8 programs from the program stack,
*
*       - call GENSSWCH with and  action  of  'A'  to  retrieve
*       the  program  switching  required, and move them to the
*       stack,
*
*       - set the current stack "action" to '*',
*
*       - add one to the program pointer and exit.
*
*  On return from this  request,  the  current  stack  "action"
*  will  be    '*' and the  options/extras  indicator  will  be
*  '?'.  To handle the return from options and extras:
*
*       -  calculate  the  premium  as  described   above    in
*       the  'Validation'  section,  and  check    that  it  is
*       within the tolerance limit,
*
*       -  blank  out  the  stack   "action",    restore    the
*       saved programs to the program stack,
*
*       -  if  the  tolerance   checking  results  in  an error
*       being detected, set WSSP-NEXTPROG  to    the    current
*       screen name (thus returning to re-display the screen).
*
* If Reassurance has been requested (the indicator = 'X'), then..
*
*  - change the Reassurance request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with and  action  of 'B' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance  indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
*  If  control  is passed to this  part  of the 4000 section on
*  the way  out  of the program,  ie.   after    screen    I/O,
*  then    the  current  stack  position  action  flag  will be
*  blank.  If the 4000 section  is  being    performed    after
*  returning      from  processing  another  program  then  the
*  current  stack  position action flag will be '*'.
*
*  If 'Enter' has been pressed  add 1 to  the  program  pointer
*  and exit.
*
*  If  'KILL'  has  been requested, (CF11), then move spaces to
*  the current program entry in the program stack and exit.
*****************************************************************
* </pre>
*/
public class P5127 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5127");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaWorkingAnb = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaPovrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPovrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaPcestermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcestermStore = new ZonedDecimalData(3, 0);
	private final int wsaaMaxOcc = 8;
	private final int wsaaMaxMort = 6;
	private PackedDecimalData wsaaCalcPrem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);
	private String wsaaPovrModified = "N";

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private Validator premNotReqd = new Validator(wsaaPremStatuz, "N");
	private Validator userPremEntered = new Validator(wsaaPremStatuz, "U");
	private FixedLengthStringData keyCrtable = new FixedLengthStringData(4);
	private ZonedDecimalData keyAgeterm = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData keyMortcls = new FixedLengthStringData(1);
	private FixedLengthStringData keySex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator policyLevel = new Validator(wsaaPlanproc, "N");
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");

	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	private Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator addComp = new Validator(wsaaFlag, "A");
	protected Validator modifyComp = new Validator(wsaaFlag, "M");
	protected Validator compEnquiry = new Validator(wsaaFlag, "I");
	protected Validator compApp = new Validator(wsaaFlag, "S", "P");
	private Validator revComp = new Validator(wsaaFlag, "R");
	private Validator addLife = new Validator(wsaaFlag, "D");
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	protected PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaStoreSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNextSeqnbr = new PackedDecimalData(3, 0).init(999);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaOldPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(17, 0);
	private ZonedDecimalData wsaaPreviousSumins = new ZonedDecimalData(17, 0).init(ZERO);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSuminsDiff = new PackedDecimalData(17, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaOldMortcls = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreZbinstprem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreSingp = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaStoreInstprem = new ZonedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaIfSuminsChanged = new FixedLengthStringData(1);
	private Validator wsaaSuminsChanged = new Validator(wsaaIfSuminsChanged, "I", "D");
	private Validator wsaaSuminsIncreased = new Validator(wsaaIfSuminsChanged, "I");
	private Validator wsaaSuminsDecreased = new Validator(wsaaIfSuminsChanged, "D");

	private FixedLengthStringData wsaaIfPremChanged = new FixedLengthStringData(1);
	private Validator wsaaPremChanged = new Validator(wsaaIfPremChanged, "I", "D");
	private Validator wsaaPremIncreased = new Validator(wsaaIfPremChanged, "I");
	private Validator wsaaPremDecreased = new Validator(wsaaIfPremChanged, "D");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
		/* WSAA-MAIN-LIFE-DETS */
	protected PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsaaSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaOptext = new FixedLengthStringData(1);
	private Validator optextYes = new Validator(wsaaOptext, "Y");
	private Validator optextNo = new Validator(wsaaOptext, "N");
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler4, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);

		/* Plan selection.*/
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private FixedLengthStringData filler6 = new FixedLengthStringData(49).isAPartOf(wsaaCovrKey, 15, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private String wsaaFirstTime = "";
	private PackedDecimalData wsaaEffDate = new PackedDecimalData(8, 0);
		/* WSBB-JOINT-LIFE-DETS */
	protected PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
	private ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsccSingPremFlag = new FixedLengthStringData(1);
	private Validator singlePremium = new Validator(wsccSingPremFlag, "Y");
	private FixedLengthStringData wsaaLextUpdates = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	//private CltsTableDAM cltsIO = new CltsTableDAM(); //ILB-456
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovrrgwTableDAM covrrgwIO = new CovrrgwTableDAM();
	protected CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
	//private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM(); //ILB-456
	private MandTableDAM mandIO = new MandTableDAM();
	//private PayrTableDAM payrIO = new PayrTableDAM();//ILB-456
	protected PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Gensswrec gensswrec = new Gensswrec();
	protected Premiumrec premiumrec = new Premiumrec();
	private T2240rec t2240rec = new T2240rec();
	private T5608rec t5608rec = new T5608rec();
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	protected T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T6005rec t6005rec = new T6005rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Wssplife wssplife = new Wssplife();
	protected S5127ScreenVars sv = getLScreenVars();//ScreenProgram.getScreenVars( S5127ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private boolean loadingFlag = false;//ILIFE-3399
	protected ExternalisedRules er = new ExternalisedRules();
	private boolean premiumflag = false;
	private boolean prmbasisFlag=false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private boolean dialdownFlag = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private boolean exclFlag = false;
	private boolean incomeProtectionflag = false;
	/*ILIFE-7118-starts*/
	private String tpd1="TPD1";
	private String tps1="TPS1";
	private boolean tpdtypeFlag=false;
	private static final String  TPD_FEATURE_ID="NBPRP060";
	/*ILIFE-7118-ends*/
	private PackedDecimalData wsaaCashValArr = new PackedDecimalData(17, 2);
	/*
	 * fwang3 ICIL-560
	 */
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Th528rec th528rec = new Th528rec();
	private boolean hasCashValue = false;
	private PackedDecimalData wsaaHoldSumin = new PackedDecimalData(15, 0);
	private boolean chinaLocalPermission;	
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	
	private T5645rec t5645rec = new T5645rec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private T5688rec t5688rec = new T5688rec();
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO",  HcsdpfDAO.class);
	private List<Hcsdpf> hsdpfList = null;
	private	Hcsdpf hcsdpf = null;
	
	private static String t5645 = "T5645";
	
	//ILIFE-7845
		private boolean riskPremflag = false;
		private static final String  RISKPREM_FEATURE_ID="NBPRP094";
		//ILIFE-7845
		private boolean stampDutyflag = false;
		private String stateCode="";
	private boolean compFlag = false;
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	private List<Rskppf> insertRiskPremList = new ArrayList<>();
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	
	//ILB-456 start 
		private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
		private Chdrpf chdrpf=new Chdrpf();
		private Covrpf covrpf = new Covrpf();
		private int covrpfCount = 0;
		private List<Covrpf> covrpfList;
		private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
		private Descpf descpf = new Descpf();
		private Lifepf lifepf = new Lifepf();
		private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
		private Clntpf clntpf= new Clntpf();
		private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
		private Payrpf payrpf = new Payrpf();
		private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
		
		private boolean isCompModify= false;
		protected Lifepf lifepf1 = new Lifepf();
		
		/*ILIFE-8248 start*/
	  	private boolean lnkgFlag = false;
		/*ILIFE-8248 end*/
	//ICIL-1310 Starts
	private boolean occupationFlag = false;
	private String occup = null;
	private String occupClass =null;
	private Itempf itempf = null;
	private T3644rec t3644rec = new T3644rec();
	private Ta610rec ta610rec = new Ta610rec();
	private String occupFeature = "NBPRP056";
	private String itemPFX = "IT";
	//ICIL-1310 End
	private List<Itempf> ta610List = new ArrayList<Itempf>();	//ICIL-1494
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();	//ICIL-1494
	private T6658rec t6658rec = new T6658rec();
	private boolean autoIncrflag = false;
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;//ALS-7685
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao;	//ALS-7685
	//ILJ-47 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-47 End
	/*IBPLIFE-2137 Start*/
	private boolean covrprpseFlag = false;
	private static final String t1688 = "T1688";
	private CovppfDAO covppfDAO= getApplicationContext().getBean("covppfDAO",CovppfDAO.class);
	private Covppf covppf = new Covppf();
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	/*IBPLIFE-2137 End*/
	
	private PackedDecimalData wsaaCommissionPrem = new PackedDecimalData(17, 2).init(0).setUnsigned();   //IBPLIFE-5237
		private FixedLengthStringData tempFlag = new FixedLengthStringData(1);
	private boolean effdateFlag;
	private static final String EFFDATE_FEATURE = "CSCOM012";
	
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccdateMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaOccdateDd = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();
	private ZonedDecimalData wsaaPtdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaPtdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaPtdateYy = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaLastAnndate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNextAnndate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaLastAnndate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaLastAnndateYy = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaLastAnndateMm = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4).setUnsigned();
	private ZonedDecimalData wsaaLastAnndateDd = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6).setUnsigned();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private FixedLengthStringData currentYrAge = new FixedLengthStringData(8);

	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setScreen1060, 
		exit1090, 
		exit1190, 
		preExit, 
		cont2030, 
		beforeExit2080, 
		exit2090, 
		checkSuminCont2120, 
		checkRcessFields2120, 
		validDate2140, 
		datesCont2140, 
		ageAnniversary2140, 
		check2140, 
		checkOccurance2140, 
		checkTermFields2150, 
		checkComplete2160, 
		checkMortcls2170, 
		loop2175, 
		checkLiencd2180, 
		loop2185, 
		checkMore2190, 
		cont, 
		exit2290, 
		updatePcdt3060, 
		exit3090, 
		exit3290, 
		exit50z0, 
		exit5290, 
		exit5390, 
		covtExist6025, 
		itemCall7023, 
		a250CallTaxSubr, 
		a290Exit
	}

	public P5127() {
		super();
		screenVars = sv;
		new ScreenModel("S5127", AppVars.getInstance(), sv);
	}

	protected S5127ScreenVars getLScreenVars() {

		return ScreenProgram.getScreenVars(S5127ScreenVars.class);

	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void largename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					contractHeader1020();
					contractCoverage1030();
					headerToScreen1040();
					lifeDetails1050();
				case setScreen1060: 
					setScreen1060();
					commisionSplit1070();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		occupationFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), occupFeature, appVars, itemPFX);	//ICIL-1310
		effdateFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), EFFDATE_FEATURE, appVars, itemPFX);
		wsaaBatckey.set(wsspcomn.batchkey);
		if(isNE(wsspcomn.flag,"X"))
		{
			wsaaFlag.set(wsspcomn.flag); 
		} 
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);	//ICIL-1494
		}
		//wsaaFlag.set(wsspcomn.flag);
		sv.dataArea.set(SPACES);
		initialize(premiumrec.premiumRec);
		premiumrec.riskPrem.set(ZERO);//ILIFE-7845
		sv.anbAtCcd.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		sv.adjustageamt.set(0);
		sv.prmbasis.set(SPACES);
		/*BRD-306 END */
		//ILIFE-3975
		sv.dialdownoption.set(SPACES);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.sumin.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.comind.set(SPACES);
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		wsaaPlanSuffix.set(ZERO);
		wsaaSumin.set(ZERO);
		wsaaOldPrem.set(ZERO);
		wsaaOldSumins.set(ZERO);
		wsaaPremDiff.set(ZERO);
		wsaaSuminsDiff.set(ZERO);
		wsaaIndex.set(ZERO);
		wsaaTaxamt.set(ZERO);
		wsaaFirstTaxCalc.set("Y");
		sv.cashvalarer.set(ZERO);//ICIL-299
		wsaaCashValArr.set(ZERO);//ICIL-299
		prmbasisFlag=false;
		/*    MOVE 'Y'                    TO  WSAA-FIRST-TIME.             */
		/* Call DATCON1 subroutine.                                        */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz); 
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		sv.zstpduty01.set(ZERO);
		//ILJ-47 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{
			sv.rcessageOut[varcom.nd.toInt()].set("Y");			
		}
		//ILJ-47 End
		/*IBPLIFE-2137 Start*/
		covrprpseFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP126", appVars, "IT");
		if(covrprpseFlag){
            sv.nbprp126lag.set("Y");
            sv.effdatex.set(varcom.vrcmMaxDate);
			sv.trancd.set(SPACE);
			sv.covrprpse.set(SPACE);
			sv.validflag.set("1");
		}else{
           sv.nbprp126lag.set("N");
		}
		/*IBPLIFE-2137 End*/
	}

protected void contractHeader1020()
	{
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			chdrmjaIO.setFormat(formatsInner.chdrmjarec);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/*BRD-306 START */
		sv.cnttype.set(chdrpf.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		//ILB-456
		//ILIFE-8194 starts
		isCompModify = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM010", appVars, "IT");		
		if(isCompModify && (chdrpf.getCnttype().equals("OIS") || chdrpf.getCnttype().equals("OIR")|| chdrpf.getCnttype().equals("SIS") || chdrpf.getCnttype().equals("SIR"))) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
		}
		else
		{
			sv.instprmOut[varcom.pr.toInt()].set("N");
		}
		//ILIFE-8194 ends
		/*descIO.setDataKey(SPACES);
		descpf.setDescpfx("IT");
		descpf.setDesccoy(wsspcomn.company.toString());
		descpf.setDesctabl(tablesInner.t5688.toString());
		descpf.setDescitem(chdrmjaIO.getCnttype().toString());
		descpf.setLanguage(wsspcomn.language.toString());*/
		descpf=descDAO.getdescData("IT", tablesInner.t5688.toString() ,chdrpf.getCnttype(),wsspcomn.company.toString(), wsspcomn.language.toString());/* IJTI-1386 */
		if (descpf == null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		/*descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}*/
		
		sv.effdate.set(chdrpf.getPtdate());//ICIL-560
		/*BRD-306 END */
		//ILIFE-3399-STARTS
		loadingFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-3399-ENDS
		stampDutyflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		autoIncrflag = FeaConfg.isFeatureExist("2", "CSCOM011", appVars, "IT");
	}
/*IBPLIFE-2137 Start*/
//IBPLIFE-3025 starts
protected void checkCoverPurpose1022(){

	    if (isEQ(wsaaToday, 0)) {
	        datcon1rec.function.set(varcom.tday);
	        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	        wsaaToday.set(datcon1rec.intDate);
	    }
	   
		//sv.dateeff.set(wsspcomn.effdate.toInt());
	    if (isEQ(wsspcomn.flag, "M") || isEQ(wsspcomn.flag, "I") || isEQ(wsspcomn.flag, "P")) {
	    	covppf=covppfDAO.getCovppfCrtable(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getCrtable()
	    			,covrpf.getCoverage(),covrpf.getRider());
	    	
	    	if(covppf != null){
	    		sv.covrprpse.set(covppf.getCovrprpse());
	    		sv.trancd.set(wsaaBatckey.batcBatctrcde);
	    		descpf=descDAO.getdescData("IT", "T1688", wsaaBatckey.batcBatctrcde.toString(), 
	    				wsspcomn.company.toString(), wsspcomn.language.toString());
	    	    if (null!=descpf) {
	    	        sv.trandesc.set(descpf.getLongdesc());
	    	    } else{
	    	    	 sv.trandesc.set("?");
	    	    }
	    	    sv.effdatex.set(wsaaToday);
	    	}
	    }
	}
	//end
	// iplife-3025 ends
/*IBPLIFE-2137 End*/
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();	//ILIFE-2181 /* IJTI-1386 START*/
	rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
	rcvdPFObject.setChdrnum(covrpf.getChdrnum());
	rcvdPFObject.setLife(covrpf.getLife());
	rcvdPFObject.setCoverage(covrpf.getCoverage());
	rcvdPFObject.setRider(covrpf.getRider());
	rcvdPFObject.setCrtable(covrpf.getCrtable());
	/* IJTI-1386 END*/
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}

	/**
	 * fwang3 ICIL-560
	 */
	private void processCashValue() {
		chinaLocalPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM007", appVars, "IT");
		if(chinaLocalPermission) {
			List<Itempf> items = itemDAO.getItemsbyItemcodeLikeOperator("IT", wsspcomn.company.toString(), tablesInner.th528.toString(), covrpf.getCrtable());/* IJTI-1386 */
			if (items.isEmpty()) {
				sv.cashvalarerOut[varcom.nd.toInt()].set("Y");
				hasCashValue = false;
			} else {
				loadTh528(items);
				hasCashValue = true;
			}
		} else {
			sv.cashvalarerOut[varcom.nd.toInt()].set("Y");
		}
	}
	
	/**
	 * @author fwang3
	 * ICIL-560
	 * @param items
	 */
	private void loadTh528(List<Itempf> items) {
		String item4parts = covrpf.getCrtable() + "" + covrpf.getAnbAtCcd() + "" + covrpf.getMortcls()	+ "" + covrpf.getSex();
		String item2parts = covrpf.getCrtable() + "" + covrpf.getAnbAtCcd() + "**";
		for (Itempf item : items) {
			if((new Integer(covrpf.getCrrcd()).compareTo(new Integer(item.getItmfrm().toString()))) >= 0){
				if (item.getItemitem().equals(item4parts)) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
				if (item.getItemitem().equals(item2parts)) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
				if (item.getItemitem().equals(covrpf.getCrtable() + "****")) {
					th528rec.th528Rec.set(StringUtil.rawToString(item.getGenarea()));
					return;
				}
			}
		}
	}

protected void contractCoverage1030()
	{
		//ILB-456
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");	
		}
		else{
			// PINNACLE-1433 added null check for stamp duty
			if(null != covrpf.getZstpduty01() && isNE(covrpf.getZstpduty01(),ZERO)){
				sv.zstpduty01.set(covrpf.getZstpduty01());
			}
		}
	    processCashValue();//fwang3 ICIL-560
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
		/* MTL002 */
			else {
				if (isNE(t2240rec.agecode04, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit04);
				}
			}
			/* MTL002 */
			}
		}
		/* Read TR52D for TaxCode.                                         */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		exclFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}
		/* Read the PAYR record to get the Billing Details.                */
		//ILB-456
		payrpf = payrpfDAO.getpayrRecordByCoyAndNumAndSeq(wsspcomn.company.toString(),chdrpf.getChdrnum(), covrpf.getPayrseqno(),"1" );/* IJTI-1386 */
		if(payrpf == null) {
			syserrrec.params.set(payrpf);
			fatalError600();
		}
		/*payrpf.setChdrcoy(wsspcomn.company);
		payrpf.setChdrnum(chdrmjaIO.getChdrnum());
		payrpf.setPayrseqno(covrmjaIO.getPayrseqno());
		payrpf.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}*/
		if (isEQ(payrpf.getBillfreq(),ZERO)) {
			wsaaEffDate.set(datcon1rec.intDate);
		}
		else {
			wsaaEffDate.set(payrpf.getBtdate());
		}
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		wsaaCrtable.set(covrpf.getCrtable());
		if (isEQ(wsaaPlanSuffix,ZERO)) {
			covrpfDAO.deleteCacheObject(covrpf);
		}
		
		initializeScreenCustomerSpecific();
		
		if (modifyComp.isTrue()
		&& isEQ(payrpf.getBillfreq(),ZERO)) {
			wsaaFlag.set("I");
			wsccSingPremFlag.set("Y");
		}
		dialdownFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		premiumflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(premiumflag || dialdownFlag){
		callReadRCVDPF();
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		//BRD-NBP-011 ends
		/*ILIFE-7118-starts*/
		tpdtypeFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(),TPD_FEATURE_ID, appVars, "IT");	
		
		if((tpdtypeFlag) && (tpd1.equals(covrpf.getCrtable().trim())||tps1.equals(covrpf.getCrtable().trim()))) {
			sv.tpdtypeOut[varcom.nd.toInt()].set("N");
		}else {
			sv.tpdtypeOut[varcom.nd.toInt()].set("Y");
		}
		/*ILIFE-7118-ends*/
		
		// ILIFE-8194 starts
		if (isCompModify) {
			lifepf1 = lifepfDAO.getLifeEnqRecord(wsspcomn.company.toString(), covrpf.getChdrnum(),
					covrpf.getLife(), "00");/* IJTI-1386 */
			if (null != lifepf) {
				sv.mortcls.set(lifepf1.getSmoking());
			}
			if(chdrpf.getCnttype().equals("LPS") || chdrpf.getCnttype().equals("SLS")|| chdrpf.getCnttype().equals("RUL")) {  //ILIFE-8433
				sv.mortclsOut[varcom.pr.toInt()].set("N");
			}else {
				sv.mortclsOut[varcom.pr.toInt()].set("Y");
			}
		}
		// ILIFE-8194 ends
        /*ILIFE-8248 start*/
		
		lnkgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(), "NBPRP055", appVars, "IT");

		if (lnkgFlag == true) {
			sv.lnkgnoOut[varcom.nd.toInt()].set("Y");
			sv.lnkgsubrefnoOut[varcom.nd.toInt()].set("Y");
			
			sv.lnkgno.set(covrpf.getLnkgno());
			sv.lnkgsubrefno.set(covrpf.getLnkgsubrefno());

		}
		/*ILIFE-8248 end*/
	}
protected void checkExcl()
{
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getCrtable(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());/* IJTI-1386 */
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
}

protected void headerToScreen1040()
	{
	//ILB-456
		descpf=descDAO.getdescData("IT", tablesInner.t5687.toString(), covrpf.getCrtable(), wsspcomn.company.toString(),  wsspcomn.language.toString());
		if (descpf == null) {
			wsaaHedline.fill("?");
		}
		else {
		wsaaHedline.set(descpf.getLongdesc());
		}
		/*descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrmjaIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}*/
		loadHeading1100();
		if(autoIncrflag)
		if(autoIncrflag)
		{
			itempf = new Itempf();
			itempf.setItempfx(itemPFX);
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.t5687.toString());
			itempf.setItemitem(covrpf.getCrtable());
			itempf = itemDAO.getItempfRecord(itempf);
			if (itempf != null) {
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			itempf = new Itempf();
			itempf.setItempfx(itemPFX);
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.t6658.toString());
			itempf.setItemitem(t5687rec.anniversaryMethod.toString());
			itempf = itemDAO.getItempfRecord(itempf);
			if (itempf != null) {
				t6658rec.t6658Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}
	}

protected void lifeDetails1050()
	{
		//ILB-456
		/*lifepf.setChdrcoy(covrmjaIO.getChdrcoy().toString());
		lifepf.setChdrnum(covrmjaIO.getChdrnum().toString());
		lifepf.setLife(covrmjaIO.getLife().toString());
		lifepf.setJlife("00");
		
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}*/
		lifepf = lifepfDAO.getLifeRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),"00");/* IJTI-1386 */
		if (lifepf== null ) {
			fatalError600();
		}
		calcAge1200();
		wsaaAnbAtCcd.set(wsaaWorkingAnb);
		if (!addComp.isTrue()) {
			wsaaAnbAtCcd.set(lifepf.getAnbAtCcd());
			if(effdateFlag){
				wsaaAnbAtCcd.set(covrpf.getAnbAtCcd());
			}
		}
		wsaaCltdob.set(lifepf.getCltdob());
		wsaaSex.set(lifepf.getCltsex());
		//ILB-546
		/*cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifepf.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		clntpf = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),lifepf.getLifcnum());
		if(clntpf==null){
			syserrrec.params.set(clntpf);
			fatalError600();
		}
		sv.lifcnum.set(lifepf.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		//ILB-456
		List<Lifepf> lifepfList = lifepfDAO.getLifeData(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), "01");/* IJTI-1386 */
		if (lifepfList.isEmpty()) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbAnbAtCcd.set(0);
			wsaaLifeind.set("S");
			wsbbSex.set(SPACES);
			goTo(GotoLabel.setScreen1060);
		}
		/*lifepf.setChdrcoy(covrmjaIO.getChdrcoy().toString());
		lifepf.setChdrnum(covrmjaIO.getChdrnum().toString());
		lifepf.setLife(covrmjaIO.getLife().toString());
		lifepf.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbAnbAtCcd.set(0);
			wsaaLifeind.set("S");
			wsbbSex.set(SPACES);
			goTo(GotoLabel.setScreen1060);
		}*/
		else {
			wsaaLifeind.set("J");
		}
		if (addComp.isTrue()) {
			calcAge1200();
			wsbbAnbAtCcd.set(wsaaWorkingAnb);
		}
		else {
			wsbbAnbAtCcd.set(lifepf.getAnbAtCcd());
		}
		wsbbCltdob.set(lifepf.getCltdob());
		wsbbSex.set(lifepf.getCltsex());
		//ILB-456
		/*cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifepf.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		clntpf = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),lifepf.getLifcnum());
		if(clntpf==null){
			syserrrec.params.set(clntpf);
			fatalError600();
		}
		sv.jlifcnum.set(lifepf.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void setScreen1060()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.numpols.set(chdrpf.getPolinc());
		sv.life.set(lifepf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		sv.currcd.set(payrpf.getCntcurr());
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)
		&& !addComp.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			covrpf.setPlanSuffix(9999);
			covtmjaIO.setPlanSuffix(9999);
			covrpfList=covrpfDAO.searchCovrRecordForContract(covrpf);
			covtmjaIO.setFunction(varcom.begn);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanproc.set("Y");
			if (addComp.isTrue()) {
				/*NEXT_SENTENCE*/
				/**                                   COVRMJA-RIDER*/
			}
		}
		else {
			wsaaPlanproc.set("N");
			covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
			covtmjaIO.setFunction(varcom.readr);
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		// ILIFE - 4167 Start
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		// ILIFE - 4167 END
		if(autoIncrflag)
		{	
			if(isEQ(t6658rec.incrFlg,"N") || isEQ(t6658rec.incrFlg,SPACES)) {
				if (modifyComp.isTrue()
				&& isNE(covrpf.getCpiDate(), ZERO)
				&& isNE(covrpf.getCpiDate(), varcom.vrcmMaxDate)) {
					scrnparams.errorCode.set(errorsInner.f862);
				}
			}
		}
		else
		{
			if (modifyComp.isTrue()
					&& isNE(covrpf.getCpiDate(), ZERO)
					&& isNE(covrpf.getCpiDate(), varcom.vrcmMaxDate)) {
						scrnparams.errorCode.set(errorsInner.f862);
					}
		}
		/*IBPLIFE-2137 Start*/
		if(covrprpseFlag){
			checkCoverPurpose1022();
		}
		/*IBPLIFE-2137 End*/
		policyLoad5000();
	}

protected void commisionSplit1070()
	{
		checkPcdt5700();
		if (isNE(tr52drec.txcode, SPACES)
		&& isEQ(t5687rec.bbmeth, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("N");
			sv.linstamtOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void loadHeading1100()
	{
		try {
			loadScreen1110();
		}
		catch (GOTOException e){
		}
	}

protected void loadScreen1110()
	{
		wsaaHeading.set(SPACES);
		for (wsaaX.set(30); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		/*VARYING WSAA-X           FROM 24 BY -1                    */
		/*SUBTRACT WSAA-X             FROM 24 GIVING WSAA-Y.           */
		compute(wsaaY, 0).set(sub(30,wsaaX));
		if (isNE(wsaaY,0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		wsaaZ.set(0);
		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1130();
		}
		/* WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1190);
	}

protected void moveChar1130()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void calcAge1200()
	{
		init1210();
	}

protected void init1210()
	{
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(lifepf.getCltdob());
		agecalcrec.intDate2.set(wsaaEffDate);
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaWorkingAnb.set(agecalcrec.agerating);
	}

protected void setupBonus1300()
	{
			para1300();
		}

protected void para1300()
	{
		sv.bappmethOut[varcom.pr.toInt()].set("N");
		sv.bappmethOut[varcom.nd.toInt()].set("N");
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covtmjaIO.getChdrcoy());
		itemIO.setItemitem(covtmjaIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
			return ;
		}
		t6005rec.t6005Rec.set(itemIO.getGenarea());
		if (isEQ(t6005rec.ind,"1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
	
	if (isEQ(wsspcomn.sbmaction,"D")) {  // Ticket #ILIFE-7244
		scrnparams.function.set(varcom.prot);
	}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (singlePremium.isTrue()) {
			scrnparams.errorCode.set(errorsInner.f008);
		}
		wsaaOldSumins.set(sv.sumin);
		if (isEQ(sv.instPrem,ZERO))
		wsaaOldPrem.set(premiumrec.calcPrem);
		else
			wsaaOldPrem.set(sv.instPrem);
			
		readPovr8000();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(),wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(),wsaaPovrLife)
		|| isNE(povrIO.getCoverage(),wsaaPovrCoverage)
		|| isNE(povrIO.getRider(),wsaaPovrRider)) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
		}
		if (isEQ(sv.rcesdteOut[varcom.pr.toInt()],"Y")
		&& !compEnquiry.isTrue()
		&& (isNE(sv.pcesdteErr,SPACES)
		|| isNE(sv.rcesdteErr,SPACES))) {
			sv.premCessDate.set(varcom.vrcmMaxDate);
			sv.riskCessDate.set(varcom.vrcmMaxDate);
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			a200CheckCalcTax();
		}
	}

protected void callScreenIo2010()
	{
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
					if(occupationFlag) checkOccupation();	//ICIL-1310
					validate2020();
				case cont2030: 
					cont2030();
				case beforeExit2080: 
					beforeExit2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'S5127IO' USING        SCRN-SCREEN-PARAMS               */
		/*                                S5127-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		wsaaIfSuminsChanged.set("N");
		wsaaIfPremChanged.set("N");
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.cont2030);
		}
		if (isEQ(sv.rcesdteOut[varcom.pr.toInt()],"Y")
		&& (isNE(sv.premCessAge,wsaaPcesageStore)
		|| isNE(sv.riskCessAge,wsaaRcesageStore)
		|| isNE(sv.premCessTerm,wsaaPcestermStore)
		|| isNE(sv.riskCessTerm,wsaaRcestermStore))) {
			sv.premCessDate.set(varcom.vrcmMaxDate);
			sv.riskCessDate.set(varcom.vrcmMaxDate);
		}
	}

	//ICIL-1310 Starts
	protected void checkOccupation()	{
		occup = lifepfDAO.getOccRecord(wsspcomn.company.toString(), sv.chdrnum.toString().trim(), sv.life.toString().trim());
		if (occup != null && !occup.trim().equals("")) {
			itempf = new Itempf();
			itempf.setItempfx(itemPFX);
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.ta610.toString());
			itempf.setItemitem(covrpf.getCrtable().trim());
			itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
			itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
			ta610List = itemDAO.findByItemDates(itempf);	//ICIL-1494
			if (ta610List.size()>0 && ta610List.get(0).getGenarea()!=null) {
				ta610rec.tA610Rec.set(StringUtil.rawToString(ta610List.get(0).getGenarea()));
				for (int i = 1; i <= 10; i++) {
					if (isEQ(ta610rec.occclassdes[i], occup)) {
						scrnparams.errorCode.set(errorsInner.rrsu);
						return;
					}
				}
				getOccupationClass();
				if(occupClass!=null) {
					for (int i = 1; i <= 10; i++) {
						if (isEQ(ta610rec.occcode[i], occupClass)) {
							scrnparams.errorCode.set(errorsInner.rrsu);
							return;
						}
					}
				}
			}
		}
	}
	protected void getOccupationClass() {
		itempf = new Itempf();
		itempf.setItempfx(itemPFX);
		itempf.setItemcoy(wsspcomn.fsuco.toString());
		itempf.setItemtabl(tablesInner.t3644.toString());
		itempf.setItemitem(occup.trim());
		itempf = itemDAO.getItempfRecord(itempf);
		if (itempf != null) {
			t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			occupClass = t3644rec.occupationClass.toString();
		}
	}
	//ICIL-1310 End

protected void validate2020()
	{
		a100CheckLimit();
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			editCoverage2100();
		}
	}

/**
 * fwang3 ICIL-560
 */
private void processCashValueInArrears() {
	// fwang3 ICIL-560 judge if the component has cash value and than code is come from submenu
	if (chinaLocalPermission && isEQ(wsaaFlag, "M") && hasCashValue && isEQ("T555", wsaaBatckey.batcBatctrcde)) {
		if (isLT(sv.sumin, wsaaCashValArr)) {
			sv.suminErr.set(errorsInner.rrj4);// ICIL-299
		}
		sv.cashvalarer.set(ZERO);
		if (isGT(sv.sumin, wsaaHoldSumin)) {
			int years = DateUtils.calYears(chdrpf.getOccdate().toString(), chdrpf.getPtdate().toString());
			PackedDecimalData data = new PackedDecimalData(11, 5).init(100000);
			compute(sv.cashvalarer,2).setRounded(mult(div(th528rec.insprm[years + 1], data), sub(sv.sumin, wsaaHoldSumin)));//fix ICIL-706
		}
	}
}

protected void cont2030()
	{
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind,"+")
		&& isNE(sv.optextind,"X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		if (isNE(sv.comind, " ")
		&& isNE(sv.comind,"+")
		&& isNE(sv.comind,"X")) {
			sv.comindErr.set(errorsInner.g620);
		}
		/* Check the premium breakdown indicator.                          */
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind,"+")
		&& isNE(sv.pbind,"X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		/* Check the Taxcode indicator.                                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		/* If  reassurance   already exists, there will be a '+' in this   */
		/* field.  A  request  to access the details is made by entering   */
		/* 'X'.  No  other  values  (other  than  blank) are allowed.      */
		/* IF S5127-RATYPIND           NOT = ' ' AND            <R96REA>*/
		/*                             NOT = '+' AND            <R96REA>*/
		/*                             NOT = 'X'                <R96REA>*/
		/*    MOVE G620                TO S5127-RATYPIND-ERR.   <R96REA>*/
		/*                                                      <R96REA>*/
		/* If Component Enquiry then skip validation.                      */
		/*    IF  COMP-ENQUIRY                                     <V4L004>*/
		if(isEQ(sv.dialdownoptionErr,"E034") && !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		if (exclFlag && isNE(sv.exclind, " ")
				&& isNE(sv.exclind, "+")
				&& isNE(sv.exclind, "X")) {
					sv.exclindErr.set(errorsInner.g620);
				}
		
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.beforeExit2080);
		}
		if (isNE(sv.sumin,wsaaOldSumins)) {
			compute(wsaaSuminsDiff, 0).set(sub(sv.sumin,wsaaOldSumins));
			if (isLT(wsaaSuminsDiff,0)) {
				compute(wsaaSuminsDiff, 0).set(mult(wsaaSuminsDiff,-1));
				wsaaIfSuminsChanged.set("D");
			}
			else {
				wsaaIfSuminsChanged.set("I");
			}
		}
		if (isNE(sv.instPrem,wsaaOldPrem)) {
			compute(wsaaPremDiff, 2).set(sub(sv.instPrem,wsaaOldPrem));
			if (isLT(wsaaPremDiff,0)) {
				compute(wsaaPremDiff, 2).set(mult(wsaaPremDiff,-1));
				wsaaIfPremChanged.set("D");
			}
			else {
				wsaaIfPremChanged.set("I");
			}
		}
		processCashValueInArrears();// fwang3 ICIL-560				
		if (isNE(sv.optextind,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& addComp.isTrue()) {
			calcPremium2200();
		}
		if (isNE(sv.optextind,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& isNE(sv.comind,"X")
		&& modifyComp.isTrue()) {
			calcPremium2200();
		}
	}

private void checkSusPrem() {
	if(!(chinaLocalPermission&&(compApp.isTrue()||modifyComp.isTrue()))) {
		return;
	}
	List<Itempf> items = itemDAO.getAllItemitem("IT", chdrpf.getChdrcoy().toString(), t5645, wsaaProg.toString());
	if(null == items || items.isEmpty()) {
		syserrrec.params.set(t5645);
		fatalError600();
	}
	t5645rec.t5645Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	Acblpf pf = acblDao.loadDataByBatch(chdrpf.getChdrcoy().toString(), t5645rec.sacscode01.toString(), 
			chdrpf.getChdrnum(), chdrpf.getCntcurr(), t5645rec.sacstype01.toString());/* IJTI-1386 */
	if (pf == null || pf.getSacscurbal() == null) {
	//	syserrrec.params.set(chdrpf.getParams());
		fatalError600();
	}
	if (isLT(mult(pf.getSacscurbal(), -1), add(sv.linstamt, sv.cashvalarer))) {
		sv.cashvalarerErr.set(errorsInner.e751);
		wsspcomn.edterror.set("Y");
		return;
	}
	postings();
}

private void postings()
{
	lifacmvrec1.lifacmvRec.set(SPACES);
	lifacmvrec1.function.set("PSTW");
	/* Read T1688 for transaction code description.                 */
	Descpf descpf = descDAO.getdescData("IT", "T1688", wsaaBatckey.batcBatctrcde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	lifacmvrec1.trandesc.set(descpf==null?"":descpf.getLongdesc());
	wsaaBatckey.set(wsspcomn.batchkey);
	lifacmvrec1.batccoy.set(wsaaBatckey.batcBatccoy);
	lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
	lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
	lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
	lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
	lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
	lifacmvrec1.rdocnum.set(chdrpf.getChdrnum());
	compute(lifacmvrec1.tranno, 0).set(chdrpf.getTranno() + 1);
	lifacmvrec1.rldgcoy.set(chdrpf.getChdrcoy());
	lifacmvrec1.origcurr.set(chdrpf.getCntcurr());
	lifacmvrec1.tranref.set(chdrpf.getChdrnum());
	lifacmvrec1.crate.set(ZERO);
	lifacmvrec1.genlcoy.set(chdrpf.getChdrcoy());
	lifacmvrec1.genlcur.set(chdrpf.getCntcurr());
	lifacmvrec1.effdate.set(datcon1rec.intDate.toInt());
	lifacmvrec1.rcamt.set(ZERO);
	lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec1.transactionDate.set(ZERO);
	lifacmvrec1.transactionTime.set(ZERO);
	lifacmvrec1.user.set(varcom.vrcmUser);
	lifacmvrec1.termid.set(varcom.vrcmTermid);
	lifacmvrec1.substituteCode[1].set(chdrpf.getCnttype());
	lifacmvrec1.substituteCode[6].set(covrpf.getCrtable());
	
	List<Itempf> itdmpfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), tablesInner.t5688.toString(), 
			chdrpf.getCnttype(), chdrpf.getOccdate());/* IJTI-1386 */

	if(itdmpfList == null || itdmpfList.isEmpty()){
		scrnparams.errorCode.set(errorsInner.f290);
		wsspcomn.edterror.set("Y");
	} else {
		t5688rec.t5688Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
	} 
	
	lifacmvrec1.rldgacct.set(chdrpf.getChdrnum());//ICIL-1007
	lifacmvrec1.sacscode.set(t5645rec.sacscode[1]);
	lifacmvrec1.sacstyp.set(t5645rec.sacstype[1]);
	lifacmvrec1.glcode.set(t5645rec.glmap[1]);
	lifacmvrec1.glsign.set(t5645rec.sign[1]);
	lifacmvrec1.contot.set(t5645rec.cnttot[1]);
	lifacmvrec1.origamt.set(sv.cashvalarer); 
	lifacmvrec1.acctamt.set(sv.cashvalarer);
	lifacmvrec1.jrnseq.set(1);
	
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec1.statuz);
		return ; 
	}
	
	//ICIL-1007 start
	/* For component level accounting, the whole entity field       */
	/* LIFA-RLDGACCT must be filled.  For contract level accounting */
	/* only the contract header no. is needed in the entity field   */
	/* LIFA-RLDGACCT.                                              */
	StringBuilder wsaaRldgacct = new StringBuilder(chdrpf.getChdrnum());/* IJTI-1386 */
	if (isEQ(t5688rec.comlvlacc, "Y")) {
		wsaaRldgacct.append(covrpf.getLife());
		wsaaRldgacct.append(covrpf.getCoverage());
		wsaaRldgacct.append(covrpf.getRider());
		
		ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
		FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
		ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		wsaaPlan.set(covrpf.getPlanSuffix());
		
		wsaaRldgacct.append(wsaaPlansuff);
	}
	lifacmvrec1.rldgacct.set(wsaaRldgacct);
	//ICIL-1007 end
	lifacmvrec1.sacscode.set(t5645rec.sacscode[2]);
	lifacmvrec1.sacstyp.set(t5645rec.sacstype[2]);
	lifacmvrec1.glcode.set(t5645rec.glmap[2]);
	lifacmvrec1.glsign.set(t5645rec.sign[2]);
	lifacmvrec1.contot.set(t5645rec.cnttot[2]);
	lifacmvrec1.origamt.set(sv.cashvalarer); 
	lifacmvrec1.acctamt.set(sv.cashvalarer);
	lifacmvrec1.jrnseq.set(2);
	
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec1.statuz);
	}
 }
protected void beforeExit2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void editCoverage2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkSumin2110();
				case checkSuminCont2120: 
					checkSuminCont2120();
				case checkRcessFields2120: 
					checkRcessFields2120();
					checkPcessFields2130();
					checkAgeTerm2140();
				case validDate2140: 
					validDate2140();
				case datesCont2140: 
					datesCont2140();
				case ageAnniversary2140: 
					ageAnniversary2140();
				case check2140: 
					check2140();
				case checkOccurance2140: 
					checkOccurance2140();
				case checkTermFields2150: 
					checkTermFields2150();
				case checkComplete2160: 
					checkComplete2160();
				case checkMortcls2170: 
					checkMortcls2170();
				case loop2175: 
					loop2175();
				case checkLiencd2180: 
					checkLiencd2180();
				case loop2185: 
					loop2185();
				case checkMore2190: 
					checkMore2190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkSumin2110()
	{
		if (isEQ(t5608rec.sumInsMax,t5608rec.sumInsMin)) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkRcessFields2120);
		}
		if (isEQ(sv.sumin,covrpf.getSumins())) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkSuminCont2120);
		}
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin,chdrpf.getPolinc()),chdrpf.getPolsum())));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
	}

protected void checkSuminCont2120()
	{
		if (isLT(wsaaSumin,t5608rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin,t5608rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

protected void checkRcessFields2120()
	{
		if (isEQ(sv.select,"J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent,SPACES)
		&& jointlif.isTrue()) {
			checkDefaults5300();
		}
		if (isGT(sv.riskCessAge,0)
		&& isGT(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(t5608rec.eaage,SPACES)
		&& isEQ(sv.riskCessAge,0)
		&& isEQ(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2130()
	{
		if (isGT(sv.premCessAge,0)
		&& isGT(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)
		&& isEQ(sv.rcessageErr,SPACES)
		&& isEQ(sv.rcesstrmErr,SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5608rec.eaage,SPACES)
		&& isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2140()
	{
		if ((isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))
		|| (isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if ((isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))
		|| (isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if (isEQ(sv.riskCessDate,varcom.vrcmMaxDate)) {
			riskCessDate5400();
		}
		if (isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			premCessDate5450();
		}
		
		//start ILIFE-1238		
		if (modifyComp.isTrue()){
			if ((isNE(sv.riskCessTerm,covtmjaIO.getRiskCessTerm())) 
			|| (isNE(sv.premCessTerm,covtmjaIO.getPremCessTerm()))) {
				covtmjaIO.setRiskCessTerm(sv.riskCessTerm);
				covtmjaIO.setPremCessTerm(sv.premCessTerm);
				riskCessDate5400();
				premCessDate5450();
			}
		}				
		//end ILIFE-1238
		
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if (isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate,sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		if (isEQ(sv.rider,"00")) {
			goTo(GotoLabel.datesCont2140);
		}
		wsaaStorePlanSuffix.set(covrpf.getPlanSuffix());
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setCoverage(covrpf.getCoverage());
		covtmjaIO.setRider("00");
		covtmjaIO.setSeqnbr(ZERO);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(covtmjaIO.getLife(),covrpf.getLife())
		|| isNE(covtmjaIO.getCoverage(),covrpf.getCoverage())
		|| isNE(covtmjaIO.getRider(),"00")) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.validDate2140);
		}
		covtmjaIO.setAnbccd(1, ZERO);
		covtmjaIO.setAnbccd(2, ZERO);
		covtmjaIO.setSingp(ZERO);
		covtmjaIO.setPlanSuffix(ZERO);
		covtmjaIO.setInstprem(ZERO);
		covtmjaIO.setPremCessAge(ZERO);
		covtmjaIO.setPremCessTerm(ZERO);
		covtmjaIO.setBenCessAge(ZERO);
		covtmjaIO.setBenCessTerm(ZERO);
		covtmjaIO.setReserveUnitsDate(ZERO);
		covtmjaIO.setSumins(ZERO);
		covtmjaIO.setPremCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setRiskCessDate(varcom.vrcmMaxDate);
		if (modifyComp.isTrue()) {
			if (isEQ(chdrpf.getPolsum(),ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isGT(chdrpf.getPolsum(),covrpf.getPlanSuffix())
				|| isEQ(chdrpf.getPolsum(),covrpf.getPlanSuffix())) {
					covrpf.setPlanSuffix(0);
				}
			}
		}
		if (addComp.isTrue()) {
			covrpf.setPlanSuffix(0);
			if (isEQ(chdrpf.getPolsum(),ZERO)) {
				if (isEQ(chdrpf.getPolinc(),1)) {
					covrpf.setPlanSuffix(0);
				}
				else {
					covrpf.setPlanSuffix(1);
				}
			}
			else {
				covrpf.setPlanSuffix(0);
			}
		}
		//ILB-456
		covrpf.setRider("00");
		covrpf=covrpfDAO.getCovrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),"1");
		if (covrpf == null) {
			fatalError600();
		}
		/*covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
		covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
	}

protected void validDate2140()
	{
		if (isGT(sv.riskCessDate,covtmjaIO.getRiskCessDate())) {
			sv.rcesdteErr.set(errorsInner.h033);
		}
		if (isGT(sv.premCessDate,covtmjaIO.getPremCessDate())) {
			sv.pcesdteErr.set(errorsInner.h044);
		}
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(sv.life);
		covtmjaIO.setCoverage(sv.coverage);
		covtmjaIO.setRider(sv.rider);
		covtmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		//ILB-456
		covrpf.setChdrcoy(covtmjaIO.getChdrcoy().toString());
		covrpf.setChdrnum(covtmjaIO.getChdrnum().toString());
		covrpf.setLife(sv.life.toString());
		covrpf.setCoverage(sv.coverage.toString());
		covrpf.setRider(sv.rider.toString());
		covrpf.setPlanSuffix(wsaaStorePlanSuffix.toInt());
		covrpfList = covrpfDAO.searchCovrpfRecord(covrpf);
		if (covrpfList.isEmpty()) {
			goTo(GotoLabel.datesCont2140);
		}
		/*covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
	}

protected void datesCont2140()
	{
		if (isEQ(sv.rider,"00")
		&& modifyComp.isTrue()) {
			covrrgwIO.setParams(SPACES);
			covrrgwIO.setChdrcoy(covrpf.getChdrcoy());
			covrrgwIO.setChdrnum(covrpf.getChdrnum());
			covrrgwIO.setLife(covrpf.getLife());
			covrrgwIO.setCoverage(covrpf.getCoverage());
			covrrgwIO.setRider(covrpf.getRider());
			covrrgwIO.setPlanSuffix(covrpf.getPlanSuffix());
			covrrgwIO.setFormat(formatsInner.covrrgwrec);
			covrrgwIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, covrrgwIO);
			if (isNE(covrrgwIO.getStatuz(),varcom.oK)
			&& isNE(covrrgwIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrrgwIO.getParams());
				syserrrec.statuz.set(covrrgwIO.getStatuz());
				fatalError600();
			}
			while ( !(isEQ(covrrgwIO.getStatuz(),varcom.endp))) {
				riderCessation2140();
			}
			
		}
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)) {
			goTo(GotoLabel.checkMortcls2170);
		}
		if (isEQ(t5608rec.eaage,"A")) {
			goTo(GotoLabel.ageAnniversary2140);
		}
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		goTo(GotoLabel.check2140);
	}

protected void ageAnniversary2140()
	{
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(wsaaEffDate);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			compute(wszzRiskCessAge, 5).set(add(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(wsaaEffDate);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			compute(wszzPremCessAge, 5).set(add(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
	}

protected void check2140()
	{
		if (addComp.isTrue()) {
			datcon3rec.intDate1.set(wsaaEffDate);
		}
		else {
			datcon3rec.intDate1.set(covrpf.getCrrcd());
		}
		if (isEQ(sv.riskCessTerm,ZERO)) {
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm,ZERO)) {
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		x.set(0);
	}

protected void checkOccurance2140()
	{
		x.add(1);
		if (isGT(x,wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2160);
		}
		if ((isEQ(t5608rec.ageIssageFrm[x.toInt()],0)
		&& isEQ(t5608rec.ageIssageTo[x.toInt()],0))
		|| (isLT(wszzAnbAtCcd,t5608rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,t5608rec.ageIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkTermFields2150);
		}
		if (isGTE(wszzRiskCessAge,t5608rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge,t5608rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge,t5608rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge,t5608rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2150()
	{
		if ((isEQ(t5608rec.termIssageFrm[x.toInt()],0)
		&& isEQ(t5608rec.termIssageTo[x.toInt()],0))
		|| (isLT(wszzAnbAtCcd,t5608rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,t5608rec.termIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkOccurance2140);
		}
		if (isGTE(wszzRiskCessTerm,t5608rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm,t5608rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm,t5608rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm,t5608rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2140);
	}

protected void checkComplete2160()
	{
		if (isNE(sv.rcesstrmErr,SPACES)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr,SPACES)
		&& isEQ(sv.premCessTerm,ZERO)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr,SPACES)
		&& isEQ(sv.riskCessAge,ZERO)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr,SPACES)
		&& isEQ(sv.premCessAge,ZERO)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2170()
	{
		if (isEQ(sv.mortclsOut[varcom.pr.toInt()],"Y")) {
			goTo(GotoLabel.checkLiencd2180);
		}
		if(sv.mortcls.equals(SPACES)) {
			sv.mortclsErr.set(errorsInner.e420);
		}
		x.set(0);
	}

protected void loop2175()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2180);
		}
		if (isNE(t5608rec.mortcls[x.toInt()],sv.mortcls)) {
			goTo(GotoLabel.loop2175);
		}
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
			if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
				sv.prmbasisErr.set(errorsInner.e186);
	}
}
protected void checkLiencd2180()
	{
		if (isEQ(sv.liencdOut[varcom.pr.toInt()],"Y")
		|| isEQ(sv.liencd,SPACES)) {
			goTo(GotoLabel.checkMore2190);
		}
		x.set(0);
	}

protected void loop2185()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2190);
		}
		if (isEQ(t5608rec.liencd[x.toInt()],SPACES)
		|| isNE(t5608rec.liencd[x.toInt()],sv.liencd)) {
			goTo(GotoLabel.loop2185);
		}
	}

protected void checkMore2190()
	{
		if (isNE(sv.bappmeth,SPACES)
		&& isNE(sv.bappmeth,t6005rec.bappmeth01)
		&& isNE(sv.bappmeth,t6005rec.bappmeth02)
		&& isNE(sv.bappmeth,t6005rec.bappmeth03)
		&& isNE(sv.bappmeth,t6005rec.bappmeth04)
		&& isNE(sv.bappmeth,t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			if (isNE(sv.select,SPACES)
			&& isNE(sv.select,"J")
			&& isNE(sv.select,"L")) {
				sv.selectErr.set(errorsInner.h039);
			}
		}
		/*Z0-EXIT*/
		if(premiumflag){
			checkIPPmandatory();
			checkPremiumBasisfield();
		}
}		
protected void checkPremiumBasisfield()
{
	boolean t5608Flag=false;
	for(int counter=1; counter<t5608rec.prmbasis.length;counter++){
		if(isNE(sv.prmbasis,SPACES)){
			if (isEQ(t5608rec.prmbasis[counter], sv.prmbasis)){
				t5608Flag=true;
				break;
			}
		}
	}
	if(!t5608Flag && isNE(sv.prmbasis,SPACES)){
		sv.prmbasisErr.set("RFV1");// new error code
	} 
}


protected void riderCessation2140()
	{
			para2140();
		}

protected void para2140()
	{
		if (isNE(covrrgwIO.getChdrcoy(),wsaaCovrChdrcoy)
		|| isNE(covrrgwIO.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(covrrgwIO.getLife(),wsaaCovrLife)
		|| isNE(covrrgwIO.getCoverage(),wsaaCovrCoverage)) {
			covrrgwIO.setStatuz(varcom.endp);
			return ;
		}
		/* IBPLIFE-7399 STARTS BY LOHITH */
		if (isEQ(covrrgwIO.getCoverage(), "01")
		&& isEQ(covrrgwIO.getRider(), "00")) {
				covrrgwIO.setStatuz(varcom.endp);
				return;
		}
		/* IBPLIFE-7399 ENDS*/
		if (isGT(covrrgwIO.getRiskCessDate(),sv.riskCessDate)) {
			sv.rcesdteErr.set(errorsInner.h033);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isGT(covrrgwIO.getPremCessDate(),sv.premCessDate)) {
			sv.pcesdteErr.set(errorsInner.h044);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrrgwIO.getStatuz(),varcom.endp)) {
			return ;
		}
		covrrgwIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrrgwIO);
		if (isNE(covrrgwIO.getStatuz(),varcom.oK)
		&& isNE(covrrgwIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrrgwIO.getParams());
			syserrrec.statuz.set(covrrgwIO.getStatuz());
			fatalError600();
		}
	}

protected void calcPremium2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2210();
					callSubr2220();
					adjustPrem2225();
				case cont: 
					cont();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2210()
	{
		if (isNE(t5687rec.bbmeth,SPACES)) {
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(t5675rec.premsubr,SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			wsaaPremStatuz.set("N");
		}
		if (premReqd.isTrue()) {
			if (isEQ(sv.instPrem,0)) {
				sv.instprmErr.set(errorsInner.g818);
			}
			goTo(GotoLabel.exit2290);
		}
		if (isGT(sv.instPrem,0)
		&& premReqd.isTrue()) {
			if (wsaaPremChanged.isTrue()
			&& modifyComp.isTrue()) {
				wsaaOldPrem.set(sv.instPrem);
				wsaaPremStatuz.set("U");
				sv.instprmErr.set(errorsInner.f404);
				goTo(GotoLabel.exit2290);
			}
		}
		if (modifyComp.isTrue()
		&& userPremEntered.isTrue()
		&& premReqd.isTrue()) {
			if (isGT(sv.instPrem,0)) {
				goTo(GotoLabel.exit2290);
			}
		}
		if (isGT(sv.instPrem,0)
		&& premReqd.isTrue()) {
			if (!wsaaPremChanged.isTrue()) {
				goTo(GotoLabel.exit2290);
			}
		}
	}

protected void callSubr2220()
	{
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if (isEQ(sv.select,"J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.effectdt.set(wsaaEffDate);
		premiumrec.termdate.set(sv.premCessDate);
		premiumrec.currcode.set(payrpf.getCntcurr());
		if (isNE(wsaaSumin,ZERO)) {
		}
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.tpdtype.set(sv.tpdtype.charAt(5));//ILIFE-7118
		premiumrec.lnkgind.set(sv.lnkgind);
		premiumrec.billfreq.set(payrpf.getBillfreq());
		premiumrec.mop.set(payrpf.getBillchnl());
		premiumrec.ratingdate.set(chdrpf.getOccdate());
		premiumrec.reRateDate.set(chdrpf.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		if (isNE(premiumrec.calcBasPrem,ZERO)) {
			premiumrec.calcPrem.set(premiumrec.calcBasPrem);
		}
		premiumrec.calcBasPrem.set(sv.instPrem);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.commissionPrem.set(ZERO);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.cnttype.set(chdrpf.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		premiumrec.benCessTerm.set(ZERO);
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		if (modifyComp.isTrue()
		&& isEQ(sv.pbindOut[varcom.nd.toInt()],"N")) {
			premiumrec.function.set("MALT");
			if (isNE(wsaaPovrModified,"Y")) {
				vf2MaltPovr8300();
				wsaaPovrModified = "Y";
			}
		}
		premiumrec.language.set(wsspcomn.language);
		
		//ILIFE-3975
		if(isNE(sv.dialdownoption,SPACES)){
			if(sv.dialdownoption.toString().startsWith("0"))
				premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
			else
				premiumrec.dialdownoption.set(sv.dialdownoption);
		}
		else
			premiumrec.dialdownoption.set("100");

		//ILIFE-3975 end
		
		// ILIFE- 8163 Starts
		if (modifyComp.isTrue()) {
			premiumrec.flag.set("Y");
		} else {
			premiumrec.flag.set("N");
		}
		// ILIFE- 8163 Ends
		
		// ILIFE-8248 - Start

				if (isEQ(sv.lnkgsubrefno, SPACE) || sv.lnkgsubrefno == null)
					premiumrec.lnkgSubRefNo.set(SPACE);
				else {
					premiumrec.lnkgSubRefNo.set(sv.lnkgsubrefno.toString().trim());
				}

				if (isEQ(sv.lnkgno, SPACE)) {
					premiumrec.linkcov.set(SPACE);
				} else {
					LinkageInfoService linkgService = new LinkageInfoService();
					FixedLengthStringData linkgCov = new FixedLengthStringData(
							linkgService.getLinkageInfo(sv.lnkgno.toString()));
					premiumrec.linkcov.set(linkgCov);
				}

		// ILIFE-8248 - ends
		
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())|| t5675rec.premsubr.toString().trim().equals("PMEX")))  // ILIFE-7584
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			/*if(isNE(sv.prmbasis,SPACES)){
				premiumrec.prmbasis.set(sv.prmbasis);
			}*/		
				if(isEQ(sv.prmbasis,"S")){
					premiumrec.prmbasis.set("Y");
				}
				else{
					premiumrec.prmbasis.set(""); 
				}
			
			premiumrec.occpcode.set("");			
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			// ILIFE-7584
			premiumrec.cownnum.set(SPACES);
			premiumrec.occdate.set(ZERO);
			if("PMEX".equals(t5675rec.premsubr.toString().trim())){
				premiumrec.setPmexCall.set("Y");
				premiumrec.batcBatctrcde.set(wsaaBatckey.batcBatctrcde);
				premiumrec.inputPrevPrem.set(ZERO);//ILIFE-8537
				premiumrec.validflag.set("Y");
				if(isNE(clntpf.getClntStateCd(), SPACES))
					premiumrec.rstate01.set(clntpf.getClntStateCd().substring(3).trim());
				else
					premiumrec.rstate01.set(SPACES);
				premiumrec.cownnum.set(chdrpf.getCownnum());
				premiumrec.occdate.set(chdrpf.getOccdate());
			}
			//ILIFE-8502-starts
			premiumrec.commTaxInd.set("Y");
			premiumrec.prevSumIns.set(ZERO);			
			clntDao = DAOFactory.getClntpfDAO();
			clnt=clntDao.getClientByClntnum(clntpf.getClntnum());/* IJTI-1386 */
			if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
				premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
			}else{
				premiumrec.stateAtIncep.set(clntpf.getClntStateCd());/* IJTI-1386 */
			}
			//ILIFE-8502-end
			// ILIFE-7584
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			if (isEQ(premiumrec.statuz,varcom.bomb)) {
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			if (isNE(premiumrec.statuz,varcom.oK)) {
				sv.instprmErr.set(premiumrec.statuz);
				goTo(GotoLabel.exit2290);
			}
	    		
			//ILIFE-7845
			premiumrec.riskPrem.set(ZERO);
			riskPremflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), RISKPREM_FEATURE_ID, appVars, "IT");
			if (riskPremflag) {
				premiumrec.cnttype.set(chdrpf.getCnttype());
				premiumrec.crtable.set(covtmjaIO.getCrtable());
				premiumrec.calcTotPrem.set(add(premiumrec.zstpduty01, premiumrec.calcPrem));
				
			if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
			{
				callProgram("RISKPREMIUM", premiumrec.premiumRec);
			}
			}	
			
			//ILIFE-7845
			if(stampDutyflag){
				compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
				sv.zstpduty01.set(premiumrec.zstpduty01);
			}
		}
		/*Ticket #IVE-792 - End		*/		
		//Ticket #ILIFE-2005 -  end
		if (isEQ(premiumrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2290);
		}
		wsaaPcesageStore.set(sv.premCessAge);
		wsaaRcesageStore.set(sv.riskCessAge);
		wsaaPcestermStore.set(sv.premCessTerm);
		wsaaRcestermStore.set(sv.riskCessTerm);
	}

protected void adjustPrem2225()
	{
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		if (isLTE(payrpf.getMandref(),SPACES)) {
			goTo(GotoLabel.cont);
		}
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrpf.getChdrcoy());
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(payrpf.getChdrnum());
		wsaaPayrseqno.set(payrpf.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		mandIO.setPayrcoy(clrfIO.getClntcoy());
		mandIO.setPayrnum(clrfIO.getClntnum());
		mandIO.setMandref(payrpf.getMandref());
		mandIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		if (isEQ(mandIO.getMandAmt(),ZERO)) {
			goTo(GotoLabel.cont);
		}
		if (isGT(payrpf.getBillfreq(),0)) {
			compute(wsaaCalcPrem, 2).set(sub(covrpf.getInstprem(),(sub(payrpf.getSinstamt06(),sv.instPrem))));
		}
		if (isNE(mandIO.getMandAmt(),wsaaCalcPrem)) {
			sv.instprmErr.set(errorsInner.ev01);
			wsspcomn.edterror.set("Y");
		}
	}

protected void cont()
	{
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/*BRD-306 START */
		
		wsaaCommissionPrem.set(premiumrec.commissionPrem);    //IBPLIFE-5237
		
		sv.loadper.set(premiumrec.loadper);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		/*BRD-306 END */
		if(isEQ(sv.effdate, chdrpf.getPtdate())) {
			sv.linstamt.set(premiumrec.calcPrem);
		} else {
			sv.linstamt.set(ZERO);
		}
		if (isEQ(sv.instPrem,0)) {
			//sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
	if((tpdtypeFlag) && (tpd1.equals(covrpf.getCrtable().trim())||tps1.equals(covrpf.getCrtable().trim()))) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
		else if (isGTE(sv.instPrem,premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
		if (isNE(premiumrec.calcPrem,ZERO)) 
			sv.instPrem.set(premiumrec.calcPrem);
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem,sv.instPrem));
		sv.instprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub,11)
		|| isEQ(sv.instprmErr,SPACES)); wsaaSub.add(1)){
			searchForTolerance2240();
		}
		if (isEQ(sv.instprmErr,SPACES)) {
			if(stampDutyflag){
				compute(sv.zbinstprem, 2).set(sub(sub(sv.instPrem, sv.zlinstprem),sv.zstpduty01));
			}else{
			compute(sv.zbinstprem, 2).set(sub(sv.instPrem,sv.zlinstprem));
			}
		}
		goTo(GotoLabel.exit2290);
	}

protected void searchForTolerance2240()
	{
		if (isEQ(payrpf.getBillfreq(),t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtol[wsaaSub.toInt()])),100));
			if (isLTE(wsaaDiff,wsaaTol)
			&& isLTE(wsaaDiff,t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.instprmErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()],ZERO)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtoln[wsaaSub.toInt()])),100));
				if (isLTE(wsaaDiff,wsaaTol)
				&& isLTE(wsaaDiff,t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.instprmErr.set(SPACES);
				}
			}
		}
		// ILIFE - 4167 Start
				if(AppVars.getInstance().getAppConfig().isVpmsEnable() && incomeProtectionflag && dialdownFlag){
					sv.instprmErr.set(SPACES);
				}
				// ILIFE - 4167 end
	}

protected void callDatcon32300()
	{
		/*CALL*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void adjustSuminsPrem2800()
	{
		/*START*/
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)
		&& isGT(chdrpf.getPolinc(),ZERO)) {
			compute(premiumrec.sumin, 3).setRounded((div(mult(premiumrec.sumin,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		sv.sumin.set(premiumrec.sumin);
		sv.instPrem.set(premiumrec.calcPrem);
		wsaaOldPrem.set(sv.instPrem);
		wsaaOldSumins.set(sv.sumin);
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
				case updatePcdt3060: 
					updatePcdt3060();
					checkSusPrem();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3090);
		}
		if (compEnquiry.isTrue()
		|| revComp.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		if (compApp.isTrue()) {
			goTo(GotoLabel.updatePcdt3060);
		}
		if (isEQ(sv.pbind,"X")) {
			goTo(GotoLabel.exit3090);
		}
		setupCovtmja3100();
		if (isEQ(wsaaLextUpdates,"Y")) {
			wsaaUpdateFlag.set("Y");
		}
		updateCovtmja3200();
		
		if(premiumflag || dialdownFlag)
		{
			insertAndUpdateRcvdpf();
		}
	}
/*IBPLIFE-2137 Start*/
protected void updateCoverPurpose3020(){
		Covppf covpp = covppfDAO.getCovppfCrtable(covtmjaIO.getChdrcoy().toString(), covtmjaIO.getChdrnum().toString(), 
				covtmjaIO.getCrtable().toString(),
				covtmjaIO.getCoverage().toString(),covtmjaIO.getRider().toString());
		if(covpp == null){
			Covppf covp = new Covppf();
			covp.setChdrcoy(covtmjaIO.getChdrcoy().toString());
			covp.setChdrnum(covtmjaIO.getChdrnum().toString());
			covp.setCrtable(covtmjaIO.getCrtable().toString());
			covp.setEffdate(sv.effdatex.toString().trim().equals("")?wsaaToday.toInt():sv.effdatex.toInt());
			covp.setValidflag("1");
			covp.setChdrpfx("CH");
			covp.setCovrprpse(sv.covrprpse.toString());
			covp.setTranCode(sv.trancd.toString().trim().equals("")?wsaaBatckey.batcBatctrcde.toString():sv.trancd.toString());
			covp.setCoverage(sv.coverage.toString());
			covp.setRider(sv.rider.toString());
			covppfDAO.insertCovppfRecord(covp);
		}
		else{
			Covppf covp = new Covppf();
			covp.setChdrcoy(covtmjaIO.getChdrcoy().toString());
			covp.setChdrnum(covtmjaIO.getChdrnum().toString());
			covp.setCrtable(covtmjaIO.getCrtable().toString());
			covp.setEffdate(sv.effdatex.toString().trim().equals("")?wsaaToday.toInt():sv.effdatex.toInt());
			covp.setCovrprpse(sv.covrprpse.toString());
			covp.setCoverage(sv.coverage.toString());
			covp.setRider(sv.rider.toString());
			covp.setUniqueNumber(covpp.getUniqueNumber());
			covppfDAO.updateCovppfCrtable(covp);
		}
}
/*IBPLIFE-2137 End*/
protected void updatePcdt3060()
	{
		if (isEQ(wsaaUpdateFlag,"N")) {
			return ;
		}
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrpf.getChdrcoy(),pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),pcdtmjaIO.getChdrnum())
		|| isNE(sv.life,pcdtmjaIO.getLife())
		|| isNE(sv.coverage,pcdtmjaIO.getCoverage())
		|| isNE(sv.rider,pcdtmjaIO.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(),pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(),varcom.endp)) {
			defaultPcdt3800();
		}
	}

protected void setupCovtmja3100()
	{
		updateReqd3110();
	}

protected void updateReqd3110()
	{
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate,covtmjaIO.getRiskCessDate())) {
			covtmjaIO.setRiskCessDate(sv.riskCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate,covtmjaIO.getPremCessDate())) {
			covtmjaIO.setPremCessDate(sv.premCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge,covtmjaIO.getRiskCessAge())) {
			covtmjaIO.setRiskCessAge(sv.riskCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge,covtmjaIO.getPremCessAge())) {
			covtmjaIO.setPremCessAge(sv.premCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm,covtmjaIO.getRiskCessTerm())) {
			covtmjaIO.setRiskCessTerm(sv.riskCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm,covtmjaIO.getPremCessTerm())) {
			covtmjaIO.setPremCessTerm(sv.premCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin,covtmjaIO.getSumins())) {
			covtmjaIO.setSumins(sv.sumin);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zbinstprem,covtmjaIO.getZbinstprem())) {
			covtmjaIO.setZbinstprem(sv.zbinstprem);
			wsaaUpdateFlag.set("Y");
		}
		
		if(isNE(wsaaCommissionPrem,ZERO)){
			covtmjaIO.setCommPrem(wsaaCommissionPrem);        //IBPLIFE-5237
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem,covtmjaIO.getZlinstprem())) {
			covtmjaIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covtmjaIO.setLoadper(sv.loadper);
			covtmjaIO.setRateadj(sv.rateadj);
			covtmjaIO.setFltmort(sv.fltmort);
			covtmjaIO.setPremadj(sv.premadj);
			covtmjaIO.setAgeadj(sv.adjustageamt);
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		if (isEQ(payrpf.getBillfreq(),"00")) {
			if (isNE(sv.linstamt,covtmjaIO.getSingp())) {
				covtmjaIO.setSingp(sv.linstamt);
				covtmjaIO.setInstprem(ZERO);
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			if (isNE(sv.linstamt,covtmjaIO.getInstprem())) {
				if(isEQ(sv.instPrem,ZERO)) 
				{
				covtmjaIO.setInstprem(premiumrec.calcPrem);
				}
				else
				{
				covtmjaIO.setInstprem(sv.linstamt);
				}
				covtmjaIO.setSingp(ZERO);
				wsaaUpdateFlag.set("Y");
				
			}
		}
		if (isNE(sv.mortcls,covtmjaIO.getMortcls())) {
			covtmjaIO.setMortcls(sv.mortcls);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd,covtmjaIO.getLiencd())) {
			covtmjaIO.setLiencd(sv.liencd);
			wsaaUpdateFlag.set("Y");
		}
		/*ILIFE-7118-starts*/
		if (isNE(sv.tpdtype,covtmjaIO.getTpdtype())) {
			covtmjaIO.setTpdtype(sv.tpdtype);
			wsaaUpdateFlag.set("Y");
		}
		/*ILIFE-7118-ends*/
		
		if (isNE(sv.lnkgind,covtmjaIO.getLnkgind())) {
			covtmjaIO.setLnkgind(sv.lnkgind);
			wsaaUpdateFlag.set("Y");
		}
		
		if ((isEQ(sv.select,"J")
		&& (isEQ(covtmjaIO.getJlife(),"00")
		|| isEQ(covtmjaIO.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select,"L"))
		&& isEQ(covtmjaIO.getJlife(),"01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select,"J")) {
				covtmjaIO.setJlife("01");
			}
			else {
				covtmjaIO.setJlife("00");
			}
		}
		if (isNE(payrpf.getBillfreq(),covtmjaIO.getBillfreq())) {
			covtmjaIO.setBillfreq(payrpf.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrpf.getBillchnl(),covtmjaIO.getBillchnl())) {
			covtmjaIO.setBillchnl(payrpf.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaAnbAtCcd,covtmjaIO.getAnbccd(1))) {
			covtmjaIO.setAnbccd(1, wsaaAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covtmjaIO.getSex(1))) {
			covtmjaIO.setSex(1, wsaaSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd,covtmjaIO.getAnbccd(2))) {
			covtmjaIO.setAnbccd(2, wsbbAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covtmjaIO.getSex(2))) {
			covtmjaIO.setSex(2, wsbbSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth,covtmjaIO.getBappmeth())) {
			covtmjaIO.setBappmeth(sv.bappmeth);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zstpduty01, covtmjaIO.getZstpduty01()) && stampDutyflag) {
			covtmjaIO.setZstpduty01(sv.zstpduty01);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(stateCode, covtmjaIO.getZclstate()) && stampDutyflag) {
			covtmjaIO.setZclstate(stateCode);
			wsaaUpdateFlag.set("Y");
		}
		/*ILIFE-8248 start*/
		if (isNE(sv.lnkgno, covtmjaIO.getLnkgno())) {
			covtmjaIO.setLnkgno(sv.lnkgno);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.lnkgsubrefno, covtmjaIO.getLnkgsubrefno())) {
			covtmjaIO.setLnkgsubrefno(sv.lnkgsubrefno);
			wsaaUpdateFlag.set("Y");
		}
		/*ILIFE-8248 end*/
	}

protected void updateCovtmja3200()
	{
		try {
			noUpdate3210();
			update3220();
		}
		catch (GOTOException e){
		}
	}

protected void noUpdate3210()
	{
		if (isEQ(wsaaUpdateFlag,"N")
		&& isNE(sv.optextind,"X")) {
			goTo(GotoLabel.exit3290);
		}
	}

protected void update3220()
	{
		Hcsdpf hcsdpf= new Hcsdpf();
		hcsdpf.setChdrcoy(covrpf.getChdrcoy());/* IJTI-1386 */
		hcsdpf.setChdrnum(covrpf.getChdrnum());/* IJTI-1386 */
		hcsdpf.setLife(covrpf.getLife());/* IJTI-1386 */
		hcsdpf.setCoverage(wsaaCovrCoverage.toString());
		hcsdpf.setRider(wsaaCovrRider.toString());
		hcsdpf.setPlnsfx(0);		
		hsdpfList = hcsdpfDAO.searchHcsdpfRecord(hcsdpf);
		covtmjaIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		covtmjaIO.setTermid(varcom.vrcmTermid);
		covtmjaIO.setUser(varcom.vrcmUser);
		covtmjaIO.setTransactionDate(varcom.vrcmDate);
		covtmjaIO.setTransactionTime(varcom.vrcmTime);
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setCoverage(wsaaCovrCoverage);
		covtmjaIO.setEffdate(wsaaEffDate);
		covtmjaIO.setRider(wsaaCovrRider);
		covtmjaIO.setPayrseqno(1);
		covtmjaIO.setBenCessAge(ZERO);
		covtmjaIO.setBenCessTerm(ZERO);
		covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
		if (addComp.isTrue()) {
			covtmjaIO.setPlanSuffix(0);
			covtmjaIO.setSeqnbr(0);
		}
		else {
			covtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
			covtmjaIO.setSeqnbr(wsaaNextSeqnbr);
		}
		wsaaNextSeqnbr.subtract(1);
		covtmjaIO.setCrtable(wsaaCrtable);
		covtmjaIO.setReserveUnitsInd(SPACES);
		covtmjaIO.setReserveUnitsDate(ZERO);
		covtmjaIO.setPolinc(ZERO);
		if (chinaLocalPermission)
		{
			covtmjaIO.setCashvalarer(sv.cashvalarer);
		}
		covtmjaIO.setRiskprem(premiumrec.riskPrem);//ILIFE-7845
		if(hsdpfList != null && !hsdpfList.isEmpty()){
			covtmjaIO.setZdivopt(hsdpfList.get(0).getZdivopt());
			covtmjaIO.setPaycurr(hsdpfList.get(0).getPaycurr());
			covtmjaIO.setPayclt(hsdpfList.get(0).getPayclt());
			covtmjaIO.setPaycoy(hsdpfList.get(0).getPaycoy());
			covtmjaIO.setPaymth(hsdpfList.get(0).getPaymth());
			covtmjaIO.setBankkey(hsdpfList.get(0).getBankkey());
			covtmjaIO.setBankacckey(hsdpfList.get(0).getBankacckey());
			covtmjaIO.setFacthous(hsdpfList.get(0).getFacthous());
		}
		/*IBPLIFE-2137 Start*/
		if(covrprpseFlag){
			updateCoverPurpose3020();
		}
		/*IBPLIFE-2137 End*/
		covtmjaIO.setFunction(varcom.updat);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if(riskPremflag) {
			writeRiskPrem();
		}
	}

protected void writeRiskPrem() {
	compFlag = rskppfDAO.checkCoverage(covrmjaIO.getChdrcoy().toString(), covrmjaIO.getChdrnum().toString(), covrmjaIO.getLife().toString(), wsaaCovrCoverage.toString(), wsaaCovrRider.toString());
	Rskppf rskppf = new Rskppf();
	rskppf = calculateDate(rskppf);
	rskppf.setChdrcoy(covrmjaIO.getChdrcoy().toString());
	rskppf.setChdrnum(covrmjaIO.getChdrnum().toString());
	rskppf.setLife(covrmjaIO.getLife().toString());
	rskppf.setCoverage(wsaaCovrCoverage.toString());
	rskppf.setRider(wsaaCovrRider.toString());
	rskppf.setCnttype(chdrmjaIO.getCnttype().toString());
	rskppf.setCrtable(covrmjaIO.getCrtable().toString());
	rskppf.setRiskprem(BigDecimal.ZERO);
	rskppf.setInstprem(premiumrec.calcPrem.getbigdata());
	rskppf.setPolfee(BigDecimal.ZERO); //IBPLIFE-4705
	insertRiskPremList.add(rskppf);
	if(compFlag) {
		if (insertRiskPremList != null && !insertRiskPremList.isEmpty()) rskppfDAO.updateRecords(insertRiskPremList);
	}else {
		if (insertRiskPremList != null && !insertRiskPremList.isEmpty()) rskppfDAO.insertRecords(insertRiskPremList);
	}
}

protected Rskppf calculateDate(Rskppf rskppf) {
	Calendar proposalDate = Calendar.getInstance();
    Calendar taxFromDate = Calendar.getInstance();
    Calendar taxToDate = Calendar.getInstance();
	try {
		proposalDate.setTime(dateFormat.parse(String.valueOf(covtmjaIO.getEffdate().toInt())));
	    int month=proposalDate.get(Calendar.MONTH)+1;
	    if(month>=7) {
	    	taxFromDate.set(proposalDate.get(Calendar.YEAR), 6, 01);
	    	taxToDate.set(proposalDate.get(Calendar.YEAR)+1, 5, 30);
	    }
	    else { 
	    	taxFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    	taxToDate.set(proposalDate.get(Calendar.YEAR), 5, 30);
	    }
	    rskppf.setDatefrm(Integer.parseInt(dateFormat.format(taxFromDate.getTime())));
	    rskppf.setDateto(Integer.parseInt(dateFormat.format(taxToDate.getTime())));
	    rskppf.setEffDate(covtmjaIO.getEffdate().toInt());
	}
	catch (ParseException e) {		
		e.printStackTrace();
	}
	return rskppf;
}

protected void defaultPcdt3800()
	{
		moveValues3800();
		call3830();
	}

protected void moveValues3800()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		for (wsaaIndex.set(1); !(isLT(wsaaIndex,10)); wsaaIndex.add(1)){
			pcdtmjaIO.setSplitc(wsaaIndex, 0);
		}
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		pcdtmjaIO.setInstprem(ZERO);
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		wsaaIndex.set(1);
	}

protected void call3830()
	{
		pcdtmjaIO.setAgntnum(wsaaIndex, chdrpf.getAgntnum());
		pcdtmjaIO.setSplitc(wsaaIndex, 100);
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
protected void insertAndUpdateRcvdpf(){
	boolean rcvdUpdateFlag=false;
	
    if(rcvdPFObject!=null){
    	rcvdPFObject.setPrmbasis(SPACE);		// ILIFE-3610 added by bkonduru2 
    	if(rcvdPFObject.getPrmbasis()!=null){  //ILIFE-3599
	       if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
		        rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
		        rcvdUpdateFlag=true;
	       }
    }
	   //BRD-NBP-011 starts
    	if(rcvdPFObject.getDialdownoption()!=null){
    		if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
    			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
    			rcvdUpdateFlag=true;
    		}
		}
		//BRD-NBP-011 ends
           if(rcvdUpdateFlag){
			   rcvdPFObject.setChdrcoy(covtmjaIO.getChdrcoy().toString());
               rcvdPFObject.setChdrnum(covtmjaIO.getChdrnum().toString());
               rcvdPFObject.setCoverage(covtmjaIO.getCoverage().toString());
               rcvdPFObject.setCrtable(covtmjaIO.getCrtable().toString());
               rcvdPFObject.setLife(covtmjaIO.getLife().toString());
               rcvdPFObject.setRider(covtmjaIO.getRider().toString());
			   rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
           }
    }else{ 
           rcvdPFObject=new Rcvdpf();
           rcvdPFObject.setChdrcoy(covtmjaIO.getChdrcoy().toString());
           rcvdPFObject.setChdrnum(covtmjaIO.getChdrnum().toString());
           rcvdPFObject.setCoverage(covtmjaIO.getCoverage().toString());
           rcvdPFObject.setCrtable(covtmjaIO.getCrtable().toString());
           rcvdPFObject.setLife(covtmjaIO.getLife().toString());
           rcvdPFObject.setRider(covtmjaIO.getRider().toString());
           rcvdPFObject.setWaitperiod(SPACES.toString());
           rcvdPFObject.setPoltyp(SPACES.toString());
           rcvdPFObject.setBentrm(SPACES.toString());
           if(isNE(sv.prmbasis,SPACES)){
	        rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
           }
           //BRD-NBP-011 starts
   			if(isNE(sv.dialdownoption,SPACES)){
   				rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
   			}
   			//BRD-NBP-011 ends
           rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
  }
    
}

protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(sv.pbind,"X")) {
			pbindExe8100();
			return ;
		}
		else {
			if (isEQ(sv.pbind,"?")) {
				pbindRet8200();
				return ;
			}
		}
		if (isEQ(sv.optextind,"X")) {
			optionsExe4500();
			return ;
		}
		else {
			if (isEQ(sv.optextind,"?")) {
				optionsRet4600();
				return ;
			}
		}
		if (isEQ(sv.comind,"X")) {
			commissionExe4900();
			return ;
		}
		else {
			if (isEQ(sv.comind,"?")) {
				commissionRet4a00();
				return ;
			}
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe8400();
			return ;
		}
		else {
			if (isEQ(sv.taxind, "?")) {
				taxRet8500();
				return ;
			}
		}
		if (isEQ(sv.exclind, "X")) {
			exclExe6000();
			return ;
		}
		else {
			if (isEQ(sv.exclind, "?")) {
				exclRet6100();
				return ;
			}
		}
		/* IF S5127-RATYPIND           = 'X'                    <R96REA>*/
		/*    PERFORM 4200-REASSURANCE-EXE                      <R96REA>*/
		/*    GO TO 4090-EXIT                                   <R96REA>*/
		/* ELSE                                                 <R96REA>*/
		/* IF S5127-RATYPIND           = '?'                    <R96REA>*/
		/*    PERFORM 4300-REASSURANCE-RET                      <R96REA>*/
		/*       GO TO 4090-EXIT.                               <R96REA>*/
		/*                                                      <R96REA>*/
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		if (planLevel.isTrue()) {
			//covrmjaIO.setDataKey(covtmjaIO.getDataKey());
			//covrmjaIO.setFunction(varcom.nextr);
			covrpfCount++; //ILB-456
			clearScreen4700();
			policyLoad5000();
			if (!covrpf.equals(null)) { //ILB-456
				wsspcomn.nextprog.set(wsaaProg);
				wayout4800();
			}
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4800();
		}
	}
protected void exclExe6000(){
	covrpfDAO.setCacheObject(covrpf);
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("G");
		wsspcomn.chdrChdrnum.set(covrpf.getChdrnum());
		wsspcomn.crtable.set(wsaaCrtable);
		
		
		if(isNE(wsspcomn.flag,"I")){
		tempFlag.set(wsspcomn.flag);
		wsspcomn.flag.set("X");
		wsspcomn.cmode.set("IF");}
		else
			wsspcomn.cmode.set("IFE");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),wsaaCrtable.toString(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());/* IJTI-1386 */
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
	if(isEQ(wsspcomn.flag,"X"))
	{
		wsspcomn.flag.set(tempFlag);
	}
	
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}

protected void gensww4210()
	{
			para4211();
		}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextrevIO.getStatuz(),varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void optionsExe4500()
	{
		keepCovr4510();
	}

protected void keepCovr4510()
	{
		//ILB-456
		covrpfDAO.setCacheObject(covrpf);
		sv.optextind.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		para4600();
	}

protected void para4600()
	{
		if (!compEnquiry.isTrue()
		&& !compApp.isTrue()
		&& !revComp.isTrue()) {
			wsaaOptext.set("Y");
			calcPremium2200();
		}
		wsaaOptext.set("N");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		checkLext5600();
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(covtmjaIO.getLife());
		wsaaPovrCoverage.set(covtmjaIO.getCoverage());
		wsaaPovrRider.set(covtmjaIO.getRider());
		if (isNE(sv.comind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void clearScreen4700()
	{
		/*PARA*/
		sv.premCessDate.set(ZERO);
		sv.riskCessDate.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.liencd.set(SPACES);
		sv.mortcls.set(SPACES);
		sv.optextind.set(SPACES);
		sv.select.set(SPACES);
		sv.comind.set(SPACES);
		sv.statFund.set(SPACES);
		sv.statSect.set(SPACES);
		sv.bappmeth.set(SPACES);
		sv.statSubsect.set(SPACES);
		sv.tpdtype.set(SPACES);//ILIFE-7118
		/*EXIT*/
		sv.lnkgind.set(SPACES);
	}

protected void wayout4800()
	{
		/*PROGRAM-EXIT*/
		/* If control is passed to this  part of the 4000 section on the*/
		/*   way out of the program, i.e. after  screen  I/O,  then  the*/
		/*   current stack position action  flag will be  blank.  If the*/
		/*   4000-section  is  being   performed  after  returning  from*/
		/*   processing another program then  the current stack position*/
		/*   action flag will be '*'.*/
		/* If 'KILL' has been requested, (CF11), then move spaces to the*/
		/*   current program entry in the program stack and exit.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/* Add 1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void commissionExe4900()
	{
		keepCovr4910();
	}

protected void keepCovr4910()
	{
		/* Keep the COVRMJA record if Whole Plan selected or Add component*/
		/*   as this will have been released within the 1000-section to*/
		/*   enable all policies to be processed.*/
		wsspcomn.tranrate.set(sv.instPrem);
		/*MOVE CHDRMJA-BTDATE         TO WSSP-EFFDATE.                 */
		/* MOVE PAYR-BTDATE            TO WSSP-EFFDATE.            <041>*/
		wssplife.effdate.set(wsaaEffDate);
		/*    IF ADD-COMP OR                                               */
		/*      PLAN-LEVEL*/
		//ILB-456
		covrpfDAO.setCacheObject(covrpf);
		/*covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		/* The first thing to consider is the handling of an Options/*/
		/*   Extras request. If the indicator is 'X', a request to visit*/
		/*   options and extras has been made. In this case:*/
		/*    - change the options/extras request indicator to '?',*/
		sv.comind.set("?");
		/* Save the next 8 programs from the program stack.*/
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4510();
		}
		/* Call GENSSWCH with and action of 'A' to retrieve the program*/
		/*   switching required, and move them to the stack.*/
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*'.*/
		/* Add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void commissionRet4a00()
	{
		/*A00-PARA*/
		/* On return from this  request, the current stack "action" will*/
		/*   be '*' and the options/extras indicator  will  be  '?'.  To*/
		/*   handle the return from options and extras:*/
		/* - calculate the  premium  as  described  above  in  the*/
		/*   'Validation' section, and check that it is within the*/
		/*   tolerance limit,*/
		calcPremium5200();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			restore4610();
		}
		/* Blank out the stack "action".*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		checkPcdt5700();
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*A90-EXIT*/
	}

protected void policyLoad5000()
	{
		try {
			loadPolicy5010();
			readCovt5020();
			fieldsToScreen5080();
			mortalityLien5090();
			ageProcessing50a0();
			optionsExtras50b0();
			enquiryProtect50c0();
		}
		catch (GOTOException e){
		}
	}

protected void loadPolicy5010()
	{
		wsaaOldPrem.set(0);
		wsaaOldSumins.set(0);
		if (planLevel.isTrue()) {
			readCovr5100();
			for (Covrpf covr : covrpfList) {
				if (covr == null) {
					goTo(GotoLabel.exit50z0);
				}
				
				else {
					covtmjaIO.setFunction(varcom.readr);
					if (addComp.isTrue()) {
						covtmjaIO.setCoverage(wsaaCovrCoverage);
						covtmjaIO.setRider(wsaaCovrRider);
						covtmjaIO.setPlanSuffix(covr.getPlanSuffix());
					}
					else {
						/*          MOVE COVRMJA-DATA-KEY    TO COVTMJA-DATA-KEY*/
						covtmjaIO.setDataKey(wsaaCovrKey);
						if (isNE(covr.getPlanSuffix(), ZERO)) {
							covtmjaIO.setPlanSuffix(covr.getPlanSuffix());
						}
					}
				}
				covrpf.setPlanSuffix(covr.getPlanSuffix());
			}
		}
		wsaaOldSumins.set(covrpf.getSumins());
		wsaaOldPrem.set(covrpf.getInstprem());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			sv.planSuffix.set(chdrpf.getPolsum());
			sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		}
		else {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
	}

protected void readCovt5020()
	{
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.mrnf)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(covtmjaIO.getLife(),covrpf.getLife())
		|| isNE(covtmjaIO.getCoverage(),covrpf.getCoverage())
		|| isNE(covtmjaIO.getRider(),covrpf.getRider())
		|| isNE(covtmjaIO.getPlanSuffix(),covrpf.getPlanSuffix())
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)
		|| isEQ(covtmjaIO.getStatuz(),varcom.mrnf)) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (!addComp.isTrue()) {
			recordToScreen6000();
			return;
		}
		else {
			if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)
			|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
				covtmjaIO.setNonKey(SPACES);
				covtmjaIO.setAnbccd(1, ZERO);
				covtmjaIO.setAnbccd(2, ZERO);
				covtmjaIO.setSingp(ZERO);
				covtmjaIO.setPlanSuffix(ZERO);
				covtmjaIO.setInstprem(ZERO);
				covtmjaIO.setZbinstprem(ZERO);
				covtmjaIO.setZlinstprem(ZERO);
				covtmjaIO.setPremCessAge(ZERO);
				covtmjaIO.setPremCessTerm(ZERO);
				covtmjaIO.setRiskCessAge(ZERO);
				covtmjaIO.setRiskCessTerm(ZERO);
				covtmjaIO.setBenCessAge(ZERO);
				covtmjaIO.setBenCessTerm(ZERO);
				covtmjaIO.setReserveUnitsDate(ZERO);
				covtmjaIO.setSumins(ZERO);
				covtmjaIO.setEffdate(wsaaEffDate);
				covtmjaIO.setRiskCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setPremCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
				covtmjaIO.setPolinc(chdrpf.getPolinc());
				covtmjaIO.setCrtable(wsaaCrtable);
				tableLoads7000();
			}
			else {
				tableLoads7000();
			}
		}
		if (isEQ(covtmjaIO.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
	}

protected void fieldsToScreen5080()
	{
		sv.planSuffix.set(chdrpf.getPolinc());
		sv.liencd.set(covtmjaIO.getLiencd());
		//ILIFE-8194 starts
		if(!isCompModify) {
		sv.mortcls.set(covtmjaIO.getMortcls());
		}
		//ILIFE-8194 ends
		sv.premCessDate.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.riskCessDate.set(covtmjaIO.getRiskCessDate());
		sv.riskCessAge.set(covtmjaIO.getRiskCessAge());
		sv.riskCessTerm.set(covtmjaIO.getRiskCessTerm());
		if (isEQ(sv.effdate, chdrpf.getPtdate())) {
			sv.instPrem.set(ZERO);
			sv.linstamt.set(covtmjaIO.getInstprem());
		} else {
			sv.instPrem.set(covtmjaIO.getInstprem());
			sv.linstamt.set(ZERO);
		}
		sv.zbinstprem.set(covtmjaIO.getZbinstprem());
		sv.zlinstprem.set(covtmjaIO.getZlinstprem());
		if(stampDutyflag){//ILIFE-8537
			if(isNE(covtmjaIO.getZstpduty01(),ZERO)){
				sv.zstpduty01.set(covtmjaIO.getZstpduty01());
			}
		}		
		/*BRD-306 START */
		sv.loadper.set(covtmjaIO.getLoadper());
		sv.rateadj.set(covtmjaIO.getRateadj());
		sv.fltmort.set(covtmjaIO.fltmort);
		sv.premadj.set(covtmjaIO.getPremadj());
		sv.adjustageamt.set(covtmjaIO.getAgeadj());
		sv.tpdtype.set(covtmjaIO.getTpdtype());//ILIFE-7118
		/*BRD-306 END */
		sv.lnkgind.set(covtmjaIO.getLnkgind());		
		sv.sumin.set(covtmjaIO.getSumins());
//		sv.effdate.set(covtmjaIO.getEffdate());/*ILIFE-4837*/ the effective date is ALWAYS the Contract PTD, so remove this line.
		/*if(!chinaLocalPermission) {
			sv.effdate.set(covtmjaIO.getEffdate());
		}*/
		setupBonus1300();
		sv.bappmeth.set(covtmjaIO.getBappmeth());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		if (isEQ(t5608rec.sumInsMax,0)
		&& isEQ(t5608rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5608rec.sumInsMax,t5608rec.sumInsMin)
		&& isNE(t5608rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5608rec.sumInsMax);
		}
		if(isNE(t5687rec.lnkgind,"Y"))
		{
			sv.lnkgindOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560 
		}
	}

protected void mortalityLien5090()
	{
		if (isEQ(t5608rec.mortclss,SPACES)) {
			sv.mortcls.set(SPACES);
			/*sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5608rec.mortcls01,SPACES)
		&& isEQ(t5608rec.mortcls02,SPACES)
		&& isEQ(t5608rec.mortcls03,SPACES)
		&& isEQ(t5608rec.mortcls04,SPACES)
		&& isEQ(t5608rec.mortcls05,SPACES)
		&& isEQ(t5608rec.mortcls06,SPACES)) {
			sv.mortcls.set(t5608rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5608rec.liencds,SPACES)) {
			sv.liencd.set(SPACES);
			//sv.liencdOut[varcom.nd.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		if (isNE(t5608rec.liencd01,SPACES)
		&& isEQ(t5608rec.liencd02,SPACES)
		&& isEQ(t5608rec.liencd03,SPACES)
		&& isEQ(t5608rec.liencd04,SPACES)
		&& isEQ(t5608rec.liencd05,SPACES)
		&& isEQ(t5608rec.liencd06,SPACES)) {
			sv.liencd.set(t5608rec.liencd01);
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void ageProcessing50a0()
	{
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (isEQ(covtmjaIO.getJlife(),"01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults5300();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& isNE(t5608rec.eaage,SPACES)) {
			riskCessDate5400();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& isNE(t5608rec.eaage,SPACES)) {
			premCessDate5450();
		}
		if (isNE(t5608rec.eaage,SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void optionsExtras50b0()
	{
		if (isEQ(t5608rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
		/*B2-PREMIUM-BREAKDOWN*/
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
	}

protected void enquiryProtect50c0()
	{
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxindOut[varcom.nd.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
			sv.lnkgnoOut[varcom.pr.toInt()].set("Y");//ILIFE-8248
			sv.lnkgsubrefnoOut[varcom.pr.toInt()].set("Y");//ILIFE-8248
			if(covrprpseFlag){
				sv.covrprpseOut[varcom.pr.toInt()].set("Y");
			}
		}
	}

protected void readCovr5100()
	{
		/*READ-COVRMJA*/
	//ILB-456 starts
	/*	SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrpf.getChdrcoy(),wsaaCovrChdrcoy)
		|| isNE(covrpf.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(covrpf.getLife(),wsaaCovrLife)
		|| isNE(covrpf.getCoverage(),wsaaCovrCoverage)
		|| isNE(covrpf.getRider(),wsaaCovrRider)
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrmjaIO.getFunction(),varcom.nextr)
		&& isEQ(covrpf.getPlanSuffix(),wsaaPlanSuffix)) {
			covrmjaIO.setStatuz(varcom.endp);
		}*/
	covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
	for (Covrpf covr:covrpfList) {
		if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
				|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
				|| isNE(covr.getLife(),wsaaCovrLife)
				|| isNE(covr.getCoverage(),wsaaCovrCoverage)
				|| isNE(covr.getRider(),wsaaCovrRider)
				|| (!covrpfList.isEmpty())) {
					return;
				}
	}
		//ILB-456 ends
		/*EXIT*/
	}

protected void calcPremium5200()
	{
		try {
			calcPremium5210();
			methodTable5220();
		}
		catch (GOTOException e){
		}
	}

protected void calcPremium5210()
	{
		wsaaPremStatuz.set("N");
		if (isNE(t5687rec.bbmeth,SPACES)) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit5290);
		}
	}

protected void methodTable5220()
	{
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/* ILIFE-3142 End*/
			t5675rec.t5675Rec.set(itemIO.getGenarea());
		}
	}

protected void checkDefaults5300()
	{
		try {
			searchTable5310();
		}
		catch (GOTOException e){
		}
	}

protected void searchTable5310()
	{
		sub1.set(1);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
		while ( !(isGT(sub1,wsaaMaxOcc))) {
			nextColumn5320();
		}
		
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		goTo(GotoLabel.exit5390);
	}

protected void nextColumn5320()
	{
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5608rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5608rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.riskCessageFrom[sub1.toInt()],t5608rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(t5608rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5608rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5608rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.premCessageFrom[sub1.toInt()],t5608rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(t5608rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5608rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5608rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.riskCesstermFrom[sub1.toInt()],t5608rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(t5608rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5608rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5608rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.premCesstermFrom[sub1.toInt()],t5608rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(t5608rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		sub1.add(1);
	}

protected void riskCessDate5400()
	{
			riskCessDatePara5400();
		}

protected void riskCessDatePara5400()
	{
		if (isEQ(sv.riskCessAge,ZERO)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.riskCessAge,0),true)
		&& isEQ(isGT(sv.riskCessTerm,0),false)){
			if (isEQ(t5608rec.eaage,"A")) {
				if (addComp.isTrue()) {
					
						datcon2rec.intDate1.set(wsaaEffDate);
					
				}
				else {
					
					   datcon2rec.intDate1.set(covrpf.getCrrcd());
					
				}
				if(effdateFlag){
					datcon2rec.intDate1.set(findAnniversary(datcon2rec.intDate1.toString()));
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge,wszzAnbAtCcd));
			}
			if (isEQ(t5608rec.eaage,"E")
			|| isEQ(t5608rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.riskCessAge);
			}
		}
		else if (isEQ(isGT(sv.riskCessAge,0),false)
		&& isEQ(isGT(sv.riskCessTerm,0),true)){
			if (isEQ(t5608rec.eaage,"A")
			|| isEQ(t5608rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					if (effdateFlag) {
						datcon2rec.intDate1.set(chdrpf.getOccdate());
					} else {
						datcon2rec.intDate1.set(wsaaEffDate);
					}
				}
				else {
					if (effdateFlag) {
						datcon2rec.intDate1.set(chdrpf.getOccdate());
					}else {
					   datcon2rec.intDate1.set(covrpf.getCrrcd());
					}
				}
				datcon2rec.freqFactor.set(sv.riskCessTerm);
			}
			if (isEQ(t5608rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.riskCessTerm,wszzAnbAtCcd),1));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.riskCessDate.set(datcon2rec.intDate2);
		}
	}

protected void premCessDate5450()
	{
			premCessDatePara5450();
		}

protected void premCessDatePara5450()
	{
		if (isEQ(sv.premCessAge,ZERO)
		&& isEQ(sv.premCessTerm,ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.premCessAge,0),true)
		&& isEQ(isGT(sv.premCessTerm,0),false)){
			if (isEQ(t5608rec.eaage,"A")) {
				if (addComp.isTrue()) {
					
						datcon2rec.intDate1.set(wsaaEffDate);
					
				}
				else {
					
						datcon2rec.intDate1.set(covrpf.getCrrcd());
					
				}
				if(effdateFlag){
					datcon2rec.intDate1.set(findAnniversary(datcon2rec.intDate1.toString()));
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge,wszzAnbAtCcd));
			}
			if (isEQ(t5608rec.eaage,"E")
			|| isEQ(t5608rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.premCessAge);
			}
		}
		else if (isEQ(isGT(sv.premCessAge,0),false)
		&& isEQ(isGT(sv.premCessTerm,0),true)){
			if (isEQ(t5608rec.eaage,"A")
			|| isEQ(t5608rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					if (effdateFlag) {
						datcon2rec.intDate1.set(chdrpf.getOccdate());
					} else {
						datcon2rec.intDate1.set(wsaaEffDate);
					}
				}
				else {
					if (effdateFlag) {
						datcon2rec.intDate1.set(chdrpf.getOccdate());
					} else {
						datcon2rec.intDate1.set(covrpf.getCrrcd());
					}
				}
				datcon2rec.freqFactor.set(sv.premCessTerm);
			}
			if (isEQ(t5608rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.premCessTerm,wszzAnbAtCcd),1));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.premCessDate.set(datcon2rec.intDate2);
		}
	}

protected void callDatcon25500()
	{
		/*CALL*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}


protected String findAnniversary(String effDate)
{
	wsaaOccdate.set(chdrpf.getOccdate());
	wsaaPtdate.set(effDate);
	wsaaLastAnndateYy.set(wsaaPtdateYy);
	wsaaLastAnndateMm.set(wsaaOccdateMm);
	wsaaLastAnndateDd.set(wsaaOccdateDd);
	datcon1rec.function.set(varcom.conv);
	datcon1rec.statuz.set(SPACES);
	while ( !(isEQ(datcon1rec.statuz, varcom.oK))) {
		datcon1rec.intDate.set(wsaaLastAnndate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			wsaaLastAnndateDd.subtract(1);
		}
	}
	currentYrAge.set(wsaaPtdateYy.toString().concat( String.valueOf(wszzCltdob).substring(4, 6)).concat(String.valueOf(wszzCltdob).substring( 6, 8)));
	if (isEQ(effDate,wsaaLastAnndate )) {
		return effDate;
	} 		
	else {
		if (isGT(wsaaLastAnndate, effDate)) {
			datcon4rec.intDate1.set(wsaaLastAnndate);
			datcon4rec.billday.set(payrpf.getDuedd());
			datcon4rec.billmonth.set(payrpf.getDuemm());
			datcon4rec.freqFactor.set(-1);
			datcon4rec.frequency.set("01");
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			wsaaLastAnndate.set(datcon4rec.intDate2);
		}
		//Next anniversary
		
		datcon4rec.intDate1.set(wsaaLastAnndate);
		datcon4rec.billday.set(payrpf.getDuedd());
		datcon4rec.billmonth.set(payrpf.getDuemm());
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set("01");
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		wsaaNextAnndate.set(datcon4rec.intDate2);
		if (isGT(effDate,currentYrAge))
			return wsaaNextAnndate.toString();
		else 
			return wsaaLastAnndate.toString();
	}

}

protected void checkLext5600()
	{
		readLext5610();
	}

protected void readLext5610()
	{
		wsaaLextUpdates.set("N");
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrcoy(chdrpf.getChdrcoy());
		lextrevIO.setChdrnum(chdrpf.getChdrnum());
		lextrevIO.setLife(covtmjaIO.getLife());
		lextrevIO.setCoverage(covtmjaIO.getCoverage());
		lextrevIO.setRider(covtmjaIO.getRider());
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(),varcom.oK)
		&& isNE(lextrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(),lextrevIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),lextrevIO.getChdrnum())
		|| isNE(covtmjaIO.getLife(),lextrevIO.getLife())
		|| isNE(covtmjaIO.getCoverage(),lextrevIO.getCoverage())
		|| isNE(covtmjaIO.getRider(),lextrevIO.getRider())) {
			lextrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextrevIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
		if (isNE(lextrevIO.getStatuz(),varcom.endp)) {
			if (isGT(lextrevIO.getTranno(),chdrpf.getTranno())) {
				wsaaLextUpdates.set("Y");
				lextrevIO.setStatuz(varcom.endp);
			}
		}
		while ( !(isEQ(lextrevIO.getStatuz(),varcom.endp))) {
			lextrevIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, lextrevIO);
			if (isNE(lextrevIO.getStatuz(),varcom.oK)
			&& isNE(lextrevIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(lextrevIO.getParams());
				syserrrec.statuz.set(lextrevIO.getStatuz());
				fatalError600();
			}
			if (isNE(chdrpf.getChdrcoy(),lextrevIO.getChdrcoy())
			|| isNE(chdrpf.getChdrnum(),lextrevIO.getChdrnum())
			|| isNE(covtmjaIO.getLife(),lextrevIO.getLife())
			|| isNE(covtmjaIO.getCoverage(),lextrevIO.getCoverage())
			|| isNE(covtmjaIO.getRider(),lextrevIO.getRider())) {
				lextrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isGT(lextrevIO.getTranno(),chdrpf.getTranno())) {
					wsaaLextUpdates.set("Y");
					lextrevIO.setStatuz(varcom.endp);
				}
			}
		}
		
	}

protected void checkPcdt5700()
	{
		readPcdt5710();
	}

protected void readPcdt5710()
	{
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		pcdtmjaIO.setChdrcoy(covtmjaIO.getChdrcoy());
		pcdtmjaIO.setChdrnum(covtmjaIO.getChdrnum());
		pcdtmjaIO.setLife(covtmjaIO.getLife());
		pcdtmjaIO.setCoverage(covtmjaIO.getCoverage());
		pcdtmjaIO.setRider(covtmjaIO.getRider());
		pcdtmjaIO.setPlanSuffix(covtmjaIO.getPlanSuffix());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getChdrcoy(),pcdtmjaIO.getChdrcoy())
		|| isNE(covtmjaIO.getChdrnum(),pcdtmjaIO.getChdrnum())
		|| isNE(covtmjaIO.getLife(),pcdtmjaIO.getLife())
		|| isNE(covtmjaIO.getCoverage(),pcdtmjaIO.getCoverage())
		|| isNE(covtmjaIO.getRider(),pcdtmjaIO.getRider())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			sv.comind.set(" ");
		}
		else {
			sv.comind.set("+");
		}
	}

protected void recordToScreen6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covrRecord6010();
					fieldsToScreen6020();
				case covtExist6025: 
					covtExist6025();
					enquiryProtect6030();
					optionsExtras603a();
					premiumBreakdown603c();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covrRecord6010()
	{
		if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			covtmjaIO.setSex01(wsaaSex);
			covtmjaIO.setSex02(wsbbSex);
			covtmjaIO.setAnbAtCcd01(wsaaAnbAtCcd);
			covtmjaIO.setAnbAtCcd02(wsbbAnbAtCcd);
			covtmjaIO.setCrtable(covrpf.getCrtable());
			covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
			covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
			covtmjaIO.setRiskCessAge(covrpf.getRiskCessAge());
			covtmjaIO.setPremCessAge(covrpf.getPremCessAge());
			covtmjaIO.setRiskCessTerm(covrpf.getRiskCessTerm());
			covtmjaIO.setPremCessTerm(covrpf.getPremCessTerm());
			covtmjaIO.setReserveUnitsDate(covrpf.getReserveUnitsDate());
			covtmjaIO.setReserveUnitsInd(covrpf.getReserveUnitsInd());
			covtmjaIO.setSumins(covrpf.getSumins());
			covtmjaIO.setInstprem(covrpf.getInstprem());
			covtmjaIO.setZbinstprem(covrpf.getZbinstprem());
			covtmjaIO.setZlinstprem(covrpf.getZlinstprem());
			covtmjaIO.setMortcls(covrpf.getMortcls());
			covtmjaIO.setLiencd(covrpf.getLiencd());
			covtmjaIO.setJlife(covrpf.getJlife());
			covtmjaIO.setEffdate(covrpf.getCrrcd());
			covtmjaIO.setBillfreq(payrpf.getBillfreq());
			covtmjaIO.setBillchnl(payrpf.getBillchnl());
			covtmjaIO.setSingp(covrpf.getSingp());
			sv.statFund.set(covrpf.getStatFund());
			sv.statSect.set(covrpf.getStatSect());
			covtmjaIO.setPayrseqno(covrpf.getPayrseqno());
			covtmjaIO.setBappmeth(covrpf.getBappmeth());
			sv.statSubsect.set(covrpf.getStatSubsect());
			covtmjaIO.setTpdtype(covrpf.getTpdtype());//ILIFE-7118
			covtmjaIO.setLnkgind(covrpf.getLnkgind());
			if(stampDutyflag){//ILIFE-8537
				// PINNACLE-1433 added null check for stamp duty
				if(null != covrpf.getZstpduty01() && isNE(covrpf.getZstpduty01(),ZERO)){
					covtmjaIO.setZstpduty01(covrpf.getZstpduty01());
				}
			}
			
		}
		covtmjaIO.setPolinc(chdrpf.getPolinc());
	}

protected void fieldsToScreen6020()
	{
		if (isEQ(covtmjaIO.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.mrnf)
		|| isEQ(covtmjaIO.getStatuz(),varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.covtExist6025);
		}
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix,1)) {
				compute(sv.sumin, 2).set((sub(covtmjaIO.getSumins(),(div(mult(covtmjaIO.getSumins(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.instPrem, 2).set((sub(covtmjaIO.getInstprem(),(div(mult(covtmjaIO.getInstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.zbinstprem, 2).set((sub(covtmjaIO.getZbinstprem(),(div(mult(covtmjaIO.getZbinstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.zlinstprem, 2).set((sub(covtmjaIO.getZlinstprem(),(div(mult(covtmjaIO.getZlinstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
			}
			else {
				compute(sv.sumin, 3).setRounded(div(covtmjaIO.getSumins(),chdrpf.getPolsum()));
				compute(sv.instPrem, 3).setRounded(div(covtmjaIO.getInstprem(),chdrpf.getPolsum()));
				compute(sv.zbinstprem, 3).setRounded(div(covtmjaIO.getZbinstprem(),chdrpf.getPolsum()));
				compute(sv.zlinstprem, 3).setRounded(div(covtmjaIO.getZlinstprem(),chdrpf.getPolsum()));
			}
		}
		else {
			sv.sumin.set(covtmjaIO.getSumins());
			sv.zbinstprem.set(covtmjaIO.getZbinstprem());
			sv.zlinstprem.set(covtmjaIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtmjaIO.getLoadper());
			sv.rateadj.set(covtmjaIO.getRateadj());
			sv.fltmort.set(covtmjaIO.fltmort);
			sv.premadj.set(covtmjaIO.getPremadj());
			sv.adjustageamt.set(covtmjaIO.getAgeadj());
			/*BRD-306 END */
			sv.instPrem.set(covtmjaIO.getInstprem());
			sv.tpdtype.set(covtmjaIO.getTpdtype());
			if(stampDutyflag){//ILIFE-8537
				if(isNE(covtmjaIO.getZstpduty01(),ZERO)){
					sv.zstpduty01.set(covtmjaIO.getZstpduty01());
				}
			}
		}
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix,1)) {
				compute(wsaaOldSumins, 0).set((sub(wsaaOldSumins,(div(mult(wsaaOldSumins,(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(wsaaOldPrem, 2).set((sub(wsaaOldPrem,(div(mult(wsaaOldPrem,(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
			}
			else {
				compute(wsaaOldSumins, 1).setRounded(div(wsaaOldSumins,chdrpf.getPolsum()));
				compute(wsaaOldPrem, 3).setRounded(div(wsaaOldPrem,chdrpf.getPolsum()));
			}
		}
		
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560 
		}
	}

protected void covtExist6025()
	{
		if (isEQ(covtmjaIO.getStatuz(),varcom.oK)) {
			sv.sumin.set(covtmjaIO.getSumins());
			sv.zbinstprem.set(covtmjaIO.getZbinstprem());
			sv.zlinstprem.set(covtmjaIO.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtmjaIO.getLoadper());
			sv.rateadj.set(covtmjaIO.getRateadj());
			sv.fltmort.set(covtmjaIO.fltmort);
			sv.premadj.set(covtmjaIO.getPremadj());
			sv.adjustageamt.set(covtmjaIO.getAgeadj());
			/*BRD-306 END */
			if (isEQ(sv.effdate, chdrpf.getPtdate())) {
				sv.instPrem.set(ZERO);
				sv.linstamt.set(covtmjaIO.getInstprem());
			} else {
				sv.instPrem.set(covtmjaIO.getInstprem());
				sv.linstamt.set(ZERO);
			}
			sv.tpdtype.set(covtmjaIO.getTpdtype());
			if(stampDutyflag){
				if(isNE(covtmjaIO.getZstpduty01(),ZERO)){
					sv.zstpduty01.set(covtmjaIO.getZstpduty01());
				}
				if(isNE(covtmjaIO.getZclstate(),SPACES)){
					stateCode=covtmjaIO.getZclstate().toString();
				}
			}
		}
		
		if (chinaLocalPermission)
		{
			sv.cashvalarer.set(covtmjaIO.getCashvalarer());
		}

	

		sv.liencd.set(covtmjaIO.getLiencd());
		sv.mortcls.set(covtmjaIO.getMortcls());
		sv.premCessDate.set(covtmjaIO.getPremCessDate());
		sv.premCessAge.set(covtmjaIO.getPremCessAge());
		sv.premCessTerm.set(covtmjaIO.getPremCessTerm());
		sv.riskCessDate.set(covtmjaIO.getRiskCessDate());
		sv.riskCessAge.set(covtmjaIO.getRiskCessAge());
		sv.riskCessTerm.set(covtmjaIO.getRiskCessTerm());
		setupBonus1300();
		sv.bappmeth.set(covtmjaIO.getBappmeth());
		sv.tpdtype.set(covtmjaIO.getTpdtype());
		sv.lnkgind.set(covtmjaIO.getLnkgind());
		if(isEQ(wsaaFlag, "M")){
			wsaaCashValArr.set(sv.sumin);
			wsaaHoldSumin.set(sv.sumin);// fwang3 ICIL-560
		}
	}

protected void enquiryProtect6030()
	{
		if (compEnquiry.isTrue()
		|| compApp.isTrue()
		|| revComp.isTrue()) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
			sv.tpdtypeOut[varcom.pr.toInt()].set("Y");
			sv.lnkgindOut[varcom.pr.toInt()].set("Y");
			sv.lnkgnoOut[varcom.pr.toInt()].set("Y");//ILIFE-8248
			sv.lnkgsubrefnoOut[varcom.pr.toInt()].set("Y");//ILIFE-8248
		}
		if (!addComp.isTrue()) {
			tableLoads7000();
		}
		if (isEQ(t5608rec.sumInsMax,0)
		&& isEQ(t5608rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5608rec.sumInsMax,t5608rec.sumInsMin)
		&& isNE(t5608rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5608rec.sumInsMax);
		}
	}

protected void optionsExtras603a()
	{
		if (isEQ(t5608rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
	}

protected void premiumBreakdown603c()
	{
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/*EXIT*/
	}

protected void tableLoads7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t5687Load7010();
					t5671Load7020();
				case itemCall7023: 
					itemCall7023();
					editRules7030();
					t5608Load7040();
					t5667Load7045();
					t5675Load7050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t5687Load7010()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),wsaaCrtable)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	**** IF T5687-RIIND              = 'N' OR SPACES          <R96REA>
	****    MOVE SPACES              TO S5127-RATYPIND        <R96REA>
	****    MOVE 'Y'                 TO S5127-RATYPIND-OUT(ND)<R96REA>
	* </pre>
	*/
protected void t5671Load7020()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtmjaIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(wsaaCrtable);
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
	}

protected void itemCall7023()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		else {
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				wsbbCrtable.set("****");
				itemIO.setItemitem(wsbbTranCrtable);
				goTo(GotoLabel.itemCall7023);
			}
			else {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
	}

protected void editRules7030()
	{
		if (isEQ(t5671rec.pgm[1],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
	}

protected void t5608Load7040()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5608);
		wsbbCurrency.set(payrpf.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(tablesInner.t5608, itdmIO.getItemtabl())
		|| isNE(wsbbTranCurrency,itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t5608rec.t5608Rec.set(itdmIO.getGenarea());
		}
		else {
			t5608rec.t5608Rec.set(SPACES);
			t5608rec.ageIssageFrms.fill("0");
			t5608rec.ageIssageTos.fill("0");
			t5608rec.termIssageFrms.fill("0");
			t5608rec.termIssageTos.fill("0");
			t5608rec.premCessageFroms.fill("0");
			t5608rec.premCessageTos.fill("0");
			t5608rec.premCesstermFroms.fill("0");
			t5608rec.premCesstermTos.fill("0");
			t5608rec.riskCessageFroms.fill("0");
			t5608rec.riskCessageTos.fill("0");
			t5608rec.riskCesstermFroms.fill("0");
			t5608rec.riskCesstermTos.fill("0");
			t5608rec.sumInsMax.set(ZERO);
			t5608rec.sumInsMin.set(ZERO);
			if (isEQ(scrnparams.errorCode,SPACES)) {
				scrnparams.errorCode.set(errorsInner.f335);
			}
		}
		if(isEQ(t5608rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else{
			prmbasisFlag=true;
		}
	}

protected void t5667Load7045()
	{
		itemIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
	}

protected void t5675Load7050()
	{
		if (singlif.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			calcPremium5200();
		}
		/*EXIT*/
	}

protected void readPovr8000()
	{
		start8010();
	}

protected void start8010()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)
		&& isNE(povrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void pbindExe8100()
	{
		start8110();
	}

protected void start8110()
	{
		readPovr8000();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(),wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(),wsaaPovrLife)
		|| isNE(povrIO.getCoverage(),wsaaPovrCoverage)
		|| isNE(povrIO.getRider(),wsaaPovrRider)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		//ILB-456
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		sv.pbind.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			save4510();
		}
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet8200()
	{
		start8210();
	}

protected void start8210()
	{
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		sv.pbind.set("+");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void vf2MaltPovr8300()
	{
		start8310();
	}

protected void start8310()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setValidflag("2");
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void taxExe8400()
	{
		start8410();
	}

protected void start8410()
	{
		/* Keep the CHDR/COVR record                                       */
		//ILB-456
		chdrpfDAO.setCacheObject(chdrpf);
		/*chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		wsaaStoreZbinstprem.set(covtmjaIO.getZbinstprem());
		wsaaStoreSingp.set(covtmjaIO.getSingp());
		wsaaStoreInstprem.set(covtmjaIO.getInstprem());
		covtmjaIO.setZbinstprem(sv.zbinstprem);
		if (isEQ(payrpf.getBillfreq(), "00")) {
			covtmjaIO.setSingp(sv.linstamt);
			covtmjaIO.setInstprem(0);
		}
		else {
			covtmjaIO.setInstprem(sv.linstamt);
			covtmjaIO.setSingp(0);
		}
		covtmjaIO.setFunction(varcom.keeps);
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			syserrrec.statuz.set(covtmjaIO.getStatuz());
			fatalError600();
		}
		wssplife.effdate.set(chdrpf.getBtdate());
		/* The first thing to consider is the handling of an Options/      */
		/*   Extras request. If the indicator is 'X', a request to visit   */
		/*   options and extras has been made. In this case:               */
		/*    - change the options/extras request indicator to '?',        */
		sv.taxind.set("?");
		/* Save the next 8 programs from the program stack.                */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		/* Call GENSSWCH with and action of 'A' to retrieve the program    */
		/*   switching required, and move them to the stack.               */
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*'.                          */
		/* Add one to the program pointer and exit.                        */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet8500()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/*   be '*' and the options/extras indicator  will  be  '?'.  To   */
		/*   handle the return from options and extras:                    */
		sv.taxind.set("+");
		covtmjaIO.setZbinstprem(wsaaStoreZbinstprem);
		covtmjaIO.setSingp(wsaaStoreSingp);
		covtmjaIO.setInstprem(wsaaStoreInstprem);
		/* Restore the saved programs to the program stack                 */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		/* Blank out the stack "action".                                   */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/* Set WSSP-NEXTPROG to the current screen name (thus returning    */
		/*   returning to re-display the screen).                          */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void a100CheckLimit()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		chkrlrec.clntnum.set(sv.lifcnum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.currcd);
		chkrlrec.cnttype.set(chdrpf.getCnttype());
		chkrlrec.crtable.set(covtmjaIO.getCrtable());
		chkrlrec.sumins.set(sv.sumin);
		chkrlrec.life.set(covtmjaIO.getLife());
		chkrlrec.chdrnum.set(chdrpf.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz,varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.suminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}

protected void a200CheckCalcTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a210Start();
				case a250CallTaxSubr: 
					a250CallTaxSubr();
				case a290Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210Start()
	{
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.a290Exit);
		}
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.a250CallTaxSubr);
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covtmjaIO.getCrtable());
		a300ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void a250CallTaxSubr()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			txcalcrec.chdrnum.set(chdrpf.getChdrnum());
			txcalcrec.life.set(covtmjaIO.getLife());
			txcalcrec.coverage.set(covtmjaIO.getCoverage());
			txcalcrec.rider.set(covtmjaIO.getRider());
			txcalcrec.planSuffix.set(covtmjaIO.getPlanSuffix());
			txcalcrec.crtable.set(covtmjaIO.getCrtable());
			txcalcrec.cnttype.set(chdrpf.getCnttype());
			txcalcrec.register.set(chdrpf.getReg());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrpf.getCntcurr());
			wsaaCntCurr.set(chdrpf.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				txcalcrec.amountIn.set(sv.instPrem);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrpf.getBtdate());
			txcalcrec.tranno.set(chdrpf.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(sv.zbinstprem);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
			}
		}
	}

protected void a300ReadTr52e()
	{
		a310Start();
	}

protected void a310Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrpf.getPtdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}
/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData f008 = new FixedLengthStringData(4).init("F008");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f335 = new FixedLengthStringData(4).init("F335");
	private FixedLengthStringData f404 = new FixedLengthStringData(4).init("F404");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData h033 = new FixedLengthStringData(4).init("H033");
	private FixedLengthStringData h044 = new FixedLengthStringData(4).init("H044");
	private FixedLengthStringData ev01 = new FixedLengthStringData(4).init("EV01");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	private FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData f862 = new FixedLengthStringData(4).init("F862");
	private FixedLengthStringData rrj4 = new FixedLengthStringData(4).init("RRJ4");
	private FixedLengthStringData e751 = new FixedLengthStringData(4).init("E751");
	private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");	//ICIL-1310
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t5608 = new FixedLengthStringData(5).init("T5608");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");//BRD-306
	private FixedLengthStringData th528 = new FixedLengthStringData(5).init("TH528");
	private FixedLengthStringData ta610 = new FixedLengthStringData(5).init("TA610");	//ICIL-1310
	private FixedLengthStringData t3644 = new FixedLengthStringData(5).init("T3644");	//ICIL-1310
	private FixedLengthStringData t6658 = new FixedLengthStringData(5).init("T6658");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC   ");
	private FixedLengthStringData covrrgwrec = new FixedLengthStringData(10).init("COVRRGWREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC   ");
}

protected void initializeScreenCustomerSpecific(){
	
}
}