package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BjrnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:01
 * Class transformed from BJRNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BjrnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 133;
	public FixedLengthStringData bjrnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData bjrnpfRecord = bjrnrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(bjrnrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(bjrnrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(bjrnrec);
	public PackedDecimalData bonusValue = DD.bonusvalue.copy().isAPartOf(bjrnrec);
	public PackedDecimalData adjustedArb = DD.adjarb.copy().isAPartOf(bjrnrec);
	public PackedDecimalData bonusDecDate = DD.bondecdate.copy().isAPartOf(bjrnrec);
	public PackedDecimalData adjBonusDecDate = DD.adjbondate.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(bjrnrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(bjrnrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(bjrnrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(bjrnrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(bjrnrec);
	public PackedDecimalData arbAdjustAmt = DD.arbadjamt.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(bjrnrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(bjrnrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public BjrnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for BjrnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public BjrnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for BjrnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public BjrnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for BjrnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public BjrnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("BJRNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"EFFDATE, " +
							"BONUSVALUE, " +
							"ADJARB, " +
							"BONDECDATE, " +
							"ADJBONDATE, " +
							"CRTABLE, " +
							"CNTTYPE, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"ARBADJAMT, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     effdate,
                                     bonusValue,
                                     adjustedArb,
                                     bonusDecDate,
                                     adjBonusDecDate,
                                     crtable,
                                     cnttype,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     arbAdjustAmt,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		effdate.clear();
  		bonusValue.clear();
  		adjustedArb.clear();
  		bonusDecDate.clear();
  		adjBonusDecDate.clear();
  		crtable.clear();
  		cnttype.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		arbAdjustAmt.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getBjrnrec() {
  		return bjrnrec;
	}

	public FixedLengthStringData getBjrnpfRecord() {
  		return bjrnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setBjrnrec(what);
	}

	public void setBjrnrec(Object what) {
  		this.bjrnrec.set(what);
	}

	public void setBjrnpfRecord(Object what) {
  		this.bjrnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(bjrnrec.getLength());
		result.set(bjrnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}