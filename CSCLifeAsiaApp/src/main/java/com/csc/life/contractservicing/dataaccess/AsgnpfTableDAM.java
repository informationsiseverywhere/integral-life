package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AsgnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:54
 * Class transformed from ASGNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AsgnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 97;
	public FixedLengthStringData asgnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData asgnpfRecord = asgnrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(asgnrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(asgnrec);
	public FixedLengthStringData asgnpfx = DD.asgnpfx.copy().isAPartOf(asgnrec);
	public FixedLengthStringData asgnnum = DD.asgnnum.copy().isAPartOf(asgnrec);
	public FixedLengthStringData assigneeName = DD.assigname.copy().isAPartOf(asgnrec);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(asgnrec);
	public PackedDecimalData commfrom = DD.commfrom.copy().isAPartOf(asgnrec);
	public PackedDecimalData commto = DD.commto.copy().isAPartOf(asgnrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(asgnrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(asgnrec);
	public FixedLengthStringData trancde = DD.trancde.copy().isAPartOf(asgnrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(asgnrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(asgnrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(asgnrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(asgnrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AsgnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AsgnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AsgnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AsgnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AsgnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AsgnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AsgnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ASGNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"ASGNPFX, " +
							"ASGNNUM, " +
							"ASSIGNAME, " +
							"REASONCD, " +
							"COMMFROM, " +
							"COMMTO, " +
							"TRANNO, " +
							"SEQNO, " +
							"TRANCDE, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     asgnpfx,
                                     asgnnum,
                                     assigneeName,
                                     reasoncd,
                                     commfrom,
                                     commto,
                                     tranno,
                                     seqno,
                                     trancde,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		asgnpfx.clear();
  		asgnnum.clear();
  		assigneeName.clear();
  		reasoncd.clear();
  		commfrom.clear();
  		commto.clear();
  		tranno.clear();
  		seqno.clear();
  		trancde.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAsgnrec() {
  		return asgnrec;
	}

	public FixedLengthStringData getAsgnpfRecord() {
  		return asgnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAsgnrec(what);
	}

	public void setAsgnrec(Object what) {
  		this.asgnrec.set(what);
	}

	public void setAsgnpfRecord(Object what) {
  		this.asgnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(asgnrec.getLength());
		result.set(asgnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}