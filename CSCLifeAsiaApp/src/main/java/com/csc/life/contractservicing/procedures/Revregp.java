/*
 * File: Revregp.java
 * Date: 30 August 2009 2:11:13
 * Author: Quipoz Limited
 * 
 * Class transformed from REVREGP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.FluprevTableDAM;
import com.csc.life.contractservicing.dataaccess.FuperevTableDAM;
import com.csc.life.contractservicing.dataaccess.RegprevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrrevTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE REGULAR PAYMENTS
*        ------------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse the results of a Regular
*   Payment. It will reverse records in the following files:-
*   Regular Payment (REGP), Accounting Movements (ACMV) and
*   Unit Transactions (UTRN).
*   The generic processing required for the UTRNs  will be
*    initiated via standard T5671 processing.
*
* Processing.
* -----------
*
* Reverse Regular Payment ( REGPs ):
*
*  Read the Regular Payment (REGPPF) using the logical view
*  REGPREV with a key of company, contract number, Life,
*  Coverage, Rider, regular payment number and tranno.
*
*  For each record found:
*   Check it is validflag '1'.
*    If not, then Read NEXT.
*   Check tranno on REGP record matches tranno we are trying
*    to reverse.
*     If tranno matches,
*        delete REGP record
*        read NEXT REGP record with same key
*          If REGP record matches key and validflag = '2'
*            reset validflag = '1' and REWRiTe REGP
*          end-if
*     else
*        read NEXT REGP record
*     end-if
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*   - Set the accounting month/year with the batch
*   - accounting month/year from linkage.
*   - Set the tranno with the NEW tranno from linkage.
*   - Set the batch key with new batch key from linkage.
*   - Multiply the original currency amount by -1.
*   - Multiply the accounting currency amount by -1.
*   - Set the transaction reference with the NEW
*   - transaction code from linkage.
*   - Set the transaction description with the long
*   - description of the new trans code from T1688.
*   - Set the effective date with todays date.
*   - Write the new record to the database by calling the
*     subroutine 'LIFACMV'.
*
* Generic processing ( For UTRNs etc... )
*
* Reversal of Unit-Linked/Traditional parts of the Contract:
*
*  Loop through all the Coverages/Riders associated with the
*   Contract we are processing and call any Generic processing
*   subroutines that are specified in T5671.
*
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing
*
*****************************************************************
* </pre>
*/
public class Revregp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVREGP ";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaRgpynum = new PackedDecimalData(5, 0).init(ZERO);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprevTableDAM fluprevIO = new FluprevTableDAM();
	private FuperevTableDAM fuperevIO = new FuperevTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrrevTableDAM payrrevIO = new PayrrevTableDAM();
	private RegprevTableDAM regprevIO = new RegprevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit3190, 
		exit8190, 
		exit9190
	}

	public Revregp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processRegps3000();
		processAcmvs4000();
		processAcmvOptical4010();
		genericProcessing5000();
		processChdr6000();
		processPayr7000();
		fluprevIO.setParams(SPACES);
		while ( !(isEQ(fluprevIO.getStatuz(), varcom.endp))) {
			reverseFluprev8000();
		}
		
		fuperevIO.setParams(SPACES);
		while ( !(isEQ(fuperevIO.getStatuz(), varcom.endp))) {
			reverseFuperev9000();
		}
		
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError9000();
		}
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
		/* read Contract Header file*/
		readContract2200();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void readContract2200()
	{
		/*START*/
		/* Need to get Contract type*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(reverserec.company);
		chdrenqIO.setChdrnum(reverserec.chdrnum);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			databaseError9500();
		}
		/*EXIT*/
	}

protected void processRegps3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Any Regular Payment REGP records we find that match on*/
		/* Company/Contract number/Life/Coverage/Rider/Regular*/
		/* Payment number/transaction number will be processed as*/
		/* follows:*/
		/* If REGP found which matches above criteria, delete and try to*/
		/*  reinstate the previous validflag '2' to validflag '1'.*/
		/*  Do NOT amend the transaction number on the REGP so that in*/
		/*  turn that REGP record can be reversed out.*/
		regprevIO.setParams(SPACES);
		regprevIO.setChdrcoy(reverserec.company);
		regprevIO.setChdrnum(reverserec.chdrnum);
		regprevIO.setLife(SPACES);
		regprevIO.setCoverage(SPACES);
		regprevIO.setRider(SPACES);
		regprevIO.setRgpynum(ZERO);
		regprevIO.setTranno(9999);
		regprevIO.setFormat(formatsInner.regprevrec);
		regprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regprevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regprevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(), varcom.oK)
		&& isNE(regprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, regprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, regprevIO.getChdrnum())
		|| isNE(regprevIO.getValidflag(), "1")
		|| isEQ(regprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		while ( !(isEQ(regprevIO.getStatuz(), varcom.endp))) {
			reverseRegps3100();
		}
		
	}

protected void reverseRegps3100()
	{
		try {
			start3100();
			getNextRegp3180();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start3100()
	{
		/* Check REGP record found*/
		/* If it matches our selection parameters, then process*/
		if (isEQ(regprevIO.getTranno(), reverserec.tranno)
		&& isEQ(regprevIO.getValidflag(), "1")) {
			wsaaLife.set(regprevIO.getLife());
			wsaaCoverage.set(regprevIO.getCoverage());
			wsaaRider.set(regprevIO.getRider());
			wsaaRgpynum.set(regprevIO.getRgpynum());
			deletRewrtRegps3200();
			goTo(GotoLabel.exit3190);
		}
	}

	/**
	* <pre>
	* Get next REGP record
	* </pre>
	*/
protected void getNextRegp3180()
	{
		regprevIO.setFunction(varcom.nextr);
		regprevIO.setFormat(formatsInner.regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(), varcom.oK)
		&& isNE(regprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, regprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, regprevIO.getChdrnum())
		|| isEQ(regprevIO.getStatuz(), varcom.endp)) {
			regprevIO.setStatuz(varcom.endp);
		}
	}

protected void deletRewrtRegps3200()
	{
		start3200();
	}

protected void start3200()
	{
		/* Delete current validflag '1' REGP*/
		regprevIO.setFunction(varcom.readh);
		regprevIO.setFormat(formatsInner.regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		regprevIO.setFunction(varcom.delet);
		regprevIO.setFormat(formatsInner.regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		/* See if there is a validflag '2' with the same key (apart from*/
		/*  the tranno) and if so, reinstate to validflag '1'.*/
		regprevIO.setFunction(varcom.nextr);
		regprevIO.setFormat(formatsInner.regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(), varcom.oK)
		&& isNE(regprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		/* If the company, contract number or STATUZ = ENDP then there*/
		/*  are no more REGP records to process*/
		if (isNE(reverserec.company, regprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, regprevIO.getChdrnum())
		|| isEQ(regprevIO.getStatuz(), varcom.endp)) {
			regprevIO.setStatuz(varcom.endp);
			return ;
		}
		/* If the Life, Coverage, Rider or Regular payment number has*/
		/*  changed then there are no more REGP records for this component*/
		/*  to process.*/
		if (isNE(wsaaLife, regprevIO.getLife())
		|| isNE(wsaaCoverage, regprevIO.getCoverage())
		|| isNE(wsaaRider, regprevIO.getRider())
		|| isNE(wsaaRgpynum, regprevIO.getRgpynum())) {
			return ;
		}
		/* Get here, so the REGP record found matches out criteria so*/
		/*  we will reinstate from validflag '2' to validflag '1'.*/
		regprevIO.setFunction(varcom.readh);
		regprevIO.setFormat(formatsInner.regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
		regprevIO.setValidflag("1");
		regprevIO.setFunction(varcom.rewrt);
		regprevIO.setFormat(formatsInner.regprevrec);
		SmartFileCode.execute(appVars, regprevIO);
		if (isNE(regprevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regprevIO.getParams());
			syserrrec.statuz.set(regprevIO.getStatuz());
			databaseError9500();
		}
	}

protected void processAcmvs4000()
	{
		start4000();
	}

protected void start4000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
	}

protected void processAcmvOptical4010()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError9500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError9500();
			}
		}
	}

protected void reverseAcmvs4100()
	{
		start4100();
		readNextAcmv4180();
	}

protected void start4100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError9000();
		}
	}

protected void readNextAcmv4180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void genericProcessing5000()
	{
		start5000();
	}

protected void start5000()
	{
		/* We will read through all the Coverage & riders attached to the*/
		/*  contract that we are processing, and for each one we will*/
		/*  do any required generic processing by calling subroutine(s)*/
		/* specified on table T5671*/
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs5100();
		}
		
	}

protected void processCovrs5100()
	{
		start5100();
	}

protected void start5100()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* check whether Life/Coverage/Rider have changed - if so, we*/
		/*  need to call any generic Subroutines that are on T5671*/
		if (isNE(covrenqIO.getLife(), wsaaLife)
		|| isNE(covrenqIO.getCoverage(), wsaaCoverage)
		|| isNE(covrenqIO.getRider(), wsaaRider)
		|| isNE(covrenqIO.getPlanSuffix(), wsaaPlanSuffix)) {
			genericSubr5200();
			wsaaLife.set(covrenqIO.getLife());
			wsaaCoverage.set(covrenqIO.getCoverage());
			wsaaRider.set(covrenqIO.getRider());
			wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void genericSubr5200()
	{
		start5200();
	}

protected void start5200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(ZERO);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs5300();
		}
	}

protected void callTrevsubs5300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError9000();
			}
		}
		/*EXIT*/
	}

protected void processChdr6000()
	{
		start6010();
	}

protected void start6010()
	{
		/*  Read and delete current CHDR record.                        */
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		/* MOVE BEGNH                  TO CHDRMJA-FUNCTION.     <LA3993>*/
		chdrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(), "1")
		|| isEQ(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		/* Record is not held as BEGNH is replaced by BEGN, so          */
		/* REWRT is not necessary.                                      */
		if (isNE(chdrmjaIO.getTranno(), reverserec.tranno)) {
			/*     MOVE REWRT              TO CHDRMJA-FUNCTION      <LA3993>*/
			/*     CALL 'CHDRMJAIO'        USING CHDRMJA-PARAMS     <LA3993>*/
			/*     IF  CHDRMJA-STATUZ      NOT = O-K                <LA3993>*/
			/*         MOVE CHDRMJA-PARAMS TO SYSR-PARAMS           <LA3993>*/
			/*         MOVE CHDRMJA-STATUZ TO SYSR-STATUZ           <LA3993>*/
			/*         PERFORM 9500-DATABASE-ERROR                  <LA3993>*/
			/*     END-IF                                           <LA3993>*/
			return ;
		}
		/* MOVE DELET                  TO CHDRMJA-FUNCTION.     <LA3993>*/
		chdrmjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		/* Re-instate the previous CHDR record.                         */
		chdrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(), "2")
		|| isEQ(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		/* MOVE REWRT                  TO CHDRMJA-FUNCTION.     <LA3993>*/
		chdrmjaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError9500();
		}
	}

protected void processPayr7000()
	{
		start7010();
	}

protected void start7010()
	{
		/*  Read and delete the current PAYR record.                    */
		payrrevIO.setParams(SPACES);
		payrrevIO.setChdrcoy(reverserec.company);
		payrrevIO.setChdrnum(reverserec.chdrnum);
		payrrevIO.setTranno(reverserec.tranno);
		payrrevIO.setPayrseqno(1);
		payrrevIO.setFormat(formatsInner.payrrevrec);
		/* MOVE BEGNH                  TO PAYRREV-FUNCTION.     <LA3993>*/
		payrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(), varcom.oK)
		&& isNE(payrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			databaseError9500();
		}
		/* IF PAYRREV-CHDRCOY       NOT = REVE-COMPANY          <LFA1136*/
		/*    OR PAYRREV-CHDRNUM    NOT = REVE-CHDRNUM          <LFA1136*/
		/*    OR PAYRREV-VALIDFLAG  NOT = '1'                   <LFA1136*/
		/*    OR PAYRREV-STATUZ         = ENDP                  <LFA1136*/
		/*     MOVE PAYRREV-PARAMS     TO SYSR-PARAMS           <LFA1136*/
		/*     MOVE PAYRREV-STATUZ     TO SYSR-STATUZ           <LFA1136*/
		/*     PERFORM 9500-DATABASE-ERROR                      <LFA1136*/
		/* END-IF.                                              <LFA1136*/
		if (isEQ(payrrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Record is not held as BEGNH is replaced by BEGN, so          */
		/* REWRT is not necessary.                                      */
		if (isNE(payrrevIO.getChdrcoy(), reverserec.company)
		|| isNE(payrrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrrevIO.getValidflag(), "1")
		|| isNE(payrrevIO.getTranno(), reverserec.tranno)) {
			/*     MOVE REWRT              TO PAYRREV-FUNCTION      <LA3993>*/
			/*     CALL 'PAYRREVIO'        USING PAYRREV-PARAMS     <LA3993>*/
			/*     IF  PAYRREV-STATUZ      NOT = O-K                <LA3993>*/
			/*         MOVE PAYRREV-PARAMS TO SYSR-PARAMS           <LA3993>*/
			/*         MOVE PAYRREV-STATUZ TO SYSR-STATUZ           <LA3993>*/
			/*         PERFORM 9500-DATABASE-ERROR                  <LA3993>*/
			/*     END-IF                                           <LA3993>*/
			return ;
		}
		/* MOVE DELET                  TO PAYRREV-FUNCTION.     <LA3993>*/
		payrrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			databaseError9500();
		}
		/* Re-instate the previous PAYR record.                         */
		payrrevIO.setFunction(varcom.nextp);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(), varcom.oK)
		&& isNE(payrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(payrrevIO.getChdrcoy(), reverserec.company)
		|| isNE(payrrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrrevIO.getValidflag(), "2")
		|| isEQ(payrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			databaseError9500();
		}
		payrrevIO.setValidflag("1");
		/* MOVE REWRT                  TO PAYRREV-FUNCTION.     <LA3993>*/
		payrrevIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrrevIO.getParams());
			syserrrec.statuz.set(payrrevIO.getStatuz());
			databaseError9500();
		}
	}

protected void reverseFluprev8000()
	{
		try {
			begnh8110();
			delete8150();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begnh8110()
	{
		fluprevIO.setChdrnum(reverserec.chdrnum);
		fluprevIO.setChdrcoy(reverserec.company);
		fluprevIO.setTranno(reverserec.tranno);
		fluprevIO.setFormat(formatsInner.fluprevrec);
		fluprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluprevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, fluprevIO);
		if ((isNE(fluprevIO.getStatuz(), varcom.oK))
		&& (isNE(fluprevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprevIO.getParams());
			systemError9000();
		}
		if ((isNE(fluprevIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(fluprevIO.getChdrcoy(), reverserec.company))
		|| (isNE(fluprevIO.getTranno(), reverserec.tranno))
		|| (isEQ(fluprevIO.getStatuz(), varcom.endp))) {
			fluprevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit8190);
		}
	}

protected void delete8150()
	{
		fluprevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fluprevIO);
		if ((isNE(fluprevIO.getStatuz(), varcom.oK))
		&& (isNE(fluprevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprevIO.getParams());
			systemError9000();
		}
	}

protected void reverseFuperev9000()
	{
		try {
			begnh9110();
			delete9150();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begnh9110()
	{
		fuperevIO.setChdrnum(reverserec.chdrnum);
		fuperevIO.setChdrcoy(reverserec.company);
		fuperevIO.setTranno(reverserec.tranno);
		fuperevIO.setFormat(formatsInner.fuperevrec);
		fuperevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fuperevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, fuperevIO);
		if ((isNE(fuperevIO.getStatuz(), varcom.oK))
		&& (isNE(fuperevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fuperevIO.getParams());
			systemError9000();
		}
		if ((isNE(fuperevIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(fuperevIO.getChdrcoy(), reverserec.company))
		|| (isNE(fuperevIO.getTranno(), reverserec.tranno))
		|| (isEQ(fuperevIO.getStatuz(), varcom.endp))) {
			fuperevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit9190);
		}
	}

protected void delete9150()
	{
		fuperevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fuperevIO);
		if ((isNE(fuperevIO.getStatuz(), varcom.oK))
		&& (isNE(fuperevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fuperevIO.getParams());
			systemError9000();
		}
	}

protected void systemError9000()
	{
		start9000();
		exit9490();
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		start9500();
		exit9990();
	}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData payrrevrec = new FixedLengthStringData(10).init("PAYRREVREC");
	private FixedLengthStringData regprevrec = new FixedLengthStringData(10).init("REGPREVREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData fluprevrec = new FixedLengthStringData(10).init("FLUPREVREC");
	private FixedLengthStringData fuperevrec = new FixedLengthStringData(10).init("FUPEREVREC");
}
}
