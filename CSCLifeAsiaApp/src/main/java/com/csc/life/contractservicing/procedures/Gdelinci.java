/*
 * File: Gdelinci.java
 * Date: 29 August 2009 22:49:36
 * Author: Quipoz Limited
 * 
 * Class transformed from GDELINCI.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.IncirevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*
* Generalised Reversal subroutine for DELETION of INCI records
* ------------------------------------------------------------
*
* This subroutine is called by reversal subroutines via
* table T5671. The parameter passed to this subroutine is
* contained in the GREVERSREC copybook.
*
* SPECIFICALLY, this subroutine is called by REVCOMPA, the
*  Component Add Reversal subroutine to DELETe any INCI records
*  created by the forward Component Add transaction.
*
* Processing is as follows:
*
*    Do a BEGN on the INCI file using the INCIREV logical view
*     and specifying the Company, Contract Number, Life,
*     Coverage, Rider and Plan Suffix.
*
*      For each INCI record read which matches the above key,
*       Read-lock and DELETe.
*
*    Return to the calling subroutine when no more INCIs are
*     found which match on the above specified component key.
*
*
*****************************************************************
* </pre>
*/
public class Gdelinci extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String incirevrec = "INCIREVREC";
	private Greversrec greversrec = new Greversrec();
		/*Individual Increase Details*/
	private IncirevTableDAM incirevIO = new IncirevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		exit2190, 
		exit9490
	}

	public Gdelinci() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		greversrec.statuz.set(varcom.oK);
		processIncis2000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processIncis2000()
	{
		try {
			start2000();
		}
		catch (GOTOException e){
		}
	}

protected void start2000()
	{
		incirevIO.setParams(SPACES);
		incirevIO.setChdrcoy(greversrec.chdrcoy);
		incirevIO.setChdrnum(greversrec.chdrnum);
		incirevIO.setCoverage(greversrec.coverage);
		incirevIO.setLife(greversrec.life);
		incirevIO.setRider(greversrec.rider);
		incirevIO.setPlanSuffix(greversrec.planSuffix);
		incirevIO.setTranno(ZERO);
		incirevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incirevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incirevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		incirevIO.setFormat(incirevrec);
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)
		&& isNE(incirevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			systemError9000();
		}
		if (isEQ(incirevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(greversrec.chdrcoy,incirevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,incirevIO.getChdrnum())
		|| isNE(greversrec.life,incirevIO.getLife())
		|| isNE(greversrec.coverage,incirevIO.getCoverage())
		|| isNE(greversrec.rider,incirevIO.getRider())
		|| isNE(greversrec.planSuffix,incirevIO.getPlanSuffix())) {
			incirevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		while ( !(isEQ(incirevIO.getStatuz(),varcom.endp))) {
			deleteIncis2100();
		}
		
	}

protected void deleteIncis2100()
	{
		try {
			start2100();
		}
		catch (GOTOException e){
		}
	}

protected void start2100()
	{
		incirevIO.setFunction(varcom.deltd);
		incirevIO.setFormat(incirevrec);
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			systemError9000();
		}
		incirevIO.setFunction(varcom.nextr);
		incirevIO.setFormat(incirevrec);
		SmartFileCode.execute(appVars, incirevIO);
		if (isNE(incirevIO.getStatuz(),varcom.oK)
		&& isNE(incirevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incirevIO.getParams());
			syserrrec.statuz.set(incirevIO.getStatuz());
			systemError9000();
		}
		if (isEQ(incirevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		if (isNE(greversrec.chdrcoy,incirevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,incirevIO.getChdrnum())
		|| isNE(greversrec.life,incirevIO.getLife())
		|| isNE(greversrec.coverage,incirevIO.getCoverage())
		|| isNE(greversrec.rider,incirevIO.getRider())
		|| isNE(greversrec.planSuffix,incirevIO.getPlanSuffix())) {
			incirevIO.setStatuz(varcom.endp);
		}
	}

protected void systemError9000()
	{
		try {
			start9000();
		}
		catch (GOTOException e){
		}
		finally{
			exit9490();
		}
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit9490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		greversrec.statuz.set(varcom.bomb);
		exit1090();
	}
}
