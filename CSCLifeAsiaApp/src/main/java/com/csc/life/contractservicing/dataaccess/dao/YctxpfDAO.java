package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;

import com.csc.life.contractservicing.dataaccess.model.Yctxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface YctxpfDAO extends BaseDAO<Yctxpf>{
	public Yctxpf fetchRecord(String chdrcoy, String chdrnum);
	public boolean insertRecord(List<Yctxpf> yctx);
	public boolean updateRecord(List<Yctxpf> yctx);
	public Yctxpf fetchRecord(String chdrcoy, String chdrnum, int startdate);
}
