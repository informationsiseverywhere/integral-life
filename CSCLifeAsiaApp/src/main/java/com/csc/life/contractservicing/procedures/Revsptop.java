/*
 * File: Revsptop.java
 * Date: 30 August 2009 2:11:40
 * Author: Quipoz Limited
 * 
 * Class transformed from REVSPTOP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZctnrevTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.dataaccess.ZptnrevTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrevTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LextchgTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.RtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.CovrrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstnudTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE SINGLE PREMIUM TOP-UP.
*        ------------------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse the results of Single Premium
*   injections to Unit-linked contracts. It will reverse all
*   Non-cash Accounting records (i.e ACMVs), processed Unit
*   Transaction records (i.e UTRNs), Receipts (i.e. RTRNs) and
*   Agent Commission records (i.e. AGCMs) which match on the
*   specified key... the key being Component, Transaction
*   number and Transaction Code.
*   Any matching but unprocessed Unit Transaction (UTRN) records
*    will be deleted.
*
*
* Processing.
* -----------
*
* Reverse Accounting Movement ( RTRN ):
*
*  Read the file (RTRNPF) using the logical view RTRNREV with
*  a key of contract number/transaction number from linkage.
*
*  For each record found:
*   Write a new record with the following...
*     Set the accounting month/year with the batch
*     accounting month/year from linkage.
*     Set the tranno with the NEW tranno from linkage.
*     Set the batch key with new batch key from linkage.
*     Multiply the original currency amount by -1.
*     Multiply the accounting currency amount by -1.
*     Set the transaction reference with the NEW
*     transaction code from linkage.
*     Set the transaction description with the long
*     description of the new trans code from T1688.
*     Set the effective date with todays date.
*     Write the new record to the database by calling the
*     subroutine 'LIFRTRN'.
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*     Set the accounting month/year with the batch
*     accounting month/year from linkage.
*     Set the tranno with the NEW tranno from linkage.
*     Set the batch key with new batch key from linkage.
*     Multiply the original currency amount by -1.
*     Multiply the accounting currency amount by -1.
*     Set the transaction reference with the NEW
*     transaction code from linkage.
*     Set the transaction description with the long
*     description of the new trans code from T1688.
*     Set the effective date with todays date.
*     Write the new record to the database by calling the
*     subroutine 'LIFACMV'.
*
* Reversal of Unit-Linked/Traditional parts of the Contract:
*
*  Loop through all the Coverages/Riders associated with the
*   Contract we are processing and call any Generic processing
*   subroutines that are specified in T5671.
*
* Update Agent commission records ( AGCMs ):
*
*  Read the Agents commission file (AGCMPF) with the logical
*  view AGCMREV using the contract number as the key.
*
*  For each record found carry out the following processing:-
*               
*          Delete the record.
*
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing
*
******************Enhancements for Life Asia 1.0****************
*
* This reversal routine will reverse COVR records processed by
* P5147AT, single premium top-up.
*
*****************************************************************
* </pre>
*/
public class Revsptop extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVSPTOP";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscode = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	private FixedLengthStringData wsaaVflag2 = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaT6661Exist = new FixedLengthStringData(1);
	private Validator t6661Found = new Validator(wsaaT6661Exist, "Y");
	private Validator t6661Nfnd = new Validator(wsaaT6661Exist, "N");

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";

	private FixedLengthStringData wsaaValidRec = new FixedLengthStringData(1);
	private Validator validRecFound = new Validator(wsaaValidRec, "Y");
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private static final String t6661 = "T6661";
	private static final String th605 = "TH605";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmrevTableDAM agcmrevIO = new AgcmrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrrevTableDAM covrrevIO = new CovrrevTableDAM();
	private CovrrnlTableDAM covrrnlIO = new CovrrnlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextchgTableDAM lextchgIO = new LextchgTableDAM();
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
	private RtrnrevTableDAM rtrnrevIO = new RtrnrevTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZctnrevTableDAM zctnrevIO = new ZctnrevTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZptnrevTableDAM zptnrevIO = new ZptnrevTableDAM();
	private ZrstnudTableDAM zrstnudIO = new ZrstnudTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Th605rec th605rec = new Th605rec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue4100, 
		readNextAcmv4180, 
		readNextAgcm5180, 
		exit5190, 
		a119Exit, 
		a219Exit, 
		a380Nextr, 
		a390Exit, 
		a450Next, 
		a450Exit, 
		call7020, 
		exit7090, 
		call8020, 
		exit8090
	}

	public Revsptop() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processRtrns3000();
		processAcmvs4000();
		processAcmvOptical4010();
		processAgcms5000();
		a100ProcessCovr();
		a400ProcessTaxd();
		genericProcessing6000();
		/* Read table TH605 to see if Bonus Workbench Extraction used      */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(th605);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench  *                                              */
		processZptn7000();
		processZctn8000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* MOVE ZEROS                  TO CLNK-CURRTO.          <V5L007>*/
		/* MOVE ZEROS                  TO CLNK-TARGET-PREM.     <V5L007>*/
		/* MOVE ZEROS                  TO CLNK-SEQNO.           <V5L007>*/
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError9000();
		}
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
		/* read Contract Header file*/
		readContract2200();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void readContract2200()
	{
		/*START*/
		/* Need to get Contract type*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(reverserec.company);
		chdrenqIO.setChdrnum(reverserec.chdrnum);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			databaseError9500();
		}
		/*EXIT*/
	}

protected void processRtrns3000()
	{
		start3000();
	}

protected void start3000()
	{
		rtrnrevIO.setParams(SPACES);
		rtrnrevIO.setRldgcoy(reverserec.company);
		rtrnrevIO.setRdocnum(reverserec.chdrnum);
		rtrnrevIO.setTranno(reverserec.tranno);
		rtrnrevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(rtrnrevIO.getStatuz(), varcom.endp))) {
			reverseRtrns3100();
		}
		
	}

protected void reverseRtrns3100()
	{
		start3100();
		nextRtrnrev3180();
	}

protected void start3100()
	{
		/* check if transaction codes match before processing RTRN*/
		if (isNE(reverserec.oldBatctrcde, rtrnrevIO.getBatctrcde())) {
			return ;
		}
		/*     Write a new record to the RTRN file*/
		/*     by calling the subroutine LIFRTRN.*/
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(reverserec.batchkey);
		lifrtrnrec.rdocnum.set(chdrenqIO.getChdrnum());
		lifrtrnrec.rldgacct.set(chdrenqIO.getChdrnum());
		lifrtrnrec.tranno.set(reverserec.newTranno);
		lifrtrnrec.jrnseq.set(rtrnrevIO.getTranseq());
		/* MOVE CHDRENQ-CHDRCOY        TO LIFR-RLDGCOY.                 */
		lifrtrnrec.rldgcoy.set(rtrnrevIO.getRldgcoy());
		lifrtrnrec.sacscode.set(rtrnrevIO.getSacscode());
		lifrtrnrec.sacstyp.set(rtrnrevIO.getSacstyp());
		lifrtrnrec.trandesc.set(wsaaTransDesc);
		lifrtrnrec.crate.set(rtrnrevIO.getCrate());
		lifrtrnrec.transactionTime.set(rtrnrevIO.getTrandate());
		lifrtrnrec.transactionDate.set(rtrnrevIO.getTrantime());
		lifrtrnrec.user.set(reverserec.user);
		lifrtrnrec.termid.set(reverserec.termid);
		compute(lifrtrnrec.acctamt, 2).set(mult(rtrnrevIO.getAcctamt(), -1));
		compute(lifrtrnrec.origamt, 2).set(mult(rtrnrevIO.getOrigamt(), -1));
		lifrtrnrec.genlcur.set(rtrnrevIO.getGenlcur());
		lifrtrnrec.origcurr.set(rtrnrevIO.getOrigccy());
		lifrtrnrec.genlcoy.set(rtrnrevIO.getGenlcoy());
		lifrtrnrec.glcode.set(rtrnrevIO.getGlcode());
		lifrtrnrec.glsign.set(rtrnrevIO.getGlsign());
		lifrtrnrec.postyear.set(rtrnrevIO.getPostyear());
		lifrtrnrec.postmonth.set(rtrnrevIO.getPostmonth());
		lifrtrnrec.effdate.set(datcon1rec.intDate);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(1);
		lifrtrnrec.substituteCode[1].set(chdrenqIO.getCnttype());
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserrrec.statuz.set(lifrtrnrec.statuz);
			systemError9000();
		}
	}

protected void nextRtrnrev3180()
	{
		rtrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAcmvs4000()
	{
		start4000();
	}

protected void start4000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
	}

protected void processAcmvOptical4010()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError9500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError9500();
			}
		}
	}

protected void reverseAcmvs4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start4100();
				case continue4100: 
					continue4100();
				case readNextAcmv4180: 
					readNextAcmv4180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start4100()
	{
		itemIO.setParams(SPACES);
		wsaaT6661Exist.set("N");
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(acmvrevIO.getRldgcoy());
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(acmvrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT6661Exist.set("N");
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				wsaaT6661Exist.set("Y");
			}
		}
		if (t6661Nfnd.isTrue()) {
			goTo(GotoLabel.continue4100);
		}
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			goTo(GotoLabel.readNextAcmv4180);
		}
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			goTo(GotoLabel.readNextAcmv4180);
		}
	}

	/**
	* <pre>
	* post equal & opposite amount to ACMV just read
	* </pre>
	*/
protected void continue4100()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.effdate.set(datcon1rec.intDate);
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		if (t6661Nfnd.isTrue()) {
			lifacmvrec.suprflag.set("Y");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError9000();
		}
	}

protected void readNextAcmv4180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAgcms5000()
	{
		/*START*/
		agcmrevIO.setParams(SPACES);
		agcmrevIO.setChdrnum(reverserec.chdrnum);
		agcmrevIO.setChdrcoy(reverserec.company);
		agcmrevIO.setPlanSuffix(ZERO);
		/* MOVE BEGNH                  TO AGCMREV-FUNCTION.             */
		agcmrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(agcmrevIO.getStatuz(),varcom.endp))) {
			reverseAgcms5100();
		}
		
		/*EXIT*/
	}

protected void reverseAgcms5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5100();
				case readNextAgcm5180: 
					readNextAgcm5180();
				case exit5190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5100()
	{
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.endp)
		&& isNE(agcmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrevIO.getParams());
			syserrrec.statuz.set(agcmrevIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(agcmrevIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit5190);
		}
		/* check AGCM record read is associated with contract*/
		/* - If not, Release record*/
		/* BEGNH is replaced by BEGN,so record is not held,RLSE is not     */
		/* needed.                                                         */
		if (isNE(reverserec.company, agcmrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, agcmrevIO.getChdrnum())) {
			/*     MOVE RLSE               TO AGCMREV-FUNCTION              */
			/*     CALL 'AGCMREVIO'        USING AGCMREV-PARAMS             */
			/*     IF AGCMREV-STATUZ       NOT = O-K                        */
			/*         MOVE AGCMREV-PARAMS TO SYSR-PARAMS                   */
			/*         MOVE AGCMREV-STATUZ TO SYSR-STATUZ                   */
			/*         PERFORM 9500-DATABASE-ERROR                          */
			/*     END-IF                                                   */
			agcmrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5190);
		}
		/* Get here, so AGCM exists for contract,*/
		/* Check tranno on AGCMREV record just read is OK for us to*/
		/*  reverse - else read Next AGCMREV record*/
		if (isNE(reverserec.tranno, agcmrevIO.getTranno())) {
			goTo(GotoLabel.readNextAgcm5180);
		}
		/* Update fields on AGCM record*/
		/*    MOVE ZEROS                  TO AGCMREV-COMPAY.               */
		/*    MOVE ZEROS                  TO AGCMREV-COMERN.               */
		/*    MOVE REVE-NEW-TRANNO        TO AGCMREV-TRANNO.               */
		/*    MOVE REWRT                  TO AGCMREV-FUNCTION.             */
		/* MOVE DELET                  TO AGCMREV-FUNCTION.     <LA3993>*/
		agcmrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrevIO.getParams());
			syserrrec.statuz.set(agcmrevIO.getStatuz());
			systemError9000();
		}
	}

protected void readNextAgcm5180()
	{
		agcmrevIO.setFunction(varcom.nextr);
	}

protected void a100ProcessCovr()
	{
		/*A101-PARA*/
		covrIO.setParams(SPACES);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setLife(SPACES);
		covrIO.setRider(SPACES);
		covrIO.setCoverage(SPACES);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		/* MOVE BEGNH                  TO COVR-FUNCTION.        <LA3993>*/
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaValidRec.set("N");
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			a110ReverseCovr();
		}
		
		/*A109-EXIT*/
	}

protected void a110ReverseCovr()
	{
		try {
			a111Para();
			a118Nextr();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void a111Para()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.endp)
		&& isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			databaseError9500();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a119Exit);
		}
		if (isGTE(covrIO.getTranno(), reverserec.tranno)) {
			/*    MOVE DELET               TO COVR-FUNCTION         <LA3993>*/
			covrIO.setFunction(varcom.deltd);
			wsaaValidRec.set("N");
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrIO.getParams());
				databaseError9500();
			}
			a200ProcessLexts();
		}
		if (isLT(covrIO.getTranno(), reverserec.tranno)
		&& isEQ(covrIO.getValidflag(), "2")
		&& !validRecFound.isTrue()) {
			covrIO.setValidflag("1");
			covrIO.setCurrto(varcom.vrcmMaxDate);
			/*     MOVE REWRT              TO COVR-FUNCTION         <LA3993>*/
			covrIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrIO.getParams());
				databaseError9500();
			}
			wsaaValidRec.set("Y");
			b100ReverseZrst();
		}
	}

protected void a118Nextr()
	{
		covrIO.setFunction(varcom.nextr);
	}

protected void a200ProcessLexts()
	{
		/*A201-PARA*/
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrnum(reverserec.chdrnum);
		lextrevIO.setChdrcoy(reverserec.company);
		lextrevIO.setLife(covrIO.getLife());
		lextrevIO.setCoverage(covrIO.getCoverage());
		lextrevIO.setRider(covrIO.getRider());
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while ( !(isEQ(lextrevIO.getStatuz(),varcom.endp))) {
			a210ReverseLext();
		}
		
		/*A209-EXIT*/
	}

protected void a210ReverseLext()
	{
		try {
			a211Para();
			a218Nextr();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void a211Para()
	{
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.endp)
		&& isNE(lextrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lextrevIO.getParams());
			databaseError9500();
		}
		if (isNE(lextrevIO.getChdrcoy(), reverserec.company)
		|| isNE(lextrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(lextrevIO.getLife(), covrIO.getLife())
		|| isNE(lextrevIO.getRider(), covrIO.getRider())
		|| isNE(lextrevIO.getCoverage(), covrIO.getCoverage())
		|| isEQ(lextrevIO.getStatuz(), varcom.endp)) {
			lextrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a219Exit);
		}
		if (isEQ(lextrevIO.getTranno(), reverserec.tranno)) {
			if (isEQ(lextrevIO.getValidflag(), "1")) {
				lextrevIO.setFunction(varcom.readh);
				lextrevIO.setFormat(formatsInner.lextrevrec);
				SmartFileCode.execute(appVars, lextrevIO);
				if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lextrevIO.getParams());
					databaseError9500();
				}
				lextrevIO.setFunction(varcom.delet);
				SmartFileCode.execute(appVars, lextrevIO);
				if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lextrevIO.getParams());
					databaseError9500();
				}
			}
			if (isEQ(lextrevIO.getValidflag(), "2")) {
				lextrevIO.setTranno(reverserec.newTranno);
				lextrevIO.setValidflag("1");
				lextrevIO.setFunction(varcom.writd);
				SmartFileCode.execute(appVars, lextrevIO);
				if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lextrevIO.getParams());
					databaseError9500();
				}
			}
		}
		else {
			if (isEQ(lextrevIO.getValidflag(), "1")) {
				lextchgIO.setParams(SPACES);
				lextchgIO.setChdrcoy(lextrevIO.getChdrcoy());
				lextchgIO.setChdrnum(lextrevIO.getChdrnum());
				lextchgIO.setLife(lextrevIO.getLife());
				lextchgIO.setCoverage(lextrevIO.getCoverage());
				lextchgIO.setRider(lextrevIO.getRider());
				lextchgIO.setSeqnbr(lextrevIO.getSeqnbr());
				lextchgIO.setTranno(ZERO);
				/*       MOVE BEGNH            TO LEXTCHG-FUNCTION      <LA3993>*/
				lextchgIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				lextchgIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				lextchgIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "SEQNBR");
				lextchgIO.setFormat(formatsInner.lextchgrec);
				wsaaVflag2.set("N");
				while ( !(isEQ(lextchgIO.getStatuz(), varcom.endp))) {
					a300CheckVflag2Lext();
				}
				
				if (isEQ(wsaaVflag2, "Y")) {
					lextrevIO.setFunction(varcom.delet);
					lextrevIO.setFormat(formatsInner.lextrevrec);
					SmartFileCode.execute(appVars, lextrevIO);
					if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(lextrevIO.getParams());
						databaseError9500();
					}
					lextrevIO.setValidflag("2");
					lextrevIO.setTranno(reverserec.tranno);
					lextrevIO.setFunction(varcom.readh);
					lextrevIO.setFormat(formatsInner.lextrevrec);
					SmartFileCode.execute(appVars, lextrevIO);
					if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(lextrevIO.getParams());
						databaseError9500();
					}
					lextrevIO.setTranno(reverserec.newTranno);
					lextrevIO.setValidflag("1");
					lextrevIO.setFunction(varcom.rewrt);
					lextrevIO.setFormat(formatsInner.lextrevrec);
					SmartFileCode.execute(appVars, lextrevIO);
					if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(lextrevIO.getParams());
						databaseError9500();
					}
				}
			}
		}
	}

protected void a218Nextr()
	{
		lextrevIO.setFunction(varcom.nextr);
	}

protected void a300CheckVflag2Lext()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a310Para();
				case a380Nextr: 
					a380Nextr();
				case a390Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a310Para()
	{
		SmartFileCode.execute(appVars, lextchgIO);
		if (isNE(lextchgIO.getStatuz(), varcom.oK)
		&& isNE(lextchgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextchgIO.getParams());
			databaseError9500();
		}
		if (isNE(lextrevIO.getChdrcoy(), lextchgIO.getChdrcoy())
		|| isNE(lextrevIO.getChdrnum(), lextchgIO.getChdrnum())
		|| isNE(lextrevIO.getLife(), lextchgIO.getLife())
		|| isNE(lextrevIO.getCoverage(), lextchgIO.getCoverage())
		|| isNE(lextrevIO.getRider(), lextchgIO.getRider())
		|| isNE(lextrevIO.getSeqnbr(), lextchgIO.getSeqnbr())
		|| isEQ(lextchgIO.getStatuz(), varcom.endp)) {
			lextchgIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a390Exit);
		}
		if (isLT(lextchgIO.getTranno(), reverserec.tranno)) {
			goTo(GotoLabel.a380Nextr);
		}
		if (isEQ(lextchgIO.getTranno(), reverserec.tranno)) {
			wsaaVflag2.set("Y");
			goTo(GotoLabel.a380Nextr);
		}
		if (isGT(lextchgIO.getTranno(), reverserec.tranno)
		&& isEQ(wsaaVflag2, "Y")) {
			/*     MOVE DELET              TO LEXTCHG-FUNCTION      <LA3993>*/
			lextchgIO.setFunction(varcom.deltd);
			lextchgIO.setFormat(formatsInner.lextchgrec);
			SmartFileCode.execute(appVars, lextchgIO);
			if (isNE(lextchgIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lextchgIO.getParams());
				databaseError9500();
			}
		}
	}

protected void a380Nextr()
	{
		lextchgIO.setFunction(varcom.nextr);
	}

protected void a400ProcessTaxd()
	{
		/*A400-START*/
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(reverserec.effdate1);
		taxdrevIO.setFunction(varcom.begn);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			a450DeleteTaxdrev();
		}
		
		/*A400-EXIT*/
	}

protected void a450DeleteTaxdrev()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a450Start();
				case a450Next: 
					a450Next();
				case a450Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a450Start()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			databaseError9500();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())
		|| isNE(reverserec.effdate1, taxdrevIO.getEffdate())
		|| isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			taxdrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a450Exit);
		}
		if (isEQ(taxdrevIO.getTranno(), reverserec.tranno)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.a450Next);
		}
		taxdrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			databaseError9500();
		}
	}

protected void a450Next()
	{
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void genericProcessing6000()
	{
		/*START*/
		/* We will read through all the Coverage & riders attached to the*/
		/*  contract that we are processing, and for each one we will*/
		/*  do any required generic processing by calling subroutine(s)*/
		/* specified on table T5671*/
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs6100();
		}
		
		/*EXIT*/
	}

protected void processCovrs6100()
	{
		start6100();
	}

protected void start6100()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* check whether Life/Coverage/Rider have changed - if so, we*/
		/*  need to call any generic Subroutines that are on T5671*/
		if (isNE(covrenqIO.getLife(), wsaaLife)
		|| isNE(covrenqIO.getCoverage(), wsaaCoverage)
		|| isNE(covrenqIO.getRider(), wsaaRider)
		|| isNE(covrenqIO.getPlanSuffix(), wsaaPlanSuffix)) {
			genericSubr6200();
			wsaaLife.set(covrenqIO.getLife());
			wsaaCoverage.set(covrenqIO.getCoverage());
			wsaaRider.set(covrenqIO.getRider());
			wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
			/* If this Coverage Component was generated when the Single        */
			/* Premium Top-Up was introduced, reverse this action.             */
			if (isEQ(covrenqIO.getTranno(), reverserec.tranno)) {
				reverseCoverage6400();
			}
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void genericSubr6200()
	{
		start6200();
	}

protected void start6200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError9500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(ZERO);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs6300();
		}
	}

protected void callTrevsubs6300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError9000();
			}
		}
		/*EXIT*/
	}

protected void reverseCoverage6400()
	{
		start6401();
	}

protected void start6401()
	{
		/* Hold & delete this component which was created when the         */
		/* Single Premium Top-up was introduced.                           */
		covrenqIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError9500();
		}
		covrenqIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError9500();
		}
		/* Reinsate the previous Coverage Component using logical          */
		/* view COVRRNL - COVRENQ excludes Coverages with Valid flag 2.    */
		covrrnlIO.setChdrcoy(reverserec.company);
		covrrnlIO.setChdrnum(reverserec.chdrnum);
		covrrnlIO.setLife(wsaaLife);
		covrrnlIO.setCoverage(wsaaCoverage);
		covrrnlIO.setRider(wsaaRider);
		covrrnlIO.setPlanSuffix(wsaaPlanSuffix);
		covrrnlIO.setFormat(formatsInner.covrrnlrec);
		covrrnlIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrrnlIO);
		if (isNE(covrrnlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrnlIO.getParams());
			syserrrec.statuz.set(covrrnlIO.getStatuz());
			databaseError9500();
		}
		covrrnlIO.setCurrto(varcom.vrcmMaxDate);
		covrrnlIO.setValidflag("1");
		covrrnlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrrnlIO);
		if (isNE(covrrnlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrnlIO.getParams());
			syserrrec.statuz.set(covrrnlIO.getStatuz());
			databaseError9500();
		}
	}

protected void processZptn7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc7010();
				case call7020: 
					call7020();
					writ7030();
					nextr7080();
				case exit7090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc7010()
	{
		zptnrevIO.setDataArea(SPACES);
		zptnrevIO.setChdrcoy(reverserec.company);
		zptnrevIO.setChdrnum(reverserec.chdrnum);
		zptnrevIO.setTranno(reverserec.tranno);
		zptnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zptnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zptnrevIO.setFormat(formatsInner.zptnrevrec);
	}

protected void call7020()
	{
		SmartFileCode.execute(appVars, zptnrevIO);
		if (isNE(zptnrevIO.getStatuz(), varcom.oK)
		&& isNE(zptnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zptnrevIO.getStatuz());
			syserrrec.params.set(zptnrevIO.getParams());
			databaseError9500();
		}
		if (isEQ(zptnrevIO.getStatuz(), varcom.endp)
		|| isNE(zptnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zptnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zptnrevIO.getTranno(), reverserec.tranno)) {
			zptnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit7090);
		}
	}

protected void writ7030()
	{
		zptnIO.setDataArea(SPACES);
		zptnIO.setChdrcoy(zptnrevIO.getChdrcoy());
		zptnIO.setChdrnum(zptnrevIO.getChdrnum());
		zptnIO.setLife(zptnrevIO.getLife());
		zptnIO.setCoverage(zptnrevIO.getCoverage());
		zptnIO.setRider(zptnrevIO.getRider());
		zptnIO.setTranno(reverserec.newTranno);
		setPrecision(zptnIO.getOrigamt(), 2);
		zptnIO.setOrigamt(mult(zptnrevIO.getOrigamt(), -1));
		zptnIO.setTransCode(reverserec.batctrcde);
		zptnIO.setEffdate(zptnrevIO.getEffdate());
		zptnIO.setBillcd(zptnrevIO.getBillcd());
		zptnIO.setInstfrom(zptnrevIO.getInstfrom());
		zptnIO.setInstto(zptnrevIO.getInstto());
		zptnIO.setTrandate(datcon1rec.intDate);
		zptnIO.setZprflg(zptnrevIO.getZprflg());
		zptnIO.setFormat(formatsInner.zptnrec);
		zptnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zptnIO.getStatuz());
			syserrrec.params.set(zptnIO.getParams());
			databaseError9500();
		}
	}

protected void nextr7080()
	{
		zptnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call7020);
	}

protected void processZctn8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc8010();
				case call8020: 
					call8020();
					writ8030();
					nextr8080();
				case exit8090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc8010()
	{
		zctnrevIO.setDataArea(SPACES);
		zctnrevIO.setChdrcoy(reverserec.company);
		zctnrevIO.setChdrnum(reverserec.chdrnum);
		zctnrevIO.setTranno(reverserec.tranno);
		zctnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zctnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zctnrevIO.setFormat(formatsInner.zctnrevrec);
	}

protected void call8020()
	{
		SmartFileCode.execute(appVars, zctnrevIO);
		if (isNE(zctnrevIO.getStatuz(), varcom.oK)
		&& isNE(zctnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zctnrevIO.getStatuz());
			syserrrec.params.set(zctnrevIO.getParams());
			databaseError9500();
		}
		if (isEQ(zctnrevIO.getStatuz(), varcom.endp)
		|| isNE(zctnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zctnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zctnrevIO.getTranno(), reverserec.tranno)) {
			zctnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit8090);
		}
	}

protected void writ8030()
	{
		zctnIO.setDataArea(SPACES);
		zctnIO.setAgntcoy(zctnrevIO.getAgntcoy());
		zctnIO.setAgntnum(zctnrevIO.getAgntnum());
		zctnIO.setChdrcoy(zctnrevIO.getChdrcoy());
		zctnIO.setChdrnum(zctnrevIO.getChdrnum());
		zctnIO.setLife(zctnrevIO.getLife());
		zctnIO.setCoverage(zctnrevIO.getCoverage());
		zctnIO.setRider(zctnrevIO.getRider());
		zctnIO.setTranno(reverserec.newTranno);
		setPrecision(zctnIO.getCommAmt(), 2);
		zctnIO.setCommAmt(mult(zctnrevIO.getCommAmt(), -1));
		setPrecision(zctnIO.getPremium(), 2);
		zctnIO.setPremium(mult(zctnrevIO.getPremium(), -1));
		zctnIO.setSplitBcomm(zctnrevIO.getSplitBcomm());
		zctnIO.setTransCode(reverserec.batctrcde);
		zctnIO.setEffdate(zctnrevIO.getEffdate());
		zctnIO.setTrandate(datcon1rec.intDate);
		zctnIO.setZprflg(zctnrevIO.getZprflg());
		zctnIO.setFormat(formatsInner.zctnrec);
		zctnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zctnIO);
		if (isNE(zctnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zctnIO.getStatuz());
			syserrrec.params.set(zctnIO.getParams());
			databaseError9500();
		}
	}

protected void nextr8080()
	{
		zctnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call8020);
	}

protected void b100ReverseZrst()
	{
		b100Start();
	}

protected void b100Start()
	{
		zrstnudIO.setParams(SPACES);
		zrstnudIO.setChdrcoy(covrIO.getChdrcoy());
		zrstnudIO.setChdrnum(covrIO.getChdrnum());
		zrstnudIO.setLife(covrIO.getLife());
		zrstnudIO.setCoverage(covrIO.getCoverage());
		zrstnudIO.setFormat(formatsInner.zrstnudrec);
		zrstnudIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zrstnudIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zrstnudIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE");
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(), varcom.oK)
		&& isNE(zrstnudIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			syserrrec.params.set(zrstnudIO.getParams());
			databaseError9500();
		}
		if (isEQ(zrstnudIO.getStatuz(), varcom.endp)
		|| isNE(zrstnudIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(zrstnudIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(zrstnudIO.getLife(), covrIO.getLife())
		|| isNE(zrstnudIO.getCoverage(), covrIO.getCoverage())) {
			return ;
		}
		while ( !(isEQ(zrstnudIO.getStatuz(), varcom.endp))) {
			b200DeleteZrst();
		}
		
	}

protected void b200DeleteZrst()
	{
		b200Start();
	}

protected void b200Start()
	{
		if (isEQ(zrstnudIO.getChdrcoy(), covrIO.getChdrcoy())
		&& isEQ(zrstnudIO.getChdrnum(), covrIO.getChdrnum())) {
//			MIBT-255
//			zrstnudIO.setFunction(varcom.readh);
//			SmartFileCode.execute(appVars, zrstnudIO);
//			if (isNE(zrstnudIO.getStatuz(), varcom.oK)) {
//				syserrrec.statuz.set(zrstnudIO.getStatuz());
//				syserrrec.params.set(zrstnudIO.getParams());
//				databaseError9500();
//			}
			if (isEQ(zrstnudIO.getTranno(), reverserec.tranno)) {
				zrstnudIO.setFunction(varcom.delet);
			}
			else {
				zrstnudIO.setZramount02(covrIO.getCoverageDebt());
				zrstnudIO.setFunction(varcom.rewrt);
			}
			SmartFileCode.execute(appVars, zrstnudIO);
			if (isNE(zrstnudIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(zrstnudIO.getStatuz());
				syserrrec.params.set(zrstnudIO.getParams());
				databaseError9500();
			}
		}
		zrstnudIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, zrstnudIO);
		if (isNE(zrstnudIO.getStatuz(), varcom.oK)
		&& isNE(zrstnudIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zrstnudIO.getStatuz());
			syserrrec.params.set(zrstnudIO.getParams());
			databaseError9500();
		}
		if (isEQ(zrstnudIO.getStatuz(), varcom.endp)
		|| isNE(zrstnudIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(zrstnudIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(zrstnudIO.getLife(), covrIO.getLife())
		|| isNE(zrstnudIO.getCoverage(), covrIO.getCoverage())) {
			zrstnudIO.setStatuz(varcom.endp);
		}
	}

protected void systemError9000()
	{
		start9000();
		exit9490();
	}

protected void start9000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError9500()
	{
		start9500();
		exit9990();
	}

protected void start9500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covrrnlrec = new FixedLengthStringData(10).init("COVRRNLREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
	private FixedLengthStringData lextchgrec = new FixedLengthStringData(10).init("LEXTCHGREC");
	private FixedLengthStringData zrstnudrec = new FixedLengthStringData(10).init("ZRSTNUDREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData zptnrevrec = new FixedLengthStringData(10).init("ZPTNREVREC");
	private FixedLengthStringData zctnrevrec = new FixedLengthStringData(10).init("ZCTNREVREC");
}
}
