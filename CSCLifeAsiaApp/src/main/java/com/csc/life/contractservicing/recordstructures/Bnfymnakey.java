package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:08:39
 * Description:
 * Copybook name: BNFYMNAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Bnfymnakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData bnfymnaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData bnfymnaKey = new FixedLengthStringData(256).isAPartOf(bnfymnaFileKey, 0, REDEFINE);
  	public FixedLengthStringData bnfymnaChdrcoy = new FixedLengthStringData(1).isAPartOf(bnfymnaKey, 0);
  	public FixedLengthStringData bnfymnaChdrnum = new FixedLengthStringData(8).isAPartOf(bnfymnaKey, 1);
  	public FixedLengthStringData bnfymnaBnytype = new FixedLengthStringData(2).isAPartOf(bnfymnaKey, 9);
  	public FixedLengthStringData bnfymnaBnyclt = new FixedLengthStringData(8).isAPartOf(bnfymnaKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(237).isAPartOf(bnfymnaKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(bnfymnaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnfymnaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}