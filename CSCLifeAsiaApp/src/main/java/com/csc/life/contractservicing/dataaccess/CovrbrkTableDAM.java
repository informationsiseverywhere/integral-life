package com.csc.life.contractservicing.dataaccess;

import com.csc.life.newbusiness.dataaccess.CovrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovrbrkTableDAM.java
 * Date: Sun, 30 Aug 2009 03:34:44
 * Class transformed from COVRBRK.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovrbrkTableDAM extends CovrpfTableDAM {

	public CovrbrkTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("COVRBRK");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", PLNSFX"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "STATCODE, " +
		            "PSTATCODE, " +
		            "STATREASN, " +
		            "CRRCD, " +
		            "ANBCCD, " +
		            "SEX, " +
		            "REPTCD01, " +
		            "REPTCD02, " +
		            "REPTCD03, " +
		            "REPTCD04, " +
		            "REPTCD05, " +
		            "REPTCD06, " +
		            "CRINST01, " +
		            "CRINST02, " +
		            "CRINST03, " +
		            "CRINST04, " +
		            "CRINST05, " +
		            "PRMCUR, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "STFUND, " +
		            "STSECT, " +
		            "STSSECT, " +
		            "CRTABLE, " +
		            "RCESDTE, " +
		            "PCESDTE, " +
		            "BCESDTE, " +
		            "NXTDTE, " +
		            "RSUNIN, " +
		            "RUNDTE, " +
		            "CHGOPT, " +
		            "FUNDSP, " +
		            "CBCVIN, " +
		            "CBRVPR, " +
		            "CBUNST, " +
		            "CPIDTE, " +
		            "ICANDT, " +
		            "EXALDT, " +
		            "IINCDT, " +
		            "RCESAGE, " +
		            "PCESAGE, " +
		            "BCESAGE, " +
		            "RCESTRM, " +
		            "PCESTRM, " +
		            "BCESTRM, " +
		            "SUMINS, " +
		            "SICURR, " +
		            "VARSI, " +
		            "MORTCLS, " +
		            "LIENCD, " +
		            "RATCLS, " +
		            "INDXIN, " +
		            "BNUSIN, " +
		            "DPCD, " +
		            "DPAMT, " +
		            "DPIND, " +
		            "TMBEN, " +
		            "SINGP, " +
		            "INSTPREM, " +
		            "EMV01, " +
		            "EMV02, " +
		            "EMVDTE01, " +
		            "EMVDTE02, " +
		            "EMVINT01, " +
		            "EMVINT02, " +
		            "CAMPAIGN, " +
		            "STSMIN, " +
		            "RTRNYRS, " +
		            "PCAMTH, " +
		            "PCADAY, " +
		            "PCTMTH, " +
		            "PCTDAY, " +
		            "RCAMTH, " +
		            "RCADAY, " +
		            "RCTMTH, " +
		            "RCTDAY, " +
		            "JLLSID, " +
		            "RRTDAT, " +
		            "RRTFRM, " +
		            "BBLDAT, " +
		            "CRDEBT, " +
		            "CBANPR, " +
		            "PAYRSEQNO, " +
		            "BAPPMETH, " +
		            "ZBINSTPREM, " +
		            "ZLINSTPREM, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "PLNSFX ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "PLNSFX DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               coverage,
                               rider,
                               planSuffix,
                               validflag,
                               tranno,
                               currfrom,
                               currto,
                               statcode,
                               pstatcode,
                               statreasn,
                               crrcd,
                               anbAtCcd,
                               sex,
                               reptcd01,
                               reptcd02,
                               reptcd03,
                               reptcd04,
                               reptcd05,
                               reptcd06,
                               crInstamt01,
                               crInstamt02,
                               crInstamt03,
                               crInstamt04,
                               crInstamt05,
                               premCurrency,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               statFund,
                               statSect,
                               statSubsect,
                               crtable,
                               riskCessDate,
                               premCessDate,
                               benCessDate,
                               nextActDate,
                               reserveUnitsInd,
                               reserveUnitsDate,
                               chargeOptionsInd,
                               fundSplitPlan,
                               convertInitialUnits,
                               reviewProcessing,
                               unitStatementDate,
                               cpiDate,
                               initUnitCancDate,
                               extraAllocDate,
                               initUnitIncrsDate,
                               riskCessAge,
                               premCessAge,
                               benCessAge,
                               riskCessTerm,
                               premCessTerm,
                               benCessTerm,
                               sumins,
                               sicurr,
                               varSumInsured,
                               mortcls,
                               liencd,
                               ratingClass,
                               indexationInd,
                               bonusInd,
                               deferPerdCode,
                               deferPerdAmt,
                               deferPerdInd,
                               totMthlyBenefit,
                               singp,
                               instprem,
                               estMatValue01,
                               estMatValue02,
                               estMatDate01,
                               estMatDate02,
                               estMatInt01,
                               estMatInt02,
                               campaign,
                               statSumins,
                               rtrnyrs,
                               premCessAgeMth,
                               premCessAgeDay,
                               premCessTermMth,
                               premCessTermDay,
                               riskCessAgeMth,
                               riskCessAgeDay,
                               riskCessTermMth,
                               riskCessTermDay,
                               jlLsInd,
                               rerateDate,
                               rerateFromDate,
                               benBillDate,
                               coverageDebt,
                               annivProcDate,
                               payrseqno,
                               bappmeth,
                               zbinstprem,
                               zlinstprem,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(238);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getPlanSuffix().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller50.setInternal(coverage.toInternal());
	nonKeyFiller60.setInternal(rider.toInternal());
	nonKeyFiller70.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(464);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getJlife().toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getValidflag().toInternal()
					+ getTranno().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getStatcode().toInternal()
					+ getPstatcode().toInternal()
					+ getStatreasn().toInternal()
					+ getCrrcd().toInternal()
					+ getAnbAtCcd().toInternal()
					+ getSex().toInternal()
					+ getReptcd01().toInternal()
					+ getReptcd02().toInternal()
					+ getReptcd03().toInternal()
					+ getReptcd04().toInternal()
					+ getReptcd05().toInternal()
					+ getReptcd06().toInternal()
					+ getCrInstamt01().toInternal()
					+ getCrInstamt02().toInternal()
					+ getCrInstamt03().toInternal()
					+ getCrInstamt04().toInternal()
					+ getCrInstamt05().toInternal()
					+ getPremCurrency().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getStatSubsect().toInternal()
					+ getCrtable().toInternal()
					+ getRiskCessDate().toInternal()
					+ getPremCessDate().toInternal()
					+ getBenCessDate().toInternal()
					+ getNextActDate().toInternal()
					+ getReserveUnitsInd().toInternal()
					+ getReserveUnitsDate().toInternal()
					+ getChargeOptionsInd().toInternal()
					+ getFundSplitPlan().toInternal()
					+ getConvertInitialUnits().toInternal()
					+ getReviewProcessing().toInternal()
					+ getUnitStatementDate().toInternal()
					+ getCpiDate().toInternal()
					+ getInitUnitCancDate().toInternal()
					+ getExtraAllocDate().toInternal()
					+ getInitUnitIncrsDate().toInternal()
					+ getRiskCessAge().toInternal()
					+ getPremCessAge().toInternal()
					+ getBenCessAge().toInternal()
					+ getRiskCessTerm().toInternal()
					+ getPremCessTerm().toInternal()
					+ getBenCessTerm().toInternal()
					+ getSumins().toInternal()
					+ getSicurr().toInternal()
					+ getVarSumInsured().toInternal()
					+ getMortcls().toInternal()
					+ getLiencd().toInternal()
					+ getRatingClass().toInternal()
					+ getIndexationInd().toInternal()
					+ getBonusInd().toInternal()
					+ getDeferPerdCode().toInternal()
					+ getDeferPerdAmt().toInternal()
					+ getDeferPerdInd().toInternal()
					+ getTotMthlyBenefit().toInternal()
					+ getSingp().toInternal()
					+ getInstprem().toInternal()
					+ getEstMatValue01().toInternal()
					+ getEstMatValue02().toInternal()
					+ getEstMatDate01().toInternal()
					+ getEstMatDate02().toInternal()
					+ getEstMatInt01().toInternal()
					+ getEstMatInt02().toInternal()
					+ getCampaign().toInternal()
					+ getStatSumins().toInternal()
					+ getRtrnyrs().toInternal()
					+ getPremCessAgeMth().toInternal()
					+ getPremCessAgeDay().toInternal()
					+ getPremCessTermMth().toInternal()
					+ getPremCessTermDay().toInternal()
					+ getRiskCessAgeMth().toInternal()
					+ getRiskCessAgeDay().toInternal()
					+ getRiskCessTermMth().toInternal()
					+ getRiskCessTermDay().toInternal()
					+ getJlLsInd().toInternal()
					+ getRerateDate().toInternal()
					+ getRerateFromDate().toInternal()
					+ getBenBillDate().toInternal()
					+ getCoverageDebt().toInternal()
					+ getAnnivProcDate().toInternal()
					+ getPayrseqno().toInternal()
					+ getBappmeth().toInternal()
					+ getZbinstprem().toInternal()
					+ getZlinstprem().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, statreasn);
			what = ExternalData.chop(what, crrcd);
			what = ExternalData.chop(what, anbAtCcd);
			what = ExternalData.chop(what, sex);
			what = ExternalData.chop(what, reptcd01);
			what = ExternalData.chop(what, reptcd02);
			what = ExternalData.chop(what, reptcd03);
			what = ExternalData.chop(what, reptcd04);
			what = ExternalData.chop(what, reptcd05);
			what = ExternalData.chop(what, reptcd06);
			what = ExternalData.chop(what, crInstamt01);
			what = ExternalData.chop(what, crInstamt02);
			what = ExternalData.chop(what, crInstamt03);
			what = ExternalData.chop(what, crInstamt04);
			what = ExternalData.chop(what, crInstamt05);
			what = ExternalData.chop(what, premCurrency);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, riskCessDate);
			what = ExternalData.chop(what, premCessDate);
			what = ExternalData.chop(what, benCessDate);
			what = ExternalData.chop(what, nextActDate);
			what = ExternalData.chop(what, reserveUnitsInd);
			what = ExternalData.chop(what, reserveUnitsDate);
			what = ExternalData.chop(what, chargeOptionsInd);
			what = ExternalData.chop(what, fundSplitPlan);
			what = ExternalData.chop(what, convertInitialUnits);
			what = ExternalData.chop(what, reviewProcessing);
			what = ExternalData.chop(what, unitStatementDate);
			what = ExternalData.chop(what, cpiDate);
			what = ExternalData.chop(what, initUnitCancDate);
			what = ExternalData.chop(what, extraAllocDate);
			what = ExternalData.chop(what, initUnitIncrsDate);
			what = ExternalData.chop(what, riskCessAge);
			what = ExternalData.chop(what, premCessAge);
			what = ExternalData.chop(what, benCessAge);
			what = ExternalData.chop(what, riskCessTerm);
			what = ExternalData.chop(what, premCessTerm);
			what = ExternalData.chop(what, benCessTerm);
			what = ExternalData.chop(what, sumins);
			what = ExternalData.chop(what, sicurr);
			what = ExternalData.chop(what, varSumInsured);
			what = ExternalData.chop(what, mortcls);
			what = ExternalData.chop(what, liencd);
			what = ExternalData.chop(what, ratingClass);
			what = ExternalData.chop(what, indexationInd);
			what = ExternalData.chop(what, bonusInd);
			what = ExternalData.chop(what, deferPerdCode);
			what = ExternalData.chop(what, deferPerdAmt);
			what = ExternalData.chop(what, deferPerdInd);
			what = ExternalData.chop(what, totMthlyBenefit);
			what = ExternalData.chop(what, singp);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, estMatValue01);
			what = ExternalData.chop(what, estMatValue02);
			what = ExternalData.chop(what, estMatDate01);
			what = ExternalData.chop(what, estMatDate02);
			what = ExternalData.chop(what, estMatInt01);
			what = ExternalData.chop(what, estMatInt02);
			what = ExternalData.chop(what, campaign);
			what = ExternalData.chop(what, statSumins);
			what = ExternalData.chop(what, rtrnyrs);
			what = ExternalData.chop(what, premCessAgeMth);
			what = ExternalData.chop(what, premCessAgeDay);
			what = ExternalData.chop(what, premCessTermMth);
			what = ExternalData.chop(what, premCessTermDay);
			what = ExternalData.chop(what, riskCessAgeMth);
			what = ExternalData.chop(what, riskCessAgeDay);
			what = ExternalData.chop(what, riskCessTermMth);
			what = ExternalData.chop(what, riskCessTermDay);
			what = ExternalData.chop(what, jlLsInd);
			what = ExternalData.chop(what, rerateDate);
			what = ExternalData.chop(what, rerateFromDate);
			what = ExternalData.chop(what, benBillDate);
			what = ExternalData.chop(what, coverageDebt);
			what = ExternalData.chop(what, annivProcDate);
			what = ExternalData.chop(what, payrseqno);
			what = ExternalData.chop(what, bappmeth);
			what = ExternalData.chop(what, zbinstprem);
			what = ExternalData.chop(what, zlinstprem);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public FixedLengthStringData getStatreasn() {
		return statreasn;
	}
	public void setStatreasn(Object what) {
		statreasn.set(what);
	}	
	public PackedDecimalData getCrrcd() {
		return crrcd;
	}
	public void setCrrcd(Object what) {
		setCrrcd(what, false);
	}
	public void setCrrcd(Object what, boolean rounded) {
		if (rounded)
			crrcd.setRounded(what);
		else
			crrcd.set(what);
	}	
	public PackedDecimalData getAnbAtCcd() {
		return anbAtCcd;
	}
	public void setAnbAtCcd(Object what) {
		setAnbAtCcd(what, false);
	}
	public void setAnbAtCcd(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd.setRounded(what);
		else
			anbAtCcd.set(what);
	}	
	public FixedLengthStringData getSex() {
		return sex;
	}
	public void setSex(Object what) {
		sex.set(what);
	}	
	public FixedLengthStringData getReptcd01() {
		return reptcd01;
	}
	public void setReptcd01(Object what) {
		reptcd01.set(what);
	}	
	public FixedLengthStringData getReptcd02() {
		return reptcd02;
	}
	public void setReptcd02(Object what) {
		reptcd02.set(what);
	}	
	public FixedLengthStringData getReptcd03() {
		return reptcd03;
	}
	public void setReptcd03(Object what) {
		reptcd03.set(what);
	}	
	public FixedLengthStringData getReptcd04() {
		return reptcd04;
	}
	public void setReptcd04(Object what) {
		reptcd04.set(what);
	}	
	public FixedLengthStringData getReptcd05() {
		return reptcd05;
	}
	public void setReptcd05(Object what) {
		reptcd05.set(what);
	}	
	public FixedLengthStringData getReptcd06() {
		return reptcd06;
	}
	public void setReptcd06(Object what) {
		reptcd06.set(what);
	}	
	public PackedDecimalData getCrInstamt01() {
		return crInstamt01;
	}
	public void setCrInstamt01(Object what) {
		setCrInstamt01(what, false);
	}
	public void setCrInstamt01(Object what, boolean rounded) {
		if (rounded)
			crInstamt01.setRounded(what);
		else
			crInstamt01.set(what);
	}	
	public PackedDecimalData getCrInstamt02() {
		return crInstamt02;
	}
	public void setCrInstamt02(Object what) {
		setCrInstamt02(what, false);
	}
	public void setCrInstamt02(Object what, boolean rounded) {
		if (rounded)
			crInstamt02.setRounded(what);
		else
			crInstamt02.set(what);
	}	
	public PackedDecimalData getCrInstamt03() {
		return crInstamt03;
	}
	public void setCrInstamt03(Object what) {
		setCrInstamt03(what, false);
	}
	public void setCrInstamt03(Object what, boolean rounded) {
		if (rounded)
			crInstamt03.setRounded(what);
		else
			crInstamt03.set(what);
	}	
	public PackedDecimalData getCrInstamt04() {
		return crInstamt04;
	}
	public void setCrInstamt04(Object what) {
		setCrInstamt04(what, false);
	}
	public void setCrInstamt04(Object what, boolean rounded) {
		if (rounded)
			crInstamt04.setRounded(what);
		else
			crInstamt04.set(what);
	}	
	public PackedDecimalData getCrInstamt05() {
		return crInstamt05;
	}
	public void setCrInstamt05(Object what) {
		setCrInstamt05(what, false);
	}
	public void setCrInstamt05(Object what, boolean rounded) {
		if (rounded)
			crInstamt05.setRounded(what);
		else
			crInstamt05.set(what);
	}	
	public FixedLengthStringData getPremCurrency() {
		return premCurrency;
	}
	public void setPremCurrency(Object what) {
		premCurrency.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}	
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}	
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getRiskCessDate() {
		return riskCessDate;
	}
	public void setRiskCessDate(Object what) {
		setRiskCessDate(what, false);
	}
	public void setRiskCessDate(Object what, boolean rounded) {
		if (rounded)
			riskCessDate.setRounded(what);
		else
			riskCessDate.set(what);
	}	
	public PackedDecimalData getPremCessDate() {
		return premCessDate;
	}
	public void setPremCessDate(Object what) {
		setPremCessDate(what, false);
	}
	public void setPremCessDate(Object what, boolean rounded) {
		if (rounded)
			premCessDate.setRounded(what);
		else
			premCessDate.set(what);
	}	
	public PackedDecimalData getBenCessDate() {
		return benCessDate;
	}
	public void setBenCessDate(Object what) {
		setBenCessDate(what, false);
	}
	public void setBenCessDate(Object what, boolean rounded) {
		if (rounded)
			benCessDate.setRounded(what);
		else
			benCessDate.set(what);
	}	
	public PackedDecimalData getNextActDate() {
		return nextActDate;
	}
	public void setNextActDate(Object what) {
		setNextActDate(what, false);
	}
	public void setNextActDate(Object what, boolean rounded) {
		if (rounded)
			nextActDate.setRounded(what);
		else
			nextActDate.set(what);
	}	
	public FixedLengthStringData getReserveUnitsInd() {
		return reserveUnitsInd;
	}
	public void setReserveUnitsInd(Object what) {
		reserveUnitsInd.set(what);
	}	
	public PackedDecimalData getReserveUnitsDate() {
		return reserveUnitsDate;
	}
	public void setReserveUnitsDate(Object what) {
		setReserveUnitsDate(what, false);
	}
	public void setReserveUnitsDate(Object what, boolean rounded) {
		if (rounded)
			reserveUnitsDate.setRounded(what);
		else
			reserveUnitsDate.set(what);
	}	
	public FixedLengthStringData getChargeOptionsInd() {
		return chargeOptionsInd;
	}
	public void setChargeOptionsInd(Object what) {
		chargeOptionsInd.set(what);
	}	
	public FixedLengthStringData getFundSplitPlan() {
		return fundSplitPlan;
	}
	public void setFundSplitPlan(Object what) {
		fundSplitPlan.set(what);
	}	
	public PackedDecimalData getConvertInitialUnits() {
		return convertInitialUnits;
	}
	public void setConvertInitialUnits(Object what) {
		setConvertInitialUnits(what, false);
	}
	public void setConvertInitialUnits(Object what, boolean rounded) {
		if (rounded)
			convertInitialUnits.setRounded(what);
		else
			convertInitialUnits.set(what);
	}	
	public PackedDecimalData getReviewProcessing() {
		return reviewProcessing;
	}
	public void setReviewProcessing(Object what) {
		setReviewProcessing(what, false);
	}
	public void setReviewProcessing(Object what, boolean rounded) {
		if (rounded)
			reviewProcessing.setRounded(what);
		else
			reviewProcessing.set(what);
	}	
	public PackedDecimalData getUnitStatementDate() {
		return unitStatementDate;
	}
	public void setUnitStatementDate(Object what) {
		setUnitStatementDate(what, false);
	}
	public void setUnitStatementDate(Object what, boolean rounded) {
		if (rounded)
			unitStatementDate.setRounded(what);
		else
			unitStatementDate.set(what);
	}	
	public PackedDecimalData getCpiDate() {
		return cpiDate;
	}
	public void setCpiDate(Object what) {
		setCpiDate(what, false);
	}
	public void setCpiDate(Object what, boolean rounded) {
		if (rounded)
			cpiDate.setRounded(what);
		else
			cpiDate.set(what);
	}	
	public PackedDecimalData getInitUnitCancDate() {
		return initUnitCancDate;
	}
	public void setInitUnitCancDate(Object what) {
		setInitUnitCancDate(what, false);
	}
	public void setInitUnitCancDate(Object what, boolean rounded) {
		if (rounded)
			initUnitCancDate.setRounded(what);
		else
			initUnitCancDate.set(what);
	}	
	public PackedDecimalData getExtraAllocDate() {
		return extraAllocDate;
	}
	public void setExtraAllocDate(Object what) {
		setExtraAllocDate(what, false);
	}
	public void setExtraAllocDate(Object what, boolean rounded) {
		if (rounded)
			extraAllocDate.setRounded(what);
		else
			extraAllocDate.set(what);
	}	
	public PackedDecimalData getInitUnitIncrsDate() {
		return initUnitIncrsDate;
	}
	public void setInitUnitIncrsDate(Object what) {
		setInitUnitIncrsDate(what, false);
	}
	public void setInitUnitIncrsDate(Object what, boolean rounded) {
		if (rounded)
			initUnitIncrsDate.setRounded(what);
		else
			initUnitIncrsDate.set(what);
	}	
	public PackedDecimalData getRiskCessAge() {
		return riskCessAge;
	}
	public void setRiskCessAge(Object what) {
		setRiskCessAge(what, false);
	}
	public void setRiskCessAge(Object what, boolean rounded) {
		if (rounded)
			riskCessAge.setRounded(what);
		else
			riskCessAge.set(what);
	}	
	public PackedDecimalData getPremCessAge() {
		return premCessAge;
	}
	public void setPremCessAge(Object what) {
		setPremCessAge(what, false);
	}
	public void setPremCessAge(Object what, boolean rounded) {
		if (rounded)
			premCessAge.setRounded(what);
		else
			premCessAge.set(what);
	}	
	public PackedDecimalData getBenCessAge() {
		return benCessAge;
	}
	public void setBenCessAge(Object what) {
		setBenCessAge(what, false);
	}
	public void setBenCessAge(Object what, boolean rounded) {
		if (rounded)
			benCessAge.setRounded(what);
		else
			benCessAge.set(what);
	}	
	public PackedDecimalData getRiskCessTerm() {
		return riskCessTerm;
	}
	public void setRiskCessTerm(Object what) {
		setRiskCessTerm(what, false);
	}
	public void setRiskCessTerm(Object what, boolean rounded) {
		if (rounded)
			riskCessTerm.setRounded(what);
		else
			riskCessTerm.set(what);
	}	
	public PackedDecimalData getPremCessTerm() {
		return premCessTerm;
	}
	public void setPremCessTerm(Object what) {
		setPremCessTerm(what, false);
	}
	public void setPremCessTerm(Object what, boolean rounded) {
		if (rounded)
			premCessTerm.setRounded(what);
		else
			premCessTerm.set(what);
	}	
	public PackedDecimalData getBenCessTerm() {
		return benCessTerm;
	}
	public void setBenCessTerm(Object what) {
		setBenCessTerm(what, false);
	}
	public void setBenCessTerm(Object what, boolean rounded) {
		if (rounded)
			benCessTerm.setRounded(what);
		else
			benCessTerm.set(what);
	}	
	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	
	public FixedLengthStringData getSicurr() {
		return sicurr;
	}
	public void setSicurr(Object what) {
		sicurr.set(what);
	}	
	public PackedDecimalData getVarSumInsured() {
		return varSumInsured;
	}
	public void setVarSumInsured(Object what) {
		setVarSumInsured(what, false);
	}
	public void setVarSumInsured(Object what, boolean rounded) {
		if (rounded)
			varSumInsured.setRounded(what);
		else
			varSumInsured.set(what);
	}	
	public FixedLengthStringData getMortcls() {
		return mortcls;
	}
	public void setMortcls(Object what) {
		mortcls.set(what);
	}	
	public FixedLengthStringData getLiencd() {
		return liencd;
	}
	public void setLiencd(Object what) {
		liencd.set(what);
	}	
	public FixedLengthStringData getRatingClass() {
		return ratingClass;
	}
	public void setRatingClass(Object what) {
		ratingClass.set(what);
	}	
	public FixedLengthStringData getIndexationInd() {
		return indexationInd;
	}
	public void setIndexationInd(Object what) {
		indexationInd.set(what);
	}	
	public FixedLengthStringData getBonusInd() {
		return bonusInd;
	}
	public void setBonusInd(Object what) {
		bonusInd.set(what);
	}	
	public FixedLengthStringData getDeferPerdCode() {
		return deferPerdCode;
	}
	public void setDeferPerdCode(Object what) {
		deferPerdCode.set(what);
	}	
	public PackedDecimalData getDeferPerdAmt() {
		return deferPerdAmt;
	}
	public void setDeferPerdAmt(Object what) {
		setDeferPerdAmt(what, false);
	}
	public void setDeferPerdAmt(Object what, boolean rounded) {
		if (rounded)
			deferPerdAmt.setRounded(what);
		else
			deferPerdAmt.set(what);
	}	
	public FixedLengthStringData getDeferPerdInd() {
		return deferPerdInd;
	}
	public void setDeferPerdInd(Object what) {
		deferPerdInd.set(what);
	}	
	public PackedDecimalData getTotMthlyBenefit() {
		return totMthlyBenefit;
	}
	public void setTotMthlyBenefit(Object what) {
		setTotMthlyBenefit(what, false);
	}
	public void setTotMthlyBenefit(Object what, boolean rounded) {
		if (rounded)
			totMthlyBenefit.setRounded(what);
		else
			totMthlyBenefit.set(what);
	}	
	public PackedDecimalData getSingp() {
		return singp;
	}
	public void setSingp(Object what) {
		setSingp(what, false);
	}
	public void setSingp(Object what, boolean rounded) {
		if (rounded)
			singp.setRounded(what);
		else
			singp.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public PackedDecimalData getEstMatValue01() {
		return estMatValue01;
	}
	public void setEstMatValue01(Object what) {
		setEstMatValue01(what, false);
	}
	public void setEstMatValue01(Object what, boolean rounded) {
		if (rounded)
			estMatValue01.setRounded(what);
		else
			estMatValue01.set(what);
	}	
	public PackedDecimalData getEstMatValue02() {
		return estMatValue02;
	}
	public void setEstMatValue02(Object what) {
		setEstMatValue02(what, false);
	}
	public void setEstMatValue02(Object what, boolean rounded) {
		if (rounded)
			estMatValue02.setRounded(what);
		else
			estMatValue02.set(what);
	}	
	public PackedDecimalData getEstMatDate01() {
		return estMatDate01;
	}
	public void setEstMatDate01(Object what) {
		setEstMatDate01(what, false);
	}
	public void setEstMatDate01(Object what, boolean rounded) {
		if (rounded)
			estMatDate01.setRounded(what);
		else
			estMatDate01.set(what);
	}	
	public PackedDecimalData getEstMatDate02() {
		return estMatDate02;
	}
	public void setEstMatDate02(Object what) {
		setEstMatDate02(what, false);
	}
	public void setEstMatDate02(Object what, boolean rounded) {
		if (rounded)
			estMatDate02.setRounded(what);
		else
			estMatDate02.set(what);
	}	
	public PackedDecimalData getEstMatInt01() {
		return estMatInt01;
	}
	public void setEstMatInt01(Object what) {
		setEstMatInt01(what, false);
	}
	public void setEstMatInt01(Object what, boolean rounded) {
		if (rounded)
			estMatInt01.setRounded(what);
		else
			estMatInt01.set(what);
	}	
	public PackedDecimalData getEstMatInt02() {
		return estMatInt02;
	}
	public void setEstMatInt02(Object what) {
		setEstMatInt02(what, false);
	}
	public void setEstMatInt02(Object what, boolean rounded) {
		if (rounded)
			estMatInt02.setRounded(what);
		else
			estMatInt02.set(what);
	}	
	public FixedLengthStringData getCampaign() {
		return campaign;
	}
	public void setCampaign(Object what) {
		campaign.set(what);
	}	
	public PackedDecimalData getStatSumins() {
		return statSumins;
	}
	public void setStatSumins(Object what) {
		setStatSumins(what, false);
	}
	public void setStatSumins(Object what, boolean rounded) {
		if (rounded)
			statSumins.setRounded(what);
		else
			statSumins.set(what);
	}	
	public PackedDecimalData getRtrnyrs() {
		return rtrnyrs;
	}
	public void setRtrnyrs(Object what) {
		setRtrnyrs(what, false);
	}
	public void setRtrnyrs(Object what, boolean rounded) {
		if (rounded)
			rtrnyrs.setRounded(what);
		else
			rtrnyrs.set(what);
	}	
	public PackedDecimalData getPremCessAgeMth() {
		return premCessAgeMth;
	}
	public void setPremCessAgeMth(Object what) {
		setPremCessAgeMth(what, false);
	}
	public void setPremCessAgeMth(Object what, boolean rounded) {
		if (rounded)
			premCessAgeMth.setRounded(what);
		else
			premCessAgeMth.set(what);
	}	
	public PackedDecimalData getPremCessAgeDay() {
		return premCessAgeDay;
	}
	public void setPremCessAgeDay(Object what) {
		setPremCessAgeDay(what, false);
	}
	public void setPremCessAgeDay(Object what, boolean rounded) {
		if (rounded)
			premCessAgeDay.setRounded(what);
		else
			premCessAgeDay.set(what);
	}	
	public PackedDecimalData getPremCessTermMth() {
		return premCessTermMth;
	}
	public void setPremCessTermMth(Object what) {
		setPremCessTermMth(what, false);
	}
	public void setPremCessTermMth(Object what, boolean rounded) {
		if (rounded)
			premCessTermMth.setRounded(what);
		else
			premCessTermMth.set(what);
	}	
	public PackedDecimalData getPremCessTermDay() {
		return premCessTermDay;
	}
	public void setPremCessTermDay(Object what) {
		setPremCessTermDay(what, false);
	}
	public void setPremCessTermDay(Object what, boolean rounded) {
		if (rounded)
			premCessTermDay.setRounded(what);
		else
			premCessTermDay.set(what);
	}	
	public PackedDecimalData getRiskCessAgeMth() {
		return riskCessAgeMth;
	}
	public void setRiskCessAgeMth(Object what) {
		setRiskCessAgeMth(what, false);
	}
	public void setRiskCessAgeMth(Object what, boolean rounded) {
		if (rounded)
			riskCessAgeMth.setRounded(what);
		else
			riskCessAgeMth.set(what);
	}	
	public PackedDecimalData getRiskCessAgeDay() {
		return riskCessAgeDay;
	}
	public void setRiskCessAgeDay(Object what) {
		setRiskCessAgeDay(what, false);
	}
	public void setRiskCessAgeDay(Object what, boolean rounded) {
		if (rounded)
			riskCessAgeDay.setRounded(what);
		else
			riskCessAgeDay.set(what);
	}	
	public PackedDecimalData getRiskCessTermMth() {
		return riskCessTermMth;
	}
	public void setRiskCessTermMth(Object what) {
		setRiskCessTermMth(what, false);
	}
	public void setRiskCessTermMth(Object what, boolean rounded) {
		if (rounded)
			riskCessTermMth.setRounded(what);
		else
			riskCessTermMth.set(what);
	}	
	public PackedDecimalData getRiskCessTermDay() {
		return riskCessTermDay;
	}
	public void setRiskCessTermDay(Object what) {
		setRiskCessTermDay(what, false);
	}
	public void setRiskCessTermDay(Object what, boolean rounded) {
		if (rounded)
			riskCessTermDay.setRounded(what);
		else
			riskCessTermDay.set(what);
	}	
	public FixedLengthStringData getJlLsInd() {
		return jlLsInd;
	}
	public void setJlLsInd(Object what) {
		jlLsInd.set(what);
	}	
	public PackedDecimalData getRerateDate() {
		return rerateDate;
	}
	public void setRerateDate(Object what) {
		setRerateDate(what, false);
	}
	public void setRerateDate(Object what, boolean rounded) {
		if (rounded)
			rerateDate.setRounded(what);
		else
			rerateDate.set(what);
	}	
	public PackedDecimalData getRerateFromDate() {
		return rerateFromDate;
	}
	public void setRerateFromDate(Object what) {
		setRerateFromDate(what, false);
	}
	public void setRerateFromDate(Object what, boolean rounded) {
		if (rounded)
			rerateFromDate.setRounded(what);
		else
			rerateFromDate.set(what);
	}	
	public PackedDecimalData getBenBillDate() {
		return benBillDate;
	}
	public void setBenBillDate(Object what) {
		setBenBillDate(what, false);
	}
	public void setBenBillDate(Object what, boolean rounded) {
		if (rounded)
			benBillDate.setRounded(what);
		else
			benBillDate.set(what);
	}	
	public PackedDecimalData getCoverageDebt() {
		return coverageDebt;
	}
	public void setCoverageDebt(Object what) {
		setCoverageDebt(what, false);
	}
	public void setCoverageDebt(Object what, boolean rounded) {
		if (rounded)
			coverageDebt.setRounded(what);
		else
			coverageDebt.set(what);
	}	
	public PackedDecimalData getAnnivProcDate() {
		return annivProcDate;
	}
	public void setAnnivProcDate(Object what) {
		setAnnivProcDate(what, false);
	}
	public void setAnnivProcDate(Object what, boolean rounded) {
		if (rounded)
			annivProcDate.setRounded(what);
		else
			annivProcDate.set(what);
	}	
	public PackedDecimalData getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(Object what) {
		setPayrseqno(what, false);
	}
	public void setPayrseqno(Object what, boolean rounded) {
		if (rounded)
			payrseqno.setRounded(what);
		else
			payrseqno.set(what);
	}	
	public FixedLengthStringData getBappmeth() {
		return bappmeth;
	}
	public void setBappmeth(Object what) {
		bappmeth.set(what);
	}	
	public PackedDecimalData getZbinstprem() {
		return zbinstprem;
	}
	public void setZbinstprem(Object what) {
		setZbinstprem(what, false);
	}
	public void setZbinstprem(Object what, boolean rounded) {
		if (rounded)
			zbinstprem.setRounded(what);
		else
			zbinstprem.set(what);
	}	
	public PackedDecimalData getZlinstprem() {
		return zlinstprem;
	}
	public void setZlinstprem(Object what) {
		setZlinstprem(what, false);
	}
	public void setZlinstprem(Object what, boolean rounded) {
		if (rounded)
			zlinstprem.setRounded(what);
		else
			zlinstprem.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getReptcds() {
		return new FixedLengthStringData(reptcd01.toInternal()
										+ reptcd02.toInternal()
										+ reptcd03.toInternal()
										+ reptcd04.toInternal()
										+ reptcd05.toInternal()
										+ reptcd06.toInternal());
	}
	public void setReptcds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getReptcds().getLength()).init(obj);
	
		what = ExternalData.chop(what, reptcd01);
		what = ExternalData.chop(what, reptcd02);
		what = ExternalData.chop(what, reptcd03);
		what = ExternalData.chop(what, reptcd04);
		what = ExternalData.chop(what, reptcd05);
		what = ExternalData.chop(what, reptcd06);
	}
	public FixedLengthStringData getReptcd(BaseData indx) {
		return getReptcd(indx.toInt());
	}
	public FixedLengthStringData getReptcd(int indx) {

		switch (indx) {
			case 1 : return reptcd01;
			case 2 : return reptcd02;
			case 3 : return reptcd03;
			case 4 : return reptcd04;
			case 5 : return reptcd05;
			case 6 : return reptcd06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setReptcd(BaseData indx, Object what) {
		setReptcd(indx.toInt(), what);
	}
	public void setReptcd(int indx, Object what) {

		switch (indx) {
			case 1 : setReptcd01(what);
					 break;
			case 2 : setReptcd02(what);
					 break;
			case 3 : setReptcd03(what);
					 break;
			case 4 : setReptcd04(what);
					 break;
			case 5 : setReptcd05(what);
					 break;
			case 6 : setReptcd06(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getEmvints() {
		return new FixedLengthStringData(estMatInt01.toInternal()
										+ estMatInt02.toInternal());
	}
	public void setEmvints(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getEmvints().getLength()).init(obj);
	
		what = ExternalData.chop(what, estMatInt01);
		what = ExternalData.chop(what, estMatInt02);
	}
	public PackedDecimalData getEmvint(BaseData indx) {
		return getEmvint(indx.toInt());
	}
	public PackedDecimalData getEmvint(int indx) {

		switch (indx) {
			case 1 : return estMatInt01;
			case 2 : return estMatInt02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setEmvint(BaseData indx, Object what) {
		setEmvint(indx, what, false);
	}
	public void setEmvint(BaseData indx, Object what, boolean rounded) {
		setEmvint(indx.toInt(), what, rounded);
	}
	public void setEmvint(int indx, Object what) {
		setEmvint(indx, what, false);
	}
	public void setEmvint(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setEstMatInt01(what, rounded);
					 break;
			case 2 : setEstMatInt02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getEmvdtes() {
		return new FixedLengthStringData(estMatDate01.toInternal()
										+ estMatDate02.toInternal());
	}
	public void setEmvdtes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getEmvdtes().getLength()).init(obj);
	
		what = ExternalData.chop(what, estMatDate01);
		what = ExternalData.chop(what, estMatDate02);
	}
	public PackedDecimalData getEmvdte(BaseData indx) {
		return getEmvdte(indx.toInt());
	}
	public PackedDecimalData getEmvdte(int indx) {

		switch (indx) {
			case 1 : return estMatDate01;
			case 2 : return estMatDate02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setEmvdte(BaseData indx, Object what) {
		setEmvdte(indx, what, false);
	}
	public void setEmvdte(BaseData indx, Object what, boolean rounded) {
		setEmvdte(indx.toInt(), what, rounded);
	}
	public void setEmvdte(int indx, Object what) {
		setEmvdte(indx, what, false);
	}
	public void setEmvdte(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setEstMatDate01(what, rounded);
					 break;
			case 2 : setEstMatDate02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getEmvs() {
		return new FixedLengthStringData(estMatValue01.toInternal()
										+ estMatValue02.toInternal());
	}
	public void setEmvs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getEmvs().getLength()).init(obj);
	
		what = ExternalData.chop(what, estMatValue01);
		what = ExternalData.chop(what, estMatValue02);
	}
	public PackedDecimalData getEmv(BaseData indx) {
		return getEmv(indx.toInt());
	}
	public PackedDecimalData getEmv(int indx) {

		switch (indx) {
			case 1 : return estMatValue01;
			case 2 : return estMatValue02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setEmv(BaseData indx, Object what) {
		setEmv(indx, what, false);
	}
	public void setEmv(BaseData indx, Object what, boolean rounded) {
		setEmv(indx.toInt(), what, rounded);
	}
	public void setEmv(int indx, Object what) {
		setEmv(indx, what, false);
	}
	public void setEmv(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setEstMatValue01(what, rounded);
					 break;
			case 2 : setEstMatValue02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCrinsts() {
		return new FixedLengthStringData(crInstamt01.toInternal()
										+ crInstamt02.toInternal()
										+ crInstamt03.toInternal()
										+ crInstamt04.toInternal()
										+ crInstamt05.toInternal());
	}
	public void setCrinsts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCrinsts().getLength()).init(obj);
	
		what = ExternalData.chop(what, crInstamt01);
		what = ExternalData.chop(what, crInstamt02);
		what = ExternalData.chop(what, crInstamt03);
		what = ExternalData.chop(what, crInstamt04);
		what = ExternalData.chop(what, crInstamt05);
	}
	public PackedDecimalData getCrinst(BaseData indx) {
		return getCrinst(indx.toInt());
	}
	public PackedDecimalData getCrinst(int indx) {

		switch (indx) {
			case 1 : return crInstamt01;
			case 2 : return crInstamt02;
			case 3 : return crInstamt03;
			case 4 : return crInstamt04;
			case 5 : return crInstamt05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCrinst(BaseData indx, Object what) {
		setCrinst(indx, what, false);
	}
	public void setCrinst(BaseData indx, Object what, boolean rounded) {
		setCrinst(indx.toInt(), what, rounded);
	}
	public void setCrinst(int indx, Object what) {
		setCrinst(indx, what, false);
	}
	public void setCrinst(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setCrInstamt01(what, rounded);
					 break;
			case 2 : setCrInstamt02(what, rounded);
					 break;
			case 3 : setCrInstamt03(what, rounded);
					 break;
			case 4 : setCrInstamt04(what, rounded);
					 break;
			case 5 : setCrInstamt05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		planSuffix.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		jlife.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		validflag.clear();
		tranno.clear();
		currfrom.clear();
		currto.clear();
		statcode.clear();
		pstatcode.clear();
		statreasn.clear();
		crrcd.clear();
		anbAtCcd.clear();
		sex.clear();
		reptcd01.clear();
		reptcd02.clear();
		reptcd03.clear();
		reptcd04.clear();
		reptcd05.clear();
		reptcd06.clear();
		crInstamt01.clear();
		crInstamt02.clear();
		crInstamt03.clear();
		crInstamt04.clear();
		crInstamt05.clear();
		premCurrency.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		statFund.clear();
		statSect.clear();
		statSubsect.clear();
		crtable.clear();
		riskCessDate.clear();
		premCessDate.clear();
		benCessDate.clear();
		nextActDate.clear();
		reserveUnitsInd.clear();
		reserveUnitsDate.clear();
		chargeOptionsInd.clear();
		fundSplitPlan.clear();
		convertInitialUnits.clear();
		reviewProcessing.clear();
		unitStatementDate.clear();
		cpiDate.clear();
		initUnitCancDate.clear();
		extraAllocDate.clear();
		initUnitIncrsDate.clear();
		riskCessAge.clear();
		premCessAge.clear();
		benCessAge.clear();
		riskCessTerm.clear();
		premCessTerm.clear();
		benCessTerm.clear();
		sumins.clear();
		sicurr.clear();
		varSumInsured.clear();
		mortcls.clear();
		liencd.clear();
		ratingClass.clear();
		indexationInd.clear();
		bonusInd.clear();
		deferPerdCode.clear();
		deferPerdAmt.clear();
		deferPerdInd.clear();
		totMthlyBenefit.clear();
		singp.clear();
		instprem.clear();
		estMatValue01.clear();
		estMatValue02.clear();
		estMatDate01.clear();
		estMatDate02.clear();
		estMatInt01.clear();
		estMatInt02.clear();
		campaign.clear();
		statSumins.clear();
		rtrnyrs.clear();
		premCessAgeMth.clear();
		premCessAgeDay.clear();
		premCessTermMth.clear();
		premCessTermDay.clear();
		riskCessAgeMth.clear();
		riskCessAgeDay.clear();
		riskCessTermMth.clear();
		riskCessTermDay.clear();
		jlLsInd.clear();
		rerateDate.clear();
		rerateFromDate.clear();
		benBillDate.clear();
		coverageDebt.clear();
		annivProcDate.clear();
		payrseqno.clear();
		bappmeth.clear();
		zbinstprem.clear();
		zlinstprem.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}