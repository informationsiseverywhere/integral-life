package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.RplnpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rplnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RplnpfDAOImpl extends BaseDAOImpl<Rplnpf> implements RplnpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RplnpfDAOImpl.class);

	@Override
	public void insertRploanpf(Rplnpf rplnpf) {

		StringBuilder sqlInsert = new StringBuilder();

		sqlInsert.append("insert into RPLNPF(CHDRNUM,REPYMOP,REPAYMENTAMT,TOTALPLPRNCPL,TOTALPLINT,TOTALAPLPRNCPL,TOTALAPLINT,REMNGPLPRNCPL,REMNGPLINT,REMNGAPLPRNCPL,REMNGAPLINT,STATUS,APRVDATE,TRANNO,USRPRF,JOBNM,DATIME,MANDREF)");

		sqlInsert.append("values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");

		PreparedStatement psRplnpfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 1;

		try {
			psRplnpfInsert.setString(index++, rplnpf.getChdrnum());
			psRplnpfInsert.setString(index++, rplnpf.getRepymop());
			psRplnpfInsert.setDouble(index++, rplnpf.getRepaymentamt());
			psRplnpfInsert.setDouble(index++, rplnpf.getTotalplprncpl());
			psRplnpfInsert.setDouble(index++, rplnpf.getTotalplint());
			psRplnpfInsert.setDouble(index++, rplnpf.getTotalaplprncpl());
			psRplnpfInsert.setDouble(index++, rplnpf.getTotalaplint());			
			psRplnpfInsert.setDouble(index++, rplnpf.getRemngplprncpl());
			psRplnpfInsert.setDouble(index++, rplnpf.getRemngplint());			
			psRplnpfInsert.setDouble(index++, rplnpf.getRemngaplprncpl());
			psRplnpfInsert.setDouble(index++, rplnpf.getRemngaplint());			
			psRplnpfInsert.setString(index++, rplnpf.getStatus());
			psRplnpfInsert.setInt(index++, rplnpf.getAprvdate());
			psRplnpfInsert.setInt(index++, rplnpf.getTranno());			
			psRplnpfInsert.setString(index++, getUsrprf());
			psRplnpfInsert.setString(index++, getJobnm());			
			psRplnpfInsert.setTimestamp(index++, new Timestamp(System.currentTimeMillis()));
			psRplnpfInsert.setString(index++, rplnpf.getMandref());

			int affectedCount = executeUpdate(psRplnpfInsert);

			LOGGER.debug("insertIntoRplnpf { {} } rows inserted.",affectedCount);//IJTI-1485

		} catch (SQLException e) {
			LOGGER.error("insertIntoRPLNPF()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psRplnpfInsert, null);
		}
	}

	@Override
	public void updateRploanRecord(Rplnpf rplnpf) {

		StringBuilder bnkoutpfUpdate = new StringBuilder(
				"UPDATE RPLNPF SET STATUS=?,APRVDATE=?,TRANNO=?,USRPRF=?,JOBNM=?,DATIME=? WHERE CHDRNUM=? AND STATUS='P' ");

		PreparedStatement rplnpfUpdate = getPrepareStatement(bnkoutpfUpdate.toString());
		try {									
			rplnpfUpdate.setString(1, rplnpf.getStatus());
			rplnpfUpdate.setInt(2, rplnpf.getAprvdate());
			rplnpfUpdate.setInt(3, rplnpf.getTranno());
			rplnpfUpdate.setString(4, getUsrprf());			    
			rplnpfUpdate.setString(5, getJobnm());		
			rplnpfUpdate.setTimestamp(6, getDatime());
			rplnpfUpdate.setString(7, rplnpf.getChdrnum());
			
				
			rplnpfUpdate.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateRploanRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			try {
				rplnpfUpdate.close();
			} catch (SQLException e) {
				LOGGER.error("Exception ocured in finally", e);//IJTI-1485
			}
		}

	}

	
	@Override
	public Rplnpf getRploanRecord(String chdrnum,String status) {
		
		Rplnpf rplnpf = null;
		String sql_select = "SELECT * FROM RPLNPF WHERE CHDRNUM= ? AND STATUS= ? ";

		ResultSet rs = null;
		PreparedStatement psSelect = null;
		try {

			psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
			psSelect.setString(1, chdrnum);
			psSelect.setString(2, status);

			rs = psSelect.executeQuery();
			while (rs.next()) {
				rplnpf = new Rplnpf();				
				rplnpf.setChdrnum(rs.getString("CHDRNUM"));
				rplnpf.setRepymop(rs.getString("REPYMOP"));
				rplnpf.setRepaymentamt(rs.getDouble("REPAYMENTAMT"));
				rplnpf.setTotalplprncpl(rs.getDouble("TOTALPLPRNCPL"));
				rplnpf.setTotalplint(rs.getDouble("TOTALPLINT"));
				rplnpf.setTotalaplprncpl(rs.getDouble("TOTALAPLPRNCPL"));
				rplnpf.setTotalaplint(rs.getDouble("TOTALAPLINT"));
				rplnpf.setRemngplprncpl(rs.getDouble("REMNGPLPRNCPL"));
				rplnpf.setRemngplint(rs.getDouble("REMNGPLINT"));
				rplnpf.setRemngaplprncpl(rs.getDouble("REMNGAPLPRNCPL"));
				rplnpf.setRemngaplint(rs.getDouble("REMNGAPLINT"));
				rplnpf.setStatus(rs.getString("STATUS"));
				rplnpf.setAprvdate(rs.getInt("APRVDATE"));
				rplnpf.setTranno(rs.getInt("TRANNO"));
				rplnpf.setMandref(rs.getString("MANDREF"));

			}

		} catch (SQLException e) {
			LOGGER.error("getRplnpfRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, rs);
		}

		return rplnpf;
	}

	@Override
	public void deleteRploanpf(String chdrnum, int tranno) {
		 StringBuilder sql = new StringBuilder("DELETE FROM RPLNPF WHERE CHDRNUM=? AND TRANNO=?");

	        PreparedStatement ps = getPrepareStatement(sql.toString());
	        ResultSet rs = null;
	        try {
	            ps.setString(1, chdrnum.trim());
	            ps.setInt(2, tranno);
	          
	            ps.execute();
	          
	        } catch (SQLException e) {
	            LOGGER.error("deleteRplnpf()", e);//IJTI-1485
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
		
	}
	public int getLoanRepayRecord(String chdrnum) {
		
		StringBuilder sqlSelectloanRecord = new StringBuilder("SELECT COUNT(*) AS ROW_COUNT FROM RPLNPF WHERE CHDRNUM=? ");
					      
			PreparedStatement psloanRecordSelect = null;
	        ResultSet rs = null;
	        int loanRepayRecord = 0;
	        try {	
	        	 psloanRecordSelect = getConnection().prepareStatement(sqlSelectloanRecord.toString());
	        	 psloanRecordSelect.setString(1, chdrnum.trim());	        	 
	        			        	 
	        	 rs = psloanRecordSelect.executeQuery();

	            while (rs.next()) {
	            	loanRepayRecord = (int) Long.parseLong(rs.getString("ROW_COUNT"));
	            	return loanRepayRecord;
	            }
	        } catch (SQLException e) {
	            LOGGER.error("findTotalCount()", e);//IJTI-1485
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psloanRecordSelect, rs);
	        }
	        return loanRepayRecord;	
	} 	
	@Override
	public void updateRploantrannoRecord(Rplnpf rplnpf) {

		StringBuilder bnkoutpfUpdate = new StringBuilder(
				"UPDATE RPLNPF SET STATUS=?,APRVDATE=? WHERE CHDRNUM=? AND TRANNO=?");

		PreparedStatement rplnpfUpdate = getPrepareStatement(bnkoutpfUpdate.toString());
		try {									
			rplnpfUpdate.setString(1, rplnpf.getStatus());
			rplnpfUpdate.setInt(2, rplnpf.getAprvdate());
			rplnpfUpdate.setString(3, rplnpf.getChdrnum());
			rplnpfUpdate.setInt(4, rplnpf.getTranno());

			rplnpfUpdate.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateRploantrannoRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			try {
				rplnpfUpdate.close();
			} catch (SQLException e) {
				LOGGER.error("Exception ocured in finally", e);//IJTI-1485
			}
		}

	}



}
