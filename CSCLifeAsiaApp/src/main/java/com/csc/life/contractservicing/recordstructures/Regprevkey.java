package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:44
 * Description:
 * Copybook name: REGPREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regprevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regprevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regprevKey = new FixedLengthStringData(64).isAPartOf(regprevFileKey, 0, REDEFINE);
  	public FixedLengthStringData regprevChdrcoy = new FixedLengthStringData(1).isAPartOf(regprevKey, 0);
  	public FixedLengthStringData regprevChdrnum = new FixedLengthStringData(8).isAPartOf(regprevKey, 1);
  	public FixedLengthStringData regprevLife = new FixedLengthStringData(2).isAPartOf(regprevKey, 9);
  	public FixedLengthStringData regprevCoverage = new FixedLengthStringData(2).isAPartOf(regprevKey, 11);
  	public FixedLengthStringData regprevRider = new FixedLengthStringData(2).isAPartOf(regprevKey, 13);
  	public PackedDecimalData regprevRgpynum = new PackedDecimalData(5, 0).isAPartOf(regprevKey, 15);
  	public PackedDecimalData regprevTranno = new PackedDecimalData(5, 0).isAPartOf(regprevKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(regprevKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regprevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regprevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}