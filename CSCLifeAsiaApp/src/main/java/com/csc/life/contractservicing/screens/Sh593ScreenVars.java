package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datadictionarydatatype.PackedDDObj;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SH593
 * @version 1.0 generated on 30/08/09 07:05
 * @author Quipoz
 */
public class Sh593ScreenVars extends SmartVarModel { 

	// CML-072 flag
	public FixedLengthStringData cslnd001Flag = new FixedLengthStringData(1);

	public FixedLengthStringData dataArea = new FixedLengthStringData(612);
	public FixedLengthStringData dataFields = new FixedLengthStringData(276)
			.isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,37);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,67);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,70);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,78);
	public ZonedDecimalData hpleamt = DD.hpleamt.copyToZonedDecimal().isAPartOf(dataFields,86);
	public ZonedDecimalData hpleint = DD.hpleint.copyToZonedDecimal().isAPartOf(dataFields,94);
	public ZonedDecimalData hpletot = DD.hpletot.copyToZonedDecimal().isAPartOf(dataFields,102);
	/*ILIFE-2819-The loan interest rate is to be shown upto 5 decimals starts.*/
	public ZonedDecimalData intanny = DD.intrt.copyToZonedDecimal().isAPartOf(dataFields,111);
	public FixedLengthStringData jlifedesc = DD.jlifedesc.copy().isAPartOf(dataFields,119);
	/*ILIFE-2819-The loan interest rate is to be shown upto 5 decimals starts.*/
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,159);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,167);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,207);
	public ZonedDecimalData numberOfLoans = DD.numloans.copyToZonedDecimal().isAPartOf(dataFields,215);
	public FixedLengthStringData ownerdesc = DD.ownerdesc.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,257);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,267);
	public FixedLengthStringData actionflag =DD.action.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84)
			.isAPartOf(dataArea, 276);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData hpleamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData hpleintErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData hpletotErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData intannyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData numloansErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ownerdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 360);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] hpleamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] hpleintOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] hpletotOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] intannyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] numloansOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ownerdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);

	//ILIFE-2371 starts
	public FixedLengthStringData subfileArea = new FixedLengthStringData(430);
	//ILIFE-2371 ends
	public FixedLengthStringData subfileFields = new FixedLengthStringData(171)
			.isAPartOf(subfileArea, 0);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData hacrint = DD.hacrint.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public ZonedDecimalData hcurbal = DD.hcurbal.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public ZonedDecimalData hpltot = DD.hpltot.copyToZonedDecimal().isAPartOf(subfileFields,18);
	public ZonedDecimalData hpndint = DD.hpndint.copyToZonedDecimal().isAPartOf(subfileFields,27);
	public ZonedDecimalData hprincipal = (new PackedDDObj(17, 2)).copyToZonedDecimal().isAPartOf(subfileFields,34);
	public ZonedDecimalData loanNumber = DD.loannumber.copyToZonedDecimal().isAPartOf(subfileFields,51);
	public ZonedDecimalData loanStartDate = DD.loanstdate.copyToZonedDecimal().isAPartOf(subfileFields,53);
	public FixedLengthStringData loanType = DD.loantype.copy().isAPartOf(subfileFields,61);
	public ZonedDecimalData numcon = DD.numstamp.copyToZonedDecimal().isAPartOf(subfileFields,62);
	public ZonedDecimalData totrepd = DD.totrepd.copyToZonedDecimal().isAPartOf(subfileFields,70);
	public FixedLengthStringData repdstat = DD.repdstat.copy().isAPartOf(subfileFields,87);
	public FixedLengthStringData repymop = DD.repymeth.copy().isAPartOf(subfileFields,117);
	public ZonedDecimalData repdate = DD.loanapdate.copyToZonedDecimal().isAPartOf(subfileFields,147);
	// CML-072
	public ZonedDecimalData nxtintbdte = DD.nxtintbdte.copyToZonedDecimal()
			.isAPartOf(subfileFields, 155);
	public ZonedDecimalData nxtcapdate = DD.nxtcapdate.copyToZonedDecimal()
			.isAPartOf(subfileFields, 163);
	//ILIFE-2371 starts
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(64)
			.isAPartOf(subfileArea, 172);
	//ILIFE-2371 ends
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData hacrintErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData hcurbalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hpltotErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hpndintErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hprincipalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData loannumberErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData loanstdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData loantypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData numconErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData totrepdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData repdstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData repymopErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData repdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	// CML-072
	public FixedLengthStringData nxtintbdteErr = new FixedLengthStringData(4)
			.isAPartOf(errorSubfile, 56);
	public FixedLengthStringData nxtcapdateErr = new FixedLengthStringData(4)
			.isAPartOf(errorSubfile, 60);

	public FixedLengthStringData outputSubfile = new FixedLengthStringData(192)
			.isAPartOf(subfileArea, 235);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] hacrintOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] hcurbalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hpltotOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hpndintOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hprincipalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] loannumberOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] loanstdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] loantypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] numconOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] totrepdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] repdstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] repymopOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] repdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	// CML-072
	public FixedLengthStringData[] nxtintbdteOut = FLSArrayPartOfStructure(12,
			1, outputSubfile, 168);
	public FixedLengthStringData[] nxtcapdateOut = FLSArrayPartOfStructure(12,
			1, outputSubfile, 180);

	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0)
			.isAPartOf(subfileArea, 427);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData loanStartDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData repdateDisp = new FixedLengthStringData(10);
	// CML-072
	public FixedLengthStringData nxtintbdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nxtcapdateDisp = new FixedLengthStringData(10);

	public LongData Sh593screensflWritten = new LongData(0);
	public LongData Sh593screenctlWritten = new LongData(0);
	public LongData Sh593screenWritten = new LongData(0);
	public LongData Sh593protectWritten = new LongData(0);
	public GeneralTable sh593screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sh593screensfl;
	}

	public Sh593ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(currcdOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		// CML-072
		fieldIndMap.put(nxtintbdteOut, new String[] { "02", "10", "-02", "20",
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(nxtcapdateOut, new String[] { "03", "11", "-03", "21",
				null, null, null, null, null, null, null, null });
		/*fieldIndMap.put(totrepdOut,new String[] {null ,null, null,"06", null, null, null, null, null, null, null, null});
		fieldIndMap.put(repdstatOut,new String[] {"03",null, "-03","07", null, null, null, null, null, null, null, null});
		fieldIndMap.put(repymopOut,new String[] {"04",null, "-04","08", null, null, null, null, null, null, null, null});
		fieldIndMap.put(repdateOut,new String[] {"05",null, "-05","09", null, null, null, null, null, null, null, null});*/
		screenSflFields = new BaseData[] { loanNumber, loanType, loanStartDate,
				cntcurr, hprincipal, hcurbal, hacrint, hpndint, hpltot, numcon,
				totrepd, repdstat, repymop, repdate, nxtintbdte, nxtcapdate };
		screenSflOutFields = new BaseData[][] { loannumberOut, loantypeOut,
				loanstdateOut, cntcurrOut, hprincipalOut, hcurbalOut,
				hacrintOut, hpndintOut, hpltotOut, numconOut, totrepdOut,
				repdstatOut, repymopOut, repdateOut, nxtintbdteOut,
				nxtcapdateOut };
		screenSflErrFields = new BaseData[] { loannumberErr, loantypeErr,
				loanstdateErr, cntcurrErr, hprincipalErr, hcurbalErr,
				hacrintErr, hpndintErr, hpltotErr, numconErr, totrepdErr,
				repdstatErr, repymopErr, repdateErr, nxtcapdateErr,
				nxtcapdateErr };
		screenSflDateFields = new BaseData[] {loanStartDate,repdate,nxtintbdte,nxtcapdate};
		screenSflDateErrFields = new BaseData[] {loanstdateErr,repdateErr,nxtintbdteErr,nxtcapdateErr};
		screenSflDateDispFields = new BaseData[] { loanStartDateDisp,
				repdateDisp, nxtintbdteDisp, nxtcapdateDisp };

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, effdate, chdrstatus, premstatus, currcd, cownnum, ownerdesc, lifenum, lifedesc, numberOfLoans, jlife, jlifedesc, hpleamt, currfrom, intanny, hpleint, ptdate, btdate, hpletot};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, effdateOut, chdrstatusOut, premstatusOut, currcdOut, cownnumOut, ownerdescOut, lifenumOut, lifedescOut, numloansOut, jlifenumOut, jlifedescOut, hpleamtOut, currfromOut, intannyOut, hpleintOut, ptdateOut, btdateOut, hpletotOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, effdateErr, chdrstatusErr, premstatusErr, currcdErr, cownnumErr, ownerdescErr, lifenumErr, lifedescErr, numloansErr, jlifenumErr, jlifedescErr, hpleamtErr, currfromErr, intannyErr, hpleintErr, ptdateErr, btdateErr, hpletotErr};
		screenDateFields = new BaseData[] {effdate, currfrom, ptdate, btdate};
		screenDateErrFields = new BaseData[] {effdateErr, currfromErr, ptdateErr, btdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp, currfromDisp, ptdateDisp, btdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sh593screen.class;
		screenSflRecord = Sh593screensfl.class;
		screenCtlRecord = Sh593screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sh593protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sh593screenctl.lrec.pageSubfile);
	}
}
