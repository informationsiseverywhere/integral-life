package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for ST528
 * @version 1.0 generated on 30/08/09 07:27
 * @author Quipoz
 */
public class St528ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(891);
	public FixedLengthStringData dataFields = new FixedLengthStringData(523).isAPartOf(dataArea, 0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData clrole = DD.clrole.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData lgivname = DD.lgivname.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData lsurname = DD.lsurname.copy().isAPartOf(dataFields,139);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,199);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,200);
	public FixedLengthStringData oragntnam = DD.oragntnam.copy().isAPartOf(dataFields,208);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,255);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,302);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,349);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,357);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,367);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,375);
	public FixedLengthStringData salutl = DD.salutl.copy().isAPartOf(dataFields,385);
	public FixedLengthStringData tsalutsd = DD.tsalutsd.copy().isAPartOf(dataFields,393);
	public FixedLengthStringData zkanasurname = DD.zkanasnm.copy().isAPartOf(dataFields,403);  // ILIFE-8682
	public FixedLengthStringData zkanagivname = DD.zkanagnm.copy().isAPartOf(dataFields,463); // ILIFE-8682
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(92).isAPartOf(dataArea, 523);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData clroleErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lgivnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lsurnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData oragntnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData salutlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData tsalutsdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData zkanasurnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84); // ILIFE-8682
	public FixedLengthStringData zkanagivnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88); // ILIFE-8682
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(276).isAPartOf(dataArea, 523);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] clroleOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lgivnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lsurnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] oragntnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] salutlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] tsalutsdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
        public FixedLengthStringData[] zkanasurnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252); // ILIFE-8682
	public FixedLengthStringData[] zkanagivnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264); // ILIFE-8682
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData St528screenWritten = new LongData(0);
	public LongData St528protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public St528ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payrnumOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payornameOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lsurnameOut,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lgivnameOut,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(salutlOut,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tsalutsdOut,new String[] {null, null, null, "45",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zkanasurnameOut,new String[] {"75","43","-75","72",null, null, null, null, null, null, null, null}); // ILIFE-8682
		fieldIndMap.put(zkanagivnameOut,new String[] {"76","43","-76","72",null, null, null, null, null, null, null, null}); // ILIFE-8682
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, rstate, pstate, cownnum, ownername, payrnum, payorname, agntnum, occdate, ptdate, billfreq, mop, oragntnam, clntnum, clrole, lsurname, lgivname, salutl, tsalutsd,zkanasurname,zkanagivname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, rstateOut, pstateOut, cownnumOut, ownernameOut, payrnumOut, payornameOut, agntnumOut, occdateOut, ptdateOut, billfreqOut, mopOut, oragntnamOut, clntnumOut, clroleOut, lsurnameOut, lgivnameOut, salutlOut, tsalutsdOut,zkanasurnameOut,zkanagivnameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, rstateErr, pstateErr, cownnumErr, ownernameErr, payrnumErr, payornameErr, agntnumErr, occdateErr, ptdateErr, billfreqErr, mopErr, oragntnamErr, clntnumErr, clroleErr, lsurnameErr, lgivnameErr, salutlErr, tsalutsdErr,zkanasurnameErr,zkanagivnameErr};
		screenDateFields = new BaseData[] {occdate, ptdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = St528screen.class;
		protectRecord = St528protect.class;
	}

}
