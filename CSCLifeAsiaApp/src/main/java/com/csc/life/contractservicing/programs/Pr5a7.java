/*
 * File: Pr5a7.java
 * Date: 17 February 2017 0:10:39
 * Author: Quipoz Limited
 * 
 * Class transformed from Pr5a7.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */

package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.smart400framework.dataaccess.model.Descpf;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClftpfDAO;
import com.csc.fsu.clients.dataaccess.dao.impl.ClftpfDAOImpl;
import com.csc.fsu.clients.dataaccess.model.Clftpf;
import com.csc.fsu.clients.recordstructures.Cltskey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.FeddpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Feddpf;
import com.csc.life.contractservicing.screens.Sr5a7ScreenVars;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import java.math.BigDecimal;

/* 
 * EDD PRIVACY WAIVER UPDATE AND ENQUIRY SCREEN
EDD completed and privacy waiver will be the editable fields in modify mode.
The values will be stored in FFEDPf table.
In enquiry all the fields will be in non-editable mode. */



public class Pr5a7 extends ScreenProgCS
{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5127");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sr5a7ScreenVars sv = ScreenProgram.getScreenVars( Sr5a7ScreenVars.class);
	private Wssplife wssplife = new Wssplife();
	private Batckey wsaaBatckey = new Batckey();
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private static final String cltsrec = "CLTSREC";
	private TablesInner tablesInner = new TablesInner();
	private boolean isFatcaAllowed=false;
	private FixedLengthStringData wsaaTableToRead = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaItemToRead = new FixedLengthStringData(8);
	private FormatsInner formatsInner = new FormatsInner();
	private static final String t5688 = "T5688";
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Th506rec th506rec = new Th506rec();
	private DescDAO descdao =new DescDAOImpl(); 
	private ClftpfDAO clftpfdao=new ClftpfDAOImpl();
	private static final String e186 = "E186";
	private Cltskey wsaaCltskey = new Cltskey();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private Map<String,Descpf> Fatcastatus;
	private Feddpf fedd;
	private FeddpfDAO feddpfDAO = getApplicationContext().getBean("feddpfDAO", FeddpfDAO.class);
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	
	 private enum GotoLabel implements GOTOInterface {
			DEFAULT,   exit4090,  
		}
	public Pr5a7() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5a7", AppVars.getInstance(), sv);
	       }
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}

	public void mainline(Object... parmArray)
		{
			sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
			scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
			wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
			wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
			try {
				super.mainline();
			}
			catch (COBOLExitProgramException e) {
			}
		}
	public void processBo(Object... parmArray) {
			sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
			scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
			wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
			wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

			try {
				processBoMainline(sv, sv.dataArea, parmArray);
			} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			}
		}

	protected void initialise1000()
	{
		initialise1010();
		getFatcavalues1020();
		readfeddrecord();
				
		if (isEQ(wsspcomn.sbmaction,"S"))
		 {
			
		 }
		else if(isEQ(wsspcomn.sbmaction,"R"))
		 {
			sv.feddflagOut[varcom.pr.toInt()].set("Y");
			sv.fpvflagOut[varcom.pr.toInt()].set("Y");

		 }
	}
	
	protected void initialise1010()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaFlag.set(wsspcomn.flag);
		sv.dataArea.set(SPACES);
		sv.chdrnum.set(SPACES);
		sv.cnttype.set(SPACES);
		sv.cownnum.set(SPACES);
		sv.ctypdesc.set(SPACES);
		sv.ownername.set(SPACES);
		sv.chdrstatus.set(SPACES);
		sv.premstatus.set(SPACES);
		sv.feddflag.set(SPACES);
		sv.fpvflag.set(SPACES);
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.cnttype.set(chdrmnaIO.getCnttype());
		
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrmnaIO.getCownnum());
		sv.cownnum.set(chdrmnaIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		wsaaTableToRead.set(tablesInner.t3623);
		wsaaItemToRead.set(chdrmnaIO.getStatcode());
		readDesc1100();
		sv.chdrstatus.set(descIO.getShortdesc());
	    
		wsaaTableToRead.set(tablesInner.t3588);
		wsaaItemToRead.set(chdrmnaIO.getPstatcode());
		readDesc1100();
		sv.premstatus.set(descIO.getShortdesc());
		sv.cownnum.set(chdrmnaIO.getCownnum());
		

		
			
		isFatcaAllowed= FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "CLMTN008", appVars, "IT");
		
		if(!isFatcaAllowed){
			sv.fatcastatusFlag.set("Y");
		}
		else{
			sv.fatcastatusFlag.set("N");
			//sv.fatcastatusOut[varcom.pr.toInt()].set("Y");
		}
		
	}
	
	protected void readDesc1100()
	{
		descIO.setRecKeyData(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaTableToRead);
		descIO.setDescitem(wsaaItemToRead);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
		
		
		
	}

	protected void plainname()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

	
	protected void corpname()
	{
		
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}


	
	protected void getFatcavalues1020()
	{

		if(isFatcaAllowed){
			
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl("TH506");
			itemIO.setItemitem(chdrmnaIO.getCnttype());
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				if (isNE(itemIO.getItemitem(), "***")) {
					itemIO.setItemitem("***");
					itemIO.setFormat(formatsInner.itemrec);
					itemIO.setFunction(varcom.readr);
					SmartFileCode.execute(appVars, itemIO);
					if (isNE(itemIO.getStatuz(), varcom.oK)
					&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
						syserrrec.params.set(itemIO.getParams());
						fatalError600();
					}
				}
			}
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				th506rec.th506Rec.set(itemIO.getGenarea());
			}
			else {
				th506rec.th506Rec.set(SPACES);
			}
			Fatcastatus= descdao.getItems("IT", wsspcomn.fsuco.toString(), "TR2DH", wsspcomn.language.toString());
			if(Fatcastatus.size() > 0) {
		     if(isEQ(th506rec.fatcaFlag,"Y"))	{
				Clftpf clftValue=clftpfdao.getRecordByClntnum("CN", wsspcomn.fsuco.toString(), sv.cownnum.toString());
				
				
				if(clftValue==null){
					
					 sv.fatcastatus.set(Fatcastatus.get("NUSWOI").getDescitem());
					 sv.fatcastatusdesc.set(Fatcastatus.get("NUSWOI").getLongdesc());
					
				}else{
				 sv.fatcastatus.set(clftValue.getFfsts().trim());
				 sv.fatcastatusdesc.set(Fatcastatus.get(clftValue.getFfsts().trim()).getLongdesc());
				 }
				}
				else{
					sv.fatcastatus.set(Fatcastatus.get("N/A").getDescitem());
					 sv.fatcastatusdesc.set(Fatcastatus.get("N/A").getLongdesc());
				}
			}
				}

	}
	
	protected void preScreenEdit() 
	{
		// TODO Auto-generated method stub
		
	}
	
	
	protected 	void validate2000()
	{
		//validate fatca status
		if(isFatcaAllowed){
			Fatcastatus= descdao.getItems("IT", wsspcomn.fsuco.toString(), "TR2DH", wsspcomn.language.toString());
			if(Fatcastatus.size() > 0) {
		     if(isEQ(th506rec.fatcaFlag,"Y"))	{
		    	 
		    	 Clftpf clftValue=clftpfdao.getRecordByClntnum("CN", wsspcomn.fsuco.toString(), sv.chdrnum.toString().trim());
				if(clftValue==null){
					 sv.fatcastatus.set(Fatcastatus.get("NUSWOI").getDescitem());
					 sv.fatcastatusdesc.set(Fatcastatus.get("NUSWOI").getLongdesc());
					
				}else{
				 sv.fatcastatus.set(clftValue.getFfsts());
				 sv.fatcastatusdesc.set(Fatcastatus.get(clftValue.getFfsts().trim()).getLongdesc());
				 }
				}
				else{
					sv.fatcastatus.set(Fatcastatus.get("N/A").getDescitem());
					 sv.fatcastatusdesc.set(Fatcastatus.get("N/A").getLongdesc());
				}
			}
				}
		//validate fatca status
		//
	}
	
	protected void readfeddrecord()
	{
		fedd = new Feddpf();
		fedd=feddpfDAO.getbychdrnum(chdrmnaIO.getChdrcoy().toString(), chdrmnaIO.getChdrnum().toString());
		if (fedd == null) {
			return;
		}
		sv.feddflag.set(fedd.getFeddflag());
		sv.fpvflag.set(fedd.getFpvflag());
		
	}
	
	protected void update3000()
	{
		
		 fedd = new Feddpf();
		if (fedd == null) {
				return;
			}
		
		fedd.setChdrpfx("CH");
		fedd.setChdrcoy("2");
		fedd.setChdrnum(sv.chdrnum.toString());
	//  fedd.setStat_change_dat(new BigDecimal(wsaaToday.toInt()));
		fedd.setStatuschangedat(new BigDecimal(wsaaToday.toInt())); //ILIFE-7887
		fedd.setValidflag("1");
		fedd.setFeddflag(sv.feddflag.toString());
		fedd.setFpvflag(sv.fpvflag.toString());
		
		feddpfDAO.updateeddRecord(fedd);
		feddpfDAO.insertFeddRecord(fedd);
		if (isEQ(wsspcomn.sbmaction,"S")){
		if(isEQ(fedd.getFeddflag(),SPACES)){
			sv.feddflagErr.set(e186);
		}
		
		}
            
	}
	
	protected void whereNext4000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4020();
				case exit4090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void nextProgram4020() {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

	 

	
	private static final class TablesInner 
	{ 
		private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
		private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
		private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	}
	private static final class FormatsInner 
	{ 
		private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	}

	
}