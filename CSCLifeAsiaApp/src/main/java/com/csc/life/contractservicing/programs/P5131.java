/*

 * File: P5131.java
 * Date: 30 August 2009 0:11:49
 * Author: Quipoz Limited
 * 
 * Class transformed from P5131.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LextchgTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.procedures.UwQuestionnaireUtil;
import com.csc.life.contractservicing.procedures.UwoccupationUtil;
import com.csc.life.contractservicing.screens.S5131ScreenVars;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.tablestructures.T5657rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BaseScreenData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This   screen/program   P5131   is   used   to   capture  the
* Special   Terms   (Options/Extras)  details  for  traditional
* generic components.
*
* Initialise
* ----------
*
* Retrieve  the  COVRMJA  record (RETRV) in order to access the
* Coverage/Rider  details,  as  these were previously read they
* should still be in storage.
*
* The COVRMJA key will be:-
*
*  Company, Contract-no, Life, Coverage-no, Rider-no.
*
* Look  up  the  description  of the coverage rider (DESC) from
* T5687.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*  - read  the life details using LIFEMJA (life number from
*       COVTTRM, joint life number '00').  Look up the name
*       from the  client  details  (CLTS)  and  format as a
*       "confirmation name".
*
*  - read the joint life details using LIFEMJA (life number
*       from COVRMJA,  joint  life number '01').  If found,
*       look up the name from the client details (CLTS) and
*       format as a "confirmation name".
*
* Read  the  general  coverage/rider details from T5687 for the
* the coverage type held on COVRMJA. Access the version for the
* original commencement date of the risk.
*
* To determine which life the non-standard terms apply to decide
* whether or  not  it  is  a Single or Joint-life case (it is a
* joint  life  case,  the  joint  life record was found above).
* Then:
*
*  - if both lives are present and the Join-life indicator is
*       'N',  or if there is only a single life,  non-dispLAy
*       the life selection option.
*
*
* OPTIONS/EXTRAS DETAILS
*
* The  subfile  is  a  fixed  size  of 8 records. Initialise it
* first,  and then randomly access the required subfile records
* to  load  any  existing  details.  The  details are loaded as
* follows:
*
*  - read  each  LEXTMJA  record (keyed: company, contract-no,
*       life-no, coverage, rider, sequence).
*
*  - if present,  retrieve  the  subfile  record  with  the
*       relative record  number of the record read.  Output
*       all   existing   values,   looking-up   the   short
*       description of the reason code from T5651. Write an
*       indicator to  a  hidden  field to identify that the
*       record  already  exists on the database. Output the
*       life  indicator  as follows; blank if joint life no
*       is blank, 'L' if '00', 'J' if '01'.
*
* Validation
* ----------
*
* If enquiry  mode  (the  WSSP-FLAG  =  'I'), protect all input
* capable fields.
*
* If enquiry  mode  or  CF11  (KILL)  is  requested,  skip  all
* validation.
*
* Edit each line  modified in the subfile according to the Help
* information.
*
* If  all the different rating options are left blank, retrieve
* the  default  from T5657 (key cover/rider-code, reason-code).
* Update  the  subfile  records  with these in case it is to be
* re-displayed.
*
* If 'CALC' was entered then re-display the screen.
*
* Updating
* --------
*
* If the WSSP-FLAG = 'I' (enquiry mode)  or the 'KILL' function
* key was pressed, then skip the updating.
*
* For each  subfile  record, update the options and extras file
* ( ie. the LEXT file ) as follows:
*
*  - if the  record   was  loaded  from  the  database
*       and all fields are now blank, rewrite the record with a
*       validflag = '2' and its TRANNO set to the contract
*       header tranno + 1.
*
*  - if the record  was  present  and  the  details are not
*       blank, try to write a validflag '2' LEXT record, with
*       the TRANNO set to the Contract header tranno + 1. Then
*       write a validflag '1' LEXT record, with the same TRANNO
*       as the validflag '2' record and the new details from the
*       subfile record being processed.
*
*  - if the record was not present, write a new LEXT record, with
*    a validflag '1' and the TRANNO set to the Contract Header
*    tranno + 1.
*
*** Programmers note..
*   The validflag '1' & '2' processing has been introduced to try
*    and enable Component Modify reversals to correctly identify
*    which Options/Extras (LEXT) records were affected by the
*    forward transaction.
*
*
* Next Program
* ------------
*
*  - add one to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5131 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5131");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	protected FixedLengthStringData wsaaT5657key = new FixedLengthStringData(6);
	protected FixedLengthStringData wsaaT5657key1 = new FixedLengthStringData(4).isAPartOf(wsaaT5657key, 0).init(SPACES);
	protected FixedLengthStringData wsaaT5657key2 = new FixedLengthStringData(2).isAPartOf(wsaaT5657key, 4).init(SPACES);
	protected FixedLengthStringData wsaaVflag2 = new FixedLengthStringData(1).init(SPACES);
	protected PackedDecimalData wsaaLextTranno = new PackedDecimalData(5, 0).init(ZERO);

	protected FixedLengthStringData wsaaEntryFound = new FixedLengthStringData(1);
	private Validator noEntryFound = new Validator(wsaaEntryFound, "N");

	private FixedLengthStringData wsaaTh549Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh549Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh549Key, 0);
	private ZonedDecimalData wsaaTh549Zmortpct = new ZonedDecimalData(3, 0).isAPartOf(wsaaTh549Key, 4).setUnsigned();
	private FixedLengthStringData wsaaTh549Zsexmort = new FixedLengthStringData(1).isAPartOf(wsaaTh549Key, 7);
		/* ERRORS */
	private String e492 = "E492";
	private String f246 = "F246";
	private String g709 = "G709";
	protected String h039 = "H039";
	private String f307 = "F307";
	protected String d032 = "D032";
	private String hl27 = "HL27";
		/* TABLES */
	protected String t5651 = "T5651";
	protected String t5657 = "T5657";
	private String t5687 = "T5687";
	private String th549 = "TH549";
	private String cltsrec = "CLTSREC";
	protected String lextmjarec = "LEXTMJAREC";
	protected String descrec = "DESCREC";
	protected String lextchgrec = "LEXTCHGREC";
		/*Contract Header File - Major Alts*/
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	protected Datcon2rec datcon2rec = new Datcon2rec();
		/*Logical File: Extra data screen*/
	protected DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	protected ItemTableDAM itemIO = new ItemTableDAM();
		/*Component Change Options/Extras logical*/
	private LextchgTableDAM lextchgIO = new LextchgTableDAM();
		/*Options and Extras Comp Change - Major A*/
	private LextTableDAM lextmjaIO = new LextTableDAM();
		/*Life Details File - Major Alts*/
	protected LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	protected T5657rec t5657rec = new T5657rec();
	protected T5687rec t5687rec = new T5687rec();
	private Wssplife wssplife = new Wssplife();
	private S5131ScreenVars sv = getPScreenVars();
	
	//ILB-456 starts
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	protected Chdrpf chdrpf=new Chdrpf();
	protected CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	protected Covrpf covrpf = new Covrpf();
	protected List<Covrpf> covrpfList;
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private boolean uwFlag = false;
	private UwQuestionnaireUtil uwQuestionnaireUtil = getApplicationContext().getBean("uwQuestionnaireUtil", UwQuestionnaireUtil.class);
	private UwoccupationUtil uwoccupationUtil = getApplicationContext().getBean("uwoccupationUtil", UwoccupationUtil.class);
	private UWQuestionnaireRec uwQuestionnaireRec = null;
	private UWQuestionnaireRec uwQuestionnaireRecReturn =null;
	private UWOccupationRec uwOccupationRec = null;
	private String uwlvrec = "UWLVREC";
	public final static String OCCU_FLAG = "OCCU_FLAG";
	public final static String QUESTION_FLAG = "QUESTION_FLAG";
	private int autoTermPos = 1;
	private List<String> reasonList = null;
	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private String crtable = "";
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		lextmja1015, 
		loadExistingLextmja1030, 
		preExit, 
		exit2490, 
		lookUpDesc2505, 
		updateErrorIndicators2520, 
		exit2699, 
		exit3490, 
		nextLine3580, 
		exit3690
	}

	public P5131() {
		super();
		screenVars = sv;
		new ScreenModel("S5131", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

protected S5131ScreenVars getPScreenVars()
{
	return ScreenProgram.getScreenVars(S5131ScreenVars.class);
}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case lextmja1015: {
					lextmja1015();
				}
				case loadExistingLextmja1030: {
					loadExistingLextmja1030();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.agerate.set(ZERO);
		sv.seqnbr.set(ZERO);
		sv.extCessTerm.set(ZERO);
		sv.insprm.set(ZERO);
		/*BRD-306 START */
		sv.premadj.set(ZERO);
		/*BRD-306 END */
		sv.znadjperc.set(ZERO);
		sv.zmortpct.set(ZERO);
		sv.oppc.set(ZERO);
		wsaaEntryFound.set("N");
		scrnparams.function.set(varcom.sclr);
		processScreen("S5131", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		//ILB-456 
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
		}
		else {
		covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
		}
	}
		/*covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		sv.chdrnum.set(covrpf.getChdrnum());
		sv.coverage.set(covrpf.getCoverage());
		sv.life.set(covrpf.getLife());
		uwFlag = FeaConfg.isFeatureExist("2", "NBPRP123",appVars, "IT");
		if(uwFlag){
			sv.crtable.set(getCrtable());
		}else{
			sv.crtable.set(covrpf.getCrtable());
		}
		crtable = sv.crtable.toString();
		sv.rider.set(covrpf.getRider());
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		else {
			sv.crtabdesc.fill("?");
		}
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(t5687,itdmIO.getItemtabl())
		|| isNE(covrpf.getCrtable(),itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		
		if(uwFlag) {
			sv.uwFlag = 1;
			uwlvIO.setDataKey(SPACES);
			uwlvIO.setUserid(wsspcomn.userid);
			uwlvIO.setCompany(wsspcomn.company);
			uwlvIO.setFunction(varcom.readr);
			uwlvIO.setFormat(uwlvrec);
			SmartFileCode.execute(appVars, uwlvIO);
			if (isNE(uwlvIO.getStatuz(),varcom.oK)
					&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(uwlvIO.getParams());
				fatalError600();
			}
			if (isEQ(uwlvIO.getStatuz(),varcom.oK)) {
				if(isEQ(uwlvIO.getSkipautouw(),"Y")){
					sv.skipautouw = 1;
				}else{
					sv.skipautouw = 2;
				}
			}else{
				sv.skipautouw = 2;
			}
		}
		sv.occup01.set(lifemjaIO.getOccup());
		sv.pursuit01.set(lifemjaIO.getPursuit01());
		sv.pursuit02.set(lifemjaIO.getPursuit02());
		sv.smoking01.set(lifemjaIO.getSmoking());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			sv.occup02.set(SPACES);
			sv.pursuit03.set(SPACES);
			sv.pursuit04.set(SPACES);
			sv.smoking02.set(SPACES);
			goTo(GotoLabel.lextmja1015);
		}
		sv.occup02.set(lifemjaIO.getOccup());
		sv.pursuit03.set(lifemjaIO.getPursuit01());
		sv.pursuit04.set(lifemjaIO.getPursuit02());
		sv.smoking02.set(lifemjaIO.getSmoking());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void lextmja1015()
	{
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)
		|| isEQ(t5687rec.jlifePresent,"N")) {
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			addBlankLine1020();
		}
		goTo(GotoLabel.loadExistingLextmja1030);
	}

protected void addBlankLine1020()
	{
		processScreen("S5131", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void loadExistingLextmja1030()
	{
		lextmjaIO.setChdrcoy(covrpf.getChdrcoy());
		lextmjaIO.setChdrnum(covrpf.getChdrnum());
		lextmjaIO.setLife(covrpf.getLife());
		lextmjaIO.setCoverage(covrpf.getCoverage());
		lextmjaIO.setRider(covrpf.getRider());
		lextmjaIO.setSeqnbr(ZERO);
		lextmjaIO.setFormat(lextmjarec);
		lextmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextmjaIO);
		if (isNE(lextmjaIO.getStatuz(),varcom.oK)
		&& isNE(lextmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextmjaIO.getParams());
			fatalError600();
		}
		while ( !(isEQ(lextmjaIO.getStatuz(),varcom.endp)
		|| isNE(lextmjaIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(lextmjaIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(lextmjaIO.getLife(),covrpf.getLife())
		|| isNE(lextmjaIO.getCoverage(),covrpf.getCoverage())
		|| isNE(lextmjaIO.getRider(),covrpf.getRider()))) {
			loadSubfile1500();
		}
		setAutoTerm();
		if (isEQ(wsspcomn.flag,"I")) {
			sv.opcdaOut[varcom.pr.toInt()].set("Y");
		}
	}
	private void setAutoTerm() {
		if(uwFlag) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			List<String> autoTermsList = (List)ThreadLocalStore.get(Pr572.AUTO_TERMS_LIST_ADD);
			if(autoTermsList != null && autoTermsList.contains(getCrtable())) {
				return;
			} else {
				loadAutoTerms();
			}
		}
	}
	private String getCrtable() {
		Covtpf covt = covtpfDAO.getCovtlnbData(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(),
				covrpf.getCoverage(), covrpf.getRider());
		if (covt != null) {
			crtable = covt.getCrtable();
			return crtable;
		}
		return "";
	}
private void loadAutoTerms() {
	loadDefaultOccupation(true);
	loadDefaultQuestion(true);
}

	private void loadDefaultOccupation(boolean addFlag) {
		uwOccupationRec = new UWOccupationRec();
		uwOccupationRec.transEffdate.set(datcon1rec.intDate.getbigdata());
		uwOccupationRec.occupCode.set(cltsIO.getOccpcode());
		uwOccupationRec.indusType.set(cltsIO.getStatcode());
		uwOccupationRec = uwoccupationUtil.getUWOccupationRec(uwOccupationRec, chdrpf.getChdrcoy().toString(),
				wsspcomn.fsuco.toString(), chdrpf.getCnttype().toString(), crtable);
		if (uwOccupationRec != null && isNE(uwOccupationRec.outputUWDec, "Accepted")
				&& isNE(uwOccupationRec.outputUWDec, SPACE)
				&& isNE(uwOccupationRec.outputUWDec, "0")) {
			if(addFlag) {
				if(reasonList != null && reasonList.contains(uwOccupationRec.outputSplTermCode.toString())) {
					return;
				}
				scrnparams.subfileRrn.set(autoTermPos);
				scrnparams.function.set(varcom.sread);
				processScreen("S5131", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
			sv.insprm.set(uwOccupationRec.outputSplTermRateAdj);
			sv.agerate.set(uwOccupationRec.outputSplTermAge);
			sv.oppc.set(uwOccupationRec.outputSplTermLoadPer);
			sv.zmortpct.set(uwOccupationRec.outputMortPerc);
			sv.znadjperc.set(uwOccupationRec.outputSplTermSAPer);
			sv.autoFlag.set("Y");
			sv.opcda.set(uwOccupationRec.outputSplTermCode);
			sv.reasind.set("1");
			sv.uwoverwrite.set("Y");
			sv.agerateOut[varcom.pr.toInt()].set("Y");
			sv.ecestrmOut[varcom.pr.toInt()].set("Y");
			sv.insprmOut[varcom.pr.toInt()].set("Y");
			// sv.opcdaOut[varcom.pr.toInt()].set("Y");
			sv.oppcOut[varcom.pr.toInt()].set("Y");
			sv.reasindOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.seqnbrOut[varcom.pr.toInt()].set("Y");
			sv.shortdescOut[varcom.pr.toInt()].set("Y");
			sv.zmortpctOut[varcom.pr.toInt()].set("Y");
			sv.znadjpercOut[varcom.pr.toInt()].set("Y");
			sv.premadjOut[varcom.pr.toInt()].set("Y");

			if(addFlag) {
				scrnparams.function.set(varcom.supd);
				processScreen("S5131", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
				autoTermPos++;
			}
		}
	}

	private void loadDefaultQuestion(boolean addFlag) {
		List<String> questions = getQuestions();

		if (questions != null) {
			for (String question : questions) {

				uwQuestionnaireRec = new UWQuestionnaireRec();
				uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
				uwQuestionnaireRec.setContractType(chdrpf.getCnttype().toString());
				uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec,
						wsspcomn.language.toString(), chdrpf.getChdrcoy().toString().trim(), question,
						crtable, "");

				if (uwQuestionnaireRecReturn != null
						&& !uwQuestionnaireRecReturn.getOutputUWDec().contains("Accepted")
						&& !uwQuestionnaireRecReturn.getOutputUWDec().contains("0")) {
					setQuestionValue(addFlag);
				}

			}
		}
	}
private void setQuestionValue(boolean addFlag) {
	for (int i = 0; i < uwQuestionnaireRecReturn.getOutputUWDec().size(); i++) {

		if(addFlag) {
			if(reasonList != null && reasonList.contains(uwQuestionnaireRecReturn.getOutputSplTermCode().get(i))) {
				continue;
			}
			scrnparams.subfileRrn.set(autoTermPos);
			scrnparams.function.set(varcom.sread);
			processScreen("S5131", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		sv.insprm.set(uwQuestionnaireRecReturn.getOutputSplTermRateAdj().get(i));
		sv.agerate.set(uwQuestionnaireRecReturn.getOutputSplTermAge().get(i));
		sv.oppc.set(uwQuestionnaireRecReturn.getOutputSplTermLoadPer().get(i));
		sv.zmortpct.set(uwQuestionnaireRecReturn.getOutputMortPerc().get(i));
		sv.znadjperc.set(uwQuestionnaireRecReturn.getOutputSplTermSAPer().get(i));
		sv.opcda.set(uwQuestionnaireRecReturn.getOutputSplTermCode().get(i));
		sv.uwoverwrite.set("Y");
		sv.autoFlag.set("Y");
		sv.reasind.set("1");
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.agerateOut[varcom.pr.toInt()].set("Y");
		sv.ecestrmOut[varcom.pr.toInt()].set("Y");
		sv.insprmOut[varcom.pr.toInt()].set("Y");
		sv.oppcOut[varcom.pr.toInt()].set("Y");
		sv.reasindOut[varcom.pr.toInt()].set("Y");
		sv.seqnbrOut[varcom.pr.toInt()].set("Y");
		sv.shortdescOut[varcom.pr.toInt()].set("Y");
		sv.zmortpctOut[varcom.pr.toInt()].set("Y");
		sv.znadjpercOut[varcom.pr.toInt()].set("Y");
		sv.premadjOut[varcom.pr.toInt()].set("Y");
		if(addFlag) {
			scrnparams.function.set(varcom.supd);
			processScreen("S5131", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			autoTermPos++;
		}
	}
}
private boolean checkOccuCode(String code) {
	if(code != null && (code.startsWith("A") || code.startsWith("B"))) {
		return true;
	}
	return false;
}

	private List<String> getQuestions() {
		List<Undqpf> undqList = undqpfDAO.searchUnqpf(lifemjaIO.getChdrcoy().toString().trim(),
				lifemjaIO.getChdrnum().toString().trim(), lifemjaIO.getLife().toString().trim(),
				lifemjaIO.getJlife().toString().trim());
		List<String> questions = new ArrayList<>();
		for (Undqpf u : undqList) {
			if ("Y".equals(u.getAnswer().trim())) {
				questions.add(u.getQuestidf());
			}
		}
		return questions;
	}
	
protected void loadSubfile1500()
	{
		para1500();
	}

protected void para1500()
	{
		autoTermPos = lextmjaIO.getSeqnbr().toInt() + 1;
		scrnparams.function.set(varcom.sread);
		processScreen("S5131", sv);
		if (isNE(scrnparams.statuz,varcom.oK) ) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(lextmjaIO.getSeqnbr());
		sv.agerate.set(lextmjaIO.getAgerate());
		sv.extCessTerm.set(lextmjaIO.getExtCessTerm());
		sv.insprm.set(lextmjaIO.getInsprm());
		/*BRD-306 START */
		sv.premadj.set(lextmjaIO.getPremadj());
		/*BRD-306 END */
		sv.opcda.set(lextmjaIO.getOpcda());
		setUWFields();
		sv.oppc.set(lextmjaIO.getOppc());
		sv.seqnbr.set(lextmjaIO.getSeqnbr());
		sv.reasind.set(lextmjaIO.getReasind());
		sv.znadjperc.set(lextmjaIO.getZnadjperc());
		sv.zmortpct.set(lextmjaIO.getZmortpct());
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(lextmjaIO.getChdrcoy());
		descIO.setDesctabl(t5651);
		descIO.setDescitem(lextmjaIO.getOpcda());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.shortdesc.set(descIO.getShortdesc());
		}
		else {
			sv.shortdesc.fill("?");
		}
		if (isEQ(lextmjaIO.getJlife(),"00")) {
			sv.select.set("L");
		}
		else {
			if (isEQ(lextmjaIO.getJlife(),"01")) {
				sv.select.set("J");
			}
			else {
				sv.select.set(SPACES);
			}
		}
		wsaaEntryFound.set("Y");
		scrnparams.function.set(varcom.supd);
		processScreen("S5131", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		lextmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lextmjaIO);
		if (isNE(lextmjaIO.getStatuz(),varcom.oK)
		&& isNE(lextmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextmjaIO.getParams());
			fatalError600();
		}
	}
private void setUWFields() {
	if(uwFlag) {
		if(reasonList == null) {
			reasonList = new ArrayList<>();
		}
		reasonList.add(lextmjaIO.getOpcda().toString());
		sv.uwoverwrite.set(lextmjaIO.getUwoverwrite());
		if(isNE(SPACE, lextmjaIO.getUwoverwrite())) {
			sv.autoFlag.set("Y");
			if(isEQ("Y", lextmjaIO.getUwoverwrite())) {
				disableRecord();
			}
		} 
	}
}
private void disableRecord() {
	sv.selectOut[varcom.pr.toInt()].set("Y");
	sv.agerateOut[varcom.pr.toInt()].set("Y");
	sv.ecestrmOut[varcom.pr.toInt()].set("Y");
	sv.insprmOut[varcom.pr.toInt()].set("Y");
	sv.oppcOut[varcom.pr.toInt()].set("Y");
	sv.reasindOut[varcom.pr.toInt()].set("Y");
	sv.seqnbrOut[varcom.pr.toInt()].set("Y");
	sv.shortdescOut[varcom.pr.toInt()].set("Y");
	sv.zmortpctOut[varcom.pr.toInt()].set("Y");
	sv.znadjpercOut[varcom.pr.toInt()].set("Y");
	sv.premadjOut[varcom.pr.toInt()].set("Y");
}
protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I") || isEQ(wsspcomn.sbmaction,"D") || isEQ(wsspcomn.flag,"P")){ // Ticket #ILIFE-7244 
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2490);
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateScreen2010()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5131", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2500();
		}
		
		if (noEntryFound.isTrue()
		&& isNE(sv.znadjperc,ZERO)) {
			sv.znadjpercErr.set(e492);
			wsspcomn.edterror.set("Y");
		}
		/*BRD-306 START */
		if (noEntryFound.isTrue()
		&& isNE(sv.premadj,ZERO)) {
			sv.premadjErr.set(e492);
			wsspcomn.edterror.set("Y");
		}
		/*BRD-306 END */
		
	}

protected void validateSubfile2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					opcda2500();
				}
				case lookUpDesc2505: {
					lookUpDesc2505();
					default2510();
				}
				case updateErrorIndicators2520: {
					updateErrorIndicators2520();
					readNextModifiedRecord2530();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void opcda2500()
	{
		if(uwFlag){
			if(isEQ(sv.uwoverwrite,"Y")){
				disableRecord();
			}else if(isEQ(sv.uwoverwrite,"N") || isEQ(sv.uwoverwrite,SPACE)){
				sv.selectOut[varcom.pr.toInt()].set("N");
				sv.agerateOut[varcom.pr.toInt()].set("N");
				sv.ecestrmOut[varcom.pr.toInt()].set("N");
				sv.insprmOut[varcom.pr.toInt()].set("N");
				sv.oppcOut[varcom.pr.toInt()].set("N");
				sv.reasindOut[varcom.pr.toInt()].set("N");
				sv.seqnbrOut[varcom.pr.toInt()].set("N");
				sv.shortdescOut[varcom.pr.toInt()].set("N");
				sv.zmortpctOut[varcom.pr.toInt()].set("N");
				sv.znadjpercOut[varcom.pr.toInt()].set("N");
				sv.premadjOut[varcom.pr.toInt()].set("N");
				sv.premadj.setEnabled(new Byte(BaseScreenData.ENABLED));
			}
		}
		if (isEQ(sv.opcda,SPACES)) {
			sv.shortdesc.set(SPACES);
		}
		if (isEQ(sv.agerate,ZERO)
		&& isEQ(sv.extCessTerm,ZERO)
		&& isEQ(sv.insprm,ZERO)
		&& isEQ(sv.oppc,ZERO)
		&& isEQ(sv.opcda,SPACES)
		&& isEQ(sv.select,SPACES)		
		/*BRD-306 START */
		&& isEQ(sv.premadj,ZERO)
		) {
			wsaaEntryFound.set("N");
		}
		else {
			/*BRD-306 END */
			wsaaEntryFound.set("Y");
		}
		//ILIFE-1838
		if(isEQ(wsaaEntryFound,"Y")) {
		validation2110CustomerSpecific() ;
				
			if (isEQ(sv.reasind,SPACES)) {
				sv.reasind.set("1");
			}
			if (isNE(sv.reasind,"1")
			&& isNE(sv.reasind,"2")
			&& isNE(sv.reasind,"3")) {
				sv.reasindErr.set(g709);
				wsspcomn.edterror.set("Y");
			}
			if (isNE(sv.zmortpct,ZERO)) {
				checkTh5492600();
			}
			if (isNE(sv.opcda,SPACES)) {
				goTo(GotoLabel.lookUpDesc2505);
			}
			if (isNE(sv.agerate,ZERO)) {
				sv.agerateErr.set(e492);
			}
			if (isNE(sv.insprm,ZERO)) {
				sv.insprmErr.set(e492);
			}
			/*BRD-306 START */
			if (isNE(sv.premadj,ZERO)) {
				sv.premadjErr.set(e492);
			}
			/*BRD-306 END */
			if (isNE(sv.oppc,ZERO)) {
				sv.oppcErr.set(e492);
			}
			if (isNE(sv.extCessTerm,ZERO)) {
				sv.ecestrmErr.set(e492);
			}
			if (isNE(sv.znadjperc,ZERO)) {
				sv.znadjpercErr.set(e492);
			}
			if (isNE(sv.select,ZERO)) {
				//sv.selectErr.set(e492);
			}
		}
		goTo(GotoLabel.updateErrorIndicators2520);
	}

protected void validation2110CustomerSpecific() {
	if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)
	|| isEQ(t5687rec.jlifePresent,"N")) {
		if (isNE(sv.select,SPACES)) {
			sv.select.set(SPACES);
		}
	}
	else {
		if (isNE(sv.select,"J")
		&& isNE(sv.select,"L")) {
			sv.selectErr.set(h039);
		}
	}
}
protected void lookUpDesc2505()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5651);
		descIO.setDescitem(sv.opcda);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.shortdesc.set(descIO.getShortdesc());
		}
		else {
			sv.shortdesc.fill("?");
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covrpf.getChdrcoy());
		itemIO.setItemtabl(t5657);
		wsaaT5657key1.set(covrpf.getCrtable());
		wsaaT5657key2.set(sv.opcda);
		itemIO.setItemitem(wsaaT5657key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf) && isNE("Y", sv.autoFlag)) {
			sv.opcdaErr.set(f246);
			goTo(GotoLabel.updateErrorIndicators2520);
		}
		t5657rec.t5657Rec.set(itemIO.getGenarea());
	}

protected void default2510()
	{
	default2511CustomerSpecific();
	
	if (isNE(t5657rec.tabtype,"N")) {
			goTo(GotoLabel.updateErrorIndicators2520);
		}
		if (isEQ(sv.agerate,ZERO)
		&& isEQ(sv.insprm,ZERO)
		&& isEQ(sv.oppc,ZERO)) {
			if (isEQ(t5657rec.t5657Rec,SPACES)) {
				sv.agerate.set(ZERO);
				sv.insprm.set(ZERO);
				sv.oppc.set(ZERO);
			}
			else {
				sv.agerate.set(t5657rec.agerate);
				sv.insprm.set(t5657rec.insprm);
				sv.oppc.set(t5657rec.oppc);
			}
		}
		if (isNE(sv.opcda,SPACES)) {
			if (isEQ(sv.agerate,ZERO)
			&& isEQ(sv.insprm,ZERO)
			&& isEQ(sv.oppc,ZERO)
			&& isEQ(sv.zmortpct,ZERO)
			/*BRD-306 START */
			&& isEQ(sv.premadj,ZERO)) {
				sv.agerateErr.set(f307);
				sv.insprmErr.set(f307);
				sv.oppcErr.set(f307);
				sv.zmortpctErr.set(f307);
				sv.premadjErr.set(f307);
			}
			/*BRD-306 END */
		}
		if(uwFlag && isEQ("Y", sv.uwoverwrite)){
			if(checkOccuCode(sv.opcda.toString().trim())) {
				loadDefaultOccupation(false);
			}else {
				loadDefaultQuestion(false);
			}
		}
	}
protected void 	default2511CustomerSpecific(){
	if (isNE(t5657rec.tabtype,"N")) {
		if (isNE(sv.agerate,ZERO)) {
			sv.agerateErr.set(d032);
			if(isNE("Y", sv.autoFlag)) {
				sv.opcdaErr.set(d032);
			}
		}
		if (isNE(sv.oppc,ZERO)) {
			sv.oppcErr.set(d032);
			if(isNE("Y", sv.autoFlag)) {
				sv.opcdaErr.set(d032);
			}
		}
		if (isNE(sv.insprm,ZERO)) {
			sv.insprmErr.set(d032);
			if(isNE("Y", sv.autoFlag)) {
				sv.opcdaErr.set(d032);
			}
		}
		}
	}

protected void updateErrorIndicators2520()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5131", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2530()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5131", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkTh5492600()
	{
		try {
			th5492601();
		}
		catch (GOTOException e){
		}
	}

protected void th5492601()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrpf.getChdrcoy());
		itdmIO.setItemtabl(th549);
		wsaaTh549Crtable.set(sv.crtable);
		wsaaTh549Zmortpct.set(sv.zmortpct);
		wsaaTh549Zsexmort.set(covrpf.getSex());
		itdmIO.setItemitem(wsaaTh549Key);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsaaTh549Key,itdmIO.getItemitem())
		|| isNE(covrpf.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),th549)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			sv.zmortpctErr.set(hl27);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2699);
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,"KILL")
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3490);
		}
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5131", sv);
		lextmjaIO.setChdrcoy(covrpf.getChdrcoy());
		lextmjaIO.setChdrnum(covrpf.getChdrnum());
		lextmjaIO.setLife(covrpf.getLife());
		lextmjaIO.setCoverage(covrpf.getCoverage());
		lextmjaIO.setRider(covrpf.getRider());
		lextmjaIO.setFormat(lextmjarec);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			srdn3500();
		}
		
	}

protected void srdn3500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para3510();
				}
				case nextLine3580: {
					nextLine3580();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3510()
	{
		if (isEQ(sv.seqnbr,ZERO)
		&& isEQ(sv.opcda,SPACES)) {
			goTo(GotoLabel.nextLine3580);
		}
		if (isNE(sv.seqnbr,ZERO)
		&& isEQ(sv.opcda,SPACES)) {
			lextchgIO.setParams(SPACES);
			lextchgIO.setChdrcoy(covrpf.getChdrcoy());
			lextchgIO.setChdrnum(covrpf.getChdrnum());
			lextchgIO.setLife(covrpf.getLife());
			lextchgIO.setCoverage(covrpf.getCoverage());
			lextchgIO.setRider(covrpf.getRider());
			lextchgIO.setSeqnbr(sv.seqnbr);
			lextchgIO.setTranno(ZERO);
			lextchgIO.setValidflag("2");
			lextchgIO.setFormat(lextchgrec);
			lextchgIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			lextchgIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			lextchgIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "SEQNBR");
			compute(wsaaLextTranno, 0).set(add(1,chdrpf.getTranno()));
			wsaaVflag2.set(SPACES);
			while ( !(isEQ(lextchgIO.getStatuz(),varcom.endp))) {
				lextCheck3600();
			}
			
			if (isEQ(wsaaVflag2,"Y")) {
				goTo(GotoLabel.nextLine3580);
			}
			lextmjaIO.setParams(SPACES);
			lextmjaIO.setChdrcoy(covrpf.getChdrcoy());
			lextmjaIO.setChdrnum(covrpf.getChdrnum());
			lextmjaIO.setLife(covrpf.getLife());
			lextmjaIO.setCoverage(covrpf.getCoverage());
			lextmjaIO.setRider(covrpf.getRider());
			lextmjaIO.setSeqnbr(sv.seqnbr);
			lextmjaIO.setValidflag("1");
			lextmjaIO.setFormat(lextmjarec);
			lextmjaIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, lextmjaIO);
			if (isNE(lextmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(lextmjaIO.getParams());
				syserrrec.statuz.set(lextmjaIO.getStatuz());
				fatalError600();
			}
			lextmjaIO.setTermid(varcom.vrcmTermid);
			lextmjaIO.setTransactionDate(varcom.vrcmDate);
			lextmjaIO.setTransactionTime(varcom.vrcmTime);
			lextmjaIO.setUser(varcom.vrcmUser);
			lextmjaIO.setValidflag("2");
			lextmjaIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, lextmjaIO);
			if (isNE(lextmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(lextmjaIO.getParams());
				syserrrec.statuz.set(lextmjaIO.getStatuz());
				fatalError600();
			}
			goTo(GotoLabel.nextLine3580);
		}
		lextmjaIO.setParams(SPACES);
		lextmjaIO.setChdrcoy(covrpf.getChdrcoy());
		lextmjaIO.setChdrnum(covrpf.getChdrnum());
		lextmjaIO.setLife(covrpf.getLife());
		lextmjaIO.setCoverage(covrpf.getCoverage());
		lextmjaIO.setRider(covrpf.getRider());
		lextmjaIO.setSeqnbr(scrnparams.subfileRrn);
		lextmjaIO.setValidflag("1");
		lextmjaIO.setFormat(lextmjarec);
		lextmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, lextmjaIO);
		if (isNE(lextmjaIO.getStatuz(),varcom.oK)
		&& isNE(lextmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lextmjaIO.getParams());
			syserrrec.statuz.set(lextmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lextmjaIO.getStatuz(),varcom.oK)) {
			lextmjaIO.setValidflag("2");
			lextmjaIO.setFormat(lextmjarec);
			lextmjaIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, lextmjaIO);
			if (isNE(lextmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(lextmjaIO.getParams());
				syserrrec.statuz.set(lextmjaIO.getStatuz());
				fatalError600();
			}
		}
		lextmjaIO.setCurrfrom(ZERO);
		lextmjaIO.setCurrto(varcom.vrcmMaxDate);
		lextmjaIO.setOpcda(sv.opcda);
		lextmjaIO.setOppc(sv.oppc);
		lextmjaIO.setInsprm(sv.insprm);
		/*BRD-306 START */
		lextmjaIO.setPremadj(sv.premadj);
		/*BRD-306 END */
		lextmjaIO.setAgerate(sv.agerate);
		lextmjaIO.setReasind(sv.reasind);
		lextmjaIO.setZnadjperc(sv.znadjperc);
		lextmjaIO.setZmortpct(sv.zmortpct);
		if (isEQ(sv.select,"L")) {
			lextmjaIO.setJlife("00");
		}
		else {
			if (isEQ(sv.select,"J")) {
				lextmjaIO.setJlife("01");
			}
			else {
				lextmjaIO.setJlife(SPACES);
			}
		}
		lextmjaIO.setExtCommDate(chdrpf.getBtdate());
		lextmjaIO.setExtCessDate(covrpf.getPremCessDate());
		lextmjaIO.setExtCessTerm(sv.extCessTerm);
		if (isNE(sv.extCessTerm,0)) {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(chdrpf.getBtdate());
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.freqFactor.set(sv.extCessTerm);
			datcon2rec.frequency.set("01");
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			else {
				lextmjaIO.setExtCessDate(datcon2rec.intDate2);
			}
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covrpf.getChdrcoy());
		itemIO.setItemtabl(t5657);
		wsaaT5657key1.set(covrpf.getCrtable());
		wsaaT5657key2.set(sv.opcda);
		itemIO.setItemitem(wsaaT5657key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5657rec.t5657Rec.set(itemIO.getGenarea());
		lextmjaIO.setSbstdl(t5657rec.sbstdl);
		lextmjaIO.setTermid(varcom.vrcmTermid);
		lextmjaIO.setTransactionDate(varcom.vrcmDate);
		lextmjaIO.setTransactionTime(varcom.vrcmTime);
		lextmjaIO.setUser(varcom.vrcmUser);
		lextmjaIO.setValidflag("1");
		setPrecision(lextmjaIO.getTranno(), 0);
		lextmjaIO.setTranno(add(1,chdrpf.getTranno()));
		/*BRD-306 START */
		lextmjaIO.setPremadj(sv.premadj);
		/*BRD-306 END */
		lextmjaIO.setFormat(lextmjarec);
		if(uwFlag){
			lextmjaIO.setUwoverwrite(sv.uwoverwrite);
		}
		lextmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lextmjaIO);
		if (isNE(lextmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lextmjaIO.getParams());
			syserrrec.statuz.set(lextmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void nextLine3580()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5131", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void lextCheck3600()
	{
		try {
			start3600();
		}
		catch (GOTOException e){
		}
	}

protected void start3600()
	{
		SmartFileCode.execute(appVars, lextchgIO);
		if (isNE(lextchgIO.getStatuz(),varcom.oK)
		&& isNE(lextchgIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextchgIO.getParams());
			syserrrec.statuz.set(lextchgIO.getStatuz());
			fatalError600();
		}
		if (isNE(lextchgIO.getChdrcoy(),covrpf.getChdrcoy())
		|| isNE(lextchgIO.getChdrnum(),covrpf.getChdrnum())
		|| isNE(lextchgIO.getLife(),covrpf.getLife())
		|| isNE(lextchgIO.getCoverage(),covrpf.getCoverage())
		|| isNE(lextchgIO.getRider(),covrpf.getRider())
		|| isNE(lextchgIO.getSeqnbr(),sv.seqnbr)
		|| isEQ(lextchgIO.getStatuz(),varcom.endp)) {
			wsaaVflag2.set("N");
			lextchgIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3690);
		}
		if (isNE(wsaaLextTranno,lextchgIO.getTranno())) {
			lextchgIO.setFunction(varcom.nextr);
		}
		else {
			wsaaVflag2.set("Y");
			lextchgIO.setStatuz(varcom.endp);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}