package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr52iscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {2, 7, 2, 53}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52iScreenVars sv = (Sr52iScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52iscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52iScreenVars screenVars = (Sr52iScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.aprem.setClassString("");
		screenVars.descript01.setClassString("");
		screenVars.descript02.setClassString("");
		screenVars.taxamt01.setClassString("");
		screenVars.taxamt02.setClassString("");
		screenVars.gndtotal.setClassString("");
		screenVars.keya.setClassString("");
	}

/**
 * Clear all the variables in Sr52iscreen
 */
	public static void clear(VarModel pv) {
		Sr52iScreenVars screenVars = (Sr52iScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.aprem.clear();
		screenVars.descript01.clear();
		screenVars.descript02.clear();
		screenVars.taxamt01.clear();
		screenVars.taxamt02.clear();
		screenVars.gndtotal.clear();
		screenVars.keya.clear();
	}
}
