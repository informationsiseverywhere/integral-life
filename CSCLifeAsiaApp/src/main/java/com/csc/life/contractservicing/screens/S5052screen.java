package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5052screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5052ScreenVars sv = (S5052ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5052screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5052ScreenVars screenVars = (S5052ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.numberOfLoans.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.surrval.setClassString("");
		screenVars.loansum.setClassString("");
		screenVars.loanallow.setClassString("");
		screenVars.loanvalue.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.zrcshamt.setClassString("");
		screenVars.tplstmdty.setClassString("");
		screenVars.netLceamt.setClassString("");
	}

/**
 * Clear all the variables in S5052screen
 */
	public static void clear(VarModel pv) {
		S5052ScreenVars screenVars = (S5052ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.numberOfLoans.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.surrval.clear();
		screenVars.loansum.clear();
		screenVars.loanallow.clear();
		screenVars.loanvalue.clear();
		screenVars.currcd.clear();
		screenVars.zrcshamt.clear();
		screenVars.tplstmdty.clear();
		screenVars.netLceamt.clear();
	}
}
