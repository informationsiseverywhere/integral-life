package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:25
 * Description:
 * Copybook name: RACTREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ractrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ractrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ractrevKey = new FixedLengthStringData(64).isAPartOf(ractrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData ractrevChdrcoy = new FixedLengthStringData(1).isAPartOf(ractrevKey, 0);
  	public FixedLengthStringData ractrevChdrnum = new FixedLengthStringData(8).isAPartOf(ractrevKey, 1);
  	public FixedLengthStringData ractrevLife = new FixedLengthStringData(2).isAPartOf(ractrevKey, 9);
  	public FixedLengthStringData ractrevCoverage = new FixedLengthStringData(2).isAPartOf(ractrevKey, 11);
  	public FixedLengthStringData ractrevRider = new FixedLengthStringData(2).isAPartOf(ractrevKey, 13);
  	public FixedLengthStringData ractrevRasnum = new FixedLengthStringData(8).isAPartOf(ractrevKey, 15);
  	public FixedLengthStringData ractrevRatype = new FixedLengthStringData(4).isAPartOf(ractrevKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(37).isAPartOf(ractrevKey, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ractrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ractrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}