package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH610
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sh610ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(637);
	public FixedLengthStringData dataFields = new FixedLengthStringData(333).isAPartOf(dataArea, 0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,89);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,90);
	public FixedLengthStringData oragntnam = DD.oragntnam.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,145);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,192);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,239);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,247);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,257);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,265);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,269);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,319);
	public FixedLengthStringData zdivopt = DD.zdivopt.copy().isAPartOf(dataFields,329);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 333);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData oragntnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData zdivoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 409);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] oragntnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] zdivoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sh610screenWritten = new LongData(0);
	public LongData Sh610protectWritten = new LongData(0);
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public boolean hasSubfile() {
		return false;
	}


	public Sh610ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payrnumOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payornameOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdivoptOut,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, rstate, pstate, cownnum, ownername, payrnum, payorname, agntnum, descrip, occdate, ptdate, billfreq, mop, zdivopt, oragntnam, reasoncd, resndesc};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, rstateOut, pstateOut, cownnumOut, ownernameOut, payrnumOut, payornameOut, agntnumOut, descripOut, occdateOut, ptdateOut, billfreqOut, mopOut, zdivoptOut, oragntnamOut, reasoncdOut, resndescOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, rstateErr, pstateErr, cownnumErr, ownernameErr, payrnumErr, payornameErr, agntnumErr, descripErr, occdateErr, ptdateErr, billfreqErr, mopErr, zdivoptErr, oragntnamErr, reasoncdErr, resndescErr};
		screenDateFields = new BaseData[] {occdate, ptdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh610screen.class;
		protectRecord = Sh610protect.class;
	}

}
