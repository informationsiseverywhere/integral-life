/*
 * File: P5150.java
 * Date: 30 August 2009 0:15:07
 * Author: Quipoz Limited
 * 
 * Class transformed from P5150.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.LinsrevTableDAM;
import com.csc.life.contractservicing.screens.S5150ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5552rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*    BILLING  REVERSAL
*
*    This subsystem handles the reversal and suppression of
*    billing on a selected contract. This transaction screen
*    (S5150) displays details of the current billing details
*    relevant to the contract and allows input of a new billed
*    to date and billing suppression date.
*
*    The actual processing is carried out by the generic
*    subroutine REVBILL which cancels outstanding instalments
*    and deletes media records which had been produced for the
*    previous billed to period. The subroutine also updates
*    the billing details on the contract/ The transaction is
*    batched and a policy transaction record is written to
*    provide an audit trail.
*
*    SCREEN S5150.
*
*    This screen shows current details from the contract header
*    when starting a transaction to reverse the current Billing
*    date of a contract.
*
*    The New Billed-to Date is the date that is to become the
*    new Billed-to date of the contract. This must be less than
*    the current billed-to date and not less than the Paid-to
*    Date.
*
*    Only contracts whose risk and premium status satisfy the
*    check against table T5679 for the transaction type are
*    allowed to have this transaction performed upon them.
*
*    When the screen first appears the current outstanding
*    details appear.
*
*    In addition to reversing the billed to date future billing
*    may also be suppressed to allow time for subsequent
*    investigation or transactions to be done.
*
*****************************************************************
* </pre>
*/
public class P5150 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5150");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaItemItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTransCode = new FixedLengthStringData(4).isAPartOf(wsaaItemItem, 0);
	private FixedLengthStringData wsaaContractType = new FixedLengthStringData(3).isAPartOf(wsaaItemItem, 4);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(193);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 18);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaTransactionRec, 20);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 22);
	private ZonedDecimalData wsaaEffdate1 = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 25).setUnsigned();
	private ZonedDecimalData wsaaEffdate2 = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 33).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaTransactionRec, 41);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaTransactionRec, 44);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 47);
	private FixedLengthStringData filler2 = new FixedLengthStringData(145).isAPartOf(wsaaTransactionRec, 48, FILLER).init(SPACES);
		/* ERRORS */
	private String e186 = "E186";
	private String e304 = "E304";
	private String e315 = "E315";
	private String f133 = "F133";
	private String f515 = "F515";
	private String g084 = "G084";
	private String g087 = "G087";
	private String g570 = "G570";
	private String h003 = "H003";
	private String h035 = "H035";
		/* TABLES */
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5552 = "T5552";
	private String t5688 = "T5688";

	private FixedLengthStringData wsaaKill = new FixedLengthStringData(1).init(SPACES);
	private Validator firstTime = new Validator(wsaaKill, "N");
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract Header Life Fields*/
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life instalments billing reversal view*/
	private LinsrevTableDAM linsrevIO = new LinsrevTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5552rec t5552rec = new T5552rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5150ScreenVars sv = ScreenProgram.getScreenVars( S5150ScreenVars.class);
	//ILIFE-8554
	private Chdrpf chdrpf=new Chdrpf();
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    //ILJ-49 Starts
    private boolean cntDteFlag = false;
    private String cntDteFeature = "NBPRP113";
    //ILJ-49 End 
    private boolean supBill = false;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1290, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		linsrevLoop2030, 
		t5552Check2040, 
		t5552Loop2040, 
		checkForErrors2080, 
		exit2090, 
		exit3090
	}

	public P5150() {
		super();
		screenVars = sv;
		new ScreenModel("S5150", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		sv.dataArea.set(SPACES);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.todate.set(varcom.vrcmMaxDate);
		sv.cntinst.set(ZERO);
		sv.osbal.set(ZERO);
		sv.suppressTo.set(varcom.vrcmMaxDate);
		supBill = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSOTH018", appVars, "IT");
		if(supBill)
		{
			sv.supflagOut[varcom.pr.toInt()].set("Y");
			sv.supflag.set("N");
		}
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
		//ILJ-49 End
		//ILIFE-8554 starts
		chdrpf =chdrpfDAO.getCacheObject(chdrpf);	
		if (null==chdrpf) {
			fatalError600();
		}
		sv.occdate.set(chdrpf.getOccdate());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.agntnum.set(chdrpf.getAgntnum());
		sv.cnttype.set(chdrpf.getCnttype());
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1400();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		if (isNE(chdrpf.getPayrnum(),SPACES)) {
			sv.payrnum.set(chdrpf.getPayrnum());
			cltsIO.setClntnum(chdrpf.getPayrnum());
			getClientDetails1400();
			if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
			|| isNE(cltsIO.getValidflag(),1)) {
				sv.payornameErr.set(e304);
				sv.payorname.set(SPACES);
			}
			else {
				plainname();
				sv.payorname.set(wsspcomn.longconfname);
			}
		}
		if (isNE(sv.agntnum,SPACES)) {
			getAgentName1200();
		}
		sv.btdate.set(chdrpf.getBtdate());
		sv.cntinst.set(chdrpf.getSinstamt06());
		sv.osbal.set(chdrpf.getOutstamt());
		sv.ptdate.set(chdrpf.getPtdate());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.mop.set(chdrpf.getBillchnl());
		sv.billfreq.set(chdrpf.getBillfreq());
	}

protected void getAgentName1200()
	{
		try {
			read1210();
		}
		catch (GOTOException e){
		}
	}

protected void read1210()
	{
		if (isEQ(sv.agntnum,SPACES)) {
			sv.agntnumErr.set(e186);
			goTo(GotoLabel.exit1290);
		}
		agntIO.setDataKey(SPACES);
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(sv.agntnum);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		sv.agentname.set(SPACES);
		if (isEQ(agntIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit1290);
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(agntIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1400()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case linsrevLoop2030: {
					linsrevLoop2030();
				}
				case t5552Check2040: {
					t5552Check2040();
				}
				case t5552Loop2040: {
					t5552Loop2040();
					billingSuppress2050();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		if (isGT(sv.todate,chdrpf.getBtdate())) {
			sv.todateErr.set(f133);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isLT(sv.todate,sv.occdate)) {
			sv.todateErr.set(f515);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.todate,chdrpf.getBtdate())) {
			sv.todateErr.set(f133);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.todate,sv.ptdate)) {
			goTo(GotoLabel.t5552Check2040);
		}
		linsrevIO.setChdrcoy(chdrpf.getChdrcoy());
		linsrevIO.setChdrnum(chdrpf.getChdrnum());
		linsrevIO.setInstto(ZERO);
		linsrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		linsrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		linsrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void linsrevLoop2030()
	{
		SmartFileCode.execute(appVars, linsrevIO);
		if (isNE(chdrpf.getChdrcoy(),linsrevIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),linsrevIO.getChdrnum())) {
			linsrevIO.setStatuz(varcom.endp);
		}
		if (isNE(linsrevIO.getStatuz(),varcom.oK)
		&& isNE(linsrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(linsrevIO.getParams());
			fatalError600();
		}
		if (isEQ(linsrevIO.getStatuz(),varcom.endp)) {
			sv.todateErr.set(h003);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		linsrevIO.setFunction(varcom.nextr);
		if (isEQ(linsrevIO.getInstto(),sv.todate)) {
			goTo(GotoLabel.t5552Check2040);
		}
		else {
			goTo(GotoLabel.linsrevLoop2030);
		}
	}

protected void t5552Check2040()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaTransCode.set(wsaaBatckey.batcBatctrcde);
		wsaaContractType.set(chdrpf.getCnttype());
	}

protected void t5552Loop2040()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemitem(wsaaItemItem);
		itemIO.setItemtabl(t5552);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		&& isNE(wsaaContractType,"***")) {
			wsaaContractType.set("***");
			goTo(GotoLabel.t5552Loop2040);
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.todateErr.set(g084);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		t5552rec.t5552Rec.set(itemIO.getGenarea());
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(chdrpf.getBtdate());
		datcon3rec.intDate2.set(sv.btdate);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isGT(datcon3rec.freqFactor,t5552rec.ptmthsbf)) {
			sv.todateErr.set(g087);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void billingSuppress2050()
	{
		if (isNE(sv.supflag,"Y")
		&& isNE(sv.supflag,"N")) {
			sv.supflagErr.set(e315);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.supflag,"N")
		&& isNE(sv.suppressTo,99999999)) {
			sv.supptoErr.set(g570);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.supflag,"N")) {
			goTo(GotoLabel.checkForErrors2080);
		}
		if ((isNE(sv.suppressTo,varcom.vrcmMaxDate))
		&& (isNE(sv.suppressTo,ZERO))) {
			if (isLT(sv.suppressTo,sv.todate)) {
				sv.supptoErr.set(h035);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.checkForErrors2080);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5150AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaLife.set(wssplife.life);
		wsaaCoverage.set(wssplife.coverage);
		wsaaRider.set(wssplife.rider);
		wsaaPlanSuffix.set(0);
		wsaaEffdate1.set(sv.todate);
		wsaaEffdate2.set(sv.suppressTo);
		wsaaTranno.set(chdrpf.getTranno());
		wsaaNewTranno.set(chdrpf.getTranno());
		//ILIFE-8554 ends
		wsaaFsuco.set(wsspcomn.fsuco);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
