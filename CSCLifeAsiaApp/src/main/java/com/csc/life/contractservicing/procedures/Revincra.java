/*
 * File: Revincra.java
 * Date: 30 August 2009 2:08:28
 * Author: Quipoz Limited
 * 
 * Class transformed from REVINCRA.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrccTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrrccTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE COMPONENT ADD FOR AUTOMATIC INCREASE
*        --------------------------------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse the results of the Compoment
*   Add process at component level.
*
*   It  will  reverse all  Non-cash Accounting  records (ACMV),
*   Agent  Commission records  (AGCM), Coverage / Rider records
*   (COVR)  and  Individual  Coverage / Rider Increase  records
*   (INCI)  which  were  created/amended  at  the  time  of the
*   forward transaction.
*
* Processing.
* -----------
*
* Reverse Coverage & Rider records ( COVRs ):
*
*    When a component is ADDed to a contract, only the New
*     component COVR record is updated with the tranno
*     for the 'ADD' transaction.
*
*    Delete the new component COVR record.
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*     Set the accounting month/year with the batch
*     accounting month/year from linkage.
*     Set the tranno with the NEW tranno from linkage.
*     Set the batch key with new batch key from linkage.
*     Multiply the original currency amount by -1.
*     Multiply the accounting currency amount by -1.
*     Set the transaction reference with the NEW
*     transaction code from linkage.
*     Set the transaction description with the long
*     description of the new trans code from T1688.
*     Set the effective date with todays date.
*     Write the new record to the database by calling the
*     subroutine 'LIFACMV'.
*
* Reverse Agent Commission records ( AGCMs ):
*
*  AGCM record processing will be done inside the COVR
*   processing loop - we only need to process AGCM records
*   associated with the Component(s) we are removing.
*
*  Read the Agent Commission file (AGCMPF) using the logical
*  view AGCMREV and DELETE all AGCMs that match the
*  component(s) we are trying to reverse.
*
* Reversal of Unit-Linked/Traditional parts of the Contract:
*
*  Perform all generic processing inside the COVR processing
*  loop above - we want to do the generic processing BEFORE
*  we delete the COVR records above.
*  Loop through all the Coverages/Riders associated with the
*   Contract we are processing and call any Generic processing
*   subroutines that are specified in T5671.
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing
* T5687 - Coverage/Rider details
* T6658 - Anniversary/Auto Increase Processing Method
*
*****************************************************************
* </pre>
*/
public class Revincra extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVINCRA";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private static final String t5687 = "T5687";
	private static final String t6658 = "T6658";
		/* FORMATS */
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String agcmrccrec = "AGCMRCCREC";
	private static final String covrrec = "COVRREC   ";
	private static final String covrrccrec = "COVRRCCREC";
	private static final String covrenqrec = "COVRENQREC";
	private static final String itemrec = "ITEMREC   ";
	private static final String itdmrec = "ITEMREC   ";
	private static final String incrselrec = "INCRSELREC";
	private static final String arcmrec = "ARCMREC";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmrccTableDAM agcmrccIO = new AgcmrccTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrrccTableDAM covrrccIO = new CovrrccTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private T6658rec t6658rec = new T6658rec();
	private Reverserec reverserec = new Reverserec();

	public Revincra() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processMainCovrs5000();
		processNewCovrs3000();
		processAcmvs4000();
		processAcmvOptical4010();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError99000();
		}
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void processNewCovrs3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* This section deals with processing the 'New' COVR record*/
		/*  ie. The one that has been 'Added' by the Automatic Increase*/
		/*      processing. The COVR record that has been 'Added' will*/
		/*      NOT have INCR records associated with it ( only the*/
		/*      original components on the contract are directly*/
		/*      associated to INCR records*/
		/*      We will have to use the INCR records to decide whether*/
		/*      the COVR we are processing is a 'New' component or not.*/
		/* Use COVR logical view to loop around coverage/rider records*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			reverseCovrs3100();
		}
		
	}

protected void reverseCovrs3100()
	{
		start3100();
		next3150();
	}

protected void start3100()
	{
		/* check whether the COVR has an associated INCR record - if not,*/
		/*   then we will process it - existing COVRs that don't qualify*/
		/*   for increases will obviously not have INCR records either*/
		/*   but these should not be selected because of the TRANNO check*/
		/*   we do later in this section.*/
		incrselIO.setParams(SPACES);
		incrselIO.setChdrcoy(covrIO.getChdrcoy());
		incrselIO.setChdrnum(covrIO.getChdrnum());
		incrselIO.setLife(covrIO.getLife());
		incrselIO.setCoverage(covrIO.getCoverage());
		incrselIO.setRider(covrIO.getRider());
		incrselIO.setPlanSuffix(covrIO.getPlanSuffix());
		incrselIO.setTranno(9999);
		incrselIO.setFormat(incrselrec);
		incrselIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incrselIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrselIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)
		&& isNE(incrselIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(incrselIO.getParams());
			syserrrec.statuz.set(incrselIO.getStatuz());
			systemError99000();
		}
		if (isEQ(incrselIO.getStatuz(), varcom.oK)) {
			if (isEQ(incrselIO.getChdrcoy(), covrIO.getChdrcoy())
			&& isEQ(incrselIO.getChdrnum(), covrIO.getChdrnum())
			&& isEQ(incrselIO.getLife(), covrIO.getLife())
			&& isEQ(incrselIO.getCoverage(), covrIO.getCoverage())
			&& isEQ(incrselIO.getRider(), covrIO.getRider())
			&& isEQ(incrselIO.getPlanSuffix(), covrIO.getPlanSuffix())) {
				return ;
			}
		}
		/* check TRANNO of COVR just read*/
		/* Read T5687 to get the Anniversary Processing method             */
		/* Then using that read T6658 to find if the Commission            */
		/* indicator was set for commission. If commission was not         */
		/* applied then there is no need to process the AGCM recs now.     */
		if (isEQ(reverserec.tranno, covrIO.getTranno())
		&& isEQ(reverserec.planSuffix, covrIO.getPlanSuffix())) {
			genericProcessing9000();
			deleteCovrs3200();
			readT568710300();
			readT665810400();
			if (isNE(t6658rec.comind, SPACES)) {
				processAgcms10000();
			}
		}
	}

protected void next3150()
	{
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void deleteCovrs3200()
	{
		start3200();
	}

protected void start3200()
	{
		covrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		covrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
	}

protected void processAcmvs4000()
	{
		start4000();
	}

protected void start4000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
	}

protected void processAcmvOptical4010()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError99500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError99500();
			}
		}
	}

protected void reverseAcmvs4100()
	{
		start4100();
		readNextAcmv4180();
	}

protected void start4100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void readNextAcmv4180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processMainCovrs5000()
	{
		start5000();
	}

protected void start5000()
	{
		/* Use COVR logical view to loop around coverage/rider records*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setLife(reverserec.life);
		covrIO.setCoverage(reverserec.coverage);
		covrIO.setRider(reverserec.rider);
		covrIO.setPlanSuffix(reverserec.planSuffix);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(covrIO.getLife(), reverserec.life)
		|| isNE(covrIO.getCoverage(), reverserec.coverage)
		|| isNE(covrIO.getRider(), reverserec.rider)
		|| isNE(covrIO.getPlanSuffix(), reverserec.planSuffix)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			reverseCovrs5100();
		}
		
	}

protected void reverseCovrs5100()
	{
		start5100();
	}

protected void start5100()
	{
		/* check TRANNO of COVR just read*/
		if (isEQ(reverserec.tranno, covrIO.getTranno())) {
			covrrccIO.setParams(SPACES);
			covrrccIO.setChdrcoy(covrIO.getChdrcoy());
			covrrccIO.setChdrnum(covrIO.getChdrnum());
			covrrccIO.setLife(covrIO.getLife());
			covrrccIO.setCoverage(covrIO.getCoverage());
			covrrccIO.setRider(covrIO.getRider());
			covrrccIO.setPlanSuffix(covrIO.getPlanSuffix());
			/*     MOVE BEGN               TO COVRRCC-FUNCTION              */
			covrrccIO.setFunction(varcom.readh);
			covrrccIO.setTranno(reverserec.tranno);
			covrrccIO.setFormat(covrrccrec);
			/*     PERFORM 5200-DELETE-COVRS UNTIL COVRRCC-STATUZ = ENDP    */
			deleteCovrsR5200();
		}
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(covrIO.getLife(), reverserec.life)
		|| isNE(covrIO.getCoverage(), reverserec.coverage)
		|| isNE(covrIO.getRider(), reverserec.rider)
		|| isNE(covrIO.getPlanSuffix(), reverserec.planSuffix)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
	}

protected void deleteCovrsR5200()
	{
		/*START*/
		covrrccIo5300();
		covrrccIO.setFunction(varcom.delet);
		covrrccIo5300();
		covrrccIO.setFunction(varcom.endh);
		covrrccIo5300();
		covrrccIO.setFunction(varcom.rewrt);
		covrrccIO.setValidflag("1");
		covrrccIO.setCurrto(99999999);
		covrrccIo5300();
		/*EXIT*/
	}

	/**
	* <pre>
	*5200-DELETE-COVRS SECTION.                                       
	*5200-START.                                                      
	**** CALL 'COVRRCCIO'            USING COVRRCC-PARAMS.            
	**** IF COVRRCC-STATUZ           NOT = O-K                        
	****    AND COVRRCC-STATUZ       NOT = ENDP                       
	****     MOVE COVRRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE COVRRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** IF COVR-CHDRCOY             NOT = COVRRCC-CHDRCOY            
	****    OR COVR-CHDRNUM          NOT = COVRRCC-CHDRNUM            
	****    OR COVR-LIFE             NOT = COVRRCC-LIFE               
	****    OR COVR-COVERAGE         NOT = COVRRCC-COVERAGE           
	****    OR COVR-RIDER            NOT = COVRRCC-RIDER              
	****    OR COVR-PLAN-SUFFIX      NOT = COVRRCC-PLAN-SUFFIX        
	****    OR COVRRCC-STATUZ        = ENDP                           
	****     MOVE ENDP               TO COVRRCC-STATUZ                
	****     GO TO 5290-EXIT                                          
	**** END-IF.                                                      
	**** IF COVRRCC-TRANNO           NOT < REVE-TRANNO                
	****     MOVE READH                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE DELET                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE NEXTR                  TO COVRRCC-FUNCTION          
	**   ELSE                                                         
	****     MOVE READH                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE '1'                    TO COVRRCC-VALIDFLAG         
	****     MOVE 99999999               TO COVRRCC-CURRTO            
	****     MOVE REWRT                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE ENDP                   TO COVRRCC-STATUZ            
	**** END-IF.                                                      
	*5290-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
protected void covrrccIo5300()
	{
		/*START*/
		SmartFileCode.execute(appVars, covrrccIO);
		if (isNE(covrrccIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrccIO.getParams());
			syserrrec.statuz.set(covrrccIO.getStatuz());
			systemError99000();
		}
		/*EXIT*/
	}

protected void genericProcessing9000()
	{
		/*START*/
		/* We will do generic processing that is required for the*/
		/*  Coverage/Rider that we are currently working with.*/
		/*  all generic processing is driven by calling subroutine(s)*/
		/*  specified on table T5671*/
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setLife(covrIO.getLife());
		covrenqIO.setCoverage(covrIO.getCoverage());
		covrenqIO.setRider(covrIO.getRider());
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs9100();
		}
		
		/*EXIT*/
	}

protected void processCovrs9100()
	{
		start9100();
	}

protected void start9100()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(), covrenqIO.getLife())
		|| isNE(covrIO.getCoverage(), covrenqIO.getCoverage())
		|| isNE(covrIO.getRider(), covrenqIO.getRider())
		|| isNE(covrIO.getPlanSuffix(), covrenqIO.getPlanSuffix())) {
			covrenqIO.setFunction(varcom.nextr);
			return ;
		}
		genericSubr9200();
		covrenqIO.setFunction(varcom.nextr);
	}

protected void genericSubr9200()
	{
		start9200();
	}

protected void start9200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(ZERO);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs9300();
		}
	}

protected void callTrevsubs9300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError99000();
			}
		}
		/*EXIT*/
	}

protected void processAgcms10000()
	{
		/*START*/
		agcmrccIO.setParams(SPACES);
		agcmrccIO.setChdrcoy(covrIO.getChdrcoy());
		agcmrccIO.setChdrnum(covrIO.getChdrnum());
		agcmrccIO.setLife(covrIO.getLife());
		agcmrccIO.setCoverage(covrIO.getCoverage());
		agcmrccIO.setRider(covrIO.getRider());
		agcmrccIO.setPlanSuffix(ZERO);
		agcmrccIO.setFormat(agcmrccrec);
		agcmrccIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrccIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmrccIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(agcmrccIO.getStatuz(),varcom.endp))) {
			reverseAgcms10100();
		}
		
		/*EXIT*/
	}

protected void reverseAgcms10100()
	{
		start10100();
	}

protected void start10100()
	{
		SmartFileCode.execute(appVars, agcmrccIO);
		if (isNE(agcmrccIO.getStatuz(), varcom.oK)
		&& isNE(agcmrccIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrccIO.getParams());
			syserrrec.statuz.set(agcmrccIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), agcmrccIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), agcmrccIO.getChdrnum())
		|| isEQ(agcmrccIO.getStatuz(), varcom.endp)) {
			agcmrccIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(covrIO.getChdrcoy(), agcmrccIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), agcmrccIO.getChdrnum())
		|| isNE(covrIO.getLife(), agcmrccIO.getLife())
		|| isNE(covrIO.getCoverage(), agcmrccIO.getCoverage())
		|| isNE(covrIO.getRider(), agcmrccIO.getRider())
		|| isNE(covrIO.getPlanSuffix(), agcmrccIO.getPlanSuffix())) {
			agcmrccIO.setFunction(varcom.nextr);
			return ;
		}
		agcmrccIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmrccIO);
		if (isNE(agcmrccIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrccIO.getParams());
			syserrrec.statuz.set(agcmrccIO.getStatuz());
			systemError99000();
		}
		agcmrccIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmrccIO);
		if (isNE(agcmrccIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrccIO.getParams());
			syserrrec.statuz.set(agcmrccIO.getStatuz());
			systemError99000();
		}
		agcmrccIO.setFunction(varcom.nextr);
	}

protected void readT568710300()
	{
		start10300();
	}

protected void start10300()
	{
		/* Read T5687 to find the anniversary method for this policy       */
		/* which we need to read t6658                                     */
		itdmIO.setParams(SPACES);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(covrIO.getChdrcoy());
		itdmIO.setItempfx("IT");
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		if (isNE(itdmIO.getItemcoy(), covrIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(covrIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT665810400()
	{
		start10400();
	}

protected void start10400()
	{
		/* Read T6658 to find if commission was applicable for this        */
		/* policy.                                                         */
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(covrIO.getChdrcoy());
		itdmIO.setItempfx("IT");
		itdmIO.setItemtabl(t6658);
		itdmIO.setItemitem(t5687rec.anniversaryMethod);
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		if (isNE(itdmIO.getItemcoy(), covrIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6658)
		|| isNE(itdmIO.getItemitem(), t5687rec.anniversaryMethod)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		else {
			t6658rec.t6658Rec.set(itdmIO.getGenarea());
		}
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError99500()
	{
		start99500();
		exit99990();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
