package com.csc.life.contractservicing.dataaccess.dao;

import com.csc.life.contractservicing.dataaccess.model.Overpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface OverpfDAO extends BaseDAO<Overpf> {
	
	public int insertOverpf(Overpf overpf);
	
	public Overpf getOverpfRecord(String chdrcoy, String chdrnum, String validflag);
	
	public void updateOverpfRecord(int enddate, long unique_number);
	
}

