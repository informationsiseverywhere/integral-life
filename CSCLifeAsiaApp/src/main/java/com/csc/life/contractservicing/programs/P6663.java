/*
 * File: P6663.java
 * Date: 30 August 2009 0:49:01
 * Author: Quipoz Limited
 * 
 * Class transformed from P6663.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.SoinTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.screens.S6663ScreenVars;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* PAYER DETAILS (BILLING CHANGE)
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Overview
* ~~~~~~~~
* One of the facilities provided by the billing change
* subsystem is to allow changing Contract Payer details.
* The function of this program is to support this facility
* and be able to display the existing Payer details (if
* there are any) and capture any modified/new details.
* Standard windowing facility is available for the Payer number.
*
* INITIALISE
*~~~~~~~~~~~~
* Contract header details are retrieved from contract header file
* (CHDRMJA) using RETRV.
* If PAYRNUM is blank, COWNNUM is used.
*
* Payer name and address are retrieved from the Client Details
* file (CLTS). The name is formatted for display using
* PLAINNAME routine.
*
* VALIDATION
*~~~~~~~~~~~~
* If scrn-statuz of CALC is pressed then the screen is
* re-displayed.
*
* If scrn-statuz of KILL is pressed then all validation
* is ignored and program is exited.
*
* If the Payer is changed (may be blank), then validate it by
* reading the CLTS file. If there is no entry on the client
* details file, output an error message, otherwise output the
* new name and address details for display.
*
* UPDATE
*~~~~~~~~
* If KILL is pressed, then skip the updating and go to the exit
* paragraph. Also skip the updating if nothing has been changed.
*
* Update the client roles as follows :-
*
*    - If the payer has changed (blank is also valid),
*       then pass the details of the old payer to
*       CLTRELN, with a function of 'REM' to remove their
*       role as a payer for this contract.
*
*    - Pass the details of the new payer to CLTRELN
*       with a function of 'ADD' to add their role of
*       payer for this contract.
*
* Update the contract header as follows:-
*
*    - if the payer has changed (blank is also valid),
*      then set the PAYRNUM to this new value.
*
*    - update the CHDR record using the KEEPS function.
*
*  Note - you cannot simply compare screen to Payer, because
*         if the Payer was blank the screen will hold COWNNUM.
*
* NEXT PROGRAM
*~~~~~~~~~~~~~~
*
* Add 1 to the program pointer and exit from the program.
*
*****************************************************************
* </pre>
*/
public class P6663 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6663");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-MISC */
	private FixedLengthStringData wsaaPayrnumSave = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private static final String i068 = "I068";
	private static final String i074 = "I074";
	private static final String t6697 = "T6697";
		/* FORMATS */
	private static final String cltsrec = "CLTSREC";
	private static final String clrfrec = "CLRFREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client Roles by Foreign Key*/
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Clients - Source of Income Details Logic*/
	private SoinTableDAM soinIO = new SoinTableDAM();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Wssplife wssplife = new Wssplife();
	private S6663ScreenVars sv = ScreenProgram.getScreenVars( S6663ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090, 
		updatePayr3060
	}

	public P6663() {
		super();
		screenVars = sv;
		new ScreenModel("S6663", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.incomeSeqNo.set(ZERO);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.incomeSeqNo.set(payrIO.getIncomeSeqNo());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6697);
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.incseqnoOut[varcom.nd.toInt()].set("Y");
			sv.incseqnoOut[varcom.pr.toInt()].set("Y");
			sv.incomeSeqNo.set(ZERO);
		}
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(clrfIO.getClntnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.oK)) {
			sv.payraddr1.set(cltsIO.getCltaddr01());
			sv.payraddr2.set(cltsIO.getCltaddr02());
			sv.payraddr3.set(cltsIO.getCltaddr03());
			sv.payraddr4.set(cltsIO.getCltaddr04());
			sv.payraddr5.set(cltsIO.getCltaddr05());
			sv.cltpcode.set(cltsIO.getCltpcode());
		}
		sv.payrnum.set(clrfIO.getClntnum());
		wsaaPayrnumSave.set(sv.payrnum);
		plainname();
		sv.payrname.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		if (isNE(sv.incomeSeqNo,ZERO)) {
			soinIO.setDataKey(SPACES);
			soinIO.setClntcoy(wsspcomn.fsuco);
			soinIO.setClntnum(sv.payrnum);
			soinIO.setIncomeSeqNo(sv.incomeSeqNo);
			soinIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, soinIO);
			if (isNE(soinIO.getStatuz(),varcom.oK)
			&& isNE(soinIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(soinIO.getParams());
				fatalError600();
			}
			if (isEQ(soinIO.getStatuz(),varcom.mrnf)) {
				sv.incseqnoErr.set(i068);
			}
		}
		if (isNE(sv.payrnum,wsaaPayrnumSave)) {
			cltsIO.setParams(SPACES);
			cltsIO.setClntnum(sv.payrnum);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setFunction(varcom.readr);
			cltsIO.setFormat(cltsrec);
			SmartFileCode.execute(appVars, cltsIO);
			if ((isNE(cltsIO.getStatuz(),varcom.oK))
			&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
				sv.payrnumErr.set(i074);
				goTo(GotoLabel.checkForErrors2080);
			}
			else {
				sv.payraddr1.set(cltsIO.getCltaddr01());
				sv.payraddr2.set(cltsIO.getCltaddr02());
				sv.payraddr3.set(cltsIO.getCltaddr03());
				sv.payraddr4.set(cltsIO.getCltaddr04());
				sv.payraddr5.set(cltsIO.getCltaddr05());
				sv.cltpcode.set(cltsIO.getCltpcode());
				sv.payrnum.set(cltsIO.getClntnum());
				plainname();
				sv.payrname.set(wsspcomn.longconfname);
			}
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					bypassUpdate3005();
					removeClientRole3010();
					addClntRole3030();
					callBldenrl3040();
					updateContactHeader3050();
				case updatePayr3060: 
					updatePayr3060();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void bypassUpdate3005()
	{
		if ((isEQ(scrnparams.statuz,varcom.kill))
		|| (isEQ(sv.payrnum,wsaaPayrnumSave))) {
			goTo(GotoLabel.updatePayr3060);
		}
	}

protected void removeClientRole3010()
	{
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(clrfIO.getClntnum());
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(wsspcomn.company);
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		cltrelnrec.forenum.set(wsaaForenum);
		cltrelnrec.clrrrole.set("PY");
		cltrelnrec.function.set("DEL  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz,varcom.oK)) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			fatalError600();
		}
	}

protected void addClntRole3030()
	{
		cltrelnrec.cltrelnRec.set(SPACES);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.payrnum);
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(wsspcomn.company);
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		cltrelnrec.forenum.set(wsaaForenum);
		cltrelnrec.clrrrole.set("PY");
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz,varcom.oK)) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			fatalError600();
		}
	}

protected void callBldenrl3040()
	{
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrmjaIO.getChdrpfx());
		bldenrlrec.company.set(chdrmjaIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrmjaIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*  Update the Contract Header with the new Payor details
	* </pre>
	*/
protected void updateContactHeader3050()
	{
		if ((isNE(sv.payrnum,wsaaPayrnumSave))
		&& (isNE(sv.payrnum,SPACES))) {
			chdrmjaIO.setPayrnum(sv.payrnum);
			chdrmjaIO.setPayrcoy(cltsIO.getClntcoy());
			chdrmjaIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if ((isNE(chdrmjaIO.getStatuz(),varcom.oK))
			&& (isNE(chdrmjaIO.getStatuz(),varcom.mrnf))) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
		}
	}

protected void updatePayr3060()
	{
		if (isNE(sv.incomeSeqNo,payrIO.getIncomeSeqNo())) {
			payrIO.setIncomeSeqNo(sv.incomeSeqNo);
			payrIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, payrIO);
			if ((isNE(payrIO.getStatuz(),varcom.oK))
			&& (isNE(payrIO.getStatuz(),varcom.mrnf))) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
		}
		if ((isNE(sv.payrnum, wsaaPayrnumSave))
		&& (isNE(sv.payrnum, SPACES))) {
			payrIO.setMandref(SPACES);
			payrIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, payrIO);
			if ((isNE(payrIO.getStatuz(), varcom.oK))
			&& (isNE(payrIO.getStatuz(), varcom.mrnf))) {
				syserrrec.params.set(payrIO.getParams());
				syserrrec.statuz.set(payrIO.getStatuz());
				fatalError600();
			}
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
