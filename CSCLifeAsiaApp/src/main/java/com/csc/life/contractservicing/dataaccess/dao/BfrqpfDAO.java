/******************************************************************************
 * File Name 		: BfrqpfDAO.java
 * Author			: smalchi2
 * Creation Date	: 04 January 2017
 * Project			: Integral Life
 * Description		: The DAO Interface for BFRQPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.contractservicing.dataaccess.model.Bfrqpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface BfrqpfDAO extends BaseDAO<Bfrqpf> {

	public List<Bfrqpf> searchBfrqpfRecord(Bfrqpf bfrqpf) throws SQLRuntimeException;

	void updateBfrqpfInvalid(List<Bfrqpf> bfrqpfList);
	
	List<Bfrqpf> getAll(int batchExtractSize, int batchID );

}
