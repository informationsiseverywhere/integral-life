package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr52escreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52eScreenVars sv = (Sr52eScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52escreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52eScreenVars screenVars = (Sr52eScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.taxind05.setClassString("");
		screenVars.taxind01.setClassString("");
		screenVars.taxind02.setClassString("");
		screenVars.taxind03.setClassString("");
		screenVars.taxind04.setClassString("");
		screenVars.taxind06.setClassString("");
		screenVars.taxind08.setClassString("");
		screenVars.taxind10.setClassString("");
		screenVars.taxind07.setClassString("");
		screenVars.taxind09.setClassString("");
		screenVars.taxind11.setClassString("");
		screenVars.txitem.setClassString("");
		screenVars.zbastyp.setClassString("");
		screenVars.taxind12.setClassString("");
		screenVars.taxind13.setClassString("");
	}

/**
 * Clear all the variables in Sr52escreen
 */
	public static void clear(VarModel pv) {
		Sr52eScreenVars screenVars = (Sr52eScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.taxind05.clear();
		screenVars.taxind01.clear();
		screenVars.taxind02.clear();
		screenVars.taxind03.clear();
		screenVars.taxind04.clear();
		screenVars.taxind06.clear();
		screenVars.taxind08.clear();
		screenVars.taxind10.clear();
		screenVars.taxind07.clear();
		screenVars.taxind09.clear();
		screenVars.taxind11.clear();
		screenVars.txitem.clear();
		screenVars.zbastyp.clear();
		screenVars.taxind12.clear();
		screenVars.taxind13.clear();
	}
}
