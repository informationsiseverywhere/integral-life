package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:08
 * Description:
 * Copybook name: LIFEREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Liferevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData liferevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData liferevKey = new FixedLengthStringData(256).isAPartOf(liferevFileKey, 0, REDEFINE);
  	public FixedLengthStringData liferevChdrcoy = new FixedLengthStringData(1).isAPartOf(liferevKey, 0);
  	public FixedLengthStringData liferevChdrnum = new FixedLengthStringData(8).isAPartOf(liferevKey, 1);
  	public PackedDecimalData liferevTranno = new PackedDecimalData(5, 0).isAPartOf(liferevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(liferevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(liferevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		liferevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}