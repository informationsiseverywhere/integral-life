/*
 * File: Revcompm.java
 * Date: 30 August 2009 2:06:40
 * Author: Quipoz Limited
 * 
 * Class transformed from REVCOMPM.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmbcrTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrrccTableDAM;
import com.csc.life.contractservicing.dataaccess.LextchgTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.RactrccTableDAM;
import com.csc.life.contractservicing.dataaccess.RactrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcorevTableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv2TableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmrevTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.LirrconTableDAM;
import com.csc.life.reassurance.dataaccess.LirrrrvTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhconTableDAM;
import com.csc.life.reassurance.dataaccess.LrrhrrvTableDAM;
import com.csc.life.reassurance.dataaccess.RacdTableDAM;
import com.csc.life.reassurance.dataaccess.RacdrrvTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE COMPONENT MODIFY.
*        -------------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse the results of the Component
*   Add process. It will reverse all Non-cash Accounting records
*   (ACMV), Reassurance records (RACT), Contract Header (CHDR),
*   Payer records (PAYR), Life Options and Extras records (LEXT),
*   Coverage/Rider records (COVR), Individual Coverage/Rider
*   Increase records (INCI) and Agent Commission (AGCM) records
*   which were created/amended at the time of the forward
*   transaction.
*
*
* Processing.
* -----------
*
* Reverse Contract Header ( CHDRs ):
*
*  Read the Contract Header file (CHDRPF) using the logical
*  view CHDRMJA with a key of company and contract number
*  from linkage.
*
*  For the record found, check it is validflag '1'.
*    If not, then ERROR.
*  Delete validflag '1' CHDR record.
*  Read Next CHDR record which is validflag '2'.
*  Reset validflag to '1'.
*  REWRiTe CHDR record.
*
* Reverse Payer ( PAYRs ):
*
*  Read the Payer file (PAYRPF) using the logical view
*  PAYRLIF with a key of company and contract number
*  from linkage.
*
*  For the record found, check it is validflag '1'.
*    If not, then ERROR.
*  Delete validflag '1' PAYR record.
*  Read Next PAYR record which is validflag '2'.
*  Reset validflag to '1'.
*  REWRiTe PAYR record.
*
* Reverse Coverage & Rider records ( COVRs ):
*
*    When a component is Modified on a contract, only the changed
*     component COVR record is updated with the tranno
*     for the 'MODIFY' transaction.
*
*     Delete the COVR whose transaction number (TRANNO) is equal
*     to the tranno on the PTRN we are trying to reverse.
*     i.e. match on company, contract number, life, coverage and
*     rider.
*     Then reinstate the most recent validflag '2' COVR record
*     (ie. the one prior to the Component Modify) to be a
*     validflag '1' COVR.
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*   - Set the accounting month/year with the batch
*   - accounting month/year from linkage.
*   - Set the tranno with the NEW tranno from linkage.
*   - Set the batch key with new batch key from linkage.
*   - Multiply the original currency amount by -1.
*   - Multiply the accounting currency amount by -1.
*   - Set the transaction reference with the NEW
*   - transaction code from linkage.
*   - Set the transaction description with the long
*   - description of the new trans code from T1688.
*   - Set the effective date with todays date.
*   - Write the new record to the database by calling the
*   - subroutine 'LIFACMV'.
*
* Reverse Life Extras & Options ( LEXTs ):
*
* IF there are any validflag '2' LEXT records which have a
*  tranno which matches the tranno we are reversing, then we will
*  reinstate those records to V/F '1', deleting any V/F'1's
*  already in existance which have identical keys to those
*  being reinstated.
* IF there are any LEXT V/F '1' records with a tranno equal to
*  the tranno we are reversing, then we will delete those
*
* Reverse Reassurance records ( RACTs ):
*
*  Read the Reassurance file (RACTPF) using the logical
*  view RACTRCC with a key of company, contract number, life,
*  coverage, rider, reassurance number, reassurance type and
*  validflag.
*
*  Within Component modify, 3 operations can be applied to
*   Reassurance: Add New Reassurance, Modify existing Reassurance
*                and Delete existing Reassurance.
*
*    Add Reass. results in a V/F '1' RACT record.
*    Modify Reass. results in V/F '2', '4' & '1' RACT records.
*    Delete Reass. results in V/F '2' & '4' RACT records.
*
*    We will use the RACTRCC logical view to read through the
*     RACT records - the selection criteria for this file is
*     Company, Contract number, Life, Coverage, Rider,
*     Reassurance number, Reassurance type & validflag.
*     The Validflag is selected 'DESCENDING', ie. it will
*      pick up the V/F '4' before the V/F '2' etc..
*
*  For each record found for the contract being processed...
*
*    IF a validflag '4' record is found,
*        check for a validflag '2' record with identical key
*        IF validflag '2' found,
*            check tranno with PTRN tranno we are reversing
*            IF trannos match
*                DELETE validflag '4' record
*        IF validflag '1' found,
*            check key and tranno with PTRN tranno
*                                           being reversed
*            IF trannos match,
*                DELETE validflag '1' record
*        IF validflag '2' was found,
*            now reset to validflag '1' & update tranno
*    END-IF
*
*    IF a validflag '1' record is found,
*        then this Reassurance RACT record was 'added' to a
*         coverage/rider so there will be no V/F '2' or '4'
*         records to process
*         DELETE validflag '1' record
*    END-IF
*
* Reverse Agent Commission detail records ( AGCMs ):
*
*  Re-validate all Agent commission records by calling the
*   Agent commission calculation subroutine ACOMCALC. We will
*   also create a temporary PCDT record for use by the
*   subroutine - if it is reversing a decrease, ie. performing
*   an increase, then it needs the PCDT record to know where
*   to assign the new commission.
*
* Reversal of Unit-Linked/Traditional parts of the Contract:
*
*  Loop through all the Coverages/Riders associated with the
*   Contract we are processing and call any Generic processing
*   subroutines that are specified in T5671.
*
*
*  PROGRAMMING NOTE.
********************
*    This subroutine uses the relatively 'new' I/O function
*     WRITD.
*     a) WRITD is used to update a record in place which has
*         had its KEY changed - the record does not need to be
*         LOCKED first
*         Because the Relative Record number hasn't changed by
*          performing the WRITD, NEXTR will then read the next
*          record in the file ( until end-of-file ) and won't
*          pick up and reprocess the one we have just written.
*
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing
* T5687 - Coverage/Rider details
* T5688 - Contract type details
* T6658 - Anniversary/Auto Increase Proccessing Method
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Inclusion of cash dividend processing.                              *
*                                                                     *
*****************************************************************
* </pre>
*/
public class Revcompm extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVCOMPM";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaValidflag1Found = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaValidflag2Found = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRasnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaRatype = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaAnnpremBefore = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaAnnpremAfter = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaFreqFactor = new PackedDecimalData(11, 5).init(ZERO);
	private FixedLengthStringData wsaaVflag2 = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0).setUnsigned();

	private FixedLengthStringData wsaaBatchkey = new FixedLengthStringData(65);
	private FixedLengthStringData wsaaContkey = new FixedLengthStringData(19).isAPartOf(wsaaBatchkey, 0);
	private FixedLengthStringData wsaaBatcpfx = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 0);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaContkey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaContkey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaContkey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaContkey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaContkey, 14);
	private FixedLengthStringData wsaaOldBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatchkey, 19);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
	private ZonedDecimalData wsaaReinstateTranno = new ZonedDecimalData(5, 0);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
		/* ERRORS */
	private static final String e308 = "E308";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private static final String t5688 = "T5688";
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmbcrTableDAM agcmbcrIO = new AgcmbcrTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrrccTableDAM covrrccIO = new CovrrccTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcorevTableDAM fpcorevIO = new FpcorevTableDAM();
	private Fpcorv2TableDAM fpcorv2IO = new Fpcorv2TableDAM();
	private FprmrevTableDAM fprmrevIO = new FprmrevTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextchgTableDAM lextchgIO = new LextchgTableDAM();
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
	private LirrconTableDAM lirrconIO = new LirrconTableDAM();
	private LirrrrvTableDAM lirrrrvIO = new LirrrrvTableDAM();
	private LrrhconTableDAM lrrhconIO = new LrrhconTableDAM();
	private LrrhrrvTableDAM lrrhrrvIO = new LrrhrrvTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private RacdTableDAM racdIO = new RacdTableDAM();
	private RacdrrvTableDAM racdrrvIO = new RacdrrvTableDAM();
	private RactrccTableDAM ractrccIO = new RactrccTableDAM();
	private RactrevTableDAM ractrevIO = new RactrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private T5688rec t5688rec = new T5688rec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	List<Exclpf> exclpfList = new ArrayList<Exclpf>();
	private Exclpf exclpf = new Exclpf();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next10150, 
		nextrRacd20020, 
		nextrLrrh21020, 
		exit21090, 
		nextrLirr22020, 
		exit22090
	}

	public Revcompm() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processChdrs3000();
		processPayrs4000();
		if (flexiblePremiumContract.isTrue()) {
			processFprm4500();
		}
		processCovrs5000();
		/* PERFORM 20000-PROCESS-RACDS.                         <LA3295>*/
		/* PERFORM 21000-PROCESS-LRRHS.                         <LA3295>*/
		/* PERFORM 22000-PROCESS-LIRRS.                         <LA3295>*/
		processAcmvs8000();
		processAcmvOptical8010();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		wsaaVflag2.set(SPACES);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError99000();
		}
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void processChdrs3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Do a BEGNH on Contract Header CHDR*/
		/*  Get validflag '1' and DELETe*/
		/*  Get NEXTRecord (validflag '2') and REWRiTe as validflag '1'.*/
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		/* MOVE BEGNH                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(), "1")
		|| isEQ(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		/* MOVE DELET                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		chdrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(), "2")
		|| isEQ(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		chdrmjaIO.setValidflag("1");
		/* MOVE REWRT                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		/* Read T5688 Contract details table to check for Component*/
		/*  Level Accounting for use in the Agent commission calculations*/
		/*  later on.*/
		readT56883100();
		/* Read T5729 to determine if this contract is of a 'flexible'     */
		/* type. If it is, later on we will reverse the FPCO and FPRM      */
		/* records.                                                        */
		readT57293200();
	}

protected void readT56883100()
	{
		start3100();
	}

protected void start3100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrmjaIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			systemError99000();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT57293200()
	{
		start3210();
	}

protected void start3210()
	{
		notFlexiblePremiumContract.setTrue();
		/*  Read T5729.                                            <D9604> */
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrmjaIO.getChdrcoy(), SPACES);
			stringVariable1.addExpression(t5729, SPACES);
			stringVariable1.addExpression(chdrmjaIO.getCnttype(), SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5729)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())) {
			return ;
		}
		flexiblePremiumContract.setTrue();
	}

protected void processPayrs4000()
	{
		start4000();
	}

protected void start4000()
	{
		/* Do a BEGNH on Contract Header PAYR*/
		/*  Get validflag '1' and DELETe*/
		/*  Get NEXTRecord (validflag '2') and REWRiTe as validflag '1'.*/
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(ZERO);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		/* MOVE BEGNH                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)
		&& isNE(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), "1")
		|| isEQ(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		/* MOVE DELET                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		payrlifIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)
		&& isNE(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), "2")
		|| isEQ(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		payrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
	}

protected void processFprm4500()
	{
		para4510();
	}

	/**
	* <pre>
	**************************************                    <D9604> 
	* </pre>
	*/
protected void para4510()
	{
		/*                                                         <D9604> */
		/* Read the FPRMREV file. If the tranno of the             <D9604> */
		/* vf1 record is not = to reversing tranno, donot update   <D9604> */
		/* the record, otherwise delete the vf1 and reinstate the  <D9604> */
		/* vf2.                                                    <D9604> */
		fprmrevIO.setDataArea(SPACES);
		fprmrevIO.setChdrcoy(reverserec.company);
		fprmrevIO.setChdrnum(reverserec.chdrnum);
		fprmrevIO.setPayrseqno(payrlifIO.getPayrseqno());
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		fprmrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			databaseError99500();
		}
		if (isNE(fprmrevIO.getValidflag(), "1")) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			databaseError99500();
		}
		/*                                                         <D9604> */
		/* Delete the VF 1 FPRMREV record then re-instate the VF2  <D9604> */
		/*                                                         <D9604> */
		if (isNE(fprmrevIO.getTranno(), reverserec.tranno)) {
			fprmrevIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, fprmrevIO);
			if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(fprmrevIO.getStatuz());
				syserrrec.params.set(fprmrevIO.getParams());
				databaseError99500();
			}
			return ;
		}
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		fprmrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			databaseError99500();
		}
		/*                                                         <D9604> */
		/* Now reinstate the previous vf2                          <D9604> */
		/*                                                         <D9604> */
		fprmrevIO.setFunction(varcom.readh);
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			databaseError99500();
		}
		if (isNE(fprmrevIO.getValidflag(), "2")) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			databaseError99500();
		}
		fprmrevIO.setValidflag("1");
		fprmrevIO.setCurrto(varcom.vrcmMaxDate);
		fprmrevIO.setFunction(varcom.rewrt);
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			databaseError99500();
		}
	}

protected void processCovrs5000()
	{
		start5000();
	}

protected void start5000()
	{
		/* Use COVR logical view to loop around coverage/rider records*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			reverseCovrs5100();
		}
		
	}

protected void reverseCovrs5100()
	{
		start5100();
	}

protected void start5100()
	{
		/* check TRANNO of COVR just read*/
		if (isEQ(reverserec.tranno, covrIO.getTranno())) {
			covrrccIO.setParams(SPACES);
			covrrccIO.setChdrcoy(covrIO.getChdrcoy());
			covrrccIO.setChdrnum(covrIO.getChdrnum());
			covrrccIO.setLife(covrIO.getLife());
			covrrccIO.setCoverage(covrIO.getCoverage());
			covrrccIO.setRider(covrIO.getRider());
			covrrccIO.setPlanSuffix(covrIO.getPlanSuffix());
			/*     MOVE BEGN               TO COVRRCC-FUNCTION              */
			covrrccIO.setFunction(varcom.readh);
			covrrccIO.setTranno(reverserec.tranno);
			covrrccIO.setFormat(formatsInner.covrrccrec);
			/*     PERFORM 5200-DELETE-COVRS UNTIL COVRRCC-STATUZ = ENDP    */
			deleteCovrsR5200();
			/*        Do Generic processing after COVRs deleted and earlier*/
			/*         COVR reinstated to validflag '1'.*/
			cashDividend11100();
			genericProcessing9000();
			processLexts6000();
			processExclusion();
			/*     PERFORM 7000-PROCESS-RACTS                               */
			agentCommissionRecalc10000();
			if (flexiblePremiumContract.isTrue()) {
				processFpco11000();
			}
		}
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
	}

protected void deleteCovrsR5200()
	{
		/*START*/
		covrrccIo5300();
		covrrccIO.setFunction(varcom.delet);
		covrrccIo5300();
		covrrccIO.setFunction(varcom.endh);
		covrrccIo5300();
		covrrccIO.setFunction(varcom.rewrt);
		covrrccIO.setValidflag("1");
		covrrccIO.setCurrto(99999999);
		covrrccIo5300();
		/*EXIT*/
	}

	/**
	* <pre>
	*5200-DELETE-COVRS SECTION.                                       
	*5200-START.                                                      
	**** CALL 'COVRRCCIO'            USING COVRRCC-PARAMS.            
	**** IF COVRRCC-STATUZ           NOT = O-K                        
	****    AND COVRRCC-STATUZ       NOT = ENDP                       
	****     MOVE COVRRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE COVRRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** IF COVR-CHDRCOY             NOT = COVRRCC-CHDRCOY            
	****    OR COVR-CHDRNUM          NOT = COVRRCC-CHDRNUM            
	****    OR COVR-LIFE             NOT = COVRRCC-LIFE               
	****    OR COVR-COVERAGE         NOT = COVRRCC-COVERAGE           
	****    OR COVR-RIDER            NOT = COVRRCC-RIDER              
	****    OR COVR-PLAN-SUFFIX      NOT = COVRRCC-PLAN-SUFFIX        
	****    OR COVRRCC-STATUZ        = ENDP                           
	****     MOVE ENDP               TO COVRRCC-STATUZ                
	****     GO TO 5290-EXIT                                          
	**** END-IF.                                                      
	**** IF COVRRCC-TRANNO           NOT < REVE-TRANNO                
	****     MOVE READH                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE DELET                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE NEXTR                  TO COVRRCC-FUNCTION          
	**** ELSE                                                         
	****     MOVE READH                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE '1'                    TO COVRRCC-VALIDFLAG         
	****     MOVE REVE-NEW-TRANNO        TO COVRRCC-TRANNO       <002>
	****     MOVE 99999999               TO COVRRCC-CURRTO            
	****     MOVE REWRT                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE ENDP                   TO COVRRCC-STATUZ            
	**** END-IF.                                                      
	*5290-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void covrrccIo5300()
	{
		/*START*/
		SmartFileCode.execute(appVars, covrrccIO);
		if (isNE(covrrccIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrccIO.getParams());
			syserrrec.statuz.set(covrrccIO.getStatuz());
			systemError99000();
		}
		/*EXIT*/
	}

protected void processLexts6000()
	{
		start6000();
	}

protected void start6000()
	{
		/* IF there are any validflag '2' LEXT records which have a*/
		/*  tranno which match the tranno we are reversing, then we will*/
		/*  reinstate those records to V/F '1', deleting any V/F'1's*/
		/*  already in existance which have identical keys to those*/
		/*  being reinstated.*/
		/* IF there are any LEXT V/F '1' records with a tranno equal to*/
		/*  the tranno we are reversing, then we will delete those*/
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrcoy(reverserec.company);
		lextrevIO.setChdrnum(reverserec.chdrnum);
		lextrevIO.setLife(covrIO.getLife());
		lextrevIO.setCoverage(covrIO.getCoverage());
		lextrevIO.setRider(covrIO.getRider());
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.oK)
		&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			systemError99000();
		}
		if (isNE(reverserec.company, lextrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, lextrevIO.getChdrnum())
		|| isNE(covrIO.getLife(), lextrevIO.getLife())
		|| isNE(covrIO.getCoverage(), lextrevIO.getCoverage())
		|| isNE(covrIO.getRider(), lextrevIO.getRider())
		|| isEQ(lextrevIO.getStatuz(), varcom.endp)) {
			lextrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(lextrevIO.getStatuz(), varcom.endp))) {
			reverseLexts6100();
		}
		
	}

protected void reverseLexts6100()
	{
		start6100();
	}

protected void start6100()
	{
		/* first check tranno of LEXT record read*/
		if (isEQ(reverserec.tranno, lextrevIO.getTranno())) {
			if (isEQ(lextrevIO.getValidflag(), "1")) {
				lextrevIO.setFunction(varcom.readh);
				lextrevIO.setFormat(formatsInner.lextrevrec);
				SmartFileCode.execute(appVars, lextrevIO);
				if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lextrevIO.getParams());
					syserrrec.statuz.set(lextrevIO.getStatuz());
				}
				lextrevIO.setFunction(varcom.delet);
				lextrevIO.setFormat(formatsInner.lextrevrec);
				SmartFileCode.execute(appVars, lextrevIO);
				if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lextrevIO.getParams());
					syserrrec.statuz.set(lextrevIO.getStatuz());
				}
				lextrevIO.setFunction(varcom.endh);
				lextrevIO.setFormat(formatsInner.lextrevrec);
				SmartFileCode.execute(appVars, lextrevIO);
				if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lextrevIO.getParams());
					syserrrec.statuz.set(lextrevIO.getStatuz());
				}
				if (isGTE(lextrevIO.getTranno(), reverserec.tranno)) {
					syserrrec.params.set("LEXT Not FND");
					syserrrec.statuz.set(varcom.bomb);
				}
				lextrevIO.setFunction(varcom.rewrt);
				lextrevIO.setValidflag("1");
				lextrevIO.setFormat(formatsInner.lextrevrec);
				SmartFileCode.execute(appVars, lextrevIO);
				if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lextrevIO.getParams());
					syserrrec.statuz.set(lextrevIO.getStatuz());
				}
				lextrevIO.setStatuz(varcom.endp);
			}
			if (isEQ(lextrevIO.getValidflag(), "2")) {
				lextrevIO.setValidflag("1");
				lextrevIO.setTranno(reverserec.newTranno);
				lextrevIO.setFunction(varcom.writd);
				lextrevIO.setFormat(formatsInner.lextrevrec);
				SmartFileCode.execute(appVars, lextrevIO);
				if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(lextrevIO.getParams());
					syserrrec.statuz.set(lextrevIO.getStatuz());
				}
			}
		}
		else {
			if (isEQ(lextrevIO.getValidflag(), "1")) {
				syserrrec.params.set("LEXT VF <> 1");
				syserrrec.statuz.set(varcom.bomb);
			}
		}
		/*            MOVE SPACES           TO LEXTCHG-PARAMS              */
		/*            MOVE LEXTREV-CHDRCOY  TO LEXTCHG-CHDRCOY             */
		/*            MOVE LEXTREV-CHDRNUM  TO LEXTCHG-CHDRNUM             */
		/*            MOVE LEXTREV-LIFE     TO LEXTCHG-LIFE                */
		/*            MOVE LEXTREV-COVERAGE TO LEXTCHG-COVERAGE            */
		/*            MOVE LEXTREV-RIDER    TO LEXTCHG-RIDER               */
		/*            MOVE LEXTREV-SEQNBR   TO LEXTCHG-SEQNBR              */
		/*            MOVE ZEROS            TO LEXTCHG-TRANNO              */
		/*            MOVE BEGN             TO LEXTCHG-FUNCTION            */
		/*            MOVE LEXTCHGREC       TO LEXTCHG-FORMAT              */
		/*            MOVE 'N'              TO WSAA-VFLAG-2                */
		/*            PERFORM 6200-CHECK-VFLAG-2-LEXT                      */
		/*                                  UNTIL LEXTCHG-STATUZ = ENDP    */
		/*            IF WSAA-VFLAG-2       = 'Y'                          */
		/*            we have found a V/flag '2' record with the tranno    */
		/*             we are reversing, so delete the V/flag '1' and then */
		/*             reinstate the V/flag '2' to '1'.                    */
		/*                MOVE DELET        TO LEXTREV-FUNCTION            */
		/*                MOVE LEXTREVREC   TO LEXTREV-FORMAT              */
		/*                CALL 'LEXTREVIO'  USING LEXTREV-PARAMS           */
		/*                IF LEXTREV-STATUZ NOT = O-K                      */
		/*                    MOVE LEXTREV-PARAMS TO SYSR-PARAMS           */
		/*                    MOVE LEXTREV-STATUZ TO SYSR-STATUZ           */
		/*                    PERFORM 99000-SYSTEM-ERROR                   */
		/*                END-IF                                           */
		/*            Lock record for updating                             */
		/*                MOVE '2'          TO LEXTREV-VALIDFLAG           */
		/*                MOVE REVE-TRANNO  TO LEXTREV-TRANNO              */
		/*                MOVE READH        TO LEXTREV-FUNCTION            */
		/*                MOVE LEXTREVREC   TO LEXTREV-FORMAT              */
		/*                CALL 'LEXTREVIO'  USING LEXTREV-PARAMS           */
		/*                IF LEXTREV-STATUZ NOT = O-K                      */
		/*                    MOVE LEXTREV-PARAMS TO SYSR-PARAMS           */
		/*                    MOVE LEXTREV-STATUZ TO SYSR-STATUZ           */
		/*                    PERFORM 99000-SYSTEM-ERROR                   */
		/*                END-IF                                           */
		/*            Update fields and REWRiTe                            */
		/*                MOVE REVE-NEW-TRANNO TO LEXTREV-TRANNO           */
		/*                MOVE '1'          TO LEXTREV-VALIDFLAG           */
		/*                MOVE REWRT        TO LEXTREV-FUNCTION            */
		/*                MOVE LEXTREVREC   TO LEXTREV-FORMAT              */
		/*                CALL 'LEXTREVIO'  USING LEXTREV-PARAMS           */
		/*                IF LEXTREV-STATUZ NOT = O-K                      */
		/*                    MOVE LEXTREV-PARAMS TO SYSR-PARAMS           */
		/*                    MOVE LEXTREV-STATUZ TO SYSR-STATUZ           */
		/*                    PERFORM 99000-SYSTEM-ERROR                   */
		/*                END-IF                                           */
		/*            END-IF                                               */
		/*        END-IF                                                   */
		/*    END-IF.                                                      */
		/* get next LEXT record*/
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.oK)
		&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			systemError99000();
		}
		if (isNE(reverserec.company, lextrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, lextrevIO.getChdrnum())
		|| isNE(covrIO.getLife(), lextrevIO.getLife())
		|| isNE(covrIO.getCoverage(), lextrevIO.getCoverage())
		|| isNE(covrIO.getRider(), lextrevIO.getRider())
		|| isEQ(lextrevIO.getStatuz(), varcom.endp)) {
			lextrevIO.setStatuz(varcom.endp);
		}
	}

protected void checkVflag2Lext6200()
	{
		start6200();
	}

protected void start6200()
	{
		SmartFileCode.execute(appVars, lextchgIO);
		if (isNE(lextchgIO.getStatuz(), varcom.oK)
		&& isNE(lextchgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextchgIO.getParams());
			syserrrec.statuz.set(lextchgIO.getStatuz());
			systemError99000();
		}
		if (isNE(lextrevIO.getChdrcoy(), lextchgIO.getChdrcoy())
		|| isNE(lextrevIO.getChdrnum(), lextchgIO.getChdrnum())
		|| isNE(lextrevIO.getLife(), lextchgIO.getLife())
		|| isNE(lextrevIO.getCoverage(), lextchgIO.getCoverage())
		|| isNE(lextrevIO.getRider(), lextchgIO.getRider())
		|| isNE(lextrevIO.getSeqnbr(), lextchgIO.getSeqnbr())
		|| isEQ(lextchgIO.getStatuz(), varcom.endp)) {
			lextchgIO.setStatuz(varcom.endp);
			return ;
		}
		/* If tranno on LEXT is less than transaction we are reversing*/
		/*  then ignore and get next.*/
		/* If tranno matches transaction we are reversing, then set*/
		/*  WSAA-VFLAG-2 to 'Y' so we can reset its V/flag in the above*/
		/*  calling section. We also want to DELETe any other LEXT records*/
		/*  which have this key, are V/flag '2' and have a tranno greater*/
		/*  than the transaction we are trying to reverse.*/
		if (isLT(lextchgIO.getTranno(), reverserec.tranno)) {
			lextchgIO.setFunction(varcom.nextr);
			return ;
		}
		if (isEQ(lextchgIO.getTranno(), reverserec.tranno)) {
			wsaaVflag2.set("Y");
			lextchgIO.setFunction(varcom.nextr);
			return ;
		}
		if (isGT(lextchgIO.getTranno(), reverserec.tranno)
		&& isEQ(wsaaVflag2, "Y")) {
			/*    we now know that there is a Validflag '1' and a Validflag*/
			/*     '2' record (with the tranno of the transaction we are*/
			/*     trying to reverse), so we can DELETe all other LEXT records*/
			/*     which match the LEXT record we are processing IF they have*/
			/*     a tranno GREATER than the transaction we are reversing.*/
			lextchgIO.setFunction(varcom.readh);
			lextchgIO.setFormat(formatsInner.lextchgrec);
			SmartFileCode.execute(appVars, lextchgIO);
			if (isNE(lextchgIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lextchgIO.getParams());
				syserrrec.statuz.set(lextchgIO.getStatuz());
				systemError99000();
			}
			lextchgIO.setFunction(varcom.delet);
			lextchgIO.setFormat(formatsInner.lextchgrec);
			SmartFileCode.execute(appVars, lextchgIO);
			if (isNE(lextchgIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lextchgIO.getParams());
				syserrrec.statuz.set(lextchgIO.getStatuz());
				systemError99000();
			}
		}
		lextchgIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*7000-PROCESS-RACTS SECTION.                                      
	*7000-START.                                                      
	**** MOVE SPACES                 TO RACTRCC-PARAMS.               
	**** MOVE REVE-COMPANY           TO RACTRCC-CHDRCOY.              
	**** MOVE REVE-CHDRNUM           TO RACTRCC-CHDRNUM.              
	**** MOVE COVR-LIFE              TO RACTRCC-LIFE.                 
	**** MOVE COVR-COVERAGE          TO RACTRCC-COVERAGE.             
	**** MOVE COVR-RIDER             TO RACTRCC-RIDER.                
	**** MOVE SPACES                 TO RACTRCC-RASNUM.               
	**** MOVE SPACES                 TO RACTRCC-RATYPE.               
	**** MOVE SPACES                 TO RACTRCC-VALIDFLAG.            
	**** MOVE ZEROS                  TO RACTRCC-TRANNO.               
	**** MOVE BEGN                   TO RACTRCC-FUNCTION.             
	**** MOVE RACTRCCREC             TO RACTRCC-FORMAT.               
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.            
	**** IF RACTRCC-STATUZ           NOT = O-K                        
	****    AND RACTRCC-STATUZ       NOT = ENDP                       
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	**** IF REVE-COMPANY             NOT =  RACTRCC-CHDRCOY           
	****    OR REVE-CHDRNUM          NOT =  RACTRCC-CHDRNUM           
	****    OR COVR-LIFE             NOT =  RACTRCC-LIFE              
	****    OR COVR-COVERAGE         NOT =  RACTRCC-COVERAGE          
	****    OR COVR-RIDER            NOT =  RACTRCC-RIDER             
	****    OR RACTRCC-STATUZ        = ENDP                           
	****     MOVE ENDP               TO RACTRCC-STATUZ                
	**** END-IF.                                                      
	**** PERFORM 7100-REVERSE-RACTS  UNTIL RACTRCC-STATUZ = ENDP.     
	*7090-EXIT.                                                       
	**** EXIT.                                                        
	*7100-REVERSE-RACTS SECTION.                                      
	*7100-START.                                                      
	**NOTE.                                                           
	***IF Reassurance was Added by the forward Component Modify,      
	****  then we will be looking for RACT records with validflags    
	****  '1' and '2' to process.                                     
	***IF Reassurance was Deleted by the forward Component Modify,    
	****  then we will be looking for RACT records with validflags    
	****  '2' and '4' to process.                                     
	***IF Reassurance was Modified by the forward Component Modify,   
	****  then we will be looking for RACT records with validflags    
	****  '1' , '2' and '4' to process.                               
	***The validflag '2' records above will ALWAYS have the tranno of 
	****the forward transaction, enabling us to select the correct    
	**** records for reversal.                                        
	**Because of the way the RACTRCC logical file is defined, with    
	***validflag DESCENDING, then the V/flag '4' RACTs for each       
	***component should be selected first...                          
	**** IF RACTRCC-VALIDFLAG        = '4'                            
	**Read next RACT record to see if a V/F '2' exists with the       
	***tranno of the forward transaction we are trying to reverse     
	***IF none exist, then this RACT 'modification' was not made      
	**** at the time of the transaction we are attempting to          
	**** reverse, so we will ignore it.                               
	****     MOVE RACTRCC-LIFE       TO WSAA-LIFE                     
	****     MOVE RACTRCC-COVERAGE   TO WSAA-COVERAGE                 
	****     MOVE RACTRCC-RIDER      TO WSAA-RIDER                    
	****     MOVE RACTRCC-RASNUM     TO WSAA-RASNUM                   
	****     MOVE RACTRCC-RATYPE     TO WSAA-RATYPE                   
	****     MOVE RACTRCC-TRANNO     TO WSAA-TRANNO                   
	****     MOVE REVE-COMPANY       TO RACTREV-CHDRCOY               
	****     MOVE REVE-CHDRNUM       TO RACTREV-CHDRNUM               
	****     MOVE RACTRCC-LIFE       TO RACTREV-LIFE                  
	****     MOVE RACTRCC-COVERAGE   TO RACTREV-COVERAGE              
	****     MOVE RACTRCC-RIDER      TO RACTREV-RIDER                 
	****     MOVE RACTRCC-RASNUM     TO RACTREV-RASNUM                
	****     MOVE RACTRCC-RATYPE     TO RACTREV-RATYPE                
	****     MOVE SPACES             TO RACTREV-VALIDFLAG             
	****     MOVE ZEROS              TO RACTREV-TRANNO                
	****     MOVE BEGNH              TO RACTREV-FUNCTION              
	****     MOVE RACTREVREC         TO RACTREV-FORMAT                
	****     MOVE 'N'                TO WSAA-VALIDFLAG-2-FOUND        
	****     PERFORM 7200-CHECK-VALIDFLAG-2                           
	****                             UNTIL RACTREV-STATUZ = ENDP      
	****                             OR WSAA-VALIDFLAG-2-FOUND = 'Y'  
	****     IF WSAA-VALIDFLAG-2-FOUND = 'Y'                          
	****     Having found a corresponding V/F '2' record, then        
	****      delete V/F '4' and V/F '1' RACT records                 
	****      then reinstate V/F '2' to V/F '1'                       
	****         PERFORM 7300-DELETE-RACTRCC-RECORD                   
	****         MOVE SPACES         TO WSAA-VALIDFLAG-1-FOUND        
	****         PERFORM 7400-FIND-VALIDFLAG-1                        
	****                      UNTIL                                   
	****                      WSAA-VALIDFLAG-1-FOUND NOT = SPACES     
	****         PERFORM 7500-REINSTATE-VALIDFLAG-2                   
	****         if no validflag '1' was found, then the RACTRCC      
	****           record pointer will have moved to the next         
	****           RACT record, which may be for a different          
	****           Component/reassurance type, or it may have got     
	****           to end-of-file, so we don't want to do the         
	****           NEXTR on the RACTRCC below.                        
	****         IF WSAA-VALIDFLAG-1-FOUND = 'N'                      
	****             GO TO 7190-EXIT                                  
	****         END-IF                                               
	****     END-IF                                                   
	**** END-IF.                                                      
	**if no validflag '4' record for a particular component, then     
	***maybe the Reassurance was 'Added', not 'Modified'.             
	***- check the tranno on the V/F '2' record, and if a match, then 
	**** delete and also delete the validflag '1'.                    
	**** IF RACTRCC-VALIDFLAG        = '2'                            
	****    AND RACTRCC-TRANNO       = REVE-TRANNO                    
	****     MOVE RACTRCC-LIFE       TO WSAA-LIFE                     
	****     MOVE RACTRCC-COVERAGE   TO WSAA-COVERAGE                 
	****     MOVE RACTRCC-RIDER      TO WSAA-RIDER                    
	****     MOVE RACTRCC-RASNUM     TO WSAA-RASNUM                   
	****     MOVE RACTRCC-RATYPE     TO WSAA-RATYPE                   
	****     MOVE RACTRCC-TRANNO     TO WSAA-TRANNO                   
	****     PERFORM 7300-DELETE-RACTRCC-RECORD                       
	****     MOVE SPACES             TO WSAA-VALIDFLAG-1-FOUND        
	****     PERFORM 7400-FIND-VALIDFLAG-1                            
	****                     UNTIL                                    
	****                     WSAA-VALIDFLAG-1-FOUND NOT = SPACES      
	****         if no validflag '1' was found, then the RACTRCC      
	****           record pointer will have moved to the next         
	****           RACT record, which may be for a different          
	****           Component/reassurance type, or it may have got     
	****           to end-of-file, so we don't want to do the         
	****           NEXTR on the RACTRCC below.                        
	****         IF WSAA-VALIDFLAG-1-FOUND = 'N'                      
	****             GO TO 7190-EXIT                                  
	****         END-IF                                               
	**** END-IF.                                                      
	**** MOVE NEXTR                  TO RACTRCC-FUNCTION.             
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.            
	**** IF RACTRCC-STATUZ           NOT = O-K                        
	****    AND RACTRCC-STATUZ       NOT = ENDP                       
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	**** IF REVE-COMPANY             NOT =  RACTRCC-CHDRCOY           
	****    OR REVE-CHDRNUM          NOT =  RACTRCC-CHDRNUM           
	****    OR COVR-LIFE             NOT =  RACTRCC-LIFE              
	****    OR COVR-COVERAGE         NOT =  RACTRCC-COVERAGE          
	****    OR COVR-RIDER            NOT =  RACTRCC-RIDER             
	****    OR RACTRCC-STATUZ        = ENDP                           
	****     MOVE ENDP               TO RACTRCC-STATUZ                
	**** END-IF.                                                      
	*7190-EXIT.                                                       
	**** EXIT.                                                        
	*7200-CHECK-VALIDFLAG-2 SECTION.                                  
	*7200-START.                                                      
	**** CALL 'RACTREVIO'            USING RACTREV-PARAMS.            
	**** IF RACTREV-STATUZ           NOT = O-K                        
	****    AND RACTREV-STATUZ       NOT = ENDP                       
	****     MOVE RACTREV-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTREV-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	**** IF RACTREV-CHDRCOY          NOT = REVE-COMPANY               
	****    OR RACTREV-CHDRNUM       NOT = REVE-CHDRNUM               
	****    OR RACTREV-LIFE          NOT = WSAA-LIFE                  
	****    OR RACTREV-COVERAGE      NOT = WSAA-COVERAGE              
	****    OR RACTREV-RIDER         NOT = WSAA-RIDER                 
	****    OR RACTREV-RASNUM        NOT = WSAA-RASNUM                
	****    OR RACTREV-RATYPE        NOT = WSAA-RATYPE                
	****    OR RACTREV-STATUZ        = ENDP                           
	****     MOVE ENDP               TO RACTREV-STATUZ                
	****     GO TO 7290-EXIT                                          
	**** END-IF.                                                      
	**** IF RACTREV-VALIDFLAG        = '2'                            
	****    AND RACTREV-TRANNO       = REVE-TRANNO                    
	****     MOVE 'Y'                TO WSAA-VALIDFLAG-2-FOUND        
	**** END-IF.                                                      
	**** MOVE NEXTR                  TO RACTREV-FUNCTION.             
	*7290-EXIT.                                                       
	**** EXIT.                                                        
	*7300-DELETE-RACTRCC-RECORD SECTION.                              
	*7300-START.                                                      
	**** MOVE READH                  TO RACTRCC-FUNCTION.             
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.            
	**** IF RACTRCC-STATUZ           NOT = O-K                        
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** MOVE DELET                  TO RACTRCC-FUNCTION.             
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.            
	**** IF RACTRCC-STATUZ           NOT = O-K                        
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	*7390-EXIT.                                                       
	**** EXIT.                                                        
	*7400-FIND-VALIDFLAG-1 SECTION.                                   
	*7400-START.                                                      
	**** MOVE NEXTR                  TO RACTRCC-FUNCTION.             
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.            
	**** IF RACTRCC-STATUZ           NOT = O-K                        
	****    AND RACTRCC-STATUZ       NOT = ENDP                       
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** IF RACTRCC-CHDRCOY          NOT = REVE-COMPANY               
	****    OR RACTRCC-CHDRNUM       NOT = REVE-CHDRNUM               
	****    OR RACTRCC-LIFE          NOT = WSAA-LIFE                  
	****    OR RACTRCC-COVERAGE      NOT = WSAA-COVERAGE              
	****    OR RACTRCC-RIDER         NOT = WSAA-RIDER                 
	****    OR RACTRCC-RASNUM        NOT = WSAA-RASNUM                
	****    OR RACTRCC-RATYPE        NOT = WSAA-RATYPE                
	****    OR RACTRCC-STATUZ        = ENDP                           
	****     MOVE 'N'                TO WSAA-VALIDFLAG-1-FOUND        
	****     GO TO 7490-EXIT                                          
	**** END-IF.                                                      
	**** IF RACTRCC-VALIDFLAG        = '1'                            
	****     PERFORM 7300-DELETE-RACTRCC-RECORD                       
	****     MOVE 'Y'                TO WSAA-VALIDFLAG-1-FOUND        
	**** END-IF.                                                      
	*7490-EXIT.                                                       
	**** EXIT.                                                        
	*7500-REINSTATE-VALIDFLAG-2 SECTION.                              
	*7500-START.                                                      
	**** MOVE '1'                    TO RACTREV-VALIDFLAG.            
	**** MOVE REVE-NEW-TRANNO        TO RACTREV-TRANNO.               
	**** MOVE REWRT                  TO RACTREV-FUNCTION.             
	**** MOVE RACTREVREC             TO RACTREV-FORMAT.               
	**** CALL 'RACTREVIO'            USING RACTREV-PARAMS.            
	**** IF RACTREV-STATUZ           NOT = O-K                        
	****     MOVE RACTREV-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTREV-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	*7590-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void processAcmvs8000()
	{
		start8000();
	}

protected void start8000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs8100();
		}
		
	}

protected void processAcmvOptical8010()
	{
		start8010();
	}

protected void start8010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError99500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs8100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError99500();
			}
		}
	}

protected void reverseAcmvs8100()
	{
		start8100();
		readNextAcmv8180();
	}

protected void start8100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void readNextAcmv8180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void genericProcessing9000()
	{
		/*START*/
		/* We do any generic prcoessing that is required for the current*/
		/*  Coverage/Rider by calling subroutine(s) specified on*/
		/*  table T5671*/
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setLife(covrIO.getLife());
		covrenqIO.setCoverage(covrIO.getCoverage());
		covrenqIO.setRider(covrIO.getRider());
		covrenqIO.setPlanSuffix(covrIO.getPlanSuffix());
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs9100();
		}
		
		/*EXIT*/
	}

protected void processCovrs9100()
	{
		/*START*/
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(), covrenqIO.getLife())
		|| isNE(covrIO.getCoverage(), covrenqIO.getCoverage())
		|| isNE(covrIO.getRider(), covrenqIO.getRider())
		|| isNE(covrIO.getPlanSuffix(), covrenqIO.getPlanSuffix())) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		genericSubr9200();
		covrenqIO.setStatuz(varcom.endp);
		/*EXIT1*/
	}

protected void genericSubr9200()
	{
		start9200();
	}

protected void start9200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		wsaaBatchkey.set(reverserec.batchkey);
		/* Set old Transaction code so that in the generic subroutine*/
		/* GRVULCHG, it can pass it into UNLCHG subroutine so that*/
		/* UNLCHG can read the table T5537 correctly.*/
		/* NOTE... There are only U/linked generic reversal subroutines*/
		/*          for Component modify.*/
		wsaaOldBatctrcde.set(reverserec.oldBatctrcde);
		greversrec.batckey.set(wsaaBatchkey);
		greversrec.effdate.set(payrlifIO.getPtdate());
		if (flexiblePremiumContract.isTrue()) {
			greversrec.effdate.set(reverserec.ptrneff);
		}
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs9300();
		}
	}

protected void callTrevsubs9300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError99000();
			}
		}
		/*EXIT*/
	}

protected void agentCommissionRecalc10000()
	{
		start10000();
	}

protected void start10000()
	{
		/* Read T5687 using CRTABLE to get Component Commission details.   */
		/* PERFORM 10100-READ-T5687.                                    */
		/*                                                       <A05940>*/
		/* Read T6658 to check if commission is applicable for this<A05940>*/
		/* policy. If it isn't, skip to the end of this section    <A05940>*/
		/* because there won't be any commission to reverse.       <A05940>*/
		/*                                                       <A05940>*/
		/* PERFORM 10300-CHECK-COMIND-ON-T6658.                    <003>*/
		/* IF  T6658-COMIND  =  SPACES                             <003>*/
		/*     GO TO 10090-EXIT                                    <003>*/
		/* END-IF.                                                 <003>*/
		/*                                                         <A05940>*/
		/* IF T5687-ANNIVERSARY-METHOD  NOT = SPACES            <A05940>*/
		/*    PERFORM 10300-CHECK-COMIND-ON-T6658               <A05940>*/
		/*    IF  T6658-COMIND  =  SPACES                       <A05940>*/
		/*        GO TO 10090-EXIT                              <A05940>*/
		/*    END-IF                                            <A05940>*/
		/* END-IF.                                              <A05940>*/
		/*                                                         <A05940>*/
		/* Depending on the billing frequency from the PAYR record,        */
		/*  calculate the 'before' & 'after' annual premiums so that       */
		/*  they can be passed into the commission recalculation           */
		/*  subroutine.                                                    */
		/* MOVE PAYRLIF-BILLFREQ       TO WSAA-BILLFREQ.                */
		/* MOVE WSAA-BILLFREQ-NUM      TO WSAA-FREQ-FACTOR.             */
		/* first the 'old' calculation                                     */
		/* IF COVR-INSTPREM              = ZEROS                        */
		/*     MOVE COVR-SINGP           TO WSAA-ANNPREM-BEFORE         */
		/* ELSE                                                         */
		/*     MULTIPLY COVR-INSTPREM    BY WSAA-FREQ-FACTOR            */
		/*                               GIVING WSAA-ANNPREM-BEFORE     */
		/* END-IF.                                                      */
		/* now the 'new' calculation                                       */
		/* IF COVRRCC-INSTPREM           = ZEROS                        */
		/*     MOVE COVRRCC-SINGP        TO WSAA-ANNPREM-AFTER          */
		/* ELSE                                                         */
		/*     MULTIPLY COVRRCC-INSTPREM BY WSAA-FREQ-FACTOR            */
		/*                               GIVING WSAA-ANNPREM-AFTER      */
		/* END-IF.                                                      */
		/* Need to write a temporary PCDT record here so that any extra    */
		/*  commission calculated by ACOMCALC gets assigned correctly.     */
		/* MOVE SPACES                 TO PCDTMJA-PARAMS.               */
		/* MOVE CHDRMJA-CHDRCOY        TO PCDTMJA-CHDRCOY.              */
		/* MOVE CHDRMJA-CHDRNUM        TO PCDTMJA-CHDRNUM.              */
		/* MOVE COVR-LIFE              TO PCDTMJA-LIFE.                 */
		/* MOVE COVR-COVERAGE          TO PCDTMJA-COVERAGE.             */
		/* MOVE COVR-RIDER             TO PCDTMJA-RIDER.                */
		/* MOVE COVR-PLAN-SUFFIX       TO PCDTMJA-PLAN-SUFFIX.          */
		/* MOVE REVE-NEW-TRANNO        TO PCDTMJA-TRANNO.               */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM01.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM02.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM03.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM04.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM05.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM06.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM07.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM08.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM09.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM10.        */
		/* MOVE CHDRMJA-AGNTNUM        TO PCDTMJA-AGNTNUM( 1 ).         */
		/* MOVE +100                   TO PCDTMJA-SPLITC( 1 ).          */
		/* MOVE ZEROS                  TO PCDTMJA-INSTPREM.             */
		/* MOVE WRITR                  TO PCDTMJA-FUNCTION.             */
		/* MOVE PCDTMJAREC             TO PCDTMJA-FORMAT.               */
		/* CALL 'PCDTMJAIO'            USING PCDTMJA-PARAMS.            */
		/* IF PCDTMJA-STATUZ           NOT = O-K                        */
		/*     MOVE PCDTMJA-PARAMS     TO SYSR-PARAMS                   */
		/*     MOVE PCDTMJA-STATUZ     TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR                               */
		/* END-IF.                                                      */
		/* Revalidate Agent Commission detail records (AGCMs) by           */
		/*  calling the Agent Commission calculation subroutine            */
		/*  known as 'ACOMCALC'.                                           */
		/* We pass the the 'old' component premium values in the           */
		/*  ACOM-COVR-XXXX fields, and the 'new' component premium         */
		/*  values in the ACOM-COVT-XXXX fields in the linkage record.     */
		/* MOVE SPACES                 TO ACOM-ACOMCAL-REC.             */
		/* MOVE COVR-CHDRCOY           TO ACOM-COMPANY.                 */
		/* MOVE COVR-CHDRNUM           TO ACOM-CHDRNUM.                 */
		/* MOVE COVR-LIFE              TO ACOM-COVR-LIFE.               */
		/* MOVE COVR-LIFE              TO ACOM-COVT-LIFE.               */
		/* MOVE COVR-COVERAGE          TO ACOM-COVR-COVERAGE.           */
		/* MOVE COVR-COVERAGE          TO ACOM-COVT-COVERAGE.           */
		/* MOVE COVR-RIDER             TO ACOM-COVR-RIDER.              */
		/* MOVE COVR-RIDER             TO ACOM-COVT-RIDER.              */
		/* MOVE COVR-PLAN-SUFFIX       TO ACOM-COVR-PLAN-SUFFIX.        */
		/* MOVE COVR-PLAN-SUFFIX       TO ACOM-COVT-PLAN-SUFFIX.        */
		/* MOVE REVE-NEW-TRANNO        TO ACOM-TRANNO.                  */
		/* MOVE REVE-TRANS-DATE        TO ACOM-TRANSACTION-DATE.        */
		/* MOVE REVE-TRANS-TIME        TO ACOM-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO ACOM-USER.                    */
		/* MOVE REVE-TERMID            TO ACOM-TERMID.                  */
		/* MOVE PAYRLIF-BTDATE         TO ACOM-PAYR-BTDATE.             */
		/* MOVE PAYRLIF-PTDATE         TO ACOM-PAYR-PTDATE.             */
		/* MOVE PAYRLIF-BILLFREQ       TO ACOM-PAYR-BILLFREQ.           */
		/* MOVE PAYRLIF-CNTCURR        TO ACOM-PAYR-CNTCURR.            */
		/* MOVE CHDRMJA-CNTTYPE        TO ACOM-CNTTYPE.                 */
		/* MOVE CHDRMJA-OCCDATE        TO ACOM-CHDR-OCCDATE.            */
		/* MOVE CHDRMJA-CURRFROM       TO ACOM-CHDR-CURRFROM.           */
		/* MOVE CHDRMJA-AGNTCOY        TO ACOM-CHDR-AGNTCOY.            */
		/* MOVE COVR-JLIFE             TO ACOM-COVR-JLIFE.              */
		/* MOVE COVR-CRTABLE           TO ACOM-COVR-CRTABLE.            */
		/* MOVE T5688-COMLVLACC        TO ACOM-COMLVLACC.               */
		/* MOVE T5687-BASIC-COMM-METH  TO ACOM-BASIC-COMM-METH.         */
		/* MOVE T5687-BASCPY           TO ACOM-BASCPY.                  */
		/* MOVE T5687-RNWCPY           TO ACOM-RNWCPY.                  */
		/* MOVE T5687-SRVCPY           TO ACOM-SRVCPY.                  */
		/* MOVE WSAA-ANNPREM-BEFORE    TO ACOM-COVR-ANNPREM.            */
		/* MOVE WSAA-ANNPREM-AFTER     TO ACOM-COVT-ANNPREM.            */
		/* MOVE REVE-LANGUAGE          TO ACOM-ATMD-LANGUAGE.           */
		/* MOVE REVE-BATCHKEY          TO ACOM-ATMD-BATCH-KEY.          */
		/* CALL 'ACOMCALC'             USING ACOM-ACOMCAL-REC.          */
		/* IF ACOM-STATUZ              NOT = O-K                        */
		/*     MOVE ACOM-ACOMCAL-REC   TO SYSR-PARAMS                   */
		/*     MOVE ACOM-STATUZ        TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR                               */
		/* END-IF.                                                      */
		/* MOVE READH                  TO PCDTMJA-FUNCTION.             */
		/* MOVE PCDTMJAREC             TO PCDTMJA-FORMAT.               */
		/* CALL 'PCDTMJAIO'            USING PCDTMJA-PARAMS.            */
		/* IF PCDTMJA-STATUZ           NOT = O-K                        */
		/*     MOVE PCDTMJA-PARAMS     TO SYSR-PARAMS                   */
		/*     MOVE PCDTMJA-STATUZ     TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR                               */
		/* END-IF.                                                      */
		/* MOVE DELET                  TO PCDTMJA-FUNCTION.             */
		/* MOVE PCDTMJAREC             TO PCDTMJA-FORMAT.               */
		/* CALL 'PCDTMJAIO'            USING PCDTMJA-PARAMS.            */
		/* IF PCDTMJA-STATUZ           NOT = O-K                        */
		/*     MOVE PCDTMJA-PARAMS     TO SYSR-PARAMS                   */
		/*     MOVE PCDTMJA-STATUZ     TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR                               */
		/* END-IF.                                                      */
		/* Delete all validflag '1' AGCMs created by the forward        */
		/* transaction.                                                 */
		wsaaReinstateTranno.set(ZERO);
		agcmbcrIO.setDataArea(SPACES);
		agcmbcrIO.setChdrcoy(covrIO.getChdrcoy());
		agcmbcrIO.setChdrnum(covrIO.getChdrnum());
		agcmbcrIO.setLife(covrIO.getLife());
		agcmbcrIO.setCoverage(covrIO.getCoverage());
		agcmbcrIO.setRider(covrIO.getRider());
		agcmbcrIO.setPlanSuffix(covrIO.getPlanSuffix());
		agcmbcrIO.setTranno(reverserec.tranno);
		agcmbcrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbcrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		agcmbcrIO.setFormat(formatsInner.agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)
		&& isNE(agcmbcrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
		if (isNE(agcmbcrIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(agcmbcrIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(agcmbcrIO.getCoverage(), covrIO.getCoverage())
		|| isNE(agcmbcrIO.getLife(), covrIO.getLife())
		|| isNE(agcmbcrIO.getRider(), covrIO.getRider())
		|| isNE(agcmbcrIO.getPlanSuffix(), covrIO.getPlanSuffix())
		|| isNE(agcmbcrIO.getTranno(), reverserec.tranno)) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
		if (isEQ(agcmbcrIO.getStatuz(), varcom.endp)) {
			return ;
		}
		while ( !(isNE(wsaaReinstateTranno, ZERO))) {
			deleteAgcms10100();
		}
		
		/* Reinstate the AGCMs set to validflag '2' by the forward      */
		/* transaction.                                                 */
		while ( !(isEQ(agcmbcrIO.getStatuz(), varcom.endp))) {
			reinstateAgcms10200();
		}
		
	}

	/**
	* <pre>
	*10100-READ-T5687 SECTION.                                        
	*10100-START.                                                     
	**** MOVE SPACES                 TO ITDM-PARAMS.                  
	**** MOVE 'IT'                   TO ITDM-ITEMPFX.                 
	**** MOVE COVR-CHDRCOY           TO ITDM-ITEMCOY.                 
	**** MOVE T5687                  TO ITDM-ITEMTABL.                
	**** MOVE COVR-CRRCD             TO ITDM-ITMFRM.                  
	**** MOVE COVR-CRTABLE           TO ITDM-ITEMITEM.                
	**** MOVE ITDMREC                TO ITDM-FORMAT.                  
	**** MOVE BEGN                   TO ITDM-FUNCTION.                
	**** CALL 'ITDMIO'               USING ITDM-PARAMS.               
	**** IF ITDM-STATUZ              NOT = O-K                        
	****    AND ITDM-STATUZ          NOT = ENDP                       
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   
	****     MOVE ITDM-STATUZ        TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** IF ITDM-ITEMCOY             NOT = COVR-CHDRCOY               
	****    OR ITDM-ITEMTABL         NOT = T5687                      
	****    OR ITDM-ITEMITEM         NOT = COVR-CRTABLE               
	****    OR ITDM-STATUZ           = ENDP                           
	****     MOVE COVR-CRTABLE       TO ITDM-ITEMITEM                 
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   
	****     MOVE ITDM-STATUZ        TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** ELSE                                                         
	****     MOVE ITDM-GENAREA       TO T5687-T5687-REC.              
	*10190-EXIT.                                                      
	**** EXIT.                                                        
	**********************************************************<A05940>
	*10300-CHECK-COMIND-ON-T6658 SECTION.                     <A05940>
	**********************************************************<A05940>
	*                                                         <A05940>
	*10300-START.                                             <A05940>
	*                                                         <A05940>
	**** MOVE SPACES                    TO ITDM-PARAMS.       <A05940>
	****                                                      <A05940>
	**** MOVE 'IT'                      TO ITDM-ITEMPFX.      <A05940>
	**** MOVE COVR-CHDRCOY              TO ITDM-ITEMCOY.      <A05940>
	**** MOVE T6658                     TO ITDM-ITEMTABL.     <A05940>
	**** MOVE COVR-CRRCD                TO ITDM-ITMFRM.       <A05940>
	**** MOVE T5687-ANNIVERSARY-METHOD  TO ITDM-ITEMITEM.     <A05940>
	**** MOVE ITDMREC                   TO ITDM-FORMAT.       <A05940>
	**** MOVE BEGN                      TO ITDM-FUNCTION.     <A05940>
	****                                                      <A05940>
	**** CALL 'ITDMIO'               USING ITDM-PARAMS.       <A05940>
	****                                                      <A05940>
	**** IF ITDM-STATUZ              NOT = O-K                <A05940>
	****    AND ITDM-STATUZ          NOT = ENDP               <A05940>
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS           <A05940>
	****     MOVE ITDM-STATUZ        TO SYSR-STATUZ           <A05940>
	****     PERFORM 99000-SYSTEM-ERROR                       <A05940>
	**** END-IF.                                              <A05940>
	****                                                      <A05940>
	**** IF ITDM-ITEMCOY             NOT = COVR-CHDRCOY       <A05940>
	****    OR ITDM-ITEMTABL         NOT = T6658              <A05940>
	****    OR ITDM-ITEMITEM         NOT = T5687-ANNIVERSARY-M<A05940>
	****    OR ITDM-STATUZ           = ENDP                   <A05940>
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS           <A05940>
	****     MOVE ITDM-STATUZ        TO SYSR-STATUZ           <A05940>
	****     PERFORM 99000-SYSTEM-ERROR                       <A05940>
	**** ELSE                                                 <A05940>
	****     MOVE ITDM-GENAREA       TO T6658-T6658-REC.      <A05940>
	*                                                         <A05940>
	*10390-EXIT.                                              <A05940>
	**** EXIT.                                                <A05940>
	*                                                         <A05940>
	* </pre>
	*/
protected void deleteAgcms10100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					check10110();
				case next10150: 
					next10150();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void check10110()
	{
		/* Do not process single premium AGCM records.                  */
		if (isEQ(agcmbcrIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.next10150);
		}
		/* Delete the AGCM record.                                      */
		agcmbcrIO.setFunction(varcom.deltd);
		agcmbcrIO.setFormat(formatsInner.agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
	}

protected void next10150()
	{
		/* Look for the next record to delete.                          */
		agcmbcrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
		if (isNE(agcmbcrIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(agcmbcrIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(agcmbcrIO.getCoverage(), covrIO.getCoverage())
		|| isNE(agcmbcrIO.getLife(), covrIO.getLife())
		|| isNE(agcmbcrIO.getRider(), covrIO.getRider())
		|| isNE(agcmbcrIO.getPlanSuffix(), covrIO.getPlanSuffix())) {
			agcmbcrIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			syserrrec.params.set(agcmbcrIO.getParams());
			systemError99000();
		}
		/* If this is a validflag '2' record, come out of the deletion  */
		/* loop and enter the reinstatement loop.                       */
		if (isEQ(agcmbcrIO.getValidflag(), "2")) {
			wsaaReinstateTranno.set(agcmbcrIO.getTranno());
		}
	}

protected void reinstateAgcms10200()
	{
		check10210();
		next10250();
	}

protected void check10210()
	{
		/* Do not process single premium AGCM records.                  */
		if (isEQ(agcmbcrIO.getPtdate(), ZERO)) {
			return ;
		}
		/* Reinstate the AGCM record.                                   */
		agcmbcrIO.setValidflag("1");
		agcmbcrIO.setCurrto(varcom.vrcmMaxDate);
		agcmbcrIO.setFunction(varcom.writd);
		agcmbcrIO.setFormat(formatsInner.agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
	}

protected void next10250()
	{
		/* Look for the next record to reinstate.                       */
		agcmbcrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)
		&& isNE(agcmbcrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
		if (isNE(agcmbcrIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(agcmbcrIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(agcmbcrIO.getCoverage(), covrIO.getCoverage())
		|| isNE(agcmbcrIO.getLife(), covrIO.getLife())
		|| isNE(agcmbcrIO.getRider(), covrIO.getRider())
		|| isNE(agcmbcrIO.getPlanSuffix(), covrIO.getPlanSuffix())
		|| isNE(agcmbcrIO.getTranno(), wsaaReinstateTranno)) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processFpco11000()
	{
		start11000();
	}

	/**
	* <pre>
	****                                                      <D9604> 
	**** Delete any FPCO created in the forward transaction an<D9604> 
	**** reinstate the previous vf2.                          <D9604> 
	****                                                      <D9604> 
	* </pre>
	*/
protected void start11000()
	{
		fpcorevIO.setChdrcoy(covrIO.getChdrcoy());
		fpcorevIO.setChdrnum(covrIO.getChdrnum());
		fpcorevIO.setLife(covrIO.getLife());
		fpcorevIO.setCoverage(covrIO.getCoverage());
		fpcorevIO.setRider(covrIO.getRider());
		fpcorevIO.setPlanSuffix(covrIO.getPlanSuffix());
		fpcorevIO.setTargfrom(varcom.vrcmMaxDate);
		/* MOVE BEGNH                  TO FPCOREV-FUNCTION.     <LA3993>*/
		fpcorevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcorevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		fpcorevIO.setFormat(formatsInner.fpcorevrec);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(fpcorevIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(fpcorevIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(fpcorevIO.getLife(), covrIO.getLife())
		|| isNE(fpcorevIO.getCoverage(), covrIO.getCoverage())
		|| isNE(fpcorevIO.getRider(), covrIO.getRider())
		|| isNE(fpcorevIO.getPlanSuffix(), covrIO.getPlanSuffix())) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			databaseError99500();
		}
		/* MOVE DELET                  TO FPCOREV-FUNCTION.     <LA3993>*/
		fpcorevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			databaseError99500();
		}
		fpcorv2IO.setChdrcoy(fpcorevIO.getChdrcoy());
		fpcorv2IO.setChdrnum(fpcorevIO.getChdrnum());
		fpcorv2IO.setLife(fpcorevIO.getLife());
		fpcorv2IO.setCoverage(fpcorevIO.getCoverage());
		fpcorv2IO.setRider(fpcorevIO.getRider());
		fpcorv2IO.setPlanSuffix(fpcorevIO.getPlanSuffix());
		fpcorv2IO.setTargfrom(fpcorevIO.getTargfrom());
		fpcorv2IO.setFormat(formatsInner.fpcorv2rec);
		fpcorv2IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			databaseError99500();
		}
		fpcorv2IO.setValidflag("1");
		fpcorv2IO.setCurrto(varcom.vrcmMaxDate);
		fpcorv2IO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			databaseError99500();
		}
	}

protected void cashDividend11100()
	{
		cashDiv11100();
	}

protected void cashDiv11100()
	{
		/* Check if HCSD record exists with the same COVR key.             */
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(covrIO.getChdrcoy());
		hcsdIO.setChdrnum(covrIO.getChdrnum());
		hcsdIO.setLife(covrIO.getLife());
		hcsdIO.setCoverage(covrIO.getCoverage());
		hcsdIO.setRider(covrIO.getRider());
		hcsdIO.setPlanSuffix(covrIO.getPlanSuffix());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)
		&& isNE(hcsdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			systemError99000();
		}
		if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)
		|| isNE(hcsdIO.getTranno(), reverserec.tranno)) {
			return ;
		}
		hcsdIO.setFunction(varcom.readh);
		hcsdio11200();
		hcsdIO.setFunction(varcom.delet);
		hcsdio11200();
		hcsdIO.setFunction(varcom.readh);
		hcsdio11200();
		hcsdIO.setValidflag("1");
		hcsdIO.setFunction(varcom.rewrt);
		hcsdio11200();
	}

protected void hcsdio11200()
	{
		/*CALL-HCSDIO*/
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			systemError99000();
		}
		/*EXIT*/
	}

protected void processRacds20000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					racd20010();
				case nextrRacd20020: 
					nextrRacd20020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void racd20010()
	{
		racdIO.setParams(SPACES);
		racdIO.setChdrcoy(reverserec.company);
		racdIO.setChdrnum(reverserec.chdrnum);
		racdIO.setPlanSuffix(ZERO);
		racdIO.setSeqno(ZERO);
		racdIO.setFormat(formatsInner.racdrec);
		racdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		racdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		racdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void nextrRacd20020()
	{
		SmartFileCode.execute(appVars, racdIO);
		if (isNE(racdIO.getStatuz(), varcom.oK)
		&& isNE(racdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdIO.getParams());
			syserrrec.statuz.set(racdIO.getStatuz());
			systemError99000();
		}
		if (isNE(racdIO.getChdrcoy(), reverserec.company)
		|| isNE(racdIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(racdIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(reverserec.tranno, racdIO.getTranno())) {
			racdrrvIO.setParams(SPACES);
			racdrrvIO.setChdrcoy(racdIO.getChdrcoy());
			racdrrvIO.setChdrnum(racdIO.getChdrnum());
			racdrrvIO.setLife(racdIO.getLife());
			racdrrvIO.setCoverage(racdIO.getCoverage());
			racdrrvIO.setRider(racdIO.getRider());
			racdrrvIO.setPlanSuffix(racdIO.getPlanSuffix());
			racdrrvIO.setSeqno(racdIO.getSeqno());
			racdrrvIO.setLrkcls(racdIO.getLrkcls());
			racdrrvIO.setTranno(racdIO.getTranno());
			racdrrvIO.setFunction(varcom.readh);
			racdrrvIO.setFormat(formatsInner.racdrrvrec);
			racdrrvIo20100();
			racdrrvIO.setFunction(varcom.delet);
			racdrrvIo20100();
			racdrrvIO.setFunction(varcom.endh);
			racdrrvIo20100();
			racdrrvIO.setFunction(varcom.rewrt);
			racdrrvIO.setValidflag("1");
			racdrrvIO.setCurrto(99999999);
			racdrrvIo20100();
		}
		racdIO.setFunction(varcom.nextr);
		goTo(GotoLabel.nextrRacd20020);
	}

protected void racdrrvIo20100()
	{
		/*RACDRRVIO*/
		SmartFileCode.execute(appVars, racdrrvIO);
		if (isNE(racdrrvIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(racdrrvIO.getParams());
			syserrrec.statuz.set(racdrrvIO.getStatuz());
			systemError99000();
		}
		/*EXIT*/
	}

protected void processLrrhs21000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					lrrh21010();
				case nextrLrrh21020: 
					nextrLrrh21020();
					continue21050();
				case exit21090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lrrh21010()
	{
		lrrhconIO.setParams(SPACES);
		lrrhconIO.setCompany(reverserec.company);
		lrrhconIO.setChdrnum(reverserec.chdrnum);
		lrrhconIO.setPlanSuffix(ZERO);
		lrrhconIO.setFormat(formatsInner.lrrhconrec);
		lrrhconIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lrrhconIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lrrhconIO.setFitKeysSearch("COMPANY", "CHDRNUM");
	}

protected void nextrLrrh21020()
	{
		SmartFileCode.execute(appVars, lrrhconIO);
		if (isNE(lrrhconIO.getStatuz(), varcom.oK)
		&& isNE(lrrhconIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lrrhconIO.getParams());
			syserrrec.statuz.set(lrrhconIO.getStatuz());
			systemError99000();
		}
		if (isNE(lrrhconIO.getCompany(), reverserec.company)
		|| isNE(lrrhconIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(lrrhconIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit21090);
		}
		if (isEQ(reverserec.tranno, lrrhconIO.getTranno())) {
			lrrhrrvIO.setParams(SPACES);
			lrrhrrvIO.setClntpfx(lrrhconIO.getClntpfx());
			lrrhrrvIO.setClntcoy(lrrhconIO.getClntcoy());
			lrrhrrvIO.setClntnum(lrrhconIO.getClntnum());
			lrrhrrvIO.setCompany(lrrhconIO.getCompany());
			lrrhrrvIO.setChdrnum(lrrhconIO.getChdrnum());
			lrrhrrvIO.setLife(lrrhconIO.getLife());
			lrrhrrvIO.setCoverage(lrrhconIO.getCoverage());
			lrrhrrvIO.setRider(lrrhconIO.getRider());
			lrrhrrvIO.setPlanSuffix(lrrhconIO.getPlanSuffix());
			lrrhrrvIO.setLrkcls(lrrhconIO.getLrkcls());
			lrrhrrvIO.setTranno(lrrhconIO.getTranno());
			lrrhrrvIO.setFunction(varcom.readh);
			lrrhrrvIO.setFormat(formatsInner.lrrhrrvrec);
			lrrhrrvIo21100();
			lrrhrrvIO.setFunction(varcom.delet);
			lrrhrrvIo21100();
			lrrhrrvIO.setFunction(varcom.endh);
			lrrhrrvIo21100();
			lrrhrrvIO.setFunction(varcom.rewrt);
			lrrhrrvIO.setValidflag("1");
			lrrhrrvIO.setCurrto(99999999);
			lrrhrrvIo21100();
		}
	}
//PINNACLE-2855
	public void processExclusion() {

		int wsaaSeqnbr=0;
		exclpfList = exclpfDAO.getExclpfRecordInReverseOrder(reverserec.company.toString(),reverserec.chdrnum.toString(), reverserec.tranno.toInt(),covrIO.life.toString(),covrIO.coverage.toString(),covrIO.rider.toString());
		if (exclpfList != null & !exclpfList.isEmpty()) {
			for (Exclpf exclpf : exclpfList) {
				if(isEQ(exclpf.getTranno(),reverserec.tranno)) {
					if (isEQ(exclpf.getValidflag(), "2")) {						
							exclpf.setValidflag("1");
							exclpf.setTranno(reverserec.newTranno.toInt());
							exclpfDAO.updateRecordToInvalidate(exclpf, true);					
						
					}else {
						exclpfDAO.deleteRecordByUniqueNumber(exclpf);
						wsaaSeqnbr = exclpf.getSeqno();
					}
				} else {
					if(isEQ(wsaaSeqnbr, exclpf.getSeqno()) & isEQ(exclpf.getValidflag(), "2")) {
						exclpf.setValidflag("1");
						exclpf.setTranno(reverserec.newTranno.toInt());
						exclpfDAO.updateRecordToInvalidate(exclpf, true);
						wsaaSeqnbr=0;
					}
				}
			}
		}

	}

protected void continue21050()
	{
		lrrhconIO.setFunction(varcom.nextr);
		goTo(GotoLabel.nextrLrrh21020);
	}

protected void lrrhrrvIo21100()
	{
		/*LRRHRRVIO*/
		SmartFileCode.execute(appVars, lrrhrrvIO);
		if (isNE(lrrhrrvIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lrrhrrvIO.getParams());
			syserrrec.statuz.set(lrrhrrvIO.getStatuz());
			systemError99000();
		}
		/*EXIT*/
	}

protected void processLirrs22000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					lirr22010();
				case nextrLirr22020: 
					nextrLirr22020();
					continue22050();
				case exit22090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lirr22010()
	{
		lirrconIO.setParams(SPACES);
		lirrconIO.setCompany(reverserec.company);
		lirrconIO.setChdrnum(reverserec.chdrnum);
		lirrconIO.setPlanSuffix(ZERO);
		lirrconIO.setFormat(formatsInner.lirrconrec);
		lirrconIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lirrconIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lirrconIO.setFitKeysSearch("COMPANY", "CHDRNUM");
	}

protected void nextrLirr22020()
	{
		SmartFileCode.execute(appVars, lirrconIO);
		if (isNE(lirrconIO.getStatuz(), varcom.oK)
		&& isNE(lirrconIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lirrconIO.getParams());
			syserrrec.statuz.set(lirrconIO.getStatuz());
			systemError99000();
		}
		if (isNE(lirrconIO.getCompany(), reverserec.company)
		|| isNE(lirrconIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(lirrconIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit22090);
		}
		if (isEQ(reverserec.tranno, lirrconIO.getTranno())) {
			lirrrrvIO.setParams(SPACES);
			lirrrrvIO.setCompany(lirrconIO.getCompany());
			lirrrrvIO.setChdrnum(lirrconIO.getChdrnum());
			lirrrrvIO.setLife(lirrconIO.getLife());
			lirrrrvIO.setCoverage(lirrconIO.getCoverage());
			lirrrrvIO.setRider(lirrconIO.getRider());
			lirrrrvIO.setPlanSuffix(lirrconIO.getPlanSuffix());
			lirrrrvIO.setRasnum(lirrconIO.getRasnum());
			lirrrrvIO.setRngmnt(lirrconIO.getRngmnt());
			lirrrrvIO.setClntpfx(lirrconIO.getClntpfx());
			lirrrrvIO.setClntcoy(lirrconIO.getClntcoy());
			lirrrrvIO.setClntnum(lirrconIO.getClntnum());
			lirrrrvIO.setTranno(lirrconIO.getTranno());
			lirrrrvIO.setFunction(varcom.readh);
			lirrrrvIO.setFormat(formatsInner.lirrrrvrec);
			lirrrrvIo22100();
			lirrrrvIO.setFunction(varcom.delet);
			lirrrrvIo22100();
			lirrrrvIO.setFunction(varcom.endh);
			lirrrrvIo22100();
			lirrrrvIO.setFunction(varcom.rewrt);
			lirrrrvIO.setValidflag("1");
			lirrrrvIO.setCurrto(99999999);
			lirrrrvIo22100();
		}
	}

protected void continue22050()
	{
		lirrconIO.setFunction(varcom.nextr);
		goTo(GotoLabel.nextrLirr22020);
	}

protected void lirrrrvIo22100()
	{
		/*LIRRRRVIO*/
		SmartFileCode.execute(appVars, lirrrrvIO);
		if (isNE(lirrrrvIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lirrrrvIO.getParams());
			syserrrec.statuz.set(lirrrrvIO.getStatuz());
			systemError99000();
		}
		/*EXIT*/
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError99500()
	{
		start99500();
		exit99990();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC   ");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covrrccrec = new FixedLengthStringData(10).init("COVRRCCREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData lextchgrec = new FixedLengthStringData(10).init("LEXTCHGREC");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData agcmbcrrec = new FixedLengthStringData(10).init("AGCMBCRREC");
	private FixedLengthStringData fprmrevrec = new FixedLengthStringData(10).init("FPRMREVREC");
	private FixedLengthStringData fpcorevrec = new FixedLengthStringData(10).init("FPCOREVREC");
	private FixedLengthStringData fpcorv2rec = new FixedLengthStringData(10).init("FPCORV2REC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData racdrec = new FixedLengthStringData(10).init("RACDREC");
	private FixedLengthStringData racdrrvrec = new FixedLengthStringData(10).init("RACDRRVREC");
	private FixedLengthStringData lrrhconrec = new FixedLengthStringData(10).init("LRRHCONREC");
	private FixedLengthStringData lrrhrrvrec = new FixedLengthStringData(10).init("LRRHRRVREC");
	private FixedLengthStringData lirrconrec = new FixedLengthStringData(10).init("LIRRCONREC");
	private FixedLengthStringData lirrrrvrec = new FixedLengthStringData(10).init("LIRRRRVREC");
}
}
