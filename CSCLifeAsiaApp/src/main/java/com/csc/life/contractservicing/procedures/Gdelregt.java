/*
 * File: Gdelregt.java
 * Date: 29 August 2009 22:49:38
 * Author: Quipoz Limited
 * 
 * Class transformed from GDELREGT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.terminationclaims.dataaccess.RegtTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*                     REGT DELETION PROGRAM.
*
* This subroutine  will be called from T5671 during the reversal
* of  a  transaction  which  created  Temporary  Regular Payment
* records,  REGTs,  for  example  during  reversal  of   Vesting
* Registration.
*
* This subroutine will delete all REGT records for the  contract
* number of the PTRN being reversed.
*
*****************************************************************
* </pre>
*/
public class Gdelregt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("GDELREGT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String regtrec = "REGTREC";
	private Greversrec greversrec = new Greversrec();
		/*Regular Payment Temporary Record*/
	private RegtTableDAM regtIO = new RegtTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Gdelregt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			initialise1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*START*/
		regtIO.setChdrcoy(greversrec.chdrcoy);
		regtIO.setChdrnum(greversrec.chdrnum);
		regtIO.setLife(greversrec.life);
		regtIO.setCoverage(greversrec.coverage);
		regtIO.setRider(greversrec.rider);
		regtIO.setSeqnbr(ZERO);
		regtIO.setRgpynum(ZERO);
		regtIO.setFormat(regtrec);
		regtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		regtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, regtIO);
		if (isNE(regtIO.getStatuz(),varcom.oK)
		&& isNE(regtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regtIO.getParams());
			syserrrec.statuz.set(regtIO.getStatuz());
			fatalError600();
		}
		while ( !(isEQ(regtIO.getStatuz(),varcom.endp)
		|| isNE(regtIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(regtIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(regtIO.getLife(),greversrec.life)
		|| isNE(regtIO.getCoverage(),greversrec.coverage)
		|| isNE(regtIO.getRider(),greversrec.rider))) {
			processRegt2000();
		}
		
		exitProgram();
	}

protected void processRegt2000()
	{
		deleteRegt2010();
	}

protected void deleteRegt2010()
	{
		regtIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, regtIO);
		if (isNE(regtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtIO.getParams());
			syserrrec.statuz.set(regtIO.getStatuz());
			fatalError600();
		}
		regtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regtIO);
		if (isNE(regtIO.getStatuz(),varcom.oK)
		&& isNE(regtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regtIO.getParams());
			syserrrec.statuz.set(regtIO.getStatuz());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
