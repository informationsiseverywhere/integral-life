package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:21
 * Description:
 * Copybook name: COVRBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrbrkFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData covrbrkKey = new FixedLengthStringData(256).isAPartOf(covrbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(covrbrkKey, 0);
  	public FixedLengthStringData covrbrkChdrnum = new FixedLengthStringData(8).isAPartOf(covrbrkKey, 1);
  	public PackedDecimalData covrbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrbrkKey, 9);
  	public FixedLengthStringData covrbrkLife = new FixedLengthStringData(2).isAPartOf(covrbrkKey, 12);
  	public FixedLengthStringData covrbrkCoverage = new FixedLengthStringData(2).isAPartOf(covrbrkKey, 14);
  	public FixedLengthStringData covrbrkRider = new FixedLengthStringData(2).isAPartOf(covrbrkKey, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(covrbrkKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}