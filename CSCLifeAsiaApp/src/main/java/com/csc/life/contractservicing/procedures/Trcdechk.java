/*
 * File: Trcdechk.java
 * Date: 30 August 2009 2:45:07
 * Author: Quipoz Limited
 * 
 * Class transformed from TRCDECHK.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tranchkrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* TRCDECHK
* ~~~~~~~~
*
* Overview
* ~~~~~~~~
* This subroutine will be called from the various On-Line Reversal
* Transaction Programs to perform a check on T6661.
*
* This check is to ensure that the most recent transaction
* performed on the contract is indeed the one we are attempting
* to reverse -
*     e.g. If the Reversal Transaction we are about to perform
*          is a Death Claim Reversal, then in order to go
*          ahead the most recent transaction should be a Death
*          Claim.
*          If it is not, then an error message is displayed.
*
* Before calling this subroutine, the most recent transaction
* is checked and if it is valid then this subroutine is not
* called.
*
* T6661 is very important to the reversal process.
* The key for T6661 is the Transaction Code and each entry gives
* the name of the reversal subroutine for the transaction, if there
* is one.
*
* A forward transaction with its own On-Line Reversal Transaction,
* e.g. Paid-Up Processing, Death Claim etc., should have an entry
* with Full Contract Reversal Flag = Spaces.
*
*
* Processing
* ~~~~~~~~~~
*
*   Read T6661 using the Transaction Code passed.
*
*   If no entry is found on T6661 for this transaction code
*      Move MRNF to subroutine status
*      Exit subroutine.
*
*   An entry has been found, check the contract reversal flag
*   and the subroutine fields.
*   If the entry is a non-significant transaction (i.e. Contract
*   Reversal Flag is 'N' and there are no subroutine entries) -
*   set the subroutine status to O-K and Exit.
*   Otherwise, set the subroutine status to MRNF.
*
*****************************************************************
* </pre>
*/
public class Trcdechk extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
		/* TABLES */
	private String t6661 = "T6661";
		/* FORMATS */
	private String itemrec = "ITEMREC";
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6661rec t6661rec = new T6661rec();
	private Tranchkrec tranchkrec = new Tranchkrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit9120
	}

	public Trcdechk() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tranchkrec.codeCheckRec = convertAndSetParam(tranchkrec.codeCheckRec, parmArray, 0);
		try {
			checkT6661100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void checkT6661100()
	{
		para110();
		exit190();
	}

protected void para110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(tranchkrec.tcdeCompany);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(tranchkrec.tcdeTranCode);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		if (isEQ(t6661rec.contRevFlag,"N")) {
			tranchkrec.tcdeStatuz.set(varcom.oK);
		}
		else {
			tranchkrec.tcdeStatuz.set(varcom.mrnf);
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9110();
				}
				case exit9120: {
					exit9120();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9110()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9120);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9120()
	{
		tranchkrec.tcdeStatuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
