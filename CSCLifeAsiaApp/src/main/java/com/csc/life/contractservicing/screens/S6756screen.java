package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6756screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6756ScreenVars sv = (S6756ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6756screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6756ScreenVars screenVars = (S6756ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.contractType.setClassString("");
		screenVars.shortds02.setClassString("");
		screenVars.shortds01.setClassString("");
		screenVars.shortds03.setClassString("");
		screenVars.cnstcur.setClassString("");
		screenVars.efdateDisp.setClassString("");
		screenVars.tranCode.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
	}

/**
 * Clear all the variables in S6756screen
 */
	public static void clear(VarModel pv) {
		S6756ScreenVars screenVars = (S6756ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.contractType.clear();
		screenVars.shortds02.clear();
		screenVars.shortds01.clear();
		screenVars.shortds03.clear();
		screenVars.cnstcur.clear();
		screenVars.efdateDisp.clear();
		screenVars.efdate.clear();
		screenVars.tranCode.clear();
		screenVars.descrip.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
	}
}
