/*
 * File: Pr51k.java
 * Date: 30 August 2009 1:36:47
 * Author: Quipoz Limited
 * 
 * Class transformed from PR51K.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List; //IBPLIFE-6053

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.screens.Sr51kScreenVars;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO; //IBPLIFE-6053
import com.csc.life.newbusiness.dataaccess.model.Covtpf; //IBPLIFE-6053
import com.csc.life.productdefinition.procedures.Vlpdrule;
import com.csc.life.productdefinition.recordstructures.Vlpdrulrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Errmesg;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The program will process cross check validation(V4) by call
* subroutine 'VLPDRULE'
*   - If has any error then add all error in subfile
*     and not allow to press enter to process next program
*
***********************************************************************
* </pre>
*/
public class Pr51k extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR51K");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaError = "";
	private PackedDecimalData wsaaIxc = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaIyc = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaProdError = new FixedLengthStringData(24);
	private FixedLengthStringData wsaaProdtyp = new FixedLengthStringData(4).isAPartOf(wsaaProdError, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaProdError, 4, FILLER).init(" ");
	private FixedLengthStringData wsaaProdErr = new FixedLengthStringData(19).isAPartOf(wsaaProdError, 5);
	private int wsaaErrCount = 0;
	private String wsaaAddChg = "";
		/* ERRORS */
	private String rlbm = "RLBM";
		/* TABLES */
	private String t5688 = "T5688";
		/* FORMATS */
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/

		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Errmesgrec errmesgrec = new Errmesgrec();
	private Vlpdrulrec vlpdrulrec = new Vlpdrulrec();
	private Batckey wsaaBatckey = new Batckey();
	private Sr51kScreenVars sv = ScreenProgram.getScreenVars( Sr51kScreenVars.class);
	
	//ILB-456 start 
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class); //IBPLIFE-6053

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit
	}

	public Pr51k() {
		super();
		screenVars = sv;
		new ScreenModel("Sr51k", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		clearSubfile1100();
		retrvChdrmja1200();
		readCovtmja1600();
		if (isEQ(wsaaAddChg,"N")) {
			goTo(GotoLabel.exit1090);
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		readT56881500();
		validateProduct1300();
	}

protected void clearSubfile1100()
	{
		begin1110();
	}

protected void begin1110()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaError = "N";
		wsaaAddChg = "Y";
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR51K", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvChdrmja1200()
	{
		/*BEGIN*/
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
	
		/*EXIT*/
	}

protected void validateProduct1300()
	{
		begin1310();
	}

protected void begin1310()
	{
		initialize(vlpdrulrec.validRec);
		vlpdrulrec.function.set("ADCH");
		vlpdrulrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		vlpdrulrec.chdrcoy.set(chdrpf.getChdrcoy());
		vlpdrulrec.chdrnum.set(chdrpf.getChdrnum());
		vlpdrulrec.cnttype.set(chdrpf.getCnttype());
		vlpdrulrec.srcebus.set(chdrpf.getSrcebus());
		vlpdrulrec.effdate.set(chdrpf.getPtdate());
		vlpdrulrec.cownnum.set(chdrpf.getCownnum());
		vlpdrulrec.billfreq.set(chdrpf.getBillfreq());
		vlpdrulrec.language.set(wsspcomn.language);
		callProgram(Vlpdrule.class, vlpdrulrec.validRec);
		if (isNE(vlpdrulrec.statuz,varcom.oK)) {
			syserrrec.subrname.set("VLPDRULE");
			syserrrec.params.set(vlpdrulrec.validRec);
			syserrrec.statuz.set(vlpdrulrec.statuz);
			fatalError600();
		}
		for (wsaaIxc.set(1); !(isGT(wsaaIxc,50)); wsaaIxc.add(1)){
			if (isEQ(vlpdrulrec.errLife[wsaaIxc.toInt()],SPACES)
			&& isEQ(vlpdrulrec.errCnttype[wsaaIxc.toInt()],SPACES)) {
				wsaaIxc.set(51);
			}
			else {
				for (wsaaIyc.set(1); !(isGT(wsaaIyc,20)); wsaaIyc.add(1)){
					if (isEQ(vlpdrulrec.errCode[wsaaIxc.toInt()][wsaaIyc.toInt()],SPACES)) {
						wsaaIyc.set(21);
					}
					else {
						errmesgrec.errmesgRec.set(SPACES);
						errmesgrec.eror.set(vlpdrulrec.errCode[wsaaIxc.toInt()][wsaaIyc.toInt()]);
						productErrorMessage1400();
					}
				}
			}
		}
	}

protected void productErrorMessage1400()
	{
		error1410();
	}

protected void error1410()
	{
		wsaaError = "Y";
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaProg);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.set(SPACES);
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		sv.life.set(vlpdrulrec.errLife[wsaaIxc.toInt()]);
		sv.jlife.set(vlpdrulrec.errJlife[wsaaIxc.toInt()]);
		sv.coverage.set(vlpdrulrec.errCoverage[wsaaIxc.toInt()]);
		sv.rider.set(vlpdrulrec.errRider[wsaaIxc.toInt()]);
		sv.payrseqno.set(ZERO);
		if (isEQ(vlpdrulrec.errDet[wsaaIxc.toInt()][wsaaIyc.toInt()],SPACES)) {
			sv.erordsc.set(errmesgrec.errmesg[1]);
		}
		else {
			wsaaProdtyp.set(vlpdrulrec.errDet[wsaaIxc.toInt()][wsaaIyc.toInt()]);
			wsaaProdErr.set(errmesgrec.errmesg[1]);
			sv.erordsc.set(wsaaProdError);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR51K", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readT56881500()
	{
		begin1510();
	}

protected void begin1510()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrpf.getChdrcoy());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
	}

protected void readCovtmja1600()
	{
		begin1610();
	}

protected void begin1610()
	{
 //IBPLIFE-6053 start, modified begn DAM call to DAO call.	
		List<Covtpf> covtpfList = null;
		covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(covtpfList.isEmpty()) {
			wsaaAddChg = "N";
		}
 //IBPLIFE-6053 end
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsaaAddChg,"N")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsaaError,"N")) {
			wsspcomn.sectionno.set("3000");
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		if (isNE(wsaaError,"N")) {
			sv.chdrnumErr.set(rlbm);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		chdrpfDAO.setCacheObject(chdrpf);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}