package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:59
 * Description:
 * Copybook name: AGCMDBCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmdbckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmdbcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmdbcKey = new FixedLengthStringData(64).isAPartOf(agcmdbcFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmdbcChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmdbcKey, 0);
  	public FixedLengthStringData agcmdbcChdrnum = new FixedLengthStringData(8).isAPartOf(agcmdbcKey, 1);
  	public FixedLengthStringData agcmdbcAgntnum = new FixedLengthStringData(8).isAPartOf(agcmdbcKey, 9);
  	public FixedLengthStringData agcmdbcLife = new FixedLengthStringData(2).isAPartOf(agcmdbcKey, 17);
  	public FixedLengthStringData agcmdbcCoverage = new FixedLengthStringData(2).isAPartOf(agcmdbcKey, 19);
  	public FixedLengthStringData agcmdbcRider = new FixedLengthStringData(2).isAPartOf(agcmdbcKey, 21);
  	public PackedDecimalData agcmdbcPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmdbcKey, 23);
  	public PackedDecimalData agcmdbcSeqno = new PackedDecimalData(2, 0).isAPartOf(agcmdbcKey, 26);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(agcmdbcKey, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmdbcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmdbcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}