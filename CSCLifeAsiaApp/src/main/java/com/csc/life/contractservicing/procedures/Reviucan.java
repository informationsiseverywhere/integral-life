/*
 * File: Reviucan.java
 * Date: 30 August 2009 2:09:33
 * Author: Quipoz Limited
 * 
 * Class transformed from REVIUCAN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* REVIUCAN - Initial Unit Cancellation Reversal Subroutine.
* ---------------------------------------------------------
*
* This subroutine will be called from the Reversal Routines Table
* (T6661) when reversing the effects of an Initial Unit
* Cancellation on a component within a contract.
*
* It will reverse the updates to the coverage file changed during
* Initial Unit Cancellation processing.
*
* For each coverage record to be updated, read T5671 and call any
* any generic reversal subroutines found.  The table should be
* set up to call GREVUTRN to delete/reverse UTRNs created by
* Initial Unit Cancellation.
*
* Delete any valid flag '1' coverage records created during
* Initial Unit Cancellation and reinstate the valid flag '2'
* record.
*
*****************************************************************
*
* </pre>
*/
public class Reviucan extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "REVIUCAN";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaBatchkey = new FixedLengthStringData(65);
	private FixedLengthStringData wsaaContkey = new FixedLengthStringData(19).isAPartOf(wsaaBatchkey, 0);
	private FixedLengthStringData wsaaBatcpfx = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 0);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaContkey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaContkey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaContkey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaContkey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaContkey, 14);
	private FixedLengthStringData wsaaOldBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatchkey, 19);
		/* TABLES */
	private String t5671 = "T5671";
		/* FORMATS */
	private String covrenqrec = "COVRENQREC";
	private String covrrec = "COVRREC";
	private String itemrec = "ITEMREC";
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Greversrec greversrec = new Greversrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Reverserec reverserec = new Reverserec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2349, 
		errorProg610
	}

	public Reviucan() {
		super();
	}

public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		start1010();
		exit1090();
	}

protected void start1010()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set("TDAY");
		datcon1rec.intDate.set(ZERO);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		processCovrs2000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processCovrs2000()
	{
		begn2010();
	}

protected void begn2010()
	{
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setLife(ZERO);
		covrenqIO.setCoverage(ZERO);
		covrenqIO.setRider(ZERO);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),reverserec.company)
		|| isNE(covrenqIO.getChdrnum(),reverserec.chdrnum)) {
			covrenqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			reverseCovrs2100();
		}
		
	}

protected void reverseCovrs2100()
	{
		check2110();
	}

protected void check2110()
	{
		if (isEQ(covrenqIO.getTranno(),reverserec.tranno)) {
			deleteAndReinstate2200();
			genericProcessing2300();
		}
		covrenqIO.setFunction(varcom.nextr);
		covrenqIO.setFormat(covrenqrec);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),reverserec.company)
		|| isNE(covrenqIO.getChdrnum(),reverserec.chdrnum)) {
			covrenqIO.setStatuz(varcom.endp);
		}
	}

protected void deleteAndReinstate2200()
	{
		begnh2210();
	}

protected void begnh2210()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(covrenqIO.getChdrcoy());
		covrIO.setChdrnum(covrenqIO.getChdrnum());
		covrIO.setLife(covrenqIO.getLife());
		covrIO.setCoverage(covrenqIO.getCoverage());
		covrIO.setRider(covrenqIO.getRider());
		covrIO.setPlanSuffix(covrenqIO.getPlanSuffix());
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(),covrenqIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),covrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(),covrenqIO.getLife())
		|| isNE(covrIO.getCoverage(),covrenqIO.getCoverage())
		|| isNE(covrIO.getRider(),covrenqIO.getRider())
		|| isNE(covrIO.getPlanSuffix(),covrenqIO.getPlanSuffix())) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		covrIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		reinstateCovr2400();
	}

protected void genericProcessing2300()
	{
		try {
			readr2310();
		}
		catch (GOTOException e){
		}
	}

protected void readr2310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			goTo(GotoLabel.exit2349);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		wsaaBatchkey.set(reverserec.batchkey);
		wsaaOldBatctrcde.set(reverserec.oldBatctrcde);
		greversrec.batckey.set(wsaaBatchkey);
		greversrec.effdate.set(datcon1rec.intDate);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callTrevsubs2350();
		}
	}

protected void callTrevsubs2350()
	{
		/*CALL*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz,varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void reinstateCovr2400()
	{
		nextr2410();
	}

protected void nextr2410()
	{
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(),covrenqIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),covrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(),covrenqIO.getLife())
		|| isNE(covrIO.getCoverage(),covrenqIO.getCoverage())
		|| isNE(covrIO.getRider(),covrenqIO.getRider())
		|| isNE(covrIO.getPlanSuffix(),covrenqIO.getPlanSuffix())) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		reverserec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
