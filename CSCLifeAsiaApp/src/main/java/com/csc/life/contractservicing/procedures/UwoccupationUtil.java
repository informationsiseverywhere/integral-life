package com.csc.life.contractservicing.procedures;

import java.util.List;

import com.csc.fsu.general.dataaccess.model.Chdrpf;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;

public interface UwoccupationUtil {

	public List<String> getUnacceptableContracts(List<Chdrpf> contractList, String company,
			UWOccupationRec uwOccupationRec, String fsuco, UWOccupationRec origUWOccupationRec);
	
	/**
	 * 
	 * @param uwOccupationRec use to pass indusType,transEffdate,occupCode
	 * @param company
	 * @param fsuco
	 * @param cnttype
	 * @return 2:Decline,1:Conditionally Accepted,0:Accepted
	 */
	public int getUWResult(UWOccupationRec uwOccupationRec, String company, String fsuco,
			String cnttype, String crtable);
	
	UWOccupationRec getUWOccupationRec(UWOccupationRec uwOccupationRec, String company, String fsuco,
			String cnttype, String crtable);

}
