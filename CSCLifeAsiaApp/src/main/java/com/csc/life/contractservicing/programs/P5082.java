/*
 * File: P5082.java
 * Date: 30 August 2009 0:04:51
 * Author: Quipoz Limited
 *
 * Class transformed from P5082.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.screens.S5082ScreenVars;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*                    SUPPRESS BILLING PROCESSING
*
* This   is   a  minor  alteration,  to  suppress  the  Overdue
* Processing on the contract header (it uses the old APL field)
* with details captured from screen/program P5082.
*
*Initialise
*----------
*
* Read CHDRMNA (RETRV)  in  order to obtain the contract header
* information.
*
* Read  the  DESCIO  in  order to obtain the description of the
* risk and premium statuses.
*
* Read the client's details and format any names required using
* the relevant copybook.
*
*
*Validation
*----------
*
* If KILL is requested, then skip the validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
* Date check.
*
*  - From Date must not be less than the CCD
*  - From Date must not be greater than the To Date
*  - To Date must not be greater than the expiry-date
*  - ensure that the 'To Date' is not greater than
*    SINSTTO on the Contract header.
*
* The SINSTTO field is not a LIFE requirement
* so any reference to this has been removed.
*
* Reason field check
*
*  - if the dates  have  changed and the reason code or the
*       reason description  are equal to spaces, then is an
*       error as  the reason code and description must also
*       change.
*
*  - if the  reason  code  is  supplied  and  there  is  no
*       description, then access table T5500 and output the
*       long description.  If the reason code is not found,
*       this is an error.
*
*
*Updating
*--------
*
* Updating occurs  with  the  updating  of  the contract header
* record, the creation of a Reason record and the creation of a
* Policy transaction (PTRN) record.
*
* If the 'KILL' function key was pressed, skip the updating.
*
*CONTRACT HEADER UPDATE
*
*  - if there were  no  changes,  then  do  not  update the
*       contract header record.
*
*  - if the current  'From  and  To Dates' are equal to the
*       VRCM-MAX-DATE,  then zeroise the Suppress-Billing(from
*       and  to)  date  fields  and  set  the  Suppress-Billing
*       indicator  to  'N'. Otherwise, set the Suppress-Billing
*       date  to   the   current   'From   and   To'  dates
*       respectively  and set the Suppress-Billing indicator to
*       'Y'.
*
*  - rewrite the contract header record.
*
* CREATE REASON RECORD
*
*  - create  a  Reason details record (RESNLNB) in order to
*       hold   the   description   and   reason   for   the
*       suppression of the billing processing.
*
* GENERAL HOUSEKEEPING
*
*       - Write a PTRN record (if updating the CHDR):
*
*           - contract key from contract header
*           - transaction number from WSSP
*           - transaction effective date from VRCM-DATE
*           - batch key information WSSP batchkey
*
*      - Update the header by calling BATCUP with a function of
*           SPACES and the following parameters:
*
*           - transaction count equals 1
*           - all other amounts equal to zero
*           - batch key from WSSP
*
*      - Release contract SFTLOCK.
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5082 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5082");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaOrigReasoncd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaOrigResndesc = new FixedLengthStringData(50);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private static final int wsaaLastTranno = 0;
	private String wsaaWriteLetter = "N";

		/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private FixedLengthStringData wsaaOkeyBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaLetokeys, 1);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 5).setUnsigned();
		/* ERRORS */
	private static final String e017 = "E017";
	private static final String e304 = "E304";
	private static final String f515 = "F515";
	private static final String e374 = "E374";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5688 = "T5688";
	private static final String t5500 = "T5500";
	private static final String tr384 = "TR384";
	private static final String t7508 = "T7508";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	protected ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private ResnTableDAM resnIO = new ResnTableDAM();
	private ResnenqTableDAM resnenqIO = new ResnenqTableDAM();
	private Batckey wsaaBatcKey = new Batckey();
	private T7508rec t7508rec = new T7508rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Wssplife wssplife = new Wssplife();
	protected S5082ScreenVars sv = ScreenProgram.getScreenVars( S5082ScreenVars.class);
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkToDate2015,
		reascd2050,
		resndesc2070,
		readTr3843220
	}

	public P5082() {
		super();
		screenVars = sv;
		new ScreenModel("S5082", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
	}

protected void initialise1010()
	{
		wsaaBatcKey.set(wsspcomn.batchkey);
		/*    IF  WSAA-TODAY NOT = ZEROS                                   */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		/* Retrieve contract fields from I/O module*/
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		/* Read the PAYR file                                              */
		payrIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmnaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/*  Load all fields from the Contract Header to the Screen*/
		sv.cownnum.set(chdrmnaIO.getCownnum());
		/* Correction of amendment tagging - this next line should have    */
		/*  been change <005>, not <004> - it is now 007                   */
		/* MOVE CHDRMNA-CNTTYPE        TO S5082-CNTTYPE.           <004>*/
		sv.cnttype.set(chdrmnaIO.getCnttype());
		sv.occdate.set(chdrmnaIO.getOccdate());
		/* MOVE CHDRMNA-PTDATE         TO S5082-PTDATE.                 */
		/* MOVE CHDRMNA-BTDATE         TO S5082-BTDATE.                 */
		/* MOVE CHDRMNA-BILLFREQ       TO S5082-BILLFREQ.               */
		/* MOVE CHDRMNA-BILLCHNL       TO S5082-MOP.                    */
		sv.ptdate.set(payrIO.getPtdate());
		sv.btdate.set(payrIO.getBtdate());
		sv.billfreq.set(payrIO.getBillfreq());
		sv.mop.set(payrIO.getBillchnl());
		sv.srcebus.set(chdrmnaIO.getSrcebus());
		sv.reptype.set(chdrmnaIO.getReptype());
		sv.register.set(chdrmnaIO.getRegister());
		/*MOVE CHDRMNA-INSTTOT06      TO S5082-INSTPRAMT.              */
		sv.instpramt.set(payrIO.getSinstamt06());
		/* MOVE CHDRMNA-CNTCURR        TO S5082-CNTCURR.                */
		/* MOVE CHDRMNA-APLSUPR        TO S5082-LAPIND.                 */
		/* IF  CHDRMNA-APLSPFROM    =  ZEROS                            */
		/*     MOVE VRCM-MAX-DATE      TO S5082-LAPSFROM                */
		/*                                S5082-LAPSTO                  */
		/* ELSE                                                         */
		/*     MOVE CHDRMNA-APLSPFROM  TO S5082-LAPSFROM                */
		/*     MOVE CHDRMNA-APLSPTO    TO S5082-LAPSTO.                 */
		/* MOVE CHDRMNA-BILLSUPR       TO S5082-BILLIND.                */
		/* IF  CHDRMNA-BILLSPFROM   =  ZEROS                            */
		/*     MOVE VRCM-MAX-DATE      TO S5082-CURRFROM                */
		/*                                S5082-CURRTO                  */
		/* ELSE                                                         */
		/*     MOVE CHDRMNA-BILLSPFROM TO S5082-CURRFROM                */
		/*     MOVE CHDRMNA-BILLSPTO   TO S5082-CURRTO.                 */
		/* MOVE CHDRMNA-NOTSSUPR       TO S5082-NOTIND.                 */
		/* IF  CHDRMNA-NOTSSPFROM   =  ZEROS                            */
		/*     MOVE VRCM-MAX-DATE      TO S5082-NOTSFROM                */
		/*                                S5082-NOTSTO                  */
		/* ELSE                                                         */
		/*     MOVE CHDRMNA-NOTSSPFROM TO S5082-NOTSFROM                */
		/*     MOVE CHDRMNA-NOTSSPTO   TO S5082-NOTSTO.                 */
		sv.cntcurr.set(payrIO.getCntcurr());
		sv.lapind.set(payrIO.getAplsupr());
		if (isEQ(payrIO.getAplspfrom(), ZERO)) {
			sv.lapsfrom.set(varcom.vrcmMaxDate);
			sv.lapsto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.lapsfrom.set(payrIO.getAplspfrom());
			sv.lapsto.set(payrIO.getAplspto());
		}
		sv.billind.set(payrIO.getBillsupr());
		if (isEQ(payrIO.getBillspfrom(), ZERO)) {
			sv.currfrom.set(varcom.vrcmMaxDate);
			sv.currto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.currfrom.set(payrIO.getBillspfrom());
			sv.currto.set(payrIO.getBillspto());
		}
		sv.notind.set(payrIO.getNotssupr());
		if (isEQ(payrIO.getNotsspfrom(), ZERO)) {
			sv.notsfrom.set(varcom.vrcmMaxDate);
			sv.notsto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.notsfrom.set(payrIO.getNotsspfrom());
			sv.notsto.set(payrIO.getNotsspto());
		}
		sv.renind.set(chdrmnaIO.getRnwlsupr());
		if (isEQ(chdrmnaIO.getRnwlspfrom(), ZERO)) {
			sv.rnwlfrom.set(varcom.vrcmMaxDate);
			sv.rnwlto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.rnwlfrom.set(chdrmnaIO.getRnwlspfrom());
			sv.rnwlto.set(chdrmnaIO.getRnwlspto());
		}
		sv.comind.set(chdrmnaIO.getCommsupr());
		if (isEQ(chdrmnaIO.getCommspfrom(), ZERO)) {
			sv.commfrom.set(varcom.vrcmMaxDate);
			sv.commto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.commfrom.set(chdrmnaIO.getCommspfrom());
			sv.commto.set(chdrmnaIO.getCommspto());
		}
		sv.bnsind.set(SPACES);
		sv.bnsfrom.set(varcom.vrcmMaxDate);
		sv.bnsto.set(varcom.vrcmMaxDate);
		/*  Look up premium status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		/* MOVE CHDRMNA-PSTATCODE      TO DESC-DESCITEM.                */
		descIO.setDescitem(payrIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/*  Look up Risk status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*   Look up all other descriptions*/
		/*       - owner name*/
		if (isNE(sv.cownnum, SPACES)) {
			formatClientName1700();
		}
		formatResnDetails5000();
	}

protected void formatClientName1700()
	{
		readClientRecord1710();
	}

protected void readClientRecord1710()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.cownnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
				case checkToDate2015:
				case reascd2050:
					reascd2050();
				case resndesc2070:
					resndesc2070();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5082IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5082-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*    Check dates entered are vaild*/
		if (isLT(sv.currfrom, sv.occdate)) {
			sv.currfromErr.set(f515);
			goTo(GotoLabel.checkToDate2015);
		}
		if (isGT(sv.currfrom, sv.currto)) {
			sv.currfromErr.set(e017);
		}
		if (isEQ(sv.currfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.currto, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.reascd2050);
		}
	}

protected void reascd2050()
	{
		/* If the reason code is equal to spaces, skip the read of      */
		/* T5500.                                                       */
		if (isEQ(sv.reasoncd, SPACES)) {
			goTo(GotoLabel.resndesc2070);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5500);
		descIO.setDescitem(sv.reasoncd);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/* IF S5082-RESNDESC           = SPACES                         */
		/*    IF DESC-STATUZ              = O-K                         */
		/*       MOVE DESC-LONGDESC       TO S5082-RESNDESC             */
		/*    ELSE                                                      */
		/*       MOVE '?'                 TO S5082-RESNDESC.            */
		if (isEQ(descIO.getStatuz(), varcom.oK)
		&& isEQ(sv.resndesc, SPACES)) {
			sv.resndesc.set(descIO.getLongdesc());
		}
		/* A reason code must not be entered if the suppression is      */
		/* being removed.                                               */
		if (isEQ(sv.currfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.currto, varcom.vrcmMaxDate)) {
			sv.reasoncdErr.set(e374);
		}
	}

protected void resndesc2070()
	{
		/* A reason description must not be entered if the suppression  */
		/* is being removed.                                            */
		if (isNE(sv.resndesc, SPACES)
		&& isEQ(sv.currfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.currto, varcom.vrcmMaxDate)) {
			sv.resndescErr.set(e374);
		}
		/*CHECK-FOR-ERRORS*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
		writeReasncd3050();
		relsfl3070();
	}

protected void updateDatabase3010()
	{
		/*   No updating if no changes made.*/
		/* IF  S5082-CURRFROM =          CHDRMNA-BILLSPFROM  AND        */
		/*     S5082-CURRTO   =          CHDRMNA-BILLSPTO               */
		/*     GO TO 3070-RELSFL.                                       */
		/*    IF  S5082-CURRFROM =          PAYR-BILLSPFROM     AND<LA4261>*/
		/*        S5082-CURRTO   =          PAYR-BILLSPTO          <LA4261>*/
		/*       AND S5082-REASONCD        = WSAA-ORIG-REASONCD    <LA4261>*/
		/*       AND S5082-RESNDESC        = WSAA-ORIG-RESNDESC    <LA4261>*/
		/*        GO TO 3070-RELSFL.                               <LA4261>*/
		/*    IF S5082-CURRFROM            = VRCM-MAX-DATE         <LA4261>*/
		/*       AND S5082-CURRTO          = VRCM-MAX-DATE         <LA4261>*/
		/*       AND PAYR-BILLSPFROM       = ZEROES                <LA4261>*/
		/*       AND PAYR-BILLSPTO         = ZEROES                <LA4261>*/
		/*        GO TO 3070-RELSFL                                <LA4261>*/
		/*    END-IF.                                              <LA4261>*/
		/*3030-KEEP-AND-WRITS-CHDR.                                        */
		/* IF  S5082-CURRFROM =           VRCM-MAX-DATE  AND            */
		/*     S5082-CURRTO   =           VRCM-MAX-DATE                 */
		/*     MOVE ZEROS TO CHDRMNA-BILLSPFROM                         */
		/*                   CHDRMNA-BILLSPTO                           */
		/*     MOVE 'N'   TO CHDRMNA-BILLSUPR                           */
		/* ELSE                                                         */
		/*     MOVE S5082-CURRFROM      TO CHDRMNA-BILLSPFROM           */
		/*     MOVE S5082-CURRTO        TO CHDRMNA-BILLSPTO             */
		/*     MOVE 'Y'                 TO CHDRMNA-BILLSUPR.            */
		/* MOVE WSSP-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO VRCM-COMP-TERMID.             */
		/* MOVE VRCM-TRANID-N          TO VRCM-COMP-TRANID-N.           */
		/* MOVE VRCM-COMP-TRANID       TO CHDRMNA-TRANID.               */
		/*  Update contract header fields as follows*/
		/* MOVE KEEPS                  TO CHDRMNA-FUNCTION.             */
		/* MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.               */
		/* CALL 'CHDRMNAIO' USING CHDRMNA-PARAMS.                       */
		/* IF CHDRMNA-STATUZ           NOT = O-K                        */
		/*    MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* MOVE WRITS                  TO CHDRMNA-FUNCTION.             */
		/* MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.               */
		/* CALL 'CHDRMNAIO' USING CHDRMNA-PARAMS.                       */
		/* IF CHDRMNA-STATUZ           NOT = O-K                        */
		/*    MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* Instead of overwrite CHDR record, update this record with       */
		/* validflag 2 and write new record with new information.          */
		/*    MOVE RLSE                   TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/*    MOVE READH                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/*    MOVE CHDRMNA-TRANNO         TO WSAA-LAST-TRANNO.     <LA4261>*/
		/*    MOVE '2'                    TO CHDRMNA-VALIDFLAG.    <LA4261>*/
		/*    MOVE REWRT                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/* Write new record.                                               */
		/*    MOVE '1'                    TO CHDRMNA-VALIDFLAG.    <LA4261>*/
		/*    ADD   1                     TO CHDRMNA-TRANNO.       <LA4261>*/
		/*    MOVE WSSP-TRANID            TO VRCM-TRANID.          <LA4261>*/
		/*    MOVE VRCM-TERMID            TO VRCM-COMP-TERMID.     <LA4261>*/
		/*    MOVE VRCM-TRANID-N          TO VRCM-COMP-TRANID-N.   <LA4261>*/
		/*    MOVE VRCM-COMP-TRANID       TO CHDRMNA-TRANID.       <LA4261>*/
		/*    IF S5082-CURRFROM       NOT = SPACES  AND            <LA4261>*/
		/*       S5082-CURRFROM       NOT = CHDRMNA-BILLSPFROM     <LA4261>*/
		/*          MOVE 'Y'                TO WSAA-WRITE-LETTER   <LA4261>*/
		/*          MOVE 'Y'                TO CHDRMNA-BILLSUPR    <LA4261>*/
		/*          MOVE S5082-CURRFROM     TO CHDRMNA-BILLSPFROM. <LA4261>*/
		/*    IF S5082-CURRTO         NOT = SPACES  AND            <LA4261>*/
		/*       S5082-CURRTO         NOT = CHDRMNA-BILLSPTO       <LA4261>*/
		/*          MOVE 'Y'                TO WSAA-WRITE-LETTER   <LA4261>*/
		/*          MOVE 'Y'                TO CHDRMNA-BILLSUPR    <LA4261>*/
		/*          MOVE S5082-CURRTO       TO CHDRMNA-BILLSPTO.   <LA4261>*/
		/*    MOVE WRITR                  TO CHDRMNA-FUNCTION.     <LA4261>*/
		/*    MOVE CHDRMNAREC             TO CHDRMNA-FORMAT.       <LA4261>*/
		/*    CALL 'CHDRMNAIO'         USING CHDRMNA-PARAMS.       <LA4261>*/
		/*    IF CHDRMNA-STATUZ           NOT = O-K                <LA4261>*/
		/*       MOVE CHDRMNA-PARAMS      TO SYSR-PARAMS           <LA4261>*/
		/*       PERFORM 600-FATAL-ERROR.                          <LA4261>*/
		/*                                                         <LA4261>*/
		/* Instead of create new record, only update existing CHDR         */
		/* record.                                                         */
		if (isEQ(sv.currfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.currto, varcom.vrcmMaxDate)) {
			chdrmnaIO.setBillspfrom(ZERO);
			chdrmnaIO.setBillspto(ZERO);
			chdrmnaIO.setBillsupr("N");
		}
		else {
			chdrmnaIO.setBillspfrom(sv.currfrom);
			chdrmnaIO.setBillspto(sv.currto);
			wsaaWriteLetter = "Y";
			chdrmnaIO.setBillsupr("Y");
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		/*  Update contract header fields as follows                       */
		chdrmnaIO.setFunction(varcom.keeps);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(formatsInner.chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaWriteLetter, "Y")) {
			writeLetter3200();
		}
		/* Update suppression details on the PAYR file.                    */
		if (isEQ(sv.currfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.currto, varcom.vrcmMaxDate)) {
			payrIO.setBillspfrom(ZERO);
			payrIO.setBillspto(ZERO);
			payrIO.setBillsupr("N");
		}
		else {
			payrIO.setBillspfrom(sv.currfrom);
			payrIO.setBillspto(sv.currto);
			payrIO.setBillsupr("Y");
		}
		payrIO.setFunction(varcom.updat);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Write a PTRN record.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		wsaaBatcKey.set(wsspcomn.batchkey);
		ptrnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmnaIO.getChdrnum());
		ptrnIO.setTranno(chdrmnaIO.getTranno());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		/* MOVE VRCM-TERM              TO PTRN-TERMID.                  */
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		/* Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsspcomn.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.statuz.set(ZERO);
		batcuprec.function.set(SPACES);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
	}

protected void writeReasncd3050()
	{
		/* If the Suppression originally had a reason record, delete it */
		/* prior to writing a new reason record (if a new one is        */
		/* necessary).  This will ensure that reason records are        */
		/* deleted when the reason fields are blanked out and updated   */
		/* when a reason field or the suppression dates are changed.    */
		if (isNE(wsaaOrigReasoncd, SPACES)
		|| isNE(wsaaOrigResndesc, SPACES)) {
			deleteReason3100();
		}
		/* Create a RESN record if reason code or narrative was entered*/
		if (isNE(sv.resndesc, SPACES)
		|| isNE(sv.reasoncd, SPACES)) {
			resnIO.setDataKey(SPACES);
			resnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
			resnIO.setChdrnum(chdrmnaIO.getChdrnum());
			resnIO.setTranno(chdrmnaIO.getTranno());
			resnIO.setReasoncd(sv.reasoncd);
			resnIO.setResndesc(sv.resndesc);
			resnIO.setTrancde(wsaaBatcKey.batcBatctrcde);
			resnIO.setTransactionDate(varcom.vrcmDate);
			resnIO.setTransactionTime(varcom.vrcmTime);
			resnIO.setUser(varcom.vrcmUser);
			resnIO.setFormat(formatsInner.resnrec);
			resnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, resnIO);
			if (isNE(resnIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(resnIO.getParams());
				fatalError600();
			}
		}
		dryProcessing8000();
	}

protected void relsfl3070()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		/*    MOVE WSAA-BATC-BATCTRCDE    TO SFTL-TRANSACTION.             */
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void deleteReason3100()
	{
		begn3110();
	}

protected void begn3110()
	{
		/* Retrieve the record to be deleted using the RESNENQ logical  */
		/* since this includes the transaction code and ensures that    */
		/* the reason record picked up will be for overdue suppression. */
		resnenqIO.setParams(SPACES);
		resnenqIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		resnenqIO.setChdrnum(chdrmnaIO.getChdrnum());
		resnenqIO.setTrancde(wsaaBatcKey.batcBatctrcde);
		resnenqIO.setTranno(99999);
		resnenqIO.setFormat(formatsInner.resnenqrec);
		resnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		resnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		resnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANCDE");
		SmartFileCode.execute(appVars, resnenqIO);
		if (isNE(resnenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(resnenqIO.getChdrcoy(), chdrmnaIO.getChdrcoy())
		|| isNE(resnenqIO.getChdrnum(), chdrmnaIO.getChdrnum())
		|| isNE(resnenqIO.getTrancde(), wsaaBatcKey.batcBatctrcde)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		/* Hold and delete the reason record using the RESN logical.    */
		resnIO.setParams(SPACES);
		resnIO.setChdrcoy(resnenqIO.getChdrcoy());
		resnIO.setChdrnum(resnenqIO.getChdrnum());
		resnIO.setTranno(resnenqIO.getTranno());
		resnIO.setFormat(formatsInner.resnrec);
		resnIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, resnIO);
		if (isNE(resnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(resnIO.getParams());
			syserrrec.statuz.set(resnIO.getStatuz());
			fatalError600();
		}
		resnIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, resnIO);
		if (isNE(resnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(resnIO.getParams());
			syserrrec.statuz.set(resnIO.getStatuz());
			fatalError600();
		}
	}

protected void writeLetter3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3210();
				case readTr3843220:
					readTr3843220();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3210()
	{
		/* Read Table T6634 for get letter-type.                           */
		wsaaItemBatctrcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaItemCnttype.set(chdrmnaIO.getCnttype());
	}

	/**
	* <pre>
	*3220-READ-T6634.                                         <PCPPRT>
	* </pre>
	*/
protected void readTr3843220()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634        TO ITEM-ITEMITEM.        <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* If record not found then read again using generic key.          */
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)
			&& isNE(wsaaItemCnttype, "***")) {
				wsaaItemCnttype.set("***");
				/*     GO TO 3220-READ-T6634                            <PCPPRT>*/
				goTo(GotoLabel.readTr3843220);
			}
			else {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF T6634-LETTER-TYPE        = SPACES                 <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*  LETRQST-RDOCNUM stores contract number instead of endt no    */
		/*  Comment the following logic of number allocation             */
		/*  MOVE 'NEXT '                TO ALNO-FUNCTION.                */
		/*  MOVE 'EN'                   TO ALNO-PREFIX.                  */
		/*  MOVE WSKY-BATC-BATCBRN      TO ALNO-GENKEY.                  */
		/*  MOVE WSSP-COMPANY           TO ALNO-COMPANY.                 */
		/*  CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                         */
		/*  IF ALNO-STATUZ              NOT = O-K                        */
		/*     MOVE ALNO-STATUZ         TO SYSR-STATUZ                   */
		/*     MOVE 'ALOCNO'            TO SYSR-PARAMS                   */
		/*     PERFORM 600-FATAL-ERROR.                                  */
		/*   Get set-up parameter for call 'HLETRQS'                   */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		/*  MOVE 'EN'                   TO LETRQST-RDOCPFX.              */
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(wsspcomn.company);
		/* MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.              */
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(wsspcomn.language);
		wsaaOkeyBatctrcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaOkeyTranno.set(wsaaLastTranno);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.branch.set(wsspcomn.branch);
		letrqstrec.servunit.set(wsspcomn.servunit);
		letrqstrec.trcde.set(wsaaBatcKey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		/*   CALL 'HLETRQS' USING LETRQST-PARAMS.                         */
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void formatResnDetails5000()
	{
		start5010();
	}

	/**
	* <pre>
	*    This section formats the latest reason detailes on screen *
	*    if reason record  present.                                 *
	* </pre>
	*/
protected void start5010()
	{
		resnenqIO.setParams(SPACES);
		resnenqIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		resnenqIO.setChdrnum(chdrmnaIO.getChdrnum());
		resnenqIO.setTrancde(wsaaBatcKey.batcBatctrcde);
		resnenqIO.setTranno(999);
		resnenqIO.setFormat(formatsInner.resnenqrec);
		resnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		resnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		resnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANCDE");
		SmartFileCode.execute(appVars, resnenqIO);
		if (isNE(resnenqIO.getStatuz(), varcom.oK)
		&& isNE(resnenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(resnenqIO.getParams());
			syserrrec.statuz.set(resnenqIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrmnaIO.getChdrcoy(), resnenqIO.getChdrcoy())
		|| isNE(chdrmnaIO.getChdrnum(), resnenqIO.getChdrnum())
		|| isNE(wsaaBatcKey.batcBatctrcde, resnenqIO.getTrancde())
		|| isEQ(resnenqIO.getStatuz(), varcom.endp)) {
			sv.reasoncd.set(SPACES);
			wsaaOrigReasoncd.set(SPACES);
			sv.resndesc.set(SPACES);
			wsaaOrigResndesc.set(SPACES);
		}
		else {
			sv.reasoncd.set(resnenqIO.getReasoncd());
			wsaaOrigReasoncd.set(resnenqIO.getReasoncd());
			wsaaOrigResndesc.set(resnenqIO.getResndesc());
			sv.resndesc.set(resnenqIO.getResndesc());
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmnaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatcKey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(wsaaToday);
		drypDryprcRecInner.drypCompany.set(wsaaBatcKey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatcKey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatcKey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmnaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(wsaaToday);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmnaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmnaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmnaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmnaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmnaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmnaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmnaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmnaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmnaIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrmnaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData resnrec = new FixedLengthStringData(10).init("RESNREC");
	private FixedLengthStringData chdrmnarec = new FixedLengthStringData(10).init("CHDRMNAREC");
	private FixedLengthStringData resnenqrec = new FixedLengthStringData(10).init("RESNENQREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
