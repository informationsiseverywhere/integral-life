package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RskppfDAOImpl extends BaseDAOImpl<Rskppf> implements RskppfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(RskppfDAOImpl.class);
	public List<Rskppf> fetchRecord(String chdrcoy, String chdrnum, int date) {
		Rskppf rskppf = null;
		List<Rskppf> rskpfSearchResult = new ArrayList<Rskppf>();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM RSKPPF WHERE CHDRCOY=? AND CHDRNUM=? AND ? BETWEEN DATEFRM AND DATETO");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setInt(3,date);
			rs = ps.executeQuery();
			while(rs.next()) {
				rskppf = new Rskppf();
				rskppf.setChdrcoy(rs.getString("CHDRCOY"));
				rskppf.setChdrnum(rs.getString("CHDRNUM"));
				rskppf.setCnttype(rs.getString("CNTTYPE"));
				rskppf.setDatefrm(rs.getInt("DATEFRM"));
				rskppf.setDateto(rs.getInt("DATETO"));
				rskppf.setRiskprem(rs.getBigDecimal("RISKPREM"));
				rskppf.setPolfee(rs.getBigDecimal("POLFEE"));
				rskppf.setEffDate(rs.getInt("EFFDATE"));
				rskppf.setUsrprf(rs.getString("USRPRF"));
				rskppf.setJobnm(rs.getString("JOBNM"));
				rskpfSearchResult.add(rskppf);
			}
		}
		catch(SQLException e) {
			LOGGER.error("fetchRecords()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return rskpfSearchResult;
	}
	
	@Override
	public boolean insertRecords(List<Rskppf> rskppfList) {
		boolean isInserted = false;
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO RSKPPF (CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CNTTYPE, CRTABLE, DATEFRM, DATETO, INSTPREM, RISKPREM, EFFDATE, USRPRF, JOBNM, DATIME, POLFEE)");
		sb.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			int i;
			for(Rskppf rskppf : rskppfList) {
				i=0;
				ps.setString(++i, rskppf.getChdrcoy());
				ps.setString(++i, rskppf.getChdrnum());
				ps.setString(++i, rskppf.getLife());
				ps.setString(++i, rskppf.getCoverage());
				ps.setString(++i, rskppf.getRider());
				ps.setString(++i, rskppf.getCnttype());
				ps.setString(++i, rskppf.getCrtable());
				ps.setInt(++i, rskppf.getDatefrm());
				ps.setInt(++i, rskppf.getDateto());
				ps.setBigDecimal(++i, rskppf.getInstprem());
				ps.setBigDecimal(++i, rskppf.getRiskprem());
				ps.setInt(++i, rskppf.getEffDate());
				ps.setString(++i, getUsrprf());
				ps.setString(++i, getJobnm());
				ps.setTimestamp(++i, new Timestamp(System.currentTimeMillis()));
				ps.setBigDecimal(++i, rskppf.getPolfee());
				ps.addBatch();
			}
			if(ps.executeBatch().length > 0) {
				isInserted = true;
			}
			rskppfList.clear();
		}
		catch(SQLException e) {
			LOGGER.error("insertRecords()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps,null);
		}
		return isInserted;
	}
	
	@Override
	public boolean updateRecords(List<Rskppf> rskppfList) {
		boolean isUpdated = false;
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE RSKPPF SET INSTPREM=? WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE = ? AND RIDER = ? ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Rskppf rskp : rskppfList) {
				ps.setBigDecimal(1, rskp.getInstprem());
				ps.setString(2, rskp.getChdrcoy());
				ps.setString(3, rskp.getChdrnum());
				ps.setString(4, rskp.getLife());
				ps.setString(5, rskp.getCoverage());
				ps.setString(6, rskp.getRider());
				ps.addBatch();
			}
			if(ps.executeBatch().length > 0) {
				isUpdated = true;
			}
		}
		catch(SQLException e){
			LOGGER.error("updateRecords()", e);
			throw new SQLRuntimeException(e);
		}
		return isUpdated;
	}
	
	@Override
	public Map<String, List<Rskppf>> searchRskppf(String coy, List<String> chdrnumList) {
		StringBuilder sb = new StringBuilder();
		//sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CNTTYPE, CRTABLE, DATEFRM, DATETO, INSTPREM, RISKPREM FROM RSKPPF ");
		//sb.append("WHERE CHDRCOY = ? AND ").append(getSqlInStr("CHDRNUM", chdrnumList));
		//sb.append(" ORDER BY UNIQUE_NUMBER ASC");
		sb.append("SELECT R.UNIQUE_NUMBER, R.CHDRCOY, R.CHDRNUM, R.LIFE, R.COVERAGE, R.RIDER, R.CNTTYPE, R.CRTABLE, R.DATEFRM, R.DATETO, R.INSTPREM, ");
		sb.append("R.RISKPREM, R.POLFEE, C.INSTPREM as covrinstprem FROM RSKPPF R JOIN COVRPF C ON C.CHDRNUM = R.CHDRNUM AND R.CHDRCOY = ? AND ").append(getSqlInStr("R.CHDRNUM", chdrnumList));
		sb.append("AND R.LIFE = C.LIFE AND R.COVERAGE = C.COVERAGE AND R.RIDER = C.RIDER AND C.VALIDFLAG='1' ");
		sb.append(" ORDER BY R.UNIQUE_NUMBER ASC");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		Map<String, List<Rskppf>> riskPremMap = new HashMap<>();
		try {
			ps.setString(1, coy);
			rs = executeQuery(ps);
			int i;
			while(rs.next()) {
				i = 1;
				Rskppf rskppf = new Rskppf();
				rskppf.setUnique_Number(rs.getLong(i++));
				rskppf.setChdrcoy(rs.getString(i++));
				rskppf.setChdrnum(rs.getString(i++));
				rskppf.setLife(rs.getString(i++));
				rskppf.setCoverage(rs.getString(i++));
				rskppf.setRider(rs.getString(i++));
				rskppf.setCnttype(rs.getString(i++));
				rskppf.setCrtable(rs.getString(i++));
				rskppf.setDatefrm(rs.getInt(i++));
				rskppf.setDateto(rs.getInt(i++));
				rskppf.setInstprem(rs.getBigDecimal(i++));
				rskppf.setRiskprem(rs.getBigDecimal(i++));
				rskppf.setPolfee(rs.getBigDecimal(i++));
				rskppf.setCovrinstprem(rs.getBigDecimal(i++));
				if(riskPremMap.containsKey(rskppf.getChdrnum()))
					riskPremMap.get(rskppf.getChdrnum()).add(rskppf);
				else {
					List<Rskppf> riskPremList = new ArrayList<>();
					riskPremList.add(rskppf);
					riskPremMap.put(rskppf.getChdrnum(), riskPremList);
				}
			}
		}
		catch(SQLException e) {
			LOGGER.error("searchRskppf()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, rs);
        }
		return riskPremMap;
	}
	
	public boolean checkCoverage(String chdrcoy, String chdrnum, String life, String coverage, String rider) {
		boolean compFlag = false;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM RSKPPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND ");
		sb.append("LIFE = ? AND COVERAGE = ? AND RIDER = ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setString(5, rider);
			rs = ps.executeQuery();
			if(rs.next()) {
				compFlag = true;
			}else {
				compFlag = false;
			}
			
		}
		catch(SQLException e) {
			LOGGER.error("checkCoverage()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, rs);
        }
		return compFlag;
	}
	
	public void updateRiskPremium(Map<String, List<Rskppf>> covrpfMapList) {
		List<Rskppf> covrList = new ArrayList<>();
		String SQL_CHDR_UPDATE = "UPDATE RSKPPF SET riskprem=?, polfee = ? WHERE CHDRNUM=? AND CHDRCOY = ? AND LIFE=? AND COVERAGE=? AND RIDER=? ";
		PreparedStatement psCovrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
		
		try {
			for(Map.Entry<String, List<Rskppf>> covrMap : covrpfMapList.entrySet()) {
				covrList = covrMap.getValue();
				for(Rskppf covr: covrList) {
					psCovrUpdate.setBigDecimal(1, covr.getRiskprem());
					psCovrUpdate.setBigDecimal(2, covr.getPolfee());
					psCovrUpdate.setString(3, covr.getChdrnum());
	            	psCovrUpdate.setString(4, covr.getChdrcoy());
	            	psCovrUpdate.setString(5, covr.getLife());
	            	psCovrUpdate.setString(6, covr.getCoverage());
	            	psCovrUpdate.setString(7, covr.getRider());
	            	psCovrUpdate.addBatch();
	            }		
				
			}
			psCovrUpdate.executeBatch();
		}
		catch (SQLException e) {
            LOGGER.error("updateRiskPrem()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psCovrUpdate, null);
        }
	}
	
	public List<Rskppf> fetchRecordReversal(String chdrcoy, String chdrnum, int date) {
		Rskppf rskppf = null;
		List<Rskppf> rskpfSearchResult = new ArrayList<Rskppf>();
		StringBuilder sb = new StringBuilder();
		//sb.append("SELECT * FROM RSKPPF WHERE CHDRCOY=? AND CHDRNUM=? AND ? BETWEEN DATEFRM AND DATETO");
		sb.append("SELECT R.UNIQUE_NUMBER, R.CHDRCOY, R.CHDRNUM, R.LIFE, R.COVERAGE, R.RIDER, R.CNTTYPE, R.CRTABLE, R.DATEFRM, "
				+ "R.DATETO, R.INSTPREM, R.RISKPREM, R.EFFDATE, R.USRPRF, R.JOBNM, R.DATIME, R.POLFEE, "
				+ "C.INSTPREM as covrinstprem FROM RSKPPF R JOIN COVRPF C ON C.CHDRNUM = R.CHDRNUM and R.CHDRCOY = ? ");
		sb.append("AND R.CHDRNUM = ? AND ? BETWEEN DATEFRM AND DATETO ");
		sb.append("AND R.LIFE = C.LIFE AND R.COVERAGE = C.COVERAGE ");
		sb.append("AND R.RIDER = C.RIDER AND C.VALIDFLAG='1' ORDER BY R.UNIQUE_NUMBER ASC ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setInt(3,date);
			rs = ps.executeQuery();
			/*while(rs.next()) {
				rskppf = new Rskppf();
				rskppf.setChdrcoy(rs.getString("CHDRCOY"));
				rskppf.setChdrnum(rs.getString("CHDRNUM"));
				rskppf.setLife(rs.getString("LIFE"));
				rskppf.setCoverage(rs.getString("COVERAGE"));
				rskppf.setRider(rs.getString("RIDER"));
				rskppf.setCnttype(rs.getString("CNTTYPE"));
				rskppf.setCrtable(rs.getString("CRTABLE"));
				rskppf.setDatefrm(rs.getInt("DATEFRM"));
				rskppf.setDateto(rs.getInt("DATETO"));
				rskppf.setRiskprem(rs.getBigDecimal("RISKPREM"));
				rskppf.setEffDate(rs.getInt("EFFDATE"));
				rskppf.setUsrprf(rs.getString("USRPRF"));
				rskppf.setJobnm(rs.getString("JOBNM"));
				rskpfSearchResult.add(rskppf);
			}*/
			while(rs.next()) {
				Rskppf rskppf1 = new Rskppf();
				rskppf1.setUnique_Number(rs.getLong("UNIQUE_NUMBER"));
				rskppf1.setChdrcoy(rs.getString("CHDRCOY"));
				rskppf1.setChdrnum(rs.getString("CHDRNUM"));
				rskppf1.setLife(rs.getString("LIFE"));
				rskppf1.setCoverage(rs.getString("COVERAGE"));
				rskppf1.setRider(rs.getString("RIDER"));
				rskppf1.setCnttype(rs.getString("CNTTYPE"));
				rskppf1.setCrtable(rs.getString("CRTABLE"));
				rskppf1.setDatefrm(rs.getInt("DATEFRM"));
				rskppf1.setDateto(rs.getInt("DATETO"));
				rskppf1.setInstprem(rs.getBigDecimal("INSTPREM"));
				rskppf1.setRiskprem(rs.getBigDecimal("RISKPREM"));
				rskppf1.setEffDate(rs.getInt("EFFDATE"));
				rskppf1.setUsrprf(rs.getString("USRPRF"));
				rskppf1.setJobnm(rs.getString("JOBNM"));
				rskppf1.setDatime(rs.getTimestamp("DATIME"));
				rskppf1.setPolfee(rs.getBigDecimal("POLFEE"));
				rskppf1.setCovrinstprem(rs.getBigDecimal("covrinstprem"));
				rskpfSearchResult.add(rskppf1);
			}
		}
		catch(SQLException e) {
			LOGGER.error("fetchRecords()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return rskpfSearchResult;
	}
	
	@Override
	public boolean updateRiskPrem(List<Rskppf> rskppfList) {
		boolean isUpdated = false;
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE RSKPPF SET RISKPREM=?, POLFEE = ? WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE = ? AND RIDER = ? AND UNIQUE_NUMBER = ? ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Rskppf rskp : rskppfList) {
				ps.setBigDecimal(1, rskp.getRiskprem());
				ps.setBigDecimal(2, rskp.getPolfee());
				ps.setString(3, rskp.getChdrcoy());
				ps.setString(4, rskp.getChdrnum());
				ps.setString(5, rskp.getLife());
				ps.setString(6, rskp.getCoverage());
				ps.setString(7, rskp.getRider());
				ps.setLong(8, rskp.getUnique_Number());
				ps.addBatch();
			}
			if(ps.executeBatch().length > 0) {
				isUpdated = true;
			}
		}
		catch(SQLException e){
			LOGGER.error("updateRecords()", e);
			throw new SQLRuntimeException(e);
		}
		return isUpdated;
	}
	
	public List<Rskppf> fetchRskppfRecord(String chdrcoy, String chdrnum, String life, String coverage, String rider,int date) {
		Rskppf rskppf = null;
		List<Rskppf> rskpfSearchResult = new ArrayList<Rskppf>();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM RSKPPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND ? BETWEEN DATEFRM AND DATETO");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setString(5, rider);
			ps.setInt(6,date);
			rs = ps.executeQuery();
			while(rs.next()) {
				rskppf = new Rskppf();
				rskppf.setUnique_Number(rs.getLong("UNIQUE_NUMBER"));
				rskppf.setChdrcoy(rs.getString("CHDRCOY"));
				rskppf.setChdrnum(rs.getString("CHDRNUM"));
				rskppf.setLife(rs.getString("LIFE"));
				rskppf.setCoverage(rs.getString("COVERAGE"));
				rskppf.setRider(rs.getString("RIDER"));
				rskppf.setCnttype(rs.getString("CNTTYPE"));
				rskppf.setCrtable(rs.getString("CRTABLE"));
				rskppf.setDatefrm(rs.getInt("DATEFRM"));
				rskppf.setDateto(rs.getInt("DATETO"));
				rskppf.setInstprem(rs.getBigDecimal("INSTPREM"));
				rskppf.setRiskprem(rs.getBigDecimal("RISKPREM"));
				rskppf.setPolfee(rs.getBigDecimal("POLFEE"));
				rskppf.setEffDate(rs.getInt("EFFDATE"));
				rskppf.setUsrprf(rs.getString("USRPRF"));
				rskppf.setJobnm(rs.getString("JOBNM"));
				rskppf.setDatime(rs.getTimestamp("DATIME"));
				rskpfSearchResult.add(rskppf);
			}
		}
		catch(SQLException e) {
			LOGGER.error("fetchRskppfRecord()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps, rs, null);
		}
		return rskpfSearchResult;
	}
	
	@Override
	public void reverseContract(String chdrcoy, String chdrnum) {
		StringBuilder sb = new StringBuilder();
		BigDecimal tempRiskPrem = BigDecimal.ZERO;
		BigDecimal tempPolFee = BigDecimal.ZERO;
		sb.append("UPDATE RSKPPF SET RISKPREM=?, POLFEE = ? WHERE CHDRCOY=? AND CHDRNUM=? ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
				ps.setBigDecimal(1, tempRiskPrem);
				ps.setBigDecimal(2, tempPolFee);
				ps.setString(3, chdrcoy);
				ps.setString(4, chdrnum);
				ps.executeUpdate();
			}
		catch(SQLException e){
			LOGGER.error("reverseContract()", e);
			throw new SQLRuntimeException(e);
		}
	}

	public Map<String, List<Rskppf>> searchRskpMap(String coy, List<String> chdrnumList) {
        StringBuilder sqlRskpSelect1 = new StringBuilder(
                "SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CNTTYPE, CRTABLE, DATEFRM, DATETO, INSTPREM, RISKPREM, POLFEE FROM RSKPPF ");
        sqlRskpSelect1.append(" WHERE CHDRCOY=? AND ");
        sqlRskpSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
        PreparedStatement psRskpSelect = getPrepareStatement(sqlRskpSelect1.toString());
        ResultSet sqlrskppf1rs = null;
        Map<String, List<Rskppf>> rskpMap = new HashMap<String, List<Rskppf>>();
        try {
            psRskpSelect.setInt(1, Integer.parseInt(coy));
            sqlrskppf1rs = executeQuery(psRskpSelect);

            while (sqlrskppf1rs.next()) {
                Rskppf rskppf = new Rskppf();
				rskppf.setUnique_Number(sqlrskppf1rs.getLong(1));
                rskppf.setChdrcoy(sqlrskppf1rs.getString(2));
                rskppf.setChdrnum(sqlrskppf1rs.getString(3));
                rskppf.setLife(sqlrskppf1rs.getString(4));
                rskppf.setCoverage(sqlrskppf1rs.getString(5));
                rskppf.setRider(sqlrskppf1rs.getString(6));
                rskppf.setCnttype(sqlrskppf1rs.getString(7));
                rskppf.setCrtable(sqlrskppf1rs.getString(8));
                rskppf.setDatefrm(sqlrskppf1rs.getInt(9));
                rskppf.setDateto(sqlrskppf1rs.getInt(10));
                rskppf.setInstprem(sqlrskppf1rs.getBigDecimal(11));
                rskppf.setRiskprem(sqlrskppf1rs.getBigDecimal(12));
                rskppf.setPolfee(sqlrskppf1rs.getBigDecimal(13));
                if (rskpMap.containsKey(rskppf.getChdrnum())) {
                    rskpMap.get(rskppf.getChdrnum()).add(rskppf);
                } else {
                    List<Rskppf> chdrList = new ArrayList<Rskppf>();
                    chdrList.add(rskppf);
                    rskpMap.put(rskppf.getChdrnum(), chdrList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchRskpMap()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psRskpSelect, sqlrskppf1rs);
        }
        return rskpMap;

    }
	 
}
