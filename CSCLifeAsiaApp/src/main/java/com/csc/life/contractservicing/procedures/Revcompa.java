/*
 * File: Revcompa.java
 * Date: 30 August 2009 2:06:25
 * Author: Quipoz Limited
 * 
 * Class transformed from REVCOMPA.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZctnrevTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.dataaccess.ZptnrevTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrccTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.RtrnrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcorevTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE COMPONENT ADD.
*        ----------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse the results of the Component
*   Add process. It will reverse all Non-cash Accounting records
*   (ACMV), Reassurance records (RACT), Contract Header (CHDR),
*   Payer records (PAYR), Life Options and Extras records (LEXT),
*   Coverage/Rider records (COVR) and Individual Coverage/Rider
*   Increase records (INCI) which were created/amended at the
*   time of the forward transaction.
*
*
* Processing.
* -----------
*
* Reverse Contract Header ( CHDRs ):
*
*  Read the Contract Header file (CHDRPF) using the logical
*  view CHDRMJA with a key of company and contract number
*  from linkage.
*
*  For the record found, check it is validflag '1'.
*    If not, then ERROR.
*  Delete validflag '1' CHDR record.
*  Read Next CHDR record which is validflag '2'.
*  Reset validflag to '1'.
*  REWRiTe CHDR record.
*
* Reverse Payer ( PAYRs ):
*
*  Read the Payer file (PAYRPF) using the logical view
*  PAYRLIF with a key of company and contract number
*  from linkage.
*
*  For the record found, check it is validflag '1'.
*    If not, then ERROR.
*  Delete validflag '1' PAYR record.
*  Read Next PAYR record which is validflag '2'.
*  Reset validflag to '1'.
*  REWRiTe PAYR record.
*
* Reverse Coverage & Rider records ( COVRs ):
*
*    When a component is ADDed to a contract, only the New
*     component COVR record is updated with the tranno
*     for the 'ADD' transaction.
*
*    Delete the new component COVR record.
*
* Reverse Life Extras & Options ( LEXTs ):
*
*    Any Options/Extras (LEXT) records that exist for the 'Added'
*     Component(s) will be physically DELETed from the LEXT file.
*
* Reverse Accounting Movement ( RTRN ):
*
*  Read the file (RTRNPF) using the logical view RTRNREV with
*  a key of contract number/transaction number from linkage.
*
*  For each record found:
*   Write a new record with the following...
*     Set the accounting month/year with the batch
*     accounting month/year from linkage.
*     Set the tranno with the NEW tranno from linkage.
*     Set the batch key with new batch key from linkage.
*     Multiply the original currency amount by -1.
*     Multiply the accounting currency amount by -1.
*     Set the transaction reference with the NEW
*     transaction code from linkage.
*     Set the transaction description with the long
*     description of the new trans code from T1688.
*     Set the effective date with todays date.
*     Write the new record to the database by calling the
*     subroutine 'LIFRTRN'.
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*     Set the accounting month/year with the batch
*     accounting month/year from linkage.
*     Set the tranno with the NEW tranno from linkage.
*     Set the batch key with new batch key from linkage.
*     Multiply the original currency amount by -1.
*     Multiply the accounting currency amount by -1.
*     Set the transaction reference with the NEW
*     transaction code from linkage.
*     Set the transaction description with the long
*     description of the new trans code from T1688.
*     Set the effective date with todays date.
*     Write the new record to the database by calling the
*     subroutine 'LIFACMV'.
*
* Reverse Reassurance records ( RACTs ):
*
*  RACT record processing will be done inside the COVR
*   processing loop - we only need to process RACT records
*   associated with the Component(s) we are removing.
*
*  Read the Reassurance file (RACTPF) using the logical
*  view RACTRCC with a key of company, contract number, life,
*  coverage, rider, reassurance number, reassurance type and
*  validflag.
*
*  For each record found for the contract being processed...
*
*    IF any RACT records are found for the Component(s) we are
*     removing from the contract, then DELETE.
*
* Reverse Agent Commission records ( AGCMs ):
*
*  AGCM record processing will be done inside the COVR
*   processing loop - we only need to process AGCM records
*   associated with the Component(s) we are removing.
*
*  Read the Agent Commission file (AGCMPF) using the logical
*  view AGCMREV and DELETE all AGCMs that match the
*  component(s) we are trying to reverse.
*
* Reversal of Unit-Linked/Traditional parts of the Contract:
*
*  Perform all generic processing inside the COVR processing
*  loop above - we want to do the generic processing BEFORE
*  we delete the COVR records above.
*  Loop through all the Coverages/Riders associated with the
*   Contract we are processing and call any Generic processing
*   subroutines that are specified in T5671.
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Inclusion of cash dividend processing.                              *
*                                                                     *
*****************************************************************
* </pre>
*/
public class Revcompa extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVCOMPA";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaFirstGenCall = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private static final String th605 = "TH605";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmrccTableDAM agcmrccIO = new AgcmrccTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrrevTableDAM covrrevIO = new CovrrevTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcorevTableDAM fpcorevIO = new FpcorevTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private RtrnrevTableDAM rtrnrevIO = new RtrnrevTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZctnrevTableDAM zctnrevIO = new ZctnrevTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZptnrevTableDAM zptnrevIO = new ZptnrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Th605rec th605rec = new Th605rec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	List<Exclpf> exclpfList = new ArrayList<Exclpf>();
	private Exclpf exclpf = new Exclpf();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		call11020, 
		exit11090, 
		call12020, 
		exit12090
	}

	public Revcompa() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processChdrs3000();
		processPayrs4000();
		readT57294500();
		processCovrs5000();
		processRtrns7200();
		processAcmvs8000();
		processAcmvOptical8010();
		/* Read table TH605 to see if Bonus Workbench Extraction used      */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(th605);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError99000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench  *                                              */
		processZptn11000();
		processZctn12000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError99000();
		}
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void processChdrs3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Do a BEGNH on Contract Header CHDR*/
		/*  Get validflag '1' and DELETe*/
		/*  Get NEXTRecord (validflag '2') and REWRiTe as validflag '1'.*/
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		/* MOVE BEGNH                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(), "1")
		|| isEQ(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		/* MOVE DELET                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		chdrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrmjaIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrmjaIO.getValidflag(), "2")
		|| isEQ(chdrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		chdrmjaIO.setValidflag("1");
		/* MOVE REWRT                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
	}

protected void processPayrs4000()
	{
		start4000();
	}

protected void start4000()
	{
		/* Do a BEGNH on Contract Header PAYR*/
		/*  Get validflag '1' and DELETe*/
		/*  Get NEXTRecord (validflag '2') and REWRiTe as validflag '1'.*/
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(ZERO);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		/* MOVE BEGNH                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)
		&& isNE(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), "1")
		|| isEQ(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		/* MOVE DELET                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		payrlifIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)
		&& isNE(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), "2")
		|| isEQ(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
		payrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
	}

protected void readT57294500()
	{
		start4510();
	}

	/**
	* <pre>
	* Read T5729 to determine if this contract is of a 'flexible'     
	* type. If it is, later on we will reverse the FPCO records       
	* </pre>
	*/
protected void start4510()
	{
		notFlexiblePremiumContract.setTrue();
		/*  Read T5729.                                            <D9604> */
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(reverserec.company, SPACES);
			stringVariable1.addExpression(t5729, SPACES);
			stringVariable1.addExpression(chdrmjaIO.getCnttype(), SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5729)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())) {
			return ;
		}
		flexiblePremiumContract.setTrue();
	}

protected void processCovrs5000()
	{
		start5000();
	}

protected void start5000()
	{
		/* Use COVR logical view to loop around coverage/rider records*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			reverseCovrs5100();
		}
		
	}

protected void reverseCovrs5100()
	{
		start5100();
	}

protected void start5100()
	{
		/* check TRANNO of COVR just read*/
		if (isEQ(reverserec.tranno, covrIO.getTranno())) {
			/* because we are going to delete some COVR records, i.e.*/
			/*  remove the components from the contract structure*/
			/*  we'd better do the generic processing first so that*/
			/*  all UTRNs, INCIs etc... are reversed for the components*/
			/*  we are going to delete before they are deleted.*/
			/* Note that the Plan Suffix doesn't have to be specified, since*/
			/*  even if a BReaKOUT has occurred after the initial Component*/
			/*  Add, we still want to remove all trace of this Component.*/
			/*  Also, a Component Add cannot be done at Plan Suffix level.*/
			genericProcessing9000();
			covrrevIO.setParams(SPACES);
			covrrevIO.setChdrcoy(covrIO.getChdrcoy());
			covrrevIO.setChdrnum(covrIO.getChdrnum());
			covrrevIO.setLife(covrIO.getLife());
			covrrevIO.setCoverage(covrIO.getCoverage());
			covrrevIO.setRider(covrIO.getRider());
			covrrevIO.setPlanSuffix(ZERO);
			covrrevIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
			covrrevIO.setFormat(formatsInner.covrrevrec);
			while ( !(isEQ(covrrevIO.getStatuz(),varcom.endp))) {
				deleteCovrs5200();
			}
			
			processLexts6000();
			processExclusion();
			processHcsds6500();
			/*     PERFORM 7000-PROCESS-RACTS                               */
			processAgcms10000();
		}
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
	}

protected void deleteCovrs5200()
	{
		start5200();
	}

protected void start5200()
	{
		SmartFileCode.execute(appVars, covrrevIO);
		if (isNE(covrrevIO.getStatuz(), varcom.oK)
		&& isNE(covrrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrrevIO.getParams());
			syserrrec.statuz.set(covrrevIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), covrrevIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), covrrevIO.getChdrnum())
		|| isNE(covrIO.getLife(), covrrevIO.getLife())
		|| isNE(covrIO.getCoverage(), covrrevIO.getCoverage())
		|| isNE(covrIO.getRider(), covrrevIO.getRider())
		|| isEQ(covrrevIO.getStatuz(), varcom.endp)) {
			covrrevIO.setStatuz(varcom.endp);
			return ;
		}
		covrrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrrevIO);
		if (isNE(covrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrevIO.getParams());
			syserrrec.statuz.set(covrrevIO.getStatuz());
			systemError99000();
		}
		covrrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, covrrevIO);
		if (isNE(covrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrevIO.getParams());
			syserrrec.statuz.set(covrrevIO.getStatuz());
			systemError99000();
		}
		/* BEFORE WE DELETE THE NEXT COVERAGE, DELETE THE FPCO             */
		if (flexiblePremiumContract.isTrue()) {
			deleteFpco5300();
		}
		covrrevIO.setFunction(varcom.nextr);
	}

protected void deleteFpco5300()
	{
		start5310();
	}

	/**
	* <pre>
	****                                                      <D9604> 
	**** Delete any FPCO created in the forward transaction   <D9604> 
	****                                                      <D9604> 
	* </pre>
	*/
protected void start5310()
	{
		fpcorevIO.setChdrcoy(covrrevIO.getChdrcoy());
		fpcorevIO.setChdrnum(covrrevIO.getChdrnum());
		fpcorevIO.setLife(covrrevIO.getLife());
		fpcorevIO.setCoverage(covrrevIO.getCoverage());
		fpcorevIO.setRider(covrrevIO.getRider());
		fpcorevIO.setPlanSuffix(covrrevIO.getPlanSuffix());
		fpcorevIO.setTargfrom(varcom.vrcmMaxDate);
		/* MOVE BEGNH                  TO FPCOREV-FUNCTION.     <LA3993>*/
		fpcorevIO.setFunction(varcom.begn);
		fpcorevIO.setFormat(formatsInner.fpcorevrec);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			systemError99000();
		}
		if (isNE(fpcorevIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(fpcorevIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(fpcorevIO.getLife(), covrIO.getLife())
		|| isNE(fpcorevIO.getCoverage(), covrIO.getCoverage())
		|| isNE(fpcorevIO.getRider(), covrIO.getRider())
		|| isNE(fpcorevIO.getPlanSuffix(), covrIO.getPlanSuffix())) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			systemError99000();
		}
		/* MOVE DELET                  TO FPCOREV-FUNCTION.     <LA3993>*/
		fpcorevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fpcorevIO);
		if (isNE(fpcorevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorevIO.getParams());
			syserrrec.statuz.set(fpcorevIO.getStatuz());
			systemError99000();
		}
	}

protected void processLexts6000()
	{
		start6000();
	}

protected void start6000()
	{
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrcoy(covrIO.getChdrcoy());
		lextrevIO.setChdrnum(covrIO.getChdrnum());
		lextrevIO.setLife(covrIO.getLife());
		lextrevIO.setCoverage(covrIO.getCoverage());
		lextrevIO.setRider(covrIO.getRider());
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		lextrevIO.setFormat(formatsInner.lextrevrec);
		while ( !(isEQ(lextrevIO.getStatuz(),varcom.endp))) {
			deleteLexts6100();
		}
		
	}

protected void deleteLexts6100()
	{
		start6100();
	}

protected void start6100()
	{
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.oK)
		&& isNE(lextrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), lextrevIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), lextrevIO.getChdrnum())
		|| isNE(covrIO.getLife(), lextrevIO.getLife())
		|| isNE(covrIO.getCoverage(), lextrevIO.getCoverage())
		|| isNE(covrIO.getRider(), lextrevIO.getRider())
		|| isEQ(lextrevIO.getStatuz(), varcom.endp)) {
			lextrevIO.setStatuz(varcom.endp);
			return ;
		}
		lextrevIO.setFunction(varcom.readh);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			systemError99000();
		}
		lextrevIO.setFunction(varcom.delet);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			systemError99000();
		}
		lextrevIO.setFunction(varcom.nextr);
		lextrevIO.setFormat(formatsInner.lextrevrec);
	}

protected void processHcsds6500()
	{
		procHcsd6500();
	}

protected void procHcsd6500()
	{
		/* Check if HCSD record exists with the same COVR key.             */
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(covrIO.getChdrcoy());
		hcsdIO.setChdrnum(covrIO.getChdrnum());
		hcsdIO.setLife(covrIO.getLife());
		hcsdIO.setCoverage(covrIO.getCoverage());
		hcsdIO.setRider(covrIO.getRider());
		hcsdIO.setPlanSuffix(covrIO.getPlanSuffix());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)
		&& isNE(hcsdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			systemError99000();
		}
		if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)
		|| isNE(hcsdIO.getTranno(), reverserec.tranno)) {
			return ;
		}
		hcsdIO.setFunction(varcom.readh);
		hcsdio6600();
		hcsdIO.setFunction(varcom.delet);
		hcsdio6600();
	}

protected void hcsdio6600()
	{
		/*CALL-HCSDIO*/
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			systemError99000();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*7000-PROCESS-RACTS SECTION.                                      
	*7000-START.                                                      
	**** MOVE SPACES                 TO RACTRCC-PARAMS.               
	**** MOVE COVR-CHDRCOY           TO RACTRCC-CHDRCOY.              
	**** MOVE COVR-CHDRNUM           TO RACTRCC-CHDRNUM.              
	**** MOVE COVR-LIFE              TO RACTRCC-LIFE.                 
	**** MOVE COVR-COVERAGE          TO RACTRCC-COVERAGE.             
	**** MOVE COVR-RIDER             TO RACTRCC-RIDER.                
	**** MOVE SPACES                 TO RACTRCC-RASNUM.               
	**** MOVE SPACES                 TO RACTRCC-RATYPE.               
	**** MOVE SPACES                 TO RACTRCC-VALIDFLAG.            
	**** MOVE ZEROS                  TO RACTRCC-TRANNO.               
	**** MOVE BEGN                   TO RACTRCC-FUNCTION.             
	**** MOVE RACTRCCREC             TO RACTRCC-FORMAT.               
	**** PERFORM 7100-REVERSE-RACTS  UNTIL RACTRCC-STATUZ = ENDP.     
	*7090-EXIT.                                                       
	**** EXIT.                                                        
	*7100-REVERSE-RACTS SECTION.                                      
	*7100-START.                                                      
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.            
	**** IF RACTRCC-STATUZ           NOT = O-K                        
	****    AND RACTRCC-STATUZ       NOT = ENDP                       
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	**** IF REVE-COMPANY             NOT = RACTRCC-CHDRCOY            
	****    OR REVE-CHDRNUM          NOT = RACTRCC-CHDRNUM            
	****    OR COVR-LIFE             NOT = RACTRCC-LIFE               
	****    OR COVR-COVERAGE         NOT = RACTRCC-COVERAGE           
	****    OR COVR-RIDER            NOT = RACTRCC-RIDER              
	****    OR RACTRCC-STATUZ        = ENDP                           
	****     MOVE ENDP               TO RACTRCC-STATUZ                
	****     GO TO 7190-EXIT                                          
	**** END-IF.                                                      
	**** MOVE READH                  TO RACTRCC-FUNCTION.             
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.            
	**** IF RACTRCC-STATUZ           NOT = O-K                        
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	**** MOVE DELET                  TO RACTRCC-FUNCTION.             
	**** CALL 'RACTRCCIO'            USING RACTRCC-PARAMS.            
	**** IF RACTRCC-STATUZ           NOT = O-K                        
	****     MOVE RACTRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE RACTRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	**** MOVE NEXTR                  TO RACTRCC-FUNCTION.             
	*7190-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void processRtrns7200()
	{
		start7000();
	}

protected void start7000()
	{
		rtrnrevIO.setParams(SPACES);
		rtrnrevIO.setRldgcoy(reverserec.company);
		rtrnrevIO.setRdocnum(reverserec.chdrnum);
		rtrnrevIO.setTranno(reverserec.tranno);
		rtrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rtrnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(rtrnrevIO.getStatuz(), varcom.endp))) {
			reverseRtrns7300();
		}
		
	}

protected void reverseRtrns7300()
	{
		start7300();
		nextRtrnrev7380();
	}

protected void start7300()
	{
		/* check if transaction codes match before processing RTRN         */
		if (isNE(reverserec.oldBatctrcde, rtrnrevIO.getBatctrcde())) {
			return ;
		}
		/* Write a new record to the RTRN file by calling the subroutine   */
		/* LIFRTRN.                                                        */
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(reverserec.batchkey);
		lifrtrnrec.rdocnum.set(chdrmjaIO.getChdrnum());
		lifrtrnrec.rldgacct.set(chdrmjaIO.getChdrnum());
		lifrtrnrec.tranno.set(reverserec.newTranno);
		lifrtrnrec.jrnseq.set(rtrnrevIO.getTranseq());
		lifrtrnrec.rldgcoy.set(rtrnrevIO.getRldgcoy());
		lifrtrnrec.sacscode.set(rtrnrevIO.getSacscode());
		lifrtrnrec.sacstyp.set(rtrnrevIO.getSacstyp());
		lifrtrnrec.trandesc.set(wsaaTransDesc);
		lifrtrnrec.crate.set(rtrnrevIO.getCrate());
		lifrtrnrec.transactionTime.set(rtrnrevIO.getTrandate());
		lifrtrnrec.transactionDate.set(rtrnrevIO.getTrantime());
		lifrtrnrec.user.set(reverserec.user);
		lifrtrnrec.termid.set(reverserec.termid);
		compute(lifrtrnrec.acctamt, 2).set(mult(rtrnrevIO.getAcctamt(), -1));
		compute(lifrtrnrec.origamt, 2).set(mult(rtrnrevIO.getOrigamt(), -1));
		lifrtrnrec.genlcur.set(rtrnrevIO.getGenlcur());
		lifrtrnrec.origcurr.set(rtrnrevIO.getOrigccy());
		lifrtrnrec.genlcoy.set(rtrnrevIO.getGenlcoy());
		lifrtrnrec.glcode.set(rtrnrevIO.getGlcode());
		lifrtrnrec.glsign.set(rtrnrevIO.getGlsign());
		lifrtrnrec.postyear.set(rtrnrevIO.getPostyear());
		lifrtrnrec.postmonth.set(rtrnrevIO.getPostmonth());
		/* MOVE DTC1-INT-DATE          TO LIFR-EFFDATE.                 */
		lifrtrnrec.effdate.set(rtrnrevIO.getEffdate());
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(1);
		lifrtrnrec.substituteCode[1].set(chdrmjaIO.getCnttype());
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserrrec.statuz.set(lifrtrnrec.statuz);
			systemError99000();
		}
	}

protected void nextRtrnrev7380()
	{
		rtrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAcmvs8000()
	{
		start8000();
	}

protected void start8000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs8100();
		}
		
	}

protected void processAcmvOptical8010()
	{
		start8010();
	}

protected void start8010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError99500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs8100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError99500();
			}
		}
	}

protected void reverseAcmvs8100()
	{
		start8100();
		readNextAcmv8180();
	}

protected void start8100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void readNextAcmv8180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void genericProcessing9000()
	{
		start9000();
	}

protected void start9000()
	{
		/* We will do generic processing that is required for the*/
		/*  Coverage/Rider that we are currently working with.*/
		/*  all generic processing is driven by calling subroutine(s)*/
		/*  specified on table T5671*/
		wsaaFirstGenCall.set("Y");
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setLife(covrIO.getLife());
		covrenqIO.setCoverage(covrIO.getCoverage());
		covrenqIO.setRider(covrIO.getRider());
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs9100();
		}
		
	}

protected void processCovrs9100()
	{
		start9100();
	}

protected void start9100()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(), covrenqIO.getLife())
		|| isNE(covrIO.getCoverage(), covrenqIO.getCoverage())
		|| isNE(covrIO.getRider(), covrenqIO.getRider())) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* check whether Plan suffix has changed -*/
		/*  if so, we need to call any generic processing subroutines*/
		/*  that are on T5671.*/
		if (isNE(covrenqIO.getPlanSuffix(), wsaaPlanSuffix)
		|| isEQ(wsaaFirstGenCall, "Y")) {
			genericSubr9200();
			wsaaPlanSuffix.set(covrenqIO.getPlanSuffix());
			wsaaFirstGenCall.set("N");
		}
		covrenqIO.setFunction(varcom.nextr);
	}

protected void genericSubr9200()
	{
		start9200();
	}

protected void start9200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(ZERO);
		if (flexiblePremiumContract.isTrue()) {
			greversrec.effdate.set(reverserec.ptrneff);
		}
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs9300();
		}
	}

protected void callTrevsubs9300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError99000();
			}
		}
		/*EXIT*/
	}

protected void processAgcms10000()
	{
		/*START*/
		agcmrccIO.setParams(SPACES);
		agcmrccIO.setChdrcoy(covrIO.getChdrcoy());
		agcmrccIO.setChdrnum(covrIO.getChdrnum());
		agcmrccIO.setLife(covrIO.getLife());
		agcmrccIO.setCoverage(covrIO.getCoverage());
		agcmrccIO.setRider(covrIO.getRider());
		agcmrccIO.setPlanSuffix(ZERO);
		agcmrccIO.setFormat(formatsInner.agcmrccrec);
		agcmrccIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrccIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmrccIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while ( !(isEQ(agcmrccIO.getStatuz(),varcom.endp))) {
			reverseAgcms10100();
		}
		
		/*EXIT*/
	}

protected void reverseAgcms10100()
	{
		start10100();
	}

protected void start10100()
	{
		SmartFileCode.execute(appVars, agcmrccIO);
		if (isNE(agcmrccIO.getStatuz(), varcom.oK)
		&& isNE(agcmrccIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrccIO.getParams());
			syserrrec.statuz.set(agcmrccIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), agcmrccIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), agcmrccIO.getChdrnum())
		|| isNE(covrIO.getLife(), agcmrccIO.getLife())
		|| isNE(covrIO.getCoverage(), agcmrccIO.getCoverage())
		|| isNE(covrIO.getRider(), agcmrccIO.getRider())
		|| isEQ(agcmrccIO.getStatuz(), varcom.endp)) {
			agcmrccIO.setStatuz(varcom.endp);
			return ;
		}
		agcmrccIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmrccIO);
		if (isNE(agcmrccIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrccIO.getParams());
			syserrrec.statuz.set(agcmrccIO.getStatuz());
			systemError99000();
		}
		agcmrccIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmrccIO);
		if (isNE(agcmrccIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrccIO.getParams());
			syserrrec.statuz.set(agcmrccIO.getStatuz());
			systemError99000();
		}
		agcmrccIO.setFunction(varcom.nextr);
	}

protected void processZptn11000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc11010();
				case call11020: 
					call11020();
					writ11030();
					nextr11080();
				case exit11090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc11010()
	{
		zptnrevIO.setDataArea(SPACES);
		zptnrevIO.setChdrcoy(reverserec.company);
		zptnrevIO.setChdrnum(reverserec.chdrnum);
		zptnrevIO.setTranno(reverserec.tranno);
		zptnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zptnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zptnrevIO.setFormat(formatsInner.zptnrevrec);
	}

protected void call11020()
	{
		SmartFileCode.execute(appVars, zptnrevIO);
		if (isNE(zptnrevIO.getStatuz(), varcom.oK)
		&& isNE(zptnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zptnrevIO.getStatuz());
			syserrrec.params.set(zptnrevIO.getParams());
			databaseError99500();
		}
		if (isEQ(zptnrevIO.getStatuz(), varcom.endp)
		|| isNE(zptnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zptnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zptnrevIO.getTranno(), reverserec.tranno)) {
			zptnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit11090);
		}
	}

protected void writ11030()
	{
		zptnIO.setDataArea(SPACES);
		zptnIO.setChdrcoy(zptnrevIO.getChdrcoy());
		zptnIO.setChdrnum(zptnrevIO.getChdrnum());
		zptnIO.setLife(zptnrevIO.getLife());
		zptnIO.setCoverage(zptnrevIO.getCoverage());
		zptnIO.setRider(zptnrevIO.getRider());
		zptnIO.setTranno(reverserec.newTranno);
		setPrecision(zptnIO.getOrigamt(), 2);
		zptnIO.setOrigamt(mult(zptnrevIO.getOrigamt(), -1));
		zptnIO.setTransCode(reverserec.batctrcde);
		zptnIO.setEffdate(zptnrevIO.getEffdate());
		zptnIO.setBillcd(zptnrevIO.getBillcd());
		zptnIO.setInstfrom(zptnrevIO.getInstfrom());
		zptnIO.setInstto(zptnrevIO.getInstto());
		zptnIO.setTrandate(datcon1rec.intDate);
		zptnIO.setZprflg(zptnrevIO.getZprflg());
		zptnIO.setFormat(formatsInner.zptnrec);
		zptnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zptnIO.getStatuz());
			syserrrec.params.set(zptnIO.getParams());
			databaseError99500();
		}
	}

protected void nextr11080()
	{
		zptnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call11020);
	}

protected void processZctn12000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc12010();
				case call12020: 
					call12020();
					writ12030();
					nextr12080();
				case exit12090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc12010()
	{
		zctnrevIO.setDataArea(SPACES);
		zctnrevIO.setChdrcoy(reverserec.company);
		zctnrevIO.setChdrnum(reverserec.chdrnum);
		zctnrevIO.setTranno(reverserec.tranno);
		zctnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zctnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zctnrevIO.setFormat(formatsInner.zctnrevrec);
	}

protected void call12020()
	{
		SmartFileCode.execute(appVars, zctnrevIO);
		if (isNE(zctnrevIO.getStatuz(), varcom.oK)
		&& isNE(zctnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zctnrevIO.getStatuz());
			syserrrec.params.set(zctnrevIO.getParams());
			databaseError99500();
		}
		if (isEQ(zctnrevIO.getStatuz(), varcom.endp)
		|| isNE(zctnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zctnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zctnrevIO.getTranno(), reverserec.tranno)) {
			zctnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit12090);
		}
	}

protected void writ12030()
	{
		zctnIO.setDataArea(SPACES);
		zctnIO.setAgntcoy(zctnrevIO.getAgntcoy());
		zctnIO.setAgntnum(zctnrevIO.getAgntnum());
		zctnIO.setChdrcoy(zctnrevIO.getChdrcoy());
		zctnIO.setChdrnum(zctnrevIO.getChdrnum());
		zctnIO.setLife(zctnrevIO.getLife());
		zctnIO.setCoverage(zctnrevIO.getCoverage());
		zctnIO.setRider(zctnrevIO.getRider());
		zctnIO.setTranno(reverserec.newTranno);
		setPrecision(zctnIO.getCommAmt(), 2);
		zctnIO.setCommAmt(mult(zctnrevIO.getCommAmt(), -1));
		setPrecision(zctnIO.getPremium(), 2);
		zctnIO.setPremium(mult(zctnrevIO.getPremium(), -1));
		zctnIO.setSplitBcomm(zctnrevIO.getSplitBcomm());
		zctnIO.setTransCode(reverserec.batctrcde);
		zctnIO.setEffdate(zctnrevIO.getEffdate());
		zctnIO.setTrandate(datcon1rec.intDate);
		zctnIO.setZprflg(zctnrevIO.getZprflg());
		zctnIO.setFormat(formatsInner.zctnrec);
		zctnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zctnIO);
		if (isNE(zctnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zctnIO.getStatuz());
			syserrrec.params.set(zctnIO.getParams());
			databaseError99500();
		}
	}
//PINNACLE-2855
public void processExclusion() {
	
	int wsaaSeqnbr=0;
	exclpfList = exclpfDAO.getExclpfRecordInReverseOrder(reverserec.company.toString(),reverserec.chdrnum.toString(), reverserec.tranno.toInt(),covrIO.life.toString(),covrIO.coverage.toString(),covrIO.rider.toString());
	if (exclpfList != null & !exclpfList.isEmpty()) {
		for (Exclpf exclpf : exclpfList) {
			if(isEQ(exclpf.getTranno(),reverserec.tranno)) {
				if (isEQ(exclpf.getValidflag(), "2")) {						
						exclpf.setValidflag("1");
						exclpf.setTranno(reverserec.newTranno.toInt());
						exclpfDAO.updateRecordToInvalidate(exclpf, true);					
					
				}else {
					exclpfDAO.deleteRecordByUniqueNumber(exclpf);
					wsaaSeqnbr = exclpf.getSeqno();
				}
			} else {
				if(isEQ(wsaaSeqnbr, exclpf.getSeqno()) & isEQ(exclpf.getValidflag(), "2")) {
					exclpf.setValidflag("1");
					exclpf.setTranno(reverserec.newTranno.toInt());
					exclpfDAO.updateRecordToInvalidate(exclpf, true);
					wsaaSeqnbr=0;
				}
			}
		}
	}

}

protected void nextr12080()
	{
		zctnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call12020);
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError99500()
	{
		start99500();
		exit99990();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData agcmrccrec = new FixedLengthStringData(10).init("AGCMRCCREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC   ");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covrrevrec = new FixedLengthStringData(10).init("COVRREVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData fpcorevrec = new FixedLengthStringData(10).init("FPCOREVREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData zptnrevrec = new FixedLengthStringData(10).init("ZPTNREVREC");
	private FixedLengthStringData zctnrevrec = new FixedLengthStringData(10).init("ZCTNREVREC");
}
}
