package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:35
 * Description:
 * Copybook name: T6633REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6633rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6633Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData loanAnnivInterest = new FixedLengthStringData(1).isAPartOf(t6633Rec, 0);
  	public FixedLengthStringData policyAnnivInterest = new FixedLengthStringData(1).isAPartOf(t6633Rec, 1);
  	public FixedLengthStringData annloan = new FixedLengthStringData(1).isAPartOf(t6633Rec, 2);
  	public FixedLengthStringData annpoly = new FixedLengthStringData(1).isAPartOf(t6633Rec, 3);
  	public FixedLengthStringData compfreq = new FixedLengthStringData(2).isAPartOf(t6633Rec, 4);
  	public ZonedDecimalData day = new ZonedDecimalData(2, 0).isAPartOf(t6633Rec, 6);
  	public ZonedDecimalData interestDay = new ZonedDecimalData(2, 0).isAPartOf(t6633Rec, 8);
  	public FixedLengthStringData interestFrequency = new FixedLengthStringData(2).isAPartOf(t6633Rec, 10);
  	public ZonedDecimalData intRate = new ZonedDecimalData(8, 5).isAPartOf(t6633Rec, 12);
  	public FixedLengthStringData inttype = new FixedLengthStringData(1).isAPartOf(t6633Rec, 20);
  	public ZonedDecimalData mperiod = new ZonedDecimalData(4, 0).isAPartOf(t6633Rec, 21);
  	public ZonedDecimalData nofDay = new ZonedDecimalData(3, 0).isAPartOf(t6633Rec, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(472).isAPartOf(t6633Rec, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6633Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6633Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}