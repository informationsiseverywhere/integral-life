package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH550
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh550ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1007);
	public FixedLengthStringData dataFields = new FixedLengthStringData(351).isAPartOf(dataArea, 0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billind = DD.billind.copy().isAPartOf(dataFields,2);
	public ZonedDecimalData billspfrom = DD.billspfrom.copyToZonedDecimal().isAPartOf(dataFields,3);
	public ZonedDecimalData billspto = DD.billspto.copyToZonedDecimal().isAPartOf(dataFields,11);
	public ZonedDecimalData bnsfrom = DD.bnsfrom.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData bnsind = DD.bnsind.copy().isAPartOf(dataFields,27);
	public ZonedDecimalData bnsto = DD.bnsto.copyToZonedDecimal().isAPartOf(dataFields,28);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,36);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,58);
	public ZonedDecimalData commfrom = DD.commfrom.copyToZonedDecimal().isAPartOf(dataFields,59);
	public ZonedDecimalData commto = DD.commto.copyToZonedDecimal().isAPartOf(dataFields,67);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,83);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,113);
	public ZonedDecimalData currto = DD.currto.copyToZonedDecimal().isAPartOf(dataFields,121);
	public FixedLengthStringData hintsupind = DD.hintsupind.copy().isAPartOf(dataFields,129);
	public ZonedDecimalData instpramt = DD.instpramt.copyToZonedDecimal().isAPartOf(dataFields,130);
	public FixedLengthStringData lapind = DD.lapind.copy().isAPartOf(dataFields,147);
	public ZonedDecimalData lapsfrom = DD.lapsfrom.copyToZonedDecimal().isAPartOf(dataFields,148);
	public ZonedDecimalData lapsto = DD.lapsto.copyToZonedDecimal().isAPartOf(dataFields,156);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData notind = DD.notind.copy().isAPartOf(dataFields,165);
	public ZonedDecimalData notsfrom = DD.notsfrom.copyToZonedDecimal().isAPartOf(dataFields,166);
	public ZonedDecimalData notsto = DD.notsto.copyToZonedDecimal().isAPartOf(dataFields,174);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,182);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,190);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,237);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,247);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,255);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,259);
	public FixedLengthStringData renind = DD.renind.copy().isAPartOf(dataFields,262);
	public FixedLengthStringData reptype = DD.reptype.copy().isAPartOf(dataFields,263);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,265);
	public ZonedDecimalData rnwlfrom = DD.rnwlfrom.copyToZonedDecimal().isAPartOf(dataFields,315);
	public ZonedDecimalData rnwlto = DD.rnwlto.copyToZonedDecimal().isAPartOf(dataFields,323);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,331);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(dataFields,341);
	public ZonedDecimalData znxtintdte = DD.znxtintdte.copyToZonedDecimal().isAPartOf(dataFields,343);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(164).isAPartOf(dataArea, 351);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billspfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billsptoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bnsfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bnsindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bnstoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData commfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData commtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData currtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData hintsupindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData instpramtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData lapindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData lapsfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData lapstoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData notindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData notsfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData notstoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData renindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData reptypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData rnwlfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData rnwltoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData srcebusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData znxtintdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(492).isAPartOf(dataArea, 515);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billspfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billsptoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bnsfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bnsindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bnstoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] commfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] commtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] currtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] hintsupindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] instpramtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] lapindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] lapsfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] lapstoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] notindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] notsfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] notstoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] renindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] reptypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] rnwlfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] rnwltoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] srcebusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] znxtintdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billspfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData billsptoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData bnsfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData bnstoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData commfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData commtoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currtoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lapsfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lapstoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData notsfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData notstoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rnwlfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rnwltoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData znxtintdteDisp = new FixedLengthStringData(10);

	public LongData Sh550screenWritten = new LongData(0);
	public LongData Sh550protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh550ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cnttypeOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypdescOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rstateOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pstateOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cownnumOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ownernameOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currfromOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currtoOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(resndescOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hintsupindOut,new String[] {null, null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, srcebus, rstate, pstate, occdate, reptype, register, cownnum, ownername, instpramt, lapind, lapsfrom, lapsto, cntcurr, billfreq, notind, notsfrom, notsto, mop, renind, rnwlfrom, rnwlto, ptdate, comind, commfrom, commto, btdate, bnsind, bnsfrom, bnsto, billind, currfrom, currto, reasoncd, resndesc, billspfrom, billspto, hintsupind, znxtintdte};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, srcebusOut, rstateOut, pstateOut, occdateOut, reptypeOut, registerOut, cownnumOut, ownernameOut, instpramtOut, lapindOut, lapsfromOut, lapstoOut, cntcurrOut, billfreqOut, notindOut, notsfromOut, notstoOut, mopOut, renindOut, rnwlfromOut, rnwltoOut, ptdateOut, comindOut, commfromOut, commtoOut, btdateOut, bnsindOut, bnsfromOut, bnstoOut, billindOut, currfromOut, currtoOut, reasoncdOut, resndescOut, billspfromOut, billsptoOut, hintsupindOut, znxtintdteOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, srcebusErr, rstateErr, pstateErr, occdateErr, reptypeErr, registerErr, cownnumErr, ownernameErr, instpramtErr, lapindErr, lapsfromErr, lapstoErr, cntcurrErr, billfreqErr, notindErr, notsfromErr, notstoErr, mopErr, renindErr, rnwlfromErr, rnwltoErr, ptdateErr, comindErr, commfromErr, commtoErr, btdateErr, bnsindErr, bnsfromErr, bnstoErr, billindErr, currfromErr, currtoErr, reasoncdErr, resndescErr, billspfromErr, billsptoErr, hintsupindErr, znxtintdteErr};
		screenDateFields = new BaseData[] {occdate, lapsfrom, lapsto, notsfrom, notsto, rnwlfrom, rnwlto, ptdate, commfrom, commto, btdate, bnsfrom, bnsto, currfrom, currto, billspfrom, billspto, znxtintdte};
		screenDateErrFields = new BaseData[] {occdateErr, lapsfromErr, lapstoErr, notsfromErr, notstoErr, rnwlfromErr, rnwltoErr, ptdateErr, commfromErr, commtoErr, btdateErr, bnsfromErr, bnstoErr, currfromErr, currtoErr, billspfromErr, billsptoErr, znxtintdteErr};
		screenDateDispFields = new BaseData[] {occdateDisp, lapsfromDisp, lapstoDisp, notsfromDisp, notstoDisp, rnwlfromDisp, rnwltoDisp, ptdateDisp, commfromDisp, commtoDisp, btdateDisp, bnsfromDisp, bnstoDisp, currfromDisp, currtoDisp, billspfromDisp, billsptoDisp, znxtintdteDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh550screen.class;
		protectRecord = Sh550protect.class;
	}

}
