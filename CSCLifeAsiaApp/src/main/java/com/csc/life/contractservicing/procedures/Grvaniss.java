/*
 * File: Grvaniss.java
 * Date: 29 August 2009 22:51:14
 * Author: Quipoz Limited
 * 
 * Class transformed from GRVANISS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.terminationclaims.dataaccess.AnntlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnnylnbTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  OVERVIEW
*  ========
*
* This is  a  new  subroutine  which  forms  part  of  the  9405
* Annuities  Development.    It  will  be called from T5671 when
* reversing a transaction which 'issued' ANNY records, i.e.  one
* which  converted  ANNT  records into ANNY records.  It will be
* called during the Alter from Inception transaction.
*
* This subroutine will delete the ANNY records  created  by  the
* contract  issue  transaction  and recreate the ANNT record for
* the proposal using the details held on the ANNY record.
*
*****************************************************************
* </pre>
*/
public class Grvaniss extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSeqnbr = new PackedDecimalData(3, 0).init(ZERO);
		/* FORMATS */
	private String anntlnbrec = "ANNTLNBREC";
	private String annylnbrec = "ANNYLNBREC";
		/*Annuity Details - Temporary*/
	private AnntlnbTableDAM anntlnbIO = new AnntlnbTableDAM();
		/*Annuity Details Life New Business*/
	private AnnylnbTableDAM annylnbIO = new AnnylnbTableDAM();
	private Greversrec greversrec = new Greversrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Grvaniss() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*STARTS*/
		getAnny1000();
		convertAnnyAnnt2000();
		deleteAnnyRewriteAnnt3000();
		exitProgram();
	}

protected void getAnny1000()
	{
		start1000();
	}

protected void start1000()
	{
		annylnbIO.setChdrcoy(greversrec.chdrcoy);
		annylnbIO.setChdrnum(greversrec.chdrnum);
		annylnbIO.setLife(greversrec.life);
		annylnbIO.setCoverage(greversrec.coverage);
		annylnbIO.setRider(greversrec.rider);
		annylnbIO.setPlanSuffix(greversrec.planSuffix);
		annylnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		annylnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		annylnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		annylnbIO.setFormat(annylnbrec);
		SmartFileCode.execute(appVars, annylnbIO);
		if (isNE(annylnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annylnbIO.getParams());
			syserrrec.statuz.set(annylnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(greversrec.chdrcoy,annylnbIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,annylnbIO.getChdrnum())
		|| isNE(greversrec.life,annylnbIO.getLife())
		|| isNE(greversrec.coverage,annylnbIO.getCoverage())
		|| isNE(greversrec.rider,annylnbIO.getRider())) {
			syserrrec.params.set(annylnbIO.getParams());
			syserrrec.statuz.set(annylnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(annylnbIO.getValidflag(),"1")
		|| isNE(annylnbIO.getTranno(),greversrec.tranno)) {
			syserrrec.params.set(annylnbIO.getParams());
			syserrrec.statuz.set(annylnbIO.getStatuz());
			fatalError600();
		}
	}

protected void convertAnnyAnnt2000()
	{
		starts2009();
	}

protected void starts2009()
	{
		anntlnbIO.setChdrcoy(annylnbIO.getChdrcoy());
		anntlnbIO.setChdrnum(annylnbIO.getChdrnum());
		anntlnbIO.setLife(annylnbIO.getLife());
		anntlnbIO.setCoverage(annylnbIO.getCoverage());
		anntlnbIO.setRider(annylnbIO.getRider());
		wsaaSeqnbr.add(1);
		anntlnbIO.setSeqnbr(wsaaSeqnbr);
		anntlnbIO.setGuarperd(annylnbIO.getGuarperd());
		anntlnbIO.setFreqann(annylnbIO.getFreqann());
		anntlnbIO.setArrears(annylnbIO.getArrears());
		anntlnbIO.setAdvance(annylnbIO.getAdvance());
		anntlnbIO.setDthpercn(annylnbIO.getDthpercn());
		anntlnbIO.setDthperco(annylnbIO.getDthperco());
		anntlnbIO.setIntanny(annylnbIO.getIntanny());
		anntlnbIO.setWithprop(annylnbIO.getWithprop());
		anntlnbIO.setWithoprop(annylnbIO.getWithoprop());
		anntlnbIO.setPpind(annylnbIO.getPpind());
		anntlnbIO.setCapcont(annylnbIO.getCapcont());
		anntlnbIO.setNomlife(annylnbIO.getNomlife());
		anntlnbIO.setPlanSuffix(ZERO);
	}

protected void deleteAnnyRewriteAnnt3000()
	{
		starts3000();
	}

protected void starts3000()
	{
		annylnbIO.setFunction(varcom.deltd);
		annylnbIO.setFormat(annylnbrec);
		SmartFileCode.execute(appVars, annylnbIO);
		if (isNE(annylnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annylnbIO.getParams());
			syserrrec.statuz.set(annylnbIO.getStatuz());
			fatalError600();
		}
		anntlnbIO.setFunction(varcom.writr);
		anntlnbIO.setFormat(anntlnbrec);
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(anntlnbIO.getParams());
			syserrrec.statuz.set(anntlnbIO.getStatuz());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
