package com.csc.life.contractservicing.recordstructures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class TaxDeductionRec extends ExternalData {
	public FixedLengthStringData statuz = new FixedLengthStringData(4);
	private String coyPfx;
	private String chdrcoy;
	private String chdrnum;
	private String cnttype;
	private int datefrm;
	private int dateto;
	private List<String> contributionAmt = new ArrayList<>();
	private List<String> contributionType = new ArrayList<>();
	private int effdate;
	private BigDecimal riskPremium;
	private String count;
	private BigDecimal totalTaxAmt;
	private boolean noticeCorrection;
	
	public String getCoyPfx() {
		return coyPfx;
	}
	public void setCoyPfx(String coyPfx) {
		this.coyPfx = coyPfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	
	public List<String> getContributionAmt() {
		return contributionAmt;
	}
	public void setContributionAmt(List<String> contributionAmt) {
		this.contributionAmt = contributionAmt;
	}
	public List<String> getContributionType() {
		return contributionType;
	}
	public void setContributionType(List<String> contributionType) {
		this.contributionType = contributionType;
	}
	public int getDatefrm() {
		return datefrm;
	}
	public void setDatefrm(int datefrm) {
		this.datefrm = datefrm;
	}
	public int getDateto() {
		return dateto;
	}
	public void setDateto(int dateto) {
		this.dateto = dateto;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public BigDecimal getRiskPremium() {
		return riskPremium;
	}
	public void setRiskPremium(BigDecimal riskPremium) {
		this.riskPremium = riskPremium;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public BigDecimal getTotalTaxAmt() {
		return totalTaxAmt;
	}
	public void setTotalTaxAmt(BigDecimal totalTaxAmt) {
		this.totalTaxAmt = totalTaxAmt;
	}
	@Override
	public void initialize() {
		// TODO Auto-generated method stub	
	}
	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public boolean isNoticeCorrection() {
		return noticeCorrection;
	}
	public void setNoticeCorrection(boolean noticeCorrection) {
		this.noticeCorrection = noticeCorrection;
	}

}
