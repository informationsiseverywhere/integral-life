package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:24
 * Description:
 * Copybook name: ZRUTNRVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrutnrvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrutnrvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zrutnrvKey = new FixedLengthStringData(64).isAPartOf(zrutnrvFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrutnrvChdrcoy = new FixedLengthStringData(1).isAPartOf(zrutnrvKey, 0);
  	public FixedLengthStringData zrutnrvChdrnum = new FixedLengthStringData(8).isAPartOf(zrutnrvKey, 1);
  	public FixedLengthStringData zrutnrvLife = new FixedLengthStringData(2).isAPartOf(zrutnrvKey, 9);
  	public FixedLengthStringData zrutnrvCoverage = new FixedLengthStringData(2).isAPartOf(zrutnrvKey, 11);
  	public PackedDecimalData zrutnrvPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(zrutnrvKey, 13);
  	public PackedDecimalData zrutnrvTranno = new PackedDecimalData(5, 0).isAPartOf(zrutnrvKey, 16);
  	public PackedDecimalData zrutnrvProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(zrutnrvKey, 19);
  	public FixedLengthStringData zrutnrvUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(zrutnrvKey, 21);
  	public FixedLengthStringData zrutnrvUnitType = new FixedLengthStringData(1).isAPartOf(zrutnrvKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(zrutnrvKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrutnrvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrutnrvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}