/******************************************************************************
 * File Name 		: LhdrqpfDAO.java
 * Author			: wli31
 * Creation Date	: 7 Aug 2019
 * Project			: Integral Life
 * Description		: The DAO Interface for Lhdrpf table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.contractservicing.dataaccess.dao;

import com.csc.life.contractservicing.dataaccess.model.Lhdrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LhdrpfDAO extends BaseDAO<Lhdrpf> {
	void insertLhdrpfRecord(Lhdrpf lhdrpf );

}
