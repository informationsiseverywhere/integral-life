/*
 * File: Genssw.java
 * Date: November 5, 2011 5:27:21 AM IST
 * Author: Quipoz Limited
 * 
 * Class transformed from GENSSW.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;


import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.life.contractservicing.recordstructures.Pd5h5rec;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*       DRIVER FOR CLIENT II SCROLL SELECTION
*
* This program drives the Client II scroll system when CF6
* is pressed on the field CLTTWO.
*
* This program runs until one of the following occurs:
*     - a command key is pressed to request a return "above"
*       the submenu (e.g. to the system or master menus),
*     - a non-blank WSSP-EDTERROR status is returned
*       (i.e. a system or database error has occurred).
*
************************************************************
* </pre>
*/
public class Pd5h5 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("PD5H5");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaNextProg = new FixedLengthStringData(5);

	private Wsspcomn wsspcomn = new Wsspcomn();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Pd5h5rec pd5h5rec = new Pd5h5rec();
	
	private BabrpfDAO babrpfDao = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	private Babrpf babrpf = null;
	private String temp = "";
	private StringBuffer tembuffer;
    private int len = 0;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		callNextProgram100, 
		endProgram200, 
		returnUpStack400
	}

	public Pd5h5() {
		super();
	}



	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		/* MOVE LOW-VALUE              TO WSSP-VALUE.                   */
		/* If '?' search specfied, WSSP-VALUE is setup with search key     */
		/* then WSSP-VALUE is not required to initialize with LOW-VALUE    */
		if (isEQ(wsspwindow.value, SPACES)) {
			wsspwindow.value.set(LOVALUE);
		}
		
		/*  Set up stack for switching*/
		pd5h5rec.bnysel.set(wsspwindow.additionalFields.substring(0, 10));
		pd5h5rec.bankkey.set(wsspwindow.additionalFields.substring(10, 20));
		pd5h5rec.paymmeth.set(wsspwindow.additionalFields.substring(20, 21));
		wsspcomn.secSwitching.set(SPACES);
		wsspcomn.secProg[1].set(wsspcomn.next2prog);
		wsspcomn.secProg[2].set(wsspcomn.next3prog);
		wsspcomn.secProg[3].set(wsspcomn.next4prog);
		wsspcomn.clntkey.set("CN9".concat(pd5h5rec.bnysel.trim()));
		wsspwindow.filler.set(wsaaProg);//ICIL-370
		if(pd5h5rec.paymmeth.equals("C")) {
			wsspcomn.programPtr.set(1);
			wsspcomn.filler.set(wsaaProg);//ICIL-370
			wsspcomn.nextprog.set(wsspcomn.secProg[wsspcomn.programPtr.toInt()]);
			wsaaNextProg.set(wsspcomn.nextprog);
			callProgram(wsaaNextProg, wsspcomn.commonArea, wsspwindow.userArea);
		}else if(pd5h5rec.paymmeth.equals("4") 
/*IBPLIFE-7075 Starts*/		|| pd5h5rec.paymmeth.equals("2") || 
				pd5h5rec.paymmeth.equals("1"))  /*IBPLIFE-7075 Ends*/
		{
			wsspcomn.programPtr.set(2);
			wsspcomn.nextprog.set(wsspcomn.secProg[wsspcomn.programPtr.toInt()]);
			wsaaNextProg.set(wsspcomn.nextprog);
			callProgram(wsaaNextProg, wsspcomn.commonArea, wsspwindow.userArea);
		}
		
		getotherField();
		wsspwindow.confirmation.set(wsspwindow.value.toString().trim());
		temp = wsspwindow.value.toString().trim();	
		if(temp != null && !temp.equals("")) {
		
	 	 len = temp.length();
		 temp = temp.substring(len-4, len);
		 tembuffer = new StringBuffer("");
		 for(int i=0; i<len-4; i++) {
			tembuffer.append("*");
		 }
		 tembuffer.append(temp);
		 wsspwindow.value.set(tembuffer.toString());
		}else {
		 wsspwindow.value.set(SPACES);
		 wsspwindow.additionalFields.set(pd5h5rec.bnysel.toString()+pd5h5rec.bankkey.toString()+pd5h5rec.paymmeth.toString());
		}
		wsspwindow.filler.set(SPACES);//ICIL-370
		exitProgram();
	
		
	}

  protected void getotherField() {
	  if(isEQ(wsspwindow.bankkey, SPACES)) return ;
	  babrpf = babrpfDao.searchBankkey(wsspwindow.bankkey.toString());
		if(babrpf != null) {
			pd5h5rec.bankkeydesc.set(babrpf.getBankdesc());
			wsspwindow.additionalFields.set(pd5h5rec.bnysel.toString().concat(wsspwindow.bankkey.toString()).concat(pd5h5rec.paymmeth.toString().concat("         "))
					.concat(pd5h5rec.bankkeydesc.toString()));
		}else {
			wsspwindow.additionalFields.set(pd5h5rec.bnysel.toString().concat(wsspwindow.bankkey.toString()).concat(pd5h5rec.paymmeth.toString().concat("         ")));
		}
		
  }


}

