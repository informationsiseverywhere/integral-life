package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Feddpf {
	private long unique_Number;
	private String chdrpfx;
	private String chdrcoy;
	private String chdrnum;
//  private BigDecimal stat_change_dat ;
	private BigDecimal statuschangedat; //ILIFE-7887
	private String validflag;
	private String feddflag;
	private String fpvflag;
//  private String user_profile;
//  private String jobname;
	private String usrprf; //ILIFE-7887
	private String jobnm; //ILIFE-7887
	private Timestamp datime;
	public long getUnique_Number() {
		return unique_Number;
	}
	public void setUnique_Number(long unique_Number) {
		this.unique_Number = unique_Number;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	/*public BigDecimal getStat_change_dat() {
		return stat_change_dat;
	}
	public void setStat_change_dat(BigDecimal stat_change_dat) {
		this.stat_change_dat = stat_change_dat;
	}*/
	//ILIFE-7887-- Start
	
	//ILIFE-7887-- End
	public String getValidflag() {
		return validflag;
	}
	public BigDecimal getStatuschangedat() {
		return statuschangedat;
	}
	public void setStatuschangedat(BigDecimal statuschangedat) {
		this.statuschangedat = statuschangedat;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getFeddflag() {
		return feddflag;
	}
	public void setFeddflag(String feddflag) {
		this.feddflag = feddflag;
	}
	public String getFpvflag() {
		return fpvflag;
	}
	public void setFpvflag(String fpvflag) {
		this.fpvflag = fpvflag;
	}
	/*public String getUser_profile() {
		return user_profile;
	}
	public void setUser_profile(String user_profile) {
		this.user_profile = user_profile;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}*/
	//ILIFE-7887-- Start
		public String getUsrprf() {
			return usrprf;
		}
		public void setUsrprf(String Usrprf) {
			this.usrprf = usrprf;
		}
		public String getJobnm() {
			return jobnm;
		}
		public void setJobnm(String Jobnm) {
			this.jobnm = jobnm;
		}
	//ILIFE-7887-- End
	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}
	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	
	
	
	
	
	
	
}
