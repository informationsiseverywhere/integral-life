package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:00
 * Description:
 * Copybook name: AGCMREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmrevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData agcmrevKey = new FixedLengthStringData(256).isAPartOf(agcmrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmrevChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmrevKey, 0);
  	public FixedLengthStringData agcmrevChdrnum = new FixedLengthStringData(8).isAPartOf(agcmrevKey, 1);
  	public FixedLengthStringData agcmrevLife = new FixedLengthStringData(2).isAPartOf(agcmrevKey, 9);
  	public FixedLengthStringData agcmrevCoverage = new FixedLengthStringData(2).isAPartOf(agcmrevKey, 11);
  	public FixedLengthStringData agcmrevRider = new FixedLengthStringData(2).isAPartOf(agcmrevKey, 13);
  	public PackedDecimalData agcmrevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmrevKey, 15);
  	public FixedLengthStringData agcmrevAgntnum = new FixedLengthStringData(8).isAPartOf(agcmrevKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(230).isAPartOf(agcmrevKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}