/*
 * File: Pr572.java
 * Date: 30 August 2009 1:43:34
 * Author: Quipoz Limited
 *
 * Class transformed from PR572.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;//ILB-499
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CvuwTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CvuwpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Cvuwpf;
import com.csc.life.contractservicing.procedures.LincCSService;
import com.csc.life.contractservicing.procedures.UwoccupationUtil;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.contractservicing.screens.Sr572ScreenVars;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.tablestructures.T5657rec;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5673rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.SftlockUtil; //ILB-499
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.SftlockRecBean; //ILB-499
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                       COMPONENT SELECTION
*
*                       ===================
* Initialise
* ----------
*
* Skip  this  section  if  returning from an optional selection
* (current stack position action flag = '*').
*
* Clear the subfile ready for loading.
*
* Read  CHDRMJA  (RETRV) in order to obtain the contract header
* information  and  read  LIFEMJA  (RETRV) for the life assured
* details.
*
* Read  the  contract definition description from T5688 for the
* contract type held on CHDRMJA.
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*       - using the life  detaiLs  passed (retrieved above from
*         LIFEMJA, joint life number '00').  Look  up the  name
*         from  the  client  details  (CLTS)  and  format as  a
*         "confirmation name".
*
*       - read the joint life details using LIFEMJA (joint life
*         number '01').  If  found, look up  the name  from the
*         client details (CLTS) and  format as  a "confirmation
*         name".
*
* This  program displays component parts for either a life or a
* coverage.  To determine which version is required, attempt to
* retrieve the current coverage transaction (from COVRMJA).  If
* there  is no COVRMJA record to retrieve (MRNF return status),
* coverages  and  riders  for the entire contract are required.
* Otherwise, just riders for the current coverage are required.
*
* Read  the  contract  structure  table  (T5673),  accessed  by
* contract  type  (from  CHDRMJA) and effective on the original
* inception  date  of  the contract. When processing this table
* item,  it  can  continue  onto  additional  (unlimited) table
* items.  These  should  be  read in turn during the processing
* described below.  The  original  inception  date must also be
* used to provide the correct version of each item read.
*
* THE CASE OF ALL COVERAGES AND RIDERS
*
* Taking one coverage at  a  time, one record is written to the
* subfile for the  coverage,  with one for each rider attaching
* to it. The coverage record is written with a blank rider code
* and the rider with a blank coverage code. In the table, there
* is  space for six riders against each coverage. It there is a
* requirement for more than six riders on a coverage, they will
* be  continued  on  the next line.  In this case, the coverage
* will  be  left  blank  to  indicate  a continuation.  NB, the
* coverage  on the last line of a table item could be continued
* onto  another  item.  Look  up  the  long  description of the
* coverage/rider   from  the  general  coverage  details  table
* (T5687) for each subfile record written.
*
* It is possible that the proposal has already been set up when
* a user  requests  this  screen.  Read  the  current  coverage
* transaction   records   for   the  coverage  being  processed
* (COVRCOV,   keyed   company,   contract-no,   coverage  type,
* coverage-no; select  rider-no  = 0). Accumulate the number of
* DIFFERENT coverages  of  the same type already stored. (There
* could be multiple  records  with  the  same  key.  Ignore the
* duplicates.) Whenever  a  transaction is read for a different
* coverage, keep  the  number  of the highest transaction read.
* This is needed  to  calculate  the next coverage number to be
* allocated. (The  coverage  number  needed  is the highest one
* used by any coverage  type.) If there is already at least one
* coverage transaction, put  a  '+'  in  the  select field.  If
* there is already the maximum allowable (as defined in T5673),
* protect the select field so that no more can be requested. If
* the coverage is  mandatory  (this is indicated by an "R", for
* required, in  the  coverage required indicator) and there are
* no transactions for  the  coverage on file, "select" the line
* by moving "X" in to the select field and protect it.
*
* When adding  rider details to  subfile, they may be mandatory
* (indicated in a similar way to mandatory coverages above). In
* this case, "select" the  line  by moving "X" in to the select
* field and protect  it.  -  note  that rider selection will be
* ignored if the coverage is not selected.
*
* Details of all  coverages  and  riders  for the contract type
* should be loaded in this case.
*
* THE CASE OF ALL RIDERS FOR ONE COVERAGE ONLY
*
* In this case, a COVRMJA record will have been retrieved. This
* will hold details of  the  coverage to which riders are to be
* added. Write a coverage  record to the subfile, with a '+' in
* the selection field,  and with this field protected. (Look up
* the description as above.)
*
* Search through  the  contract structure table entries (T5673)
* for the applicable  coverage  code. This will give the riders
* attachable to this  coverage  in this case. Write a record to
* the subfile for each applicable rider.
*
* It is possible that  the riders have already been set up when
* a  user   requests   this  screen.  Read  the  current  rider
* transaction record  for  the  rider being processed (COVRRID,
* keyed  company, contract-no, coverage-no, rider type;  select
* rider-no *ne 0).  If  there  is already a record, this is the
* maximum allowable, so  put  a  '+'  in  the  select field and
* protect it so that no more can be requested.  If the rider is
* mandatory (this is  indicated by an "R", for required, in the
* rider required indicator) and there is no transaction for the
* rider on file, "select"  the  line  by  moving  "X" in to the
* select field and protect it.
*
* Whenever a transaction  is  read  for a different rider, keep
* the number of  the  highest transaction read.  This is needed
* to calculate the next rider number to be allocated.
*
* In all cases, load  all pages required in the subfile and set
* the subfile more indicator to no.
*
* Validation
* ----------
*
* Skip  this  section  if  returning from an optional selection
* (current stack position action flag = '*').
*
* Read  all   modified  subfile  records.  The  only  allowable
* selections are  '+',  'X'  and  blank.  Ignore inconsistences
* between riders being selected for coverages which have not.
*
* Updating
* --------
*
* This program performs no updating.
*
* Next program
* ------------
*
* If returning from  a  selection (action action is '*'), check
* that  if  the  current  component  was  mandatory,  that  his
* actually been entered.  (Use  a  hidden  field in the subfile
* record when loading in  the 1000 section to indicate if it is
* a required  section?)  Read the current coverage/ transaction
* record again (COVRMJA,  the  key  was  set up by this program
* previously.) If  there  is  no  transaction for the mandatory
* component, KEEPS the  current  key  again and exit.
*
*
*
*
*
* If not returning  from  a  component (stack action is blank),
* save the next four  programs currently on the stack. Read the
* first record from  the  subfile. If this is not selected ('X'
* in select field), read  the  next  one  and  so  on,  until a
* selected record is  found,  or  the  end  of  the  subfile is
* reached.
*
* For the case  when  all  coverages  and riders are displayed,
* keep track of whether  a  coverage  is  selected or not. If a
* coverage is not  selected,  skip  all its riders even if they
* are selected.  If  a  coverage  is  selected, re-set the next
* rider sequence number to zero.
*
* For the case  when  only  the  riders  for  one  coverage are
* displayed, the coverage cannot be selected. Process the rider
* selected even though the coverage has not.
*
* Once the end  of  the  subfile  has been reached, restore the
* previously saved  four  programs, blank out the stack action,
* add one to the pointer and exit.
*
* If a subfile  record  has been selected, look up the programs
* required to  be  processed  from  the coverage/rider programs
* table (T5671  -  accessed  by transaction number concatenated
* with coverage/rider code from the subfile record). Move these
* four programs into  the  program  stack  and  set the current
* stack action to '*'  (so  that the system will return to this
* program to process the next one).
*
* Set up the  key details of the coverage/rider to be processed
* (in COVRMJA using  the KEEPS function) by the called programs
* as follows:
*
*       Company - WSSP company
*       Contract no - from CHDRMJA
*       Life number - from LIFEMJA
*       Coverage number -  from COVRMJA if read in the 1000
*            section  (i.e.  adding  to  coverage case), or
*            sequentially from the number calculated in the
*            1000  section for each coverage selected (i.e.
*            adding to life case)
*       Rider number -  '00'  if  this  is  a  coverage, or
*            sequentially from the number calculated above
*
* Add one to  the  program  pointer  and  exit  to  process the
* required generic component.
*
******************Enhancements for Life Asia 1.0****************
*
* In order to allow components to be selected on a specific life,
* a new indicatore has been introduced to T5673. This indicator
* specifies whether the component can be selected for just the
* primary life, or just the subsequent lives or for all lives.
* This module uses the indicator T5673-ZRLIFIND to determine
* which component may be selected. The processing is as follows :
*
* - For each component found on T5673, check the T5673-ZRLIFIND :
*
*   - If we are processing a primary life and the T5673-ZRLIFIND
*     contains an 'S' for that component, then don't set up the
*     component details for display.
*
*   - If we are processing subsequent lives and T5673-ZRLIFIND
*     contains an 'P' for that component, then don't set up the
*     component details for display.
*
*****************************************************************
* </pre>
*/
public class Pr572 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
//	public int numberOfParameters = 0; // to resolve findbug commented 
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR572");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub3 = new PackedDecimalData(2, 0).init(1);
		/*    WSAA-SUB4 and WSAA-SUB5 relate to Program pointers and
		    counts for the handling of the program stack.*/
	private PackedDecimalData wsaaSub4 = new PackedDecimalData(2, 0).init(ZERO);
	private PackedDecimalData wsaaSub5 = new PackedDecimalData(2, 0).init(ZERO);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData sub3 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData ctSub = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData rdSub = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaCcount = new PackedDecimalData(2, 0).init(ZERO);
	private String wsaaNoSelect = "N";
	private String wsaaLifeorcov = "N";
	private FixedLengthStringData wsaaPrevCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRetrvLife = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaIfCovrSelected = new FixedLengthStringData(1).init("N");
	private Validator wsaaCovrSelected = new Validator(wsaaIfCovrSelected, "Y");
			/*    The following are used will store the Rider number being
		    selected.*/
	private FixedLengthStringData wsaaRiderRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaRider = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiderRider, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaRiderRider, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaRiderR = new FixedLengthStringData(2).isAPartOf(filler, 0);

		/*    The following are used will store the Coverage number being
		    selected.*/
	private FixedLengthStringData wsaaCoverCover = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaCoverage = new ZonedDecimalData(2, 0).isAPartOf(wsaaCoverCover, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaCoverCover, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaCoverageR = new FixedLengthStringData(2).isAPartOf(filler1, 0);
		/* WSAA-RIDER-NO-TABLE */
	private ZonedDecimalData[] wsaaRidrTab = ZDInittedArray(99, 2, 0, UNSIGNED_TRUE);
	private BinaryData wsaaRidrSub = new BinaryData(3, 0).init(0);

	private FixedLengthStringData wsaaNextCovridno = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaNextCovno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 0);
	private FixedLengthStringData wsaaNextRidno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 2);

	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaNextCovridno, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaNextCovnoR = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextRidnoR = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2).setUnsigned();

		/*    flag to determine whether a you are processing a  LIFE or a
		    COVERAGE Cover type.*/
	private FixedLengthStringData wsaaLineType = new FixedLengthStringData(1);
	private Validator wsaaLcover = new Validator(wsaaLineType, "1");
	private Validator wsaaCcover = new Validator(wsaaLineType, "2");

		/*    flag to determine if the next component is a Cover or Rider
		    component.*/
	private FixedLengthStringData wsaaCoverFlag = new FixedLengthStringData(1);
	private Validator wsaaCover = new Validator(wsaaCoverFlag, "Y");

		/*    flag for exit of perform processing.*/
	private FixedLengthStringData wsaaExitFlag = new FixedLengthStringData(1);
	private Validator wsaaExit = new Validator(wsaaExitFlag, "Y");

		/*    flag for exit of 4000 Next selection check.*/
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");

		/*    flag for exit of 4000 processing.*/
	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");

		/*    Program save area for next four programs.*/
	private FixedLengthStringData wsaaProgramSave = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(4, 5, wsaaProgramSave, 0);

		/*    Concatination of Cover/Rider for table T5671.*/
	private FixedLengthStringData wsaaConcatName = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrancode = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 4);
		/*    LIFEMJA 1st Life key held for access search of Joint life.*/
	private FixedLengthStringData wsaaLifeKey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaIndicStatus = new FixedLengthStringData(1);
	private Validator indicFound = new Validator(wsaaIndicStatus, "Y");
	private FixedLengthStringData wsaaClcode = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLastCovrmjaCrtable = new FixedLengthStringData(4);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	
	private FixedLengthStringData wsaaSelectedCoverTable = new FixedLengthStringData(192);
	private FixedLengthStringData[] wsaaSelectedCover = FLSArrayPartOfStructure(48, 4, wsaaSelectedCoverTable, 0);
		/* WSBB-COVER-NUMBER-USED */
	private FixedLengthStringData[] wsbbClcode = FLSInittedArray(48, 4);
	private ZonedDecimalData[] wsbbCoverage = ZDInittedArray(48, 2, 0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsbbLastRider = ZDInittedArray(48, 2, 0, UNSIGNED_TRUE);
	private ZonedDecimalData wsbbLastCoverage = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private String wsaaChange = "";
		
		/* ERRORS */
	private static final String e005 = "E005";
	private static final String e032 = "E032";
	private static final String g433 = "G433";
	private static final String h999 = "H999";
	private static final String h248 = "H248";
	private static final String h249 = "H249";
	private static final String hl01 = "HL01";
	private static final String f321 = "F321";
	private static final String f665 = "F665";
	private static final String g931 = "G931";
		/* TABLES */
                                             
	private static final String t5673 = "T5673";
                
	private IntegerData ctIndex = new IntegerData();
	private IntegerData rdIndex = new IntegerData();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
                                                  
		/*Coverage Comp Logical View - Major Alts*/
                                                           
		/*Coverage/Rider details - Major Alts*/
	//private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Rider Component Logical View - Major Alt*/
                                                           
		/*Coverage transaction Record - Major Alts*/

		/*Coverage underwriting details*/
	private CvuwTableDAM cvuwIO = new CvuwTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
                                                  
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();   //ILB-499
		/*Policy transaction history for reversals*/
//	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5673rec t5673rec = new T5673rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T7508rec t7508rec = new T7508rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Wssplife wssplife = new Wssplife();
	private Sr572ScreenVars sv = ScreenProgram.getScreenVars( Sr572ScreenVars.class);
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT5673StoredTableInner wsaaT5673StoredTableInner = new WsaaT5673StoredTableInner();
	
	//TSD-266 Starts
	private ZonedDecimalData wsaaLDob = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaJlDob = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCDob = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaJcDob = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaLSex = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaCSex = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaJlSex = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaJcSex = new FixedLengthStringData(1).init(SPACES);
	private boolean ispermission = false;
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	//TSD-266 Ends


//ILIFE-3310-add by liwei
 //ILIFE-8723 start
//	private Map<String, List<Itempf>> t5679ListMap = new HashMap<String, List<Itempf>>();
//	private Map<String, List<Itempf>> t5671ListMap = new HashMap<String, List<Itempf>>();	
//	private Map<String, List<Itempf>> t7508ListMap = new HashMap<String, List<Itempf>>();
	/*private Map<String,Descpf> t5688Map =  new HashMap<String, Descpf>();
	private Map<String,Descpf> t3623Map =  new HashMap<String, Descpf>();
	private Map<String,Descpf> t3588Map =  new HashMap<String, Descpf>();
	private Map<String,Descpf> t5687Map =  new HashMap<String, Descpf>();*/
 //ILIFE-8723 end
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private CvuwpfDAO cvuwpfDAO = getApplicationContext().getBean("cvuwpfDAO", CvuwpfDAO.class);
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	
	private Cvuwpf cvuwpf = null;
	//private Covrpf covrrid = null;
	private Covtpf covtrid = null;
	private Covtpf covtpf = new Covtpf();
	private Clntpf clntpf = null;
	private List<Covtpf> covtList = new ArrayList<Covtpf>();
	private List<Covtpf> covtcovList = new ArrayList<Covtpf>();
//	private List<Covrpf> covrcovList = new ArrayList<Covrpf>();
	
	//private Covrpf lastCovrmja = null;
	
	 //ILB-456 start 
    private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;
//ILB-499 start
	private Ptrnpf ptrnpf;
	private List<Ptrnpf> ptrnpfList = null;
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private SftlockUtil sftlockUtil = new SftlockUtil();
	//ILB-499 end
 //ILIFE-8723 start	
	private Descpf descpf = new Descpf();
	private Itempf itempf = new Itempf();
 //ILIFE-8723 end 
	private boolean lincFlag = false;
	private static final String lincFeature = "NBPRP116";
	private static final String tjl47 = "TJL47";
	
	private UwoccupationUtil uwoccupationUtil = getApplicationContext().getBean("uwoccupationUtil", UwoccupationUtil.class);
	private UWOccupationRec uwOccupationRec = new UWOccupationRec();
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private boolean uwFlag = false;
	public static final String AUTO_TERMS_LIST_ADD = "AUTO_TERMS_LIST_ADD";
	private ItemTableDAM itemIO = new ItemTableDAM();
	private int seqnbr = 1;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090,
		exit1190,
		subfileLoad1210,
		exit1490,
		selectCheck2060,
		exit2090,
		exit4190,
		exit4290,
		exit4490,
		exit4590,
		readProgramTable4610,
		exit4690,
		exit4790,
		exit4990
	}

	public Pr572() {
		super();
		screenVars = sv;
		new ScreenModel("Sr572", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
		//	initSmartTable();//Ilife3310- add by liwei  //ILIFE-8723
			skipOnFlag1010();
			initialiseSubfile1020();
			retreiveHeader1030();
			readContractLongdesc1040();
			retreiveUwdate1045();
			retreiveLife1050();
			lifeOrJlife1060();
			coverRiderTable1070();
			// IBPLIFE-1503
			uwFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP123", appVars, smtpfxcpy.item.toString());
			if(uwFlag) {
				sv.uwFlag = 1;
				initialiseUWparameter();
			}else {
				sv.uwFlag = 2;
			}
			retrieveCurrCoverage1080();
			contractTypeStatus1070(); //ILIFE-1403 START by nnazeer
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}
private void initialiseUWparameter() {
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	uwOccupationRec = new UWOccupationRec();
	uwOccupationRec.indusType.set(clntpf.getStatcode());
	uwOccupationRec.occupCode.set(clntpf.getOccpcode());
	uwOccupationRec.transEffdate.set(datcon1rec.intDate);
	uwlvIO.setParams(SPACES);
	uwlvIO.setCompany(wsspcomn.company);
	uwlvIO.setUserid(wsspcomn.userid);
	uwlvIO.setFormat("UWLVREC");
	uwlvIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, uwlvIO);
	sv.uwResultMap.clear();
	sv.mainCovrDisFlag = -1;
	sv.tableCovrDisFlag = -1;
	sv.mainCovrDisPos = -1;
	sv.tableCovrDisPos = -1;
}

/**
 * 
 * @param crtable
 * @return 0:accept, 1:Decline & user has authority, 2:Decline & user doesn't has authority
 */
private int checkComponent(String crtable) {
	int result = -1;
	if(sv.uwResultMap.containsKey(crtable)) {
		result = sv.uwResultMap.get(crtable);
	}else {
		String company = wsspcomn.company.toString();
		String fsuco = wsspcomn.fsuco.toString();
		String cnttype = chdrpf.getCnttype();
		if (crtable != null && !crtable.isEmpty()) {
			result = uwoccupationUtil.getUWResult(uwOccupationRec, company, fsuco, cnttype, crtable);
			if (sv.uwResultMap != null) {
				sv.uwResultMap.put(crtable, result);
			}
		}
	}
	
	if (result == 2) {
		if (!"Y".equals(uwlvIO.getUwdecision().toString().trim())) {
			// DISABLED
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			return 2;
		}
		return 1;
	}
	return 0;
}

protected void initSmartTable(){
	String coy = wsspcomn.company.toString();
 //ILIFE-8723 start
//	t5679ListMap = itemDAO.loadSmartTable("IT", coy, "T5679");
//	t5671ListMap = itemDAO.loadSmartTable("IT", coy, "T5671");	
//	t7508ListMap = itemDAO.loadSmartTable("IT", coy, "T7508");
//	t5688Map = descDAO.getItems("IT", coy, "T5688", wsspcomn.language.toString());
//	t3623Map = descDAO.getItems("IT", coy, "T3623", wsspcomn.language.toString());
//	t3588Map = descDAO.getItems("IT", coy, "T3588", wsspcomn.language.toString());
//	t5687Map = descDAO.getItems("IT", coy, "T5687", wsspcomn.language.toString());
 //ILIFE-8723 end
}
protected void skipOnFlag1010()
	{
		/*    Skip  this section if  returning from an optional selection*/
		/*    (current stack position action flag = '*').*/
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsaaBatckey.set(wsspcomn.batchkey);
		/*INIT-RIDR-TABLE*/
		/*    Clear the Rider table for numbering.                         */
		for (wsaaRidrSub.set(1); !(isGT(wsaaRidrSub,99)); wsaaRidrSub.add(1)){
			wsaaRidrTab[wsaaRidrSub.toInt()].set(0);
		}
	}

protected void initialiseSubfile1020()
	{
		/*    Clear the Subfile ready for loading.*/
		wsaaSub1.set(1);
		wsaaSub2.set(1);
		wsaaSub4.set(1);
		wsaaSub5.set(1);
		wsaaLineType.set(ZERO);
		wsbbLastCoverage.set(ZERO);
		/* MOVE 'Y'                    TO SCRN-SUBFILE-MORE.            */
		scrnparams.subfileMore.set("N");
		wsaaCoverFlag.set("N");
		wsaaExitFlag.set("N");
		wsaaSelectFlag.set("N");
		wsaaNoSelect = "N";
		wsaaImmexitFlag.set("N");
		wsaaProgramSave.set(SPACES);
		wsaaConcatName.set(SPACES);
		wsaaNextCovno.set("00");
		wsaaNextRidno.set("00");
		wsaaSub1.set(1);
		wsaaSub2.set(1);
		wsaaCcount.set(ZERO);
		wsaaCoverage.set(ZERO);
		wsaaRider.set(ZERO);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaT5673StoredTableInner.wsaaT5673StoredTable.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		lincFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), lincFeature, appVars, "IT");
	}

protected void retreiveHeader1030()
	{
		/*    Read CHDRMJA (retrv) in order to obtain the contract header.*/
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
			&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		sv.chdrnum.set(chdrpf.getChdrnum());  //ILIFE-1403 START by nnazeer
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrpf.getPolinc());   //ILIFE-1403 END
	}

protected void readContractLongdesc1040()
	{
		/*    Read Contract header Long description  from table T5688 for*/
		/*    the contract type held on CHDRLND.*/
		//Ilife-3310 add by liwei
 //ILIFE-8723 start
	//	String longDesc = "";
	/*	if(t5688Map != null){
			if(t5688Map.get(chdrpf.getCnttype()) != null){
				longDesc = t5688Map.get(chdrpf.getCnttype()).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}*/
		String longDesc = "";
		descpf=descDAO.getdescData("IT", "T5688" ,chdrpf.getCnttype(),wsspcomn.company.toString(), wsspcomn.language.toString());/* IJTI-1386 */
		/*if(descpf == null){
			descpf.setLongdesc("??????????????????????????????");
		}*/
		if(descpf!=null) {
			longDesc = descpf.getLongdesc();
		}
		else {
			longDesc = "??????????????????????????????";
		}
		
		sv.chdrnum.set(chdrpf.getChdrnum());  
		sv.cnttype.set(chdrpf.getCnttype());  
		sv.ctypedes.set(longDesc);
	}
 //ILIFE-8723 end 
protected void retreiveUwdate1045()
	{
		/* Read CVUW to get the underwriting decision date.                */
		cvuwpf = cvuwpfDAO.getCvuwpfRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1523 */
		if (cvuwpf == null) {
			sv.huwdcdte.set(varcom.vrcmMaxDate);
		}
		else {
			sv.huwdcdte.set(cvuwpf.getHuwdcdte());
		}
	}

protected void retreiveLife1050()
	{
		/*    Read LIFEMJA for the life assured details.*/
		/*    MOVE RETRV                  TO LIFEMJA-FUNCTION.             */
		lifemjaIO.setFunction(varcom.retrv);
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(chdrpf.getChdrnum()); //ILB-499
		lifeDetails1100();
		wsaaRetrvLife.set(lifemjaIO.getLife());
	}

protected void lifeOrJlife1060()
	{
		/*    LIFEMJA must have the key released in order to read the file*/
		/*    again for the Joint Life details.*/
		/*    MOVE RLSE                   TO LIFEMJA-FUNCTION.             */
		/*    CALL 'LIFEMJAIO' USING LIFEMJA-PARAMS.                       */
		/*    Re-Read of the Life and Client details (LIFEMJA and CLTS).*/
		/*    MOVE WSAA-LIFE-KEY          TO LIFEMJA-DATA-KEY.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		lifeDetails1100();
		/*    Initial Cover/Rider Table.*/
		/*    n.b. Tables can continue indefinitly.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemtabl(t5673);
		itdmIO.setItemitem(chdrpf.getCnttype());
	}

protected void coverRiderTable1070()
	{
		/*    Read Table  which is  dated by  effective date of contract.*/
		itdmIO.setItemcoy(wsspcomn.company);
		/*  MOVE CHDRMJA-CNTTYPE        TO ITDM-ITEMITEM.                */
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		/*    Check for any change in key resulting from Table not found*/
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5673)
		|| (isNE(itdmIO.getItemitem(),chdrpf.getCnttype())
		&& isNE(itdmIO.getItemitem(),t5673rec.gitem))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			/*       MOVE F290                TO SR572-CHDRNUM-ERR*/
			sv.chdrnumErr.set(h999);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1090);
		}
		else {
			t5673rec.t5673Rec.set(itdmIO.getGenarea());
		}
		ctSub.set(0);
		rdSub.set(0);
		sub3.set(0);
		for (sub1.set(1); !(isGT(sub1,8)); sub1.add(1)){
			a1100StoreT5673Table();
		}
		for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
			wsbbClcode[ix.toInt()].set(SPACES);
			wsbbLastRider[ix.toInt()].set(ZERO);
		}
		//Ilife-3310 add by liwei
		readT5679();
	}

protected void readT5679() {
	String keyItemitem = wsaaBatckey.batcBatctrcde.toString();
 //ILIFE-8723 start	
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if(itempf!=null) {
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			sv.chdrnumErr.set(f321);
			wsspcomn.edterror.set("Y");
		}
		
                                                   
	/*if (t5679ListMap.containsKey(keyItemitem)) {
		List<Itempf> itempfList = t5679ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
                                
			Itempf itempf = iterator.next();
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			break;
		}
	}*/
 //ILIFE-8723 end
}
protected void retrieveCurrCoverage1080()
	{
		wsaaNextCovnoR.set(0);
		wsaaNextRidnoR.set(0);
		wsaaPrevCoverage.set(SPACES);
		covrpfList = covrpfDAO.searchCovrmjaByChdrnumCoy(chdrpf.getChdrnum(), chdrpf.getChdrcoy().toString());
		ctIndex.set(1);
		Iterator<Covrpf> icovr = covrpfList.iterator(); 
		boolean breakFlag = false;
		while (isLTE(ctIndex,48) && !breakFlag) {
			breakFlag = a1200LoadSubfileFromCovr(icovr);
		}
		//MIBT-94 STARTS
		//No need to load the coverage as it creates duplicate of the original set
		//Uncommented for Ticket ILIFE-2181 by snayeni STARTS 
		for (ctIndex.set(1); !(isGT(ctIndex,48)
		|| isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], SPACES)); ctIndex.add(1)){
			a1500LoadSubfileFromT5673();
		}
		//Uncommented for Ticket ILIFE-2181 by snayeni END
		//MIBT-94 ENDS
		/*    IF COVRMJA-STATUZ           = O-K*/
		/*       MOVE 2                   TO WSAA-LINE-TYPE*/
		/*    ELSE*/
		/*       MOVE 1                   TO WSAA-LINE-TYPE.*/
		/*    LOAD the Subfile Screen until the end of the Subfile has*/
		/*    been reached.*/
		/*    PERFORM 1200-LOAD-SUBFILE UNTIL SCRN-SUBFILE-MORE = 'N'.*/
		updateScreenIndic1900();
	}

//ILIFE-1403 START by nnazeer
protected void contractTypeStatus1070()
{
	//Ilife-3310 add by liwei
 //ILIFE-8723 start	
	descpf=descDAO.getdescData("IT", "T5688" ,chdrpf.getCnttype(),wsspcomn.company.toString(), wsspcomn.language.toString());
	if (descpf == null) {
		sv.ctypedes.fill("?");
	}
	else {
		sv.ctypedes.set(descpf.getLongdesc());
	}
	
	/*if(t5688Map != null){
		if(t5688Map.get(chdrpf.getCnttype()) != null){
			sv.ctypedes.set(t5688Map.get(chdrpf.getCnttype()).getLongdesc());
		}else{
			sv.ctypedes.fill("?");
		}
	}*/
	
	descpf=descDAO.getdescData("IT", "T3623", chdrpf.getStatcode(),wsspcomn.company.toString(), wsspcomn.language.toString());
	if (descpf == null) {
		sv.chdrstatus.fill("?");
	}
	else {
		sv.chdrstatus.set(descpf.getLongdesc());
	}
	
	/*if(t3623Map != null){
		if(t3623Map.get(chdrpf.getStatcode()) != null){
			sv.chdrstatus.set(t3623Map.get(chdrpf.getStatcode()).getShortdesc());
		}else{
			sv.chdrstatus.fill("?");
		}
	}*/
	
	descpf=descDAO.getdescData("IT", "T3588", chdrpf.getPstcde(),wsspcomn.company.toString(), wsspcomn.language.toString());
	if (descpf == null) {
		sv.premstatus.fill("?");
	}
	else {
		sv.premstatus.set(descpf.getLongdesc());
	}
	
	
	/*if(t3588Map != null){
		if(t3588Map.get(chdrpf.getPstcde()) != null){
			sv.premstatus.set(t3588Map.get(chdrpf.getPstcde()).getShortdesc());
		}else{
			sv.premstatus.fill("?");
		}
	}*/
 //ILIFE-8723 end
}
//ILIFE-1403 END

protected void lifeDetails1100()
	{
		try {
			readLife1110();
			retreiveClientDetails1120();
			readLifeJlife1140();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*    The following will read the life file and format the client
	*    name for processing.
	* </pre>
	*/
protected void readLife1110()
	{
		/*    Read the life file for either the Life or Joint Life.*/
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)
		&& isNE(lifemjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1190);
		}
		if (isNE(lifemjaIO.getChdrcoy(),wsspcomn.company)
		|| isNE(lifemjaIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(lifemjaIO.getStatuz(),varcom.endp)) {
			lifemjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
	}

protected void retreiveClientDetails1120()
	{
		/*    Read the Client details for the associated Life.*/
		clntpf = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(),lifemjaIO.getLifcnum().toString());
		/*CLIENT-NAME-FORMAT*/
		/*    Format the Client name extracted. Special Format.*/
		/*    SEE Copy Confname in the procedure division.*/
		plainname();
	}

protected void readLifeJlife1140()
	{
		/*    IF entering on a First Life*/
		/*    THEN extract the Joint Life details if they exist*/
		/*    ELSE If entering on a Joint Life*/
		/*         THEN extract the First Life details.*/
		if (isEQ(lifemjaIO.getJlife(),"00")
		|| isEQ(lifemjaIO.getJlife(), "  ")) {
			lifeToScreen1150();
		}
		else {
			jlifeToScreen1160();
		}
		goTo(GotoLabel.exit1190);
	}

protected void lifeToScreen1150()
	{
		/*    Move Life details to screen and set up key for Joint Life*/
		/*    Search.*/
		sv.life.set(lifemjaIO.getLife());
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		sv.linsname.set(wsspcomn.longconfname);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.jlife.set(SPACES);
		//TSD-266 Starts
		wsaaLDob.set(lifemjaIO.getCltdob());
		wsaaLSex.set(lifemjaIO.getCltsex());
		wsaaCDob.set(clntpf.getCltdob());
		wsaaCSex.set(clntpf.getCltsex());
		//TSD-266 Ends
	}

protected void jlifeToScreen1160()
	{
		/*    Move Joint Life details to screen.*/
		//TSD-266 Starts
		wsaaJlDob.set(lifemjaIO.getCltdob());
		wsaaJlSex.set(lifemjaIO.getCltsex());
		wsaaJcDob.set(clntpf.getCltdob());
		wsaaJcSex.set(clntpf.getCltsex());
		//TSD-266 Ends
		sv.jlife.set(lifemjaIO.getJlife());
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		sv.jlinsname.set(wsspcomn.longconfname);
		sv.selectOut[varcom.pr.toInt()].set("Y");
	}

protected void a1100StoreT5673Table()
	{
               
		if (isNE(t5673rec.ctable[sub1.toInt()],SPACES)) {
			ctSub.add(1);
			rdSub.set(1);
			wsaaT5673StoredTableInner.wsaaZrlifind[ctSub.toInt()].set(t5673rec.zrlifind[sub1.toInt()]);
			wsaaT5673StoredTableInner.wsaaCreq[ctSub.toInt()].set(t5673rec.creq[sub1.toInt()]);
			wsaaT5673StoredTableInner.wsaaCtable[ctSub.toInt()].set(t5673rec.ctable[sub1.toInt()]);
			/*      MOVE T5673-CTMAXCOV (SUB1) TO WSAA-CTMAXCOV (CT-SUB).   */
			wsaaT5673StoredTableInner.wsaaCtmaxcov[ctSub.toInt()].set(t5673rec.ctmaxcov[sub1.toInt()]);
			wsaaT5673StoredTableInner.wsaaNoOfCovs[ctSub.toInt()].set(ZERO);
		}
		for (sub2.set(1); !(isGT(sub2,6)); sub2.add(1)){
			sub3.add(1);
			if (isEQ(t5673rec.rtable[sub3.toInt()],SPACES)) {
				continue;
			}
			wsaaT5673StoredTableInner.wsaaRreq[ctSub.toInt()][rdSub.toInt()].set(t5673rec.rreq[sub3.toInt()]);
			wsaaT5673StoredTableInner.wsaaRtable[ctSub.toInt()][rdSub.toInt()].set(t5673rec.rtable[sub3.toInt()]);
			rdSub.add(1);
		}
		/*  We must check if there is a continuation item but we do        */
		/*  this once the last coverage on the XDS and its riders          */
		/*  have been stored.  Once continuation item is detected,         */
		/*  the subscripts to read the XDS coverages (SUB1) and            */
		/*  riders (SUB3) must be reset.                                   */
		if (isEQ(sub1,8)) {
			if (isNE(t5673rec.gitem,SPACES)) {
				itdmIO.setStatuz(varcom.oK);
				itdmIO.setItemitem(t5673rec.gitem);
				itdmIO.setFunction("BEGN");
				SmartFileCode.execute(appVars, itdmIO);
				if (isNE(itdmIO.getStatuz(),varcom.oK)
				&& isNE(itdmIO.getStatuz(),varcom.endp)) {
					syserrrec.statuz.set(itdmIO.getStatuz());
					fatalError600();
				}
				if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
				|| isNE(itdmIO.getItemtabl(),t5673)
				|| isNE(itdmIO.getItemitem(),t5673rec.gitem)
				|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
					sv.chdrnumErr.set(h999);
					wsspcomn.edterror.set("Y");
				}
				t5673rec.t5673Rec.set(itdmIO.getGenarea());
				sub1.set(0);
				sub3.set(0);
			}
		}
  

                                     
  
                 
              
                                                   
           
   
                                                                                                    
                                                                                                        
               
                
	}

/**
* <pre>
*    Creation of Screen Lines from existing covr and riders
*    from t5673 table.
* </pre>
*/

protected boolean a1200LoadSubfileFromCovr(Iterator<Covrpf> icovrmjaIO)
	{
                      
		/*  The screen will now be loaded at change of coverage            */
		/*  rather than as the COVRS are being read. We must not           */
		/*  write anything to the screen on the first time in.             */
		boolean breakFlag = false;
		//Covrpf covrmjaIO = null;
		if (!icovrmjaIO.hasNext()){
			breakFlag = true;
		}else{
			covrpf = icovrmjaIO.next();
		}
		if(breakFlag || (isNE(covrpf.getCoverage(),wsaaPrevCoverage)
				&& isNE(wsaaPrevCoverage, "  ")) ){
			for (ctIndex.set(1); !(isGT(ctIndex,48)
			|| isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], SPACES)); ctIndex.add(1)){
				if(isEQ(covrpf.getLife(),wsaaRetrvLife))
					a1300CoverToScreen(covrpf);
			}
			if (breakFlag) {
				/*     GO TO A1290-EXIT.                                        */
				/*          MOVE +49        TO CT-INDEX                          */
				ctIndex.set(49);
				return true;
			}
		}
		//lastCovrmja = covrmjaIO;
		/*    IF COVRMJA-RIDER         > WSAA-NEXT-RIDNO                   */
		/*          MOVE COVRMJA-RIDER    TO WSAA-NEXT-RIDNO.              */
		if (isEQ(covrpf.getRider(),"00")) {
			wsaaLastCovrmjaCrtable.set(covrpf.getCrtable());
			if (isGT(covrpf.getCoverage(),wsbbLastCoverage)) {
				wsbbLastCoverage.set(covrpf.getCoverage());
			}
			for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
				if (isEQ(wsbbClcode[ix.toInt()],SPACES)) {
					wsbbClcode[ix.toInt()].set(covrpf.getCrtable());
					ix.set(49);
				}
			}
		}
		else {
			for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
				if (isEQ(wsbbClcode[ix.toInt()],wsaaLastCovrmjaCrtable)) {
					if (isGT(covrpf.getRider(),wsbbLastRider[ix.toInt()])) {
						wsbbLastRider[ix.toInt()].set(covrpf.getRider());
					}
					ix.set(49);
				}
			}
		}
		wsaaRidrSub.set(covrpf.getCoverage());
		if (isGT(covrpf.getRider(),wsaaRidrTab[wsaaRidrSub.toInt()])) {
			wsaaRidrTab[wsaaRidrSub.toInt()].set(covrpf.getRider());
		}
		if (isGT(covrpf.getCoverage(),wsaaNextCovno) && isEQ(covrpf.getLife(),wsaaRetrvLife)) {
			wsaaNextCovno.set(covrpf.getCoverage());
		}
		/* IF  COVRMJA-RIDER NOT = '00'                                 */
		/*     GO TO A1290-EXIT.                                        */
		/*   If the coverage read is the same as the previous we           */
		/*   have a rider.  Mark off the rider in the array as             */
		/*   being on the contract and exit.  Otherwise we have            */
		/*   another coverage which should be searched and                 */
		/*   'marked off'.                                                 */
		if (isEQ(covrpf.getCoverage(),wsaaPrevCoverage)) {
			for (rdIndex.set(1); !(isGT(rdIndex,48)
			|| isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], SPACES)); rdIndex.add(1)){
				if (isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], covrpf.getCrtable())) {
					wsaaT5673StoredTableInner.wsaaRtableUsed[ctIndex.toInt()][rdIndex.toInt()].set("Y");
					wsaaT5673StoredTableInner.wsaaRtableRisk[ctIndex.toInt()][rdIndex.toInt()].set(covrpf.getStatcode());
					/*            MOVE 49 TO RD-INDEX                        <S19FIX>*/
					rdIndex.set(49);
				}
			}
		}
		else {
			ctIndex.set(1);
			 searchlabel1:
			{
				for (; isLT(ctIndex, wsaaT5673StoredTableInner.wsaaT5673Info.length); ctIndex.add(1)){
					if (isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], covrpf.getCrtable())) {
						wsaaT5673StoredTableInner.wsaaCtableUsed[ctIndex.toInt()].set("Y");
						break searchlabel1;
					}
				}
				sv.crcodeErr.set(h249);
				return false;
			}
		}
		/*     GO TO A1290-EXIT.                                        */
		wsaaPrevCoverage.set(covrpf.getCoverage());
		return false;
		}

protected void a1300CoverToScreen(Covrpf covrmjaIO)
	{
		/*    Reference Cover Long description  and output details to the*/
		/*    Screen.*/
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.indic.set(SPACES);
		sv.rreq.set(SPACES);
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcodeOut[varcom.nd.toInt()].set(SPACES);
		/*   If the coverage appears on the contract already, we           */
		/*   display it and keep a coverage count to compare               */
		/*   against the maximum later.<                                   */
		if (isEQ(wsaaT5673StoredTableInner.wsaaCtableUsed[ctIndex.toInt()], "Y")) {
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaT5673StoredTableInner.wsaaNoOfCovs[ctIndex.toInt()].add(1);
			for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
				if (isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], wsaaSelectedCover[ix.toInt()])) {
					ix.set(49);
				}
				else {
					if (isEQ(wsaaSelectedCover[ix.toInt()],SPACES)) {
						wsaaSelectedCover[ix.toInt()].set(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]);
						ix.set(49);
					}
				}
			}
		}
		else {
			return ;
		}
	//Ilife-3310 add by liwei
 //ILIFE-8723 start
		String longDesc = "";
		
		descpf=descDAO.getdescData("IT", "T5687", wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()].toString(), wsspcomn.company.toString(),  wsspcomn.language.toString());
		/*if (descpf == null) {
		descpf.setLongdesc("??????????????????????????????");
		}*/
		if(descpf!=null) {
			longDesc = descpf.getLongdesc();
		}
		else {
			longDesc = "??????????????????????????????";
		}
		/*if(t5687Map != null){
			if(t5687Map.get(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]) != null){
				longDesc = t5687Map.get(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}*/
		sv.crcode.set(covrmjaIO.getCrtable());
		sv.crcode.set(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]);
		sv.longdesc.set(longDesc);
 //ILIFE-8723 end 
		sv.coverage.set(covrmjaIO.getCoverage());
		sv.rider.set(covrmjaIO.getRider());
		if(uwFlag) {
			sv.mainCovrDisFlag = checkComponent(sv.crcode.toString());
			if(sv.mainCovrDisFlag == 1) {
				sv.mainCovrDisPos = sv.getSubfileRrn().toInt() + 1;
			}
		}
		/*    ADD a line to the Screen and increment Line count.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		for (rdIndex.set(1); !(isGT(rdIndex,48)
		|| isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], SPACES)); rdIndex.add(1)){
			a1400AsocRiderToScreen(covrmjaIO);
		}
		wsaaT5673StoredTableInner.wsaaCtableUsed[ctIndex.toInt()].set(SPACES);
	}

protected void a1400AsocRiderToScreen(Covrpf covrmjaIO)
	{
		sv.rtable.set(SPACES);
		/*    Validate Rider for being Mandatory.*/
		/*    If  Mandatory,  Protect  and  set  Hidden  field  for  4000*/
		/*       Processing.*/
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.indic.set(SPACES);
		sv.rreq.set(SPACES);
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcode.set(covrmjaIO.getCoverage());
		sv.crcode.set(wsaaPrevCoverage);
		sv.crcodeOut[varcom.nd.toInt()].set("Y");
		sv.rtable.set(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]);
		if (isEQ(wsaaT5673StoredTableInner.wsaaRtableUsed[ctIndex.toInt()][rdIndex.toInt()], "Y")) {
			/*    If Rider risk status is invalid and SR572-SELECT is '+', set */
			/*    SR572-SELECT back to ' ', this change is to enable client to */
			/*    purchase that rider again.                                   */
			wsaaSub3.set(1);
			while ( !((isGT(wsaaSub3,12)
			|| (isEQ(t5679rec.ridRiskStat[wsaaSub3.toInt()], wsaaT5673StoredTableInner.wsaaRtableRisk[ctIndex.toInt()][rdIndex.toInt()]))))) {
				wsaaSub3.add(1);
			}
	
			if (isGT(wsaaSub3,12)) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.select.set("+");
				sv.selectOut[varcom.pr.toInt()].set("Y");
				wsaaT5673StoredTableInner.wsaaRtableUsed[ctIndex.toInt()][rdIndex.toInt()].set(SPACES);
				for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
					if (isEQ(wsbbClcode[ix.toInt()],wsaaLastCovrmjaCrtable)) {
						if (isGT(covrmjaIO.getRider(),wsbbLastRider[ix.toInt()])) {
							wsbbLastRider[ix.toInt()].set(covrmjaIO.getRider());
						}
						ix.set(49);
					}
				}
			}
		}
		/*    Reference Rider Long description  and output details to the*/
		/*    Screen.*/
	//Ilife-3310 add by liwei
 //ILIFE-8723 start		
		String longDesc = "";
		descpf=descDAO.getdescData("IT", "T5687", wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()].toString(), wsspcomn.company.toString(),  wsspcomn.language.toString());
		/*if (descpf == null) {
			descpf.setLongdesc("??????????????????????????????");
		}*/
		if(descpf!=null) {
			longDesc = descpf.getLongdesc();
		}
		else {
			longDesc = "??????????????????????????????";
		}
		
	/*	String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]) != null){
				longDesc = t5687Map.get(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}*/
		sv.longdesc.set(longDesc);
		if(uwFlag) {
			if(sv.mainCovrDisFlag == 2) {
				sv.select.set("+");
				sv.selectOut[varcom.pr.toInt()].set("Y");
			}else {
				checkComponent(sv.rtable.toString());// sv.uwResultMap
			}
		}
 //ILIFE-8723 end
		/*    ADD a line to the Screen and increment Line count.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void a1500LoadSubfileFromT5673()
	{
		/*A1510-START*/
		if (isEQ(wsaaT5673StoredTableInner.wsaaZrlifind[ctIndex.toInt()], "P")
		&& isNE(wsaaRetrvLife,"01") && isNE(wsaaRetrvLife,"02")) {
			return ;
		}
		if (isEQ(wsaaT5673StoredTableInner.wsaaZrlifind[ctIndex.toInt()], "S")
		&& isEQ(wsaaRetrvLife,"01")) {
			return ;
		}
		if (isEQ(wsaaT5673StoredTableInner.wsaaZrlifind[ctIndex.toInt()], "P")
		&& isEQ(wsaaRetrvLife,"02")) {
			return ;
		}
		/* Skip display of coverage which already on cover.                */
		/*for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
			if (isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], wsbbClcode[ix.toInt()])
			&& isLT(wsaaT5673StoredTableInner.wsaaCtmaxcov[ctIndex.toInt()], 100)) {
				return ;
			}
		}*/
		if(wsbbLastCoverage.equals(wsaaT5673StoredTableInner.wsaaCtmaxcov[ctIndex.toInt()])){ //ILIFE-7821
			return;
		}
		a1600TableCoverToScreen();
		for (rdIndex.set(1); !(isGT(rdIndex,48)
		|| isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], SPACES)); rdIndex.add(1)){
			a1700AsocRiderToScreen();
		}
		/*A1590-EXIT*/
	}

protected void a1600TableCoverToScreen()
	{
		/*    Reference Cover Long description  and output details to the*/
		/*    Screen.*/
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.indic.set(SPACES);
		sv.rreq.set(SPACES);
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcodeOut[varcom.nd.toInt()].set(SPACES);
		//Ilife-3310 add by liwei
	 //ILIFE-8723 start	
		String longDesc = "";
		descpf=descDAO.getdescData("IT", "T5687", wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()].toString(), wsspcomn.company.toString(),  wsspcomn.language.toString());
		/*if (descpf == null) {
			descpf.setLongdesc("??????????????????????????????");
		}*/
		if(descpf!=null) {
			longDesc = descpf.getLongdesc();
		}
		else {
			longDesc = "??????????????????????????????";
		}
		
		/*String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]) != null){
				longDesc = t5687Map.get(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}*/
		sv.crcode.set(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]);
		sv.rtable.set(SPACES);
		sv.longdesc.set(longDesc);
 //ILIFE-8723 end
		sv.select.set(SPACES);
		for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
			if (isEQ(wsbbClcode[ix.toInt()], wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()])) {
				ix.set(49);
			}
			else {
				if (isEQ(wsbbClcode[ix.toInt()],SPACES)) {
					wsbbClcode[ix.toInt()].set(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()]);
					ix.set(49);
				}
			}
		}
		if(uwFlag) {
			sv.tableCovrDisFlag = checkComponent(sv.crcode.toString());
			if(sv.tableCovrDisFlag == 1) {
				sv.tableCovrDisPos = sv.getSubfileRrn().toInt() + 1;
			}
		}
		/*    MOVE 'Y'                 TO SR572-SELECT-OUT(PR).*/
		/*    ADD a line to the Screen and increment Line count.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void a1700AsocRiderToScreen()
	{
               
  

                           
  
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.indic.set(SPACES);
		sv.rreq.set(SPACES);
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.crcodeOut[varcom.nd.toInt()].set(SPACES);
		sv.rtable.set(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]);
		sv.rreq.set(wsaaT5673StoredTableInner.wsaaRreq[ctIndex.toInt()][rdIndex.toInt()]);
		/*    Reference Rider Long description  and output details to the*/
		/*    Screen.*/
		//Ilife-3310 add by liwei
 //ILIFE-8723 start		
		String longDesc = "";
		descpf=descDAO.getdescData("IT", "T5687", wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()].toString(), wsspcomn.company.toString(),  wsspcomn.language.toString());
		/*if (descpf == null) {
			descpf.setLongdesc("??????????????????????????????");
		}*/
		if(descpf!=null) {
			longDesc = descpf.getLongdesc();
		}
		else {
			longDesc = "??????????????????????????????";
		}
		
		/*String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]) != null){
				longDesc = t5687Map.get(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}*/
		sv.longdesc.set(longDesc);
		if(uwFlag) {
			if(sv.tableCovrDisFlag == 2) {
				sv.select.set("+");
				sv.selectOut[varcom.pr.toInt()].set("Y");
			}else {
				checkComponent(sv.rtable.toString());// sv.uwResultMap
			}
		}
 //ILIFE-8723 end
		/*    ADD a line to the Screen and increment Line count.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case subfileLoad1210:
					subfileLoad1210();
				case exit1490:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    Creation of Screen Lines for Selection or Viewing.
	* </pre>
	*/
protected void subfileLoad1210()
	{
		/*    Check to determine if  this program  is being entered on a*/
		/*    LIFE.*/
		/*           Display : ALL Coverages and Associated Riders.*/
		/*    Check to determine if  this program  is being  entered on a*/
		/*    COVERAGE.*/
		/*           Display : Coverage and Associated Riders ONLY.*/
		//if (isEQ(covrmjaIO.getStatuz(),varcom.oK)) 
		if(covrpf != null){
			coverageTrans1410();
		}
		else {
			lifeTrans1310();
		}
		/*    Check  to  determine  if an  additional  Continuation Table*/
		/*    should be loaded. e.g. T5673-GITEM = 'T5673A'.*/
		if (isGT(wsaaSub1,8)
		&& isGT(wsaaSub2,48)) {
			if (isNE(t5673rec.gitem,SPACES)) {
				/*          MOVE T5673-GITEM      TO ITDM-ITEMTABL*/
				itdmIO.setItemitem(t5673rec.gitem);
				wsaaSub1.set(1);
				wsaaSub2.set(1);
				scrnparams.subfileMore.set("Y");
				coverRiderTable1070();
				if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
					goTo(GotoLabel.subfileLoad1210);
				}
				else {
					scrnparams.subfileMore.set("N");
				}
			}
			else {
				scrnparams.subfileMore.set("N");
			}
		}
		goTo(GotoLabel.exit1490);
	}

protected void lifeTrans1310()
	{
		/*    Entry on LIFE details: additional Continuation Table should*/
		/*    be loaded. i.e. T5673-GITEM = 'T5673A'.*/
		/*    IF processing a Cover*/
		/*    ELSE processing a Rider.*/
		/*    WSAA-LINE-TYPE : = 1 - Life processing a Cover.*/
		/*                     = 2 - Coverage processing that Cover.*/
		/*                     = 3 - Processing the Associate Riders.*/
		if (wsaaLcover.isTrue()) {
			checkCover1330();
			wsaaLineType.set("3");
		}
		else {
			for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
				riderFile1370();
			}
			wsaaLineType.set("1");
		}
		/*    The following check identifies the end of the subfile before*/
		/*    the end of table T5673 (Cover/Rider) is reached.*/
		if (isLTE(wsaaSub1,8)
		&& isLTE(wsaaSub2,48)) {
			endSubfile1320();
		}
	}

protected void endSubfile1320()
	{
		/*    If BOTH Cover / Rider Field within table  = spaces then set*/
		/*    end of table flag on.*/
		/*    If it is the end of the Subfile then set the Screen flag to*/
		/*    no more pages.*/
		if (isEQ(t5673rec.ctable[wsaaSub1.toInt()],SPACES)
		&& isEQ(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
	}

//ILIFE-3310 by liwei
protected void checkCover1330(){
	/*    Read Logical View for a match of Cover types.*/
	/*    Validate Cover type.*/
	/*    Move Cover details to Screen.*/
	wsaaExitFlag.set("N");
	wsaaNoSelect = "N";
	if (isNE(t5673rec.ctable[wsaaSub1.toInt()],SPACES)) {
		wsaaCcount.set(ZERO);
		//ILIFE-3310 by liwei
		covrpfList = covrpfDAO.getCovrcovData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), lifemjaIO.getLife().toString(), t5673rec.ctable[wsaaSub1.toInt()].toString());
		if(covrpfList != null && covrpfList.size() >0){
			for(int i=0; i < covrpfList.size() || !(wsaaExit.isTrue()); i++ ){
				if(covrpfList.get(i) == null){
					fatalError600();
				}
				if(i == covrpfList.size()){
					checkCovtcov1360();
				} else {
					if (isEQ(covrpfList.get(i).getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
						wsaaCcount.add(1);
					}
				}
				if (isGT(covrpfList.get(i).getCoverage(),wsaaNextCovno)) {
					wsaaNextCovno.set(covrpfList.get(i).getCoverage());
				}
			}
		}
		coverValidation1350();
		coverToScreen1600();
	}
	wsaaSub1.add(1);
}

protected void coverValidation1350()
	{
		/*    Validate Cover:*/
		/*       IF a match exists and  this can be  added to Place a '+'*/
		/*          in select. (Must contain an 'X' to be processed.*/
		/*       IF a Required Type then Protect with an 'X' as a Protect*/
		/*          Selection.*/
		/*       IF a Type has reached  its Maximum  Copies  then Protect*/
		/*          the Select Space. No Selection allowed.*/
		if (isGT(wsaaCcount,ZERO)) {
			sv.select.set("+");
		}
		if (isEQ(t5673rec.ctmaxcov[wsaaSub1.toInt()],wsaaCcount)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaNoSelect = "Y";
		}
		if (isEQ(t5673rec.creq[wsaaSub1.toInt()],"R")
		&& isLT(wsaaCcount,t5673rec.ctmaxcov[wsaaSub1.toInt()])
		&& isNE(sv.select,"+")) {
			sv.select.set("X");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.hrequired.set("Y");
		}
	}

protected void checkCovtcov1360()
	{
		covtcovList = covtpfDAO.getCovtcovData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(),
				lifemjaIO.getLife().toString(), t5673rec.ctable[wsaaSub1.toInt()].toString());/* IJTI-1523 */
		if(covtcovList != null && covtcovList.size() > 0){
			for(int i=0; i < covtcovList.size() || !(wsaaExit.isTrue()); i++ ){
				if(covtcovList.get(i) == null && i != covtcovList.size()){
					fatalError600();
				}
				if(i == covtcovList.size()){
					wsaaExitFlag.set("Y");
				}else{
					if (isEQ(covtcovList.get(i).getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
						wsaaCcount.add(1);
					}
				}
				if (isGT(covtcovList.get(i).getCoverage(),wsaaNextCovno)) {
					wsaaNextCovno.set(covtcovList.get(i).getCoverage());
				}
			}
		}
	}

protected void riderFile1370()
	{
		/*    Process the Table 6 times to find a RIDER.*/
		/*    For each Cover  there is a  Possible SIX Riders  within the*/
		/*    Table. However, If the following Cover is Blank, the Riders*/
		/*    are being continued on to the next 6 and so on....*/
		/*    Move Rider Details to screen.*/
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.indic.set(SPACES);
		sv.rreq.set(SPACES);
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		if (isNE(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
			riderToScreen1700();
		}
		wsaaSub2.add(1);
	}

protected void coverageTrans1410()
	{
		/*    IF entering on a Coverage, Find and move to Screen*/
		/*       MOVE 3 to a flag to Process  the Riders  associated with*/
		/*       the previous Cover.*/
		if (wsaaCcover.isTrue()) {
			for (wsaaSub1.set(1); !(wsaaExit.isTrue()); wsaaSub1.add(1)){
				findCover1420();
			}
		}
		else {
			riderInfo1430();
		}
	}

protected void findCover1420()
	{
		/*    Match the entry Coverage with a Table entry.*/
		/*    Move Cover to Screen.*/
		/*    6 is added to  the Rider  subscript  in  order to  find the*/
		/*    riders associated to the found Cover.*/
		wsaaCoverageR.set(covrpf.getCoverage());
		wsaaNextRidno.set(ZERO);
		if (isEQ(wsaaNoSelect,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(covrpf.getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
			sv.select.set("+");
			wsaaExitFlag.set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			coverToScreen1600();
			wsaaLineType.set("3");
		}
		else {
			wsaaSub2.add(6);
		}
		/*    The cover may exist on a continuation table. Therefore the*/
		/*    flag is set to exit to - 1210-load-subfile where the table*/
		/*    is loaded.*/
		/*    Note: WSAA-LINE-TYPE is still at 2 for Cover search.*/
		if (isGT(wsaaSub1,8)) {
			wsaaExitFlag.set("Y");
		}
	}

protected void riderInfo1430()
	{
		/*    Screen fields are initialised before check for Rider.*/
		sv.crcode.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.indic.set(SPACES);
		sv.rreq.set(SPACES);
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		/*    Read the table for The associated Rider.*/
		/*    Only 1 rider of any type can be entered.*/
		/*    IF end of riders, set flag to process next cover.*/
		/*    Move Rider Details to screen.*/
		if (isEQ(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
			scrnparams.subfileMore.set("N");
			wsaaLineType.set("2");
		}
		else {
			scrnparams.subfileMore.set("Y");
			riderCheck1440();
			riderToScreen1700();
		}
		wsaaSub2.add(1);
	}

protected void riderCheck1440()
	{
		/*    Check the Rider Logical View File for LIKE Riders.*/
		//ILIFE-3310 by liwei
		covrpf = covrpfDAO.getCovrridData(wsspcomn.company.toString(), chdrpf.getChdrnum(), lifemjaIO.getLife().toString(), 
				covrpf.getCoverage(),t5673rec.rtable[wsaaSub2.toInt()].toString());/* IJTI-1523 */
		/*    If Rider Exists move a '+'  to the select field and Protect*/
		/*    it. NO Selection can be made.*/
		/*    Only 1 rider of any type can be entered.*/
		/*    Store the highest Rider number read.*/
		if (covrpf != null) {
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			/*    IF COVRRID-RIDER         > WSAA-NEXT-RIDNO*/
			/*       MOVE COVRRID-RIDER    TO WSAA-NEXT-RIDNO*/
			wsaaRidrSub.set(covrpf.getCoverage());
			if (isGT(covrpf.getRider(),wsaaRidrTab[wsaaRidrSub.toInt()])) {
				wsaaRidrTab[wsaaRidrSub.toInt()].set(covrpf.getRider());
			}
		}
		else {
			covtRiderCheck1450();
		}
	}

protected void covtRiderCheck1450()
	{
		/*    Check the Rider Logical View File for LIKE Riders.*/
		covtrid = covtpfDAO.getCovtridData(wsspcomn.company.toString(), chdrpf.getChdrnum(), lifemjaIO.getLife().toString(), 
				covrpf.getCoverage(),t5673rec.rtable[wsaaSub2.toInt()].toString());/* IJTI-1523 */
		/*    If Rider Exists move a '+'  to the select field and Protect*/
		/*    it. NO Selection can be made.*/
		/*    Only 1 rider of any type can be entered.*/
		/*    Store the highest Rider number read.*/
		if (covtrid != null) {
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaRidrSub.set(covtrid.getCoverage());
			if (isGT(covtrid.getRider(),wsaaRidrTab[wsaaRidrSub.toInt()])) {
				wsaaRidrTab[wsaaRidrSub.toInt()].set(covtrid.getRider());
			}
		}
	}

protected void coverToScreen1600()
	{
		coverToLine1610();
	}

protected void coverToLine1610()
	{
		/*    Reference Cover Long description  and output details to the*/
		/*    Screen.*/
		//Ilife-3310 add by liwei
 //ILIFE-8723 start	
		String longDesc = "";
		descpf=descDAO.getdescData("IT", "T5687", t5673rec.ctable[wsaaSub1.toInt()].toString(), wsspcomn.company.toString(),  wsspcomn.language.toString());
		/*if (descpf == null) {
			descpf.setLongdesc("??????????????????????????????");
		}*/
		if(descpf!=null) {
			longDesc = descpf.getLongdesc();
		}
		else {
			longDesc = "??????????????????????????????";
		}
		/*String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(t5673rec.ctable[wsaaSub1.toInt()]) != null){
				longDesc = t5687Map.get(t5673rec.ctable[wsaaSub1.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}*/
		sv.crcode.set(t5673rec.ctable[wsaaSub1.toInt()]);
		sv.longdesc.set(longDesc);
 //ILIFE-8723 end
		/*    ADD a line to the Screen and increment Line count.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void riderToScreen1700()
	{
		riderToLine1710();
	}

protected void riderToLine1710()
	{
		sv.rtable.set(SPACES);
		/*    Validate Rider for being Mandatory.*/
		/*    If  Mandatory,  Protect  and  set  Hidden  field  for  4000*/
		/*       Processing.*/
		if (isEQ(t5673rec.rreq[wsaaSub2.toInt()],"R")) {
			sv.select.set("X");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.hrequired.set("Y");
		}
		sv.crcode.set(SPACES);
		sv.rtable.set(t5673rec.rtable[wsaaSub2.toInt()]);
		/*    Reference Rider Long description  and output details to the*/
		/*    Screen.*/
		
		//Ilife-3310 add by liwei
 //ILIFE-8723 start		
		String longDesc = "";
		descpf=descDAO.getdescData("IT", "T5687", t5673rec.rtable[wsaaSub2.toInt()].toString(), wsspcomn.company.toString(),  wsspcomn.language.toString());
		/*if (descpf == null) {
			descpf.setLongdesc("??????????????????????????????");
		}*/
		if(descpf!=null) {
			longDesc = descpf.getLongdesc();
		}
		else {
			longDesc = "??????????????????????????????";
		}
		
		
		/*String longDesc = "";
		if(t5687Map != null){
			if(t5687Map.get(t5673rec.rtable[wsaaSub2.toInt()]) != null){
				longDesc = t5687Map.get(t5673rec.rtable[wsaaSub2.toInt()]).getLongdesc();
			}else{
				longDesc = "??????????????????????????????";
			}
		}*/
		sv.longdesc.set(longDesc);
 //ILIFE-8723 end
		/*    ADD a line to the Screen and increment Line count.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void updateScreenIndic1900()
	{

		wsaaSelectedCoverTable.set(SPACES);
		scrnparams.function.set(varcom.sstrt);
		scrnparams.statuz.set(varcom.oK);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			obtainIndic1910();
		}

		scrnparams.statuz.set(varcom.oK);
		/*EXIT*/
	}

protected void obtainIndic1910()
	{
  		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(scrnparams.statuz,varcom.oK)) {
			return ;
		}
		if (isNE(sv.crcode,SPACES)
		&& isEQ(sv.rtable,SPACES)) {
			wsaaClcode.set(sv.crcode);
		}
		sv.clcode.set(wsaaClcode);
		findIndic1920();
		if (indicFound.isTrue()
		|| isEQ(sv.select,"+")) {
			for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
				if (isEQ(sv.clcode,wsaaSelectedCover[ix.toInt()])) {
					ix.set(49);
				}
				else {
					if (isEQ(wsaaSelectedCover[ix.toInt()],SPACES)) {
						wsaaSelectedCover[ix.toInt()].set(sv.clcode);
						ix.set(49);
					}
				}
			}
		}
		if (indicFound.isTrue()) {
			sv.indic.set("*");
			sv.coverage.set(covtpf.getCoverage());
			sv.rider.set(covtpf.getRider());
			if (isGT(covtpf.getCoverage(),wsbbLastCoverage)) {
				wsbbLastCoverage.set(covtpf.getCoverage());
			}
			for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
				if (isEQ(sv.clcode,wsbbClcode[ix.toInt()])) {
					if (isGT(covtpf.getRider(),wsbbLastRider[ix.toInt()])) {
						wsbbLastRider[ix.toInt()].set(covtpf.getRider());
					}
					ix.set(49);
				}
			}
		}
		if (isEQ(sv.select,"+")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set(SPACES);
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
  

                              
  
           
		}

protected void findIndic1920()
	{
		wsaaIndicStatus.set("N");
		if (isEQ(sv.select,SPACES)) {
			findFromCovt1930();
		}
		if (isEQ(wsaaIndicStatus,"N")) {
			return ;
		}
		if (isNE(sv.clcode,sv.crcode)) {
			for (ix.set(1); !(isGT(ix,48)); ix.add(1)){
				if (isEQ(wsaaSelectedCover[ix.toInt()],SPACES)) {
					wsaaIndicStatus.set("N");
					return ;
				}
				else {
					if (isEQ(wsaaSelectedCover[ix.toInt()],sv.clcode)) {
						ix.set(49);
					}
				}
			}
		}
		for (ctIndex.set(1); !(isGT(ctIndex,48)); ctIndex.add(1)){
			if (isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], sv.clcode)) {
				for (rdIndex.set(1); !(isGT(rdIndex,48)); rdIndex.add(1)){
					if (isEQ(wsaaT5673StoredTableInner.wsaaRtable[ctIndex.toInt()][rdIndex.toInt()], covtpf.getCrtable())) {
						if (isEQ(wsaaT5673StoredTableInner.wsaaRtableUsed[ctIndex.toInt()][rdIndex.toInt()], SPACES)) {
							wsaaT5673StoredTableInner.wsaaRtableUsed[ctIndex.toInt()][rdIndex.toInt()].set("Y");
						}
						else {
							wsaaIndicStatus.set("N");
						}
						/*                 MOVE +49 TO CT-INDEX RD-INDEX        <S19FIX>*/
						ctIndex.set(49);
						rdIndex.set(49);
					}
				}
			}
		}
		}
  

protected void findFromCovt1930(){
		// ILIFE-3310 by liwei
		covtList = covtpfDAO.getCovtpfData(wsspcomn.company.toString(),chdrpf.getChdrnum(), lifemjaIO.getLife().toString(), "00", "00", 0);
		if(covtList != null && covtList.size() > 0){
			for (int i = 0; i <= covtList.size() || !indicFound.isTrue(); i++) {
				covtpf = covtList.get(i);
				if (isEQ(covtpf.getCrtable(), sv.crcode)|| isEQ(covtpf.getCrtable(), sv.rtable)) {

					wsaaIndicStatus.set("Y");
					return;
				}
			}
		}
		/* EXIT */
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    Skip this section  if returning  from an optional selection  */
		/*    (current stack position action flag = '*').                  */
		ispermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "MTL26601", appVars,smtpfxcpy.item.toString());//TSD-266
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		//TSD-266 Starts
		if(ispermission){
			if (isNE(sv.lifcnum,SPACES)) {
				if (isNE(wsaaLDob,wsaaCDob)) {
					sv.lifcnumErr.set(formatsInner.rfpk);
					wsspcomn.edterror.set("Y");
				}
				else {
					if(isNE(wsaaLSex,wsaaCSex)) {
						sv.lifcnumErr.set(formatsInner.pfpl);
						wsspcomn.edterror.set("Y");
					}
				}
			}
	
			if (isNE(sv.jlifcnum,SPACES)) {
				if (isNE(wsaaJlDob,wsaaJcDob)) {
					sv.jlifcnumErr.set(formatsInner.rfpk);
					wsspcomn.edterror.set("Y");
				}
				else {
					if (isNE(wsaaJlSex,wsaaJcSex)) {
						sv.jlifcnumErr.set(formatsInner.pfpl);
						wsspcomn.edterror.set("Y");
					}
				}
			}
		}
		//TSD-266 Ends
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validateSubfile2050();
				case selectCheck2060:
					selectCheck2060();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SR572IO' USING SCRN-SCREEN-PARAMS                      */
		/*                                SR572-DATA-AREA                  */
		/*                                SR572-SUBFILE-AREA.              */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*ROLL-UP*/
		/*    Check if another page has been requested.*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)
		|| isEQ(scrnparams.statuz,varcom.rold)) {
			validateSubfile2050();
			goTo(GotoLabel.exit2090);
		}
		
		
/** Check U/W date selected or not
 */		
		if (isNE(sv.huwdcdte,varcom.vrcmMaxDate)) {
			if (isEQ(sv.huwdcdte,0)) {
				scrnparams.errorCode.set(e032);
				wsspcomn.edterror.set("Y");
			}
			else {
				if (isGT(sv.huwdcdte,wsaaToday)) {
					sv.huwdcdteErr.set(hl01);
					wsspcomn.edterror.set("Y");
				}
			}
		}else{
			scrnparams.errorCode.set(f665);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSubfile2050()
	{
		/*    Check for a Change in any of the Screen Fields.*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*    If user did not select any component then exit.              */
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			scrnparams.errorCode.set(g931);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		wsaaIfCovrSelected.set("N");
	}

protected void selectCheck2060()
	{
		/*    Validate  Selection being  one  of the following conditions.*/
		/*    IF Valid then go to 4000-next-program.*/
		if (isEQ(sv.select, " ")
		|| isEQ(sv.select,"+")
		|| isEQ(sv.select,"X")) {
			/*NEXT_SENTENCE*/
		}
		else {
			/*    IF Invalid then create error condition and update Screen.*/
			sv.selectErr.set(e005);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.select,"X")) {
			if (isEQ(sv.rtable,SPACES)) {
				wsaaIfCovrSelected.set("Y");
			}
		}
		/*   Check if we have used the maximum number for this             */
		/*   coverage.                                                     */
		if (isNE(sv.crcode,SPACES)) {
			ctIndex.set(1);
			 searchlabel1:
			{
				for (; isLT(ctIndex, wsaaT5673StoredTableInner.wsaaT5673Info.length); ctIndex.add(1)){
					if (isEQ(wsaaT5673StoredTableInner.wsaaCtable[ctIndex.toInt()], sv.crcode)) {
						if (isEQ(wsaaT5673StoredTableInner.wsaaNoOfCovs[ctIndex.toInt()], wsaaT5673StoredTableInner.wsaaCtmaxcov[ctIndex.toInt()])) {
							sv.selectErr.set(h248);
							wsspcomn.edterror.set("Y");
						}
						break searchlabel1;
					}
				}
				/*CONTINUE_STMT*/
			}
		}
		if (isEQ(sv.select,"X")) {
			if (isNE(sv.rtable,SPACES)
			&& isEQ(sv.crcode,SPACES)) {
				if (!wsaaCovrSelected.isTrue()) {
					sv.selectErr.set(e005);
					wsspcomn.edterror.set("Y");
				}
			}
		}
		if (isNE(wsspcomn.edterror,varcom.oK)) {
			scrnparams.function.set(varcom.supd);
			processScreen("SR572", sv);
		}
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*    If user did not select any component then exit.              */
		/* IF SCRN-STATUZ             = ENDP                            */
		/*    GO                      TO 2090-EXIT.                     */
		/*    Re-check for Change in Screen fields.*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*    Loop until a Valid change is found.*/
		if (isNE(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.selectCheck2060);
		}
		
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
                     
   

                                
  
		/*    If  not  returning  from  a  selection ACTION  =  ' '  i.e.*/
		/*       First selection.*/
		/*       Find  first  selection  'X'  and  determine  conditions.*/
		/*    If returning from a selection ACTION = '*' then verify that*/
		/*       if it was mandatory, a transaction has been created?*/
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			initSelection4100();
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.function.set(varcom.srdn);
			if (isEQ(sv.hrequired,"Y")) {
				verifyComponent4200();
			}
		}
		/*    If the  end of  the  subfile or a  mandatory type  does not*/
		/*       have  a  transaction  record on  return.  Then exit  the*/
		/*       program  to either  the next  program  or  the  previous*/
		/*       component program respectively.*/
		if (wsaaImmExit.isTrue()) {
			return ;
		}
		/*    Read and Verify the next Subfile record.*/
		verifyNextSelect4300();
		/*    If the end of the subfile, exit to next initial program.*/
		if (wsaaImmExit.isTrue()) {
			/*    PERFORM 4900-UPDATE-HISTORY                       <LA3995>*/
			/*    PERFORM 8000-DRY-PROCESSING                       <LA3995>*/
			/*    PERFORM 5100-RELEASE-SFTLCK                       <LA3995>*/
			/*    PERFORM 5200-UPDATE-UWDATE                        <LA3995>*/
			checkChanges9000();
			if (isEQ(wsaaChange,"Y")) {
				updateHistory4900();
				dryProcessing8000();
			}
			releaseSftlck5100();
			if (isEQ(wsaaChange,"Y")) {
				updateUwdate5200();
			}
			if(lincFlag)
				performLincProcessing();
			return ;
		}
		/*    Read  Program  table to  find  Cover / Rider programs to be*/
		/*    executed.*/
		coverRiderProgs4500();
		/*    Read the  Program  table T5671 for  the components programs*/
		/*    to be executed next.*/
		readProgramTables4600();
		screenUpdate4800();
  

                                  
  
       
                       
   
                          
                                                      
		}
  

protected void initSelection4100()
	{
		/*    Save the  next  four  programs  after  PR572  into  Working*/
		/*    Storage for re-instatement at the end of the Subfile.*/
		compute(wsaaSub4, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub5.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			/*    This loop will load the next four  programs from WSSP Stack*/
			/*    into a Working Storage Save area.*/
			wsaaSecProg[wsaaSub5.toInt()].set(wsspcomn.secProg[wsaaSub4.toInt()]);
			wsaaSub4.add(1);
			wsaaSub5.add(1);
		}
		/*    First Read of the Subfile for a Selection.*/
		scrnparams.function.set(varcom.sstrt);
		/*    Go to the first  record  in  the Subfile to  find the First*/
		/*    Selection.*/
                           
	}

protected void verifyComponent4200()
	{
       
		/*    IF Returning from an optional Selection then Verify*/
		/*       that any Mandatory Selections were created.*/
		wsaaExitFlag.set("N");
		wsaaImmexitFlag.set("N");
		covrpfList = covrpfDAO.searchValidCovrpfRecord(wsspcomn.company.toString(),chdrpf.getChdrnum(),lifemjaIO.getLife().toString(),wsaaCoverageR.toString(),wsaaRiderR.toString());

		/*    IF the transaction has not been created then Keep the*/
		/*       transaction key and pass an error message to the*/
		/*       previous program, then exit.*/
		if (covrpfList == null || covrpfList.isEmpty()) {
			wsspcomn.msgarea.set(" MANDATORY TRANSACTION - PLEASE COMPLETE.");
			wsaaImmexitFlag.set("Y");
			readProgramTables4600();
		}
		wsaaExitFlag.set("N");
		/*    IF the mandatory Selection was not created then exit*/
		/*       to the previous program.*/
		/*    ELSE if OK then find next selection from Subfile.*/
		goTo(GotoLabel.exit4290);
	}
                                
protected void verifyNextSelect4300()
	{
		/*VERIFY*/
		/*    Read the Subfile until a Selection has been made.*/
		wsaaSelectFlag.set("N");
		wsaaImmexitFlag.set("N");
		while ( !(wsaaSelection.isTrue()
		|| wsaaImmExit.isTrue())) {
			next4400();
		}

		/*EXIT*/
	}

protected void next4400()
	{
		nextRec4410();
		ifSubfileEndp4420();
	}

protected void nextRec4410()
	{
		/*    Read next subfile record sequentially.*/
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4420()
	{
		/*    Check for the end of the Subfile.*/
		/*    If end of Subfile re-load the Saved four programs to*/
		/*       Return to initial stack order.*/
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			reloadProgsWssp4430();
			return;
		}
		/*    Flag set off on each sequential read down on a Cover.*/
		/*    Will be set on if selected in the following Condition check.*/
		/*    IF SR572-CRCODE             NOT = SPACES*/
		if (isEQ(sv.rtable,SPACES)) {
			wsaaCoverFlag.set("N");
		}
		/*    IF entering on a LIFE then verify Cover/Rider.*/
		/*       n.b. If Cover not selected then riders can not be*/
		/*       processed.*/
		/*    IF entering on a COVERAGE then the Flag will be set on*/
		/*       as the Cover should not be selected for the Riders*/
		/*       to be processed.*/
		/*    IF Rider = spaces then you are processing a Cover*/
		/*    ELSE you are processing a rider.*/
		/*    IF SR572-SELECT             = 'X'*/
		/*       IF WSAA-LIFEORCOV        = 'L'*/
		/*          IF SR572-CRCODE       NOT = SPACES*/
		/*             MOVE 'Y'           TO WSAA-COVER-FLAG*/
		/*             MOVE 'Y'           TO WSAA-SELECT-FLAG*/
		/*          ELSE*/
		/*             IF WSAA-COVER*/
		/*                MOVE 'Y'        TO WSAA-SELECT-FLAG*/
		/*             ELSE*/
		/*                MOVE 'N'        TO WSAA-SELECT-FLAG*/
		/*       ELSE*/
		/*          MOVE 'Y'              TO WSAA-COVER-FLAG*/
		/*          MOVE 'Y'              TO WSAA-SELECT-FLAG*/
		/*    ELSE*/
		/*       MOVE 'N'                 TO WSAA-SELECT-FLAG.*/
		if (isEQ(sv.select,"X")) {
			wsaaSelectFlag.set("Y");
			if (isEQ(sv.rtable,SPACES)) {
				wsaaCoverFlag.set("Y");
			}
		}
		scrnparams.function.set(varcom.srdn);
                           
	}

protected void reloadProgsWssp4430()
	{
		/*    Set Flag for immediate exit as the next action is to go to*/
		/*    the next program in the stack.*/
		/*    Set Subscripts and call Loop.*/
		wsaaImmexitFlag.set("Y");
		compute(wsaaSub4, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub5.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
			/*    Re-load the Saved four programs from Working Storage to*/
			/*    the WSSP Program Stack.*/
			wsspcomn.secProg[wsaaSub4.toInt()].set(wsaaSecProg[wsaaSub5.toInt()]);
			wsaaSub4.add(1);
			wsaaSub5.add(1);
		}
		/*    Set action flag to ' ' in order that the program will*/
		/*    resume initial stack order and go to the next program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.programPtr.add(1);
                           
	}

protected void coverRiderProgs4500()
	{
       
		initCovrmja4510();
		coverOrRider4520();
	}

protected void initCovrmja4510()
{
	/*    Initialise COVRMJA Fields for the following KEEPS.*/
	//covrmjaIO.setDataArea(SPACES);
	covrpf.setPlanSuffix(0);
	covrpf.setPayrseqno(1);
	covrpf.setCrInstamt01(new BigDecimal(0));
	covrpf.setCrInstamt02(new BigDecimal(0));
	covrpf.setCrInstamt03(new BigDecimal(0));
	covrpf.setCrInstamt04(new BigDecimal(0));
	covrpf.setCrInstamt05(new BigDecimal(0));
	covrpf.setEstMatValue01(new BigDecimal(0));
	covrpf.setEstMatValue02(new BigDecimal(0));
	covrpf.setEstMatDate01(0);
	covrpf.setEstMatDate02(0);
	covrpf.setEstMatInt01(new BigDecimal(0));
	covrpf.setEstMatInt02(new BigDecimal(0));
	covrpf.setTranno(0);
	covrpf.setCurrfrom(0);
	covrpf.setCurrto(0);
	covrpf.setCrrcd(0);
	covrpf.setAnbAtCcd(0);
	covrpf.setRiskCessDate(0);
	covrpf.setPremCessDate(0);
	covrpf.setBenCessDate(0);
	covrpf.setNextActDate(0);
	covrpf.setReserveUnitsDate(0);
	covrpf.setConvertInitialUnits(0);
	covrpf.setReviewProcessing(0);
	covrpf.setUnitStatementDate(0);
	covrpf.setCpiDate(0);
	covrpf.setInitUnitCancDate(0);
	covrpf.setExtraAllocDate(0);
	covrpf.setInitUnitIncrsDate(0);
	covrpf.setRiskCessAge(0);
	covrpf.setPremCessAge(0);
	covrpf.setBenCessAge(0);
	covrpf.setRiskCessTerm(0);
	covrpf.setPremCessTerm(0);
	covrpf.setBenCessTerm(0);
	covrpf.setSumins(new BigDecimal(0));
	covrpf.setVarSumInsured(new BigDecimal(0));
	covrpf.setDeferPerdAmt(new BigDecimal(0));
	covrpf.setTotMthlyBenefit(new BigDecimal(0));
	covrpf.setSingp(new BigDecimal(0));
	covrpf.setInstprem(new BigDecimal(0));
	covrpf.setStatSumins(new BigDecimal(0));
	covrpf.setRtrnyrs(0);
	covrpf.setPremCessAgeMth(0);
	covrpf.setPremCessAgeDay(0);
	covrpf.setPremCessTermMth(0);
	covrpf.setPremCessTermDay(0);
	covrpf.setRiskCessAgeMth(0);
	covrpf.setRiskCessAgeDay(0);
	covrpf.setRiskCessTermMth(0);
	covrpf.setRiskCessTermDay(0);
	covrpf.setRerateDate(0);
	covrpf.setRerateFromDate(0);
	covrpf.setBenBillDate(0);
	covrpf.setCoverageDebt(new BigDecimal(0));
	covrpf.setAnnivProcDate(0);
	covrpf.setTransactionDate(varcom.vrcmDate.toInt());
	covrpf.setTransactionTime(varcom.vrcmTime.toInt());
	covrpf.setUser(varcom.vrcmUser.toInt());
}

protected void coverOrRider4520()
	{
		/*    Subfile record a COVER or a RIDER?*/
		/*    n.b. If entering  on a  COVERAGE, the Cover details  should*/
		/*    not get this far.*/
		/*    IF SR572-CRCODE             NOT = SPACES*/
		if (isEQ(sv.rtable,SPACES)) {
			coverNextProg4530();
		}
		if (isNE(sv.rtable,SPACES)) {
			riderNextProg4540();
		}
		/*    Keep the key of the COVRMJA transaction file for the next*/
		/*       program.*/
		keepsCovrmja4550();
		if(uwFlag) {
			saveAutoTerms();
		}
	}
	private void saveAutoTerms() {
		checkOccupationTerm();
	}

	private void checkOccupationTerm() {
		uwOccupationRec = new UWOccupationRec();
		uwOccupationRec.transEffdate.set(datcon1rec.intDate.getbigdata());
		uwOccupationRec.occupCode.set(clntpf.getOccpcode());
		uwOccupationRec.indusType.set(clntpf.getStatcode());
		
		String company = wsspcomn.company.toString();
		String fsuco = wsspcomn.fsuco.toString();
		String cnttype = chdrpf.getCnttype();
		
		uwOccupationRec = uwoccupationUtil.getUWOccupationRec(uwOccupationRec, company,
				fsuco, cnttype, wsaaCrtable.toString().trim());
		if (uwOccupationRec != null && isNE(uwOccupationRec.outputUWDec, SPACE)
				&& isNE(uwOccupationRec.outputUWDec, "Accepted")
				&& isNE(uwOccupationRec.outputUWDec, "0")) {
			LextTableDAM lextIO = initLextpf();
			lextIO.setInsprm(uwOccupationRec.outputSplTermRateAdj);
			lextIO.setAgerate(uwOccupationRec.outputSplTermAge);
			lextIO.setOppc(uwOccupationRec.outputSplTermLoadPer);
			lextIO.setZmortpct(uwOccupationRec.outputMortPerc);
			lextIO.setZnadjperc(uwOccupationRec.outputSplTermSAPer);
			lextIO.setOpcda(uwOccupationRec.outputSplTermCode);
			setT5657Sbstdl(lextIO);
			saveLextpf(lextIO);
			List<String> autoTermsList = (List)ThreadLocalStore.get(AUTO_TERMS_LIST_ADD);
			if(autoTermsList == null) {
				autoTermsList = new ArrayList<String>();
				ThreadLocalStore.put(AUTO_TERMS_LIST_ADD, autoTermsList);
			}
			autoTermsList.add(wsaaCrtable.toString().trim());
		}
	}

	private LextTableDAM initLextpf() {
		LextTableDAM lextIO = new LextTableDAM();

		lextIO.setChdrcoy(chdrpf.getChdrcoy());
		lextIO.setChdrnum(chdrpf.getChdrnum());
		lextIO.setLife(covrpf.getLife());
		lextIO.setCoverage(covrpf.getCoverage());
		lextIO.setRider(covrpf.getRider());

		lextIO.setSeqnbr(seqnbr++);
		lextIO.setValidflag("1");
		lextIO.setCurrfrom(ZERO);
		lextIO.setCurrto(varcom.vrcmMaxDate);
		lextIO.setReasind("1");
		//lextIO.setJlife(lifemjaIO.getJlife());

		lextIO.setTranno(chdrpf.getTranno());
		lextIO.setExtCommDate(chdrpf.getBtdate());
		lextIO.setExtCessDate(covrpf.getPremCessDate());

		lextIO.setUwoverwrite("Y");
		lextIO.setTermid(varcom.vrcmTermid);
		lextIO.setTransactionDate(varcom.vrcmDate);
		lextIO.setTransactionTime(varcom.vrcmTime);
		lextIO.setUser(varcom.vrcmUser);
		
		return lextIO;
	}

private void setT5657Sbstdl(LextTableDAM lextIO) {
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(covrpf.getChdrcoy());
	itemIO.setItemtabl("T5657");
	
	StringBuilder wsaaT5657key = new StringBuilder(); 
	wsaaT5657key.append(wsaaCrtable.toString().trim());
	wsaaT5657key.append(lextIO.getOpcda().toString().trim());
	itemIO.setItemitem(wsaaT5657key.toString());
	itemIO.setFunction(varcom.readr);
	itemIO.setFormat(itemrec);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	T5657rec t5657rec = new T5657rec();
	t5657rec.t5657Rec.set(itemIO.getGenarea());
	lextIO.setSbstdl(t5657rec.sbstdl);
}
private void saveLextpf(LextTableDAM lextIO) {
	lextIO.setFormat("LEXTREC");
	lextIO.setFunction(varcom.writr);
	SmartFileCode.execute(appVars, lextIO);
	if (isNE(lextIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(lextIO.getParams());
		fatalError600();
	}
}

protected void coverNextProg4530()
	{
		/*    The following flag is set up previously to show that*/
		/*    all conditions have been satisfied.*/
		/*    IF entering on a LIFE and Cover selected 'X' then the*/
		/*       flag is set ON.*/
		/*    IF entering on a COVERAGE then the flag is set OFF.*/
		/*    n.b. The cover can not be selected.*/
		/*    KEY set up to reference program table T5671*/
		/*       n.b. this will say which programs are called for*/
		/*       the required Cover.*/
		if (wsaaCover.isTrue()) {
			wsaaCrtable.set(sv.crcode);
			wsaaTrancode.set(wsaaBatckey.batcBatctrcde);
		}
		/*    The next sequential number of the Cover is passed to*/
		/*    the transaction key (only if flag is ON).*/
		/*    Rider key reset = '00'.*/
		/*    STORE the Cover key for Rider processing.*/
		wsaaNextCovnoR.add(1);
		wsaaCoverage.set(wsaaNextCovnoR);
		covrpf.setCoverage(wsaaNextCovnoR.toString());
		wsaaRider.set(wsaaNextRidnoR);
		covrpf.setRider(wsaaNextRidnoR.equals("00") ? "00" : wsaaNextRidnoR.toString());
	}

protected void riderNextProg4540()
	{
		/*    The following hidden field is set up previously to*/
		/*    provide a flag showing that all conditions have been*/
		/*    satisfied.*/
		/*    IF entering on a LIFE and Cover selected 'X' then the*/
		/*       flag is set ON.*/
		/*    ELSE Riders can not be selected, flag OFF.*/
		/*    IF entering on a COVERAGE and Rider selected 'X' then*/
		/*       flag is set ON.*/
		/*    KEY set up to reference program table T5671*/
		/*       n.b. this will say which programs are called for*/
		/*       the required Rider.*/
		/*    IF ADD-ON RIDER ON EXISTING COVERAGE , THE EXITING COVERAGE*/
		/*    NUMBER HELD IN SR572-CRCODE.*/
		/*  IF WSAA-COVER                                                */
		/*         OR                                                    */
		/*     SR572-CRCODE NOT = SPACE                                  */
		wsaaCrtable.set(sv.rtable);
		wsaaTrancode.set(wsaaBatckey.batcBatctrcde);
		/*    The next sequential number of the Rider.*/
		/*    IF a LIFE then rider number  is sequential on the  order of*/
		/*       selection.*/
		/*    ELSE entry is on a COVERAGE where the  rider number assigned*/
		/*       to the number following the highest rider on file.*/
		if (isNE(sv.crcode,SPACES)) {
			covrpf.setCoverage(sv.crcode.toString());
		}
		else {
			covrpf.setCoverage(wsaaCoverage.toString());
		}
		/*   ADD 1                    TO WSAA-NEXT-RIDNO-R                */
		wsaaRidrSub.set(covrpf.getCoverage());
		compute(wsaaNextRidnoR, 0).set(add(1,wsaaRidrTab[wsaaRidrSub.toInt()]));
		wsaaRidrTab[wsaaRidrSub.toInt()].set(wsaaNextRidno);
		covrpf.setRider(wsaaNextRidno.toString());
	}

	/**
	* <pre>
	*    IF WSAA-LIFEORCOV           = 'L'
	*       MOVE WSAA-COVERAGE       TO COVRMJA-COVERAGE
	*       ADD 1                    TO WSAA-RIDER
	*       MOVE WSAA-RIDER          TO COVRMJA-RIDER
	*    ELSE
	*       MOVE WSAA-COVERAGE       TO COVRMJA-COVERAGE
	*       ADD 1                    TO WSAA-NEXT-RIDNO-R
	*       MOVE WSAA-NEXT-RIDNO     TO COVRMJA-RIDER.
	* </pre>
	*/
protected void keepsCovrmja4550()
	{
		/*    KEEP the key for the transaction record COVRMJA,*/
		/*    which will be accessed in the next program.*/
		covrpf.setCrtable(wsaaCrtable.toString());
		covrpf.setChdrcoy(wsspcomn.company.toString());
		covrpf.setChdrnum(chdrpf.getChdrnum());
		covrpf.setLife(lifemjaIO.getLife().toString());
		/*covrmjaIO.setFunction(varcom.keeps);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		covrpfDAO.setCacheObject(covrpf);
	}

protected void readProgramTables4600()
	{
		if(readProgramTable4610()){
			loadProgsToWssp4620();
		}
	}

protected boolean readProgramTable4610()
	{
		/*    Read the  Program  table T5671 for  the components programs*/
		/*    to be executed next.*/
		wsspcomn.nextprog.set(wsaaProg);
		boolean itemFound = false;
		String keyItemitem = wsaaConcatName.toString();
 //ILIFE-8723 start
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5671");
		itempf.setItemitem(keyItemitem);
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			if (isEQ(wsaaCrtable,"****")) {
				sv.selectErr.set("G433");
				wsspcomn.edterror.set("Y");
				wsspcomn.nextprog.set(scrnparams.scrname);
				goTo(GotoLabel.exit4490);
			}
			else {
				wsaaCrtable.set("****");
				goTo(GotoLabel.readProgramTable4610);
			}
		}
		else {
			t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
                                                    
		/*if (t5671ListMap.containsKey(keyItemitem)) {
			List<Itempf> itempfList = t5671ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
                                 
				Itempf itempf = iterator.next();
				t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
				break;
			}
		}else{
			syserrrec.params.set(t5671rec.t5671Rec);
			fatalError600();
		}
		if(!itemFound) {
			if (isEQ(wsaaCrtable,"****")) {
				sv.selectErr.set(g433);
				wsspcomn.edterror.set("Y");
				wsspcomn.nextprog.set(scrnparams.scrname);
				          PERFORM 4700-PLAN-RELOAD                               
				return false;

			}
			else {
				wsaaCrtable.set("****");
                                         
			}
		}*/
 //ILIFE-8723 end 
		return true;
	}
  
protected void loadProgsToWssp4620()
	{
		/*    Move the component programs to the WSSP stack.*/
		wsaaSub4.set(1);
		compute(wsaaSub5, 0).set(add(1,wsspcomn.programPtr));
		for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
			/*    This loop will load four programs from table T5671 to*/
			/*    the WSSP stack.*/
			wsspcomn.secProg[wsaaSub5.toInt()].set(t5671rec.pgm[wsaaSub4.toInt()]);
			wsaaSub4.add(1);
			wsaaSub5.add(1);
		}
		/*    Reset the Action to '*' signifying action desired to*/
		/*    the next program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void planReload4700()
	{
		try {
			reloadProgsWssp4710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void reloadProgsWssp4710()
	{
		/* Set Flag for immediate exit as the next action is to go to the  */
		/* next program in the stack.                                      */
		/* Set Subscripts and call Loop.                                   */
		compute(wsaaSub1, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 4); loopVar5 += 1){
			loop34720();
		}
		/* Set action flag to ' ' in order that the program will resume    */
		/* initial stack order and go to the next program.                 */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		goTo(GotoLabel.exit4790);
	}

protected void loop34720()
	{
		/* Re-load the Saved four programs from Working Storage to the     */
		/* WSSP Program Stack.                                             */
		wsspcomn.secProg[wsaaSub1.toInt()].set(wsaaSecProg[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
	}

protected void screenUpdate4800()
	{
		/*UPDATE-SCREEN*/
		scrnparams.function.set(varcom.supd);
		processScreen("SR572", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateHistory4900()
	{
       
		readPtrnrev4910();
		writePtrn4920();
	}

protected boolean readPtrnrev4910()
	{
		/* READ PTRNREV LAST TRANSACTION CODE                              */
		compute(wsaaNewTranno, 0).set(add(1,chdrpf.getTranno()));
		List<Ptrnpf> ptrnpfList = ptrnpfDAO.searchPtrnrevData(chdrpf.getChdrcoy().toString(),
				chdrpf.getChdrnum());

		if (ptrnpfList == null || ptrnpfList.isEmpty()) {
			syserrrec.params.set(chdrpf.getChdrnum());
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600();
		}
		Ptrnpf ptrnrevIO = ptrnpfList.get(0);
		if (isEQ(wsaaBatckey.batcBatctrcde, ptrnrevIO.getBatctrcde())) {
			return false;
		}
		return true;
	}

protected void writePtrn4920()
	{
		/* Write a PTRN record.                                            */
		//ILB-499 start
		/*ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrpf.getChdrcoy());
		ptrnIO.setChdrnum(chdrpf.getChdrnum());
		ptrnIO.setValidflag("1");
		ptrnIO.setTranno(wsaaNewTranno);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}*/
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		ptrnpf.setTranno(wsaaNewTranno.toInt());//ILIFE-8132
		ptrnpf.setPtrneff(wsspcomn.currfrom.toInt());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setValidflag("1");
		ptrnpf.setTrdt(wsaaToday.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setDatesub(wsaaToday.toInt());
		ptrnpf.setChdrpfx("CH");
		//ILB-499 start
		ptrnpfDAO.insertPtrnPF(ptrnpf);
		/*ptrnpfList = new ArrayList<Ptrnpf>();
		ptrnpfList.add(ptrnpf);	
		boolean result;
		if(ptrnpfList != null && ptrnpfList.size() > 0){
			result = ptrnpfDAO.insertPtrnPF(ptrnpfList);
			if (!result) {
				syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum().toString()));
				fatalError600();
			} else ptrnpfList.clear();
		}*/
		updateContractStatus5000();
		//ILB-499 end 
	}

protected void updateContractStatus5000()
	{
		updateCurrent4840();
		createNew5020();
		keepsChdrmja5030();
	}

protected void updateCurrent4840()
	{
	/* UPDATE CHDRMJA RECORD VALID FLAG TO '2' AND CURRTO TO TODAY'S DT*/
//ILB-456
/*	chdrmjaIO.setFunction(varcom.rlse);
	SmartFileCode.execute(appVars, chdrmjaIO);
	if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrmjaIO.getParams());
		syserrrec.statuz.set(chdrmjaIO.getStatuz());
		fatalError600();
	}
	 DO A READH ON CHDRMJA RECORD BEFORE REWRT                       
	chdrmjaIO.setFunction(varcom.readh);
	SmartFileCode.execute(appVars, chdrmjaIO);
	if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrmjaIO.getParams());
		syserrrec.statuz.set(chdrmjaIO.getStatuz());
		fatalError600();
	}*/
	chdrpfDAO.deleteCacheObject(chdrpf);
	chdrpf = chdrpfDAO.getchdrRecordservunit(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
	if (chdrpf == null) {
		fatalError600();
	}
	/* SET THE CHDRMJA RECORD VALID FLAG TO '2'                        */

	chdrpf.setCurrto(wsaaToday.toInt());
	//chdrpf.setValidflag("2".charAt(0));
	/*chdrmjaIO.setFunction(varcom.rewrt);
	SmartFileCode.execute(appVars, chdrmjaIO);
	if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
		syserrrec.statuz.set(chdrmjaIO.getStatuz());
		syserrrec.params.set(chdrmjaIO.getParams());
		fatalError600();
	}*/
	
	boolean flag = chdrpfDAO.updateChdrValidflag(chdrpf, "2");
	if (flag == false) {
		fatalError600();
	}	
	}

protected void createNew5020()
	{
	/* CREATE NEW RECORD IN CHDRMJA WITH STATCODE FROM T5679           */
	chdrpf.setStatcode(t5679rec.setCnRiskStat.toString());
	chdrpf.setCurrfrom(wsaaToday.toInt());
	chdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	chdrpf.setValidflag("1".charAt(0));
	chdrpf.setTranlused(chdrpf.getTranno());
	setPrecision(chdrpf.getTranno(), 0);
	chdrpf.setTranno(add(chdrpf.getTranno(),1).toInt());
	//ILB-456
	chdrpfDAO.insertChdrRecords(chdrpf); //ILIFE-8076
	}

protected void keepsChdrmja5030()
	{
		/* DO A KEEPS ON CHDRMJA RECORD FOR THE NEXT PROGRAM               */
		//ILB-456
		chdrpfDAO.setCacheObject(chdrpf);
		/*EXIT*/
	}

protected void releaseSftlck5100()
	{
		/* Release the soft lock on the contract.                          */
		//ILB-499 start
		/*sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum().toString());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}*/
		
		sftlockRecBean.setFunction("UNLK");
		sftlockRecBean.setCompany(wsspcomn.company.toString().trim());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(chdrpf.getChdrnum().trim());
		sftlockRecBean.setTransaction(wsaaBatckey.batcBatctrcde.toString());
		sftlockRecBean.setUser(varcom.vrcmUser.toString());
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())
			&& !"LOCK".equals(sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				fatalError600();
		}
		 //ILB-499 ends
	}

protected void updateUwdate5200()
	{
		/*BEGIN*/
		cvuwIO.setRecKeyData(SPACES);
		cvuwIO.setChdrcoy(chdrpf.getChdrcoy().toString()); //ILB-499
		cvuwIO.setChdrnum(chdrpf.getChdrnum()); //ILB-499
		cvuwIO.setEffdate(wsspcomn.currfrom);
		cvuwIO.setHuwdcdte(sv.huwdcdte);
		cvuwIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, cvuwIO);
		if (isNE(cvuwIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cvuwIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void dryProcessing8000()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrpf.getCnttype());/* IJTI-1523 */
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		//Ilife-3310 add by liwei                 */
 //ILIFE-8723 start
	/*	boolean itemFound = false;
		itemFound = readT75088100(wsaaT7508Key.toString().trim());
		if (!itemFound) {
			wsaaT7508Cnttype.set("***");
			itemFound = readT75088100(wsaaT7508Key.toString().trim());
			if (!itemFound) {
				return;
			}
		}
		t7508rec.t7508Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		*/
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (itempf == null) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (itempf == null) {
			return ;
		}
		t7508rec.t7508Rec.set(StringUtil.rawToString(itempf.getGenarea()));
 //ILIFE-8723 end
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrpf.getChdrnum()); //ILB-499
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrpf.getTranno()); //ILB-499
		drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl()); //ILB-499
		drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq()); //ILB-499
		drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
		drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
		drypDryprcRecInner.drypStmdte.set(chdrpf.getStatdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(varcom.vrcmMaxDate);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}

	}
 //ILIFE-8723 start
protected void readT75088100() {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T7508");
		itempf.setItemitem(wsaaT7508Key.toString());
	    itempf = itemDAO.getItemRecordByItemkey(itempf);
		/*boolean itemFound = false;
                                                    
		if (t7508ListMap.containsKey(keyItemitem)) {
			List<Itempf> itempfList = t7508ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
                                 
				Itempf itempf = iterator.next();
				t7508rec.t7508Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;
				break;
			}
		}
		return itemFound;*/
 //ILIFE-8723 end
	}

protected void checkChanges9000()
	{
		wsaaChange = "N";
 //IBPLIFE-6053, DAM to DAO conversion		
		List<Covtpf> covtpfList = null;
		covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(!covtpfList.isEmpty()) {
			wsaaChange = "Y";
			return ;
		}
	
		 //IBPLIFE-6053 end
		if (isNE(sv.huwdcdte,ZERO)
		&& isNE(sv.huwdcdte,varcom.vrcmMaxDate)) {
			wsaaChange = "Y";
			return ;
		}
		if(cvuwpf != null){
			wsaaChange = "Y";
			return ;
		}else{
			fatalError600();
		}
	}

protected void performLincProcessing() {
	Tjl47rec tjl47rec = new Tjl47rec(); 
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
	itempf.setItemtabl(tjl47);
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if(itempf != null) {
		tjl47rec.tjl47Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		LincCSService lincService = (LincCSService) getApplicationContext().getBean(tjl47rec.lincsubr.toString().toLowerCase());
		lincService.addComponentProposal(setLincpfHelper(), tjl47rec);
	}
}

	protected LincpfHelper setLincpfHelper() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		LincpfHelper lincpfHelper = new LincpfHelper();
		lincpfHelper.setChdrcoy(wsspcomn.company.toString());
		lincpfHelper.setChdrnum(chdrpf.getChdrnum());
		lincpfHelper.setOwncoy(wsspcomn.fsuco.toString());
		lincpfHelper.setClntnum(chdrpf.getCownnum().trim());
		lincpfHelper.setCownnum(chdrpf.getCownnum().trim());
		lincpfHelper.setLifcnum(lifemjaIO.getLifcnum().toString());
		lincpfHelper.setLcltdob(lifemjaIO.getCltdob().toInt());
		lincpfHelper.setLcltsex(lifemjaIO.getCltsex().toString());
		lincpfHelper.setLzkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setLzkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setLzkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setLzkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setLzkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setLzkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setLzkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setLgivname(clntpf.getGivname());
		lincpfHelper.setLsurname(clntpf.getSurname());
		lincpfHelper.setCnttype(chdrpf.getCnttype());
		lincpfHelper.setOccdate(chdrpf.getOccdate());	
		lincpfHelper.setTransactionDate(datcon1rec.intDate.toInt());
		lincpfHelper.setTranscd(wsaaBatckey.batcBatctrcde.toString());
		lincpfHelper.setLanguage(wsspcomn.language.toString());
		lincpfHelper.setTranno(chdrpf.getTranno());
		lincpfHelper.setPtrnEff(wsspcomn.currfrom.toInt());
		lincpfHelper.setTermId(varcom.vrcmTermid.toString());
		lincpfHelper.setTrdt(varcom.vrcmDate.toInt());
		lincpfHelper.setTrtm(varcom.vrcmTime.toInt());
		lincpfHelper.setUsrprf(varcom.vrcmUser.toString());
		lincpfHelper.setJobnm(wsspcomn.userid.toString());
		lincpfHelper = setClientDetails(lincpfHelper);
		return lincpfHelper;
	}
	
	protected LincpfHelper setClientDetails(LincpfHelper lincpfHelper) {
		Clntpf clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(lincpfHelper.getOwncoy());
		clntpf.setClntnum(lincpfHelper.getClntnum());
		clntpf = clntpfDAO.selectClient(clntpf);
		lincpfHelper.setZkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setZkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setZkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setZkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setZkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setZkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setZkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setGivname(clntpf.getGivname());
		lincpfHelper.setSurname(clntpf.getSurname());
		lincpfHelper.setCltsex(clntpf.getCltsex());
		lincpfHelper.setCltdob(clntpf.getCltdob());
		lincpfHelper.setClttype(clntpf.getClttype());
		return lincpfHelper;
	}

/*
 * Class transformed  from Data Structure WSAA-T5673-STORED-TABLE--INNER
 */
private static final class WsaaT5673StoredTableInner {

		/*   The array is amended to cater for continuation items.
		   A maximum of 48 coverages seems sufficient.                   */
	private FixedLengthStringData wsaaT5673StoredTable = new FixedLengthStringData(18960);
		/* 03  WSAA-T5673-INFO OCCURS 8 TIMES                           */
	private FixedLengthStringData[] wsaaT5673Info = FLSArrayPartOfStructure(48, 395, wsaaT5673StoredTable, 0);
	private FixedLengthStringData[] wsaaZrlifind = FLSDArrayPartOfArrayStructure(1, wsaaT5673Info, 0);
	private FixedLengthStringData[] wsaaCreq = FLSDArrayPartOfArrayStructure(1, wsaaT5673Info, 1);
	private FixedLengthStringData[] wsaaCtable = FLSDArrayPartOfArrayStructure(4, wsaaT5673Info, 2);
	private ZonedDecimalData[] wsaaCtmaxcov = ZDArrayPartOfArrayStructure(2, 0, wsaaT5673Info, 6);
	private ZonedDecimalData[] wsaaNoOfCovs = ZDArrayPartOfArrayStructure(2, 0, wsaaT5673Info, 8);
	private FixedLengthStringData[] wsaaCtableUsed = FLSDArrayPartOfArrayStructure(1, wsaaT5673Info, 10);
	private FixedLengthStringData[][] wsaaT5673RidrInfo = FLSDArrayPartOfArrayStructure(48, 8, wsaaT5673Info, 11);
	private FixedLengthStringData[][] wsaaRreq = FLSDArrayPartOfArrayStructure(1, wsaaT5673RidrInfo, 0);
	private FixedLengthStringData[][] wsaaRtable = FLSDArrayPartOfArrayStructure(4, wsaaT5673RidrInfo, 1);
	private FixedLengthStringData[][] wsaaRtableUsed = FLSDArrayPartOfArrayStructure(1, wsaaT5673RidrInfo, 5);
	private FixedLengthStringData[][] wsaaRtableRisk = FLSDArrayPartOfArrayStructure(2, wsaaT5673RidrInfo, 6);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	//TSD-266 Start
                                                                                
	private FixedLengthStringData rfpk = new FixedLengthStringData(4).init("RFPK");
	private FixedLengthStringData pfpl = new FixedLengthStringData(4).init("RFPL");
	//TSD-266 Ends
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

		/*  COPY SR572SCR.                                               */
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}