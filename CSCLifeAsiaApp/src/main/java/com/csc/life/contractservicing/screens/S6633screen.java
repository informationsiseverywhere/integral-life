package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6633screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6633ScreenVars sv = (S6633ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6633screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6633ScreenVars screenVars = (S6633ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.intRate.setClassString("");
		screenVars.nofDay.setClassString("");
		screenVars.inttype.setClassString("");
		screenVars.mperiod.setClassString("");
		screenVars.day.setClassString("");
		screenVars.compfreq.setClassString("");
		screenVars.annpoly.setClassString("");
		screenVars.annloan.setClassString("");
		screenVars.policyAnnivInterest.setClassString("");
		screenVars.loanAnnivInterest.setClassString("");
		screenVars.interestFrequency.setClassString("");
		screenVars.interestDay.setClassString("");
	}

/**
 * Clear all the variables in S6633screen
 */
	public static void clear(VarModel pv) {
		S6633ScreenVars screenVars = (S6633ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.intRate.clear();
		screenVars.nofDay.clear();
		screenVars.inttype.clear();
		screenVars.mperiod.clear();
		screenVars.day.clear();
		screenVars.compfreq.clear();
		screenVars.annpoly.clear();
		screenVars.annloan.clear();
		screenVars.policyAnnivInterest.clear();
		screenVars.loanAnnivInterest.clear();
		screenVars.interestFrequency.clear();
		screenVars.interestDay.clear();
	}
}
