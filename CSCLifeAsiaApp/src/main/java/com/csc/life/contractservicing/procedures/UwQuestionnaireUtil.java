package com.csc.life.contractservicing.procedures;

import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;

public interface UwQuestionnaireUtil {
	UWQuestionnaireRec getUWMessage(UWQuestionnaireRec uwQuestionnaireRec, String language,String company,String question,String crtable,String reasondCode);

}
