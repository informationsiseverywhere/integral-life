package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CprppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:37
 * Class transformed from CPRPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CprppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 76;
	public FixedLengthStringData cprprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData cprppfRecord = cprprec;
	
	public FixedLengthStringData chdrpfx = DD.chdrpfx.copy().isAPartOf(cprprec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(cprprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(cprprec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(cprprec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(cprprec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(cprprec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(cprprec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(cprprec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(cprprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(cprprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(cprprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(cprprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CprppfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CprppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CprppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CprppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CprppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CprppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CprppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CPRPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRPFX, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"TRANNO, " +
							"INSTPREM, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrpfx,
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     tranno,
                                     instprem,
                                     validflag,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrpfx.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		tranno.clear();
  		instprem.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCprprec() {
  		return cprprec;
	}

	public FixedLengthStringData getCprppfRecord() {
  		return cprppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCprprec(what);
	}

	public void setCprprec(Object what) {
  		this.cprprec.set(what);
	}

	public void setCprppfRecord(Object what) {
  		this.cprppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(cprprec.getLength());
		result.set(cprprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}