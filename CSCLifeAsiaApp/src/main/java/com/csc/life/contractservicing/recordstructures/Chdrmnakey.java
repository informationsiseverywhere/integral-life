package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:21
 * Description:
 * Copybook name: CHDRMNAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrmnakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrmnaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData chdrmnaKey = new FixedLengthStringData(256).isAPartOf(chdrmnaFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrmnaChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrmnaKey, 0);
  	public FixedLengthStringData chdrmnaChdrnum = new FixedLengthStringData(8).isAPartOf(chdrmnaKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(chdrmnaKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrmnaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrmnaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}