/*
 * File: Pr52k.java
 * Date: December 3, 2013 3:26:31 AM ICT
 * Author: CSC
 * 
 * Class transformed from PR52K.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.procedures.Revgenat;
import com.csc.life.contractservicing.screens.Sr52kScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tr52mrec;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrssurTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*       FREE LOOK CANCELLATION PROCESSING.
*       ---------------------------------------
*
* Overview.
* ---------
*
* This program is used to display the details of the Contract
*  that is going to be free look Cancelled .
*
* The new Freelook Cancelation facility has been developed to
* address some drawbacks of the current AFI facility.
*
* Initialisation.
* ---------------
*
* Default the Effective date to the contract RCD.
*
* Read the Contract header (CHDRLNB) using the RETRV function to g t
*  the relevant data necessary for obtaining the status descriptio 
*  and the agent details.
*
* Obtain the necessary descriptions for Contract Type, Contract
*  Risk Status, Contract Premium Status and Transaction Code (for
*  the screen title) by calling 'DESCIO' for the respective tables
*  and then display these descriptions on the screen SR52K.
*
* Client name
*
*     Read the Client Details file to get the client's name
*
* Paid-to date
*
*     Read the PAYR file to get the Paid-to date
*          and the Billed-to Date
*
* Payer name
*
*     Read the Client Role file (CLRF) to get the client number
*          of the payer, then read the Client details file with
*          payer client number to get payer name
*
* Agent details
*
*     In order to read the agent file use the agent details
*          from the contract header (READR on the AGLFLNB file).
*
* Read T6661 using the current transaction code so that we can get
*  the transaction code held on the T6661 record which we can
*  pass to the AT module to tell it what transaction to stop at
*  when it is processing the PTRNs for the contract.
*
* Validation.
* -----------
*
* If the user pressed 'KILL', exit from the program.
*
* Updating.
* ---------
*
* If the user pressed 'KILL', don't continue with rest of section
*  and exit from program.
*
* Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
* Call the AT submission module to submit REVGENAT, passing the
*  contract number as the 'primary key'. This performs the
*  AFI/CFI transaction.
*
* Tables Used.
* ------------
*
* T1688 - Transaction codes
* T3588 - Contract Premium Status
* T3623 - Contract Risk Status
* T5688 - Contract Structure
* T6661 - Transaction Reversal Parameters
*
*
****************************************************************** ****
* </pre>
*/
public class Pr52k extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR52K");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(28).isAPartOf(wsaaPrimaryKey, 8, FILLER).init(SPACES);

	private FixedLengthStringData wsaaPayrKey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaPayrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrKey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrKey, 8);
		/* WSAA-REFUND-BASIS */
	private static final String wsaaRefundPrem = "P";
	private static final String wsaaRefundUnit = "U";
		/* WSAA-MESSAGES */
	private FixedLengthStringData wsaaMsgNoUnit = new FixedLengthStringData(30).init(" ");
	private FixedLengthStringData wsaaMsgPremOnly = new FixedLengthStringData(30).init(" ");

	private FixedLengthStringData wsaaTrancd = new FixedLengthStringData(4);
	private Validator spTopUp = new Validator(wsaaTrancd, "T679");
	private Validator spTopUpSa = new Validator(wsaaTrancd, "TA69");

	private FixedLengthStringData wsaaTr52mItem = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr52mBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaTr52mItem, 0);
	private FixedLengthStringData wsaaTr52mCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52mItem, 4);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
		/* ERRORS */
	private static final String rgds = "RGDS";
	private static final String rlch = "RLCH";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String t6661 = "T6661";
	private static final String tr386 = "TR386";
	private static final String tr52m = "TR52M";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String clrfrec = "CLRFREC   ";
	private static final String cltsrec = "CLTSREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String hitsrec = "HITSREC   ";
	private static final String payrrec = "PAYRREC   ";
	private static final String utrssurrec = "UTRSSURREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
//	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private UtrssurTableDAM utrssurIO = new UtrssurTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T6661rec t6661rec = new T6661rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tr52mrec tr52mrec = new Tr52mrec();
	private Atmodrec atmodrec = new Atmodrec();
	private Sr52kScreenVars sv = ScreenProgram.getScreenVars( Sr52kScreenVars.class);
	private WsaaTransactionAreaInner wsaaTransactionAreaInner = new WsaaTransactionAreaInner();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End

	
	boolean ctcnl002Permission = false; // ICIL-93
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		a100CallPtrnrev, 
		a100Exit
	}

	public Pr52k() {
		super();
		screenVars = sv;
		new ScreenModel("Sr52k", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(cntDteFlag)	{
					sv.iljCntDteFlag.set("Y");
				} else {
					sv.iljCntDteFlag.set("N");
				}
		//ILJ-49 End 
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaItem.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		/*    Set Effective date to today by using DATCON1*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		
		ctcnl002Permission  = FeaConfg.isFeatureExist("2", "CTCNL002", appVars, "IT");     // ICIL-93
		/* Retrieve contract fields from I/O module*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		sv.cownnum.set(chdrlnbIO.getCownnum());
		sv.payrnum.set(chdrlnbIO.getPayrnum());
		sv.agntnum.set(chdrlnbIO.getAgntnum());
		sv.occdate.set(chdrlnbIO.getOccdate());
		sv.effdate.set(wsaaToday);
		if(ctcnl002Permission){
			if(!isEQ(wsspcomn.occdate,varcom.vrcmMaxDate)){
				sv.effdate.set(wsspcomn.occdate);
			}	
		}
		sv.effdate.set(chdrlnbIO.getOccdate());
		/* Get rest of details to display on screen*/
		readT66611200();
		readTr3861300();
		readTr52m1400();
		clientDetails1600();
		agentDetails1700();
		payerDetails1800();
		otherDetails1900();
	}

protected void readT66611200()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
	}


protected void readTr3861300()
	{
		/*--- Read TR386 table to get screen literals*/
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
	}


protected void readTr52m1400()
	{
		/* Read TR52M for Taxcode*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52m);
		wsaaTr52mBatctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaTr52mCnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaTr52mItem);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52m);
			wsaaTr52mBatctrcde.set(wsaaBatckey.batcBatctrcde);
			wsaaTr52mCnttype.set("***");
			itemIO.setItemitem(wsaaTr52mItem);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52mrec.tr52mRec.set(itemIO.getGenarea());
		sv.ratebas.set(tr52mrec.ratebas);
		if (isNE(tr52mrec.fieldEditCode, "Y")) {
			sv.ratebasOut[varcom.pr.toInt()].set("Y");
		}
		/*  begin on the UTRS*/
		utrssurIO.setParams(SPACES);
		utrssurIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		utrssurIO.setChdrnum(chdrlnbIO.getChdrnum());
		utrssurIO.setPlanSuffix(ZERO);
		utrssurIO.setFormat(utrssurrec);
		utrssurIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, utrssurIO);
		if (isNE(utrssurIO.getStatuz(), varcom.oK)
		&& isNE(utrssurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrssurIO.getParams());
			syserrrec.statuz.set(utrssurIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrnum(), utrssurIO.getChdrnum())
		|| isNE(chdrlnbIO.getChdrcoy(), utrssurIO.getChdrcoy())
		|| isEQ(utrssurIO.getStatuz(), varcom.endp)) {
			hitsIO.setParams(SPACES);
			hitsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			hitsIO.setChdrnum(chdrlnbIO.getChdrnum());
			hitsIO.setPlanSuffix(ZERO);
			hitsIO.setFormat(hitsrec);
			hitsIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, hitsIO);
			if (isNE(hitsIO.getStatuz(), varcom.oK)
			&& isNE(hitsIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(hitsIO.getParams());
				syserrrec.statuz.set(hitsIO.getStatuz());
				fatalError600();
			}
			if (isNE(chdrlnbIO.getChdrnum(), hitsIO.getChdrnum())
			|| isNE(chdrlnbIO.getChdrcoy(), hitsIO.getChdrcoy())
			|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
				sv.ratebas.set(wsaaRefundPrem);
				sv.ratebasOut[varcom.pr.toInt()].set("Y");
			}
		}
		if (isEQ(sv.ratebas, "U")) {
			a100CheckSpTrans();
		}

	}


protected void clientIo1500()
	{
		/*START*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			wsspcomn.longconfname.set(SPACES);
		}
		else {
			plainname();
		}
		/*EXIT*/
	}

protected void clientDetails1600()
	{
		/*START*/
		/* Get contract owners name*/
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(chdrlnbIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		clientIo1500();
		sv.ownername.set(wsspcomn.longconfname);
		/*EXIT*/
	}

protected void agentDetails1700()
	{
		/* Get Agent details*/
		aglflnbIO.setParams(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(chdrlnbIO.getAgntnum());
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)
		&& isNE(aglflnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(aglflnbIO.getStatuz(), varcom.mrnf)) {
			sv.agentname.set(SPACES);
		}
		else {
			cltsIO.setParams(SPACES);
			cltsIO.setClntnum(aglflnbIO.getClntnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFormat(cltsrec);
			clientIo1500();
			sv.agentname.set(wsspcomn.longconfname);
		}
	}

protected void payerDetails1800()
	{
		/* Get contract Paid-to date from PAYR file*/
		payrIO.setParams(SPACES);
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		payrIO.setPayrseqno(ZERO);
		payrIO.setFunction(varcom.begn);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Check PAYR record read is correct for contract*/
		if (isNE(chdrlnbIO.getChdrnum(), payrIO.getChdrnum())
		|| isNE(chdrlnbIO.getChdrcoy(), payrIO.getChdrcoy())
		|| isEQ(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Set up billing information on screen*/
		sv.billfreq.set(payrIO.getBillfreq());
		sv.mop.set(payrIO.getBillchnl());
		/* Set up Paid-to and Billed-to dates on screen*/
		sv.btdate.set(payrIO.getBtdate());
		sv.ptdate.set(payrIO.getPtdate());
		/* Get contract Payers name from client role file using the*/
		/*  CLRF logical view - Client roles by foreign key*/
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaPayrChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaPayrKey);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		clrfIO.setFormat(clrfrec);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			syserrrec.statuz.set(clrfIO.getStatuz());
			fatalError600();
		}
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(clrfIO.getClntnum());
		cltsIO.setClntcoy(clrfIO.getClntcoy());
		cltsIO.setClntpfx(clrfIO.getClntpfx());
		clientIo1500();
		sv.payrnum.set(clrfIO.getClntnum());
		sv.payorname.set(wsspcomn.longconfname);
	}

protected void otherDetails1900()
	{
		/* Get Contract type description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t5688);
		wsaaItem.set(chdrlnbIO.getCnttype());
		getDescriptions1950();
		sv.ctypdesc.set(descIO.getLongdesc());
		/* Get Contract Risk status description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3623);
		wsaaItem.set(chdrlnbIO.getStatcode());
		getDescriptions1950();
		sv.rstate.set(descIO.getShortdesc());
		/* Get Premium status description.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3588);
		wsaaItem.set(chdrlnbIO.getPstatcode());
		getDescriptions1950();
		sv.pstate.set(descIO.getShortdesc());
		/* Get Screen title from transaction we are processing*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		wsaaItem.set(wsaaBatckey.batcBatctrcde);
		getDescriptions1950();
		sv.descrip.set(descIO.getLongdesc());
	}


protected void getDescriptions1950()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaItem);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setDescitem(wsaaItem);
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}

	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/* Check if KILL pressed*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/* Check if refresh pressed*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.ratebas, wsaaRefundUnit)
		&& isNE(sv.ratebas, wsaaRefundPrem)) {
			sv.ratebasErr.set(rgds);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		wsspcomn.clonekey.set(sv.ratebas);
		if (isEQ(sv.ratebas, wsaaRefundUnit)) {
			return ;
		}
		atmodrec.atmodRec.set(SPACES);
		wsaaChdrnum.set(chdrlnbIO.getChdrnum());
		wsaaTransactionAreaInner.wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionAreaInner.wsaaTodate.set(sv.effdate);
		wsaaTransactionAreaInner.wsaaUser.set(varcom.vrcmUser);
		wsaaTransactionAreaInner.wsaaTranDate.set(varcom.vrcmDate);
		wsaaTransactionAreaInner.wsaaTranTime.set(varcom.vrcmTime);
		wsaaTransactionAreaInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransactionAreaInner.wsaaSupflag.set("N");
		wsaaTransactionAreaInner.wsaaSuppressTo.set(varcom.vrcmMaxDate);
		wsaaTransactionAreaInner.wsaaPlnsfx.set(ZERO);
		wsaaTransactionAreaInner.wsaaBbldat.set(ZERO);
		wsaaTransactionAreaInner.wsaaTranNum.set(1);
		wsaaTransactionAreaInner.wsaaCfiafiTranCode.set(t6661rec.trcode);
		wsaaTransactionAreaInner.wsaaCfiafiTranno.set(1);
		wsaaTransactionAreaInner.wsaaFlag.set(wsspcomn.flag);
		atmodrec.batchKey.set(wsspcomn.batchkey);
		atmodrec.language.set(wsspcomn.language);
		atmodrec.company.set(wsspcomn.company);
		atmodrec.primaryKey.set(wsaaPrimaryKey);
		atmodrec.transArea.set(wsaaTransactionAreaInner.wsaaTransactionArea);
		atmodrec.statuz.set("****");
		/*  call REVGENAT module*/
		callProgram(Revgenat.class, atmodrec.atmodRec);
		if (isNE(atmodrec.statuz, varcom.oK)) {
			syserrrec.params.set(atmodrec.atmodRec);
			syserrrec.statuz.set(atmodrec.statuz);
			fatalError600();
		}
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/*  Softlock the contract again here*/
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.function.set("LOCK");
		sftlockrec.enttyp.set(chdrlnbIO.getChdrpfx());
		sftlockrec.company.set(chdrlnbIO.getChdrcoy());
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}

	}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a100CheckSpTrans()
 {
		List<Ptrnpf> ptrnrevList = ptrnpfDAO.searchPtrnrevData(wsspcomn.company.toString(), chdrlnbIO.getChdrnum()
				.toString());
		if (ptrnrevList != null && !ptrnrevList.isEmpty()) {
			for (Ptrnpf p : ptrnrevList) {
				wsaaTrancd.set(p.getBatctrcde());
				if ((spTopUp.isTrue() || spTopUpSa.isTrue()) && isEQ(tr52mrec.basind, "1")) {
					scrnparams.errorCode.set(rlch);
				}
			}
		}
	}
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-AREA--INNER
 */
private static final class WsaaTransactionAreaInner { 

	private FixedLengthStringData wsaaTransactionArea = new FixedLengthStringData(200);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransactionArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransactionArea, 45).setUnsigned();
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransactionArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransactionArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 62).setUnsigned();
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 70);
	private FixedLengthStringData filler = new FixedLengthStringData(129).isAPartOf(wsaaTransactionArea, 71, FILLER).init(SPACES);
}
}
