package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:57
 * Description:
 * Copybook name: BRKOUTREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Brkoutrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData outRec = new FixedLengthStringData(23);
  	public FixedLengthStringData brkChdrcoy = new FixedLengthStringData(1).isAPartOf(outRec, 0);
  	public FixedLengthStringData brkChdrnum = new FixedLengthStringData(8).isAPartOf(outRec, 1);
  	public PackedDecimalData brkOldSummary = new PackedDecimalData(4, 0).isAPartOf(outRec, 9);
  	public PackedDecimalData brkNewSummary = new PackedDecimalData(4, 0).isAPartOf(outRec, 12);
  	public FixedLengthStringData brkStatuz = new FixedLengthStringData(4).isAPartOf(outRec, 15);
  	public FixedLengthStringData brkBatctrcde = new FixedLengthStringData(4).isAPartOf(outRec, 19);


	public void initialize() {
		COBOLFunctions.initialize(outRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		outRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}