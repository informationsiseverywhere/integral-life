/*
 * File: Loanpymt.java
 * Date: 29 August 2009 22:58:40
 * Author: Quipoz Limited
 * 
 * Class transformed from LOANPYMT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Addacmv;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Addacmvrec;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Rldgkey;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.dataaccess.AcblclmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              LOAN PAYMENTS SUBROUTINE.
*              =========================
*
* Overview.
* ---------
*
* This routine is called from within the Cash receipts subsystem
*  and from the various Claims subsystems.
* Its function is to post cash received against any Loan(s) the
*  contract may have.
* It may pay off Loan principal or Loan interest - it all depends
*  on what Sub-account Code & Type are passed in - thus sites can
*  dicate whether Loan principal or interest should be paid-off
*  first/if at all.
* IF the whole balance of an ACBL is paid-off, then the program
*  will check whether the Loan has any debt left outstanding on
*  it - ie check whether the Principal and Interest ACBLs are
*  both zero, and if so, the Loan is deemed to be Paid off and so
*  the program will V/Flag '2' the Loan record
* It will try and post as much of the Amount that is passed in as
* possible - depending on the Function specified, it may post any
* residual cash to Suspense or it may just return the Residual
* amount to the calling program.
*
* Given:     The CASHEDREC copybook, but we will only use the
*             following fields...
*              CSHD-SACSCODE, CSHD-SACSTYP, CSHD-ORIGAMT,
*              CSHD-ORIGCCY, CSHD-GENLKEY, CSHD-FUNCTION,
*              CSHD-CHDRNUM, CSHD-CHDRCOY, CSHD-SIGN.
*
*
* Gives:     CSHD-STATUZ, CSHD-DOCAMT ( this is the
*                                            residual ....
*                                            the cash left over )
*
* Processing.
* -----------
*
* Do a BEGN on the ACBLCLM file using the Company,
*  Contract number, Sub-account Code and Sub-account type
*  specified in the linkage area
*
*
*    IF CSHD-ORIGAMT  >=  ACBLCLM balance,
*       Write ACMV record for ACBLCLM balance using ACBLCLM key
*       Check whether Loan has been totally paid-off, and if so,
*         Update the LOAN record and Validflag '2' it
*       Subtract ACBLCLM balance from CSHD-ORIGAMT
*
*    ELSE
*       Write ACMV record for amount using key from ACBLCLM
*       set Residual amount to Zero
*       EXIT subroutine
*    END IF
*
* Read Next ACBLCLM and process as above.
*
*    IF no more ACBLCLM records and CSHD-AMOUNT still > Zero
*
*        IF CSHD-FUNCTION =  UPDAT
*           Post Amount left ( ie Residual ) to Suspense account
*
*        ELSE
*           Set Residual = Amount left
*           EXIT subroutine
*        END IF
*    END IF
*
*********
* NOTE !!
*********
*      This program uses T5645 and therefore expects to find
*       entries for the following....
*
*     entry No 1 .... Cash Suspense
*
*     entries 2 & 3   Loan Principal & Loan interest for Policy
*                      Loans
*
*     entries 4 & 5   Loan Principal & Loan interest for
*                      Non-forfeiture APLs ( Automatic Policy
*                                                      Loans )
*
*
*****************************************************************
* </pre>
*/
public class Loanpymt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "LOANPYMT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlPrefix = new FixedLengthStringData(2).isAPartOf(wsaaGenlkey, 0);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLoanno = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaTurnOffLoan = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaAcblSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcblSacstype = new FixedLengthStringData(2);

	protected FixedLengthStringData wsaaReadRldgacct = new FixedLengthStringData(16);
	protected FixedLengthStringData wsaaReadChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaReadRldgacct, 0);
	protected FixedLengthStringData wsaaReadLoanno = new FixedLengthStringData(2).isAPartOf(wsaaReadRldgacct, 8);
	protected FixedLengthStringData wsaaReadXtra = new FixedLengthStringData(6).isAPartOf(wsaaReadRldgacct, 10);
	private PackedDecimalData wsaaResidual = new PackedDecimalData(15, 2);
	protected PackedDecimalData wsaaAmount = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaPostAmount = new PackedDecimalData(17, 2);
	protected FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3);
	private static final int wsaaMaxDate = 99999999;
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(4, 0).setUnsigned();
	private String wsaaSkip = "N";
	private PackedDecimalData wsaaCount = new PackedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();

		/* WSAA-KEYS */
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
		/* FORMATS */
	private static final String acblrec = "ACBLREC   ";
	private static final String acblclmrec = "ACBLCLMREC";
	private static final String itemrec = "ITEMREC   ";
	private static final String loanrec = "LOANREC   ";
	private static final String chdrlifrec = "CHDRLIFREC";
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t5645 = "T5645";
		/* ERRORS */
	private static final String g418 = "G418";
	private AcblTableDAM acblIO = new AcblTableDAM();
	protected AcblclmTableDAM acblclmIO = new AcblclmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Rdockey wsaaRdockey = new Rdockey();
	private Rldgkey wsaaRldgkey = new Rldgkey();
	protected Varcom varcom = new Varcom();
	private Addacmvrec addacmvrec = new Addacmvrec();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();
	protected Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Syserrrec syserrrec = new Syserrrec();
	private T3629rec t3629rec = new T3629rec();
	protected T5645rec t5645rec = new T5645rec();
	protected Cashedrec cashedrec = new Cashedrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		reRead5120
	}

	public Loanpymt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		cashedrec.cashedRec = convertAndSetParam(cashedrec.cashedRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
			start100();
			exit190();
		}

protected void start100()
	{
		initialise1000();
		if (isEQ(wsaaSkip,"Y")) {
			return ;
		}
		mainProcessing2000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		cashedrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaSkip = "N";
		wsaaGenlkey.set(SPACES);
		wsaaCurrency.set(SPACES);
		wsaaRldgacct.set(SPACES);
		wsaaReadRldgacct.set(SPACES);
		wsaaAcblSacscode.set(SPACES);
		wsaaAcblSacstype.set(SPACES);
		wsaaTurnOffLoan.set(SPACES);
		wsaaAmount.set(ZERO);
		wsaaPostAmount.set(ZERO);
		wsaaAmount.set(cashedrec.origamt);
		wsaaCurrency.set(cashedrec.origccy);
		wsaaRdockey.rdocKey.set(cashedrec.doctkey);
		varcom.vrcmTranid.set(cashedrec.tranid);
		wsaaBatckey.batcKey.set(cashedrec.batchkey);
		wsaaRldgkey.rldgKey.set(cashedrec.trankey);
		wsaaGenlkey.set(cashedrec.genlkey);
		wsaaSequenceNo.set(ZERO);
		wsaaSequenceNo.set(cashedrec.transeq);
		wsaaCount.set(ZERO);
		wsaaNominalRate.set(ZERO);
		wsaaLedgerCcy.set(SPACES);
		/* read T5645 for use later on, whether for posting back to*/
		/*  suspense, or to check whether LOANs have been paid off or not*/
		readT56457000();
		/* Read Contract Header file to get the Contract Type for       */
		/* substitution.                                                */
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(cashedrec.chdrcoy);
		chdrlifIO.setChdrnum(cashedrec.chdrnum);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			databaseError99500();
		}
	}

protected void mainProcessing2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* Read ACBL for suspense balance.*/
		acblclmIO.setParams(SPACES);
		wsaaRldgChdrnum.set(cashedrec.chdrnum);
		wsaaRldgLoanno.set("00");
		acblclmIO.setRldgacct(wsaaRldgacct);
		acblclmIO.setRldgcoy(cashedrec.chdrcoy);
		acblclmIO.setOrigcurr(SPACES);
		acblclmIO.setSacscode(cashedrec.sacscode);
		acblclmIO.setSacstyp(cashedrec.sacstyp);
		acblclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acblclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acblclmIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "SACSTYP");
		acblclmIO.setFormat(acblclmrec);
		while ( !(isEQ(acblclmIO.getStatuz(),varcom.endp))) {
			processAcbls3000();
		}
		
		endProcessing6000();
		/* Return the Jrnseq count from the ACMV reocrds written to the*/
		/*  calling program.*/
		cashedrec.transeq.set(wsaaSequenceNo);
	}

protected void processAcbls3000()
	{
			start3000();
		}

protected void start3000()
	{
		SmartFileCode.execute(appVars, acblclmIO);
		if (isNE(acblclmIO.getStatuz(),varcom.oK)
		&& isNE(acblclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acblclmIO.getParams());
			syserrrec.statuz.set(acblclmIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(acblclmIO.getStatuz(),varcom.endp)) {
			return ;
		}
		/* we have definitely got an ACBL record, but is it OK ???*/
		wsaaReadRldgacct.set(acblclmIO.getRldgacct());
		if (isNE(acblclmIO.getRldgcoy(),cashedrec.chdrcoy)
		|| isNE(acblclmIO.getSacscode(),cashedrec.sacscode)
		|| isNE(acblclmIO.getSacstyp(),cashedrec.sacstyp)
		|| isNE(wsaaReadChdrnum,cashedrec.chdrnum)
		|| isEQ(wsaaReadLoanno,SPACES)
		|| isNE(wsaaReadXtra,SPACES)) {
			acblclmIO.setStatuz(varcom.endp);
			return ;
		}
		/* Check that the ACBL balance is not Zero*/
		if (isEQ(acblclmIO.getSacscurbal(),ZERO)) {
			checkActiveLoan9000();
			acblclmIO.setFunction(varcom.nextr);
			return ;
		}
		/* Negate the Account balances if it is negative (for the       */
		/* case of Cash accounts).                                      */
		if (isLT(acblclmIO.getSacscurbal(),0)) {
			setPrecision(acblclmIO.getSacscurbal(), 2);
			acblclmIO.setSacscurbal(mult(acblclmIO.getSacscurbal(),(-1)));
		}
		/* Get here so the ACBL we have read is OK*/
		/*  check that ACBL currency matches the currency of the amount*/
		/*  passed in in the CSHD-ORIGCCY field - if not, we must convert*/
		/*  the passed in amount, CSHD-ORIGCCY to the ACBL currency.*/
		if (isNE(wsaaCurrency,acblclmIO.getOrigcurr())) {
			/* When currency conversion used posting should be done*/
			/* before and after.*/
			lifacmvrec.lifacmvRec.set(SPACES);
			lifacmvrec.origamt.set(wsaaAmount);
			lifacmvrec.origcurr.set(wsaaCurrency);
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			currencyPosting11000();
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.currOut.set(acblclmIO.getOrigcurr());
			convertCurrency4000();
			wsaaCurrency.set(acblclmIO.getOrigcurr());
			wsaaAmount.set(conlinkrec.amountOut);
			lifacmvrec.lifacmvRec.set(SPACES);
			lifacmvrec.origamt.set(wsaaAmount);
			lifacmvrec.origcurr.set(wsaaCurrency);
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			currencyPosting11000();
		}
		/* Now values are in correct currencies so lets do our sums*/
		if (isGT(wsaaAmount,acblclmIO.getSacscurbal())
		|| isEQ(wsaaAmount,acblclmIO.getSacscurbal())) {
			wsaaPostAmount.set(acblclmIO.getSacscurbal());
			post5000();
			/* we have paid off the whole of the ACBL balance, so we will go*/
			/*  and see whether the Loan is still active or whether all debts*/
			/*  have now been cleared*/
			checkActiveLoan9000();
			compute(wsaaAmount, 2).set(sub(wsaaAmount,acblclmIO.getSacscurbal()));
			acblclmIO.setFunction(varcom.nextr);
		}
		else {
			wsaaPostAmount.set(wsaaAmount);
			post5000();
			wsaaAmount.set(ZERO);
			acblclmIO.setStatuz(varcom.endp);
		}
	}

protected void convertCurrency4000()
	{
		start4000();
	}

protected void start4000()
	{
		/* call the convert subroutine*/
		conlinkrec.amountIn.set(wsaaAmount);
		conlinkrec.statuz.set(SPACES);
		conlinkrec.function.set("REAL");
		conlinkrec.currIn.set(wsaaCurrency);
		conlinkrec.cashdate.set(wsaaMaxDate);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(cashedrec.chdrcoy);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			systemError99000();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void post5000()
	{
		start5000();
	}

protected void start5000()
	{
		addacmvrec.addacmvRec.set(SPACES);
		addacmvrec.contot.set(0);
		addacmvrec.termid.set(varcom.vrcmTermid);
		addacmvrec.user.set(varcom.vrcmUser);
		addacmvrec.transactionDate.set(varcom.vrcmDate);
		addacmvrec.transactionTime.set(varcom.vrcmTime);
		addacmvrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		addacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		addacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		addacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		addacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		addacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		addacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		addacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		addacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		addacmvrec.rdocnum.set(wsaaRdockey.rdocRdocnum);
		addacmvrec.rldgcoy.set(wsaaRldgkey.rldgRldgcoy);
		addacmvrec.rldgacct.set(wsaaReadRldgacct);
		addacmvrec.genlcoy.set(wsaaGlCompany);
		addacmvrec.glcode.set(wsaaGlMap);
		addacmvrec.sacscode.set(cashedrec.sacscode);
		addacmvrec.sacstyp.set(cashedrec.sacstyp);
		addacmvrec.glsign.set(cashedrec.sign);
		wsaaSequenceNo.add(1);
		addacmvrec.transeq.set(wsaaSequenceNo);
		addacmvrec.effdate.set(cashedrec.trandate);
		addacmvrec.trandesc.set(cashedrec.trandesc);
		addacmvrec.origamt.set(wsaaPostAmount);
		addacmvrec.origccy.set(wsaaCurrency);
		addacmvrec.acctccy.set(cashedrec.acctccy);
		/* Make sure that if posting currency is different to Gen Ledger*/
		/*  currency that the conversions are made properly.*/
		getGlCurrency5100();
		if (isEQ(cashedrec.function,"UPDAT")) {
			addacmvrec.tranno.set(ZERO);
		}
		else {
			addacmvrec.tranno.set(cashedrec.tranno);
		}
		addacmvrec.rdocpfx.set("CA");
		addacmvrec.creddte.set(varcom.vrcmMaxDate);
		addacmvrec.chdrstcda.set(chdrlifIO.getCnttype());
		addacmvrec.function.set(SPACES);
		callProgram(Addacmv.class, addacmvrec.addacmvRec);
		if (isNE(addacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(addacmvrec.addacmvRec);
			syserrrec.statuz.set(addacmvrec.statuz);
			systemError99000();
		}
	}

protected void getGlCurrency5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5100();
				case reRead5120: 
					reRead5120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5100()
	{
		/* Use posting currency to Read T3629 Currency table to get*/
		/*  General Ledger currency & conversion rate to the GL currency.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(addacmvrec.batccoy);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(wsaaCurrency);
	}

protected void reRead5120()
	{
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(ZERO);
		wsaaCount.set(1);
		while ( !(isNE(wsaaNominalRate,ZERO)
		|| isGT(wsaaCount,7))) {
			findRate5200();
		}
		
		if (isEQ(wsaaNominalRate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.reRead5120);
			}
			else {
				syserrrec.statuz.set(g418);
				systemError99000();
			}
		}
		/* get here so we have a conversion rate so lets calculate*/
		/*  the ADDA-ACCTAMT*/
		addacmvrec.crate.set(wsaaNominalRate);
		addacmvrec.genlcur.set(wsaaLedgerCcy);
		compute(addacmvrec.acctamt, 9).set(mult(wsaaPostAmount,wsaaNominalRate));
	}

protected void findRate5200()
	{
		/*START*/
		if (isGTE(addacmvrec.effdate,t3629rec.frmdate[wsaaCount.toInt()])
		&& isLTE(addacmvrec.effdate,t3629rec.todate[wsaaCount.toInt()])) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaCount.toInt()]);
		}
		else {
			wsaaCount.add(1);
		}
		/*EXIT*/
	}

protected void endProcessing6000()
	{
		start6000();
	}

protected void start6000()
	{
		if (isGT(wsaaAmount,ZERO)) {
			if (isEQ(cashedrec.function,"UPDAT")) {
				/*            we will credit any leftover cash to the suspense*/
				/*            account - but first convert it to the Original*/
				/*            currency passed in*/
				if (isNE(wsaaCurrency,cashedrec.origccy)) {
					lifacmvrec.lifacmvRec.set(SPACES);
					lifacmvrec.origamt.set(wsaaAmount);
					lifacmvrec.origcurr.set(wsaaCurrency);
					lifacmvrec.sacscode.set(t5645rec.sacscode06);
					lifacmvrec.sacstyp.set(t5645rec.sacstype06);
					lifacmvrec.glcode.set(t5645rec.glmap06);
					lifacmvrec.glsign.set(t5645rec.sign06);
					currencyPosting11000();
					conlinkrec.clnk002Rec.set(SPACES);
					conlinkrec.currOut.set(cashedrec.origccy);
					convertCurrency4000();
					wsaaAmount.set(conlinkrec.amountOut);
					wsaaCurrency.set(cashedrec.origccy);
					lifacmvrec.lifacmvRec.set(SPACES);
					lifacmvrec.origamt.set(wsaaAmount);
					lifacmvrec.origcurr.set(wsaaCurrency);
					lifacmvrec.sacscode.set(t5645rec.sacscode07);
					lifacmvrec.sacstyp.set(t5645rec.sacstype07);
					lifacmvrec.glcode.set(t5645rec.glmap07);
					lifacmvrec.glsign.set(t5645rec.sign07);
					currencyPosting11000();
				}
				creditSuspense8000();
				cashedrec.docamt.set(ZERO);
			}
			else {
				/* return amount left over in the currency it was passed in as*/
				if (isNE(wsaaCurrency,cashedrec.origccy)) {
					lifacmvrec.lifacmvRec.set(SPACES);
					lifacmvrec.origamt.set(wsaaAmount);
					lifacmvrec.origcurr.set(wsaaCurrency);
					lifacmvrec.sacscode.set(t5645rec.sacscode06);
					lifacmvrec.sacstyp.set(t5645rec.sacstype06);
					lifacmvrec.glcode.set(t5645rec.glmap06);
					lifacmvrec.glsign.set(t5645rec.sign06);
					currencyPosting11000();
					conlinkrec.currOut.set(cashedrec.origccy);
					convertCurrency4000();
					cashedrec.docamt.set(conlinkrec.amountOut);
					lifacmvrec.lifacmvRec.set(SPACES);
					lifacmvrec.origamt.set(conlinkrec.amountOut);
					lifacmvrec.origcurr.set(conlinkrec.currOut);
					lifacmvrec.sacscode.set(t5645rec.sacscode07);
					lifacmvrec.sacstyp.set(t5645rec.sacstype07);
					lifacmvrec.glcode.set(t5645rec.glmap07);
					lifacmvrec.glsign.set(t5645rec.sign07);
					currencyPosting11000();
				}
				else {
					cashedrec.docamt.set(wsaaAmount);
				}
			}
		}
		else {
			cashedrec.docamt.set(ZERO);
		}
	}

protected void readT56457000()
	{
		start7000();
	}

protected void start7000()
	{
		/* we are going to post residual cash to suspense, so read T5645*/
		/*  to get Subaccount code & type etc...*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(cashedrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void creditSuspense8000()
	{
		start8000();
	}

protected void start8000()
	{
		addacmvrec.addacmvRec.set(SPACES);
		addacmvrec.contot.set(0);
		addacmvrec.termid.set(varcom.vrcmTermid);
		addacmvrec.user.set(varcom.vrcmUser);
		addacmvrec.transactionDate.set(varcom.vrcmDate);
		addacmvrec.transactionTime.set(varcom.vrcmTime);
		addacmvrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		addacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		addacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		addacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		addacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		addacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		addacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		addacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		addacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		addacmvrec.rdocnum.set(wsaaRdockey.rdocRdocnum);
		addacmvrec.rldgcoy.set(wsaaRldgkey.rldgRldgcoy);
		addacmvrec.rldgacct.set(wsaaRldgkey.rldgRldgacct);
		addacmvrec.genlcoy.set(wsaaGlCompany);
		addacmvrec.genlcur.set(wsaaGlCurrency);
		/* MOVE WSAA-GL-MAP            TO ADDA-GLCODE.                  */
		addacmvrec.glcode.set(t5645rec.glmap01);
		addacmvrec.sacscode.set(t5645rec.sacscode01);
		addacmvrec.sacstyp.set(t5645rec.sacstype01);
		addacmvrec.glsign.set(t5645rec.sign01);
		wsaaSequenceNo.add(1);
		addacmvrec.transeq.set(wsaaSequenceNo);
		addacmvrec.effdate.set(cashedrec.trandate);
		addacmvrec.trandesc.set(cashedrec.trandesc);
		addacmvrec.origamt.set(wsaaAmount);
		addacmvrec.acctamt.set(ZERO);
		addacmvrec.origccy.set(wsaaCurrency);
		addacmvrec.acctccy.set(cashedrec.acctccy);
		addacmvrec.crate.set(cashedrec.dissrate);
		if (isNE(cashedrec.tranno,NUMERIC)) {
			cashedrec.tranno.set(0);
		}
		if (isEQ(cashedrec.function,"UPDAT")) {
			addacmvrec.tranno.set(ZERO);
		}
		else {
			addacmvrec.tranno.set(cashedrec.tranno);
		}
		addacmvrec.rdocpfx.set("CA");
		addacmvrec.creddte.set(varcom.vrcmMaxDate);
		addacmvrec.chdrstcda.set(chdrlifIO.getCnttype());
		addacmvrec.function.set(SPACES);
		callProgram(Addacmv.class, addacmvrec.addacmvRec);
		if (isNE(addacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(addacmvrec.addacmvRec);
			syserrrec.statuz.set(addacmvrec.statuz);
			systemError99000();
		}
	}

protected void checkActiveLoan9000()
	{
			start9000();
		}

protected void start9000()
	{
		/* Read LOAN record*/
		wsaaTurnOffLoan.set(SPACES);
		loanIO.setParams(SPACES);
		loanIO.setChdrcoy(acblclmIO.getRldgcoy());
		loanIO.setChdrnum(wsaaReadChdrnum);
		loanIO.setLoanNumber(wsaaReadLoanno);
		loanIO.setFormat(loanrec);
		/*    MOVE READH                  TO LOAN-FUNCTION.                */
		loanIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(),varcom.oK)
		&& isNE(loanIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(loanIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		/* get here so we have read & locked the LOAN record*/
		/* we need to get the Loan type so that we can get the correct*/
		/*  entries from T5645.*/
		/* we know that when we get here we have just zeroised one of the*/
		/*  2 subaccount types that is associated with a Loan, whether it*/
		/*  be a Policy loan or an APL - so we need to check which one we*/
		/*  have just zeroised, and then read the ACBL balance for the*/
		/*  other one and it that one is also zero, then the Loan has been*/
		/*  paid off and we can Validflag '2' the Loan record.*/
		if (isEQ(loanIO.getLoanType(),"P")) {
			/* check which of the 2 policy loan ACBLs we have just cleared and*/
			/*  then go and see if the other one is also zero*/
			if (isEQ(acblclmIO.getSacscode(),t5645rec.sacscode02)
			&& isEQ(acblclmIO.getSacstyp(),t5645rec.sacstype02)) {
				wsaaAcblSacscode.set(t5645rec.sacscode03);
				wsaaAcblSacstype.set(t5645rec.sacstype03);
				checkAccBalance10000();
			}
			if (isEQ(acblclmIO.getSacscode(),t5645rec.sacscode03)
			&& isEQ(acblclmIO.getSacstyp(),t5645rec.sacstype03)) {
				wsaaAcblSacscode.set(t5645rec.sacscode02);
				wsaaAcblSacstype.set(t5645rec.sacstype02);
				checkAccBalance10000();
			}
		}
		if (isEQ(loanIO.getLoanType(),"A")) {
			/* check which of the 2 APL loan ACBLs we have just cleared and*/
			/*  then go and see if the other one is also zero*/
			if (isEQ(acblclmIO.getSacscode(),t5645rec.sacscode04)
			&& isEQ(acblclmIO.getSacstyp(),t5645rec.sacstype04)) {
				wsaaAcblSacscode.set(t5645rec.sacscode05);
				wsaaAcblSacstype.set(t5645rec.sacstype05);
				checkAccBalance10000();
			}
			if (isEQ(acblclmIO.getSacscode(),t5645rec.sacscode05)
			&& isEQ(acblclmIO.getSacstyp(),t5645rec.sacstype05)) {
				wsaaAcblSacscode.set(t5645rec.sacscode04);
				wsaaAcblSacstype.set(t5645rec.sacstype04);
				checkAccBalance10000();
			}
		}
		if (isEQ(loanIO.getLoanType(),"E")) {
			/* check which of the 2 APL loan ACBLs we have just cleared and    */
			/*  then go and see if the other one is also zero                  */
			if (isEQ(acblclmIO.getSacscode(),t5645rec.sacscode08)
			&& isEQ(acblclmIO.getSacstyp(),t5645rec.sacstype08)) {
				wsaaAcblSacscode.set(t5645rec.sacscode09);
				wsaaAcblSacstype.set(t5645rec.sacstype09);
				checkAccBalance10000();
			}
			if (isEQ(acblclmIO.getSacscode(),t5645rec.sacscode09)
			&& isEQ(acblclmIO.getSacstyp(),t5645rec.sacstype09)) {
				wsaaAcblSacscode.set(t5645rec.sacscode08);
				wsaaAcblSacstype.set(t5645rec.sacstype08);
				checkAccBalance10000();
			}
		}
		/*                                                         <V4L001>*/
		/* For Advance Premium Deposit, Check which of the 2 APA loan      */
		/* ACBLs we have just cleared and then go and see if the other     */
		/* one is also zero                                        <V4L001>*/
		/*                                                         <V4L001>*/
		if (isEQ(loanIO.getLoanType(),"D")) {
			if (isEQ(acblclmIO.getSacscode(),t5645rec.sacscode10)
			&& isEQ(acblclmIO.getSacstyp(),t5645rec.sacstype10)) {
				wsaaAcblSacscode.set(t5645rec.sacscode11);
				wsaaAcblSacstype.set(t5645rec.sacstype11);
				checkAccBalance10000();
			}
			if (isEQ(acblclmIO.getSacscode(),t5645rec.sacscode11)
			&& isEQ(acblclmIO.getSacstyp(),t5645rec.sacstype11)) {
				wsaaAcblSacscode.set(t5645rec.sacscode10);
				wsaaAcblSacstype.set(t5645rec.sacstype10);
				checkAccBalance10000();
			}
		}
		if (isEQ(wsaaTurnOffLoan,"Y")) {
			loanIO.setValidflag("2");
			if (isEQ(cashedrec.function,"UPDAT")) {
				loanIO.setLastTranno(ZERO);
			}
			else {
				loanIO.setLastTranno(cashedrec.tranno);
			}
			/*        MOVE REWRT              TO LOAN-FUNCTION                 */
			loanIO.setFunction(varcom.writd);
		}
		else {
			/*        MOVE RLSE               TO LOAN-FUNCTION                 */
			/*        MOVE REWRT              TO LOAN-FUNCTION         <V65L19>*/
			loanIO.setFunction(varcom.writd);
		}
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			databaseError99500();
		}
	}

protected void checkAccBalance10000()
	{
			start10000();
		}

protected void start10000()
	{
		/* Read ACBL file with given parameters*/
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(acblclmIO.getRldgcoy());
		acblIO.setRldgacct(wsaaReadRldgacct);
		acblIO.setSacscode(wsaaAcblSacscode);
		acblIO.setSacstyp(wsaaAcblSacstype);
		acblIO.setOrigcurr(acblclmIO.getOrigcurr());
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaTurnOffLoan.set("Y");
			return ;
		}
		if (isEQ(acblIO.getSacscurbal(),ZERO)) {
			wsaaTurnOffLoan.set("Y");
		}
		else {
			wsaaTurnOffLoan.set("N");
		}
	}

protected void currencyPosting11000()
	{
		start11000();
	}

protected void start11000()
	{
		lifacmvrec.contot.set(0);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.rdocnum.set(wsaaRdockey.rdocRdocnum);
		lifacmvrec.rldgcoy.set(wsaaRldgkey.rldgRldgcoy);
		lifacmvrec.rldgacct.set(wsaaReadRldgacct);
		lifacmvrec.genlcoy.set(wsaaGlCompany);
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		lifacmvrec.effdate.set(cashedrec.trandate);
		lifacmvrec.trandesc.set(cashedrec.trandesc);
		lifacmvrec.tranref.set(cashedrec.chdrnum);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.tranno.set(cashedrec.tranno);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(cashedrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(acblclmIO.getOrigcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError99000();
		}
		/*A900-EXIT*/
	}

protected void systemError99000()
	{
			start99000();
			exit99490();
		}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		cashedrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
			start99500();
			exit99590();
		}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		cashedrec.statuz.set(varcom.bomb);
		exit190();
	}
}
