package com.csc.life.contractservicing.dataaccess.dao;


import com.csc.life.contractservicing.dataaccess.model.Feddpf;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface FeddpfDAO extends BaseDAO<Feddpf>{
	public Feddpf  getbychdrnum(String chdrcoy,String chdrnum);
	public void insertFeddRecord(Feddpf fedd);
	public void updateeddRecord(Feddpf feddpf);
}
