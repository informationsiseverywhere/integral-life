package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5049
 * @version 1.0 generated on 30/08/09 06:32
 * @author Quipoz
 */
public class S5049ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(62);
	public FixedLengthStringData dataFields = new FixedLengthStringData(14).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData actionflag = DD.action.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData fuflag = DD.action.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData rfundflg = DD.rfundflg.copy().isAPartOf(dataFields,13); //ICIL-658
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 14);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData rfundflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8); //ICIL-658
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 26);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] rfundflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24); //ICIL-658
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5049screenWritten = new LongData(0);
	public LongData S5049protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5049ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rfundflgOut,new String[] {null ,null, null ,"03" , null, null, null, null, null, null, null, null}); //ICIL-658
		screenFields = new BaseData[] {chdrsel, action, rfundflg};
		screenOutFields = new BaseData[][] {chdrselOut, actionOut, rfundflgOut};
		screenErrFields = new BaseData[] {chdrselErr, actionErr, rfundflgErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S5049screen.class;
		protectRecord = S5049protect.class;
	}

}
