package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.FeddpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Feddpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.database.QPBaseDataSource;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.IntegralDBProperties;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class FeddpfDAOImpl  extends BaseDAOImpl <Feddpf> implements FeddpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(FeddpfDAOImpl.class);
	
	AppConfig appConfig = AppConfig.getInstance();  //ILIFE-6328
	String dbType = IntegralDBProperties.getType().trim(); // ILIFE-6328 //IJTI-449
		
	public void updateeddRecord(Feddpf feddpf){
   		if (feddpf != null) {
		      
			StringBuilder sql_feddpf_update = new StringBuilder();
			sql_feddpf_update.append("UPDATE feddpf SET VALIDFLAG=2 WHERE CHDRNUM=? and chdrcoy=? and chdrpfx=?");
			LOGGER.info(sql_feddpf_update.toString());
			PreparedStatement psUpdate = null;
			ResultSet rs=null;
			try
			{
				psUpdate = getPrepareStatement(sql_feddpf_update.toString());
				psUpdate.setString(1, feddpf.getChdrnum());					
				psUpdate.setString(2, feddpf.getChdrcoy());
				psUpdate.setString(3, feddpf.getChdrpfx());	
					
				 
				psUpdate.executeUpdate();	
			}
			catch (SQLException e) {
                LOGGER.error("insertFeddRecord", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psUpdate, rs);
            }
		}
   		return;
		
	}
	
  	public void insertFeddRecord(Feddpf feddpf){
   		if (feddpf != null) {
			StringBuilder sql_feddpf_insert = new StringBuilder();  //ILIFE-6328 Different attribute names used for MSSQL and Oracle.
   			if(QPBaseDataSource.DATABASE_ORACLE.equals(dbType)){  //ILIFE-6328
   				sql_feddpf_insert.append("INSERT INTO FEDDPF(CHDRPFX,CHDRCOY,CHDRNUM,STATUSCHANGEDAT,VALIDFLAG,FEDDFLAG,FPVFLAG,USRPRF,JOBNM,DATIME)");  //ILIFE-6328 
   			}  //ILIFE-6328
   			else {  //ILIFE-6328
   				sql_feddpf_insert.append("INSERT INTO FEDDPF(CHDRPFX,CHDRCOY,CHDRNUM,STATUSCHANGEDAT,VALIDFLAG,FEDDFLAG,FPVFLAG,USRPRF,JOBNM,DATIME)"); //ILIFE-7887
   			}  //ILIFE-6328
			sql_feddpf_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?)");
			LOGGER.info(sql_feddpf_insert.toString());
			PreparedStatement psInsert = null;
			ResultSet rs=null;
			try
			{
				psInsert = getPrepareStatement(sql_feddpf_insert.toString());
				    psInsert.setString(1, feddpf.getChdrpfx());		
					psInsert.setString(2, feddpf.getChdrcoy());
					psInsert.setString(3, feddpf.getChdrnum());
				//  psInsert.setBigDecimal(4, feddpf.getStat_change_dat());
					psInsert.setBigDecimal(4, feddpf.getStatuschangedat()); //ILIFE-7887
					psInsert.setString(5, feddpf.getValidflag());
					psInsert.setString(6, feddpf.getFeddflag());
					psInsert.setString(7, feddpf.getFpvflag());
					psInsert.setString(8, this.getUsrprf());
					psInsert.setString(9,this.getJobnm());
					psInsert.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
					psInsert.executeUpdate();	
			}
			catch (SQLException e) {
                LOGGER.error("insertFeddRecord", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psInsert, rs);
            }
		}
   		return;
		
	}

   public Feddpf getbychdrnum(String chdrcoy,String chdrnum){
	   
	   		//ILIFE-6293 S
	   		String sql_feddpf_select;
	   		if(QPBaseDataSource.DATABASE_ORACLE.equals(dbType)){
	   			sql_feddpf_select=new String("SELECT CHDRPFX,CHDRCOY,CHDRNUM,STATUSCHANGEDAT,VALIDFLAG,FEDDFLAG,FPVFLAG,USRPRF,JOBNM,DATIME FROM  FEDDPF  WHERE CHDRNUM=? AND CHDRCOY=?"
	      		+ "	AND VALIDFLAG=1");
	   		}
	   		else{
	   			sql_feddpf_select=new String("SELECT CHDRPFX,CHDRCOY,CHDRNUM,STATUSCHANGEDAT,VALIDFLAG,FEDDFLAG,FPVFLAG,USRPRF,JOBNM,DATIME FROM  FEDDPF  WHERE CHDRNUM=? AND CHDRCOY=?"
	   		      		+ "	AND VALIDFLAG=1"); //ILIFE-7887
	   		}
	      	//ILIFE-6293 E
			
			
			  ResultSet rs = null;
			PreparedStatement psSelect=null;
			Feddpf feddpf=null;
			
			

			try
			{
				 psSelect = getPrepareStatement(sql_feddpf_select);/* IJTI-1523 */
				    psSelect.setString(1, chdrnum);	
				    psSelect.setString(2, chdrcoy);
				   
				   
					 rs = psSelect.executeQuery();
				while(rs.next()){
					feddpf = new Feddpf();
					feddpf.setChdrpfx(rs.getString(1));
					feddpf.setChdrcoy(rs.getString(2));
					feddpf.setChdrnum(rs.getString(3));
				//  feddpf.setStat_change_dat(rs.getBigDecimal(4));
					feddpf.setStatuschangedat(rs.getBigDecimal(4)); //ILIFE-7887
					feddpf.setValidflag(rs.getString(5));
					feddpf.setFeddflag(rs.getString(6));
					feddpf.setFpvflag(rs.getString(7));
				}
				  
			}
			catch (SQLException e) {
               LOGGER.error("getbychdrnum()", e);//IJTI-1561
               throw new SQLRuntimeException(e);
           } finally {
               close(psSelect, rs);
           }
			return feddpf ;
		}
		
	}

   
   

   
		
	

