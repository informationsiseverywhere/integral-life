package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sr5a7
 * @version 1.0 generated on 22/02/17 06:29
 * @author Quipoz
 */
public class Sr5a7ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(364);
	public FixedLengthStringData dataFields = new FixedLengthStringData(204).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,126);
	public FixedLengthStringData fatcastatus=DD.fatcastatus.copy().isAPartOf(dataFields,156 );
	public FixedLengthStringData fatcastatusdesc=DD.fatcastatusdesc.copy().isAPartOf(dataFields,162);
	public FixedLengthStringData feddflag = DD.feddflag.copy().isAPartOf(dataFields,202);
	public FixedLengthStringData fpvflag  = DD.fpvflag.copy().isAPartOf(dataFields,203);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 204);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData fatcastatusErr=new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData feddflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData fpvflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 40);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] fatcastatusOut= FLSArrayPartOfStructure(12, 1, outputIndicators,84 );
	public FixedLengthStringData[] feddflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] fpvflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	
	
	
	
	public FixedLengthStringData fatcastatusFlag = new FixedLengthStringData(10);
	
	public LongData Sr5a7screenWritten = new LongData(0);
	public LongData Sr5a7protectWritten = new LongData(0);
	public boolean hasSubfile() {
		return false;
	}

	
	public Sr5a7ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(fatcastatusOut,new String[] {null,"90", null,null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(feddflagOut,new String[] {"01","02", "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fpvflagOut,new String[] {"03","04", "-03",null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, cownnum, ownername,premstatus,fatcastatus,feddflag,fpvflag};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut,premstatusOut,fatcastatusOut,feddflagOut,fpvflagOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr,premstatusErr,fatcastatusErr,feddflagErr,fpvflagErr};
		
		
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr5a7screen.class;
		protectRecord = Sr5a7protect.class;
}
	}
	
	

	