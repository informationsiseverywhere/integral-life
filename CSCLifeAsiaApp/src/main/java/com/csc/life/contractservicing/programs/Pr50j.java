/*
 * File: Pr50j.java
 * Date: 30 August 2009 1:31:34
 * Author: Quipoz Limited
 * 
 * Class transformed from PR50J.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.screens.Sr50jScreenVars;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Life Assured Changes Submenu
*
* This screen shows the action allow for user to do Life Assured
* Changes. This is part of Contract Alterations.
*
***********************************************************************
* </pre>
*/
public class Pr50j extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR50J");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaValidStatcode = new FixedLengthStringData(1);
	private Validator validStatcode = new Validator(wsaaValidStatcode, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1);
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String cltsrec = "CLTSREC";
	private static final String itemrec = "ITEMREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String lifeenqrec = "LIFEENQREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec1 = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sr50jScreenVars sv = ScreenProgram.getScreenVars( Sr50jScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();

	private T5679rec t5679rec = new T5679rec();
	private FixedLengthStringData wsaaStatFlag = new FixedLengthStringData(1);
	private Validator wsaaExitStat = new Validator(wsaaStatFlag, "Y");
	boolean CSMIN003Permission  = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2390, 
		exit2990, 
		exit3090
	}

	public Pr50j() {
		super();
		screenVars = sv;
		new ScreenModel("Sr50j", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
		if(CSMIN003Permission) {
			sv.fuflag.set("Y");
		} else {
			sv.fuflag.set("N");
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
			validateKey12210();
		}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.clttwo,SPACES)) {
			sv.clttwoErr.set(errorsInner.e186);
			return ;
		}
		if (isEQ(subprogrec.key2,"Y")) {
			if (isEQ(sv.action,"C")) {
				if (isNE(sv.clttwo,SPACES)) {
					sv.clttwoErr.set(errorsInner.rgh2);//IBPLIFE-666
					return;
				} 
			} 
			validateContract2300();
			if (isEQ(sv.action,"C")) {
				chdrmjaIO.setParams(SPACES);
				chdrmjaIO.setChdrcoy(wsspcomn.company);
				chdrmjaIO.setChdrnum(sv.chdrsel);
				chdrmjaIO.setChdrcoy(wsspcomn.company);
				chdrmjaIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, chdrmjaIO);
				if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
				&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
					syserrrec.params.set(chdrmjaIO.getParams());
					syserrrec.statuz.set(chdrmjaIO.getStatuz());
					fatalError600();
				}
				if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)) {
					sv.chdrselErr.set(errorsInner.e544);
					return;
				}
			}
			if (isNE(sv.chdrselErr,SPACES)) {
				return ;
			}
			else {
				wsspcomn.chdrChdrnum.set(sv.chdrsel);
				wsspcomn.currfrom.set(chdrlifIO.getPtdate());
			}
		}
		if (isEQ(sv.action,"A")
		&& isNE(sv.chdrsel,SPACES)) {
			sv.chdrselErr.set(errorsInner.rgh2);
		}
		if (isNE(sv.clttwo,SPACES))//IBPLIFE-2353
		{
		cltsIO.setDataKey(SPACES);
		cltsIO.setParams(SPACES);
		cltsIO.setStatuz(varcom.oK);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.clttwo);
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.clttwoErr.set(errorsInner.e058);
			wsspcomn.edterror.set("Y");
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.clnt);
		sdasancrec.entycoy.set(wsspcomn.fsuco);
		sdasancrec.entynum.set(sv.clttwo);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.clttwoErr.set(sdasancrec.statuz);
			return ;
		}
		wsspcomn.chdrChdrnum.set(sv.chdrsel);
		if (isEQ(cltsIO.getCltind(),"L")) {
			sv.clttwoErr.set(errorsInner.g499);
			return ;
		}
		if (isEQ(cltsIO.getCltind(),"D")) {
			sv.clttwoErr.set(errorsInner.h939);
			return ;
		}
		if (isEQ(cltsIO.getClttype(),"C")) {
			sv.clttwoErr.set(errorsInner.rf56);
			return ;
		}
		cltsIO.setFunction(varcom.keeps);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		}//IBPLIFE-2353
	}
	

protected void validateContract2300()
	{
		try {
			start2310();
			checkStatus2320();
			if (isNE(sv.action,"C")){
				getLifeAssured2330();
			}
			lockContract2340();
		}
		catch (GOTOException e){
		}
	}

protected void start2310()
	{
		if (isEQ(sv.chdrsel,SPACES)) {
			sv.chdrselErr.set(errorsInner.e186);
			goTo(GotoLabel.exit2390);
		}
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(wsspcomn.company);
		chdrlifIO.setChdrnum(sv.chdrsel);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)
		&& isNE(chdrlifIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			fatalError600();
		}
		if (isEQ(chdrlifIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.e544);
			goTo(GotoLabel.exit2390);
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			goTo(GotoLabel.exit2390);
		}
	}

protected void checkStatus2320()
	{
		itemIO.setParams(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.e767);
			goTo(GotoLabel.exit2390);
		}
		t5679rec1.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus.set(SPACES);
		for (ix.set(1); !(isGT(ix,12)
		|| validStatus.isTrue()); ix.add(1)){
			if (isNE(t5679rec1.cnRiskStat[ix.toInt()],SPACES)
			&& isEQ(t5679rec1.cnRiskStat[ix.toInt()],chdrlifIO.getStatcode())) {
				wsaaValidStatus.set("Y");
			}
		}
		if (!validStatus.isTrue()) {
			sv.chdrselErr.set(errorsInner.e767);
			goTo(GotoLabel.exit2390);
		}
		wsaaValidStatus.set(SPACES);
		for (ix.set(1); !(isGT(ix,12)
		|| validStatus.isTrue()); ix.add(1)){
			if (isNE(t5679rec1.cnPremStat[ix.toInt()],SPACES)
			&& isEQ(t5679rec1.cnPremStat[ix.toInt()],chdrlifIO.getPstatcode())) {
				wsaaValidStatus.set("Y");
			}
		}
		if (!validStatus.isTrue()) {
			sv.chdrselErr.set(errorsInner.e767);
			goTo(GotoLabel.exit2390);
		}
	}

protected void getLifeAssured2330()
	{
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setStatuz(varcom.oK);
		lifeenqIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		lifeenqIO.setFormat(lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifeenqIO.getStatuz(),varcom.endp)
		|| isNE(lifeenqIO.getChdrcoy(),chdrlifIO.getChdrcoy())
		|| isNE(lifeenqIO.getChdrnum(),chdrlifIO.getChdrnum())) {
			sv.clttwo.set(SPACES);
		}
		if (isNE(sv.clttwo,lifeenqIO.getLifcnum())) {
			sv.clttwoErr.set(errorsInner.f049);
			goTo(GotoLabel.exit2390);
		}
	}

protected void lockContract2340()
	{
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.entity.set(sv.chdrsel);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("LOCK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			return ;
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(errorsInner.e073);
			goTo(GotoLabel.exit2990);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		try {
			
			if(isEQ(sv.action,"C")) {
				chdrmjaIO.setFunction("KEEPS");
				chdrmjaIO.setFormat("CHDRMJAREC");
				SmartFileCode.execute(appVars, chdrmjaIO);
				if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(chdrmjaIO.getParams());
					fatalError600();
				}
			}
			updateWssp3010();
			batching3080();
		}
		catch (GOTOException e){
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.sbmaction,"A") || isEQ(wsspcomn.sbmaction,"C") ) {
			wsspcomn.flag.set("M");
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		softLockClient3200();
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void softLockClient3200()
	{
			start3210();
		}

protected void start3210()
	{
		if (isNE(sv.action,"A")
		&& isNE(sv.action,"B")) {
			return ;
		}
		initialize(sftlockrec.sftlockRec);
		sftlockrec.company.set(wsspcomn.fsuco);
		sftlockrec.enttyp.set("CN");
		sftlockrec.entity.set(sv.clttwo);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.date_var.set(varcom.vrcmDate);
		sftlockrec.time.set(varcom.vrcmTime);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("LOCK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.clttwoErr.set(errorsInner.rg41);
			wsspcomn.edterror.set("Y");
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e058 = new FixedLengthStringData(4).init("E058");
	private FixedLengthStringData rg41 = new FixedLengthStringData(4).init("RG41");
	private FixedLengthStringData g499 = new FixedLengthStringData(4).init("G499");
	private FixedLengthStringData h939 = new FixedLengthStringData(4).init("H939");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f049 = new FixedLengthStringData(4).init("F049");
	private FixedLengthStringData rf56 = new FixedLengthStringData(4).init("RF56");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData rgh2 = new FixedLengthStringData(4).init("RGH2");
}
}
