package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:36
 * Description:
 * Copybook name: CWFDDELKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cwfddelkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData cwfddelFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData cwfddelKey = new FixedLengthStringData(64).isAPartOf(cwfddelFileKey, 0, REDEFINE);
  	public FixedLengthStringData cwfddelChdrcoy = new FixedLengthStringData(1).isAPartOf(cwfddelKey, 0);
  	public FixedLengthStringData cwfddelChdrnum = new FixedLengthStringData(8).isAPartOf(cwfddelKey, 1);
  	public FixedLengthStringData cwfddelTrancd = new FixedLengthStringData(4).isAPartOf(cwfddelKey, 9);
  	public PackedDecimalData cwfddelEfdate = new PackedDecimalData(8, 0).isAPartOf(cwfddelKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(cwfddelKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(cwfddelFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		cwfddelFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}