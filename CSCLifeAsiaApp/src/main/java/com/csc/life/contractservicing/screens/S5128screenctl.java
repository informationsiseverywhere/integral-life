package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5128screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S5128screensfl";
		lrec.subfileClass = S5128screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 16;
		lrec.pageSubfile = 15;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 7, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5128ScreenVars sv = (S5128ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5128screenctlWritten, sv.S5128screensflWritten, av, sv.s5128screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5128ScreenVars screenVars = (S5128ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.register.setClassString(""); //ILIFE-1403 START by nnazeer
		screenVars.chdrstatus.setClassString(""); //ILIFE-1403 START by nnazeer
		screenVars.premstatus.setClassString(""); //ILIFE-1403 START by nnazeer
		screenVars.cntcurr.setClassString(""); //ILIFE-1403 START by nnazeer
		screenVars.numpols.setClassString(""); //ILIFE-1403 START by nnazeer
		screenVars.entity.setClassString(""); //ILIFE-1403 START by nnazeer
		screenVars.planSuffix.setClassString(""); //ILIFE-1403 START by nnazeer
		screenVars.life.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.lifeorcov.setClassString("");
	}

/**
 * Clear all the variables in S5128screenctl
 */
	public static void clear(VarModel pv) {
		S5128ScreenVars screenVars = (S5128ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.register.clear();//ILIFE-1403 START by nnazeer
		screenVars.chdrstatus.clear();//ILIFE-1403 START by nnazeer
		screenVars.premstatus.clear();//ILIFE-1403 START by nnazeer
		screenVars.cntcurr.clear();//ILIFE-1403 START by nnazeer
		screenVars.numpols.clear();//ILIFE-1403 START by nnazeer
		screenVars.entity.clear();//ILIFE-1403 START by nnazeer
		screenVars.planSuffix.clear();//ILIFE-1403 START by nnazeer
		screenVars.life.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.jlife.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.lifeorcov.clear();
	}
}
