package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:48
 * Description:
 * Copybook name: UTRNCFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrncfikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrncfiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData utrncfiKey = new FixedLengthStringData(64).isAPartOf(utrncfiFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrncfiChdrcoy = new FixedLengthStringData(1).isAPartOf(utrncfiKey, 0);
  	public FixedLengthStringData utrncfiChdrnum = new FixedLengthStringData(8).isAPartOf(utrncfiKey, 1);
  	public FixedLengthStringData utrncfiLife = new FixedLengthStringData(2).isAPartOf(utrncfiKey, 9);
  	public FixedLengthStringData utrncfiCoverage = new FixedLengthStringData(2).isAPartOf(utrncfiKey, 11);
  	public FixedLengthStringData utrncfiRider = new FixedLengthStringData(2).isAPartOf(utrncfiKey, 13);
  	public PackedDecimalData utrncfiPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrncfiKey, 15);
  	public PackedDecimalData utrncfiTranno = new PackedDecimalData(5, 0).isAPartOf(utrncfiKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(utrncfiKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrncfiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrncfiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}