/*
 * File: Revlaps.java
 * Date: 30 August 2009 2:09:37
 * Author: Quipoz Limited
 * 
 * Class transformed from REVLAPS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrvsTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv1TableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcorv2TableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmrevTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
*  REVLAPS - Lapse/Paid-up Reversal
* ---------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  The following transactions are reversed :
*
*  - CHDR
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*            relevant contract.
*       DELET this record.
*       NEXTR on  the  Contract  Header record (CHDRLIF) for the
*            same contract. This should read a record with
*            valid flag of '2'. If not, then this is an error.
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*            altering  the  valid  flag from '2' to '1'.
*
*  - COVRs
*
*       Only reverse COVR records whose TRANNO is equal to the
*           Transaction number (of the PTRN) being reversed.
*       READH on  the  COVR  records for the relevant contract.
*       DELET this record.
*       NEXTR on  the  COVR  file should read a record with valid
*            flag  of  '2'. If not, then this is an error.
*       REWRT this  COVR  record after altering the valid flag
*            from  '2'  to  '1' . This will reinstate the
*            coverage prior to the LAPSE/PUP.
*
*  - UTRNs
*
*       Call the T5671 generic subroutine to reverse Unit
*       transactions for the TRANNO being reversed.
*       The subroutine GREVUTRN should be entered in the
*       relevant table entry.
*
*  - ACMVs
*
*       Do a BEGNH on the ACMV file for this transaction no. and
*       call LIFACMV to post the reversal to the sub-account.
*
*  - AGCMs
*
*       Read the first AGCM record (AGCMRVS).
*       Delete this record (via AGCMDBC).
*       Read the next AGCM looking for valid flag 2 records.
*       Update these AGCM records with valid flag 1.
*
*  - PAYR
*
*       READH on the Payor record (PAYRLIF) for the relevant
*            contract.
*       DELET this record.
*       NEXTR on the Payor record (PAYRLIF) for the same
*            contract. This should read a record with valid
*            flag of '2'. If not, then this is an error.
*       REWRT this Payor record (PAYRLIF) after altering the
*            valid flag from '2' to '1'.
*
* AGENT/GOVERNMENT STATISTICS RECORDS REVERSAL.
*
* All of these records are reversed when REVGENAT, the AT module,
*  calls the statistical subroutine LIFSTTR.
*
* NO processing is therefore required in this subroutine.
*
*
****************************************************************
* </pre>
*/
public class Revlaps extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVLAPS";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(SPACES);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");

	private FixedLengthStringData wsaaFprmUpdate = new FixedLengthStringData(1).init(SPACES);
	private Validator fprmUpdated = new Validator(wsaaFprmUpdate, "Y");

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t1688 = "T1688";
	private static final String t5729 = "T5729";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private AgcmrvsTableDAM agcmrvsIO = new AgcmrvsTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private Fpcorv1TableDAM fpcorv1IO = new Fpcorv1TableDAM();
	private Fpcorv2TableDAM fpcorv2IO = new Fpcorv2TableDAM();
	private FprmrevTableDAM fprmrevIO = new FprmrevTableDAM();
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		processComponent3050, 
		exit3090, 
		findRecordToReverse5120
	}

	public Revlaps() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		syserrrec.subrname.set(wsaaSubr);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		wsaaRevTrandesc.set(descIO.getLongdesc());
	}

protected void process2000()
	{
		/*PROCESS*/
		processChdrs2100();
		processCovrs3000();
		processAcmvs4000();
		processAcmvOptical4010();
		processAgcms5000();
		processPayrs6000();
		if (flexiblePremium.isTrue()) {
			updateFprm7000();
			if (fprmUpdated.isTrue()) {
				processFpco7500();
			}
		}
		/*EXIT*/
	}

protected void processChdrs2100()
	{
		start2101();
	}

protected void start2101()
	{
		/*  delete valid flag 1 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getValidflag(), 1)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		chdrlifIO.setFunction(varcom.delet);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*    Select the next record which will be the valid flag 2 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		/* MOVE BEGNH                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrlifIO.getValidflag(), 2)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.writd);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*                                                         <D9604>*/
		/* Read T5729 to determine whether this contract is a flexi<D9604>*/
		/* premium 'type'                                          <D9604>*/
		/*                                                         <D9604>*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5729);
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemitem(chdrlifIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			wsaaFlexiblePremium.set("Y");
		}
	}

protected void processCovrs3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covers3001();
				case processComponent3050: 
					processComponent3050();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covers3001()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrIO.getStatuz(), varcom.endp)
		|| isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void processComponent3050()
	{
		/*    If this component has a TRANNO equal to the TRANNO we are*/
		/*    reversing, then it has been Lapsed/Paid-up and must now be*/
		/*    reversed.  If not, then find the next component.*/
		wsaaCovrKey.set(covrIO.getDataKey());
		if (isEQ(covrIO.getTranno(), reverserec.tranno)) {
			deleteAndUpdatComp3200();
			processGenericSubr3300();
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp)
		|| isNE(covrIO.getDataKey(), wsaaCovrKey))) {
			getNextCovr3500();
		}
		
		if (isNE(covrIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.processComponent3050);
		}
	}

protected void deleteAndUpdatComp3200()
	{
		deleteRecs3201();
	}

protected void deleteRecs3201()
	{
		if (isNE(covrIO.getValidflag(), 1)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/* valid flag equal to 1*/
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/* Check for a increase record created by the forward           */
		/* transaction and delete it if one exists.                     */
		checkIncr3600();
		covrIO.setFunction(varcom.delet);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/*    As we have just deleted the VALIDFLAG '1' record there*/
		/*    must be a VALIDFLAG '2' record.*/
		if (isNE(covrIO.getValidflag(), 2)
		|| isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/* MOVE REVE-TRANS-DATE        TO COVR-TRANSACTION-DATE         */
		/* MOVE REVE-TRANS-TIME        TO COVR-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO COVR-USER.                    */
		/* MOVE REVE-TERMID            TO COVR-TERMID.                  */
		/* MOVE REVE-NEW-TRANNO        TO COVR-TRANNO.                  */
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processGenericSubr3300()
	{
		readGenericTable3301();
	}

protected void readGenericTable3301()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Trancode.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		/* MOVE ZEROES                 TO GREV-PLAN-SUFFIX.             */
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.life.set(covrIO.getLife());
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callSubprog3400();
		}
	}

protected void callSubprog3400()
	{
		/*GO*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
		}
		if (isNE(greversrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(greversrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void getNextCovr3500()
	{
		/*GET-RECS*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkIncr3600()
	{
		readh3610();
	}

protected void readh3610()
	{
		/* Look for a historic INCR record for the forward transaction. */
		/* If one exists for this component, delete it, thereby         */
		/* reinstating the previous record.                             */
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(covrIO.getChdrcoy());
		incrselIO.setChdrnum(covrIO.getChdrnum());
		incrselIO.setLife(covrIO.getLife());
		incrselIO.setCoverage(covrIO.getCoverage());
		incrselIO.setRider(covrIO.getRider());
		incrselIO.setPlanSuffix(covrIO.getPlanSuffix());
		incrselIO.setTranno(reverserec.tranno);
		incrselIO.setFunction(varcom.readh);
		incrselIO.setFormat(formatsInner.incrselrec);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)
		&& isNE(incrselIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(incrselIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrselIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			xxxxFatalError();
		}
	}

protected void processAcmvs4000()
	{
		readSubAcctTable4001();
	}

protected void readSubAcctTable4001()
	{
		/*    UNITDEAL will reverse all the ACMVs created by B5463, so*/
		/*    here we reverse all other ACMVs.*/
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
	}

protected void processAcmvOptical4010()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			xxxxFatalError();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void reverseAcmvs4100()
	{
		start4100();
		readNextAcmv4180();
	}

protected void start4100()
	{
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaRevTrandesc);
		/* MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			xxxxFatalError();
		}
	}

protected void readNextAcmv4180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAgcms5000()
	{
		agcms5001();
	}

protected void agcms5001()
	{
		agcmrvsIO.setParams(SPACES);
		agcmrvsIO.setChdrcoy(reverserec.company);
		agcmrvsIO.setChdrnum(reverserec.chdrnum);
		agcmrvsIO.setTranno(reverserec.tranno);
		agcmrvsIO.setFormat(formatsInner.agcmrvsrec);
		agcmrvsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrvsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(), varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmrvsIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(agcmrvsIO.getChdrcoy(), reverserec.company)
		|| isNE(agcmrvsIO.getTranno(), reverserec.tranno)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmrvsIO.getStatuz(), varcom.endp))) {
			reverseAgcm5100();
		}
		
	}

protected void reverseAgcm5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					deleteRecs5101();
				case findRecordToReverse5120: 
					findRecordToReverse5120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void deleteRecs5101()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/*    Delete this record*/
		agcmdbcIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/*    valid flag 2 record that was changed by the original*/
		/*    Lapse/PUP transaction.  If this contract has had single*/
		/*    premium top-ups or lump sums at contract issue, we may find*/
		/*    a valid flag 1 record for the key we are reading.  Ignore*/
		/*    these records and continue the quest for the valid flag 2*/
		/*    record.*/
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmdbcIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void findRecordToReverse5120()
	{
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getChdrcoy(), agcmrvsIO.getChdrcoy())
		|| isNE(agcmdbcIO.getChdrnum(), agcmrvsIO.getChdrnum())
		|| isNE(agcmdbcIO.getAgntnum(), agcmrvsIO.getAgntnum())
		|| isNE(agcmdbcIO.getLife(), agcmrvsIO.getLife())
		|| isNE(agcmdbcIO.getCoverage(), agcmrvsIO.getCoverage())
		|| isNE(agcmdbcIO.getRider(), agcmrvsIO.getRider())
		|| isNE(agcmdbcIO.getPlanSuffix(), agcmrvsIO.getPlanSuffix())
		|| isNE(agcmdbcIO.getSeqno(), agcmrvsIO.getSeqno())) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getValidflag(), "2")) {
			agcmdbcIO.setFunction(varcom.nextr);
			goTo(GotoLabel.findRecordToReverse5120);
		}
		/*  Update The Selected Record on the Coverage File.*/
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmrvsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(), varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(agcmrvsIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(agcmrvsIO.getChdrcoy(), reverserec.company)
		|| isNE(agcmrvsIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(agcmrvsIO.getTranno(), reverserec.tranno)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
	}

protected void processPayrs6000()
	{
		start6001();
	}

protected void start6001()
	{
		/*  delete valid flag 1 record                                     */
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getValidflag(), 1)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setFunction(varcom.delet);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*    Select the next record which will be the valid flag 2 record */
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		/* MOVE BEGNH                  TO PAYRLIF-FUNCTION      <LA3993>*/
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), 2)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		payrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO PAYRLIF-FUNCTION      <LA3993>*/
		payrlifIO.setFunction(varcom.writd);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void updateFprm7000()
	{
		para7010();
	}

	/**
	* <pre>
	**************************************                    <D9604>
	* </pre>
	*/
protected void para7010()
	{
		/*                                                         <D9604>*/
		/* Read and update the FPRMREV file. This file holds       <D9604> */
		/* details of the premium received. The amount on the LINS <D9604>*/
		/* the amount to be deducted from the record.              <D9604>*/
		/*                                                         <D9604>*/
		fprmrevIO.setDataArea(SPACES);
		fprmrevIO.setChdrcoy(reverserec.company);
		fprmrevIO.setChdrnum(reverserec.chdrnum);
		fprmrevIO.setPayrseqno(payrlifIO.getPayrseqno());
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		fprmrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(fprmrevIO.getValidflag(), "1")) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		/*                                                         <D9604>*/
		/* Delete the VF 1 FPRMREV record then re-instate the VF2  <D9604> */
		/*                                                         <D9604> */
		if (isNE(fprmrevIO.getTranno(), reverserec.tranno)) {
			fprmrevIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, fprmrevIO);
			if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(fprmrevIO.getStatuz());
				syserrrec.params.set(fprmrevIO.getParams());
				xxxxFatalError();
			}
			return ;
		}
		wsaaFprmUpdate.set("Y");
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		fprmrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		/*                                                         <D9604>*/
		/* Now reinstate the previous vf2                          <D9604>*/
		/*                                                         <D9604>*/
		fprmrevIO.setFunction(varcom.readh);
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(fprmrevIO.getValidflag(), "2")) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
		fprmrevIO.setValidflag("1");
		fprmrevIO.setCurrto(varcom.vrcmMaxDate);
		fprmrevIO.setFunction(varcom.rewrt);
		fprmrevIO.setFormat(formatsInner.fprmrevrec);
		SmartFileCode.execute(appVars, fprmrevIO);
		if (isNE(fprmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fprmrevIO.getStatuz());
			syserrrec.params.set(fprmrevIO.getParams());
			xxxxFatalError();
		}
	}

protected void processFpco7500()
	{
		/*PARA*/
		fpcorv1IO.setChdrcoy(reverserec.company);
		fpcorv1IO.setChdrnum(reverserec.chdrnum);
		fpcorv1IO.setEffdate(reverserec.ptrneff);
		/* MOVE BEGNH                  TO FPCORV1-FUNCTION.     <LA3993>*/
		fpcorv1IO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcorv1IO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		fpcorv1IO.setFormat(formatsInner.fpcorv1rec);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(), varcom.oK)
		|| isNE(fpcorv1IO.getChdrcoy(), reverserec.company)
		|| isNE(fpcorv1IO.getChdrnum(), reverserec.chdrnum)
		|| isNE(fpcorv1IO.getEffdate(), reverserec.ptrneff)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		while ( !(isEQ(fpcorv1IO.getStatuz(), varcom.endp))) {
			reverseFpco7600();
		}
		
		/*EXIT*/
	}

	/**
	* <pre>
	**************************************                    <D9604>
	* </pre>
	*/
protected void reverseFpco7600()
	{
		para7600();
		readNext7680();
	}

	/**
	* <pre>
	**************************************                    <D9604>
	* </pre>
	*/
protected void para7600()
	{
		/*                                                         <D9604>*/
		/*    Reverse all FPCO records where the effective date    <D9604>*/
		/*    is equal to the PTRN effective date.                 <D9604>*/
		/*                                                         <D9604>*/
		fpcorv2IO.setChdrcoy(fpcorv1IO.getChdrcoy());
		fpcorv2IO.setChdrnum(fpcorv1IO.getChdrnum());
		fpcorv2IO.setLife(fpcorv1IO.getLife());
		fpcorv2IO.setCoverage(fpcorv1IO.getCoverage());
		fpcorv2IO.setRider(fpcorv1IO.getRider());
		fpcorv2IO.setPlanSuffix(fpcorv1IO.getPlanSuffix());
		fpcorv2IO.setTargfrom(fpcorv1IO.getTargfrom());
		/*    Reverse all FPCO records where the effective date    <D9604>*/
		/*    is equal to the PTRN effective date.                 <D9604>*/
		/*                                                         <D9604>*/
		/* MOVE DELET                  TO FPCORV1-FUNCTION.     <LA3993>*/
		/*bug #ILIFE-899 start*/
		//fpcorv1IO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		fpcorv2IO.setFormat(formatsInner.fpcorv2rec);
		fpcorv2IO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			xxxxFatalError();
		}
		fpcorv1IO.setFunction(varcom.deltd);
		/*bug #ILIFE-899 end*/
		fpcorv2IO.setValidflag("1");
		fpcorv2IO.setCurrto(varcom.vrcmMaxDate);
		fpcorv2IO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcorv2IO);
		if (isNE(fpcorv2IO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcorv2IO.getStatuz());
			syserrrec.params.set(fpcorv2IO.getParams());
			xxxxFatalError();
		}
	}

protected void readNext7680()
	{
		fpcorv1IO.setFunction(varcom.nextr);
		fpcorv1IO.setFormat(formatsInner.fpcorv1rec);
		SmartFileCode.execute(appVars, fpcorv1IO);
		if (isNE(fpcorv1IO.getStatuz(), varcom.oK)
		&& isNE(fpcorv1IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fpcorv1IO.getParams());
			xxxxFatalError();
		}
		if (isNE(fpcorv1IO.getChdrcoy(), reverserec.company)
		|| isNE(fpcorv1IO.getChdrnum(), reverserec.chdrnum)
		|| isNE(fpcorv1IO.getEffdate(), reverserec.ptrneff)) {
			/*    MOVE REWRT               TO FPCORV1-FUNCTION      <LA3993>*/
			/*    CALL 'FPCORV1IO'         USING FPCORV1-PARAMS     <LA3993>*/
			/*    IF FPCORV1-STATUZ        NOT = O-K                <LA3993>*/
			/*       MOVE FPCORV1-PARAMS   TO SYSR-PARAMS           <LA3993>*/
			/*       PERFORM XXXX-FATAL-ERROR                       <LA3993>*/
			/*    END-IF                                            <LA3993>*/
			fpcorv1IO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorBomb();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
		/**A000-STATISTICS SECTION.                                    <001>*/
		/*************************                                     <001>*/
		/**A010-START.                                                 <001>*/
		/*****                                                         <001>*/
		/***** MOVE REVE-BATCCOY           TO LIFS-BATCCOY.            <001>*/
		/***** MOVE REVE-BATCBRN           TO LIFS-BATCBRN.            <001>*/
		/***** MOVE REVE-BATCACTYR         TO LIFS-BATCACTYR.          <001>*/
		/***** MOVE REVE-BATCACTMN         TO LIFS-BATCACTMN.          <001>*/
		/***** MOVE REVE-BATCTRCDE         TO LIFS-BATCTRCDE.          <001>*/
		/***** MOVE REVE-BATCBATCH         TO LIFS-BATCBATCH.          <001>*/
		/*****                                                         <001>*/
		/***** MOVE CHDRLIF-CHDRCOY        TO LIFS-CHDRCOY.            <001>*/
		/***** MOVE CHDRLIF-CHDRNUM        TO LIFS-CHDRNUM.            <001>*/
		/***** MOVE CHDRLIF-TRANNO         TO LIFS-TRANNO.             <001>*/
		/***** MOVE REVE-TRANNO            TO LIFS-TRANNOR.            <001>*/
		/***** MOVE SPACES                 TO LIFS-AGNTNUM.            <001>*/
		/***** MOVE SPACES                 TO LIFS-OLD-AGNTNUM.        <001>*/
		/*****                                                         <001>*/
		/***** CALL 'LIFSTTR'              USING LIFS-LIFSTTR-REC.     <001>*/
		/*****                                                         <001>*/
		/***** IF LIFS-STATUZ              NOT = O-K                   <001>*/
		/*****    MOVE LIFS-LIFSTTR-REC    TO SYSR-PARAMS              <001>*/
		/*****    MOVE LIFS-STATUZ         TO SYSR-STATUZ              <001>*/
		/*****    PERFORM XXXX-FATAL-ERROR.                            <001>*/
		/*****                                                         <001>*/
		/**A090-EXIT.                                                  <001>*/
		/***** EXIT.                                                   <001>*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData agcmrvsrec = new FixedLengthStringData(10).init("AGCMRVSREC");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData incrselrec = new FixedLengthStringData(10).init("INCRSELREC");
	private FixedLengthStringData fpcorv1rec = new FixedLengthStringData(10).init("FPCORV1REC");
	private FixedLengthStringData fpcorv2rec = new FixedLengthStringData(10).init("FPCORV2REC");
	private FixedLengthStringData fprmrevrec = new FixedLengthStringData(10).init("FPRMREVREC");
}
}
