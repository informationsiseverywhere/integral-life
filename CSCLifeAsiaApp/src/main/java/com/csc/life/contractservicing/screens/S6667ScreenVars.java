package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6667
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class S6667ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(417);
	public FixedLengthStringData dataFields = new FixedLengthStringData(193).isAPartOf(dataArea, 0);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,80);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,90);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData mandref = DD.mandref.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData numsel = DD.numsel.copy().isAPartOf(dataFields,138);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,148);
	public FixedLengthStringData mrbnk = DD.mrbnk.copy().isAPartOf(dataFields, 156);		// ILIFE-2476
	public FixedLengthStringData bnkbrn = DD.bnkbrn.copy().isAPartOf(dataFields, 157);	// ILIFE-2476
	public FixedLengthStringData bnkbrndesc = DD.branchdesc.copy().isAPartOf(dataFields, 163);// ILIFE-2476
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 193);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData facthousErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData mandrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData numselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData mrbnkErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);	// ILIFE-2476
	public FixedLengthStringData bnkbrnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);	// ILIFE-2476
	public FixedLengthStringData bnkbrndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);	// ILIFE-2476
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 249);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] facthousOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] mandrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] numselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] mrbnkOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);	// ILIFE-2476
	public FixedLengthStringData[] bnkbrnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);	// ILIFE-2476
	public FixedLengthStringData[] bnkbrndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);// ILIFE-2476
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);

	public LongData S6667screenWritten = new LongData(0);
	public LongData S6667windowWritten = new LongData(0);
	public LongData S6667protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6667ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(mandrefOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mrbnkOut,new String[] {"03",null, "-03","04", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {payrnum, currcode, facthous, numsel, billcd, mandref, bankkey, bankdesc, branchdesc, bankacckey, bankaccdsc, mrbnk, bnkbrn, bnkbrndesc};
		screenOutFields = new BaseData[][] {payrnumOut, currcodeOut, facthousOut, numselOut, billcdOut, mandrefOut, bankkeyOut, bankdescOut, branchdescOut, bankacckeyOut, bankaccdscOut, mrbnkOut, bnkbrnOut, bnkbrndescOut};
		screenErrFields = new BaseData[] {payrnumErr, currcodeErr, facthousErr, numselErr, billcdErr, mandrefErr, bankkeyErr, bankdescErr, branchdescErr, bankacckeyErr, bankaccdscErr, mrbnkErr, bnkbrnErr, bnkbrndescErr};
		screenDateFields = new BaseData[] {billcd};
		screenDateErrFields = new BaseData[] {billcdErr};
		screenDateDispFields = new BaseData[] {billcdDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6667screen.class;
		protectRecord = S6667protect.class;
	}

}
