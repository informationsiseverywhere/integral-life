package com.csc.life.contractservicing.dataaccess;

import com.csc.life.newbusiness.dataaccess.BnfypfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BnfymnaTableDAM.java
 * Date: Sun, 30 Aug 2009 03:30:01
 * Class transformed from BNFYMNA.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BnfymnaTableDAM extends BnfypfTableDAM {

	public BnfymnaTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("BNFYMNA");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", BNYTYPE"
		             + ", BNYCLT";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "BNYCLT, " +
		            "TRANNO, " +
		            "VALIDFLAG, " +
		            "BNYRLN, " +
		            "BNYCD, " +
		            "BNYPC, " +
		            "CURRTO, " +
		            "CURRFR, " +
		            "TERMID, " +
		            "USER_T, " +
		            "TRTM, " +
		            "TRDT, " +
		            "EFFDATE, " +
		            "BNYTYPE, " +
		            "CLTRELN, " +
		            "RELTO, " +
		            "SELFIND, " +
		            "REVCFLG, " +
		            "ENDDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "SEQUENCE, " + 
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "BNYTYPE ASC, " +
		            "SEQUENCE ASC, " +
		            "BNYCLT ASC, " +
					"UNIQUE_NUMBER ASC";//ILIFE-7220
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "BNYTYPE DESC, " +
		            "SEQUENCE DESC, " +
		            "BNYCLT DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               bnyclt,
                               tranno,
                               validflag,
                               bnyrln,
                               bnycd,
                               bnypc,
                               currto,
                               currfrom,
                               termid,
                               user,
                               transactionTime,
                               transactionDate,
                               effdate,
                               bnytype,
                               cltreln,
                               relto,
                               selfind,
                               revcflg,
                               enddate,
                               userProfile,
                               jobName,
                               datime,
                               sequence,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(237);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getBnytype().toInternal()
					+ getBnyclt().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, bnytype);
			what = ExternalData.chop(what, bnyclt);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller160 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(bnyclt.toInternal());
	nonKeyFiller160.setInternal(bnytype.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(121);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getTranno().toInternal()
					+ getValidflag().toInternal()
					+ getBnyrln().toInternal()
					+ getBnycd().toInternal()
					+ getBnypc().toInternal()
					+ getCurrto().toInternal()
					+ getCurrfrom().toInternal()
					+ getTermid().toInternal()
					+ getUser().toInternal()
					+ getTransactionTime().toInternal()
					+ getTransactionDate().toInternal()
					+ getEffdate().toInternal()
					+ nonKeyFiller160.toInternal()
					+ getCltreln().toInternal()
					+ getRelto().toInternal()
					+ getSelfind().toInternal()
					+ getRevcflg().toInternal()
					+ getEnddate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getSequence().toInternal());//ILIFE-7220
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, bnyrln);
			what = ExternalData.chop(what, bnycd);
			what = ExternalData.chop(what, bnypc);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, nonKeyFiller160);
			what = ExternalData.chop(what, cltreln);
			what = ExternalData.chop(what, relto);
			what = ExternalData.chop(what, selfind);
			what = ExternalData.chop(what, revcflg);
			what = ExternalData.chop(what, enddate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, sequence);//ILIFE-7220
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getBnytype() {
		return bnytype;
	}
	public void setBnytype(Object what) {
		bnytype.set(what);
	}
	public FixedLengthStringData getBnyclt() {
		return bnyclt;
	}
	public void setBnyclt(Object what) {
		bnyclt.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getBnyrln() {
		return bnyrln;
	}
	public void setBnyrln(Object what) {
		bnyrln.set(what);
	}	
	public FixedLengthStringData getBnycd() {
		return bnycd;
	}
	public void setBnycd(Object what) {
		bnycd.set(what);
	}	
	public PackedDecimalData getBnypc() {
		return bnypc;
	}
	public void setBnypc(Object what) {
		setBnypc(what, false);
	}
	public void setBnypc(Object what, boolean rounded) {
		if (rounded)
			bnypc.setRounded(what);
		else
			bnypc.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getEnddate() {
		return enddate;
	}
	public void setEnddate(Object what) {
		setEnddate(what, false);
	}
	public void setEnddate(Object what, boolean rounded) {
		if (rounded)
			enddate.setRounded(what);
		else
			enddate.set(what);
	}
	public FixedLengthStringData getCltreln() {
		return cltreln;
	}
	public void setCltreln(Object what) {
		cltreln.set(what);
	}	
	public FixedLengthStringData getRelto() {
		return relto;
	}
	public void setRelto(Object what) {
		relto.set(what);
	}	
	public FixedLengthStringData getSelfind() {
		return selfind;
	}
	public void setSelfind(Object what) {
		selfind.set(what);
	}	
	public FixedLengthStringData getRevcflg() {
		return revcflg;
	}
	public void setRevcflg(Object what) {
		revcflg.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}
	//ILIFE-7220
	public FixedLengthStringData getSequence() {
		return sequence;
	}
	public void setSequence(Object what) {
		sequence.set(what);
	}	
	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		bnytype.clear();
		bnyclt.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		tranno.clear();
		validflag.clear();
		bnyrln.clear();
		bnycd.clear();
		bnypc.clear();
		currto.clear();
		currfrom.clear();
		termid.clear();
		user.clear();
		transactionTime.clear();
		transactionDate.clear();
		effdate.clear();
		nonKeyFiller160.clear();
		cltreln.clear();
		relto.clear();
		selfind.clear();
		revcflg.clear();
		enddate.clear();
		userProfile.clear();
		jobName.clear();		
		datime.clear();
		sequence.clear();//ILIFE-7220
	}


}