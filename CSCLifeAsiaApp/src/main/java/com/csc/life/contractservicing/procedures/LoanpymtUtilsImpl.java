 /*
 * File: LoanpymtPojo.java
 * Date: 13 Oct 2018 22:58:40
 * Author: sbatra9
 * 
 * Purpose :curd data for Loanpymt
 * 
 * Copyright (2007) DXC, all rights reserved.
 */ 
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Addacmv;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Addacmvrec;
import com.csc.fsu.general.recordstructures.CashedPojo;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Rldgkey;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.productdefinition.procedures.LifacmvPojo;
import com.csc.life.productdefinition.procedures.LifacmvUtils;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              LOAN PAYMENTS SUBROUTINE.
*              =========================
*
* Overview.
* ---------
*
* This routine is called from within the Cash receipts subsystem
*  and from the various Claims subsystems.
* Its function is to post cash received against any Loan(s) the
*  contract may have.
* It may pay off Loan principal or Loan interest - it all depends
*  on what Sub-account Code & Type are passed in - thus sites can
*  dicate whether Loan principal or interest should be paid-off
*  first/if at all.
* IF the whole balance of an ACBL is paid-off, then the program
*  will check whether the Loan has any debt left outstanding on
*  it - ie check whether the Principal and Interest ACBLs are
*  both zero, and if so, the Loan is deemed to be Paid off and so
*  the program will V/Flag '2' the Loan record
* It will try and post as much of the Amount that is passed in as
* possible - depending on the Function specified, it may post any
* residual cash to Suspense or it may just return the Residual
* amount to the calling program.
*
* Given:     The CASHEDREC copybook, but we will only use the
*             following fields...
*              CSHD-SACSCODE, CSHD-SACSTYP, CSHD-ORIGAMT,
*              CSHD-ORIGCCY, CSHD-GENLKEY, CSHD-FUNCTION,
*              CSHD-CHDRNUM, CSHD-CHDRCOY, CSHD-SIGN.
*
*
* Gives:     CSHD-STATUZ, CSHD-DOCAMT ( this is the
*                                            residual ....
*                                            the cash left over )
*
* Processing.
* -----------
*
* Do a BEGN on the ACBLCLM file using the Company,
*  Contract number, Sub-account Code and Sub-account type
*  specified in the linkage area
*
*
*    IF CSHD-ORIGAMT  >=  ACBLCLM balance,
*       Write ACMV record for ACBLCLM balance using ACBLCLM key
*       Check whether Loan has been totally paid-off, and if so,
*         Update the LOAN record and Validflag '2' it
*       Subtract ACBLCLM balance from CSHD-ORIGAMT
*
*    ELSE
*       Write ACMV record for amount using key from ACBLCLM
*       set Residual amount to Zero
*       EXIT subroutine
*    END IF
*
* Read Next ACBLCLM and process as above.
*
*    IF no more ACBLCLM records and CSHD-AMOUNT still > Zero
*
*        IF CSHD-FUNCTION =  UPDAT
*           Post Amount left ( ie Residual ) to Suspense account
*
*        ELSE
*           Set Residual = Amount left
*           EXIT subroutine
*        END IF
*    END IF
*
*********
* NOTE !!
*********
*      This program uses T5645 and therefore expects to find
*       entries for the following....
*
*     entry No 1 .... Cash Suspense
*
*     entries 2 & 3   Loan Principal & Loan interest for Policy
*                      Loans
*
*     entries 4 & 5   Loan Principal & Loan interest for
*                      Non-forfeiture APLs ( Automatic Policy
*                                                      Loans )
*
*
*****************************************************************
* </pre>
*/
public class LoanpymtUtilsImpl implements LoanpymtUtils{
	private static final Logger LOGGER = LoggerFactory.getLogger(LoanpymtUtilsImpl.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "LOANPYMT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlPrefix = new FixedLengthStringData(2).isAPartOf(wsaaGenlkey, 0);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLoanno = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaTurnOffLoan = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaAcblSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcblSacstype = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaReadRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaReadChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaReadRldgacct, 0);
	private FixedLengthStringData wsaaReadLoanno = new FixedLengthStringData(2).isAPartOf(wsaaReadRldgacct, 8);
	private FixedLengthStringData wsaaReadXtra = new FixedLengthStringData(6).isAPartOf(wsaaReadRldgacct, 10);
	private PackedDecimalData wsaaAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPostAmount = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3);
	private static final int wsaaMaxDate = 99999999;
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(4, 0).setUnsigned();
	private String wsaaSkip = "N";
	private PackedDecimalData wsaaCount = new PackedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();

		/* WSAA-KEYS */
	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);

	private Batckey wsaaBatckey = new Batckey();
	private Rdockey wsaaRdockey = new Rdockey();
	private Rldgkey wsaaRldgkey = new Rldgkey();
	private Varcom varcom = new Varcom();
	private Addacmvrec addacmvrec = new Addacmvrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Xcvrt xcvrt = new Xcvrt();
	private Addacmv addacmv = new Addacmv();
	private Syserrrec syserrrec = new Syserrrec();
	private Syserr syserr = new Syserr();
	private T3629rec t3629rec = new T3629rec();
	private T5645rec t5645rec = new T5645rec();
	
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrlifIO = new Chdrpf();
	private AcblpfDAO acblpfDAO =  getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	
	private LoanpfDAO loanpfDAO = getApplicationContext().getBean("loanpfDAO", LoanpfDAO.class);
	private Acblpf acblpf = null;
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Loanpf> loanpfList = null;
	private CashedPojo cashedPojo = new CashedPojo();
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
    private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
    private Chdrpf readChdrpf = null;
    private Loanpf loanpf = null;
	private LifacmvUtils lifacmvUtils = getApplicationContext().getBean("lifacmvUtils", LifacmvUtils.class);
	private LifacmvPojo lifacmvPojo = new LifacmvPojo();

	public LoanpymtUtilsImpl() {
		super();
	}


public void callLoanpymt(CashedPojo cashedPojo) {
	start1000(cashedPojo);
	if (isEQ(wsaaSkip,"Y")) {
		return ;
	}
	mainProcessing2000(cashedPojo);
}

protected void start1000(CashedPojo cashedPojo){
	cashedPojo.setStatuz("****");
	syserrrec.subrname.set(wsaaSubr);
	wsaaSkip = "N";
	wsaaGenlkey.set(SPACES);
	wsaaCurrency.set(SPACES);
	wsaaRldgacct.set(SPACES);
	wsaaReadRldgacct.set(SPACES);
	wsaaAcblSacscode.set(SPACES);
	wsaaAcblSacstype.set(SPACES);
	wsaaTurnOffLoan.set(SPACES);
	wsaaAmount.set(ZERO);
	wsaaPostAmount.set(ZERO);
	wsaaAmount.set(cashedPojo.getOrigamt());
	wsaaCurrency.set(cashedPojo.getOrigccy());
	wsaaRdockey.rdocKey.set(cashedPojo.getDoctkey());
	varcom.vrcmTranid.set(cashedPojo.getTranid());
	wsaaBatckey.batcKey.set(cashedPojo.getBatchkey());
	wsaaRldgkey.rldgKey.set(cashedPojo.getTrankey());
	wsaaGenlkey.set(cashedPojo.getGenlkey());
	wsaaSequenceNo.set(ZERO);
	wsaaSequenceNo.set(cashedPojo.getTranseq());
	wsaaCount.set(ZERO);
	wsaaNominalRate.set(ZERO);
	wsaaLedgerCcy.set(SPACES);
	readT56457000(cashedPojo);                                                
	readChdrpf = chdrpfDAO.getChdrlifRecord(cashedPojo.getChdrcoy(), cashedPojo.getChdrnum());
	if(readChdrpf == null ){
		databaseError99500(cashedPojo,"1");
	}

}

protected void mainProcessing2000(CashedPojo cashedPojo){
	wsaaRldgChdrnum.set(cashedPojo.getChdrnum());
	wsaaRldgLoanno.set("00");
	List<Acblpf> acblpfList = acblpfDAO.loadDataBulk(cashedPojo.getChdrcoy(),"",cashedPojo.getSacscode(),wsaaRldgacct.toString(),"",cashedPojo.getSacstyp());
    for (Acblpf a : acblpfList) {
    	acblpf = a;
        if (BigDecimal.ZERO.compareTo(a.getSacscurbal()) == 0) {
        	checkActiveLoan9000(cashedPojo);
        	continue;
        }
        /* Negate the Account balances if it is negative (for the */
        /* case of Cash accounts). */
        if (isLT(acblpf.getSacscurbal(), 0)) {
        	acblpf.setSacscurbal(acblpf.getSacscurbal().negate());
        }
        
        /* Get here so the ACBL we have read is OK*/
		/*  check that ACBL currency matches the currency of the amount*/
		/*  passed in in the CSHD-ORIGCCY field - if not, we must convert*/
		/*  the passed in amount, CSHD-ORIGCCY to the ACBL currency.*/
		if (isNE(wsaaCurrency,acblpf.getOrigcurr())) {
			/* When currency conversion used posting should be done*/
			/* before and after.*/
			lifacmvPojo.setOrigamt(new BigDecimal(wsaaAmount.toString()));
			lifacmvPojo.setOrigcurr(wsaaCurrency.toString());
			lifacmvPojo.setSacscode(t5645rec.sacscode06.toString());
			lifacmvPojo.setSacstyp(t5645rec.sacstype06.toString());
			lifacmvPojo.setGlcode(t5645rec.glmap06.toString());
			lifacmvPojo.setGlsign(t5645rec.sign06.toString());
			currencyPosting11000(cashedPojo);
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.currOut.set("SGD");
			convertCurrency4000(cashedPojo);
			wsaaCurrency.set("SGD");
			wsaaAmount.set(conlinkrec.amountOut);
			lifacmvPojo.setOrigamt(new BigDecimal(wsaaAmount.toString()));
			lifacmvPojo.setOrigcurr(wsaaCurrency.toString());
			lifacmvPojo.setSacscode(t5645rec.sacscode07.toString());
			lifacmvPojo.setSacstyp(t5645rec.sacstype07.toString());
			lifacmvPojo.setGlcode(t5645rec.glmap07.toString());
			lifacmvPojo.setGlsign(t5645rec.sign07.toString());
			currencyPosting11000(cashedPojo);
		
		}
		
		/* Now values are in correct currencies so lets do our sums*/
		if (isGT(wsaaAmount,acblpf.getSacscurbal())
		|| isEQ(wsaaAmount,acblpf.getSacscurbal())) {
			wsaaPostAmount.set(100);
			post5000(cashedPojo);
			/* we have paid off the whole of the ACBL balance, so we will go*/
			/*  and see whether the Loan is still active or whether all debts*/
			/*  have now been cleared*/
			checkActiveLoan9000(cashedPojo);
			compute(wsaaAmount, 2).set(sub(wsaaAmount,acblpf.getSacscurbal()));
		}
		else {
			wsaaPostAmount.set(wsaaAmount);
			post5000(cashedPojo);
			wsaaAmount.set(ZERO);
		}

	endProcessing6000(cashedPojo);
	/* Return the Jrnseq count from the ACMV reocrds written to the*/
	/*  calling program.*/
	cashedPojo.setTranseq(wsaaSequenceNo.toString());;
    }
}



protected void convertCurrency4000(CashedPojo cashedPojo){
	/* call the convert subroutine*/
	conlinkrec.amountIn.set(wsaaAmount);
	conlinkrec.statuz.set(SPACES);
	conlinkrec.function.set("REAL");
	conlinkrec.currIn.set(wsaaCurrency);
	conlinkrec.cashdate.set(wsaaMaxDate);
	conlinkrec.amountOut.set(ZERO);
	conlinkrec.company.set(cashedPojo.getChdrcoy());
	//callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
	xcvrt.mainline(new Object[] { conlinkrec.clnk002Rec });
	if (isNE(conlinkrec.statuz,varcom.oK)) {
		syserrrec.params.set(conlinkrec.clnk002Rec);
		syserrrec.statuz.set(conlinkrec.statuz);
		databaseError99500(cashedPojo,"2");
	}
	zrdecplcPojo.setAmountIn(new BigDecimal(conlinkrec.amountOut.toString()));
	a000CallRounding(zrdecplcPojo,cashedPojo);
	conlinkrec.amountOut.set(zrdecplcPojo.getAmountOut());
}

protected void post5000(CashedPojo cashedPojo){
	addacmvrec.addacmvRec.set(SPACES);
	addacmvrec.contot.set(0);
	addacmvrec.termid.set(varcom.vrcmTermid);
	addacmvrec.user.set(varcom.vrcmUser);
	addacmvrec.transactionDate.set(varcom.vrcmDate);
	addacmvrec.transactionTime.set(varcom.vrcmTime);
	addacmvrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
	addacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
	addacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
	addacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
	addacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
	addacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	addacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
	addacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
	addacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
	addacmvrec.rdocnum.set(wsaaRdockey.rdocRdocnum);
	addacmvrec.rldgcoy.set(wsaaRldgkey.rldgRldgcoy);
	addacmvrec.rldgacct.set(wsaaReadRldgacct);
	addacmvrec.genlcoy.set(wsaaGlCompany);
	addacmvrec.glcode.set(wsaaGlMap);
	addacmvrec.sacscode.set(cashedPojo.getSacscode());
	addacmvrec.sacstyp.set(cashedPojo.getSacstyp());
	addacmvrec.glsign.set(cashedPojo.getSign());
	wsaaSequenceNo.add(1);
	addacmvrec.transeq.set(wsaaSequenceNo);
	addacmvrec.effdate.set(cashedPojo.getTrandate());
	addacmvrec.trandesc.set(cashedPojo.getTrandesc());
	addacmvrec.origamt.set(wsaaPostAmount);
	addacmvrec.origccy.set(wsaaCurrency);
	addacmvrec.acctccy.set(cashedPojo.getAcctccy());
	/* Make sure that if posting currency is different to Gen Ledger*/
	/*  currency that the conversions are made properly.*/
	getGlCurrency5100(wsaaCurrency.toString());
	if (isEQ(cashedPojo.getFunction(),"UPDAT")) {
		addacmvrec.tranno.set(ZERO);
	}
	else {
		addacmvrec.tranno.set(cashedPojo.getTranno());
	}
	addacmvrec.rdocpfx.set("CA");
	addacmvrec.creddte.set(varcom.vrcmMaxDate);
	addacmvrec.chdrstcda.set(chdrlifIO.getCnttype());
	addacmvrec.function.set(SPACES);
	//callProgram(Addacmv.class, addacmvrec.addacmvRec);
	addacmv.mainline(new Object[] { addacmvrec.addacmvRec });
	if (isNE(addacmvrec.statuz,varcom.oK)) {
		syserrrec.params.set(addacmvrec.addacmvRec);
		syserrrec.statuz.set(addacmvrec.statuz);
		databaseError99500(cashedPojo,"2");
	}
}

protected void getGlCurrency5100(String itemitem){
		/* Use posting currency to Read T3629 Currency table to get*/
		/*  General Ledger currency & conversion rate to the GL currency.*/
		String itemcoy = addacmvrec.batccoy.toString();
		List<Itempf> items = itemDAO.getAllItemitem("IT", itemcoy, "T3629",itemitem);
		if (null == items || items.isEmpty()) {
			syserrrec.params.set("T3629");
			databaseError99500(cashedPojo,"3");
		}
		t3629rec.t3629Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(ZERO);
		wsaaCount.set(1);
		while ( !(isNE(wsaaNominalRate,ZERO)
		|| isGT(wsaaCount,7))) {
			findRate5200();
		}
		
		if (isEQ(wsaaNominalRate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				getGlCurrency5100(t3629rec.contitem.toString());
			}
			else {
				syserrrec.statuz.set("G418");
				databaseError99500(cashedPojo,"2");
			}
		}
		/* get here so we have a conversion rate so lets calculate*/
		/*  the ADDA-ACCTAMT*/
		addacmvrec.crate.set(wsaaNominalRate);
		addacmvrec.genlcur.set(wsaaLedgerCcy);
		compute(addacmvrec.acctamt, 9).set(mult(wsaaPostAmount,wsaaNominalRate));
	}

protected void findRate5200()
	{
		/*START*/
		if (isGTE(addacmvrec.effdate,t3629rec.frmdate[wsaaCount.toInt()])
		&& isLTE(addacmvrec.effdate,t3629rec.todate[wsaaCount.toInt()])) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaCount.toInt()]);
		}
		else {
			wsaaCount.add(1);
		}
		/*EXIT*/
	}

protected void endProcessing6000(CashedPojo cashedPojo){
	if (isGT(wsaaAmount,ZERO)) {
		if (isEQ(cashedPojo.getFunction(),"UPDAT")) {
			/*            we will credit any leftover cash to the suspense*/
			/*            account - but first convert it to the Original*/
			/*            currency passed in*/
			if (isNE(wsaaCurrency,cashedPojo.getOrigccy())) {
    			lifacmvPojo.setOrigamt(new BigDecimal(wsaaAmount.toString()));
    			lifacmvPojo.setOrigcurr(wsaaCurrency.toString());
    			lifacmvPojo.setSacscode(t5645rec.sacscode06.toString());
    			lifacmvPojo.setSacstyp(t5645rec.sacstype06.toString());
    			lifacmvPojo.setGlcode(t5645rec.glmap06.toString());
    			lifacmvPojo.setGlsign(t5645rec.sign06.toString());
				currencyPosting11000(cashedPojo);
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.currOut.set(cashedPojo.getOrigccy());
				convertCurrency4000(cashedPojo);
				wsaaAmount.set(conlinkrec.amountOut);
				wsaaCurrency.set(cashedPojo.getOrigccy());
    			lifacmvPojo.setOrigamt(new BigDecimal(wsaaAmount.toString()));
    			lifacmvPojo.setOrigcurr(wsaaCurrency.toString());
    			lifacmvPojo.setSacscode(t5645rec.sacscode07.toString());
    			lifacmvPojo.setSacstyp(t5645rec.sacstype07.toString());
    			lifacmvPojo.setGlcode(t5645rec.glmap07.toString());
    			lifacmvPojo.setGlsign(t5645rec.sign07.toString());
				currencyPosting11000(cashedPojo);
			}
			creditSuspense8000(cashedPojo);
			cashedPojo.setDocamt(BigDecimal.ZERO);
		}
		else {
			/* return amount left over in the currency it was passed in as*/
			if (isNE(wsaaCurrency,cashedPojo.getOrigccy())) {
    			lifacmvPojo.setOrigamt(new BigDecimal(wsaaAmount.toString()));
    			lifacmvPojo.setOrigcurr(wsaaCurrency.toString());
    			lifacmvPojo.setSacscode(t5645rec.sacscode06.toString());
    			lifacmvPojo.setSacstyp(t5645rec.sacstype06.toString());
    			lifacmvPojo.setGlcode(t5645rec.glmap06.toString());
    			lifacmvPojo.setGlsign(t5645rec.sign06.toString());
    			
				currencyPosting11000(cashedPojo);
				conlinkrec.currOut.set(cashedPojo.getOrigccy());
				convertCurrency4000(cashedPojo);
				cashedPojo.setDocamt(new BigDecimal(conlinkrec.amountOut.toString()));
    			lifacmvPojo.setOrigamt(new BigDecimal(wsaaAmount.toString()));
    			lifacmvPojo.setOrigcurr(wsaaCurrency.toString());
    			lifacmvPojo.setSacscode(t5645rec.sacscode07.toString());
    			lifacmvPojo.setSacstyp(t5645rec.sacstype07.toString());
    			lifacmvPojo.setGlcode(t5645rec.glmap07.toString());
    			lifacmvPojo.setGlsign(t5645rec.sign07.toString());
				currencyPosting11000(cashedPojo);
			}
			else {
				cashedPojo.setDocamt(new BigDecimal(wsaaAmount.toString()));
			}
		}
	}
	else {
		cashedPojo.setDocamt(BigDecimal.ZERO);
	}
}

protected void readT56457000(CashedPojo cashedPojo){
		/* we are going to post residual cash to suspense, so read T5645*/
		/*  to get Subaccount code & type etc...*/
	String itemcoy = cashedPojo.getChdrcoy();/* IJTI-1523 */
	String itemitem = wsaaSubr;
	
	List<Itempf> items = itemDao.getAllItemitem("IT", itemcoy, "T5645", itemitem);
	if (null == items || items.isEmpty()) {
		syserrrec.params.set("T5645");
		databaseError99500(cashedPojo,"3");
	} else {
		t5645rec.t5645Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	}	
}

protected void creditSuspense8000(CashedPojo cashedPojo){
	addacmvrec.addacmvRec.set(SPACES);
	addacmvrec.contot.set(0);
	addacmvrec.termid.set(varcom.vrcmTermid);
	addacmvrec.user.set(varcom.vrcmUser);
	addacmvrec.transactionDate.set(varcom.vrcmDate);
	addacmvrec.transactionTime.set(varcom.vrcmTime);
	addacmvrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
	addacmvrec.batccoy.set(cashedPojo.getChdrcoy());
	addacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
	addacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
	addacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
	addacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	addacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
	addacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
	addacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
	addacmvrec.rdocnum.set(wsaaRdockey.rdocRdocnum);
	addacmvrec.rldgcoy.set(wsaaRldgkey.rldgRldgcoy);
	addacmvrec.rldgacct.set(wsaaRldgkey.rldgRldgacct);
	addacmvrec.genlcoy.set(wsaaGlCompany);
	addacmvrec.genlcur.set(wsaaGlCurrency);
	/* MOVE WSAA-GL-MAP            TO ADDA-GLCODE.                  */
	addacmvrec.glcode.set(t5645rec.glmap01);
	addacmvrec.sacscode.set(t5645rec.sacscode01);
	addacmvrec.sacstyp.set(t5645rec.sacstype01);
	addacmvrec.glsign.set(t5645rec.sign01);
	wsaaSequenceNo.add(1);
	addacmvrec.transeq.set(wsaaSequenceNo);
	addacmvrec.effdate.set(cashedPojo.getTrandate());
	addacmvrec.trandesc.set(cashedPojo.getTrandesc());
	addacmvrec.origamt.set(wsaaAmount);
	addacmvrec.acctamt.set(ZERO);
	addacmvrec.origccy.set(wsaaCurrency);
	addacmvrec.acctccy.set(cashedPojo.getAcctccy());
	addacmvrec.crate.set(cashedPojo.getDissrate());
	if (isNE(cashedPojo.getTranno(),NUMERIC)) {
		cashedPojo.setTranno("0");
	}
	if (isEQ(cashedPojo.getFunction(),"UPDAT")) {
		addacmvrec.tranno.set(ZERO);
	}
	else {
		addacmvrec.tranno.set(cashedPojo.getTranno());
	}
	addacmvrec.rdocpfx.set("CA");
	addacmvrec.creddte.set(varcom.vrcmMaxDate);
	addacmvrec.chdrstcda.set(chdrlifIO.getCnttype());
	addacmvrec.function.set(SPACES);
//	callProgram(Addacmv.class, addacmvrec.addacmvRec);
	addacmv.mainline(new Object[] { addacmvrec.addacmvRec });
	if (isNE(addacmvrec.statuz,varcom.oK)) {
		syserrrec.params.set(addacmvrec.addacmvRec);
		syserrrec.statuz.set(addacmvrec.statuz);
		databaseError99500(cashedPojo,"2");
	}
}

protected void checkActiveLoan9000(CashedPojo cashedPojo){
	/* Read LOAN record*/
	wsaaTurnOffLoan.set(SPACES);
	loanpfList = loanpfDAO.loadDataByLoanNum("2", wsaaReadChdrnum.toString(), wsaaReadLoanno.toString());
	if(loanpfList == null || loanpfList.size() == 0){
		return;
	}else{
		loanpf = loanpfList.get(0);
	}
	if (isEQ(loanpf.getLoantype(),"P")) {
		/* check which of the 2 policy loan ACBLs we have just cleared and*/
		/*  then go and see if the other one is also zero*/
		if (isEQ(acblpf.getSacscode(),t5645rec.sacscode02)
		&& isEQ(acblpf.getSacstyp(),t5645rec.sacstype02)) {
			wsaaAcblSacscode.set(t5645rec.sacscode03);
			wsaaAcblSacstype.set(t5645rec.sacstype03);
			checkAccBalance10000();
		}
		if (isEQ(acblpf.getSacscode(),t5645rec.sacscode03)
		&& isEQ(acblpf.getSacstyp(),t5645rec.sacstype03)) {
			wsaaAcblSacscode.set(t5645rec.sacscode02);
			wsaaAcblSacstype.set(t5645rec.sacstype02);
			checkAccBalance10000();
		}
	}
	if (isEQ(loanpf.getLoantype(),"A")) {
		/* check which of the 2 APL loan ACBLs we have just cleared and*/
		/*  then go and see if the other one is also zero*/
		if (isEQ(acblpf.getSacscode(),t5645rec.sacscode04)
		&& isEQ(acblpf.getSacstyp(),t5645rec.sacstype04)) {
			wsaaAcblSacscode.set(t5645rec.sacscode05);
			wsaaAcblSacstype.set(t5645rec.sacstype05);
			checkAccBalance10000();
		}
		if (isEQ(acblpf.getSacscode(),t5645rec.sacscode05)
		&& isEQ(acblpf.getSacstyp(),t5645rec.sacstype05)) {
			wsaaAcblSacscode.set(t5645rec.sacscode04);
			wsaaAcblSacstype.set(t5645rec.sacstype04);
			checkAccBalance10000();
		}
	}
	if (isEQ(loanpf.getLoantype(),"E")) {
		/* check which of the 2 APL loan ACBLs we have just cleared and    */
		/*  then go and see if the other one is also zero                  */
		if (isEQ(acblpf.getSacscode(),t5645rec.sacscode08)
		&& isEQ(acblpf.getSacstyp(),t5645rec.sacstype08)) {
			wsaaAcblSacscode.set(t5645rec.sacscode09);
			wsaaAcblSacstype.set(t5645rec.sacstype09);
			checkAccBalance10000();
		}
		if (isEQ(acblpf.getSacscode(),t5645rec.sacscode09)
		&& isEQ(acblpf.getSacstyp(),t5645rec.sacstype09)) {
			wsaaAcblSacscode.set(t5645rec.sacscode08);
			wsaaAcblSacstype.set(t5645rec.sacstype08);
			checkAccBalance10000();
		}
	}
	/*                                                         <V4L001>*/
	/* For Advance Premium Deposit, Check which of the 2 APA loan      */
	/* ACBLs we have just cleared and then go and see if the other     */
	/* one is also zero                                        <V4L001>*/
	/*                                                         <V4L001>*/
	if (isEQ(loanpf.getLoantype(),"D")) {
		if (isEQ(acblpf.getSacscode(),t5645rec.sacscode10)
		&& isEQ(acblpf.getSacstyp(),t5645rec.sacstype10)) {
			wsaaAcblSacscode.set(t5645rec.sacscode11);
			wsaaAcblSacstype.set(t5645rec.sacstype11);
			checkAccBalance10000();
		}
		if (isEQ(acblpf.getSacscode(),t5645rec.sacscode11)
		&& isEQ(acblpf.getSacstyp(),t5645rec.sacstype11)) {
			wsaaAcblSacscode.set(t5645rec.sacscode10);
			wsaaAcblSacstype.set(t5645rec.sacstype10);
			checkAccBalance10000();
		}
	}
	if (isEQ(wsaaTurnOffLoan,"Y")) {
		loanpf.setValidflag("2");
		if (isEQ(cashedPojo.getFunction(),"UPDAT")) {
			loanpf.setLtranno(0);
		}
		else {
			loanpf.setLtranno(Long.valueOf(cashedPojo.getTranno()));
		}
	}
	List<Loanpf> insertLoanList = new ArrayList<Loanpf>();
	insertLoanList.add(loanpf);
	loanpfDAO.insertLoanRec(insertLoanList);
	
}

protected void checkAccBalance10000(){
		/* Read ACBL file with given parameters*/
	Acblpf acblpf1 = acblpfDAO.getAcblpfRecord("2", wsaaReadRldgacct.toString(), wsaaAcblSacscode.toString(), 
				wsaaAcblSacstype.toString(),"SGD");
	if(acblpf1 == null){
		wsaaTurnOffLoan.set("Y");
		return;
	}
	if (isEQ(acblpf.getSacscurbal(),ZERO)) {
		wsaaTurnOffLoan.set("Y");
	}
	else {
		wsaaTurnOffLoan.set("N");
	}
}

protected void currencyPosting11000(CashedPojo cashedPojo){
	lifacmvPojo.setContot(0);
	lifacmvPojo.setTermid(varcom.vrcmTermid.toString());
	lifacmvPojo.setUser(varcom.vrcmUser.toInt());
	lifacmvPojo.setTransactionDate(varcom.vrcmDate.toInt());
	lifacmvPojo.setTransactionTime(varcom.vrcmTime.toInt());
	lifacmvPojo.setBatccoy(cashedPojo.getChdrcoy());
	lifacmvPojo.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
	lifacmvPojo.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
	lifacmvPojo.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
	lifacmvPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	lifacmvPojo.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
	lifacmvPojo.setPostmonth(wsaaBatckey.batcBatcactmn.toString());
	lifacmvPojo.setPostyear(wsaaBatckey.batcBatcactyr.toString());
	lifacmvPojo.setRdocnum(wsaaRdockey.rdocRdocnum.toString());
	lifacmvPojo.setRldgcoy(wsaaRldgkey.rldgRldgcoy.toString());
	lifacmvPojo.setRldgacct(wsaaReadRldgacct.toString());
	lifacmvPojo.setGenlcoy(wsaaGlCompany.toString());
	wsaaSequenceNo.add(1);
	lifacmvPojo.setJrnseq(wsaaSequenceNo.toInt());
	lifacmvPojo.setEffdate(Integer.parseInt(cashedPojo.getTrandate()));
	lifacmvPojo.setTrandesc(cashedPojo.getTrandesc());
	lifacmvPojo.setTranref(cashedPojo.getChdrnum());
	lifacmvPojo.setCrate(BigDecimal.ZERO);
	lifacmvPojo.setAcctamt(BigDecimal.ZERO);
	lifacmvPojo.setRcamt(0);
	lifacmvPojo.setFrcdate(99999999);
	lifacmvPojo.setTranno(Integer.parseInt(cashedPojo.getTranno()));
	lifacmvPojo.setFunction("PSTW");
	String[] substituteCodes = new String[6];
	lifacmvPojo.setSubstituteCodes(substituteCodes);
	lifacmvUtils.calcLifacmv(lifacmvPojo);
	if (isNE(lifacmvPojo.getStatuz(),varcom.oK)) {
		syserrrec.params.set(lifacmvPojo);
		syserrrec.statuz.set(lifacmvPojo.getStatuz());
		databaseError99500(cashedPojo,"2");
	}
}

protected void a000CallRounding(ZrdecplcPojo zrdecplcPojo,CashedPojo cashedPojo){
		/*A100-CALL*/
	
	zrdecplcPojo.setCompany(cashedPojo.getChdrcoy());/* IJTI-1523 */
	zrdecplcPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	zrdecplcPojo.setCurrency(acblpf.getOrigcurr());
	zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
	
	if (!Varcom.oK.toString().equals(zrdecplcPojo.getStatuz())) {
		syserrrec.statuz.set(zrdecplcPojo.getStatuz());
		syserrrec.params.set(zrdecplcPojo.toString());
		String errtype = (!syserrrec.statuz.equals("")) || (!syserrrec.syserrStatuz.equals("")) ? "3" : "4";
		databaseError99500(cashedPojo,errtype);
	}
}

protected void databaseError99500(CashedPojo cashedPojo,String syserrType){
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	syserrrec.syserrType.set(syserrType);
	syserr.mainline(new Object[] { syserrrec.syserrRec });
	cashedPojo.setStatuz("BOMB");
	LOGGER.error("COBOLConvCodeModel.callProgram exception", new COBOLExitProgramException());
	throw new RuntimeException(new COBOLExitProgramException());
}
private ApplicationContext getApplicationContext() {
	return IntegralApplicationContext.getApplicationContext();
}
}
