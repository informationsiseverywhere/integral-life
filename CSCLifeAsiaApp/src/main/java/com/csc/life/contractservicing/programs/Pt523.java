/*
 * File: Pt523.java
 * Date: 30 August 2009 2:00:50
 * Author: Quipoz Limited
 * 
 * Class transformed from PT523.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.screens.St523ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*                                                                     *
* </pre>
*/
public class Pt523 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PT523");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaMessages = new FixedLengthStringData(150);
	private FixedLengthStringData[] wsaaMessage = FLSArrayPartOfStructure(3, 50, wsaaMessages, 0);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);
	private ZonedDecimalData wsaaPrimaryTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrimaryKey, 9).setUnsigned();
	private FixedLengthStringData wsaaPrimaryAction = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 14);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 19);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private String g008 = "G008";
	private String t065 = "T065";
	private String e017 = "E017";
	private String h366 = "H366";
	private String g640 = "G640";
	private String g639 = "G639";
	private String f910 = "F910";
	private String f967 = "F967";
	private String w067 = "W067";
	private String tl45 = "TL45";
	private String tl46 = "TL46";
		/* TABLES */
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t5688 = "T5688";
	private String t3629 = "T3629";
	private String tr386 = "TR386";
	private String lifelnbrec = "LIFELNBREC";
	private String tpoldbtrec = "TPOLDBTREC";
	private String cltsrec = "CLTSREC";
	private String covrrec = "COVRREC";
	private String itemrec = "ITEMREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
		/*Policy Debt*/
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	private Tr386rec tr386rec = new Tr386rec();
	private Batckey wsaaBatckey = new Batckey();
	private St523ScreenVars sv = ScreenProgram.getScreenVars( St523ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		read1310, 
		read1410, 
		exit1590, 
		preExit, 
		validateB2030, 
		exit3090
	}

	public Pt523() {
		super();
		screenVars = sv;
		new ScreenModel("St523", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{	
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.tranno.set(ZERO);
		sv.orgamnt.set(ZERO);
		sv.tdbtrate.set(ZERO);
		sv.tdbtamt.set(ZERO);
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		descIO.setRecKeyData(SPACES);
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setDesctabl(t5688);
		getDesc1100();
		sv.ctypedes.set(descIO.getLongdesc());
		getLife1300();
		cltsIO.setRecKeyData(SPACES);
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		getClientDetails1200();
		sv.linsname.set(wsspcomn.longconfname);
		cltsIO.setRecKeyData(SPACES);
		sv.cownnum.set(chdrlnbIO.getCownnum());
		cltsIO.setClntnum(chdrlnbIO.getCownnum());
		getClientDetails1200();
		sv.ownername.set(wsspcomn.longconfname);
		sv.occdate.set(chdrlnbIO.getOccdate());
		descIO.setRecKeyData(SPACES);
		descIO.setDescitem(chdrlnbIO.getStatcode());
		descIO.setDesctabl(t3623);
		getDesc1100();
		sv.rstate.set(descIO.getShortdesc());
		descIO.setRecKeyData(SPACES);
		descIO.setDescitem(chdrlnbIO.getPstatcode());
		descIO.setDesctabl(t3588);
		getDesc1100();
		sv.pstate.set(descIO.getShortdesc());
		sv.ptdate.set(chdrlnbIO.getPtdate());
		sv.btdate.set(chdrlnbIO.getBtdate());
		if (isNE(sv.ptdate,sv.btdate)) {
			sv.ptdateErr.set(g008);
			sv.btdateErr.set(g008);
		}
		if (isEQ(chdrlnbIO.getPtdate(),chdrlnbIO.getCcdate())) {
			sv.ptdateErr.set(t065);
		}
		descIO.setRecKeyData(SPACES);
		sv.currcd.set(chdrlnbIO.getCntcurr());
		descIO.setDescitem(chdrlnbIO.getCntcurr());
		descIO.setDesctabl(t3629);
		getDesc1100();
		sv.currds.set(descIO.getShortdesc());
		sv.tranno.set(chdrlnbIO.getTranno());
		sv.validflag.set(chdrlnbIO.getValidflag());
		sv.effdate.set(wsspcomn.currfrom);
		sv.fromdate.set(varcom.vrcmMaxDate);
		sv.todate.set(varcom.vrcmMaxDate);
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itemIO.setStatuz(varcom.oK);
		iy.set(1);
		initialize(wsaaMessages);
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			loadTr3861500();
		}
		
		if (isEQ(wsspcomn.sbmaction,"A")) {
			sv.sdei.set(wsaaMessage[1]);
			sv.orgamntOut[varcom.nd.toInt()].set(SPACES);
		}
		else {
			sv.sdei.set(wsaaMessage[2]);
			sv.orgamntOut[varcom.nd.toInt()].set("Y");
		}
		getRiskCessdate1400();
	}

protected void getDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		read1210();
	}

protected void read1210()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			wsspcomn.longconfname.fill("?");
		}
		else {
			plainname();
		}
	}

protected void getLife1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					getLifePara1300();
				}
				case read1310: {
					read1310();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getLifePara1300()
	{
		lifelnbIO.setRecKeyData(SPACES);
		lifelnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void read1310()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrcoy(),lifelnbIO.getChdrcoy())
		|| isNE(chdrlnbIO.getChdrnum(),lifelnbIO.getChdrnum())
		|| isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isNE(lifelnbIO.getValidflag(),"1")) {
			lifelnbIO.setFunction(varcom.nextr);
			goTo(GotoLabel.read1310);
		}
		/*EXIT*/
	}

protected void getRiskCessdate1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1400();
				}
				case read1410: {
					read1410();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1400()
	{
		covrIO.setDataKey(SPACES);
		covrIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covrIO.setChdrnum(chdrlnbIO.getChdrnum());
		covrIO.setLife(lifelnbIO.getLife());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrIO.setFormat(covrrec);
	}

protected void read1410()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrcoy(),covrIO.getChdrcoy())
		|| isNE(chdrlnbIO.getChdrnum(),covrIO.getChdrnum())
		|| isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if (isNE(covrIO.getValidflag(),"1")) {
			covrIO.setFunction(varcom.nextr);
			goTo(GotoLabel.read1410);
		}
		/*EXIT*/
	}

protected void loadTr3861500()
	{
		try {
			start1510();
		}
		catch (GOTOException e){
		}
	}

protected void start1510()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(),wsspcomn.company)
		|| isNE(itemIO.getItemtabl(),tr386)
		|| isNE(itemIO.getItemitem(),wsaaTr386Key)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			if (isEQ(itemIO.getFunction(),varcom.begn)) {
				itemIO.setItemitem(wsaaTr386Key);
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit1590);
			}
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		ix.set(1);
		while ( !(isGT(ix,10))) {
			if (isNE(tr386rec.progdesc[ix.toInt()],SPACES)
			&& isLT(iy,4)) {
				wsaaMessage[iy.toInt()].set(tr386rec.progdesc[ix.toInt()]);
				iy.add(1);
			}
			ix.add(1);
		}
		
		itemIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case validateB2030: {
					validateB2030();
					checkForErrors2080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isNE(wsspcomn.sbmaction,"A")) {
			goTo(GotoLabel.validateB2030);
		}
		if (isLT(sv.fromdate,sv.btdate)) {
			sv.fromdateErr.set(tl45);
		}
		if (isEQ(sv.fromdate,0)
		|| isEQ(sv.fromdate,varcom.vrcmMaxDate)) {
			sv.fromdateErr.set(g640);
		}
		if (isGT(sv.fromdate,sv.todate)) {
			sv.fromdateErr.set(e017);
			sv.todateErr.set(e017);
		}
		if (isEQ(sv.todate,0)
		|| isEQ(sv.todate,varcom.vrcmMaxDate)) {
			sv.todateErr.set(g639);
		}
		if (isGT(sv.todate,covrIO.getRiskCessDate())) {
			sv.todateErr.set(tl46);
		}
	}

protected void validateB2030()
	{
		if (isEQ(sv.tdbtdesc,SPACES)) {
			sv.tdbtdescErr.set(h366);
		}
		if (isLT(sv.orgamnt,0)) {
			sv.orgamntErr.set(f967);
		}
		if (isLT(sv.tdbtrate,0)) {
			sv.tdbtrateErr.set(f967);
		}
	}

protected void checkForErrors2080()
	{
		if (isEQ(sv.errorIndicators,SPACES)) {
			calcDebt2100();
		}
		if (isEQ(sv.tdbtamt,0)) {
			sv.tdbtamtErr.set(w067);
		}
		if (isNE(sv.errorIndicators,SPACES)
		|| isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void calcDebt2100()
	{
		start2110();
	}

protected void start2110()
	{
		if (isEQ(wsspcomn.sbmaction,"A")) {
			sv.tdbtamt.set(ZERO);
			initialize(datcon3rec.datcon3Rec);
			datcon3rec.intDate1.set(sv.fromdate);
			datcon3rec.intDate2.set(sv.todate);
			datcon3rec.frequency.set("DY");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				syserrrec.statuz.set(datcon3rec.statuz);
				fatalError600();
			}
			compute(sv.tdbtamt, 6).setRounded(div(mult(div(mult(sv.orgamnt,sv.tdbtrate),100),datcon3rec.freqFactor),365));
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
		tpoldbtIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		tpoldbtIO.setChdrnum(sv.chdrnum);
		tpoldbtIO.setTranno(sv.tranno);
		tpoldbtIO.setCnttype(sv.cnttype);
		tpoldbtIO.setEffdate(sv.effdate);
		tpoldbtIO.setOrgamnt(sv.orgamnt);
		tpoldbtIO.setTdbtdesc(sv.tdbtdesc);
		tpoldbtIO.setTdbtrate(sv.tdbtrate);
		tpoldbtIO.setTdbtamt(sv.tdbtamt);
		tpoldbtIO.setFromdate(sv.fromdate);
		tpoldbtIO.setTodate(sv.todate);
		tpoldbtIO.setValidflag("1");
		tpoldbtIO.setFunction(varcom.writr);
		tpoldbtIO.setFormat(tpoldbtrec);
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			fatalError600();
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sv.chdrnum);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("PT523AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrcoy.set(chdrlnbIO.getChdrcoy());
		wsaaPrimaryChdrnum.set(sv.chdrnum);
		wsaaPrimaryTranno.set(sv.tranno);
		wsaaPrimaryAction.set(wsspcomn.sbmaction);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaCntcurr.set(sv.currcd);
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		atreqrec.transArea.set(wsaaTransactionRec);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
