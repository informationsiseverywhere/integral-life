package com.csc.life.contractservicing.dataaccess.model;

import java.math.BigDecimal;

public class Cocontpf {
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String datercvd;
	private BigDecimal cocontamnt;
	private BigDecimal liscamnt;
	

	public BigDecimal getCocontamnt() {
		return cocontamnt;
	}
	public void setCocontamnt(BigDecimal cocontamnt) {
		this.cocontamnt = cocontamnt;
	}
	public BigDecimal getLiscamnt() {
		return liscamnt;
	}
	public void setLiscamnt(BigDecimal liscamnt) {
		this.liscamnt = liscamnt;
	}
	private String prcdflag;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getDatercvd() {
		return datercvd;
	}
	public void setDatercvd(String datercvd) {
		this.datercvd = datercvd;
	}

	public String getPrcdflag() {
		return prcdflag;
	}
	public void setPrcdflag(String prcdflag) {
		this.prcdflag = prcdflag;
	}

}
