/*
 * File: Revodue.java
 * Date: 30 August 2009 2:10:15
 * Author: Quipoz Limited
 *
 * Class transformed from REVODUE.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuarevTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrvsTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.dataaccess.PrmhactTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.Ta524rec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.terminationclaims.procedures.Calprpm;
import com.csc.life.terminationclaims.recordstructures.Calprpmrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
* <pre>
*REMARKS
*
*  REVODUE - Reversal of Overdue processing.
* -----------------------------------------
*
*  This subroutine is called by the Full Contract Reversal
*  program P5155 via Table T6661. The parameter passed to this
*  subroutine is contained in the REVERSEREC copybook.
*
*  The processing incorporates all that is done in the REVLAPS
*  reversal subroutine combined with all that is done in the
*  REVPLOAN subroutine. This means that it is capable of
*  reversing any transaction that can have been triggered
*  automatically out of overdue processing ie. Automatic
*  Paid Up Processing, Automatic Policy Loan processing
*  and Automatic Lapse processing.
*
*  The following transactions are reversed :
*
*
*  - CHDR
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*            relevant contract.
*       DELET this record.
*       NEXTR on  the  Contract  Header record (CHDRLIF) for the
*            same contract. This should read a record with
*            valid flag of '2'. If not, then this is an error.
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*            altering  the  valid  flag from '2' to '1'.
*
*  - COVRs
*
*       Only reverse COVR records whose TRANNO is equal to the
*           Transaction number (of the PTRN) being reversed.
*       READH on  the  COVR  records for the relevant contract.
*       DELET this record.
*       NEXTR on  the  COVR  file should read a record with valid
*            flag  of  '2'. If not, then this is an error.
*       REWRT this  COVR  record after altering the valid flag
*            from  '2'  to  '1' . This will reinstate the
*            coverage prior to the LAPSE/PUP.
*
*  - UTRNs
*
*       Call the T5671 generic subroutine to reverse Unit
*       transactions for the TRANNO being reversed.
*       The subroutine GREVUTRN should be entered in the
*       relevant table entry.
*
*  - ACMVs
*
*       Do a BEGNH on the ACMV file for this transaction no. and
*       call LIFACMV to post the reversal to the sub-account.
*
*  - AGCMs
*
*       Read the first AGCM record (AGCMRVS).
*       Delete this record (via AGCMDBC).
*       Read the next AGCM looking for valid flag 2 records.
*       Update these AGCM records with valid flag 1.
*
*  - PAYR
*
*       REWRITE the PAYR record with the current TRANNO.
*
*  - LOANs
*
*       For any loans that were automatically issued
*       out of overdue processing (APLs), read the loan record
*       and delete the LOAN record from LOANPF.
*
*       For any loans which were 'switched off' during
*       overdue processing, reinstate the LOAN by setting
*       the VALID FLAG to '1' and set the LAST TRANNO to zeroes.
*       Note this should only be done where the LAST TRANNO
*       matches the TRANNO of the 'forward' transaction
*       currently being reversed, indicating that this
*       transaction actually 'switched off' the loan.
*
*****************************************************
* </pre>
*/
public class Revodue extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVODUE";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaReversedTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
		/*                                                         <R90>   */
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private PackedDecimalData wsaaLstInterestDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNxtInterestDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLoanno = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private ZonedDecimalData wsaaRldgLoannocv = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgLoanno, 0).setUnsigned();
	private FixedLengthStringData wsaaRldgRest = new FixedLengthStringData(6).isAPartOf(wsaaRldgacct, 10);

	private FixedLengthStringData wsaaLoanLoanno = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaLoanLoannum = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanLoanno, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaLoanFlag = new FixedLengthStringData(1).init("N");
	private Validator noLoans = new Validator(wsaaLoanFlag, "N");
	private Validator loanFound = new Validator(wsaaLoanFlag, "Y");

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";

		/*                                                         <R90>   */
	private FixedLengthStringData wsaaAcmvTrcde = new FixedLengthStringData(4).init(SPACES);
	private Validator loanInterestBilling = new Validator(wsaaAcmvTrcde, "BA69");
	private Validator loanInterestCapital = new Validator(wsaaAcmvTrcde, "BA68");
		/* ERRORS */
	private static final String e723 = "E723";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5671 = "T5671";
	private static final String t6633 = "T6633";
	private static final String t1688 = "T1688";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private AgcmrvsTableDAM agcmrvsIO = new AgcmrvsTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private HpuarevTableDAM hpuarevIO = new HpuarevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private PrmhactTableDAM prmhactIO = new PrmhactTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T6633rec t6633rec = new T6633rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();
	private boolean prmhldtrad = false;
	private boolean covrReinstated = false;
	private HashMap<Long,List<Covrpf>> reratedPremKeeps  = new LinkedHashMap<>();
	private  List<Covrpf> covrpfwopl = new ArrayList<Covrpf>();
	private Calprpmrec calprpmrec = new Calprpmrec();
	private List<Covrpf> covrUpdateList;
	private List<Covrpf> covrInsertList;
	private List<Covrpf> covrpfList;
	private List<Incrpf> insertIncrList;
	private Payrpf payrpf;
	private List<Payrpf> payrpfList;
	private List<Chdrpf> chdrpfList;
	private Ta524rec ta524rec = new Ta524rec();
	private static final String ta524 = "TA524";
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Calprpm calprpmph = getApplicationContext().getBean("prorateCalcUtils", Calprpm.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		processComponent3050,
		exit3090,
		findRecordToReverse5120
	}

	public Revodue() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		syserrrec.subrname.set(wsaaSubr);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		wsaaRevTrandesc.set(descIO.getLongdesc());
		wsaaLoanFlag.set("N");
	}

protected void process2000()
	{
		process2001();
	}

protected void process2001()
	{
		processChdrs2100();
		processCovrs3000();
		processAcmvs4000();
		processAcmvOptical4010();
		processAgcms5000();
		if (loanFound.isTrue()) {
			processLoan6000();
		}
		reinstateLoans7000();
		processPayrs8000();
		processHcsds8100();
		processPrmhact8200();
		if(prmhldtrad) {
			Itempf itempf = null;
			itempf = itempfDAO.findItemByDate("IT", chdrlifIO.getChdrcoy().toString(), ta524, chdrlifIO.getCnttype().toString(), reverserec.effdate1.toString(), "1");
			if(itempf != null)
				ta524rec.ta524Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			if(isNE(ta524rec.ta524Rec, SPACES)) {
				calcProrate();
			}
		}
	}

protected void processChdrs2100()
	{
		start2101();
	}

protected void start2101()
	{
		/*  delete valid flag 1 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getValidflag(), 1)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		wsaaReversedTranno.set(chdrlifIO.getTranno());
		chdrlifIO.setFunction(varcom.delet);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*    Select the next record which will be the valid flag 2 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		/* MOVE BEGNH                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrlifIO.getValidflag(), 2)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.writd);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		prmhldtrad = FeaConfg.isFeatureExist(reverserec.company.toString(), "CSOTH010", appVars, "IT");
	}

protected void processCovrs3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					covers3001();
				case processComponent3050:
					processComponent3050();
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covers3001()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrIO.getStatuz(), varcom.endp)
		|| isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void processComponent3050()
	{
		/*    If this component has a TRANNO equal to the TRANNO we are*/
		/*    reversing, then it has been Lapsed/Paid-up and must now be*/
		/*    reversed.  If not, then find the next component.*/
		wsaaCovrKey.set(covrIO.getDataKey());
		if (isEQ(covrIO.getTranno(), reverserec.tranno)) {
			deleteAndUpdatComp3200();
			/*     PERFORM 3250-REWRITE-PAYR                                */
			h100ProcessHpua();
			processGenericSubr3300();
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp)
		|| isNE(covrIO.getDataKey(), wsaaCovrKey))) {
			getNextCovr3500();
		}

		if (isNE(covrIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.processComponent3050);
		}
	}

protected void deleteAndUpdatComp3200()
	{
		deleteRecs3201();
	}

protected void deleteRecs3201()
	{
		if (isNE(covrIO.getValidflag(), 1)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/* valid flag equal to 1*/
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.delet);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getValidflag(),2)
		|| isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/* MOVE REVE-TRANS-DATE        TO COVR-TRANSACTION-DATE         */
		/* MOVE REVE-TRANS-TIME        TO COVR-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO COVR-USER.                    */
		/* MOVE REVE-TERMID            TO COVR-TERMID.                  */
		/* MOVE REVE-NEW-TRANNO        TO COVR-TRANNO.                  */
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	*3250-REWRITE-PAYR SECTION.
	*3250-START.
	***READH and REWRT the PAYR record with the new TRANNO.
	**** MOVE SPACES                 TO PAYRLIF-DATA-KEY.
	**** MOVE COVR-CHDRCOY           TO PAYRLIF-CHDRCOY.
	**** MOVE COVR-CHDRNUM           TO PAYRLIF-CHDRNUM.
	**** MOVE COVR-PAYRSEQNO         TO PAYRLIF-PAYRSEQNO.
	**** MOVE PAYRLIFREC             TO PAYRLIF-FORMAT.
	**** MOVE READH                  TO PAYRLIF-FUNCTION.
	**** CALL 'PAYRLIFIO'            USING PAYRLIF-PARAMS.
	**** IF PAYRLIF-STATUZ              NOT = O-K
	****    MOVE PAYRLIF-PARAMS         TO SYSR-PARAMS
	****    MOVE PAYRLIF-STATUZ         TO SYSR-STATUZ
	****    PERFORM XXXX-FATAL-ERROR.
	**** IF PAYRLIF-VALIDFLAG           NOT = 1
	****    MOVE PAYRLIF-PARAMS         TO SYSR-PARAMS
	****    PERFORM XXXX-FATAL-ERROR.
	**** MOVE REVE-TRANNO            TO PAYRLIF-TRANNO.
	**** MOVE REWRT                  TO PAYRLIF-FUNCTION.
	**** MOVE PAYRLIFREC             TO PAYRLIF-FORMAT.
	**** CALL 'PAYRLIFIO'            USING PAYRLIF-PARAMS.
	**** IF PAYRLIF-STATUZ              NOT = O-K
	****    MOVE PAYRLIF-PARAMS         TO SYSR-PARAMS
	****    MOVE PAYRLIF-STATUZ         TO SYSR-STATUZ
	****    PERFORM XXXX-FATAL-ERROR.
	*3259-EXIT.
	**** EXIT.
	* </pre>
	*/
protected void processGenericSubr3300()
	{
		readGenericTable3301();
	}

protected void readGenericTable3301()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Trancode.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		/* MOVE ZEROES                 TO GREV-PLAN-SUFFIX.             */
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.life.set(covrIO.getLife());
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callSubprog3400();
		}
	}

protected void callSubprog3400()
	{
		/*GO*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
		}
		if (isNE(greversrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(greversrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void getNextCovr3500()
	{
		/*GET-RECS*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void h100ProcessHpua()
	{
		h101Start();
	}

protected void h101Start()
	{
		hpuarevIO.setDataArea(SPACES);
		hpuarevIO.setChdrcoy(reverserec.company);
		hpuarevIO.setChdrnum(reverserec.chdrnum);
		hpuarevIO.setLife(covrIO.getLife());
		hpuarevIO.setCoverage(covrIO.getCoverage());
		hpuarevIO.setRider(covrIO.getRider());
		hpuarevIO.setPlanSuffix(covrIO.getPlanSuffix());
		hpuarevIO.setTranno(reverserec.tranno);
		hpuarevIO.setFunction(varcom.begn);
		hpuarevIO.setFormat(formatsInner.hpuarevrec);
		/* Read sequentially until key break or ENDP to delete             */
		/* validflag '1' HPUA records and reinstate the most recent        */
		/* validflag '2' HPUA records.                                     */
		while ( !(isEQ(hpuarevIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, hpuarevIO);
			if (isNE(hpuarevIO.getStatuz(), varcom.oK)
			&& isNE(hpuarevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(hpuarevIO.getParams());
				xxxxFatalError();
			}
			if (isNE(reverserec.company,hpuarevIO.getChdrcoy())
			|| isNE(reverserec.chdrnum,hpuarevIO.getChdrnum())
			|| isNE(covrIO.getLife(),hpuarevIO.getLife())
			|| isNE(covrIO.getCoverage(),hpuarevIO.getCoverage())
			|| isNE(covrIO.getRider(),hpuarevIO.getRider())
			|| isNE(covrIO.getPlanSuffix(),hpuarevIO.getPlanSuffix())
			|| isNE(reverserec.tranno,hpuarevIO.getTranno())) {
				hpuarevIO.setStatuz(varcom.endp);
			}
			if (isEQ(hpuarevIO.getStatuz(), varcom.oK)) {
				h170DeleteUpdateHpua();
			}
			hpuarevIO.setFunction(varcom.nextr);
		}

	}

protected void h170DeleteUpdateHpua()
	{
		h170Start();
	}

protected void h170Start()
	{
		hpuaIO.setParams(SPACES);
		hpuaIO.setRrn(hpuarevIO.getRrn());
		hpuaIO.setFormat(formatsInner.hpuarec);
		hpuaIO.setFunction(varcom.readd);
		h199CallHpuaio();
		hpuaIO.setFunction(varcom.deltd);
		h199CallHpuaio();
		hpuaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)
		&& isNE(hpuaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			xxxxFatalError();
		}
		if (isNE(hpuaIO.getChdrcoy(), hpuarevIO.getChdrcoy())
		|| isNE(hpuaIO.getChdrnum(), hpuarevIO.getChdrnum())
		|| isNE(hpuaIO.getLife(), hpuarevIO.getLife())
		|| isNE(hpuaIO.getCoverage(), hpuarevIO.getCoverage())
		|| isNE(hpuaIO.getRider(), hpuarevIO.getRider())
		|| isNE(hpuaIO.getPlanSuffix(), hpuarevIO.getPlanSuffix())
		|| isNE(hpuaIO.getPuAddNbr(), hpuarevIO.getPuAddNbr())
		|| isEQ(hpuaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		else {
			if (isEQ(hpuaIO.getValidflag(), "2")) {
				hpuaIO.setValidflag("1");
				hpuaIO.setFunction(varcom.writd);
				h199CallHpuaio();
			}
			/**        IF HPUA-VALIDFLAG   NOT = '2'                    <C16N>  */
			/**            MOVE HPUA-PARAMS    TO SYSR-PARAMS           <C16N>  */
			/**            PERFORM XXXX-FATAL-ERROR                     <C16N>  */
			/**        END-IF                                           <C16N>  */
			/**        MOVE '1'                TO HPUA-VALIDFLAG        <C16N>  */
			/**        MOVE WRITD              TO HPUA-FUNCTION         <C16N>  */
			/**        PERFORM H199-CALL-HPUAIO                         <C16N>  */
		}
	}

protected void h199CallHpuaio()
	{
		/*H199-START*/
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpuaIO.getParams());
			xxxxFatalError();
		}
		/*H199-EXIT1*/
	}

protected void processAcmvs4000()
	{
		readSubAcctTable4001();
	}

protected void readSubAcctTable4001()
	{
		/*    UNITDEAL will reverse all the ACMVs created by B5463, so*/
		/*    here we reverse all other ACMVs.*/
		wsaaRldgacct.set(SPACES);
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}

	}

protected void processAcmvOptical4010()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			xxxxFatalError();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}

		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
		}
	}

protected void reverseAcmvs4100()
	{
		start4100();
		readNextAcmv4180();
	}

protected void start4100()
	{
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		if (isNE(acmvrevIO.getRcamt(), 0)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaRevTrandesc);
		/* MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			xxxxFatalError();
		}
		/*    MOVE SPACES                 TO WSAA-RLDGACCT.                */
		if (isNE(acmvrevIO.getRldgacct(), SPACES)) {
			wsaaRldgacct.set(acmvrevIO.getRldgacct());
		}
		/*  Check to see if this is a policy loan ACMV. This will be the*/
		/*  case if the loan number is present and the rest of the entity*/
		/*  is blank.*/
		if (isNE(wsaaRldgLoanno, SPACES)
		&& isEQ(wsaaRldgRest, SPACES)) {
			wsaaLoanLoanno.set(wsaaRldgLoanno);
			wsaaLoanFlag.set("Y");
		}
	}

protected void readNextAcmv4180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAgcms5000()
	{
		agcms5001();
	}

protected void agcms5001()
	{
		agcmrvsIO.setParams(SPACES);
		agcmrvsIO.setChdrcoy(reverserec.company);
		agcmrvsIO.setChdrnum(reverserec.chdrnum);
		agcmrvsIO.setTranno(reverserec.tranno);
		agcmrvsIO.setFormat(formatsInner.agcmrvsrec);
		agcmrvsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrvsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(), varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmrvsIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(agcmrvsIO.getChdrcoy(), reverserec.company)
		|| isNE(agcmrvsIO.getTranno(), reverserec.tranno)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmrvsIO.getStatuz(), varcom.endp))) {
			reverseAgcm5100();
		}

	}

protected void reverseAgcm5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					deleteRecs5101();
				case findRecordToReverse5120:
					findRecordToReverse5120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void deleteRecs5101()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/*    Delete this record*/
		agcmdbcIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/*    valid flag 2 record that was changed by the original*/
		/*    Lapse/PUP transaction.  If this contract has had single*/
		/*    premium top-ups or lump sums at contract issue, we may find*/
		/*    a valid flag 1 record for the key we are reading.  Ignore*/
		/*    these records and continue the quest for the valid flag 2*/
		/*    record.*/
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmdbcIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void findRecordToReverse5120()
	{
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getChdrcoy(), agcmrvsIO.getChdrcoy())
		|| isNE(agcmdbcIO.getChdrnum(), agcmrvsIO.getChdrnum())
		|| isNE(agcmdbcIO.getAgntnum(), agcmrvsIO.getAgntnum())
		|| isNE(agcmdbcIO.getLife(), agcmrvsIO.getLife())
		|| isNE(agcmdbcIO.getCoverage(), agcmrvsIO.getCoverage())
		|| isNE(agcmdbcIO.getRider(), agcmrvsIO.getRider())
		|| isNE(agcmdbcIO.getPlanSuffix(), agcmrvsIO.getPlanSuffix())
		|| isNE(agcmdbcIO.getSeqno(), agcmrvsIO.getSeqno())) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getValidflag(), "2")) {
			agcmdbcIO.setFunction(varcom.nextr);
			goTo(GotoLabel.findRecordToReverse5120);
		}
		/*  Update The Selected Record on the Coverage File.*/
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmrvsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(), varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(agcmrvsIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(agcmrvsIO.getChdrcoy(), reverserec.company)
		|| isNE(agcmrvsIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(agcmrvsIO.getTranno(), reverserec.tranno)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
	}

protected void processLoan6000()
	{
		start6000();
	}

	/**
	* <pre>
	*  Look for any loans which were encountered in the ACMV
	*  processing. This will indicate a LOAN which was
	*  actually set up as a result of OVERDUE processing and
	*  this LOAN record can now be deleted.
	*  NOTE this is different to finding loans which were
	*  already in existence at the time of the OVERDUE
	*  processing and which were switched off as a result of
	*  the contract being LAPSED. Such loans must be reinstated
	*  during the reversal and are done so separately later in
	*  this program.
	* </pre>
	*/
protected void start6000()
	{
		loanIO.setDataArea(SPACES);
		loanIO.setChdrcoy(reverserec.company);
		loanIO.setChdrnum(reverserec.chdrnum);
		/*    MOVE WSAA-RLDG-LOANNO       TO LOAN-LOAN-NUMBER.             */
		loanIO.setLoanNumber(wsaaLoanLoannum);
		loanIO.setFunction(varcom.readh);
		loanIO.setFormat(formatsInner.loanrec);
		SmartFileCode.execute(appVars, loanIO);
		if (isEQ(loanIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			xxxxFatalError();
		}
		/* found LOAN record so now delete it*/
		loanIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void reinstateLoans7000()
	{
		/*START*/
		/*  BEGN on the LOAN file using COMPANY, CHDRNUM and LOAN NUMBER*/
		/*  of zeroes.*/
		loanenqIO.setDataArea(SPACES);
		loanenqIO.setChdrcoy(reverserec.company);
		loanenqIO.setChdrnum(reverserec.chdrnum);
		loanenqIO.setLoanNumber(ZERO);
		/* MOVE BEGNH                      TO LOANENQ-FUNCTION. <LA3993>*/
		loanenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loanenqIO.setFormat(formatsInner.loanenqrec);
		while ( !(isEQ(loanenqIO.getStatuz(), varcom.endp))) {
			processLoans7100();
		}

		/*EXIT*/
	}

protected void processLoans7100()
	{
		start7100();
	}

protected void start7100()
	{
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(), varcom.oK)
		&& isNE(loanenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(loanenqIO.getChdrcoy(), reverserec.company)
		|| isNE(loanenqIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(loanenqIO.getStatuz(), varcom.endp)) {
			loanenqIO.setStatuz(varcom.endp);
			return ;
		}
		/*        MOVE ENDP                   TO LOANENQ-STATUZ.   <R69>   */
		/* get here, so we have found a Loan record.......                 */
		/*  does it need changing ???                                      */
		/*  check this by comparing the Last Tranno on the Loan record     */
		/* IF it matches, we will update record, otherwise we will NEXTR   */
		if (isEQ(loanenqIO.getLastTranno(), wsaaReversedTranno)) {
			obtainInterestDates7200();
			loanenqIO.setLastIntBillDate(wsaaLstInterestDate);
			loanenqIO.setNextIntBillDate(wsaaNxtInterestDate);
			loanenqIO.setValidflag("1");
			loanenqIO.setLastTranno(ZERO);
			/*     MOVE REWRT              TO LOANENQ-FUNCTION      <LA3993>*/
			loanenqIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, loanenqIO);
			if (isNE(loanenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(loanenqIO.getParams());
				syserrrec.statuz.set(loanenqIO.getStatuz());
				xxxxFatalError();
			}
		}
		loanenqIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*7200-SWITCH-ON-LOANS SECTION.
	*  If this loan has a LAST TRANNO = TRANNO of current forward
	*  transaction, set the LAST TRANNO to zeroes and set VALID
	*  FLAG to '1'.
	*  Read the next loan record.
	*7200-START.
	**** IF  LOANENQ-LAST-TRANNO         = REVE-TRANNO
	****     MOVE ZEROES                 TO LOANENQ-LAST-TRANNO
	****     MOVE '1'                    TO LOANENQ-VALIDFLAG
	**** END-IF.
	*    Rewrite the record (this is necessary even if the record
	*    has not been updated to release it from being held).
	**** MOVE REWRT                      TO LOANENQ-FUNCTION
	**** CALL 'LOANENQIO' USING LOANENQ-PARAMS.
	**** IF  LOANENQ-STATUZ          NOT = O-K
	****     MOVE LOANENQ-PARAMS     TO SYSR-PARAMS
	****     MOVE LOANENQ-STATUZ     TO SYSR-STATUZ
	****     PERFORM XXXX-FATAL-ERROR.
	*  Read the next loan.
	**** MOVE NEXTR                      TO LOANENQ-FUNCTION.
	**** CALL 'LOANENQIO' USING LOANENQ-PARAMS.
	**** IF  LOANENQ-STATUZ          NOT = O-K
	**** AND LOANENQ-STATUZ          NOT = ENDP
	****     MOVE LOANENQ-PARAMS         TO SYSR-PARAMS
	****     MOVE LOANENQ-STATUZ         TO SYSR-STATUZ
	****     PERFORM XXXX-FATAL-ERROR.
	**** IF  LOANENQ-CHDRCOY         NOT = REVE-COMPANY
	**** OR  LOANENQ-CHDRNUM         NOT = REVE-CHDRNUM
	****     MOVE ENDP                   TO LOANENQ-STATUZ.
	*6290-EXIT.
	*7290-EXIT.                                               <R90>
	**** EXIT.
	* </pre>
	*/
protected void obtainInterestDates7200()
	{
		start7200();
	}

protected void start7200()
	{
		readT5645Table7300();
		/* Use entries on the T5645 record read to get Loan interest       */
		/*  ACMVs for this loan number.                                    */
		if (isEQ(loanenqIO.getLoanType(), "P")) {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstype.set(t5645rec.sacstype01);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstype.set(t5645rec.sacstype02);
		}
		wsaaRldgacct.set(SPACES);
		wsaaLstInterestDate.set(ZERO);
		wsaaRldgChdrnum.set(loanenqIO.getChdrnum());
		wsaaRldgLoanno.set(loanenqIO.getLoanNumber());
		/*  We will begin reading on the ACMV original surrender           */
		/*  transactions and read backwards to get the movements           */
		/*  before the original surrender (nextp)                          */
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(loanenqIO.getChdrnum());
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvrevIO.setFitKeysSearch("RDOCNUM");
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			searchAcmvRecs7400();
		}

		/*  if any interest records for the loan being processed           */
		/*  (before the surrender movements) i.e there is a value          */
		/*  in WSAA-LST-INTEREST-DATE use this otherwise no interest       */
		/*  has been calculated since the loan start date                  */
		if (isEQ(wsaaLstInterestDate, ZERO)) {
			wsaaLstInterestDate.set(loanenqIO.getLoanStartDate());
		}
		/*  calculate next interest date from the last interest            */
		/*  date retrieved (i.e the effective date on the relevant         */
		/*  acmv                                                           */
		readT6633Table7500();
		calcNextInterestDate7600();
	}

protected void readT5645Table7300()
	{
		start7300();
	}

protected void start7300()
	{
		/*  Read the accounting rules table                                */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(loanenqIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void searchAcmvRecs7400()
	{
		start7400();
	}

protected void start7400()
	{
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(acmvrevIO.getRldgcoy(), loanenqIO.getChdrcoy())
		|| isNE(acmvrevIO.getRdocnum(), loanenqIO.getChdrnum())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		/*  Read the accounting rules table                                */
		wsaaAcmvTrcde.set(acmvrevIO.getBatctrcde());
		if (isLT(acmvrevIO.getTranno(), reverserec.tranno)
		&& isEQ(acmvrevIO.getSacscode(), wsaaSacscode)
		&& isEQ(acmvrevIO.getSacstyp(), wsaaSacstype)
		&& isEQ(acmvrevIO.getRldgacct(), wsaaRldgacct)
		&& (loanInterestBilling.isTrue()
		|| loanInterestCapital.isTrue())) {
			wsaaLstInterestDate.set(acmvrevIO.getEffdate());
			acmvrevIO.setStatuz(varcom.endp);
		}
		else {
			acmvrevIO.setFunction(varcom.nextp);
		}
	}

protected void readT6633Table7500()
	{
		start7500();
	}

protected void start7500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(loanenqIO.getChdrcoy());
		itdmIO.setItemtabl(t6633);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(wsaaLstInterestDate);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), loanenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6633)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e723);
			xxxxFatalError();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
	}

protected void calcNextInterestDate7600()
	{
		start7600();
	}

protected void start7600()
	{
		/* Check the interest  details on T6633 in the following           */
		/*  order: i) Calculate interest on Loan anniv ... Y/N             */
		/*        ii) Calculate interest on Policy anniv.. Y/N             */
		/*       iii) Check Int freq & whether a specific Day is chosen    */
		wsaaLoanDate.set(loanenqIO.getLoanStartDate());
		wsaaEffdate.set(wsaaLstInterestDate);
		wsaaContractDate.set(chdrlifIO.getOccdate());
		/* Check for loan anniversary flag set                             */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next loan anniv  */
		/*     date after the Effective date we are using now.             */
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(loanenqIO.getLoanStartDate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
				callDatcon47700();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}

			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Check for contract anniversary flag set                         */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next contract    */
		/*     anniversary date after the Effective date we are using now. */
		if (isEQ(t6633rec.policyAnnivInterest, "Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(chdrlifIO.getOccdate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
				callDatcon47700();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}

			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Get here so the next interest calc. date isn't based on loan    */
		/*  or contract anniversarys.                                      */
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified     */
		/* ...if not, use the Loan day                                     */
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet7800();
		}
		else {
			if (isNE(t6633rec.interestDay, ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate2.set(ZERO);
		datcon4rec.intDate1.set(wsaaNewDate);
		datcon4rec.freqFactor.set(1);
		/* check if table T6633 has a fixed frequency for interest calcs,  */
		/* ...if not, use 1 year as the default interest calc. frequency.  */
		if (isEQ(t6633rec.interestFrequency, SPACES)) {
			datcon4rec.frequency.set("01");
		}
		else {
			datcon4rec.frequency.set(t6633rec.interestFrequency);
		}
		while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
			callDatcon47700();
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}

		wsaaNxtInterestDate.set(datcon4rec.intDate2);
	}

protected void callDatcon47700()
	{
		/*START*/
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void dateSet7800()
	{
		/*START*/
		/* we have to check that the date we are going to call Datcon4     */
		/*  with is a valid from-date... ie IF the interest/capn day in    */
		/*  T6633 is > 28, we have to make sure the from-date isn't        */
		/*  something like 31/02/nnnn or 31/06/nnnn                        */
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
	}

protected void processPayrs8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* Read PAYR file for the validflag '1' record.                    */
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		payrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getValidflag(), "1")) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		/* If the record was not created by the transaction being          */
		/* reversed, then there are no PAYR records to reverse.            */
		if (isNE(payrlifIO.getTranno(), reverserec.tranno)) {
			return ;
		}
		payrlifIO.setFunction(varcom.readh);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setFunction(varcom.delet);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		/* Reinstate the validflag '2' record.                             */
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		/* MOVE BEGNH                  TO PAYRLIF-FUNCTION.     <LA3993>*/
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(), "2")) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO PAYRLIF-FUNCTION.     <LA3993>*/
		payrlifIO.setFunction(varcom.writd);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
	}

protected void processHcsds8100()
	{
		start8110();
	}

protected void start8110()
	{
		/* Read HCSD file for the validflag '1' record.                    */
		hcsdIO.setParams(SPACES);
		hcsdIO.setChdrcoy(reverserec.company);
		hcsdIO.setChdrnum(reverserec.chdrnum);
		hcsdIO.setLife(reverserec.life);
		hcsdIO.setCoverage(reverserec.coverage);
		hcsdIO.setRider(reverserec.rider);
		hcsdIO.setPlanSuffix(reverserec.planSuffix);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			xxxxFatalError();
		}
		if (isNE(hcsdIO.getValidflag(), "1")) {
			syserrrec.params.set(hcsdIO.getParams());
			xxxxFatalError();
		}
		/* If the record was not created by the transaction being          */
		/* reversed, then there are no HCSD records to reverse.            */
		if (isNE(hcsdIO.getTranno(), reverserec.tranno)) {
			return ;
		}
		hcsdIO.setFunction(varcom.readh);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			xxxxFatalError();
		}
		hcsdIO.setFunction(varcom.delet);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			xxxxFatalError();
		}
		/* Reinstate the validflag '2' record.                             */
		hcsdIO.setParams(SPACES);
		hcsdIO.setChdrcoy(reverserec.company);
		hcsdIO.setChdrnum(reverserec.chdrnum);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		/* MOVE BEGNH                  TO HCSD-FUNCTION.        <LA3993>*/
		hcsdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hcsdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hcsdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			xxxxFatalError();
		}
		if (isNE(hcsdIO.getChdrcoy(), reverserec.company)
		|| isNE(hcsdIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(hcsdIO.getValidflag(), "2")) {
			syserrrec.params.set(hcsdIO.getParams());
			xxxxFatalError();
		}
		hcsdIO.setValidflag("1");
		/* MOVE REWRT                  TO HCSD-FUNCTION.        <LA3993>*/
		hcsdIO.setFunction(varcom.writd);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			xxxxFatalError();
		}
	}

protected void processPrmhact8200()
	{
		start8210();
	}

protected void start8210()
	{
		prmhactIO.setParams(SPACES);
		prmhactIO.setChdrcoy(reverserec.company);
		prmhactIO.setChdrnum(reverserec.chdrnum);
		prmhactIO.setTranno(reverserec.tranno);
		prmhactIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, prmhactIO);
		if (isNE(prmhactIO.getStatuz(), varcom.oK)
		&& isNE(prmhactIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(prmhactIO.getStatuz());
			syserrrec.params.set(prmhactIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(prmhactIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		prmhactIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, prmhactIO);
		if (isNE(prmhactIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(prmhactIO.getStatuz());
			syserrrec.params.set(prmhactIO.getParams());
			xxxxFatalError();
		}
	}

//ILIFE-8509
protected void calcProrate() {
	reratedPremKeeps.clear();
	try{
		List<String> chdrnumList = new ArrayList<>();
		Map<String, List<Chdrpf>> chdrpfMap = new HashMap();
		List<Chdrpf> tempChdrList;
		covrUpdateList = new ArrayList<>();
		covrInsertList = new ArrayList<>();
		insertIncrList = new ArrayList<>();
		payrpfList = new ArrayList<>();
		chdrpfList = new ArrayList<>();
		chdrnumList.add(reverserec.chdrnum.toString());
		Incrpf incrIO = new Incrpf();
		Incrpf incr;
		BigDecimal totalPrem = BigDecimal.ZERO;
		Covrpf updateCovr;
		Covrpf insertCovr;
		covrpfList = covrpfDAO.searchCovrmjaByChdrnumCoy(reverserec.chdrnum.toString(), reverserec.company.toString());
		if(covrpfList != null && !covrpfList.isEmpty()) {
			for(Covrpf covrpf : covrpfList){
				if("1".equals(covrpf.getValidflag()) && covrpf.getReinstated()!=null && "N".equals(covrpf.getReinstated())) {
					covrReinstated = false;
					calprpmrec.increasePrem.set(ZERO);
					calprpmrec.rerateprem.set(ZERO);
					calprpmrec.chdrcoy.set(reverserec.company);
					calprpmrec.chdrnum.set(reverserec.chdrnum);
					calprpmrec.lastPTDate.set(payrlifIO.getPtdate());
					calprpmrec.nextPTDate.set(reverserec.effdate2);
					calprpmrec.reinstDate.set(reverserec.effdate1);
					calprpmrec.transcode.set(reverserec.batctrcde);
					calprpmrec.language.set(reverserec.language);
					calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
					calprpmrec.tranno.set(reverserec.newTranno);
					if(prmhldtrad && (covrpf.getReinstated()==null || !"Y".equals(covrpf.getReinstated()))){
						calprpmph.mainline(calprpmrec.calprpmRec,reratedPremKeeps, appVars, incrIO);
					}
					else if(prmhldtrad && (covrpf.getReinstated()!=null || "Y".equals(covrpf.getReinstated()))) {
						covrReinstated = true;
					}
					if(!covrReinstated) {
						if (isNE(calprpmrec.statuz, varcom.oK)) {
							syserrrec.params.set(calprpmrec.statuz);
							xxxxFatalError();
						}
					
						if(isEQ(calprpmrec.mode,"Y")){
							covrpfwopl.add(covrpf);
							calprpmrec.mode.set(SPACES);
							continue;
						}
						if(isNE(calprpmrec.rerateprem, ZERO)){
							compute(calprpmrec.prem, 2).set(add(calprpmrec.prem, calprpmrec.rerateprem));		
						}

						updateCovr = new Covrpf(covrpf);
						updateCovr.setValidflag("2");
						covrUpdateList.add(updateCovr);
						insertCovr = new Covrpf(covrpf);
						insertCovr.setInstprem(calprpmrec.prem.getbigdata());
						insertCovr.setZbinstprem(calprpmrec.zbinstprem.getbigdata());
						insertCovr.setZlinstprem(calprpmrec.zlinstprem.getbigdata());
						insertCovr.setSumins(calprpmrec.increaseSum.getbigdata());
						insertCovr.setReinstated("Y");
						insertCovr.setZstpduty01(calprpmrec.zstpduty01.getbigdata());
						insertCovr.setTranno(reverserec.newTranno.toInt());
						covrInsertList.add(insertCovr);
						incr = new Incrpf(incrIO);
						insertIncrList.add(incr);
						totalPrem = totalPrem.add(calprpmrec.prem.getbigdata());
					}
				}
			}
		}
		if (!(covrpfwopl == null || covrpfwopl.size() == 0 )){
			for(Covrpf covrpf : covrpfwopl){
				calprpmrec.increasePrem.set(ZERO);
				calprpmrec.zlinstprem.set(ZERO);
				calprpmrec.rerateprem.set(ZERO);
				calprpmrec.chdrcoy.set(reverserec.company);
				calprpmrec.chdrnum.set(reverserec.chdrnum);
				calprpmrec.lastPTDate.set(payrlifIO.getPtdate());
				calprpmrec.nextPTDate.set(reverserec.effdate2);
				calprpmrec.reinstDate.set(reverserec.effdate1);
				calprpmrec.transcode.set(reverserec.batctrcde);
				calprpmrec.language.set(reverserec.language);
				calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
				calprpmrec.tranno.set(reverserec.newTranno);
				calprpmrec.mode.set("T");	
				if(prmhldtrad && (covrpf.getReinstated()==null || !"Y".equals(covrpf.getReinstated())))
					calprpmph.mainline(calprpmrec.calprpmRec,reratedPremKeeps, appVars, incrIO);
				else if(prmhldtrad && (covrpf.getReinstated()!=null || "Y".equals(covrpf.getReinstated()))) {
					totalPrem = totalPrem.add(covrpf.getInstprem());
					covrReinstated = true;
				}
				if(!covrReinstated) {
					if (isNE(calprpmrec.statuz, varcom.oK)) {
						syserrrec.params.set(calprpmrec.statuz);
						xxxxFatalError();
					}	
					if(isNE(calprpmrec.rerateprem, ZERO)){
						compute(calprpmrec.prem, 2).set(add(covrpf.getInstprem(), calprpmrec.rerateprem));
					}
					updateCovr = new Covrpf(covrpf);
					updateCovr.setValidflag("2");
					covrUpdateList.add(updateCovr);
					insertCovr = new Covrpf(covrpf);
					insertCovr.setInstprem(calprpmrec.prem.getbigdata());
					insertCovr.setZbinstprem(calprpmrec.zbinstprem.getbigdata());
					insertCovr.setZlinstprem(calprpmrec.zlinstprem.getbigdata());
					insertCovr.setSumins(calprpmrec.increaseSum.getbigdata());
					insertCovr.setReinstated("Y");
					insertCovr.setZstpduty01(calprpmrec.zstpduty01.getbigdata());
					insertCovr.setTranno(reverserec.newTranno.toInt());
					covrInsertList.add(insertCovr);
					totalPrem = totalPrem.add(calprpmrec.prem.getbigdata());
				}
			}
		}
		if(totalPrem.compareTo(BigDecimal.ZERO) > 0) {
			payrpf = payrpfDAO.getpayrRecord(reverserec.company.toString(), reverserec.chdrnum.toString());
			if(payrpf != null) {
				payrpfList.add(payrpf);
				payrpfDAO.updatePayrRecord(payrpfList, 0);
				payrpf.setSinstamt01(totalPrem);
				payrpf.setSinstamt06(add(payrpf.getSinstamt01(), payrpf.getSinstamt02(), payrpf.getSinstamt03(), payrpf.getSinstamt04(), payrpf.getSinstamt05()).getbigdata());
				payrpf.setTranno(reverserec.newTranno.toInt());
				payrpfDAO.insertPayrpfList(payrpfList);
			}
			chdrpfMap = chdrpfDAO.searchChdrmjaByChdrnum(chdrnumList);
			if(chdrpfMap != null && !chdrpfMap.isEmpty()) {
				tempChdrList = chdrpfMap.get(reverserec.chdrnum.toString());
				if(tempChdrList!=null && !tempChdrList.isEmpty()) {
					for(Chdrpf chdr: tempChdrList) {
						if(chdr != null && "1".equals(chdr.getValidflag().toString())) {
							chdr.setCurrto(reverserec.effdate1.toInt());
							chdrpfList.add(chdr);
							chdrpfDAO.updateChdrRecord(chdrpfList, 0);
							chdr.setSinstamt01(totalPrem);
							chdr.setCurrfrom(reverserec.effdate1.toInt());
							chdr.setCurrto(varcom.maxdate.toInt());
							chdr.setSinstamt06(add(chdr.getSinstamt01(), chdr.getSinstamt02(), chdr.getSinstamt03(), chdr.getSinstamt04(), chdr.getSinstamt05()).getbigdata());
							chdr.setTranno(reverserec.newTranno.toInt());
							chdrpfDAO.insertChdrRecord(chdrpfList, 0);
						}
					}
				}
			}
			incrpfDAO.insertIncrList(insertIncrList);
		    insertIncrList.clear();
		    covrpfDAO.updateCovrRecord(covrUpdateList, 0);
		    covrUpdateList.clear();
		    covrpfDAO.insertCovrpfList(covrInsertList);
		    covrInsertList.clear();
		}
	}catch(SQLRuntimeException e){
		xxxxFatalError();
	}
}

protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorBomb();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

		/*                                                         <R90>   */
	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData agcmrvsrec = new FixedLengthStringData(10).init("AGCMRVSREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData loanrec = new FixedLengthStringData(10).init("LOANREC   ");
	private FixedLengthStringData loanenqrec = new FixedLengthStringData(10).init("LOANENQREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData hpuarec = new FixedLengthStringData(10).init("HPUAREC");
	private FixedLengthStringData hpuarevrec = new FixedLengthStringData(10).init("HPUAREVREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
}
}
