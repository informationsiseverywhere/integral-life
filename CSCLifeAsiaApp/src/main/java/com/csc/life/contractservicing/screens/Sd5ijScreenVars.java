package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sd5ijScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(450); 
	public FixedLengthStringData dataFields = new FixedLengthStringData(290).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData owners = new FixedLengthStringData(40).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] owner = FLSArrayPartOfStructure(5, 8, owners, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(owners, 0, FILLER_REDEFINE);
	public FixedLengthStringData owner1 = DD.clntnum.copy().isAPartOf(filler,0);
	public FixedLengthStringData owner2 = DD.clntnum.copy().isAPartOf(filler,8);
	public FixedLengthStringData owner3 = DD.clntnum.copy().isAPartOf(filler,16);
	public FixedLengthStringData owner4 = DD.clntnum.copy().isAPartOf(filler,24);
	public FixedLengthStringData owner5 = DD.clntnum.copy().isAPartOf(filler,32);
	
	public FixedLengthStringData ownername = new FixedLengthStringData(250).isAPartOf(dataFields, 40);
	public FixedLengthStringData[] ownname = FLSArrayPartOfStructure(5, 50, ownername, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(250).isAPartOf(ownername, 0, FILLER_REDEFINE);
	public FixedLengthStringData ownname1 = DD.clntname.copy().isAPartOf(filler1,0);
	public FixedLengthStringData ownname2 = DD.clntname.copy().isAPartOf(filler1,50);
	public FixedLengthStringData ownname3 = DD.clntname.copy().isAPartOf(filler1,100);
	public FixedLengthStringData ownname4 = DD.clntname.copy().isAPartOf(filler1,150);
	public FixedLengthStringData ownname5 = DD.clntname.copy().isAPartOf(filler1,200);	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea,290);	
	public FixedLengthStringData ownersErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] ownerErr = FLSArrayPartOfStructure(5, 4, ownersErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(ownersErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData owner1Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData owner2Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData owner3Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData owner4Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData owner5Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	
	public FixedLengthStringData ownersnameErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] ownernameErr = FLSArrayPartOfStructure(5, 4, ownersnameErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(ownersnameErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ownname1Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData ownname2Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData ownname3Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData ownname4Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData ownname5Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea,330); 
	
	public FixedLengthStringData ownersOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] ownerOut = FLSArrayPartOfStructure(5, 12, ownersOut, 0);
	public FixedLengthStringData[][] cownnum = FLSDArrayPartOfArrayStructure(12, 1, ownerOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(60).isAPartOf(ownersOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] owner1Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] owner2Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] owner3Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] owner4Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] owner5Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	
	
	public FixedLengthStringData ownernamesOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(5, 12, ownernamesOut, 0);
	public FixedLengthStringData[][] clntname = FLSDArrayPartOfArrayStructure(12, 1, ownernameOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(60).isAPartOf(ownernamesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ownname1Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] ownname2Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] ownname3Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] ownname4Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] ownname5Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sd5ijscreenWritten = new LongData(0);
	public LongData Sd5ijwindowWritten = new LongData(0);
	public LongData Sd5ijhideWritten = new LongData(0);
	public LongData Sd5ijprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5ijScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(owner1Out,new String[] {"01","02","-01","03", null, null, null, null, null, null, null, null});
		fieldIndMap.put(owner2Out,new String[] {"04","05","-04","06", null, null, null, null, null, null, null, null});
		fieldIndMap.put(owner3Out,new String[] {"07","08","-07","09", null, null, null, null, null, null, null, null});
		fieldIndMap.put(owner4Out,new String[] {"10","11","-10","12", null, null, null, null, null, null, null, null});
		fieldIndMap.put(owner5Out,new String[] {"13","14","-13","15", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {owner1,owner2,owner3,owner4,owner5,ownname1,ownname2,ownname3,ownname4,ownname5};
		screenOutFields = new BaseData[][] {owner1Out, owner2Out, owner3Out, owner4Out, owner5Out,ownname1Out,ownname2Out,ownname3Out,ownname4Out,ownname5Out};
		screenErrFields = new BaseData[] {owner1Err, owner2Err, owner3Err, owner4Err, owner5Err,ownname1Err,ownname2Err,ownname3Err,ownname4Err,ownname5Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5ijscreen.class;
		protectRecord = Sd5ijprotect.class;
		//hideRecord = Sd5ijhide.class;
	}

}
