/*
 * File: Bchgunl.java
 * Date: 29 August 2009 21:26:28
 * Author: Quipoz Limited
 *
 * Class transformed from BCHGUNL.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.recordstructures.Bchgallrec;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.Untallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.InciTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.IncibchTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*BCHGUNL - Billing Change Generic Subroutine
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Overview
*~~~~~~~~
*
*One of the  facilities  provided  by  the  billing  change
*subsystem  is to allow changing the billing frequency of a
*contract. The function of this program is to  support  the
*unit  linked  part of this facility. This program is to be
*obtained from T5671 which  keyed  on  transaction  code  +
*CRTABLE.  The  program  will be called once for every unit
*linked coverage.
*
*
*INCIBCH
*~~~~~~~
*
*Create a logical file INCIBCH based on the  physical  file
*INCIPF  and  include  all the fields from the physical and
*with the following key order:-
*
*CHDRCOY
*CHDRNUM
*LIFE
*COVERAGE
*RIDER
*PLNSFX
*INCINUM
*SEQNO
*
*Select on VALIDFLAG = '1' only.
*
*
*BCHGALLREC(Linkage)
*~~~~~~~~~~~~~~~~~~~
*
*Create  a  copybook  BCHGALLREC  to  have  the   following
*fields:-
*(Already created)
*
*01 BCHG-BCHGALL-REC.
*  03  BCHG-FUNCTION                PIC X(05).
*  03  BCHG-STATUZ                  PIC X(04).
*  03  BCHG-COMPANY                 PIC X(01).
*  03  BCHG-CHDRNUM                 PIC X(08).
*  03  BCHG-LIFE                    PIC X(02).
*  03  BCHG-COVERAGE                PIC X(02).
*  03  BCHG-RIDER                   PIC X(02).
*  03  BCHG-PLAN-SUFFIX             PIC S9(04).
*  03  BCHG-OLD-BILLFREQ            PIC X(02).
*  03  BCHG-NEW-BILLFREQ            PIC X(02).
*  03  BCHG-EFFDATE                 PIC 9(08).
*  03  BCHG-TRANNO                  PIC 9(05).
*  03  BCHG-CRTABLE                 PIC X(04).
*  03  FILLER                       PIC X(25).
*
*
*BCHGUNL
*~~~~~~~
*
*Tables: T5687 - General Coverage/Rider Details
*   T5540 - General Unit Linked Coverage/Rider Details
*   T5541 - Frequency loading Factors
*
*Files: INCIBCH - Individual Increase Details
*  INCI    - Individual Increase Details
*
*
*100-MAINLINE Section
*~~~~~~~~~~~~~~~~~~~~
*
*Set BCHG-STATUZ to '****'.
*
*Perform 200-INITIALISATION.
*
*If BCHG-STATUZ not = '****'
*Exit this section.
*
*If BCHG-FUNCTION = 'BCHG '
*Perform 300-Update-INCI.
*
*190-EXIT.
*EXIT PROGRAM.
*
*
*200-INITIALISATION Section
*~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Set WSAA-KEY-BREAK to 'N'.
*
*Get  frequency  alteration  basis  for the non unit linked
*part from T5687.
*Read T5687(Dated table) using  BCHG-CRTABLE  as  the  item
*and BCHG-EFFDATE as item from date.
*Perform normal error handling.
*
*If no entry found
*   Set BCHG-STATUZ to an error code which holds the
*   message 'T5687 entry not found'
*   Exit this section
*Else
*   Set T5687-T5687-REC to ITDM-GENAREA.
*
*If T5687-XFREQ-ALT-BASIS = space
*   Set BCHG-STATUZ to an error code which holds the
*   message 'T5687-FREQ-BASIS = space'
*   Exit this section.
*
*Use T5687-FREQ-ALT-BASIS to read T5541.
*Match the BCHG-NEW-BILLFREQ with an entry from T5541.
*This will now give us the loading factor for the frequency.
*Match the BCHG-OLD-BILLFREQ with an entry from T5541.
*This will now give us the loading factor for the frequency.
*Error if either entries are not found.
*
*
*300-Update-INCI Section
*~~~~~~~~~~~~~~~~~~~~~~~
*
*Perform 800-FIND-THE-FIRST-RECORD.
*
*Perform 1000-MAIN-PROCESSING
*until WSAA-KEY-BREAK = 'Y'.
*
*
*800-FIND-THE-FIRST-RECORD Section
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Set INCIBCH-PARAMS to space.
*Set INCIBCH-CHDRCOY to BCHG-COMPANY.
*Set INCIBCH-CHDRNUM to BCHG-CHDRNUM.
*Set INCIBCH-COVERAGE to BCHG-COVERAGE.
*Set INCIBCH-LIFE to BCHG-LIFE.
*Set INCIBCH-RIDER to BCHG-RIDER.
*Set INCIBCH-PLAN-SUFFIX to BCHG-PLAN-SUFFIX.
*Set INCIBCH-INCINUM to 0.
*Set INCIBCH-SEQNO to 0.
*Set INCIBCH-FUNCTION to 'BEGN'
*
*Call 'INCIBCHIO' to read and hold the record.
*
*Perform normal error handling.
*
*If INCIBCH-STATUZ = ENDP
*   Set WSAA-KEY-BREAK to 'Y'
*   Exit this section.
*
*If Key break(Up to and including plan suffix)
*   Set WSAA-KEY-BREAK to 'Y'
*   Rewrite the record with function 'REWRT' to release
*   the lock
*   Perform normal error handling.
*
*
*1000-MAIN-PROCESSING Section
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Move INCIBCH fields to INCI fields respectively
*
*Set INCIBCH-FUNCTION to 'NEXTR'.
*
*Read the next INCIBCH record.
*Perform error handling.
*
*If INCIBCH-STATUZ = ENDP
*   Set WSAA-KEY-BREAK to 'Y'.
*
*If Key break(Up to and including plan suffix)
*   Set WSAA-KEY-BREAK to 'Y'.
*
*Rewrite  the  record  by  calling  'INCIIO'  with function
*'REWRT'and VALID-FLAG = '2'.
*Perform normal error handling.
*
*Set INCI-VALIDFLAG to '1'.
*Set INCI-TRANNO to BCHG-TRANNO.
*
*Compute INCI-ORIG-PREM rounded = INCI-ORIG-PREM *
*             INCI-ORIG-PREM * (OLD-FREQUENCY /
*                               NEW-FREQUENCY  )
*                            * (NEW-LOADING-FACTOR /
*                               OLD-LOADING-FACTOR )
*
*Compute INCI-CURR-PREM rounded =
*             INCI-CURR-PREM * (OLD-FREQUENCY /
*                               NEW-FREQUENCY  )
*                            * (NEW-LOADING-FACTOR /
*                               OLD-LOADING-FACTOR )
*
*
*Write the new record by  calling  'INCIIO'  with  function
*'WRITR'.
*Perform error handling.
*
*****************************************************************
* </pre>
*/
public class Bchgunl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("BCHGUNL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-MISC */
	private String wsaaKeyBreak = "";
	private FixedLengthStringData wsaaNewLfMatch = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOldLfMatch = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaNewLoadFactor = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldLoadFactor = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNewFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5541Count = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(3, 0);
	private PackedDecimalData iy = new PackedDecimalData(3, 0);
	private PackedDecimalData iz = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTerm = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNofOldPeriods = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaNofNewPeriods = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaFreqFactor1 = new PackedDecimalData(17, 12);
	private PackedDecimalData wsaaFreqFactor2 = new PackedDecimalData(17, 12);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(15, 2);
	private String wsaaFlexiPremium = "";
	private String wsaaVf1RecCreated = "";
	private ZonedDecimalData wsaaFlexPremFq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaLifeCltsex = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaLifeAnbAtCcd = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT5537Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5537Basis = new FixedLengthStringData(3).isAPartOf(wsaaT5537Key, 0);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Key, 3);
	private FixedLengthStringData wsaaT5537Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT5537Key, 4);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(17, 11);
	private PackedDecimalData wsaaOldTcurrPrem = new PackedDecimalData(17, 2);

	private FixedLengthStringData[] filler1 = FLSInittedArray (99, 22);
	private BinaryData[] wsaaInciRrn = BDArrayPartOfArrayStructure(9, 0, filler1, 0);
	private PackedDecimalData[] wsaaOldCurrPrem = PDArrayPartOfArrayStructure(17, 2, filler1, 4);
	private PackedDecimalData[] wsaaNewCurrPrem = PDArrayPartOfArrayStructure(17, 2, filler1, 13);
		/* ERRORS */
	private static final String h053 = "H053";
	private static final String h054 = "H054";
	private static final String h059 = "H059";
	private static final String t075 = "T075";
	private static final String t076 = "T076";
	private static final String e706 = "E706";
	private static final String e707 = "E707";
	private static final String incirec = "INCIREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String covrunlrec = "COVRUNLREC";
	private static final String fpcorec = "FPCOREC";
		/* TABLES */
	private static final String t5687 = "T5687";
	private static final String t5541 = "T5541";
	private static final String t5729 = "T5729";
	private static final String t5536 = "T5536";
	private static final String t5537 = "T5537";
	private static final String t5540 = "T5540";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private InciTableDAM inciIO = new InciTableDAM();
	private IncibchTableDAM incibchIO = new IncibchTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5687rec t5687rec = new T5687rec();
	private T5541rec t5541rec = new T5541rec();
	private T5536rec t5536rec = new T5536rec();
	private T5537rec t5537rec = new T5537rec();
	private T5540rec t5540rec = new T5540rec();
	private Bchgallrec bchgallrec = new Bchgallrec();
	private WsaaT5536ValuesInner wsaaT5536ValuesInner = new WsaaT5536ValuesInner();
	private ExternalisedRules er = new ExternalisedRules();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit290,
		readInci412, 
		nextInci418, 
		exit419, 
		readInci720, 
		nextInci780, 
		exit790, 
		readT55372330, 
		readT5537Done2340, 
		xOrdinate2370
	}

	public Bchgunl() {
		super();
	}

public void mainline(Object... parmArray)
	{
		bchgallrec.bchgallRec = convertAndSetParam(bchgallrec.bchgallRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
			main110();
			exit190();
		}

protected void main110()
	{
		bchgallrec.statuz.set(varcom.oK);
		initialisation200();
		if (isNE(bchgallrec.statuz,varcom.oK)) {
			return ;
		}
		if (isEQ(bchgallrec.function,"BCHG ")) {
			updateInci300();
		}
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialisation200()
	{
		try {
			initialise210();
			readT5687NonUl220();
			readT5541230();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise210()
	{
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(bchgallrec.company);
		chdrlnbIO.setChdrnum(bchgallrec.chdrnum);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			databaseError500();
		}
	}

protected void readT5687NonUl220()
	{
		/*  Get frequency alteration basis for the non unit linked*/
		/*  part from T5687.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(bchgallrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(bchgallrec.crtable);
		itdmIO.setItmfrm(bchgallrec.effdate);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");*/

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError500();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			bchgallrec.statuz.set(h059);
			goTo(GotoLabel.exit290);
		}
		if (isNE(itdmIO.getItemcoy(),bchgallrec.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),bchgallrec.crtable)) {
			bchgallrec.statuz.set(h053);
			goTo(GotoLabel.exit290);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5687rec.xfreqAltBasis,SPACES)) {
			bchgallrec.statuz.set(h054);
			goTo(GotoLabel.exit290);
		}
	}

	/**
	* <pre>
	*  Concatenate T5687-XFREQ-ALT-BASIS with BCHG-OLD-BILLFREQ       
	*  to become WSAA-T6630-NON-UL-ITEM.                              
	**** MOVE T5687-XFREQ-ALT-BASIS  TO WSAA-NON-XFREQ-ALT-BASIS.     
	**** MOVE BCHG-OLD-BILLFREQ      TO WSAA-NON-OLD-BILLFREQ.        
	*230-READ-T5540-UL.                                               
	***Get frequency alteration basis for the unit linked part        
	***from T5687.                                                    
	**** MOVE SPACES                 TO ITDM-DATA-KEY.                
	**** MOVE BCHG-COMPANY           TO ITDM-ITEMCOY.                 
	**** MOVE T5540                  TO ITDM-ITEMTABL.                
	**** MOVE BCHG-CRTABLE           TO ITDM-ITEMITEM.                
	**** MOVE BCHG-EFFDATE           TO ITDM-ITMFRM.                  
	**** MOVE BEGN                   TO ITDM-FUNCTION.                
	**** CALL 'ITDMIO' USING ITDM-PARAMS.                             
	**** IF ITDM-STATUZ          NOT = O-K  AND                       
	****                         NOT = ENDP                           
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	****    MOVE ITDM-STATUZ         TO SYSR-STATUZ                   
	****    PERFORM 500-DATABASE-ERROR                                
	**** END-IF.                                                      
	**** IF ITDM-STATUZ              = ENDP                           
	****    MOVE H059                TO BCHG-STATUZ                   
	****    GO TO                    290-EXIT                         
	**** END-IF.                                                      
	**** IF ITDM-ITEMCOY        NOT  = BCHG-COMPANY OR                
	****    ITDM-ITEMTABL       NOT  = T5540        OR                
	****    ITDM-ITEMITEM       NOT  = BCHG-CRTABLE                   
	****    MOVE H055                TO BCHG-STATUZ                   
	****    GO TO                    290-EXIT                         
	**** ELSE                                                         
	****    MOVE ITDM-GENAREA        TO T5540-T5540-REC               
	**** END-IF.                                                      
	**** IF T5540-XFREQ-ALT-BASIS    = SPACES                         
	****    MOVE H056                TO BCHG-STATUZ                   
	****    GO TO                    290-EXIT                         
	**** END-IF.                                                      
	***Concatenate T5540-XFREQ-ALT-BASIS with BCHG-OLD-BILLFREQ       
	***to become WSAA-T6630-UL-ITEM.                                  
	**** MOVE T5540-XFREQ-ALT-BASIS  TO WSAA-XFREQ-ALT-BASIS.         
	**** MOVE BCHG-OLD-BILLFREQ      TO WSAA-OLD-BILLFREQ.            
	* </pre>
	*/
protected void readT5541230()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(bchgallrec.company);
		itdmIO.setItemtabl(t5541);
		itdmIO.setItemitem(t5687rec.xfreqAltBasis);
		itdmIO.setItmfrm(bchgallrec.effdate);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");*/

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError500();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			bchgallrec.statuz.set(h059);
			return ;
		}
		if (isNE(itdmIO.getItemcoy(),bchgallrec.company)
		|| isNE(itdmIO.getItemtabl(),t5541)
		|| isNE(itdmIO.getItemitem(),t5687rec.xfreqAltBasis)) {
			bchgallrec.statuz.set(t075);
			return ;
		}
		else {
			t5541rec.t5541Rec.set(itdmIO.getGenarea());
		}
		/*  Match BCHG-NEW-BILLFREQ with those on T5541-T5541-REC.         */
		wsaaNewLfMatch.set(SPACES);
		wsaaOldLfMatch.set(SPACES);
		for (wsaaT5541Count.set(1); !((isEQ(wsaaT5541Count,12))
		|| (isEQ(wsaaNewLfMatch,"Y")
		&& isEQ(wsaaOldLfMatch,"Y"))); wsaaT5541Count.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaT5541Count.toInt()],bchgallrec.newBillfreq)) {
				wsaaNewLoadFactor.set(t5541rec.lfact[wsaaT5541Count.toInt()]);
				wsaaNewLfMatch.set("Y");
			}
			if (isEQ(t5541rec.freqcy[wsaaT5541Count.toInt()],bchgallrec.oldBillfreq)) {
				wsaaOldLoadFactor.set(t5541rec.lfact[wsaaT5541Count.toInt()]);
				wsaaOldLfMatch.set("Y");
			}
		}
		if ((isNE(wsaaNewLfMatch,"Y"))
		&& (isNE(wsaaOldLfMatch,"Y"))) {
			bchgallrec.statuz.set(t076);
			return ;
		}
	}

protected void updateInci300()
	{
		updatePara310();
	}

protected void updatePara310()
	{
		/*    PERFORM 800-FIND-THE-FIRST-RECORD.                           */
		/*    PERFORM 1000-MAIN-PROCESSING                                 */
		/*      UNTIL WSAA-KEY-BREAK = 'Y'.                                */
		wsaaOldFreq.set(bchgallrec.oldBillfreq);
		wsaaNewFreq.set(bchgallrec.newBillfreq);
		readT57292100();
		readCovrunl2200();
		getAllocBasis2300();
		//readT55362400();
		//getNewOldT55362500();		
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models starts
		//Ilife-3802 LIFE VPMS Externalization - Integration issue with Unit Allocation and Enhanced Unit Allocation
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VALCL") && er.isExternalized(chdrlnbIO.cnttype.toString(), covrunlIO.crtable.toString())))
		{
			readT55362400();
			getNewOldT55362500();	
		}
		else
		{
			Untallrec untallrec = new Untallrec();
			untallrec.initialize();
			//call new bill frequency
			untallrec.untaCompany.set(bchgallrec.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(bchgallrec.occdate);
			untallrec.untaBillfreq.set(bchgallrec.newBillfreq);	
			
			callProgram("VALCL", untallrec.untallrec,chdrlnbIO);
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[1].set(untallrec.untaMaxPeriod01);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[1].set(untallrec.untaPcInitUnit01);
			wsaaT5536ValuesInner.wsaaNewPcUnit[1].set(untallrec.untaPcUnit01);	
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[2].set(untallrec.untaMaxPeriod02);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[2].set(untallrec.untaPcInitUnit02);
			wsaaT5536ValuesInner.wsaaNewPcUnit[2].set(untallrec.untaPcUnit02);
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[3].set(untallrec.untaMaxPeriod03);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[3].set(untallrec.untaPcInitUnit03);
			wsaaT5536ValuesInner.wsaaNewPcUnit[3].set(untallrec.untaPcUnit03);
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[4].set(untallrec.untaMaxPeriod04);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[4].set(untallrec.untaPcInitUnit04);
			wsaaT5536ValuesInner.wsaaNewPcUnit[4].set(untallrec.untaPcUnit04);
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[5].set(untallrec.untaMaxPeriod05);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[5].set(untallrec.untaPcInitUnit05);
			wsaaT5536ValuesInner.wsaaNewPcUnit[5].set(untallrec.untaPcUnit05);
			
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[6].set(untallrec.untaMaxPeriod06);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[6].set(untallrec.untaPcInitUnit06);
			wsaaT5536ValuesInner.wsaaNewPcUnit[6].set(untallrec.untaPcUnit06);
			
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[7].set(untallrec.untaMaxPeriod07);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[7].set(untallrec.untaPcInitUnit07);
			wsaaT5536ValuesInner.wsaaNewPcUnit[7].set(untallrec.untaPcUnit07);
			
			//call old bill frequency
			untallrec.initialize();
			untallrec.untaCompany.set(bchgallrec.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(bchgallrec.occdate);
			untallrec.untaBillfreq.set(bchgallrec.oldBillfreq);	

			callProgram("VALCL", untallrec.untallrec,chdrlnbIO);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[1].set(untallrec.untaMaxPeriod01);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[1].set(untallrec.untaPcInitUnit01);
			wsaaT5536ValuesInner.wsaaOldPcUnit[1].set(untallrec.untaPcUnit01);	
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[2].set(untallrec.untaMaxPeriod02);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[2].set(untallrec.untaPcInitUnit02);
			wsaaT5536ValuesInner.wsaaOldPcUnit[2].set(untallrec.untaPcUnit02);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[3].set(untallrec.untaMaxPeriod03);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[3].set(untallrec.untaPcInitUnit03);
			wsaaT5536ValuesInner.wsaaOldPcUnit[3].set(untallrec.untaPcUnit03);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[4].set(untallrec.untaMaxPeriod04);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[4].set(untallrec.untaPcInitUnit04);
			wsaaT5536ValuesInner.wsaaOldPcUnit[4].set(untallrec.untaPcUnit04);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[5].set(untallrec.untaMaxPeriod05);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[5].set(untallrec.untaPcInitUnit05);
			wsaaT5536ValuesInner.wsaaOldPcUnit[5].set(untallrec.untaPcUnit05);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[6].set(untallrec.untaMaxPeriod06);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[6].set(untallrec.untaPcInitUnit06);
			wsaaT5536ValuesInner.wsaaOldPcUnit[6].set(untallrec.untaPcUnit06);
			
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[7].set(untallrec.untaMaxPeriod07);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[7].set(untallrec.untaPcInitUnit07);
			wsaaT5536ValuesInner.wsaaOldPcUnit[7].set(untallrec.untaPcUnit07);
		}
		//VPMS-799 RUL Product - Unit Allocation Calculation - Integration with latest PA compatible models  end
		proratePremium400();
		processInci700();
		wsaaInstprem.set(covrunlIO.getInstprem());
		updateFpco3000();
		}

protected void proratePremium400()
	{
		initialization401();
	}

protected void initialization401()
	{
		for (ix.set(1); !(isGT(ix, 99)); ix.add(1)){
			wsaaInciRrn[ix.toInt()].set(ZERO);
			wsaaOldCurrPrem[ix.toInt()].set(ZERO);
			wsaaNewCurrPrem[ix.toInt()].set(ZERO);
		}
		ix.set(ZERO);
		wsaaOldTcurrPrem.set(ZERO);
		saveOldCurrPrem410();
		wsaaInstprem.set(covrunlIO.getInstprem());
		for (iy.set(1); !(isEQ(iy, ix)); iy.add(1)){
			compute(wsaaFactor, 11).set(div(wsaaOldCurrPrem[iy.toInt()], wsaaOldTcurrPrem));
			/*            / WSAA-OLD-TCURR-PREM (IY)                 <S19FIX>*/
			compute(wsaaNewCurrPrem[iy.toInt()], 12).setRounded(mult(wsaaFactor, covrunlIO.getInstprem()));
			wsaaInstprem.subtract(wsaaNewCurrPrem[iy.toInt()]);
		}
		wsaaNewCurrPrem[ix.toInt()].set(wsaaInstprem);
	}

protected void saveOldCurrPrem410()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
		try {
				switch (nextMethod) {
				case DEFAULT: 
					start411();
				case readInci412: 
					readInci412();
					saveInci415();
				case nextInci418: 
					nextInci418();
				case exit419: 
				}
				break;
		}
		catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start411()
	{
		inciIO.setParams(SPACES);
		inciIO.setChdrcoy(bchgallrec.company);
		inciIO.setChdrnum(bchgallrec.chdrnum);
		inciIO.setCoverage(bchgallrec.coverage);
		inciIO.setLife(bchgallrec.life);
		inciIO.setRider(bchgallrec.rider);
		inciIO.setPlanSuffix(bchgallrec.planSuffix);
		inciIO.setInciNum(ZERO);
		inciIO.setSeqno(ZERO);
		inciIO.setFunction(varcom.begn);
	}

protected void readInci412()
	{
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(), varcom.oK)
		&& isNE(inciIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
		if (isNE(inciIO.getStatuz(), varcom.oK)
		|| isNE(inciIO.getChdrcoy(), bchgallrec.company)
		|| isNE(inciIO.getChdrnum(), bchgallrec.chdrnum)
		|| isNE(inciIO.getCoverage(), bchgallrec.coverage)
		|| isNE(inciIO.getLife(), bchgallrec.life)
		|| isNE(inciIO.getRider(), bchgallrec.rider)
		|| isNE(inciIO.getPlanSuffix(), bchgallrec.planSuffix)) {
			goTo(GotoLabel.exit419);
		}
		if (isEQ(inciIO.getDormantFlag(), "Y")) {
			goTo(GotoLabel.nextInci418);
		}
	}

protected void saveInci415()
	{
		ix.add(1);
		wsaaInciRrn[ix.toInt()].set(inciIO.getRrn());
		wsaaOldCurrPrem[ix.toInt()].set(inciIO.getCurrPrem());
		wsaaOldTcurrPrem.add(inciIO.getCurrPrem());
	}

protected void nextInci418()
	{
		inciIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readInci412);
	}

protected void databaseError500()
	{
		para500();
			exit590();
		}

protected void para500()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			bchgallrec.statuz.set(syserrrec.statuz);
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrType);
	}

protected void exit590()
	{
		bchgallrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void processInci700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
		try {
				switch (nextMethod) {
				case DEFAULT: 
					start710();
				case readInci720: 
					readInci720();
					vflag2Inci750();
					vflag1Inci760();
				case nextInci780: 
					nextInci780();
				case exit790: 
				}
				break;
		}
		catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start710()
	{
		inciIO.setParams(SPACES);
		inciIO.setChdrcoy(bchgallrec.company);
		inciIO.setChdrnum(bchgallrec.chdrnum);
		inciIO.setCoverage(bchgallrec.coverage);
		inciIO.setLife(bchgallrec.life);
		inciIO.setRider(bchgallrec.rider);
		inciIO.setPlanSuffix(bchgallrec.planSuffix);
		inciIO.setInciNum(ZERO);
		inciIO.setSeqno(ZERO);
		inciIO.setFunction(varcom.begn);
	}

protected void readInci720()
	{
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(), varcom.oK)
		&& isNE(inciIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
		if (isNE(inciIO.getStatuz(), varcom.oK)
		|| isNE(inciIO.getChdrcoy(), bchgallrec.company)
		|| isNE(inciIO.getChdrnum(), bchgallrec.chdrnum)
		|| isNE(inciIO.getCoverage(), bchgallrec.coverage)
		|| isNE(inciIO.getLife(), bchgallrec.life)
		|| isNE(inciIO.getRider(), bchgallrec.rider)
		|| isNE(inciIO.getPlanSuffix(), bchgallrec.planSuffix)) {
			goTo(GotoLabel.exit790);
		}
		if (isEQ(inciIO.getDormantFlag(), "Y")) {
			goTo(GotoLabel.nextInci780);
		}
		if (isEQ(inciIO.getTranno(), bchgallrec.tranno)) {
			goTo(GotoLabel.nextInci780);
		}
	}

protected void vflag2Inci750()
	{
		inciIO.setValidflag("2");
		inciIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
	}

protected void vflag1Inci760()
	{
		wsaaInstprem.set(ZERO);
		for (iy.set(1); !(isGT(iy, 99)); iy.add(1)){
			if (isEQ(inciIO.getRrn(), wsaaInciRrn[iy.toInt()])) {
				wsaaInstprem.set(wsaaNewCurrPrem[iy.toInt()]);
				iy.set(99);
			}
		}
		for (ix.set(1); !(isGT(ix, 4)); ix.add(1)){
			calcSlots2600();
		}
		inciIO.setCurrPrem(wsaaInstprem);
		inciIO.setOrigPrem(wsaaInstprem);
		inciIO.setTranno(bchgallrec.tranno);
		inciIO.setValidflag("1");
		inciIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
	}

protected void nextInci780()
	{
		inciIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readInci720);
	}

protected void findTheFirstRecord800()
	{
		getFirstRecord810();
	}

protected void getFirstRecord810()
	{
		incibchIO.setParams(SPACES);
		incibchIO.setChdrcoy(bchgallrec.company);
		incibchIO.setChdrnum(bchgallrec.chdrnum);
		incibchIO.setCoverage(bchgallrec.coverage);
		incibchIO.setLife(bchgallrec.life);
		incibchIO.setRider(bchgallrec.rider);
		incibchIO.setPlanSuffix(bchgallrec.planSuffix);
		incibchIO.setInciNum(ZERO);
		incibchIO.setSeqno(ZERO);
		incibchIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*incibchIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incibchIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");*/

		SmartFileCode.execute(appVars, incibchIO);
		if (isNE(incibchIO.getStatuz(),varcom.oK)
		&& isNE(incibchIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incibchIO.getParams());
			syserrrec.statuz.set(incibchIO.getStatuz());
			databaseError500();
		}
		if (isEQ(incibchIO.getStatuz(),varcom.endp)) {
			return ;
		}
		/*  Check for Key Break up to and including plan suffix*/
		wsaaVf1RecCreated = "N";
	}

protected void mainProcessing1000()
	{
		setUpInciRecord1010();
		getNextRecord1020();
		processRecords1030();
	}

	/**
	* <pre>
	* Set up the fields for INCI
	* </pre>
	*/
protected void setUpInciRecord1010()
	{
		inciIO.setDataKey(incibchIO.getDataKey());
		inciIO.setPremsts(incibchIO.getPremsts());
		inciIO.setPremcurrs(incibchIO.getPremcurrs());
		inciIO.setPcunits(incibchIO.getPcunits());
		inciIO.setUsplitpcs(incibchIO.getUsplitpcs());
		inciIO.setValidflag(incibchIO.getValidflag());
		inciIO.setTranno(incibchIO.getTranno());
		inciIO.setRcdate(incibchIO.getRcdate());
		inciIO.setPremCessDate(incibchIO.getPremCessDate());
		inciIO.setCrtable(incibchIO.getCrtable());
		inciIO.setOrigPrem(incibchIO.getOrigPrem());
		inciIO.setCurrPrem(incibchIO.getCurrPrem());
		inciIO.setDormantFlag(incibchIO.getDormantFlag());
		inciIO.setUser(incibchIO.getUser());
		inciIO.setTermid(incibchIO.getTermid());
		inciIO.setTransactionDate(incibchIO.getTransactionDate());
		inciIO.setTransactionTime(incibchIO.getTransactionTime());
	}

protected void getNextRecord1020()
	{
		/*  Read the next INCIBCH record*/
		incibchIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incibchIO);
		if ((isNE(incibchIO.getStatuz(),varcom.oK))
		&& (isNE(incibchIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(incibchIO.getParams());
			syserrrec.statuz.set(incibchIO.getStatuz());
			databaseError500();
		}
		/*  Check for Key Break up to and including plan suffix*/
	}

protected void processRecords1030()
	{
		/*  Rewrite all the valid revelant INCI records with validflag*/
		/*  equals to '2'*/
		/*    MOVE BCHG-TRANNO            TO INCI-TRANNO.*/
		inciIO.setValidflag("2");
		inciIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
		if (isEQ(wsaaVf1RecCreated, "Y")) {
			return ;
		}
		/*  Write a new record, each with a validflag '1' and new details*/
		inciIO.setValidflag("1");
		inciIO.setTranno(bchgallrec.tranno);
		wsaaOldFreq.set(bchgallrec.oldBillfreq);
		wsaaNewFreq.set(bchgallrec.newBillfreq);
		setPrecision(inciIO.getOrigPrem(), 5);
		inciIO.setOrigPrem(mult(mult(inciIO.getOrigPrem(),(div(wsaaOldFreq,wsaaNewFreq))),(div(wsaaNewLoadFactor,wsaaOldLoadFactor))), true);
		zrdecplrec.amountIn.set(inciIO.getOrigPrem());
		callRounding4000();
		inciIO.setOrigPrem(zrdecplrec.amountOut);
		setPrecision(inciIO.getCurrPrem(), 5);
		inciIO.setCurrPrem(mult(mult(inciIO.getCurrPrem(),(div(wsaaOldFreq,wsaaNewFreq))),(div(wsaaNewLoadFactor,wsaaOldLoadFactor))), true);
		zrdecplrec.amountIn.set(inciIO.getCurrPrem());
		callRounding4000();
		inciIO.setCurrPrem(zrdecplrec.amountOut);
		/* COMPUTE INCI-ORIG-PREM ROUNDED    = INCI-ORIG-PREM *         */
		/*                                     WSAA-NON-UL-FACTOR.      */
		/* COMPUTE INCI-CURR-PREM ROUNDED    = INCI-CURR-PREM *         */
		/*                                     WSAA-NON-UL-FACTOR.      */
		/* COMPUTE INCI-PREM-CURR01 ROUNDED  = INCI-PREM-CURR01 *       */
		/*                                     WSAA-NON-UL-FACTOR.      */
		/* COMPUTE INCI-PREM-CURR02 ROUNDED  = INCI-PREM-CURR02 *       */
		/*                                     WSAA-NON-UL-FACTOR.      */
		/* COMPUTE INCI-PREM-CURR03 ROUNDED  = INCI-PREM-CURR03 *       */
		/*                                     WSAA-NON-UL-FACTOR.      */
		/* COMPUTE INCI-PREM-CURR04 ROUNDED  = INCI-PREM-CURR04 *       */
		/*                                     WSAA-NON-UL-FACTOR.      */
		/* COMPUTE INCI-PREM-CURR01 ROUNDED  = INCI-PREM-CURR01 *       */
		/*                                     WSAA-UL-FACTOR.          */
		/* COMPUTE INCI-PREM-CURR02 ROUNDED  = INCI-PREM-CURR02 *       */
		/*                                     WSAA-UL-FACTOR.          */
		/* COMPUTE INCI-PREM-CURR03 ROUNDED  = INCI-PREM-CURR03 *       */
		/*                                     WSAA-UL-FACTOR.          */
		/* COMPUTE INCI-PREM-CURR04 ROUNDED  = INCI-PREM-CURR04 *       */
		reshuffleInci2000();
		updateFpco3000();
		inciIO.setFunction(varcom.writr);
		inciIO.setFormat(incirec);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			databaseError500();
		}
		wsaaVf1RecCreated = "Y";
	}

protected void reshuffleInci2000()
	{
		/*PARA*/
		readT57292100();
		readCovrunl2200();
		getAllocBasis2300();
		//readT55362400();
		//getNewOldT55362500();
		//VPMS externalization code start
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VALCL")  && er.isExternalized(chdrlnbIO.cnttype.toString(), covrunlIO.crtable.toString())))
		{
			readT55362400();
			getNewOldT55362500();	
		}
		else
		{
			Untallrec untallrec = new Untallrec();
			untallrec.initialize();
			//call new bill frequency
			untallrec.untaCompany.set(bchgallrec.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(bchgallrec.occdate);
			untallrec.untaBillfreq.set(bchgallrec.newBillfreq);	

			callProgram("VALCL", untallrec.untallrec,chdrlnbIO);
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[1].set(untallrec.untaMaxPeriod01);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[1].set(untallrec.untaPcInitUnit01);
			wsaaT5536ValuesInner.wsaaNewPcUnit[1].set(untallrec.untaPcUnit01);	
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[2].set(untallrec.untaMaxPeriod02);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[2].set(untallrec.untaPcInitUnit02);
			wsaaT5536ValuesInner.wsaaNewPcUnit[2].set(untallrec.untaPcUnit02);
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[3].set(untallrec.untaMaxPeriod03);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[3].set(untallrec.untaPcInitUnit03);
			wsaaT5536ValuesInner.wsaaNewPcUnit[3].set(untallrec.untaPcUnit03);
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[4].set(untallrec.untaMaxPeriod04);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[4].set(untallrec.untaPcInitUnit04);
			wsaaT5536ValuesInner.wsaaNewPcUnit[4].set(untallrec.untaPcUnit04);
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[5].set(untallrec.untaMaxPeriod05);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[5].set(untallrec.untaPcInitUnit05);
			wsaaT5536ValuesInner.wsaaNewPcUnit[5].set(untallrec.untaPcUnit05);
			
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[6].set(untallrec.untaMaxPeriod06);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[6].set(untallrec.untaPcInitUnit06);
			wsaaT5536ValuesInner.wsaaNewPcUnit[6].set(untallrec.untaPcUnit06);
			
			
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[7].set(untallrec.untaMaxPeriod07);	
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[7].set(untallrec.untaPcInitUnit07);
			wsaaT5536ValuesInner.wsaaNewPcUnit[7].set(untallrec.untaPcUnit07);
			
			//call old bill frequency
			untallrec.initialize();
			untallrec.untaCompany.set(bchgallrec.company);
			untallrec.untaAalbas.set(wsaaAllocBasis);
			untallrec.untaEffdate.set(bchgallrec.occdate);
			untallrec.untaBillfreq.set(bchgallrec.oldBillfreq);	

			callProgram("VALCL", untallrec.untallrec,chdrlnbIO);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[1].set(untallrec.untaMaxPeriod01);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[1].set(untallrec.untaPcInitUnit01);
			wsaaT5536ValuesInner.wsaaOldPcUnit[1].set(untallrec.untaPcUnit01);	
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[2].set(untallrec.untaMaxPeriod02);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[2].set(untallrec.untaPcInitUnit02);
			wsaaT5536ValuesInner.wsaaOldPcUnit[2].set(untallrec.untaPcUnit02);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[3].set(untallrec.untaMaxPeriod03);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[3].set(untallrec.untaPcInitUnit03);
			wsaaT5536ValuesInner.wsaaOldPcUnit[3].set(untallrec.untaPcUnit03);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[4].set(untallrec.untaMaxPeriod04);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[4].set(untallrec.untaPcInitUnit04);
			wsaaT5536ValuesInner.wsaaOldPcUnit[4].set(untallrec.untaPcUnit04);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[5].set(untallrec.untaMaxPeriod05);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[5].set(untallrec.untaPcInitUnit05);
			wsaaT5536ValuesInner.wsaaOldPcUnit[5].set(untallrec.untaPcUnit05);
			
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[6].set(untallrec.untaMaxPeriod06);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[6].set(untallrec.untaPcInitUnit06);
			wsaaT5536ValuesInner.wsaaOldPcUnit[6].set(untallrec.untaPcUnit06);
			
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[7].set(untallrec.untaMaxPeriod07);	
			wsaaT5536ValuesInner.wsaaOldPcInitUnit[7].set(untallrec.untaPcInitUnit07);
			wsaaT5536ValuesInner.wsaaOldPcUnit[7].set(untallrec.untaPcUnit07);
			
		}
		//VPMS externalization code end
		for (ix.set(1); !(isGT(ix, 4)); ix.add(1)){
			calcSlots2600();
		}
		inciIO.setCurrPrem(wsaaInstprem);
		inciIO.setOrigPrem(wsaaInstprem);
		/*EXIT*/
	}

protected void readT57292100()
	{
		/*PARA*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(bchgallrec.company);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(bchgallrec.cnttype);
		itdmIO.setItmfrm(bchgallrec.occdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		/*EXIT*/
	}

protected void readCovrunl2200()
	{
		para2210();
		calculateTerm2220();
		readLife2250();
	}

protected void para2210()
	{
		covrunlIO.setParams(SPACES);
		covrunlIO.setChdrcoy(bchgallrec.company);
		covrunlIO.setChdrnum(bchgallrec.chdrnum);
		covrunlIO.setLife(bchgallrec.life);
		covrunlIO.setCoverage(bchgallrec.coverage);
		covrunlIO.setRider(bchgallrec.rider);
		covrunlIO.setPlanSuffix(bchgallrec.planSuffix);
		covrunlIO.setFormat(covrunlrec);
		covrunlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrunlIO.getParams());
			syserrrec.statuz.set(covrunlIO.getStatuz());
			databaseError500();
		}
	}

protected void calculateTerm2220()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(covrunlIO.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			databaseError500();
		}
		wsaaTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor, wsaaTerm)) {
			wsaaTerm.add(1);
		}
	}

protected void readLife2250()
	{
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(bchgallrec.company);
		lifemjaIO.setChdrnum(bchgallrec.chdrnum);
		lifemjaIO.setLife(bchgallrec.life);
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		|| isNE(lifemjaIO.getChdrcoy(), bchgallrec.company)
		|| isNE(lifemjaIO.getChdrnum(), bchgallrec.chdrnum)
		|| isNE(lifemjaIO.getLife(), bchgallrec.life)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			databaseError500();
		}
		/* Need to see if a joint life exists. If so, use the younger of   */
		/* the two lives to find the unit allocation basis.                */
		wsaaLifeCltsex.set(lifemjaIO.getCltsex());
		wsaaLifeAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		lifemjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			databaseError500();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.oK)
		&& isEQ(lifemjaIO.getChdrcoy(), bchgallrec.company)
		&& isEQ(lifemjaIO.getChdrnum(), bchgallrec.chdrnum)
		&& isEQ(lifemjaIO.getLife(), bchgallrec.life)) {
			if (isLT(lifemjaIO.getAnbAtCcd(), wsaaLifeAnbAtCcd)) {
				wsaaLifeAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
				wsaaLifeCltsex.set(lifemjaIO.getCltsex());
			}
		}
	}

protected void getAllocBasis2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readT55402310();
					setT5537Key2320();
				case readT55372330: 
					readT55372330();
					readT5537AllSex2330();
					readT5537AllTrancode2330();
					readT5537AllSexTrncde2330();
					readT5537AllStars2330();
				case readT5537Done2340: 
					readT5537Done2340();
					increment2360();
				case xOrdinate2370: 
					xOrdinate2370();
					increment2380();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT55402310()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(bchgallrec.company);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(covrunlIO.getCrtable());
		itdmIO.setItmfrm(covrunlIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError500();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), bchgallrec.company)
		|| isNE(itdmIO.getItemtabl(), t5540)
		|| isNE(itdmIO.getItemitem(), bchgallrec.crtable)) {
			itdmIO.setItemcoy(bchgallrec.company);
			itdmIO.setItemtabl(t5540);
			itdmIO.setItemitem(covrunlIO.getCrtable());
			itdmIO.setItmfrm(covrunlIO.getCrrcd());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.mrnf);
			databaseError500();
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
		wsaaInstprem.set(covrunlIO.getInstprem());
	}

protected void setT5537Key2320()
	{
		wsaaT5537Basis.set(t5540rec.allbas);
		wsaaT5537Trancode.set(bchgallrec.trancode);
		wsaaT5537Sex.set(wsaaLifeCltsex);
	}

protected void readT55372330()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bchgallrec.company);
		itemIO.setItemtabl(t5537);
		itemIO.setItemitem(wsaaT5537Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.readT5537Done2340);
		}
	}

protected void readT5537AllSex2330()
	{
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.readT5537Done2340);
		}
	}

protected void readT5537AllTrancode2330()
	{
		wsaaT5537Sex.set(wsaaLifeCltsex);
		wsaaT5537Trancode.fill("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.readT5537Done2340);
		}
	}

protected void readT5537AllSexTrncde2330()
	{
		wsaaT5537Trancode.fill("*");
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.readT5537Done2340);
		}
	}

protected void readT5537AllStars2330()
	{
		itemIO.setItemitem("********");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError500();
		}
	}

protected void readT5537Done2340()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*Y-ORDINATE*/
		/*  Work out the array's Y index based on ANB-AT-CCD.              */
		iz.set(0);
	}

protected void increment2360()
	{
		iz.add(1);
		/* Check the continuation field for a key to re-read table.        */
		if (isGT(iz, 10)) {
			if (isNE(t5537rec.agecont, SPACES)) {
				wsaaT5537Key.set(t5537rec.agecont);
				goTo(GotoLabel.readT55372330);
			}
			else {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(e706);
				databaseError500();
			}
		}
		if (isGT(wsaaLifeAnbAtCcd, t5537rec.ageIssageTo[iz.toInt()])) {
			increment2360();
			return ;
		}
		iy.set(iz);
	}

protected void xOrdinate2370()
	{
		/*  Work out the array's X index based on TERM.                    */
		iz.set(0);
	}

protected void increment2380()
	{
		iz.add(1);
		/* Check the term continuation field for a key to re-read table.   */
		if (isGT(iz, 9)) {
			if (isNE(t5537rec.trmcont, SPACES)) {
				itemIO.setItemitem(t5537rec.trmcont);
				SmartFileCode.execute(appVars, itemIO);
				if (isNE(itemIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(itemIO.getParams());
					databaseError500();
				}
				t5537rec.t5537Rec.set(itemIO.getGenarea());
				goTo(GotoLabel.xOrdinate2370);
			}
			else {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(e707);
				databaseError500();
			}
		}
		if (isGT(wsaaTerm, t5537rec.toterm[iz.toInt()])) {
			increment2380();
			return ;
		}
		ix.set(iz);
		/* Now we have the correct position(IX, IY) of the basis.          */
		/* All we have to do now is to work out the corresponding position */
		/* in the copybook by using these co-ordinates.(The copybook is    */
		/* defined as one-dimensional.)                                    */
		iy.subtract(1);
		compute(iz, 0).set(mult(iy, 9));
		iz.add(ix);
		wsaaAllocBasis.set(t5537rec.actAllocBasis[iz.toInt()]);
	}

protected void readT55362400()
	{
		para2410();
	}

protected void para2410()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(bchgallrec.company);
		itdmIO.setItemtabl(t5536);
		itdmIO.setItemitem(wsaaAllocBasis);
		itdmIO.setItmfrm(bchgallrec.occdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError500();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), bchgallrec.company)
		|| isNE(itdmIO.getItemtabl(), t5536)
		|| isNE(itdmIO.getItemitem(), wsaaAllocBasis)) {
			itdmIO.setItemcoy(bchgallrec.company);
			itdmIO.setItemtabl(t5536);
			itdmIO.setItemitem(wsaaAllocBasis);
			itdmIO.setItmfrm(bchgallrec.occdate);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(varcom.mrnf);
			databaseError500();
		}
		t5536rec.t5536Rec.set(itdmIO.getGenarea());
	}

protected void getNewOldT55362500()
	{
		init2510();
		getNewT55362520();
		getOldT55362530();
	}

protected void init2510()
	{
		for (ix.set(1); !(isGT(ix, 7)); ix.add(1)){
			wsaaT5536ValuesInner.wsaaNewMaxPeriod[ix.toInt()].set(ZERO);
			wsaaT5536ValuesInner.wsaaNewPcInitUnit[ix.toInt()].set(ZERO);
			wsaaT5536ValuesInner.wsaaNewPcUnit[ix.toInt()].set(ZERO);
			wsaaT5536ValuesInner.wsaaOldMaxPeriod[ix.toInt()].set(ZERO);
		}
	}

protected void getNewT55362520()
	{
		iy.set(ZERO);
		for (ix.set(1); !(isGT(ix, 6)); ix.add(1)){
			if (isEQ(bchgallrec.newBillfreq, t5536rec.billfreq[ix.toInt()])) {
				iy.set(ix);
				ix.set(7);
			}
		}
		if (isEQ(iy, 1)){
			wsaaT5536ValuesInner.wsaaNewMaxPeriods.set(t5536rec.maxPeriodas);
			wsaaT5536ValuesInner.wsaaNewPcInitUnits.set(t5536rec.pcInitUnitsas);
			wsaaT5536ValuesInner.wsaaNewPcUnits.set(t5536rec.pcUnitsas);
		}
		else if (isEQ(iy, 2)){
			wsaaT5536ValuesInner.wsaaNewMaxPeriods.set(t5536rec.maxPeriodbs);
			wsaaT5536ValuesInner.wsaaNewPcInitUnits.set(t5536rec.pcInitUnitsbs);
			wsaaT5536ValuesInner.wsaaNewPcUnits.set(t5536rec.pcUnitsbs);
		}
		else if (isEQ(iy, 3)){
			wsaaT5536ValuesInner.wsaaNewMaxPeriods.set(t5536rec.maxPeriodcs);
			wsaaT5536ValuesInner.wsaaNewPcInitUnits.set(t5536rec.pcInitUnitscs);
			wsaaT5536ValuesInner.wsaaNewPcUnits.set(t5536rec.pcUnitscs);
		}
		else if (isEQ(iy, 4)){
			wsaaT5536ValuesInner.wsaaNewMaxPeriods.set(t5536rec.maxPeriodds);
			wsaaT5536ValuesInner.wsaaNewPcInitUnits.set(t5536rec.pcInitUnitsds);
			wsaaT5536ValuesInner.wsaaNewPcUnits.set(t5536rec.pcUnitsds);
		}
		else if (isEQ(iy, 5)){
			wsaaT5536ValuesInner.wsaaNewMaxPeriods.set(t5536rec.maxPeriodes);
			wsaaT5536ValuesInner.wsaaNewPcInitUnits.set(t5536rec.pcInitUnitses);
			wsaaT5536ValuesInner.wsaaNewPcUnits.set(t5536rec.pcUnitses);
		}
		else if (isEQ(iy, 6)){
			wsaaT5536ValuesInner.wsaaNewMaxPeriods.set(t5536rec.maxPeriodfs);
			wsaaT5536ValuesInner.wsaaNewPcInitUnits.set(t5536rec.pcInitUnitsfs);
			wsaaT5536ValuesInner.wsaaNewPcUnits.set(t5536rec.pcUnitsfs);
		}
	}

protected void getOldT55362530()
	{
		iy.set(ZERO);
		for (ix.set(1); !(isGT(ix, 6)); ix.add(1)){
			if (isEQ(bchgallrec.oldBillfreq, t5536rec.billfreq[ix.toInt()])) {
				iy.set(ix);
				ix.set(7);
			}
		}
		if (isEQ(iy, 1)){
			wsaaT5536ValuesInner.wsaaOldMaxPeriods.set(t5536rec.maxPeriodas);
		}
		else if (isEQ(iy, 2)){
			wsaaT5536ValuesInner.wsaaOldMaxPeriods.set(t5536rec.maxPeriodbs);
		}
		else if (isEQ(iy, 3)){
			wsaaT5536ValuesInner.wsaaOldMaxPeriods.set(t5536rec.maxPeriodcs);
		}
		else if (isEQ(iy, 4)){
			wsaaT5536ValuesInner.wsaaOldMaxPeriods.set(t5536rec.maxPeriodds);
		}
		else if (isEQ(iy, 5)){
			wsaaT5536ValuesInner.wsaaOldMaxPeriods.set(t5536rec.maxPeriodes);
		}
		else if (isEQ(iy, 6)){
			wsaaT5536ValuesInner.wsaaOldMaxPeriods.set(t5536rec.maxPeriodfs);
		}
	}

protected void calcSlots2600()
	{
		para2610();
	}

protected void para2610()
	{
		inciIO.setPcunit(ix, wsaaT5536ValuesInner.wsaaNewPcUnit[ix.toInt()]);
		inciIO.setUsplitpc(ix, wsaaT5536ValuesInner.wsaaNewPcInitUnit[ix.toInt()]);
		if (isEQ(wsaaT5536ValuesInner.wsaaNewMaxPeriod[ix.toInt()], 0)) {
			inciIO.setPremst(ix, ZERO);
			inciIO.setPremcurr(ix, ZERO);
			return ;
		}
		if (isGT(ix, 1)) {
			compute(iy, 0).set(sub(ix, 1));
			compute(wsaaNofNewPeriods, 0).set(sub(wsaaT5536ValuesInner.wsaaNewMaxPeriod[ix.toInt()], wsaaT5536ValuesInner.wsaaNewMaxPeriod[iy.toInt()]));
			compute(wsaaNofOldPeriods, 0).set(sub(wsaaT5536ValuesInner.wsaaOldMaxPeriod[ix.toInt()], wsaaT5536ValuesInner.wsaaOldMaxPeriod[iy.toInt()]));
		}
		else {
			wsaaNofNewPeriods.set(wsaaT5536ValuesInner.wsaaNewMaxPeriod[ix.toInt()]);
			wsaaNofOldPeriods.set(wsaaT5536ValuesInner.wsaaOldMaxPeriod[ix.toInt()]);
		}
		if (isEQ(inciIO.getPremcurr(ix), inciIO.getPremst(ix))) {
			setPrecision(inciIO.getPremst(ix), 2);
			inciIO.setPremst(ix, mult(wsaaInstprem, wsaaNofNewPeriods));
			inciIO.setPremcurr(ix, inciIO.getPremst(ix));
			return ;
		}
		if (isEQ(inciIO.getPremcurr(ix), ZERO)) {
			setPrecision(inciIO.getPremst(ix), 2);
			inciIO.setPremst(ix, mult(wsaaInstprem, wsaaNofNewPeriods));
			return ;
		}
		compute(wsaaFreqFactor1, 12).set(div(inciIO.getPremcurr(ix), inciIO.getCurrPrem()));
		compute(wsaaFreqFactor1, 12).set(sub(wsaaNofOldPeriods, wsaaFreqFactor1));
		compute(wsaaFreqFactor2, 12).set(mult((div(wsaaFreqFactor1, wsaaOldFreq)), wsaaNewFreq));
		setPrecision(inciIO.getPremcurr(ix), 12);
		inciIO.setPremcurr(ix, mult(wsaaInstprem, (sub(wsaaNofNewPeriods, wsaaFreqFactor2))));
		setPrecision(inciIO.getPremst(ix), 2);
		inciIO.setPremst(ix, mult(wsaaInstprem, wsaaNofNewPeriods));
	}

protected void updateFpco3000()
	{
		start3010();
	}

protected void start3010()
	{
		fpcoIO.setParams(SPACES);
		fpcoIO.setChdrcoy(bchgallrec.company);
		fpcoIO.setChdrnum(bchgallrec.chdrnum);
		fpcoIO.setCoverage(bchgallrec.coverage);
		fpcoIO.setLife(bchgallrec.life);
		fpcoIO.setRider(bchgallrec.rider);
		fpcoIO.setPlanSuffix(bchgallrec.planSuffix);
		fpcoIO.setTargfrom(0);
		fpcoIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)
		&& isNE(fpcoIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(fpcoIO.getStatuz());
			syserrrec.params.set(fpcoIO.getParams());
			databaseError500();
		}
		if (isNE(fpcoIO.getChdrcoy(), bchgallrec.company)
		|| isNE(fpcoIO.getChdrnum(), bchgallrec.chdrnum)
		|| isNE(fpcoIO.getCoverage(), bchgallrec.coverage)
		|| isNE(fpcoIO.getLife(), bchgallrec.life)
		|| isNE(fpcoIO.getRider(), bchgallrec.rider)
		|| isNE(fpcoIO.getPlanSuffix(), bchgallrec.planSuffix)
		|| isEQ(fpcoIO.getStatuz(), varcom.endp)) {
			return ;
		}
		fpcoIO.setValidflag("2");
		fpcoIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			databaseError500();
		}
		wsaaFlexPremFq.set(bchgallrec.newBillfreq);
		setPrecision(fpcoIO.getTargetPremium(), 2);
		fpcoIO.setTargetPremium(mult(wsaaInstprem, wsaaFlexPremFq));
		fpcoIO.setTranno(bchgallrec.tranno);
		fpcoIO.setValidflag("1");
		fpcoIO.setEffdate(bchgallrec.effdate);
		fpcoIO.setFunction(varcom.writr);
		fpcoIO.setFormat(fpcorec);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcoIO.getStatuz());
			syserrrec.params.set(fpcoIO.getParams());
			databaseError500();
		}
	}

protected void callRounding4000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bchgallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(bchgallrec.cntcurr);
		zrdecplrec.batctrcde.set(bchgallrec.trancode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			databaseError500();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-T5536-VALUES--INNER
 */
private static final class WsaaT5536ValuesInner { 

		/* WSAA-T5536-VALUES */
	private FixedLengthStringData wsaaNewMaxPeriods = new FixedLengthStringData(28);
	private ZonedDecimalData[] wsaaNewMaxPeriod = ZDArrayPartOfStructure(7, 4, 0, wsaaNewMaxPeriods, 0);

	private FixedLengthStringData wsaaNewPcUnits = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaNewPcUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaNewPcUnits, 0);

	private FixedLengthStringData wsaaNewPcInitUnits = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaNewPcInitUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaNewPcInitUnits, 0);

	private FixedLengthStringData wsaaOldMaxPeriods = new FixedLengthStringData(28);
	private ZonedDecimalData[] wsaaOldMaxPeriod = ZDArrayPartOfStructure(7, 4, 0, wsaaOldMaxPeriods, 0);

	private FixedLengthStringData wsaaOldPcUnits = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaOldPcUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaOldPcUnits, 0);

	private FixedLengthStringData wsaaOldPcInitUnits = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaOldPcInitUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaOldPcInitUnits, 0);
	}
}
