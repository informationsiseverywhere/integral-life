/*
 * File: Ph593.java
 * Date: 30 August 2009 1:09:50
 * Author: Quipoz Limited
 * 
 * Class transformed from PH593.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.RpdetpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rpdetpf;
import com.csc.life.contractservicing.procedures.Intcalc;
import com.csc.life.contractservicing.recordstructures.Intcalcrec;
import com.csc.life.contractservicing.screens.Sh593ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*    Policy Loan Enquiry
*
*    This program is aim to provide a Loan Enquiry which gives
*    a summary total of all loans and interest outstanding,
*    together with individual loan details showing the current
*    balance and interest outstanding.
*
*    Summary Totals will initially be displayed in contract
*    currency, but the summary loan amounts can be displayed in
*    any valid currency selected.
*
* Initialise.
* -----------
*
* Read the  CHDRENQ contract  (function  RETRV)  and  read  the
* relevant data necessary for obtaining the status description,
* the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
* and the Bill-to-date.
*
* Obtain the  necessary  descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Format the Ownername and the subsequent Life & Joint life
*  names by first reading the respective Client (CLTS) record
*  and use the CONFNAME procedure division copybook to format
*  the required names.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* The Life & Joint life Client numbers can be obtained from the
* first Coverage record read.
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*      - READR  the   life  details  using  LIFExxx  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*
*      - READR the  joint-life  details using LIFExxx (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
* OUTSTANDING LOAN VALUES
*
* Call the 'TOTLOAN' subroutine to calculate the current value
*  of all existing ( if any ) loans held against the contract
*  we are processing.
* 'TOTLOAN' will return the total loan Principal and the total
*  loan Interest outstanding at the given effective date.
*
* Validate.
* ---------
* Validate chosen currency
* IF user changes currency loan is to be paid in, re-calculate
* summary total values on screen and redisplay SH593.
*
* Next Program.
* -------------
*    Add 1 to program pointer
*
* TABLES USED.
* ------------
*
* T3588 - Contract Premium statii
* T3623 - Contract Risk statii
* T3629 - Currency Code Details
* T5645 - Transaction Accounting Rules
* T5688 - Contract Types
* T6633 - Loan Interest Rules
*
*****************************************************************
* </pre>
*/
public class Ph593 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH593");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3);
	private PackedDecimalData wsaaPrincipal = new PackedDecimalData(8, 2);
	private PackedDecimalData wsaaInterest = new PackedDecimalData(8, 2);
	private PackedDecimalData wsaaTotal = new PackedDecimalData(9, 2);
	private FixedLengthStringData wsaaHeldCurrency = new FixedLengthStringData(3);
	private PackedDecimalData wsaaHeldPrincipal = new PackedDecimalData(8, 2);
	private PackedDecimalData wsaaHeldInterest = new PackedDecimalData(8, 2);
	private PackedDecimalData wsaaHeldTotal = new PackedDecimalData(9, 2);
	private PackedDecimalData wsaaIndTotal = new PackedDecimalData(9, 2);
	private PackedDecimalData wsaaRepayTotal = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaapproval = new FixedLengthStringData(10);
	private FixedLengthStringData repaymethod = new FixedLengthStringData(1);
	private FixedLengthStringData repaystatus = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSubfileSize = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private PackedDecimalData wsaaBaseIdx = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaT5645Idx = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();

	private FixedLengthStringData wsaaT6633Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6633Item, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(5).isAPartOf(wsaaT6633Item, 3, FILLER).init("P    ");

		/* FORMATS */
	private static final String loanrec = "LOANREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private LoanTableDAM loanIO = new LoanTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Intcalcrec intcalcrec = new Intcalcrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5645rec t5645rec = new T5645rec();
	private T6633rec t6633rec = new T6633rec();
	protected Sh593ScreenVars sv = getLScreenVars();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);//ILIFE-5826
	private RpdetpfDAO rpdetpfDAO = getApplicationContext().getBean("rpdetpfDAO", RpdetpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Chdrpf chdrpf = new Chdrpf();//ILIFE-5826
	private boolean policyLoanFlag = false;
	private Descpf descpf = null;
	List<Rpdetpf> rpdetpfList = new ArrayList<Rpdetpf>();
	
	// CML-072
	boolean CSLND001Permission = false;
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO" , LifepfDAO.class);
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private Clntpf cltsIO = null;
	private Itempf itempf = null;
	private Acblpf acblpf = null;
	protected Sh593ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sh593ScreenVars.class);
	}
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2050, 
		exit2090, 
		writeToSubfile5010, 
		skipLoanRead5080, 
		exit5090
	}

	public Ph593() {
		super();
		screenVars = sv;
		new ScreenModel("Sh593", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		// CML072Start
		CSLND001Permission = FeaConfg.isFeatureExist("2", "CSLND001", appVars,
				"IT");
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit2090);
		}
		// CML072End
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaPrincipal.set(ZERO);
		wsaaInterest.set(ZERO);
		wsaaTotal.set(ZERO);
		wsaaCurrency.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		/*    Build screen....*/
		/*    Retrieve Contract header to get the ball rolling..*/
		//ILIFE-5826
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.currcd.set(chdrpf.getCntcurr());
		wsaaCurrency.set(chdrpf.getCntcurr());
		wsaaHeldCurrency.set(chdrpf.getCntcurr());
		sv.currfrom.set(chdrpf.getOccdate());
		sv.cownnum.set(chdrpf.getCownnum());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());
		/* Get Descriptions*/
		/* Get Contract Descriptions*/
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), "T5688" ,chdrpf.getCnttype(),chdrpf.getChdrcoy().toString(), wsspcomn.language.toString());
		if (descpf != null) {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		/* Get Contract Status Descriptions*/
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), "T3623" ,chdrpf.getStatcode(),chdrpf.getChdrcoy().toString(), wsspcomn.language.toString());
		if (descpf != null) {
			sv.chdrstatus.set(descpf.getShortdesc());
		}
		else {
			sv.chdrstatus.fill("?");
		}
		/* Get Premium Status Descriptions*/
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), "T3588" ,chdrpf.getPstcde(),chdrpf.getChdrcoy().toString(), wsspcomn.language.toString());
		if (descpf != null) {
			sv.premstatus.set(descpf.getShortdesc());
		}
		else {
			sv.premstatus.fill("?");
		}
		/* Set Effective Date from WSSP date passed in from submenu.*/
		sv.effdate.set(wsspcomn.currfrom);
		/* Get Contract Owner Name.*/
		cltsIO = clntpfDAO.searchClntRecord(fsupfxcpy.clnt.toString(),chdrpf.getCowncoy().toString(),chdrpf.getCownnum());
		if (cltsIO == null
		|| isNE(cltsIO.getValidflag(), "1")) {
			sv.ownerdescErr.set("E304");
			sv.ownerdesc.set(SPACES);
		}
		else {
			plainname();
			sv.ownerdesc.set(wsspcomn.longconfname);
		}
		/* Get Life Assured Name.*/
		Lifepf lifeenqIO = new Lifepf();
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy().toString());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setValidflag("1");
		Map<String, Lifepf> lifepfMap =lifepfDAO.getLifeEnqData(lifeenqIO);
		lifeenqIO= lifepfMap.get(lifeenqIO.getChdrcoy().trim()+""+lifeenqIO.getChdrnum().trim()+""+lifeenqIO.getLife().trim()+""+lifeenqIO.getJlife().trim()+""+lifeenqIO.getValidflag().trim());
		if (lifeenqIO == null
		|| isNE(lifeenqIO.getValidflag(), "1")) {
			sv.lifedescErr.set("E355");
			sv.lifedesc.set(SPACES);
		}
		else {
			sv.lifenum.set(lifeenqIO.getLifcnum());
			plainname();
			sv.lifedesc.set(wsspcomn.longconfname);
		}
		/* Get J/Life Name.*/
		lifeenqIO = new Lifepf();
		lifeenqIO.setChdrcoy(chdrpf.getChdrcoy().toString());
		lifeenqIO.setChdrnum(chdrpf.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("01");
		lifeenqIO.setValidflag("1");
		lifepfMap =lifepfDAO.getLifeEnqData(lifeenqIO);
		lifeenqIO= lifepfMap.get(lifeenqIO.getChdrcoy().trim()+""+lifeenqIO.getChdrnum().trim()+""+lifeenqIO.getLife().trim()+""+lifeenqIO.getJlife().trim()+""+lifeenqIO.getValidflag().trim());

		if (lifeenqIO == null
		|| isNE(lifeenqIO.getValidflag(), "1")) {
			sv.jlife.set(SPACES);
			sv.jlifedesc.set(SPACES);
		}
		else {
			sv.jlife.set(lifeenqIO.getLifcnum());
			plainname();
			sv.jlifedesc.set(wsspcomn.longconfname);
		}
		wsaaItemCnttype.set(chdrpf.getCnttype());
		List<Itempf> itempfList = itemDAO.getItdmByFrmdate(chdrpf.getChdrcoy().toString(),"T6633",wsaaT6633Item.toString(),chdrpf.getOccdate());
		
		if (itempfList == null || itempfList.isEmpty()) {
			syserrrec.statuz.set("E723");
			fatalError600();
		}
		else {
			t6633rec.t6633Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		sv.intanny.set(t6633rec.intRate);
		/* Check the presence of existing loans and the values of*/
		/* those loans*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(sv.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		sv.numberOfLoans.set(totloanrec.loanCount);
		sv.hpleamt.set(totloanrec.principal);
		wsaaPrincipal.set(totloanrec.principal);
		wsaaHeldPrincipal.set(totloanrec.principal);
		sv.hpleint.set(totloanrec.interest);
		wsaaInterest.set(totloanrec.interest);
		wsaaHeldInterest.set(totloanrec.interest);
		compute(wsaaTotal, 3).setRounded(add(wsaaPrincipal, wsaaInterest));
		sv.hpletot.set(wsaaTotal);
		wsaaHeldTotal.set(wsaaTotal);
		/* Read T5645 for finanical accounting rules.*/
		itempf = new Itempf();
	    itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(chdrpf.getChdrcoy().toString());
		itempf.setItemtabl("T5645");
		itempf.setItemitem("HRTOTLON");
	    itempf = itemDAO.getItempfRecord(itempf);
		if (itempf == null) {
			syserrrec.statuz.set("H134");
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		scrnparams.function.set(varcom.sclr);
		processScreen("SH593", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		policyLoanFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSLND002", appVars, "IT");
		/*    Perform section to load first page of subfile*/
		loanIO.setParams(SPACES);
		loanIO.setChdrcoy(chdrpf.getChdrcoy());
		loanIO.setChdrnum(chdrpf.getChdrnum());
		loanIO.setLoanNumber(0);
		loanIO.setFormat(loanrec);
		loanIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		if (isGT(totloanrec.loanCount, 0)) {
			readLoan1500();
			loadSubfile5000();
		}
		else {
			scrnparams.function.set(varcom.prot);
		}

		if(policyLoanFlag){
			sv.actionflag.set("Y");
		}
		else{
			sv.actionflag.set("N");
		}
		
		if (!CSLND001Permission) {
			sv.cslnd001Flag.set("N");
		}
 else {
			sv.cslnd001Flag.set("Y");
		}
		
		scrnparams.subfileRrn.set(1);
	}
protected void writeToSubfile5011(Loanpf loanobj){
	
}


protected void readLoan1500()
	{
		/*START*/
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)
		&& isNE(loanIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(loanIO.getParams());
			fatalError600();
		}
		if (isNE(loanIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(loanIO.getChdrnum(), chdrpf.getChdrnum())
		|| isEQ(loanIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanIO.getParams());
			syserrrec.statuz.set(loanIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateScreen2010();
					checkPfkey2040();
				case checkForErrors2050: 
					checkForErrors2050();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SH593IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         SH593-DATA-AREA                         */
		/*                         SH593-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			loadSubfile5000();
			wsspcomn.edterror.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.currcd, SPACES)) {
			sv.currcdErr.set("H960");
			goTo(GotoLabel.checkForErrors2050);
		}
		/* Convert Currency if we have to*/
		if (isNE(sv.currcd, wsaaCurrency)) {
			if (isEQ(sv.currcd, wsaaHeldCurrency)) {
				sv.hpleamt.set(wsaaHeldPrincipal);
				sv.hpleint.set(wsaaHeldInterest);
				sv.hpletot.set(wsaaHeldTotal);
				wsaaCurrency.set(sv.currcd);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			itempf = new Itempf();
		    itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(chdrpf.getChdrcoy().toString());
			itempf.setItemtabl("T3629");
			itempf.setItemitem(sv.currcd.toString());
		    itempf = itemDAO.getItempfRecord(itempf);
		    
			if (itempf == null) {
				syserrrec.params.set("F982");
				sv.currcdErr.set("F982");
				goTo(GotoLabel.checkForErrors2050);
			}
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(wsaaHeldPrincipal);
			conlinkrec.statuz.set(SPACES);
			conlinkrec.function.set("REAL");
			conlinkrec.currIn.set(wsaaHeldCurrency);
			conlinkrec.cashdate.set(varcom.vrcmMaxDate);
			conlinkrec.currOut.set(sv.currcd);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(chdrpf.getChdrcoy());
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				scrnparams.errorCode.set(conlinkrec.statuz);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			else {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(sv.currcd);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
				wsaaPrincipal.set(conlinkrec.amountOut);
				sv.hpleamt.set(conlinkrec.amountOut);
			}
			conlinkrec.amountIn.set(wsaaHeldInterest);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				scrnparams.errorCode.set(conlinkrec.statuz);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			else {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(sv.currcd);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
				wsaaInterest.set(conlinkrec.amountOut);
				sv.hpleint.set(conlinkrec.amountOut);
			}
			compute(wsaaTotal, 2).set(add(wsaaPrincipal, wsaaInterest));
			sv.hpletot.set(wsaaTotal);
			wsaaCurrency.set(sv.currcd);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkPfkey2040()
	{
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SH593", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SH593", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SH593", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadSubfile5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case writeToSubfile5010: 
					writeToSubfile5010();
				case skipLoanRead5080: 
					skipLoanRead5080();
				case exit5090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void writeToSubfile5010()
	{
		if (isNE(loanIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(loanIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(loanIO.getValidflag(), "1")
		|| isEQ(loanIO.getStatuz(), varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
			goTo(GotoLabel.exit5090);
		}
		compute(wsaaCnt, 0).setDivide(scrnparams.subfileRrn, (wsaaSubfileSize));
		wsaaRem.setRemainder(wsaaCnt);
		if (isEQ(wsaaRem, 0)) {
			scrnparams.subfileMore.set("Y");
			goTo(GotoLabel.exit5090);
		}
		wsaaIndTotal.set(0);
		/* Work out Pending Interest Since last Billing date.*/
		if (isLT(loanIO.getLastIntBillDate(), sv.effdate)) {
			intcalcrec.intcalcRec.set(SPACES);
			intcalcrec.loanNumber.set(loanIO.getLoanNumber());
			intcalcrec.chdrcoy.set(loanIO.getChdrcoy());
			intcalcrec.chdrnum.set(loanIO.getChdrnum());
			intcalcrec.cnttype.set(chdrpf.getCnttype());
			intcalcrec.interestTo.set(sv.effdate);
			intcalcrec.interestFrom.set(loanIO.getLastIntBillDate());
			intcalcrec.loanorigam.set(loanIO.getLastCapnLoanAmt());
			intcalcrec.lastCaplsnDate.set(loanIO.getLastCapnDate());
			intcalcrec.loanStartDate.set(loanIO.getLoanStartDate());
			intcalcrec.interestAmount.set(ZERO);
			intcalcrec.loanCurrency.set(loanIO.getLoanCurrency());
			intcalcrec.loanType.set(loanIO.getLoanType());
			callProgram(Intcalc.class, intcalcrec.intcalcRec);
			if (isNE(intcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(intcalcrec.intcalcRec);
				syserrrec.statuz.set(intcalcrec.statuz);
			}
		}
		else {
			intcalcrec.interestAmount.set(0);
		}
		zrdecplrec.amountIn.set(intcalcrec.interestAmount);
		zrdecplrec.currency.set(loanIO.getLoanCurrency());
		a000CallRounding();
		intcalcrec.interestAmount.set(zrdecplrec.amountOut);
		sv.hpndint.set(intcalcrec.interestAmount);
		wsaaIndTotal.add(intcalcrec.interestAmount);
		/* Work out Current Interest Balance.*/
		/* Initialise index to read the Accounting Rules in Table*/
		/* T5645. Currently the Accounting Rules for this process*/
		/* are stored in the following order:*/
		/*    1st 4 - Policy Loan*/
		/*    2nd 4 - APL*/
		/*    3rd 4 - Cash Deposit (Anticipated Endowment)*/
		/* Thus, we initialise the index with 0, 4, and 8 for Policy*/
		/* Loan, APL and Cash Deposit respectively depending on the*/
		/* Loan Type.*/
		/* The Loan Types are 'P for Policy Loan, 'A' for APL and 'E'*/
		/* for Cash Deposit.*/
		if (isEQ(loanIO.getLoanType(), "P")){
			wsaaBaseIdx.set(0);
		}
		else if (isEQ(loanIO.getLoanType(), "A")){
			wsaaBaseIdx.set(4);
		}
		else if (isEQ(loanIO.getLoanType(), "E")){
			wsaaBaseIdx.set(8);
		}
		else{
			goTo(GotoLabel.skipLoanRead5080);
		}
		/* Get all Existing Loan Interest ACBL which exist for this*/
		/* loan.*/
		getInterestAcbl6000();
		/* Get all Existing Loan Balance ACBL which exist for this*/
		/* loan.*/
		getLoanAcbl6100();
		sv.hpltot.set(wsaaIndTotal);
		sv.hprincipal.set(loanIO.getLoanOriginalAmount());
		sv.loanNumber.set(loanIO.getLoanNumber());
		sv.loanType.set(loanIO.getLoanType());
		sv.cntcurr.set(loanIO.getLoanCurrency());
		sv.loanStartDate.set(loanIO.getLoanStartDate());
		// CML-075
		if (CSLND001Permission) {
			sv.nxtintbdte.set(loanIO.getNextIntBillDate());
			sv.nxtcapdate.set(loanIO.getNextCapnDate());
		}
		sv.numcon.set(loanIO.getTplstmdty());
		if(policyLoanFlag){
			getRpdetRecord();
			//compute(wsaaIndTotal, 2).set(sub(wsaaIndTotal, wsaaRepayTotal));
		}
		//sv.hpltot.set(wsaaIndTotal);
		scrnparams.function.set(varcom.sadd);
		processScreen("SH593", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.add(1);
	}

protected void getRpdetRecord(){
	
	rpdetpfList = rpdetpfDAO.getRpdetRecord(chdrpf.getChdrnum().trim(), sv.loanNumber.toInt());/* IJTI-1523 */
	for(Rpdetpf rpdetobj : rpdetpfList){
		wsaaRepayTotal.add(rpdetobj.getRepayamt());
		wsaaapproval.set(rpdetobj.getApprovaldate());
		repaymethod.set(rpdetobj.getRepaymethod());
		repaystatus.set(rpdetobj.getRepaystatus());	
	}
	if (rpdetpfList.isEmpty()) {
		sv.totrepd.set(0);
		sv.repdstat.set(SPACES);
		sv.repymop.set(SPACES);
		sv.repdate.clear();
		sv.repdateDisp.clear();
	
	} else {
		sv.totrepd.set(wsaaRepayTotal);
		getlongdesctn();
		sv.repdate.set(wsaaapproval.toString());
	   }
	}

protected void getlongdesctn(){
	descpf = descDAO.getdescData("IT", "TD5HA", repaystatus.toString().trim(), chdrpf.getChdrcoy().toString().trim(),
			wsspcomn.language.toString().trim());
	if (descpf == null) {
		sv.repdstat.set(SPACES);
	} else {
		sv.repdstat.set(descpf.getLongdesc());
	}
	
	descpf = descDAO.getdescData("IT", "TD5H2", repaymethod.toString().trim(), chdrpf.getChdrcoy().toString().trim(),
					wsspcomn.language.toString().trim());
	if (descpf == null) {
		sv.repymop.set(SPACES);
	} else {
		sv.repymop.set(descpf.getLongdesc());
	}		
}
protected void skipLoanRead5080()
	{
		initialize(sv.subfileFields);
		loanIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, loanIO);
		if (isNE(loanIO.getStatuz(), varcom.oK)
		&& isNE(loanIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanIO.getParams());
			fatalError600();
		}
		goTo(GotoLabel.writeToSubfile5010);
	}

protected void getInterestAcbl6000(){
	
	/* Use entry 2 on the T5645 record read to get Loan interest*/
	/* ACBL for this loan number.*/
	/* COMPUTE WSAA-T5645-IDX      =  WSAA-BASE-IDX + 2.*/
	/* COMPUTE WSAA-T5645-IDX      =  WSAA-BASE-IDX + 4.            */
	compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 2));
	wsaaSacscode.set(t5645rec.sacscode[wsaaT5645Idx.toInt()]);
	wsaaSacstype.set(t5645rec.sacstype[wsaaT5645Idx.toInt()]);
	
	wsaaRldgacct.set(SPACES);
	wsaaChdrnum.set(loanIO.getChdrnum());
	wsaaLoanNumber.set(loanIO.getLoanNumber());
	
	acblpf = acblDao.loadDataByBatch(loanIO.getChdrcoy().toString(),wsaaSacscode.toString(),
			wsaaRldgacct.toString(),loanIO.getLoanCurrency().toString(),wsaaSacstype.toString());
	if (acblpf == null) {
		acblpf = new Acblpf();
		acblpf.setSacscurbal(BigDecimal.ZERO);
	}
	/*    GO TO 6090-EXIT.*/
	sv.hacrint.set(acblpf.getSacscurbal());
	wsaaIndTotal.set(new BigDecimal(wsaaIndTotal.toString().trim()).add(acblpf.getSacscurbal()));
}

protected void getLoanAcbl6100(){
		/* Use entry 1 on the T5645 record read to get Loan*/
		/* ACBL for this loan number.*/
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 1));
		wsaaSacscode.set(t5645rec.sacscode[wsaaT5645Idx.toInt()]);
		wsaaSacstype.set(t5645rec.sacstype[wsaaT5645Idx.toInt()]);
		
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(loanIO.getChdrnum());
		wsaaLoanNumber.set(loanIO.getLoanNumber());
		
		acblpf = acblDao.loadDataByBatch(loanIO.getChdrcoy().toString(),wsaaSacscode.toString(),
				wsaaRldgacct.toString(),loanIO.getLoanCurrency().toString(),wsaaSacstype.toString());

		if (acblpf == null) {
			fatalError600();
		}
		sv.hcurbal.set(acblpf.getSacscurbal());
		wsaaIndTotal.set(new BigDecimal(wsaaIndTotal.toString().trim()).add(acblpf.getSacscurbal()));
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
}