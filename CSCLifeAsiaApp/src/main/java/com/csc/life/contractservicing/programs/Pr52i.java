/*
 * File: Pr52i.java
 * Date: December 3, 2013 3:26:28 AM ICT
 * Author: CSC

 * 
 * Class transformed from PR52I.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.contractservicing.screens.Sr52iScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
****************************************************************** ****
* </pre>
*/
public class Pr52i extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR52I");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaItemToRead = new FixedLengthStringData(8);
	private PackedDecimalData wsaaGndtotal = new PackedDecimalData(17, 2);
		/* TABLES */
	private static final String tr52b = "TR52B";
	private static final String tr52d = "TR52D";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String descrec = "DESCREC";
//	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
//	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr52drec tr52drec = new Tr52drec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Sr52iScreenVars sv = ScreenProgram.getScreenVars( Sr52iScreenVars.class);
	private ChdrpfDAO chdrDao = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrenqpf = new Covrpf();
	private Tr52erec tr52erec = new Tr52erec();
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	
	public Pr52i() {
		super();
		screenVars = sv;
		new ScreenModel("Sr52i", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		retrvChdr1020();
		retrvCovt1030();
		readTr52d1040();
		callTaxSubroutine1050();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaGndtotal.set(ZERO);
		/*    Dummy field initilisation for prototype version.*/
		sv.aprem.set(ZERO);
		sv.gndtotal.set(ZERO);
		sv.taxamt01.set(ZERO);
		sv.taxamt02.set(ZERO);
	}

protected void retrvChdr1020()
	{
		/* Retrieve CHDR*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		chdrpf = chdrDao.getCacheObject(chdrpf);

	}

protected void retrvCovt1030()
	{
		/* Retrieve key of the coverage/rider being processed*/
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction*/
		covrenqpf = covrpfDAO.getCacheObject(covrenqpf);
	}

protected void readTr52d1040()
	{
		/* Read table TR52D using CHDRENQ-REGISTER.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFormat(itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void callTaxSubroutine1050()
	{
		readTr52d1051();
		/* Call subroutine from TR52D*/
		initialize(txcalcrec.linkRec);
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.function.set("CALC");
		txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
		txcalcrec.chdrnum.set(chdrpf.getChdrnum());
		txcalcrec.life.set(covrenqpf.getLife());
		txcalcrec.coverage.set(covrenqpf.getCoverage());
		txcalcrec.rider.set(covrenqpf.getRider());
		txcalcrec.planSuffix.set(covrenqpf.getPlanSuffix());
		txcalcrec.crtable.set(covrenqpf.getCrtable());
		txcalcrec.cnttype.set(chdrpf.getCnttype());
		txcalcrec.register.set(chdrpf.getReg());
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		txcalcrec.taxrule.set(SPACES);
		//txcalcrec.rateItem.set(SPACES);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrpf.getCntcurr());
		wsaaCntCurr.set(chdrpf.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		//txcalcrec.amountIn.set(ZERO);
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(covrenqpf.getZbinstprem());
		}
		else {
			txcalcrec.amountIn.set(covrenqpf.getInstprem());
		}
		txcalcrec.transType.set("PREM");
		txcalcrec.txcode.set(tr52drec.txcode);
		if (isEQ(chdrpf.getBillfreq(), "00")) {
			txcalcrec.effdate.set(covrenqpf.getCrrcd());
		}
		else {
			txcalcrec.effdate.set(chdrpf.getPtdate());
		}
		txcalcrec.tranno.set(chdrpf.getTranno());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		/*    Set screen fields*/
		sv.aprem.set(txcalcrec.amountIn);
		wsaaItemToRead.set(SPACES);
		wsaaItemToRead.set(txcalcrec.taxType[1]);
		readDesc1100();
		sv.descript01.set(descIO.getLongdesc());
		if (sv.descript01.containsOnly("?")) {
			sv.descript01.set(SPACES);
		}
		sv.taxamt01.set(txcalcrec.taxAmt[1]);
		wsaaItemToRead.set(SPACES);
		wsaaItemToRead.set(txcalcrec.taxType[2]);
		readDesc1100();
		sv.descript02.set(descIO.getLongdesc());
		if (sv.descript02.containsOnly("?")) {
			sv.descript02.set(SPACES);
		}
		sv.taxamt02.set(txcalcrec.taxAmt[2]);
		compute(wsaaGndtotal, 3).setRounded(add(txcalcrec.taxAmt[1], txcalcrec.taxAmt[2]));
		sv.gndtotal.set(wsaaGndtotal);
	}

protected void readDesc1100()
	{
		start1110();
	}

protected void start1110()
	{
		/* Get description from table TR52D*/
		sv.gndtotal.set(wsaaGndtotal);
		descIO.setRecKeyData(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDesctabl(tr52b);
		descIO.setDescitem(wsaaItemToRead);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}

protected void readTr52d1051() {
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrpf.getCnttype());
	wsaaTr52eCrtable.set(covrenqpf.getCrtable());
	a300ReadTr52e();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set("****");
		a300ReadTr52e();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a300ReadTr52e();
	}
}

protected void a300ReadTr52e() {
	//	List<Itempf> tr52eList = itemDAO.getAllitemsbyCurrency("IT", wsspcomn.company.toString(), "TR52E", wsaaTr52eKey.toString());
		List<Itempf> tr52eList = itempfDAO.getAllItemitemByDateFrm("IT", wsspcomn.company.toString(), "TR52E", wsaaTr52eKey.toString().trim(), chdrpf.getCcdate()); //ILFE-7560
		if (tr52eList != null && !tr52eList.isEmpty()) {
			Iterator<Itempf> iterator = tr52eList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (itempf.getItmfrm().compareTo(new BigDecimal(chdrpf.getOccdate().toString())) <= 0) {
					tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));
					break;
				}
			}
		}else{
			if (isEQ(subString(wsaaTr52eKey, 2, 7), "*******")) {
				syserrrec.params.set(wsaaTr52eKey);
				fatalError600();
			}
		}
	}
}
