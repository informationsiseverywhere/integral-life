package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sd5klScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(261);
	public FixedLengthStringData dataFields = new FixedLengthStringData(69).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,9);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData chdrnum = DD.chdrnumfrm.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData chdrnum1 = DD.chdrnumto.copy().isAPartOf(dataFields,35);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,43);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,51);
	public ZonedDecimalData taxFrmYear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields, 61);
	public ZonedDecimalData taxToYear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields, 65);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 69);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData chdrnumtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData taxFrmYearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData taxToYearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 117);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] chdrnumtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] taxFrmYearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] taxToYearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dateFrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dateToDisp = new FixedLengthStringData(10);
	public LongData Sd5klscreenWritten = new LongData(0);
	public LongData Sd5klprotectWritten = new LongData(0);

	@Override
	public boolean hasSubfile() {
		return false;
	}

	public Sd5klScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumfrmOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumtoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxFrmYearOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxToYearOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {effdate, acctmonth, jobq, acctyear, scheduleName, scheduleNumber, bbranch, bcompany, chdrnum, chdrnum1, taxFrmYear, taxToYear};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, jobqOut, acctyearOut, bschednamOut, bschednumOut, bbranchOut, bcompanyOut, chdrnumfrmOut, chdrnumtoOut, taxFrmYearOut, taxToYearOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, jobqErr, acctyearErr, bschednamErr, bschednumErr, bbranchErr, bcompanyErr, chdrnumfrmErr, chdrnumtoErr, taxFrmYearErr, taxToYearErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp, dateFrmDisp, dateToDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5klscreen.class;
		protectRecord = Sd5klprotect.class;
	}

}
