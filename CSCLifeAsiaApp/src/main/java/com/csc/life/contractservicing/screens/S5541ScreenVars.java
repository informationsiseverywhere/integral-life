package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5541
 * @version 1.0 generated on 30/08/09 06:43
 * @author Quipoz
 */
public class S5541ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(624);
	public FixedLengthStringData dataFields = new FixedLengthStringData(144).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData freqcys = new FixedLengthStringData(24).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] freqcy = FLSArrayPartOfStructure(12, 2, freqcys, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(freqcys, 0, FILLER_REDEFINE);
	public FixedLengthStringData freqcy01 = DD.freqcy.copy().isAPartOf(filler,0);
	public FixedLengthStringData freqcy02 = DD.freqcy.copy().isAPartOf(filler,2);
	public FixedLengthStringData freqcy03 = DD.freqcy.copy().isAPartOf(filler,4);
	public FixedLengthStringData freqcy04 = DD.freqcy.copy().isAPartOf(filler,6);
	public FixedLengthStringData freqcy05 = DD.freqcy.copy().isAPartOf(filler,8);
	public FixedLengthStringData freqcy06 = DD.freqcy.copy().isAPartOf(filler,10);
	public FixedLengthStringData freqcy07 = DD.freqcy.copy().isAPartOf(filler,12);
	public FixedLengthStringData freqcy08 = DD.freqcy.copy().isAPartOf(filler,14);
	public FixedLengthStringData freqcy09 = DD.freqcy.copy().isAPartOf(filler,16);
	public FixedLengthStringData freqcy10 = DD.freqcy.copy().isAPartOf(filler,18);
	public FixedLengthStringData freqcy11 = DD.freqcy.copy().isAPartOf(filler,20);
	public FixedLengthStringData freqcy12 = DD.freqcy.copy().isAPartOf(filler,22);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,33);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,41);
	public FixedLengthStringData lfacts = new FixedLengthStringData(60).isAPartOf(dataFields, 49);
	public ZonedDecimalData[] lfact = ZDArrayPartOfStructure(12, 5, 4, lfacts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(lfacts, 0, FILLER_REDEFINE);
	public ZonedDecimalData lfact01 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData lfact02 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,5);
	public ZonedDecimalData lfact03 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData lfact04 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,15);
	public ZonedDecimalData lfact05 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,20);
	public ZonedDecimalData lfact06 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,25);
	public ZonedDecimalData lfact07 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData lfact08 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,35);
	public ZonedDecimalData lfact09 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,40);
	public ZonedDecimalData lfact10 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,45);
	public ZonedDecimalData lfact11 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,50);
	public ZonedDecimalData lfact12 = DD.lfact.copyToZonedDecimal().isAPartOf(filler1,55);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,139);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 144);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData freqcysErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] freqcyErr = FLSArrayPartOfStructure(12, 4, freqcysErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(freqcysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData freqcy01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData freqcy02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData freqcy03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData freqcy04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData freqcy05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData freqcy06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData freqcy07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData freqcy08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData freqcy09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData freqcy10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData freqcy11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData freqcy12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lfactsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] lfactErr = FLSArrayPartOfStructure(12, 4, lfactsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(lfactsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData lfact01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData lfact02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData lfact03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData lfact04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData lfact05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData lfact06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData lfact07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData lfact08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData lfact09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData lfact10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData lfact11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData lfact12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 264);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData freqcysOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] freqcyOut = FLSArrayPartOfStructure(12, 12, freqcysOut, 0);
	public FixedLengthStringData[][] freqcyO = FLSDArrayPartOfArrayStructure(12, 1, freqcyOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(144).isAPartOf(freqcysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] freqcy01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] freqcy02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] freqcy03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] freqcy04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] freqcy05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] freqcy06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] freqcy07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] freqcy08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] freqcy09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] freqcy10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] freqcy11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] freqcy12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData lfactsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] lfactOut = FLSArrayPartOfStructure(12, 12, lfactsOut, 0);
	public FixedLengthStringData[][] lfactO = FLSDArrayPartOfArrayStructure(12, 1, lfactOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(144).isAPartOf(lfactsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] lfact01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] lfact02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] lfact03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] lfact04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] lfact05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] lfact06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] lfact07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] lfact08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] lfact09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] lfact10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] lfact11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] lfact12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5541screenWritten = new LongData(0);
	public LongData S5541protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5541ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(lfact01Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact02Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact03Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact04Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact05Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact06Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact07Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact08Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact09Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact10Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact11Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lfact12Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, lfact01, lfact02, lfact03, lfact04, lfact05, lfact06, lfact07, lfact08, lfact09, lfact10, lfact11, lfact12, freqcy01, freqcy02, freqcy03, freqcy04, freqcy05, freqcy06, freqcy07, freqcy08, freqcy09, freqcy10, freqcy11, freqcy12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, lfact01Out, lfact02Out, lfact03Out, lfact04Out, lfact05Out, lfact06Out, lfact07Out, lfact08Out, lfact09Out, lfact10Out, lfact11Out, lfact12Out, freqcy01Out, freqcy02Out, freqcy03Out, freqcy04Out, freqcy05Out, freqcy06Out, freqcy07Out, freqcy08Out, freqcy09Out, freqcy10Out, freqcy11Out, freqcy12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, lfact01Err, lfact02Err, lfact03Err, lfact04Err, lfact05Err, lfact06Err, lfact07Err, lfact08Err, lfact09Err, lfact10Err, lfact11Err, lfact12Err, freqcy01Err, freqcy02Err, freqcy03Err, freqcy04Err, freqcy05Err, freqcy06Err, freqcy07Err, freqcy08Err, freqcy09Err, freqcy10Err, freqcy11Err, freqcy12Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5541screen.class;
		protectRecord = S5541protect.class;
	}

}
