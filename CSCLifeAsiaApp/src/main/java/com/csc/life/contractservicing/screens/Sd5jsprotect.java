package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5jsprotect extends ScreenRecord {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {9, 4, 22, 17, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}
	
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5jsScreenVars sv = (Sd5jsScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5jsprotectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5jsScreenVars screenVars = (Sd5jsScreenVars)pv;
	}
		
	/**
	 * Clear all the variables in SD5JSprotect
	 */
		
	public static void clear(VarModel pv) {
		Sd5jsScreenVars screenVars = (Sd5jsScreenVars) pv;
	}
}
