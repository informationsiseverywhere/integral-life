package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: TashpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:33
 * Class transformed from TASHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class TashpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 85;
	public FixedLengthStringData tashrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData tashpfRecord = tashrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(tashrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(tashrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(tashrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(tashrec);
	public FixedLengthStringData asgnpfx = DD.asgnpfx.copy().isAPartOf(tashrec);
	public FixedLengthStringData asgnnum = DD.asgnnum.copy().isAPartOf(tashrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(tashrec);
	public PackedDecimalData commfrom = DD.commfrom.copy().isAPartOf(tashrec);
	public PackedDecimalData commto = DD.commto.copy().isAPartOf(tashrec);
	public FixedLengthStringData trancde = DD.trancde.copy().isAPartOf(tashrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(tashrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(tashrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(tashrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public TashpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for TashpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public TashpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for TashpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public TashpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for TashpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public TashpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("TASHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"VALIDFLAG, " +
							"ASGNPFX, " +
							"ASGNNUM, " +
							"SEQNO, " +
							"COMMFROM, " +
							"COMMTO, " +
							"TRANCDE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     validflag,
                                     asgnpfx,
                                     asgnnum,
                                     seqno,
                                     commfrom,
                                     commto,
                                     trancde,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		validflag.clear();
  		asgnpfx.clear();
  		asgnnum.clear();
  		seqno.clear();
  		commfrom.clear();
  		commto.clear();
  		trancde.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getTashrec() {
  		return tashrec;
	}

	public FixedLengthStringData getTashpfRecord() {
  		return tashpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setTashrec(what);
	}

	public void setTashrec(Object what) {
  		this.tashrec.set(what);
	}

	public void setTashpfRecord(Object what) {
  		this.tashpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(tashrec.getLength());
		result.set(tashrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}