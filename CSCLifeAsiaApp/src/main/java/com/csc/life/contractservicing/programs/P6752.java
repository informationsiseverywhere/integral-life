/*
 * File: P6752.java
 * Date: 30 August 2009 0:55:42
 * Author: Quipoz Limited
 * 
 * Class transformed from P6752.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.CwfdregTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrncwfTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnwfdtTableDAM;
import com.csc.life.contractservicing.screens.S6752ScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*               Windforward Registration Selection
*               ==================================
*
*     This program is accessed from the Windforward Submenu when
* Windforward Registration is selected.  It displays a list of
* reversed transactions which have not subsequently been
* woundforward.
*     Reversed transactions which are available for Windforward
* may be selected by the user.  Once selected, an asterisk is
* displayed alongside the transaction to indicate that it has
* been selected for windforward.  Selected transactions may also
* be cancelled from this screen.
*     It is also possible to switch to the Insert Transaction
* and Windforward Transaction List screens from this screen
* through function key switching.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6752 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6752");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaLineCount = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-VARIABLES */
	private ZonedDecimalData l = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private PackedDecimalData wsaaRrn = new PackedDecimalData(9, 0);
	private String wsaaPtrnInd = "";

	private FixedLengthStringData wsaaTranskey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaCntype = new FixedLengthStringData(3).isAPartOf(wsaaTranskey, 0);
	private FixedLengthStringData wsaaTrcode = new FixedLengthStringData(4).isAPartOf(wsaaTranskey, 3);

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1).init("N");
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");

	private FixedLengthStringData wsaaOptionSelected = new FixedLengthStringData(1).init("N");
	private Validator optionSelected = new Validator(wsaaOptionSelected, "Y");

	private FixedLengthStringData wsaaFunctionSelected = new FixedLengthStringData(1).init("N");
	private Validator functionSelected = new Validator(wsaaFunctionSelected, "Y");
	private ZonedDecimalData wsaaOptionCode = new ZonedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private String h234 = "H234";
	private String o001 = "O001";
	private String e301 = "E301";
	private String d005 = "D005";
		/* TABLES */
	private String t6757 = "T6757";
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String ptrncwfrec = "PTRNCWFREC";
	private String ptrnwfdtrec = "PTRNWFDTREC";
	private String descrec = "DESCREC";
	private String cltsrec = "CLTSREC";
	private String chdrenqrec = "CHDRENQREC";
	private String cwfdregrec = "CWFDREGREC";
	private String lifeenqrec = "LIFEENQREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Windforward file by Tranno*/
	private CwfdregTableDAM cwfdregIO = new CwfdregTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*PTRN - File for Windforward*/
	private PtrncwfTableDAM ptrncwfIO = new PtrncwfTableDAM();
		/*PTRN Logical File for windforward*/
	private PtrnwfdtTableDAM ptrnwfdtIO = new PtrnwfdtTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6752ScreenVars sv = ScreenProgram.getScreenVars( S6752ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1190, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		readNext1220, 
		preExit, 
		exit2090, 
		updateSubfile2270, 
		lineSwitching4030, 
		switch4070, 
		updateSubfile4170, 
		exit4190
	}

	public P6752() {
		super();
		screenVars = sv;
		new ScreenModel("S6752", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6752", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		findDesc5000();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		findDesc5000();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.set(SPACES);
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		findDesc5000();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.set(SPACES);
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getRegister());
		getLifeDetails1100();
		ptrnwfdtIO.setDataArea(SPACES);
		ptrnwfdtIO.setChdrcoy(chdrenqIO.getChdrcoy());
		ptrnwfdtIO.setChdrnum(chdrenqIO.getChdrnum());		
		ptrnwfdtIO.setTranno(99999);
		ptrnwfdtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnwfdtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnwfdtIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		ptrnwfdtIO.setFormat(ptrnwfdtrec);
		SmartFileCode.execute(appVars, ptrnwfdtIO);
		if (isNE(ptrnwfdtIO.getStatuz(),varcom.oK)
		&& isNE(ptrnwfdtIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ptrnwfdtIO.getStatuz());
			syserrrec.params.set(ptrnwfdtIO.getParams());
			fatalError600();
		}
		if (isNE(ptrnwfdtIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(ptrnwfdtIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			ptrnwfdtIO.setStatuz(varcom.endp);
		}
		wsaaLineCount.set(1);
		while ( !(isEQ(ptrnwfdtIO.getStatuz(),varcom.endp)
		|| isGT(wsaaLineCount,sv.subfilePage))) {
			processPtrn1200();
		}
		
		if (isNE(ptrnwfdtIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set("N");
		}
		wsaaRrn.set(scrnparams.subfileRrn);
	}

protected void getLifeDetails1100()
	{
		try {
			begn1110();
		}
		catch (GOTOException e){
		}
	}

protected void begn1110()
	{
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		lifeenqIO.setFormat(lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isNE(lifeenqIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(lifeenqIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			lifeenqIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeenqIO.getStatuz(),varcom.mrnf)) {
			sv.jlinsname.set(SPACES);
			sv.jownnum.set(SPACES);
			goTo(GotoLabel.exit1190);
		}
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		sv.jownnum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void processPtrn1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					go1210();
				}
				case readNext1220: {
					readNext1220();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go1210()
	{
		ptrncwfIO.setParams(SPACES);
		ptrncwfIO.setChdrcoy(ptrnwfdtIO.getChdrcoy());
		ptrncwfIO.setChdrnum(ptrnwfdtIO.getChdrnum());
		ptrncwfIO.setBatctrcde(ptrnwfdtIO.getBatctrcde());
		ptrncwfIO.setPtrneff(ptrnwfdtIO.getPtrneff());
		setPrecision(ptrncwfIO.getTranno(), 0);
		ptrncwfIO.setTranno(add(ptrnwfdtIO.getTranno(),1));
		ptrncwfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrncwfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrncwfIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "BATCTRCDE", "PTRNEFF");
		ptrncwfIO.setFormat(ptrncwfrec);
		wsaaPtrnInd = "N";
		findReinstatedPtrn1300();
		if (isEQ(wsaaPtrnInd,"Y")) {
			goTo(GotoLabel.readNext1220);
		}
		cwfdregIO.setDataArea(SPACES);
		cwfdregIO.setChdrcoy(chdrenqIO.getChdrcoy());
		cwfdregIO.setChdrnum(chdrenqIO.getChdrnum());
		cwfdregIO.setTranno(ptrnwfdtIO.getTranno());
		cwfdregIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cwfdregIO);
		if (isNE(cwfdregIO.getStatuz(),varcom.oK)
		&& isNE(cwfdregIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(cwfdregIO.getStatuz());
			syserrrec.params.set(cwfdregIO.getParams());
			fatalError600();
		}
		if (isEQ(cwfdregIO.getStatuz(),varcom.mrnf)) {
			sv.ind.set(SPACES);
		}
		else {
			sv.ind.set("*");
		}
		wsaaCntype.set(chdrenqIO.getCnttype());
		wsaaTrcode.set(ptrnwfdtIO.getBatctrcde());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6757);
		itemIO.setItemitem(wsaaTranskey);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(ptrnwfdtIO.getDatesub(),99999999)
		|| isEQ(ptrnwfdtIO.getDatesub(),ZERO)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(ptrnwfdtIO.getBatctrcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.descrip.set(SPACES);
		}
		else {
			sv.descrip.set(descIO.getLongdesc());
		}
		sv.trcode.set(ptrnwfdtIO.getBatctrcde());
		sv.datesub.set(ptrnwfdtIO.getDatesub());
		sv.tranno.set(ptrnwfdtIO.getTranno());
		sv.effdate.set(ptrnwfdtIO.getPtrneff());
		scrnparams.function.set(varcom.sadd);
		processScreen("S6752", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		compute(wsaaLineCount, 0).set(add(wsaaLineCount,1));
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
	}

protected void readNext1220()
	{
		wsaaPtrnInd = "N";
		ptrnwfdtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnwfdtIO);
		if (isNE(ptrnwfdtIO.getStatuz(),varcom.oK)
		&& isNE(ptrnwfdtIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ptrnwfdtIO.getStatuz());
			syserrrec.params.set(ptrnwfdtIO.getParams());
			fatalError600();
		}
		if (isNE(ptrnwfdtIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(ptrnwfdtIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			ptrnwfdtIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void findReinstatedPtrn1300()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, ptrncwfIO);
		if (isNE(ptrncwfIO.getStatuz(),varcom.oK)
		&& isNE(ptrncwfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrncwfIO.getParams());
			syserrrec.statuz.set(ptrncwfIO.getStatuz());
			fatalError600();
		}
		if (isNE(ptrncwfIO.getChdrcoy(),ptrnwfdtIO.getChdrcoy())
		|| isNE(ptrncwfIO.getChdrnum(),ptrnwfdtIO.getChdrnum())
		|| isNE(ptrncwfIO.getBatctrcde(),ptrnwfdtIO.getBatctrcde())
		|| isNE(ptrncwfIO.getPtrneff(),ptrnwfdtIO.getPtrneff())) {
			ptrncwfIO.setStatuz(varcom.endp);
		}
		if (isEQ(ptrncwfIO.getStatuz(),varcom.oK)) {
			wsaaPtrnInd = "Y";
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
			completeScreen2010();
			screenIo2020();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
	}

protected void completeScreen2010()
	{
		l.set(1);
		sv.hflag01Out[varcom.nd.toInt()].set("Y");
		sv.hflag02Out[varcom.nd.toInt()].set("Y");
		for (ix.set(1); !(isGT(ix,20)); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")) {
				sv.optdsc[l.toInt()].set(optswchrec.optsDsc[ix.toInt()]);
				l.add(1);
			}
			if (isEQ(optswchrec.optsType[ix.toInt()],"F")
			&& isEQ(optswchrec.optsNo[ix.toInt()],7)) {
				sv.hflag01Out[varcom.nd.toInt()].set(SPACES);
			}
			if (isEQ(optswchrec.optsType[ix.toInt()],"F")
			&& isEQ(optswchrec.optsNo[ix.toInt()],8)) {
				sv.hflag02Out[varcom.nd.toInt()].set(SPACES);
			}
		}
	}

protected void screenIo2020()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2020();
			checkForErrors2050();
			validateSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaScrnStatuz.set(scrnparams.statuz);
	}

protected void validateScreen2020()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaLineCount.set(1);
			while ( !(isEQ(ptrnwfdtIO.getStatuz(),varcom.endp)
			|| isGT(wsaaLineCount,sv.subfilePage))) {
				processPtrn1200();
			}
			
			if (isNE(ptrnwfdtIO.getStatuz(),varcom.endp)) {
				scrnparams.subfileMore.set("Y");
			}
			else {
				scrnparams.subfileMore.set("N");
			}
			wsspcomn.edterror.set("Y");
			wsaaRrn.set(scrnparams.subfileRrn);
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForErrors2050()
	{
		validateHeader2100();
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6752", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2200();
		}
		
		if (isEQ(wsaaScrnStatuz,varcom.delt)
		|| isEQ(wsaaScrnStatuz,varcom.insr)) {
			checkFunction2300();
		}
	}

protected void validateHeader2100()
	{
		/*VALIDATE-HEADER*/
		if (isEQ(sv.optdsc[1],SPACES)) {
			sv.optdsc01Err.set(h234);
		}
		/*EXIT*/
	}

protected void validateSubfile2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSubfile2210();
				}
				case updateSubfile2270: {
					updateSubfile2270();
					readNextChange2280();
				}
				} 
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSubfile2210()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateSubfile2270);
		}
		if (isNE(sv.select,NUMERIC)) {
			sv.selectErr.set(o001);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateSubfile2270);
		}
		if (isEQ(sv.select,"1")) {
			checkCwfdRecord2400();
			if (isNE(cwfdregIO.getStatuz(),varcom.mrnf)) {
				sv.selectErr.set(e301);
				wsspcomn.edterror.set("Y");
				//ILIFE - 1253 STARTS
				sv.select.set(SPACES);
				//ILIFE - 1253 ENDS
				goTo(GotoLabel.updateSubfile2270);
			}
		}
		if (isEQ(sv.select,"2")) {
			checkCwfdRecord2400();
			if (isEQ(cwfdregIO.getStatuz(),varcom.mrnf)) {
				sv.selectErr.set(d005);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.updateSubfile2270);
			}
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(sv.select);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateSubfile2270()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6752", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextChange2280()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6752", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkFunction2300()
	{
		check2310();
	}

protected void check2310()
	{
		optswchrec.optsSelCode.set(wsaaScrnStatuz);
		optswchrec.optsSelType.set("F");
		optswchrec.optsSelOptno.set(ZERO);
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			if (isEQ(wsaaScrnStatuz,varcom.insr)) {
				sv.hflag01Err.set(optswchrec.optsStatuz);
			}
			else {
				sv.hflag02Err.set(optswchrec.optsStatuz);
			}
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkCwfdRecord2400()
	{
		check2400();
	}

protected void check2400()
	{
		cwfdregIO.setDataArea(SPACES);
		cwfdregIO.setChdrnum(chdrenqIO.getChdrnum());
		cwfdregIO.setChdrcoy(chdrenqIO.getChdrcoy());
		cwfdregIO.setTranno(sv.tranno);
		cwfdregIO.setFunction(varcom.readr);
		cwfdregIO.setFormat(cwfdregrec);
		SmartFileCode.execute(appVars, cwfdregIO);
		if (isNE(cwfdregIO.getStatuz(),varcom.oK)
		&& isNE(cwfdregIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(cwfdregIO.getStatuz());
			syserrrec.params.set(cwfdregIO.getParams());
			fatalError600();
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case lineSwitching4030: {
					lineSwitching4030();
				}
				case switch4070: {
					switch4070();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsaaFunctionSelected.set("N");
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			rlseSoftlock4300();
			goTo(GotoLabel.switch4070);
		}
		if (isEQ(wsaaScrnStatuz,varcom.oK)) {
			goTo(GotoLabel.lineSwitching4030);
		}
		optswchrec.optsSelType.set("F");
		optswchrec.optsSelCode.set(wsaaScrnStatuz);
		wsaaScrnStatuz.set(varcom.oK);
		wsaaFunctionSelected.set("Y");
		chdrenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		goTo(GotoLabel.switch4070);
	}

protected void lineSwitching4030()
	{
		wsaaFoundSelection.set("N");
		scrnparams.subfileRrn.set(0);
		while ( !(isEQ(scrnparams.statuz,varcom.mrnf)
		|| foundSelection.isTrue())) {
			lineSels4100();
		}
		
	}

protected void switch4070()
	{
		if (!functionSelected.isTrue()
		&& !foundSelection.isTrue()
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			rlseSoftlock4300();
		}
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			loadSubfile4200();
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		}
	}

protected void lineSels4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					lineSelect4110();
					keepsForLineSelectPgm4150();
				}
				case updateSubfile4170: {
					updateSubfile4170();
				}
				case exit4190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lineSelect4110()
	{
		scrnparams.subfileRrn.add(1);
		scrnparams.function.set(varcom.sread);
		processScreen("S6752", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.mrnf)) {
			goTo(GotoLabel.exit4190);
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.exit4190);
		}
		wsaaOptionCode.set(sv.select);
		for (ix.set(1); !(isGT(ix,20)); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")
			&& isEQ(optswchrec.optsNo[ix.toInt()],wsaaOptionCode)) {
				optswchrec.optsInd[ix.toInt()].set("X");
				wsaaFoundSelection.set("Y");
				wsaaCntype.set(sv.cnttype);
				wsaaTrcode.set(sv.trcode);
				wssplife.keysoptItem.set(wsaaTranskey);
			}
		}
		if (!foundSelection.isTrue()) {
			goTo(GotoLabel.updateSubfile4170);
		}
		wsaaRrn.set(scrnparams.subfileRrn);
	}

protected void keepsForLineSelectPgm4150()
	{
		chdrenqIO.setFunction(varcom.keeps);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		ptrnwfdtIO.setDataArea(SPACES);
		ptrnwfdtIO.setChdrcoy(chdrenqIO.getChdrcoy());
		ptrnwfdtIO.setChdrnum(chdrenqIO.getChdrnum());
		ptrnwfdtIO.setTranno(sv.tranno);
		ptrnwfdtIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, ptrnwfdtIO);
		if (isNE(ptrnwfdtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnwfdtIO.getParams());
			syserrrec.statuz.set(ptrnwfdtIO.getStatuz());
			fatalError600();
		}
	}

protected void updateSubfile4170()
	{
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("S6752", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void loadSubfile4200()
	{
		load4210();
	}

protected void load4210()
	{
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		findDesc5000();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.set(SPACES);
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("S6752", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		ptrnwfdtIO.setDataArea(SPACES);
		ptrnwfdtIO.setChdrcoy(chdrenqIO.getChdrcoy());
		ptrnwfdtIO.setChdrnum(chdrenqIO.getChdrnum());
		ptrnwfdtIO.setTranno(ZERO);
		ptrnwfdtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnwfdtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnwfdtIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, ptrnwfdtIO);
		if (isNE(ptrnwfdtIO.getStatuz(),varcom.oK)
		&& isNE(ptrnwfdtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnwfdtIO.getParams());
			syserrrec.statuz.set(ptrnwfdtIO.getStatuz());
			fatalError600();
		}
		if (isNE(ptrnwfdtIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(ptrnwfdtIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			ptrnwfdtIO.setStatuz(varcom.endp);
		}
		wsaaLineCount.set(1);
		while ( !(isEQ(ptrnwfdtIO.getStatuz(),varcom.endp)
		|| isGT(wsaaLineCount,sv.subfilePage))) {
			processPtrn1200();
		}
		
		if (isNE(ptrnwfdtIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set("N");
		}
		wsaaRrn.set(scrnparams.subfileRrn);
	}

protected void rlseSoftlock4300()
	{
		rlse4310();
	}

protected void rlse4310()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrenqIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void findDesc5000()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
}
