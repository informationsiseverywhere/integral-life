package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:08
 * Description:
 * Copybook name: LIFEMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifemjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifemjaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifemjaKey = new FixedLengthStringData(256).isAPartOf(lifemjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifemjaChdrcoy = new FixedLengthStringData(1).isAPartOf(lifemjaKey, 0);
  	public FixedLengthStringData lifemjaChdrnum = new FixedLengthStringData(8).isAPartOf(lifemjaKey, 1);
  	public FixedLengthStringData lifemjaLife = new FixedLengthStringData(2).isAPartOf(lifemjaKey, 9);
  	public FixedLengthStringData lifemjaJlife = new FixedLengthStringData(2).isAPartOf(lifemjaKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifemjaKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifemjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifemjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}