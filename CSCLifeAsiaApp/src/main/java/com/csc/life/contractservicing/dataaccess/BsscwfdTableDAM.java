package com.csc.life.contractservicing.dataaccess;

import com.csc.smart.dataaccess.BsscpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;


/**
 * 	
 * File: BsscwfdTableDAM.java
 * Date: Sun, 30 Aug 2009 03:30:34
 * Class transformed from BSSCWFD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BsscwfdTableDAM extends BsscpfTableDAM {

	public BsscwfdTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("BSSCWFD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "BSCHEDNAM"
		             + ", BSCHEDNUM";
		
		QUALIFIEDCOLUMNS = 
		            "BSCHEDNAM, " +
		            "BSCHEDNUM, " +
		            "BSHDSTATUS, " +
		            "BCURNOTHDS, " +
		            "BREQNOTHDS, " +
		            "BSUSERNAME, " +
		            "BSDATMINTD, " +
		            "BSDATMSTRT, " +
		            "BSDATMENDD, " +
		            "BPRCEFFDAT, " +
		            "BPRCACCYR, " +
		            "BPRCACCMTH, " +
		            "LANGUAGE, " +
		            "BSHDINITBR, " +
		            "BSPRSABORT, " +
		            "BSPRSFAILD, " +
		            "BSPSCMPLTD, " +
		            "BSPRSCANC, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "BSCHEDNAM ASC, " +
		            "BSCHEDNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "BSCHEDNAM DESC, " +
		            "BSCHEDNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               scheduleName,
                               scheduleNumber,
                               scheduleStatus,
                               currNofThreads,
                               reqdNofThreads,
                               userName,
                               datimeInit,
                               datimeStarted,
                               datimeEnded,
                               effectiveDate,
                               acctYear,
                               acctMonth,
                               language,
                               initBranch,
                               processesAborted,
                               processesFailed,
                               processesCompleted,
                               processesCancelled,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(49);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getScheduleName().toInternal()
					+ getScheduleNumber().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, scheduleName);
			what = ExternalData.chop(what, scheduleNumber);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(10);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(scheduleName.toInternal());
	nonKeyFiller20.setInternal(scheduleNumber.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(179);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getScheduleStatus().toInternal()
					+ getCurrNofThreads().toInternal()
					+ getReqdNofThreads().toInternal()
					+ getUserName().toInternal()
					+ getDatimeInit().toInternal()
					+ getDatimeStarted().toInternal()
					+ getDatimeEnded().toInternal()
					+ getEffectiveDate().toInternal()
					+ getAcctYear().toInternal()
					+ getAcctMonth().toInternal()
					+ getLanguage().toInternal()
					+ getInitBranch().toInternal()
					+ getProcessesAborted().toInternal()
					+ getProcessesFailed().toInternal()
					+ getProcessesCompleted().toInternal()
					+ getProcessesCancelled().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, scheduleStatus);
			what = ExternalData.chop(what, currNofThreads);
			what = ExternalData.chop(what, reqdNofThreads);
			what = ExternalData.chop(what, userName);
			what = ExternalData.chop(what, datimeInit);
			what = ExternalData.chop(what, datimeStarted);
			what = ExternalData.chop(what, datimeEnded);
			ZonedDecimalData effectiveDateTEMP = new ZonedDecimalData(8);
			what = ExternalData.chop(what, effectiveDateTEMP);
			setEffectiveDate(effectiveDateTEMP);
			what = ExternalData.chop(what, acctYear);
			what = ExternalData.chop(what, acctMonth);
			what = ExternalData.chop(what, language);
			what = ExternalData.chop(what, initBranch);
			what = ExternalData.chop(what, processesAborted);
			what = ExternalData.chop(what, processesFailed);
			what = ExternalData.chop(what, processesCompleted);
			what = ExternalData.chop(what, processesCancelled);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getScheduleName() {
		return scheduleName;
	}
	public void setScheduleName(Object what) {
		scheduleName.set(what);
	}
	public PackedDecimalData getScheduleNumber() {
		return scheduleNumber;
	}
	public void setScheduleNumber(Object what) {
		setScheduleNumber(what, false);
	}
	public void setScheduleNumber(Object what, boolean rounded) {
		if (rounded)
			scheduleNumber.setRounded(what);
		else
			scheduleNumber.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getScheduleStatus() {
		return scheduleStatus;
	}
	public void setScheduleStatus(Object what) {
		scheduleStatus.set(what);
	}	
	public PackedDecimalData getCurrNofThreads() {
		return currNofThreads;
	}
	public void setCurrNofThreads(Object what) {
		setCurrNofThreads(what, false);
	}
	public void setCurrNofThreads(Object what, boolean rounded) {
		if (rounded)
			currNofThreads.setRounded(what);
		else
			currNofThreads.set(what);
	}	
	public PackedDecimalData getReqdNofThreads() {
		return reqdNofThreads;
	}
	public void setReqdNofThreads(Object what) {
		setReqdNofThreads(what, false);
	}
	public void setReqdNofThreads(Object what, boolean rounded) {
		if (rounded)
			reqdNofThreads.setRounded(what);
		else
			reqdNofThreads.set(what);
	}	
	public FixedLengthStringData getUserName() {
		return userName;
	}
	public void setUserName(Object what) {
		userName.set(what);
	}	
	public FixedLengthStringData getDatimeInit() {
		return datimeInit;
	}
	public void setDatimeInit(Object what) {
		datimeInit.set(what);
	}	
	public FixedLengthStringData getDatimeStarted() {
		return datimeStarted;
	}
	public void setDatimeStarted(Object what) {
		datimeStarted.set(what);
	}	
	public FixedLengthStringData getDatimeEnded() {
		return datimeEnded;
	}
	public void setDatimeEnded(Object what) {
		datimeEnded.set(what);
	}	
	public ZonedDecimalData getEffectiveDate() {
		return dateStringToZoned(effectiveDate);
	}
	public void setEffectiveDate(Object what) {
		effectiveDate.set(dateZonedToString(what));
	}	
	public PackedDecimalData getAcctYear() {
		return acctYear;
	}
	public void setAcctYear(Object what) {
		setAcctYear(what, false);
	}
	public void setAcctYear(Object what, boolean rounded) {
		if (rounded)
			acctYear.setRounded(what);
		else
			acctYear.set(what);
	}	
	public PackedDecimalData getAcctMonth() {
		return acctMonth;
	}
	public void setAcctMonth(Object what) {
		setAcctMonth(what, false);
	}
	public void setAcctMonth(Object what, boolean rounded) {
		if (rounded)
			acctMonth.setRounded(what);
		else
			acctMonth.set(what);
	}	
	public FixedLengthStringData getLanguage() {
		return language;
	}
	public void setLanguage(Object what) {
		language.set(what);
	}	
	public FixedLengthStringData getInitBranch() {
		return initBranch;
	}
	public void setInitBranch(Object what) {
		initBranch.set(what);
	}	
	public PackedDecimalData getProcessesAborted() {
		return processesAborted;
	}
	public void setProcessesAborted(Object what) {
		setProcessesAborted(what, false);
	}
	public void setProcessesAborted(Object what, boolean rounded) {
		if (rounded)
			processesAborted.setRounded(what);
		else
			processesAborted.set(what);
	}	
	public PackedDecimalData getProcessesFailed() {
		return processesFailed;
	}
	public void setProcessesFailed(Object what) {
		setProcessesFailed(what, false);
	}
	public void setProcessesFailed(Object what, boolean rounded) {
		if (rounded)
			processesFailed.setRounded(what);
		else
			processesFailed.set(what);
	}	
	public PackedDecimalData getProcessesCompleted() {
		return processesCompleted;
	}
	public void setProcessesCompleted(Object what) {
		setProcessesCompleted(what, false);
	}
	public void setProcessesCompleted(Object what, boolean rounded) {
		if (rounded)
			processesCompleted.setRounded(what);
		else
			processesCompleted.set(what);
	}	
	public PackedDecimalData getProcessesCancelled() {
		return processesCancelled;
	}
	public void setProcessesCancelled(Object what) {
		setProcessesCancelled(what, false);
	}
	public void setProcessesCancelled(Object what, boolean rounded) {
		if (rounded)
			processesCancelled.setRounded(what);
		else
			processesCancelled.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		scheduleName.clear();
		scheduleNumber.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		scheduleStatus.clear();
		currNofThreads.clear();
		reqdNofThreads.clear();
		userName.clear();
		datimeInit.clear();
		datimeStarted.clear();
		datimeEnded.clear();
		effectiveDate.clear();
		acctYear.clear();
		acctMonth.clear();
		language.clear();
		initBranch.clear();
		processesAborted.clear();
		processesFailed.clear();
		processesCompleted.clear();
		processesCancelled.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}