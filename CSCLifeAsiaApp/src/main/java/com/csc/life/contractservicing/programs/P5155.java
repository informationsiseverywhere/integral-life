/*
 * File: P5155.java
 * Date: 30 August 2009 0:16:49
 * Author: Quipoz Limited
 * 
 * Class transformed from P5155.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.screens.S5155ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.dao.CrsvpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5155
* ~~~~~
*
* Initialise
* ~~~~~~~~~~
* Get todays date using DATCON1.
*
* Clear the subfile with a function SCLR.
*
* Perform a RETRV on CHDRLIF logical view to retrieve the Contract
* Header information.
* Most fields on the screen can be set up using this data.
*
* In addition the following fields require setting up:-
*
*    Contract type long description from T5688.
*    Contract status short description from T3623.
*    Premium status short description from T3588.
*
* Perform a BEGN on PTRNREV logical view, setting
* TRANNO to 99999.
*
* Perform Load-the-Subfile Until
*         (Key Break            ) or
*         (PTRNREV-STATUZ = ENDP) or
*         (WSAA-COUNT     = Subfile Size).
*
* If (PTRNREV-STATUZ not = ENDP) and
*    (There has been no Key Break)
*     Move 'Y' to SCRN-SUBFILE-MORE
* Else
*     Move 'N' to SCRN-SUBFILE-MORE.
*
* Load the Subfile
* ~~~~~~~~~~~~~~~~
* Most of the subfile fields can be set up from PTRNREV logical
* view.
*
* In addition the following fields require setting up:-
*    Transaction Code description from T1688.
*
* Read T6661 using the transaction code from the PTRN as the
* key.
* If MRNF
*    Set a flag to indicate that this and all subsequent
*    records are to be protected against selection.
*
* Add the record to the subfile using function SADD.
*
* Get the next PTRN record.
*
*
* Validation
* ~~~~~~~~~~
* If SCRN-STATUZ = ROLU
*    Perform Load-the-Subfile Until
*            (Key Break            ) or
*            (PTRNREV-STATUZ = ENDP) or
*            (WSAA-COUNT     = Subfile Size)
*
*    If (PTRNREV-STATUZ not = ENDP) and
*       (There has been no Key Break)
*        Move 'Y' to SCRN-SUBFILE-MORE
*    Else
*        Move 'N' to SCRN-SUBFILE-MORE.
*    End-If
*    Move 'Y' to WSSP-EDTERROR
*    Go To Exit
* End-If.
*
* The Paid To Date must equal the Billed to Date in order to
* go ahead with the reversal transaction.
*
* The Bill Suppression Flag must be 'Y' or 'N'.
*
* If the Bill Suppression Flag is 'N', there is no need to enter
* the Bill Suppress Date.
*
* If the Bill Suppression Flag is 'Y', the Bill Suppression Date
* must be entered.
*
* The Bill Suppression Date must be greater than the Billed To Date.
*
* Perform a SSTRT on the subfile.
*
* If a record has been selected, set a flag to indiacte that this
* is so.
*
* Perform Validate-Subfile until SCRN-STATUZ = ENDP.
*
* If no record has been selected
*    Display a message 'PTRN must be selected'.
*
* If screen error indicators not = spaces
*    Move 'Y' to WSSP-EDTERROR.
*
*
* Validate Subfile
* ~~~~~~~~~~~~~~~~
* Perform SRDN on the subfile to get the next record.
*
* If SCRN-STATUZ = ENDP or the select field is blank
*    Go To Exit.
*
* If a record has not already been selected
*    Set the flag to indicate that one has now been
*    selected
*    Perform Validate-Selected-Record
*    Go To Exit.
*
* We must ensure that only one PTRN is selected.
* The program will only get this far if another record has
* been selected - therefore, display an error message.
*
* Update the subfile error indicators using SUPD.
*
* Validate Selected Record
* ~~~~~~~~~~~~~~~~~~~~~~~~
* The Effective Date of the PTRN selected must NOT be greater
* than the Billed To Date.
*
* The Effective Date of the PTRN selected must not be less than
* the Risk Commencement Date.
*
*
* Update
* ~~~~~~
* All updating will be  carried  out  in  AT  because  of the
* amount of processing which  may be involved. A request must be
* submitted to the AT queue by calling the subroutine ATREQ with
* the following parameters:
*
*     AT module - T656AT
*     Batch key - from WSSP
*     Requesting program - WSAA-PROG
*     Transaction ID - from VARCOM
*     User - from VARCOM
*     Terminal - from VARCOM
*     Request date - todays date (TDAY from DATCON1)
*     Time - from VARCOM
*     Language - from WSSP
*     Primary key - contract number
*     Transaction area - reverse to date, bill suppress flag
*     and bill suppress to date.
*
*****************************************************************
* </pre>
*/
public class P5155 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5155");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	
	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(75);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 70);
	private FixedLengthStringData wsaaCover = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 71);//ILIFE-8123
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 73);//ILIFE-8123 

	private FixedLengthStringData wsaaSelectRecord = new FixedLengthStringData(1);
	private Validator wsaaRecordSelected = new Validator(wsaaSelectRecord, "Y");
	private Validator wsaaRecordNotSelected = new Validator(wsaaSelectRecord, "N");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaSubfileSize = 12;

	private FixedLengthStringData wsaaSelectionFlag = new FixedLengthStringData(1);
	private Validator wsaaSelectAllowed = new Validator(wsaaSelectionFlag, "Y");
	private Validator wsaaPreventSelection = new Validator(wsaaSelectionFlag, "N");
	private String wsaaAsterisks = "";

	private FixedLengthStringData wsaaSubfileFields = new FixedLengthStringData(48);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaSubfileFields, 0);
	private FixedLengthStringData wsaaSelect = new FixedLengthStringData(1).isAPartOf(wsaaSubfileFields, 8);
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30).isAPartOf(wsaaSubfileFields, 9);
	private ZonedDecimalData wsaaScreenTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaSubfileFields, 39);
	private FixedLengthStringData wsaaTrcode = new FixedLengthStringData(4).isAPartOf(wsaaSubfileFields, 44);
	private ZonedDecimalData wsaaRrn = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaPtrnRrn = new ZonedDecimalData(5, 0).setUnsigned();
		/* ERRORS */
	private String e315 = "E315";
	private String f515 = "F515";
	private String g570 = "G570";
	private String h035 = "H035";
	private String i031 = "I031";
	private String i032 = "I032";
	private String i033 = "I033";
		/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String t6661 = "T6661";
	private String itemrec = "ITEMREC";
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private static final String chdrlifrec = "CHDRLIFREC";
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);

	//private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	//private ItemTableDAM itemIO = new ItemTableDAM();
		/*Policy transaction history for reversals*/
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T6661rec t6661rec = new T6661rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5155ScreenVars sv = ScreenProgram.getScreenVars( S5155ScreenVars.class);
	private static final String ROLLOVER_FEATURE_ID="NBPRP104";
	private boolean rolloverFlag = false;
	private Covrpf covrpf=null;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Ptrnpf ptrnpf = null;
	private Iterator<Ptrnpf> ptrnItr = null;
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private CrsvpfDAO crsvpfDAO = getApplicationContext().getBean("crsvpfDAO", CrsvpfDAO.class);
	boolean CMDTH010Permission = false;
	//ILJ-49 End 
	boolean contDtCalcFlag;
	private boolean supBill = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1190, 
		preExit, 
		checkForErrors2080, 
		exit2090, 
		exit2690, 
		exit2890, 
		exit3090
	}

	public P5155() {
		super();
		screenVars = sv;
		new ScreenModel("S5155", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", "CMDTH010", appVars, "IT");//ILJ-383
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
				//ILJ-49 End
		supBill = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSOTH018", appVars, "IT");
		if(supBill) {
			sv.supflagOut[varcom.pr.toInt()].set("Y");
			sv.supflag.set("N");
		}
		wsaaTodate.set(ZERO);
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.cntinst.set(ZERO);
		sv.osbal.set(ZERO);
		sv.suppressTo.set(varcom.vrcmMaxDate);
		if (isEQ(wsspcomn.flag,"R")) {
			sv.supflag.set("N");
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5155", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		descpf=descDAO.getdescData("IT", t1688 ,wsaaBatckey.batcBatctrcde.toString(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.longdesc.fill("?");
		}
		else {
			sv.longdesc.set(descpf.getLongdesc());
		}
		/*descIO.setDataKey(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.fill("?");
		}
		else {
			sv.longdesc.set(descIO.getLongdesc());
		}*/
		
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrlifIO.setFunction(varcom.retrv);
			chdrlifIO.setFormat(chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		
		/*chdrlifIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}*/
		
		descpf=descDAO.getdescData("IT", t5688 ,chdrpf.getCnttype(),wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1485
		if (descpf == null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
	/*	descIO.setDataKey(SPACES);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrlifIO.getCnttype());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}*/
		
		descpf=descDAO.getdescData("IT", t3623,chdrpf.getStatcode(),wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1485
		if (descpf == null) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descpf.getShortdesc());
		}
		
		/*descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlifIO.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}*/
		
		descpf=descDAO.getdescData("IT",t3588 ,chdrpf.getPstcde(),wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1485
		if (descpf == null) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descpf.getShortdesc());
		}
		
	/*	descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrlifIO.getPstatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.occdate.set(chdrpf.getOccdate());
		sv.cntinst.set(chdrpf.getSinstamt06());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.osbal.set(chdrpf.getOutstamt());
		sv.btdate.set(chdrpf.getBtdate());
		sv.billfreq.set(chdrpf.getBillfreq());
		sv.mop.set(chdrpf.getBillchnl());
		List<Ptrnpf> list =  ptrnpfDAO.getPtrnrevData(wsspcomn.company.toString(),chdrpf.getChdrnum(),"99999");//IJTI-1485
		
		if(list!=null && list.size()>0) {			
		 ptrnItr = list.iterator();
			while(ptrnItr.hasNext()) {
				ptrnpf = ptrnItr.next();
				processPtrn1100();
			}
		   scrnparams.subfileMore.set("Y");
		}else {
			scrnparams.subfileMore.set("N");
		}
		wsaaCount.set(ZERO);
		wsaaSelectRecord.set("N");
		wsaaSelectionFlag.set("Y");
		wsaaAsterisks = "N";
		wsaaRrn.set(scrnparams.subfileRrn);
	}

protected void processPtrn1100()
	{
		try {
			go1100();
			writeSubfile1110();
		}
		catch (GOTOException e){
		}
	}

protected void go1100()
	{
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.selectOut[varcom.nd.toInt()].set(SPACES);
		wsaaSubfileFields.set(SPACES);
		wsaaScreenTranno.set(ptrnpf.getTranno());
		wsaaEffdate.set(ptrnpf.getPtrneff());
		wsaaTrcode.set(ptrnpf.getBatctrcde());
		
		descpf=descDAO.getdescData("IT", t1688 ,ptrnpf.getBatctrcde(),wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1485
		if (descpf == null) {
			wsaaTrandesc.fill("?");
		}
		else {
			wsaaTrandesc.set(descpf.getLongdesc());
		}
		
	/*	descIO.setDataKey(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(ptrnrevIO.getBatctrcde());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			wsaaTrandesc.fill("?");
		}
		else {
			wsaaTrandesc.set(descIO.getLongdesc());
		}*/
		
		
		List<Itempf> itempfList = itemDAO.findItem("IT", wsspcomn.company.toString(), t6661, ptrnpf.getBatctrcde());//IJTI-1485

		if(itempfList!=null && !itempfList.isEmpty()){
			
			Itempf itempf = itempfList.get(0);
			
			if(itempf==null && isNE(t6661rec.contRevFlag,"Y") &&  isNE(t6661rec.contRevFlag,"N") )
			{
		     wsaaSelectionFlag.set("N");
			
				//fatalError600();
				
			}
			t6661rec.t6661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			} else {
				t6661rec.t6661Rec.set(SPACES);	//IBPLIFE-2472
			}
		
		
		/*itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());*/
		if (isNE(t6661rec.contRevFlag,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
			if (isEQ(wsspcomn.flag,"R")) {
				wsaaPtrnRrn.set(scrnparams.subfileRrn);
				wsaaPtrnRrn.add(1);
			}
		}
		else {
			if (isEQ(wsspcomn.flag,"R")) {
				sv.select.set(SPACES);
				wsaaSelect.set(sv.select);
				wsaaPtrnRrn.set(scrnparams.subfileRrn);
				wsaaPtrnRrn.add(1);
			}
		}
		/*if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaSelectionFlag.set("N");
		}
		if (isNE(t6661rec.contRevFlag,"Y")
		&& isNE(t6661rec.contRevFlag,"N")
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaSelectionFlag.set("N");
		}*/
		
		if (isEQ(wsaaSelectionFlag,"N")
		&& isEQ(wsaaAsterisks,"N")) {
			sv.effdate.set(99999999);
			sv.tranno.set(99999);
			sv.select.set(SPACES);
			sv.trcode.set(SPACES);
			sv.trandesc.fill("*");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
			scrnparams.function.set(varcom.sadd);
			processScreen("S5155", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			wsaaAsterisks = "Y";
			wsaaCount.add(1);
			if (isEQ(wsaaCount,wsaaSubfileSize)) {
				goTo(GotoLabel.exit1190);
			}
		}
		/*ILIFE-5437*/
		if (wsaaPreventSelection.isTrue() && isNE(t6661rec.contRevFlag,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void writeSubfile1110()
	{
		sv.subfileFields.set(wsaaSubfileFields);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5155", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
		if (isEQ(wsaaCount,wsaaSubfileSize)) {
			goTo(GotoLabel.exit1190);
		}
	}



/*protected void findDesc1300()
	{
		READ
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		EXIT
	}*/

protected void updateSelect1400()
	{
		update1410();
	}

protected void update1410()
	{
		scrnparams.subfileRrn.set(wsaaPtrnRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S5155", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.select.set("Y");
		scrnparams.function.set(varcom.supd);
		processScreen("S5155", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*SCREEN-IO*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validateScreen2010();
					validate2050();
					validateSubfile2060();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaBatckey.set(wsspcomn.batchkey);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			scrnparams.subfileRrn.set(wsaaRrn);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaCount.set(ZERO);
		
		
			scrnparams.subfileMore.set("N");		
		
			while(ptrnItr.hasNext()) {
				ptrnpf = ptrnItr.next();
				processPtrn1100();
				scrnparams.subfileMore.set("Y");
			}
		   
			wsspcomn.edterror.set("Y");
			wsaaRrn.set(scrnparams.subfileRrn);
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2050()
	{
		if ((isNE(sv.supflag,"Y"))
		&& (isNE(sv.supflag,"N"))) {
			sv.supflagErr.set(e315);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if ((isEQ(sv.supflag,"N"))
		&& (isNE(sv.suppressTo,99999999))) {
			sv.supptoErr.set(g570);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2080);
		}
		if ((isEQ(sv.supflag,"Y"))) {
			if ((isEQ(sv.suppressTo,varcom.vrcmMaxDate))
			|| (isEQ(sv.suppressTo,ZERO))) {
				sv.supptoErr.set(i032);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.checkForErrors2080);
			}
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5155", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if ((isEQ(scrnparams.statuz,varcom.oK))
		&& (isNE(sv.select,SPACES))) {
			wsaaSelectRecord.set("Y");
			validateRecSelected2800();
		}
		else {
			wsaaSelectRecord.set("N");
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		scrnparams.subfileRrn.set(1);
		if (wsaaRecordNotSelected.isTrue()) {
			scrnparams.errorCode.set(i033);
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2600()
	{
		try {
			readNextRecord2610();
		}
		catch (GOTOException e){
		}
	}

protected void readNextRecord2610()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5155", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if ((isEQ(scrnparams.statuz,varcom.endp))
		|| (isEQ(sv.select,SPACES))) {
			goTo(GotoLabel.exit2690);
		}
		if (wsaaRecordNotSelected.isTrue()) {
			wsaaSelectRecord.set("Y");
			validateRecSelected2800();
			goTo(GotoLabel.exit2690);
		}
		sv.selectErr.set(i031);
		updateErrorIndicators2700();
	}

protected void updateErrorIndicators2700()
	{
		/*PARA*/
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		wsspcomn.edterror.set("Y");
		scrnparams.function.set(varcom.supd);
		processScreen("S5155", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void validateRecSelected2800()
	{
		try {
			para2810();
		}
		catch (GOTOException e){
		}
	}

protected void para2810()
	{
		if ((isNE(sv.suppressTo,varcom.vrcmMaxDate))
		&& (isNE(sv.suppressTo,ZERO))) {
			if (isLT(sv.suppressTo,sv.effdate)) {
				sv.supptoErr.set(h035);
				updateErrorIndicators2700();
				goTo(GotoLabel.exit2890);
			}
		}
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP113", appVars, "IT");
	
		if (isLT(sv.effdate,sv.occdate)) {
			if(!contDtCalcFlag) {
			sv.effdateErr.set(f515);
			updateErrorIndicators2700();
			goTo(GotoLabel.exit2890);
			}
		}
		wsaaTodate.set(sv.effdate);
		wsaaSupflag.set(sv.supflag);
		wsaaSuppressTo.set(sv.suppressTo);
		wsaaTranNum.set(sv.tranno);
	
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{

		// ILIFE-8123-starts
		rolloverFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), ROLLOVER_FEATURE_ID, appVars,
				"IT");
		if (rolloverFlag) {
			covrpf = covrpfDAO.getByTranno(chdrpf.getChdrcoy().toString().trim(),
					chdrpf.getChdrnum().trim(), wsaaTranNum.toString().trim(), "1");//IJTI-1485
		}
		// ILIFE-8123-ends
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz,varcom.oK))
		&& (isNE(sftlockrec.statuz,"LOCK"))) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum());
		wsaaBatckey.set(wsspcomn.batchkey);
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTranDate.set(varcom.vrcmDate);
		wsaaTranTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaPlnsfx.set(ZERO);
		wsaaCfiafiTranno.set(ZERO);
		wsaaCfiafiTranCode.set(SPACES);
		wsaaBbldat.set(ZERO);
		wsaaFlag.set(wsspcomn.flag);
		if(rolloverFlag) {
		if(covrpf != null && isEQ(sv.cnttype.toString(),"SIS")) {//ILIFE-8123
		wsaaCover.set(covrpf.getCoverage());//ILIFE-8123
		wsaaLife.set(covrpf.getLife()); //ILIFE-8123
		}
	}
		atreqrec.transArea.set(wsaaTransArea);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
		//ILJ-383 Start
		if(CMDTH010Permission){
		deleteClmReserve3610();
		}
	}
protected void deleteClmReserve3610(){
	List<Crsvpf> crsvpf = crsvpfDAO.getCrsvpfRecord(chdrpf.getChdrnum(), "L", "1", "T671");
	if(!crsvpf.isEmpty()){
		crsvpfDAO.deleteCrsvpfRecord(crsvpf.get(0).getUniqueNumber());
		crsvpf.clear();
		crsvpf = crsvpfDAO.getCrsvpfRecord(chdrpf.getChdrnum(), "L", "2", "T668");
		if(!crsvpf.isEmpty()){
			crsvpf.get(0).setTrdt(wsaaToday.toInt());
			crsvpfDAO.updateValidFlgCrsvRecord(crsvpf.get(0),"1");
		}
	}
 }
 //ILJ-383 Ends
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
