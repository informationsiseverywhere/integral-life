/*
 * File: Revincrm.java
 * Date: 30 August 2009 2:08:36
 * Author: Quipoz Limited
 * 
 * Class transformed from REVINCRM.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmbcrTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrrccTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE COMPONENT MODIFY FROM AUTOMATIC INCREASE
*        ------------------------------------------------
*
* Overview.
* ---------
*
*  This subroutine will reverse the results of the Compoment Add
*  process.  It  will  reverse all  Non-cash  Accounting records
*  (ACMV)  Coverage/Rider  records  (COVR), Individual  Coverage
*  /Rider  Increase records (INCI) and  Agent  Commission (AGCM)
*  records which were created/amended at the time of the forward
*  transaction.
*
* Processing.
* -----------
*
*  Read the Contract Header file (CHDRPF) using the logical
*  view CHDRMJA with a key of company and contract number
*  from linkage.
*
* Reverse Payer ( PAYRs ):
*
*  Read the Payer file (PAYRPF) using the logical view
*  PAYRLIF with a key of company and contract number
*  from linkage.
*
* Reverse Coverage & Rider records ( COVRs ):
*
*     Delete the COVR record whose transaction number (TRANNO) is
*     equal to the tranno on the PTRN we are trying to reverse.
*     i.e. match on company, contract number, life, coverage and
*     rider.
*     Then reinstate the most recent validflag '2' COVR record
*      (ie. the one prior to the Component Modify) to be a
*      validflag '1' COVR.
*
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*     Set the accounting month/year with the batch
*     accounting month/year from linkage.
*     Set the tranno with the NEW tranno from linkage.
*     Set the batch key with new batch key from linkage.
*     Multiply the original currency amount by -1.
*     Multiply the accounting currency amount by -1.
*     Set the transaction reference with the NEW
*     transaction code from linkage.
*     Set the transaction description with the long
*     description of the new trans code from T1688.
*     Set the effective date with todays date.
*     Write the new record to the database by calling the
*     subroutine 'LIFACMV'.
*
* Reverse Agent Commission detail records ( AGCMs ):
*
*  Re-validate all Agent commission records by calling the
*   Agent commission calculation subroutine ACOMCALC. We will
*   also create a temporary PCDT record for use by the
*   subroutine - if it is reversing a decrease, ie. performing
*   an increase, then it needs the PCDT record to know where
*   to assign the new commission.
*
* Reversal of Unit-Linked/Traditional parts of the Contract:
*
*  Loop through all the Coverages/Riders associated with the
*   Contract we are processing and call any Generic processing
*   subroutines that are specified in T5671.
*
*
*  PROGRAMMING NOTE.
********************
*    This subroutine uses the relatively 'new' I/O function
*     WRITD.
*     a) WRITD is used to update a record in place which has
*         had its KEY changed - the record does not need to be
*         LOCKED first
*         Because the Relative Record number hasn't changed by
*          performing the WRITD, NEXTR will then read the next
*          record in the file ( until end-of-file ) and won't
*          pick up and reprocess the one we have just written.
*
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5671 - Generic processing
* T5687 - Coverage/Rider details
* T5688 - Contract type details
* T6658 - Anniversary/Auto Increase Processing Method
*
*****************************************************************
* </pre>
*/
public class Revincrm extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVINCRM";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaValidflag1Found = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaValidflag2Found = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRasnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaRatype = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaAnnpremBefore = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaAnnpremAfter = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaFreqFactor = new PackedDecimalData(11, 5).init(ZERO);
	private FixedLengthStringData wsaaVflag2 = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);

	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0).setUnsigned();

	private FixedLengthStringData wsaaBatchkey = new FixedLengthStringData(65);
	private FixedLengthStringData wsaaContkey = new FixedLengthStringData(19).isAPartOf(wsaaBatchkey, 0);
	private FixedLengthStringData wsaaBatcpfx = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 0);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaContkey, 2);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaContkey, 3);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaContkey, 5);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaContkey, 8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaContkey, 10);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaContkey, 14);
	private FixedLengthStringData wsaaOldBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatchkey, 19);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
	private ZonedDecimalData wsaaReinstateTranno = new ZonedDecimalData(5, 0);
		/* ERRORS */
	private static final String e308 = "E308";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6658 = "T6658";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmbcrTableDAM agcmbcrIO = new AgcmbcrTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrrccTableDAM covrrccIO = new CovrrccTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6658rec t6658rec = new T6658rec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next10450
	}

	public Revincrm() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processChdrs3000();
		processPayrs4000();
		processCovrs5000();
		processAcmvs8000();
		processAcmvOptical8010();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaTransDesc.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaSub.set(ZERO);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError99000();
		}
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void processChdrs3000()
	{
		start3000();
	}

protected void start3000()
	{
		/*  READ Contract Header CHDR*/
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(reverserec.company);
		chdrmjaIO.setChdrnum(reverserec.chdrnum);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			databaseError99500();
		}
		/* Read T5688 Contract details table to check for Component*/
		/*  Level Accounting for use in the Agent commission calculations*/
		/*  later on.*/
		readT56883100();
	}

protected void readT56883100()
	{
		start3100();
	}

protected void start3100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrmjaIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			systemError99000();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void processPayrs4000()
	{
		start4000();
	}

protected void start4000()
	{
		/*  READ Contract Header PAYR*/
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		payrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			databaseError99500();
		}
	}

protected void processCovrs5000()
	{
		start5000();
	}

protected void start5000()
	{
		/* Use COVR logical view to loop around coverage/rider records*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setLife(reverserec.life);
		covrIO.setCoverage(reverserec.coverage);
		covrIO.setRider(reverserec.rider);
		covrIO.setPlanSuffix(reverserec.planSuffix);
		covrIO.setFormat(formatsInner.covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(covrIO.getLife(), reverserec.life)
		|| isNE(covrIO.getCoverage(), reverserec.coverage)
		|| isNE(covrIO.getRider(), reverserec.rider)
		|| isNE(covrIO.getPlanSuffix(), reverserec.planSuffix)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			reverseCovrs5100();
		}
		
	}

protected void reverseCovrs5100()
	{
		start5100();
	}

protected void start5100()
	{
		/* check TRANNO of COVR just read*/
		if (isEQ(reverserec.tranno, covrIO.getTranno())
		&& isEQ(reverserec.planSuffix, covrIO.getPlanSuffix())) {
			covrrccIO.setParams(SPACES);
			covrrccIO.setChdrcoy(covrIO.getChdrcoy());
			covrrccIO.setChdrnum(covrIO.getChdrnum());
			covrrccIO.setLife(covrIO.getLife());
			covrrccIO.setCoverage(covrIO.getCoverage());
			covrrccIO.setRider(covrIO.getRider());
			covrrccIO.setPlanSuffix(covrIO.getPlanSuffix());
			/*     MOVE BEGN               TO COVRRCC-FUNCTION              */
			covrrccIO.setFunction(varcom.readh);
			covrrccIO.setTranno(reverserec.tranno);
			covrrccIO.setFormat(formatsInner.covrrccrec);
			/*     PERFORM 5200-DELETE-COVRS UNTIL COVRRCC-STATUZ = ENDP    */
			deleteCovrsR5200();
			/*        Do Generic processing after COVRs deleted and earlier*/
			/*         COVR reinstated to validflag '1'.*/
			genericProcessing9000();
			agentCommissionRecalc10000();
		}
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			systemError99000();
		}
		if (isNE(covrIO.getChdrcoy(), reverserec.company)
		|| isNE(covrIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(covrIO.getLife(), reverserec.life)
		|| isNE(covrIO.getCoverage(), reverserec.coverage)
		|| isNE(covrIO.getRider(), reverserec.rider)
		|| isNE(covrIO.getPlanSuffix(), reverserec.planSuffix)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
	}

protected void deleteCovrsR5200()
	{
		/*START*/
		covrrccIo5300();
		covrrccIO.setFunction(varcom.delet);
		covrrccIo5300();
		covrrccIO.setFunction(varcom.endh);
		covrrccIo5300();
		covrrccIO.setFunction(varcom.rewrt);
		covrrccIO.setValidflag("1");
		covrrccIO.setCurrto(99999999);
		covrrccIo5300();
		/*EXIT*/
	}

	/**
	* <pre>
	*5200-DELETE-COVRS SECTION.                                       
	*5200-START.                                                      
	**** CALL 'COVRRCCIO'            USING COVRRCC-PARAMS.            
	**** IF COVRRCC-STATUZ           NOT = O-K                        
	****    AND COVRRCC-STATUZ       NOT = ENDP                       
	****     MOVE COVRRCC-PARAMS     TO SYSR-PARAMS                   
	****     MOVE COVRRCC-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** IF COVR-CHDRCOY             NOT = COVRRCC-CHDRCOY            
	****    OR COVR-CHDRNUM          NOT = COVRRCC-CHDRNUM            
	****    OR COVR-LIFE             NOT = COVRRCC-LIFE               
	****    OR COVR-COVERAGE         NOT = COVRRCC-COVERAGE           
	****    OR COVR-RIDER            NOT = COVRRCC-RIDER              
	****    OR COVR-PLAN-SUFFIX      NOT = COVRRCC-PLAN-SUFFIX        
	****    OR COVRRCC-STATUZ        = ENDP                           
	****     MOVE ENDP               TO COVRRCC-STATUZ                
	****     GO TO 5290-EXIT                                          
	**** END-IF.                                                      
	**** IF COVRRCC-TRANNO           NOT < REVE-TRANNO                
	****     MOVE READH                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE DELET                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE NEXTR                  TO COVRRCC-FUNCTION          
	**** ELSE                                                         
	****     MOVE READH                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE '1'                    TO COVRRCC-VALIDFLAG         
	****     MOVE 99999999               TO COVRRCC-CURRTO            
	****     MOVE REWRT                  TO COVRRCC-FUNCTION          
	****     PERFORM 5300-COVRRCC-IO                                  
	****     MOVE ENDP                   TO COVRRCC-STATUZ            
	**** END-IF.                                                      
	*5290-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
protected void covrrccIo5300()
	{
		/*START*/
		SmartFileCode.execute(appVars, covrrccIO);
		if (isNE(covrrccIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrccIO.getParams());
			syserrrec.statuz.set(covrrccIO.getStatuz());
			systemError99000();
		}
		/*EXIT*/
	}

protected void processAcmvs8000()
	{
		start8000();
	}

protected void start8000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs8100();
		}
		
	}

protected void processAcmvOptical8010()
	{
		start8010();
	}

protected void start8010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError99500();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs8100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError99500();
			}
		}
	}

protected void reverseAcmvs8100()
	{
		start8100();
		readNextAcmv8180();
	}

protected void start8100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
			return ;
		}
		/* post equal & opposite amount to ACMV just read*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void readNextAcmv8180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void genericProcessing9000()
	{
		/*START*/
		/* We do any generic processing that is required for the current*/
		/*  Coverage/Rider by calling subroutine(s) specified on*/
		/*  table T5671*/
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(reverserec.company);
		covrenqIO.setChdrnum(reverserec.chdrnum);
		covrenqIO.setLife(covrIO.getLife());
		covrenqIO.setCoverage(covrIO.getCoverage());
		covrenqIO.setRider(covrIO.getRider());
		covrenqIO.setPlanSuffix(covrIO.getPlanSuffix());
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			processCovrs9100();
		}
		
		/*EXIT*/
	}

protected void processCovrs9100()
	{
		/*START*/
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, covrenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrenqIO.getChdrnum())
		|| isNE(covrIO.getLife(), covrenqIO.getLife())
		|| isNE(covrIO.getCoverage(), covrenqIO.getCoverage())
		|| isNE(covrIO.getRider(), covrenqIO.getRider())
		|| isNE(covrIO.getPlanSuffix(), covrenqIO.getPlanSuffix())) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		genericSubr9200();
		covrenqIO.setStatuz(varcom.endp);
		/*EXIT1*/
	}

protected void genericSubr9200()
	{
		start9200();
	}

protected void start9200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrenqIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrenqIO.getChdrcoy());
		greversrec.chdrnum.set(covrenqIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrenqIO.getPlanSuffix());
		greversrec.life.set(covrenqIO.getLife());
		greversrec.coverage.set(covrenqIO.getCoverage());
		greversrec.rider.set(covrenqIO.getRider());
		greversrec.crtable.set(covrenqIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		wsaaBatchkey.set(reverserec.batchkey);
		/* Set old Transaction code so that in the generic subroutine*/
		/* GRVULCHG, it can pass it into UNLCHG subroutine so that*/
		/* UNLCHG can read the table T5537 correctly.*/
		/* NOTE... There are only U/linked generic reversal subroutines*/
		/*          for Component modify.*/
		wsaaOldBatctrcde.set(reverserec.oldBatctrcde);
		greversrec.batckey.set(wsaaBatchkey);
		greversrec.effdate.set(payrlifIO.getPtdate());
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs9300();
		}
	}

protected void callTrevsubs9300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError99000();
			}
		}
		/*EXIT*/
	}

protected void agentCommissionRecalc10000()
	{
		start10000();
	}

protected void start10000()
	{
		/* Read T5687 using CRTABLE to get Component Commission details*/
		readT568710100();
		/* Read Table T6658 to check if Commission was applicable for      */
		/* this policy. If it wasn't, then skip to the end of this         */
		/* section, because there won't be anything to reverse.            */
		checkComindOnT665810300();
		if (isEQ(t6658rec.comind, SPACES)) {
			return ;
		}
		/* Depending on the billing frequency from the PAYR record,        */
		/*  calculate the 'before' & 'after' annual premiums so that       */
		/*  they can be passed into the commission recalculation           */
		/*  subroutine.                                                    */
		/* MOVE PAYRLIF-BILLFREQ       TO WSAA-BILLFREQ.                */
		/* MOVE WSAA-BILLFREQ-NUM      TO WSAA-FREQ-FACTOR.             */
		/* first the 'old' calculation                                     */
		/* IF COVR-INSTPREM              = ZEROS                        */
		/*     MOVE COVR-SINGP           TO WSAA-ANNPREM-BEFORE         */
		/* ELSE                                                         */
		/*     MULTIPLY COVR-INSTPREM    BY WSAA-FREQ-FACTOR            */
		/*                               GIVING WSAA-ANNPREM-BEFORE     */
		/* END-IF.                                                      */
		/* now the 'new' calculation                                       */
		/* IF COVRRCC-INSTPREM           = ZEROS                        */
		/*     MOVE COVRRCC-SINGP        TO WSAA-ANNPREM-AFTER          */
		/* ELSE                                                         */
		/*     MULTIPLY COVRRCC-INSTPREM BY WSAA-FREQ-FACTOR            */
		/*                               GIVING WSAA-ANNPREM-AFTER      */
		/* END-IF.                                                      */
		/* Need to write a temporary PCDT record here so that any extra    */
		/*  commission calculated by ACOMCALC gets assigned correctly.     */
		/* MOVE SPACES                 TO PCDTMJA-PARAMS.               */
		/* MOVE CHDRMJA-CHDRCOY        TO PCDTMJA-CHDRCOY.              */
		/* MOVE CHDRMJA-CHDRNUM        TO PCDTMJA-CHDRNUM.              */
		/* MOVE COVR-LIFE              TO PCDTMJA-LIFE.                 */
		/* MOVE COVR-COVERAGE          TO PCDTMJA-COVERAGE.             */
		/* MOVE COVR-RIDER             TO PCDTMJA-RIDER.                */
		/* MOVE COVR-PLAN-SUFFIX       TO PCDTMJA-PLAN-SUFFIX.          */
		/* MOVE CHDRMJA-TRANNO         TO PCDTMJA-TRANNO.               */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM01.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM02.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM03.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM04.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM05.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM06.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM07.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM08.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM09.        */
		/* MOVE ZEROS                  TO PCDTMJA-SPLIT-BCOMM10.        */
		/* MOVE CHDRMJA-AGNTNUM        TO PCDTMJA-AGNTNUM( 1 ).         */
		/* MOVE +100                   TO PCDTMJA-SPLITC( 1 ).          */
		/* MOVE ZEROS                  TO PCDTMJA-INSTPREM.             */
		/* MOVE WRITR                  TO PCDTMJA-FUNCTION.             */
		/* MOVE PCDTMJAREC             TO PCDTMJA-FORMAT.               */
		/* CALL 'PCDTMJAIO'            USING PCDTMJA-PARAMS.            */
		/* IF PCDTMJA-STATUZ           NOT = O-K                        */
		/*     MOVE PCDTMJA-PARAMS     TO SYSR-PARAMS                   */
		/*     MOVE PCDTMJA-STATUZ     TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR                               */
		/* END-IF.                                                      */
		/* Revalidate Agent Commission detail records (AGCMs) by           */
		/*  calling the Agent Commission calculation subroutine            */
		/*  known as 'ACOMCALC'.                                           */
		/* We pass the the 'old' component premium values in the           */
		/*  ACOM-COVR-XXXX fields, and the 'new' component premium         */
		/*  values in the ACOM-COVT-XXXX fields in the linkage record.     */
		/* MOVE SPACES                 TO ACOM-ACOMCAL-REC.             */
		/* MOVE COVR-CHDRCOY           TO ACOM-COMPANY.                 */
		/* MOVE COVR-CHDRNUM           TO ACOM-CHDRNUM.                 */
		/* MOVE COVR-LIFE              TO ACOM-COVR-LIFE.               */
		/* MOVE COVR-LIFE              TO ACOM-COVT-LIFE.               */
		/* MOVE COVR-COVERAGE          TO ACOM-COVR-COVERAGE.           */
		/* MOVE COVR-COVERAGE          TO ACOM-COVT-COVERAGE.           */
		/* MOVE COVR-RIDER             TO ACOM-COVR-RIDER.              */
		/* MOVE COVR-RIDER             TO ACOM-COVT-RIDER.              */
		/* MOVE COVR-PLAN-SUFFIX       TO ACOM-COVR-PLAN-SUFFIX.        */
		/* MOVE COVR-PLAN-SUFFIX       TO ACOM-COVT-PLAN-SUFFIX.        */
		/* MOVE REVE-NEW-TRANNO        TO ACOM-TRANNO.                  */
		/* MOVE REVE-TRANS-DATE        TO ACOM-TRANSACTION-DATE.        */
		/* MOVE REVE-TRANS-TIME        TO ACOM-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO ACOM-USER.                    */
		/* MOVE REVE-TERMID            TO ACOM-TERMID.                  */
		/* MOVE PAYRLIF-BTDATE         TO ACOM-PAYR-BTDATE.             */
		/* MOVE PAYRLIF-PTDATE         TO ACOM-PAYR-PTDATE.             */
		/* MOVE PAYRLIF-BILLFREQ       TO ACOM-PAYR-BILLFREQ.           */
		/* MOVE PAYRLIF-CNTCURR        TO ACOM-PAYR-CNTCURR.            */
		/* MOVE CHDRMJA-CNTTYPE        TO ACOM-CNTTYPE.                 */
		/* MOVE CHDRMJA-OCCDATE        TO ACOM-CHDR-OCCDATE.            */
		/* MOVE CHDRMJA-CURRFROM       TO ACOM-CHDR-CURRFROM.           */
		/* MOVE CHDRMJA-AGNTCOY        TO ACOM-CHDR-AGNTCOY.            */
		/* MOVE COVR-JLIFE             TO ACOM-COVR-JLIFE.              */
		/* MOVE COVR-CRTABLE           TO ACOM-COVR-CRTABLE.            */
		/* MOVE T5688-COMLVLACC        TO ACOM-COMLVLACC.               */
		/* MOVE T5687-BASIC-COMM-METH  TO ACOM-BASIC-COMM-METH.         */
		/* MOVE T5687-BASCPY           TO ACOM-BASCPY.                  */
		/* MOVE T5687-RNWCPY           TO ACOM-RNWCPY.                  */
		/* MOVE T5687-SRVCPY           TO ACOM-SRVCPY.                  */
		/* MOVE WSAA-ANNPREM-BEFORE    TO ACOM-COVR-ANNPREM.            */
		/* MOVE WSAA-ANNPREM-AFTER     TO ACOM-COVT-ANNPREM.            */
		/* MOVE REVE-LANGUAGE          TO ACOM-ATMD-LANGUAGE.           */
		/* MOVE REVE-BATCHKEY          TO ACOM-ATMD-BATCH-KEY.          */
		/* CALL 'ACOMCALC'             USING ACOM-ACOMCAL-REC.          */
		/* IF ACOM-STATUZ              NOT = O-K                        */
		/*     MOVE ACOM-ACOMCAL-REC   TO SYSR-PARAMS                   */
		/*     MOVE ACOM-STATUZ        TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR                               */
		/* END-IF.                                                      */
		/* MOVE READH                  TO PCDTMJA-FUNCTION.             */
		/* MOVE PCDTMJAREC             TO PCDTMJA-FORMAT.               */
		/* CALL 'PCDTMJAIO'            USING PCDTMJA-PARAMS.            */
		/* IF PCDTMJA-STATUZ           NOT = O-K                        */
		/*     MOVE PCDTMJA-PARAMS     TO SYSR-PARAMS                   */
		/*     MOVE PCDTMJA-STATUZ     TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR                               */
		/* END-IF.                                                      */
		/* MOVE DELET                  TO PCDTMJA-FUNCTION.             */
		/* MOVE PCDTMJAREC             TO PCDTMJA-FORMAT.               */
		/* CALL 'PCDTMJAIO'            USING PCDTMJA-PARAMS.            */
		/* IF PCDTMJA-STATUZ           NOT = O-K                        */
		/*     MOVE PCDTMJA-PARAMS     TO SYSR-PARAMS                   */
		/*     MOVE PCDTMJA-STATUZ     TO SYSR-STATUZ                   */
		/*     PERFORM 99000-SYSTEM-ERROR                               */
		/* END-IF.                                                      */
		/* Delete all validflag '1' AGCMs created by the forward        */
		/* transaction.                                                 */
		wsaaReinstateTranno.set(ZERO);
		agcmbcrIO.setDataArea(SPACES);
		agcmbcrIO.setChdrcoy(covrIO.getChdrcoy());
		agcmbcrIO.setChdrnum(covrIO.getChdrnum());
		agcmbcrIO.setLife(covrIO.getLife());
		agcmbcrIO.setCoverage(covrIO.getCoverage());
		agcmbcrIO.setRider(covrIO.getRider());
		agcmbcrIO.setPlanSuffix(covrIO.getPlanSuffix());
		agcmbcrIO.setTranno(reverserec.tranno);
		agcmbcrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmbcrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		agcmbcrIO.setFormat(formatsInner.agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)
		&& isNE(agcmbcrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
		if (isNE(agcmbcrIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(agcmbcrIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(agcmbcrIO.getCoverage(), covrIO.getCoverage())
		|| isNE(agcmbcrIO.getLife(), covrIO.getLife())
		|| isNE(agcmbcrIO.getRider(), covrIO.getRider())
		|| isNE(agcmbcrIO.getPlanSuffix(), covrIO.getPlanSuffix())
		|| isNE(agcmbcrIO.getTranno(), reverserec.tranno)) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
		if (isEQ(agcmbcrIO.getStatuz(), varcom.endp)) {
			return ;
		}
		while ( !(isNE(wsaaReinstateTranno, ZERO))) {
			deleteAgcms10400();
		}
		
		/* Reinstate the AGCMs set to validflag '2' by the forward      */
		/* transaction.                                                 */
		while ( !(isEQ(agcmbcrIO.getStatuz(), varcom.endp))) {
			reinstateAgcms10500();
		}
		
	}

protected void readT568710100()
	{
		start10100();
	}

protected void start10100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(), covrIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(covrIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkComindOnT665810300()
	{
		start10300();
	}

protected void start10300()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrIO.getChdrcoy());
		itdmIO.setItemtabl(t6658);
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setItemitem(t5687rec.anniversaryMethod);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if (isNE(itdmIO.getItemcoy(), covrIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6658)
		|| isNE(itdmIO.getItemitem(), t5687rec.anniversaryMethod)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		else {
			t6658rec.t6658Rec.set(itdmIO.getGenarea());
		}
	}

protected void deleteAgcms10400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					check10410();
				case next10450: 
					next10450();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void check10410()
	{
		/* Do not process single premium AGCM records.                  */
		if (isEQ(agcmbcrIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.next10450);
		}
		/* Delete the AGCM record.                                      */
		agcmbcrIO.setFunction(varcom.deltd);
		agcmbcrIO.setFormat(formatsInner.agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
	}

protected void next10450()
	{
		/* Look for the next record to delete.                          */
		agcmbcrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
		if (isNE(agcmbcrIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(agcmbcrIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(agcmbcrIO.getCoverage(), covrIO.getCoverage())
		|| isNE(agcmbcrIO.getLife(), covrIO.getLife())
		|| isNE(agcmbcrIO.getRider(), covrIO.getRider())
		|| isNE(agcmbcrIO.getPlanSuffix(), covrIO.getPlanSuffix())) {
			agcmbcrIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			syserrrec.params.set(agcmbcrIO.getParams());
			systemError99000();
		}
		/* If this is a validflag '2' record, come out of the deletion  */
		/* loop and enter the reinstatement loop.                       */
		if (isEQ(agcmbcrIO.getValidflag(), "2")) {
			wsaaReinstateTranno.set(agcmbcrIO.getTranno());
		}
	}

protected void reinstateAgcms10500()
	{
		check10510();
		next10550();
	}

protected void check10510()
	{
		/* Do not process single premium AGCM records.                  */
		if (isEQ(agcmbcrIO.getPtdate(), ZERO)) {
			return ;
		}
		/* Reinstate the AGCM record.                                   */
		agcmbcrIO.setValidflag("1");
		agcmbcrIO.setCurrto(varcom.vrcmMaxDate);
		agcmbcrIO.setFunction(varcom.writd);
		agcmbcrIO.setFormat(formatsInner.agcmbcrrec);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
	}

protected void next10550()
	{
		/* Look for the next record to reinstate.                       */
		agcmbcrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbcrIO);
		if (isNE(agcmbcrIO.getStatuz(), varcom.oK)
		&& isNE(agcmbcrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmbcrIO.getParams());
			syserrrec.statuz.set(agcmbcrIO.getStatuz());
			systemError99000();
		}
		if (isNE(agcmbcrIO.getChdrcoy(), covrIO.getChdrcoy())
		|| isNE(agcmbcrIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(agcmbcrIO.getCoverage(), covrIO.getCoverage())
		|| isNE(agcmbcrIO.getLife(), covrIO.getLife())
		|| isNE(agcmbcrIO.getRider(), covrIO.getRider())
		|| isNE(agcmbcrIO.getPlanSuffix(), covrIO.getPlanSuffix())
		|| isNE(agcmbcrIO.getTranno(), wsaaReinstateTranno)) {
			agcmbcrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError99500()
	{
		start99500();
		exit99990();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC   ");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covrrccrec = new FixedLengthStringData(10).init("COVRRCCREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData agcmbcrrec = new FixedLengthStringData(10).init("AGCMBCRREC");
}
}
