package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5066
 * @version 1.0 generated on 30/08/09 06:32
 * @author Quipoz
 */
public class S5066ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(850);
	public FixedLengthStringData dataFields = new FixedLengthStringData(434).isAPartOf(dataArea, 0);
	public ZonedDecimalData clamant = DD.clamant.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,17);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,20);
	public ZonedDecimalData estimateTotalValue = DD.estimtotal.copyToZonedDecimal().isAPartOf(dataFields,28);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,45);
	public ZonedDecimalData policyloan = DD.policyloan.copyToZonedDecimal().isAPartOf(dataFields,62);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,83);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,133);
	public ZonedDecimalData tdbtamt = DD.tdbtamt.copyToZonedDecimal().isAPartOf(dataFields,150);
	public ZonedDecimalData zrcshamt = DD.zrcshamt.copyToZonedDecimal().isAPartOf(dataFields,167);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,180);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,207);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,237);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,245);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,292);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,300);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,347);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,355);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,402);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,406);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,416);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,424);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 434);
	public FixedLengthStringData clamantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData estimtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData tdbtamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData zrcshamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea, 538);
	public FixedLengthStringData[] clamantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] estimtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] tdbtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] zrcshamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(224);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(62).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public FixedLengthStringData fundtype = DD.fundtype.copy().isAPartOf(subfileFields,39);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,40);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,44);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,46);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,48);
	public FixedLengthStringData fund = DD.virtfund.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 62);
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData fundtypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData virtfundErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 102);
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] fundtypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] virtfundOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 222);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5066screensflWritten = new LongData(0);
	public LongData S5066screenctlWritten = new LongData(0);
	public LongData S5066screenWritten = new LongData(0);
	public LongData S5066protectWritten = new LongData(0);
	public GeneralTable s5066screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5066screensfl;
	}

	public S5066ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {hcrtable, life, coverage, rider, fund, fundtype, shortds, cnstcur, estMatValue, actvalue};
		screenSflOutFields = new BaseData[][] {hcrtableOut, lifeOut, coverageOut, riderOut, virtfundOut, fundtypeOut, shortdsOut, cnstcurOut, emvOut, actvalueOut};
		screenSflErrFields = new BaseData[] {hcrtableErr, lifeErr, coverageErr, riderErr, virtfundErr, fundtypeErr, shortdsErr, cnstcurErr, emvErr, actvalueErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, planSuffix, effdate, policyloan, otheradjst, currcd, estimateTotalValue, clamant, reasoncd, resndesc, zrcshamt, tdbtamt, taxamt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, plnsfxOut, effdateOut, policyloanOut, otheradjstOut, currcdOut, estimtotalOut, clamantOut, reasoncdOut, resndescOut, zrcshamtOut, tdbtamtOut, taxamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, plnsfxErr, effdateErr, policyloanErr, otheradjstErr, currcdErr, estimtotalErr, clamantErr, reasoncdErr, resndescErr, zrcshamtErr, tdbtamtErr, taxamtErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp};
		
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5066screen.class;
		screenSflRecord = S5066screensfl.class;
		screenCtlRecord = S5066screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5066protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5066screenctl.lrec.pageSubfile);
	}
}
