package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:37
 * Description:
 * Copybook name: CWFDPRCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cwfdprckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData cwfdprcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData cwfdprcKey = new FixedLengthStringData(64).isAPartOf(cwfdprcFileKey, 0, REDEFINE);
  	public FixedLengthStringData cwfdprcChdrcoy = new FixedLengthStringData(1).isAPartOf(cwfdprcKey, 0);
  	public FixedLengthStringData cwfdprcChdrnum = new FixedLengthStringData(8).isAPartOf(cwfdprcKey, 1);
  	public PackedDecimalData cwfdprcPrcSeqNbr = new PackedDecimalData(5, 0).isAPartOf(cwfdprcKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(cwfdprcKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(cwfdprcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		cwfdprcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}