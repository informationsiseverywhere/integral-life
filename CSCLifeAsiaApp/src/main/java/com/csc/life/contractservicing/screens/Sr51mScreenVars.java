package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR51M
 * @version 1.0 generated on 30/08/09 07:17
 * @author Quipoz
 */
public class Sr51mScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(879);
	public FixedLengthStringData dataFields = new FixedLengthStringData(399).isAPartOf(dataArea, 0);
	public ZonedDecimalData cmax = DD.cmax.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData hprjptdate = DD.hprjptdate.copyToZonedDecimal().isAPartOf(dataFields,17);
	public ZonedDecimalData manadj = DD.manadj.copyToZonedDecimal().isAPartOf(dataFields,25);
	public FixedLengthStringData optdsc = DD.optdsc.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData optind = DD.optind.copy().isAPartOf(dataFields,53);
	public ZonedDecimalData reqamt = DD.reqamt.copyToZonedDecimal().isAPartOf(dataFields,54);
	public ZonedDecimalData totalfee = DD.totalfee.copyToZonedDecimal().isAPartOf(dataFields,71);
	public ZonedDecimalData xamt = DD.xamt.copyToZonedDecimal().isAPartOf(dataFields,88);
	public FixedLengthStringData acdes = DD.acdes.copy().isAPartOf(dataFields,105);
	public FixedLengthStringData apind = DD.apind.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,136);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,138);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,146);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,154);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,167);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,170);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,178);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,208);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,216);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,263);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,271);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,272);
	public ZonedDecimalData osbal = DD.osbal.copyToZonedDecimal().isAPartOf(dataFields,280);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,297);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,344);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,354);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,362);
	public ZonedDecimalData susamt = DD.susamt.copyToZonedDecimal().isAPartOf(dataFields,365);
	public ZonedDecimalData tolerance = DD.toler.copyToZonedDecimal().isAPartOf(dataFields,382);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 399);
	public FixedLengthStringData cmaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData hprjptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData manadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData optdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData optindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData reqamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData totalfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData xamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData acdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData apindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData osbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData susamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData tolerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 519);
	public FixedLengthStringData[] cmaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] hprjptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] manadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] reqamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] totalfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] xamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] acdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] apindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] osbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] susamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] tolerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(3479);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(2933).isAPartOf(subfileArea, 0);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData bftpaym = DD.bftpaym.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData chgflag = DD.chgflag.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(subfileFields,6);
	public FixedLengthStringData crtabled = DD.crtabled.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData datakey = DD.datakey.copy().isAPartOf(subfileFields,40);
	public FixedLengthStringData genarea = DD.genarea.copy().isAPartOf(subfileFields,296);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(subfileFields,796);
	public ZonedDecimalData instprem = DD.instprem.copyToZonedDecimal().isAPartOf(subfileFields,797);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,814);
	public FixedLengthStringData livesno = DD.livesno.copy().isAPartOf(subfileFields,816);
	public ZonedDecimalData origSum = DD.origsum.copyToZonedDecimal().isAPartOf(subfileFields,817);
	public FixedLengthStringData premind = DD.premind.copy().isAPartOf(subfileFields,834);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(subfileFields,835);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,837);
	public ZonedDecimalData rerateDate = DD.rrtdat.copyToZonedDecimal().isAPartOf(subfileFields,839);
	public ZonedDecimalData rtrnwfreq = DD.rtrnwfreq.copyToZonedDecimal().isAPartOf(subfileFields,847);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,849);
	public ZonedDecimalData singp = DD.singp.copyToZonedDecimal().isAPartOf(subfileFields,850);
	public FixedLengthStringData statcde = DD.statcde.copy().isAPartOf(subfileFields,867);
	public ZonedDecimalData sumins = DD.sumins.copyToZonedDecimal().isAPartOf(subfileFields,869);
	public FixedLengthStringData waivercode = DD.waivercode.copy().isAPartOf(subfileFields,886);
	public FixedLengthStringData waiverprem = DD.waiverprem.copy().isAPartOf(subfileFields,890);
	public FixedLengthStringData workAreaData = DD.workarea.copy().isAPartOf(subfileFields,891);
	public FixedLengthStringData wvfind = DD.wvfind.copy().isAPartOf(subfileFields,2891);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(subfileFields,2892);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(subfileFields,2909);
	public FixedLengthStringData zrwvflgs = new FixedLengthStringData(5).isAPartOf(subfileFields, 2926);
	public FixedLengthStringData[] zrwvflg = FLSArrayPartOfStructure(5, 1, zrwvflgs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(5).isAPartOf(zrwvflgs, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrwvflg01 = DD.zrwvflg.copy().isAPartOf(filler,0);
	public FixedLengthStringData zrwvflg02 = DD.zrwvflg.copy().isAPartOf(filler,1);
	public FixedLengthStringData zrwvflg03 = DD.zrwvflg.copy().isAPartOf(filler,2);
	public FixedLengthStringData zrwvflg04 = DD.zrwvflg.copy().isAPartOf(filler,3);
	public FixedLengthStringData zrwvflg05 = DD.zrwvflg.copy().isAPartOf(filler,4);
	public ZonedDecimalData zunit = DD.zunit.copyToZonedDecimal().isAPartOf(subfileFields,2931);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(136).isAPartOf(subfileArea, 2933);
	public FixedLengthStringData benplnErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData bftpaymErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData chgflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData crtabledErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData datakeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData genareaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData instpremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData livesnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData origsumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData premindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData rrtdatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	public FixedLengthStringData rtrnwfreqErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 68);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 72);
	public FixedLengthStringData singpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 76);
	public FixedLengthStringData statcdeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 80);
	public FixedLengthStringData suminsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 84);
	public FixedLengthStringData waivercodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 88);
	public FixedLengthStringData waiverpremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 92);
	public FixedLengthStringData workareaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 96);
	public FixedLengthStringData wvfindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 100);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 104);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 108);
	public FixedLengthStringData zrwvflgsErr = new FixedLengthStringData(20).isAPartOf(errorSubfile, 112);
	public FixedLengthStringData[] zrwvflgErr = FLSArrayPartOfStructure(5, 4, zrwvflgsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(zrwvflgsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrwvflg01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData zrwvflg02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData zrwvflg03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData zrwvflg04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData zrwvflg05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData zunitErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 132);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(408).isAPartOf(subfileArea, 3069);
	public FixedLengthStringData[] benplnOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] bftpaymOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] chgflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] crtabledOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] datakeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] genareaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] instpremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] livesnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] origsumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] premindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] rrtdatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	public FixedLengthStringData[] rtrnwfreqOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 204);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 216);
	public FixedLengthStringData[] singpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 228);
	public FixedLengthStringData[] statcdeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 240);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 252);
	public FixedLengthStringData[] waivercodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 264);
	public FixedLengthStringData[] waiverpremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 276);
	public FixedLengthStringData[] workareaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 288);
	public FixedLengthStringData[] wvfindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 300);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 312);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 324);
	public FixedLengthStringData zrwvflgsOut = new FixedLengthStringData(60).isAPartOf(outputSubfile, 336);
	public FixedLengthStringData[] zrwvflgOut = FLSArrayPartOfStructure(5, 12, zrwvflgsOut, 0);
	public FixedLengthStringData[][] zrwvflgO = FLSDArrayPartOfArrayStructure(12, 1, zrwvflgOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(zrwvflgsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrwvflg01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] zrwvflg02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] zrwvflg03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] zrwvflg04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] zrwvflg05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] zunitOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 396);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 3477);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData hprjptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateDateDisp = new FixedLengthStringData(10);

	public LongData Sr51mscreensflWritten = new LongData(0);
	public LongData Sr51mscreenctlWritten = new LongData(0);
	public LongData Sr51mscreenWritten = new LongData(0);
	public LongData Sr51mprotectWritten = new LongData(0);
	public GeneralTable sr51mscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr51mscreensfl;
	}

	public Sr51mScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","51","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(apindOut,new String[] {"48","68","-48",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tolerOut,new String[] {null, null, null, "71",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optindOut,new String[] {"03","63","-03","53",null, null, null, null, null, null, null, null});
		fieldIndMap.put(manadjOut,new String[] {"45","75","-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenSflFields = new BaseData[] {zrwvflg01, zrwvflg02, zrwvflg03, zrwvflg04, waiverprem, ind, wvfind, genarea, datakey, zbinstprem, zlinstprem, rerateDate, singp, origSum, workAreaData, rtrnwfreq, benpln, zrwvflg05, livesno, waivercode, chgflag, zunit, bftpaym, premind, select, life, coverage, rider, crtabled, crtable, statcde, pstatcode, sumins, instprem};
		screenSflOutFields = new BaseData[][] {zrwvflg01Out, zrwvflg02Out, zrwvflg03Out, zrwvflg04Out, waiverpremOut, indOut, wvfindOut, genareaOut, datakeyOut, zbinstpremOut, zlinstpremOut, rrtdatOut, singpOut, origsumOut, workareaOut, rtrnwfreqOut, benplnOut, zrwvflg05Out, livesnoOut, waivercodeOut, chgflagOut, zunitOut, bftpaymOut, premindOut, selectOut, lifeOut, coverageOut, riderOut, crtabledOut, crtableOut, statcdeOut, pstatcodeOut, suminsOut, instpremOut};
		screenSflErrFields = new BaseData[] {zrwvflg01Err, zrwvflg02Err, zrwvflg03Err, zrwvflg04Err, waiverpremErr, indErr, wvfindErr, genareaErr, datakeyErr, zbinstpremErr, zlinstpremErr, rrtdatErr, singpErr, origsumErr, workareaErr, rtrnwfreqErr, benplnErr, zrwvflg05Err, livesnoErr, waivercodeErr, chgflagErr, zunitErr, bftpaymErr, premindErr, selectErr, lifeErr, coverageErr, riderErr, crtabledErr, crtableErr, statcdeErr, pstatcodeErr, suminsErr, instpremErr};
		screenSflDateFields = new BaseData[] {rerateDate};
		screenSflDateErrFields = new BaseData[] {rrtdatErr};
		screenSflDateDispFields = new BaseData[] {rerateDateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, register, chdrstatus, premstatus, cownnum, ownername, lifenum, lifename, occdate, billfreq, mop, ptdate, btdate, susamt, effdate, acdes, osbal, apind, tolerance, optdsc, optind, xamt, manadj, totalfee, cmax, reqamt, hprjptdate};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, registerOut, chdrstatusOut, premstatusOut, cownnumOut, ownernameOut, lifenumOut, lifenameOut, occdateOut, billfreqOut, mopOut, ptdateOut, btdateOut, susamtOut, effdateOut, acdesOut, osbalOut, apindOut, tolerOut, optdscOut, optindOut, xamtOut, manadjOut, totalfeeOut, cmaxOut, reqamtOut, hprjptdateOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, registerErr, chdrstatusErr, premstatusErr, cownnumErr, ownernameErr, lifenumErr, lifenameErr, occdateErr, billfreqErr, mopErr, ptdateErr, btdateErr, susamtErr, effdateErr, acdesErr, osbalErr, apindErr, tolerErr, optdscErr, optindErr, xamtErr, manadjErr, totalfeeErr, cmaxErr, reqamtErr, hprjptdateErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate, hprjptdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr, hprjptdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp, hprjptdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr51mscreen.class;
		screenSflRecord = Sr51mscreensfl.class;
		screenCtlRecord = Sr51mscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr51mprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr51mscreenctl.lrec.pageSubfile);
	}
}
