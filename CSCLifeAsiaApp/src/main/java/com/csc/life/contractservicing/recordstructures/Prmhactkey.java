package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:04
 * Description:
 * Copybook name: PRMHACTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Prmhactkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData prmhactFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData prmhactKey = new FixedLengthStringData(256).isAPartOf(prmhactFileKey, 0, REDEFINE);
  	public FixedLengthStringData prmhactChdrcoy = new FixedLengthStringData(1).isAPartOf(prmhactKey, 0);
  	public FixedLengthStringData prmhactChdrnum = new FixedLengthStringData(8).isAPartOf(prmhactKey, 1);
  	public PackedDecimalData prmhactTranno = new PackedDecimalData(5, 0).isAPartOf(prmhactKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(prmhactKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(prmhactFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		prmhactFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}