package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.RpdetpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rpdetpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RpdetpfDAOImpl extends BaseDAOImpl<Rpdetpf> implements RpdetpfDAO  {

	private static final Logger LOGGER = LoggerFactory.getLogger(RplnpfDAOImpl.class);

		@Override
		public void insertRpdetRecord(List<Rpdetpf> rpdetpfList) {

			StringBuilder sqlInsert = new StringBuilder();

			sqlInsert.append("insert into RPDETPF(LOANNUM,CHDRNUM,CHDRCOY,CHDRPFX,REPAYAMT,REPAYSTATUS,APPROVALDATE,REPAYMETHOD,TRANNO,USERID,JOBNM,DATIME,LOANTYPE,EFFTDATE,LOANCURR,INTEREST,PRINAMNT,CURRAMNT,ACCRDINT,PENDINT,STATUS)");

			sqlInsert.append("values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			PreparedStatement psRpdetpfInsert = getPrepareStatement(sqlInsert.toString());
			int index ;
			
			try {

				for (Rpdetpf rpdet : rpdetpfList) {
					index = 1;
					psRpdetpfInsert.setInt(index++, rpdet.getLoannum());
					psRpdetpfInsert.setString(index++, rpdet.getChdrnum());
					psRpdetpfInsert.setString(index++, rpdet.getChdrcoy());
					psRpdetpfInsert.setString(index++, rpdet.getChdrpfx());
					psRpdetpfInsert.setDouble(index++, rpdet.getRepayamt());
					psRpdetpfInsert.setString(index++, rpdet.getRepaystatus());
					psRpdetpfInsert.setInt(index++, rpdet.getApprovaldate());			
					psRpdetpfInsert.setString(index++, rpdet.getRepaymethod());
					psRpdetpfInsert.setInt(index++, rpdet.getTranno());		
					psRpdetpfInsert.setString(index++, getUsrprf());
					psRpdetpfInsert.setString(index++, getJobnm());			
					psRpdetpfInsert.setTimestamp(index++, new Timestamp(System.currentTimeMillis()));	
					psRpdetpfInsert.setString(index++, rpdet.getLoantype());
					psRpdetpfInsert.setInt(index++, rpdet.getEfftdate());		
					psRpdetpfInsert.setString(index++, rpdet.getLoancurr());
					psRpdetpfInsert.setInt(index++, rpdet.getInterest());
					psRpdetpfInsert.setDouble(index++, rpdet.getPrinamnt());
					psRpdetpfInsert.setDouble(index++, rpdet.getCurramnt());
					psRpdetpfInsert.setDouble(index++, rpdet.getAccrdint());
					psRpdetpfInsert.setDouble(index++, rpdet.getPendint());
					psRpdetpfInsert.setString(index++, rpdet.getStatus());
				
					psRpdetpfInsert.addBatch();

				}
				psRpdetpfInsert.executeBatch();

				LOGGER.debug("insertRpdetRecord {} rows inserted.", rpdetpfList.size());//IJTI-1561

			} catch (SQLException e) {
				LOGGER.error("insertRpdetRecord()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psRpdetpfInsert, null);
			}
		}

		@Override
		public void updateRpdetRecordList(List<Rpdetpf> rpdetpfUpList) {
			
			StringBuilder updateString = new StringBuilder("UPDATE RPDETPF SET REPAYSTATUS=?,APPROVALDATE=?,STATUS=?,REPAYAMT=?,TRANNO=?,USERID=?,JOBNM=?,DATIME=?");
			updateString.append(" WHERE CHDRNUM=? AND LOANNUM=?  AND STATUS='P' "); 			
			PreparedStatement psUpdate = null;
			ResultSet rs = null;
			try{
				psUpdate = getPrepareStatement(updateString.toString());
				for (Rpdetpf rpdet : rpdetpfUpList) {	
					psUpdate.setString(1,rpdet.getRepaystatus());/* IJTI-1523 */
					psUpdate.setInt(2, rpdet.getApprovaldate());
					psUpdate.setString(3, rpdet.getStatus());
					psUpdate.setDouble(4, rpdet.getRepayamt());
					psUpdate.setInt(5, rpdet.getTranno());
					psUpdate.setString(6, getUsrprf());			    
					psUpdate.setString(7, getJobnm());		
					psUpdate.setTimestamp(8, getDatime());
					psUpdate.setString(9, rpdet.getChdrnum());					
					psUpdate.setInt(10, rpdet.getLoannum());
										
					psUpdate.addBatch();	
				}			
				psUpdate.executeBatch();
			}catch (SQLException e) {
				LOGGER.error("updateRpdetRecordList()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psUpdate, rs);			
			}		
			
		}
		
		@Override
		public List<Rpdetpf> getRpdetRecord(String chdrnum, int loannum) {
			
			Rpdetpf rpdetpf = null;
			String sql_select = "SELECT REPAYAMT,REPAYSTATUS,REPAYMETHOD,APPROVALDATE FROM RPDETPF WHERE CHDRNUM= ? AND LOANNUM= ? AND  STATUS='A' ORDER BY APPROVALDATE ASC";

			ResultSet rs = null;
			PreparedStatement psSelect = null;
			 List<Rpdetpf> rpdetpfList = new ArrayList<Rpdetpf>();
			try {

				psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
				psSelect.setString(1, chdrnum);
				psSelect.setInt(2, loannum);

				rs = psSelect.executeQuery();
				while (rs.next()) {
					rpdetpf = new Rpdetpf();				
					rpdetpf.setRepayamt(rs.getDouble(1));
					rpdetpf.setRepaystatus(rs.getString(2));
					rpdetpf.setRepaymethod(rs.getString(3));
					rpdetpf.setApprovaldate(rs.getInt(4));
					rpdetpfList.add(rpdetpf);
				}

			} catch (SQLException e) {
				LOGGER.error("getRpdetRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psSelect, rs);
			}

			return rpdetpfList;
		}

		@Override
		public List<Rpdetpf> getRpdetRecordList(String chdrnum, int tranno) {
			
			Rpdetpf rpdetpf = null;
			StringBuilder selectRpdetpf = new StringBuilder("");
			selectRpdetpf.append("SELECT * FROM RPDETPF WHERE CHDRNUM= ? AND TRANNO= ? ");
			selectRpdetpf.append("ORDER BY  LOANNUM ASC ");
			ResultSet rs = null;
			PreparedStatement psSelect = null;
			 List<Rpdetpf> rpdetpfRcdList = new ArrayList<Rpdetpf>();
			try {

				psSelect = getPrepareStatement(selectRpdetpf.toString());
				psSelect.setString(1, chdrnum.trim());
				psSelect.setInt(2, tranno);

				rs = psSelect.executeQuery();
				while (rs.next()) {
					rpdetpf = new Rpdetpf();	
				
					rpdetpf.setLoannum(rs.getInt("LOANNUM")); 
					rpdetpf.setChdrnum(rs.getString("CHDRNUM"));
					rpdetpf.setChdrcoy(rs.getString("CHDRCOY"));
					rpdetpf.setChdrpfx(rs.getString("CHDRPFX"));
					rpdetpf.setRepayamt(rs.getDouble("REPAYAMT"));
					rpdetpf.setRepaystatus(rs.getString("REPAYSTATUS"));
					rpdetpf.setApprovaldate(rs.getInt("APPROVALDATE"));
					rpdetpf.setRepaymethod(rs.getString("REPAYMETHOD"));
					rpdetpf.setTranno(rs.getInt("TRANNO"));
					rpdetpf.setLoantype(rs.getString("LOANTYPE"));
					rpdetpf.setEfftdate(rs.getInt("EFFTDATE"));
					rpdetpf.setLoancurr(rs.getString("LOANCURR"));
					rpdetpf.setInterest(rs.getInt("INTEREST"));
					rpdetpf.setPrinamnt(rs.getDouble("PRINAMNT"));
					rpdetpf.setCurramnt(rs.getDouble("CURRAMNT"));
					rpdetpf.setAccrdint(rs.getDouble("ACCRDINT"));
					rpdetpf.setPendint(rs.getDouble("PENDINT"));	
					rpdetpf.setStatus(rs.getString("STATUS"));	
				
					rpdetpfRcdList.add(rpdetpf);
				}

			} catch (SQLException e) {
				LOGGER.error("getRpdetRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(psSelect, rs);
			}

			return rpdetpfRcdList;
		}

		@Override
		public void deleteRpdetpf(String chdrnum, int tranno) {
			 StringBuilder sql_delete = new StringBuilder("DELETE FROM RPDETPF WHERE CHDRNUM=? AND TRANNO=?");

		        PreparedStatement psDelete = getPrepareStatement(sql_delete.toString());
		        ResultSet rs = null;
		        try {
		        	psDelete.setString(1, chdrnum.trim());
		        	psDelete.setInt(2, tranno);
		          
		        	psDelete.execute();
		          
		        } catch (SQLException e) {
		            LOGGER.error("deleteRplnpf()", e);//IJTI-1561
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psDelete, rs);
		        }
			
		}
		@Override
		public void updateRpdetpftrannoRecord(Rpdetpf rpdetpf) {

			StringBuilder updaterpdetrecord = new StringBuilder(
					"UPDATE RPDETPF SET REPAYSTATUS=?,APPROVALDATE=?,REPAYAMT=?,STATUS=? WHERE CHDRNUM=? AND TRANNO=?");

			PreparedStatement rpdetpfUpdate = getPrepareStatement(updaterpdetrecord.toString());
			try {									
				rpdetpfUpdate.setString(1, rpdetpf.getRepaystatus());
				rpdetpfUpdate.setInt(2, rpdetpf.getApprovaldate());
				rpdetpfUpdate.setDouble(3, rpdetpf.getRepayamt());
				rpdetpfUpdate.setString(4, rpdetpf.getStatus());
				rpdetpfUpdate.setString(5, rpdetpf.getChdrnum());
				rpdetpfUpdate.setInt(6, rpdetpf.getTranno());

				rpdetpfUpdate.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateRpdetpftrannoRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				try {
					rpdetpfUpdate.close();
				} catch (SQLException e) {
					LOGGER.error("Exception occured:-",e);
				}
			}

		}


	}

