/*
 * File: P6633.java
 * Date: 30 August 2009 0:47:34
 * Author: Quipoz Limited
 * 
 * Class transformed from P6633.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.screens.S6633ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.csc.util.SmartTableUtility;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
 *
 *
 *****************************************************************
 * </pre>
 */
public class P6633 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6633");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	/* Logical File: Extra data screen */
	private DescTableDAM descIO = new DescTableDAM();
	/* Dated items by from date */
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T6633rec t6633rec = new T6633rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6633ScreenVars sv = ScreenProgram.getScreenVars(S6633ScreenVars.class);

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	boolean cslnd004Permission = false;  //ICIL-549

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, generalArea1045, preExit, exit2090, exit3090
	}

	public P6633() {
		super();
		screenVars = sv;
		new ScreenModel("S6633", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void initialise1010() {
		/* INITIALISE-SCREEN */
		sv.dataArea.set(SPACES);
		/* READ-PRIMARY-RECORD */
		/* READ-RECORD */
		//ICIL-549 start
		cslnd004Permission = FeaConfg.isFeatureExist("2", "CSLND004", appVars, "IT");
		if(!cslnd004Permission) {
			sv.nofDayOut[varcom.nd.toInt()].set("Y");
			sv.nofDayOut[varcom.pr.toInt()].set("Y");
		}	
		//ICIL-549 end
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/* READ-SECONDARY-RECORDS */
	}

	protected void readRecord1031() {
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

	protected void moveToScreen1040() {
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t6633rec.t6633Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t6633rec.day.set(ZERO);
		t6633rec.interestDay.set(ZERO);
		t6633rec.intRate.set(ZERO);
		t6633rec.nofDay.set(ZERO);
		t6633rec.mperiod.set(ZERO);
	}

	protected void generalArea1045() {
		sv.loanAnnivInterest.set(t6633rec.loanAnnivInterest);
		sv.policyAnnivInterest.set(t6633rec.policyAnnivInterest);
		sv.annloan.set(t6633rec.annloan);
		sv.annpoly.set(t6633rec.annpoly);
		sv.compfreq.set(t6633rec.compfreq);
		sv.day.set(t6633rec.day);
		sv.interestDay.set(t6633rec.interestDay);
		sv.interestFrequency.set(t6633rec.interestFrequency);
		sv.intRate.set(t6633rec.intRate);
		sv.nofDay.set(t6633rec.nofDay);
		sv.inttype.set(t6633rec.inttype);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		} else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		} else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.mperiod.set(t6633rec.mperiod);
	}

	protected void preScreenEdit() {
		try {
			preStart();
		} catch (GOTOException e) {
		}
	}

	protected void preStart() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

	protected void screenEdit2000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(varcom.oK);
		/* VALIDATE */
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/* OTHER */
	}

	protected void exit2090() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* EXIT */
	}

	protected void update3000() {
		try {
			preparation3010();
			updatePrimaryRecord3050();
			updateRecord3055();
		} catch (GOTOException e) {
		}
	}

	protected void preparation3010() {
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
	}

	protected void updatePrimaryRecord3050() { // IJTI-327 STARTS
		/*
		 * itmdIO.setFunction(varcom.readh);
		 * itmdIO.setDataKey(wsspsmart.itmdkey); SmartFileCode.execute(appVars,
		 * itmdIO); if (isNE(itmdIO.getStatuz(),varcom.oK)) {
		 * syserrrec.params.set(itmdIO.getParams()); fatalError600(); }
		 */
		// IJTI-327 ENDS
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

	protected void updateRecord3055() {
		checkChanges3100();
		itmdIO.setItemTableprog(wsaaProg);
		// IJTI-327 STARTS
		/*
		 * itmdIO.setItemGenarea(t6633rec.t6633Rec);
		 * itmdIO.setFunction(varcom.rewrt); SmartFileCode.execute(appVars,
		 * itmdIO); if (isNE(itmdIO.getStatuz(),varcom.oK)) {
		 * syserrrec.params.set(itmdIO.getParams()); fatalError600(); }
		 */
		/* OTHER */
		Itempf itempf = SmartTableUtility.getItempfByItmdTableDAM(itmdIO);
		itempf.setGenarea(t6633rec.t6633Rec.toString().getBytes());
		itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
				.getGENAREAJString(t6633rec.t6633Rec.toString().getBytes(), t6633rec));
		itemDAO.updateSmartTableItem(itempf);
		// IJTI-327 ENDS
	}
	

	protected void checkChanges3100() {
		check3100();
	}

	protected void check3100() {
		if (isNE(sv.loanAnnivInterest, t6633rec.loanAnnivInterest)) {
			t6633rec.loanAnnivInterest.set(sv.loanAnnivInterest);
		}
		if (isNE(sv.policyAnnivInterest, t6633rec.policyAnnivInterest)) {
			t6633rec.policyAnnivInterest.set(sv.policyAnnivInterest);
		}
		if (isNE(sv.annloan, t6633rec.annloan)) {
			t6633rec.annloan.set(sv.annloan);
		}
		if (isNE(sv.annpoly, t6633rec.annpoly)) {
			t6633rec.annpoly.set(sv.annpoly);
		}
		if (isNE(sv.compfreq, t6633rec.compfreq)) {
			t6633rec.compfreq.set(sv.compfreq);
		}
		if (isNE(sv.day, t6633rec.day)) {
			t6633rec.day.set(sv.day);
		}
		if (isNE(sv.interestDay, t6633rec.interestDay)) {
			t6633rec.interestDay.set(sv.interestDay);
		}
		if (isNE(sv.interestFrequency, t6633rec.interestFrequency)) {
			t6633rec.interestFrequency.set(sv.interestFrequency);
		}
		if (isNE(sv.intRate, t6633rec.intRate)) {
			t6633rec.intRate.set(sv.intRate);
		}
		if (isNE(sv.nofDay, t6633rec.nofDay)) {
			t6633rec.nofDay.set(sv.nofDay);
		}
		if (isNE(sv.inttype, t6633rec.inttype)) {
			t6633rec.inttype.set(sv.inttype);
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
		}
		if (isNE(sv.mperiod, t6633rec.mperiod)) {
			t6633rec.mperiod.set(sv.mperiod);
		}
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}
}
