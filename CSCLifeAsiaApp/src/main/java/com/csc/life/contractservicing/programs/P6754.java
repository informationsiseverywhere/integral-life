/*
 * File: P6754.java
 * Date: 30 August 2009 0:55:59
 * Author: Quipoz Limited
 *
 * Class transformed from P6754.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CwfdTableDAM;
import com.csc.life.contractservicing.dataaccess.CwfdprcTableDAM;
//ILIFE-1254 - STARTS
//import com.csc.life.contractservicing.dataaccess.PtrnwfdTableDAM;
//ILIFE-1254 - ENDS
import com.csc.life.contractservicing.dataaccess.PtrnwfdtTableDAM;
import com.csc.life.contractservicing.tablestructures.T6757rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Errmesg;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*                      Windforward Register
*                      --------------------
*
*  This is a non-screen program which is accessed from the Insert
*  Transaction screen or following a windforward selection on the
*  Windforward Registration screen.
*
*  The program performs all of the necessary processing to add a
*  Windforward record to a contract.
*
*  The program reads the Windforward Transactions table  to  get
*  the schedules for submission for this combination of contract
*  type and transaction.
*
*  For each schedule on the table, the  program  checks  whether
*  the schedule exists and if the  user  is  sanctioned  to that
*  schedule, preventing  the  transaction from continuing if any
*  of the validation checks fail.
*
*  Following successful  validation,  one  Contract  Windforward
*  record is written for every schedule defined on the table.
*
*  If no Contract Windforward  records existed for this contract
*  prior to this transaction,  the  program  will  write  a  new
*  Contract Header record with a  changed  status to reflect the
*  fact that the contract is pending windforward.  A transaction
*  record is also  written  to  record  the Windforward Register
*  transaction.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6754 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6754");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaPrcSeqNbr = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private String wsaaInsert = "";
	private String wsaaUpdateChdr = "";

	private FixedLengthStringData wsaaConkey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaContractType = new FixedLengthStringData(3).isAPartOf(wsaaConkey, 0);
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaConkey, 3);
	private String wsaaValid = "";

	private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(37);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaMsgarea, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsgVariable = new FixedLengthStringData(10).isAPartOf(wsaaMsgarea, 1);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaMsgarea, 11, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsg = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 12);
		/* ERRORS */
	private String e011 = "E011";
	private String e045 = "E045";
	private String f084 = "F084";
	private String f085 = "F085";
		/* FORMATS */
	private String bscdrec = "BSCDREC";
	private String chdrenqrec = "CHDRENQREC";
	private String chdrmjarec = "CHDRMJAREC";
	private String cwfdprcrec = "CWFDPRCREC";
	private String cwfdrec = "CWFDREC";
	private String itemrec = "ITEMREC";
	private String ptrnrec = "PTRNREC";
	//ILIFE-1254 - STARTS
	//private String ptrnwfdrec = "PTRNWFDREC";
	private String ptrnwfdtrec = "PTRNWFDTREC";
	//ILIFE-1254 - ENDS
		/* TABLES */
	private String t5679 = "T5679";
	private String t6757 = "T6757";
		/*Schedule Definition - Multi Thread Batch*/
	private BscdTableDAM bscdIO = new BscdTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Contract Windforward Logical*/
	private CwfdTableDAM cwfdIO = new CwfdTableDAM();
		/*Contract Windforward logical by ProcSeqN*/
	private CwfdprcTableDAM cwfdprcIO = new CwfdprcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Errmesgrec errmesgrec = new Errmesgrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*PTRN Logical File for windforward*/
	//ILIFE-1254 - STARTS
	//private PtrnwfdTableDAM ptrnwfdIO = new PtrnwfdTableDAM();
	private PtrnwfdtTableDAM ptrnwfdtIO = new PtrnwfdtTableDAM();
	//ILIFE-1254 - ENDS
	private Sanctnrec sanctnrec = new Sanctnrec();
	private T5679rec t5679rec = new T5679rec();
	private T6757rec t6757rec = new T6757rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1190,
		exit3090,
		exit3190
	}

	public P6754() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaInsert = "N";
		wsaaTranno.set(ZERO);
		chdrenqIO.setFunction(varcom.retrv);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.rlse);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		wsaaConkey.set(wssplife.keysoptItem);
		wsaaBatckey.set(wsspcomn.batchkey);
		retrvPtrn1100();
	}

protected void retrvPtrn1100()
	{
		try {
			retrv1110();
		}
		catch (GOTOException e){
		}
	}
//ILIFE-1254 - STARTS
protected void retrv1110()
	{
		ptrnwfdtIO.setFunction(varcom.retrv);
		ptrnwfdtIO.setFormat(ptrnwfdtrec);
		SmartFileCode.execute(appVars, ptrnwfdtIO);
		if (isNE(ptrnwfdtIO.getStatuz(),varcom.oK)
		&& isNE(ptrnwfdtIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(ptrnwfdtIO.getStatuz());
			syserrrec.params.set(ptrnwfdtIO.getParams());
			fatalError600();
		}
		if (isEQ(ptrnwfdtIO.getStatuz(),varcom.mrnf)) {
			wsaaInsert = "Y";
			goTo(GotoLabel.exit1190);
		}
		ptrnwfdtIO.setFunction(varcom.rlse);
		ptrnwfdtIO.setFormat(ptrnwfdtrec);
		SmartFileCode.execute(appVars, ptrnwfdtIO);
		if (isNE(ptrnwfdtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnwfdtIO.getParams());
			syserrrec.statuz.set(ptrnwfdtIO.getStatuz());
			fatalError600();
		}
		wsaaTranno.set(ptrnwfdtIO.getTranno());
	}
//ILIFE-1254 - ENDS
protected void screenEdit2000()
	{
		/*SCR-EDIT*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			update3010();
		}
		catch (GOTOException e){
		}
	}

protected void update3010()
	{
		wsaaValid = "Y";
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6757);
		itdmIO.setItemitem(wsaaConkey);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6757)
		|| isNE(itdmIO.getItemitem(),wsaaConkey)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t6757rec.t6757Rec.set(itdmIO.getGenarea());
		bscdIO.setParams(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub,8)
		|| isEQ(t6757rec.jobnameJob[wsaaSub.toInt()],SPACES)); wsaaSub.add(1)){
			checkSchedule3100();
		}
		if (isEQ(wsaaValid,"N")) {
			goTo(GotoLabel.exit3090);
		}
		processSeqNumber3200();
	}

protected void checkSchedule3100()
	{
		try {
			read3110();
		}
		catch (GOTOException e){
		}
	}

protected void read3110()
	{
		bscdIO.setScheduleName(t6757rec.jobnameJob[wsaaSub.toInt()]);
		bscdIO.setFunction(varcom.readr);
		bscdIO.setFormat(bscdrec);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(),varcom.oK)
		&& isNE(bscdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(bscdIO.getParams());
			syserrrec.statuz.set(bscdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(bscdIO.getStatuz(),varcom.mrnf)) {
			errmesgrec.eror.set(f084);
			wsaaMsgVariable.set(t6757rec.jobnameJob[wsaaSub.toInt()]);
			formatError3700();
			goTo(GotoLabel.exit3190);
		}
		sanctnrec.sanctnRec.set(SPACES);
		sanctnrec.company.set("*");
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.function.set("TNCD");
		sanctnrec.transcd.set(bscdIO.getAuthCode());
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isNE(sanctnrec.statuz,varcom.oK)
		&& isNE(sanctnrec.statuz,e011)
		&& isNE(sanctnrec.statuz,e045)) {
			syserrrec.params.set(bscdIO.getParams());
			syserrrec.statuz.set(bscdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(sanctnrec.statuz,e011)
		|| isEQ(sanctnrec.statuz,e045)) {
			errmesgrec.eror.set(f085);
			wsaaMsgVariable.set(bscdIO.getScheduleName());
			formatError3700();
		}
	}

protected void processSeqNumber3200()
	{
		begn3210();
		start3310();
	}

protected void begn3210()
	{
		cwfdprcIO.setDataArea(SPACES);
		cwfdprcIO.setChdrnum(chdrenqIO.getChdrnum());
		cwfdprcIO.setChdrcoy(chdrenqIO.getChdrcoy());
		cwfdprcIO.setPrcSeqNbr(99999);
		cwfdprcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		cwfdprcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		cwfdprcIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		cwfdprcIO.setFormat(cwfdprcrec);
		SmartFileCode.execute(appVars, cwfdprcIO);
		if (isNE(cwfdprcIO.getStatuz(),varcom.oK)
		&& isNE(cwfdprcIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(cwfdprcIO.getStatuz());
			syserrrec.params.set(cwfdprcIO.getParams());
			fatalError600();
		}
		if (isEQ(cwfdprcIO.getStatuz(),varcom.endp)
		|| isNE(cwfdprcIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isNE(cwfdprcIO.getChdrcoy(),chdrenqIO.getChdrcoy())) {
			wsaaPrcSeqNbr.set(0);
			wsaaUpdateChdr = "Y";
		}
		else {
			wsaaPrcSeqNbr.set(cwfdprcIO.getPrcSeqNbr());
			wsaaUpdateChdr = "N";
		}
	}

protected void start3310()
	{
		for (wsaaSub.set(1); !(isGT(wsaaSub,8)
		|| isEQ(t6757rec.jobnameJob[wsaaSub.toInt()],SPACES)); wsaaSub.add(1)){
			writeCwfd3400();
		}
		if (isEQ(wsaaUpdateChdr,"Y")) {
			updateContract3500();
		}
		/*EXIT*/
	}

protected void writeCwfd3400()
	{
		writr3410();
	}

protected void writr3410()
	{
		cwfdIO.setDataArea(SPACES);
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setEfdate(varcom.vrcmMaxDate);
		cwfdIO.setChdrcoy(chdrenqIO.getChdrcoy());
		cwfdIO.setChdrnum(chdrenqIO.getChdrnum());
		cwfdIO.setTrancd(wsaaTranCode);
		cwfdIO.setTranno(wsaaTranno);
		if (isEQ(wsaaInsert,"Y")) {
			cwfdIO.setEfdate(wssplife.effdate);
		}
		//ILIFE-1254 - STARTS
		else {
			cwfdIO.setEfdate(ptrnwfdtIO.getDatesub());
		}
		//ILIFE-1254 - ENDS
		wsaaPrcSeqNbr.add(1);
		cwfdIO.setPrcSeqNbr(wsaaPrcSeqNbr);
		cwfdIO.setScheduleName(t6757rec.jobnameJob[wsaaSub.toInt()]);
		cwfdIO.setJobnameNumber(ZERO);
		cwfdIO.setFunction(varcom.writr);
		cwfdIO.setFormat(cwfdrec);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cwfdIO.getParams());
			syserrrec.statuz.set(cwfdIO.getStatuz());
			fatalError600();
		}
	}

protected void updateContract3500()
	{
		update3510();
	}

protected void update3510()
	{
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(chdrenqIO.getChdrcoy());
		chdrmjaIO.setChdrnum(chdrenqIO.getChdrnum());
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		chdrmjaIO.setCurrto(wsaaToday);
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		chdrmjaIO.setValidflag("1");
		chdrmjaIO.setCurrfrom(wsaaToday);
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		writePtrn3600();
	}

protected void writePtrn3600()
	{
		ptrn3610();
	}

protected void ptrn3610()
	{
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setChdrcoy(chdrenqIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrenqIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setValidflag("1");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setCrtuser(wsspcomn.userid);  //PINNACLE-2931
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError600();
		}
	}

protected void formatError3700()
	{
		call3710();
	}

protected void call3710()
	{
		errmesgrec.company.set(wsspcomn.company);
		errmesgrec.language.set(wsspcomn.language);
		errmesgrec.erorProg.set(wsaaProg);
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		if (isNE(errmesgrec.statuz,varcom.oK)) {
			syserrrec.params.set(errmesgrec.errmesgRec);
			syserrrec.statuz.set(errmesgrec.statuz);
			fatalError600();
		}
		wsaaMsg.set(errmesgrec.errmesg[1]);
		wsspcomn.msgarea.set(wsaaMsgarea);
		wsaaValid = "N";
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(wsaaValid,"Y")) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.programPtr.subtract(1);
			chdrenqIO.setFunction("KEEPS");
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				fatalError600();
			}
		}
		/*EXIT*/
	}
}
