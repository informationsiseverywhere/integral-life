package com.csc.life.contractservicing.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:01
 * Description:
 * Copybook name: INTCALCREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Intcalcrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData intcalcRec = new FixedLengthStringData(65);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(intcalcRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(intcalcRec, 5);
  	public ZonedDecimalData loanNumber = new ZonedDecimalData(2, 0).isAPartOf(intcalcRec, 9).setUnsigned();
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(intcalcRec, 11);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(intcalcRec, 12);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(intcalcRec, 20);
  	public PackedDecimalData interestFrom = new PackedDecimalData(8, 0).isAPartOf(intcalcRec, 23);
  	public PackedDecimalData interestTo = new PackedDecimalData(8, 0).isAPartOf(intcalcRec, 28);
  	public PackedDecimalData loanorigam = new PackedDecimalData(17, 2).isAPartOf(intcalcRec, 33);
  	public PackedDecimalData lastCaplsnDate = new PackedDecimalData(8, 0).isAPartOf(intcalcRec, 42);
  	public PackedDecimalData loanStartDate = new PackedDecimalData(8, 0).isAPartOf(intcalcRec, 47);
  	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2).isAPartOf(intcalcRec, 52);
  	public FixedLengthStringData loanCurrency = new FixedLengthStringData(3).isAPartOf(intcalcRec, 61);
  	public FixedLengthStringData loanType = new FixedLengthStringData(1).isAPartOf(intcalcRec, 64);


	public void initialize() {
		COBOLFunctions.initialize(intcalcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		intcalcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}