package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:26
 * Description:
 * Copybook name: ULNKREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ulnkrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ulnkrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ulnkrevKey = new FixedLengthStringData(64).isAPartOf(ulnkrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData ulnkrevChdrcoy = new FixedLengthStringData(1).isAPartOf(ulnkrevKey, 0);
  	public FixedLengthStringData ulnkrevChdrnum = new FixedLengthStringData(8).isAPartOf(ulnkrevKey, 1);
  	public FixedLengthStringData ulnkrevLife = new FixedLengthStringData(2).isAPartOf(ulnkrevKey, 9);
  	public FixedLengthStringData ulnkrevCoverage = new FixedLengthStringData(2).isAPartOf(ulnkrevKey, 11);
  	public FixedLengthStringData ulnkrevRider = new FixedLengthStringData(2).isAPartOf(ulnkrevKey, 13);
  	public PackedDecimalData ulnkrevPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ulnkrevKey, 15);
  	public PackedDecimalData ulnkrevTranno = new PackedDecimalData(5, 0).isAPartOf(ulnkrevKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(ulnkrevKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ulnkrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ulnkrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}