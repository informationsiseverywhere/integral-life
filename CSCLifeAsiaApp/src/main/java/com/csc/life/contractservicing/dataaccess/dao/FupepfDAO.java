package com.csc.life.contractservicing.dataaccess.dao;

import java.util.List;

import com.csc.life.contractservicing.dataaccess.model.Fupepf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface FupepfDAO extends BaseDAO<Fupepf>{
	public List<Fupepf> readRecord(String chdrcoy, String chdrnum, int fupno);
	public void deleteRecords(String chdrcoy, String chdrnum, int fupno);
    public void insertRecord(Fupepf fupepf);
    public List<Fupepf> readRecord(Fupepf fupepf);
}
