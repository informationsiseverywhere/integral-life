package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.text.SimpleDateFormat;
import java.util.List;

import com.csc.life.contractservicing.recordstructures.Pd5klrec;
import com.csc.life.contractservicing.screens.Sd5klScreenVars;
import com.csc.life.contractservicing.tablestructures.Tr5lxrec;
import com.csc.smart.dataaccess.BparTableDAM;
import com.csc.smart.dataaccess.BppdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.model.Itempf;

public class Pd5kl extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pd5kl");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private static final String a123 = "A123";
	private static final String bscdrec = "BSCDREC";
	private static final String bsscrec = "BSSCREC";
	private static final String buparec = "BUPAREC";
	private static final String bppdrec = "BPPDREC";
	private static final String bparrec = "BPARREC";
	private BparTableDAM bparIO = new BparTableDAM();
	private BppdTableDAM bppdIO = new BppdTableDAM();
	private BscdTableDAM bscdIO = new BscdTableDAM();
	private BsscTableDAM bsscIO = new BsscTableDAM();
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sd5klScreenVars sv = new Sd5klScreenVars();
	private Pd5klrec pd5klrec = new Pd5klrec();
	private Tr5lxrec tr5lxrec = new Tr5lxrec();
	SimpleDateFormat integralDateFormat = new SimpleDateFormat("yyyyMMdd");
	private FixedLengthStringData tr5lx = new FixedLengthStringData(5).init("TR5LX");
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private List<Itempf> itempfList;
	private boolean itemFound = false;
	
	public Pd5kl() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5kl", AppVars.getInstance(), sv);
	}
	
	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	@Override
	protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Retrieve Schedule.*/
		bsscIO.setFormat(bsscrec);
		bsscIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		/* Retrieve Schedule Definition.*/
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		/* Retrieve Parameter Prompt Definition.*/
		bppdIO.setFormat(bppdrec);
		bppdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bppdIO);
		if (isNE(bppdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bppdIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.scheduleName.set(bsscIO.getScheduleName());
		sv.scheduleNumber.set(bsscIO.getScheduleNumber());
		sv.effdate.set(bsscIO.getEffectiveDate());
		sv.acctmonth.set(bsscIO.getAcctMonth());
		sv.acctyear.set(bsscIO.getAcctYear());
		sv.jobq.set(bscdIO.getJobq());
		sv.bcompany.set(bppdIO.getCompany());
		sv.bbranch.set(bsscIO.getInitBranch());
		getTaxPeriod();
		if (isNE(wsspcomn.flag, "I")
		&& isNE(wsspcomn.flag, "M")) {
			return ;
		}
		bparIO.setParams(SPACES);
		bparIO.setScheduleName(bsscIO.getScheduleName());
		bparIO.setCompany(bppdIO.getCompany());
		bparIO.setParmPromptProg(wsaaProg);
		bparIO.setBruntype(subString(wsspcomn.inqkey, 1, 8));
		bparIO.setBrunoccur(subString(wsspcomn.inqkey, 9, 3));
		bparIO.setEffectiveDate(bsscIO.getEffectiveDate());
		bparIO.setAcctmonth(bsscIO.getAcctMonth());
		bparIO.setAcctyear(bsscIO.getAcctYear());
		bparIO.setFunction(varcom.readr);
		bparIO.setFormat(bparrec);
		SmartFileCode.execute(appVars, bparIO);
		if (isNE(bparIO.getStatuz(), varcom.oK)
		&& isNE(bparIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(bparIO.getStatuz());
			syserrrec.params.set(bparIO.getParams());
			fatalError600();
		}
		if (isEQ(bparIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(a123);
			return ;
		}
		pd5klrec.parmRecord.set(bparIO.getParmarea());
		sv.chdrnum.set(pd5klrec.chdrnum);
		sv.chdrnum1.set(pd5klrec.chdrnum1);
	}

	protected void getTaxPeriod() {
		itempfList = itempfDAO.findItemByTable("IT", bppdIO.getCompany().toString(), tr5lx.toString(), "CTAX", "1");
		if(itempfList != null && !itempfList.isEmpty()) {
			for(Itempf itempf: itempfList) {
				if(itempf.getItmfrm() <= bsscIO.getEffectiveDate().toInt() && itempf.getItmto() >= bsscIO.getEffectiveDate().toInt()) {
					tr5lxrec.tr5lxRec.set(StringUtil.rawToString(itempf.getGenarea()));
					sv.taxFrmYear.set(tr5lxrec.taxPeriodFrm);
					sv.taxToYear.set(tr5lxrec.taxPeriodTo);
				}
			}
		}
	}

	@Override
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
	}
	
	@Override
	protected void screenEdit2000()
	{
		screenIo2010();
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(varcom.oK);
		if(isEQ(sv.taxFrmYear, 0)) {
			sv.taxFrmYearErr.set("E186");
			wsspcomn.edterror.set("Y");
			return;
		}
		if(isEQ(sv.taxToYear, 0)) {
			sv.taxToYearErr.set("E186");
			wsspcomn.edterror.set("Y");
			return;
		}
		validateTaxYear();
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
	}
	
	protected void validateTaxYear()
	{
		for(Itempf itempf: itempfList) {
			tr5lxrec.tr5lxRec.set(StringUtil.rawToString(itempf.getGenarea()));
			if(sv.taxFrmYear.toInt() == tr5lxrec.taxPeriodFrm.toInt() && sv.taxToYear.toInt() == tr5lxrec.taxPeriodTo.toInt()) {
				itemFound = true;
				break;
			}
		}
		if(!itemFound) {
			sv.taxFrmYearErr.set("RRNV");
			sv.taxToYearErr.set("RRNV");
			wsspcomn.edterror.set("Y");
		}
	}
	@Override
	protected void update3000() {
		/*LOAD-FIELDS*/
		moveParameters3100();
		writeParamRecord3200();
		/*EXIT*/
	}

	protected void moveParameters3100() {
		/*MOVE-TO-PARM-RECORD*/
		pd5klrec.chdrnum.set(sv.chdrnum);
		pd5klrec.chdrnum1.set(sv.chdrnum1);
		pd5klrec.taxFrmYear.set(sv.taxFrmYear);
		pd5klrec.taxToYear.set(sv.taxToYear);
		/*EXIT*/
	}

	protected void writeParamRecord3200() {
		writBupa3210();
	}

	protected void writBupa3210() {
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(sv.scheduleName);
		bupaIO.setScheduleNumber(sv.scheduleNumber);
		bupaIO.setEffectiveDate(sv.effdate);
		bupaIO.setAcctMonth(sv.acctmonth);
		bupaIO.setAcctYear(sv.acctyear);
		bupaIO.setCompany(sv.bcompany);
		bupaIO.setBranch(sv.bbranch);
		bupaIO.setParmarea(pd5klrec.parmRecord);
		bupaIO.setParmPromptProg(bppdIO.getParmPromptProg());
		bupaIO.setFormat(buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
	}

	@Override
	protected void whereNext4000() {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}
}
