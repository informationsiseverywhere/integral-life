package com.csc.life.contractservicing.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZrrapfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:02
 * Class transformed from ZRRAPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZrrapfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 83;
	public FixedLengthStringData zrrarec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zrrapfRecord = zrrarec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zrrarec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zrrarec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zrrarec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zrrarec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zrrarec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(zrrarec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zrrarec);
	public PackedDecimalData instfrom = DD.instfrom.copy().isAPartOf(zrrarec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(zrrarec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(zrrarec);
	public PackedDecimalData moniesDate = DD.moniesdt.copy().isAPartOf(zrrarec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zrrarec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zrrarec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zrrarec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZrrapfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZrrapfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZrrapfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZrrapfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrrapfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZrrapfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrrapfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZRRAPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"TRANNO, " +
							"INSTFROM, " +
							"BILLFREQ, " +
							"BATCTRCDE, " +
							"MONIESDT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     tranno,
                                     instfrom,
                                     billfreq,
                                     batctrcde,
                                     moniesDate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		tranno.clear();
  		instfrom.clear();
  		billfreq.clear();
  		batctrcde.clear();
  		moniesDate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZrrarec() {
  		return zrrarec;
	}

	public FixedLengthStringData getZrrapfRecord() {
  		return zrrapfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZrrarec(what);
	}

	public void setZrrarec(Object what) {
  		this.zrrarec.set(what);
	}

	public void setZrrapfRecord(Object what) {
  		this.zrrapfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zrrarec.getLength());
		result.set(zrrarec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}