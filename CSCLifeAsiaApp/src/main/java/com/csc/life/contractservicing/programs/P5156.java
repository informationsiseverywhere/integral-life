/*
 * File: P5156.java
 * Date: 30 August 2009 0:16:57
 * Author: Quipoz Limited
 * 
 * Class transformed from P5156.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.procedures.Trcdechk;
import com.csc.life.contractservicing.screens.S5156ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tranchkrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifesurTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5156 - Full Surrender Reversal Selection
* -----------------------------------------
*
* Initialise
* ----------
*
* Clear the subfile ready for loading.
*
* The  details of the contract being worked with will have been
* stored  in  the  CHDRSUR I/O module. Retrieve the details and
* set up the header portion of the screen.
*
* Look up the following descriptions and names:
*
*   -  Contract Type, (CNTTYPE) - long description from T5688,
*   -  Contract  Status,  (STATCODE)  -  short description from
*      T3623,
*   -  Premium  Status,  (PSTATCODE)  -  short description from
*      T3588,
*   -  The owner's client (CLTS) details.
*   -  The joint  owner's  client (CLTS) details if they exist.
*
* Ensure that the last active transaction is a Full Surrender
* transaction by sequentially reading PTRNREV and ignoring
* non-significant transactions. Non-significant transactions
* are defined as those transaction having an entry in T6661
* with no subroutine specified and the reversal flag set to N.
*
* If the last active transaction is not a Full Surrender then
* no reversal request can be entertained, otherwise display
* the TRANNO and a  description of  the  transaction  to  be
* reversed.
*
* Load the subfile as follows:
*
*    Read  the  first  COVRSUR  record on the contract, using
*    zero  in  Plan  Suffix,  '01'  in  the  Life field and a
*    function of BEGN.
*
*    If the Plan Suffix from the returned record is zero then
*    add 1 to it before display. Obtain the long descriptions
*    for  the  coverage risk status, STATCODE, from T5682 and
*    the  premium  status  code,  PSTATCODE,  from  T5681 and
*    display them.
*
*    Check the transaction number TRANNO of the COVR record.
*    If it is the same as the TRANNO of the PTRNREV record,
*    then automatically select this COVR for reversal.
*    Otherwise, protect this COVR from selection.
*
*    Repeat  the  above  line  for as many times as there are
*    summarised policies, incrementing the Plan Suffix by one
*    for each subfile record written.
*
*    Store  the  life number and increment the Plan Suffix by
*    one.  Perform  a BEGN to obtain the first COVR record
*    for  the  next  Plan  Suffix.  Write  out its details as
*    before  and repeat the process until either the Company,
*    Contract Header Number or Life Number changes.
*
*    Load all pages  required  in  the subfile and set the
*    subfile more indicator to no.
*
* Updating
* --------
*
*      No Updating is required.
*
*
* Next Program
* ------------
*
* If there is only one policy in the plan, KEEPS the COVR
* record, call GENSSW with a function of 'A' and exit.
*
* Read through the subfile to find which record has been
* selected. Save the plan-suffix and perform a KEEPS on the
* COVR record. Call GENSSW with a function of 'A' and exit.
*
*****************************************************************
* </pre>
*/
public class P5156 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5156");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaMiscellaneous = new FixedLengthStringData(20);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaMiscellaneous, 0).setUnsigned();
	private ZonedDecimalData wsaaActualWritten = new ZonedDecimalData(3, 0).isAPartOf(wsaaMiscellaneous, 4);
	private PackedDecimalData wsaaCovrTimes = new PackedDecimalData(4, 0).isAPartOf(wsaaMiscellaneous, 7).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).isAPartOf(wsaaMiscellaneous, 10).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).isAPartOf(wsaaMiscellaneous, 12).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).isAPartOf(wsaaMiscellaneous, 14).setUnsigned();
	private ZonedDecimalData wsaaSavePlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaMiscellaneous, 16).setUnsigned();
	private String wsaaFirstTime = "";
	private FixedLengthStringData wsaaTransSurr = new FixedLengthStringData(4);
	private String wsaaTransErr = "";

	private FixedLengthStringData wsaaProcessingMsg = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaMsgProcess = new FixedLengthStringData(19).isAPartOf(wsaaProcessingMsg, 0).init(SPACES);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 19, FILLER).init(SPACES);
	private ZonedDecimalData wsaaMsgTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaProcessingMsg, 20).setUnsigned();
	private FixedLengthStringData filler1 = new FixedLengthStringData(3).isAPartOf(wsaaProcessingMsg, 25, FILLER).init(" - ");
	private FixedLengthStringData wsaaMsgTrantype = new FixedLengthStringData(4).isAPartOf(wsaaProcessingMsg, 28).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 32, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsgTrandesc = new FixedLengthStringData(27).isAPartOf(wsaaProcessingMsg, 33);

	private FixedLengthStringData wsaaConfirmMsg = new FixedLengthStringData(60);
	private FixedLengthStringData filler3 = new FixedLengthStringData(19).isAPartOf(wsaaConfirmMsg, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsg = new FixedLengthStringData(22).isAPartOf(wsaaConfirmMsg, 19).init(SPACES);
	private FixedLengthStringData filler4 = new FixedLengthStringData(19).isAPartOf(wsaaConfirmMsg, 41, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
	private String e350 = "E350";
	private String h143 = "H143";
	private String h145 = "H145";
	private String h093 = "H093";
		/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5679 = "T5679";
	private String t5681 = "T5681";
	private String t5682 = "T5682";
	private String t5688 = "T5688";
	private String t6661 = "T6661";
	private String tr386 = "TR386";
	private String covrsurrec = "COVRSURREC";
	private String itemrec = "ITEMREC";
	private String ptrnrevrec = "PTRNREVREC";
	private String surhclmrec = "SURHCLMREC";
		/*Contract Header - Surrenders*/
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage and Rider Details - Full Surren*/
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life logical file - full surrender*/
	private LifesurTableDAM lifesurIO = new LifesurTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
		/*Full Surrender header record*/
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private T6661rec t6661rec = new T6661rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tranchkrec tranchkrec = new Tranchkrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5156ScreenVars sv = ScreenProgram.getScreenVars( S5156ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		search1410, 
		search1510, 
		readJlife1630, 
		exit1690, 
		readPtrn1820, 
		badTrans1850, 
		exit1890, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		readNextModifiedRecord2680, 
		exit3090, 
		exit4090, 
		exit4110
	}

	public P5156() {
		super();
		screenVars = sv;
		new ScreenModel("S5156", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaFirstTime = "Y";
		wsaaTransErr = "N";
		wsaaMiscellaneous.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5156", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrsurIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaTransSurr.set(t6661rec.trcode);
		checkLastTrans1800();
		readT56791100();
		if ((isEQ(chdrsurIO.getPolsum(),chdrsurIO.getPolinc()))
		&& (isEQ(wsaaTransErr,"N"))) {
			sv.planSuffix.set(0);
			sv.select.set("X");
			scrnparams.function.set(varcom.sadd);
			processScreen("S5156", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		compute(wsaaCovrTimes, 0).set(sub(chdrsurIO.getPolinc(),chdrsurIO.getPolsum()));
		if (isEQ(chdrsurIO.getPolinc(),1)) {
			wsaaPlanSuffix.set(ZERO);
		}
		else {
			wsaaPlanSuffix.set(chdrsurIO.getPolinc());
		}
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaCovrTimes);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			loadCovrSubfile1200();
		}
		if (isNE(chdrsurIO.getPolsum(),ZERO)) {
			processSummary1150();
		}
		fillScreen1600();
		if (isEQ(wsaaTransErr,"Y")) {
			goTo(GotoLabel.exit1090);
		}
		surhclmIO.setParams(SPACES);
		surhclmIO.setTranno(ptrnrevIO.getTranno());
		surhclmIO.setPlanSuffix(ZERO);
		surhclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
		surhclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surhclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		surhclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surhclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		surhclmIO.setFormat(surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if ((isNE(surhclmIO.getStatuz(),varcom.oK))
		&& (isNE(surhclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		if ((isNE(surhclmIO.getChdrnum(),chdrsurIO.getChdrnum()))
		|| (isNE(surhclmIO.getChdrcoy(),chdrsurIO.getChdrcoy()))
		|| (isNE(surhclmIO.getTranno(),ptrnrevIO.getTranno()))
		|| (isEQ(surhclmIO.getStatuz(),varcom.endp))) {
			surhclmIO.setStatuz(varcom.endp);
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
	}

protected void readT56791100()
	{
		readStatusTable1110();
	}

protected void readStatusTable1110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void processSummary1150()
	{
		go1151();
	}

protected void go1151()
	{
		covrsurIO.setDataArea(SPACES);
		covrsurIO.setChdrcoy(wsspcomn.company);
		covrsurIO.setChdrnum(chdrsurIO.getChdrnum());
		covrsurIO.setPlanSuffix(ZERO);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrsurIO);
		if ((isNE(covrsurIO.getStatuz(),varcom.oK))
		&& (isNE(covrsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,covrsurIO.getChdrcoy()))
		|| (isNE(chdrsurIO.getChdrnum(),covrsurIO.getChdrnum()))
		|| (isEQ(covrsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		wsaaSub.set(ZERO);
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		checkStatusCoverage1400();
		wsaaSub.set(ZERO);
		checkStatusPremium1500();
		if (isEQ(sv.selectOut[varcom.pr.toInt()],"Y")) {
		}
		descIO.setDescitem(covrsurIO.getStatcode());
		descIO.setDesctabl(t5682);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.rstatdesc.fill("?");
		}
		descIO.setDescitem(covrsurIO.getPstatcode());
		descIO.setDesctabl(t5681);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.pstatdesc.fill("?");
		}
		PackedDecimalData loopEndVar2 = new PackedDecimalData(7, 0);
		loopEndVar2.set(chdrsurIO.getPolsum());
		for (int loopVar2 = 0; !(isEQ(loopVar2,loopEndVar2.toInt())); loopVar2 += 1){
			loadSummarySubfile1250();
		}
	}

protected void loadCovrSubfile1200()
	{
		go1210();
	}

protected void go1210()
	{
		covrsurIO.setDataArea(SPACES);
		covrsurIO.setChdrcoy(wsspcomn.company);
		covrsurIO.setChdrnum(chdrsurIO.getChdrnum());
		covrsurIO.setPlanSuffix(wsaaPlanSuffix);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrsurIO);
		if ((isNE(covrsurIO.getStatuz(),varcom.oK))
		&& (isNE(covrsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,covrsurIO.getChdrcoy()))
		|| (isNE(chdrsurIO.getChdrnum(),covrsurIO.getChdrnum()))
		|| (isEQ(covrsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		sv.planSuffix.set(wsaaPlanSuffix);
		wsaaSub.set(ZERO);
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		checkStatusCoverage1400();
		wsaaSub.set(ZERO);
		checkStatusPremium1500();
		if (isEQ(sv.selectOut[varcom.pr.toInt()],"Y")) {
		}
		descIO.setDescitem(covrsurIO.getStatcode());
		descIO.setDesctabl(t5682);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.rstatdesc.fill("?");
		}
		descIO.setDescitem(covrsurIO.getPstatcode());
		descIO.setDesctabl(t5681);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.pstatdesc.fill("?");
		}
		if ((isEQ(chdrsurIO.getPolinc(),chdrsurIO.getPolsum()))
		|| (isEQ(wsaaTransErr,"Y"))
		|| (isNE(ptrnrevIO.getTranno(),covrsurIO.getTranno()))) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
		else {
			if (isEQ(ptrnrevIO.getTranno(),covrsurIO.getTranno())) {
				sv.select.set("X");
			}
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5156", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.subtract(1);
	}

protected void loadSummarySubfile1250()
	{
		go1260();
	}

protected void go1260()
	{
		sv.planSuffix.set(wsaaPlanSuffix);
		if ((isEQ(chdrsurIO.getPolinc(),chdrsurIO.getPolsum()))
		|| (isEQ(wsaaTransErr,"Y"))
		|| (isNE(ptrnrevIO.getTranno(),covrsurIO.getTranno()))) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
		else {
			if (isEQ(ptrnrevIO.getTranno(),covrsurIO.getTranno())) {
				sv.select.set("X");
			}
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5156", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.subtract(1);
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkStatusCoverage1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case search1410: {
					search1410();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search1410()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.rstatcode.set(covrsurIO.getStatcode());
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
		else {
			if (isNE(covrsurIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search1410);
			}
			else {
				sv.rstatcode.set(covrsurIO.getStatcode());
				sv.selectOut[varcom.pr.toInt()].set("N");
				sv.selectOut[varcom.nd.toInt()].set("N");
			}
		}
		/*EXIT*/
	}

protected void checkStatusPremium1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case search1510: {
					search1510();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search1510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			sv.pstatcode.set(covrsurIO.getPstatcode());
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
		else {
			if (isNE(covrsurIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search1510);
			}
			else {
				sv.pstatcode.set(covrsurIO.getPstatcode());
				sv.selectOut[varcom.pr.toInt()].set("N");
				sv.selectOut[varcom.nd.toInt()].set("N");
			}
		}
		/*EXIT*/
	}

protected void fillScreen1600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					headings1610();
				}
				case readJlife1630: {
					readJlife1630();
				}
				case exit1690: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void headings1610()
	{
		sv.cntcurr.set(chdrsurIO.getCntcurr());
		sv.chdrnum.set(chdrsurIO.getChdrnum());
		sv.cnttype.set(chdrsurIO.getCnttype());
		descIO.setDescitem(chdrsurIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrsurIO.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		else {
			sv.chdrstatus.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrsurIO.getPstatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.premstatus.set(descIO.getShortdesc());
		}
		else {
			sv.premstatus.fill("?");
		}
		lifesurIO.setDataArea(SPACES);
		lifesurIO.setChdrnum(chdrsurIO.getChdrnum());
		lifesurIO.setChdrcoy(chdrsurIO.getChdrcoy());
		lifesurIO.setLife(covrsurIO.getLife());
		lifesurIO.setJlife("00");
		lifesurIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifesurIO);
		if ((isNE(lifesurIO.getStatuz(),varcom.oK))
		&& (isNE(lifesurIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isEQ(lifesurIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.readJlife1630);
		}
		sv.lifenum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1700();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.lifenameErr.set(h143);
			sv.lifename.set(SPACES);
		}
		else {
			plainname();
			sv.lifename.set(wsspcomn.longconfname);
		}
	}

protected void readJlife1630()
	{
		lifesurIO.setJlife("01");
		lifesurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifesurIO);
		if ((isNE(lifesurIO.getStatuz(),varcom.oK))
		&& (isNE(lifesurIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isEQ(lifesurIO.getStatuz(),varcom.mrnf)) {
			sv.jlife.set("NONE");
			sv.jlifename.set(SPACES);
			goTo(GotoLabel.exit1690);
		}
		sv.jlife.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1700();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),"1"))) {
			sv.jlifenameErr.set(e350);
			sv.jlifename.set(SPACES);
		}
		else {
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
		}
	}

protected void getClientDetails1700()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkLastTrans1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readPtrn1810();
				}
				case readPtrn1820: {
					readPtrn1820();
				}
				case badTrans1850: {
					badTrans1850();
				}
				case exit1890: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readPtrn1810()
	{
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrsurIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrsurIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction("BEGN");
	}

protected void readPtrn1820()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if ((isNE(chdrsurIO.getChdrnum(),ptrnrevIO.getChdrnum()))
		|| (isNE(chdrsurIO.getChdrcoy(),ptrnrevIO.getChdrcoy()))) {
			ptrnrevIO.setStatuz(varcom.endp);
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if ((isEQ(ptrnrevIO.getBatctrcde(),wsaaTransSurr))) {
			wsaaMsgTranno.set(ptrnrevIO.getTranno());
			wsaaMsgTrantype.set(ptrnrevIO.getBatctrcde());
			descIO.setDescitem(ptrnrevIO.getBatctrcde());
			descIO.setDesctabl(t1688);
			findDesc1300();
			wsaaMsgTrandesc.set(descIO.getLongdesc());
			itemIO.setFormat(itemrec);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr386);
			wsaaTr386Lang.set(wsspcomn.language);
			wsaaTr386Pgm.set(wsaaProg);
			wsaaTr386Id.set(SPACES);
			itemIO.setItemitem(wsaaTr386Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			tr386rec.tr386Rec.set(itemIO.getGenarea());
			wsaaMsgProcess.set(tr386rec.progdesc01);
			sv.sfcdesc.set(wsaaProcessingMsg);
			wsaaMsg.set(tr386rec.progdesc02);
			sv.confirm.set(wsaaConfirmMsg);
			sv.sfcdescOut[varcom.nd.toInt()].set(SPACES);
			sv.sfcdesdOut[varcom.nd.toInt()].set(SPACES);
			keepPtrnrev1900();
			goTo(GotoLabel.exit1890);
		}
		tranchkrec.codeCheckRec.set(SPACES);
		tranchkrec.tcdeStatuz.set(varcom.oK);
		tranchkrec.tcdeTranCode.set(ptrnrevIO.getBatctrcde());
		tranchkrec.tcdeCompany.set(wsspcomn.company);
		callProgram(Trcdechk.class, tranchkrec.codeCheckRec);
		if ((isNE(tranchkrec.tcdeStatuz,varcom.oK))
		&& (isNE(tranchkrec.tcdeStatuz,varcom.mrnf))) {
			syserrrec.params.set(tranchkrec.codeCheckRec);
			fatalError600();
		}
		if (isEQ(tranchkrec.tcdeStatuz,varcom.mrnf)) {
			goTo(GotoLabel.badTrans1850);
		}
		if (isEQ(tranchkrec.tcdeStatuz,varcom.oK)) {
			ptrnrevIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readPtrn1820);
		}
	}

protected void badTrans1850()
	{
		sv.sfcdescOut[varcom.nd.toInt()].set("Y");
		sv.sfcdesdOut[varcom.nd.toInt()].set("Y");
		sv.chdrnumErr.set(h145);
		wsspcomn.edterror.set("Y");
		wsaaTransErr = "Y";
	}

protected void keepPtrnrev1900()
	{
		/*KEEP*/
		ptrnrevIO.setFunction(varcom.keeps);
		ptrnrevIO.setFormat(ptrnrevrec);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateScreen2010();
		checkForErrors2050();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
	}

protected void validateScreen2010()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5156", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		wsaaFirstTime = "N";
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsaaTransErr,"Y")) {
			sv.chdrnumErr.set(h145);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2610();
				}
				case readNextModifiedRecord2680: {
					readNextModifiedRecord2680();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.readNextModifiedRecord2680);
		}
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.selectOut[varcom.nd.toInt()].set("N");
		scrnparams.function.set(varcom.supd);
		processScreen("S5156", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			scrnparams.function.set(varcom.srnch);
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		processScreen("S5156", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		goTo(GotoLabel.exit3090);
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(chdrsurIO.getPolinc(),1)) {
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
			goTo(GotoLabel.exit4090);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5156", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		while ( !((isNE(sv.select,SPACES))
		|| (isEQ(scrnparams.statuz,varcom.endp)))) {
			processScreen("S5156", sv);
			if ((isNE(scrnparams.statuz,varcom.oK))
			&& (isNE(scrnparams.statuz,varcom.endp))) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (isNE(sv.select,SPACES)) {
			covrsurIO.setPlanSuffix(sv.planSuffix);
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
		}
	}

protected void callGenssw4100()
	{
		try {
			callGenssw4110();
		}
		catch (GOTOException e){
		}
	}

protected void callGenssw4110()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz,varcom.oK))
		&& (isNE(gensswrec.statuz,varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4110);
		}
		compute(wsaaSub1, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			gensToWsspProgs4300();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.programPtr.add(1);
	}

protected void gensToWsspProgs4300()
	{
		/*GENS-TO-WSSP-PROGS*/
		wsspcomn.secProg[wsaaSub1.toInt()].set(gensswrec.progOut[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
		/*EXIT*/
	}

protected void keepsCovr4400()
	{
		/*KEEPS*/
		covrsurIO.setFunction(varcom.keeps);
		covrsurIO.setPlanSuffix(surhclmIO.getPlanSuffix());
		covrsurIO.setFormat(covrsurrec);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
}
