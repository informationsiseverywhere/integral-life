/*
 * File: Grevutrn.java
 * Date: 29 August 2009 22:51:03
 * Author: Quipoz Limited
 * 
 * Class transformed from GREVUTRN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitdrevTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrcfiTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnsurTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*   Generalised Reversal Subroutine for UTRNs.
*  -------------------------------------------
*
*       The parameters passed to this program are set up in the
*       copybook GREVERSREC.
*
* REVERSE OUT UTRN TRANSACTIONS
*
*    Perform a BEGNH on the UTRN file using the logical
*    view UTRNSUR. Set up the key from the information in
*    the copybook. Check for a key break, otherwise continue
*    processing until end of file.
*
*    If the retrieved record is unprocessed, i.e. the feedback
*     indicator is spaces, it must be deleted.
*
*    If the record is processed, compute the reversed amounts,
*     blank out the Trigger-Key, Trigger Module and Feed back
*     Indicator and write a new UTRN.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*     Include generic reversal of interest bearing fund               *
*     transaction records, HITRs.                                     *
*                                                                     *
*****************************************************************
* </pre>
*/
public class Grevutrn extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "GREVUTRN";
		/* FORMATS */
	private static final String utrnsurrec = "UTRNSURREC";
	private static final String utrnrec = "UTRNREC";
	private static final String hitrcfirec = "HITRCFIREC";
	private static final String hitdrec = "HITDREC";
	private static final String hitdrevrec = "HITDREVREC";
	private HitdTableDAM hitdIO = new HitdTableDAM();
		/*I/B fund interest details Reversal*/
	private HitdrevTableDAM hitdrevIO = new HitdrevTableDAM();
		/*Interest Bearing Transaction Details*/
	private HitrcfiTableDAM hitrcfiIO = new HitrcfiTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrnsurTableDAM utrnsurIO = new UtrnsurTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Greversrec greversrec = new Greversrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		hitd080
	}

	public Grevutrn() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start010();
				case hitd080: 
					hitd080();
					exit090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start010()
	{
		greversrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		greversrec.contractAmount.set(ZERO);
		a100ProcessHitrs();
		mainProcessing200();
		if (isEQ(utrnsurIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.hitd080);
		}
		utrnsurIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, utrnsurIO);
		if (isNE(utrnsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnsurIO.getParams());
			syserrrec.statuz.set(utrnsurIO.getStatuz());
			fatalError9000();
		}
	}

protected void hitd080()
	{
		hitdrevIO.setParams(SPACES);
		hitdrevIO.setChdrcoy(greversrec.chdrcoy);
		hitdrevIO.setChdrnum(greversrec.chdrnum);
		hitdrevIO.setLife(greversrec.life);
		hitdrevIO.setCoverage(greversrec.coverage);
		hitdrevIO.setRider(greversrec.rider);
		hitdrevIO.setPlanSuffix(greversrec.planSuffix);
		hitdrevIO.setTranno(greversrec.tranno);
		hitdrevIO.setFormat(hitdrevrec);
		hitdrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		hitdrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, hitdrevIO);
		if (isNE(hitdrevIO.getStatuz(),varcom.oK)
		&& isNE(hitdrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitdrevIO.getParams());
			fatalError9000();
		}
		if (isNE(greversrec.chdrcoy,hitdrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitdrevIO.getChdrnum())
		|| isNE(greversrec.life,hitdrevIO.getLife())
		|| isNE(greversrec.coverage,hitdrevIO.getCoverage())
		|| isNE(greversrec.rider,hitdrevIO.getRider())
		|| isNE(greversrec.planSuffix,hitdrevIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hitdrevIO.getTranno())) {
			hitdrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hitdrevIO.getStatuz(),varcom.endp))) {
			reverseHitds2000();
		}
		
	}

protected void exit090()
	{
		exitProgram();
	}

protected void mainProcessing200()
	{
		start201();
	}

protected void start201()
	{
		utrnsurIO.setParams(SPACES);
		utrnsurIO.setChdrcoy(greversrec.chdrcoy);
		utrnsurIO.setChdrnum(greversrec.chdrnum);
		utrnsurIO.setTranno(greversrec.tranno);
		utrnsurIO.setPlanSuffix(greversrec.planSuffix);
		utrnsurIO.setCoverage(greversrec.coverage);
		utrnsurIO.setRider(greversrec.rider);
		utrnsurIO.setLife(greversrec.life);
		utrnsurIO.setFormat(utrnsurrec);
		utrnsurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnsurIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, utrnsurIO);
		if ((isNE(utrnsurIO.getStatuz(),varcom.oK))
		&& (isNE(utrnsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrnsurIO.getParams());
			syserrrec.statuz.set(utrnsurIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(greversrec.chdrcoy,utrnsurIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,utrnsurIO.getChdrnum()))
		|| (isNE(greversrec.tranno,utrnsurIO.getTranno()))
		|| (isNE(greversrec.coverage,utrnsurIO.getCoverage()))
		|| (isNE(greversrec.rider,utrnsurIO.getRider()))
		|| (isNE(greversrec.life,utrnsurIO.getLife()))
		|| (isNE(greversrec.planSuffix,utrnsurIO.getPlanSuffix()))) {
			utrnsurIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(utrnsurIO.getStatuz(),varcom.endp))) {
			setupUtrns300();
		}
		
	}

protected void setupUtrns300()
	{
		start301();
	}

protected void start301()
	{
		greversrec.contractAmount.add(utrnsurIO.getContractAmount());
		if (isEQ(utrnsurIO.getFeedbackInd(),SPACES)) {
			/*     MOVE READH              TO UTRNSUR-FUNCTION      <LA4553>*/
			/*                                                      <LA4553>*/
			/*     CALL 'UTRNSURIO'     USING UTRNSUR-PARAMS        <LA4553>*/
			/*                                                      <LA4553>*/
			/*     IF UTRNSUR-STATUZ    NOT = O-K                   <LA4553>*/
			/*        MOVE UTRNSUR-PARAMS  TO SYSR-PARAMS           <LA4553>*/
			/*        MOVE UTRNSUR-STATUZ  TO SYSR-STATUZ           <LA4553>*/
			/*        PERFORM 9000-FATAL-ERROR                      <LA4553>*/
			/*     END-IF                                           <LA4553>*/
			/*                                                      <LA4553>*/
			/*     MOVE DELET              TO UTRNSUR-FUNCTION              */
			/*     CALL 'UTRNSURIO'     USING UTRNSUR-PARAMS                */
			/*     IF UTRNSUR-STATUZ    NOT = O-K                           */
			/*        MOVE UTRNSUR-PARAMS  TO SYSR-PARAMS                   */
			/*        MOVE UTRNSUR-STATUZ  TO SYSR-STATUZ                   */
			/*        PERFORM 9000-FATAL-ERROR                              */
			/*     END-IF                                                   */
			utrnIO.setParams(SPACES);
			utrnIO.setRrn(utrnsurIO.getRrn());
			utrnIO.setFormat(utrnrec);
			utrnIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, utrnIO);
			if (isNE(utrnIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(utrnIO.getParams());
				syserrrec.statuz.set(utrnIO.getStatuz());
				fatalError9000();
			}
			utrnIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, utrnIO);
			if (isNE(utrnIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(utrnIO.getParams());
				syserrrec.statuz.set(utrnIO.getStatuz());
				fatalError9000();
			}
		}
		if (isNE(utrnsurIO.getFeedbackInd(),SPACES)) {
			/*                                UTRNSUR-TRIGGER-MODULE        */
			/*                                UTRNSUR-TRIGGER-KEY           */
			/*     MOVE ZEROES             TO UTRNSUR-SURRENDER-PERCENT     */
			/*     COMPUTE UTRNSUR-PROC-SEQ-NO     =                <LA4553>*/
			/*             UTRNSUR-PROC-SEQ-NO     * -1             <LA4553>*/
			/*     COMPUTE UTRNSUR-CONTRACT-AMOUNT =                        */
			/*             UTRNSUR-CONTRACT-AMOUNT * -1                     */
			/*     COMPUTE UTRNSUR-FUND-AMOUNT =                            */
			/*             UTRNSUR-FUND-AMOUNT * -1                         */
			/*     COMPUTE UTRNSUR-NOF-DUNITS =                             */
			/*             UTRNSUR-NOF-DUNITS * -1                          */
			/*     COMPUTE UTRNSUR-NOF-UNITS =                              */
			/*             UTRNSUR-NOF-UNITS * -1                           */
			/*     ADD 1                   TO WSAA-PROC-SEQ                 */
			/*     MOVE WSAA-PROC-SEQ      TO UTRNSUR-PROC-SEQ-NO           */
			/*     MOVE GREV-TRANS-DATE    TO UTRNSUR-TRANSACTION-DATE      */
			/*     MOVE GREV-TRANS-TIME    TO UTRNSUR-TRANSACTION-TIME      */
			/*     MOVE GREV-NEW-TRANNO    TO UTRNSUR-TRANNO                */
			/*     MOVE GREV-TERMID        TO UTRNSUR-TERMID                */
			/*     MOVE GREV-USER          TO UTRNSUR-USER                  */
			/*     MOVE GREV-BATCKEY       TO WSAA-BATCKEY                  */
			/*     MOVE WSKY-BATC-BATCTRCDE TO UTRNSUR-BATCTRCDE            */
			/*     MOVE WSKY-BATC-BATCCOY   TO UTRNSUR-BATCCOY              */
			/*     MOVE WSKY-BATC-BATCBRN   TO UTRNSUR-BATCBRN              */
			/*     MOVE WSKY-BATC-BATCACTYR TO UTRNSUR-BATCACTYR            */
			/*     MOVE WSKY-BATC-BATCACTMN TO UTRNSUR-BATCACTMN            */
			/*     MOVE WSKY-BATC-BATCBATCH TO UTRNSUR-BATCBATCH            */
			/*     MOVE WRITR               TO UTRNSUR-FUNCTION             */
			/*     CALL 'UTRNSURIO'     USING UTRNSUR-PARAMS                */
			/*     IF UTRNSUR-STATUZ    NOT = O-K                           */
			/*         MOVE UTRNSUR-PARAMS TO SYSR-PARAMS                   */
			/*         MOVE UTRNSUR-STATUZ TO SYSR-STATUZ                   */
			/*         PERFORM 9000-FATAL-ERROR                             */
			/*     END-IF                                                   */
			utrnIO.setParams(SPACES);
			utrnIO.setRrn(utrnsurIO.getRrn());
			utrnIO.setFormat(utrnrec);
			utrnIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, utrnIO);
			if (isNE(utrnIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(utrnIO.getParams());
				syserrrec.statuz.set(utrnIO.getStatuz());
				fatalError9000();
			}
			utrnIO.setFeedbackInd(SPACES);
			utrnIO.setTriggerModule(SPACES);
			utrnIO.setTriggerKey(SPACES);
			utrnIO.setSurrenderPercent(ZERO);
			setPrecision(utrnIO.getProcSeqNo(), 0);
			utrnIO.setProcSeqNo(mult(utrnIO.getProcSeqNo(), -1));
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(mult(utrnIO.getContractAmount(), -1));
			setPrecision(utrnIO.getFundAmount(), 2);
			utrnIO.setFundAmount(mult(utrnIO.getFundAmount(), -1));
			setPrecision(utrnIO.getNofDunits(), 5);
			utrnIO.setNofDunits(mult(utrnIO.getNofDunits(), -1));
			setPrecision(utrnIO.getNofUnits(), 5);
			utrnIO.setNofUnits(mult(utrnIO.getNofUnits(), -1));
			utrnIO.setTransactionDate(greversrec.transDate);
			utrnIO.setTransactionTime(greversrec.transTime);
			utrnIO.setTranno(greversrec.newTranno);
			utrnIO.setTermid(greversrec.termid);
			utrnIO.setUser(greversrec.user);
			wsaaBatckey.set(greversrec.batckey);
			utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			utrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
			utrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			utrnIO.setFormat(utrnrec);
			utrnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, utrnIO);
			if (isNE(utrnIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(utrnIO.getParams());
				syserrrec.statuz.set(utrnIO.getStatuz());
				fatalError9000();
			}
		}
		utrnsurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrnsurIO);
		if ((isNE(utrnsurIO.getStatuz(),varcom.oK))
		&& (isNE(utrnsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrnsurIO.getParams());
			syserrrec.statuz.set(utrnsurIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(greversrec.chdrcoy,utrnsurIO.getChdrcoy()))
		|| (isNE(greversrec.chdrnum,utrnsurIO.getChdrnum()))
		|| (isNE(greversrec.tranno,utrnsurIO.getTranno()))
		|| (isNE(greversrec.coverage,utrnsurIO.getCoverage()))
		|| (isNE(greversrec.rider,utrnsurIO.getRider()))
		|| (isNE(greversrec.life,utrnsurIO.getLife()))
		|| (isNE(greversrec.planSuffix,utrnsurIO.getPlanSuffix()))) {
			utrnsurIO.setStatuz(varcom.endp);
		}
	}

protected void reverseHitds2000()
	{
					hitd2010();
					next2080();
				}

protected void hitd2010()
	{
		hitdIO.setParams(SPACES);
		hitdIO.setRrn(hitdrevIO.getRrn());
		hitdIO.setFormat(hitdrec);
		hitdIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError9000();
		}
		hitdIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError9000();
		}
		hitdIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)
		&& isNE(hitdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError9000();
		}
		if (isNE(hitdIO.getChdrcoy(),hitdrevIO.getChdrcoy())
		|| isNE(hitdIO.getChdrnum(),hitdrevIO.getChdrnum())
		|| isNE(hitdIO.getCoverage(),hitdrevIO.getCoverage())
		|| isNE(hitdIO.getLife(),hitdrevIO.getLife())
		|| isNE(hitdIO.getRider(),hitdrevIO.getRider())
		|| isNE(hitdIO.getPlanSuffix(),hitdrevIO.getPlanSuffix())
		|| isNE(hitdIO.getZintbfnd(),hitdrevIO.getZintbfnd())
		|| isEQ(hitdIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(hitdIO.getValidflag(),"2")) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError9000();
		}
		hitdIO.setValidflag("1");
		hitdIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, hitdIO);
		if (isNE(hitdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitdIO.getParams());
			fatalError9000();
		}
	}

protected void next2080()
	{
		hitdrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitdrevIO);
		if (isNE(hitdrevIO.getStatuz(),varcom.oK)
		&& isNE(hitdrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitdrevIO.getParams());
			fatalError9000();
		}
		if (isNE(greversrec.chdrcoy,hitdrevIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitdrevIO.getChdrnum())
		|| isNE(greversrec.life,hitdrevIO.getLife())
		|| isNE(greversrec.coverage,hitdrevIO.getCoverage())
		|| isNE(greversrec.rider,hitdrevIO.getRider())
		|| isNE(greversrec.planSuffix,hitdrevIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hitdrevIO.getTranno())) {
			hitdrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void a100ProcessHitrs()
	{
		a110Hitr();
	}

protected void a110Hitr()
	{
		hitrcfiIO.setParams(SPACES);
		hitrcfiIO.setChdrcoy(greversrec.chdrcoy);
		hitrcfiIO.setChdrnum(greversrec.chdrnum);
		hitrcfiIO.setTranno(greversrec.tranno);
		hitrcfiIO.setPlanSuffix(greversrec.planSuffix);
		hitrcfiIO.setCoverage(greversrec.coverage);
		hitrcfiIO.setRider(greversrec.rider);
		hitrcfiIO.setLife(greversrec.life);
		hitrcfiIO.setFormat(hitrcfirec);
		hitrcfiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		hitrcfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, hitrcfiIO);
		if (isNE(hitrcfiIO.getStatuz(),varcom.oK)
		&& isNE(hitrcfiIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrcfiIO.getParams());
			fatalError9000();
		}
		if (isNE(greversrec.chdrcoy,hitrcfiIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitrcfiIO.getChdrnum())
		|| isNE(greversrec.life,hitrcfiIO.getLife())
		|| isNE(greversrec.coverage,hitrcfiIO.getCoverage())
		|| isNE(greversrec.rider,hitrcfiIO.getRider())
		|| isNE(greversrec.planSuffix,hitrcfiIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hitrcfiIO.getTranno())) {
			hitrcfiIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hitrcfiIO.getStatuz(),varcom.endp))) {
			a200SetupHitrs();
		}
		
	}

protected void a200SetupHitrs()
	{
		a210Hitr();
	}

protected void a210Hitr()
	{
		greversrec.contractAmount.add(hitrcfiIO.getContractAmount());
		if (isEQ(hitrcfiIO.getFeedbackInd(),SPACES)) {
			hitrcfiIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hitrcfiIO);
			if (isNE(hitrcfiIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hitrcfiIO.getParams());
				fatalError9000();
			}
		}
		else {
			hitrcfiIO.setFeedbackInd(SPACES);
			hitrcfiIO.setTriggerModule(SPACES);
			hitrcfiIO.setTriggerKey(SPACES);
			hitrcfiIO.setSurrenderPercent(ZERO);
			setPrecision(hitrcfiIO.getProcSeqNo(), 0);
			hitrcfiIO.setProcSeqNo(mult(hitrcfiIO.getProcSeqNo(),-1));
			setPrecision(hitrcfiIO.getContractAmount(), 2);
			hitrcfiIO.setContractAmount(mult(hitrcfiIO.getContractAmount(),-1));
			setPrecision(hitrcfiIO.getFundAmount(), 2);
			hitrcfiIO.setFundAmount(mult(hitrcfiIO.getFundAmount(),-1));
			hitrcfiIO.setTranno(greversrec.newTranno);
			wsaaBatckey.set(greversrec.batckey);
			hitrcfiIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
			hitrcfiIO.setBatccoy(wsaaBatckey.batcBatccoy);
			hitrcfiIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
			hitrcfiIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
			hitrcfiIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
			hitrcfiIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
			hitrcfiIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hitrcfiIO);
			if (isNE(hitrcfiIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hitrcfiIO.getParams());
				fatalError9000();
			}
		}
		hitrcfiIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitrcfiIO);
		if (isNE(hitrcfiIO.getStatuz(),varcom.oK)
		&& isNE(hitrcfiIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrcfiIO.getParams());
			fatalError9000();
		}
		if (isNE(greversrec.chdrcoy,hitrcfiIO.getChdrcoy())
		|| isNE(greversrec.chdrnum,hitrcfiIO.getChdrnum())
		|| isNE(greversrec.life,hitrcfiIO.getLife())
		|| isNE(greversrec.coverage,hitrcfiIO.getCoverage())
		|| isNE(greversrec.rider,hitrcfiIO.getRider())
		|| isNE(greversrec.planSuffix,hitrcfiIO.getPlanSuffix())
		|| isNE(greversrec.tranno,hitrcfiIO.getTranno())) {
			hitrcfiIO.setStatuz(varcom.endp);
		}
	}

protected void fatalError9000()
	{
					error9010();
					exit9020();
				}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		greversrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
