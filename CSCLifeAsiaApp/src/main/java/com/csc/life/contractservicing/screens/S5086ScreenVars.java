package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5086
 * @version 1.0 generated on 30/08/09 06:34
 * @author Quipoz
 */
public class S5086ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(918);
	public FixedLengthStringData dataFields = new FixedLengthStringData(326).isAPartOf(dataArea, 0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData billfrom = DD.billfrom.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData billind = DD.billind.copy().isAPartOf(dataFields,10);
	public ZonedDecimalData billto = DD.billto.copyToZonedDecimal().isAPartOf(dataFields,11);
	public ZonedDecimalData bnsfrom = DD.bnsfrom.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData bnsind = DD.bnsind.copy().isAPartOf(dataFields,27);
	public ZonedDecimalData bnsto = DD.bnsto.copyToZonedDecimal().isAPartOf(dataFields,28);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,36);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,67);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,97);
	public ZonedDecimalData currto = DD.currto.copyToZonedDecimal().isAPartOf(dataFields,105);
	public ZonedDecimalData instpramt = DD.instpramt.copyToZonedDecimal().isAPartOf(dataFields,113);
	public FixedLengthStringData lapind = DD.lapind.copy().isAPartOf(dataFields,130);
	public ZonedDecimalData lapsfrom = DD.lapsfrom.copyToZonedDecimal().isAPartOf(dataFields,131);
	public ZonedDecimalData lapsto = DD.lapsto.copyToZonedDecimal().isAPartOf(dataFields,139);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,147);
	public FixedLengthStringData notind = DD.notind.copy().isAPartOf(dataFields,148);
	public ZonedDecimalData notsfrom = DD.notsfrom.copyToZonedDecimal().isAPartOf(dataFields,149);
	public ZonedDecimalData notsto = DD.notsto.copyToZonedDecimal().isAPartOf(dataFields,157);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,165);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,173);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,220);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,230);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,238);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,242);
	public FixedLengthStringData renind = DD.renind.copy().isAPartOf(dataFields,245);
	public FixedLengthStringData reptype = DD.reptype.copy().isAPartOf(dataFields,246);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,248);
	public ZonedDecimalData rnwlfrom = DD.rnwlfrom.copyToZonedDecimal().isAPartOf(dataFields,298);
	public ZonedDecimalData rnwlto = DD.rnwlto.copyToZonedDecimal().isAPartOf(dataFields,306);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,314);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(dataFields,324);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(148).isAPartOf(dataArea, 326);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bnsfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bnsindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bnstoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData currtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData instpramtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lapindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lapsfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lapstoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData notindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData notsfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData notstoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData renindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData reptypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData rnwlfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData rnwltoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData srcebusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(444).isAPartOf(dataArea, 474);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bnsfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bnsindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bnstoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] currtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] instpramtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lapindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lapsfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lapstoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] notindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] notsfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] notstoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] renindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] reptypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] rnwlfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] rnwltoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] srcebusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData billtoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData bnsfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData bnstoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currtoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lapsfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lapstoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData notsfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData notstoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rnwlfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rnwltoDisp = new FixedLengthStringData(10);

	public LongData S5086screenWritten = new LongData(0);
	public LongData S5086protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5086ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cnttypeOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypdescOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rstateOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pstateOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cownnumOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ownernameOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currfromOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currtoOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(resndescOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, srcebus, rstate, pstate, occdate, reptype, register, cownnum, ownername, instpramt, lapind, lapsfrom, lapsto, cntcurr, billfreq, billind, billfrom, billto, mop, notind, notsfrom, notsto, ptdate, renind, rnwlfrom, rnwlto, btdate, bnsind, bnsfrom, bnsto, comind, currfrom, currto, reasoncd, resndesc};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, srcebusOut, rstateOut, pstateOut, occdateOut, reptypeOut, registerOut, cownnumOut, ownernameOut, instpramtOut, lapindOut, lapsfromOut, lapstoOut, cntcurrOut, billfreqOut, billindOut, billfromOut, billtoOut, mopOut, notindOut, notsfromOut, notstoOut, ptdateOut, renindOut, rnwlfromOut, rnwltoOut, btdateOut, bnsindOut, bnsfromOut, bnstoOut, comindOut, currfromOut, currtoOut, reasoncdOut, resndescOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, srcebusErr, rstateErr, pstateErr, occdateErr, reptypeErr, registerErr, cownnumErr, ownernameErr, instpramtErr, lapindErr, lapsfromErr, lapstoErr, cntcurrErr, billfreqErr, billindErr, billfromErr, billtoErr, mopErr, notindErr, notsfromErr, notstoErr, ptdateErr, renindErr, rnwlfromErr, rnwltoErr, btdateErr, bnsindErr, bnsfromErr, bnstoErr, comindErr, currfromErr, currtoErr, reasoncdErr, resndescErr};
		screenDateFields = new BaseData[] {occdate, lapsfrom, lapsto, billfrom, billto, notsfrom, notsto, ptdate, rnwlfrom, rnwlto, btdate, bnsfrom, bnsto, currfrom, currto};
		screenDateErrFields = new BaseData[] {occdateErr, lapsfromErr, lapstoErr, billfromErr, billtoErr, notsfromErr, notstoErr, ptdateErr, rnwlfromErr, rnwltoErr, btdateErr, bnsfromErr, bnstoErr, currfromErr, currtoErr};
		screenDateDispFields = new BaseData[] {occdateDisp, lapsfromDisp, lapstoDisp, billfromDisp, billtoDisp, notsfromDisp, notstoDisp, ptdateDisp, rnwlfromDisp, rnwltoDisp, btdateDisp, bnsfromDisp, bnstoDisp, currfromDisp, currtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5086screen.class;
		protectRecord = S5086protect.class;
	}

}
