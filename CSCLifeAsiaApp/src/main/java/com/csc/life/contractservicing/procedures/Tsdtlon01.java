/*
 * File: Tsdtlon01.java
 * Date: 30 August 2009 2:46:11
 * Author: Quipoz Limited
 * 
 * Class transformed from TSDTLON01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.productdefinition.tablestructures.Tsdtcalrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* TSDTLON01 - Stamp Duty Calculation for policy loan.
*
* Logic:
*
* .  Calculate Stamp Duty round up =
*            ( input Amount * SD rate / loan rate)
*
*****************************************************************
*
/
***********************************************************************
*                                                                     *
* </pre>
*/
public class Tsdtlon01 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private ZonedDecimalData wsaaStampAmount = new ZonedDecimalData(17, 2).init(0).setUnsigned();
	private ZonedDecimalData wsaaStampRound = new ZonedDecimalData(15, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaLoanRate = new ZonedDecimalData(5, 0).init(10000).setUnsigned();
	private Syserrrec syserrrec = new Syserrrec();
	private Tsdtcalrec tsdtcalrec = new Tsdtcalrec();
	private Varcom varcom = new Varcom();

	public Tsdtlon01() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tsdtcalrec.tsdtcalRec = convertAndSetParam(tsdtcalrec.tsdtcalRec, parmArray, 0);
		try {
			subroutineMain000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void subroutineMain000()
	{
		/*MAIN*/
		tsdtcalrec.statuz.set("****");
		compute(wsaaStampAmount, 2).set(add((div(mult(tsdtcalrec.amount,tsdtcalrec.sdutyRate),wsaaLoanRate)),0.49));
		wsaaStampRound.setRounded(wsaaStampAmount);
		if (isLT(wsaaStampRound,tsdtcalrec.sdutyRate)) {
			tsdtcalrec.sdutyAmount.set(tsdtcalrec.sdutyRate);
		}
		else {
			tsdtcalrec.sdutyAmount.set(wsaaStampRound);
		}
		/*EXIT*/
		exitProgram();
	}

protected void fatalError600()
	{
		/*FATAL-ERROR*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		tsdtcalrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
