/*
 * File: Revpincr.java
 * Date: 30 August 2009 2:10:41
 * Author: Quipoz Limited
 * 
 * Class transformed from REVPINCR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.CovrrccTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.regularprocessing.dataaccess.IncrrefTableDAM;
import com.csc.life.regularprocessing.dataaccess.LifergpTableDAM;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*        REVERSE PENDING INCREASE.
*        -------------------------
*
* Processing.
* -----------
*
* Update Pending Increase Record ( INCR ):
*
*    When  a  component is  at  a stage of  Increase Pending an
*    INCR   record  exists.  This  record  contains information
*    information pertaining to the Increase.
*
*    Therefore,  to  reverse   the  process   on   this   INCR,
*    Validflag  '2' the record and set the Refusal  Flag as per
*    information entered on  the refusal  screen. ie.  Reversal
*    (Y/N).
*
*    Additionally,  based on the  Cease  Indicator  passed from
*    the input screen and/or if the Maximum No.  of refusals on
*    T6658 has been exceeded set the CPI-DATE to Hi-date.
*
* Reverse Coverage & Rider records ( COVRs ):
*
*    When  a  component is  at  a stage of Increase Pending the
*    COVR records are updated with the tranno for the 'PENDING'
*    transaction and the Indexation-Ind is set to 'F'.
*
*    Therefore, to reverse, process the  COVR file and for  any
*    COVR record that exists (validflag '1' or '2') which has a
*    tranno which  matches  the  PTRN  tranno  being  reversed,
*    delete all COVR records which  have the same component key
*    i.e. match on company, contract number, life, coverage and
*    rider.
*
*    For  the  reinstated  COVR record(s) the CPI-DATE must  be
*    recalculated  to  the  next date (for refusals only). This
*    date will be calculated as at New Business.
*
* Delete INCTPF record.
*
* No generic reversal processing is necessary.
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
*
*****************************************************************
* </pre>
*/
public class Revpincr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVPINCR";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaCeaseFuture = "";
	private ZonedDecimalData wsaaFixNoIncr = new ZonedDecimalData(3, 0);
	private FixedLengthStringData wsaaRefusalFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaMinTrmToCess = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRefusals = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaLastCrrcd = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaAnb = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private static final String h036 = "H036";
		/* TABLES */
	private static final String t6658 = "T6658";
	private static final String t1693 = "T1693";
		/* FORMATS */
	private static final String incrrec = "INCRREC   ";
	private static final String incrrefrec = "INCRREFREC";
	private static final String covrrec = "COVRREC   ";
	private static final String covrrccrec = "COVRRCCREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrrccTableDAM covrrccIO = new CovrrccTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private IncrTableDAM incrIO = new IncrTableDAM();
	private IncrrefTableDAM incrrefIO = new IncrrefTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifergpTableDAM lifergpIO = new LifergpTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T6658rec t6658rec = new T6658rec();
	private T1693rec t1693rec = new T1693rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Reverserec reverserec = new Reverserec();

	public Revpincr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaRefusalFlag.set(SPACES);
		processIncrs2000();
		processCovrs3000();
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processIncrs2000()
	{
			start2000();
		}

protected void start2000()
	{
		/* Use INCR logical view to loop around Increase records*/
		incrIO.setParams(SPACES);
		incrIO.setChdrcoy(reverserec.company);
		incrIO.setChdrnum(reverserec.chdrnum);
		incrIO.setPlanSuffix(ZERO);
		incrIO.setFormat(incrrec);
		/* MOVE BEGNH                  TO INCR-FUNCTION.                */
		incrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		incrIo2300();
		if (isEQ(incrIO.getStatuz(),varcom.endp)) {
			/*      REWRT the record to Release it*/
			/* Since BEGNH is replaced by BEGN,record is not held,          */
			/* REWRT is not necessary.                                      */
			/*     MOVE REWRT              TO INCR-FUNCTION                 */
			/*     PERFORM 2300-INCR-IO                                     */
			return ;
		}
		wsaaCeaseFuture = "N";
		while ( !(isEQ(incrIO.getStatuz(),varcom.endp))) {
			updateIncrs2100();
		}
		
	}

protected void updateIncrs2100()
	{
					start2100();
					rewrt2105();
				}

protected void start2100()
	{
		/* If the INCR has not been selected, or*/
		/* has been reversed already ('R'), or*/
		/* has been refused already ('Y'), or*/
		/* has not run through REVAINC first (ie. setting VF to '1')*/
		/*       then bypass the record.*/
		if (isEQ(incrIO.getRefusalFlag(),SPACES)
		|| isEQ(incrIO.getRefusalFlag(),"R")
		|| isEQ(incrIO.getRefusalFlag(),"Y")
		|| isNE(incrIO.getValidflag(),"1")) {
			return ;
		}
		if (isEQ(incrIO.getRefusalFlag(),"F")) {
			incrIO.setRefusalFlag("Y");
			wsaaRefusalFlag.set("Y");
		}
		if (isEQ(incrIO.getRefusalFlag(),"V")) {
			incrIO.setRefusalFlag("R");
		}
		if (isEQ(incrIO.getCeaseInd(),"Y")) {
			wsaaCeaseFuture = "Y";
		}
		incrIO.setValidflag("2");
	}

protected void rewrt2105()
	{
		/* MOVE REWRT                  TO INCR-FUNCTION.                */
		incrIO.setFunction(varcom.writd);
		incrIo2300();
		incrIO.setFunction(varcom.nextr);
		incrIo2300();
		/*EXIT*/
	}

protected void incrIo2300()
	{
		/*START*/
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(),varcom.oK)
		&& isNE(incrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrIO.getParams());
			syserrrec.statuz.set(incrIO.getStatuz());
			databaseError99500();
		}
		if (isNE(incrIO.getChdrcoy(),reverserec.company)
		|| isNE(incrIO.getChdrnum(),reverserec.chdrnum)) {
			incrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processCovrs3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Use COVR logical view to loop around coverage/rider records*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError99500();
		}
		if (isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(),varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
			reverseCovrs3100();
		}
		
	}

protected void reverseCovrs3100()
	{
		start3100();
	}

protected void start3100()
	{
		/* check TRANNO of COVR just read*/
		if (isEQ(reverserec.tranno,covrIO.getTranno())) {
			covrrccIO.setParams(SPACES);
			covrrccIO.setChdrcoy(covrIO.getChdrcoy());
			covrrccIO.setChdrnum(covrIO.getChdrnum());
			covrrccIO.setLife(covrIO.getLife());
			covrrccIO.setCoverage(covrIO.getCoverage());
			covrrccIO.setRider(covrIO.getRider());
			covrrccIO.setPlanSuffix(covrIO.getPlanSuffix());
			/*     MOVE BEGN               TO COVRRCC-FUNCTION              */
			covrrccIO.setFunction(varcom.readh);
			covrrccIO.setTranno(reverserec.tranno);
			covrrccIO.setFormat(covrrccrec);
			deleteCovrs3200();
			/* Having processed a Coverage/Rider, set plan suffix to 9999*/
			/*  to ensure that I/O bypassss all other COVR records with*/
			/*  same key and selects next Coverage/Rider. (if there is one)*/
			covrIO.setPlanSuffix(9999);
		}
		covrIO.setFunction(varcom.nextr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			databaseError99500();
		}
		if (isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)
		|| isEQ(covrIO.getStatuz(),varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
		}
	}

protected void deleteCovrs3200()
	{
			start3200();
		}

	/**
	* <pre>
	* Instead of doing a begin then read next etc., now a READH       
	* is being done, with tranno included in the key of the           
	* COVRRCC logical.                                                
	* </pre>
	*/
protected void start3200()
	{
		SmartFileCode.execute(appVars, covrrccIO);
		if (isNE(covrrccIO.getStatuz(),varcom.oK)
		&& isNE(covrrccIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrrccIO.getParams());
			syserrrec.statuz.set(covrrccIO.getStatuz());
			databaseError99500();
		}
		/* IF COVR-CHDRCOY             NOT = COVRRCC-CHDRCOY            */
		/*    OR COVR-CHDRNUM          NOT = COVRRCC-CHDRNUM            */
		/*    OR COVR-LIFE             NOT = COVRRCC-LIFE               */
		/*    OR COVR-COVERAGE         NOT = COVRRCC-COVERAGE           */
		/*    OR COVR-RIDER            NOT = COVRRCC-RIDER              */
		/*    OR COVR-PLAN-SUFFIX      NOT = COVRRCC-PLAN-SUFFIX        */
		/*    OR COVR-TRANNO           NOT = COVRRCC-TRANNO             */
		/*    OR COVRRCC-STATUZ        = ENDP                           */
		/*     MOVE ENDP               TO COVRRCC-STATUZ                */
		/*     GO TO 3290-EXIT                                          */
		/* END-IF.                                                      */
		/* MOVE READH                  TO COVRRCC-FUNCTION.             */
		/* PERFORM 3300-COVRRCC-IO.                                     */
		covrrccIO.setFunction(varcom.delet);
		covrrccIo3300();
		/* MOVE NEXTR                  TO COVRRCC-FUNCTION.             */
		covrrccIO.setFunction(varcom.endh);
		SmartFileCode.execute(appVars, covrrccIO);
		if (isNE(covrrccIO.getStatuz(),varcom.oK)
		&& isNE(covrrccIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrrccIO.getParams());
			syserrrec.statuz.set(covrrccIO.getStatuz());
			databaseError99500();
		}
		if (isNE(covrIO.getChdrcoy(),covrrccIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),covrrccIO.getChdrnum())
		|| isNE(covrIO.getLife(),covrrccIO.getLife())
		|| isNE(covrIO.getCoverage(),covrrccIO.getCoverage())
		|| isNE(covrIO.getRider(),covrrccIO.getRider())
		|| isNE(covrIO.getPlanSuffix(),covrrccIO.getPlanSuffix())
		|| isEQ(covrrccIO.getStatuz(),varcom.endp)) {
			covrrccIO.setStatuz(varcom.endp);
			return ;
		}
		/* MOVE READH                  TO COVRRCC-FUNCTION.             */
		/* PERFORM 3300-COVRRCC-IO.                                     */
		covrrccIO.setValidflag("1");
		/* MOVE REVE-NEW-TRANNO        TO COVRRCC-TRANNO.               */
		covrrccIO.setCurrto(99999999);
		covrrccIO.setFunction(varcom.rewrt);
		if (isEQ(wsaaCeaseFuture,"Y")) {
			covrrccIO.setCpiDate(99999999);
		}
		/* At this stage the INCR record we have is either End of File*/
		/* or next record ( after the INCR BEGN/NEXTR loop performed was*/
		/* the 2000- Section). As a result, another READ on the INCR*/
		/* file is performed to get the relvant INCR details for this*/
		/* coverage.*/
		incrIO.setParams(SPACES);
		incrIO.setChdrcoy(covrrccIO.getChdrcoy());
		incrIO.setChdrnum(covrrccIO.getChdrnum());
		incrIO.setLife(covrrccIO.getLife());
		incrIO.setCoverage(covrrccIO.getCoverage());
		incrIO.setRider(covrrccIO.getRider());
		incrIO.setPlanSuffix(covrrccIO.getPlanSuffix());
		incrIO.setFormat(incrrec);
		incrIO.setFunction(varcom.readr);
		incrIo2300();
		readT66583500();
		if (isEQ(wsaaRefusalFlag,"Y")
		&& isNE(wsaaCeaseFuture,"Y")) {
			calcNextDate4500();
		}
		wsaaRefusals.set(ZERO);
		incrrefIO.setParams(SPACES);
		incrrefIO.setChdrcoy(reverserec.company);
		incrrefIO.setChdrnum(reverserec.chdrnum);
		incrrefIO.setPlanSuffix(ZERO);
		incrrefIO.setCrrcd(ZERO);
		incrrefIO.setFormat(incrrefrec);
		incrrefIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incrrefIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrrefIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaLastCrrcd.set(ZERO);
		while ( !(isEQ(incrrefIO.getStatuz(),varcom.endp))) {
			maxRefusals3400();
		}
		
		if (isGT(wsaaRefusals,t6658rec.maxRefusals)
		|| isEQ(wsaaRefusals,t6658rec.maxRefusals)) {
			covrrccIO.setCpiDate(99999999);
		}
		covrrccIo3300();
	}

protected void covrrccIo3300()
	{
		/*START*/
		SmartFileCode.execute(appVars, covrrccIO);
		if (isNE(covrrccIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrrccIO.getParams());
			syserrrec.statuz.set(covrrccIO.getStatuz());
			databaseError99500();
		}
		/*EXIT*/
	}

protected void maxRefusals3400()
	{
		/*START*/
		incrrefIo3450();
		if (isEQ(incrrefIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(incrrefIO.getCrrcd(),wsaaLastCrrcd)) {
			wsaaRefusals.add(1);
		}
		incrrefIO.setFunction(varcom.nextr);
		wsaaLastCrrcd.set(incrrefIO.getCrrcd());
		/*EXIT*/
	}

protected void incrrefIo3450()
	{
		/*START*/
		SmartFileCode.execute(appVars, incrrefIO);
		if (isNE(incrrefIO.getStatuz(),varcom.oK)
		&& isNE(incrrefIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrrefIO.getParams());
			syserrrec.statuz.set(incrrefIO.getStatuz());
			databaseError99500();
		}
		if (isNE(incrrefIO.getChdrcoy(),reverserec.company)
		|| isNE(incrrefIO.getChdrnum(),reverserec.chdrnum)
		|| isEQ(incrrefIO.getStatuz(),varcom.endp)) {
			incrrefIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void readT66583500()
	{
		start3500();
	}

protected void start3500()
	{
		/*  This section retrieves the Maximum Refusal No. from table*/
		/*  T6658.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItmfrm(incrIO.getCrrcd());
		itdmIO.setItemtabl(t6658);
		itdmIO.setItemitem(incrIO.getAnniversaryMethod());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			databaseError99500();
		}
		if (isNE(itdmIO.getItemcoy(),reverserec.company)
		|| isNE(itdmIO.getItemtabl(),t6658)
		|| isNE(itdmIO.getItemitem(),incrIO.getAnniversaryMethod())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(reverserec.company);
			itdmIO.setItemtabl(t6658);
			itdmIO.setItemitem(incrIO.getAnniversaryMethod());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h036);
			systemError99000();
		}
		t6658rec.t6658Rec.set(itdmIO.getGenarea());
	}

protected void calcNextDate4500()
	{
		calc4500();
		calcDate4507();
	}

protected void calc4500()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(covrrccIO.getCpiDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t6658rec.billfreq);
	}

protected void calcDate4507()
	{
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			databaseError99500();
		}
		covrrccIO.setCpiDate(datcon2rec.intDate2);
		getLifergpDetails5000();
		datcon3rec.intDate1.set(covrrccIO.getCpiDate());
		datcon3rec.intDate2.set(covrrccIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			databaseError99500();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaMinTrmToCess.set(datcon3rec.freqFactor);
		datcon3rec.intDate1.set(covrrccIO.getCrrcd());
		datcon3rec.intDate2.set(covrrccIO.getCpiDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			databaseError99500();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaFixNoIncr.set(datcon3rec.freqFactor);
		wsaaFreq.set(t6658rec.billfreq);
		compute(wsaaFixNoIncr, 0).set(mult(wsaaFixNoIncr,wsaaFreq));
		/*    IF  T6658-MAX-AGE        NOT > 0                             */
		/*        MOVE 99                 TO T6658-MAX-AGE.                */
		if (isLTE(t6658rec.agemax, 0)) {
			/*     MOVE 99                 TO T6658-AGEMAX.         <LA4754>*/
			t6658rec.agemax.set(999);
		}
		if (isLTE(t6658rec.fixdtrm,0)) {
			/*     MOVE 99                 TO T6658-FIXDTRM.                */
			t6658rec.fixdtrm.set(999);
		}
		/*    IF  T6658-MINCTRM            > WSAA-MIN-TRM-TO-CESS          */
		/*    OR  T6658-FIXDTRM            < WSAA-FIX-NO-INCR              */
		/*    OR  T6658-MAX-AGE            < WSAA-ANB                      */
		/*    OR  COVRRCC-CPI-DATE         > COVRRCC-RISK-CESS-DATE        */
		if (isGT(t6658rec.minctrm,wsaaMinTrmToCess)
		|| isLT(t6658rec.fixdtrm,wsaaFixNoIncr)
		|| isLT(t6658rec.agemax, wsaaAnb)
		|| isGT(covrrccIO.getCpiDate(),covrrccIO.getRiskCessDate())) {
			covrrccIO.setCpiDate(99999999);
		}
	}

protected void getLifergpDetails5000()
	{
		lifergp5005();
	}

protected void lifergp5005()
	{
		/* LIFE ASSURED AND JOINT LIFE DETAILS*/
		/* To obtain the LIFE assured and joint-LIFE details (if any) do*/
		/* the following;-*/
		/*  - read  the LIFE details using LIFERGP (LIFE number from*/
		/*       COVR, joint LIFE number '00').*/
		lifergpIO.setChdrcoy(covrrccIO.getChdrcoy());
		lifergpIO.setChdrnum(covrrccIO.getChdrnum());
		lifergpIO.setLife(covrrccIO.getLife());
		lifergpIO.setJlife("00");
		lifergpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifergpIO);
		if (isNE(lifergpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifergpIO.getParams());
			databaseError99500();
		}
		/*    Save Main LIFE details within Working Storage for later use.*/
		calculateAnb5500();
		/*  - read the joint LIFE details using LIFERGP (LIFE number*/
		/*       from COVR,  joint  LIFE number '01').  If found,*/
		/*       look up the name from the client details (CLTS) and*/
		/*       format as a "confirmation name".*/
		lifergpIO.setChdrcoy(covrrccIO.getChdrcoy());
		lifergpIO.setChdrnum(covrrccIO.getChdrnum());
		lifergpIO.setLife(covrrccIO.getLife());
		lifergpIO.setJlife("01");
		lifergpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifergpIO);
		if (isNE(lifergpIO.getStatuz(),varcom.oK)
		&& isNE(lifergpIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifergpIO.getParams());
			databaseError99500();
		}
		if (isNE(lifergpIO.getStatuz(),varcom.mrnf)) {
			calculateAnb5500();
		}
	}

protected void calculateAnb5500()
	{
			para5505();
		}

protected void para5505()
	{
		if (isEQ(lifergpIO.getCltdob(),ZERO)) {
			covrrccIO.setCpiDate(ZERO);
			return ;
		}
		/*    MOVE SPACES                 TO DTC3-FUNCTION.                */
		/*    MOVE LIFERGP-CLTDOB         TO DTC3-INT-DATE-1.              */
		/*    MOVE COVRRCC-CPI-DATE       TO DTC3-INT-DATE-2.              */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC.                       */
		/*    IF DTC3-STATUZ              NOT = O-K                        */
		/*        MOVE DTC3-DATCON3-REC   TO SYSR-PARAMS                   */
		/*        MOVE DTC3-STATUZ        TO SYSR-STATUZ                   */
		/*        PERFORM 99000-SYSTEM-ERROR.                              */
		/*    ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB.                 */
		/*  Identify Contract type                                         */
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setChdrcoy(covrrccIO.getChdrcoy());
		chdrlnbIO.setChdrnum(covrrccIO.getChdrnum());
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			databaseError99500();
		}
		/*  Identify FSU Company                                           */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(covrrccIO.getChdrcoy());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(reverserec.language);
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(lifergpIO.getCltdob());
		agecalcrec.intDate2.set(covrrccIO.getCpiDate());
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError99000();
		}
		wsaaAnb.set(agecalcrec.agerating);
	}

protected void systemError99000()
	{
			start99000();
			exit99490();
		}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError99500()
	{
			start99500();
			exit99990();
		}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}
}
