package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52I
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr52iScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(227);
	public FixedLengthStringData dataFields = new FixedLengthStringData(115).isAPartOf(dataArea, 0);
	public ZonedDecimalData aprem = DD.aprem.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData descripts = new FixedLengthStringData(48).isAPartOf(dataFields, 15);
	public FixedLengthStringData[] descript = FLSArrayPartOfStructure(2, 24, descripts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(48).isAPartOf(descripts, 0, FILLER_REDEFINE);
	public FixedLengthStringData descript01 = DD.descript.copy().isAPartOf(filler,0);
	public FixedLengthStringData descript02 = DD.descript.copy().isAPartOf(filler,24);
	public ZonedDecimalData gndtotal = DD.gndtotal.copyToZonedDecimal().isAPartOf(dataFields,63);
	public FixedLengthStringData keya = DD.keya.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData taxamts = new FixedLengthStringData(34).isAPartOf(dataFields, 81);
	public ZonedDecimalData[] taxamt = ZDArrayPartOfStructure(2, 17, 2, taxamts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(34).isAPartOf(taxamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData taxamt01 = DD.taxamt.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData taxamt02 = DD.taxamt.copyToZonedDecimal().isAPartOf(filler1,17);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 115);
	public FixedLengthStringData apremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData descriptsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] descriptErr = FLSArrayPartOfStructure(2, 4, descriptsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(descriptsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData descript01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData descript02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData gndtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData keyaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData taxamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] taxamtErr = FLSArrayPartOfStructure(2, 4, taxamtsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(taxamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData taxamt01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData taxamt02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 143);
	public FixedLengthStringData[] apremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData descriptsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] descriptOut = FLSArrayPartOfStructure(2, 12, descriptsOut, 0);
	public FixedLengthStringData[][] descriptO = FLSDArrayPartOfArrayStructure(12, 1, descriptOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(descriptsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] descript01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] descript02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] gndtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] keyaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData taxamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(2, 12, taxamtsOut, 0);
	public FixedLengthStringData[][] taxamtO = FLSDArrayPartOfArrayStructure(12, 1, taxamtOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(taxamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] taxamt01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] taxamt02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr52iscreenWritten = new LongData(0);
	public LongData Sr52iwindowWritten = new LongData(0);
	public LongData Sr52ihideWritten = new LongData(0);
	public LongData Sr52iprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52iScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {aprem, descript01, descript02, taxamt01, taxamt02, gndtotal, keya};
		screenOutFields = new BaseData[][] {apremOut, descript01Out, descript02Out, taxamt01Out, taxamt02Out, gndtotalOut, keyaOut};
		screenErrFields = new BaseData[] {apremErr, descript01Err, descript02Err, taxamt01Err, taxamt02Err, gndtotalErr, keyaErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52iscreen.class;
		protectRecord = Sr52iprotect.class;
		hideRecord = Sr52ihide.class;
	}

}
