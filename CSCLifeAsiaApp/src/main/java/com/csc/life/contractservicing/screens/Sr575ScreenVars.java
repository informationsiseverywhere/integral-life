package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR575
 * @version 1.0 generated on 30/08/09 07:20
 * @author Quipoz
 */
public class Sr575ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1137);
	public FixedLengthStringData dataFields = new FixedLengthStringData(449).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,114);
	public ZonedDecimalData matage = DD.matage.copyToZonedDecimal().isAPartOf(dataFields,161);
	public ZonedDecimalData mattcess = DD.mattcess.copyToZonedDecimal().isAPartOf(dataFields,164);
	public ZonedDecimalData mattrm = DD.mattrm.copyToZonedDecimal().isAPartOf(dataFields,172);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,175);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,176);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,177);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,180);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,183);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,187);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,191);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,201);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,202);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,219);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,222);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,226);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,241);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,258);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,259);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,272);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,289);
	/*BRD-306 START */
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 306);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 309);
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 339);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 356);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 373);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 390);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 407);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,424);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,432);
	/*BRD-306 END */
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(172).isAPartOf(dataArea, 449);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData matageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mattcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mattrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	/*BRD-306 START */
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);

	/*BRD-306 END */
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(516).isAPartOf(dataArea, 621);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] matageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mattcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mattrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	/*BRD-306 START */
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);

	/*BRD-306 END */
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData mattcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);

	public LongData Sr575screenWritten = new LongData(0);
	public LongData Sr575protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr575ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminOut,new String[] {"03","01","-03","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(matageOut,new String[] {"14","13","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mattrmOut,new String[] {"16","15","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mattcessOut,new String[] {"17","18","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"07","06","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"09","08","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premcessOut,new String[] {"10","11","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"24","23","-24","43",null, null, null, null, null, null, null, null});
		fieldIndMap.put(singprmOut,new String[] {"26","25","-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"34","36","-34","35",null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"41","60","-41","60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"32","31","-32","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {"55", "59", "55", "54",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"47","46","-47","46",null, null, null, null, null, null, null, null});
		/*BRD-306 START */
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"22","60","-22","32",null, null, null, null, null, null, null, null});
		/*BRD-306 END */
		/*ILIFE-3399-STRATS */
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "57", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "58", null, null, null, null, null, null, null, null});
		/*ILIFE-3399-ENDS */
		screenFields = new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifenum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, polinc, planSuffix, sumin, currcd, matage, mattrm, mattcess, premCessAge, premCessTerm, premcess, zlinstprem, liencd, singlePremium, mortcls, comind, optextind, select, taxamt, taxind,loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt,instPrem};
		screenOutFields = new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifenumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, polincOut, plnsfxOut, suminOut, currcdOut, matageOut, mattrmOut, mattcessOut, pcessageOut, pcesstrmOut, premcessOut, zlinstpremOut, liencdOut, singprmOut, mortclsOut, comindOut, optextindOut, selectOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut,instprmOut};
		screenErrFields = new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifenumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, polincErr, plnsfxErr, suminErr, currcdErr, matageErr, mattrmErr, mattcessErr, pcessageErr, pcesstrmErr, premcessErr, zlinstpremErr, liencdErr, singprmErr, mortclsErr, comindErr, optextindErr, selectErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr,instprmErr};
		screenDateFields = new BaseData[] {mattcess, premcess,effdate};
		screenDateErrFields = new BaseData[] {mattcessErr, premcessErr,effdateErr};
		screenDateDispFields = new BaseData[] {mattcessDisp, premcessDisp,effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr575screen.class;
		protectRecord = Sr575protect.class;
	}

}
