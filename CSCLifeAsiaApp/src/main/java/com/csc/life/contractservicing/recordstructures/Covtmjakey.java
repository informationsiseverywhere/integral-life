package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:29
 * Description:
 * Copybook name: COVTMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtmjaKey = new FixedLengthStringData(64).isAPartOf(covtmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(covtmjaKey, 0);
  	public FixedLengthStringData covtmjaChdrnum = new FixedLengthStringData(8).isAPartOf(covtmjaKey, 1);
  	public FixedLengthStringData covtmjaLife = new FixedLengthStringData(2).isAPartOf(covtmjaKey, 9);
  	public FixedLengthStringData covtmjaCoverage = new FixedLengthStringData(2).isAPartOf(covtmjaKey, 11);
  	public FixedLengthStringData covtmjaRider = new FixedLengthStringData(2).isAPartOf(covtmjaKey, 13);
  	public PackedDecimalData covtmjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covtmjaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covtmjaKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}