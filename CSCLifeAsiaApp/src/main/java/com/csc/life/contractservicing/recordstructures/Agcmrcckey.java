package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:00
 * Description:
 * Copybook name: AGCMRCCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmrcckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmrccFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmrccKey = new FixedLengthStringData(64).isAPartOf(agcmrccFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmrccChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmrccKey, 0);
  	public FixedLengthStringData agcmrccChdrnum = new FixedLengthStringData(8).isAPartOf(agcmrccKey, 1);
  	public FixedLengthStringData agcmrccLife = new FixedLengthStringData(2).isAPartOf(agcmrccKey, 9);
  	public FixedLengthStringData agcmrccCoverage = new FixedLengthStringData(2).isAPartOf(agcmrccKey, 11);
  	public FixedLengthStringData agcmrccRider = new FixedLengthStringData(2).isAPartOf(agcmrccKey, 13);
  	public PackedDecimalData agcmrccPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmrccKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(agcmrccKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmrccFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmrccFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}