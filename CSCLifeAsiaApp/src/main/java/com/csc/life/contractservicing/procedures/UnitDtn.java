package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.contractservicing.recordstructures.TaxDeductionRec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.tablestructures.T5645rec;

/**
 * <pre>
 * REMARKS
 * 				UNIT DEDUCTION SUBROUTINE
 * --------------------------------------------------------------------------------------
 * This subroutine is called by Contributions Tax Processing batch job. This subroutine
 * takes in COVR list,  T5645 List, Tax deduction rec and batcdorrec passed as parameters
 * from calling batch program.
 * 1. Read CHDR data passed by calling batch program.
 * 2. Retrieve T5645 data being passed by calling batch program.
 * 4. Write a ZRST record and build a coverage debt on primary coverage, since all the
 *    calculations are at policy level rather than coverage level.
 * 5. Write ACMV and return control back to calling program.
 * 6. Coverage Debt will be later deducted in the form of units by L2NEWUNITD.
 * </pre>
 * 
 * @author gsaluja2
 * @version 1.0
 * @since 05 February 2019
 *
 */

public class UnitDtn extends TaxComputeDtn{
	/*	Subroutine params	*/
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final String SPACES = " ";
	private static final String wsaaSubr = "UNITDTN";
	
	/*	Rec's Used	*/
	private Syserrrec syserrrec = new Syserrrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5645rec t5645rec = new T5645rec();
	
	/*	DAO Used	*/
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	
	/*	Objects Used	*/
	private Descpf descpf;
	private Chdrpf chdrpf;
	private Zrstpf zrstpf;
	private Varcom varcom = new Varcom();
	private Covrpf covrpf;
	
	/*	Lists Used	*/
	private List<Zrstpf> zrstList;
	private List<Zrstpf> zrstpfInsertList;
	
	/*	Variables Used	*/
	private int wsaaJrnseq;
	private BigDecimal wsaaTotalCd;
	private int wsaaSeqno;
	private boolean itemFound = false;
	private static final String h134 = "H134";
	private String trandesc;
	
	/*	Smart Table Used	*/
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	
	@Override
	public void mainline(Object... parmArray){
		trandesc = (String) parmArray[4];
		covrpf = (Covrpf) convertAndSetParam(covrpf, parmArray, 3); //to be uncommented
		t5645rec.t5645Rec = convertAndSetParam(t5645rec.t5645Rec, parmArray, 2);
		taxdrec = (TaxDeductionRec) convertAndSetParam(taxdrec, parmArray, 1);
		batcdorrec.batcdorRec = convertAndSetParam(batcdorrec.batcdorRec, parmArray, 0);
		try{
			startSubr();
		}catch(COBOLExitProgramException e) {
			/*	EXIT	*/
		}
	}
	
	protected void startSubr() {
		taxdrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		initialise();
		exitProgram();
	}
	
	protected void initialise() {
		chdrpf = new Chdrpf();
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		readZrst();
		writeUnitHoldings();
	}
	
	
	protected void readZrst() {
		zrstList = zrstpfDAO.searchZrstReccordByChdrnumFdbkInd(taxdrec.getChdrcoy(), taxdrec.getChdrnum());
		wsaaTotalCd = BigDecimal.ZERO;
		if(!zrstList.isEmpty()) {
			for(Zrstpf zrst: zrstList)
				wsaaTotalCd = wsaaTotalCd.add(zrst.getZramount01());
		}
		wsaaTotalCd = wsaaTotalCd.add(taxdrec.getTotalTaxAmt());
	}
	
	protected void writeUnitHoldings() {
		zrstpf = new Zrstpf();
		zrstpfInsertList = new ArrayList<>();
		updatePostings();
	}
	
	protected void setZrstpf() {
		zrstpf.setChdrcoy(taxdrec.getChdrcoy());
		zrstpf.setChdrnum(taxdrec.getChdrnum());
		zrstpf.setLife(covrpf.getLife());
		zrstpf.setJlife(SPACES);
		zrstpf.setCoverage(covrpf.getCoverage());
		zrstpf.setRider(covrpf.getRider());
		zrstpf.setBatctrcde(batcdorrec.trcde.toString());
		zrstpf.setTranno(chdrpf.getTranno()+1);
		zrstpf.setTrandate(taxdrec.getEffdate());
		zrstpf.setZramount01(lifacmvrec.origamt.getbigdata());
		++wsaaSeqno;
		zrstpf.setZramount02(wsaaTotalCd);
		zrstpf.setXtranno(0);
		zrstpf.setSeqno(wsaaSeqno);
		zrstpf.setSacstyp(lifacmvrec.sacstyp.toString());
		zrstpf.setUstmno(0);
		zrstpf.setFeedbackInd(SPACES);
		zrstpfInsertList.add(zrstpf);
	}
	
	protected void updatePostings() {
		postAcmv(batcdorrec,taxdrec,varcom,t5645rec,trandesc,chdrpf,covrpf);
		zrstpfDAO.insertZrstpfRecord(zrstpfInsertList);
		rewriteCovr();
	}
	

	protected void rewriteCovr() {
		covrpf.setCoverageDebt(wsaaTotalCd);
		if(!covrpfDAO.updateCrdebt(covrpf)) {
			fatalError();
		}
	}
	
	protected void callLifacmv() {
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		wsaaJrnseq++;
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError();
		}
	}
	
	@Override
	public  void dtnPostings(T5645rec t5645rec, Covrpf covrpf,TaxDeductionRec taxdrec,Lifacmvrec lifacmvrec,int wsaaJrnseq)
	{
		lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.rldgacct.set(taxdrec.getChdrnum()+covrpf.getLife()+covrpf.getCoverage()+covrpf.getRider()+new DecimalFormat("00").format(covrpf.getPlanSuffix()));
		callLifacmv(lifacmvrec,++wsaaJrnseq);
	}
	
}
