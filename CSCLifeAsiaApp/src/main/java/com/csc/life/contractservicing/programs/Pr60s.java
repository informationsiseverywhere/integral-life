/*
 * File: Pr60s.java
 * Date: 30 August 2009 0:49:15
 * Author: Quipoz Limited
 * 
 * Class transformed from Pr60s.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.SchmpfDAO;
import com.csc.fsu.general.dataaccess.model.Schmpf;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.contractservicing.screens.Sr60sScreenVars;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS
*
*               Pr60s - Contribution maintenance Submenu
*               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Overview
*~~~~~~~~
*
*This screen is to show the user what the system can  do  and
*what the user is allowed to do for billing changes.
*This is part of contract alterations.
*
*Table Entries
*~~~~~~~~~~~~~
*
*Create the following items in T1688:(already created)
*
*    SRG8 - Contribution Maintenance Submenu
*    TRG9 - Client Contract Scroll
*
*Create a continuation item in T1691 with the following:
*
*    item: EP5048A
*
*    item details:   Action = X
*                    Submenu Program = Pr60s
*                    Next action = A
*                    Transaction code = SRG8
*                    Description = Contribution Maintenance
*                    MENU code = L
*
*
*Create the item Pr60s in T1690 with the following:
*
*    Action = A
*    Key1 = Y
*    Key2 = Y
*    Key3 = ' '
*    NEXTPROG1 = P6668
*    NEXTPROG2 = NEXTPROG3 = NEXTPROG4 = space
*    Transaction code = T522
*    Batch required = Y
*
*
*Sr60s
*~~~~~
*
*Design a screen to have the followings:-
*
*     Contribution Maintenance Submenu          Sr60s 01
*
*
*          A - Client Contract Scroll
*
*
*
*Pr60s
*~~~~~
*
*We  should  now  have  a  program(Pr60s)  generated  by  the
*system(successful compilation of SR60S).
*
*
*1000-Initialisation Section
*~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*Read  T5679 with the transaction code(from WSSP-BATCHKEY) as
*the item  to  get  all  the  valid  status  codes  for  this
*particular transaction.
*
*Note: You have to set up the entry (ie T522) in T5679.
*
*
*2000-Screen-Edit Section
*~~~~~~~~~~~~~~~~~~~~~~~~
*
*Normal validations required for key1, key2 and key3.
*Use  the  captured  contract  number  to  read  the contract
*header file(CHDRMJA) to check if the  contract  is  a  valid
*contract.
*
*If no record found
*    Display message 'Contract not found'
*    Exit section.
*
*Check the risk and premium statii against those on T5679.
*
*If   they   are  not  on  T5679(invalid  contract  for  this
*transaction)
*    Display message 'Invalid contract'
*    Exit this section.
*
*If validflag not = '1'
*    Display message 'Invalid contract'
*    Exit this section.
*
*If    Bill-to-Date(BTDATE)    is    not    the    same    as
*Paid-to-Date(PTDATE)
*    Display message 'BTDATE not = PTDATE'
*    Exit this section.
*
*
*3000-Update Section
*~~~~~~~~~~~~~~~~~~~
*
*Set WSSP-FLAG to Sr60s-ACTION.
*
*Soft  lock  the  contract by calling 'SFTLOCK' with function
*'LOCK'.
*
*Call 'CHDRMJAIO' with function 'KEEPS' to store  a  copy  of
*the  contract  header  record in the working storage so that
*it can be retrieved by the subsequent programs.
*
*4000-Where-Next Section
*~~~~~~~~~~~~~~~~~~~~~~~
*
*No additional codings required.
*
*****************************************************************
* </pre>
*/
public class Pr60s extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR60S");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaStat = new FixedLengthStringData(1);
	private Validator wsaaStatOk = new Validator(wsaaStat, "Y");
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String f782 = "F782";
	private static final String rfxo = "RFXO";
	private static final String f321 = "F321";
	private static final String f910 = "F910";
	private static final String h945 = "H945";
	private static final String f442 = "F442";
	private static final String rfx9 = "RFX9";
	private static final String rg41 = "RG41";
	private static final String rfpr = "RFPR";  
	private static final String h137 = "H137";
	private static final String h387 = "H387";
	private static final String e331 = "E331";
	private FixedLengthStringData e058 = new FixedLengthStringData(4).init("E058");
		/* TABLES */
	private static final String t5679 = "T5679";
		/* Dummy user wssp. Replace with required version.*/
	
	private Batckey wsaaBatchkey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sr60sScreenVars sv = ScreenProgram.getScreenVars( Sr60sScreenVars.class);
	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	SchmpfDAO schmpfDAO = getApplicationContext().getBean("schmpfDAO", SchmpfDAO.class);
	ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	private int zctxpfCnt=0;
	Chdrpf chdrpf = null;
	Clntpf clntpf = null;
	Schmpf schmpf = null;
	List<Itempf> listT5679 = new ArrayList<Itempf>();
	private Clntkey wsaaClntkey = new Clntkey();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	




	public Pr60s() {
		super();
		screenVars = sv;
		new ScreenModel("Sr60s", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
		}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		wsspcomn.bchaction.set(" ");
		wsspcomn.clntkey.set(SPACES);
		wsspcomn.longconfname.set(SPACES);
		wsspcomn.chdrMplnum.set(SPACES);
		wsspwindow.value.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		wsspcomn.currto.set(SPACE); //ILIFE-8006
		
		/* Check against table*/
		subprogrec.action.set(wsspcomn.sbmaction);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			return ;
		}
		if (isNE(wsaaChdrnum, SPACES)) {
			unlkContract1100();
			wsaaChdrnum.set(SPACES);
		}
		listT5679 = itemDAO.getAllItemitem("IT",wsspcomn.company.toString(), t5679, subprogrec.transcd.toString());
		
		if (listT5679.size() == 0) {
			sv.actionErr.set(f321);
			return ;
		}
		
		boolean nbpropo7 = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPROP07", appVars, "IT");
		if(!nbpropo7)
		{
			sv.s290hideflag.set("Y");
		}
		chdrpf = new Chdrpf();
		if(chdrpfDAO.getCacheObject(chdrpf) != null)
		{
			chdrpfDAO.deleteCacheObject(chdrpf);
		}		
	}
protected void unlkContract1100()
{
	unlk1110();
}

protected void unlk1110()
{
	initialize(sftlockrec.sftlockRec);
	sftlockrec.function.set("UNLK");
	sftlockrec.company.set(wsspcomn.company);
	sftlockrec.enttyp.set("CH");
	sftlockrec.entity.set(wsaaChdrnum);
	sftlockrec.transaction.set(subprogrec.transcd);
	sftlockrec.user.set(varcom.vrcmUser);
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)
	&& isNE(sftlockrec.statuz, varcom.mrnf)) {
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'Sr60sIO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      Sr60s-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		checkAgainstTable2110();
			
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			return;
		}
		checkSanctions2120();	
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");

		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{	
			validateKey12210();	
		
	}

protected void validateKey12210()
	{
		int inputCnt = 0;
		if (isNE(sv.chdrsel,SPACES))
				inputCnt ++;
		if(isNE(sv.ownersel,SPACES))
				inputCnt++;
		if(isNE(sv.schmno,SPACES)) 
				inputCnt++;
		if(inputCnt > 1) {
			scrnparams.errorCode.set(rfxo);
			wsspcomn.edterror.set("Y");
			return;
		}
		if (isEQ(sv.chdrsel,SPACES) && isEQ(sv.ownersel,SPACES) && isEQ(sv.schmno,SPACES)) {
			scrnparams.errorCode.set(f442);
			wsspcomn.edterror.set("Y");
			return;
		}
		
		if(isNE(sv.chdrsel,SPACES))
			checkChdr();
		if(isNE(sv.ownersel,SPACES))
			checkclnt();
		if(isNE(sv.schmno,SPACES))
			checkschm();
	}

protected void checkChdr()
	{
		chdrpf = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), sv.chdrsel.toString());
		/* No record found*/
		if (chdrpf == null) {
			sv.chdrselErr.set(h945);
			return ;
		}
		checkStatus();
		if(isNE(sv.chdrselErr,SPACES)) {
			return;	
		}
		wsspcomn.chdrCnttype.set(chdrpf.getCnttype());
		Clntkey clntkey = new Clntkey();
		clntkey.clntClntpfx.set(chdrpf.getCownpfx());
		clntkey.clntClntnum.set(chdrpf.getCownnum());
		clntkey.clntClntcoy.set(chdrpf.getCowncoy());
		wsspcomn.clntkey.set(clntkey);
		zctxpfCnt =  zctxpfDAO.getZctxpfCnt(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1523 */
		if(isEQ(zctxpfCnt, ZERO.intValue())) {
			sv.chdrselErr.set(h387);
			return;
		}
		if(isEQ(sv.action,"C"))
		{
			wsspcomn.tranno.set(chdrpf.getTranno());
			wsspcomn.trancurr.set(chdrpf.getCntcurr());
			Itempf item = itemDAO.findItemByItem(chdrpf.getChdrcoy().toString(), "TR59X", chdrpf.getCnttype());
			if(item == null)
			{
				sv.chdrselErr.set("F732");
				return;
			}
			chdrpfDAO.setCacheObject(chdrpf);
		}		
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		
}

protected void checkStatus()
{
	for(Itempf itempf:listT5679) {
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* Check the risk and premium statii against those*/
		/* on T5679*/
		wsaaStat.set("N");
		wsaaSub.set(ZERO);
		while ( !(isGT(wsaaSub,12)
		|| wsaaStatOk.isTrue())) {
			wsaaSub.add(1);
			if (isGT(wsaaSub,12)) {
				sv.chdrselErr.set(h137);
				return ;
			}
			if (isEQ(chdrpf.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaStat.set("Y");
			}
		}
		
		wsaaStat.set("N");
		wsaaSub.set(ZERO);
		while ( !(isGT(wsaaSub,12)
		|| wsaaStatOk.isTrue())) {
			wsaaSub.add(1);
			if (isGT(wsaaSub,12)) {
				sv.chdrselErr.set(h137);
				return ;
			}
			if (isEQ(chdrpf.getPstcde(),t5679rec.cnPremStat[wsaaSub.toInt()])) {
				wsaaStat.set("Y");
			}
		}
	}
}
protected void checkclnt()
{
	clntpf = clntpfDAO.findClientByClntnum(sv.ownersel.toString());
	/* No record found*/
	if (clntpf == null) { // ILIFE-7697
		sv.ownerselErr.set(e058);
		return ;
	}
	if(isEQ(clntpf.getValidflag(),2)){
		sv.ownerselErr.set(f782);
		return ;
	}
	if(isEQ(chdrpfDAO.getChdrpfByKey("CN",sv.ownersel.toString(), wsspcomn.company.toString()).size() ,ZERO)) {
		sv.schmnoErr.set(e331);
		return;
	}
	initialize(sdasancrec.sancRec);
	sdasancrec.function.set("VENTY");
	sdasancrec.statuz.set(varcom.oK);
	sdasancrec.userid.set(wsspcomn.userid);
	sdasancrec.entypfx.set(fsupfxcpy.clnt);
	sdasancrec.entycoy.set(wsspcomn.fsuco);
	sdasancrec.entynum.set(sv.ownersel);
	callProgram(Sdasanc.class, sdasancrec.sancRec);
	if (isEQ(sdasancrec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(sdasancrec.statuz);
		syserrrec.params.set(sdasancrec.sancRec);
		fatalError600();
	}
	formatClientName();
}

protected void formatClientName()
{
	plainname();		
}

protected void plainname()
{
	/*PLAIN-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(), "C")) {
		corpname();
		return ;
	}
	if (isNE(clntpf.getGivname(), SPACES)) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getSurname(), "  ");
		stringVariable1.addExpression(", ");
		stringVariable1.addExpression(clntpf.getGivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
	}
	else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}
	/*PLAIN-EXIT*/
}

protected void corpname()
{
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(clntpf.getLsurname(), "  ");
	stringVariable1.addExpression(" ");
	stringVariable1.addExpression(clntpf.getLgivname(), "  ");
	stringVariable1.setStringInto(wsspcomn.longconfname);
	/*CORP-EXIT*/
}


protected void checkschm()
{
	schmpf = schmpfDAO.getSchmpfRecord(sv.schmno.toString());
	/* No record found*/
	if (schmpf == null) {
		sv.schmnoErr.set(rfx9);
		return ;
	}
	if(isEQ(chdrpfDAO.getchdrSchmRecord(sv.schmno.toString()) ,ZERO)) {
		sv.schmnoErr.set(e331);
		return;
	}
	
}
protected void verifyBatchControl2900()
	{
		validateRequest2910();
				
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			/*      MOVE E073               TO Sr60s-ACTION-ERR.             */
			sv.actionErr.set(e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2990-EXIT.                                          */
			return;
		}
		retrieveBatchProgs2920();
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2990-EXIT.                                          */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
				updateWssp3010();							
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsspcomn.bchaction.set("B");		
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			return;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/* Set WSSP-FLAG to Sr60s-ACTION*/
		wsspcomn.flag.set(sv.action);
		
		if(isNE(sv.chdrsel,SPACES)) {
			/* Soft lock contract.*/	
			wsspcomn.chdrChdrcoy.set(chdrpf.getChdrcoy());
			wsspcomn.chdrChdrnum.set(chdrpf.getChdrnum());
			wsspcomn.chdrMplnum.set(chdrpf.getChdrnum());
			if (isNE(wsspcomn.flag,"B")) {
			sftlockrec.sftlockRec.set(SPACES);
			sftlockrec.function.set("LOCK");
			sftlockrec.company.set(wsspcomn.company);
			sftlockrec.enttyp.set("CH");
			sftlockrec.entity.set(sv.chdrsel);
			sftlockrec.transaction.set(subprogrec.transcd);
			sftlockrec.user.set(varcom.vrcmUser);
			callProgram(Sftlock.class, sftlockrec.sftlockRec);
			if ((isNE(sftlockrec.statuz,varcom.oK))
			&& (isNE(sftlockrec.statuz,"LOCK"))) {
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
			if (isEQ(sftlockrec.statuz,"LOCK")) {
				sv.chdrselErr.set(f910);
				wsspcomn.edterror.set("Y");
				wsaaChdrnum.set(SPACES);
				return;
			}
			else {
				wsaaChdrnum.set(sv.chdrsel);
			}
			}
		}
		if(isNE(sv.ownersel,SPACES)) {
			/* Soft lock contract.*/	
			wsaaClntkey.clntClntpfx.set("CN");
			wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
			wsaaClntkey.clntClntnum.set(sv.ownersel);
			wsspcomn.clntkey.set(wsaaClntkey);
			
		}
		if(isNE(sv.schmno,SPACES)) {
			/* Soft lock contract.*/	
			wsspwindow.value.set(sv.schmno);			
		}
		batching3080();	
	}


protected void batching3080()
	{
		if ((isEQ(subprogrec.bchrqd,"Y"))
		&& (isEQ(sv.errorIndicators,SPACES))) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
