/*
 * File: P6753.java
 * Date: 30 August 2009 0:55:51
 * Author: Quipoz Limited
 * 
 * Class transformed from P6753.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.CwfdTableDAM;
import com.csc.life.contractservicing.dataaccess.CwfdregTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnwfdTableDAM;
import com.csc.life.contractservicing.screens.S6753ScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
* REMARKS.
* --------
*
*                    Display Windforward List
*                    ========================
*
* This screen is displayed when the user selects:
*
*        - 'Windforward Confirmation' from the submenu
*        - 'Cancel Windforward' from the submenu
*        - 'Display Windforward List' from the submenu
*        - 'Display Windforward' function key from the
*           Windforward Registration Screen.
*
* The screen displays some basic Contract Header information and
* then loads Windforward records for the contract onto the screen
* in ascending sequence number order. The fields on the subfile
* are as follows:
*
*                - Selection
*                - Sequence Number
*                - Effective Date
*                - Transaction Code
*                - Transaction Description
*
* When the screen is displayed via 'Windforward Confirmation' or
* 'Cancel Windforward' from the submenu, all sub-file lines will
* be protected from input. The next screen for both actions will
* be the Windforward Confirmation screen which will ensure that
* the user wants to complete the transaction.
*
* When the screen is displayed via 'Display Windforward List'
* from the submenu or by switching from the Windforward
* Registration screen, windforward records may be deleted by
* selecting them.  Two non-screen programs perform the necessary
* processing for this operation.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6753 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(P6753.class);//IJTI-318
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6753");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaLineCount = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-VARIABLES */
	private ZonedDecimalData l = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaRrn = new PackedDecimalData(9, 0);

	private FixedLengthStringData wsaaTranskey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaCntype = new FixedLengthStringData(3).isAPartOf(wsaaTranskey, 0);
	private FixedLengthStringData wsaaTrcode = new FixedLengthStringData(4).isAPartOf(wsaaTranskey, 3);
		/* WSAA-SUBFILE-FIELDS */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaPrcSeqNbr = new ZonedDecimalData(5, 0);

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1).init("N");
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");
	private ZonedDecimalData wsaaOptionCode = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
		/* ERRORS */
	private String h234 = "H234";
	private String o001 = "O001";
		/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String descrec = "DESCREC";
	private String cltsrec = "CLTSREC";
	private String cwfdrec = "CWFDREC";
	private String chdrenqrec = "CHDRENQREC";
	private String lifeenqrec = "LIFEENQREC";
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Contract Windforward Logical*/
	private CwfdTableDAM cwfdIO = new CwfdTableDAM();
		/*Contract Windforward file by Tranno*/
	private CwfdregTableDAM cwfdregIO = new CwfdregTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*PTRN Logical File for windforward*/
	private PtrnwfdTableDAM ptrnwfdIO = new PtrnwfdTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatcKey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S6753ScreenVars sv = ScreenProgram.getScreenVars( S6753ScreenVars.class);
	private String wsaaDeleteOccurred = "N";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1190, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		updateSubfile2270, 
		exit3190,
		switch4070, 
		updateSubfile4170, 
		exit4190
	}

	public P6753() {
		super();
		screenVars = sv;
		new ScreenModel("S6753", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaDeleteOccurred = "N";
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.prcSeqNbr.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6753", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		chdrenqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.set(SPACES);
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.set(SPACES);
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrenqIO.getPstatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.set(SPACES);
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		sv.chdrsel.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getRegister());
		getLifeDetails1100();
		cwfdIO.setDataArea(SPACES);
		cwfdIO.setChdrcoy(wsspcomn.company);
		cwfdIO.setChdrnum(chdrenqIO.getChdrnum());
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		cwfdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		cwfdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		cwfdIO.setFormat(cwfdrec);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)
		&& isNE(cwfdIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		if (isNE(cwfdIO.getChdrcoy(),wsspcomn.company)
		|| isNE(cwfdIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			cwfdIO.setStatuz(varcom.endp);
		}
		for (wsaaLineCount.set(1); !(isEQ(cwfdIO.getStatuz(),varcom.endp)
		|| isGT(wsaaLineCount,sv.subfilePage)); wsaaLineCount.add(1)){
			processCwfd1200();
		}
		if (isNE(cwfdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set("N");
		}
		wsaaRrn.set(scrnparams.subfileRrn);
	}

protected void getLifeDetails1100()
	{
		try {
			begn1110();
		}
		catch (GOTOException e){
		}
	}

protected void begn1110()
	{
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		lifeenqIO.setFormat(lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isNE(lifeenqIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(lifeenqIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			lifeenqIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(lifeenqIO.getStatuz());
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeenqIO.getStatuz(),varcom.mrnf)) {
			sv.jlinsname.set(SPACES);
			sv.jownnum.set(SPACES);
			goTo(GotoLabel.exit1190);
		}
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		sv.jownnum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void processCwfd1200()
	{
		go1210();
	}

protected void go1210()
	{
		wsaaPrcSeqNbr.set(cwfdIO.getPrcSeqNbr());
		wsaaEffdate.set(cwfdIO.getEfdate());
		wsaaTrcode.set(cwfdIO.getTrancd());
		wsaaCntype.set(chdrenqIO.getCnttype());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(cwfdIO.getTrancd());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.descrip.set(SPACES);
		}
		else {
			sv.descrip.set(descIO.getLongdesc());
		}
		sv.trancd.set(cwfdIO.getTrancd());
		sv.efdate.set(wsaaEffdate);
		sv.prcSeqNbr.set(wsaaPrcSeqNbr);
		if (isNE(wsspcomn.flag,"E")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");			
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6753", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		cwfdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)
		&& isNE(cwfdIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		if (isNE(cwfdIO.getChdrcoy(),wsspcomn.company)
		|| isNE(cwfdIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			cwfdIO.setStatuz(varcom.endp);
		}
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
			screenIoTwo2030();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*COMPLETE-SCREEN*/
		l.set(1);
		for (ix.set(1); !(isGT(ix,20)); ix.add(1)){
			if (isEQ(optswchrec.optsType[ix.toInt()],"L")) {
				sv.optdsc[l.toInt()].set(optswchrec.optsDsc[ix.toInt()]);
				l.add(1);
			}
		}
	}

protected void screenIoTwo2030()
	{
		if (isNE(wsspcomn.flag,"M")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2040();
			checkForErrors2050();
			if (isEQ(wsaaScrnStatuz,varcom.delt)
					|| isEQ(wsaaScrnStatuz,varcom.insr)) {
						checkFunction2300();
			}
		}
		catch (GOTOException e){
		}
	}

protected void checkFunction2300()
{
	check2310();
}

protected void check2310()
{
	optswchrec.optsSelCode.set(wsaaScrnStatuz);
	optswchrec.optsSelType.set("F");
	optswchrec.optsSelOptno.set(ZERO);
	optswchrec.optsFunction.set("CHCK");
	optswchrec.optsCallingProg.set(wsaaProg);
	optswchrec.optsDteeff.set(ZERO);
	optswchrec.optsCompany.set(wsspcomn.company);
	optswchrec.optsLanguage.set(wsspcomn.language);
	varcom.vrcmTranid.set(wsspcomn.tranid);
	optswchrec.optsUser.set(varcom.vrcmUser);
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if (isNE(optswchrec.optsStatuz,varcom.oK)) {
		if (isEQ(wsaaScrnStatuz,varcom.insr)) {
			sv.hflag01Err.set(optswchrec.optsStatuz);
		}
		else {
			sv.hflag02Err.set(optswchrec.optsStatuz);
		}
		wsspcomn.edterror.set("Y");
	}
}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaScrnStatuz.set(scrnparams.statuz);
	}

protected void validateScreen2040()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			for (wsaaLineCount.set(1); !(isEQ(cwfdIO.getStatuz(),varcom.endp)
			|| isGT(wsaaLineCount,sv.subfilePage)); wsaaLineCount.add(1)){
				processCwfd1200();
			}
			if (isNE(cwfdIO.getStatuz(),varcom.endp)) {
				scrnparams.subfileMore.set("Y");
			}
			else {
				scrnparams.subfileMore.set("N");
			}
			wsspcomn.edterror.set("Y");
			wsaaRrn.set(scrnparams.subfileRrn);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.sbmaction,"D")
		|| isEQ(wsspcomn.sbmaction,"F")
		|| isEQ(wsspcomn.sbmaction,"E")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForErrors2050()
	{
		validateHeader2100();
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.srnch);
		processScreen("S6753", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2200();
		}
		
		/*CONTINUE*/
		scrnparams.subfileRrn.set(0);
	}

protected void validateHeader2100()
	{
		/*VALIDATE-HEADER*/
		if (isEQ(sv.optdsc[1],SPACES)) {
			sv.optdsc01Err.set(h234);
		}
		/*EXIT*/
	}

protected void validateSubfile2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSubfile2210();
				}
				case updateSubfile2270: {
					updateSubfile2270();
					readNextChange2280();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSubfile2210()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateSubfile2270);
		}
		if (isNE(sv.select,NUMERIC)) {
			sv.selectErr.set(o001);
			goTo(GotoLabel.updateSubfile2270);
		}
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(sv.select);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			sv.selectErr.set(optswchrec.optsStatuz);
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateSubfile2270()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6753", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextChange2280()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6753", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
{
	/*  Bypass this section if returning from a previous use of this*/
	/*  program*/ 
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
	|| isEQ(wsspcomn.flag, "I")
	|| isEQ(scrnparams.statuz, "KILL")) {
		return ;
	}
	
	scrnparams.statuz.set(varcom.oK);
	scrnparams.subfileRrn.set(1);
	scrnparams.function.set(varcom.sstrt);
	while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
		readSubfile3100();
	}

}

protected void readSubfile3100()
{
	try {
		callScreen3110();
		itdmsmt3120();
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
	}
}

protected void callScreen3110()
{
	processScreen("S6753", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	scrnparams.function.set(varcom.srdn);
	if ((isNE(sv.select, "1")
	&& isNE(sv.select, "9")
	&& isNE(sv.select, "*"))
	|| isEQ(scrnparams.statuz, varcom.endp)) {
		goTo(GotoLabel.exit3190);
	}
	/**    IF WSSP-FLAG                = 'I'*/
	/**    AND S0025-SELECT            = '1'*/
	/**        GO TO 3190-EXIT*/
	/**    END-IF.*/
}

protected void itdmsmt3120()
{
	cwfdIO.setDataArea(SPACES);
	cwfdIO.setChdrcoy(wsspcomn.company);
	cwfdIO.setChdrnum(chdrenqIO.getChdrnum());
	cwfdIO.setPrcSeqNbr(sv.prcSeqNbr);
	if (isEQ(sv.select, "9")) {
		cwfdIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		} else {
			wsaaDeleteOccurred = "Y";
		}
	}
	/* If we are deleteing or performing an automatic update of the*/
	/* dates we do not want to leave this program. Hence we will*/
	/* clear the select field as we have completed the task.*/
	if (isNE(sv.select, "1")) {
		//sv.select.set(SPACES);
		updateScreen2500();
		scrnparams.function.set(varcom.srdn);
	}
}



protected void updateScreen2500()
{
	scrnparams.function.set(varcom.supd);
	processScreen("S6753", sv);
	if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case switch4070: {
					switch4070();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		LOGGER.info(wsspcomn.sbmaction.toString());//IJTI-318
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			if (isEQ(wsspcomn.sbmaction,"D")
			|| isEQ(wsspcomn.sbmaction,"E")
			|| isEQ(wsspcomn.sbmaction,"F")) {
				releaseSoftlock4200();
			}
			goTo(GotoLabel.switch4070);
		}
		if (isEQ(wsspcomn.sbmaction,"F")) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
				releaseSoftlock4200();
			}
			else {
				storeChdr4400();
				optswchrec.optsSelType.set("I");
				optswchrec.optsSelOptno.set(1);
			}
			goTo(GotoLabel.switch4070);
		}
		wsaaFoundSelection.set("N");
		scrnparams.subfileRrn.set(0);
		while ( !(isEQ(scrnparams.statuz,varcom.mrnf)
		|| foundSelection.isTrue())) {
			lineSels4100();
		}
		
		if (isEQ(wsspcomn.sbmaction,"D") || isEQ(wsspcomn.sbmaction,"E")) {
			storeChdr4400();
		}
		if (isEQ(wsspcomn.sbmaction,"E")
		&& !foundSelection.isTrue()) {
		// ILIFE-7726
		//&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			releaseSoftlock4200();
		}
	}

protected void switch4070()
	{
	if(isEQ(wsaaDeleteOccurred, "N")){
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			loadSubfile4300();
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		
	} else {
		storeChdr4400();
		wsaaDeleteOccurred = "N";
		optswchrec.optsSelType.set("I");
		optswchrec.optsSelOptno.set(1);	
	}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		}
	}

protected void lineSels4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					lineSels4110();
					keepsForLineSelectPgm4120();
				}
				case updateSubfile4170: {
					updateSubfile4170();
				}
				case exit4190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void lineSels4110()
	{
		scrnparams.subfileRrn.add(1);
		scrnparams.function.set(varcom.sread);
		processScreen("S6753", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.mrnf)) {
			goTo(GotoLabel.exit4190);
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.exit4190);
		}
		wsaaOptionCode.set(sv.select);
		for (ix.set(1); !(isGT(ix,20)); ix.add(1)){
			if (isEQ(optswchrec.optsNo[ix.toInt()],wsaaOptionCode)) {
				optswchrec.optsInd[ix.toInt()].set("X");
				wsaaFoundSelection.set("Y");
				wsaaCntype.set(sv.cnttype);
				wsaaTrcode.set(sv.trancd);
				wssplife.keysoptItem.set(wsaaTranskey);
			}
		}
		if (!foundSelection.isTrue()) {
			goTo(GotoLabel.updateSubfile4170);
		}
	}

protected void keepsForLineSelectPgm4120()
	{
		wsaaRrn.set(scrnparams.subfileRrn);
		if (isEQ(wsaaOptionCode,1)) {
			storeChdr4400();
			cwfdIO.setDataArea(SPACES);
			cwfdIO.setChdrcoy(wsspcomn.company);
			cwfdIO.setChdrnum(chdrenqIO.getChdrnum());
			cwfdIO.setPrcSeqNbr(sv.prcSeqNbr);
			cwfdIO.setFunction(varcom.reads);
			SmartFileCode.execute(appVars, cwfdIO);
			if (isNE(cwfdIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cwfdIO.getParams());
				syserrrec.statuz.set(cwfdIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void updateSubfile4170()
	{
		sv.select.set(SPACES);
		scrnparams.function.set(varcom.supd);
		processScreen("S6753", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void releaseSoftlock4200()
	{
		release4210();
	}

protected void release4210()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrenqIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void loadSubfile4300()
	{
		para4310();
	}

protected void para4310()
	{
		scrnparams.function.set(varcom.sclr);
		processScreen("S6753", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		cwfdIO.setDataArea(SPACES);
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setChdrcoy(wsspcomn.company);
		cwfdIO.setChdrnum(chdrenqIO.getChdrnum());
		cwfdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		cwfdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		cwfdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(),varcom.oK)
		&& isNE(cwfdIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(cwfdIO.getParams());
			syserrrec.statuz.set(cwfdIO.getStatuz());
			fatalError600();
		}
		if (isNE(cwfdIO.getChdrcoy(),wsspcomn.company)
		|| isNE(cwfdIO.getChdrnum(),chdrenqIO.getChdrnum())) {
			cwfdIO.setStatuz(varcom.endp);
		}
		for (wsaaLineCount.set(1); !(isEQ(cwfdIO.getStatuz(),varcom.endp)
		|| isGT(wsaaLineCount,sv.subfilePage)); wsaaLineCount.add(1)){
			processCwfd1200();
		}
		if (isNE(cwfdIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set("N");
		}
		wsaaRrn.set(scrnparams.subfileRrn);
	}

protected void storeChdr4400()
	{
		/*KEEPS*/
		chdrenqIO.setFunction(varcom.keeps);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
}
