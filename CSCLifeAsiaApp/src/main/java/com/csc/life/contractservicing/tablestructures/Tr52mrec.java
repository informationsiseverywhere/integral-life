package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:26
 * Description:
 * Copybook name: TR52MREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr52mrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr52mRec = new FixedLengthStringData(500);
  	public ZonedDecimalData admfee = new ZonedDecimalData(12, 2).isAPartOf(tr52mRec, 0);
  	public FixedLengthStringData admpcs = new FixedLengthStringData(6).isAPartOf(tr52mRec, 12);
  	public ZonedDecimalData[] admpc = ZDArrayPartOfStructure(2, 3, 0, admpcs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(admpcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData admpc01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData admpc02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public FixedLengthStringData basind = new FixedLengthStringData(1).isAPartOf(tr52mRec, 18);
  	public FixedLengthStringData fieldEditCode = new FixedLengthStringData(1).isAPartOf(tr52mRec, 19);
  	public FixedLengthStringData medpcs = new FixedLengthStringData(6).isAPartOf(tr52mRec, 20);
  	public ZonedDecimalData[] medpc = ZDArrayPartOfStructure(2, 3, 0, medpcs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(medpcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData medpc01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData medpc02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public FixedLengthStringData ratebas = new FixedLengthStringData(1).isAPartOf(tr52mRec, 26);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(473).isAPartOf(tr52mRec, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr52mRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr52mRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}