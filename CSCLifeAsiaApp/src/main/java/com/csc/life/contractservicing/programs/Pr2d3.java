/*
 * File: Pr2d3.java
 * Date: 3 December 2011 10:41:26 PM
 * Author: Quipoz Limited
 * 
 * Class transformed from Pr2d3.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.programs.P2465;
import com.csc.fsu.clients.recordstructures.Clprkey;
import com.csc.fsu.clients.screens.Sr2d3ScreenVars;
import com.csc.fsu.general.dataaccess.model.Chdrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3; //ILIFE-2390
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec; //ILIFE-2390
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.procedures.UwoccupationUtil;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.ZlifelcTableDAM;
import com.csc.life.newbusiness.tablestructures.Tr627rec;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
* </pre>
*/
public class Pr2d3 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR2D3");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaSmoking = new FixedLengthStringData(2);
	private PackedDecimalData wsaaCnt = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaRrn = new PackedDecimalData(7, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaValidStatcode = new FixedLengthStringData(1);
	private Validator validStatcode = new Validator(wsaaValidStatcode, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1);
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");
	private ZonedDecimalData wsaaDobPlusTr627 = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaFndFlg = new FixedLengthStringData(1).init(SPACES);
		/* ERRORS */
	private static final String g813 = "G813";
	private static final String g957 = "G957";
	private static final String f910 = "F910";
	private static final String rlar = "RLAR";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t5679 = "T5679";
	private static final String tr627 = "TR627";
	private static final String t3623 = "T3623";
	private static final String zlifelcrec = "ZLIFELCREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String itemrec = "ITEMREC";
	private static final String payrrec = "PAYRREC";
	private static final String lifemjarec = "LIFEMJAREC";
	private static final String hpadrec = "HPADREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private ZlifelcTableDAM zlifelcIO = new ZlifelcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Optswchrec optswchrec = new Optswchrec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Tr627rec tr627rec = new Tr627rec();
	private Sr2d3ScreenVars sv = ScreenProgram.getScreenVars( Sr2d3ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private Clprkey wsaaClprkey = new Clprkey();
	private DescTableDAM descIO = new DescTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec(); //ILIFE-2390
	private StringUtil stringUtil = new StringUtil();
	private List<Chdrpf> contractList = new ArrayList<>();
	private UwoccupationUtil uwoccupationUtil = getApplicationContext().getBean("uwoccupationUtil", UwoccupationUtil.class);
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private boolean uwFlag = false;

	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		readSfl3020, 
		exit3090, 
		nextr5080
	}

	public Pr2d3() {
		super();
		screenVars = sv;
		new ScreenModel("Sr2d3", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void largename() {
		/* LGNM-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* LGNM-EXIT */
	}

	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();

		} else if (isNE(cltsIO.getGivname(), SPACES)) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = ",";

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);

		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* PLAIN-EXIT */
	}

	protected void payeename() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else if (isEQ(cltsIO.getEthorig(), "1")) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = "";
			String salute = cltsIO.getSalutl().toString();

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = getStringUtil().includeSalute(salute, fullName);

			wsspcomn.longconfname.set(fullNameWithSalute);
		} else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable2.addExpression(". ");
			stringVariable2.addExpression(cltsIO.getGivname(), "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(cltsIO.getSurname(), "  ");
			stringVariable2.setStringInto(wsspcomn.longconfname);
		}
		/* PAYEE-EXIT */
	}

	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = cltsIO.getLgivname().toString();
		String lastName = cltsIO.getLsurname().toString();
		String delimiter = "";

		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/* CORP-EXIT */
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.ccdate.set(varcom.maxdate);
		sv.ptdate.set(varcom.maxdate);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaCnt.set(0);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		initOptswch1300();
		/*cltsIO.setParams(SPACES);
		cltsIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		
		cltsIO.setDataKey(wsspcomn.clntkey);
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		
		
		sv.clntnum.set(cltsIO.getClntnum());
		plainname();
		wsaaClprkey.set(wsspcomn.clntkey);
		sv.clntname.set(wsspcomn.longconfname);
		sv.cltsex.set(cltsIO.getCltsex()); //ILIFE-2392 
		/*zlifelcIO.setDataKey(SPACES);
		zlifelcIO.setParams(SPACES);
		//zlifelcIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, zlifelcIO);
		if (isNE(zlifelcIO.getStatuz(),varcom.oK)
		&& isNE(zlifelcIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zlifelcIO.getParams());
			syserrrec.statuz.set(zlifelcIO.getStatuz());
			fatalError600();
		}
		wsaaSmoking.set(zlifelcIO.getSmoking());*/
		readT56791100();
		contractList.clear();
		setSubfile1200();
		checkImpactedContracts();
	}

	private void checkImpactedContracts() {
		uwFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP123", appVars, smtpfxcpy.item.toString());
		if (uwFlag) {
			String originalOccu = (String)ThreadLocalStore.get(P2465.ORIGINAL_OCCUPATION_CACHE);
			String originalIndustry = (String)ThreadLocalStore.get(P2465.ORIGINAL_INDUSTRY_CACHE);
			sv.uwFlag = 1;
			UWOccupationRec uwOccupationRec = new UWOccupationRec();
			uwOccupationRec.transEffdate.set(wsaaToday);
			uwOccupationRec.occupCode.set(cltsIO.getOccpcode());
			uwOccupationRec.indusType.set(cltsIO.getStatcode());
			
			UWOccupationRec origUWOccupationRec = new UWOccupationRec();
			origUWOccupationRec.transEffdate.set(wsaaToday);
			origUWOccupationRec.occupCode.set(originalOccu);
			origUWOccupationRec.indusType.set(originalIndustry);
			sv.chdrCheckMap = new HashMap<>();
			sv.chdrCheckMap.put("Decline", uwoccupationUtil.getUnacceptableContracts(contractList,
					wsspcomn.company.toString(), uwOccupationRec, 
					wsspcomn.fsuco.toString(), origUWOccupationRec));
		} else {
			sv.uwFlag = 2;
		}
	}

protected void readT56791100()
	{
		start1110();
	}

protected void start1110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaProg);
		/*StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());*/
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void setSubfile1200()
	{
		start1210();
	}

protected void start1210()
	{
		scrnparams.function.set(varcom.sclr);
		callSr2d3io1900();
		scrnparams.subfileRrn.set(1);
		zlifelcIO.setDataKey(SPACES);
		zlifelcIO.setParams(SPACES);
		zlifelcIO.setChdrcoy(wsspcomn.company);
		zlifelcIO.setLifcnum(cltsIO.getClntnum());
		zlifelcIO.setFormat(zlifelcrec);
		zlifelcIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, zlifelcIO);
		if (isNE(zlifelcIO.getStatuz(),varcom.oK)
		&& isNE(zlifelcIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zlifelcIO.getParams());
			syserrrec.statuz.set(zlifelcIO.getStatuz());
			fatalError600();
		}
		if (isEQ(zlifelcIO.getStatuz(), varcom.endp)
		|| isNE(zlifelcIO.getChdrcoy(), wsspcomn.company)
		|| isNE(zlifelcIO.getLifcnum(), cltsIO.getClntnum())) {
			return ;
		}
		while ( !(isEQ(zlifelcIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaCnt,sv.subfilePage))) {
			loadSubfile5000();
		}
		
	}

protected void initOptswch1300()
	{
		start1310();
	}

protected void start1310()
	{
		optswchrec.optsFunction.set(varcom.init);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}

protected void callSr2d3io1900()
	{
		/*START*/
		processScreen("SR2D3", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callSr2d3io1900a()
	{
		/*A-START*/
		processScreen("SR2D3", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*A-EXIT*/
	}

protected void preScreenEdit()
	{
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
		wsspcomn.edterror.set(varcom.oK);
		try {
			/*screenIo2010();
			validateScreen2010();
			checkForErrors2050();*/
			validateScreen2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			sv.errorIndicators.set(SPACES);
			wsaaCnt.set(0);
			contractList.clear();
			while ( !(isEQ(zlifelcIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaCnt,sv.subfilePage))) {
				loadSubfile5000();
			}
			checkImpactedContracts();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,"INSR")) {
			startSubfile2200();
			while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
				insertValSubfile2300();
			}
			
			wsspcomn.edterror.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.sstrt);
		scrnparams.subfileRrn.set(1);
		callSr2d3io1900a();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void sftlockChdr2100()
	{
		lockChdr2110();
	}

protected void lockChdr2110()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.fsuco);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sv.chdrnum);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.selectErr.set(f910);
		}
	}

protected void startSubfile2200()
	{
		/*START*/
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		scrnparams.subfileRrn.set(1);
		processScreen("SR2D3", sv);
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			scrnparams.subfileRrn.set(1);
		}
		else {
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void insertValSubfile2300()
	{
		/*START*/
		/*sv.select.set("1");*/
		/*UPDATE-ERROR-INDICATORS*/
		scrnparams.function.set(varcom.supd);
		callSr2d3io1900();
		/*READ-NEXT-RECORD*/
		scrnparams.function.set(varcom.srdn);
		callSr2d3io1900a();
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
					validation2610();
					updateErrorIndicators2670();
				}

protected void validation2610()
	{
		if (isNE(sv.select,SPACES)) {
			if (isEQ(sv.select,"1")) {
				sftlockChdr2100();
			}
			else {
				sv.selectErr.set(g813);
				return ;
			}
			chckSelection2700();
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				sv.selectErr.set(optswchrec.optsStatuz);
			}
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		callSr2d3io1900();
		/*READ-NEXT-MODIFIED-RECORD*/
		scrnparams.function.set(varcom.srdn);
		callSr2d3io1900a();
		/*EXIT*/
	}

protected void chckSelection2700()
	{
		start2710();
	}

protected void start2710()
	{
		optswchrec.optsStatuz.set(varcom.oK);
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(wsaaToday);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		/*while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					startSfl3010();
				case readSfl3020: 
					readSfl3020();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}*/
	}

protected void startSfl3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		scrnparams.function.set(varcom.sstrt);
	}

protected void readSfl3020()
	{
		callSr2d3io1900a();
		if (isNE(scrnparams.statuz,varcom.oK)) {
			goTo(GotoLabel.exit3090);
		}
		if (isNE(sv.select,SPACES)) {
			updateLife3100();
		}
		/*NEXT-SFL*/
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.readSfl3020);
	}

protected void updateLife3100()
	{
		rlseLife3110();
		readLife3120();
		getPaidToDate3130();
		setValidflag23140();
		validflag13150();
	}

protected void rlseLife3110()
	{
		zlifelcIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, zlifelcIO);
		if (isNE(zlifelcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zlifelcIO.getParams());
			syserrrec.statuz.set(zlifelcIO.getStatuz());
			fatalError600();
		}
	}

protected void readLife3120()
	{
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(sv.chdrnum);
		lifemjaIO.setLife(sv.life);
		lifemjaIO.setJlife(sv.jlife);
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readh);
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
	}

protected void getPaidToDate3130()
	{
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(sv.chdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void setValidflag23140()
	{
		lifemjaIO.setCurrto(payrIO.getPtdate());
		lifemjaIO.setValidflag("2");
		lifemjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
	}

protected void validflag13150()
	{
		sv.tranno.add(1);
		lifemjaIO.setTranno(sv.tranno);
		lifemjaIO.setCurrfrom(payrIO.getPtdate());
		lifemjaIO.setCurrto(varcom.vrcmMaxDate);
		lifemjaIO.setCltsex(cltsIO.getCltsex());
		lifemjaIO.setCltdob(cltsIO.getCltdob());
		lifemjaIO.setOccup(cltsIO.getOccpcode());
		lifemjaIO.setSmoking(wsaaSmoking); 
		lifemjaIO.setAnbAtCcd(sv.anbage);
		lifemjaIO.setValidflag("1");
		lifemjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		readHpad7100();
		if (isEQ(wsaaFndFlg,"Y")) {
			hpadProcess7200();
		}
	}

protected void releaseClient3500()
	{
		release3510();
	}

protected void release3510()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.fsuco);
		sftlockrec.enttyp.set("CN");
		sftlockrec.entity.set(sv.clntnum);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		wsspcomn.programPtr.add(1);
	}

protected void releaseContract3700()
{
	rlseChdr3710();
}

protected void rlseChdr3710()
{
	sftlockrec.sftlockRec.set(SPACES);
	sftlockrec.company.set(wsspcomn.company);
	sftlockrec.entity.set(wsspcomn.chdrChdrnum);
	sftlockrec.enttyp.set("CH");
	sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
	sftlockrec.user.set(varcom.vrcmUser);
	sftlockrec.function.set("UNLK");
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz,varcom.oK)) {
		syserrrec.params.set(sftlockrec.sftlockRec);
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}

protected void optswchCall4500()
	{
		call4510();
	}

protected void call4510()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,varcom.oK))
		&& (isNE(optswchrec.optsStatuz,varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void loadSubfile5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5010();
				}
				case nextr5080: {
					nextr5080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5010()
	{
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(zlifelcIO.getChdrcoy());
		chdrmjaIO.setChdrnum(zlifelcIO.getChdrnum());
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.sbmaction,"B")
		&& isNE(wsspcomn.chdrChdrnum, SPACES)
		&& isNE(chdrmjaIO.getChdrnum(),wsspcomn.chdrChdrnum)) {
			goTo(GotoLabel.nextr5080);
		}
		validateStatus5100();
		if (!validStatcode.isTrue()
		&& !validStatcode.isTrue()) {
			goTo(GotoLabel.nextr5080);
		}
		//ILIFE-2390 Start
		if (isEQ(chdrmjaIO.getStatcode(), "IF") || isEQ(chdrmjaIO.getStatcode(), "LA")) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			//calculate5Years
			if (isEQ(chdrmjaIO.getStatcode(), "IF")) {
				datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
			} else {
				datcon3rec.intDate1.set(chdrmjaIO.getPtdate());
			}
			datcon3rec.intDate2.set(datcon1rec.intDate);
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
			if (datcon3rec.freqFactor.toFloat() > 5) {
				goTo(GotoLabel.nextr5080);
			}
		}
		//ILIFE-2390 End
		
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttyp.set(chdrmjaIO.getCnttype());
		sv.agntnum.set(chdrmjaIO.getAgntnum());
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.set(SPACES);
		}
		else {
			sv.chdrstatus.set(descIO.getLongdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.set(SPACES);
		}
		else {
			sv.premstatus.set(descIO.getLongdesc());
		}
		sv.ccdateDisp.set(""); //ILIFE-2390
		sv.ccdate.set(chdrmjaIO.getCcdate());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.tranno.set(chdrmjaIO.getTranno());
		sv.life.set(zlifelcIO.getLife());
		sv.jlife.set(zlifelcIO.getJlife());
		//IBPLIFE-6414 starts
        if ( isNE(chdrmjaIO.getStatuz(),varcom.mrnf))
        {
        	calcAnb5200();
        }
        //IBPLIFE-6414 ends
		scrnparams.function.set(varcom.sadd);
		callSr2d3io1900();
		wsaaCnt.add(1);		
		Chdrpf chdr = new Chdrpf();
		chdr.setChdrnum(chdrmjaIO.getChdrnum().toString());
		chdr.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		chdr.setCnttype(chdrmjaIO.getCnttype().toString());
		contractList.add(chdr);
	}

protected void nextr5080()
	{
		zlifelcIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, zlifelcIO);
		if (isNE(zlifelcIO.getStatuz(),varcom.oK)
		&& isNE(zlifelcIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(zlifelcIO.getParams());
			syserrrec.statuz.set(zlifelcIO.getStatuz());
			fatalError600();
		}
		if (isNE(zlifelcIO.getLifcnum(),cltsIO.getClntnum())) {
			zlifelcIO.setStatuz(varcom.endp);
		}
		if (isEQ(zlifelcIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
	}

protected void validateStatus5100()
	{
		/*START*/
		wsaaValidStatcode.set("N");
		wsaaValidPstatcode.set("N");
		wsaaIndex.set(ZERO);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| validStatcode.isTrue()); wsaaIndex.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()],chdrmjaIO.getStatcode())) {
				wsaaValidStatcode.set("Y");
			}
		}
		if (validStatcode.isTrue()) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| validPstatcode.isTrue()); wsaaIndex.add(1)){
				if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()],chdrmjaIO.getPstatcode())) {
					wsaaValidPstatcode.set("Y");
				}
			}
		}
		/*EXIT*/
	}

protected void calcAnb5200()
	{
		start5210();
	}

protected void start5210()
	{
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
		agecalcrec.company.set(wsspcomn.fsuco);
		agecalcrec.intDate1.set(cltsIO.getCltdob());
		agecalcrec.intDate2.set(chdrmjaIO.getOccdate());
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK) ) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		sv.anbage.set(agecalcrec.agerating);
	}

protected void keepsPayr6000()
	{
		start6010();
	}

protected void start6010()
	{
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		if (isEQ(payrIO.getStatuz(),varcom.oK)) {
			payrIO.setFunction(varcom.keeps);
			payrIO.setFormat(payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
		}
	}

protected void readHpad7100()
	{
			start7110();
		}

protected void start7110()
	{
		wsaaFndFlg.set(SPACES);
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(wsspcomn.company);
		hpadIO.setChdrnum(sv.chdrnum);
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)
		&& isNE(hpadIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hpadIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		wsaaFndFlg.set("Y");
	}

protected void hpadProcess7200()
	{
			readTr6277210();
		}

protected void readTr6277210()
	{
		readChdrlnb7500();
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr627);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		tr627rec.tr627Rec.set(itemIO.getGenarea());
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(cltsIO.getCltdob());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(tr627rec.zsufcage);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.subrname.set("DATCON2");
			fatalError600();
		}
		wsaaDobPlusTr627.set(datcon2rec.intDate2);
		if (isGT(wsaaDobPlusTr627,wsaaToday)) {
			scrnparams.errorCode.set(rlar);
			wsspcomn.edterror.set("Y");
		}
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon2rec.frequency.set("01");
		compute(datcon2rec.freqFactor, 0).set(sub(tr627rec.zsufcage,lifemjaIO.getAnbAtCcd()));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.subrname.set("DATCON2");
			fatalError600();
		}
		if (isGT(wsaaDobPlusTr627,datcon2rec.intDate2)) {
			datcon2rec.intDate1.set(datcon2rec.intDate2);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.subrname.set("DATCON2");
				fatalError600();
			}
		}
		if (isEQ(cltsIO.getClntnum(),chdrlnbIO.getCownnum())) {
			return ;
		}
		updateHpad7300();
	}

protected void updateHpad7300()
	{
		/*START*/
		hpadIO.setZsufcdte(datcon2rec.intDate2);
		hpadIO.setValidflag("1");
		hpadIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readChdrlnb7500()
	{
		/*START*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.chdrnum);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

private static final class FormatsInner {
	/* FORMATS */
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	
	}

	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}


}
