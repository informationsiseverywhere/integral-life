package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:16
 * Description:
 * Copybook name: ANNTMJAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Anntmjakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData anntmjaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData anntmjaKey = new FixedLengthStringData(64).isAPartOf(anntmjaFileKey, 0, REDEFINE);
  	public FixedLengthStringData anntmjaChdrcoy = new FixedLengthStringData(1).isAPartOf(anntmjaKey, 0);
  	public FixedLengthStringData anntmjaChdrnum = new FixedLengthStringData(8).isAPartOf(anntmjaKey, 1);
  	public FixedLengthStringData anntmjaLife = new FixedLengthStringData(2).isAPartOf(anntmjaKey, 9);
  	public FixedLengthStringData anntmjaCoverage = new FixedLengthStringData(2).isAPartOf(anntmjaKey, 11);
  	public FixedLengthStringData anntmjaRider = new FixedLengthStringData(2).isAPartOf(anntmjaKey, 13);
  	public PackedDecimalData anntmjaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(anntmjaKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(anntmjaKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(anntmjaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		anntmjaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}