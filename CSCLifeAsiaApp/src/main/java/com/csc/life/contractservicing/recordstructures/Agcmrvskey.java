package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:01
 * Description:
 * Copybook name: AGCMRVSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmrvskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmrvsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmrvsKey = new FixedLengthStringData(64).isAPartOf(agcmrvsFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmrvsChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmrvsKey, 0);
  	public FixedLengthStringData agcmrvsChdrnum = new FixedLengthStringData(8).isAPartOf(agcmrvsKey, 1);
  	public PackedDecimalData agcmrvsTranno = new PackedDecimalData(5, 0).isAPartOf(agcmrvsKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(agcmrvsKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmrvsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmrvsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}