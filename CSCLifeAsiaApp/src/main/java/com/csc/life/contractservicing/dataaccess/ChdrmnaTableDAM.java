package com.csc.life.contractservicing.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChdrmnaTableDAM.java
 * Date: Sun, 30 Aug 2009 03:32:08
 * Class transformed from CHDRMNA.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChdrmnaTableDAM extends ChdrpfTableDAM {

	public ChdrmnaTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHDRMNA");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "VALIDFLAG, " +
		            "RECODE, " +
		            "SERVUNIT, " +
		            "PROCTRANCD, " +
		            "PROCFLAG, " +
		            "PROCID, " +
		            "STATREASN, " +
		            "TRANLUSED, " +
		            "CCDATE, " +
		            "CRDATE, " +
		            "ANNAMT01, " +
		            "ANNAMT02, " +
		            "ANNAMT03, " +
		            "ANNAMT04, " +
		            "ANNAMT05, " +
		            "ANNAMT06, " +
		            "CURRTO, " +
		            "CURRFROM, " +
		            "RNLTYPE, " +
		            "RNLNOTS, " +
		            "RNLNOTTO, " +
		            "RNLATTN, " +
		            "RNLDURN, " +
		            "REPNUM, " +
		            "OCCDATE, " +
		            "PTDATE, " +
		            "BTDATE, " +
		            "STATCODE, " +
		            "STATDATE, " +
		            "STATTRAN, " +
		            "PSTCDE, " +
		            "PSTTRN, " +
		            "CNTBRANCH, " +
		            "CNTTYPE, " +
		            "CNTCURR, " +
		            "ACCTCCY, " +
		            "CRATE, " +
		            "PAYPLAN, " +
		            "ACCTMETH, " +
		            "BILLCURR, " +
		            "BILLFREQ, " +
		            "BILLCHNL, " +
		            "COLLCHNL, " +
		            "INSTBCHNL, " +
		            "INSTCCHNL, " +
		            "INSTFREQ, " +
		            "BILLCD, " +
		            "BILLDAY, " +
		            "BILLMONTH, " +
		            "PAYFLAG, " +
		            "COWNPFX, " +
		            "COWNNUM, " +
		            "COWNCOY, " +
		            "JOWNNUM, " +
		            "PAYRPFX, " +
		            "PAYRCOY, " +
		            "PAYRNUM, " +
		            "ASGNPFX, " +
		            "ASGNCOY, " +
		            "ASGNNUM, " +
		            "DESPPFX, " +
		            "DESPCOY, " +
		            "DESPNUM, " +
		            "AGNTPFX, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "SRCEBUS, " +
		            "REPTYPE, " +
		            "REG, " +
		            "APLSUPR, " +
		            "APLSPFROM, " +
		            "APLSPTO, " +
		            "BILLSUPR, " +
		            "BILLSPFROM, " +
		            "BILLSPTO, " +
		            "NOTSSUPR, " +
		            "NOTSSPFROM, " +
		            "NOTSSPTO, " +
		            "RNWLSUPR, " +
		            "RNWLSPFROM, " +
		            "RNWLSPTO, " +
		            "COMMSUPR, " +
		            "COMMSPFROM, " +
		            "COMMSPTO, " +
		            "SINSTFROM, " +
		            "SINSTTO, " +
		            "SINSTAMT01, " +
		            "SINSTAMT02, " +
		            "SINSTAMT03, " +
		            "SINSTAMT04, " +
		            "SINSTAMT05, " +
		            "SINSTAMT06, " +
		            "INSTFROM, " +
		            "INSTTO, " +
		            "INSTJCTL, " +
		            "INSTTOT01, " +
		            "INSTTOT02, " +
		            "INSTTOT03, " +
		            "INSTTOT04, " +
		            "INSTTOT05, " +
		            "INSTTOT06, " +
		            "OUTSTAMT, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "BANKCODE, " +
		            "MANDREF, " +
		            "STCA, " +
		            "STCB, " +
		            "STCC, " +
		            "STCD, " +
		            "STCE, " +
		            "POLPLN, " +
		            "CHGFLAG, " +
		            "LAPRIND, " +
		            "SPECIND, " +
		            "DUEFLG, " +
		            "BFCHARGE, " +
		            "DISHNRCNT, " +
		            "PDTYPE, " +
		            "DISHNRDTE, " +
		            "STMPDTYAMT, " +
		            "STMPDTYDTE, " +
		            "POLINC, " +
		            "POLSUM, " +
		            "NXTSFX, " +
		            "AVLISU, " +
		            "STMDTE, " +
		            "CNTISS, " +
		            "CNTRCV, " +
		            "COPPN, " +
		            "COTYPE, " +
		            "COVERNT, " +
		            "DOCNUM, " +
		            "DTECAN, " +
		            "QUOTENO, " +
		            "RNLSTS, " +
		            "SUSTRCDE, " +
		            "BILLDATE01, " +
		            "BILLDATE02, " +
		            "BILLDATE03, " +
		            "BILLDATE04, " +
		            "GRUPKEY, " +
		            "MEMBSEL, " +
		            "CAMPAIGN, " +
		            "PSTDAT, " +
		            "TFRSWUSED, " +
		            "TFRSWLEFT, " +
		            "LASTSWDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "ZMANDREF,	" + // ILIFE-2472
					"REQNTYPE," + // ILIFE-2472
		            "PAYCLT," + //ILIFE-2472 PH2
		            "COWNNUM2,"+ //ILIFE-7277-start
					"COWNNUM3,"+
					"COWNNUM4,"+
					"COWNNUM5,"+//ILIFE-7277-end
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               tranno,
                               tranid,
                               validflag,
                               recode,
                               servunit,
                               proctrancd,
                               procflag,
                               procid,
                               statreasn,
                               tranlused,
                               ccdate,
                               crdate,
                               annamt01,
                               annamt02,
                               annamt03,
                               annamt04,
                               annamt05,
                               annamt06,
                               currto,
                               currfrom,
                               rnltype,
                               rnlnots,
                               rnlnotto,
                               rnlattn,
                               rnldurn,
                               repnum,
                               occdate,
                               ptdate,
                               btdate,
                               statcode,
                               statdate,
                               stattran,
                               pstatcode,
                               pstattran,
                               cntbranch,
                               cnttype,
                               cntcurr,
                               acctccy,
                               crate,
                               payplan,
                               acctmeth,
                               billcurr,
                               billfreq,
                               billchnl,
                               collchnl,
                               instbchnl,
                               instcchnl,
                               instfreq,
                               billcd,
                               billday,
                               billmonth,
                               payflag,
                               cownpfx,
                               cownnum,
                               cowncoy,
                               jownnum,
                               payrpfx,
                               payrcoy,
                               payrnum,
                               asgnpfx,
                               asgncoy,
                               asgnnum,
                               desppfx,
                               despcoy,
                               despnum,
                               agntpfx,
                               agntcoy,
                               agntnum,
                               srcebus,
                               reptype,
                               register,
                               aplsupr,
                               aplspfrom,
                               aplspto,
                               billsupr,
                               billspfrom,
                               billspto,
                               notssupr,
                               notsspfrom,
                               notsspto,
                               rnwlsupr,
                               rnwlspfrom,
                               rnwlspto,
                               commsupr,
                               commspfrom,
                               commspto,
                               sinstfrom,
                               sinstto,
                               sinstamt01,
                               sinstamt02,
                               sinstamt03,
                               sinstamt04,
                               sinstamt05,
                               sinstamt06,
                               instfrom,
                               instto,
                               instjctl,
                               insttot01,
                               insttot02,
                               insttot03,
                               insttot04,
                               insttot05,
                               insttot06,
                               outstamt,
                               facthous,
                               bankkey,
                               bankacckey,
                               bankcode,
                               mandref,
                               chdrstcda,
                               chdrstcdb,
                               chdrstcdc,
                               chdrstcdd,
                               chdrstcde,
                               polpln,
                               chgflag,
                               laprind,
                               specind,
                               dueflg,
                               bfcharge,
                               dishnrcnt,
                               pdtype,
                               dishnrdte,
                               stmpdtyamt,
                               stmpdtydte,
                               polinc,
                               polsum,
                               nxtsfx,
                               avlisu,
                               statementDate,
                               cntiss,
                               cntrcv,
                               coppn,
                               cotype,
                               covernt,
                               docnum,
                               dtecan,
                               quoteno,
                               rnlsts,
                               sustrcde,
                               billdate01,
                               billdate02,
                               billdate03,
                               billdate04,
                               grupkey,
                               membsel,
                               campaign,
                               pstatdate,
                               freeSwitchesUsed,
                               freeSwitchesLeft,
                               lastSwitchDate,
                               userProfile,
                               jobName,
                               datime,
                               zmandref, //ILIFE-2472
                               reqntype, //ILIFE-2472
                               payclt, //ILIFE-2472 PH2
                               cownnum2,//ILIFE-7277-start
                               cownnum3,
                               cownnum4,
                               cownnum5,//ILIFE-7277-end
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller20.setInternal(chdrcoy.toInternal());
	nonKeyFiller30.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		//FixedLengthStringData nonKeyData = new FixedLengthStringData(803);
		//FixedLengthStringData nonKeyData = new FixedLengthStringData(809);//ILIFE-2472
		FixedLengthStringData nonKeyData = new FixedLengthStringData(817);//ILIFE-2472 PH2
		nonKeyData.set(
					getChdrpfx().toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getTranno().toInternal()
					+ getTranid().toInternal()
					+ getValidflag().toInternal()
					+ getRecode().toInternal()
					+ getServunit().toInternal()
					+ getProctrancd().toInternal()
					+ getProcflag().toInternal()
					+ getProcid().toInternal()
					+ getStatreasn().toInternal()
					+ getTranlused().toInternal()
					+ getCcdate().toInternal()
					+ getCrdate().toInternal()
					+ getAnnamt01().toInternal()
					+ getAnnamt02().toInternal()
					+ getAnnamt03().toInternal()
					+ getAnnamt04().toInternal()
					+ getAnnamt05().toInternal()
					+ getAnnamt06().toInternal()
					+ getCurrto().toInternal()
					+ getCurrfrom().toInternal()
					+ getRnltype().toInternal()
					+ getRnlnots().toInternal()
					+ getRnlnotto().toInternal()
					+ getRnlattn().toInternal()
					+ getRnldurn().toInternal()
					+ getRepnum().toInternal()
					+ getOccdate().toInternal()
					+ getPtdate().toInternal()
					+ getBtdate().toInternal()
					+ getStatcode().toInternal()
					+ getStatdate().toInternal()
					+ getStattran().toInternal()
					+ getPstatcode().toInternal()
					+ getPstattran().toInternal()
					+ getCntbranch().toInternal()
					+ getCnttype().toInternal()
					+ getCntcurr().toInternal()
					+ getAcctccy().toInternal()
					+ getCrate().toInternal()
					+ getPayplan().toInternal()
					+ getAcctmeth().toInternal()
					+ getBillcurr().toInternal()
					+ getBillfreq().toInternal()
					+ getBillchnl().toInternal()
					+ getCollchnl().toInternal()
					+ getInstbchnl().toInternal()
					+ getInstcchnl().toInternal()
					+ getInstfreq().toInternal()
					+ getBillcd().toInternal()
					+ getBillday().toInternal()
					+ getBillmonth().toInternal()
					+ getPayflag().toInternal()
					+ getCownpfx().toInternal()
					+ getCownnum().toInternal()
					+ getCowncoy().toInternal()
					+ getJownnum().toInternal()
					+ getPayrpfx().toInternal()
					+ getPayrcoy().toInternal()
					+ getPayrnum().toInternal()
					+ getAsgnpfx().toInternal()
					+ getAsgncoy().toInternal()
					+ getAsgnnum().toInternal()
					+ getDesppfx().toInternal()
					+ getDespcoy().toInternal()
					+ getDespnum().toInternal()
					+ getAgntpfx().toInternal()
					+ getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getSrcebus().toInternal()
					+ getReptype().toInternal()
					+ getRegister().toInternal()
					+ getAplsupr().toInternal()
					+ getAplspfrom().toInternal()
					+ getAplspto().toInternal()
					+ getBillsupr().toInternal()
					+ getBillspfrom().toInternal()
					+ getBillspto().toInternal()
					+ getNotssupr().toInternal()
					+ getNotsspfrom().toInternal()
					+ getNotsspto().toInternal()
					+ getRnwlsupr().toInternal()
					+ getRnwlspfrom().toInternal()
					+ getRnwlspto().toInternal()
					+ getCommsupr().toInternal()
					+ getCommspfrom().toInternal()
					+ getCommspto().toInternal()
					+ getSinstfrom().toInternal()
					+ getSinstto().toInternal()
					+ getSinstamt01().toInternal()
					+ getSinstamt02().toInternal()
					+ getSinstamt03().toInternal()
					+ getSinstamt04().toInternal()
					+ getSinstamt05().toInternal()
					+ getSinstamt06().toInternal()
					+ getInstfrom().toInternal()
					+ getInstto().toInternal()
					+ getInstjctl().toInternal()
					+ getInsttot01().toInternal()
					+ getInsttot02().toInternal()
					+ getInsttot03().toInternal()
					+ getInsttot04().toInternal()
					+ getInsttot05().toInternal()
					+ getInsttot06().toInternal()
					+ getOutstamt().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getBankcode().toInternal()
					+ getMandref().toInternal()
					+ getChdrstcda().toInternal()
					+ getChdrstcdb().toInternal()
					+ getChdrstcdc().toInternal()
					+ getChdrstcdd().toInternal()
					+ getChdrstcde().toInternal()
					+ getPolpln().toInternal()
					+ getChgflag().toInternal()
					+ getLaprind().toInternal()
					+ getSpecind().toInternal()
					+ getDueflg().toInternal()
					+ getBfcharge().toInternal()
					+ getDishnrcnt().toInternal()
					+ getPdtype().toInternal()
					+ getDishnrdte().toInternal()
					+ getStmpdtyamt().toInternal()
					+ getStmpdtydte().toInternal()
					+ getPolinc().toInternal()
					+ getPolsum().toInternal()
					+ getNxtsfx().toInternal()
					+ getAvlisu().toInternal()
					+ getStatementDate().toInternal()
					+ getCntiss().toInternal()
					+ getCntrcv().toInternal()
					+ getCoppn().toInternal()
					+ getCotype().toInternal()
					+ getCovernt().toInternal()
					+ getDocnum().toInternal()
					+ getDtecan().toInternal()
					+ getQuoteno().toInternal()
					+ getRnlsts().toInternal()
					+ getSustrcde().toInternal()
					+ getBilldate01().toInternal()
					+ getBilldate02().toInternal()
					+ getBilldate03().toInternal()
					+ getBilldate04().toInternal()
					+ getGrupkey().toInternal()
					+ getMembsel().toInternal()
					+ getCampaign().toInternal()
					+ getPstatdate().toInternal()
					+ getFreeSwitchesUsed().toInternal()
					+ getFreeSwitchesLeft().toInternal()
					+ getLastSwitchDate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getZmandref().toInternal()  //ILIFE-2472
					+ getReqntype().toInternal()  //ILIFE-2472
					+ getPayclt().toInternal() //ILIFE-2472 PH2
					//ILIFE-7277-start
		            + getCownnum2().toInternal()
                    + getCownnum3().toInternal()
                    + getCownnum4().toInternal()
                    + getCownnum5().toInternal());
		            //ILIFE-7277-end
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, recode);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, proctrancd);
			what = ExternalData.chop(what, procflag);
			what = ExternalData.chop(what, procid);
			what = ExternalData.chop(what, statreasn);
			what = ExternalData.chop(what, tranlused);
			what = ExternalData.chop(what, ccdate);
			what = ExternalData.chop(what, crdate);
			what = ExternalData.chop(what, annamt01);
			what = ExternalData.chop(what, annamt02);
			what = ExternalData.chop(what, annamt03);
			what = ExternalData.chop(what, annamt04);
			what = ExternalData.chop(what, annamt05);
			what = ExternalData.chop(what, annamt06);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, rnltype);
			what = ExternalData.chop(what, rnlnots);
			what = ExternalData.chop(what, rnlnotto);
			what = ExternalData.chop(what, rnlattn);
			what = ExternalData.chop(what, rnldurn);
			what = ExternalData.chop(what, repnum);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, statdate);
			what = ExternalData.chop(what, stattran);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, pstattran);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, acctccy);
			what = ExternalData.chop(what, crate);
			what = ExternalData.chop(what, payplan);
			what = ExternalData.chop(what, acctmeth);
			what = ExternalData.chop(what, billcurr);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, collchnl);
			what = ExternalData.chop(what, instbchnl);
			what = ExternalData.chop(what, instcchnl);
			what = ExternalData.chop(what, instfreq);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, billday);
			what = ExternalData.chop(what, billmonth);
			what = ExternalData.chop(what, payflag);
			what = ExternalData.chop(what, cownpfx);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, jownnum);
			what = ExternalData.chop(what, payrpfx);
			what = ExternalData.chop(what, payrcoy);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, asgnpfx);
			what = ExternalData.chop(what, asgncoy);
			what = ExternalData.chop(what, asgnnum);
			what = ExternalData.chop(what, desppfx);
			what = ExternalData.chop(what, despcoy);
			what = ExternalData.chop(what, despnum);
			what = ExternalData.chop(what, agntpfx);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, reptype);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, aplsupr);
			what = ExternalData.chop(what, aplspfrom);
			what = ExternalData.chop(what, aplspto);
			what = ExternalData.chop(what, billsupr);
			what = ExternalData.chop(what, billspfrom);
			what = ExternalData.chop(what, billspto);
			what = ExternalData.chop(what, notssupr);
			what = ExternalData.chop(what, notsspfrom);
			what = ExternalData.chop(what, notsspto);
			what = ExternalData.chop(what, rnwlsupr);
			what = ExternalData.chop(what, rnwlspfrom);
			what = ExternalData.chop(what, rnwlspto);
			what = ExternalData.chop(what, commsupr);
			what = ExternalData.chop(what, commspfrom);
			what = ExternalData.chop(what, commspto);
			what = ExternalData.chop(what, sinstfrom);
			what = ExternalData.chop(what, sinstto);
			what = ExternalData.chop(what, sinstamt01);
			what = ExternalData.chop(what, sinstamt02);
			what = ExternalData.chop(what, sinstamt03);
			what = ExternalData.chop(what, sinstamt04);
			what = ExternalData.chop(what, sinstamt05);
			what = ExternalData.chop(what, sinstamt06);
			what = ExternalData.chop(what, instfrom);
			what = ExternalData.chop(what, instto);
			what = ExternalData.chop(what, instjctl);
			what = ExternalData.chop(what, insttot01);
			what = ExternalData.chop(what, insttot02);
			what = ExternalData.chop(what, insttot03);
			what = ExternalData.chop(what, insttot04);
			what = ExternalData.chop(what, insttot05);
			what = ExternalData.chop(what, insttot06);
			what = ExternalData.chop(what, outstamt);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, bankcode);
			what = ExternalData.chop(what, mandref);
			what = ExternalData.chop(what, chdrstcda);
			what = ExternalData.chop(what, chdrstcdb);
			what = ExternalData.chop(what, chdrstcdc);
			what = ExternalData.chop(what, chdrstcdd);
			what = ExternalData.chop(what, chdrstcde);
			what = ExternalData.chop(what, polpln);
			what = ExternalData.chop(what, chgflag);
			what = ExternalData.chop(what, laprind);
			what = ExternalData.chop(what, specind);
			what = ExternalData.chop(what, dueflg);
			what = ExternalData.chop(what, bfcharge);
			what = ExternalData.chop(what, dishnrcnt);
			what = ExternalData.chop(what, pdtype);
			what = ExternalData.chop(what, dishnrdte);
			what = ExternalData.chop(what, stmpdtyamt);
			what = ExternalData.chop(what, stmpdtydte);
			what = ExternalData.chop(what, polinc);
			what = ExternalData.chop(what, polsum);
			what = ExternalData.chop(what, nxtsfx);
			what = ExternalData.chop(what, avlisu);
			what = ExternalData.chop(what, statementDate);
			what = ExternalData.chop(what, cntiss);
			what = ExternalData.chop(what, cntrcv);
			what = ExternalData.chop(what, coppn);
			what = ExternalData.chop(what, cotype);
			what = ExternalData.chop(what, covernt);
			what = ExternalData.chop(what, docnum);
			what = ExternalData.chop(what, dtecan);
			what = ExternalData.chop(what, quoteno);
			what = ExternalData.chop(what, rnlsts);
			what = ExternalData.chop(what, sustrcde);
			what = ExternalData.chop(what, billdate01);
			what = ExternalData.chop(what, billdate02);
			what = ExternalData.chop(what, billdate03);
			what = ExternalData.chop(what, billdate04);
			what = ExternalData.chop(what, grupkey);
			what = ExternalData.chop(what, membsel);
			what = ExternalData.chop(what, campaign);
			what = ExternalData.chop(what, pstatdate);
			what = ExternalData.chop(what, freeSwitchesUsed);
			what = ExternalData.chop(what, freeSwitchesLeft);
			what = ExternalData.chop(what, lastSwitchDate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);	
			what = ExternalData.chop(what, zmandref); //ILIFE-2472
			what = ExternalData.chop(what, reqntype); //ILIFE-2472	
			what = ExternalData.chop(what, payclt); //ILIFE-2472 PH2
			//ILIFE-7277-start
			what = ExternalData.chop(what, cownnum2);
			what = ExternalData.chop(what, cownnum3);
			what = ExternalData.chop(what, cownnum4);
			what = ExternalData.chop(what, cownnum5);
			//ILIFE-7277-end
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getRecode() {
		return recode;
	}
	public void setRecode(Object what) {
		recode.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getProctrancd() {
		return proctrancd;
	}
	public void setProctrancd(Object what) {
		proctrancd.set(what);
	}	
	public FixedLengthStringData getProcflag() {
		return procflag;
	}
	public void setProcflag(Object what) {
		procflag.set(what);
	}	
	public FixedLengthStringData getProcid() {
		return procid;
	}
	public void setProcid(Object what) {
		procid.set(what);
	}	
	public FixedLengthStringData getStatreasn() {
		return statreasn;
	}
	public void setStatreasn(Object what) {
		statreasn.set(what);
	}	
	public PackedDecimalData getTranlused() {
		return tranlused;
	}
	public void setTranlused(Object what) {
		setTranlused(what, false);
	}
	public void setTranlused(Object what, boolean rounded) {
		if (rounded)
			tranlused.setRounded(what);
		else
			tranlused.set(what);
	}	
	public PackedDecimalData getCcdate() {
		return ccdate;
	}
	public void setCcdate(Object what) {
		setCcdate(what, false);
	}
	public void setCcdate(Object what, boolean rounded) {
		if (rounded)
			ccdate.setRounded(what);
		else
			ccdate.set(what);
	}	
	public PackedDecimalData getCrdate() {
		return crdate;
	}
	public void setCrdate(Object what) {
		setCrdate(what, false);
	}
	public void setCrdate(Object what, boolean rounded) {
		if (rounded)
			crdate.setRounded(what);
		else
			crdate.set(what);
	}	
	public PackedDecimalData getAnnamt01() {
		return annamt01;
	}
	public void setAnnamt01(Object what) {
		setAnnamt01(what, false);
	}
	public void setAnnamt01(Object what, boolean rounded) {
		if (rounded)
			annamt01.setRounded(what);
		else
			annamt01.set(what);
	}	
	public PackedDecimalData getAnnamt02() {
		return annamt02;
	}
	public void setAnnamt02(Object what) {
		setAnnamt02(what, false);
	}
	public void setAnnamt02(Object what, boolean rounded) {
		if (rounded)
			annamt02.setRounded(what);
		else
			annamt02.set(what);
	}	
	public PackedDecimalData getAnnamt03() {
		return annamt03;
	}
	public void setAnnamt03(Object what) {
		setAnnamt03(what, false);
	}
	public void setAnnamt03(Object what, boolean rounded) {
		if (rounded)
			annamt03.setRounded(what);
		else
			annamt03.set(what);
	}	
	public PackedDecimalData getAnnamt04() {
		return annamt04;
	}
	public void setAnnamt04(Object what) {
		setAnnamt04(what, false);
	}
	public void setAnnamt04(Object what, boolean rounded) {
		if (rounded)
			annamt04.setRounded(what);
		else
			annamt04.set(what);
	}	
	public PackedDecimalData getAnnamt05() {
		return annamt05;
	}
	public void setAnnamt05(Object what) {
		setAnnamt05(what, false);
	}
	public void setAnnamt05(Object what, boolean rounded) {
		if (rounded)
			annamt05.setRounded(what);
		else
			annamt05.set(what);
	}	
	public PackedDecimalData getAnnamt06() {
		return annamt06;
	}
	public void setAnnamt06(Object what) {
		setAnnamt06(what, false);
	}
	public void setAnnamt06(Object what, boolean rounded) {
		if (rounded)
			annamt06.setRounded(what);
		else
			annamt06.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public FixedLengthStringData getRnltype() {
		return rnltype;
	}
	public void setRnltype(Object what) {
		rnltype.set(what);
	}	
	public FixedLengthStringData getRnlnots() {
		return rnlnots;
	}
	public void setRnlnots(Object what) {
		rnlnots.set(what);
	}	
	public FixedLengthStringData getRnlnotto() {
		return rnlnotto;
	}
	public void setRnlnotto(Object what) {
		rnlnotto.set(what);
	}	
	public FixedLengthStringData getRnlattn() {
		return rnlattn;
	}
	public void setRnlattn(Object what) {
		rnlattn.set(what);
	}	
	public PackedDecimalData getRnldurn() {
		return rnldurn;
	}
	public void setRnldurn(Object what) {
		setRnldurn(what, false);
	}
	public void setRnldurn(Object what, boolean rounded) {
		if (rounded)
			rnldurn.setRounded(what);
		else
			rnldurn.set(what);
	}	
	public FixedLengthStringData getRepnum() {
		return repnum;
	}
	public void setRepnum(Object what) {
		repnum.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public PackedDecimalData getStatdate() {
		return statdate;
	}
	public void setStatdate(Object what) {
		setStatdate(what, false);
	}
	public void setStatdate(Object what, boolean rounded) {
		if (rounded)
			statdate.setRounded(what);
		else
			statdate.set(what);
	}	
	public PackedDecimalData getStattran() {
		return stattran;
	}
	public void setStattran(Object what) {
		setStattran(what, false);
	}
	public void setStattran(Object what, boolean rounded) {
		if (rounded)
			stattran.setRounded(what);
		else
			stattran.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getPstattran() {
		return pstattran;
	}
	public void setPstattran(Object what) {
		setPstattran(what, false);
	}
	public void setPstattran(Object what, boolean rounded) {
		if (rounded)
			pstattran.setRounded(what);
		else
			pstattran.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getAcctccy() {
		return acctccy;
	}
	public void setAcctccy(Object what) {
		acctccy.set(what);
	}	
	public PackedDecimalData getCrate() {
		return crate;
	}
	public void setCrate(Object what) {
		setCrate(what, false);
	}
	public void setCrate(Object what, boolean rounded) {
		if (rounded)
			crate.setRounded(what);
		else
			crate.set(what);
	}	
	public FixedLengthStringData getPayplan() {
		return payplan;
	}
	public void setPayplan(Object what) {
		payplan.set(what);
	}	
	public FixedLengthStringData getAcctmeth() {
		return acctmeth;
	}
	public void setAcctmeth(Object what) {
		acctmeth.set(what);
	}	
	public FixedLengthStringData getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(Object what) {
		billcurr.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public FixedLengthStringData getCollchnl() {
		return collchnl;
	}
	public void setCollchnl(Object what) {
		collchnl.set(what);
	}	
	public FixedLengthStringData getInstbchnl() {
		return instbchnl;
	}
	public void setInstbchnl(Object what) {
		instbchnl.set(what);
	}	
	public FixedLengthStringData getInstcchnl() {
		return instcchnl;
	}
	public void setInstcchnl(Object what) {
		instcchnl.set(what);
	}	
	public FixedLengthStringData getInstfreq() {
		return instfreq;
	}
	public void setInstfreq(Object what) {
		instfreq.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public FixedLengthStringData getBillday() {
		return billday;
	}
	public void setBillday(Object what) {
		billday.set(what);
	}	
	public FixedLengthStringData getBillmonth() {
		return billmonth;
	}
	public void setBillmonth(Object what) {
		billmonth.set(what);
	}	
	public FixedLengthStringData getPayflag() {
		return payflag;
	}
	public void setPayflag(Object what) {
		payflag.set(what);
	}	
	public FixedLengthStringData getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(Object what) {
		cownpfx.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getJownnum() {
		return jownnum;
	}
	public void setJownnum(Object what) {
		jownnum.set(what);
	}	
	public FixedLengthStringData getPayrpfx() {
		return payrpfx;
	}
	public void setPayrpfx(Object what) {
		payrpfx.set(what);
	}	
	public FixedLengthStringData getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(Object what) {
		payrcoy.set(what);
	}	
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}	
	public FixedLengthStringData getAsgnpfx() {
		return asgnpfx;
	}
	public void setAsgnpfx(Object what) {
		asgnpfx.set(what);
	}	
	public FixedLengthStringData getAsgncoy() {
		return asgncoy;
	}
	public void setAsgncoy(Object what) {
		asgncoy.set(what);
	}	
	public FixedLengthStringData getAsgnnum() {
		return asgnnum;
	}
	public void setAsgnnum(Object what) {
		asgnnum.set(what);
	}	
	public FixedLengthStringData getDesppfx() {
		return desppfx;
	}
	public void setDesppfx(Object what) {
		desppfx.set(what);
	}	
	public FixedLengthStringData getDespcoy() {
		return despcoy;
	}
	public void setDespcoy(Object what) {
		despcoy.set(what);
	}	
	public FixedLengthStringData getDespnum() {
		return despnum;
	}
	public void setDespnum(Object what) {
		despnum.set(what);
	}	
	public FixedLengthStringData getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(Object what) {
		agntpfx.set(what);
	}	
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}	
	public FixedLengthStringData getReptype() {
		return reptype;
	}
	public void setReptype(Object what) {
		reptype.set(what);
	}	
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}	
	public FixedLengthStringData getAplsupr() {
		return aplsupr;
	}
	public void setAplsupr(Object what) {
		aplsupr.set(what);
	}	
	public PackedDecimalData getAplspfrom() {
		return aplspfrom;
	}
	public void setAplspfrom(Object what) {
		setAplspfrom(what, false);
	}
	public void setAplspfrom(Object what, boolean rounded) {
		if (rounded)
			aplspfrom.setRounded(what);
		else
			aplspfrom.set(what);
	}	
	public PackedDecimalData getAplspto() {
		return aplspto;
	}
	public void setAplspto(Object what) {
		setAplspto(what, false);
	}
	public void setAplspto(Object what, boolean rounded) {
		if (rounded)
			aplspto.setRounded(what);
		else
			aplspto.set(what);
	}	
	public FixedLengthStringData getBillsupr() {
		return billsupr;
	}
	public void setBillsupr(Object what) {
		billsupr.set(what);
	}	
	public PackedDecimalData getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(Object what) {
		setBillspfrom(what, false);
	}
	public void setBillspfrom(Object what, boolean rounded) {
		if (rounded)
			billspfrom.setRounded(what);
		else
			billspfrom.set(what);
	}	
	public PackedDecimalData getBillspto() {
		return billspto;
	}
	public void setBillspto(Object what) {
		setBillspto(what, false);
	}
	public void setBillspto(Object what, boolean rounded) {
		if (rounded)
			billspto.setRounded(what);
		else
			billspto.set(what);
	}	
	public FixedLengthStringData getNotssupr() {
		return notssupr;
	}
	public void setNotssupr(Object what) {
		notssupr.set(what);
	}	
	public PackedDecimalData getNotsspfrom() {
		return notsspfrom;
	}
	public void setNotsspfrom(Object what) {
		setNotsspfrom(what, false);
	}
	public void setNotsspfrom(Object what, boolean rounded) {
		if (rounded)
			notsspfrom.setRounded(what);
		else
			notsspfrom.set(what);
	}	
	public PackedDecimalData getNotsspto() {
		return notsspto;
	}
	public void setNotsspto(Object what) {
		setNotsspto(what, false);
	}
	public void setNotsspto(Object what, boolean rounded) {
		if (rounded)
			notsspto.setRounded(what);
		else
			notsspto.set(what);
	}	
	public FixedLengthStringData getRnwlsupr() {
		return rnwlsupr;
	}
	public void setRnwlsupr(Object what) {
		rnwlsupr.set(what);
	}	
	public PackedDecimalData getRnwlspfrom() {
		return rnwlspfrom;
	}
	public void setRnwlspfrom(Object what) {
		setRnwlspfrom(what, false);
	}
	public void setRnwlspfrom(Object what, boolean rounded) {
		if (rounded)
			rnwlspfrom.setRounded(what);
		else
			rnwlspfrom.set(what);
	}	
	public PackedDecimalData getRnwlspto() {
		return rnwlspto;
	}
	public void setRnwlspto(Object what) {
		setRnwlspto(what, false);
	}
	public void setRnwlspto(Object what, boolean rounded) {
		if (rounded)
			rnwlspto.setRounded(what);
		else
			rnwlspto.set(what);
	}	
	public FixedLengthStringData getCommsupr() {
		return commsupr;
	}
	public void setCommsupr(Object what) {
		commsupr.set(what);
	}	
	public PackedDecimalData getCommspfrom() {
		return commspfrom;
	}
	public void setCommspfrom(Object what) {
		setCommspfrom(what, false);
	}
	public void setCommspfrom(Object what, boolean rounded) {
		if (rounded)
			commspfrom.setRounded(what);
		else
			commspfrom.set(what);
	}	
	public PackedDecimalData getCommspto() {
		return commspto;
	}
	public void setCommspto(Object what) {
		setCommspto(what, false);
	}
	public void setCommspto(Object what, boolean rounded) {
		if (rounded)
			commspto.setRounded(what);
		else
			commspto.set(what);
	}	
	public PackedDecimalData getSinstfrom() {
		return sinstfrom;
	}
	public void setSinstfrom(Object what) {
		setSinstfrom(what, false);
	}
	public void setSinstfrom(Object what, boolean rounded) {
		if (rounded)
			sinstfrom.setRounded(what);
		else
			sinstfrom.set(what);
	}	
	public PackedDecimalData getSinstto() {
		return sinstto;
	}
	public void setSinstto(Object what) {
		setSinstto(what, false);
	}
	public void setSinstto(Object what, boolean rounded) {
		if (rounded)
			sinstto.setRounded(what);
		else
			sinstto.set(what);
	}	
	public PackedDecimalData getSinstamt01() {
		return sinstamt01;
	}
	public void setSinstamt01(Object what) {
		setSinstamt01(what, false);
	}
	public void setSinstamt01(Object what, boolean rounded) {
		if (rounded)
			sinstamt01.setRounded(what);
		else
			sinstamt01.set(what);
	}	
	public PackedDecimalData getSinstamt02() {
		return sinstamt02;
	}
	public void setSinstamt02(Object what) {
		setSinstamt02(what, false);
	}
	public void setSinstamt02(Object what, boolean rounded) {
		if (rounded)
			sinstamt02.setRounded(what);
		else
			sinstamt02.set(what);
	}	
	public PackedDecimalData getSinstamt03() {
		return sinstamt03;
	}
	public void setSinstamt03(Object what) {
		setSinstamt03(what, false);
	}
	public void setSinstamt03(Object what, boolean rounded) {
		if (rounded)
			sinstamt03.setRounded(what);
		else
			sinstamt03.set(what);
	}	
	public PackedDecimalData getSinstamt04() {
		return sinstamt04;
	}
	public void setSinstamt04(Object what) {
		setSinstamt04(what, false);
	}
	public void setSinstamt04(Object what, boolean rounded) {
		if (rounded)
			sinstamt04.setRounded(what);
		else
			sinstamt04.set(what);
	}	
	public PackedDecimalData getSinstamt05() {
		return sinstamt05;
	}
	public void setSinstamt05(Object what) {
		setSinstamt05(what, false);
	}
	public void setSinstamt05(Object what, boolean rounded) {
		if (rounded)
			sinstamt05.setRounded(what);
		else
			sinstamt05.set(what);
	}	
	public PackedDecimalData getSinstamt06() {
		return sinstamt06;
	}
	public void setSinstamt06(Object what) {
		setSinstamt06(what, false);
	}
	public void setSinstamt06(Object what, boolean rounded) {
		if (rounded)
			sinstamt06.setRounded(what);
		else
			sinstamt06.set(what);
	}	
	public PackedDecimalData getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Object what) {
		setInstfrom(what, false);
	}
	public void setInstfrom(Object what, boolean rounded) {
		if (rounded)
			instfrom.setRounded(what);
		else
			instfrom.set(what);
	}	
	public PackedDecimalData getInstto() {
		return instto;
	}
	public void setInstto(Object what) {
		setInstto(what, false);
	}
	public void setInstto(Object what, boolean rounded) {
		if (rounded)
			instto.setRounded(what);
		else
			instto.set(what);
	}	
	public FixedLengthStringData getInstjctl() {
		return instjctl;
	}
	public void setInstjctl(Object what) {
		instjctl.set(what);
	}	
	public PackedDecimalData getInsttot01() {
		return insttot01;
	}
	public void setInsttot01(Object what) {
		setInsttot01(what, false);
	}
	public void setInsttot01(Object what, boolean rounded) {
		if (rounded)
			insttot01.setRounded(what);
		else
			insttot01.set(what);
	}	
	public PackedDecimalData getInsttot02() {
		return insttot02;
	}
	public void setInsttot02(Object what) {
		setInsttot02(what, false);
	}
	public void setInsttot02(Object what, boolean rounded) {
		if (rounded)
			insttot02.setRounded(what);
		else
			insttot02.set(what);
	}	
	public PackedDecimalData getInsttot03() {
		return insttot03;
	}
	public void setInsttot03(Object what) {
		setInsttot03(what, false);
	}
	public void setInsttot03(Object what, boolean rounded) {
		if (rounded)
			insttot03.setRounded(what);
		else
			insttot03.set(what);
	}	
	public PackedDecimalData getInsttot04() {
		return insttot04;
	}
	public void setInsttot04(Object what) {
		setInsttot04(what, false);
	}
	public void setInsttot04(Object what, boolean rounded) {
		if (rounded)
			insttot04.setRounded(what);
		else
			insttot04.set(what);
	}	
	public PackedDecimalData getInsttot05() {
		return insttot05;
	}
	public void setInsttot05(Object what) {
		setInsttot05(what, false);
	}
	public void setInsttot05(Object what, boolean rounded) {
		if (rounded)
			insttot05.setRounded(what);
		else
			insttot05.set(what);
	}	
	public PackedDecimalData getInsttot06() {
		return insttot06;
	}
	public void setInsttot06(Object what) {
		setInsttot06(what, false);
	}
	public void setInsttot06(Object what, boolean rounded) {
		if (rounded)
			insttot06.setRounded(what);
		else
			insttot06.set(what);
	}	
	public PackedDecimalData getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(Object what) {
		setOutstamt(what, false);
	}
	public void setOutstamt(Object what, boolean rounded) {
		if (rounded)
			outstamt.setRounded(what);
		else
			outstamt.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getBankcode() {
		return bankcode;
	}
	public void setBankcode(Object what) {
		bankcode.set(what);
	}	
	public FixedLengthStringData getMandref() {
		return mandref;
	}
	public void setMandref(Object what) {
		mandref.set(what);
	}	
	public FixedLengthStringData getChdrstcda() {
		return chdrstcda;
	}
	public void setChdrstcda(Object what) {
		chdrstcda.set(what);
	}	
	public FixedLengthStringData getChdrstcdb() {
		return chdrstcdb;
	}
	public void setChdrstcdb(Object what) {
		chdrstcdb.set(what);
	}	
	public FixedLengthStringData getChdrstcdc() {
		return chdrstcdc;
	}
	public void setChdrstcdc(Object what) {
		chdrstcdc.set(what);
	}	
	public FixedLengthStringData getChdrstcdd() {
		return chdrstcdd;
	}
	public void setChdrstcdd(Object what) {
		chdrstcdd.set(what);
	}	
	public FixedLengthStringData getChdrstcde() {
		return chdrstcde;
	}
	public void setChdrstcde(Object what) {
		chdrstcde.set(what);
	}	
	public FixedLengthStringData getPolpln() {
		return polpln;
	}
	public void setPolpln(Object what) {
		polpln.set(what);
	}	
	public FixedLengthStringData getChgflag() {
		return chgflag;
	}
	public void setChgflag(Object what) {
		chgflag.set(what);
	}	
	public FixedLengthStringData getLaprind() {
		return laprind;
	}
	public void setLaprind(Object what) {
		laprind.set(what);
	}	
	public FixedLengthStringData getSpecind() {
		return specind;
	}
	public void setSpecind(Object what) {
		specind.set(what);
	}	
	public FixedLengthStringData getDueflg() {
		return dueflg;
	}
	public void setDueflg(Object what) {
		dueflg.set(what);
	}	
	public FixedLengthStringData getBfcharge() {
		return bfcharge;
	}
	public void setBfcharge(Object what) {
		bfcharge.set(what);
	}	
	public PackedDecimalData getDishnrcnt() {
		return dishnrcnt;
	}
	public void setDishnrcnt(Object what) {
		setDishnrcnt(what, false);
	}
	public void setDishnrcnt(Object what, boolean rounded) {
		if (rounded)
			dishnrcnt.setRounded(what);
		else
			dishnrcnt.set(what);
	}	
	public FixedLengthStringData getPdtype() {
		return pdtype;
	}
	public void setPdtype(Object what) {
		pdtype.set(what);
	}	
	public PackedDecimalData getDishnrdte() {
		return dishnrdte;
	}
	public void setDishnrdte(Object what) {
		setDishnrdte(what, false);
	}
	public void setDishnrdte(Object what, boolean rounded) {
		if (rounded)
			dishnrdte.setRounded(what);
		else
			dishnrdte.set(what);
	}	
	public PackedDecimalData getStmpdtyamt() {
		return stmpdtyamt;
	}
	public void setStmpdtyamt(Object what) {
		setStmpdtyamt(what, false);
	}
	public void setStmpdtyamt(Object what, boolean rounded) {
		if (rounded)
			stmpdtyamt.setRounded(what);
		else
			stmpdtyamt.set(what);
	}	
	public PackedDecimalData getStmpdtydte() {
		return stmpdtydte;
	}
	public void setStmpdtydte(Object what) {
		setStmpdtydte(what, false);
	}
	public void setStmpdtydte(Object what, boolean rounded) {
		if (rounded)
			stmpdtydte.setRounded(what);
		else
			stmpdtydte.set(what);
	}	
	public PackedDecimalData getPolinc() {
		return polinc;
	}
	public void setPolinc(Object what) {
		setPolinc(what, false);
	}
	public void setPolinc(Object what, boolean rounded) {
		if (rounded)
			polinc.setRounded(what);
		else
			polinc.set(what);
	}	
	public PackedDecimalData getPolsum() {
		return polsum;
	}
	public void setPolsum(Object what) {
		setPolsum(what, false);
	}
	public void setPolsum(Object what, boolean rounded) {
		if (rounded)
			polsum.setRounded(what);
		else
			polsum.set(what);
	}	
	public PackedDecimalData getNxtsfx() {
		return nxtsfx;
	}
	public void setNxtsfx(Object what) {
		setNxtsfx(what, false);
	}
	public void setNxtsfx(Object what, boolean rounded) {
		if (rounded)
			nxtsfx.setRounded(what);
		else
			nxtsfx.set(what);
	}	
	public FixedLengthStringData getAvlisu() {
		return avlisu;
	}
	public void setAvlisu(Object what) {
		avlisu.set(what);
	}	
	public PackedDecimalData getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(Object what) {
		setStatementDate(what, false);
	}
	public void setStatementDate(Object what, boolean rounded) {
		if (rounded)
			statementDate.setRounded(what);
		else
			statementDate.set(what);
	}	
	public PackedDecimalData getCntiss() {
		return cntiss;
	}
	public void setCntiss(Object what) {
		setCntiss(what, false);
	}
	public void setCntiss(Object what, boolean rounded) {
		if (rounded)
			cntiss.setRounded(what);
		else
			cntiss.set(what);
	}	
	public PackedDecimalData getCntrcv() {
		return cntrcv;
	}
	public void setCntrcv(Object what) {
		setCntrcv(what, false);
	}
	public void setCntrcv(Object what, boolean rounded) {
		if (rounded)
			cntrcv.setRounded(what);
		else
			cntrcv.set(what);
	}	
	public PackedDecimalData getCoppn() {
		return coppn;
	}
	public void setCoppn(Object what) {
		setCoppn(what, false);
	}
	public void setCoppn(Object what, boolean rounded) {
		if (rounded)
			coppn.setRounded(what);
		else
			coppn.set(what);
	}	
	public FixedLengthStringData getCotype() {
		return cotype;
	}
	public void setCotype(Object what) {
		cotype.set(what);
	}	
	public FixedLengthStringData getCovernt() {
		return covernt;
	}
	public void setCovernt(Object what) {
		covernt.set(what);
	}	
	public FixedLengthStringData getDocnum() {
		return docnum;
	}
	public void setDocnum(Object what) {
		docnum.set(what);
	}	
	public PackedDecimalData getDtecan() {
		return dtecan;
	}
	public void setDtecan(Object what) {
		setDtecan(what, false);
	}
	public void setDtecan(Object what, boolean rounded) {
		if (rounded)
			dtecan.setRounded(what);
		else
			dtecan.set(what);
	}	
	public FixedLengthStringData getQuoteno() {
		return quoteno;
	}
	public void setQuoteno(Object what) {
		quoteno.set(what);
	}	
	public FixedLengthStringData getRnlsts() {
		return rnlsts;
	}
	public void setRnlsts(Object what) {
		rnlsts.set(what);
	}	
	public FixedLengthStringData getSustrcde() {
		return sustrcde;
	}
	public void setSustrcde(Object what) {
		sustrcde.set(what);
	}	
	public PackedDecimalData getBilldate01() {
		return billdate01;
	}
	public void setBilldate01(Object what) {
		setBilldate01(what, false);
	}
	public void setBilldate01(Object what, boolean rounded) {
		if (rounded)
			billdate01.setRounded(what);
		else
			billdate01.set(what);
	}	
	public PackedDecimalData getBilldate02() {
		return billdate02;
	}
	public void setBilldate02(Object what) {
		setBilldate02(what, false);
	}
	public void setBilldate02(Object what, boolean rounded) {
		if (rounded)
			billdate02.setRounded(what);
		else
			billdate02.set(what);
	}	
	public PackedDecimalData getBilldate03() {
		return billdate03;
	}
	public void setBilldate03(Object what) {
		setBilldate03(what, false);
	}
	public void setBilldate03(Object what, boolean rounded) {
		if (rounded)
			billdate03.setRounded(what);
		else
			billdate03.set(what);
	}	
	public PackedDecimalData getBilldate04() {
		return billdate04;
	}
	public void setBilldate04(Object what) {
		setBilldate04(what, false);
	}
	public void setBilldate04(Object what, boolean rounded) {
		if (rounded)
			billdate04.setRounded(what);
		else
			billdate04.set(what);
	}	
	public FixedLengthStringData getGrupkey() {
		return grupkey;
	}
	public void setGrupkey(Object what) {
		grupkey.set(what);
	}	
	public FixedLengthStringData getMembsel() {
		return membsel;
	}
	public void setMembsel(Object what) {
		membsel.set(what);
	}	
	public FixedLengthStringData getCampaign() {
		return campaign;
	}
	public void setCampaign(Object what) {
		campaign.set(what);
	}	
	public PackedDecimalData getPstatdate() {
		return pstatdate;
	}
	public void setPstatdate(Object what) {
		setPstatdate(what, false);
	}
	public void setPstatdate(Object what, boolean rounded) {
		if (rounded)
			pstatdate.setRounded(what);
		else
			pstatdate.set(what);
	}	
	public PackedDecimalData getFreeSwitchesUsed() {
		return freeSwitchesUsed;
	}
	public void setFreeSwitchesUsed(Object what) {
		setFreeSwitchesUsed(what, false);
	}
	public void setFreeSwitchesUsed(Object what, boolean rounded) {
		if (rounded)
			freeSwitchesUsed.setRounded(what);
		else
			freeSwitchesUsed.set(what);
	}	
	public PackedDecimalData getFreeSwitchesLeft() {
		return freeSwitchesLeft;
	}
	public void setFreeSwitchesLeft(Object what) {
		setFreeSwitchesLeft(what, false);
	}
	public void setFreeSwitchesLeft(Object what, boolean rounded) {
		if (rounded)
			freeSwitchesLeft.setRounded(what);
		else
			freeSwitchesLeft.set(what);
	}	
	public PackedDecimalData getLastSwitchDate() {
		return lastSwitchDate;
	}
	public void setLastSwitchDate(Object what) {
		setLastSwitchDate(what, false);
	}
	public void setLastSwitchDate(Object what, boolean rounded) {
		if (rounded)
			lastSwitchDate.setRounded(what);
		else
			lastSwitchDate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//ILIFE-2472
	public FixedLengthStringData getZmandref() {
		return zmandref;
	}
	//ILIFE-2472
	public void setZmandref(Object what) {
		zmandref.set(what);
	}
	//ILIFE-2472	
	public FixedLengthStringData getReqntype() {
		return reqntype;
	}
	//ILIFE-2472	
	public void setReqntype(Object what) {
		reqntype.set(what);
	}
	
	//ILIFE-2472 PH2
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	//ILIFE-2472 PH2	
	public void setPayclt(Object what) {
		payclt.set(what);
	}
	public FixedLengthStringData getCownnum2() {
		return cownnum2;
	}
	//ILIFE-7277-start
	public void setCownnum2(Object what) {
		cownnum2.set(what);
	}
	public FixedLengthStringData getCownnum3() {
		return cownnum3;
	}
	public void setCownnum3(Object what) {
		cownnum3.set(what);
	}
	public FixedLengthStringData getCownnum4() {
		return cownnum4;
	}
	public void setCownnum4(Object what) {
		cownnum4.set(what);
	}
	public FixedLengthStringData getCownnum5() {
		return cownnum5;
	}
	public void setCownnum5(Object what) {
		cownnum5.set(what);
	}
	//ILIFE-7277-end
	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSinstamts() {
		return new FixedLengthStringData(sinstamt01.toInternal()
										+ sinstamt02.toInternal()
										+ sinstamt03.toInternal()
										+ sinstamt04.toInternal()
										+ sinstamt05.toInternal()
										+ sinstamt06.toInternal());
	}
	public void setSinstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSinstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, sinstamt01);
		what = ExternalData.chop(what, sinstamt02);
		what = ExternalData.chop(what, sinstamt03);
		what = ExternalData.chop(what, sinstamt04);
		what = ExternalData.chop(what, sinstamt05);
		what = ExternalData.chop(what, sinstamt06);
	}
	public PackedDecimalData getSinstamt(BaseData indx) {
		return getSinstamt(indx.toInt());
	}
	public PackedDecimalData getSinstamt(int indx) {

		switch (indx) {
			case 1 : return sinstamt01;
			case 2 : return sinstamt02;
			case 3 : return sinstamt03;
			case 4 : return sinstamt04;
			case 5 : return sinstamt05;
			case 6 : return sinstamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSinstamt(BaseData indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(BaseData indx, Object what, boolean rounded) {
		setSinstamt(indx.toInt(), what, rounded);
	}
	public void setSinstamt(int indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSinstamt01(what, rounded);
					 break;
			case 2 : setSinstamt02(what, rounded);
					 break;
			case 3 : setSinstamt03(what, rounded);
					 break;
			case 4 : setSinstamt04(what, rounded);
					 break;
			case 5 : setSinstamt05(what, rounded);
					 break;
			case 6 : setSinstamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInsttots() {
		return new FixedLengthStringData(insttot01.toInternal()
										+ insttot02.toInternal()
										+ insttot03.toInternal()
										+ insttot04.toInternal()
										+ insttot05.toInternal());
	}
	public void setInsttots(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInsttots().getLength()).init(obj);
	
		what = ExternalData.chop(what, insttot01);
		what = ExternalData.chop(what, insttot02);
		what = ExternalData.chop(what, insttot03);
		what = ExternalData.chop(what, insttot04);
		what = ExternalData.chop(what, insttot05);
	}
	public PackedDecimalData getInsttot(BaseData indx) {
		return getInsttot(indx.toInt());
	}
	public PackedDecimalData getInsttot(int indx) {

		switch (indx) {
			case 1 : return insttot01;
			case 2 : return insttot02;
			case 3 : return insttot03;
			case 4 : return insttot04;
			case 5 : return insttot05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInsttot(BaseData indx, Object what) {
		setInsttot(indx, what, false);
	}
	public void setInsttot(BaseData indx, Object what, boolean rounded) {
		setInsttot(indx.toInt(), what, rounded);
	}
	public void setInsttot(int indx, Object what) {
		setInsttot(indx, what, false);
	}
	public void setInsttot(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInsttot01(what, rounded);
					 break;
			case 2 : setInsttot02(what, rounded);
					 break;
			case 3 : setInsttot03(what, rounded);
					 break;
			case 4 : setInsttot04(what, rounded);
					 break;
			case 5 : setInsttot05(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBilldates() {
		return new FixedLengthStringData(billdate01.toInternal()
										+ billdate02.toInternal()
										+ billdate03.toInternal()
										+ billdate04.toInternal());
	}
	public void setBilldates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBilldates().getLength()).init(obj);
	
		what = ExternalData.chop(what, billdate01);
		what = ExternalData.chop(what, billdate02);
		what = ExternalData.chop(what, billdate03);
		what = ExternalData.chop(what, billdate04);
	}
	public PackedDecimalData getBilldate(BaseData indx) {
		return getBilldate(indx.toInt());
	}
	public PackedDecimalData getBilldate(int indx) {

		switch (indx) {
			case 1 : return billdate01;
			case 2 : return billdate02;
			case 3 : return billdate03;
			case 4 : return billdate04;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBilldate(BaseData indx, Object what) {
		setBilldate(indx, what, false);
	}
	public void setBilldate(BaseData indx, Object what, boolean rounded) {
		setBilldate(indx.toInt(), what, rounded);
	}
	public void setBilldate(int indx, Object what) {
		setBilldate(indx, what, false);
	}
	public void setBilldate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBilldate01(what, rounded);
					 break;
			case 2 : setBilldate02(what, rounded);
					 break;
			case 3 : setBilldate03(what, rounded);
					 break;
			case 4 : setBilldate04(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAnnamts() {
		return new FixedLengthStringData(annamt01.toInternal()
										+ annamt02.toInternal()
										+ annamt03.toInternal()
										+ annamt04.toInternal()
										+ annamt05.toInternal()
										+ annamt06.toInternal());
	}
	public void setAnnamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnnamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, annamt01);
		what = ExternalData.chop(what, annamt02);
		what = ExternalData.chop(what, annamt03);
		what = ExternalData.chop(what, annamt04);
		what = ExternalData.chop(what, annamt05);
		what = ExternalData.chop(what, annamt06);
	}
	public PackedDecimalData getAnnamt(BaseData indx) {
		return getAnnamt(indx.toInt());
	}
	public PackedDecimalData getAnnamt(int indx) {

		switch (indx) {
			case 1 : return annamt01;
			case 2 : return annamt02;
			case 3 : return annamt03;
			case 4 : return annamt04;
			case 5 : return annamt05;
			case 6 : return annamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnnamt(BaseData indx, Object what) {
		setAnnamt(indx, what, false);
	}
	public void setAnnamt(BaseData indx, Object what, boolean rounded) {
		setAnnamt(indx.toInt(), what, rounded);
	}
	public void setAnnamt(int indx, Object what) {
		setAnnamt(indx, what, false);
	}
	public void setAnnamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAnnamt01(what, rounded);
					 break;
			case 2 : setAnnamt02(what, rounded);
					 break;
			case 3 : setAnnamt03(what, rounded);
					 break;
			case 4 : setAnnamt04(what, rounded);
					 break;
			case 5 : setAnnamt05(what, rounded);
					 break;
			case 6 : setAnnamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		chdrpfx.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		tranno.clear();
		tranid.clear();
		validflag.clear();
		recode.clear();
		servunit.clear();
		proctrancd.clear();
		procflag.clear();
		procid.clear();
		statreasn.clear();
		tranlused.clear();
		ccdate.clear();
		crdate.clear();
		annamt01.clear();
		annamt02.clear();
		annamt03.clear();
		annamt04.clear();
		annamt05.clear();
		annamt06.clear();
		currto.clear();
		currfrom.clear();
		rnltype.clear();
		rnlnots.clear();
		rnlnotto.clear();
		rnlattn.clear();
		rnldurn.clear();
		repnum.clear();
		occdate.clear();
		ptdate.clear();
		btdate.clear();
		statcode.clear();
		statdate.clear();
		stattran.clear();
		pstatcode.clear();
		pstattran.clear();
		cntbranch.clear();
		cnttype.clear();
		cntcurr.clear();
		acctccy.clear();
		crate.clear();
		payplan.clear();
		acctmeth.clear();
		billcurr.clear();
		billfreq.clear();
		billchnl.clear();
		collchnl.clear();
		instbchnl.clear();
		instcchnl.clear();
		instfreq.clear();
		billcd.clear();
		billday.clear();
		billmonth.clear();
		payflag.clear();
		cownpfx.clear();
		cownnum.clear();
		cowncoy.clear();
		jownnum.clear();
		payrpfx.clear();
		payrcoy.clear();
		payrnum.clear();
		asgnpfx.clear();
		asgncoy.clear();
		asgnnum.clear();
		desppfx.clear();
		despcoy.clear();
		despnum.clear();
		agntpfx.clear();
		agntcoy.clear();
		agntnum.clear();
		srcebus.clear();
		reptype.clear();
		register.clear();
		aplsupr.clear();
		aplspfrom.clear();
		aplspto.clear();
		billsupr.clear();
		billspfrom.clear();
		billspto.clear();
		notssupr.clear();
		notsspfrom.clear();
		notsspto.clear();
		rnwlsupr.clear();
		rnwlspfrom.clear();
		rnwlspto.clear();
		commsupr.clear();
		commspfrom.clear();
		commspto.clear();
		sinstfrom.clear();
		sinstto.clear();
		sinstamt01.clear();
		sinstamt02.clear();
		sinstamt03.clear();
		sinstamt04.clear();
		sinstamt05.clear();
		sinstamt06.clear();
		instfrom.clear();
		instto.clear();
		instjctl.clear();
		insttot01.clear();
		insttot02.clear();
		insttot03.clear();
		insttot04.clear();
		insttot05.clear();
		insttot06.clear();
		outstamt.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		bankcode.clear();
		mandref.clear();
		chdrstcda.clear();
		chdrstcdb.clear();
		chdrstcdc.clear();
		chdrstcdd.clear();
		chdrstcde.clear();
		polpln.clear();
		chgflag.clear();
		laprind.clear();
		specind.clear();
		dueflg.clear();
		bfcharge.clear();
		dishnrcnt.clear();
		pdtype.clear();
		dishnrdte.clear();
		stmpdtyamt.clear();
		stmpdtydte.clear();
		polinc.clear();
		polsum.clear();
		nxtsfx.clear();
		avlisu.clear();
		statementDate.clear();
		cntiss.clear();
		cntrcv.clear();
		coppn.clear();
		cotype.clear();
		covernt.clear();
		docnum.clear();
		dtecan.clear();
		quoteno.clear();
		rnlsts.clear();
		sustrcde.clear();
		billdate01.clear();
		billdate02.clear();
		billdate03.clear();
		billdate04.clear();
		grupkey.clear();
		membsel.clear();
		campaign.clear();
		pstatdate.clear();
		freeSwitchesUsed.clear();
		freeSwitchesLeft.clear();
		lastSwitchDate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		zmandref.clear(); //ILIFE-2472
  		reqntype.clear(); //ILIFE-2472
  		payclt.clear();//ILIFE-2472 PH2
        //ILIFE-7277-start
  		cownnum2.clear();
  		cownnum3.clear();
  		cownnum4.clear();
  		cownnum5.clear();
  	    //ILIFE-7277-end
	}


}