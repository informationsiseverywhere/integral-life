package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for ST523
 * @version 1.0 generated on 30/08/09 07:27
 * @author Quipoz
 */
public class St523ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(727);
	public FixedLengthStringData dataFields = new FixedLengthStringData(343).isAPartOf(dataArea, 0);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,60);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,70);
	public ZonedDecimalData fromdate = DD.fromdate.copyToZonedDecimal().isAPartOf(dataFields,78);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,86);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,94);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,141);
	public ZonedDecimalData orgamnt = DD.orgamnt.copyToZonedDecimal().isAPartOf(dataFields,149);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,166);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,213);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,223);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,231);
	public FixedLengthStringData sdei = DD.sdei.copy().isAPartOf(dataFields,241);
	public ZonedDecimalData tdbtamt = DD.tdbtamt.copyToZonedDecimal().isAPartOf(dataFields,278);
	public FixedLengthStringData tdbtdesc = DD.tdbtdesc.copy().isAPartOf(dataFields,295);
	public ZonedDecimalData tdbtrate = DD.tdbtrate.copyToZonedDecimal().isAPartOf(dataFields,325);
	public ZonedDecimalData todate = DD.todate.copyToZonedDecimal().isAPartOf(dataFields,329);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(dataFields,337);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(dataFields,342);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 343);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData fromdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData orgamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData sdeiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData tdbtamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData tdbtdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData tdbtrateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData todateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(288).isAPartOf(dataArea, 439);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] fromdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] orgamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] sdeiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] tdbtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] tdbtdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] tdbtrateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] todateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fromdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData todateDisp = new FixedLengthStringData(10);

	public LongData St523screenWritten = new LongData(0);
	public LongData St523protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public St523ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(orgamntOut,new String[] {"25","31","-25","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(fromdateOut,new String[] {"27","31","-27","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdbtdescOut,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdbtamtOut,new String[] {"30","-31","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdbtrateOut,new String[] {"26","31","-26","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(todateOut,new String[] {"28","31","-28","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = new BaseData[] {sdei, ptdate, occdate, cownnum, lifcnum, chdrnum, cnttype, ctypedes, linsname, ownername, rstate, btdate, pstate, currcd, currds, tranno, effdate, orgamnt, fromdate, tdbtdesc, tdbtamt, validflag, tdbtrate, todate};
		screenOutFields = new BaseData[][] {sdeiOut, ptdateOut, occdateOut, cownnumOut, lifcnumOut, chdrnumOut, cnttypeOut, ctypedesOut, linsnameOut, ownernameOut, rstateOut, btdateOut, pstateOut, currcdOut, currdsOut, trannoOut, effdateOut, orgamntOut, fromdateOut, tdbtdescOut, tdbtamtOut, validflagOut, tdbtrateOut, todateOut};
		screenErrFields = new BaseData[] {sdeiErr, ptdateErr, occdateErr, cownnumErr, lifcnumErr, chdrnumErr, cnttypeErr, ctypedesErr, linsnameErr, ownernameErr, rstateErr, btdateErr, pstateErr, currcdErr, currdsErr, trannoErr, effdateErr, orgamntErr, fromdateErr, tdbtdescErr, tdbtamtErr, validflagErr, tdbtrateErr, todateErr};
		screenDateFields = new BaseData[] {ptdate, occdate, btdate, effdate, fromdate, todate};
		screenDateErrFields = new BaseData[] {ptdateErr, occdateErr, btdateErr, effdateErr, fromdateErr, todateErr};
		screenDateDispFields = new BaseData[] {ptdateDisp, occdateDisp, btdateDisp, effdateDisp, fromdateDisp, todateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = St523screen.class;
		protectRecord = St523protect.class;
	}

}
