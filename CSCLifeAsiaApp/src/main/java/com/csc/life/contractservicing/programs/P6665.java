/*
 * File: P6665.java
 * Date: 30 August 2009 0:49:07
 * Author: Quipoz Limited
 *
 * Class transformed from P6665.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.fsu.clients.dataaccess.ClbaTableDAM;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.BcrdTableDAM;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.tablestructures.T3684rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.screens.S6665ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*            P6665 - Billing Change Create Mandate
*            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Overview
*~~~~~~~~
*One  of  the  facilities  provided  by  the  billing  change
*subsystem is to change direct debit details.  If the  Prompt
*Function Key is pressed on the MANDREF field, a list of  all
*the  mandates belonging to the payer will be displayed.  Use
*of the appropriate Function Key allows the user to create  a
*new mandate. To cater for this, P6665  will  have  the  same
*function as P6278. The reason for not using  P6278  is  that
*it  uses  the  logical  CHDRLNB and at billing change we use
*CHDRMJA. This program is to be obtained from T1690.
*
*Tables
*~~~~~~
*Set  up  an  item  'T105'  in  T1688   for   this   creation
*transaction. (Already created)
*Modify   the   item   'P6276'   in   T1690  to  insert  this
*transaction. Add the following to the item content:-
*    Action: B
*    PROG01: P6665
*    Transaction code: T105
*    Batch Required: N
*
*S6665
*~~~~~
*Copy the display file from S6278.
*Change the description of the file.
*Compile the screen with type *DSPF.
*
*P6665
*~~~~~
*Copy the source from P6278.
*Replace all 'P6278' by 'P6665'.
*Replace all 'S6278' by 'S6665'.
*Replace all 'CHDRLNB' by 'CHDRMJA'.
*Remove all commented-out codings.
*Change the description of the program.
*Specify the function of  the  program  at  the  top  of  the
*program.
*
*Include validation in the 2000 section to check the Current
*From and Current To dates of the bank account against the
*effective date of the mandate.
*
*If Current From date of the bank account > effective date (i.e.
*the bank account is not yet effective) then display an error
*message.
*
*If Current To date of the bank account < effective date (i.e.
*the bank account is no longer effective) then display an error
*message.
*
*****************************************************************
* </pre>
*/
public class P6665 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6665");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaMandrefer = new FixedLengthStringData(5);
	private ZonedDecimalData wsaaMandref = new ZonedDecimalData(5, 0).isAPartOf(wsaaMandrefer, 0).setUnsigned();

	private FixedLengthStringData wsaaBillcd = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBillcdDay = new FixedLengthStringData(2).isAPartOf(wsaaBillcd, 0);
	private FixedLengthStringData fillerillcdDay = new FixedLengthStringData(1).isAPartOf(wsaaBillcd, 2);
	private FixedLengthStringData wsaaBillcdMonth = new FixedLengthStringData(2).isAPartOf(wsaaBillcd, 3);
	private FixedLengthStringData fillerillcdMonth = new FixedLengthStringData(1).isAPartOf(wsaaBillcd, 5);
	private FixedLengthStringData wsaaBillcdYear = new FixedLengthStringData(4).isAPartOf(wsaaBillcd, 6);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 0);
	private FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 30);
		/* ERRORS */
	private String ev02 = "EV02";
	private String e186 = "E186";
	private String e304 = "E304";
	private String e756 = "E756";
	private String g600 = "G600";
	private String h046 = "H046";
	private String f053 = "F053";
	private String f054 = "F054";
	private String f373 = "F373";
	private String f955 = "F955";
	private String i020 = "I020";
		/* TABLES */
	private String t3684 = "T3684";
	private String t1693 = "T1693";
	private String t3678 = "T3678";
	private String t3000 = "T3000";
	private String mandrec = "MANDREC";
	private String bcrdrec = "BCRDREC";
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*ACCESS CARD FILE*/
	private BcrdTableDAM bcrdIO = new BcrdTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClbaTableDAM clbaIO = new ClbaTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Mandate Logical File*/
	private MandTableDAM mandIO = new MandTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T3684rec t3684rec = new T3684rec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private S6665ScreenVars sv = ScreenProgram.getScreenVars( S6665ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		lgnmExit,
		plainExit,
		payeeExit,
		preExit,
		readT36842030,
		checkAccount2040,
		checkForErrors2080,
		exit2090,
		exit3090
	}

	public P6665() {
		super();
		screenVars = sv;
		new ScreenModel("S6665", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.payrcoy.set(SPACES);
		sv.payrnum.set(SPACES);
		sv.bankkey.set(SPACES);
		sv.bankacckey.set(SPACES);
		sv.mandref.set(SPACES);
		sv.mandstat.set(SPACES);
		sv.timesUse.set(ZERO);
		sv.mandAmt.set(ZERO);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		sv.numsel.set(wsspwindow.payrnum);
		wsspcomn.clntkey.set(SPACES);
		sv.currcode.set(SPACES);
		sv.facthous.set(SPACES);
		mandIO.setParams(SPACES);
		mandIO.setPayrcoy(wsspcomn.fsuco);
		mandIO.setPayrnum(wsspwindow.payrnum);
		mandIO.setMandref("99999");
		mandIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mandIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mandIO.setFitKeysSearch("PAYRCOY", "PAYRNUM");
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(),varcom.oK)
		&& isNE(mandIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		if (isEQ(mandIO.getStatuz(),varcom.endp)
		|| isNE(mandIO.getPayrcoy(),wsspcomn.fsuco)
		|| isNE(mandIO.getPayrnum(),wsspwindow.payrnum)) {
			sv.mandref.set("00001");
		}
		else {
			wsaaMandrefer.set(mandIO.getMandref());
			compute(wsaaMandref, 0).set(add(wsaaMandref,1));
			sv.mandref.set(wsaaMandrefer);
		}
		sv.payrcoy.set(wsspcomn.fsuco);
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(sv.payrcoy);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.compdesc.set(descIO.getLongdesc());
		}
		sv.payrnum.set(wsspwindow.payrnum);
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),"1")) {
			sv.payrnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.timesUse.set(999);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		sv.effdate.set(datcon1rec.intDate);
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case readT36842030: {
					readT36842030();
				}
				case checkAccount2040: {
					checkAccount2040();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validate2020()
	{
		if (isLTE(sv.timesUse,0)) {
			sv.timesuseErr.set(ev02);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.effdate,ZERO)
		|| isEQ(sv.effdate,varcom.vrcmMaxDate)) {
			sv.effdateErr.set(h046);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.bankkey,SPACES)) {
			sv.bankkeyErr.set(e186);
			goTo(GotoLabel.checkAccount2040);
		}
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(e756);
			goTo(GotoLabel.checkAccount2040);
		}
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		if (isEQ(sv.facthous,SPACES)) {
			sv.facthousErr.set(e186);
		}
		else {
			goTo(GotoLabel.readT36842030);
		}
	}

protected void readT36842030()
	{
		clblIO.setParams(SPACES);
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)
		&& isNE(clblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankacckeyErr.set(g600);
			goTo(GotoLabel.checkForErrors2080);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t3684);
		itemIO.setItemitem(clblIO.getFacthous());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t3684rec.t3684Rec.set(itemIO.getGenarea());
		}
		if (isEQ(t3684rec.mandamtType,"2")) {
			sv.mandAmt.set(ZERO);
			sv.mandamtOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t3684rec.mandamtType,"1")) {
			if (isLTE(sv.mandAmt,0)) {
				sv.mandamtErr.set(ev02);
				wsspcomn.edterror.set("Y");
			}
		}
		sv.mandstat.set(t3684rec.mandstat);
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3678);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(t3684rec.mandstat);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.statdets.set(descIO.getLongdesc());
		}
	}

protected void checkAccount2040()
	{
		if (isEQ(sv.bankacckey,SPACES)) {
			sv.bankacckeyErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)
		&& isNE(clblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankacckeyErr.set(g600);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isGT(clblIO.getCurrfrom(),sv.effdate)) {
			sv.bankacckeyErr.set(i020);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(clblIO.getCurrto(),ZERO)) {
			if (isLT(clblIO.getCurrto(),sv.effdate)) {
				sv.bankacckeyErr.set(i020);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.bankacckeyErr.set(f373);
			goTo(GotoLabel.checkForErrors2080);
		}
		sv.currcode.set(clblIO.getCurrcode());
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3000);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(sv.currcode);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.curdesc.set(descIO.getLongdesc());
		}
		if (isNE(chdrmjaIO.getBillcurr(),clblIO.getCurrcode())) {
			sv.bankacckeyErr.set(f955);
			goTo(GotoLabel.checkForErrors2080);
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isLT(sv.effdate,datcon1rec.intDate)) {
			if (isNE(sv.effdate,wsaaEffdate)) {
				wsaaEffdate.set(sv.effdate);
				sv.effdateErr.set(f053);
				scrnparams.errorCode.set(f054);
				wsspcomn.edterror.set("Y");
			}
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
			keepBcrd3020();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		mandIO.setParams(SPACES);
		mandIO.setPayrcoy(sv.payrcoy);
		mandIO.setPayrnum(sv.payrnum);
		mandIO.setMandref(sv.mandref);
		mandIO.setValidflag("1");
		mandIO.setBankkey(sv.bankkey);
		mandIO.setBankacckey(sv.bankacckey);
		mandIO.setTimesUse(sv.timesUse);
		mandIO.setMandstat(sv.mandstat);
		mandIO.setStatChangeDate(varcom.vrcmMaxDate);
		mandIO.setLastUseDate(varcom.vrcmMaxDate);
		mandIO.setLastUseAmt(ZERO);
		mandIO.setLuStatCode(SPACES);
		mandIO.setMandAmt(sv.mandAmt);
		mandIO.setEffdate(sv.effdate);
		mandIO.setTransactionTime(varcom.vrcmTime);
		mandIO.setTransactionDate(varcom.vrcmDate);
		mandIO.setTermid(varcom.vrcmTermid);
		mandIO.setUser(varcom.vrcmUser);
		mandIO.setPayind("C");//ILIFE-2472  
		mandIO.setFormat(mandrec);
		mandIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		wsspwindow.value.set(mandIO.getMandref());
		wsspwindow.bankkey.set(mandIO.getBankkey());
		wsspwindow.bankacckey.set(mandIO.getBankacckey());
	}

protected void keepBcrd3020()
	{
		bcrdIO.setBankacckey(mandIO.getBankacckey());
		bcrdIO.setPayrcoy(mandIO.getPayrcoy());
		bcrdIO.setPayrnum(mandIO.getPayrnum());
		bcrdIO.setMandref(mandIO.getMandref());
		bcrdIO.setFormat(bcrdrec);
		bcrdIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, bcrdIO);
		if (isNE(bcrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bcrdIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
