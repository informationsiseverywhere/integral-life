package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR518
 * @version 1.0 generated on 30/08/09 07:16
 * @author Quipoz
 */
public class Sr518ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1417);
	public FixedLengthStringData dataFields = new FixedLengthStringData(505).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData anntind = DD.anntind.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,4);
	public ZonedDecimalData benCessDate = DD.bcesdte.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData benCessAge = DD.bcessage.copyToZonedDecimal().isAPartOf(dataFields,16);
	public ZonedDecimalData benCessTerm = DD.bcesstrm.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,66);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,76);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,148);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,150);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,158);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,160);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,207);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,208);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,212);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,213);
	public ZonedDecimalData premCessDate = DD.pcesdte.copyToZonedDecimal().isAPartOf(dataFields,214);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,222);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,225);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,228);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,232);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,240);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,243);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,246);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,248);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,249);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,250);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,252);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,256);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,273);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,274);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,287);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,304);
	public ZonedDecimalData zrsumin = DD.zrsumin.copyToZonedDecimal().isAPartOf(dataFields,321);
	/*BRD-306 START */
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 334);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 337);
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 367);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 384);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 401);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 418);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 435);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,452);
	public ZonedDecimalData linstamt = DD.linstamt.copyToZonedDecimal().isAPartOf(dataFields,460);
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,477);
	/*BRD-306 END */
	public FixedLengthStringData waitperiod = DD.waitperiod.copy().isAPartOf(dataFields,494);
	public FixedLengthStringData bentrm = DD.bentrm.copy().isAPartOf(dataFields,497);
	public FixedLengthStringData poltyp = DD.zpoltyp.copy().isAPartOf(dataFields,499);
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,500);
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,501);//BRD-NBP-011
	public FixedLengthStringData exclind = DD.optextind.copy().isAPartOf(dataFields,504);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 505);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData anntindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData pcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData zrsuminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	/*BRD-306 START */
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData linstamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);	
	/*BRD-306 END */
	public FixedLengthStringData waitperiodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData bentrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData poltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);//BRD-NBP-011
	public FixedLengthStringData exclindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(684).isAPartOf(dataArea, 733);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] anntindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] pcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] zrsuminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	/*BRD-306 START */
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] linstamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[]zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	/*BRD-306 END */
	public FixedLengthStringData[]waitperiodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[]bentrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[]poltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);//BRD-NBP-011
	public FixedLengthStringData[] exclindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	/*BRD-306 START */
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	/*BRD-306 END */
	public FixedLengthStringData benCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);

	public LongData Sr518screenWritten = new LongData(0);
	public LongData Sr518protectWritten = new LongData(0);	
	

	public boolean hasSubfile() {
		return false;
	}


	public Sr518ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrsuminOut,new String[] {"14","02","-14","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqdescOut,new String[] {null, null, null, "01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcessageOut,new String[] {"15","03","-15","88", null, null, null, null, null, null, null, null});//ILJ-47
		fieldIndMap.put(rcesstrmOut,new String[] {"16","04","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesdteOut,new String[] {"29","28","-29","89", null, null, null, null, null, null, null, null});//ILJ-47
		fieldIndMap.put(pcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"19","06","-19","05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"20","08","-20","07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {"38","39","-38","41",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"21","09","-21","09",null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"22","31","-22","32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(anntindOut,new String[] {"34","35","-34","33",null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"23","26","-23","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"36","37","-36","37",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zlinstpremOut,new String[] {null, null, null, "32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {"36", "37", "-36", "42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"36","37","-36","42",null, null, null, null, null, null, null, null});
		/*BRD-306 START */
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		fieldIndMap.put(linstamtOut,new String[] {"22","31","-22","32",null, null, null, null, null, null, null, null});
		/*BRD-306 END */
		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(waitperiodOut,new String[] {"58", "59", "-58", "54", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bentrmOut,new String[] {"60", "61", "-60", "55", null, null, null, null, null, null, null, null});
		fieldIndMap.put(poltypOut,new String[] {"62", "63", "-62", "56", null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmbasisOut,new String[] {"64", "65", "-64", "57", null, null, null, null, null, null, null, null});
		//ILIFE-3399-STARTS
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "66", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "67", null, null, null, null, null, null, null, null});
		//ILIFE-3399-ENDS
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(exclindOut,new String[] {null, null, null, "73", null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifcnum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, numpols, planSuffix, zrsumin, frqdesc, currcd, riskCessAge, riskCessTerm, riskCessDate, premCessAge, premCessTerm, premCessDate, benCessAge, benCessTerm, benCessDate, mortcls, liencd, bappmeth, optextind, instPrem, anntind, comind, select, pbind, zlinstprem, taxamt, taxind,loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt,linstamt,zstpduty01,waitperiod,bentrm,poltyp,prmbasis,dialdownoption,exclind};
		screenOutFields = new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, numpolsOut, plnsfxOut, zrsuminOut, frqdescOut, currcdOut, rcessageOut, rcesstrmOut, rcesdteOut, pcessageOut, pcesstrmOut, pcesdteOut, bcessageOut, bcesstrmOut, bcesdteOut, mortclsOut, liencdOut, bappmethOut, optextindOut, instprmOut, anntindOut, comindOut, selectOut, pbindOut, zlinstpremOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut,linstamtOut,zstpduty01Out,waitperiodOut,bentrmOut,poltypOut,prmbasisOut,dialdownoptionOut,exclindOut};
		screenErrFields = new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, numpolsErr, plnsfxErr, zrsuminErr, frqdescErr, currcdErr, rcessageErr, rcesstrmErr, rcesdteErr, pcessageErr, pcesstrmErr, pcesdteErr, bcessageErr, bcesstrmErr, bcesdteErr, mortclsErr, liencdErr, bappmethErr, optextindErr, instprmErr, anntindErr, comindErr, selectErr, pbindErr, zlinstpremErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr,linstamtErr,zstpduty01Err,waitperiodErr,bentrmErr,poltypErr,prmbasisErr,dialdownoptionErr,exclindErr};
		screenDateFields = new BaseData[] {riskCessDate, premCessDate, benCessDate,effdate};
		screenDateErrFields = new BaseData[] {rcesdteErr, pcesdteErr, bcesdteErr,effdateErr};
		screenDateDispFields = new BaseData[] {riskCessDateDisp, premCessDateDisp, benCessDateDisp,effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr518screen.class;
		protectRecord = Sr518protect.class;
	}

}
