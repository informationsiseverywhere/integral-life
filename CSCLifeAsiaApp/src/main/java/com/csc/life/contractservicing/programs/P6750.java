/*
 * File: P6750.java
 * Date: 30 August 2009 0:55:29
 * Author: Quipoz Limited
 * 
 * Class transformed from P6750.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.contractservicing.screens.S6750ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                Windforward Transaction Selection
*                =================================
*
* Overview
* --------
*
*      This program is accessed from the Insert Windforward
* Transaction screen when windowing on the Transaction Code
* field.  The screen will display all transactions eligible for
* Windforward for this contract type.  This is based upon the
* entries on the Windforward Transactions table.  Selecting one
* of the transactions listed will return the user back to the
* Insert Windforward Transaction screen where the transaction
* selected will be displayed on the screen.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6750 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6750");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRedefines = new FixedLengthStringData(200);
	private FixedLengthStringData wsaaCntyp = new FixedLengthStringData(3).isAPartOf(wsaaRedefines, 0);

	private FixedLengthStringData wsaaT6757Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6757Key, 0);
	private FixedLengthStringData wsaaTrcode = new FixedLengthStringData(4).isAPartOf(wsaaT6757Key, 3);
		/* TABLES */
	private String t6757 = "T6757";
		/* FORMATS */
	private String itdmrec = "ITEMREC";
	private String descrec = "DESCREC";
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private S6750ScreenVars sv = ScreenProgram.getScreenVars( S6750ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNext1150, 
		preExit, 
		exit2090, 
		exit2290
	}

	public P6750() {
		super();
		screenVars = sv;
		new ScreenModel("S6750", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6750", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		wsaaRedefines.set(wsspwindow.additionalFields);
		itdmIO.setParams(SPACES);
		itdmIO.setItemtabl(t6757);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(datcon1rec.intDate);
		wsaaCnttype.set(wsaaCntyp);
		wsaaTrcode.set(SPACES);
		itdmIO.setItemitem(wsaaT6757Key);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(subString(itdmIO.getItemitem(), 1, 3),wsaaCnttype)) {
			itdmIO.setStatuz(varcom.endp);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,sv.subfilePage)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)); wsaaSub.add(1)){
			loadSubfile1100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
	}

protected void loadSubfile1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					load1110();
				}
				case readNext1150: {
					readNext1150();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void load1110()
	{
		if (isGT(itdmIO.getItmfrm(),datcon1rec.intDate)
		|| isLT(itdmIO.getItmto(),datcon1rec.intDate)) {
			goTo(GotoLabel.readNext1150);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6757);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(itdmIO.getItemitem());
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.transCode.set(subString(itdmIO.getItemitem(), 4, 4));
		sv.descr.set(descIO.getLongdesc());
		scrnparams.function.set(varcom.sadd);
		processScreen("S6750", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNext1150()
	{
		itdmIO.setItmfrm(datcon1rec.intDate);
		itdmIO.setFunction(varcom.nextr);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(subString(itdmIO.getItemitem(), 1, 3),wsaaCnttype)) {
			itdmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			selection2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			rollUp2100();
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void selection2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6750", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			selectTransaction2200();
		}
		
	}

protected void rollUp2100()
	{
		/*ROLL*/
		for (wsaaSub.set(1); !(isGT(wsaaSub,sv.subfilePage)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)); wsaaSub.add(1)){
			loadSubfile1100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		wsspcomn.edterror.set("Y");
		/*EXIT*/
	}

protected void selectTransaction2200()
	{
		try {
			validate2210();
		}
		catch (GOTOException e){
		}
	}

protected void validate2210()
	{
		if (isNE(sv.select,SPACES)) {
			wsspwindow.value.set(sv.transCode);
			wsspwindow.confirmation.set(sv.descr);
			scrnparams.statuz.set(varcom.endp);
			goTo(GotoLabel.exit2290);
		}
		scrnparams.function.set(varcom.srnch);
		processScreen("S6750", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(SPACES);
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
