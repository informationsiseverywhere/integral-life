/*
 * File: P5047.java
 * Date: 30 August 2009 0:00:06
 * Author: Quipoz Limited
 * 
 * Class transformed from P5047.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.screens.S5047ScreenVars;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.regularprocessing.tablestructures.T5654rec;
import com.csc.life.regularprocessing.tablestructures.T6626rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifesurTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Automatic Increase Refusal Screen
*
* This  transaction  allows  the  refusal  or  reversal  of  an
* increase record.
*
*Initialise
*----------
*
* Read the  Contract  header  (function  RETRV)  and  read  the
* relevant data necessary for obtaining the status description,
* the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
* and the Bill-to-date.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*      - READR  the   life  details  using  LIFESUR  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*      - READR the  joint-life  details using LIFESUR (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
* Obtain the  necessary  descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Format Name
*
*      Read the  client  details  record  and  use the relevant
*      copybook in order to format the required names.
*
* Build the Coverage/Rider review details from INCR records.
*
*      For each INCR attached to the policy
*
*      - Check that the INCR has a valid status  for refusal by
*        reading  T5679. Invalid status  components will not be
*        displayed.
*
*      - Its TRANNO matches the Last Tranno for the INCR record.
*        ie. can only reverse one Increase period at a time.
*
*Validation
*----------
*
* If KILL was entered, then skip the remainder of the validation
* and exit from the program.
*
* Reversal (Y/N)
*
*      If this refusal is not to be registered as a true
*      refusal then a Y or N must be entered. Anything else
*      will result in an error.
*
* Cease all Future Offers
*
*      If all future increase offers are to cease then a
*      Y must be entered in this field.
*
*Updating
*--------
*
* If the KILL function key  was pressed,  skip the updating and
* exit from the program.
*
* If the  WSSP-FLAG is 'I' then this is an Enquiry. As a result
* exit from this section, and perform no updating.
*
* Call SFTLOCK to lock the contract.
*
* Read  T6661 with the  transaction code. Using  this read  the
* PTRN  file until  the last PTRN with this transaction code is
* found. This is where the reversal is to be performed back to.
* If  the  last transaction performed  is not  Pending or Actual
* a message  is  displayed  asking the user  to perform a Full
* Contract Reversal first.
*
* Perform the above Pending/Actual transaction code check by
*  using the table T6626.
*
* Call REVGENAT to reverse all necessary processing.
*
* For each selected INCR, flag the following fields:-
*
*     - INCR-REFUSAL-FLAG    -  'V' Reversal
*                              *(Note that this becomes 'R'
*                                after the AT has completed
*                                successfully).
*                            - *'F' Refusal
*                              *(Note that this becomes 'Y'
*                                after the AT has completed
*                                successfully).
*     - INCR-CEASE-IND       -  'Y' Cease all future Offers
*
* Update the selected INCR record(s).
*
*Next Program
*------------
*
* If the KILL function  key was pressed, exit from the program.
*
* Add 1 to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5047 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5047");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaLastIncrTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaSelectedInd = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaValidRev = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
	private FixedLengthStringData wsaaErrorIndicators = new FixedLengthStringData(100);
	private String wsaaValidStatus = "";
	private String wsaaMandatory = "";
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaFirstPtrnCheck = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private String e304 = "E304";
	private String f910 = "F910";
	private String g552 = "G552";
	private String g553 = "G553";
	private String g555 = "G555";
	private String g616 = "G616";
	private String f438 = "F438";
	private String g931 = "G931";
	private String h036 = "H036";
	private String t065 = "T065";
	private String g558 = "G558";
	private String g559 = "G559";
		/* TABLES */
	private String t5688 = "T5688";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t1688 = "T1688";
	private String t6658 = "T6658";
	private String t5679 = "T5679";
	private String t5654 = "T5654";
	private String t6661 = "T6661";
	private String t6626 = "T6626";
		/* FORMATS */
	private String incrrec = "INCRREC";
	private String itemrec = "ITEMREC";
	private String covrrec = "COVRREC   ";
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract Header - Surrenders*/
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Automatic Increase File*/
	private IncrTableDAM incrIO = new IncrTableDAM();
		/*Automatic Increase Refusal Select File*/
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life logical file - full surrender*/
	private LifesurTableDAM lifesurIO = new LifesurTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5654rec t5654rec = new T5654rec();
	private T5679rec t5679rec = new T5679rec();
	private T6626rec t6626rec = new T6626rec();
	private T6658rec t6658rec = new T6658rec();
	private T6661rec t6661rec = new T6661rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5047ScreenVars sv = ScreenProgram.getScreenVars( S5047ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1030, 
		exit1349, 
		nextr1555, 
		exit1669, 
		exit1679, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		checkForErrors2050, 
		exit2090, 
		updateSubfile2150, 
		exit2190, 
		readPtrn2260, 
		getNext2270, 
		exit2290, 
		next2310, 
		exit2390, 
		readPtrn3060, 
		callAt3070, 
		exit3090, 
		exit4090
	}

	public P5047() {
		super();
		screenVars = sv;
		new ScreenModel("S5047", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case continue1030: {
					continue1030();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaErrorIndicators.set(SPACES);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsaaBatckey.batcBatctrcde.toString());
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		chdrsurIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5047", sv);
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
		//ILJ-49 End 
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.cpiDate.set(varcom.vrcmMaxDate);
		sv.occdate.set(chdrsurIO.getOccdate());
		sv.chdrnum.set(chdrsurIO.getChdrnum());
		sv.cnttype.set(chdrsurIO.getCnttype());
		descIO.setDescitem(chdrsurIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.descrip.set(descIO.getLongdesc());
		}
		else {
			sv.descrip.fill("?");
		}
		sv.cownnum.set(chdrsurIO.getCownnum());
		cltsIO.setClntnum(chdrsurIO.getCownnum());
		getClientDetails1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrsurIO.getBtdate());
		sv.ptdate.set(chdrsurIO.getPtdate());
		if (isEQ(chdrsurIO.getPtdate(),chdrsurIO.getCcdate())) {
			sv.ptdateErr.set(t065);
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrsurIO.getStatcode());
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrsurIO.getPstatcode());
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		sv.reversalInd.set("N");
		lifesurIO.setDataArea(SPACES);
		lifesurIO.setChdrcoy(chdrsurIO.getChdrcoy());
		lifesurIO.setChdrnum(chdrsurIO.getChdrnum());
		lifesurIO.setLife("01");
		lifesurIO.setJlife("00");
		lifesurIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifesurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifesurIO.setFitKeysSearch("CHDRNUM");
		SmartFileCode.execute(appVars, lifesurIO);
		if (isNE(lifesurIO.getStatuz(),varcom.oK)
		&& isNE(lifesurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifesurIO.getChdrcoy())
		|| isNE(chdrsurIO.getChdrnum(),lifesurIO.getChdrnum())
		|| isNE(lifesurIO.getJlife(),"00")
		&& isNE(lifesurIO.getJlife(),SPACES)
		|| isEQ(lifesurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		lifesurIO.setJlife("01");
		lifesurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifesurIO);
		if ((isNE(lifesurIO.getStatuz(),varcom.oK))
		&& (isNE(lifesurIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isEQ(lifesurIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
		}
		sv.jlifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.jlinsnameErr.set(e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void continue1030()
	{
		readIncrs1300();
		wsaaErrorIndicators.set(sv.errorIndicators);
		/*EXIT*/
	}

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readIncrs1300()
	{
		try {
			read1310();
		}
		catch (GOTOException e){
		}
	}

protected void read1310()
	{
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(chdrsurIO.getChdrcoy());
		incrselIO.setChdrnum(chdrsurIO.getChdrnum());
		incrselIO.setLife("01");
		incrselIO.setPlanSuffix(ZERO);
		incrselIO.setTranno(9999);
		incrselIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		incrselIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrselIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)
		&& isNE(incrselIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrselIO.getParams());
			fatalError600();
		}
		if (isNE(incrselIO.getChdrcoy(),chdrsurIO.getChdrcoy())
		|| isNE(incrselIO.getChdrnum(),chdrsurIO.getChdrnum())
		|| isEQ(incrselIO.getStatuz(),varcom.endp)) {
			incrselIO.setStatuz(varcom.endp);
		}
		wsaaLastIncrTranno.set(incrselIO.getTranno());
		if (isEQ(incrselIO.getStatuz(),varcom.endp)) {
			if (isNE(wsspcomn.flag,"I")) {
				wsspcomn.flag.set("Z");
			}
			sv.subfileArea.set(SPACES);
			sv.lastSumi.set(ZERO);
			sv.lastInst.set(ZERO);
			sv.newsumi.set(ZERO);
			sv.newinst.set(ZERO);
			sv.crrcd.set(ZERO);
			sv.hrrn.set(ZERO);
			loadSubfile1600();
		}
		if (isEQ(wsspcomn.flag,"I")) {
			sv.revindOut[varcom.pr.toInt()].set("Y");
			sv.revindOut[varcom.nd.toInt()].set("Y");
			sv.ceaseindOut[varcom.pr.toInt()].set("Y");
			sv.ceaseindOut[varcom.nd.toInt()].set("Y");
		}
		readT56541700();
		if (isEQ(wsaaMandatory,"Y")) {
			sv.ceaseindOut[varcom.pr.toInt()].set("Y");
			sv.ceaseindOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(incrselIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1349);
		}
		while ( !(isEQ(incrselIO.getStatuz(),varcom.endp))) {
			findNextComponent1550();
		}
		
	}

protected void findNextComponent1550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					read1510();
				}
				case nextr1555: {
					nextr1555();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read1510()
	{
		if (isNE(wsspcomn.flag,"I")) {
			validateStatus1650();
			if (isEQ(wsaaValidStatus,"N")) {
				goTo(GotoLabel.nextr1555);
			}
		}
		sv.hrrn.set(incrselIO.getRrn());
		sv.hiddenAnnvryMethod.set(incrselIO.getAnniversaryMethod());
		sv.coverage.set(incrselIO.getCoverage());
		if (isEQ(incrselIO.getRider(),"00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(incrselIO.getRider());
		}
		if (isNE(incrselIO.getRider(),"00")
		&& isNE(incrselIO.getRider(),SPACES)) {
			sv.coverage.set(SPACES);
		}
		sv.lastSumi.set(incrselIO.getLastSum());
		sv.lastInst.set(incrselIO.getLastInst());
		sv.newsumi.set(incrselIO.getNewsum());
		sv.newinst.set(incrselIO.getNewinst());
		sv.crrcd.set(incrselIO.getCrrcd());
		sv.refusalFlag.set(incrselIO.getRefusalFlag());
		if (isEQ(incrselIO.getTranno(),wsaaLastIncrTranno)
		|| isEQ(wsspcomn.flag,"I")) {
			loadSubfile1600();
		}
	}

protected void nextr1555()
	{
		incrselIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(),varcom.oK)
		&& isNE(incrselIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(incrselIO.getParams());
			fatalError600();
		}
		if (isNE(incrselIO.getChdrcoy(),chdrsurIO.getChdrcoy())
		|| isNE(incrselIO.getChdrnum(),chdrsurIO.getChdrnum())) {
			incrselIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void loadSubfile1600()
	{
		addToSubfile1630();
	}

protected void addToSubfile1630()
	{
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		if (isNE(sv.refusalFlag,SPACES)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"Z")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5047", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(incrselIO.getStatuz(),varcom.endp)) {
			getCpiDate1800();
		}
	}

protected void validateStatus1650()
	{
		/*START*/
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
			riskStatusCheckWp1660();
		}
		if (isEQ(wsaaValidStatus,"Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
				premStatusCheckWp1670();
			}
		}
		/*EXIT*/
	}

protected void riskStatusCheckWp1660()
	{
		try {
			start1660();
		}
		catch (GOTOException e){
		}
	}

protected void start1660()
	{
		if (isEQ(incrselIO.getRider(),ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],incrselIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					goTo(GotoLabel.exit1669);
				}
			}
		}
		if (isGT(incrselIO.getRider(),ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],incrselIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					goTo(GotoLabel.exit1669);
				}
			}
		}
	}

protected void premStatusCheckWp1670()
	{
		try {
			start1670();
		}
		catch (GOTOException e){
		}
	}

protected void start1670()
	{
		if (isEQ(incrselIO.getRider(),ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],incrselIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					goTo(GotoLabel.exit1679);
				}
			}
		}
		if (isGT(incrselIO.getRider(),ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()],SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()],incrselIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					goTo(GotoLabel.exit1679);
				}
			}
		}
	}

protected void readT56541700()
	{
		read1710();
	}

protected void read1710()
	{
		wsaaMandatory = "N";
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5654);
		itdmIO.setItemitem(chdrsurIO.getCnttype());
		itdmIO.setItmfrm(chdrsurIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5654)
		|| isNE(itdmIO.getItemitem(),chdrsurIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaMandatory = "Y";
		}
		else {
			t5654rec.t5654Rec.set(itdmIO.getGenarea());
		}
	}

protected void getCpiDate1800()
	{
		start1800();
	}

protected void start1800()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(incrselIO.getChdrcoy());
		covrIO.setChdrnum(incrselIO.getChdrnum());
		covrIO.setLife(incrselIO.getLife());
		covrIO.setCoverage(incrselIO.getCoverage());
		covrIO.setRider(incrselIO.getRider());
		covrIO.setPlanSuffix(incrselIO.getPlanSuffix());
		covrIO.setFunction(varcom.readr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isLT(covrIO.getCpiDate(),sv.cpiDate)) {
			sv.cpiDate.set(covrIO.getCpiDate());
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validateScreen2010();
					validateSelectionFields2070();
				}
				case checkForErrors2050: {
					checkForErrors2050();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		sv.errorIndicators.set(wsaaErrorIndicators);
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.reversalInd,"Y")
		&& isNE(sv.reversalInd,"N")) {
			sv.revindErr.set(g616);
		}
		if (isNE(sv.ceaseInd,"Y")
		&& isNE(sv.ceaseInd,SPACES)) {
			sv.ceaseindErr.set(f438);
		}
		validTrans2200();
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSelectionFields2070()
	{
		wsaaSelectedInd.set("N");
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		if (isEQ(sv.reversalInd,"Y")
		|| isEQ(sv.reversalInd,"N")) {
			wsaaValidRev.set("N");
			validateReverse2300();
			if (isEQ(wsaaValidRev,"N")
			&& isEQ(sv.reversalInd,"Y")) {
				scrnparams.errorCode.set(g555);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.checkForErrors2050);
			}
			if (isEQ(wsaaValidRev,"N")
			&& isEQ(sv.reversalInd,"N")) {
				scrnparams.errorCode.set(g559);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.checkForErrors2050);
			}
		}
		if (isEQ(wsaaSelectedInd,"N")) {
			scrnparams.errorCode.set(g931);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2050);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2100();
				}
				case updateSubfile2150: {
					updateSubfile2150();
				}
				case exit2190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2100()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5047", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		sv.hselect.set(SPACES);
		if (isNE(sv.select,SPACES)) {
			wsaaSelectedInd.set("Y");
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setItemtabl(t6658);
		itdmIO.setItemitem(sv.hiddenAnnvryMethod);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6658)
		|| isNE(itdmIO.getItemitem(),sv.hiddenAnnvryMethod)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setItemtabl(t6658);
			itdmIO.setItmfrm(sv.occdate);
			itdmIO.setItemitem(sv.hiddenAnnvryMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h036);
			fatalError600();
		}
		t6658rec.t6658Rec.set(itdmIO.getGenarea());
		datcon2rec.freqFactor.set(t6658rec.refusalPeriod);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(sv.crrcd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		if (isLT(datcon2rec.intDate2,datcon1rec.intDate)
		&& isEQ(sv.reversalInd,"N")) {
			sv.selectErr.set(g553);
			goTo(GotoLabel.updateSubfile2150);
		}
		if (isEQ(sv.reversalInd,"N")
		&& isEQ(t6658rec.optind,SPACES)
		&& isNE(t6658rec.manopt,SPACES)) {
			sv.selectErr.set(g558);
			goTo(GotoLabel.updateSubfile2150);
		}
		if (isNE(sv.select,SPACES)) {
			sv.hselect.set("Y");
		}
	}

protected void updateSubfile2150()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5047", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void validTrans2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readPtrnrev2250();
				}
				case readPtrn2260: {
					readPtrn2260();
				}
				case getNext2270: {
					getNext2270();
				}
				case exit2290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readPtrnrev2250()
	{
		readT66262400();
		wsaaFirstPtrnCheck.set("Y");
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrsurIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrsurIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void readPtrn2260()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrsurIO.getChdrnum(),ptrnrevIO.getChdrnum()))
		|| (isNE(chdrsurIO.getChdrcoy(),ptrnrevIO.getChdrcoy()))
		|| isEQ(ptrnrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2290);
		}
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,5)); wsaaSub.add(1)){
			if (isEQ(ptrnrevIO.getBatctrcde(),t6626rec.trncd[wsaaSub.toInt()])) {
				wsaaSub.set(6);
				goTo(GotoLabel.exit2290);
			}
		}
		wsaaSub.set(ZERO);
		for (wsaaSub.set(6); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			if (isEQ(ptrnrevIO.getBatctrcde(),t6626rec.trncd[wsaaSub.toInt()])) {
				wsaaSub.set(11);
				wsaaFirstPtrnCheck.set("N");
				goTo(GotoLabel.getNext2270);
			}
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		if (isEQ(wsaaFirstPtrnCheck,"Y")) {
			if (isEQ(t6661rec.contRevFlag,SPACES)
			|| isEQ(t6661rec.contRevFlag,"Y")) {
				sv.chdrnumErr.set(g552);
				goTo(GotoLabel.exit2290);
			}
		}
	}

protected void getNext2270()
	{
		ptrnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readPtrn2260);
	}

protected void validateReverse2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2300();
				}
				case next2310: {
					next2310();
				}
				case exit2390: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2300()
	{
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5047", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.exit2390);
		}
	}

protected void next2310()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5047", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.exit2390);
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			wsaaValidRev.set("Y");
			goTo(GotoLabel.exit2390);
		}
		goTo(GotoLabel.next2310);
	}

protected void readT66262400()
	{
		start2400();
	}

protected void start2400()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6626);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6626rec.t6626Rec.set(itemIO.getGenarea());
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateDatabase3010();
					readSubfile3020();
					trsfrAt3030();
					readT66613040();
					readPtrnrev3050();
				}
				case readPtrn3060: {
					readPtrn3060();
				}
				case callAt3070: {
					callAt3070();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void readSubfile3020()
	{
		wsaaSelectedInd.set("N");
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5047", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			readSubfile3100();
		}
		
		if (isEQ(wsaaSelectedInd,"N")) {
			resetSftlock3300();
			goTo(GotoLabel.exit3090);
		}
	}

protected void trsfrAt3030()
	{
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrsurIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void readT66613040()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
	}

protected void readPtrnrev3050()
	{
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrsurIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrsurIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void readPtrn3060()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrsurIO.getChdrnum(),ptrnrevIO.getChdrnum()))
		|| (isNE(chdrsurIO.getChdrcoy(),ptrnrevIO.getChdrcoy()))
		|| isEQ(ptrnrevIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.callAt3070);
		}
		if (isNE(ptrnrevIO.getBatctrcde(),t6661rec.trcode)) {
			ptrnrevIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readPtrn3060);
		}
	}

protected void callAt3070()
	{
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(ZERO);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrsurIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTranDate.set(varcom.vrcmDate);
		wsaaTranTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaTranNum.set(ptrnrevIO.getTranno());
		wsaaTranCode.set(ptrnrevIO.getBatctrcde());
		wsaaTodate.set(ptrnrevIO.getPtrneff());
		wsaaBbldat.set(ZERO);
		if (isNE(sv.reversalInd,"Y")) {
			wsaaSupflag.set("R");
		}
		else {
			wsaaSupflag.set("N");
		}
		wsaaSuppressTo.set(ZERO);
		wsaaPlnsfx.set(ZERO);
		wsaaCfiafiTranno.set(ZERO);
		wsaaCfiafiTranCode.set(SPACES);
		atreqrec.transArea.set(wsaaTransArea);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void readSubfile3100()
	{
		/*READ-NEXT-RECORD*/
		if (isEQ(sv.hselect,"Y")) {
			wsaaSelectedInd.set("Y");
			updateIncr3200();
		}
		scrnparams.function.set(varcom.srdn);
		processScreen("S5047", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateIncr3200()
	{
		start3200();
	}

protected void start3200()
	{
		incrIO.setChdrnum(chdrsurIO.getChdrnum());
		incrIO.setChdrcoy(chdrsurIO.getChdrcoy());
		incrIO.setRrn(sv.hrrn);
		incrIO.setFunction("READD");
		incrIO.setFormat(incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incrIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.reversalInd,"Y")) {
			incrIO.setRefusalFlag("V");
		}
		else {
			incrIO.setRefusalFlag("F");
		}
		if (isEQ(sv.ceaseInd,"Y")) {
			incrIO.setCeaseInd("Y");
		}
		incrIO.setFunction("WRITD");
		incrIO.setFormat(incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incrIO.getParams());
			fatalError600();
		}
	}

protected void resetSftlock3300()
	{
		reset3305();
	}

protected void reset3305()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.function.set("UNLK");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrsurIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit4090);
		}
		wsspcomn.programPtr.add(1);
	}
}
