package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52L
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr52lScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1234);
	public FixedLengthStringData dataFields = new FixedLengthStringData(626).isAPartOf(dataArea, 0);
	public FixedLengthStringData adjamts = new FixedLengthStringData(24).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] adjamt = ZDArrayPartOfStructure(2, 12, 2, adjamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(adjamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData adjamt01 = DD.adjamt.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData adjamt02 = DD.adjamt.copyToZonedDecimal().isAPartOf(filler,12);
	public FixedLengthStringData admamts = new FixedLengthStringData(24).isAPartOf(dataFields, 24);
	public ZonedDecimalData[] admamt = ZDArrayPartOfStructure(2, 12, 2, admamts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(admamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData admamt01 = DD.admamt.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData admamt02 = DD.admamt.copyToZonedDecimal().isAPartOf(filler1,12);
	public FixedLengthStringData admfees = new FixedLengthStringData(24).isAPartOf(dataFields, 48);
	public ZonedDecimalData[] admfee = ZDArrayPartOfStructure(2, 12, 2, admfees, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(admfees, 0, FILLER_REDEFINE);
	public ZonedDecimalData admfee01 = DD.admfee.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData admfee02 = DD.admfee.copyToZonedDecimal().isAPartOf(filler2,12);
	public ZonedDecimalData amnt = DD.amnt.copyToZonedDecimal().isAPartOf(dataFields,72);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(dataFields,89);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData dtldescs = new FixedLengthStringData(270).isAPartOf(dataFields, 134);
	public FixedLengthStringData[] dtldesc = FLSArrayPartOfStructure(9, 30, dtldescs, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(270).isAPartOf(dtldescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData dtldesc01 = DD.dtldesc.copy().isAPartOf(filler3,0);
	public FixedLengthStringData dtldesc02 = DD.dtldesc.copy().isAPartOf(filler3,30);
	public FixedLengthStringData dtldesc03 = DD.dtldesc.copy().isAPartOf(filler3,60);
	public FixedLengthStringData dtldesc04 = DD.dtldesc.copy().isAPartOf(filler3,90);
	public FixedLengthStringData dtldesc05 = DD.dtldesc.copy().isAPartOf(filler3,120);
	public FixedLengthStringData dtldesc06 = DD.dtldesc.copy().isAPartOf(filler3,150);
	public FixedLengthStringData dtldesc07 = DD.dtldesc.copy().isAPartOf(filler3,180);
	public FixedLengthStringData dtldesc08 = DD.dtldesc.copy().isAPartOf(filler3,210);
	public FixedLengthStringData dtldesc09 = DD.dtldesc.copy().isAPartOf(filler3,240);
	public FixedLengthStringData erordsc = DD.erordsc.copy().isAPartOf(dataFields,404);
	public FixedLengthStringData medamts = new FixedLengthStringData(24).isAPartOf(dataFields, 428);
	public ZonedDecimalData[] medamt = ZDArrayPartOfStructure(2, 12, 2, medamts, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(medamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData medamt01 = DD.medamt.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData medamt02 = DD.medamt.copyToZonedDecimal().isAPartOf(filler4,12);
	public FixedLengthStringData medfees = new FixedLengthStringData(24).isAPartOf(dataFields, 452);
	public ZonedDecimalData[] medfee = ZDArrayPartOfStructure(2, 12, 2, medfees, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(medfees, 0, FILLER_REDEFINE);
	public ZonedDecimalData medfee01 = DD.medfee.copyToZonedDecimal().isAPartOf(filler5,0);
	public ZonedDecimalData medfee02 = DD.medfee.copyToZonedDecimal().isAPartOf(filler5,12);
	public ZonedDecimalData premi = DD.premi.copyToZonedDecimal().isAPartOf(dataFields,476);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,488);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,492);
	public ZonedDecimalData reserveUnitsDate = DD.rundte.copyToZonedDecimal().isAPartOf(dataFields,542);
	public FixedLengthStringData sacscodes = new FixedLengthStringData(4).isAPartOf(dataFields, 550);
	public FixedLengthStringData[] sacscode = FLSArrayPartOfStructure(2, 2, sacscodes, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(4).isAPartOf(sacscodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData sacscode01 = DD.sacscode.copy().isAPartOf(filler6,0);
	public FixedLengthStringData sacscode02 = DD.sacscode.copy().isAPartOf(filler6,2);
	public FixedLengthStringData sacstypws = new FixedLengthStringData(4).isAPartOf(dataFields, 554);
	public FixedLengthStringData[] sacstypw = FLSArrayPartOfStructure(2, 2, sacstypws, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(4).isAPartOf(sacstypws, 0, FILLER_REDEFINE);
	public FixedLengthStringData sacstypw01 = DD.sacstypw.copy().isAPartOf(filler7,0);
	public FixedLengthStringData sacstypw02 = DD.sacstypw.copy().isAPartOf(filler7,2);
	public ZonedDecimalData tdeduct = DD.tdeduct.copyToZonedDecimal().isAPartOf(dataFields,558);
	public FixedLengthStringData transcode = DD.transcode.copy().isAPartOf(dataFields,570);
	public FixedLengthStringData zdescs = new FixedLengthStringData(40).isAPartOf(dataFields, 574);
	public FixedLengthStringData[] zdesc = FLSArrayPartOfStructure(2, 20, zdescs, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(40).isAPartOf(zdescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData zdesc01 = DD.zdesc.copy().isAPartOf(filler8,0);
	public FixedLengthStringData zdesc02 = DD.zdesc.copy().isAPartOf(filler8,20);
	public ZonedDecimalData ztotamt = DD.ztotamt.copyToZonedDecimal().isAPartOf(dataFields,614);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(152).isAPartOf(dataArea, 626);
	public FixedLengthStringData adjamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] adjamtErr = FLSArrayPartOfStructure(2, 4, adjamtsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(8).isAPartOf(adjamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData adjamt01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData adjamt02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData admamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] admamtErr = FLSArrayPartOfStructure(2, 4, admamtsErr, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(admamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData admamt01Err = new FixedLengthStringData(4).isAPartOf(filler10, 0);
	public FixedLengthStringData admamt02Err = new FixedLengthStringData(4).isAPartOf(filler10, 4);
	public FixedLengthStringData admfeesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] admfeeErr = FLSArrayPartOfStructure(2, 4, admfeesErr, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(8).isAPartOf(admfeesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData admfee01Err = new FixedLengthStringData(4).isAPartOf(filler11, 0);
	public FixedLengthStringData admfee02Err = new FixedLengthStringData(4).isAPartOf(filler11, 4);
	public FixedLengthStringData amntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData batctrcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData dtldescsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] dtldescErr = FLSArrayPartOfStructure(9, 4, dtldescsErr, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(36).isAPartOf(dtldescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData dtldesc01Err = new FixedLengthStringData(4).isAPartOf(filler12, 0);
	public FixedLengthStringData dtldesc02Err = new FixedLengthStringData(4).isAPartOf(filler12, 4);
	public FixedLengthStringData dtldesc03Err = new FixedLengthStringData(4).isAPartOf(filler12, 8);
	public FixedLengthStringData dtldesc04Err = new FixedLengthStringData(4).isAPartOf(filler12, 12);
	public FixedLengthStringData dtldesc05Err = new FixedLengthStringData(4).isAPartOf(filler12, 16);
	public FixedLengthStringData dtldesc06Err = new FixedLengthStringData(4).isAPartOf(filler12, 20);
	public FixedLengthStringData dtldesc07Err = new FixedLengthStringData(4).isAPartOf(filler12, 24);
	public FixedLengthStringData dtldesc08Err = new FixedLengthStringData(4).isAPartOf(filler12, 28);
	public FixedLengthStringData dtldesc09Err = new FixedLengthStringData(4).isAPartOf(filler12, 32);
	public FixedLengthStringData erordscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData medamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData[] medamtErr = FLSArrayPartOfStructure(2, 4, medamtsErr, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(8).isAPartOf(medamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData medamt01Err = new FixedLengthStringData(4).isAPartOf(filler13, 0);
	public FixedLengthStringData medamt02Err = new FixedLengthStringData(4).isAPartOf(filler13, 4);
	public FixedLengthStringData medfeesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] medfeeErr = FLSArrayPartOfStructure(2, 4, medfeesErr, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(8).isAPartOf(medfeesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData medfee01Err = new FixedLengthStringData(4).isAPartOf(filler14, 0);
	public FixedLengthStringData medfee02Err = new FixedLengthStringData(4).isAPartOf(filler14, 4);
	public FixedLengthStringData premiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rundteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData sacscodesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData[] sacscodeErr = FLSArrayPartOfStructure(2, 4, sacscodesErr, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(8).isAPartOf(sacscodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sacscode01Err = new FixedLengthStringData(4).isAPartOf(filler15, 0);
	public FixedLengthStringData sacscode02Err = new FixedLengthStringData(4).isAPartOf(filler15, 4);
	public FixedLengthStringData sacstypwsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData[] sacstypwErr = FLSArrayPartOfStructure(2, 4, sacstypwsErr, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(8).isAPartOf(sacstypwsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sacstypw01Err = new FixedLengthStringData(4).isAPartOf(filler16, 0);
	public FixedLengthStringData sacstypw02Err = new FixedLengthStringData(4).isAPartOf(filler16, 4);
	public FixedLengthStringData tdeductErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData transcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData zdescsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData[] zdescErr = FLSArrayPartOfStructure(2, 4, zdescsErr, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(8).isAPartOf(zdescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zdesc01Err = new FixedLengthStringData(4).isAPartOf(filler17, 0);
	public FixedLengthStringData zdesc02Err = new FixedLengthStringData(4).isAPartOf(filler17, 4);
	public FixedLengthStringData ztotamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(456).isAPartOf(dataArea, 778);
	public FixedLengthStringData adjamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] adjamtOut = FLSArrayPartOfStructure(2, 12, adjamtsOut, 0);
	public FixedLengthStringData[][] adjamtO = FLSDArrayPartOfArrayStructure(12, 1, adjamtOut, 0);
	public FixedLengthStringData filler18 = new FixedLengthStringData(24).isAPartOf(adjamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] adjamt01Out = FLSArrayPartOfStructure(12, 1, filler18, 0);
	public FixedLengthStringData[] adjamt02Out = FLSArrayPartOfStructure(12, 1, filler18, 12);
	public FixedLengthStringData admamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] admamtOut = FLSArrayPartOfStructure(2, 12, admamtsOut, 0);
	public FixedLengthStringData[][] admamtO = FLSDArrayPartOfArrayStructure(12, 1, admamtOut, 0);
	public FixedLengthStringData filler19 = new FixedLengthStringData(24).isAPartOf(admamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] admamt01Out = FLSArrayPartOfStructure(12, 1, filler19, 0);
	public FixedLengthStringData[] admamt02Out = FLSArrayPartOfStructure(12, 1, filler19, 12);
	public FixedLengthStringData admfeesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] admfeeOut = FLSArrayPartOfStructure(2, 12, admfeesOut, 0);
	public FixedLengthStringData[][] admfeeO = FLSDArrayPartOfArrayStructure(12, 1, admfeeOut, 0);
	public FixedLengthStringData filler20 = new FixedLengthStringData(24).isAPartOf(admfeesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] admfee01Out = FLSArrayPartOfStructure(12, 1, filler20, 0);
	public FixedLengthStringData[] admfee02Out = FLSArrayPartOfStructure(12, 1, filler20, 12);
	public FixedLengthStringData[] amntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] batctrcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData dtldescsOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] dtldescOut = FLSArrayPartOfStructure(9, 12, dtldescsOut, 0);
	public FixedLengthStringData[][] dtldescO = FLSDArrayPartOfArrayStructure(12, 1, dtldescOut, 0);
	public FixedLengthStringData filler21 = new FixedLengthStringData(108).isAPartOf(dtldescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] dtldesc01Out = FLSArrayPartOfStructure(12, 1, filler21, 0);
	public FixedLengthStringData[] dtldesc02Out = FLSArrayPartOfStructure(12, 1, filler21, 12);
	public FixedLengthStringData[] dtldesc03Out = FLSArrayPartOfStructure(12, 1, filler21, 24);
	public FixedLengthStringData[] dtldesc04Out = FLSArrayPartOfStructure(12, 1, filler21, 36);
	public FixedLengthStringData[] dtldesc05Out = FLSArrayPartOfStructure(12, 1, filler21, 48);
	public FixedLengthStringData[] dtldesc06Out = FLSArrayPartOfStructure(12, 1, filler21, 60);
	public FixedLengthStringData[] dtldesc07Out = FLSArrayPartOfStructure(12, 1, filler21, 72);
	public FixedLengthStringData[] dtldesc08Out = FLSArrayPartOfStructure(12, 1, filler21, 84);
	public FixedLengthStringData[] dtldesc09Out = FLSArrayPartOfStructure(12, 1, filler21, 96);
	public FixedLengthStringData[] erordscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData medamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 252);
	public FixedLengthStringData[] medamtOut = FLSArrayPartOfStructure(2, 12, medamtsOut, 0);
	public FixedLengthStringData[][] medamtO = FLSDArrayPartOfArrayStructure(12, 1, medamtOut, 0);
	public FixedLengthStringData filler22 = new FixedLengthStringData(24).isAPartOf(medamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] medamt01Out = FLSArrayPartOfStructure(12, 1, filler22, 0);
	public FixedLengthStringData[] medamt02Out = FLSArrayPartOfStructure(12, 1, filler22, 12);
	public FixedLengthStringData medfeesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] medfeeOut = FLSArrayPartOfStructure(2, 12, medfeesOut, 0);
	public FixedLengthStringData[][] medfeeO = FLSDArrayPartOfArrayStructure(12, 1, medfeeOut, 0);
	public FixedLengthStringData filler23 = new FixedLengthStringData(24).isAPartOf(medfeesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] medfee01Out = FLSArrayPartOfStructure(12, 1, filler23, 0);
	public FixedLengthStringData[] medfee02Out = FLSArrayPartOfStructure(12, 1, filler23, 12);
	public FixedLengthStringData[] premiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rundteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData sacscodesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 348);
	public FixedLengthStringData[] sacscodeOut = FLSArrayPartOfStructure(2, 12, sacscodesOut, 0);
	public FixedLengthStringData[][] sacscodeO = FLSDArrayPartOfArrayStructure(12, 1, sacscodeOut, 0);
	public FixedLengthStringData filler24 = new FixedLengthStringData(24).isAPartOf(sacscodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sacscode01Out = FLSArrayPartOfStructure(12, 1, filler24, 0);
	public FixedLengthStringData[] sacscode02Out = FLSArrayPartOfStructure(12, 1, filler24, 12);
	public FixedLengthStringData sacstypwsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 372);
	public FixedLengthStringData[] sacstypwOut = FLSArrayPartOfStructure(2, 12, sacstypwsOut, 0);
	public FixedLengthStringData[][] sacstypwO = FLSDArrayPartOfArrayStructure(12, 1, sacstypwOut, 0);
	public FixedLengthStringData filler25 = new FixedLengthStringData(24).isAPartOf(sacstypwsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sacstypw01Out = FLSArrayPartOfStructure(12, 1, filler25, 0);
	public FixedLengthStringData[] sacstypw02Out = FLSArrayPartOfStructure(12, 1, filler25, 12);
	public FixedLengthStringData[] tdeductOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] transcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData zdescsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 420);
	public FixedLengthStringData[] zdescOut = FLSArrayPartOfStructure(2, 12, zdescsOut, 0);
	public FixedLengthStringData[][] zdescO = FLSDArrayPartOfArrayStructure(12, 1, zdescOut, 0);
	public FixedLengthStringData filler26 = new FixedLengthStringData(24).isAPartOf(zdescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zdesc01Out = FLSArrayPartOfStructure(12, 1, filler26, 0);
	public FixedLengthStringData[] zdesc02Out = FLSArrayPartOfStructure(12, 1, filler26, 12);
	public FixedLengthStringData[] ztotamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData reserveUnitsDateDisp = new FixedLengthStringData(10);

	public LongData Sr52lscreenWritten = new LongData(0);
	public LongData Sr52lprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52lScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(rundteOut,new String[] {"05","52","-05","51",null, null, null, null, null, null, null, null});
		fieldIndMap.put(amntOut,new String[] {null, null, "-07","06",null, null, null, null, null, null, null, null});
		fieldIndMap.put(billcurrOut,new String[] {null, null, "-09","08",null, null, null, null, null, null, null, null});
		fieldIndMap.put(medamt01Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(admamt01Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstypw01Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjamt01Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(medamt02Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(admamt02Out,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstypw02Out,new String[] {"46",null, "-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjamt02Out,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ztotamtOut,new String[] {"47",null, "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {transcode, chdrnum, ctypedes, batctrcde, reserveUnitsDate, erordsc, amnt, billcurr, dtldesc01, medfee01, medamt01, dtldesc02, admfee01, admamt01, dtldesc03, sacscode01, sacstypw01, zdesc01, adjamt01, dtldesc04, tdeduct, dtldesc05, premi, dtldesc06, medfee02, medamt02, dtldesc07, admfee02, admamt02, dtldesc08, sacscode02, sacstypw02, zdesc02, adjamt02, dtldesc09, ztotamt, reasoncd, resndesc};
		screenOutFields = new BaseData[][] {transcodeOut, chdrnumOut, ctypedesOut, batctrcdeOut, rundteOut, erordscOut, amntOut, billcurrOut, dtldesc01Out, medfee01Out, medamt01Out, dtldesc02Out, admfee01Out, admamt01Out, dtldesc03Out, sacscode01Out, sacstypw01Out, zdesc01Out, adjamt01Out, dtldesc04Out, tdeductOut, dtldesc05Out, premiOut, dtldesc06Out, medfee02Out, medamt02Out, dtldesc07Out, admfee02Out, admamt02Out, dtldesc08Out, sacscode02Out, sacstypw02Out, zdesc02Out, adjamt02Out, dtldesc09Out, ztotamtOut, reasoncdOut, resndescOut};
		screenErrFields = new BaseData[] {transcodeErr, chdrnumErr, ctypedesErr, batctrcdeErr, rundteErr, erordscErr, amntErr, billcurrErr, dtldesc01Err, medfee01Err, medamt01Err, dtldesc02Err, admfee01Err, admamt01Err, dtldesc03Err, sacscode01Err, sacstypw01Err, zdesc01Err, adjamt01Err, dtldesc04Err, tdeductErr, dtldesc05Err, premiErr, dtldesc06Err, medfee02Err, medamt02Err, dtldesc07Err, admfee02Err, admamt02Err, dtldesc08Err, sacscode02Err, sacstypw02Err, zdesc02Err, adjamt02Err, dtldesc09Err, ztotamtErr, reasoncdErr, resndescErr};
		screenDateFields = new BaseData[] {reserveUnitsDate};
		screenDateErrFields = new BaseData[] {rundteErr};
		screenDateDispFields = new BaseData[] {reserveUnitsDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52lscreen.class;
		protectRecord = Sr52lprotect.class;
	}

}
