package com.csc.life.contractservicing.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.contractservicing.dataaccess.dao.TpdbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Tpdbpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class TpdbpfDAOImpl extends BaseDAOImpl<Tpdbpf> implements TpdbpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(TpdbpfDAOImpl.class);
	
	@Override
	public  List<Tpdbpf> getTpdbtamt(String chdrcoy,String chdrnum) {
		
		StringBuilder sqlCovrSelect1 = new StringBuilder("SELECT TDBTAMT FROM TPDBPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = '1' ");
		PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
		 ResultSet sqlcovrpf1rs = null;
		 List<Tpdbpf> tpdblist = new ArrayList<Tpdbpf>();
		
		try {
				ps.setInt(1,Integer.parseInt(chdrcoy));
				ps.setString(2, chdrnum);
				sqlcovrpf1rs = executeQuery(ps);
				while(sqlcovrpf1rs.next()){
					Tpdbpf tpdbpf = new Tpdbpf();
					tpdbpf.setTdbtamt(sqlcovrpf1rs.getBigDecimal(1));
					tpdblist.add(tpdbpf);
				}
		}	
		 catch (SQLException e) {
			LOGGER.error("getTdbtamt()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		return tpdblist;
	}
public  List<Tpdbpf> getTpoldbt(String chdrcoy,String chdrnum) {
		
		StringBuilder sqlCovrSelect1 = new StringBuilder("SELECT TDBTAMT,CHDRNUM,CHDRCOY FROM VM1DTA.TPOLDBT WHERE CHDRNUM = ? AND CHDRCOY = ? AND VALIDFLAG = '1' ");
		PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
		 ResultSet sqlcovrpf1rs = null;
		 List<Tpdbpf> tpoldbtist = new ArrayList<Tpdbpf>();
		
		try {
				ps.setString(1,chdrcoy.trim());
				ps.setString(2, chdrnum.trim());
				sqlcovrpf1rs = executeQuery(ps);
				while(sqlcovrpf1rs.next()){
				Tpdbpf tpdbpf = new Tpdbpf();
				tpdbpf.setTdbtamt(sqlcovrpf1rs.getBigDecimal(1));
				tpdbpf.setChdrnum(sqlcovrpf1rs.getString(2));
				tpdbpf.setChdrcoy(sqlcovrpf1rs.getString(3));
				tpoldbtist.add(tpdbpf);
			}
		}	
		 catch (SQLException e) {
			LOGGER.error("getTpoldbt()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		return tpoldbtist;
	}
public  List<Tpdbpf> getTpoldbtAll(String chdrcoy,String chdrnum) {
	
	StringBuilder sqlCovrSelect1 = new StringBuilder("SELECT TDBTAMT,CHDRNUM,CHDRCOY FROM VM1DTA.TPOLDBT WHERE CHDRNUM = ? AND CHDRCOY = ? ");
	PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
	 ResultSet sqlcovrpf1rs = null;
	 List<Tpdbpf> tpoldbtist = new ArrayList<Tpdbpf>();
	
	try {
			ps.setString(1,chdrcoy.trim());
			ps.setString(2, chdrnum.trim());
			sqlcovrpf1rs = executeQuery(ps);
			while(sqlcovrpf1rs.next()){
			Tpdbpf tpdbpf = new Tpdbpf();
			tpdbpf.setTdbtamt(sqlcovrpf1rs.getBigDecimal("TDBTAMT"));
			tpdbpf.setChdrnum(sqlcovrpf1rs.getString("CHDRNUM"));
			tpdbpf.setChdrcoy(sqlcovrpf1rs.getString("CHDRCOY"));
			tpoldbtist.add(tpdbpf);
		}
	}	
	 catch (SQLException e) {
		LOGGER.error("getTpoldbtAll()", e);//IJTI-1561
		throw new SQLRuntimeException(e);
	} finally {
		close(ps, null);
	}
	return tpoldbtist;
}
}