/*
 * File: P5173at.java
 * Date: 30 August 2009 0:17:20
 * Author: Quipoz Limited
 *
 * Class transformed from P5173AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.cls.P5173cl;
import com.csc.life.contractservicing.dataaccess.BsprwfdTableDAM;
import com.csc.life.contractservicing.dataaccess.BsscwfdTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CwfdTableDAM;
import com.csc.life.contractservicing.reports.R5173Report;
import com.csc.life.contractservicing.tablestructures.T6760rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.BprdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsctTableDAM;
import com.csc.smart.dataaccess.BsnrTableDAM;
import com.csc.smart.dataaccess.BspdTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getstamp;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.batch.cls.Sbmthread;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
*
* P5173AT - Windforward AT Module
* -------------------------------
*
* This program performs the processing  to submit the necessary
* batch schedules for Windforward. The batch programs come from
* the  Windforward Transactions Table, T6757.  The  program  is
* called  from  the  Windforward  AT Submission module which is
* performed following Windforward Confirmation.
*
*    Program Details
*    ---------------
*
* This program reads the Contract  Header record and then looks
* for Contract Windforward records for that contract.
*
* Processing Loop
* ---------------
*  For each Contract Windforward (CWFD) record for the contract:
*
*     Read the Windforward Batch Parameters  table,  using  the
*     program name to get the job  queue  and  parameter prompt
*     program and company.
*
*     Read and hold the Schedule Definition record (BSCD) to get
*     the details for the schedule.
*
*     Add 1 to the last used schedule number.
*
*     Rewrite the Schedule Definition record.
*
*     Write a new Submitted Schedule record (BSSC) using the
*     Schedule Definition details.
*
*     Write a User Parameter Area record (BUPA) for the schedule.
*
*     Loop through the Schedule Process Definition file (BSPDPF)
*     to get all of the initial processes for the schedule.
*
*     For each record found:
*
*         Read the Batch Process Definition file (BPRDPF).
*
*         Write a Batch Process record (BSPR) for each initial
*         process.  (There should only be one occurrence for
*         each.)
*
*     Submit the first Thread by calling SBMTHREAD, holding the
*     schedules on the queue.
*
*     Obtain the schedule run details for the schedule just
*     submitted by calling a CL program to retrieve the 'Job
*     Submitted' message containing the schedule run details.
*
*     Update the Contract Windforward record with the schedule
*     details.
*
*  Once all of the batches have been successfully submitted, do
*  the following processing:
*
*     Write a new contract header record, setting the old record
*     to validflag 2.
*
*     Write a PTRN record for the transaction
*
*     Remove the softlock which was applied to the contract.
*
*     Loop through all of the Contract Windforward records for
*     the contract, using the schedule run details to release the
*     held schedules on the queue.
*
*         Use the schedule run details on the record to release
*         held schedules submitted earlier.
*
*         Write a line out to the Windforward Report indicating
*         which schedule has been submitted for each Contract
*         Windforward record.
*
*         Delete each Contract Windforward record.
*
***********************************************************************
* </pre>
*/
public class P5173at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5173Report printerFile = new R5173Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5173AT");
	private String wsaaOverflow = "Y";

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(201);
	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaAcctmonth = new PackedDecimalData(2, 0).isAPartOf(wsaaTransArea, 22);
	private PackedDecimalData wsaaAcctyear = new PackedDecimalData(4, 0).isAPartOf(wsaaTransArea, 24);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaTransArea, 27);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 29).setUnsigned();
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 37);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 41);
	private FixedLengthStringData filler1 = new FixedLengthStringData(156).isAPartOf(wsaaTransArea, 45, FILLER).init(SPACES);

	private FixedLengthStringData r5173h01Record = new FixedLengthStringData(49);
	private FixedLengthStringData r5173h01O = new FixedLengthStringData(49).isAPartOf(r5173h01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5173h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5173h01O, 1);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5173h01O, 31);
	private FixedLengthStringData occdate = new FixedLengthStringData(10).isAPartOf(r5173h01O, 39);

	private FixedLengthStringData r5173d01Record = new FixedLengthStringData(75);
	private FixedLengthStringData r5173d01O = new FixedLengthStringData(75).isAPartOf(r5173d01Record, 0);
	private FixedLengthStringData bschednam = new FixedLengthStringData(10).isAPartOf(r5173d01O, 0);
	private FixedLengthStringData desc = new FixedLengthStringData(50).isAPartOf(r5173d01O, 10);
	private ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(r5173d01O, 60);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(r5173d01O, 65);
	private FixedLengthStringData wsaaJobq = new FixedLengthStringData(10);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaCnt = new PackedDecimalData(3, 0);
	private static final String wsaaTimestampDft = "1900-01-01-00.00.00.000000";
	private FixedLengthStringData wsaaUsrprf = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaJobnm = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaDatime = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaJobqIndicator = new FixedLengthStringData(1);
	private PackedDecimalData wsaaScheduleThreadNo = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaHold = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaRestart = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrPstatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaClParameters = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaStatus = new FixedLengthStringData(4).isAPartOf(wsaaClParameters, 0);
	private ZonedDecimalData wsaaJobnum = new ZonedDecimalData(6, 0).isAPartOf(wsaaClParameters, 4);
	private FixedLengthStringData filler2 = new FixedLengthStringData(6).isAPartOf(wsaaJobnum, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaJobnumx = new FixedLengthStringData(6).isAPartOf(filler2, 0);
	private FixedLengthStringData wsaaJobuser = new FixedLengthStringData(10).isAPartOf(wsaaClParameters, 10);
	private FixedLengthStringData wsaaJobname = new FixedLengthStringData(10).isAPartOf(wsaaClParameters, 20);
		/* TABLES */
	private static final String t1659 = "T1659";
	private static final String t1693 = "T1693";
	private static final String t6760 = "T6760";
	private BprdTableDAM bprdIO = new BprdTableDAM();
	private BscdTableDAM bscdIO = new BscdTableDAM();
	private BsctTableDAM bsctIO = new BsctTableDAM();
	private BsnrTableDAM bsnrIO = new BsnrTableDAM();
	private BspdTableDAM bspdIO = new BspdTableDAM();
	private BsprwfdTableDAM bsprwfdIO = new BsprwfdTableDAM();
	private BsscwfdTableDAM bsscwfdIO = new BsscwfdTableDAM();
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CwfdTableDAM cwfdIO = new CwfdTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private P6671par p6671par = new P6671par();
	private T1693rec t1693rec = new T1693rec();
	private T6760rec t6760rec = new T6760rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Atmodrec atmodrec = new Atmodrec();
	private FormatsInner formatsInner = new FormatsInner();
	
	//ILIFE-960
	private ZonedDecimalData wsaaProcessOccNum = new ZonedDecimalData(3, 0).setUnsigned();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		detailLine3240,
		errorProg610
	}

	public P5173at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialize1000();
		processingLoop2000();
		finishProcessing3000();
		printerFile.close();
		exitProgram();
	}

protected void initialize1000()
	{
		getChdr1010();
	}

protected void getChdr1010()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaJobq.set(SPACES);
		printerFile.openOutput();
		/* Retrieve the contract header.*/
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrmjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), atmodrec.company)
		|| isNE(chdrmjaIO.getChdrnum(), wsaaPrimaryChdrnum)) {
			chdrmjaIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void processingLoop2000()
	{
		process2010();
	}

protected void process2010()
	{
		/* Read T6760 to get the job queue, parameter prompt program*/
		/* and company.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(t6760);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6760rec.t6760Rec.set(itemIO.getGenarea());
		/* Get the full name of the job queue from the short*/
		/* description on T1659.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1659);
		descIO.setDescitem(t6760rec.jobq);
		descIO.setLanguage(atmodrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaJobq.set(descIO.getShortdesc());
		/* Look for the first Contract Windforward record for this*/
		/* contract.*/
		cwfdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		cwfdIO.setChdrnum(chdrmjaIO.getChdrnum());
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setFormat(formatsInner.cwfdrec);
		cwfdIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		/* If the Contract Windforward record is not for the correct*/
		/* contract, abend.*/
		if (isNE(cwfdIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(cwfdIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			cwfdIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, cwfdIO);
			if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(cwfdIO.getStatuz());
				syserrrec.params.set(cwfdIO.getParams());
				fatalError600();
			}
			cwfdIO.setFunction(varcom.begnh);
			cwfdIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		/* Loop through all of the Contract Windforward Records for*/
		/* this contract, submitting schedules.*/
		while ( !(isEQ(cwfdIO.getStatuz(), varcom.endp))) {
			submitSchedule2100();
			update2300();
			nextCwfd2400();
		}

	}

protected void submitSchedule2100()
	{
		readh2110();
	}

protected void readh2110()
	{
		/* Read and hold the Schedule Definition record (BSCD) to get*/
		/* the schedule details for subsequent use and updating.*/
		//ILIFE-960
		bscdIO.setParams(SPACES);
		bscdIO.setScheduleName(cwfdIO.getScheduleName());
		/* MOVE READH                  TO BSCD-FUNCTION.                */
		bscdIO.setFunction(varcom.readr);
		bscdIO.setFormat(formatsInner.bscdrec);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(bscdIO.getStatuz());
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		/* MOVE CWFD-SCHEDULE-NAME     TO BSNR-SCHEDULE-NAME.   <LA4006>*/
		/* MOVE READH                  TO BSNR-FUNCTION.        <LA4006>*/
		/* MOVE BSNRREC                TO BSNR-FORMAT.          <LA4006>*/
		/* CALL 'BSNRIO'               USING BSNR-PARAMS.       <LA4006>*/
		/* IF BSNR-STATUZ           NOT = O-K                   <LA4006>*/
		/*     MOVE BSNR-STATUZ        TO SYSR-STATUZ           <LA4006>*/
		/*     MOVE BSNR-PARAMS        TO SYSR-PARAMS           <LA4006>*/
		/*     PERFORM 600-FATAL-ERROR                          <LA4006>*/
		/* END-IF.                                              <LA4006>*/
		/* Increment the last used schedule number.*/
		/* ADD 1                       TO BSCD-LAST-USED-SCHED-NO.      */
		/* ADD 1                       TO BSNR-LAST-USED-SCHED-NO.      */
		/* MOVE REWRT                  TO BSCD-FUNCTION.                */
		/* MOVE BSCDREC                TO BSCD-FORMAT.                  */
		/* CALL 'BSCDIO'               USING BSCD-PARAMS.               */
		/* IF BSCD-STATUZ           NOT = O-K                           */
		/*     MOVE BSCD-STATUZ        TO SYSR-STATUZ                   */
		/*     MOVE BSCD-PARAMS        TO SYSR-PARAMS                   */
		/*     PERFORM 600-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		//ILIFE-960
		bsnrIO.setParams(SPACES);
		bsnrIO.setScheduleName(cwfdIO.getScheduleName());
		bsnrIO.setFunction(varcom.readh);
		bsnrIO.setFormat(formatsInner.bsnrrec);
		SmartFileCode.execute(appVars, bsnrIO);
		if (isNE(bsnrIO.getStatuz(), varcom.oK)
		&& isNE(bsnrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(bsnrIO.getStatuz());
			syserrrec.params.set(bsnrIO.getParams());
			fatalError600();
		}
		if (isEQ(bsnrIO.getStatuz(), varcom.oK)) {
			setPrecision(bsnrIO.getLastUsedSchedNo(), 0);
			bsnrIO.setLastUsedSchedNo(add(bsnrIO.getLastUsedSchedNo(), 1));
			bsnrIO.setFunction(varcom.rewrt);
		}
		else {
			bsnrIO.setLastUsedSchedNo(1);
			bsnrIO.setFunction(varcom.writr);
		}
		bsnrIO.setFormat(formatsInner.bsnrrec);
		SmartFileCode.execute(appVars, bsnrIO);
		if (isNE(bsnrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(bsnrIO.getStatuz());
			syserrrec.params.set(bsnrIO.getParams());
			fatalError600();
		}
		/* Write the Schedule Record.*/
		//ILIFE-960
		bsscwfdIO.setParams(SPACES);
		bsscwfdIO.setScheduleName(bscdIO.getScheduleName());
		/* MOVE BSCD-LAST-USED-SCHED-NO TO BSSCWFD-SCHEDULE-NUMBER.     */
		bsscwfdIO.setScheduleNumber(bsnrIO.getLastUsedSchedNo());
		bsscwfdIO.setEffectiveDate(cwfdIO.getEfdate());
		bsscwfdIO.setReqdNofThreads(bscdIO.getNofThreads());
		bsscwfdIO.setAcctYear(wsaaAcctyear);
		bsscwfdIO.setAcctMonth(wsaaAcctmonth);
		bsscwfdIO.setLanguage(atmodrec.language);
		bsscwfdIO.setInitBranch(wsaaBranch);
		bsscwfdIO.setScheduleStatus("50");
		bsscwfdIO.setCurrNofThreads(1);
		callProgram(Getstamp.class, wsaaUsrprf, wsaaJobnm, wsaaDatime);
		bsscwfdIO.setUserName(wsaaUsrprf);
		bsscwfdIO.setDatimeInit(wsaaDatime);
		varcom.vrcmTranid.set(wsaaTranid);
		bsscwfdIO.getDatimeInit().setSub1String(21, 6, varcom.vrcmUser);
		bsscwfdIO.setDatimeStarted(wsaaTimestampDft);
		bsscwfdIO.setDatimeEnded(wsaaTimestampDft);
		bsscwfdIO.setProcessesAborted(ZERO);
		bsscwfdIO.setProcessesFailed(ZERO);
		bsscwfdIO.setProcessesCancelled(ZERO);
		bsscwfdIO.setProcessesCompleted(ZERO);
		bsscwfdIO.setFormat(formatsInner.bsscwfdrec);
		bsscwfdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bsscwfdIO);
		if (isNE(bsscwfdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(bsscwfdIO.getStatuz());
			syserrrec.params.set(bsscwfdIO.getParams());
			fatalError600();
		}
		/* Create a Contract Range User Parameter record for the*/
		/* schedule.*/
		//ILIFE-960
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(bsscwfdIO.getScheduleName());
		/* MOVE BSCD-LAST-USED-SCHED-NO TO BUPA-SCHEDULE-NUMBER.        */
		bupaIO.setScheduleNumber(bsnrIO.getLastUsedSchedNo());
		bupaIO.setCompany(t6760rec.batccoy);
		bupaIO.setParmPromptProg(t6760rec.prgm);
		bupaIO.setAcctYear(bsscwfdIO.getAcctYear());
		bupaIO.setAcctMonth(bsscwfdIO.getAcctMonth());
		bupaIO.setEffectiveDate(bsscwfdIO.getEffectiveDate());
		bupaIO.setBranch(bsscwfdIO.getInitBranch());
		/* Set up the contract range and move this into the User*/
		/* Parameter record.*/
		p6671par.chdrnum.set(chdrmjaIO.getChdrnum());
		p6671par.chdrnum1.set(chdrmjaIO.getChdrnum());
		bupaIO.setParmarea(p6671par.parmRecord);
		bupaIO.setUserProfile(bsscwfdIO.getUserProfile());
		bupaIO.setJobName(bsscwfdIO.getJobName());
		bupaIO.setDatime(bsscwfdIO.getDatime());
		bupaIO.setFormat(formatsInner.buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(bupaIO.getStatuz());
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
		/* Read through the Schedule Process Definition file to obtain*/
		/* all of the Initial Processes.*/
		//ILIFE-960
		bspdIO.setParams(SPACES);
		bspdIO.setScheduleName(bsscwfdIO.getScheduleName());
		bspdIO.setCompany(SPACES);
		bspdIO.setProcessName(SPACES);
		bspdIO.setFunction(varcom.begn);
		bspdIO.setFormat(formatsInner.bspdrec);
		SmartFileCode.execute(appVars, bspdIO);
		if (isNE(bspdIO.getStatuz(), varcom.oK)
		&& isNE(bspdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(bspdIO.getParams());
			fatalError600();
		}
		if (isNE(bspdIO.getScheduleName(), bsscwfdIO.getScheduleName())) {
			bspdIO.setStatuz(varcom.endp);
		}
		/* Write all of the Initial Processes for the Schedule.*/
		while ( !(isNE(bspdIO.getStatuz(), varcom.oK))) {
			writeInitialProcesses2200();
		}
		
		//ILIFE-960
		// Before submitting job i.e. L2WFBILLNG, it is required to perform commit to update BSSCPF and BSPR records
		// otherwise Bmanmthrdb won't pick this batch
		appVars.commit();

		/*  Submit the First Thread.*/
		wsaaJobqIndicator.set("P");
		wsaaScheduleThreadNo.set(1);
		//ILIFE-960 START
		if (isEQ(bscdIO.getDebugMode(), "Y")) {
			wsaaHold.set("*YES");
		}
		else {
			wsaaHold.set("*NO");
		}
		wsaaRestart.set(SPACES);
		wsaaUser.set(bsscwfdIO.getUserProfile());
		wsaaJobq.set(bscdIO.getJobq());
		//ILIFE-960 END
		callProgram(Sbmthread.class, bscdIO.getScheduleName(), bsscwfdIO.getScheduleNumber(), wsaaJobqIndicator, wsaaScheduleThreadNo, wsaaJobq, bscdIO.getJobqPriority(), bscdIO.getJobDescription(), wsaaHold, wsaaUser, wsaaRestart);
	}

protected void writeInitialProcesses2200()
	{
		readr2210();
	}

protected void readr2210()
	{
		/* Read the Process Definition record to obtain Process Details*/
		bprdIO.setFunction(varcom.readr);
		bprdIO.setCompany(bspdIO.getCompany());
		bprdIO.setProcessName(bspdIO.getProcessName());
		SmartFileCode.execute(appVars, bprdIO);
		if (isNE(bprdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(bprdIO.getStatuz());
			syserrrec.params.set(bprdIO.getParams());
			fatalError600();
		}
		/*  Get the FSU company value by reading T1693*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(bspdIO.getCompany());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		/*  Write a Process Record for each Initial Processes.*/
		bsprwfdIO.setScheduleName(bsscwfdIO.getScheduleName());
		bsprwfdIO.setScheduleNumber(bsscwfdIO.getScheduleNumber());
		bsprwfdIO.setCompany(bprdIO.getCompany());
		bsprwfdIO.setProcessName(bprdIO.getProcessName());
		bsprwfdIO.setProcessStatus("50");
		bsprwfdIO.setScheduleThreadNo(ZERO);
		bsprwfdIO.setSchedulingPriority(bprdIO.getSchedulingPriority());
		bsprwfdIO.setDatimeStarted(wsaaTimestampDft);
		bsprwfdIO.setDatimeLogged(wsaaTimestampDft);
		bsprwfdIO.setTotalElapsedTime(ZERO);
		bsprwfdIO.setCycleCount(ZERO);
		bsprwfdIO.setNotProcessedCount(ZERO);
		bsprwfdIO.setErrorCount(ZERO);
		bsprwfdIO.setCmtrestart(ZERO);
		bsprwfdIO.setFsuco(t1693rec.fsuco);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt, 50)); wsaaCnt.add(1)){
			bsprwfdIO.setBcontot(wsaaCnt, ZERO);
		}
		bsprwfdIO.setFunction(varcom.writr);
		bsprwfdIO.setFormat(formatsInner.bsprwfdrec);
		/*  Write a process record for each occurrence required.*/
		//ILIFE-960 START
		wsaaProcessOccNum.set(ZERO);
		for (wsaaProcessOccNum.set(1); !(isGT(wsaaProcessOccNum, bprdIO.getThreadsThisProcess())); wsaaProcessOccNum.add(1)){
			setPrecision(bsprwfdIO.getProcessOccNum(), 0);
			bsprwfdIO.setProcessOccNum(wsaaProcessOccNum);
		//ILIFE-960 END
			SmartFileCode.execute(appVars, bsprwfdIO);
			if (isNE(bsprwfdIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(bsprwfdIO.getStatuz());
				syserrrec.params.set(bsprwfdIO.getParams());
				fatalError600();
			}
		}
		/*  Read through the Schedule Process Definition file for the*/
		/*  next Initial process.*/
		bspdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, bspdIO);
		if (isNE(bspdIO.getStatuz(), varcom.oK)
		&& isNE(bspdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(bspdIO.getStatuz());
			syserrrec.params.set(bspdIO.getParams());
			fatalError600();
		}
		if (isNE(bspdIO.getScheduleName(), bsscwfdIO.getScheduleName())) {
			bspdIO.setStatuz(varcom.endp);
		}
	}

protected void update2300()
	{
		update2310();
	}

protected void update2310()
	{
		/* Call a CL program to retrieve the 'Job Submitted' message in*/
		/* order to obtain the schedule run details for addition to the*/
		/* Contract Windforward Record.*/
		wsaaClParameters.set(SPACES);
		callProgram(P5173cl.class, wsaaStatus, wsaaJobnumx, wsaaJobuser, wsaaJobname);
		if (isNE(wsaaStatus, varcom.oK)) {
			syserrrec.subrname.set(wsaaProg);
			syserrrec.params.set(wsaaClParameters);
			fatalError600();
		}
		/* Update the Contract Windforward Record which has just been*/
		/* processed with the schedule details.*/
		
		//ILIFE-960 START
		/* Using correct parameters to pass to CWFD */
		cwfdIO.setJobnameUser(wsaaUser);
		cwfdIO.setJobnameJob(bscdIO.getScheduleName());
		cwfdIO.setJobnameNumber(bsscwfdIO.getScheduleNumber());
		//ILIFE-960 END
		cwfdIO.setFormat(formatsInner.cwfdrec);
		cwfdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
	}

protected void nextCwfd2400()
	{
		begnh2410();
	}

protected void begnh2410()
	{
		/* Look for the next Contract Windforward record for this*/
		/* contract.*/
		cwfdIO.setFormat(formatsInner.cwfdrec);
		cwfdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)
		&& isNE(cwfdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		/* If the Contract Windforward record is not for the correct*/
		/* contract, move ENDP to the status to end the loop.*/
		if (isNE(cwfdIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(cwfdIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			cwfdIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, cwfdIO);
			if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(cwfdIO.getStatuz());
				syserrrec.params.set(cwfdIO.getParams());
				fatalError600();
			}
			cwfdIO.setStatuz(varcom.endp);
		}
	}

protected void finishProcessing3000()
	{
		finish3010();
	}

protected void finish3010()
	{
		/* Write a PTRN for the transaction.*/
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		setPrecision(ptrnIO.getTranno(), 0);
		ptrnIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError600();
		}
		/* Read the validflag '2' contract header to get the previous*/
		/* status codes.*/
		chdrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getChdrcoy(), atmodrec.company)
		|| isNE(chdrmjaIO.getChdrnum(), wsaaPrimaryChdrnum)) {
			chdrmjaIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		wsaaChdrStatcode.set(chdrmjaIO.getStatcode());
		wsaaChdrPstatcode.set(chdrmjaIO.getPstatcode());
		/* Read and hold the contract header record for updating.*/
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setStatcode(wsaaChdrStatcode);
		chdrmjaIO.setPstatcode(wsaaChdrPstatcode);
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/* Remove the softlock from the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/* Release the schedules submitted earlier as held, using the*/
		/* schedule details from the CWFD records.  Once the schedule*/
		/* has been released, delete the CWFD record.*/
		cwfdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		cwfdIO.setChdrnum(chdrmjaIO.getChdrnum());
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setFormat(formatsInner.cwfdrec);
		cwfdIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		/* If the Contract Windforward record is not for the correct*/
		/* contract, abend.*/
		if (isNE(cwfdIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(cwfdIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			cwfdIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, cwfdIO);
			if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(cwfdIO.getStatuz());
				syserrrec.params.set(cwfdIO.getParams());
				fatalError600();
			}
			cwfdIO.setFunction(varcom.begnh);
			cwfdIO.setStatuz(varcom.endp);
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		/* Loop through all of the Contract Windforward Records for*/
		/* this contract, releasing the submitted schedules.*/
		while ( !(isEQ(cwfdIO.getStatuz(), varcom.endp))) {
			//ILIFE-960
			/* We don't have RLSJOB command implemented in Integral Java and this command is used to release job from
			 * subsystem in iSeries. This AT job internally submits a job i.e. L2WFBILLNG for windforward billing. So, purpose
			 * in iSeries could be to release that job from subsystem.
			 * This job is submitted by calling Sbmthread i.e. the same way we submit batches online. Here also we 
			 * don't use RLSJOB command.
			 * Hence, I have commented below code line.*/
//			releaseSchedule3100();
			printReport3200();
			delete3300();
			nextCwfd3400();
		}

	}

protected void releaseSchedule3100()
	{
		/*RELEASE*/
		wsaaJobnum.set(cwfdIO.getJobnameNumber());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("RLSJOB JOB(");
		stringVariable1.addExpression(wsaaJobnumx);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(cwfdIO.getJobnameUser(), SPACES);
		stringVariable1.addExpression("/", SPACES);
		stringVariable1.addExpression(cwfdIO.getJobnameJob());
		stringVariable1.addExpression(")");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void printReport3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3210();
				case detailLine3240:
					detailLine3240();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3210()
	{
		printerRec.set(SPACES);
		/* First time through print headings,*/
		/* subsequently at EOP.*/
		if (isNE(wsaaOverflow, "Y")) {
			goTo(GotoLabel.detailLine3240);
		}
		/* Move details to heading line*/
		company.set(cwfdIO.getChdrcoy());
		chdrnum.set(cwfdIO.getChdrnum());
		/* Get the Company Description from T1693*/
		descIO.setDataArea(SPACES);
		descIO.setDescitem(cwfdIO.getChdrcoy());
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setLanguage(atmodrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		companynm.set(descIO.getLongdesc());
		/* Convert Contract Commencement Date*/
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(chdrmjaIO.getOccdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		occdate.set(datcon1rec.extDate);
		wsaaOverflow = "N";
		printerFile.printR5173h01(r5173h01Record);
	}

protected void detailLine3240()
	{
		printerRec.set(SPACES);
		/* Convert CWFD Effective Date*/
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(cwfdIO.getEfdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		effdate.set(datcon1rec.extDate);
		/* Get the Batch Schedule Description*/
		/* MOVE CWFD-SCHEDULE-NAME     TO BSCD-SCHEDULE-NAME.           */
		/* MOVE READR                  TO BSCD-FUNCTION.                */
		/* MOVE BSCDREC                TO BSCD-FORMAT.                  */
		/* CALL 'BSCDIO'               USING BSCD-PARAMS.               */
		/* IF BSCD-STATUZ          NOT = O-K AND                        */
		/*                         NOT = MRNF                           */
		/*     MOVE BSCD-PARAMS        TO SYSR-PARAMS                   */
		/*     PERFORM 600-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		/* MOVE BSCD-DESC              TO DESC OF R5173D01-O.           */
		bsctIO.setScheduleName(cwfdIO.getScheduleName());
		bsctIO.setLanguage(atmodrec.language);
		bsctIO.setFunction(varcom.readr);
		bsctIO.setFormat(formatsInner.bsctrec);
		SmartFileCode.execute(appVars, bsctIO);
		if (isNE(bsctIO.getStatuz(), varcom.oK)
		&& isNE(bsctIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(bsctIO.getParams());
			fatalError600();
		}
		desc.set(bsctIO.getDesc());
		tranno.set(cwfdIO.getTranno());
		bschednam.set(cwfdIO.getScheduleName());
		wsaaOverflow = "N";
		printerFile.printR5173d01(r5173d01Record);
	}

protected void delete3300()
	{
		/*DELET*/
		/* Delete the Contract Windforward record.*/
		cwfdIO.setFormat(formatsInner.cwfdrec);
		cwfdIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void nextCwfd3400()
	{
		begnh3410();
	}

protected void begnh3410()
	{
		/* Look for the next Contract Windforward record for this*/
		/* contract.  It is possible to use a BEGNH as the last record*/
		/* processed was deleted.*/
		cwfdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		cwfdIO.setChdrnum(chdrmjaIO.getChdrnum());
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setFormat(formatsInner.cwfdrec);
		cwfdIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)
		&& isNE(cwfdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(cwfdIO.getStatuz());
			syserrrec.params.set(cwfdIO.getParams());
			fatalError600();
		}
		/* If the Contract Windforward record is not for the correct*/
		/* contract, move ENDP to the status to end the loop.*/
		if (isNE(cwfdIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(cwfdIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			cwfdIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, cwfdIO);
			if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(cwfdIO.getStatuz());
				syserrrec.params.set(cwfdIO.getParams());
				fatalError600();
			}
			cwfdIO.setStatuz(varcom.endp);
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					fatal601();
				case errorProg610:
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatal601()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		atmodrec.statuz.set(varcom.bomb);
		/* Delete the submitted schedules prior to exiting the program.*/
		deleteSchedulesOnError700();
		/*EXIT*/
		exitProgram();
	}

protected void stop699()
	{
		stopRun();
	}

protected void deleteSchedulesOnError700()
	{
		begn710();
	}

protected void begn710()
	{
		/* Delete the schedules submitted earlier as held, using the*/
		/* schedule details from the CWFD records.*/
		cwfdIO.setChdrcoy(atmodrec.company);
		cwfdIO.setChdrnum(wsaaPrimaryChdrnum);
		cwfdIO.setPrcSeqNbr(ZERO);
		cwfdIO.setFormat(formatsInner.cwfdrec);
		cwfdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		cwfdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		cwfdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/* If the Contract Windforward record is not for the correct*/
		/* contract, leave the section.*/
		if (isNE(cwfdIO.getChdrcoy(), atmodrec.company)
		|| isNE(cwfdIO.getChdrnum(), wsaaPrimaryChdrnum)) {
			return ;
		}
		/* Loop through all of the Contract Windforward Records for*/
		/* this contract which have had schedules submitted for them,*/
		/* deleting the submitted schedules.*/
		while ( !(isEQ(cwfdIO.getStatuz(), varcom.endp)
		|| isEQ(cwfdIO.getJobnameJob(), SPACES))) {
			deleteSchedule720();
			nextCwfd740();
		}

	}

protected void deleteSchedule720()
	{
		/*DELETE*/
		wsaaJobnum.set(cwfdIO.getJobnameNumber());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("ENDJOB JOB(");
		stringVariable1.addExpression(wsaaJobnumx);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(cwfdIO.getJobnameUser(), SPACES);
		stringVariable1.addExpression("/", SPACES);
		stringVariable1.addExpression(cwfdIO.getJobnameJob());
		stringVariable1.addExpression(")");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void nextCwfd740()
	{
		/*NEXTR*/
		/* Look for the next Contract Windforward record for this*/
		/* contract.*/
		cwfdIO.setFormat(formatsInner.cwfdrec);
		cwfdIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, cwfdIO);
		if (isNE(cwfdIO.getStatuz(), varcom.oK)
		&& isNE(cwfdIO.getStatuz(), varcom.endp)) {
			cwfdIO.setStatuz(varcom.endp);
		}
		/* If the Contract Windforward record is not for the correct*/
		/* contract, move ENDP to the status to end the loop.*/
		if (isNE(cwfdIO.getChdrcoy(), atmodrec.company)
		|| isNE(cwfdIO.getChdrnum(), wsaaPrimaryChdrnum)) {
			cwfdIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData cwfdrec = new FixedLengthStringData(10).init("CWFDREC");
	private FixedLengthStringData bscdrec = new FixedLengthStringData(10).init("BSCDREC");
	private FixedLengthStringData bsctrec = new FixedLengthStringData(10).init("BSCTREC");
	private FixedLengthStringData buparec = new FixedLengthStringData(10).init("BUPAREC");
	private FixedLengthStringData bsscwfdrec = new FixedLengthStringData(10).init("BSSCWFDREC");
	private FixedLengthStringData bspdrec = new FixedLengthStringData(10).init("BSPDREC");
	private FixedLengthStringData bsprwfdrec = new FixedLengthStringData(10).init("BSPRWFDREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData bsnrrec = new FixedLengthStringData(10).init("BSNRREC");
}
}
