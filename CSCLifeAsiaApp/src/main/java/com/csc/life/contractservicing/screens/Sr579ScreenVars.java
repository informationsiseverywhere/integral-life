package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR579
 * @version 1.0 generated on 30/08/09 07:20
 * @author Quipoz
 */
public class Sr579ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(890);
	public FixedLengthStringData dataFields = new FixedLengthStringData(330).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,44);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,47);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,110);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,122);
	public ZonedDecimalData matage = DD.matage.copyToZonedDecimal().isAPartOf(dataFields,169);
	public ZonedDecimalData mattcess = DD.mattcess.copyToZonedDecimal().isAPartOf(dataFields,172);
	public ZonedDecimalData mattrm = DD.mattrm.copyToZonedDecimal().isAPartOf(dataFields,180);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,183);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,184);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,185);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,188);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,191);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,195);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,199);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,207);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,209);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,226);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,227);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,229);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,233);
	public ZonedDecimalData susamt = DD.susamt.copyToZonedDecimal().isAPartOf(dataFields,248);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,265);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,282);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,283);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,296);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,313);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(140).isAPartOf(dataArea, 330);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData matageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mattcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mattrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData susamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(420).isAPartOf(dataArea, 470);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] matageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mattcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mattrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] susamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData mattcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);

	public LongData Sr579screenWritten = new LongData(0);
	public LongData Sr579protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr579ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminOut,new String[] {"03","01","-03","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"28","29","-28","29",null, null, null, null, null, null, null, null});
		fieldIndMap.put(matageOut,new String[] {"14","13","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mattrmOut,new String[] {"16","15","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mattcessOut,new String[] {"17","18","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"07","06","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"09","08","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premcessOut,new String[] {"10","11","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"24","23","-24","43",null, null, null, null, null, null, null, null});
		fieldIndMap.put(singprmOut,new String[] {"26","25","-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"34","36","-34","35",null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"41","44","-41","44",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"32","31","-32","31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"47","46","-47","46",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifenum, linsname, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, polinc, planSuffix, sumin, effdate, currcd, susamt, matage, mattrm, mattcess, premCessAge, premCessTerm, premcess, liencd, singlePremium, mortcls, comind, optextind, zagelit, zlinstprem, taxamt, taxind};
		screenOutFields = new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifenumOut, linsnameOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, polincOut, plnsfxOut, suminOut, effdateOut, currcdOut, susamtOut, matageOut, mattrmOut, mattcessOut, pcessageOut, pcesstrmOut, premcessOut, liencdOut, singprmOut, mortclsOut, comindOut, optextindOut, zagelitOut, zlinstpremOut, taxamtOut, taxindOut};
		screenErrFields = new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifenumErr, linsnameErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, polincErr, plnsfxErr, suminErr, effdateErr, currcdErr, susamtErr, matageErr, mattrmErr, mattcessErr, pcessageErr, pcesstrmErr, premcessErr, liencdErr, singprmErr, mortclsErr, comindErr, optextindErr, zagelitErr, zlinstpremErr, taxamtErr, taxindErr};
		screenDateFields = new BaseData[] {effdate, mattcess, premcess};
		screenDateErrFields = new BaseData[] {effdateErr, mattcessErr, premcessErr};
		screenDateDispFields = new BaseData[] {effdateDisp, mattcessDisp, premcessDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr579screen.class;
		protectRecord = Sr579protect.class;
	}

}
