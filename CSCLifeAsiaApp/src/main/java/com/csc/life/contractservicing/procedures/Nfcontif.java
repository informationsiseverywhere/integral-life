/*
 * File: Nflnapl.java
 * Date: 29 August 2009 23:00:12
 * Author: Quipoz Limited
 * 
 * Class transformed from NFLNAPL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;
import com.csc.life.contractservicing.dataaccess.dao.OverpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Overpf;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
*
*REMARKS.
*
*        CONTINUE TO INFORCE (DUMY) SUBROUTINE.
*        ===================================
*\
*/
public class Nfcontif extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Ovrduerec ovrduerec1 = new Ovrduerec();
	private Varcom varcom = new Varcom();
    private OverpfDAO overpfDAO =  getApplicationContext().getBean("overpfDAO", OverpfDAO.class);


@Override
public void mainline(Object... parmArray)
	{
		ovrduerec1.ovrdueRec = convertAndSetParam(ovrduerec1.ovrdueRec, parmArray, 0);
		try {
			mainline100();
			
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
	ovrduerec1.statuz.set(varcom.oK);
	Overpf overpf = overpfDAO.getOverpfRecord(ovrduerec1.chdrcoy.toString(),ovrduerec1.chdrnum.toString() , "1");
	
	if(overpf == null)
		updateOver();
}

protected void updateOver()
{
	  Overpf overpf = new  Overpf();
	  overpf.setChdrcoy(ovrduerec1.chdrcoy.toString());
	  overpf.setCnttype(ovrduerec1.cnttype.toString());	
	  overpf.setChdrnum(ovrduerec1.chdrnum.toString());
	  overpf.setValidflag("1");
	  overpf.setOverdueflag("O");
	  overpf.setEffectivedate(ovrduerec1.effdate.toInt());
	  overpf.setEnddate(varcom.vrcmMaxDate.toInt());
	  
	  overpfDAO.insertOverpf(overpf);
	
}
}

