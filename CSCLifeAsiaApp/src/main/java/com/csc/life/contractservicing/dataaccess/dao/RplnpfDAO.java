package com.csc.life.contractservicing.dataaccess.dao;


import com.csc.life.contractservicing.dataaccess.model.Rplnpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface RplnpfDAO {
	
	public Rplnpf getRploanRecord(String chdrnum ,String status);
	public void updateRploanRecord(Rplnpf rplnpf);
	public void insertRploanpf(Rplnpf rplnpf)throws SQLRuntimeException;
	
	public void deleteRploanpf(String chdrnum , int tranno);
	public int getLoanRepayRecord(String chdrnum);
	public void updateRploantrannoRecord(Rplnpf rplnpf);
}
