package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6661
 * @version 1.0 generated on 30/08/09 06:56
 * @author Quipoz
 */
public class S6661ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(197);
	public FixedLengthStringData dataFields = new FixedLengthStringData(69).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData contRevFlag = DD.contrevflg.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData subprogs = new FixedLengthStringData(20).isAPartOf(dataFields, 40);
	public FixedLengthStringData[] subprog = FLSArrayPartOfStructure(2, 10, subprogs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(subprogs, 0, FILLER_REDEFINE);
	public FixedLengthStringData subprog01 = DD.subprog.copy().isAPartOf(filler,0);
	public FixedLengthStringData subprog02 = DD.subprog.copy().isAPartOf(filler,10);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 69);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData contrevflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData subprogsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] subprogErr = FLSArrayPartOfStructure(2, 4, subprogsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(subprogsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData subprog01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData subprog02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 101);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] contrevflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData subprogsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] subprogOut = FLSArrayPartOfStructure(2, 12, subprogsOut, 0);
	public FixedLengthStringData[][] subprogO = FLSDArrayPartOfArrayStructure(12, 1, subprogOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(subprogsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] subprog01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] subprog02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6661screenWritten = new LongData(0);
	public LongData S6661protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6661ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(subprog01Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subprog02Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(contrevflgOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcodeOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, subprog01, subprog02, contRevFlag, trcode};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, subprog01Out, subprog02Out, contrevflgOut, trcodeOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, subprog01Err, subprog02Err, contrevflgErr, trcodeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6661screen.class;
		protectRecord = S6661protect.class;
	}

}
