/*
 * File: Pt524at.java
 * Date: 30 August 2009 2:01:18
 * Author: Quipoz Limited
 *
 * Class transformed from PT524AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*  POLICY DEBT REVERSAL   -   AT MODULE
*  ------------------------------------
*
*  This AT module is called by the Policy Debt Reversal
*  program PT524  and the Contract number with Trans. no are
*  pass in the AT parameters area as the "primary key".
*
*  AT PROCESSING
*
*  Check the return status  codes  for  each  of  the
*  following records being updated.
*
*  CONTRACT HEADER RECORD
*
*  Read the  Contract Header record (CHDRLNB) for the relevant
*  contract.  Then, updates trans. no to CHDRLNB for the
*  corresponding record.
*
*  Only  post  when  the amount  is greater  than zero.  Call
*  LIFACMV  ("cash"   posting  subroutine)  to  post  to  the
*  correct  account. The  posting  required is defined in the
*  appropriate line no.  on  the  T5645  table entry.  Set up
*  and pass the linkage area as follows:
*
*  Post to the sub-account (01) or (02) see below:
*  Use the  reversed amount from TPOLDBT on the  sub-account
*  with the sign from T5645.
*
*  If it is amount from interest rate
*  then post to the sub-account (01).
*
*  If it is amount without interest rate
*  then post to the sub-account (02).
*
*  Amount between interest or without interest can be identified
*  by considering the Batch Trans Code (otherwise by action code).
*     - TT65 is trans. code for debt amount from interest rate.
*     - TT66 is trans. code for debt amount from non interest rate.
*
*  Then post to the sub-account (03) as the above counterpart.
*
*  The accounting entry will be the following :
*
*  Dr.   LP  CI or CN            999,999.99    ----| This is reversed
*     Cr.   LP  OR                  999,999.99 ----|         amount.
*
*  Delete record from TPOLDBT with the according trans no.
*
*  GENERAL HOUSE-KEEPING
*
*  1) Update the  batch  header  by  calling  BATCUP with a
*     function of WRITS and the following parameters:
*
*  - transaction count equals 1
*  - all other amounts zero
*  - batch key from AT linkage
*
*  3) Release contract SFTLOCK.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pt524at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PT524AT");
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);
	private ZonedDecimalData wsaaPrimaryTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaPrimaryKey, 9).setUnsigned();
	private FixedLengthStringData wsaaPrimaryAction = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 14);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaTransArea, 16);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 19);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 20).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();

		/*    88  WITH-INTEREST           VALUE 'TT65'.
		    88  WITHOUT-INTEREST        VALUE 'TT66'.                    */
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaTrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTranref, 0);
	private FixedLengthStringData wsaaTrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTranref, 1);
	private ZonedDecimalData wsaaTrTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTranref, 9).setUnsigned();
	private FixedLengthStringData wsaaTrAction = new FixedLengthStringData(1).isAPartOf(wsaaTranref, 14);

	private FixedLengthStringData wsaaAction = new FixedLengthStringData(1);
	private Validator withInterest = new Validator(wsaaAction, "A");
	private Validator withoutInterest = new Validator(wsaaAction, "B");
		/* ERRORS */
	private static final String f931 = "F931";
		/* TABLES */
	private static final String t5645 = "T5645";
		/* FORMATS */
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String tpoldbtrec = "TPOLDBTREC";
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String ptrnrec = "PTRNREC";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Atmodrec atmodrec = new Atmodrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		read1202
	}

	public Pt524at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		housekeepingTerminate3000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		getTranAcctRules1100();
		checkInt1200();
		callDatcons1300();
		readContractHeader1400();
		/*EXIT*/
	}

	/**
	* <pre>
	* Initial read of the Contract Header.                        *
	* </pre>
	*/
protected void getTranAcctRules1100()
	{
		read1101();
	}

protected void read1101()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("PT524");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(atmodrec.company);
		descIO.setDesctabl(t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("PT524");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			xxxxFatalError();
		}
	}

protected void checkInt1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					check1201();
				case read1202:
					read1202();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void check1201()
	{
		/* MOVE SPACES                 TO WSAA-TRAN-CODE.               */
		wsaaTranref.set(SPACES);
		acmvrevIO.setRecKeyData(SPACES);
		acmvrevIO.setRldgcoy(wsaaPrimaryChdrcoy);
		acmvrevIO.setRdocnum(wsaaPrimaryChdrnum);
		acmvrevIO.setTranno(wsaaPrimaryTranno);
		acmvrevIO.setFormat(acmvrevrec);
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void read1202()
	{
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acmvrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(acmvrevIO.getRldgcoy(), wsaaPrimaryChdrcoy)
		|| isNE(acmvrevIO.getRdocnum(),wsaaPrimaryChdrnum)
		|| isNE(acmvrevIO.getTranno(),wsaaPrimaryTranno)) {
			acmvrevIO.setRldgcoy(wsaaPrimaryChdrcoy);
			acmvrevIO.setRdocnum(wsaaPrimaryChdrnum);
			acmvrevIO.setTranno(wsaaPrimaryTranno);
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(f931);
			xxxxFatalError();
		}
		/* MOVE ACMVREV-BATCTRCDE      TO WSAA-TRAN-CODE.               */
		wsaaTranref.set(acmvrevIO.getTranref());
		wsaaAction.set(wsaaTrAction);
		if ((withInterest.isTrue()
		|| withoutInterest.isTrue())
		&& isEQ(acmvrevIO.getRldgacct(),wsaaPrimaryChdrnum)) {
			return ;
		}
		acmvrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read1202);
	}

	/**
	* <pre>
	* Call DATCON1 subroutine.                                    *
	* </pre>
	*/
protected void callDatcons1300()
	{
		/*DATCON1*/
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

	/**
	* <pre>
	* Initial read of the Contract Header.                        *
	* </pre>
	*/
protected void readContractHeader1400()
	{
		/*READ-CONTRACT-HEADER*/
		chdrlnbIO.setRecKeyData(SPACES);
		chdrlnbIO.setChdrcoy(wsaaPrimaryChdrcoy);
		chdrlnbIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/
protected void process2000()
	{
		/*PROCESS*/
		processContractHeader2100();
		postPolDebt2400();
		deleteTpoldbt2500();
		writePtrn2600();
		/*EXIT*/
	}

	/**
	* <pre>
	* Process the contract header.                                *
	* </pre>
	*/
protected void processContractHeader2100()
	{
		/*PROCESS-CONTRACT-HEADER*/
		setPrecision(chdrlnbIO.getTranno(), 0);
		chdrlnbIO.setTranno(add(chdrlnbIO.getTranno(),1));
		chdrlnbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Post the policy debt.                                       *
	* </pre>
	*/
protected void postPolDebt2400()
	{
			go2401();
		}

protected void go2401()
	{
		/* Post to sub account (01) or (02) depend on action code or*/
		/* batch trans. code.*/
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrlnbIO.getChdrnum());
		tpoldbtIO.setTranno(wsaaPrimaryTranno);
		tpoldbtIO.setFormat(tpoldbtrec);
		tpoldbtIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			xxxxFatalError();
		}
		initialize(lifacmvrec.lifacmvRec);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		if (withInterest.isTrue()) {
			wsaaI.set(1);
		}
		else {
			wsaaI.set(2);
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaI.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaI.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaI.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaI.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaI.toInt()]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		compute(lifacmvrec.origamt, 2).set(mult(tpoldbtIO.getTdbtamt(),-1));
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(wsaaPrimaryKey);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(wsaaEffdate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(tpoldbtIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Post to sub account (03)*/
		initialize(lifacmvrec.lifacmvRec);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		wsaaI.set(3);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaI.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaI.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaI.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaI.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaI.toInt()]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		compute(lifacmvrec.origamt, 2).set(mult(tpoldbtIO.getTdbtamt(),-1));
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(wsaaPrimaryKey);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(wsaaEffdate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(tpoldbtIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void deleteTpoldbt2500()
	{
		go2501();
	}

protected void go2501()
	{
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrlnbIO.getChdrnum());
		tpoldbtIO.setTranno(wsaaPrimaryTranno);
		tpoldbtIO.setFormat(tpoldbtrec);
		tpoldbtIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			xxxxFatalError();
		}
		tpoldbtIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			xxxxFatalError();
		}
	}

protected void writePtrn2600()
	{
		start2610();
	}

protected void start2610()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrlnbIO.getTranno());
		/* MOVE WSAA-EFFDATE           TO PTRN-PTRNEFF          <LA5184>*/
		/*                                PTRN-DATESUB.         <LA5184>*/
		ptrnIO.setPtrneff(wsaaEffdate);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Housekeeping prior to termination.                          *
	* </pre>
	*/
protected void housekeepingTerminate3000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		batchHeader3200();
		releaseSoftlock3300();
		/*EXIT*/
	}

	/**
	* <pre>
	* Write batch header transaction.                             *
	* </pre>
	*/
protected void batchHeader3200()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock3300()
	{
		releaseSoftlock3301();
	}

protected void releaseSoftlock3301()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}
}
