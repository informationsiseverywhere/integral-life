/*
 * File: P5132.java
 * Date: 30 August 2009 0:11:59
 * Author: Quipoz Limited
 * 
 * Class transformed from P5132.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;//ILIFE-7968
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILIFE-7968
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
*  P5132 - Component Change End Processing
*  ---------------------------------------
*
*  This program will be used to initiate the  final  processing
*  of  the  on-line  portion  of Component Change and Component
*  Addition.  It has no screen and is performed after  all  the
*  on-line processing has been completed.
*
*  It will perform the following tasks:
*
*       . Transfer the softlock to AT
*
*       . Request the AT function which will complete the
*         processing and call breakout if required.
*
*
*  Processing:
*  -----------
*
*  Perform a RETRV on CHDRMJA.
*
*  Perform a RLSE on CHDRMJA.
*
*  Check  to  see  whether  there  are  any COVTMJA records. If
*  there are not then  no  changes  were  made  and  so  no  AT
*  processing  is  necessary.  If this is the case call SFTLOCK
*  to unlock the  contract  and  finish  processing.  Otherwise
*  proceed as follows:
*
*  Further  updating  will  be  carried  out  under  AT so call
*  SFTLOCK with a function of  'TOAT',  (transfer  to  AT),  to
*  allocate  possession  of the contract to the AT transaction.
*  Use the following parameters:
*
*  .  SFTL-FUNCTION     - 'TOAT'
*  .  SFTL-COMPANY      - CHDRMJA-COMPANY
*  .  SFTL-ENTITY       - CHDRMJA-CHDRNUM
*  .  SFTL-ENTTYP       - 'CH'
*  .  SFTL-TRANSACTION  - WSKY-BATC-BATCTRCDE
*  .  SFTL-USER         - VRCM-USER
*
*  Then call ATREQ with a request to  run  P5132AT  which  will
*  complete  the  processing and also call BRKOUT if necessary.
*  Set the Primary Key for AT as the Company, Contract  Number,
*  Termid and User.
*
*  After  this  control will return to the Sub-Menu. Set up the
*  following message in WSSP for display by the Sub-Menu:
*
*       "AT request submitted for XXXXXXXX"
*
*  where XXXXXXXX is the contract number.
*
*  Add 1 to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5132 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5132");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaAtSubmitFlag = new FixedLengthStringData(1).init(SPACES);
	private Validator atSubmissionReqd = new Validator(wsaaAtSubmitFlag, "Y");

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 16);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 24);
	private FixedLengthStringData filler = new FixedLengthStringData(175).isAPartOf(wsaaTransactionRec, 25, FILLER).init(SPACES);

		/* WSAA-MESSAGE-AREA */
	private FixedLengthStringData wsaaMsgarea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaMessage = new FixedLengthStringData(25).isAPartOf(wsaaMsgarea, 0).init("AT request submitted for ");
	private FixedLengthStringData wsaaMsgnum = new FixedLengthStringData(8).isAPartOf(wsaaMsgarea, 25);
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Atreqrec atreqrec = new Atreqrec();
	private Batcuprec batcuprec = new Batcuprec();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatckey = new Batckey();
	//ILIFE-7968 start
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	//ILIFE-7968 end

	public P5132() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		releaseHeader1030();
		readCovt1040();
	}

protected void initialise1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*RETRIEVE-HEADER*/
		//ILIFE-7968 start
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());//ILIFE-7991
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
				
		/*chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		//ILIFE-7968 end
	}

protected void releaseHeader1030()
	{
		chdrmjaIO.setFunction("RLSE");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
	}

protected void readCovt1040()
	{
		wsaaAtSubmitFlag.set("N");
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setChdrcoy(wsspcomn.company);
		covtmjaIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 //IJTI-1485
		covtmjaIO.setPlanSuffix(ZERO);
		covtmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		//ILIFE-7968
		if (isNE(wsspcomn.company,covtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),covtmjaIO.getChdrnum())) {
			covtmjaIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtmjaIO.getStatuz(),varcom.oK)) {
			wsaaAtSubmitFlag.set("Y");
		}
		else {
			wsaaAtSubmitFlag.set("N");
		}
	}

protected void screenEdit2000()
	{
		/*EDIT*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		batchUpdate3010();
		batchUpdate3020();
	}

protected void batchUpdate3010()
	{
		if (atSubmissionReqd.isTrue()) {
			sftlckCallAt3100();
		}
		else {
			rlseSftlock3200();
		}
	}

protected void batchUpdate3020()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		batcuprec.batccoy.set(wsaaBatckey.batcBatccoy);
		batcuprec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		batcuprec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		batcuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}

protected void sftlckCallAt3100()
	{
		softlock3110();
	}

protected void softlock3110()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());//ILIFE-7968 //IJTI-1485
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("TOAT");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5132AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		atreqrec.primaryKey.set(chdrpf.getChdrnum());//ILIFE-7968 //IJTI-1485
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaPolsum.set(chdrpf.getPolsum());//ILIFE-7968
		wsaaEffdate.set(covtmjaIO.getEffdate());
		wsaaFsuCoy.set(wsspcomn.fsuco);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
		wsaaMsgnum.set(chdrpf.getChdrnum());//ILIFE-7968 //IJTI-1485
		wsspcomn.msgarea.set(wsaaMsgarea);
	}

protected void rlseSftlock3200()
	{
		unlockContract3210();
	}

protected void unlockContract3210()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());//ILIFE-7968 //IJTI-1485
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
