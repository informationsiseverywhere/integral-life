package com.csc.life.contractservicing.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Td5izrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData td5izRec = new FixedLengthStringData(500);
  	public ZonedDecimalData mthscompadd = new ZonedDecimalData(10, 0).isAPartOf(td5izRec, 0).setUnsigned();
  	public ZonedDecimalData mthscompmod = new ZonedDecimalData(10, 0).isAPartOf(td5izRec, 10).setUnsigned();
  	
  	public FixedLengthStringData filler = new FixedLengthStringData(480).isAPartOf(td5izRec, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(td5izRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			td5izRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}