package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.StringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sr60s
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class Sr60sScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(91);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(27).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData schmno = DD.schemekey.copy().isAPartOf(dataFields,11);//ALS-317
	public FixedLengthStringData ownersel = DD.clttwo.copy().isAPartOf(dataFields,19);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 27);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData schmnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ownerselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);

	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 43);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] schmnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ownerselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public StringData s290hideflag = new StringData();
	public LongData Sr60sscreenWritten = new LongData(0);
	public LongData Sr60sprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr60sScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(schmnoOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ownerselOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});

		
		screenFields = new BaseData[] {action, chdrsel, ownersel, schmno};
		screenOutFields = new BaseData[][] {actionOut, chdrselOut, ownerselOut, schmnoOut};
		screenErrFields = new BaseData[] {actionErr, chdrselErr, ownerselErr, schmnoErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sr60sscreen.class;
		protectRecord = Sr60sprotect.class;
	}

}
