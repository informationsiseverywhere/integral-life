package com.csc.life.contractservicing.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6752screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 16, 7, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 1, 2, 8, 7}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6752ScreenVars sv = (S6752ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6752screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6752screensfl, 
			sv.S6752screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6752ScreenVars sv = (S6752ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6752screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6752ScreenVars sv = (S6752ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6752screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6752screensflWritten.gt(0))
		{
			sv.s6752screensfl.setCurrentIndex(0);
			sv.S6752screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6752ScreenVars sv = (S6752ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6752screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6752ScreenVars screenVars = (S6752ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.tranno.setFieldName("tranno");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.trcode.setFieldName("trcode");
				screenVars.datesubDisp.setFieldName("datesubDisp");
				screenVars.ind.setFieldName("ind");
				screenVars.descrip.setFieldName("descrip");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.tranno.set(dm.getField("tranno"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.trcode.set(dm.getField("trcode"));
			screenVars.datesubDisp.set(dm.getField("datesubDisp"));
			screenVars.ind.set(dm.getField("ind"));
			screenVars.descrip.set(dm.getField("descrip"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6752ScreenVars screenVars = (S6752ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.tranno.setFieldName("tranno");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.trcode.setFieldName("trcode");
				screenVars.datesubDisp.setFieldName("datesubDisp");
				screenVars.ind.setFieldName("ind");
				screenVars.descrip.setFieldName("descrip");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("tranno").set(screenVars.tranno);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("trcode").set(screenVars.trcode);
			dm.getField("datesubDisp").set(screenVars.datesubDisp);
			dm.getField("ind").set(screenVars.ind);
			dm.getField("descrip").set(screenVars.descrip);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6752screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6752ScreenVars screenVars = (S6752ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.tranno.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.trcode.clearFormatting();
		screenVars.datesubDisp.clearFormatting();
		screenVars.ind.clearFormatting();
		screenVars.descrip.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6752ScreenVars screenVars = (S6752ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.trcode.setClassString("");
		screenVars.datesubDisp.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.descrip.setClassString("");
	}

/**
 * Clear all the variables in S6752screensfl
 */
	public static void clear(VarModel pv) {
		S6752ScreenVars screenVars = (S6752ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.tranno.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.trcode.clear();
		screenVars.datesubDisp.clear();
		screenVars.datesub.clear();
		screenVars.ind.clear();
		screenVars.descrip.clear();
	}
}
