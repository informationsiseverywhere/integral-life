/*
 * File: P6759.java
 * Date: 30 August 2009 0:56:19
 * Author: Quipoz Limited
 * 
 * Class transformed from P6759.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.screens.S6759ScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This is the Windforward Confirmation Screen which is displayed
* before submitting the Windforward AT and before deleting all
* Windforward records for a contract.  The message displayed on-
* screen depends on the option from which the screen is accessed.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P6759 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6759");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaErrorline = new FixedLengthStringData(78);
		/* FORMATS */
	private static final String chdrenqrec = "CHDRENQREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private S6759ScreenVars sv = ScreenProgram.getScreenVars( S6759ScreenVars.class);
	private static final String e186 = "E186";
	private static final String f509 = "F509";

	


	public P6759() {
		super();
		screenVars = sv;
		new ScreenModel("S6759", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaErrorline.set(scrnparams.errorline);
		chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction("RLSE");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isNE(wsaaErrorline,SPACES)) {
			sv.hflag03Out[varcom.nd.toInt()].set("N");
			wsspcomn.edterror.set("Y");
			scrnparams.errorline.set(wsaaErrorline);
		}
		else {
			sv.hflag03Out[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.flag,"C")) {
			sv.hflag02Out[varcom.nd.toInt()].set("Y");
			sv.hflag01Out[varcom.nd.toInt()].set("N");
		}
		if (isEQ(wsspcomn.flag,"D")) {
			sv.hflag01Out[varcom.nd.toInt()].set("Y");
			sv.hflag02Out[varcom.nd.toInt()].set("N");
		}
		return ;
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
	sv.confirmOut[varcom.nd.toInt()].set("N");
	/*UPDATE-DATABASE*/
	if (isEQ(scrnparams.statuz, varcom.oK)){
		sv.confirm.set("Y");
	}
	if(sv.confirm.toString().equalsIgnoreCase("y")){
		chdrenqIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}
	else if (isEQ(sv.confirm, SPACES)|| sv.confirm.toString().equalsIgnoreCase("n")) {
		lock();
	}
	else if	(!sv.confirm.toString().equalsIgnoreCase("y") && !sv.confirm.toString().equalsIgnoreCase("n")) {
		lock();
		sv.confirmErr.set(f509);
		sv.confirmOut[varcom.nd.toInt()].set("Y");
		preScreenEdit();
	}
		/*EXIT*/
	}

protected void whereNext4000(){
	if(sv.confirm.toString().equalsIgnoreCase("y")){
		//NEXT-PROGRAM
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		//EXIT
	}
	else if (isEQ(sv.confirm, SPACES) || sv.confirm.toString().equalsIgnoreCase("n")) {
		wsspcomn.programPtr.subtract(1);
		
	}
}

protected void lock(){
	chdrenqIO.setFormat(chdrenqrec);
	chdrenqIO.setFunction("KEEPS");
	SmartFileCode.execute(appVars, chdrenqIO);
	if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
		syserrrec.statuz.set(chdrenqIO.getStatuz());
		syserrrec.params.set(chdrenqIO.getParams());
		fatalError600();
	}
}

}
