package com.csc.life.contractservicing.dataaccess.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.csc.life.contractservicing.dataaccess.model.Covrpf;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CovrpfDAO extends BaseDAO<Covrpf> {

	public int updateIndexationInd(String contractNum, String indexationInd, String compy);
	public List<Covrpf>  getCovrlnbPF(String chdrcoy, String chdrnum, String life, String coverage, String rider, Integer planSuffix);
	public List<Covrpf> searchCovrRecordByCoyNumDescUniquNo(String chdrcoy, String chdrNum);// ILIFE-7584
	public int bulkUpdateCovrData(Map<String, Covtpf> covtUpdateMap);// ILIFE-7584
	public List<Covrpf> getCovrpfRecord(String chdrcoy, String chdrNum);
	public BigDecimal getPreviousInstprem(String chdrcoy, String chdrNum, String life, String coverage, String rider, String crtable, String currfrom);
	
	
}