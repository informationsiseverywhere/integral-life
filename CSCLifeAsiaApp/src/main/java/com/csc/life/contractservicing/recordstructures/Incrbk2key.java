package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:58
 * Description:
 * Copybook name: INCRBK2KEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Incrbk2key extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData incrbk2FileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData incrbk2Key = new FixedLengthStringData(64).isAPartOf(incrbk2FileKey, 0, REDEFINE);
  	public FixedLengthStringData incrbk2Chdrcoy = new FixedLengthStringData(1).isAPartOf(incrbk2Key, 0);
  	public FixedLengthStringData incrbk2Chdrnum = new FixedLengthStringData(8).isAPartOf(incrbk2Key, 1);
  	public FixedLengthStringData incrbk2Life = new FixedLengthStringData(2).isAPartOf(incrbk2Key, 9);
  	public FixedLengthStringData incrbk2Coverage = new FixedLengthStringData(2).isAPartOf(incrbk2Key, 11);
  	public FixedLengthStringData incrbk2Rider = new FixedLengthStringData(2).isAPartOf(incrbk2Key, 13);
  	public PackedDecimalData incrbk2PlanSuffix = new PackedDecimalData(4, 0).isAPartOf(incrbk2Key, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(incrbk2Key, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(incrbk2FileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		incrbk2FileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}