package com.csc.life.contractservicing.dataaccess.model;

import java.util.Date;

/**
 * Tashpf for table
 */
public class Tashpf implements java.io.Serializable {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private Integer tranno;
	private String validflag;
	private String asgnpfx;
	private String asgnnum;
	private Integer seqno;
	private Integer commfrom;
	private Integer commto;
	private String trancde;
	private String usrprf;
	private String jobnm;
	private Date datime;

	public Tashpf() {
	}

	public Tashpf(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public Tashpf(long uniqueNumber, String chdrcoy,
			String chdrnum, Integer tranno, String validflag,
			String asgnpfx, String asgnnum, Integer seqno,
			Integer commfrom, Integer commto, String trancde,
			String usrprf, String jobnm, Date datime) {
		this.uniqueNumber = uniqueNumber;
		this.chdrcoy = chdrcoy;
		this.chdrnum = chdrnum;
		this.tranno = tranno;
		this.validflag = validflag;
		this.asgnpfx = asgnpfx;
		this.asgnnum = asgnnum;
		this.seqno = seqno;
		this.commfrom = commfrom;
		this.commto = commto;
		this.trancde = trancde;
		this.usrprf = usrprf;
		this.jobnm = jobnm;
		this.datime = (Date) datime.clone(); //IJTI-460
	}

	public long getUniqueNumber() {
		return this.uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return this.chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return this.chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public Integer getTranno() {
		return this.tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public String getValidflag() {
		return this.validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public String getAsgnpfx() {
		return this.asgnpfx;
	}

	public void setAsgnpfx(String asgnpfx) {
		this.asgnpfx = asgnpfx;
	}

	public String getAsgnnum() {
		return this.asgnnum;
	}

	public void setAsgnnum(String asgnnum) {
		this.asgnnum = asgnnum;
	}

	public Integer getSeqno() {
		return this.seqno;
	}

	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}

	public Integer getCommfrom() {
		return this.commfrom;
	}

	public void setCommfrom(Integer commfrom) {
		this.commfrom = commfrom;
	}

	public Integer getCommto() {
		return this.commto;
	}

	public void setCommto(Integer commto) {
		this.commto = commto;
	}

	public String getTrancde() {
		return this.trancde;
	}

	public void setTrancde(String trancde) {
		this.trancde = trancde;
	}

	public String getUsrprf() {
		return this.usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return this.jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	//IJTI-461 START
	public Date getDatime() {
		if (this.datime==null){
			return null;
		} else {
			return (Date) this.datime.clone();
		}
	} //IJTI-461 END

	public void setDatime(Date datime) {
		this.datime = (Date) datime.clone(); //IJTI-460
	}

}
