package com.csc.life.contractservicing.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sr57h
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class Sr57hScreenVars extends SmartVarModel { 

	//203 + 48 + 144
	public FixedLengthStringData dataArea = new FixedLengthStringData(395);
	public FixedLengthStringData dataFields = new FixedLengthStringData(203).isAPartOf(dataArea, 0);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,80);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,90);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData mandref = DD.mandref.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData numsel = DD.numsel.copy().isAPartOf(dataFields,138);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,148);
	/* TSD 321 STARTs */
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields, 156); //47 
	/* TSD 321 ENDs */
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 203);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData facthousErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData mandrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData numselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	/* TSD 321 STARTs */
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	/* TSD 321 ENDs */
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 251);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] facthousOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] mandrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] numselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	/* TSD 321 STARTs */
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	/* TSD 321 ENDs */
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);

	public LongData Sr57hscreenWritten = new LongData(0);
	public LongData Sr57hwindowWritten = new LongData(0);
	public LongData Sr57hprotectWritten = new LongData(0);

	@Override
	public boolean hasSubfile() {
		return false;
	}


	public Sr57hScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {
		/* TSD 321 Starts */
		fieldIndMap.put(payrnumOut,new String[] {" NN"," NN "," NN "," NN ",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payornameOut,new String[] {" NN"," NN "," NN "," NN ",null, null, null, null, null, null, null, null});
		/*TSD 321 Ends */
		
		fieldIndMap.put(mandrefOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {payrnum, payorname, currcode, facthous, numsel, billcd, mandref, bankkey, bankdesc, branchdesc, bankacckey, bankaccdsc};
		screenOutFields = new BaseData[][] {payrnumOut, payornameOut, currcodeOut, facthousOut, numselOut, billcdOut, mandrefOut, bankkeyOut, bankdescOut, branchdescOut, bankacckeyOut, bankaccdscOut};
		screenErrFields = new BaseData[] {payrnumErr, payornameErr, currcodeErr, facthousErr, numselErr, billcdErr, mandrefErr, bankkeyErr, bankdescErr, branchdescErr, bankacckeyErr, bankaccdscErr};
		screenDateFields = new BaseData[] {billcd};
		screenDateErrFields = new BaseData[] {billcdErr};
		screenDateDispFields = new BaseData[] {billcdDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr57hscreen.class;
		protectRecord = Sr57hprotect.class;
	}

}
