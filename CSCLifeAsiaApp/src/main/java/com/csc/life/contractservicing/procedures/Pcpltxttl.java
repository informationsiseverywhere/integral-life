/*
 * File: Pcpltxttl.java
 * Date: December 3, 2013 3:17:18 AM ICT
 * Author: CSC
 * 
 * Class transformed from PCPLTXTTL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.contractservicing.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.Iterator; //IBPLIFE-680
import java.util.List; //IBPLIFE-680

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO; //IBPLIFE-680
import com.csc.life.productdefinition.dataaccess.model.Covrpf; //IBPLIFE-680
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description............................................. .
*
*  001  Tax Desc   (i.e. Service Tax)
*  002  Tax Amount
*
*  Overview.
*  ---------
*  This subroutine is used in letter printing to extract the total
*  taxes as at the contract billed to date.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Pcpltxttl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLTXTTL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(5000).init(SPACES);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
		/* Subroutine specific fields.*/
	private ZonedDecimalData wsaaTotalAmt1 = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaTotalAmt2 = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaTotalTax1 = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaTotalTax2 = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaTaxDesc1 = new FixedLengthStringData(24).init(SPACES);
	private FixedLengthStringData wsaaTaxDesc2 = new FixedLengthStringData(24).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (100, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String covtlnbrec = "COVTLNBREC";
		/* TABLES */
	private static final String tr52b = "TR52B";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
 //IBPLIFE-680 start	
	protected Covrpf covrpf = null;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpflist ;
 //IBPLIFE-680 end  

	public Pcpltxttl() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			initialise100();
			getCntfee200();
			if (isEQ(chdrlnbIO.getValidflag(), "1")) {
				if(!covrpflist.isEmpty()) {
					getCovrAndFormat300();
				}
			}
			else {
				while ( !(isEQ(covtlnbIO.getStatuz(), varcom.endp))) {
					getCovtAndFormat400();
				}
			}
			formatChdrValues500();
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		if (isEQ(wsaaCounter, ZERO)) {
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		else {
			if (isGT(pcpdatarec.fldOccur, wsaaCounter)
			|| isGT(pcpdatarec.groupOccurance, wsaaCounter)) {
				pcpdatarec.statuz.set(varcom.endp);
				return ;
			}
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a costant number of positions between fields.*/
		if (isEQ(pcpdatarec.fldOffset, 1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset, (mult((sub(pcpdatarec.fldOccur, 1)), wsaaConstant))));
		}
		if (isNE(strpos[offset.toInt()], NUMERIC)
		|| isNE(fldlen[offset.toInt()], NUMERIC)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		if (isEQ(strpos[offset.toInt()], 0)
		|| isEQ(fldlen[offset.toInt()], 0)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		/*  INITIALIZE                     WSAA-START-AND-LENGTH.        */
		for (offset.set(1); !(isGT(offset, 100)); offset.add(1)){
			wsaaStartAndLength[offset.toInt()].set(ZERO);
		}
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		if (isNE(pcpdatarec.groupOccurance, NUMERIC)) {
			pcpdatarec.groupOccurance.set(1);
		}
		wsaaCounter.set(ZERO);
		offset.set(1);
		strpos[offset.toInt()].set(1);
		covrpf = new Covrpf(); //IBPLIFE-680
		covrpflist = covrpfDAO.getCovrByComAndNumAllFlagRecord(letcIO.getRdoccoy().toString(),letcIO.getRdocnum().toString()); //IBPLIFE-680
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setFunction(varcom.begn);
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(letcIO.getRdoccoy());
		chdrlnbIO.setChdrnum(letcIO.getRdocnum());
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError900();
		}
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/* Read TR52D for Taxcode*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRdoccoy());
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError900();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(letcIO.getRdoccoy());
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError900();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void getCntfee200()
	{
		start200();
	}

protected void start200()
	{
		if (isEQ(tr52drec.txcode, SPACES)
		|| isEQ(chdrlnbIO.getSinstamt02(), ZERO)) {
			return ;
		}
		/* Read table TR52E*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(letcIO.getRdoccoy());
		itdmIO.setItemtabl(tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable()); //IBPLIFE-680
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getBtdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError900();
		}
		if ((isNE(itdmIO.getItemcoy(), letcIO.getRdoccoy()))
		|| (isNE(itdmIO.getItemtabl(), tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(letcIO.getRdoccoy());
			itdmIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itdmIO.setItmfrm(chdrlnbIO.getBtdate());
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError900();
			}
		}
		if ((isNE(itdmIO.getItemcoy(), letcIO.getRdoccoy()))
		|| (isNE(itdmIO.getItemtabl(), tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setDataKey(SPACES);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(letcIO.getRdoccoy());
			itdmIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itdmIO.setItemitem(wsaaTr52eKey);
			itdmIO.setItmfrm(chdrlnbIO.getBtdate());
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError900();
			}
		}
		tr52erec.tr52eRec.set(itdmIO.getGenarea());
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		/* Call TR52D tax subroutine*/
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.amountIn.set(chdrlnbIO.getSinstamt02());
		txcalcrec.transType.set("CNTF");
		txcalcrec.effdate.set(letcIO.getLetterRequestDate());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError900();
		}
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTotalAmt1.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTotalAmt2.add(txcalcrec.taxAmt[2]);
		}
		if (isNE(txcalcrec.taxType[1], SPACES)) {
			descIO.setRecKeyData(SPACES);
			descIO.setDescitem(txcalcrec.taxType[1]);
			descIO.setDesctabl(tr52b);
			readDesc800();
			wsaaTaxDesc1.set(descIO.getLongdesc());
		}
		if (isNE(txcalcrec.taxType[2], SPACES)) {
			descIO.setRecKeyData(SPACES);
			descIO.setDescitem(txcalcrec.taxType[2]);
			descIO.setDesctabl(tr52b);
			readDesc800();
			wsaaTaxDesc2.set(descIO.getLongdesc());
		}
	}

protected void getCovrAndFormat300()
	{
		getData300();
	}

protected void getData300()
	{
 //IBPLIFE-680 start
	    if(covrpflist == null || covrpflist.isEmpty()){
	    	return ;
        } else { 
        	Iterator<Covrpf> iterator = covrpflist.iterator(); 
        } 
	    if (isNE(covrpf.getValidflag(), "1")) {
			return ;
		}
	    getPremTax600();
	 //IBPLIFE-680 end    
	    
	    
	}

protected void getCovtAndFormat400()
	{
		getData400();
	}

protected void getData400()
	{
		covtlnbIO.setDataKey(SPACES);
		covtlnbIO.setStatuz(varcom.oK);
		covtlnbIO.setChdrcoy(letcIO.getRdoccoy());
		covtlnbIO.setChdrnum(letcIO.getRdocnum());
		covtlnbIO.setLife("01");
		covtlnbIO.setCoverage(SPACES);
		covtlnbIO.setRider(SPACES);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
		&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError900();
		}
		if (isEQ(covtlnbIO.getStatuz(), varcom.endp)
		|| isNE(covtlnbIO.getChdrcoy(), letcIO.getRdoccoy())
		|| isNE(covtlnbIO.getChdrnum(), letcIO.getRdocnum())) {
			covtlnbIO.setStatuz(varcom.endp);
			return ;
		}
		getPremTax600();
		covtlnbIO.setFunction(varcom.nextr);
	}

protected void formatChdrValues500()
	{
		start500();
		format515();
		format520();
	}

protected void start500()
	{
		/*  The following counter is incremented by 1 per each*/
		/*  successful read of tax info .It is used to stop*/
		/*  retrieving recurring fields when there are no more*/
		/*  records.*/
		wsaaCounter.add(1);
		/*FORMAT*/
		/*  Tax Description 1.*/
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(wsaaTaxDesc1, " ", "CHARACTERS", "  ", null));
		if (isNE(wsaaTaxDesc1, SPACES)) {
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaTaxDesc1, 1, fldlen[offset.toInt()]));
		}
		/*FORMAT*/
		/*  Tax amount 1.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaTotalTax1.set(wsaaTotalAmt1);
		fldlen[offset.toInt()].set(length(wsaaTotalTax1));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTotalTax1);
	}

protected void format515()
	{
		/*  Tax Description 2.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(wsaaTaxDesc2, " ", "CHARACTERS", "  ", null));
		if (isNE(wsaaTaxDesc2, SPACES)) {
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaTaxDesc2, 1, fldlen[offset.toInt()]));
		}
	}

protected void format520()
	{
		/*  Tax amount 2.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaTotalTax2.set(wsaaTotalAmt2);
		fldlen[offset.toInt()].set(length(wsaaTotalTax2));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTotalTax2);
		/*EXIT*/
	}

protected void getPremTax600()
	{
		start600();
	}

protected void start600()
	{
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read table TR52E*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRdoccoy());
		itemIO.setItemtabl(tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		if (isEQ(chdrlnbIO.getValidflag(), "1")) {
			wsaaTr52eCrtable.set(covrpf.getCrtable()); //IBPLIFE-680
		}
		else {
			wsaaTr52eCrtable.set(covtlnbIO.getCrtable());
		}
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError900();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(letcIO.getRdoccoy());
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError900();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(letcIO.getRdoccoy());
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError900();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		/* Call TR52D tax subroutine*/
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		if (isEQ(chdrlnbIO.getValidflag(), "1")) {
 //IBPLIFE-680 start
			txcalcrec.life.set(covrpf.getLife());
			txcalcrec.coverage.set(covrpf.getCoverage());
			txcalcrec.rider.set(covrpf.getRider());
			txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
			txcalcrec.crtable.set(covrpf.getCrtable());
 //IBPLIFE-680 end
		}
		else {
			txcalcrec.life.set(covtlnbIO.getLife());
			txcalcrec.coverage.set(covtlnbIO.getCoverage());
			txcalcrec.rider.set(covtlnbIO.getRider());
			/*       MOVE COVTLNB-PLAN-SUFFIX TO TXCL-PLAN-SUFFIX*/
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covtlnbIO.getCrtable());
		}
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(wsaaRateItem);
		if (isEQ(tr52erec.zbastyp, "Y")) {
			if (isEQ(chdrlnbIO.getValidflag(), "1")) {
				txcalcrec.amountIn.set(covrpf.getZbinstprem()); //IBPLIFE-680
			}
			else {
				txcalcrec.amountIn.set(covtlnbIO.getZbinstprem());
			}
		}
		else {
			if (isEQ(chdrlnbIO.getValidflag(), "1")) {
				txcalcrec.amountIn.set(covrpf.getInstprem()); //IBPLIFE-680
			}
			else {
				txcalcrec.amountIn.set(covtlnbIO.getInstprem());
			}
		}
		txcalcrec.transType.set("PREM");
		txcalcrec.effdate.set(letcIO.getLetterRequestDate());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError900();
		}
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTotalAmt1.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTotalAmt2.add(txcalcrec.taxAmt[2]);
		}
		if (isNE(txcalcrec.taxType[1], SPACES)) {
			descIO.setRecKeyData(SPACES);
			descIO.setDescitem(txcalcrec.taxType[1]);
			descIO.setDesctabl(tr52b);
			readDesc800();
			wsaaTaxDesc1.set(descIO.getLongdesc());
		}
		if (isNE(txcalcrec.taxType[2], SPACES)) {
			descIO.setRecKeyData(SPACES);
			descIO.setDescitem(txcalcrec.taxType[2]);
			descIO.setDesctabl(tr52b);
			readDesc800();
			wsaaTaxDesc2.set(descIO.getLongdesc());
		}
	}

protected void readDesc800()
	{
		start800();
	}

protected void start800()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(letcIO.getRdoccoy());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError900();
		}
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc("??????????????????????????????");
			descIO.setShortdesc("??????????");
		}
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES)
		|| isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
