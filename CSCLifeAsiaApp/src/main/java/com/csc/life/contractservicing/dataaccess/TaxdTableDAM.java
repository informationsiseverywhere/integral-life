package com.csc.life.contractservicing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: TaxdTableDAM.java
 * Date: Tue, 3 Dec 2013 04:10:07
 * Class transformed from TAXD.LF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class TaxdTableDAM extends TaxdpfTableDAM {

	public TaxdTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("TAXD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLANSFX"
		             + ", INSTFROM"
		             + ", TRANTYPE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLANSFX, " +
		            "EFFDATE, " +
		            "TRANREF, " +
		            "INSTFROM, " +
		            "INSTTO, " +
		            "BILLCD, " +
		            "TRANNO, " +
		            "BASEAMT, " +
		            "TRANTYPE, " +
		            "TAXAMT01, " +
		            "TAXAMT02, " +
		            "TAXAMT03, " +
		            "TXABSIND01, " +
		            "TXABSIND02, " +
		            "TXABSIND03, " +
		            "TXTYPE01, " +
		            "TXTYPE02, " +
		            "TXTYPE03, " +
		            "POSTFLG, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLANSFX ASC, " +
		            "INSTFROM ASC, " +
		            "TRANTYPE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLANSFX DESC, " +
		            "INSTFROM DESC, " +
		            "TRANTYPE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               plansfx,
                               effdate,
                               tranref,
                               instfrom,
                               instto,
                               billcd,
                               tranno,
                               baseamt,
                               trantype,
                               taxamt01,
                               taxamt02,
                               taxamt03,
                               txabsind01,
                               txabsind02,
                               txabsind03,
                               txtype01,
                               txtype02,
                               txtype03,
                               postflg,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(229);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlansfx().toInternal()
					+ getInstfrom().toInternal()
					+ getTrantype().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, plansfx);
			what = ExternalData.chop(what, instfrom);
			what = ExternalData.chop(what, trantype);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(4);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(plansfx.toInternal());
	nonKeyFiller90.setInternal(instfrom.toInternal());
	nonKeyFiller140.setInternal(trantype.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(167);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getEffdate().toInternal()
					+ getTranref().toInternal()
					+ nonKeyFiller90.toInternal()
					+ getInstto().toInternal()
					+ getBillcd().toInternal()
					+ getTranno().toInternal()
					+ getBaseamt().toInternal()
					+ nonKeyFiller140.toInternal()
					+ getTaxamt01().toInternal()
					+ getTaxamt02().toInternal()
					+ getTaxamt03().toInternal()
					+ getTxabsind01().toInternal()
					+ getTxabsind02().toInternal()
					+ getTxabsind03().toInternal()
					+ getTxtype01().toInternal()
					+ getTxtype02().toInternal()
					+ getTxtype03().toInternal()
					+ getPostflg().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, tranref);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, instto);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, baseamt);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, taxamt01);
			what = ExternalData.chop(what, taxamt02);
			what = ExternalData.chop(what, taxamt03);
			what = ExternalData.chop(what, txabsind01);
			what = ExternalData.chop(what, txabsind02);
			what = ExternalData.chop(what, txabsind03);
			what = ExternalData.chop(what, txtype01);
			what = ExternalData.chop(what, txtype02);
			what = ExternalData.chop(what, txtype03);
			what = ExternalData.chop(what, postflg);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlansfx() {
		return plansfx;
	}
	public void setPlansfx(Object what) {
		setPlansfx(what, false);
	}
	public void setPlansfx(Object what, boolean rounded) {
		if (rounded)
			plansfx.setRounded(what);
		else
			plansfx.set(what);
	}
	public PackedDecimalData getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Object what) {
		setInstfrom(what, false);
	}
	public void setInstfrom(Object what, boolean rounded) {
		if (rounded)
			instfrom.setRounded(what);
		else
			instfrom.set(what);
	}
	public FixedLengthStringData getTrantype() {
		return trantype;
	}
	public void setTrantype(Object what) {
		trantype.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getTranref() {
		return tranref;
	}
	public void setTranref(Object what) {
		tranref.set(what);
	}	
	public PackedDecimalData getInstto() {
		return instto;
	}
	public void setInstto(Object what) {
		setInstto(what, false);
	}
	public void setInstto(Object what, boolean rounded) {
		if (rounded)
			instto.setRounded(what);
		else
			instto.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getBaseamt() {
		return baseamt;
	}
	public void setBaseamt(Object what) {
		setBaseamt(what, false);
	}
	public void setBaseamt(Object what, boolean rounded) {
		if (rounded)
			baseamt.setRounded(what);
		else
			baseamt.set(what);
	}	
	public PackedDecimalData getTaxamt01() {
		return taxamt01;
	}
	public void setTaxamt01(Object what) {
		setTaxamt01(what, false);
	}
	public void setTaxamt01(Object what, boolean rounded) {
		if (rounded)
			taxamt01.setRounded(what);
		else
			taxamt01.set(what);
	}	
	public PackedDecimalData getTaxamt02() {
		return taxamt02;
	}
	public void setTaxamt02(Object what) {
		setTaxamt02(what, false);
	}
	public void setTaxamt02(Object what, boolean rounded) {
		if (rounded)
			taxamt02.setRounded(what);
		else
			taxamt02.set(what);
	}	
	public PackedDecimalData getTaxamt03() {
		return taxamt03;
	}
	public void setTaxamt03(Object what) {
		setTaxamt03(what, false);
	}
	public void setTaxamt03(Object what, boolean rounded) {
		if (rounded)
			taxamt03.setRounded(what);
		else
			taxamt03.set(what);
	}	
	public FixedLengthStringData getTxabsind01() {
		return txabsind01;
	}
	public void setTxabsind01(Object what) {
		txabsind01.set(what);
	}	
	public FixedLengthStringData getTxabsind02() {
		return txabsind02;
	}
	public void setTxabsind02(Object what) {
		txabsind02.set(what);
	}	
	public FixedLengthStringData getTxabsind03() {
		return txabsind03;
	}
	public void setTxabsind03(Object what) {
		txabsind03.set(what);
	}	
	public FixedLengthStringData getTxtype01() {
		return txtype01;
	}
	public void setTxtype01(Object what) {
		txtype01.set(what);
	}	
	public FixedLengthStringData getTxtype02() {
		return txtype02;
	}
	public void setTxtype02(Object what) {
		txtype02.set(what);
	}	
	public FixedLengthStringData getTxtype03() {
		return txtype03;
	}
	public void setTxtype03(Object what) {
		txtype03.set(what);
	}	
	public FixedLengthStringData getPostflg() {
		return postflg;
	}
	public void setPostflg(Object what) {
		postflg.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getTxtypes() {
		return new FixedLengthStringData(txtype01.toInternal()
										+ txtype02.toInternal()
										+ txtype03.toInternal());
	}
	public void setTxtypes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTxtypes().getLength()).init(obj);
	
		what = ExternalData.chop(what, txtype01);
		what = ExternalData.chop(what, txtype02);
		what = ExternalData.chop(what, txtype03);
	}
	public FixedLengthStringData getTxtype(BaseData indx) {
		return getTxtype(indx.toInt());
	}
	public FixedLengthStringData getTxtype(int indx) {

		switch (indx) {
			case 1 : return txtype01;
			case 2 : return txtype02;
			case 3 : return txtype03;
			default: return null; // Throw error instead?
		}
	
	}
	public void setTxtype(BaseData indx, Object what) {
		setTxtype(indx.toInt(), what);
	}
	public void setTxtype(int indx, Object what) {

		switch (indx) {
			case 1 : setTxtype01(what);
					 break;
			case 2 : setTxtype02(what);
					 break;
			case 3 : setTxtype03(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getTxabsinds() {
		return new FixedLengthStringData(txabsind01.toInternal()
										+ txabsind02.toInternal()
										+ txabsind03.toInternal());
	}
	public void setTxabsinds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTxabsinds().getLength()).init(obj);
	
		what = ExternalData.chop(what, txabsind01);
		what = ExternalData.chop(what, txabsind02);
		what = ExternalData.chop(what, txabsind03);
	}
	public FixedLengthStringData getTxabsind(BaseData indx) {
		return getTxabsind(indx.toInt());
	}
	public FixedLengthStringData getTxabsind(int indx) {

		switch (indx) {
			case 1 : return txabsind01;
			case 2 : return txabsind02;
			case 3 : return txabsind03;
			default: return null; // Throw error instead?
		}
	
	}
	public void setTxabsind(BaseData indx, Object what) {
		setTxabsind(indx.toInt(), what);
	}
	public void setTxabsind(int indx, Object what) {

		switch (indx) {
			case 1 : setTxabsind01(what);
					 break;
			case 2 : setTxabsind02(what);
					 break;
			case 3 : setTxabsind03(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getTaxamts() {
		return new FixedLengthStringData(taxamt01.toInternal()
										+ taxamt02.toInternal()
										+ taxamt03.toInternal());
	}
	public void setTaxamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTaxamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, taxamt01);
		what = ExternalData.chop(what, taxamt02);
		what = ExternalData.chop(what, taxamt03);
	}
	public PackedDecimalData getTaxamt(BaseData indx) {
		return getTaxamt(indx.toInt());
	}
	public PackedDecimalData getTaxamt(int indx) {

		switch (indx) {
			case 1 : return taxamt01;
			case 2 : return taxamt02;
			case 3 : return taxamt03;
			default: return null; // Throw error instead?
		}
	
	}
	public void setTaxamt(BaseData indx, Object what) {
		setTaxamt(indx, what, false);
	}
	public void setTaxamt(BaseData indx, Object what, boolean rounded) {
		setTaxamt(indx.toInt(), what, rounded);
	}
	public void setTaxamt(int indx, Object what) {
		setTaxamt(indx, what, false);
	}
	public void setTaxamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setTaxamt01(what, rounded);
					 break;
			case 2 : setTaxamt02(what, rounded);
					 break;
			case 3 : setTaxamt03(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		plansfx.clear();
		instfrom.clear();
		trantype.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		effdate.clear();
		tranref.clear();
		nonKeyFiller90.clear();
		instto.clear();
		billcd.clear();
		tranno.clear();
		baseamt.clear();
		nonKeyFiller140.clear();
		taxamt01.clear();
		taxamt02.clear();
		taxamt03.clear();
		txabsind01.clear();
		txabsind02.clear();
		txabsind03.clear();
		txtype01.clear();
		txtype02.clear();
		txtype03.clear();
		postflg.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}