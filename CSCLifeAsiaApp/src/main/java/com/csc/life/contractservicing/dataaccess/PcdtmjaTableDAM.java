package com.csc.life.contractservicing.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PcdtmjaTableDAM.java
 * Date: Sun, 30 Aug 2009 03:44:19
 * Class transformed from PCDTMJA.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PcdtmjaTableDAM extends PcdtpfTableDAM {

	public PcdtmjaTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("PCDTMJA");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", PLNSFX"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "PLNSFX, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "TRANNO, " +
		            "AGNTNUM01, " +
		            "AGNTNUM02, " +
		            "AGNTNUM03, " +
		            "AGNTNUM04, " +
		            "AGNTNUM05, " +
		            "AGNTNUM06, " +
		            "AGNTNUM07, " +
		            "AGNTNUM08, " +
		            "AGNTNUM09, " +
		            "AGNTNUM10, " +
		            "SPLITC01, " +
		            "SPLITC02, " +
		            "SPLITC03, " +
		            "SPLITC04, " +
		            "SPLITC05, " +
		            "SPLITC06, " +
		            "SPLITC07, " +
		            "SPLITC08, " +
		            "SPLITC09, " +
		            "SPLITC10, " +
		            "INSTPREM, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "PLNSFX ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "PLNSFX DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               planSuffix,
                               life,
                               coverage,
                               rider,
                               tranno,
                               agntnum01,
                               agntnum02,
                               agntnum03,
                               agntnum04,
                               agntnum05,
                               agntnum06,
                               agntnum07,
                               agntnum08,
                               agntnum09,
                               agntnum10,
                               splitBcomm01,
                               splitBcomm02,
                               splitBcomm03,
                               splitBcomm04,
                               splitBcomm05,
                               splitBcomm06,
                               splitBcomm07,
                               splitBcomm08,
                               splitBcomm09,
                               splitBcomm10,
                               instprem,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(43);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getPlanSuffix().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(planSuffix.toInternal());
	nonKeyFiller4.setInternal(life.toInternal());
	nonKeyFiller5.setInternal(coverage.toInternal());
	nonKeyFiller6.setInternal(rider.toInternal());
	nonKeyFiller7.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(186);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ getAgntnum01().toInternal()
					+ getAgntnum02().toInternal()
					+ getAgntnum03().toInternal()
					+ getAgntnum04().toInternal()
					+ getAgntnum05().toInternal()
					+ getAgntnum06().toInternal()
					+ getAgntnum07().toInternal()
					+ getAgntnum08().toInternal()
					+ getAgntnum09().toInternal()
					+ getAgntnum10().toInternal()
					+ getSplitBcomm01().toInternal()
					+ getSplitBcomm02().toInternal()
					+ getSplitBcomm03().toInternal()
					+ getSplitBcomm04().toInternal()
					+ getSplitBcomm05().toInternal()
					+ getSplitBcomm06().toInternal()
					+ getSplitBcomm07().toInternal()
					+ getSplitBcomm08().toInternal()
					+ getSplitBcomm09().toInternal()
					+ getSplitBcomm10().toInternal()
					+ getInstprem().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, agntnum01);
			what = ExternalData.chop(what, agntnum02);
			what = ExternalData.chop(what, agntnum03);
			what = ExternalData.chop(what, agntnum04);
			what = ExternalData.chop(what, agntnum05);
			what = ExternalData.chop(what, agntnum06);
			what = ExternalData.chop(what, agntnum07);
			what = ExternalData.chop(what, agntnum08);
			what = ExternalData.chop(what, agntnum09);
			what = ExternalData.chop(what, agntnum10);
			what = ExternalData.chop(what, splitBcomm01);
			what = ExternalData.chop(what, splitBcomm02);
			what = ExternalData.chop(what, splitBcomm03);
			what = ExternalData.chop(what, splitBcomm04);
			what = ExternalData.chop(what, splitBcomm05);
			what = ExternalData.chop(what, splitBcomm06);
			what = ExternalData.chop(what, splitBcomm07);
			what = ExternalData.chop(what, splitBcomm08);
			what = ExternalData.chop(what, splitBcomm09);
			what = ExternalData.chop(what, splitBcomm10);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getAgntnum01() {
		return agntnum01;
	}
	public void setAgntnum01(Object what) {
		agntnum01.set(what);
	}	
	public FixedLengthStringData getAgntnum02() {
		return agntnum02;
	}
	public void setAgntnum02(Object what) {
		agntnum02.set(what);
	}	
	public FixedLengthStringData getAgntnum03() {
		return agntnum03;
	}
	public void setAgntnum03(Object what) {
		agntnum03.set(what);
	}	
	public FixedLengthStringData getAgntnum04() {
		return agntnum04;
	}
	public void setAgntnum04(Object what) {
		agntnum04.set(what);
	}	
	public FixedLengthStringData getAgntnum05() {
		return agntnum05;
	}
	public void setAgntnum05(Object what) {
		agntnum05.set(what);
	}	
	public FixedLengthStringData getAgntnum06() {
		return agntnum06;
	}
	public void setAgntnum06(Object what) {
		agntnum06.set(what);
	}	
	public FixedLengthStringData getAgntnum07() {
		return agntnum07;
	}
	public void setAgntnum07(Object what) {
		agntnum07.set(what);
	}	
	public FixedLengthStringData getAgntnum08() {
		return agntnum08;
	}
	public void setAgntnum08(Object what) {
		agntnum08.set(what);
	}	
	public FixedLengthStringData getAgntnum09() {
		return agntnum09;
	}
	public void setAgntnum09(Object what) {
		agntnum09.set(what);
	}	
	public FixedLengthStringData getAgntnum10() {
		return agntnum10;
	}
	public void setAgntnum10(Object what) {
		agntnum10.set(what);
	}	
	public PackedDecimalData getSplitBcomm01() {
		return splitBcomm01;
	}
	public void setSplitBcomm01(Object what) {
		setSplitBcomm01(what, false);
	}
	public void setSplitBcomm01(Object what, boolean rounded) {
		if (rounded)
			splitBcomm01.setRounded(what);
		else
			splitBcomm01.set(what);
	}	
	public PackedDecimalData getSplitBcomm02() {
		return splitBcomm02;
	}
	public void setSplitBcomm02(Object what) {
		setSplitBcomm02(what, false);
	}
	public void setSplitBcomm02(Object what, boolean rounded) {
		if (rounded)
			splitBcomm02.setRounded(what);
		else
			splitBcomm02.set(what);
	}	
	public PackedDecimalData getSplitBcomm03() {
		return splitBcomm03;
	}
	public void setSplitBcomm03(Object what) {
		setSplitBcomm03(what, false);
	}
	public void setSplitBcomm03(Object what, boolean rounded) {
		if (rounded)
			splitBcomm03.setRounded(what);
		else
			splitBcomm03.set(what);
	}	
	public PackedDecimalData getSplitBcomm04() {
		return splitBcomm04;
	}
	public void setSplitBcomm04(Object what) {
		setSplitBcomm04(what, false);
	}
	public void setSplitBcomm04(Object what, boolean rounded) {
		if (rounded)
			splitBcomm04.setRounded(what);
		else
			splitBcomm04.set(what);
	}	
	public PackedDecimalData getSplitBcomm05() {
		return splitBcomm05;
	}
	public void setSplitBcomm05(Object what) {
		setSplitBcomm05(what, false);
	}
	public void setSplitBcomm05(Object what, boolean rounded) {
		if (rounded)
			splitBcomm05.setRounded(what);
		else
			splitBcomm05.set(what);
	}	
	public PackedDecimalData getSplitBcomm06() {
		return splitBcomm06;
	}
	public void setSplitBcomm06(Object what) {
		setSplitBcomm06(what, false);
	}
	public void setSplitBcomm06(Object what, boolean rounded) {
		if (rounded)
			splitBcomm06.setRounded(what);
		else
			splitBcomm06.set(what);
	}	
	public PackedDecimalData getSplitBcomm07() {
		return splitBcomm07;
	}
	public void setSplitBcomm07(Object what) {
		setSplitBcomm07(what, false);
	}
	public void setSplitBcomm07(Object what, boolean rounded) {
		if (rounded)
			splitBcomm07.setRounded(what);
		else
			splitBcomm07.set(what);
	}	
	public PackedDecimalData getSplitBcomm08() {
		return splitBcomm08;
	}
	public void setSplitBcomm08(Object what) {
		setSplitBcomm08(what, false);
	}
	public void setSplitBcomm08(Object what, boolean rounded) {
		if (rounded)
			splitBcomm08.setRounded(what);
		else
			splitBcomm08.set(what);
	}	
	public PackedDecimalData getSplitBcomm09() {
		return splitBcomm09;
	}
	public void setSplitBcomm09(Object what) {
		setSplitBcomm09(what, false);
	}
	public void setSplitBcomm09(Object what, boolean rounded) {
		if (rounded)
			splitBcomm09.setRounded(what);
		else
			splitBcomm09.set(what);
	}	
	public PackedDecimalData getSplitBcomm10() {
		return splitBcomm10;
	}
	public void setSplitBcomm10(Object what) {
		setSplitBcomm10(what, false);
	}
	public void setSplitBcomm10(Object what, boolean rounded) {
		if (rounded)
			splitBcomm10.setRounded(what);
		else
			splitBcomm10.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSplitcs() {
		return new FixedLengthStringData(splitBcomm01.toInternal()
										+ splitBcomm02.toInternal()
										+ splitBcomm03.toInternal()
										+ splitBcomm04.toInternal()
										+ splitBcomm05.toInternal()
										+ splitBcomm06.toInternal()
										+ splitBcomm07.toInternal()
										+ splitBcomm08.toInternal()
										+ splitBcomm09.toInternal()
										+ splitBcomm10.toInternal());
	}
	public void setSplitcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSplitcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, splitBcomm01);
		what = ExternalData.chop(what, splitBcomm02);
		what = ExternalData.chop(what, splitBcomm03);
		what = ExternalData.chop(what, splitBcomm04);
		what = ExternalData.chop(what, splitBcomm05);
		what = ExternalData.chop(what, splitBcomm06);
		what = ExternalData.chop(what, splitBcomm07);
		what = ExternalData.chop(what, splitBcomm08);
		what = ExternalData.chop(what, splitBcomm09);
		what = ExternalData.chop(what, splitBcomm10);
	}
	public PackedDecimalData getSplitc(BaseData indx) {
		return getSplitc(indx.toInt());
	}
	public PackedDecimalData getSplitc(int indx) {

		switch (indx) {
			case 1 : return splitBcomm01;
			case 2 : return splitBcomm02;
			case 3 : return splitBcomm03;
			case 4 : return splitBcomm04;
			case 5 : return splitBcomm05;
			case 6 : return splitBcomm06;
			case 7 : return splitBcomm07;
			case 8 : return splitBcomm08;
			case 9 : return splitBcomm09;
			case 10 : return splitBcomm10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSplitc(BaseData indx, Object what) {
		setSplitc(indx, what, false);
	}
	public void setSplitc(BaseData indx, Object what, boolean rounded) {
		setSplitc(indx.toInt(), what, rounded);
	}
	public void setSplitc(int indx, Object what) {
		setSplitc(indx, what, false);
	}
	public void setSplitc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSplitBcomm01(what, rounded);
					 break;
			case 2 : setSplitBcomm02(what, rounded);
					 break;
			case 3 : setSplitBcomm03(what, rounded);
					 break;
			case 4 : setSplitBcomm04(what, rounded);
					 break;
			case 5 : setSplitBcomm05(what, rounded);
					 break;
			case 6 : setSplitBcomm06(what, rounded);
					 break;
			case 7 : setSplitBcomm07(what, rounded);
					 break;
			case 8 : setSplitBcomm08(what, rounded);
					 break;
			case 9 : setSplitBcomm09(what, rounded);
					 break;
			case 10 : setSplitBcomm10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAgntnums() {
		return new FixedLengthStringData(agntnum01.toInternal()
										+ agntnum02.toInternal()
										+ agntnum03.toInternal()
										+ agntnum04.toInternal()
										+ agntnum05.toInternal()
										+ agntnum06.toInternal()
										+ agntnum07.toInternal()
										+ agntnum08.toInternal()
										+ agntnum09.toInternal()
										+ agntnum10.toInternal());
	}
	public void setAgntnums(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAgntnums().getLength()).init(obj);
	
		what = ExternalData.chop(what, agntnum01);
		what = ExternalData.chop(what, agntnum02);
		what = ExternalData.chop(what, agntnum03);
		what = ExternalData.chop(what, agntnum04);
		what = ExternalData.chop(what, agntnum05);
		what = ExternalData.chop(what, agntnum06);
		what = ExternalData.chop(what, agntnum07);
		what = ExternalData.chop(what, agntnum08);
		what = ExternalData.chop(what, agntnum09);
		what = ExternalData.chop(what, agntnum10);
	}
	public FixedLengthStringData getAgntnum(BaseData indx) {
		return getAgntnum(indx.toInt());
	}
	public FixedLengthStringData getAgntnum(int indx) {

		switch (indx) {
			case 1 : return agntnum01;
			case 2 : return agntnum02;
			case 3 : return agntnum03;
			case 4 : return agntnum04;
			case 5 : return agntnum05;
			case 6 : return agntnum06;
			case 7 : return agntnum07;
			case 8 : return agntnum08;
			case 9 : return agntnum09;
			case 10 : return agntnum10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAgntnum(BaseData indx, Object what) {
		setAgntnum(indx.toInt(), what);
	}
	public void setAgntnum(int indx, Object what) {

		switch (indx) {
			case 1 : setAgntnum01(what);
					 break;
			case 2 : setAgntnum02(what);
					 break;
			case 3 : setAgntnum03(what);
					 break;
			case 4 : setAgntnum04(what);
					 break;
			case 5 : setAgntnum05(what);
					 break;
			case 6 : setAgntnum06(what);
					 break;
			case 7 : setAgntnum07(what);
					 break;
			case 8 : setAgntnum08(what);
					 break;
			case 9 : setAgntnum09(what);
					 break;
			case 10 : setAgntnum10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		planSuffix.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		agntnum01.clear();
		agntnum02.clear();
		agntnum03.clear();
		agntnum04.clear();
		agntnum05.clear();
		agntnum06.clear();
		agntnum07.clear();
		agntnum08.clear();
		agntnum09.clear();
		agntnum10.clear();
		splitBcomm01.clear();
		splitBcomm02.clear();
		splitBcomm03.clear();
		splitBcomm04.clear();
		splitBcomm05.clear();
		splitBcomm06.clear();
		splitBcomm07.clear();
		splitBcomm08.clear();
		splitBcomm09.clear();
		splitBcomm10.clear();
		instprem.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}