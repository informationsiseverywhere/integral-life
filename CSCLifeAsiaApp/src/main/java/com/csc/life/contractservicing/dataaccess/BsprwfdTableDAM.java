package com.csc.life.contractservicing.dataaccess;

import com.csc.smart.dataaccess.BsprpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BsprwfdTableDAM.java
 * Date: Sun, 30 Aug 2009 03:30:28
 * Class transformed from BSPRWFD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BsprwfdTableDAM extends BsprpfTableDAM {

	public BsprwfdTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("BSPRWFD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "BSCHEDNAM"
		             + ", BSCHEDNUM"
		             + ", COMPANY"
		             + ", BPROCESNAM"
		             + ", BPRCOCCNO";
		
		QUALIFIEDCOLUMNS = 
		            "BSCHEDNAM, " +
		            "BSCHEDNUM, " +
		            "COMPANY, " +
		            "BPROCESNAM, " +
		            "BPRCOCCNO, " +
		            "BPRCSTATUS, " +
		            "BSHDTHRDNO, " +
		            "BSCHEDPRTY, " +
		            "BPDATMSTRT, " +
		            "BPDATMLLOG, " +
		            "BTOTPRCTIM, " +
		            "BPCYCLCNT, " +
		            "BPNOTPRCNT, " +
		            "BPRCERRCNT, " +
		            "FSUCO, " +
		            "CMTRESTART, " +
		            "BCONTOT01, " +
		            "BCONTOT02, " +
		            "BCONTOT03, " +
		            "BCONTOT04, " +
		            "BCONTOT05, " +
		            "BCONTOT06, " +
		            "BCONTOT07, " +
		            "BCONTOT08, " +
		            "BCONTOT09, " +
		            "BCONTOT10, " +
		            "BCONTOT11, " +
		            "BCONTOT12, " +
		            "BCONTOT13, " +
		            "BCONTOT14, " +
		            "BCONTOT15, " +
		            "BCONTOT16, " +
		            "BCONTOT17, " +
		            "BCONTOT18, " +
		            "BCONTOT19, " +
		            "BCONTOT20, " +
		            "BCONTOT21, " +
		            "BCONTOT22, " +
		            "BCONTOT23, " +
		            "BCONTOT24, " +
		            "BCONTOT25, " +
		            "BCONTOT26, " +
		            "BCONTOT27, " +
		            "BCONTOT28, " +
		            "BCONTOT29, " +
		            "BCONTOT30, " +
		            "BCONTOT31, " +
		            "BCONTOT32, " +
		            "BCONTOT33, " +
		            "BCONTOT34, " +
		            "BCONTOT35, " +
		            "BCONTOT36, " +
		            "BCONTOT37, " +
		            "BCONTOT38, " +
		            "BCONTOT39, " +
		            "BCONTOT40, " +
		            "BCONTOT41, " +
		            "BCONTOT42, " +
		            "BCONTOT43, " +
		            "BCONTOT44, " +
		            "BCONTOT45, " +
		            "BCONTOT46, " +
		            "BCONTOT47, " +
		            "BCONTOT48, " +
		            "BCONTOT49, " +
		            "BCONTOT50, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "BSCHEDNAM ASC, " +
		            "BSCHEDNUM ASC, " +
		            "COMPANY ASC, " +
		            "BPROCESNAM ASC, " +
		            "BPRCOCCNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "BSCHEDNAM DESC, " +
		            "BSCHEDNUM DESC, " +
		            "COMPANY DESC, " +
		            "BPROCESNAM DESC, " +
		            "BPRCOCCNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               scheduleName,
                               scheduleNumber,
                               company,
                               processName,
                               processOccNum,
                               processStatus,
                               scheduleThreadNo,
                               schedulingPriority,
                               datimeStarted,
                               datimeLogged,
                               totalElapsedTime,
                               cycleCount,
                               notProcessedCount,
                               errorCount,
                               fsuco,
                               cmtrestart,
                               bcontot01,
                               bcontot02,
                               bcontot03,
                               bcontot04,
                               bcontot05,
                               bcontot06,
                               bcontot07,
                               bcontot08,
                               bcontot09,
                               bcontot10,
                               bcontot11,
                               bcontot12,
                               bcontot13,
                               bcontot14,
                               bcontot15,
                               bcontot16,
                               bcontot17,
                               bcontot18,
                               bcontot19,
                               bcontot20,
                               bcontot21,
                               bcontot22,
                               bcontot23,
                               bcontot24,
                               bcontot25,
                               bcontot26,
                               bcontot27,
                               bcontot28,
                               bcontot29,
                               bcontot30,
                               bcontot31,
                               bcontot32,
                               bcontot33,
                               bcontot34,
                               bcontot35,
                               bcontot36,
                               bcontot37,
                               bcontot38,
                               bcontot39,
                               bcontot40,
                               bcontot41,
                               bcontot42,
                               bcontot43,
                               bcontot44,
                               bcontot45,
                               bcontot46,
                               bcontot47,
                               bcontot48,
                               bcontot49,
                               bcontot50,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(36);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getScheduleName().toInternal()
					+ getScheduleNumber().toInternal()
					+ getCompany().toInternal()
					+ getProcessName().toInternal()
					+ getProcessOccNum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, scheduleName);
			what = ExternalData.chop(what, scheduleNumber);
			what = ExternalData.chop(what, company);
			what = ExternalData.chop(what, processName);
			what = ExternalData.chop(what, processOccNum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(10);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(10);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(scheduleName.toInternal());
	nonKeyFiller20.setInternal(scheduleNumber.toInternal());
	nonKeyFiller30.setInternal(company.toInternal());
	nonKeyFiller40.setInternal(processName.toInternal());
	nonKeyFiller50.setInternal(processOccNum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(657);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getProcessStatus().toInternal()
					+ getScheduleThreadNo().toInternal()
					+ getSchedulingPriority().toInternal()
					+ getDatimeStarted().toInternal()
					+ getDatimeLogged().toInternal()
					+ getTotalElapsedTime().toInternal()
					+ getCycleCount().toInternal()
					+ getNotProcessedCount().toInternal()
					+ getErrorCount().toInternal()
					+ getFsuco().toInternal()
					+ getCmtrestart().toInternal()
					+ getBcontot01().toInternal()
					+ getBcontot02().toInternal()
					+ getBcontot03().toInternal()
					+ getBcontot04().toInternal()
					+ getBcontot05().toInternal()
					+ getBcontot06().toInternal()
					+ getBcontot07().toInternal()
					+ getBcontot08().toInternal()
					+ getBcontot09().toInternal()
					+ getBcontot10().toInternal()
					+ getBcontot11().toInternal()
					+ getBcontot12().toInternal()
					+ getBcontot13().toInternal()
					+ getBcontot14().toInternal()
					+ getBcontot15().toInternal()
					+ getBcontot16().toInternal()
					+ getBcontot17().toInternal()
					+ getBcontot18().toInternal()
					+ getBcontot19().toInternal()
					+ getBcontot20().toInternal()
					+ getBcontot21().toInternal()
					+ getBcontot22().toInternal()
					+ getBcontot23().toInternal()
					+ getBcontot24().toInternal()
					+ getBcontot25().toInternal()
					+ getBcontot26().toInternal()
					+ getBcontot27().toInternal()
					+ getBcontot28().toInternal()
					+ getBcontot29().toInternal()
					+ getBcontot30().toInternal()
					+ getBcontot31().toInternal()
					+ getBcontot32().toInternal()
					+ getBcontot33().toInternal()
					+ getBcontot34().toInternal()
					+ getBcontot35().toInternal()
					+ getBcontot36().toInternal()
					+ getBcontot37().toInternal()
					+ getBcontot38().toInternal()
					+ getBcontot39().toInternal()
					+ getBcontot40().toInternal()
					+ getBcontot41().toInternal()
					+ getBcontot42().toInternal()
					+ getBcontot43().toInternal()
					+ getBcontot44().toInternal()
					+ getBcontot45().toInternal()
					+ getBcontot46().toInternal()
					+ getBcontot47().toInternal()
					+ getBcontot48().toInternal()
					+ getBcontot49().toInternal()
					+ getBcontot50().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, processStatus);
			what = ExternalData.chop(what, scheduleThreadNo);
			what = ExternalData.chop(what, schedulingPriority);
			what = ExternalData.chop(what, datimeStarted);
			what = ExternalData.chop(what, datimeLogged);
			what = ExternalData.chop(what, totalElapsedTime);
			what = ExternalData.chop(what, cycleCount);
			what = ExternalData.chop(what, notProcessedCount);
			what = ExternalData.chop(what, errorCount);
			what = ExternalData.chop(what, fsuco);
			what = ExternalData.chop(what, cmtrestart);
			what = ExternalData.chop(what, bcontot01);
			what = ExternalData.chop(what, bcontot02);
			what = ExternalData.chop(what, bcontot03);
			what = ExternalData.chop(what, bcontot04);
			what = ExternalData.chop(what, bcontot05);
			what = ExternalData.chop(what, bcontot06);
			what = ExternalData.chop(what, bcontot07);
			what = ExternalData.chop(what, bcontot08);
			what = ExternalData.chop(what, bcontot09);
			what = ExternalData.chop(what, bcontot10);
			what = ExternalData.chop(what, bcontot11);
			what = ExternalData.chop(what, bcontot12);
			what = ExternalData.chop(what, bcontot13);
			what = ExternalData.chop(what, bcontot14);
			what = ExternalData.chop(what, bcontot15);
			what = ExternalData.chop(what, bcontot16);
			what = ExternalData.chop(what, bcontot17);
			what = ExternalData.chop(what, bcontot18);
			what = ExternalData.chop(what, bcontot19);
			what = ExternalData.chop(what, bcontot20);
			what = ExternalData.chop(what, bcontot21);
			what = ExternalData.chop(what, bcontot22);
			what = ExternalData.chop(what, bcontot23);
			what = ExternalData.chop(what, bcontot24);
			what = ExternalData.chop(what, bcontot25);
			what = ExternalData.chop(what, bcontot26);
			what = ExternalData.chop(what, bcontot27);
			what = ExternalData.chop(what, bcontot28);
			what = ExternalData.chop(what, bcontot29);
			what = ExternalData.chop(what, bcontot30);
			what = ExternalData.chop(what, bcontot31);
			what = ExternalData.chop(what, bcontot32);
			what = ExternalData.chop(what, bcontot33);
			what = ExternalData.chop(what, bcontot34);
			what = ExternalData.chop(what, bcontot35);
			what = ExternalData.chop(what, bcontot36);
			what = ExternalData.chop(what, bcontot37);
			what = ExternalData.chop(what, bcontot38);
			what = ExternalData.chop(what, bcontot39);
			what = ExternalData.chop(what, bcontot40);
			what = ExternalData.chop(what, bcontot41);
			what = ExternalData.chop(what, bcontot42);
			what = ExternalData.chop(what, bcontot43);
			what = ExternalData.chop(what, bcontot44);
			what = ExternalData.chop(what, bcontot45);
			what = ExternalData.chop(what, bcontot46);
			what = ExternalData.chop(what, bcontot47);
			what = ExternalData.chop(what, bcontot48);
			what = ExternalData.chop(what, bcontot49);
			what = ExternalData.chop(what, bcontot50);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getScheduleName() {
		return scheduleName;
	}
	public void setScheduleName(Object what) {
		scheduleName.set(what);
	}
	public PackedDecimalData getScheduleNumber() {
		return scheduleNumber;
	}
	public void setScheduleNumber(Object what) {
		setScheduleNumber(what, false);
	}
	public void setScheduleNumber(Object what, boolean rounded) {
		if (rounded)
			scheduleNumber.setRounded(what);
		else
			scheduleNumber.set(what);
	}
	public FixedLengthStringData getCompany() {
		return company;
	}
	public void setCompany(Object what) {
		company.set(what);
	}
	public FixedLengthStringData getProcessName() {
		return processName;
	}
	public void setProcessName(Object what) {
		processName.set(what);
	}
	public PackedDecimalData getProcessOccNum() {
		return processOccNum;
	}
	public void setProcessOccNum(Object what) {
		setProcessOccNum(what, false);
	}
	public void setProcessOccNum(Object what, boolean rounded) {
		if (rounded)
			processOccNum.setRounded(what);
		else
			processOccNum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getProcessStatus() {
		return processStatus;
	}
	public void setProcessStatus(Object what) {
		processStatus.set(what);
	}	
	public PackedDecimalData getScheduleThreadNo() {
		return scheduleThreadNo;
	}
	public void setScheduleThreadNo(Object what) {
		setScheduleThreadNo(what, false);
	}
	public void setScheduleThreadNo(Object what, boolean rounded) {
		if (rounded)
			scheduleThreadNo.setRounded(what);
		else
			scheduleThreadNo.set(what);
	}	
	public PackedDecimalData getSchedulingPriority() {
		return schedulingPriority;
	}
	public void setSchedulingPriority(Object what) {
		setSchedulingPriority(what, false);
	}
	public void setSchedulingPriority(Object what, boolean rounded) {
		if (rounded)
			schedulingPriority.setRounded(what);
		else
			schedulingPriority.set(what);
	}	
	public FixedLengthStringData getDatimeStarted() {
		return datimeStarted;
	}
	public void setDatimeStarted(Object what) {
		datimeStarted.set(what);
	}	
	public FixedLengthStringData getDatimeLogged() {
		return datimeLogged;
	}
	public void setDatimeLogged(Object what) {
		datimeLogged.set(what);
	}	
	public PackedDecimalData getTotalElapsedTime() {
		return totalElapsedTime;
	}
	public void setTotalElapsedTime(Object what) {
		setTotalElapsedTime(what, false);
	}
	public void setTotalElapsedTime(Object what, boolean rounded) {
		if (rounded)
			totalElapsedTime.setRounded(what);
		else
			totalElapsedTime.set(what);
	}	
	public PackedDecimalData getCycleCount() {
		return cycleCount;
	}
	public void setCycleCount(Object what) {
		setCycleCount(what, false);
	}
	public void setCycleCount(Object what, boolean rounded) {
		if (rounded)
			cycleCount.setRounded(what);
		else
			cycleCount.set(what);
	}	
	public PackedDecimalData getNotProcessedCount() {
		return notProcessedCount;
	}
	public void setNotProcessedCount(Object what) {
		setNotProcessedCount(what, false);
	}
	public void setNotProcessedCount(Object what, boolean rounded) {
		if (rounded)
			notProcessedCount.setRounded(what);
		else
			notProcessedCount.set(what);
	}	
	public PackedDecimalData getErrorCount() {
		return errorCount;
	}
	public void setErrorCount(Object what) {
		setErrorCount(what, false);
	}
	public void setErrorCount(Object what, boolean rounded) {
		if (rounded)
			errorCount.setRounded(what);
		else
			errorCount.set(what);
	}	
	public FixedLengthStringData getFsuco() {
		return fsuco;
	}
	public void setFsuco(Object what) {
		fsuco.set(what);
	}	
	public PackedDecimalData getCmtrestart() {
		return cmtrestart;
	}
	public void setCmtrestart(Object what) {
		setCmtrestart(what, false);
	}
	public void setCmtrestart(Object what, boolean rounded) {
		if (rounded)
			cmtrestart.setRounded(what);
		else
			cmtrestart.set(what);
	}	
	public PackedDecimalData getBcontot01() {
		return bcontot01;
	}
	public void setBcontot01(Object what) {
		setBcontot01(what, false);
	}
	public void setBcontot01(Object what, boolean rounded) {
		if (rounded)
			bcontot01.setRounded(what);
		else
			bcontot01.set(what);
	}	
	public PackedDecimalData getBcontot02() {
		return bcontot02;
	}
	public void setBcontot02(Object what) {
		setBcontot02(what, false);
	}
	public void setBcontot02(Object what, boolean rounded) {
		if (rounded)
			bcontot02.setRounded(what);
		else
			bcontot02.set(what);
	}	
	public PackedDecimalData getBcontot03() {
		return bcontot03;
	}
	public void setBcontot03(Object what) {
		setBcontot03(what, false);
	}
	public void setBcontot03(Object what, boolean rounded) {
		if (rounded)
			bcontot03.setRounded(what);
		else
			bcontot03.set(what);
	}	
	public PackedDecimalData getBcontot04() {
		return bcontot04;
	}
	public void setBcontot04(Object what) {
		setBcontot04(what, false);
	}
	public void setBcontot04(Object what, boolean rounded) {
		if (rounded)
			bcontot04.setRounded(what);
		else
			bcontot04.set(what);
	}	
	public PackedDecimalData getBcontot05() {
		return bcontot05;
	}
	public void setBcontot05(Object what) {
		setBcontot05(what, false);
	}
	public void setBcontot05(Object what, boolean rounded) {
		if (rounded)
			bcontot05.setRounded(what);
		else
			bcontot05.set(what);
	}	
	public PackedDecimalData getBcontot06() {
		return bcontot06;
	}
	public void setBcontot06(Object what) {
		setBcontot06(what, false);
	}
	public void setBcontot06(Object what, boolean rounded) {
		if (rounded)
			bcontot06.setRounded(what);
		else
			bcontot06.set(what);
	}	
	public PackedDecimalData getBcontot07() {
		return bcontot07;
	}
	public void setBcontot07(Object what) {
		setBcontot07(what, false);
	}
	public void setBcontot07(Object what, boolean rounded) {
		if (rounded)
			bcontot07.setRounded(what);
		else
			bcontot07.set(what);
	}	
	public PackedDecimalData getBcontot08() {
		return bcontot08;
	}
	public void setBcontot08(Object what) {
		setBcontot08(what, false);
	}
	public void setBcontot08(Object what, boolean rounded) {
		if (rounded)
			bcontot08.setRounded(what);
		else
			bcontot08.set(what);
	}	
	public PackedDecimalData getBcontot09() {
		return bcontot09;
	}
	public void setBcontot09(Object what) {
		setBcontot09(what, false);
	}
	public void setBcontot09(Object what, boolean rounded) {
		if (rounded)
			bcontot09.setRounded(what);
		else
			bcontot09.set(what);
	}	
	public PackedDecimalData getBcontot10() {
		return bcontot10;
	}
	public void setBcontot10(Object what) {
		setBcontot10(what, false);
	}
	public void setBcontot10(Object what, boolean rounded) {
		if (rounded)
			bcontot10.setRounded(what);
		else
			bcontot10.set(what);
	}	
	public PackedDecimalData getBcontot11() {
		return bcontot11;
	}
	public void setBcontot11(Object what) {
		setBcontot11(what, false);
	}
	public void setBcontot11(Object what, boolean rounded) {
		if (rounded)
			bcontot11.setRounded(what);
		else
			bcontot11.set(what);
	}	
	public PackedDecimalData getBcontot12() {
		return bcontot12;
	}
	public void setBcontot12(Object what) {
		setBcontot12(what, false);
	}
	public void setBcontot12(Object what, boolean rounded) {
		if (rounded)
			bcontot12.setRounded(what);
		else
			bcontot12.set(what);
	}	
	public PackedDecimalData getBcontot13() {
		return bcontot13;
	}
	public void setBcontot13(Object what) {
		setBcontot13(what, false);
	}
	public void setBcontot13(Object what, boolean rounded) {
		if (rounded)
			bcontot13.setRounded(what);
		else
			bcontot13.set(what);
	}	
	public PackedDecimalData getBcontot14() {
		return bcontot14;
	}
	public void setBcontot14(Object what) {
		setBcontot14(what, false);
	}
	public void setBcontot14(Object what, boolean rounded) {
		if (rounded)
			bcontot14.setRounded(what);
		else
			bcontot14.set(what);
	}	
	public PackedDecimalData getBcontot15() {
		return bcontot15;
	}
	public void setBcontot15(Object what) {
		setBcontot15(what, false);
	}
	public void setBcontot15(Object what, boolean rounded) {
		if (rounded)
			bcontot15.setRounded(what);
		else
			bcontot15.set(what);
	}	
	public PackedDecimalData getBcontot16() {
		return bcontot16;
	}
	public void setBcontot16(Object what) {
		setBcontot16(what, false);
	}
	public void setBcontot16(Object what, boolean rounded) {
		if (rounded)
			bcontot16.setRounded(what);
		else
			bcontot16.set(what);
	}	
	public PackedDecimalData getBcontot17() {
		return bcontot17;
	}
	public void setBcontot17(Object what) {
		setBcontot17(what, false);
	}
	public void setBcontot17(Object what, boolean rounded) {
		if (rounded)
			bcontot17.setRounded(what);
		else
			bcontot17.set(what);
	}	
	public PackedDecimalData getBcontot18() {
		return bcontot18;
	}
	public void setBcontot18(Object what) {
		setBcontot18(what, false);
	}
	public void setBcontot18(Object what, boolean rounded) {
		if (rounded)
			bcontot18.setRounded(what);
		else
			bcontot18.set(what);
	}	
	public PackedDecimalData getBcontot19() {
		return bcontot19;
	}
	public void setBcontot19(Object what) {
		setBcontot19(what, false);
	}
	public void setBcontot19(Object what, boolean rounded) {
		if (rounded)
			bcontot19.setRounded(what);
		else
			bcontot19.set(what);
	}	
	public PackedDecimalData getBcontot20() {
		return bcontot20;
	}
	public void setBcontot20(Object what) {
		setBcontot20(what, false);
	}
	public void setBcontot20(Object what, boolean rounded) {
		if (rounded)
			bcontot20.setRounded(what);
		else
			bcontot20.set(what);
	}	
	public PackedDecimalData getBcontot21() {
		return bcontot21;
	}
	public void setBcontot21(Object what) {
		setBcontot21(what, false);
	}
	public void setBcontot21(Object what, boolean rounded) {
		if (rounded)
			bcontot21.setRounded(what);
		else
			bcontot21.set(what);
	}	
	public PackedDecimalData getBcontot22() {
		return bcontot22;
	}
	public void setBcontot22(Object what) {
		setBcontot22(what, false);
	}
	public void setBcontot22(Object what, boolean rounded) {
		if (rounded)
			bcontot22.setRounded(what);
		else
			bcontot22.set(what);
	}	
	public PackedDecimalData getBcontot23() {
		return bcontot23;
	}
	public void setBcontot23(Object what) {
		setBcontot23(what, false);
	}
	public void setBcontot23(Object what, boolean rounded) {
		if (rounded)
			bcontot23.setRounded(what);
		else
			bcontot23.set(what);
	}	
	public PackedDecimalData getBcontot24() {
		return bcontot24;
	}
	public void setBcontot24(Object what) {
		setBcontot24(what, false);
	}
	public void setBcontot24(Object what, boolean rounded) {
		if (rounded)
			bcontot24.setRounded(what);
		else
			bcontot24.set(what);
	}	
	public PackedDecimalData getBcontot25() {
		return bcontot25;
	}
	public void setBcontot25(Object what) {
		setBcontot25(what, false);
	}
	public void setBcontot25(Object what, boolean rounded) {
		if (rounded)
			bcontot25.setRounded(what);
		else
			bcontot25.set(what);
	}	
	public PackedDecimalData getBcontot26() {
		return bcontot26;
	}
	public void setBcontot26(Object what) {
		setBcontot26(what, false);
	}
	public void setBcontot26(Object what, boolean rounded) {
		if (rounded)
			bcontot26.setRounded(what);
		else
			bcontot26.set(what);
	}	
	public PackedDecimalData getBcontot27() {
		return bcontot27;
	}
	public void setBcontot27(Object what) {
		setBcontot27(what, false);
	}
	public void setBcontot27(Object what, boolean rounded) {
		if (rounded)
			bcontot27.setRounded(what);
		else
			bcontot27.set(what);
	}	
	public PackedDecimalData getBcontot28() {
		return bcontot28;
	}
	public void setBcontot28(Object what) {
		setBcontot28(what, false);
	}
	public void setBcontot28(Object what, boolean rounded) {
		if (rounded)
			bcontot28.setRounded(what);
		else
			bcontot28.set(what);
	}	
	public PackedDecimalData getBcontot29() {
		return bcontot29;
	}
	public void setBcontot29(Object what) {
		setBcontot29(what, false);
	}
	public void setBcontot29(Object what, boolean rounded) {
		if (rounded)
			bcontot29.setRounded(what);
		else
			bcontot29.set(what);
	}	
	public PackedDecimalData getBcontot30() {
		return bcontot30;
	}
	public void setBcontot30(Object what) {
		setBcontot30(what, false);
	}
	public void setBcontot30(Object what, boolean rounded) {
		if (rounded)
			bcontot30.setRounded(what);
		else
			bcontot30.set(what);
	}	
	public PackedDecimalData getBcontot31() {
		return bcontot31;
	}
	public void setBcontot31(Object what) {
		setBcontot31(what, false);
	}
	public void setBcontot31(Object what, boolean rounded) {
		if (rounded)
			bcontot31.setRounded(what);
		else
			bcontot31.set(what);
	}	
	public PackedDecimalData getBcontot32() {
		return bcontot32;
	}
	public void setBcontot32(Object what) {
		setBcontot32(what, false);
	}
	public void setBcontot32(Object what, boolean rounded) {
		if (rounded)
			bcontot32.setRounded(what);
		else
			bcontot32.set(what);
	}	
	public PackedDecimalData getBcontot33() {
		return bcontot33;
	}
	public void setBcontot33(Object what) {
		setBcontot33(what, false);
	}
	public void setBcontot33(Object what, boolean rounded) {
		if (rounded)
			bcontot33.setRounded(what);
		else
			bcontot33.set(what);
	}	
	public PackedDecimalData getBcontot34() {
		return bcontot34;
	}
	public void setBcontot34(Object what) {
		setBcontot34(what, false);
	}
	public void setBcontot34(Object what, boolean rounded) {
		if (rounded)
			bcontot34.setRounded(what);
		else
			bcontot34.set(what);
	}	
	public PackedDecimalData getBcontot35() {
		return bcontot35;
	}
	public void setBcontot35(Object what) {
		setBcontot35(what, false);
	}
	public void setBcontot35(Object what, boolean rounded) {
		if (rounded)
			bcontot35.setRounded(what);
		else
			bcontot35.set(what);
	}	
	public PackedDecimalData getBcontot36() {
		return bcontot36;
	}
	public void setBcontot36(Object what) {
		setBcontot36(what, false);
	}
	public void setBcontot36(Object what, boolean rounded) {
		if (rounded)
			bcontot36.setRounded(what);
		else
			bcontot36.set(what);
	}	
	public PackedDecimalData getBcontot37() {
		return bcontot37;
	}
	public void setBcontot37(Object what) {
		setBcontot37(what, false);
	}
	public void setBcontot37(Object what, boolean rounded) {
		if (rounded)
			bcontot37.setRounded(what);
		else
			bcontot37.set(what);
	}	
	public PackedDecimalData getBcontot38() {
		return bcontot38;
	}
	public void setBcontot38(Object what) {
		setBcontot38(what, false);
	}
	public void setBcontot38(Object what, boolean rounded) {
		if (rounded)
			bcontot38.setRounded(what);
		else
			bcontot38.set(what);
	}	
	public PackedDecimalData getBcontot39() {
		return bcontot39;
	}
	public void setBcontot39(Object what) {
		setBcontot39(what, false);
	}
	public void setBcontot39(Object what, boolean rounded) {
		if (rounded)
			bcontot39.setRounded(what);
		else
			bcontot39.set(what);
	}	
	public PackedDecimalData getBcontot40() {
		return bcontot40;
	}
	public void setBcontot40(Object what) {
		setBcontot40(what, false);
	}
	public void setBcontot40(Object what, boolean rounded) {
		if (rounded)
			bcontot40.setRounded(what);
		else
			bcontot40.set(what);
	}	
	public PackedDecimalData getBcontot41() {
		return bcontot41;
	}
	public void setBcontot41(Object what) {
		setBcontot41(what, false);
	}
	public void setBcontot41(Object what, boolean rounded) {
		if (rounded)
			bcontot41.setRounded(what);
		else
			bcontot41.set(what);
	}	
	public PackedDecimalData getBcontot42() {
		return bcontot42;
	}
	public void setBcontot42(Object what) {
		setBcontot42(what, false);
	}
	public void setBcontot42(Object what, boolean rounded) {
		if (rounded)
			bcontot42.setRounded(what);
		else
			bcontot42.set(what);
	}	
	public PackedDecimalData getBcontot43() {
		return bcontot43;
	}
	public void setBcontot43(Object what) {
		setBcontot43(what, false);
	}
	public void setBcontot43(Object what, boolean rounded) {
		if (rounded)
			bcontot43.setRounded(what);
		else
			bcontot43.set(what);
	}	
	public PackedDecimalData getBcontot44() {
		return bcontot44;
	}
	public void setBcontot44(Object what) {
		setBcontot44(what, false);
	}
	public void setBcontot44(Object what, boolean rounded) {
		if (rounded)
			bcontot44.setRounded(what);
		else
			bcontot44.set(what);
	}	
	public PackedDecimalData getBcontot45() {
		return bcontot45;
	}
	public void setBcontot45(Object what) {
		setBcontot45(what, false);
	}
	public void setBcontot45(Object what, boolean rounded) {
		if (rounded)
			bcontot45.setRounded(what);
		else
			bcontot45.set(what);
	}	
	public PackedDecimalData getBcontot46() {
		return bcontot46;
	}
	public void setBcontot46(Object what) {
		setBcontot46(what, false);
	}
	public void setBcontot46(Object what, boolean rounded) {
		if (rounded)
			bcontot46.setRounded(what);
		else
			bcontot46.set(what);
	}	
	public PackedDecimalData getBcontot47() {
		return bcontot47;
	}
	public void setBcontot47(Object what) {
		setBcontot47(what, false);
	}
	public void setBcontot47(Object what, boolean rounded) {
		if (rounded)
			bcontot47.setRounded(what);
		else
			bcontot47.set(what);
	}	
	public PackedDecimalData getBcontot48() {
		return bcontot48;
	}
	public void setBcontot48(Object what) {
		setBcontot48(what, false);
	}
	public void setBcontot48(Object what, boolean rounded) {
		if (rounded)
			bcontot48.setRounded(what);
		else
			bcontot48.set(what);
	}	
	public PackedDecimalData getBcontot49() {
		return bcontot49;
	}
	public void setBcontot49(Object what) {
		setBcontot49(what, false);
	}
	public void setBcontot49(Object what, boolean rounded) {
		if (rounded)
			bcontot49.setRounded(what);
		else
			bcontot49.set(what);
	}	
	public PackedDecimalData getBcontot50() {
		return bcontot50;
	}
	public void setBcontot50(Object what) {
		setBcontot50(what, false);
	}
	public void setBcontot50(Object what, boolean rounded) {
		if (rounded)
			bcontot50.setRounded(what);
		else
			bcontot50.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getBcontots() {
		return new FixedLengthStringData(bcontot01.toInternal()
										+ bcontot02.toInternal()
										+ bcontot03.toInternal()
										+ bcontot04.toInternal()
										+ bcontot05.toInternal()
										+ bcontot06.toInternal()
										+ bcontot07.toInternal()
										+ bcontot08.toInternal()
										+ bcontot09.toInternal()
										+ bcontot10.toInternal()
										+ bcontot11.toInternal()
										+ bcontot12.toInternal()
										+ bcontot13.toInternal()
										+ bcontot14.toInternal()
										+ bcontot15.toInternal()
										+ bcontot16.toInternal()
										+ bcontot17.toInternal()
										+ bcontot18.toInternal()
										+ bcontot19.toInternal()
										+ bcontot20.toInternal()
										+ bcontot21.toInternal()
										+ bcontot22.toInternal()
										+ bcontot23.toInternal()
										+ bcontot24.toInternal()
										+ bcontot25.toInternal()
										+ bcontot26.toInternal()
										+ bcontot27.toInternal()
										+ bcontot28.toInternal()
										+ bcontot29.toInternal()
										+ bcontot30.toInternal()
										+ bcontot31.toInternal()
										+ bcontot32.toInternal()
										+ bcontot33.toInternal()
										+ bcontot34.toInternal()
										+ bcontot35.toInternal()
										+ bcontot36.toInternal()
										+ bcontot37.toInternal()
										+ bcontot38.toInternal()
										+ bcontot39.toInternal()
										+ bcontot40.toInternal()
										+ bcontot41.toInternal()
										+ bcontot42.toInternal()
										+ bcontot43.toInternal()
										+ bcontot44.toInternal()
										+ bcontot45.toInternal()
										+ bcontot46.toInternal()
										+ bcontot47.toInternal()
										+ bcontot48.toInternal()
										+ bcontot49.toInternal()
										+ bcontot50.toInternal());
	}
	public void setBcontots(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBcontots().getLength()).init(obj);
	
		what = ExternalData.chop(what, bcontot01);
		what = ExternalData.chop(what, bcontot02);
		what = ExternalData.chop(what, bcontot03);
		what = ExternalData.chop(what, bcontot04);
		what = ExternalData.chop(what, bcontot05);
		what = ExternalData.chop(what, bcontot06);
		what = ExternalData.chop(what, bcontot07);
		what = ExternalData.chop(what, bcontot08);
		what = ExternalData.chop(what, bcontot09);
		what = ExternalData.chop(what, bcontot10);
		what = ExternalData.chop(what, bcontot11);
		what = ExternalData.chop(what, bcontot12);
		what = ExternalData.chop(what, bcontot13);
		what = ExternalData.chop(what, bcontot14);
		what = ExternalData.chop(what, bcontot15);
		what = ExternalData.chop(what, bcontot16);
		what = ExternalData.chop(what, bcontot17);
		what = ExternalData.chop(what, bcontot18);
		what = ExternalData.chop(what, bcontot19);
		what = ExternalData.chop(what, bcontot20);
		what = ExternalData.chop(what, bcontot21);
		what = ExternalData.chop(what, bcontot22);
		what = ExternalData.chop(what, bcontot23);
		what = ExternalData.chop(what, bcontot24);
		what = ExternalData.chop(what, bcontot25);
		what = ExternalData.chop(what, bcontot26);
		what = ExternalData.chop(what, bcontot27);
		what = ExternalData.chop(what, bcontot28);
		what = ExternalData.chop(what, bcontot29);
		what = ExternalData.chop(what, bcontot30);
		what = ExternalData.chop(what, bcontot31);
		what = ExternalData.chop(what, bcontot32);
		what = ExternalData.chop(what, bcontot33);
		what = ExternalData.chop(what, bcontot34);
		what = ExternalData.chop(what, bcontot35);
		what = ExternalData.chop(what, bcontot36);
		what = ExternalData.chop(what, bcontot37);
		what = ExternalData.chop(what, bcontot38);
		what = ExternalData.chop(what, bcontot39);
		what = ExternalData.chop(what, bcontot40);
		what = ExternalData.chop(what, bcontot41);
		what = ExternalData.chop(what, bcontot42);
		what = ExternalData.chop(what, bcontot43);
		what = ExternalData.chop(what, bcontot44);
		what = ExternalData.chop(what, bcontot45);
		what = ExternalData.chop(what, bcontot46);
		what = ExternalData.chop(what, bcontot47);
		what = ExternalData.chop(what, bcontot48);
		what = ExternalData.chop(what, bcontot49);
		what = ExternalData.chop(what, bcontot50);
	}
	public PackedDecimalData getBcontot(BaseData indx) {
		return getBcontot(indx.toInt());
	}
	public PackedDecimalData getBcontot(int indx) {

		switch (indx) {
			case 1 : return bcontot01;
			case 2 : return bcontot02;
			case 3 : return bcontot03;
			case 4 : return bcontot04;
			case 5 : return bcontot05;
			case 6 : return bcontot06;
			case 7 : return bcontot07;
			case 8 : return bcontot08;
			case 9 : return bcontot09;
			case 10 : return bcontot10;
			case 11 : return bcontot11;
			case 12 : return bcontot12;
			case 13 : return bcontot13;
			case 14 : return bcontot14;
			case 15 : return bcontot15;
			case 16 : return bcontot16;
			case 17 : return bcontot17;
			case 18 : return bcontot18;
			case 19 : return bcontot19;
			case 20 : return bcontot20;
			case 21 : return bcontot21;
			case 22 : return bcontot22;
			case 23 : return bcontot23;
			case 24 : return bcontot24;
			case 25 : return bcontot25;
			case 26 : return bcontot26;
			case 27 : return bcontot27;
			case 28 : return bcontot28;
			case 29 : return bcontot29;
			case 30 : return bcontot30;
			case 31 : return bcontot31;
			case 32 : return bcontot32;
			case 33 : return bcontot33;
			case 34 : return bcontot34;
			case 35 : return bcontot35;
			case 36 : return bcontot36;
			case 37 : return bcontot37;
			case 38 : return bcontot38;
			case 39 : return bcontot39;
			case 40 : return bcontot40;
			case 41 : return bcontot41;
			case 42 : return bcontot42;
			case 43 : return bcontot43;
			case 44 : return bcontot44;
			case 45 : return bcontot45;
			case 46 : return bcontot46;
			case 47 : return bcontot47;
			case 48 : return bcontot48;
			case 49 : return bcontot49;
			case 50 : return bcontot50;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBcontot(BaseData indx, Object what) {
		setBcontot(indx, what, false);
	}
	public void setBcontot(BaseData indx, Object what, boolean rounded) {
		setBcontot(indx.toInt(), what, rounded);
	}
	public void setBcontot(int indx, Object what) {
		setBcontot(indx, what, false);
	}
	public void setBcontot(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBcontot01(what, rounded);
					 break;
			case 2 : setBcontot02(what, rounded);
					 break;
			case 3 : setBcontot03(what, rounded);
					 break;
			case 4 : setBcontot04(what, rounded);
					 break;
			case 5 : setBcontot05(what, rounded);
					 break;
			case 6 : setBcontot06(what, rounded);
					 break;
			case 7 : setBcontot07(what, rounded);
					 break;
			case 8 : setBcontot08(what, rounded);
					 break;
			case 9 : setBcontot09(what, rounded);
					 break;
			case 10 : setBcontot10(what, rounded);
					 break;
			case 11 : setBcontot11(what, rounded);
					 break;
			case 12 : setBcontot12(what, rounded);
					 break;
			case 13 : setBcontot13(what, rounded);
					 break;
			case 14 : setBcontot14(what, rounded);
					 break;
			case 15 : setBcontot15(what, rounded);
					 break;
			case 16 : setBcontot16(what, rounded);
					 break;
			case 17 : setBcontot17(what, rounded);
					 break;
			case 18 : setBcontot18(what, rounded);
					 break;
			case 19 : setBcontot19(what, rounded);
					 break;
			case 20 : setBcontot20(what, rounded);
					 break;
			case 21 : setBcontot21(what, rounded);
					 break;
			case 22 : setBcontot22(what, rounded);
					 break;
			case 23 : setBcontot23(what, rounded);
					 break;
			case 24 : setBcontot24(what, rounded);
					 break;
			case 25 : setBcontot25(what, rounded);
					 break;
			case 26 : setBcontot26(what, rounded);
					 break;
			case 27 : setBcontot27(what, rounded);
					 break;
			case 28 : setBcontot28(what, rounded);
					 break;
			case 29 : setBcontot29(what, rounded);
					 break;
			case 30 : setBcontot30(what, rounded);
					 break;
			case 31 : setBcontot31(what, rounded);
					 break;
			case 32 : setBcontot32(what, rounded);
					 break;
			case 33 : setBcontot33(what, rounded);
					 break;
			case 34 : setBcontot34(what, rounded);
					 break;
			case 35 : setBcontot35(what, rounded);
					 break;
			case 36 : setBcontot36(what, rounded);
					 break;
			case 37 : setBcontot37(what, rounded);
					 break;
			case 38 : setBcontot38(what, rounded);
					 break;
			case 39 : setBcontot39(what, rounded);
					 break;
			case 40 : setBcontot40(what, rounded);
					 break;
			case 41 : setBcontot41(what, rounded);
					 break;
			case 42 : setBcontot42(what, rounded);
					 break;
			case 43 : setBcontot43(what, rounded);
					 break;
			case 44 : setBcontot44(what, rounded);
					 break;
			case 45 : setBcontot45(what, rounded);
					 break;
			case 46 : setBcontot46(what, rounded);
					 break;
			case 47 : setBcontot47(what, rounded);
					 break;
			case 48 : setBcontot48(what, rounded);
					 break;
			case 49 : setBcontot49(what, rounded);
					 break;
			case 50 : setBcontot50(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		scheduleName.clear();
		scheduleNumber.clear();
		company.clear();
		processName.clear();
		processOccNum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		processStatus.clear();
		scheduleThreadNo.clear();
		schedulingPriority.clear();
		datimeStarted.clear();
		datimeLogged.clear();
		totalElapsedTime.clear();
		cycleCount.clear();
		notProcessedCount.clear();
		errorCount.clear();
		fsuco.clear();
		cmtrestart.clear();
		bcontot01.clear();
		bcontot02.clear();
		bcontot03.clear();
		bcontot04.clear();
		bcontot05.clear();
		bcontot06.clear();
		bcontot07.clear();
		bcontot08.clear();
		bcontot09.clear();
		bcontot10.clear();
		bcontot11.clear();
		bcontot12.clear();
		bcontot13.clear();
		bcontot14.clear();
		bcontot15.clear();
		bcontot16.clear();
		bcontot17.clear();
		bcontot18.clear();
		bcontot19.clear();
		bcontot20.clear();
		bcontot21.clear();
		bcontot22.clear();
		bcontot23.clear();
		bcontot24.clear();
		bcontot25.clear();
		bcontot26.clear();
		bcontot27.clear();
		bcontot28.clear();
		bcontot29.clear();
		bcontot30.clear();
		bcontot31.clear();
		bcontot32.clear();
		bcontot33.clear();
		bcontot34.clear();
		bcontot35.clear();
		bcontot36.clear();
		bcontot37.clear();
		bcontot38.clear();
		bcontot39.clear();
		bcontot40.clear();
		bcontot41.clear();
		bcontot42.clear();
		bcontot43.clear();
		bcontot44.clear();
		bcontot45.clear();
		bcontot46.clear();
		bcontot47.clear();
		bcontot48.clear();
		bcontot49.clear();
		bcontot50.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}