package com.csc.life.contractservicing.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:18
 * Description:
 * Copybook name: ASGNMNAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Asgnmnakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData asgnmnaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData asgnmnaKey = new FixedLengthStringData(256).isAPartOf(asgnmnaFileKey, 0, REDEFINE);
  	public FixedLengthStringData asgnmnaChdrcoy = new FixedLengthStringData(1).isAPartOf(asgnmnaKey, 0);
  	public FixedLengthStringData asgnmnaChdrnum = new FixedLengthStringData(8).isAPartOf(asgnmnaKey, 1);
  	public PackedDecimalData asgnmnaSeqno = new PackedDecimalData(2, 0).isAPartOf(asgnmnaKey, 9);
  	public FixedLengthStringData asgnmnaAsgnnum = new FixedLengthStringData(8).isAPartOf(asgnmnaKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(237).isAPartOf(asgnmnaKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(asgnmnaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		asgnmnaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}