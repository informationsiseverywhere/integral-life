/*
 * File: B5061.java
 * Date: 29 August 2009 20:53:28
 * Author: Quipoz Limited
 *
 * Class transformed from B5061.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.group.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrrevTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*          PAYRPF CONVERSION PROGRAM.
*          --------------------------
*
*   PAYR conversion program for AQR 4686.
*     Populate the following fields on the PAYR file....
*                                    BILLDAY
*                                    BILLMONTH
*                                    DUEDD
*                                    DUEMM
*
* PROCESSING......
*
*    BEGN on PAYR using PAYRLIF logical view
*
*       READR on Contract file using CHDRENQ logical view
*        Get Contract OCCDATE.
*
*
*       BEGN on Life installments file using LINS logical view
*         to get INSTTO date.
*
*       IF no LINS,
*          BEGN on PAYR file using PAYRREV logical view to get
*            1st ever PAYR record for contract
*            Get BILLCD date instead on INSTTO date.
*
*       READH on PAYRLIF record
*         Update BILLDAY, BILLMONTH, DUEDD, DUEMM.
*
*       REWRT PAYRLIF
*
*       Update all PAYRLIF records which match on the current
*          PAYRLIF key
*
*       IF PAYRLIF key changes, then GOTO CHDRENQ READR above.
*
*
*    Continue above processing until ENDP statuz is achieved
*        on PAYRLIF processing.
*
*****************************************************************
* </pre>
*/
public class B5061 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5061");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-DATE1 */
	private ZonedDecimalData wsaaInstto = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaInsttox = new FixedLengthStringData(8).isAPartOf(wsaaInstto, 0, REDEFINE);
	private ZonedDecimalData wsaaInsttoMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaInsttox, 4).setUnsigned();
	private ZonedDecimalData wsaaInsttoDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaInsttox, 6).setUnsigned();
		/* WSAA-DATE2 */
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaOccdatex = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, REDEFINE);
	private ZonedDecimalData wsaaOccdateMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaOccdatex, 4).setUnsigned();
	private ZonedDecimalData wsaaOccdateDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaOccdatex, 6).setUnsigned();
	private FixedLengthStringData wsaaKeyChange = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private String wsaaNoLinsFound = "";
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);
		/* FORMATS */
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String linsrec = "LINSREC";
	private static final String payrlifrec = "PAYRLIFREC";
	private static final String payrrevrec = "PAYRREVREC";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Life instalments billed details*/
	private LinsTableDAM linsIO = new LinsTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
		/*Alter from Inception view of PAYR*/
	private PayrrevTableDAM payrrevIO = new PayrrevTableDAM();
	private Varcom varcom = new Varcom();
	private P6671par p6671par = new P6671par();

	public B5061() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		control1000();
		/*EXIT*/
	}

protected void control1000()
	{
		start1000();
	}

protected void start1000()
	{
		p6671par.parmRecord.set(conjobrec.params);
		wsaaChdrnumfrm.set(p6671par.chdrnum);
		wsaaChdrnumto.set(p6671par.chdrnum1);
		if (isEQ(p6671par.chdrnum,SPACES)) {
			wsaaChdrnumfrm.set(ZERO);
		}
		if (isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumto.set("99999999");
		}
		payrlifIO.setParams(SPACES);
		payrlifIO.setChdrcoy(runparmrec.company);
		payrlifIO.setChdrnum(wsaaChdrnumfrm);
		payrlifIO.setPayrseqno(ZERO);
		payrlifIO.setFunction(varcom.begnh);
		payrlifIO.setFormat(payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)
		&& isNE(payrlifIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(payrlifIO.getParams());
			conerrrec.statuz.set(payrlifIO.getStatuz());
			databaseError006();
		}
		if (isNE(payrlifIO.getChdrcoy(),runparmrec.company)
		|| isEQ(payrlifIO.getStatuz(),varcom.endp)) {
			payrlifIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(payrlifIO.getStatuz(),varcom.endp))) {
			processPayr1100();
		}

	}

protected void processPayr1100()
	{
		start1100();
	}

protected void start1100()
	{
		/* Check whether Contract is is range or not.*/
		if (isLT(payrlifIO.getChdrnum(), wsaaChdrnumfrm)
		|| isGT(payrlifIO.getChdrnum(), wsaaChdrnumto)) {
			payrlifIO.setStatuz(varcom.endp);
			return ;
		}
		/* We need to get a CHDR record to get the OCCDATE and the*/
		/*   earliest LINS record to get the first INSTTO date.*/
		readContract2000();
		getLins3000();
		/*  If there was no LINS, then we need to get the BTDATE from the*/
		/*    first ever PAYR record for this contract, so we will use the*/
		/*    PAYRREV logical, set TRANNO in the key to ZEROS, and do a*/
		/*    BEGN on it.*/
		if (isEQ(wsaaNoLinsFound, "Y")) {
			firstPayr4000();
		}
		/* Now UPDATE PAYR record we have got and then UPDATE ALL PAYR*/
		/*   records which have the same COY, CHDRNUM & PAYRSEQNO before*/
		/*  we start the loop again up at 1100-START.*/
		wsaaKeyChange.set(SPACES);
		wsaaChdrcoy.set(payrlifIO.getChdrcoy());
		wsaaChdrnum.set(payrlifIO.getChdrnum());
		while ( !(isEQ(payrlifIO.getStatuz(),varcom.endp)
		|| isEQ(wsaaKeyChange,"Y"))) {
			payrUpdates5000();
		}

	}

protected void readContract2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* We need to get a CHDR record to get the OCCDATE*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(payrlifIO.getChdrcoy());
		chdrenqIO.setChdrnum(payrlifIO.getChdrnum());
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrenqIO.getParams());
			conerrrec.statuz.set(chdrenqIO.getStatuz());
			databaseError006();
		}
		wsaaOccdate.set(chdrenqIO.getOccdate());
	}

protected void getLins3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* We need to get the 1st LINS record for the contract we are*/
		/*   currently processing.*/
		wsaaNoLinsFound = "N";
		linsIO.setParams(SPACES);
		linsIO.setChdrcoy(payrlifIO.getChdrcoy());
		linsIO.setChdrnum(payrlifIO.getChdrnum());
		linsIO.setFormat(linsrec);
		linsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		linsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, linsIO);
		if (isNE(linsIO.getStatuz(),varcom.oK)
		&& isNE(linsIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(linsIO.getParams());
			conerrrec.statuz.set(linsIO.getStatuz());
			databaseError006();
		}
		if (isNE(payrlifIO.getChdrcoy(),linsIO.getChdrcoy())
		|| isNE(payrlifIO.getChdrnum(),linsIO.getChdrnum())
		|| isEQ(linsIO.getStatuz(),varcom.endp)) {
			wsaaNoLinsFound = "Y";
			return ;
		}
		wsaaInstto.set(linsIO.getInstto());
	}

protected void firstPayr4000()
	{
		start4000();
	}

protected void start4000()
	{
		/*  Use the PAYRREV logical view to get first ever PAYR record*/
		/*   for specified key.*/
		payrrevIO.setParams(SPACES);
		payrrevIO.setChdrcoy(payrlifIO.getChdrcoy());
		payrrevIO.setChdrnum(payrlifIO.getChdrnum());
		payrrevIO.setPayrseqno(payrlifIO.getPayrseqno());
		payrrevIO.setTranno(ZERO);
		payrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		payrrevIO.setFormat(payrrevrec);
		SmartFileCode.execute(appVars, payrrevIO);
		if (isNE(payrrevIO.getStatuz(),varcom.oK)
		&& isNE(payrrevIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(payrrevIO.getParams());
			conerrrec.statuz.set(payrrevIO.getStatuz());
			databaseError006();
		}
		if (isNE(payrlifIO.getChdrcoy(), payrrevIO.getChdrcoy())
		|| isNE(payrlifIO.getChdrnum(), payrrevIO.getChdrnum())
		|| isEQ(payrrevIO.getStatuz(), varcom.endp)) {
			/*        if we get here then major problems because what PAYR*/
			/*         are we processing*/
			conerrrec.params.set(payrrevIO.getParams());
			conerrrec.statuz.set(payrrevIO.getStatuz());
			databaseError006();
		}
		wsaaInstto.set(payrrevIO.getBillcd());
	}

protected void payrUpdates5000()
	{
		start5000();
	}

protected void start5000()
	{
		payrlifIO.setBillday(wsaaInsttoDd);
		payrlifIO.setBillmonth(wsaaInsttoMm);
		payrlifIO.setDuedd(wsaaOccdateDd);
		payrlifIO.setDuemm(wsaaOccdateMm);
		payrlifIO.setFunction(varcom.rewrt);
		payrlifIO.setFormat(payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(payrlifIO.getParams());
			conerrrec.statuz.set(payrlifIO.getStatuz());
			databaseError006();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		payrlifIO.setFunction(varcom.nextr);
		payrlifIO.setFormat(payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)
		&& isNE(payrlifIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(payrlifIO.getParams());
			conerrrec.statuz.set(payrlifIO.getStatuz());
			databaseError006();
		}
		if (isNE(wsaaChdrcoy,payrlifIO.getChdrcoy())
		|| isNE(wsaaChdrnum,payrlifIO.getChdrnum())) {
			wsaaKeyChange.set("Y");
			return ;
		}
		if (isEQ(payrlifIO.getStatuz(), varcom.endp)) {
			return ;
		}
	}
}
