/*
 * File: Grpltrig.java
 * Date: 29 August 2009 22:51:07
 * Author: Quipoz Limited
 *
 * Class transformed from GRPLTRIG.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.group.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.tablestructures.Triggerrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*             GROUP RECONCILIATION TRIGGER MODULE.
*             ------------------------------------
* Overview.
* ---------
*
* This subroutine will write 2 ACMV records each time it is
*  called from the Account Reconciliation AT module P2806AT.
* These records credit the individual Contract Suspense Account
*   and debit the Contract Billed premium account.
*
*Functionality
*-------------
*
* Read the table T5645 to get the posting data.
* Write the 2 ACMVs using information passed in the TRIGGERREC
*  linkage.
*
*****************************************************************
* </pre>
*/
public class Grpltrig extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "GRPLTRIG";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
		/* FORMATS */
	private String itemrec = "ITEMREC   ";
		/* TABLES */
	private String t5645 = "T5645";
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Triggerrec triggerrec = new Triggerrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit99490,
		exit99590
	}

	public Grpltrig() {
		super();
	}

public void mainline(Object... parmArray)
	{
		triggerrec.triggerRec = convertAndSetParam(triggerrec.triggerRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		control100();
		exit190();
	}

protected void control100()
	{
		initialise1000();
		writeAcmvs2000();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		/*START*/
		triggerrec.statuz.set(varcom.oK);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			systemError99000();
		}
		wsaaToday.set(datcon1rec.intDate);
		readT56451100();
		/*EXIT*/
	}

protected void readT56451100()
	{
		start1100();
	}

protected void start1100()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(triggerrec.rdoccoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void writeAcmvs2000()
	{
		start2000();
	}

protected void start2000()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(triggerrec.batchkey);
		lifacmvrec.rdocnum.set(triggerrec.rdocnum);
		lifacmvrec.tranno.set(triggerrec.tranno);
		lifacmvrec.rldgcoy.set(triggerrec.rldgcoy);
		lifacmvrec.rldgacct.set(triggerrec.rdocnum);
		lifacmvrec.origcurr.set(triggerrec.origccy);
		lifacmvrec.tranref.set(triggerrec.tranref);
		lifacmvrec.trandesc.set(triggerrec.trandesc);
		lifacmvrec.crate.set(triggerrec.crate);
		lifacmvrec.genlcoy.set(triggerrec.genlcoy);
		lifacmvrec.genlcur.set(triggerrec.genlcur);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(triggerrec.termid);
		lifacmvrec.user.set(triggerrec.user);
		lifacmvrec.transactionTime.set(triggerrec.transactionTime);
		lifacmvrec.transactionDate.set(triggerrec.transactionDate);
		writeFirst2100();
		writeSecond2200();
	}

protected void writeFirst2100()
	{
		start2100();
	}

protected void start2100()
	{
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origamt.set(triggerrec.origamt);
		lifacmvrec.acctamt.set(triggerrec.acctamt);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.jrnseq.set(1);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void writeSecond2200()
	{
		start2200();
	}

protected void start2200()
	{
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.origamt.set(triggerrec.origamt);
		lifacmvrec.acctamt.set(triggerrec.acctamt);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.jrnseq.set(2);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void systemError99000()
	{
		try {
			start99000();
		}
		catch (GOTOException e){
		}
		finally{
			exit99490();
		}
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99490);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		triggerrec.statuz.set(varcom.bomb);
		exit190();
	}

protected void databaseError99500()
	{
		try {
			start99500();
		}
		catch (GOTOException e){
		}
		finally{
			exit99590();
		}
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz,varcom.bomb))) {
			goTo(GotoLabel.exit99590);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99590()
	{
		triggerrec.statuz.set(varcom.bomb);
		exit190();
	}
}
