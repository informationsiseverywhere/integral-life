/*
 * File: Grpwd.java
 * Date: 29 August 2009 22:51:11
 * Author: Quipoz Limited
 *
 * Class transformed from GRPWD.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.group.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.smart.procedures.Smtchkprg;
import com.csc.smart.procedures.Wsspio;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Confmsgrec;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart.recordstructures.Wssprec;
import com.csc.smart400framework.procedures.Confmsg;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*       DRIVER FOR BANKACCKEY SCROLL SELECTION
*
* This program drives the Bankacckey scroll system when CF6
* is pressed on the field BANKACCKEY.
*
* This program runs until one of the following occurs:
*     - a command key is pressed to request a return "above"
*       the submenu (e.g. to the system or master menus),
*     - a non-blank WSSP-EDTERROR status is returned
*       (i.e. a system or database error has occurred).
*
************************************************************
* </pre>
*/
public class Grpwd extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("GRPWD");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaNextProg = new FixedLengthStringData(5);
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(1536);
	private Confmsgrec confmsgrec = new Confmsgrec();
	private Batckey wsaaBatchkey = new Batckey();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Wssprec wssprec = new Wssprec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		callNextProgram100,
		endProgram200,
		returnUpStack400
	}

	public Grpwd() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					mainlinePara000();
				}
				case callNextProgram100: {
					callNextProgram100();
				}
				case endProgram200: {
					endProgram200();
				}
				case returnUpStack400: {
					returnUpStack400();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void mainlinePara000()
	{
		wsspcomn.secSwitching.set(SPACES);
		wsspcomn.secProg[1].set(wsspcomn.next2prog);
		wsspcomn.secProg[2].set(wsspcomn.next3prog);
		wsspcomn.secProg[3].set(wsspcomn.next4prog);
		wsspcomn.programPtr.set(1);
	}

protected void callNextProgram100()
	{
		if (isEQ(wsspcomn.secProg[wsspcomn.programPtr.toInt()],"COMIT")) {
			appVars.commit();
			wsspcomn.programPtr.add(1);
			confmsgrec.language.set(wsspcomn.language);
			confmsgrec.type.set("C");
			wsaaBatchkey.set(wsspcomn.batchkey);
			confmsgrec.transaction.set(wsaaBatchkey.batcBatctrcde);
			confmsgrec.key.set(wsspcomn.confirmationKey);
			callProgram(Confmsg.class, confmsgrec.confmsgRec);
			if (isEQ(confmsgrec.statuz,"****")) {
				wsspcomn.msgarea.set(confmsgrec.message);
			}
			else {
				wsspcomn.msgarea.set("CMessage error");
			}
		}
		wsspcomn.nextprog.set(wsspcomn.secProg[wsspcomn.programPtr.toInt()]);
		if (isEQ(wsspcomn.nextprog,SPACES)) {
			returnUpStack400();
		}
		if (isEQ(wsspcomn.nextprog,SPACES)) {
			goTo(GotoLabel.endProgram200);
		}
		callProgram(Smtchkprg.class, wsspcomn.userid, wsspcomn.nextprog);
		wsaaNextProg.set(wsspcomn.nextprog);
		wssprec.wsspcomn.set(wsspcomn.commonArea);
		wssprec.function.set("REWRT");
		callProgram(Wsspio.class, wssprec.params);
		callProgram(wsaaNextProg, wsspcomn.commonArea, wsspUserArea);
		if (isEQ(wsspcomn.edterror,SPACES)
		&& isNE(wsspcomn.nextprog,"SYSM")
		&& isNE(wsspcomn.nextprog,"MASM")
		&& isNE(wsspcomn.nextprog,"SUBM")
		&& isNE(wsspcomn.nextprog,"CMDE")
		&& isNE(wsspcomn.nextprog,"SOFF")
		&& isNE(wsspcomn.nextprog,SPACES)) {
			goTo(GotoLabel.callNextProgram100);
		}
	}

protected void endProgram200()
	{
		exitProgram();
	}

protected void returnUpStack400()
	{
		wsspcomn.programPtr.subtract(1);
		if (isNE(wsspcomn.programPtr,0)) {
			if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
				wsspcomn.nextprog.set(wsspcomn.secProg[wsspcomn.programPtr.toInt()]);
			}
			else {
				goTo(GotoLabel.returnUpStack400);
			}
		}
	}
}
