package com.csc.life.terminationclaims.dataaccess.dao.impl;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.HmtdpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Hmtdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HmtdpfDAOImpl extends BaseDAOImpl<Hmtdpf> implements HmtdpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HmtdpfDAOImpl.class);

	@Override
	public boolean insertHmtdpf(List<Hmtdpf> hmtdpfList) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO HMTDPF (CHDRCOY, CHDRNUM, TRANNO, LIFE, JLIFE, COVERAGE, RIDER, CRTABLE, PLNSFX, HACTVAL, HEMV, HCNSTCUR, TYPE_T, USRPRF, JOBNM, DATIME) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Hmtdpf hmtd : hmtdpfList) {
				ps.setString(1, hmtd.getChdrcoy());
				ps.setString(2, hmtd.getChdrnum());
				ps.setInt(3, hmtd.getTranno());
				ps.setString(4, hmtd.getLife());
				ps.setString(5, hmtd.getJlife());
				ps.setString(6, hmtd.getCoverage());
				ps.setString(7, hmtd.getRider());
				ps.setString(8, hmtd.getCrtable());
				ps.setInt(9, hmtd.getPlnsfx());
				ps.setBigDecimal(10, hmtd.getHactval());
				ps.setBigDecimal(11, hmtd.getHemv());
				ps.setString(12, hmtd.getHcnstcur());
				ps.setString(13, hmtd.getType_t());
				ps.setString(14, this.getUsrprf());
				ps.setString(15, this.getJobnm());
				ps.setTimestamp(16, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertHmtdpf()", e);//IJTI-1561
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return isInsertSuccessful;
	}
	
	 public Hmtdpf getHmtdData(String chdrcoy,String chdrnum,String life,String coverage,String crtable, String type){
		 
		 Hmtdpf hmtdpf = null;
 		PreparedStatement psHmtdSelect = null;
 		ResultSet sqlHmtd1rs = null;
 		StringBuilder sqlHmtdSelectBuilder = new StringBuilder();

 		sqlHmtdSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, TYPE_T,  HACTVAL");
 		sqlHmtdSelectBuilder.append(" FROM HMTDPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE=? AND COVERAGE=? AND CRTABLE=? AND  TYPE_T=?");
 		sqlHmtdSelectBuilder.append(" ORDER BY ");
 		sqlHmtdSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
 		psHmtdSelect = getPrepareStatement(sqlHmtdSelectBuilder.toString());
 		
 		try {
 			psHmtdSelect.setString(1, chdrcoy);
 			psHmtdSelect.setString(2, chdrnum);
 			psHmtdSelect.setString(3, life);
 			psHmtdSelect.setString(4, coverage);
 			psHmtdSelect.setString(5, crtable);
 			psHmtdSelect.setString(6, type);
 			sqlHmtd1rs = executeQuery(psHmtdSelect);
 			while (sqlHmtd1rs.next()) {
 				hmtdpf = new Hmtdpf();
 				hmtdpf.setChdrcoy(sqlHmtd1rs.getString(1));
 				hmtdpf.setChdrnum(sqlHmtd1rs.getString(2));
 				hmtdpf.setLife(sqlHmtd1rs.getString(3));
 				hmtdpf.setCoverage(sqlHmtd1rs.getString(4));
 				hmtdpf.setRider(sqlHmtd1rs.getString(5));
 				hmtdpf.setCrtable(sqlHmtd1rs.getString(6));
 				hmtdpf.setType_t(sqlHmtd1rs.getString(7));
 				hmtdpf.setHactval(sqlHmtd1rs.getBigDecimal(8));				
 			}

 		} catch (SQLException e) {
 			LOGGER.error("getHmtdData()", e);//IJTI-1561
 			throw new SQLRuntimeException(e);
 		} finally {
 			close(psHmtdSelect, sqlHmtd1rs);
 		}
 		return hmtdpf;
 	}


}
