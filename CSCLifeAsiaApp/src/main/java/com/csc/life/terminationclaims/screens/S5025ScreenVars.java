package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5025
 * @version 1.0 generated on 30/08/09 06:31
 * @author Quipoz
 */
public class S5025ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(67);
	public FixedLengthStringData dataFields = new FixedLengthStringData(19).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData efdate = DD.efdate.copyToZonedDecimal().isAPartOf(dataFields,11);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 19);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData efdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 31);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] efdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData efdateDisp = new FixedLengthStringData(10);

	public LongData S5025screenWritten = new LongData(0);
	public LongData S5025protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5025ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(efdateOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrsel, efdate, action};
		screenOutFields = new BaseData[][] {chdrselOut, efdateOut, actionOut};
		screenErrFields = new BaseData[] {chdrselErr, efdateErr, actionErr};
		screenDateFields = new BaseData[] {efdate};
		screenDateErrFields = new BaseData[] {efdateErr};
		screenDateDispFields = new BaseData[] {efdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S5025screen.class;
		protectRecord = S5025protect.class;
	}

}
