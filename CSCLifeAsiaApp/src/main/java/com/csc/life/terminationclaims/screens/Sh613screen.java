package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh613screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh613ScreenVars sv = (Sh613ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh613screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh613ScreenVars screenVars = (Sh613ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownerdesc.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifedesc.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifedesc.setClassString("");
		screenVars.sfcdesc.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.hprjptdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.hregp.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.sacscurbal.setClassString("");
		screenVars.intanny.setClassString("");
		screenVars.osbal.setClassString("");
		screenVars.hbillosprm.setClassString("");
		screenVars.hrifeecnt.setClassString("");
		screenVars.hreinstfee.setClassString("");
		screenVars.xamt.setClassString("");
		screenVars.newamnt.setClassString("");
		screenVars.toamount.setClassString("");
		screenVars.zlstfndval.setClassString("");
		screenVars.tolerance.setClassString("");
		screenVars.taxamt01.setClassString("");
		screenVars.taxamt02.setClassString("");
	}

/**
 * Clear all the variables in Sh613screen
 */
	public static void clear(VarModel pv) {
		Sh613ScreenVars screenVars = (Sh613ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.cownnum.clear();
		screenVars.ownerdesc.clear();
		screenVars.lifenum.clear();
		screenVars.lifedesc.clear();
		screenVars.jlife.clear();
		screenVars.jlifedesc.clear();
		screenVars.sfcdesc.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.hprjptdateDisp.clear();
		screenVars.hprjptdate.clear();
		screenVars.billfreq.clear();
		screenVars.mop.clear();
		screenVars.hregp.clear();
		screenVars.cntcurr.clear();
		screenVars.billcurr.clear();
		screenVars.sacscurbal.clear();
		screenVars.intanny.clear();
		screenVars.osbal.clear();
		screenVars.hbillosprm.clear();
		screenVars.hrifeecnt.clear();
		screenVars.hreinstfee.clear();
		screenVars.xamt.clear();
		screenVars.newamnt.clear();
		screenVars.toamount.clear();
		screenVars.zlstfndval.clear();
		screenVars.tolerance.clear();
		screenVars.taxamt01.clear();
		screenVars.taxamt02.clear();
	}
}
