/*
 * File: P5022at.java
 * Date: 29 August 2009 23:56:37
 * Author: Quipoz Limited
 *
 * Class transformed from P5022AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult; //MIBT-196
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SvltclmTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
* P5022AT - Bonus Surrender AT Module.
* -----------------------------------
*
*  This AT module is used to process Bonus Surrenders.
*  The Contract number to be processed is passed in the AT as the
*  parameters "primary key".
*
*  Processing is driven by the SURRENDER detail file. All
*  records for this contract for this CHDRNUM for this TRANNO
*  are processed sequentially.
*
*  PROCESSING.
*  ----------
*
*  - READH the CHDR record and store the TRANNO.
*
*  - READH the SURH record to get the EFFDATE of the transaction.
*
*  - Increment the TRANNO.
*
*  - Update the CHDR file by making the current CHDR record
*    VALIDFLAG 2 and writing a new record with the new TRANNO
*    and VALIDFLAG 1. Set 'from' and 'to' dates accordingly.
*
*  - Read tables to establish whether component level
*    accounting or not, to get accounting rules and to get
*    transaction description.
*
*  - BEGN on the SURD file.
*
*  MAIN PROCESSING LOOP.
*  --------------------
*
*  PERFORM UNTIL CHANGE OF CHDRNUM OR TRANNO ON THE SURD FILE.
*
*  - Write accounting entries :
*
*    1. Post Amount of BONUS to Reversionary Bonus Sub Account.
*       A single sided entry effecting ACBL balance for
*       reversionary Bonus. This is set up in T5645 without
*       a GLMAP which will cause GLPOST to exclude it from
*       the true general ledger postings. This is required as
*       all postings cannot be excluded from this single
*       transaction code.
*       Use Full Component Key in ACMV-RLDGACCT.
*
*    2. Cash Bonus Posting Component level if Component Level
*       Accounting = 'Y'. ACMV-RLDGACCT = Full Component Key.
*
*       OR
*
*    3. Cash Bonus Posting Contract Level if Component Level
*       Accounting = 'N'. ACMV-RLDGACCT = Contract Number.
*
*    4. Suspense Bonus Posting (Contract Level)
*       Use Full Component Key in ACMV-RLDGACCT.
*
*    - If this is a NOTIONAL POLICY, then BREAKOUT the
*      ACBL's and the ACMV's.
*
*    - Store details in working storage for printing out letter
*
*    - DELET this SURD record.
*
*    - NEXTR the SURD file.
*
*    END OF MAIN PROCESSING LOOP.
*    ---------------------------
*
*    - Write LETC and SVLT letter
*
*    - DELET the SURH record.
*
*    - Release the Softlock on the contract
*
*****************************************************
* </pre>
*/
public class P5022at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5022AT");
	private final String wsaaT5645Prog = "P5022";

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaDayBefore = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator componentLevelAccounting = new Validator(wsaaAcctLevel, "Y");
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSvltSub = new ZonedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String g437 = "G437";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t1688 = "T1688";
	private static final String tr384 = "TR384";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String surdclmrec = "SURDCLMREC";
	private static final String surhclmrec = "SURHCLMREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String svltclmrec = "SVLTCLMREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
		/*Full Surrender header record*/
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
		/*Surr Val Letters detail file for claims*/
	private SvltclmTableDAM svltclmIO = new SvltclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Batcuprec batcuprec = new Batcuprec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Atmodrec atmodrec = new Atmodrec();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();
	private WsaaStoredLetterDetailInner wsaaStoredLetterDetailInner = new WsaaStoredLetterDetailInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit5299
	}

	public P5022at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*START*/
		initialise1000();
		while ( !(isEQ(surdclmIO.getStatuz(),varcom.endp))) {
			processSurdRecords2000();
		}

		writeLetters5200();
		deleteSurh3000();
		updateBatchHeader3500();
		releaseSoftlock4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
		getTodaysDate1010();
	}

protected void start1000()
	{
		sysrSyserrRecInner.sysrSubrname.set(wsaaProg);
	}

protected void getTodaysDate1010()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon1rec.datcon1Rec);
			sysrSyserrRecInner.sysrStatuz.set(datcon1rec.statuz);
			fatalError9000();
		}
		wsaaToday.set(datcon1rec.intDate);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		contractHeader1100();
		readTables1200();
		writePtrnRecord1300();
		beginSurdFile1400();
		clearLetterWs1500();
	}

protected void contractHeader1100()
	{
		/*START*/
		readContractHeader1110();
		readSurhFile1120();
		rewriteContractHeader1130();
		writeNewContractHeader1140();
		/*EXIT*/
	}

protected void readContractHeader1110()
	{
		start1110();
	}

protected void start1110()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrmjaIO.getStatuz());
			fatalError9000();
		}
		compute(wsaaTranno, 0).set(add(1,chdrmjaIO.getTranno()));
	}

protected void readSurhFile1120()
	{
		start1120();
		getDayBeforeEffdate1125();
	}

protected void start1120()
	{
		surhclmIO.setParams(SPACES);
		surhclmIO.setChdrcoy(atmodrec.company);
		surhclmIO.setChdrnum(chdrmjaIO.getChdrnum());
		surhclmIO.setTranno(wsaaTranno);
		surhclmIO.setPlanSuffix(ZERO);
		surhclmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		surhclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surhclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");

		surhclmIO.setFormat(surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)
		&& isNE(surhclmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surhclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(surhclmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(surhclmIO.getChdrcoy(),atmodrec.company)
		|| isNE(surhclmIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(surhclmIO.getTranno(),wsaaTranno)
		|| isEQ(surhclmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surhclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(surhclmIO.getStatuz());
			fatalError9000();
		}
		surhclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(surhclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(surhclmIO.getStatuz());
			fatalError9000();
		}
	}

protected void getDayBeforeEffdate1125()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(surhclmIO.getEffdate());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon2rec.datcon2Rec);
			sysrSyserrRecInner.sysrStatuz.set(datcon2rec.statuz);
			fatalError9000();
		}
		wsaaDayBefore.set(datcon2rec.intDate2);
		wsaaEffdate.set(surhclmIO.getEffdate());
	}

protected void rewriteContractHeader1130()
	{
		start1130();
	}

protected void start1130()
	{
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmjaIO.setTranid(varcom.vrcmCompTranid);
		chdrmjaIO.setCurrto(wsaaDayBefore);
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrmjaIO.getStatuz());
			fatalError9000();
		}
	}

protected void writeNewContractHeader1140()
	{
		start1140();
	}

protected void start1140()
	{
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		chdrmjaIO.setCurrfrom(surhclmIO.getEffdate());
		chdrmjaIO.setCurrto(99999999);
		chdrmjaIO.setFunction(varcom.writr);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setValidflag("1");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrmjaIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrmjaIO.getStatuz());
			fatalError9000();
		}
	}

protected void readTables1200()
	{
		/*START*/
		readAccountingRules1210();
		getAccountingLevel1220();
		getTranDesc1230();
		/*EXIT*/
	}

protected void readAccountingRules1210()
	{
		start1210();
	}

protected void start1210()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaT5645Prog);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void getAccountingLevel1220()
	{
		start1220();
	}

protected void start1220()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrmjaIO.getCnttype());
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrmjaIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(chdrmjaIO.getCnttype());
			sysrSyserrRecInner.sysrStatuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaAcctLevel.set(t5688rec.comlvlacc);
	}

protected void getTranDesc1230()
	{
		start1230();
	}

protected void start1230()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmjaIO.getChdrcoy());
		descIO.setDesctabl(t1688);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(descIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(descIO.getStatuz());
			fatalError9000();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
	}

protected void writePtrnRecord1300()
	{
		start1300();
	}

protected void start1300()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(ptrnIO.getStatuz());
			fatalError9000();
		}
	}

protected void beginSurdFile1400()
	{
		start1400();
	}

protected void start1400()
	{
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(atmodrec.company);
		surdclmIO.setChdrnum(chdrmjaIO.getChdrnum());
		surdclmIO.setTranno(wsaaTranno);
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");

		surdclmIO.setFormat(surdclmrec);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surdclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(surdclmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(surdclmIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(surdclmIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(surdclmIO.getTranno(),wsaaTranno)) {
			surdclmIO.setStatuz(varcom.endp);
		}
	}

protected void clearLetterWs1500()
	{
		/*START*/
		wsaaStoredLetterDetailInner.wsaaStoredLetterDetail.set(SPACES);
		/*  Initialise table numeric items in SVLT rec.*/
		for (wsaaSvltSub.set(1); !(isGT(wsaaSvltSub,10)); wsaaSvltSub.add(1)){
			svltclmIO.setEmv(wsaaSvltSub, 0);
			svltclmIO.setActvalue(wsaaSvltSub, 0);
			wsaaStoredLetterDetailInner.wsaaSvltEmv[wsaaSvltSub.toInt()].set(0);
			wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()].set(0);
		}
		wsaaSvltSub.set(ZERO);
		/*EXIT*/
	}

protected void processSurdRecords2000()
	{
		/*START*/
		readAndHoldSurd2100();
		writeAccountingEntries2200();
		if (isLTE(surdclmIO.getPlanSuffix(),chdrmjaIO.getPolsum())) {
			breakout2300();
		}
		addToSvlt5000();
		deleteSurd2400();
		readNextSurd2500();
		/*EXIT*/
	}

protected void readAndHoldSurd2100()
	{
		/*START*/
		surdclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surdclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(surdclmIO.getStatuz());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void writeAccountingEntries2200()
	{
		start2200();
	}

protected void start2200()
	{
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.rldgcoy.set(chdrmjaIO.getChdrcoy());
		lifacmvrec1.origcurr.set(chdrmjaIO.getCntcurr());
		lifacmvrec1.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec1.tranno.set(chdrmjaIO.getTranno());
		lifacmvrec1.tranref.set(chdrmjaIO.getChdrnum());
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(wsaaTransactionDate);
		lifacmvrec1.transactionTime.set(wsaaTransactionTime);
		lifacmvrec1.user.set(wsaaUser);
		lifacmvrec1.termid.set(wsaaTermid);
		lifacmvrec1.batccoy.set(chdrmjaIO.getChdrcoy());
		lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.genlcoy.set(surdclmIO.getChdrcoy());
		lifacmvrec1.effdate.set(surhclmIO.getEffdate());
		lifacmvrec1.trandesc.set(wsaaLongdesc);
		lifacmvrec1.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifacmvrec1.substituteCode[6].set(surdclmIO.getCrtable());
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		lifacmvrec1.sacscode.set(t5645rec.sacscode01);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec1.glcode.set(t5645rec.glmap01);
		lifacmvrec1.glsign.set(t5645rec.sign01);
		lifacmvrec1.contot.set(t5645rec.cnttot01);
		wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum());
		wsaaPlan.set(surdclmIO.getPlanSuffix());
		wsaaRldgLife.set(surdclmIO.getLife());
		wsaaRldgCoverage.set(surdclmIO.getCoverage());
		wsaaRldgRider.set(surdclmIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.origamt.set(surdclmIO.getEstMatValue());
		//ILIFE-1261, value will be added or subtracted depending on the table value T5645, commenting the below fix. 
		//MIBT-196 Modified to avoid incorrect sub account value
		//compute(lifacmvrec1.origamt,2).set(mult(lifacmvrec1.origamt,-1));
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec1.lifacmvRec);
			sysrSyserrRecInner.sysrStatuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		lifacmvrec1.origcurr.set(surdclmIO.getCurrcd());
		lifacmvrec1.origamt.set(surdclmIO.getActvalue());
		if (componentLevelAccounting.isTrue()) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode02);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec1.glcode.set(t5645rec.glmap02);
			lifacmvrec1.glsign.set(t5645rec.sign02);
			lifacmvrec1.contot.set(t5645rec.cnttot02);
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode03);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec1.glcode.set(t5645rec.glmap03);
			lifacmvrec1.glsign.set(t5645rec.sign03);
			lifacmvrec1.contot.set(t5645rec.cnttot03);
		}
		lifacmvrec1.rldgacct.set(SPACES);
		if (componentLevelAccounting.isTrue()) {
			wsaaRldgChdrnum.set(chdrmjaIO.getChdrnum());
			wsaaPlan.set(surdclmIO.getPlanSuffix());
			wsaaRldgLife.set(surdclmIO.getLife());
			wsaaRldgCoverage.set(surdclmIO.getCoverage());
			wsaaRldgRider.set(surdclmIO.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
		}
		else {
			lifacmvrec1.rldgacct.set(surdclmIO.getChdrnum());
		}
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec1.lifacmvRec);
			sysrSyserrRecInner.sysrStatuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		lifacmvrec1.sacscode.set(t5645rec.sacscode04);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec1.glcode.set(t5645rec.glmap04);
		lifacmvrec1.glsign.set(t5645rec.sign04);
		lifacmvrec1.contot.set(t5645rec.cnttot04);
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(surdclmIO.getChdrnum());
		lifacmvrec1.origamt.set(surdclmIO.getActvalue());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec1.lifacmvRec);
			sysrSyserrRecInner.sysrStatuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
	}

protected void breakout2300()
	{
			start2300();
		}

protected void start2300()
	{
		if (isEQ(chdrmjaIO.getPolsum(),1)
		|| isEQ(surdclmIO.getPlanSuffix(),0)) {
			return ;
		}
		brkoutrec.brkOldSummary.set(chdrmjaIO.getPolsum());
		compute(brkoutrec.brkNewSummary, 0).set(sub(surdclmIO.getPlanSuffix(),1));
		brkoutrec.brkChdrnum.set(chdrmjaIO.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrmjaIO.getChdrcoy());
		brkoutrec.brkStatuz.set(SPACES);
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(brkoutrec.outRec);
			sysrSyserrRecInner.sysrStatuz.set(brkoutrec.brkStatuz);
			fatalError9000();
		}
	}

protected void deleteSurd2400()
	{
		/*START*/
		surdclmIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surdclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(surdclmIO.getStatuz());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void readNextSurd2500()
	{
		/*START*/
		surdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surdclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(surdclmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(surdclmIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(surdclmIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(surdclmIO.getTranno(),wsaaTranno)) {
			surdclmIO.setStatuz("ENDP");
		}
		/*EXIT*/
	}

protected void deleteSurh3000()
	{
		/*DELETE-SURH-RECORD*/
		surhclmIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)
		&& isNE(surhclmIO.getStatuz(),varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surhclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(surhclmIO.getStatuz());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void updateBatchHeader3500()
	{
		/*UPDATE-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			sysrSyserrRecInner.sysrStatuz.set(batcuprec.statuz);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void releaseSoftlock4000()
	{
		start4000();
	}

protected void start4000()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(sftlockrec.sftlockRec);
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			fatalError9000();
		}
	}

protected void addToSvlt5000()
	{
		/*START*/
		wsaaSvltSub.add(1);
		if (isLTE(wsaaSvltSub,10)) {
			wsaaStoredLetterDetailInner.wsaaSvltCoverage[wsaaSvltSub.toInt()].set(surdclmIO.getCoverage());
			wsaaStoredLetterDetailInner.wsaaSvltRider[wsaaSvltSub.toInt()].set(surdclmIO.getRider());
			wsaaStoredLetterDetailInner.wsaaSvltShortds[wsaaSvltSub.toInt()].set(surdclmIO.getShortds());
			wsaaStoredLetterDetailInner.wsaaSvltType[wsaaSvltSub.toInt()].set(surdclmIO.getFieldType());
			/*   MOVE SURDCLM-RIIND    TO WSAA-SVLT-RIIND (WSAA-SVLT-SUB)    */
			wsaaStoredLetterDetailInner.wsaaSvltVirtfund[wsaaSvltSub.toInt()].set(surdclmIO.getVirtualFund());
			wsaaStoredLetterDetailInner.wsaaSvltCnstcur[wsaaSvltSub.toInt()].set(surdclmIO.getCurrcd());
			wsaaStoredLetterDetailInner.wsaaSvltEmv[wsaaSvltSub.toInt()].set(surdclmIO.getEstMatValue());
			wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()].set(surdclmIO.getActvalue());
		}
		/*EXIT*/
	}

protected void writeLetters5200()
	{
		try {
			readT66345210();
			letcRecord5220();
			svltRecord5230();
		}
		catch (GOTOException e){
		}
	}

protected void readT66345210()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(g437);
			fatalError9000();
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType,SPACES)) {
			goTo(GotoLabel.exit5299);
		}
	}

protected void letcRecord5220()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaEffdate);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrmjaIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		letrqstrec.otherKeys.set(SPACES);
		letrqstrec.function.set("ADD");
		if (isNE(tr384rec.letterType,SPACES)) {
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz,varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(letrqstrec.params);
				sysrSyserrRecInner.sysrStatuz.set(letrqstrec.statuz);
				fatalError9000();
			}
		}
	}

protected void svltRecord5230()
	{
		svltclmIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		svltclmIO.setChdrnum(chdrmjaIO.getChdrnum());
		svltAlreadyExists5300();
		/*  Move details stored in Working Storage to SVLT Record.*/
		svltclmIO.setCoverages(wsaaStoredLetterDetailInner.wsaaSvltCoverages);
		svltclmIO.setRiders(wsaaStoredLetterDetailInner.wsaaSvltRiders);
		svltclmIO.setShortdss(wsaaStoredLetterDetailInner.wsaaSvltShortdss);
		svltclmIO.setTypes(wsaaStoredLetterDetailInner.wsaaSvltTypes);
		/*  MOVE WSAA-SVLT-RIINDS           TO SVLTCLM-RIINDS.           */
		svltclmIO.setVirtfunds(wsaaStoredLetterDetailInner.wsaaSvltVirtfunds);
		svltclmIO.setCnstcurs(wsaaStoredLetterDetailInner.wsaaSvltCnstcurs);
		svltclmIO.setEmvs(wsaaStoredLetterDetailInner.wsaaSvltEmvs);
		svltclmIO.setActvalues(wsaaStoredLetterDetailInner.wsaaSvltActvalues);
		/*  Store general Contract details in SVLT record area.*/
		svltclmIO.setPlanSuffix(surhclmIO.getPlanSuffix());
		svltclmIO.setCnttype(surhclmIO.getCnttype());
		svltclmIO.setCownnum(chdrmjaIO.getCownnum());
		svltclmIO.setOccdate(chdrmjaIO.getOccdate());
		svltclmIO.setPtdate(chdrmjaIO.getPtdate());
		svltclmIO.setTransactionDate(wsaaTransactionDate);
		svltclmIO.setTransactionTime(wsaaTransactionTime);
		svltclmIO.setUser(wsaaUser);
		svltclmIO.setTermid(wsaaTermid);
		svltclmIO.setValidflag("1");
		svltclmIO.setFunction(varcom.writr);
		svltclmIO.setFormat(svltclmrec);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(svltclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(svltclmIO.getStatuz());
			fatalError9000();
		}
	}

protected void svltAlreadyExists5300()
	{
			start5300();
		}

protected void start5300()
	{
		svltclmIO.setFunction(varcom.readr);
		svltclmIO.setFormat(svltclmrec);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(),varcom.oK)
		&& isNE(svltclmIO.getStatuz(),varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(svltclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(svltclmIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(svltclmIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		/*  Record already exists, READH, update valid flag and rewrite.*/
		svltclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(svltclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(svltclmIO.getStatuz());
			fatalError9000();
		}
		svltclmIO.setValidflag("2");
		svltclmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(),varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(svltclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(svltclmIO.getStatuz());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
					fatalErrors9000();
					errorProg9000();
				}

protected void fatalErrors9000()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			return ;
		}
		sysrSyserrRecInner.sysrSyserrStatuz.set(sysrSyserrRecInner.sysrStatuz);
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void errorProg9000()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure WSAA-STORED-LETTER-DETAIL--INNER
 */
private static final class WsaaStoredLetterDetailInner {

	private FixedLengthStringData wsaaStoredLetterDetail = new FixedLengthStringData(400);
	private FixedLengthStringData wsaaSvltCoverages = new FixedLengthStringData(20).isAPartOf(wsaaStoredLetterDetail, 0);
	private FixedLengthStringData[] wsaaSvltCoverage = FLSArrayPartOfStructure(10, 2, wsaaSvltCoverages, 0);
	private FixedLengthStringData wsaaSvltRiders = new FixedLengthStringData(20).isAPartOf(wsaaStoredLetterDetail, 20);
	private FixedLengthStringData[] wsaaSvltRider = FLSArrayPartOfStructure(10, 2, wsaaSvltRiders, 0);
	private FixedLengthStringData wsaaSvltShortdss = new FixedLengthStringData(100).isAPartOf(wsaaStoredLetterDetail, 40);
	private FixedLengthStringData[] wsaaSvltShortds = FLSArrayPartOfStructure(10, 10, wsaaSvltShortdss, 0);
	private FixedLengthStringData wsaaSvltTypes = new FixedLengthStringData(10).isAPartOf(wsaaStoredLetterDetail, 140);
	private FixedLengthStringData[] wsaaSvltType = FLSArrayPartOfStructure(10, 1, wsaaSvltTypes, 0);
		/* 03  WSAA-SVLT-RIINDS.                                        */
	private FixedLengthStringData wsaaSvltVirtfunds = new FixedLengthStringData(40).isAPartOf(wsaaStoredLetterDetail, 150);
	private FixedLengthStringData[] wsaaSvltVirtfund = FLSArrayPartOfStructure(10, 4, wsaaSvltVirtfunds, 0);
	private FixedLengthStringData wsaaSvltCnstcurs = new FixedLengthStringData(30).isAPartOf(wsaaStoredLetterDetail, 190);
	private FixedLengthStringData[] wsaaSvltCnstcur = FLSArrayPartOfStructure(10, 3, wsaaSvltCnstcurs, 0);
		/* 03  WSAA-SVLT-EMVS.                                  <D96NUM>
		 03  WSAA-SVLT-ACTVALUES.                             <D96NUM>*/
	private FixedLengthStringData wsaaSvltEmvs = new FixedLengthStringData(90).isAPartOf(wsaaStoredLetterDetail, 220);
	private PackedDecimalData[] wsaaSvltEmv = PDArrayPartOfStructure(10, 17, 2, wsaaSvltEmvs, 0);
	private FixedLengthStringData wsaaSvltActvalues = new FixedLengthStringData(90).isAPartOf(wsaaStoredLetterDetail, 310);
	private PackedDecimalData[] wsaaSvltActvalue = PDArrayPartOfStructure(10, 17, 2, wsaaSvltActvalues, 0);
}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/*  System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSubrname = new FixedLengthStringData(50).isAPartOf(sysrMsgarea, 0);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
}
}
