package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6597
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6597ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(453);
	public FixedLengthStringData dataFields = new FixedLengthStringData(117).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cpstats = new FixedLengthStringData(8).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] cpstat = FLSArrayPartOfStructure(4, 2, cpstats, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(cpstats, 0, FILLER_REDEFINE);
	public FixedLengthStringData cpstat01 = DD.cpstat.copy().isAPartOf(filler,0);
	public FixedLengthStringData cpstat02 = DD.cpstat.copy().isAPartOf(filler,2);
	public FixedLengthStringData cpstat03 = DD.cpstat.copy().isAPartOf(filler,4);
	public FixedLengthStringData cpstat04 = DD.cpstat.copy().isAPartOf(filler,6);
	public FixedLengthStringData crstats = new FixedLengthStringData(8).isAPartOf(dataFields, 9);
	public FixedLengthStringData[] crstat = FLSArrayPartOfStructure(4, 2, crstats, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(crstats, 0, FILLER_REDEFINE);
	public FixedLengthStringData crstat01 = DD.crstat.copy().isAPartOf(filler1,0);
	public FixedLengthStringData crstat02 = DD.crstat.copy().isAPartOf(filler1,2);
	public FixedLengthStringData crstat03 = DD.crstat.copy().isAPartOf(filler1,4);
	public FixedLengthStringData crstat04 = DD.crstat.copy().isAPartOf(filler1,6);
	public FixedLengthStringData durmnths = new FixedLengthStringData(9).isAPartOf(dataFields, 17);
	public ZonedDecimalData[] durmnth = ZDArrayPartOfStructure(3, 3, 0, durmnths, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(9).isAPartOf(durmnths, 0, FILLER_REDEFINE);
	public ZonedDecimalData durmnth01 = DD.durmnth.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData durmnth02 = DD.durmnth.copyToZonedDecimal().isAPartOf(filler2,3);
	public ZonedDecimalData durmnth03 = DD.durmnth.copyToZonedDecimal().isAPartOf(filler2,6);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,26);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,34);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,42);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData premsubrs = new FixedLengthStringData(32).isAPartOf(dataFields, 80);
	public FixedLengthStringData[] premsubr = FLSArrayPartOfStructure(4, 8, premsubrs, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(32).isAPartOf(premsubrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData premsubr01 = DD.subr.copy().isAPartOf(filler3,0);
	public FixedLengthStringData premsubr02 = DD.subr.copy().isAPartOf(filler3,8);
	public FixedLengthStringData premsubr03 = DD.subr.copy().isAPartOf(filler3,16);
	public FixedLengthStringData premsubr04 = DD.subr.copy().isAPartOf(filler3,24);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 117);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cpstatsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] cpstatErr = FLSArrayPartOfStructure(4, 4, cpstatsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(cpstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cpstat01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData cpstat02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData cpstat03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData cpstat04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData crstatsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] crstatErr = FLSArrayPartOfStructure(4, 4, crstatsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(16).isAPartOf(crstatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData crstat01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData crstat02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData crstat03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData crstat04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData durmnthsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] durmnthErr = FLSArrayPartOfStructure(3, 4, durmnthsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(12).isAPartOf(durmnthsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData durmnth01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData durmnth02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData durmnth03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData subrsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] subrErr = FLSArrayPartOfStructure(4, 4, subrsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(16).isAPartOf(subrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData subr01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData subr02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData subr03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData subr04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 201);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData cpstatsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] cpstatOut = FLSArrayPartOfStructure(4, 12, cpstatsOut, 0);
	public FixedLengthStringData[][] cpstatO = FLSDArrayPartOfArrayStructure(12, 1, cpstatOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(48).isAPartOf(cpstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cpstat01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] cpstat02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] cpstat03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] cpstat04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData crstatsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] crstatOut = FLSArrayPartOfStructure(4, 12, crstatsOut, 0);
	public FixedLengthStringData[][] crstatO = FLSDArrayPartOfArrayStructure(12, 1, crstatOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(48).isAPartOf(crstatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] crstat01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] crstat02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] crstat03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] crstat04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData durmnthsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] durmnthOut = FLSArrayPartOfStructure(3, 12, durmnthsOut, 0);
	public FixedLengthStringData[][] durmnthO = FLSDArrayPartOfArrayStructure(12, 1, durmnthOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(36).isAPartOf(durmnthsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] durmnth01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] durmnth02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] durmnth03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData subrsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] subrOut = FLSArrayPartOfStructure(4, 12, subrsOut, 0);
	public FixedLengthStringData[][] subrO = FLSDArrayPartOfArrayStructure(12, 1, subrOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(48).isAPartOf(subrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] subr01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] subr02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] subr03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] subr04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6597screenWritten = new LongData(0);
	public LongData S6597protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6597ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(durmnth01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cpstat01Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crstat01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr01Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(durmnth02Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr02Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crstat02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cpstat02Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(durmnth03Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr03Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crstat03Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cpstat03Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(subr04Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crstat04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cpstat04Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, durmnth01, cpstat01, crstat01, premsubr01, durmnth02, premsubr02, crstat02, cpstat02, durmnth03, premsubr03, crstat03, cpstat03, premsubr04, crstat04, cpstat04};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, durmnth01Out, cpstat01Out, crstat01Out, subr01Out, durmnth02Out, subr02Out, crstat02Out, cpstat02Out, durmnth03Out, subr03Out, crstat03Out, cpstat03Out, subr04Out, crstat04Out, cpstat04Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, durmnth01Err, cpstat01Err, crstat01Err, subr01Err, durmnth02Err, subr02Err, crstat02Err, cpstat02Err, durmnth03Err, subr03Err, crstat03Err, cpstat03Err, subr04Err, crstat04Err, cpstat04Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6597screen.class;
		protectRecord = S6597protect.class;
	}

}
