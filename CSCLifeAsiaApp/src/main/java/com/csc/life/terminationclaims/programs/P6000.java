/*
 * File: P6000.java
 * Date: 30 August 2009 0:35:27
 * Author: Quipoz Limited
 * 
 * Class transformed from P6000.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.dataaccess.CovrpupTableDAM;
import com.csc.life.terminationclaims.screens.S6000ScreenVars;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcchk;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                   P6000 - Paid Up Processing Submenu
*                   ----------------------------------
*
*  The submenu program is  automatically  produced  via  Smart,
*  with additional validation required as follows:
*
*
*  Validation
*  ----------
*
*  The contract number must be entered.
*
*  The  contract  number must exist on the contract header file
*  (CHDR).
*
*  The paid to date must equal the  billed  to  date;  if  not,
*  issue an error and disallow selection.
*
*  The  billing  frequency cannot equal zeros (ie. the contract
*  is a  single  premium  only,  hence  there  are  no  premium
*  instalments  and  nothing to 'pay up'); if it does, issue an
*  error and disallow selection.
*
*  Read the Component Status  Table  (T5679)  for  the  submenu
*  transaction.   The contract must have a Contract Risk Status
*  and a Contract Premium Status that matches one of  those  on
*  the table entry.
*
*
*  Update
*  ------
*  Keep  and  softlock  the contract header record for the next
*  programs.
*
*****************************************************************
* </pre>
*/
public class P6000 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6000");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).setUnsigned();
	private String wsaaValidStatus = "";
	private String wsaaPupOk = "";
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	//ILIFE-907
//	private final String wsaaSumFlag = "";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String itemrec = "ITEMREC";
	private static final String covrpuprec = "COVRPUPREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Covers & Riders File Paid-Up Processing*/
	private CovrpupTableDAM covrpupIO = new CovrpupTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcchkrec batcchkrec = new Batcchkrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S6000ScreenVars sv = ScreenProgram.getScreenVars( S6000ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		validate2200, 
		checkScreenErrors2250, 
		verifyBatchControl2300, 
		exit2090, 
		loopCovrpup2650, 
		exit2690, 
		checkEdterror3150, 
		continue3400, 
		exit3900
	}

	public P6000() {
		super();
		screenVars = sv;
		new ScreenModel("S6000", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		sv.effdate.set(varcom.vrcmMaxDate);
		if (isEQ(wsaaToday,ZERO)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateAction2100();
				case validate2200: 
					validate2200();
				case checkScreenErrors2250: 
					checkScreenErrors2250();
				case verifyBatchControl2300: 
					verifyBatchControl2300();
					batchProgs2310();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateAction2100()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.validate2200);
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			goTo(GotoLabel.validate2200);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.verifyBatchControl2300);
		}
	}

protected void validate2200()
	{
		if (isEQ(sv.chdrsel,SPACES)) {
			sv.chdrselErr.set(errorsInner.g667);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.f259);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Validate the contract status first as all other checks are      */
		/* irrelevant if the status is incorrect.                          */
		/* Read the valid statii from table T5679.                         */
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
			headerStatuzCheck2910();
		}
		if (isNE(wsaaValidStatus,"Y")) {
			sv.chdrselErr.set(errorsInner.e717);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.action,"A")) {
			wsaaPupOk = "N";
			chkValidPup2600();
			if (isEQ(wsaaPupOk,"N")) {
				sv.chdrselErr.set(errorsInner.e658);
				goTo(GotoLabel.checkScreenErrors2250);
			}
			}
		/*     The paid to date must equal the billed to date*/
		/* Note that SUM coverages can be made paid-up when the            */
		/* paid-to is not equal to the billed-to date.                     */
		/*    MOVE 'Y'            TO WSAA-SUM-FLAG.                   <006>*/
		/*    PERFORM 8100-CHECK-SUM-CONTRACT                         <006>*/
		/*    IF     WSAA-SUM-FLAG NOT = SPACE                     <LA4754>*/
		/*       AND S6000-EFFDATE     = VRCM-MAX-DATE             <LA4754>*/
		/*           PERFORM 8200-CHECK-SUM-EFFDATE.               <LA4754>*/
		if (isNE(chdrmjaIO.getBtdate(), chdrmjaIO.getPtdate()))
		//ILIFE-907
//		&& isEQ(wsaaSumFlag, SPACES)) 
		{
			sv.chdrselErr.set(errorsInner.h013);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO 2900-EXIT.                                             */
			goTo(GotoLabel.exit2090);
		}
		/*     The billing frequency cannot equal zeros*/
		if (isEQ(chdrmjaIO.getBillfreq(), "00")) {
			sv.chdrselErr.set(errorsInner.h014);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO 2900-EXIT.                                             */
			goTo(GotoLabel.exit2090);
		}
		/* Perform the check earlier to avoid DATCON errors when using a   */
		/* PTDATE of 0.                                                    */
		/*Read the valid statii from table T5679.                         */
		/* MOVE SPACES                 TO ITEM-PARAMS.                  */
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/* MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.                 */
		/* MOVE T5679                  TO ITEM-ITEMTABL.                */
		/* MOVE SUBP-TRANSCD           TO ITEM-ITEMITEM.                */
		/* MOVE ITEMREC                TO ITEM-FORMAT.                  */
		/* MOVE READR                  TO ITEM-FUNCTION.                */
		/* CALL 'ITEMIO' USING ITEM-PARAMS.                             */
		/* IF ITEM-STATUZ              NOT = O-K                        */
		/*    MOVE ITEM-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* MOVE ITEM-GENAREA           TO T5679-T5679-REC.              */
		/* MOVE 'N'                    TO WSAA-VALID-STATUS.            */
		/* PERFORM 2910-HEADER-STATUZ-CHECK                             */
		/*         VARYING WSAA-INDEX FROM 1 BY 1                       */
		/*                           UNTIL WSAA-INDEX > 12              */
		/*                           OR WSAA-VALID-STATUS = 'Y'.        */
		/* IF WSAA-VALID-STATUS        NOT = 'Y'                        */
		/*    MOVE E717                TO S6000-CHDRSEL-ERR.            */
		/* IF SCRN-ACTION              = 'A'                    <LA5125>*/
		/*     MOVE 'N'                TO WSAA-PUP-OK           <LA5125>*/
		/*     PERFORM 2600-CHK-VALID-PUP                       <LA5125>*/
		/*     IF  WSAA-PUP-OK         = 'N'                    <LA5125>*/
		/*         MOVE E658           TO S6000-CHDRSEL-ERR     <LA5125>*/
		/*     END-IF                                           <LA5125>*/
		/* END-IF.                                              <LA5125>*/
		/*     MOVE 'N'            TO WSAA-PUP-OK.                         */
		/*     PERFORM 2600-CHK-VALID-PUP.                                 */
		/*     IF  WSAA-PUP-OK      = 'N'                                  */
		/*         MOVE E658       TO S6000-CHDRSEL-ERR.                   */
		/* Set 'N' to WSSP-FLAG where contract has already broken out.*/
		/* The user is not allowed to process the whole-plan.*/
		if (isNE(chdrmjaIO.getPolsum(),chdrmjaIO.getPolinc())) {
			wsspcomn.flag.set("N");
		}
		/* Effective date should be equal to paid-to-date date for the     */
		/* contract.                                                       */
		/* If left blank then default to paid-to-date date for the         */
		/* contract.                                                       */
		if (isEQ(sv.effdate,varcom.vrcmMaxDate)) {
			sv.effdate.set(chdrmjaIO.getPtdate());
			goTo(GotoLabel.checkScreenErrors2250);
		}
		if (isNE(sv.effdate,chdrmjaIO.getPtdate()))
		//ILIFE-907
//		&& isEQ(wsaaSumFlag,SPACES)) { 
		{
			sv.effdateErr.set(errorsInner.t018);
		}
	}

protected void checkScreenErrors2250()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    GO TO 2900-EXIT.                                             */
		goTo(GotoLabel.exit2090);
	}

protected void verifyBatchControl2300()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(errorsInner.e073);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2900-EXIT.                                         */
			goTo(GotoLabel.exit2090);
		}
		bcbprogrec.transcd.set(subprogrec.transcd);
	}

protected void batchProgs2310()
	{
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2900-EXIT.                                         */
			return ;
		}
		wsspcomn.secProg[1].set(bcbprogrec.nxtprog1);
		wsspcomn.secProg[2].set(bcbprogrec.nxtprog2);
		wsspcomn.secProg[3].set(bcbprogrec.nxtprog3);
		wsspcomn.secProg[4].set(bcbprogrec.nxtprog4);
	}

protected void chkValidPup2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readCovrpup2610();
				case loopCovrpup2650: 
					loopCovrpup2650();
					readT56872670();
				case exit2690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCovrpup2610()
	{
		covrpupIO.setParams(SPACES);
		covrpupIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrpupIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrpupIO.setPlanSuffix(ZERO);
		covrpupIO.setFormat(covrpuprec);
		covrpupIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrpupIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrpupIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
 
	}

protected void loopCovrpup2650()
	{
		SmartFileCode.execute(appVars, covrpupIO);
		if (isNE(covrpupIO.getStatuz(),varcom.oK)
		&& isNE(covrpupIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrpupIO.getParams());
			fatalError600();
		}
		if (isNE(covrpupIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(covrpupIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(covrpupIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2690);
		}
	}

protected void readT56872670()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrpupIO.getCrtable());
		itdmIO.setItmfrm(chdrmjaIO.getPtdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covrpupIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covrpupIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.f294);
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (isNE(t5687rec.pumeth,SPACES)) {
			wsaaPupOk = "Y";
			return ;
		}
		covrpupIO.setFunction(varcom.nextr);
		goTo(GotoLabel.loopCovrpup2650);
	}

	/**
	* <pre>
	*     CHECK THE CONTRACT HEADER STATUS AGAINST T5679.
	* </pre>
	*/
protected void headerStatuzCheck2910()
	{
		/*HEADER-STATUZ-CHECK*/
		/*IF T5679-CN-RISK-STAT(WSAA-INDEX) NOT = SPACE                */
		/*    IF (T5679-CN-RISK-STAT(WSAA-INDEX) = CHDRMJA-STATCODE)   */
		/*   AND (T5679-CN-PREM-STAT(WSAA-INDEX) = CHDRMJA-PSTATCODE)  */
		/*        MOVE 'Y'            TO WSAA-VALID-STATUS             */
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*        GO TO 2990-EXIT.                                     */
		/*        GO TO 2090-EXIT.                                     */
		if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()],chdrmjaIO.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
				if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()],chdrmjaIO.getPstatcode())) {
					wsaaValidStatus = "Y";
				}
			}
		}
		/*EXIT1*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3100();
				case checkEdterror3150: 
					checkEdterror3150();
					updateBatchControl3200();
					openNewBatch3300();
				case continue3400: 
					continue3400();
				case exit3900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3100()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		wsspcomn.currfrom.set(sv.effdate);
		/*  Update WSSP Key details*/
		if (isEQ(scrnparams.action,"I")
		|| isEQ(scrnparams.action,"J")
		|| isEQ(scrnparams.action,"Q")) {
			wsspcomn.flag.set("I");
		}
		/*  Keep the contract header record.*/
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/* Do not lock if enquiry                                          */
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.checkEdterror3150);
		}
		/* Softlock the contract header record.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S6000-CHDRSEL          TO SFTL-ENTITY.                  */
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		if (isEQ(scrnparams.deviceInd,"*RMT")) {
			goTo(GotoLabel.checkEdterror3150);
		}
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
		}
	}

protected void checkEdterror3150()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3900);
		}
		if (isNE(subprogrec.bchrqd,"Y")) {
			goTo(GotoLabel.exit3900);
		}
	}

protected void updateBatchControl3200()
	{
		batcchkrec.batcchkRec.set(SPACES);
		batcchkrec.function.set("CHECK");
		batcchkrec.batchkey.set(wsspcomn.batchkey);
		batcchkrec.tranid.set(wsspcomn.tranid);
		callProgram(Batcchk.class, batcchkrec.batcchkRec);
		if (isEQ(batcchkrec.statuz,varcom.oK)) {
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			goTo(GotoLabel.exit3900);
		}
		if (isEQ(batcchkrec.statuz,varcom.dupr)) {
			sv.actionErr.set(errorsInner.e132);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3900);
		}
		if (isEQ(batcchkrec.statuz,"INAC")) {
			batcdorrec.function.set("ACTIV");
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			goTo(GotoLabel.continue3400);
		}
		if (isNE(batcchkrec.statuz,varcom.mrnf)) {
			sv.actionErr.set(batcchkrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3900);
		}
	}

protected void openNewBatch3300()
	{
		batcdorrec.function.set("AUTO");
	}

protected void continue3400()
	{
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
			wsspcomn.edterror.set("Y");
			rollback();
			return ;
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.set(1);
		/*EXIT*/
		/**                                                            <006>*/
		/** Keep the SUM additions seperate.                                */
		/**8100-CHECK-SUM-CONTRACT SECTION.                            <006>*/
		/** Loop round all the coverages and FLAG as a SUM contract if      */
		/** all the coverages use SUM ..........                            */
		/**8110-START.                                                 <006>*/
		/**     MOVE SPACES                TO COVRPUP-PARAMS.          <006>*/
		/**     MOVE CHDRMJA-CHDRCOY       TO COVRPUP-CHDRCOY.         <006>*/
		/**     MOVE CHDRMJA-CHDRNUM       TO COVRPUP-CHDRNUM.         <006>*/
		/**     MOVE ZEROES                TO COVRPUP-PLAN-SUFFIX.     <006>*/
		/**     MOVE COVRPUPREC            TO COVRPUP-FORMAT.          <006>*/
		/**     MOVE BEGN                  TO COVRPUP-FUNCTION.        <006>*/
		/**                                                            <006>*/
		/**8120-LOOP-COVRPUP.                                          <006>*/
		/**    CALL 'COVRPUPIO' USING COVRPUP-PARAMS.                  <006>*/
		/**                                                            <006>*/
		/**    IF COVRPUP-STATUZ NOT = O-K AND NOT = ENDP              <006>*/
		/**       MOVE COVRPUP-PARAMS      TO SYSR-PARAMS              <006>*/
		/**       MOVE COVRPUP-STATUZ      TO SYSR-STATUZ              <006>*/
		/**       PERFORM 600-FATAL-ERROR.                             <006>*/
		/**                                                            <006>*/
		/**    IF    COVRPUP-STATUZ      = ENDP                        <006>*/
		/**       OR COVRPUP-CHDRCOY NOT = CHDRMJA-CHDRCOY             <006>*/
		/**       OR COVRPUP-CHDRNUM NOT = CHDRMJA-CHDRNUM             <006>*/
		/**          GO TO 8190-EXIT.                                  <006>*/
		/**                                                            <006>*/
		/** Check if the coverage uses the calculation package SUM.         */
		/** All coverages on the contract must use SUM if SUM processing    */
		/** is to be applicable. Note that flag set to non-space denotes    */
		/** a SUM product & a single coverage outside will set to space.    */
		/**    IF WSAA-SUM-FLAG NOT = SPACE                            <006>*/
		/**       MOVE SPACES              TO ITEM-PARAMS              <006>*/
		/**       MOVE 'IT'                TO ITEM-ITEMPFX             <006>*/
		/**       MOVE T5602               TO ITEM-ITEMTABL            <006>*/
		/**       MOVE COVRPUP-CHDRCOY     TO ITEM-ITEMCOY             <006>*/
		/**       MOVE COVRPUP-CRTABLE     TO ITEM-ITEMITEM            <006>*/
		/**       MOVE ITEMREC             TO ITEM-FORMAT              <006>*/
		/**       MOVE READR               TO ITEM-FUNCTION            <006>*/
		/**       CALL 'ITEMIO' USING ITEM-PARAMS                      <006>*/
		/**                                                            <006>*/
		/**       IF ITEM-STATUZ NOT = O-K                             <006>*/
		/**          MOVE SPACE            TO WSAA-SUM-FLAG.           <006>*/
		/**          GO TO 8190-EXIT.                                  <006>*/
		/**                                                            <006>*/
		/**    MOVE NEXTR                  TO COVRPUP-FUNCTION.        <006>*/
		/**    GO TO 8120-LOOP-COVRPUP.                                <006>*/
		/**                                                            <006>*/
		/**8190-EXIT.                                                  <006>*/
		/**    EXIT.                                                   <006>*/
		/**                                                            <006>*/
		/**8200-CHECK-SUM-EFFDATE SECTION.                             <006>*/
		/**8210-START.                                                 <006>*/
		/** Set the default date for a SUM product ......                   */
		/**    MOVE CHDRMJA-PTDATE        TO S6000-EFFDATE.            <006>*/
		/**    MOVE SPACES                TO DTC2-DATCON2-REC.         <006>*/
		/**    MOVE '12'                  TO DTC2-FREQUENCY.           <006>*/
		/**                                                            <006>*/
		/** As of 26/4/95 default to the paid to date if in the future.<006>*/
		/*************IF S6000-EFFDATE > WSAA-TODAY                         */
		/*************** MOVE -1           TO DTC2-FREQ-FACTOR              */
		/*************** PERFORM 8300-GET-EFFDATE UNTIL                     */
		/***************      S6000-EFFDATE < WSAA-TODAY                    */
		/*************** MOVE DTC2-INT-DATE-1 TO S6000-EFFDATE              */
		/*************ELSE                                                  */
		/**              IF S6000-EFFDATE < WSAA-TODAY              <LA4754>*/
		/**                 MOVE 1         TO DTC2-FREQ-FACTOR      <LA4754>*/
		/**                 PERFORM 8300-GET-EFFDATE UNTIL          <LA4754>*/
		/**                         S6000-EFFDATE > WSAA-TODAY.     <LA4754>*/
		/**8290-EXIT.                                               <LA4754>*/
		/**    EXIT.                                                <LA4754>*/
		/**                                                         <LA4754>*/
		/**8300-GET-EFFDATE SECTION.                                   <006>*/
		/**8310-START.                                                 <006>*/
		/** Use DATCON2 to add or subtract a month until the date is less   */
		/** than or greater than today's date ........                      */
		/** Where it is less than take the last date, where greater than    */
		/** use the higher date returned ...                                */
		/**                                                            <006>*/
		/**    MOVE S6000-EFFDATE          TO DTC2-INT-DATE-1.         <006>*/
		/**    MOVE ZERO                   TO DTC2-INT-DATE-2.         <006>*/
		/**                                                            <006>*/
		/**    CALL 'DATCON2' USING DTC2-DATCON2-REC.                  <006>*/
		/**    IF DTC2-STATUZ NOT = O-K                                <006>*/
		/**       MOVE DTC2-DATCON2-REC    TO SYSR-PARAMS              <006>*/
		/**       MOVE DTC2-STATUZ         TO SYSR-STATUZ              <006>*/
		/**       PERFORM 600-FATAL-ERROR.                             <006>*/
		/**                                                            <006>*/
		/** Move the new date to the screen effective date....         <006>*/
		/**    MOVE DTC2-INT-DATE-2        TO S6000-EFFDATE.           <006>*/
		/**                                                            <006>*/
		/**8390-EXIT.                                                  <006>*/
		/**    EXIT.                                                   <006>*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e132 = new FixedLengthStringData(4).init("E132");
	private FixedLengthStringData e717 = new FixedLengthStringData(4).init("E717");
	private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
	private FixedLengthStringData g667 = new FixedLengthStringData(4).init("G667");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData h013 = new FixedLengthStringData(4).init("H013");
	private FixedLengthStringData h014 = new FixedLengthStringData(4).init("H014");
	private FixedLengthStringData e658 = new FixedLengthStringData(4).init("E658");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData t018 = new FixedLengthStringData(4).init("T018");
	}
}
