package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5239
 * @version 1.0 generated on 30/08/09 06:38
 * @author Quipoz
 */
public class S5239ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(193);
	public FixedLengthStringData dataFields = new FixedLengthStringData(97).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData hselect = DD.hselect.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 97);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData hselectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 121);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] hselectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(179);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(65).isAPartOf(subfileArea, 0);
	public FixedLengthStringData hflag = DD.hflag.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData hrrn = DD.hrrn.copyToZonedDecimal().isAPartOf(subfileFields,1);
	public ZonedDecimalData pymt = DD.pymt.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public ZonedDecimalData rgpynum = DD.rgpynum.copyToZonedDecimal().isAPartOf(subfileFields,27);
	public FixedLengthStringData rgpytype = DD.rgpytype.copy().isAPartOf(subfileFields,32);
	public FixedLengthStringData rptldesc = DD.rptldesc.copy().isAPartOf(subfileFields,34);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,64);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 65);
	public FixedLengthStringData hflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData hrrnErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData pymtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData rgpynumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData rgpytypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData rptldescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 93);
	public FixedLengthStringData[] hflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] hrrnOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] pymtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] rgpynumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] rgpytypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] rptldescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 177);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5239screensflWritten = new LongData(0);
	public LongData S5239screenctlWritten = new LongData(0);
	public LongData S5239screenWritten = new LongData(0);
	public LongData S5239protectWritten = new LongData(0);
	public GeneralTable s5239screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData scflag = new FixedLengthStringData(1);

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5239screensfl;
	}

	public S5239ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"02","04","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hselectOut,new String[] {null, null, null, "03",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {rgpynum, pymt, rgpytype, rptldesc, hrrn, hflag, select};
		screenSflOutFields = new BaseData[][] {rgpynumOut, pymtOut, rgpytypeOut, rptldescOut, hrrnOut, hflagOut, selectOut};
		screenSflErrFields = new BaseData[] {rgpynumErr, pymtErr, rgpytypeErr, rptldescErr, hrrnErr, hflagErr, selectErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cownnum, cnttype, ownername, ctypedes, hselect};
		screenOutFields = new BaseData[][] {chdrnumOut, cownnumOut, cnttypeOut, ownernameOut, ctypedesOut, hselectOut};
		screenErrFields = new BaseData[] {chdrnumErr, cownnumErr, cnttypeErr, ownernameErr, ctypedesErr, hselectErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5239screen.class;
		screenSflRecord = S5239screensfl.class;
		screenCtlRecord = S5239screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5239protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5239screenctl.lrec.pageSubfile);
	}
}
