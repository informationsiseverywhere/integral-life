package com.csc.life.terminationclaims.batchprograms;


import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.LetcpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.model.Letcpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.life.terminationclaims.dataaccess.dao.MatdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MathpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MatypfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Matdpf;
import com.csc.life.terminationclaims.dataaccess.model.Mathpf;
import com.csc.life.terminationclaims.dataaccess.model.Matypf;
import com.csc.life.terminationclaims.reports.Rr59hReport;
import com.csc.smart.recordstructures.Batckey;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.T2634rec;
import com.csc.fsu.printing.tablestructures.Tr383rec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.terminationclaims.recordstructures.Matpcpy;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;

import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Br59h extends Mainb {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	/* ERRORS */
	protected static final String e652 = "E652";

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR59H");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	/*
	 * These fields are required by MAINB processing and should not be deleted.
	 */
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	protected FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	protected PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0).setUnsigned();
	protected PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	/* WSAA-TRANID */
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4);
	protected ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	protected PackedDecimalData wsaaChdrlifTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaMatyFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaMatyFn, 0, FILLER).init("MATY");
	private FixedLengthStringData wsaaMatyRunid = new FixedLengthStringData(2).isAPartOf(wsaaMatyFn, 4);
	private ZonedDecimalData wsaaMatyJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaMatyFn, 6).setUnsigned();
    private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	protected FixedLengthStringData wsaaMaturePlan = new FixedLengthStringData(1).init("Y");
	private Validator matureWholePlan = new Validator(wsaaMaturePlan, "Y");

	protected FixedLengthStringData wsaaFullyMatured = new FixedLengthStringData(1).init(SPACES);
	protected FixedLengthStringData wsaaBatchTrancde = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaUnitdTrancde = new FixedLengthStringData(4).init("B633");
	/*ILIFE-3331 Starts*/
	protected FixedLengthStringData wsaaOkey = new FixedLengthStringData(29);
	protected PackedDecimalData wsaaOkey1 = new PackedDecimalData(4, 0).isAPartOf(wsaaOkey, 0);
	/*ILIFE-3331 ends */
	protected Validator fullyMatured = new Validator(wsaaFullyMatured, "Y");
	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	protected Validator endOfFile = new Validator(wsaaEof, "Y");
	protected Rr59hReport printerFile = new Rr59hReport();
	protected String matyTableName;
	protected String wsaamathclmFound="Y";
	protected FixedLengthStringData wsaaLetterType=new FixedLengthStringData(8);
	 /*ILIFE-3335 Starts*/
	protected FixedLengthStringData wsaaLetterStatus=new FixedLengthStringData(1);
	 /*ILIFE-3335 Ends*/
	private int wsaaCount;
	protected int wsaaPlnsfx;
	protected BigDecimal wsaaClmamt;
	protected BigDecimal wsaaInstpremTot = new BigDecimal(0.00);
	 /*ILIFE-3335 Starts*/
	protected boolean printPendingLetter = false;
	private int wsaaAcmvCount=0;
	 /*ILIFE-3335 Ends*/
	private static final String ivrm = "IVRM";
	
	/*  Control indicators*/
	protected FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	protected Validator newPageReq = new Validator(wsaaOverflow, "Y");


	/* Main, standard page headings */
	protected FixedLengthStringData rr59hH01 = new FixedLengthStringData(93);
	private FixedLengthStringData rr59hh01O = new FixedLengthStringData(93).isAPartOf(rr59hH01, 0);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr59hh01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr59hh01O, 2);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr59hh01O, 3);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr59hh01O, 13);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr59hh01O, 43);

	protected Rr59hD01Inner rr59hD01Inner = new Rr59hD01Inner();
	
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
	
	protected FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	/* CONTROL-TOTALS */
	private static final int ct01 = 1;

	private Batckey wsaaBatckey = new Batckey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Matccpy matccpy = new Matccpy();

	protected T5534rec t5534rec = new T5534rec();
	protected T5679rec t5679rec = new T5679rec();
	protected T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	protected Tr384rec tr384rec = new Tr384rec();
	protected Tr383rec tr383rec = new Tr383rec();
	protected T2634rec t2634rec = new T2634rec();
	private Trmreasrec trmreasrec = new Trmreasrec();
	protected Matpcpy matpcpy = new Matpcpy();
	private Brkoutrec brkoutrec = new Brkoutrec();
	protected Letrqstrec letrqstrec = new Letrqstrec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	private P6671par p6671par = new P6671par();
	protected Varcom varcom =new Varcom();

	private Map<String, List<Itempf>> t5534Map = null;
	private List<Itempf> t5679List = null;
	private Map<String, List<Itempf>> t5679Map = null;
	private Map<String, List<Itempf>> t5687Map = null;
	private Map<String, List<Itempf>> t6598Map = null;
	private List<Itempf> tr384List = null;
	private Map<String, List<Itempf>> t2634Map = null;
	protected Map<String, List<Itempf>> tr383Map = null;

	protected Iterator<Matypf> iteratorList;

	protected Chdrpf chdrlif;
	protected Chdrpf chdrlifUpdate;
	protected Payrpf payrUpdate;
	private Covrpf covrincr;
	protected Covrpf covrmatins;
	protected Incrpf incr;
	protected Matypf matypf;
	protected Payrpf payrpf;
	
	private Matdpf matdpf;
	protected Lifepf insertlifedata;
	protected Ptrnpf ptrnpf;
	protected Acmvpf acmvpf;
	protected Letcpf letc;
	private Hpadpf hpad;
	private Clntpf clntpf;
	
	private List<Lifepf> lifepfList = new ArrayList<Lifepf>();
	private List<Covrpf> covrmatList = new ArrayList<Covrpf>();
	protected List<Payrpf> updatePayrList = new ArrayList<Payrpf>();
	protected List<Chdrpf> updateChdrlifList =  new ArrayList<Chdrpf>();
	private List<Covrpf> updateCovrpflist = new ArrayList<Covrpf>();
	protected List<Lifepf> updateLifepflist =  new ArrayList<Lifepf>();
	protected List<Letcpf> pendingLetcList = new ArrayList<Letcpf>();
	protected List<Letcpf> Letcpflist= new ArrayList<Letcpf>();
	protected List<Letrqstrec> letrqstList = new ArrayList<Letrqstrec>();
	

	protected List<Matypf> matypfList = new ArrayList<Matypf>();
	protected List<Mathpf> mathclmList = new ArrayList<Mathpf>();
	private List<Incrpf> incrmjaList = new ArrayList<Incrpf>();
	private List<Matdpf> matdclmList = new ArrayList<Matdpf>();														
	
	protected List<Incrpf> insertIncrList = new ArrayList<Incrpf>();
	protected List<Covrpf> insertCovrpfList = new ArrayList<Covrpf>();
	protected List<Chdrpf> insertChdrpfList = new ArrayList<Chdrpf>();
	protected List<Payrpf> insertPayrpfList = new ArrayList<Payrpf>();
	protected List<Lifepf> insertLifepflist = new ArrayList<Lifepf>();
	protected List<Ptrnpf> insertPtrnList = new ArrayList<Ptrnpf>();

	private List<Descpf> t1693List;
	private List<Descpf> t1692List;
	private List<Descpf> t5687List;

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	protected MatypfDAO matypfDAO = getApplicationContext().getBean("matypfDAO", MatypfDAO.class);
	private MathpfDAO mathpfDAO = getApplicationContext().getBean("mathpfDAO", MathpfDAO.class);
	protected IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private MatdpfDAO matdpfDAO = getApplicationContext().getBean("matdpfDAO", MatdpfDAO.class);
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	protected PayrpfDAO payrDAO = getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	protected PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	protected AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);
	protected LetcpfDAO letcpfDAO = getApplicationContext().getBean("letcpfDAO", LetcpfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);

	private static final Logger LOGGER = LoggerFactory.getLogger(Br59h.class);

	public Br59h() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	@Override
	protected FixedLengthStringData getLsaaStatuz() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		// TODO Auto-generated method stub

	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);

		try {
			super.mainline();

		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/* RESTART */
		/* EXIT */

	}

	protected void initialise1000() {
		
		initialise1100();
		setUpHeadingCompany1001();
		setUpHeadingBranch1002();
		setUpHeadingDates1003();

	}

	

	protected void initialise1100() {
		wsspEdterror.set(varcom.oK);
		wsaaFullyMatured.set("Y");
		wsaaTransactionDate.set(datcon1rec.intDate);
		wsaaTransactionTime.set(getCobolTime());
		varcom.vrcmTranid.set(batcdorrec.tranid);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		 /*ILIFE-3335 Starts*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaChdrnumFrom.set(p6671par.chdrnum);
		wsaaChdrnumTo.set(p6671par.chdrnum1);
		if (isEQ(p6671par.chdrnum, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
		}
		if (isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumTo.set(HIVALUE);
		}
		 /*ILIFE-3335 Ends*/
		wsaaEffdate.set(bsscIO.getEffectiveDate().toString());
		printerFile.openOutput();
		/* Check the restart method is compatible with the program. */
		/* This program is restartable but will always re-run from */
		/* the beginning. This is because the extract file, GEXT, is */
		/* not under commitment control. */
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsaaBatchTrancde.set(bprdIO.getAuthCode());
		wsaaMatyRunid.set(bprdIO.getSystemParam04());
		wsaaMatyJobno.set(bsscIO.getScheduleNumber());
		wsaaBatckey.set(batcdorrec.batchkey);
		matyTableName = wsaaMatyFn.trim();
		wsaaCount = matypfDAO.getMatyCount(matyTableName);
		
		String coy = bsprIO.getCompany().toString();
		String pfx = smtpfxcpy.item.toString();
		
		t5679Map = itemDAO.loadSmartTable(pfx, coy, "T5679");
		t5679List = t5679Map.get(bprdIO.getAuthCode().toString());
		
	        if (t5679List == null || t5679List.size() == 0) {
	            syserrrec.params.set("T5679" + bprdIO.getAuthCode());
	            fatalError600();
	        }
	        t5679rec.t5679Rec.set(StringUtil.rawToString(t5679List.get(0).getGenarea()));
	   
		t5687Map = itemDAO.loadSmartTable(pfx, coy, "T5687");
		t5534Map = itemDAO.loadSmartTable(pfx, coy, "T5534");
		t6598Map = itemDAO.loadSmartTable(pfx, coy, "T6598");
		
		tr384List = itemDAO.getItemsbyItemcodeLikeOperator(pfx, coy, "TR384","***BRFS");
		if (tr384List == null || tr384List.size() == 0) {
            syserrrec.params.set("T5679" + bprdIO.getAuthCode());
            fatalError600();
        }
        tr384rec.tr384Rec.set(StringUtil.rawToString(tr384List.get(0).getGenarea()));
        
		tr383Map = itemDAO.loadSmartTable(pfx, coy, "TR383");
		t2634Map = itemDAO.loadSmartTable(pfx, coy, "T2634");

	}

	

	protected void setUpHeadingCompany1001() {
		// Read DESCPF for an item bsprIO.getCompany() and bsscio. Language from
		// T1693
		t1693List = descDAO.getItemByDescItem(smtpfxcpy.item.toString(), "0", "T1693",bsprIO.getCompany().toString(), 
				bsscIO.language.toString());
		boolean itemFound = false;
		for (Descpf descItem : t1693List) {
			if (descItem.getDescitem().trim().equals(bsprIO.getCompany().toString().trim())) {
				rh01Company.set(bsprIO.getCompany());
				rh01Companynm.set(descItem.getLongdesc());
				itemFound = true;
				break;
			}
		}
		if (!itemFound) {
			fatalError600();
		}

	}

	protected void setUpHeadingBranch1002() {
		// Read DESCPF for an item bprdIO.getDefaultBranch() and language -
		// bsscio. Language from T1692 and company bsprIO.getCompany()
		t1692List = descDAO.getItemByDescItem(smtpfxcpy.item.toString(), bsprIO.getCompany().toString(),"T1692",
				bprdIO.getDefaultBranch().toString(),  bsscIO.language.toString());
		boolean itemFound = false;
		for (Descpf descItem : t1692List) {
			if (descItem.getDescitem().trim().equals(bprdIO.getDefaultBranch().toString().trim())) {

				rh01Branch.set(bprdIO.getDefaultBranch());
				rh01Branchnm.set(descItem.getLongdesc());
				itemFound = true;
				break;
			}
		}
		if (!itemFound) {
			fatalError600();
		}

	}

	protected void setUpHeadingDates1003() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
	}

	protected void readFile2000() {
		if (wsaaCount > 0) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(wsaaCount);
			callProgram(Contot.class, contotrec.contotRec);
			wsspEdterror.set(varcom.oK);
			wsaaCount = 0;
			wsaaEof.set("N");

		} else {
			 /*ILIFE-3335 Starts*/
			if(!printPendingLetter)
			{
				printPendingLetter = TRUE;
				wsspEdterror.set(varcom.oK);
			}
			else
			wsspEdterror.set(varcom.endp);
			 /*ILIFE-3335 Ends*/
	}
}

	protected void edit2500() {
		wsspEdterror.set(varcom.oK);
	}

	@Override
	protected void update3000() {
		 /*ILIFE-3335 Starts*/
		if(printPendingLetter){
			PrintPendingLetters();
		}
		else{
			 /*ILIFE-3335 Ends*/
		matypfList = matypfDAO.getMatypfList(matyTableName); 
		iteratorList = matypfList.iterator();
		readMatyData3100();
		wsaaChdrChdrnum.set(SPACES);
		while (!endOfFile.isTrue()) {
			while ((isNE(matypf.getChdrnum(), wsaaChdrChdrnum))) {
				wsaaChdrChdrcoy.set(matypf.getChdrcoy());
				wsaaChdrChdrnum.set(matypf.getChdrnum());
				lockContract();
				initialisation1000();
				/* Continue processing until contract break. */

				if (isEQ(wsaamathclmFound, "Y")) {
					
					for (Mathpf mathclm : mathclmList) {
						if (isEQ(mathclm.getPlnsfx(), ZERO)) {
							wsaaMaturePlan.set("Y");
							matureWholeContract2000(mathclm);
							wsaaClmamt=mathclm.getClamamt();
							wsaaPlnsfx=mathclm.getPlnsfx();
						} else {
							wsaaMaturePlan.set("N");
							matureSelectedPolicies2500();
						}
						break;
					}
					writeDetailsReport4700();
					updateContractHeader4500();
					if (fullyMatured.isTrue()) {
						processLives4000();
					}
					writeLetcRecord5000();
					writePtrnTransaction6000();
					releaseSoftlock8000();
				}
				readMatyData3100();
			}
		 }
		 /*ILIFE-3335 Starts*/
		sendSMSnotification3200();
	   }
		 /*ILIFE-3335 Ends*/
	}

	protected void readMatyData3100() {
		if (iteratorList.hasNext()) {
			matypf = new Matypf();
			matypf = iteratorList.next();
		} else {
			wsaaEof.set("Y");
		}
	}

	protected void lockContract() {
		sftlockrec.function.set("LOCK");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(wsaaChdrChdrnum);
		sftlockrec.transaction.set(wsaaBatchTrancde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK) && isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	protected void initialisation1000() {
		updateContractPayrHeader1050();
		getFirstMathclm1200();
		pendingIncrease1300();
	}

	protected void updateContractPayrHeader1050() {
		chdrlifUpdate = new Chdrpf();

		getChdrpfRec();

		// Read CHDRPF, include wsaaChdrChdrnum, wsaaChdrChdrcoy, SERVUNIT =
		// 'LP' in where condition and retrieve a record and store in
		// chdrlifupdate.
		if (chdrlifUpdate == null) {
			syserrrec.params.set(wsaaChdrChdrnum);
			/* once check */ // syserrrec.params.set(chdrlifupdate.getparams());
			fatalError600();
		}
		compute(wsaaChdrlifTranno, 0).set(add(chdrlifUpdate.getTranno(), 1));
		chdrlifUpdate.setValidflag('2');
		chdrlifUpdate.setCurrto(wsaaEffdate.toInt());
		updateChdrlifList.add(chdrlifUpdate);

		chdrlif = new Chdrpf(chdrlifUpdate);
		chdrlif.setTranno(wsaaChdrlifTranno.toInt());
		chdrlif.setValidflag('1');
		chdrlif.setCurrfrom(wsaaEffdate.toInt());
		chdrlif.setCurrto(varcom.vrcmMaxDate.toInt());

		payrUpdate = new Payrpf();

		payrUpdate = payrDAO.getpayrRecord(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString());
		if (payrUpdate == null) {
			/* once check */ // syserrrec.params.set(chdrlifupdate.getparams());
			fatalError600();
		}
		payrUpdate.setValidflag("2");
		updatePayrList.add(payrUpdate);

		payrpf = new Payrpf(payrUpdate);
		payrpf.setTranno(wsaaChdrlifTranno.toInt());
		payrpf.setValidflag("1");
		payrpf.setEffdate(wsaaEffdate.toInt());

	}

	protected void getChdrpfRec() {
		chdrlifUpdate = chdrpfDAO.getchdrRecordservunit(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString());


	}

	protected void getFirstMathclm1200() {
		mathclmList = mathpfDAO.getmathclmRecordList(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString(),
				wsaaChdrlifTranno.toInt());
		if (mathclmList == null) {
			wsaamathclmFound = "N";
		}
	}

	protected void pendingIncrease1300() {
		incrmjaList = incrpfDAO.getIncrpfList(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString());
		
		for (Incrpf incrdata : incrmjaList) {
			resetCovrIndexation1400(incrdata);
			/*
			 * Delete query Write a dao to delete this record from IN CRPD.Keep
			 * unique number;
			 */
         incrpfDAO.deleteIncrpf(incrdata.getUniqueNumber());
		}
	}
	

	protected void resetCovrIndexation1400(Incrpf incrdata) {
		covrincr = new Covrpf();
		// Read COVRPF, include incrdate.CHDRNUM, incrdate .CHDRCOY,
		// incrdate.LIFE, incrdate.COVERAGE, incrdate.RIDER,
		// incrdate.PlanSuffix, VALIDFLAG != 2 in where condition and retrieve a
		// record and store in covrincr (object of COVRPF).
		covrincr = covrpfDAO.getcovrincrRecord(incrdata.getChdrcoy(), incrdata.getChdrnum(), incrdata.getLife(),
				incrdata.getCoverage(), incrdata.getRider(), incrdata.getPlnsfx());
		if (covrincr == null) {
			fatalError600();
		}
		// Update covrincr object in covrpf table with below values
		covrincr.setIndexationInd(SPACES.toString());
		// Put unique number in where condition
		covrpfDAO.updateCovrIndexationind(covrincr);

	}

	protected void matureWholeContract2000(Mathpf mathclm) {
	
		covrmatList = covrpfDAO.getcovrmatRecordsForMature(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString());
		for (Covrpf covrmat : covrmatList) {
			if (isLT(wsaaEffdate.toInt(), covrmat.getRiskCessDate())) {
				wsaaFullyMatured.set("N");
				continue;
			}
			if (isEQ(covrmat.getRider(), "00")) {
				rr59hD01Inner.rd01Rcessterm.set(covrmat.getRiskCessTerm());
			}
			processReassurance2900(covrmat);
			covrmat.setValidflag("2");
			covrmat.setCurrto(wsaaEffdate.toInt());
			updateCovrpflist.add(covrmat);
			incrCheck2900(covrmat);
			searchTableT56873000(covrmat);

			if (isLT(covrmat.getPremCessDate(), covrmat.getRiskCessDate())) {
				wsaaCrtable.set(covrmat.getCrtable());
				continue2104(covrmat);
				continue;
			}

			if ((isNE(covrmat.getRider(), "00") && isNE(covrmat.getRider(), "  ")) && isNE(t5687rec.bbmeth, SPACES)) {
				readTableT55342150();
				if (isNE(t5534rec.unitFreq, SPACES) && isNE(t5534rec.subprog, SPACES)
						&& isNE(t5534rec.adfeemth, SPACES)) {
					continue2104(covrmat);
					continue;
				}
			}
			wsaaCrtable.set(covrmat.getCrtable());
			wsaaInstpremTot=wsaaInstpremTot.add(covrmat.getInstprem());
			continue2104(covrmat);

		}
		
		processMatdclms3000(mathclm);
	}

	protected void processReassurance2900(Covrpf covrmat) {
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("TRMR");
		trmreasrec.chdrcoy.set(covrmat.getChdrcoy());
		trmreasrec.chdrnum.set(covrmat.getChdrnum());
		trmreasrec.life.set(covrmat.getLife());
		trmreasrec.coverage.set(covrmat.getCoverage());
		trmreasrec.rider.set(covrmat.getRider());
		trmreasrec.planSuffix.set(covrmat.getPlanSuffix());
		trmreasrec.crtable.set(covrmat.getCrtable());
		trmreasrec.cnttype.set(chdrlif.getCnttype());
		trmreasrec.polsum.set(chdrlif.getPolsum());
		trmreasrec.effdate.set(wsaaEffdate);
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(chdrlif.getTranno());
		trmreasrec.language.set(bsscIO.getLanguage());
		trmreasrec.billfreq.set(chdrlif.getBillfreq());
		trmreasrec.ptdate.set(chdrlif.getPtdate());
		trmreasrec.origcurr.set(chdrlif.getCntcurr());
		trmreasrec.acctcurr.set(covrmat.getPremCurrency());
		trmreasrec.crrcd.set(covrmat.getCrrcd());
		trmreasrec.convUnits.set(covrmat.getConvertInitialUnits());
		trmreasrec.jlife.set(covrmat.getJlife());
		trmreasrec.singp.set(covrmat.getSingp());
		trmreasrec.oldSumins.set(covrmat.getSumins());
		trmreasrec.pstatcode.set(covrmat.getPstatcode());
		trmreasrec.newSumins.set(ZERO);
		trmreasrec.clmPercent.set(100);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			fatalError600();
		}
	}

	protected void incrCheck2900(Covrpf covrmat) {
		incr = new Incrpf();
		/*
		 * Read INCRPF include chdrcoy,chdrnum,life,rider,coverage,plansuffix
		 * from covrmat and store in incr
		 */
		incr = incrpfDAO.getIncrData(covrmat.getChdrcoy(), covrmat.getChdrnum(), covrmat.getLife(), 
				covrmat.getCoverage(),covrmat.getRider(), covrmat.getPlanSuffix());

		if (incr == null) {
			return;
		}

		// Insert above record overwrite below fields in above object
		incr.setValidflag("2");
		if (isNE(t5679rec.setCovPremStat, SPACES)) {
			incr.setPstatcode(t5679rec.setCovPremStat.toString());
		}
		if (isNE(t5679rec.setCovRiskStat, SPACES)) {
			incr.setStatcode(t5679rec.setCovRiskStat.toString());
		}
		incr.setTransactionDate(wsaaTransactionDate.toInt());
		incr.setTransactionTime(varcom.vrcmTime.toInt());
		incr.setUser(varcom.vrcmUser.toInt());
		incr.setTermid(varcom.vrcmTermid.toString());
		incr.setTranno(wsaaChdrlifTranno.toInt());
		/* add INCR to insertIncrList for bulk insert */
		insertIncrList.add(incr);

	}

	protected void searchTableT56873000(Covrpf covrmat) {
		if (t5687Map.containsKey(covrmat.getCrtable())) {
			List<Itempf> t5687ItemList = t5687Map.get(covrmat.getCrtable());
			Itempf t5687Item = t5687ItemList.get(0);
			t5687rec.t5687Rec.set(StringUtil.rawToString(t5687Item.getGenarea()));
			return;
		}
		syserrrec.statuz.set(e652);
		syserrrec.params.set(covrmat.getCrtable());
		fatalError600();
	}

	protected void continue2104(Covrpf covrmat) {
		/*
		 * insert a new record in covrpf. Use the covrmat object and overwrtite
		 * the below fields then insert
		 */
		covrmatins = new Covrpf(covrmat);
		covrmatins.setTranno(wsaaChdrlifTranno.toInt());
		covrmatins.setCurrfrom(wsaaEffdate.toInt());
		/* Update the status code with status code from T5679. Use */
		/* the regular premium status code if the billing frequency, */
		/* from the contract header, is equal to '00' and the single */
		/* premium indicator on table T5687 is not 'Y', otherwise use */
		/* the single-premium status from T5679. */
		/* IF CHDRLIF-QILLFREQ = '00' */
		/* AND T5687-SINGLE-PREM-IND NOT = 'Y' */
		/* IF T5679-SET-COV-PREM-STAT NOT = SPACES */
		/* MOVE T5679-SET-COV-PREM-STAT */
		/* TO COVRMAT-PSTATCODE */
		/* ELSE */
		/* NEXT SENTENCE */
		/* ELSE */
		/* IF T5679-SET-SNGP-COV-STAT NOT = SPACES */
		/* MOVE T5679-SET-SNGP-COV-STAT */
		/* TO COVRMAT-PSTATCODE. */
		/* IF CHDRLIF-BILLFREQ = '00' <A0569 */
		if (isEQ(payrpf.getBillfreq(), "00")) {
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				if (isEQ(covrmatins.getRider(), "00")) {
					if (isNE(t5679rec.setSngpCovStat, SPACES)) {
						covrmatins.setPstatcode(t5679rec.setSngpCovStat.toString());
					}
				} else {
					if (isNE(t5679rec.setSngpRidStat, SPACES)) {
						covrmatins.setPstatcode(t5679rec.setSngpRidStat.toString());
					}
				}
			}
		}
		if (isNE(payrpf.getBillfreq(), "00")) {
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				if (isEQ(covrmatins.getRider(), "00")) {
					if (isNE(t5679rec.setSngpCovStat, SPACES)) {
						covrmatins.setPstatcode(t5679rec.setSngpCovStat.toString());
					}
				} else {
					if (isNE(t5679rec.setSngpRidStat, SPACES)) {
						covrmatins.setPstatcode(t5679rec.setSngpRidStat.toString());
					}
				}
			} else {
				if (isEQ(covrmatins.getRider(), "00")) {
					if (isNE(t5679rec.setCovPremStat, SPACES)) {
						covrmatins.setPstatcode(t5679rec.setCovPremStat.toString());
					}
				} else {
					if (isNE(t5679rec.setRidPremStat, SPACES)) {
						covrmatins.setPstatcode(t5679rec.setRidPremStat.toString());
					}
				}
			}
		}
		if (isNE(t5679rec.setCovRiskStat, SPACES)) {
			covrmatins.setStatcode(t5679rec.setCovRiskStat.toString());
		}
		covrmatins.setValidflag("1");
		covrmatins.setCurrto(varcom.vrcmMaxDate.toInt());
		covrmatins.setPayrseqno(1);
		covrmatins.setTransactionDate(wsaaTransactionDate.toInt());
		covrmatins.setTransactionTime(varcom.vrcmTime.toInt());
		covrmatins.setUser(varcom.vrcmUser.toInt());
		covrmatins.setTermid(varcom.vrcmTermid.toString());
		insertCovrpfList.add(covrmatins);
	}

	protected void readTableT55342150() {
		if (t5534Map.containsKey(t5687rec.bbmeth)) {
			List<Itempf> t5534ItemList = t5534Map.get(t5687rec.bbmeth);
			Itempf t5534Item = t5534ItemList.get(0);
			t5534rec.t5534Rec.set(StringUtil.rawToString(t5534Item.getGenarea()));
			return;
		}
		t5534rec.t5534Rec.set(SPACES);
	}

	protected void processMatdclms3000(Mathpf mathclm) {
		// read MATDPF include wsaaChdrChdrnum, wsaaChdrChdrcoy,
		// mathclm.getTranno(),mathclm. getPlanSuffix ()and store in matdclmlist
		matdpf = new Matdpf();
		matdclmList = matdpfDAO.getmatdclmListRecord(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString(),
				mathclm.getTranno(), mathclm.getPlnsfx());

		for (Matdpf matdclm : matdclmList) {
			wsaaCrtable.set(matdclm.getCrtable());
			if (isEQ(wsaaCrtable, SPACES)) {
				continue;
			}
			readTableT56871500();// using wsaaCrtable.. if item not found throw
									// e652 refer logic used previously
			readTableT65981600(); // using t5687rec.maturityCalcMeth.. if item
									// not found throw DBERROR

			if (isEQ(t6598rec.procesprog, SPACES)) {
				continue;
			}
			matpcpy.planSuffix.set(matdclm.getPlnsfx());
			matpcpy.crtable.set(matdclm.getCrtable());
			matpcpy.cnttype.set(mathclm.getCnttype());
			matpcpy.chdrcoy.set(matdclm.getChdrcoy());
			matpcpy.chdrnum.set(matdclm.getChdrnum());
			matpcpy.life.set(matdclm.getLife());
			matpcpy.jlife.set(matdclm.getJlife());
			matpcpy.coverage.set(matdclm.getCoverage());
			matpcpy.rider.set(matdclm.getRider());
			matpcpy.fund.set(matdclm.getVrtfund());
			matpcpy.tranno.set(wsaaChdrlifTranno);
			matpcpy.estimatedVal.set(matdclm.getEmv());
			matpcpy.actualVal.set(matdclm.getActvalue());
			matpcpy.type.set(matdclm.getType_t());
			if (isEQ(mathclm.getCurrcd(), SPACES)) {
				matpcpy.currcode.set(matdclm.getCurrcd());
				matpcpy.cntcurr.set(chdrlif.getCntcurr());
			} else {
				matpcpy.currcode.set(mathclm.getCurrcd());
				matpcpy.cntcurr.set(matdclm.getCurrcd());
			}
			matpcpy.batckey.set(wsaaBatckey.batcFileKey);
			matpcpy.termid.set(wsaaTermid);
			matpcpy.effdate.set(wsaaEffdate);
			matpcpy.date_var.set(wsaaTransactionDate);
			matpcpy.time.set(wsaaTransactionTime);
			matpcpy.user.set(wsaaUser);
			matpcpy.language.set(bsscIO.language);
			matpcpy.status.set(varcom.oK);
			callMaturityProcess3200();
			if (isEQ(matpcpy.status, varcom.endp) || isEQ(matpcpy.status, varcom.bomb)) {
				// matdclm.setStatuz(varcom.endp);
				break;
			}
		}
	}

	protected void readTableT56871500() {
		if (t5687Map.containsKey(wsaaCrtable)) {
			List<Itempf> t5687ItemList = t5687Map.get(wsaaCrtable);
			Itempf t5687Item = t5687ItemList.get(0);
			t5687rec.t5687Rec.set(StringUtil.rawToString(t5687Item.getGenarea()));
			return;
		}
		syserrrec.statuz.set(e652);
		syserrrec.params.set(wsaaCrtable);
		fatalError600();

	}

	protected void readTableT65981600() {
		if (t6598Map.containsKey(t5687rec.maturityCalcMeth)) {
			List<Itempf> t6598ItemList = t6598Map.get(t5687rec.maturityCalcMeth);
			Itempf t6598Item = t6598ItemList.get(0);
			t6598rec.t6598Rec.set(StringUtil.rawToString(t6598Item.getGenarea()));
			return;
		}
		syserrrec.statuz.set(e652);
		syserrrec.params.set(t5687rec.maturityCalcMeth);
		fatalError600();

	}

	protected void callMaturityProcess3200() {
		callProgram(t6598rec.procesprog, matpcpy.maturityRec);
		if (isEQ(matpcpy.status, varcom.bomb)) {
			syserrrec.params.set(matpcpy.maturityRec);
			syserrrec.statuz.set(matpcpy.status);
			fatalError600();
		}
	}

	protected void matureSelectedPolicies2500() {
		callBreakout2600();

		for (Mathpf mathclm : mathclmList) {
			processMathclm2700(mathclm);
		}
	}

	protected void callBreakout2600() {

		// initlialize mathclmList
		// read mathpf for wsaaPrimaryChdrnum, wsaaPrimaryChdrcoy,
		// wsaaChdrlifTranno plansuffix=ZERO and store in a list mathclmlist
		mathclmList = mathpfDAO.getmathclmRecordList(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString(),
				wsaaChdrlifTranno.toInt());
		if (mathclmList == null) {
			syserrrec.params.set(wsaaChdrChdrnum);
			fatalError600();
		}

		else {
			for (Mathpf mathclm : mathclmList) {
				if (isGT(mathclm.getPlnsfx(), chdrlif.getPolsum()) || isEQ(chdrlif.getPolsum(), 1)
						|| isEQ(mathclm.getPlnsfx(), 0)) {
					break;
				}
				brkoutrec.brkOldSummary.set(chdrlif.getPolsum());
				compute(brkoutrec.brkNewSummary, 0).set(sub(mathclm.getPlnsfx(), 1));
				brkoutrec.brkChdrnum.set(chdrlif.getChdrnum());
				brkoutrec.brkChdrcoy.set(chdrlif.getChdrcoy());
				brkoutrec.brkBatctrcde.set(bprdIO.getAuthCode());
				brkoutrec.brkStatuz.set(SPACES);
				callProgram(Brkout.class, brkoutrec.outRec);
				if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
					syserrrec.params.set(brkoutrec.outRec);
					syserrrec.statuz.set(brkoutrec.brkStatuz);
					fatalError600();
				}
			}
		}
	}

	protected void processMathclm2700(Mathpf mathclm) {
		// Initialize covrmatList;
		covrmatList = new ArrayList<Covrpf>();
	//	covrmatList = covrpfDAO.getcovrmatRecordsForMature(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString());
		// Read COVRPF, include wsaaChdrChdrnum, wsaaChdrChdrcoy, VALIDFLAG != 2
		// , PlanSuffix from mathclm in where condition and retrieve a record
		// and store in covrmatList (object of COVRPF).
		covrmatList = covrpfDAO.getcovrmatRecordsForMature(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString());
		processSameSuffix2702(mathclm);
	}

	protected void processSameSuffix2702(Mathpf mathclm) {
		for (Covrpf covrdata : covrmatList) {
			processReassurance2900(covrdata);
			// update COVRPf set below values and include covrmat.unique number
			// in where condition
			covrdata.setValidflag("2");
			covrdata.setCurrto(wsaaEffdate.toInt());
			updateCovrpflist.add(covrdata);
			incrCheck2900(covrdata);
			searchTableT56873000(covrdata);

			if (isLT(covrdata.getPremCessDate(), covrdata.getRiskCessDate())) {
				continue2104(covrdata);
				continue;
			}
			if ((isNE(covrdata.getRider(), "00") && isNE(covrdata.getRider(), "  ")) && isNE(t5687rec.bbmeth, SPACES)) {
				readTableT55342150();
				if (isNE(t5534rec.unitFreq, SPACES) && isNE(t5534rec.subprog, SPACES)
						&& isNE(t5534rec.adfeemth, SPACES)) {
					continue2104(covrdata);
					continue;
				}
			}
			wsaaInstpremTot.add(covrdata.getInstprem());
			continue2104(covrdata);

		}

		processMatdclms3000(mathclm);
	}

	protected void updateContractHeader4500() {
		if (matureWholePlan.isTrue()) {
			if (!fullyMatured.isTrue()) {
				updateInstprem4501();
			}
			chkStatusCode4502();
			rewriteChdrlifPayr4504();
			return;

		}
		chkComponents4510();
		if (fullyMatured.isTrue()) {
			chkStatusCode4502();
			rewriteChdrlifPayr4504();
			return;
		}
		updateInstprem4501();
		chkStatusCode4502();
		rewriteChdrlifPayr4504();

	}

	protected void updateInstprem4501() {
		if (isNE(payrpf.getBillfreq(), "00") && isNE(payrpf.getBillfreq(), "  ")) {
			setPrecision(chdrlif.getSinstamt01(), 2);
			chdrlif.setSinstamt01(new BigDecimal(chdrlif.getSinstamt01().toString())
					.subtract(new BigDecimal(wsaaInstpremTot.toString())));
			setPrecision(payrpf.getSinstamt01(), 2);
			payrpf.setSinstamt01(new BigDecimal(payrpf.getSinstamt01().toString())
					.subtract(new BigDecimal(wsaaInstpremTot.toString())));
			setPrecision(chdrlif.getSinstamt06(), 2);
			chdrlif.setSinstamt06(new BigDecimal(chdrlif.getSinstamt06().toString())
					.subtract(new BigDecimal(wsaaInstpremTot.toString())));
			setPrecision(payrpf.getSinstamt06(), 2);
			payrpf.setSinstamt06(new BigDecimal(payrpf.getSinstamt06().toString())
					.subtract(new BigDecimal(wsaaInstpremTot.toString())));

		}
	}

	protected void chkStatusCode4502() {
		if (isNE(t5679rec.setCnPremStat, SPACES) || isNE(t5679rec.setSngpCnStat, SPACES)) {
			if (isEQ(payrpf.getBillfreq(), "00") || isEQ(payrpf.getBillfreq(), "  ")) {
				chdrlif.setPstcde(t5679rec.setSngpCnStat.toString());
			} else {
				chdrlif.setPstcde(t5679rec.setCnPremStat.toString());
			}
		}
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrlif.setStatcode(t5679rec.setCnRiskStat.toString());
		}
	}

	protected void rewriteChdrlifPayr4504() {
		/* Update chdrpf using chdrlif set below fields */
		chdrlif.setStattran(wsaaChdrlifTranno.toInt());
		chdrlif.setCurrfrom(wsaaEffdate.toInt());
		chdrlif.setStatdate(wsaaEffdate.toInt());
		chdrlif.setCurrto(varcom.vrcmMaxDate.toInt());
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrlif.setTranid(varcom.vrcmCompTranid.toString());
		insertChdrpfList.add(chdrlif);

		// Update payrpf using payr set below fields
		payrpf.setEffdate(wsaaEffdate.toInt());
		payrpf.setTermid(wsaaTermid.toString());
		payrpf.setUser(wsaaUser.toInt());
		payrpf.setTransactionDate(wsaaTransactionDate.toInt());
		payrpf.setTransactionTime(wsaaTransactionTime.toInt());
		payrpf.setPstatcode(chdrlif.getPstcde());
		insertPayrpfList.add(payrpf);

	}

	protected void chkComponents4510() {
		// Initialize covrmatList;
		// Read COVRPF, include wsaaChdrChdrnum, wsaaChdrChdrcoy, VALIDFLAG != 2
		// , PlanSuffix = ZERO in where condition and retrieve a record and
		// store in covrmatList (object of COVRPF).
		wsaaFullyMatured.set("Y");

		for (Covrpf covrmat : covrmatList) {
			processCovrmats4520(covrmat);
		}

	}

	protected void processCovrmats4520(Covrpf covrmat) {
		lookForStat4530(covrmat);
		if (!fullyMatured.isTrue()) {
			return;
		}

	}

	protected void lookForStat4530(Covrpf covrmat) {
		wsaaSub.set(ZERO);
		loop4532(covrmat);
	}

	protected void loop4532(Covrpf covrmat) {
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			wsaaFullyMatured.set("Y");
			return;
		}
		if (isEQ(covrmat.getStatcode(), t5679rec.covRiskStat[wsaaSub.toInt()])) {
			wsaaFullyMatured.set("N");
			return;
		}
		loop4532(covrmat);
	}

	protected void processLives4000() {
		// Read lifepf include wsaaChdrChdrnum, wsaaChdrChdrcoy in where
		// condition and retrieve a record and store in lifeList (object of
		// lifepf).
		

		lifepfList = lifepfDAO.getLifeList(wsaaChdrChdrcoy.toString(), wsaaChdrChdrnum.toString());
		for (Lifepf lifedata : lifepfList) {
			if (isNE(lifedata.getValidflag(), "1")) {
				continue;
			}
			updateLives4100(lifedata);
		}
	}

	protected void updateLives4100(Lifepf lifedata) {
		
		// Update lifedata in lifepf table set below fields

		lifedata.setValidflag("2");
		lifedata.setCurrto(wsaaEffdate.toInt());
		updateLifepflist.add(lifedata);
		// insert new record in lifepf using same lifedata object but overwrite
		// below fields
		insertlifedata = new Lifepf(lifedata);
		insertlifedata.setTransactionDate(wsaaTransactionDate.toInt());
		insertlifedata.setTransactionTime(wsaaTransactionTime.toInt());
		insertlifedata.setUser(wsaaUser.toInt());
		insertlifedata.setTermid(wsaaTermid.toString());
		insertlifedata.setTranno(wsaaChdrlifTranno.toInt());
		insertlifedata.setValidflag("1");
		insertlifedata.setCurrto(varcom.vrcmMaxDate.toInt());
		insertlifedata.setCurrfrom(wsaaEffdate.toInt());
		writeNewLife4102(lifedata);
		insertLifepflist.add(insertlifedata);

	}

	protected void writeNewLife4102(Lifepf lifedata) {

		if (isEQ(wsaaPlnsfx	, "0")) {
			if (isEQ(lifedata.getJlife(), "00")
					|| isEQ(lifedata.getJlife(), "  ") && isNE(t5679rec.setLifeStat, SPACES)) {
				insertlifedata.setStatcode(t5679rec.setLifeStat.toString());
			} else {
				if (isNE(t5679rec.setJlifeStat, SPACES)) {
					insertlifedata.setStatcode(t5679rec.setJlifeStat.toString());
				}
			}
		}
	}
	 /*ILIFE-3335 Starts*/
	protected void PrintPendingLetters(){
      	wsaaLetterStatus.set("1");
		pendingLetcList = letcpfDAO.getPendingletcData(wsaaChdrnumFrom.toString(),wsaaChdrnumTo.toString(),tr384rec.letterType.toString(),wsaaLetterStatus.toString(),wsaaBatchTrancde.toString());
		for (Letcpf letc : pendingLetcList){
			acmvpf = new Acmvpf();
			
			wsaaAcmvCount = acmvpfDAO.getAcmvpfRecordCount(letc.getChdrcoy(),letc.getChdrnum(),wsaaUnitdTrancde.toString(),letc.getTranno());/* IJTI-1523 */
			
			if(isGT(wsaaAcmvCount, 0))
			{	
			
			letrqstrec.statuz.set(SPACES);
			letrqstrec.requestCompany.set(letc.getChdrcoy());
			/* MOVE T6634-LETTER-TYPE TO LETRQST-LETTER-TYPE. */
			
			letrqstrec.letterType.set(letc.getLettype()); //IFSU-3359 
			letrqstrec.letterRequestDate.set(wsaaEffdate);
			letrqstrec.clntcoy.set(letc.getClntcoy());
			letrqstrec.clntnum.set(letc.getClntnum());
			letrqstrec.rdocpfx.set(letc.getRdocpfx());
			letrqstrec.rdoccoy.set(letc.getChdrcoy());
			letrqstrec.rdocnum.set(letc.getRdocnum());
			letrqstrec.tranno.set(letc.getTranno());
			letrqstrec.hsublet.set(letc.getHsublet());
			letrqstrec.despnum.set(letc.getDespnum());
			letrqstrec.chdrcoy.set(letc.getChdrcoy());
			letrqstrec.chdrnum.set(letc.getChdrnum());
			letrqstrec.branch.set(letc.getBranch());
			letrqstrec.otherKeys.set(letc.getLetokey()); //IFSU-3359 
			/* MOVE CHDRLIF-CHDRNUM TO LETRQST-OTHER-KEYS. */
			letrqstrec.function.set("ADD");

			letrqstList.add(letrqstrec);
			wsaaAcmvCount = 0;
		}
	 }
	}
	 /*ILIFE-3335 Ends*/
	protected void writeLetcRecord5000() {
				
		if (isEQ(tr384rec.letterType, SPACES)) {
			return;
		}
		else
		{
			wsaaLetterType.set(tr384rec.letterType);
		} 

		writeRecord5001();
	}

	protected void writeRecord5001() {
		wsaaOkey1.set(wsaaPlnsfx);
		acmvpf = new Acmvpf();
		
		acmvpf = acmvpfDAO.getAcmvpfRecord(chdrlif.getChdrcoy().toString(),chdrlif.getChdrnum(),wsaaBatchTrancde.toString());
		
		if (acmvpf== null){
			letc = new Letcpf();
			
			letc = letcpfDAO.getletcData(chdrlif.getChdrcoy().toString(),wsaaLetterType.toString(),chdrlif.getCowncoy().toString(),chdrlif.getCownnum());
			
		/*	letc.setRequestCompany(letrqstrec.requestCompany.toString());
			letc.setLetterType(letrqstrec.letterType.toString());
			letc.setClntcoy(letrqstrec.clntcoy.toString());
		   letc.setClntnum(letrqstrec.clntnum.toString()); */
			
			if( letc==null ){
				letc = new Letcpf();
			letc.setLetseqno(1); //IFSU-3359 
		  }
		 else 
		  {
				setPrecision(letc.getLetseqno(), 0); //IFSU-3359 
			letc.setLetseqno(add(letc.getLetseqno(), 1).toInt()); //IFSU-3359 
		   }
			if (tr383Map.containsKey(tr384rec.letterType)) {
				List<Itempf> tr383ItemList = tr383Map.get(tr384rec.letterType);
				Itempf tr383Item = tr383ItemList.get(0);
				tr383rec.tr383Rec.set(StringUtil.rawToString(tr383Item.getGenarea()));
				
			}
			else{
			syserrrec.statuz.set(e652);
			syserrrec.params.set(tr384rec.letterType);
			fatalError600();
			}
		    letc.setHsublet(tr383rec.hsublet[1].toString());
			letc.setLetstat("1"); //IFSU-3359 
			letc.setReqcoy(batcdorrec.company.toString()); //IFSU-3359 
			letc.setServunit(chdrlif.getServunit());/* IJTI-1523 */
			letc.setLreqdate(wsaaToday.toInt()); //IFSU-3359 
			letc.setLprtdate(datcon1rec.intDate.toInt()); //IFSU-3359 
			letc.setClntcoy(chdrlif.getCowncoy().toString());
			letc.setClntnum(chdrlif.getCownnum());
			letc.setDespnum(letrqstrec.despnum.toString());
			letc.setRdocpfx(chdrlif.getChdrpfx());
			letc.setRdoccoy(chdrlif.getChdrcoy().toString());
			letc.setRdocnum(chdrlif.getChdrnum());
			letc.setLettype(wsaaLetterType.toString()); //IFSU-3359 
			if (isEQ(letrqstrec.tranno, NUMERIC)) {
				letc.setTranno(chdrlif.getTranno());
			}
			else {
				letc.setTranno(0);
			}	
			letc.setLetokey(wsaaOkey.toString());	/*ILIFE-3331 */ //IFSU-3359 
			letc.setBranch(chdrlif.getCntbranch());
			letc.setChdrcoy(chdrlif.getChdrcoy().toString());
			letc.setChdrnum(chdrlif.getChdrnum());
			letc.setTrcde(wsaaBatchTrancde.toString()); //IFSU-3359 
			
			readT2634400();
			if (isEQ(t2634rec.zzcopies, ZERO)) {
				letc.setZzcopies(letrqstrec.copies.toInt());
			}
			else {
				letc.setZzcopies(t2634rec.zzcopies.toInt());
			}
			if (isEQ(t2634rec.zduplex, "Y")) {
				letc.setZduplex("Y");
			}
			else {
				letc.setZduplex("N");
			}
		Letcpflist.add(letc);
					
		}
	else{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlif.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE TO LETRQST-LETTER-TYPE. */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaEffdate);
		letrqstrec.clntcoy.set(chdrlif.getCowncoy());
		letrqstrec.clntnum.set(chdrlif.getCownnum());
		letrqstrec.rdocpfx.set(chdrlif.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlif.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlif.getChdrnum());
		letrqstrec.tranno.set(chdrlif.getTranno());
		letrqstrec.chdrcoy.set(chdrlif.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlif.getChdrnum());
		letrqstrec.branch.set(chdrlif.getCntbranch());
	
		letrqstrec.otherKeys.set(wsaaOkey);	/*ILIFE-3331 */
		/* MOVE CHDRLIF-CHDRNUM TO LETRQST-OTHER-KEYS. */
		letrqstrec.function.set("ADD");

		letrqstList.add(letrqstrec);
	}

	}

	protected void writePtrnTransaction6000() {	
		Gettoday();
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setTranno(chdrlif.getTranno());
		ptrnpf.setPtrneff(wsaaEffdate.toInt());
		/* MOVE WSAA-EFFECTIVE-DATE TO ptrnpf-DATESUB. <LA5184> */
		ptrnpf.setDatesub(wsaaToday.toInt());
		ptrnpf.setChdrpfx(chdrlif.getChdrpfx());
		ptrnpf.setChdrcoy(chdrlif.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrlif.getChdrnum());
		ptrnpf.setValidflag("1");
		ptrnpf.setPrtflg(" ");
		ptrnpf.setRecode(chdrlif.getRecode());
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		ptrnpf.setUserT(wsaaUser.toInt());
		ptrnpf.setTermid(wsaaTermid.toString());
		insertPtrnList.add(ptrnpf);
	}

	protected void Gettoday() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}
	protected void	writeDetailsReport4700()
	{
		
		/* If first page/overflow - write standard headings*/
		
		if (newPageReq.isTrue()) {
			printerFile.printRr59hh01(rr59hH01, indicArea);
			wsaaOverflow.set("N");
		}
		
		rr59hD01Inner.rd01Chdrnum.set(matpcpy.chdrnum);
		
		rr59hD01Inner.rd01Crtable.set(matpcpy.crtable);
         getProdDesc();		
           getIssueDate();
		

		rr59hD01Inner.rd01Sumin.set(ZERO);
		if (isEQ(matccpy.type, "B")
		|| isEQ(matccpy.type, "M")
		|| isEQ(matccpy.type, "T")
		|| isEQ(matccpy.type, "X")) {
		}
		else {
			rr59hD01Inner.rd01Sumin.set(matpcpy.actualVal);
		}
		rr59hD01Inner.rd01Matamt.set(wsaaClmamt);
		rr59hD01Inner.rd01Rcesdte.set(matpcpy.effdate);
		rr59hD01Inner.rd01Curr.set(matpcpy.currcode);
		rr59hD01Inner.rd01Vrtfund.set(matpcpy.fund);
		rr59hD01Inner.rd01Unitval.set(matpcpy.fund);

    getclientDetails();
   printerFile.printRr59hd01(rr59hD01Inner.rr59hD01, indicArea);
	}
	protected void	getIssueDate(){
	//read HPAD for contract number and company get issue date from Hissdte column 
		hpad =new Hpadpf();
	
		hpad = hpadpfDAO.getHpadData(wsaaChdrChdrcoy.toString(),wsaaChdrChdrnum.toString());
      rr59hD01Inner.rd01Issuedte.set(hpad.getHissdte());

}
	protected void	getclientDetails(){
//Read client name and number from CLNTPF dao  send below details and store the record in CLNTPF
			clntpf = new Clntpf();		
			
			clntpf =clntpfDAO.findClientByClntnum(chdrlif.getCownnum());
			if ((clntpf != null)){
				rr59hD01Inner. rd01Clntnum.set(clntpf.getClntnum());
				plainname();
				rr59hD01Inner.rd01Clntnm.set(wsspLongconfname);
				return;
		
		}
		
		fatalError600();
}
protected void plainname()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return;
		}
		if (clntpf.getGivname()!= null) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(clntpf.getSurname());
		}											
	}
protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntpf.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}
protected void  getProdDesc()
{																				
//Read DESCPF for an item matpcpy.crtable
	t5687List = descDAO.getItemByDescItem(smtpfxcpy.item.toString(), bsprIO.getCompany().toString(), "T5687",wsaaCrtable.toString(), 
			bsscIO.language.toString());
	boolean itemFound = false;
	for (Descpf descItem : t5687List) {
		if (descItem.getDescitem().trim().equals(wsaaCrtable.toString().trim())) {
			if (isEQ(descItem.getShortdesc(),SPACES)) {
				rr59hD01Inner.rd01Descrip.fill("?");
			}
			else {
				rr59hD01Inner.rd01Descrip.set(descItem.getShortdesc());
			}
			
			itemFound = true;
			break;
		}
	}
	if (!itemFound) {
		fatalError600();
	}
		
		
		
}

protected void readT2634400(){
if (t2634Map.containsKey(tr384rec.letterType)) {
	List<Itempf> t2634ItemList = t2634Map.get(tr384rec.letterType);
	Itempf t2634Item = t2634ItemList.get(0);
	t2634rec.t2634Rec.set(StringUtil.rawToString(t2634Item.getGenarea()));
	return;
}
syserrrec.statuz.set(e652);
syserrrec.params.set(tr384rec.letterType);
fatalError600();
}

	protected void updateBatcLast4500() {
		update4510();
	}

	protected void update4510() {
		if (isNE(bprdIO.getAuthCode(), SPACES)) {
			batcuprec.function.set(varcom.writs);
			batcuprec.trancnt.set(ZERO);
			batcuprec.sub.set(ZERO);
			batcuprec.etreqcnt.set(ZERO);
			batcuprec.bcnt.set(ZERO);
			batcuprec.bval.set(ZERO);
			batcuprec.ascnt.set(ZERO);
			batcuprec.batcpfx.set("BA");
			batcuprec.batccoy.set(bsprIO.getCompany());
			if (isEQ(bsprIO.getDefaultBranch(), SPACES)) {
				batcuprec.batcbrn.set(bsscIO.getInitBranch());
			} else {
				batcuprec.batcbrn.set(bsprIO.getDefaultBranch());
			}
			batcuprec.batcactyr.set(bsscIO.getAcctYear());
			batcuprec.batcactmn.set(bsscIO.getAcctMonth());
			batcuprec.batctrcde.set(bprdIO.getAuthCode());
			batcuprec.batcbatch.set(batcdorrec.batch);
			callProgram(Batcup.class, batcuprec.batcupRec);
			if (isNE(batcuprec.statuz, varcom.oK)) {
				syserrrec.params.set(batcuprec.batcupRec);
				syserrrec.statuz.set(batcuprec.statuz);
				fatalError600();
			}
		}
	}

	protected void releaseSoftlock8000() {
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(wsaaChdrChdrnum);
		sftlockrec.transaction.set(wsaaBatchTrancde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	@Override
	protected void commit3500() {
		 int busDate = bsscIO.getEffectiveDate().toInt();
		if (insertChdrpfList.size()> 0){
			chdrpfDAO.updateChdrRecord(updateChdrlifList,busDate);
			chdrpfDAO.insertChdrpfMatureList(insertChdrpfList, busDate);
		}
		if (updatePayrList.size()> 0){
			payrDAO.updatePayrRecord(updatePayrList,busDate);
			}
		if (insertPayrpfList.size()> 0){
		payrDAO.insertPayrpfList(insertPayrpfList);
		}
		if (insertIncrList.size()> 0){
			incrpfDAO.insertIncrList(insertIncrList);
			}
		if (insertPtrnList.size()> 0){
			ptrnpfDAO.insertPtrnPF(insertPtrnList);
			}
		if (updateCovrpflist.size()> 0){
			covrpfDAO.updateCovrRecord(updateCovrpflist,busDate);
			}
		if (insertCovrpfList.size()> 0){
			covrpfDAO.insertCovrpfList(insertCovrpfList);
		}
		if (updateLifepflist.size()> 0){
			lifepfDAO.updateLifeRecord(updateLifepflist,busDate);
			}
		if (insertLifepflist.size()> 0){
			lifepfDAO.insertLifepfList(insertLifepflist);
		}
		if (Letcpflist.size()> 0){
			letcpfDAO.insertLetcpfList(Letcpflist);
		}
		printerFile.close();
		for(Letrqstrec letrqstrec : letrqstList){
		callProgram(Letrqst.class, letrqstrec.params);
				if (isNE(letrqstrec.statuz, varcom.oK)) {
					syserrrec.params.set(letrqstrec.params);
					syserrrec.statuz.set(letrqstrec.statuz);
					fatalError600();
				}

		}

	}
	
	protected void rollback3600() {
		 /* ROLLBACK */
        /* EXIT */

	}

	
	protected void close4000() {
		 /* CLOSE-FILES */
		t5687Map.clear();
        t5534Map.clear();
        
        t2634Map.clear();
        t5679Map.clear();
        tr384List.clear();
        t5679List.clear();
        if (insertChdrpfList  != null) {
        	insertChdrpfList.clear();
        }
        if (insertIncrList  != null) {
        	insertIncrList.clear();
        }
        if (insertCovrpfList != null) {
        	insertCovrpfList.clear();
        }
        if (insertPayrpfList!= null) {
        	insertPayrpfList.clear();
        }
        if (insertLifepflist  != null) {
        	insertLifepflist.clear();
        }
        if (insertPtrnList  != null) {
        	insertPtrnList.clear();
        }
        if (updateChdrlifList != null) {
        	updateChdrlifList.clear();
        }
        if (updateCovrpflist!= null) {
        	updateCovrpflist.clear();
        }
        if (updateLifepflist != null) {
        	updateLifepflist.clear();
        }
        if (matdclmList!= null) {
        	matdclmList.clear();
        }
        if (matypfList!= null) {
        	matypfList.clear();
        }
        if (covrmatList!= null) {
        	covrmatList.clear();
        }
        if (mathclmList!= null) {
        	mathclmList.clear();
        }
        if (incrmjaList!= null) {
        	incrmjaList.clear();
        }
        if (Letcpflist!= null) {
        	Letcpflist.clear();
        }
        if (letrqstList!= null) {
        	letrqstList.clear();
        }
        if (pendingLetcList!= null) {
        	pendingLetcList.clear();
        }
        
        t5687Map = null;
        t5534Map = null;
        tr384List = null;
        t2634Map = null;
        t5679Map = null;
        insertChdrpfList  = null;
        insertIncrList   = null;
        insertCovrpfList  = null;
        insertPayrpfList   = null;
        insertLifepflist  = null;
        insertPtrnList  = null;
        updatePayrList  = null;
        updateChdrlifList  = null;
        updateCovrpflist  = null;
        updateLifepflist  = null;
        covrmatList  = null;
        mathclmList  = null;
        incrmjaList  = null;
        matdclmList  = null;
        Letcpflist = null;
        letrqstList=null;
        pendingLetcList=null;
       
        lsaaStatuz.set(varcom.oK);
        /* EXIT */
	}

	protected void sendSMSnotification3200() {
		
	}

	/*
	 * Class transformed from Data Structure RR59H-D01--INNER
	 */
	protected static final class Rr59hD01Inner {

		
		public FixedLengthStringData rr59hD01 = new FixedLengthStringData(155);
		private FixedLengthStringData rr59hd01O = new FixedLengthStringData(155).isAPartOf(rr59hD01, 0);
		private FixedLengthStringData rd01Clntnm = new FixedLengthStringData(40).isAPartOf(rr59hd01O, 0);
		private FixedLengthStringData rd01Clntnum = new FixedLengthStringData(8).isAPartOf(rr59hd01O, 40);
		public FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rr59hd01O, 48);
		private FixedLengthStringData rd01Descrip = new FixedLengthStringData(30).isAPartOf(rr59hd01O, 56);
		public FixedLengthStringData rd01Crtable = new FixedLengthStringData(4).isAPartOf(rr59hd01O, 86);
		private FixedLengthStringData rd01Issuedte = new FixedLengthStringData(10).isAPartOf(rr59hd01O, 90);
		public FixedLengthStringData rd01Rcessterm = new FixedLengthStringData(2).isAPartOf(rr59hd01O, 100);
		public FixedLengthStringData rd01Rcesdte = new FixedLengthStringData(10).isAPartOf(rr59hd01O, 102);
		public ZonedDecimalData rd01Sumin = new ZonedDecimalData(15, 0).isAPartOf(rr59hd01O, 112);
		public FixedLengthStringData rd01Curr = new FixedLengthStringData(3).isAPartOf(rr59hd01O, 127);
		public FixedLengthStringData rd01Vrtfund = new FixedLengthStringData(4).isAPartOf(rr59hd01O, 130);
		public FixedLengthStringData rd01Unitval = new FixedLengthStringData(4).isAPartOf(rr59hd01O, 134);
		public ZonedDecimalData rd01Matamt = new ZonedDecimalData(17, 2).isAPartOf(rr59hd01O, 138);
	}

}
