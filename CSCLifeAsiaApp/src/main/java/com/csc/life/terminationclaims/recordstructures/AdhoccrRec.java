package com.csc.life.terminationclaims.recordstructures;

import java.util.ArrayList;
import java.util.List;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class AdhoccrRec extends ExternalData {
	//AdhoccrRec
	private static final long serialVersionUID = 1L;
	
  //*******************************
  //Attribute Declarations
  //*******************************
	public FixedLengthStringData statuz = new FixedLengthStringData(4);
	
	private String chdrnum;
	private List<String> cnttype;
	private List<String> svMethod;
	private List<String> riskComDate;
	private List<String> riskCesDate;
	private List<String> ptdate;
	private List<String> billfreq;
	private List<String> paidPrem;
	private List<List<String>> covrCoverage;
	private List<List<String>> crtable;
	private List<List<String>> covrSVMethod;
	private List<List<String>> covrcd;
	private List<List<String>> covrPaidPrem;
	private List<String> surrValRfnDys;
	private List<String> surrValPol;
	private List<List<String>> surrValCovr;

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public List<String> getCnttype() {
		return cnttype;
	}

	public void setCnttype(List<String> cnttype) {
		this.cnttype = cnttype;
	}

	public List<String> getSvMethod() {
		return svMethod;
	}

	public void setSvMethod(List<String> svMethod) {
		this.svMethod = svMethod;
	}

	public List<String> getRiskComDate() {
		return riskComDate;
	}

	public void setRiskComDate(List<String> riskComDate) {
		this.riskComDate = riskComDate;
	}

	public List<String> getRiskCesDate() {
		return riskCesDate;
	}

	public void setRiskCesDate(List<String> riskCesDate) {
		this.riskCesDate = riskCesDate;
	}

	public List<String> getPtdate() {
		return ptdate;
	}

	public void setPtdate(List<String> ptdate) {
		this.ptdate = ptdate;
	}

	public List<String> getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(List<String> billfreq) {
		this.billfreq = billfreq;
	}

	public List<String> getPaidPrem() {
		return paidPrem;
	}

	public void setPaidPrem(List<String> paidPrem) {
		this.paidPrem = paidPrem;
	}

	public List<List<String>> getCovrCoverage() {
		return covrCoverage;
	}

	public void setCovrCoverage(List<List<String>> covrCoverage) {
		this.covrCoverage = covrCoverage;
	}

	public List<List<String>> getCrtable() {
		return crtable;
	}

	public void setCrtable(List<List<String>> crtable) {
		this.crtable = crtable;
	}

	public List<List<String>> getCovrSVMethod() {
		return covrSVMethod;
	}

	public void setCovrSVMethod(List<List<String>> covrSVMethod) {
		this.covrSVMethod = covrSVMethod;
	}

	public List<List<String>> getCovrcd() {
		return covrcd;
	}

	public void setCovrcd(List<List<String>> covrcd) {
		this.covrcd = covrcd;
	}

	public List<List<String>> getCovrPaidPrem() {
		return covrPaidPrem;
	}

	public void setCovrPaidPrem(List<List<String>> covrPaidPrem) {
		this.covrPaidPrem = covrPaidPrem;
	}


	public List<String> getSurrValRfnDys() {
		return surrValRfnDys;
	}

	public void setSurrValRfnDys(List<String> surrValRfnDys) {
		this.surrValRfnDys = surrValRfnDys;
	}

	public List<String> getSurrValPol() {
		return surrValPol;
	}

	public void setSurrValPol(List<String> surrValPol) {
		this.surrValPol = surrValPol;
	}

	public List<List<String>> getSurrValCovr() {
		return surrValCovr;
	}

	public void setSurrValCovr(List<List<String>> surrValCovr) {
		this.surrValCovr = surrValCovr;
	}

	public void initialise(){
		surrValRfnDys = new ArrayList<>();
		surrValPol = new ArrayList<>();
	  	surrValCovr = new ArrayList<>();
	}

	@Override
	public void initialize() {
	
	}

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}