package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.PymtpfDAO;
import com.csc.fsu.general.dataaccess.model.Pymtpf;
import com.csc.life.terminationclaims.screens.Sjl53ScreenVars;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

/**
* CLAIM PAYMENT SUB MENU.
**/

public class Pjl53 extends ScreenProgCS{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl53.class);
	private static final String SJL53 = "Sjl53";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL53");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sjl53ScreenVars sv = ScreenProgram.getScreenVars( Sjl53ScreenVars.class);
	private List<Pymtpf> pymtpfList;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private PymtpfDAO pymtpfDAO = getApplicationContext().getBean("pymtpfDAO",PymtpfDAO.class);
	private ZonedDecimalData startingIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaRecordFoundFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaNoRecordFound = new Validator(wsaaRecordFoundFlag, "N");
	private Validator wsaaRecordFound = new Validator(wsaaRecordFoundFlag, "Y");
	
	private FixedLengthStringData wsaaPaymentFrom = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaPayee = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPaymentDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaAmountFromvalue = new ZonedDecimalData(17,2);
	private ZonedDecimalData wsaaAmountTovalue = new ZonedDecimalData(17,2);
	private FixedLengthStringData wsaaPaymentMethod = new FixedLengthStringData(1);
	private FixedLengthStringData wsaabankCode= new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPaymentStatus = new FixedLengthStringData(2);
	private boolean isFilterSame;
	private boolean isFilterBlank = false;
	private Clntpf clntpf;
	private FixedLengthStringData wsaaOwnerselSave = new FixedLengthStringData(10);
	private static final String E304 = "E304";
	private static final String E032 = "E032";
	

	public Pjl53() {
		super();
		screenVars = sv;
		new ScreenModel(SJL53, AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.info("mainline {}", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo {}", e);
		}
}

	@Override
	protected void initialise1000()
		{
			initialise1001();
		}
	

	protected void initialise1001()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		initializeWsaaVariables();
			
		sv.paymentFrom.set(SPACES);
		sv.payee.set(SPACES);
		sv.paymentDate.set(varcom.maxdate);
		sv.amountTo.set(0);
		sv.amountFrom.set(0);	
		if(pymtpfList != null) {
			pymtpfList.clear();
		}
		scrnparams.function.set(Varcom.sclr);
		processScreen(SJL53, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		pymtpfList = pymtpfDAO.getpaymentList();
		loadSubfile1002(pymtpfList);
		if (pymtpfList.isEmpty()) {			
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		
	}
	
	private void initializeWsaaVariables() {
		initialize(wsaaPaymentFrom);
		initialize(wsaaPayee);
		wsaaPaymentDate.set(99999999);
		initialize(wsaaAmountFromvalue);
		initialize(wsaaAmountTovalue);
		initialize(wsaaPaymentMethod);
		initialize(wsaabankCode);
		initialize(wsaaPaymentStatus);
	}

	protected void loadSubfile1002(List<Pymtpf> pymtpfList2)
	{
		try{
			startingIndex.set(1);
		for (Pymtpf pymtpf: pymtpfList2){
				sv.seqenum.set(startingIndex);
				sv.paymentNum.set(pymtpf.getPaynum_pymt());
				sv.paymtDate.set(pymtpf.getPaydate());
				sv.clntId.set(pymtpf.getPayee().concat(",").concat(pymtpf.getClntname()));	
				sv.amount.set(pymtpf.getTotalpyblamount());
				Descpf descpfObj;
				descpfObj = descDAO.getdescData("IT",  "T3672", pymtpf.getReqntype(), wsspcomn.company.toString(),
						wsspcomn.language.toString());
				sv.bankAccount.set(pymtpf.getBankacckey());
				if(descpfObj != null) {
					sv.paymtMethod.set(descpfObj.getLongdesc());
				}
				Descpf descpfObj1;
				descpfObj1 = descDAO.getdescData("IT",  "T3593", pymtpf.getProcind(), wsspcomn.company.toString(),
						wsspcomn.language.toString());
				if(descpfObj1 != null) {
					sv.paymtStatus.set(pymtpf.getProcind().concat(" ").concat(descpfObj1.getLongdesc().trim()));
				}
				
				scrnparams.function.set(Varcom.sadd);
				screenIo9000();
				startingIndex.add(1);
			}
		}
		catch(Exception e){
			LOGGER.info("loadSubfile1002 {}", e);
		}
	}

	protected void loadEmptySubfile1003() {
		sv.seqenum.set(SPACES);
		sv.paymentNum.set(SPACES);
		sv.paymtDateDisp.set(0);
		sv.clntId.set(SPACES);
		sv.amount.set(SPACES);
		sv.paymtMethod.set(SPACES);
		sv.bankAccount.set(SPACES);
		sv.paymtStatus.set(SPACES);
		scrnparams.function.set(Varcom.sadd);
		screenIo9000();
	}
	
	@Override
	protected void preScreenEdit() {
		return;
		
	}
	
	protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen(SJL53, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	@Override
	protected void screenEdit2000() {
		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		screenEditSrnch();
		checkForRecordSelect2030();
		if(!wsaaRecordFound.isTrue()) {
			wsspwindow.value.set(SPACES);
		}
		if (wsaaRecordFound.isTrue() || isFilterSame) {
			isFilterSame = false;
			return;
		}
		if (isEQ(wsaaOwnerselSave, SPACES)
				|| isNE(sv.payee, wsaaOwnerselSave)) {
			formatClientName1700();
		}
		if(isEQ(sv.paymentDate, 0)) {
			sv.paymentDateErr.set(E032);
		}
		filterSearch();
		saveNewFilters2060();
		scrnparams.subfileRrn.set(1);
		
	}

	private void screenEditSrnch() {
		scrnparams.function.set(Varcom.srnch);
		processScreen(SJL53, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void checkForRecordSelect2030()
	{
		/* Check to see if a record has been 'Selected' from the screen,
		 and if so load the information from that record into the WSSP
		 fields.*/
		wsaaNoRecordFound.setTrue();
		while ( !(wsaaRecordFound.isTrue()
		|| isEQ(scrnparams.statuz, Varcom.endp))) {
			checkForSelection2100();
		}
		
		if (wsaaRecordFound.isTrue()) {
			loadWsspData2200();
			 /* The exit paragraph name will be standardised to 2090 -EXIT.    
			       GO TO 2900-EXIT. */                                         
			return;
		}
		/*CHECK-FOR-FILTER-CHANGES
		 If the bank description and branch description in the start and
		 scan areas of the screen have not been changed, no action needed*/
		checkFilterIfSame();
	}

	private void checkFilterIfSame() {
		
		boolean isFilterSame1 = false;
		if (isEQ(sv.paymentFrom, wsaaPaymentFrom)
		&& isEQ(sv.paymentDate, wsaaPaymentDate)
		&& isEQ(sv.payee, wsaaPayee)) {
			isFilterSame1 = true;
		}
		if(isFilterSame1 && isEQ(sv.amountFrom, wsaaAmountFromvalue)
		&& isEQ(sv.amountTo, wsaaAmountTovalue)
		&& isEQ(sv.paymentMethod, wsaaPaymentMethod)) {
			isFilterSame1 = true;
		}else {
			isFilterSame1 = false;
		}
		
		if(isFilterSame1 && isEQ(sv.bankcode, wsaabankCode)
		&& isEQ(sv.paymentStatus, wsaaPaymentStatus)) { 
			isFilterSame = true;
		}
	}
	
	protected void checkForSelection2100()
	{
		//CHECK
		if (isNE(sv.select, SPACES)) {
			wsaaRecordFound.setTrue();
		}
		//EXIT
		//**     LOAD WSSP FIELDS FROM SELECTED DATABASE RECORD*//*
	}
	
	protected void loadWsspData2200()
	{
		check2200();
	}

protected void check2200()
	{
		 /*When a record has been selected on the screen, the database
		 record is read for the selection to obtain additional fields
		 required for passing back via the WSSP fields.*/
		wsspwindow.value.set(sv.paymentNum);
		wsspwindow.confirmation.set(sv.clntId);
		
	}

	protected void filterSearch() {
					
			scrnparams.function.set(Varcom.sclr);
			screenIo9000();
			List<Pymtpf> filteredList = new ArrayList<>();
			
			String paymentFromValue = sv.paymentFrom.toString().trim();
			boolean isPaymentFromEmpty = false;
			if(("".equalsIgnoreCase(paymentFromValue) || ("000000000".equals(paymentFromValue)))) {
				isPaymentFromEmpty = true;
			}
			String paymentDateStr = sv.paymentDate.toString().trim();
			boolean isPaymentDateEmpty = false;
			if("".equalsIgnoreCase(paymentDateStr) || "99999999".equalsIgnoreCase(paymentDateStr)) {
				isPaymentDateEmpty = true;
			}
			
			String payeeStr = sv.payee.toString().trim();
			boolean isPayeeEmpty = "".equalsIgnoreCase(payeeStr);
			List<String> pymtListByPayee = new ArrayList<>();
			if(!isPayeeEmpty) {
				pymtListByPayee = pymtpfDAO.getPymtpfListByPayee(payeeStr);
			}
			
			ZonedDecimalData amountFromvalue = sv.amountFrom;
			boolean isAmountFromZero = false;
			if(isEQ(amountFromvalue,0)) {
				isAmountFromZero = true;
			}
			ZonedDecimalData amountTovalue = sv.amountTo;
			boolean isAmountToZero = false;
			if(isEQ(amountTovalue,0)) {
				isAmountToZero = true;
			}
			String paymentMethodStr = sv.paymentMethod.toString().trim();
			boolean isPaymentMethodEmpty = "".equalsIgnoreCase(paymentMethodStr);
			String bankCodeStr = sv.bankcode.toString().trim();
			boolean isBankCodeEmpty = "".equalsIgnoreCase(bankCodeStr);
			String paymentStatusStr = sv.paymentStatus.toString().trim();
			boolean ispaymentStatusEmpty = "".equalsIgnoreCase(paymentStatusStr);

			for (Pymtpf pymtpfRec : pymtpfList) {
				boolean payeeCheck = false;
				for(String payee1: pymtListByPayee) {
					if(payee1.equals(pymtpfRec.getPaynum_pymt())) {
						payeeCheck = true;
					}
				}
				boolean amountCheck = false;
				if((isAmountFromZero || isGTE(pymtpfRec.getTotalpyblamount(), amountFromvalue))
						&& (isAmountToZero || (isLTE(pymtpfRec.getTotalpyblamount(), amountTovalue)))) {
					amountCheck = true;
				}else {
					amountCheck = false;
				}
				if(!isAmountFromZero && (isGTE(pymtpfRec.getTotalpyblamount(), amountFromvalue) 
						&& (!isAmountToZero && (isLTE(pymtpfRec.getTotalpyblamount(), amountTovalue))))) {
					amountCheck = true;
				}
				if ((isPaymentFromEmpty || (Integer.parseInt(paymentFromValue) <= Integer.valueOf(pymtpfRec.getPaynum_pymt()))) 
					&& (isPaymentDateEmpty || (pymtpfRec.getPaydate() == Integer.valueOf(paymentDateStr))) 
					&& (isPayeeEmpty || payeeCheck) 
					&& amountCheck 
					&& (isPaymentMethodEmpty || (pymtpfRec.getReqntype().equals(paymentMethodStr))) 
					&& (isBankCodeEmpty || (pymtpfRec.getReqnbcde().equals(bankCodeStr))) 
					&& (ispaymentStatusEmpty || (pymtpfRec.getProcind().equals(paymentStatusStr)))) {
						filteredList.add(pymtpfRec);
					}
				}
			processPymtpf(filteredList);
			wsspcomn.edterror.set("Y");
		}

		
	protected void processPymtpf(List<Pymtpf> pymtpfTempList) {
		loadSubfile1002(pymtpfTempList);
	} 
	
	protected void saveNewFilters2060()
	{
		/* Move the new START and SCAN filters to WS save areas.*/
		wsaaPaymentFrom.set(sv.paymentFrom);
		wsaaPaymentDate.set(sv.paymentDate);
		wsaaPayee.set(sv.payee);
		wsaaAmountFromvalue.set(sv.amountFrom);
		wsaaAmountTovalue.set(sv.amountTo);
		wsaaPaymentMethod.set(sv.paymentMethod);
		wsaabankCode.set(sv.bankcode);
		wsaaPaymentStatus.set(sv.paymentStatus);
		wsspcomn.edterror.set("Y");
		/**  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/**2900-EXIT.                                                       */
	}
	
	protected void formatClientName1700()
	{
		readClientRecord1710();
	}

	protected void readClientRecord1710()
	{
		if (isNE(sv.payee, SPACES)) {
			
		wsaaOwnerselSave.set(sv.payee);
		clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), sv.payee.toString().trim());
		if (null == clntpf) {
			sv.payeeErr.set(E304);
			sv.payeeName.set(SPACES);
		}
		else {
			plainname();
			sv.payeeName.set(wsspcomn.longconfname);
		}
		}
	}
	
	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getLgivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getLsurname().trim(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getLgivname().trim(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}
	
	protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname().trim(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname().trim(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}
	
	@Override
	protected void update3000() {
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}
	
	@Override
	protected void whereNext4000()	{
		wsspcomn.nextprog.set(SPACES);
		clearSavedFilter();
	}

	private void clearSavedFilter() {
		wsaaPaymentFrom.clear();
		wsaaPaymentDate.clear();
		wsaaPayee.clear();
		wsaaAmountFromvalue.clear();
		wsaaAmountTovalue.clear();
		wsaaPaymentMethod.clear();
		wsaabankCode.clear();
		wsaaPaymentStatus.clear();
	}
		
}
