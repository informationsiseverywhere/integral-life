/*
 * File: Regbpay.java
 * Date: 30 August 2009 2:04:39
 * Author: Quipoz Limited
 * 
 * Class transformed from REGBPAY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Subcoderec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.annuities.dataaccess.dao.AnnypaypfDAO;
import com.csc.life.annuities.dataaccess.dao.AnnyregpfDAO;
import com.csc.life.annuities.dataaccess.model.Annypaypf;
import com.csc.life.annuities.dataaccess.model.Annyregpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.terminationclaims.tablestructures.T6624rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.life.terminationclaims.tablestructures.Taxcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;

/**
* <pre>
*REMARKS.
*
*
*  REGBPAY - Regular Benefit Payments Subroutine.
*  ----------------------------------------------
*
*  Overview.
*  ---------
*
*  This subroutine will be called from the main module  of  the
*  Regular  Payments  batch job. It will be registered on T5671
*  which is keyed by Transaction Code and  Component  Code  and
*  so  the  processing  subroutine called may disagree for each
*  component type.
*
*  It will perform the actual payment  processing  for  Regular
*  Benefit  components  for  which Regular Payment records have
*  been created. It will be called when the Next  Payment  Date
*  on  the  Regular  Payment  record  is  reached  or passed in
*  relation to the Effective Date of the batch run.
*
*  Its  duties  will  be  to  make  the  Sub-Account   movement
*  postings  for the 'From' Sub-Account from where the money is
*  obtained and for the 'To' Sub-Account to where the money  is
*  destined.  If  the  Method  of Payment details indicate that
*  payment is to be external rather than  internal  as  in  the
*  case  of  a  premium waiver claim, ie. bank details or payee
*  details are required, then a cheque requisition record  will
*  also be generated.
*
*  If  the  Regular  Payment record has reached its Anniversary
*  Date then this module will call the  Anniversary  Processing
*  subroutine,  if  one  exists,  and  will  also  advance  the
*  Anniversary Date by one Anniversary Frequency.
*
*  There is a basic and important assumption  made  here;  that
*  the  anniversary  frequency  will  not  be  greater than the
*  payment frequency. The implication of this  is  inherent  in
*  the  structure  of  the  program  which  will  carry out the
*  indexation processing before the payment processing  if  the
*  Anniversary  Date  indicates  that this is necessary, but it
*  will then go on to perform the payment processing  and  will
*  NOT  attempt to perform the indexation more than once BEFORE
*  carrying out the payment  processing.  The  indexation  will
*  therefore  not be carried out more than once between payment
*  frequencies.
*
*
*  NB. The  Anniversary  Date,  Frequency  and  Subroutine  are
*  sometimes  referred to as the Indexation Date, Frequency and
*  Subroutine.
*
*  The  subroutine  will  be  called   using   the   subroutine
*  regpsubrec3.  It will also use this to pass information on to
*  the Indexation subroutine if it is called.
*
*  Processing.
*  -----------
*
*  Set the return STATUZ in linkage to O-K.
*
*  Perform a RETRV on REGP.
*
*  Perform a READR on CHDRRGP.
*
*  Read table T3695 for description.
*
*  Read table T6694.
*
*  Read table T6696.
*
*  Read table T5645.
*
*  Check Anniversary Processing.
*  -----------------------------
*
*  The first task is to see whether the  indexation  processing
*  should be invoked before performing the payment processing.
*
*  Check  the Anniversary Date on REGP. If this is less than or
*  equal to the Effective Date AND less than or  equal  to  the
*  Next  Payment  Date  then Anniversary Processing is required
*  so proceed as follows:
*
*       The Indexation Subroutine  will  be  called.  Call  the
*       subroutine  using  the  linkage  area  in  the copybook
*       regpsubrec3. On return check the STATUZ  code.  If  this
*       is  not  O-K  then  use it and the linkage area to pass
*       control immediately back to the calling module.
*
*       Perform a RETRV on REGP
*
*       Advance  the  Anniversary  Date   by   one   Indexation
*       Frequency, (from T6696).
*
*
*  Payment Processing.
*  -------------------
*
*  If  the  payment  currency  is  different  from the contract
*  currency then some currency conversion  will  be  necessary.
*  If  this  is the case then call the subroutine XCVRT passing
*  the contract currency as the 'From'  currency,  the  payment
*  currency  from  REGP  as  the  'To' currency and the payment
*  amount from REGP as the 'From' amount.  Use  a  function  of
*  'CVRT'  for  'Convert'.  On return from the subroutine check
*  the return STATUZ. If it is not O-K then  place  it  in  the
*  return STATUZ and pass control back to the calling module.
*
*  Check  if  the  payment  is  purely internal by checking the
*  entry on T6694 for the Method of Payment. If  the  'Contract
*  Details  Required'  flag  on  this  entry  is 'Y' then it is
*  considered  to  be  a  purely  internal   posting   and   no
*  requisition  is  necessary.  If  this  is the case then this
*  program is to make two ACMV postings as follows:
*
*       Use the Sub-Account details from T6696 along  with  the
*       amount,  (PYMNT),  from  the  Regular Payment record to
*       create an ACMV posting for the 'From' account.
*
*       Use the Sub-Account details from the REGP record  along
*       with  the  amount,  (PYMNT),  to create an ACMV posting
*       for the 'To' account.
*
*  If  the  payment is external,  do  the  following  postings:
*
*     Line #         Description
*     ======         ===========
*       3            CONTRACT  level debit to Regular  Payments
*                    funding account.
*       4            COMPONENT level debit to Regular  Payments
*                    funding account.
*       5            CONTRACT level credit to Regular  Payments
*                    disbursement account.
*       6            COMPONENT level credit to Regular Payments
*                    disbursement account.
*
*  The  FSU  Payment  Request  subroutine  PAYREQ  will then be
*  called  to  carry  out  the  postings  and  create the Media
*  Request  Record  on  CHEQPF.  Call  the  subroutine with the
*  following parameters:
*
*  Read T5645 with a key of 'REGBPAY'.
*
*       . Function          - 'REQN'
*
*       . The full Batch Key.
*
*       . 'From' Sub Ledger Details:
*            - Prefix       - Contract Header Prefix, ('CH')
*            - Company      - Contract Header Company, ie. the
*                             REGP Company.
*            - Account      - Contract Number
*
*       . 'To' Sub Ledger Details:
*            - Prefix       - Client Number Prefix, ('CN')
*            - Company      - Client Company, ie. the FSU
*                             Company.
*            - Account      - Client Number
*
*       . 'From' G/L Details:
*            - SACSCODE     - obtained from T5645 line # 1
*            - SACSTYPE     -    "       "    "     "  # 1
*            - G/L Code     -    "       "    "     "  # 1
*            - Sign         -    "       "    "     "  # 1
*
*       . Company Bank Code
*
*       . Effective Date
*
*       Cheque Prefix       -  Either  PRFX-BANK-TAPE,  ('BT'),
*       if  the  Bank  Details  on  REGP  are   non-blank,   or
*       PRFX-SEMI-CHEQ, (XQ').
*
*       Ensure  that the amount is passed in the payment amount
*       is the converted amount
*
*       Call PAYREQ with the  parameters  and  then  on  return
*       check  the  return  STATUZ. If it is not O-K then it is
*       an error message, so use it and  the  linkage  area  to
*       pass control back to the calling module.
*
*  Add  the  amount paid out this time to the Total Amount Paid
*  on REGP and perform a KEEPS. NB. This amount will be in  the
*  Contract currency.
*
*
*  Notes.
*  ------
*
*  If  any bad STATUZ returns are received from any access to a
*  file then place the return code  in  the  subroutine  return
*  STATUZ and return to the calling module.
*
*  If  the  read  of  any  table  entry  fails  then  set  up a
*  meaningful error message and pass this back to  the  calling
*  module in the return STATUZ in the linkage area.
*
*
*  Tables Used.
*  ------------
*
*  . T3695 - Sub Account Types
*            Key: SACSTYPE
*
*  . T5645 - Ledger Posting Details
*            Key: Program Id. ie. REGBPAY.
*
*  . T6694 - Regular Payment Method of Payment
*            Key: Regular Payment M.O.P.
*
*  . T6696 - Regular Claim Detail Rules
*            Key: CRTABLE || Reason Code
*
*  . T3629 - Currency code details
*            Key: Payment currency
*
*  . T6624 - Dissection Methods
*            Key: Dissection method from T6696.
*
*  . T1688 - Transaction Codes
*            Key: Batch Transaction code.
*****************************************************************
* </pre>
*/
public class Regbpay extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected final String wsaaSubr = "REGBPAY   ";
	protected PackedDecimalData wsaaNetPymt = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaAdjamt = new PackedDecimalData(17, 2); //ICIL-556
	protected PackedDecimalData wsaaPayamt = new PackedDecimalData(17, 2); //ICIL-556
	protected PackedDecimalData wsaaConvertedNet = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaConvertedAdj = new PackedDecimalData(17, 2); //ICIL-556
	protected ZonedDecimalData wsaaSub1 = new ZonedDecimalData(1, 0).setUnsigned();
	protected ZonedDecimalData wsaaSub2 = new ZonedDecimalData(1, 0).setUnsigned();
	protected ZonedDecimalData wsaaSub3 = new ZonedDecimalData(1, 0).setUnsigned(); //ICIL-556
	private ZonedDecimalData wsaaSub4 = new ZonedDecimalData(1, 0).setUnsigned(); //ILJ-286
	private ZonedDecimalData wsaaSub5 = new ZonedDecimalData(1, 0).setUnsigned(); //ILJ-286
	
	protected ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private static final String g346 = "G346";
	private static final String g450 = "G450";
	private static final String g522 = "G522";
	private static final String g529 = "G529";
	protected static final String g641 = "G641";
	private static final String h134 = "H134";
	private static final String h135 = "H135";
	private static final String e308 = "E308";
		/* TABLES */
	private static final String t3695 = "T3695";
	private static final String t5645 = "T5645";
	private static final String t6694 = "T6694";
	private static final String t6696 = "T6696";
	private static final String t5688 = "T5688";
	protected static final String t3629 = "T3629";
	private static final String t6624 = "T6624";
		/* FORMATS */
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String itdmrec = "ITEMREC";
	private static final String itemrec = "ITEMREC";
	private static final String regprec = "REGPREC";

	private FixedLengthStringData wsaaT6696Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6696Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6696Key, 0);
	private FixedLengthStringData wsaaT6696Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6696Key, 4);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	protected FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	protected FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	protected FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	protected FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	protected FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	protected ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	protected ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	protected ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected RegpTableDAM regpIO = new RegpTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	protected Syserrrec syserrrec = new Syserrrec();
	protected Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	protected Conlinkrec conlinkrec = new Conlinkrec();
	protected Payreqrec payreqrec2 = new Payreqrec();
	private Varcom varcom = new Varcom();
	protected T5645rec t5645rec = new T5645rec();
	protected T6694rec t6694rec = new T6694rec();
	private T6696rec t6696rec = new T6696rec();
	protected T5688rec t5688rec = new T5688rec();
	protected T3629rec t3629rec = new T3629rec();
	protected Subcoderec subcoderec = new Subcoderec();
	private T6624rec t6624rec = new T6624rec();
	private Taxcpy taxcpy = new Taxcpy();
	private Trmreasrec trmreasrec = new Trmreasrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Regpsubrec regpsubrec3 = new Regpsubrec();
	
	private Descpf descpfT1688;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected boolean cmrpy005Permission = false;  //ICIL-556
	/*
	 * ICIL-1043
	 */
	protected boolean cmoth002Permission = false;
	private static final String FEATUREID_DEFERRED_INTEREST_CALCULATION = "CMOTH002";
	private Annyregpf annyregpf = new Annyregpf(); //ILJ-216
	private AnnyregpfDAO annyregpfDAO = getApplicationContext().getBean("annyregpfDAO" , AnnyregpfDAO.class);
	boolean suotr008Permission = false; //ILJ-216
	
	private AnnypaypfDAO annypaypfDAO = getApplicationContext().getBean("annypaypfDAO" , AnnypaypfDAO.class);//ILJ-286
	BigDecimal witholdtax = BigDecimal.ZERO; //ILJ-286
	private PackedDecimalData wsaaIntAmt = new PackedDecimalData(17, 2);
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		currencyCheck211, 
		exit219
	}

	public Regbpay() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		regpsubrec3.batcsubRec = convertAndSetParam(regpsubrec3.batcsubRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		regpsubrec3.subSubrname.set(wsaaSubr);
		initialise100();
		if (isEQ(regpsubrec3.subStatuz, varcom.oK)) {
			checkAnniversaryProc200();
			getDissection210();
		}
		if (isEQ(regpsubrec3.subStatuz, varcom.oK)) {
			paymentProcessing300();
		}
		processReassurance400();
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaNetPymt.set(ZERO);
		suotr008Permission  = FeaConfg.isFeatureExist("2", "SUOTR008", appVars, "IT");
		//ICIL-556 start
		cmrpy005Permission = FeaConfg.isFeatureExist("2", "CMRPY005", appVars, "IT");	//ICIL-556  
		if ((cmrpy005Permission)) {
			wsaaAdjamt.set(ZERO);
			wsaaPayamt.set(ZERO);
			wsaaConvertedAdj.set(ZERO);
			wsaaSub3.set(ZERO);
		}
		//ICIL-556 end
		cmoth002Permission = FeaConfg.isFeatureExist("2", FEATUREID_DEFERRED_INTEREST_CALCULATION, appVars, "IT");//ICIL-1043
		wsaaConvertedNet.set(ZERO);
		wsaaSub1.set(ZERO);
		wsaaSub2.set(ZERO);
		regpsubrec3.subStatuz.set(varcom.oK);
		/*  Retrieve REGP record*/
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			regpsubrec3.subStatuz.set(regpIO.getStatuz());
			return ;
		}
		/*  Read in CHDR record*/
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrnum(regpIO.getChdrnum());
		chdrrgpIO.setChdrcoy(regpIO.getChdrcoy());
		chdrrgpIO.setFunction(varcom.readr);
		chdrrgpIO.setFormat(chdrrgprec);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if ((isNE(chdrrgpIO.getStatuz(), varcom.oK))) {
			regpsubrec3.subStatuz.set(chdrrgpIO.getStatuz());
			return ;
		}
		/*  Read table T6696*/
		wsaaT6696Crtable.set(regpIO.getCrtable());
		wsaaT6696Cltype.set(regpIO.getPayreason());
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(regpsubrec3.subCompany);
		itdmIO.setFormat(itdmrec);
		itdmIO.setItempfx("IT");
		itdmIO.setItemtabl(t6696);
		itdmIO.setItemitem(wsaaT6696Key);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec3.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), regpsubrec3.subCompany)
		|| isNE(itdmIO.getItempfx(), "IT")
		|| isNE(itdmIO.getItemtabl(), t6696)
		|| isNE(itdmIO.getItemitem(), wsaaT6696Key)) {
			regpsubrec3.subStatuz.set(g522);
			return ;
		}
		else {
			t6696rec.t6696Rec.set(itdmIO.getGenarea());
		}
		/*  Read table T6694*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(regpsubrec3.subCompany);
		itdmIO.setFormat(itdmrec);
		itdmIO.setItemtabl(t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(regpIO.getRgpymop());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec3.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), regpsubrec3.subCompany)
		|| isNE(itdmIO.getItempfx(), "IT")
		|| isNE(itdmIO.getItemtabl(), t6694)
		|| isNE(itdmIO.getItemitem(), regpIO.getRgpymop())) {
			regpsubrec3.subStatuz.set(g529);
			return ;
		}
		else {
			t6694rec.t6694Rec.set(itdmIO.getGenarea());
		}
		/* Read table T5645 in order to obtain the sub account details **/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(regpIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("REGBPAY");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			regpsubrec3.subStatuz.set(h134);
			return ;
		}
		else {
			t5645rec.t5645Rec.set(itemIO.getGenarea());
		}
		/* Read T5688 to find out if contract type wants Component         */
		/*  level Accounting.                                              */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(regpsubrec3.subCompany);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItmfrm(regpsubrec3.subEffdate);
		itdmIO.setItemitem(regpsubrec3.subCnttype);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec3.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if ((isNE(itdmIO.getItemcoy(), regpsubrec3.subCompany)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), regpsubrec3.subCnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec3.subStatuz.set(e308);
			return ;
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		/*   Read T3695 to obtain the 'From' transaction description.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(regpIO.getChdrcoy());
		descIO.setDesctabl(t3695);
		descIO.setLanguage(regpsubrec3.subLanguage);
		descIO.setDescitem(t5645rec.sacstype01);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			regpsubrec3.subStatuz.set(h135);
			return ;
		}
		payreqrec2.trandesc.set(descIO.getLongdesc());
		/*  Ensure that the sacscode,type, GL account, sign and control    */
		/*  all exist on T5645. They are vital for the Payment request.    */
		/*  If the payment is at component level test line 2 of T5645 else */
		/*  test line 1.                                                   */
		/*  If they have not been entered, let the user know, ie error.    */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			if (isEQ(t5645rec.sacscode[2], SPACES)
			|| isEQ(t5645rec.sacstype[2], SPACES)
			|| isEQ(t5645rec.glmap[2], SPACES)
			|| isEQ(t5645rec.sign[2], SPACES)) {
				/*       T5645-SIGN     (2)   = SPACES OR              <A06297>*/
				/*       T5645-CNTTOT   (2)   = ZEROES                 <A06297>*/
				regpsubrec3.subStatuz.set(g450);
				return ;
			}
		}
		else {
			if (isEQ(t5645rec.sacscode[1], SPACES)
			|| isEQ(t5645rec.sacstype[1], SPACES)
			|| isEQ(t5645rec.glmap[1], SPACES)
			|| isEQ(t5645rec.sign[1], SPACES)) {
				/*       T5645-SIGN     (1)   = SPACES OR              <A06297>*/
				/*       T5645-CNTTOT   (1)   = ZEROES                 <A06297>*/
				regpsubrec3.subStatuz.set(g450);
				return ;
			}
		}
	}

protected void checkAnniversaryProc200()
	{
		para201();
	}

protected void para201()
	{
		if ((isLTE(regpIO.getAnvdate(), regpsubrec3.subEffdate))) {
			if ((isLTE(regpIO.getAnvdate(), regpIO.getNextPaydate()))) {
				/*NEXT_SENTENCE*/
			}
			else {
				return ;
			}
		}
		else {
			return ;
		}
	
		if(isNE(t6696rec.inxsbm,SPACES))
		{	
		regpsubrec3.subStatuz.set(SPACES);
		callProgram(t6696rec.inxsbm, regpsubrec3.batcsubRec);
		if (isNE(regpsubrec3.subStatuz, varcom.oK)) {
			return ;
		}
		
		regpsubrec3.subSubrname.set(wsaaSubr);
		/*  Retrieve the REGP record to obtain indexation changes*/
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			regpsubrec3.subStatuz.set(regpIO.getStatuz());
			/*   GO TO 190-EXIT                                            */
			return ;
		}
		}
		if(isNE(t6696rec.inxfrq,SPACES))
		{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.intDate1.set(regpIO.getAnvdate());
		datcon2rec.frequency.set(t6696rec.inxfrq);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(datcon2rec.statuz);
			return ;
		}
		else {
			regpIO.setAnvdate(datcon2rec.intDate2);
		}
		}
	}

protected void getDissection210()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start210();
				case currencyCheck211: 
					currencyCheck211();
				case exit219: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start210()
	{
		//ICIL-556 start
        if (cmrpy005Permission) {
        	if(isEQ(regpIO.getNetamt(), ZERO))
        	{
        		wsaaNetPymt.set(regpIO.getPymt());
        	}
        	else
        	{
        		wsaaNetPymt.set(regpIO.getNetamt());
        	}
			wsaaAdjamt.set(regpIO.getAdjamt());
		} else {
			wsaaNetPymt.set(regpIO.getPymt());
		}	
        wsaaIntAmt.set(regpIO.getInterestamt());
		//ICIL-556 end
		if (isEQ(t6696rec.dissmeth, SPACES)) {
			goTo(GotoLabel.currencyCheck211);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(regpsubrec3.subCompany);
		itdmIO.setFormat(itemrec);
		itdmIO.setItemtabl(t6624);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(t6696rec.dissmeth);
		itdmIO.setItmfrm(regpsubrec3.subEffdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			regpsubrec3.subStatuz.set(itdmIO.getStatuz());
			goTo(GotoLabel.exit219);
		}
		if (isNE(regpsubrec3.subCompany, itdmIO.getItemcoy())
		|| isNE(t6624, itdmIO.getItemtabl())
		|| isNE(t6696rec.dissmeth, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			regpsubrec3.subStatuz.set(g346);
			goTo(GotoLabel.exit219);
		}
		t6624rec.t6624Rec.set(itdmIO.getGenarea());
		if (isEQ(t6624rec.premsubr, SPACES)) {
			goTo(GotoLabel.currencyCheck211);
		}
		if (isEQ(regpIO.getRegpayfreq(), "00")) {
			goTo(GotoLabel.currencyCheck211);
		}
		/* Set up call to dissection subroutine.                        */
		taxcpy.taxcpyRec.set(SPACES);
		taxcpy.statuz.set(varcom.oK);
		taxcpy.function.set(SPACES);
		taxcpy.chdrcoy.set(regpIO.getChdrcoy());
		taxcpy.chdrnum.set(regpIO.getChdrnum());
		taxcpy.life.set(regpIO.getLife());
		taxcpy.coverage.set(regpIO.getCoverage());
		taxcpy.rider.set(regpIO.getRider());
		taxcpy.planSuffix.set(regpIO.getPlanSuffix());
		compute(taxcpy.tranno, 0).set(add(chdrrgpIO.getTranno(), 1));
		taxcpy.crtable.set(regpIO.getCrtable());
		taxcpy.cntcurr.set(chdrrgpIO.getCntcurr());
		taxcpy.batckey.set(SPACES);
		taxcpy.batcpfx.set(regpsubrec3.subBatcpfx);
		taxcpy.batctrcde.set(regpsubrec3.subBatctrcde);
		taxcpy.batcactyr.set(regpsubrec3.subBatcactyr);
		taxcpy.batcactmn.set(regpsubrec3.subBatcactmn);
		taxcpy.batcbatch.set(regpsubrec3.subBatcbatch);
		taxcpy.batcbrn.set(regpsubrec3.subBatcbrn);
		taxcpy.batccoy.set(regpsubrec3.subCompany);
		taxcpy.user.set(regpsubrec3.subUser);
		taxcpy.termid.set(regpsubrec3.subTermid);
		taxcpy.cnttype.set(chdrrgpIO.getCnttype());
		taxcpy.effdate.set(regpIO.getCrtdate());
		taxcpy.pymt.set(regpIO.getPymt());
		taxcpy.language.set(regpsubrec3.subLanguage);
		taxcpy.paycurr.set(regpIO.getCurrcd());
		taxcpy.nextPaydate.set(regpIO.getNextPaydate());
		taxcpy.regpPrcnt.set(regpIO.getPrcnt());
		taxcpy.regpayfreq.set(regpIO.getRegpayfreq());
		taxcpy.prcnt.set(t6624rec.percent);
		taxcpy.capcont.set(ZERO);
		taxcpy.taxamount.set(ZERO);
		/* Call the T6624-PREMSUBR.                                     */
		callProgram(t6624rec.premsubr, taxcpy.taxcpyRec);
		if (isNE(taxcpy.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(taxcpy.statuz);
			goTo(GotoLabel.exit219);
		}
		compute(wsaaNetPymt, 2).set(sub(wsaaNetPymt, taxcpy.taxamount));
	}

	/**
	* <pre>
	**** Convert WSAA-NET-PYMT if the payment and contract currencies 
	**** differ.                                                      
	* </pre>
	*/
protected void currencyCheck211()
	{
		wsaaConvertedAdj.set(wsaaAdjamt);
		wsaaConvertedNet.set(wsaaNetPymt);
		if (isEQ(chdrrgpIO.getCntcurr(), regpIO.getCurrcd())) {
			return ;
		}
		
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(regpIO.getNextPaydate());
		conlinkrec.currIn.set(chdrrgpIO.getCntcurr());
		conlinkrec.currOut.set(regpIO.getCurrcd());
		conlinkrec.amountIn.set(wsaaNetPymt);
		conlinkrec.company.set(regpIO.getChdrcoy());
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(conlinkrec.statuz);
			return ;
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		wsaaConvertedNet.set(conlinkrec.amountOut);
		
		//ICIL-556 start
		if (cmrpy005Permission) {
			
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.rateUsed.set(ZERO);
			conlinkrec.cashdate.set(regpIO.getNextPaydate());
			conlinkrec.currIn.set(chdrrgpIO.getCntcurr());
			conlinkrec.currOut.set(regpIO.getCurrcd());
			conlinkrec.amountIn.set(wsaaAdjamt);
			conlinkrec.company.set(regpIO.getChdrcoy());
			conlinkrec.function.set("CVRT");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				regpsubrec3.subStatuz.set(conlinkrec.statuz);
				return ;
			}
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			wsaaConvertedAdj.set(conlinkrec.amountOut);
		}
		//ICIL-556 end
	}

protected void paymentProcessing300()
	{
		/*PARA*/
		/* Check if the payment currency and the contract currency differ*/
		
		if (isNE(regpsubrec3.subStatuz, varcom.oK)) {
			return ;
		}
		//ICIL-556 start
		if (cmrpy005Permission) {
			compute(wsaaPayamt, 2).set(sub(wsaaNetPymt, wsaaAdjamt));
			
			if (isGT(wsaaIntAmt,ZERO))
			{
				compute(wsaaPayamt, 2).set(sub(wsaaPayamt, wsaaIntAmt));
			}
		}
		if (isNE(chdrrgpIO.getCntcurr(), regpIO.getCurrcd())) {
			currencyConvert310();
		}
		//ICIL-556 end
		// ICIL-1043 START
		/*if (cmoth002Permission) {
			wsaaNetPymt.set(regpIO.getPymt());
		}*/
		// ICIL-1043 END
		String coy = regpsubrec3.subBatccoy.toString();
		if(descpfT1688 == null || !regpsubrec3.subBatctrcde.trim().equals(descpfT1688.getDescitem().trim()))
		{
			descpfT1688 = descDAO.getdescData("IT",  "T1688",regpsubrec3.subBatctrcde.toString(),coy, regpsubrec3.subLanguage.toString());			
		}
		if (isEQ(t6694rec.contreq, "Y")) {
			postFromAcmv320();
			postToAcmv330();
		}
		else {
			postings360();
			callPayreq340();
		}
		updateRegp350();
		/*EXIT*/
	}

protected void currencyConvert310()
	{
		para311();
	}

protected void para311()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(regpIO.getNextPaydate());
		conlinkrec.currIn.set(chdrrgpIO.getCntcurr());
		conlinkrec.currOut.set(regpIO.getCurrcd());
		/* MOVE REGP-PYMT              TO CLNK-AMOUNT-IN.               */
		conlinkrec.amountIn.set(wsaaPayamt);
		/* MOVE REGP-PAYCOY            TO CLNK-COMPANY.                 */
		conlinkrec.company.set(regpIO.getChdrcoy());
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(conlinkrec.statuz);
			return ;
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void postFromAcmv320()
	{
		para321();
	}

protected void para321()
	{
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		//lifacmvrec1.batckey.set(regpsubrec3.subBatckey);
		//lifacmvrec1.filler(regpsubrec3.subBatcpfx);
		lifacmvrec1.batccoy.set(regpsubrec3.subBatccoy);
		lifacmvrec1.batcactyr.set(regpsubrec3.subBatcactyr);
		lifacmvrec1.batcactmn.set(regpsubrec3.subBatcactmn);
		lifacmvrec1.batcbatch.set(regpsubrec3.subBatcbatch);
		lifacmvrec1.batcbrn.set(regpsubrec3.subBatcbrn);
		lifacmvrec1.batctrcde.set(regpsubrec3.subBatctrcde);
		if(descpfT1688 == null)
		{
			descpfT1688 = new Descpf();
	         descpfT1688.setLongdesc("");
		}
        lifacmvrec1.trandesc.set(descpfT1688.getLongdesc());
		lifacmvrec1.rdocnum.set(regpIO.getChdrnum());
		compute(lifacmvrec1.tranno, 0).set(add(chdrrgpIO.getTranno(), 1));
		lifacmvrec1.sacscode.set(t6696rec.sacscode);
		lifacmvrec1.sacstyp.set(t6696rec.sacstype);
		lifacmvrec1.glsign.set(t6696rec.sign);
		lifacmvrec1.glcode.set(t6696rec.glact);
		lifacmvrec1.jrnseq.set(1);
		lifacmvrec1.rldgcoy.set(regpIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(regpIO.getChdrcoy());
		lifacmvrec1.rldgacct.set(regpIO.getChdrnum());
		//TMLII-294 AC-01-007 START
		//lifacmvrec1.rldgacct.set(regpIO.getChdrnum());
		wsaaPlan.set(regpIO.getPlanSuffix());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(regpIO.getChdrnum());
		stringVariable1.append(regpIO.getLife());
		stringVariable1.append(regpIO.getCoverage());
		stringVariable1.append(regpIO.getRider());
		stringVariable1.append(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(stringVariable1);
		//TMLII-294 AC-01-007 END
		lifacmvrec1.origcurr.set(regpIO.getCurrcd());
		if (isEQ(regpIO.getCurrcd(), chdrrgpIO.getCntcurr())) {
			/*    MOVE REGP-PYMT           TO LIFA-ORIGAMT                  */
			lifacmvrec1.origamt.set(wsaaNetPymt);
		}
		else {
			lifacmvrec1.origamt.set(wsaaConvertedNet);
		}
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(regpsubrec3.subBatcactyr);
		lifacmvrec1.postmonth.set(regpsubrec3.subBatcactmn);
		lifacmvrec1.tranref.set(regpIO.getChdrnum());
		lifacmvrec1.effdate.set(regpsubrec3.subEffdate);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(regpsubrec3.subCnttype);
		/* Set up Substitution code 6 for Coverage/Rider table             */
		/* MOVE SPACES                 TO LIFA-SUBSTITUTE-CODE(06).<002>*/
		lifacmvrec1.substituteCode[6].set(regpIO.getCrtable());
		varcom.vrcmTranid.set(regpsubrec3.subTranid);
		lifacmvrec1.termid.set(varcom.vrcmTermid);
		lifacmvrec1.user.set(varcom.vrcmUser);
		lifacmvrec1.transactionTime.set(varcom.vrcmTime);
		lifacmvrec1.transactionDate.set(varcom.vrcmDate);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(lifacmvrec1.statuz);
			return ;
		}
	}

protected void postToAcmv330()
	{
		para331();
	}

protected void para331()
	{
		if (isNE(regpsubrec3.subStatuz, varcom.oK)) {
			return ;
		}
		lifacmvrec1.sacscode.set(regpIO.getSacscode());
		lifacmvrec1.sacstyp.set(regpIO.getSacstype());
		lifacmvrec1.glsign.set(regpIO.getDebcred());
		lifacmvrec1.glcode.set(regpIO.getGlact());
		lifacmvrec1.rldgacct.set(regpIO.getDestkey());
		lifacmvrec1.acctamt.set(ZERO);
		if (isEQ(regpIO.getCurrcd(), chdrrgpIO.getCntcurr())) {
			/*    MOVE REGP-PYMT           TO LIFA-ORIGAMT                  */
			lifacmvrec1.origamt.set(wsaaNetPymt);
		}
		else {
			lifacmvrec1.origamt.set(wsaaConvertedNet);
		}
		
		//ICIL-556 start
		if (cmrpy005Permission || cmoth002Permission) {
			lifacmvrec1.origamt.set(wsaaPayamt);
			if (isEQ(regpIO.getCurrcd(), chdrrgpIO.getCntcurr())) {
				lifacmvrec1.origamt.set(wsaaPayamt);
			}
			else {
				
				lifacmvrec1.origamt.set(conlinkrec.amountOut);
			}
		}
		//ICIL-556 end
		//setSuspenseAmount();// ICIL-1043
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(lifacmvrec1.statuz);
			return ;
		}
		
		//ICIL-556 start
		if (cmrpy005Permission) {
			wsaaSub3.set(7);
			lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaSub3.toInt()]);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaSub3.toInt()]);
			lifacmvrec1.glcode.set(t5645rec.glmap[wsaaSub3.toInt()]);
			lifacmvrec1.glsign.set(t5645rec.sign[wsaaSub3.toInt()]);
			//lifacmvrec1.rldgacct.set(regpIO.getDestkey());
			lifacmvrec1.rldgacct.set(regpIO.getChdrnum());
			wsaaPlan.set(regpIO.getPlanSuffix());
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(regpIO.getChdrnum());
			stringVariable1.append(regpIO.getLife());
			stringVariable1.append(regpIO.getCoverage());
			stringVariable1.append(regpIO.getRider());
			stringVariable1.append(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(stringVariable1);
			lifacmvrec1.origamt.set(wsaaAdjamt);
			lifacmvrec1.acctamt.set(wsaaAdjamt);
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz, varcom.oK)) {
				regpsubrec3.subStatuz.set(lifacmvrec1.statuz);
				return ;
			}
		}
		//ICIL-556 end
		postInterest();// ICIL-1043
	}

	/**
	 * ICIL-1043
	 * posting Interest
	 */
	protected void postInterest() {
		if (cmoth002Permission && isNE(regpIO.getInterestamt(),0)) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode[8]);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype[8]);
			lifacmvrec1.glcode.set(t5645rec.glmap[8]);
			lifacmvrec1.glsign.set(t5645rec.sign[8]);
			lifacmvrec1.rldgacct.set(regpIO.getChdrnum());
			lifacmvrec1.origamt.set(regpIO.getInterestamt());
			lifacmvrec1.acctamt.set(regpIO.getInterestamt());
			if (isNE(lifacmvrec1.jrnseq, 1)) {
				wsaaSequenceNo.add(1);
				lifacmvrec1.jrnseq.set(wsaaSequenceNo);
			}
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz, varcom.oK)) {
				regpsubrec3.subStatuz.set(lifacmvrec1.statuz);
				return;
			}
		}
	}
	
protected void callPayreq340()
	{
		para341();
	}

protected void para341()
	{
		payreqrec2.rec.set(SPACES);
		/* Read table T3629 in order to obtain the bankcode            *   */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(regpIO.getChdrcoy());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(regpIO.getCurrcd());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			regpsubrec3.subStatuz.set(g641);
			return ;
		}
		else {
			t3629rec.t3629Rec.set(itemIO.getGenarea());
		}
		/* MOVE REGP-SUB-BATCKEY       TO PAYREQ-BATCKEY.               */
		payreqrec2.bankcode.set(t3629rec.bankcode);
		payreqrec2.batckey.set(regpsubrec3.subBatckey);
		/* MOVE REGP-SUB-COMPANY       TO PAYREQ-COMPANY.          <001>*/
		/* MOVE REGP-SUB-BATCBRN       TO PAYREQ-BRANCH.           <001>*/
		/* MOVE PRFX-CHDR              TO PAYREQ-FRM-RLDGPFX.           */
		/* MOVE REGP-CHDRCOY           TO PAYREQ-FRM-RLDGCOY.           */
		/* MOVE REGP-CHDRNUM           TO PAYREQ-FRM-RLDGACCT           */
		/*                                PAYREQ-CHDRNUM.               */
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			/*     Set up extra PAYREQ fields for Component Level Accounting   */
			wsaaRldgacct.set(SPACES);
			payreqrec2.frmRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(regpIO.getChdrnum());
			wsaaRldgLife.set(regpIO.getLife());
			wsaaRldgCoverage.set(regpIO.getCoverage());
			wsaaRldgRider.set(regpIO.getRider());
			wsaaPlan.set(regpIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			payreqrec2.frmRldgacct.set(wsaaRldgacct);
			/* Use new procedure division copybook SUBCODE to perform the      */
			/*  substitution in the GLCODE field before it is passed to        */
			/*  PAYREQ and written to the PREQ file.                           */
			subcoderec.codeRec.set(SPACES);
			subcoderec.code[1].set(regpsubrec3.subCnttype);
			subcoderec.code[6].set(regpIO.getCrtable());
			subcoderec.glcode.set(t5645rec.glmap02);
			subcode();
			payreqrec2.glcode.set(subcoderec.glcode);
			payreqrec2.sacscode.set(t5645rec.sacscode02);
			payreqrec2.sacstype.set(t5645rec.sacstype02);
			payreqrec2.sign.set(t5645rec.sign02);
			payreqrec2.cnttot.set(t5645rec.cnttot02);
		}
		else {
			payreqrec2.frmRldgacct.set(SPACES);
			payreqrec2.frmRldgacct.set(regpIO.getChdrnum());
			/* Use new procedure division copybook SUBCODE to perform the      */
			/*  substitution in the GLCODE field before it is passed to        */
			/*  PAYREQ and written to the PREQ file.                           */
			subcoderec.codeRec.set(SPACES);
			subcoderec.code[1].set(regpsubrec3.subCnttype);
			subcoderec.glcode.set(t5645rec.glmap01);
			subcode();
			payreqrec2.glcode.set(subcoderec.glcode);
			payreqrec2.sacscode.set(t5645rec.sacscode01);
			payreqrec2.sacstype.set(t5645rec.sacstype01);
			payreqrec2.sign.set(t5645rec.sign01);
			payreqrec2.cnttot.set(t5645rec.cnttot01);
		}
		/* MOVE PRFX-CLNT              TO PAYREQ-TO-RLDGPFX.            */
		/* MOVE REGP-PAYCOY            TO PAYREQ-TO-RLDGCOY.            */
		/* MOVE REGP-PAYCLT            TO PAYREQ-TO-RLDGACCT.           */
		/* MOVE T5645-SACSCODE-01      TO PAYREQ-SACSCODE.              */
		/* MOVE T5645-SACSTYPE-01      TO PAYREQ-SACSTYPE.              */
		/* MOVE T5645-GLMAP-01         TO PAYREQ-GLCODE.                */
		/* MOVE T5645-SIGN-01          TO PAYREQ-SIGN.                  */
		/* MOVE REGP-CHDRNUM           TO PAYREQ-FRM-RLDGACCT.     <001>*/
		payreqrec2.effdate.set(regpsubrec3.subEffdate);
		/* ADD CHDRRGP-TRANNO, 1   GIVING PAYREQ-TRANNO.                */
		payreqrec2.paycurr.set(regpIO.getCurrcd());
		if (isNE(regpIO.getCurrcd(), chdrrgpIO.getCntcurr())) {
			payreqrec2.pymt.set(conlinkrec.amountOut);
		}
		else {
			/*    MOVE REGP-PYMT           TO PAYREQ-PYMT                   */
			payreqrec2.pymt.set(wsaaNetPymt);
		}
		payreqrec2.termid.set(regpsubrec3.subTermid);
		payreqrec2.user.set(regpsubrec3.subUser);
		/* MOVE REGP-SUB-BATCACTMN     TO PAYREQ-POSTMONTH.        <001>*/
		/* MOVE REGP-SUB-BATCACTYR     TO PAYREQ-POSTYEAR.         <001>*/
		/*MOVE REGP-RGPYTYPE          TO PAYREQ-REQNTYPE.         <001>*/
		payreqrec2.reqntype.set(t6694rec.reqntype);
		/* MOVE REGP-SUB-COMPANY       TO PAYREQ-CLNTCOY.          <001>*/
		payreqrec2.clntcoy.set(regpIO.getPaycoy());
		payreqrec2.clntnum.set(regpIO.getPayclt());
		/* IF REGP-BANKKEY          NOT = SPACES                        */
		/*    MOVE PRFX-BANK-TAPE      TO PAYREQ-CHEQPFX                */
		/* ELSE                                                         */
		/*    MOVE PRFX-SEMI-CHEQ      TO PAYREQ-CHEQPFX                */
		/* END-IF.                                                      */
		if (isNE(regpIO.getBankkey(), SPACES)) {
			payreqrec2.bankkey.set(regpIO.getBankkey());
			payreqrec2.bankacckey.set(regpIO.getBankacckey());
		}
		payreqrec2.zbatctrcde.set(regpsubrec3.subBatctrcde);			
		payreqrec2.tranref.set(regpIO.getChdrnum());
		payreqrec2.language.set(regpsubrec3.subLanguage);
		payreqrec2.function.set("REQN");
		regpsubrec3.subSubrname.set("PAYREQ");
		callProgram(Payreq.class, payreqrec2.rec);
		if (isNE(payreqrec2.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(payreqrec2.statuz);
			regpsubrec3.subSubrname.set(payreqrec2.errorFormat);
			return ;
		}
		regpsubrec3.subSubrname.set(wsaaSubr);
	}

protected void updateRegp350()
	{
		/*PARA*/
		if (isNE(regpsubrec3.subStatuz, varcom.oK)) {
			return ;
		}
		setPrecision(regpIO.getTotamnt(), 2);
		regpIO.setTotamnt(add(regpIO.getTotamnt(), regpIO.getPymt()));
		regpIO.setFunction(varcom.keeps);
		regpIO.setFormat(regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			regpsubrec3.subStatuz.set(regpIO.getStatuz());
			return ;
		}
		/*EXIT*/
	}

protected void postings360()
	{
		setUp360();
	}

	
protected void setUp360()
	{
		lifacmvrec1.lifacmvRec.set(SPACES);
		wsaaRldgacct.set(SPACES);
		wsaaSequenceNo.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		/* Read T1688 for transaction code description.                 */
		if(descpfT1688 == null)
		{
			return;
		}
		lifacmvrec1.trandesc.set(descpfT1688.getLongdesc());
		lifacmvrec1.batccoy.set(regpsubrec3.subBatccoy);
		lifacmvrec1.batcbrn.set(regpsubrec3.subBatcbrn);
		lifacmvrec1.batcactyr.set(regpsubrec3.subBatcactyr);
		lifacmvrec1.batcactmn.set(regpsubrec3.subBatcactmn);
		lifacmvrec1.batctrcde.set(regpsubrec3.subBatctrcde);
		lifacmvrec1.batcbatch.set(regpsubrec3.subBatcbatch);
		lifacmvrec1.rdocnum.set(regpIO.getChdrnum());
		compute(lifacmvrec1.tranno, 0).set(add(chdrrgpIO.getTranno(), 1));
		lifacmvrec1.rldgcoy.set(regpsubrec3.subCompany);
		lifacmvrec1.origcurr.set(chdrrgpIO.getCntcurr());
		lifacmvrec1.tranref.set(regpIO.getChdrnum());
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.genlcoy.set(regpsubrec3.subCompany);
		lifacmvrec1.genlcur.set(regpIO.getCurrcd());
		lifacmvrec1.effdate.set(regpsubrec3.subEffdate);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(regpsubrec3.subUser);
		lifacmvrec1.termid.set(regpsubrec3.subTermid);
		lifacmvrec1.substituteCode[1].set(regpsubrec3.subCnttype);
		lifacmvrec1.substituteCode[6].set(regpIO.getCrtable());
		wsaaRldgChdrnum.set(regpIO.getChdrnum());
		/* For component level accounting, the whole entity field       */
		/* LIFA-RLDGACCT must be filled.  For contract level accounting */
		/* only the contract header no. is needed in the entity field   */
		/* LIFA-RLDGACCT.                                               */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			wsaaPlan.set(regpIO.getPlanSuffix());
			wsaaRldgLife.set(regpIO.getLife());
			wsaaRldgCoverage.set(regpIO.getCoverage());
			wsaaRldgRider.set(regpIO.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			
			if(suotr008Permission) //ILJ-216
			{	
				
				annyregpf = annyregpfDAO.getAnnyregpf(regpIO.getChdrcoy().toString(), regpIO.getChdrnum().trim());
				List<Annypaypf> annypaypfList = annypaypfDAO.getAnnypaypfRecords(regpIO.getChdrcoy().toString(), regpIO.getChdrnum().trim());
				if(!annypaypfList.isEmpty())
				{	
					for(Annypaypf annypay : annypaypfList){ 
						if(isEQ(annypay.getAnnpmntopt(),"03")&&isEQ(regpIO.getRgpynum().toInt(),1)){
							witholdtax = BigDecimal.ZERO;
							break;
						}
						else if(isEQ(annypay.getPmntno(),regpIO.getRgpynum().toInt())
								&&isEQ(annypay.getPmntstat().trim(),"PP")){
							witholdtax = annypay.getWitholdtax(); //ILJ-286
						}
					}
				}
				Optional<Annyregpf> isExists = Optional.ofNullable(annyregpf);
				if(isExists.isPresent() && isEQ(chdrrgpIO.getStatcode(),"MA") && isEQ(chdrrgpIO.getPstatcode(),"AR"))
				{
					wsaaSub1.set(7);
					wsaaSub2.set(6);
					
					if(isNE(witholdtax,BigDecimal.ZERO)){ //ILJ-286
						wsaaSub4.set(7);
						wsaaSub5.set(8);
					}
				}
				else
				{
					wsaaSub1.set(4);
					wsaaSub2.set(6);
				}
			}
			else
			{
				wsaaSub1.set(4);
				wsaaSub2.set(6);
				
			}
			
		}
		else {
			wsaaSub1.set(3);
			wsaaSub2.set(5);
		}
		//ICIL-556 start
		if (cmrpy005Permission) {
			wsaaSub3.set(7);
		}
		//ICIL-556 end
		
		if(suotr008Permission && (isEQ(wsaaSub1,7)||isEQ(wsaaSub5,8)))
		{
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(regpIO.getChdrnum());
			wsaaPlan.set("");
			wsaaRldgLife.set("");
			wsaaRldgCoverage.set("");
			wsaaRldgRider.set("");
			wsaaRldgPlanSuffix.set("");
		}
		
		if(suotr008Permission && isEQ(wsaaSub5,8)){
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaSub5.toInt()]);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaSub5.toInt()]);
			lifacmvrec1.glcode.set(t5645rec.glmap[wsaaSub5.toInt()]);
			lifacmvrec1.glsign.set(t5645rec.sign[wsaaSub5.toInt()]);
			lifacmvrec1.contot.set(t5645rec.cnttot[wsaaSub5.toInt()]);
			lifacmvrec1.origamt.set(witholdtax);
			if (isEQ(regpIO.getCurrcd(), chdrrgpIO.getCntcurr())) {
				lifacmvrec1.acctamt.set(witholdtax);
			}
			
			wsaaSequenceNo.add(1);
			lifacmvrec1.jrnseq.set(wsaaSequenceNo);
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz, varcom.oK)) {
				regpsubrec3.subStatuz.set(lifacmvrec1.statuz);
			}
		}
		
		if(isNE(wsaaNetPymt,0) ||  isNE(wsaaConvertedNet,0)){
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		lifacmvrec1.glcode.set(t5645rec.glmap[wsaaSub1.toInt()]);
		lifacmvrec1.glsign.set(t5645rec.sign[wsaaSub1.toInt()]);
		lifacmvrec1.contot.set(t5645rec.cnttot[wsaaSub1.toInt()]);
		lifacmvrec1.origamt.set(wsaaNetPymt);
		if (isEQ(regpIO.getCurrcd(), chdrrgpIO.getCntcurr())) {
			lifacmvrec1.acctamt.set(wsaaNetPymt);
		}
		else {
			lifacmvrec1.acctamt.set(wsaaConvertedNet);
		}
		if(suotr008Permission && isNE(witholdtax,BigDecimal.ZERO)){//ILJ-286
			lifacmvrec1.origamt.set(add(wsaaNetPymt,witholdtax));
			lifacmvrec1.acctamt.set(add(wsaaNetPymt,witholdtax));
		}
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(lifacmvrec1.statuz);
			return ;
		}
		
		if(suotr008Permission && isEQ(wsaaSub2,6))
		{
			wsaaRldgacct.set(SPACES);
			wsaaRldgChdrnum.set(regpIO.getChdrnum());
			wsaaPlan.set(regpIO.getPlanSuffix());
			wsaaRldgLife.set(regpIO.getLife());
			wsaaRldgCoverage.set(regpIO.getCoverage());
			wsaaRldgRider.set(regpIO.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
		}
		

		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaSub2.toInt()]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaSub2.toInt()]);
		lifacmvrec1.glcode.set(t5645rec.glmap[wsaaSub2.toInt()]);
		lifacmvrec1.glsign.set(t5645rec.sign[wsaaSub2.toInt()]);
		lifacmvrec1.contot.set(t5645rec.cnttot[wsaaSub2.toInt()]);
		lifacmvrec1.origamt.set(wsaaNetPymt);
		if (isEQ(regpIO.getCurrcd(), chdrrgpIO.getCntcurr())) {
			lifacmvrec1.acctamt.set(wsaaNetPymt);
		}
		else {
			lifacmvrec1.acctamt.set(wsaaConvertedNet);
		}
		//ICIL-556 start
		if (cmrpy005Permission || cmoth002Permission) {
			lifacmvrec1.origamt.set(wsaaPayamt);
			lifacmvrec1.acctamt.set(wsaaPayamt);
		}
		
		//ICIL-556 end
		//setSuspenseAmount(); // ICIL-1043 
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(lifacmvrec1.statuz);
		}
		}
		
		//ICIL-556 start
		if (cmrpy005Permission && isNE(wsaaAdjamt,0) ) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaSub3.toInt()]);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaSub3.toInt()]);
			lifacmvrec1.glcode.set(t5645rec.glmap[wsaaSub3.toInt()]);
			lifacmvrec1.glsign.set(t5645rec.sign[wsaaSub3.toInt()]);
			lifacmvrec1.contot.set(t5645rec.cnttot[wsaaSub3.toInt()]);
			lifacmvrec1.origamt.set(wsaaAdjamt);
			lifacmvrec1.acctamt.set(wsaaAdjamt);
			wsaaSequenceNo.add(1);
			lifacmvrec1.jrnseq.set(wsaaSequenceNo);
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz, varcom.oK)) {
				regpsubrec3.subStatuz.set(lifacmvrec1.statuz);
			}
		}	
		//ICIL-556 end
		postInterest();// ICIL-1043
	}

	/**
	* <pre>
	* copy in new procedure division copybook                         
	* </pre>
	*/
protected void subcode()
	{
		sbcd100Start();
	}

protected void sbcd100Start()
	{
		if (isNE(subcoderec.code[1], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[1]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[2], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[2]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[3], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[3]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[4], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[4]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[5], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[5]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[6], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[6]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode04));
		}
	}

protected void processReassurance400()
	{
		trmreas401();
	}

protected void trmreas401()
	{
		List<Covrpf> data = covrpfDAO.getcovrtrbRecord(regpIO.getChdrcoy().toString(),regpIO.getChdrnum().toString(),regpIO.getLife().toString(),regpIO.getCoverage().toString(),regpIO.getRider().toString());
		if(data.isEmpty()){
			return;
		}
		Covrpf covrpf = data.get(0);
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("TRMR");
		trmreasrec.chdrcoy.set(covrpf.getChdrcoy());
		trmreasrec.chdrnum.set(covrpf.getChdrnum());
		trmreasrec.life.set(covrpf.getLife());
		trmreasrec.coverage.set(covrpf.getCoverage());
		trmreasrec.rider.set(covrpf.getRider());
		trmreasrec.planSuffix.set(covrpf.getPlanSuffix());
		trmreasrec.crtable.set(covrpf.getCrtable());
		trmreasrec.cnttype.set(chdrrgpIO.getCnttype());
		trmreasrec.polsum.set(chdrrgpIO.getPolsum());
		trmreasrec.effdate.set(regpsubrec3.subEffdate);
		trmreasrec.batckey.set(regpsubrec3.subBatckey);
		trmreasrec.tranno.set(chdrrgpIO.getTranno());
		trmreasrec.language.set(regpsubrec3.subLanguage);
		/* TRRA-BILLFREQ can be SPACES as, because it is Regular          */
		/* Payments, there will never be any need to call the             */
		/* Reserves Routine, for which the Billing Frequency is           */
		/* needed.                                                        */
		trmreasrec.billfreq.set(SPACES);
		trmreasrec.ptdate.set(chdrrgpIO.getPtdate());
		trmreasrec.origcurr.set(chdrrgpIO.getCntcurr());
		trmreasrec.acctcurr.set(covrpf.getPremCurrency());
		trmreasrec.crrcd.set(covrpf.getCrrcd());
		trmreasrec.convUnits.set(covrpf.getConvertInitialUnits());
		trmreasrec.jlife.set(covrpf.getJlife());
		trmreasrec.singp.set(covrpf.getSingp());
		trmreasrec.newSumins.set(ZERO);
		trmreasrec.oldSumins.set(covrpf.getSumins());
		trmreasrec.pstatcode.set(covrpf.getPstatcode());
		trmreasrec.clmPercent.set(regpIO.getPrcnt());
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			return ;
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(regpsubrec3.subCompany);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(regpIO.getCurrcd());
		zrdecplrec.batctrcde.set(regpsubrec3.subBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			regpsubrec3.subStatuz.set(zrdecplrec.statuz);
		}
		/*A000-EXIT*/
	}
}
