package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HdcdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:26
 * Class transformed from HDCDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HdcdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 92;
	public FixedLengthStringData hdcdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hdcdpfRecord = hdcdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hdcdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hdcdrec);
	public PackedDecimalData hactval = DD.hactval.copy().isAPartOf(hdcdrec);
	public PackedDecimalData hemv = DD.hemv.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hdcdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hdcdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HdcdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HdcdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HdcdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HdcdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdcdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HdcdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HdcdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HDCDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"HACTVAL, " +
							"HEMV, " +
							"HCNSTCUR, " +
							"TYPE_T, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     crtable,
                                     hactval,
                                     hemv,
                                     hcnstcur,
                                     fieldType,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		hactval.clear();
  		hemv.clear();
  		hcnstcur.clear();
  		fieldType.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHdcdrec() {
  		return hdcdrec;
	}

	public FixedLengthStringData getHdcdpfRecord() {
  		return hdcdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHdcdrec(what);
	}

	public void setHdcdrec(Object what) {
  		this.hdcdrec.set(what);
	}

	public void setHdcdpfRecord(Object what) {
  		this.hdcdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hdcdrec.getLength());
		result.set(hdcdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}