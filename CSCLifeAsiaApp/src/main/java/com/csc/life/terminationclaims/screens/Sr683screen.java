package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr683screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 5, 18, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr683ScreenVars sv = (Sr683ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr683screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr683ScreenVars screenVars = (Sr683ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.benefits01.setClassString("");
		screenVars.benefits02.setClassString("");
		screenVars.benefits03.setClassString("");
		screenVars.benefits04.setClassString("");
		screenVars.benefits05.setClassString("");
		screenVars.benefits06.setClassString("");
		screenVars.benefits07.setClassString("");
		screenVars.benefits08.setClassString("");
		screenVars.benefits09.setClassString("");
		screenVars.benefits10.setClassString("");
		screenVars.benefits11.setClassString("");
		screenVars.benefits12.setClassString("");
		screenVars.benefits13.setClassString("");
		screenVars.benfamt01.setClassString("");
		screenVars.benfamt02.setClassString("");
		screenVars.benfamt03.setClassString("");
		screenVars.benfamt04.setClassString("");
		screenVars.benfamt05.setClassString("");
		screenVars.benfamt06.setClassString("");
		screenVars.benfamt07.setClassString("");
		screenVars.benfamt08.setClassString("");
		screenVars.benfamt09.setClassString("");
		screenVars.benfamt10.setClassString("");
		screenVars.benfamt11.setClassString("");
		screenVars.benfamt12.setClassString("");
		screenVars.benfamt13.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.claimamt01.setClassString("");
		screenVars.claimamt02.setClassString("");
		screenVars.claimamt03.setClassString("");
		screenVars.claimamt04.setClassString("");
		screenVars.claimamt05.setClassString("");
		screenVars.claimamt06.setClassString("");
		screenVars.claimamt07.setClassString("");
		screenVars.claimamt08.setClassString("");
		screenVars.claimamt09.setClassString("");
		screenVars.claimamt10.setClassString("");
		screenVars.claimamt11.setClassString("");
		screenVars.claimamt12.setClassString("");
		screenVars.claimamt13.setClassString("");
		screenVars.claimtot.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.daclaim01.setClassString("");
		screenVars.daclaim02.setClassString("");
		screenVars.daclaim03.setClassString("");
		screenVars.daclaim04.setClassString("");
		screenVars.daclaim05.setClassString("");
		screenVars.daclaim06.setClassString("");
		screenVars.daclaim07.setClassString("");
		screenVars.daclaim08.setClassString("");
		screenVars.daclaim09.setClassString("");
		screenVars.daclaim10.setClassString("");
		screenVars.daclaim11.setClassString("");
		screenVars.daclaim12.setClassString("");
		screenVars.daclaim13.setClassString("");
	}

/**
 * Clear all the variables in Sr683screen
 */
	public static void clear(VarModel pv) {
		Sr683ScreenVars screenVars = (Sr683ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.benefits01.clear();
		screenVars.benefits02.clear();
		screenVars.benefits03.clear();
		screenVars.benefits04.clear();
		screenVars.benefits05.clear();
		screenVars.benefits06.clear();
		screenVars.benefits07.clear();
		screenVars.benefits08.clear();
		screenVars.benefits09.clear();
		screenVars.benefits10.clear();
		screenVars.benefits11.clear();
		screenVars.benefits12.clear();
		screenVars.benefits13.clear();
		screenVars.benfamt01.clear();
		screenVars.benfamt02.clear();
		screenVars.benfamt03.clear();
		screenVars.benfamt04.clear();
		screenVars.benfamt05.clear();
		screenVars.benfamt06.clear();
		screenVars.benfamt07.clear();
		screenVars.benfamt08.clear();
		screenVars.benfamt09.clear();
		screenVars.benfamt10.clear();
		screenVars.benfamt11.clear();
		screenVars.benfamt12.clear();
		screenVars.benfamt13.clear();
		screenVars.crtable.clear();
		screenVars.mortcls.clear();
		screenVars.claimamt01.clear();
		screenVars.claimamt02.clear();
		screenVars.claimamt03.clear();
		screenVars.claimamt04.clear();
		screenVars.claimamt05.clear();
		screenVars.claimamt06.clear();
		screenVars.claimamt07.clear();
		screenVars.claimamt08.clear();
		screenVars.claimamt09.clear();
		screenVars.claimamt10.clear();
		screenVars.claimamt11.clear();
		screenVars.claimamt12.clear();
		screenVars.claimamt13.clear();
		screenVars.claimtot.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.daclaim01.clear();
		screenVars.daclaim02.clear();
		screenVars.daclaim03.clear();
		screenVars.daclaim04.clear();
		screenVars.daclaim05.clear();
		screenVars.daclaim06.clear();
		screenVars.daclaim07.clear();
		screenVars.daclaim08.clear();
		screenVars.daclaim09.clear();
		screenVars.daclaim10.clear();
		screenVars.daclaim11.clear();
		screenVars.daclaim12.clear();
		screenVars.daclaim13.clear();
	}
}
