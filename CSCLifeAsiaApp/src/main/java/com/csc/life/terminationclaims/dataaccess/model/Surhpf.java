package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;

public class Surhpf {
	private String chdrcoy;
	private String chdrnum;
	private int planSuffix;
	private String life;
	private String jlife;
	private int tranno;
	private String termid;
	private int transactionDate;
	private int user;
	private int effdate;
	private String currcd;
	private BigDecimal policyloan;
	private BigDecimal tdbtamt;
	private BigDecimal taxamt;
	private BigDecimal otheradjst;
	private String reasoncd;
	private String resndesc;
	private String cnttype;
	private int transactionTime;
	private BigDecimal zrcshamt;
	private String userProfile;
	private String jobName;
	private String datime;
	private BigDecimal suspenseamt;
	private BigDecimal unexpiredPremium;
	
	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public int getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	public String getCurrcd() {
		return currcd;
	}

	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}

	public BigDecimal getPolicyloan() {
		return policyloan;
	}

	public void setPolicyloan(BigDecimal policyloan) {
		this.policyloan = policyloan;
	}

	public BigDecimal getTdbtamt() {
		return tdbtamt;
	}

	public void setTdbtamt(BigDecimal tdbtamt) {
		this.tdbtamt = tdbtamt;
	}

	public BigDecimal getTaxamt() {
		return taxamt;
	}

	public void setTaxamt(BigDecimal taxamt) {
		this.taxamt = taxamt;
	}

	public BigDecimal getOtheradjst() {
		return otheradjst;
	}

	public void setOtheradjst(BigDecimal otheradjst) {
		this.otheradjst = otheradjst;
	}

	public String getReasoncd() {
		return reasoncd;
	}

	public void setReasoncd(String reasoncd) {
		this.reasoncd = reasoncd;
	}

	public String getResndesc() {
		return resndesc;
	}

	public void setResndesc(String resndesc) {
		this.resndesc = resndesc;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public int getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}

	public BigDecimal getZrcshamt() {
		return zrcshamt;
	}

	public void setZrcshamt(BigDecimal zrcshamt) {
		this.zrcshamt = zrcshamt;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}
	public BigDecimal getSuspenseamt() {
		return suspenseamt;
	}

	public void setSuspenseamt(BigDecimal suspenseamt) {
		this.suspenseamt = suspenseamt;
	}

	public BigDecimal getUnexpiredPremium() {
		return unexpiredPremium;
	}

	public void setUnexpiredPremium(BigDecimal unexpiredPremium) {
		this.unexpiredPremium = unexpiredPremium;
	}

}
