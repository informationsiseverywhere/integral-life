package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6696screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6696ScreenVars sv = (S6696ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6696screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6696ScreenVars screenVars = (S6696ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.dfclmpct.setClassString("");
		screenVars.mxovrpct.setClassString("");
		screenVars.mnovrpct.setClassString("");
		screenVars.dfdefprd.setClassString("");
		screenVars.freqcy01.setClassString("");
		screenVars.mndefprd.setClassString("");
		screenVars.freqcy02.setClassString("");
		screenVars.revitrm.setClassString("");
		screenVars.freqcy03.setClassString("");
		screenVars.frqoride.setClassString("");
		screenVars.inxfrq.setClassString("");
		screenVars.inxsbm.setClassString("");
		screenVars.sacscode.setClassString("");
		screenVars.sacstype.setClassString("");
		screenVars.glact.setClassString("");
		screenVars.sign.setClassString("");
		screenVars.dissmeth.setClassString("");
	}

/**
 * Clear all the variables in S6696screen
 */
	public static void clear(VarModel pv) {
		S6696ScreenVars screenVars = (S6696ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.dfclmpct.clear();
		screenVars.mxovrpct.clear();
		screenVars.mnovrpct.clear();
		screenVars.dfdefprd.clear();
		screenVars.freqcy01.clear();
		screenVars.mndefprd.clear();
		screenVars.freqcy02.clear();
		screenVars.revitrm.clear();
		screenVars.freqcy03.clear();
		screenVars.frqoride.clear();
		screenVars.inxfrq.clear();
		screenVars.inxsbm.clear();
		screenVars.sacscode.clear();
		screenVars.sacstype.clear();
		screenVars.glact.clear();
		screenVars.sign.clear();
		screenVars.dissmeth.clear();
	}
}
