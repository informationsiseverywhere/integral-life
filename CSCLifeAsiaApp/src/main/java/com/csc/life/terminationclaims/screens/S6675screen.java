package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6675screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {18, 23, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6675ScreenVars sv = (S6675ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6675screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6675ScreenVars screenVars = (S6675ScreenVars)pv;
		screenVars.effdateDisp.setClassString("");
		screenVars.policyloan.setClassString("");
		screenVars.netOfSvDebt.setClassString("");
		screenVars.otheradjst.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.estimateTotalValue.setClassString("");
		screenVars.clamant.setClassString("");
		screenVars.letterPrintFlag.setClassString("");
		screenVars.zrcshamt.setClassString("");
		screenVars.tdbtamt.setClassString("");
		screenVars.taxamt.setClassString("");
	}

/**
 * Clear all the variables in S6675screen
 */
	public static void clear(VarModel pv) {
		S6675ScreenVars screenVars = (S6675ScreenVars) pv;
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.policyloan.clear();
		screenVars.netOfSvDebt.clear();
		screenVars.otheradjst.clear();
		screenVars.currcd.clear();
		screenVars.estimateTotalValue.clear();
		screenVars.clamant.clear();
		screenVars.letterPrintFlag.clear();
		screenVars.zrcshamt.clear();
		screenVars.tdbtamt.clear();
		screenVars.taxamt.clear();
	}
}
