package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:58
 * Description:
 * Copybook name: SVLTSURKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Svltsurkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData svltsurFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData svltsurKey = new FixedLengthStringData(64).isAPartOf(svltsurFileKey, 0, REDEFINE);
  	public FixedLengthStringData svltsurChdrcoy = new FixedLengthStringData(1).isAPartOf(svltsurKey, 0);
  	public FixedLengthStringData svltsurChdrnum = new FixedLengthStringData(8).isAPartOf(svltsurKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(svltsurKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(svltsurFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		svltsurFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}