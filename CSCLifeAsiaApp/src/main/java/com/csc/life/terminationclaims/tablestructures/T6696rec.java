package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:16
 * Description:
 * Copybook name: T6696REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6696rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6696Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData dfclmpct = new ZonedDecimalData(5, 2).isAPartOf(t6696Rec, 0);
  	public ZonedDecimalData dfdefprd = new ZonedDecimalData(3, 0).isAPartOf(t6696Rec, 5);
  	public FixedLengthStringData dissmeth = new FixedLengthStringData(4).isAPartOf(t6696Rec, 8);
  	public FixedLengthStringData freqcys = new FixedLengthStringData(6).isAPartOf(t6696Rec, 12);
  	public FixedLengthStringData[] freqcy = FLSArrayPartOfStructure(3, 2, freqcys, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(freqcys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freqcy01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData freqcy02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData freqcy03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData frqoride = new FixedLengthStringData(1).isAPartOf(t6696Rec, 18);
  	public FixedLengthStringData glact = new FixedLengthStringData(14).isAPartOf(t6696Rec, 19);
  	public FixedLengthStringData inxfrq = new FixedLengthStringData(2).isAPartOf(t6696Rec, 33);
  	public FixedLengthStringData inxsbm = new FixedLengthStringData(10).isAPartOf(t6696Rec, 35);
  	public ZonedDecimalData mndefprd = new ZonedDecimalData(3, 0).isAPartOf(t6696Rec, 45);
  	public ZonedDecimalData mnovrpct = new ZonedDecimalData(5, 2).isAPartOf(t6696Rec, 48);
  	public ZonedDecimalData mxovrpct = new ZonedDecimalData(5, 2).isAPartOf(t6696Rec, 53);
  	public ZonedDecimalData revitrm = new ZonedDecimalData(3, 0).isAPartOf(t6696Rec, 58);
  	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(t6696Rec, 61);
  	public FixedLengthStringData sacstype = new FixedLengthStringData(2).isAPartOf(t6696Rec, 63);
  	public FixedLengthStringData sign = new FixedLengthStringData(1).isAPartOf(t6696Rec, 65);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(434).isAPartOf(t6696Rec, 66, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6696Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6696Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}