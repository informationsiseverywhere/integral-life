package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:28
 * Description:
 * Copybook name: HCLALTLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hclaltlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hclaltlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hclaltlKey = new FixedLengthStringData(256).isAPartOf(hclaltlFileKey, 0, REDEFINE);
  	public FixedLengthStringData hclaltlChdrcoy = new FixedLengthStringData(1).isAPartOf(hclaltlKey, 0);
  	public FixedLengthStringData hclaltlChdrnum = new FixedLengthStringData(8).isAPartOf(hclaltlKey, 1);
  	public FixedLengthStringData hclaltlLife = new FixedLengthStringData(2).isAPartOf(hclaltlKey, 9);
  	public FixedLengthStringData hclaltlCoverage = new FixedLengthStringData(2).isAPartOf(hclaltlKey, 11);
  	public FixedLengthStringData hclaltlRider = new FixedLengthStringData(2).isAPartOf(hclaltlKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(hclaltlKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hclaltlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hclaltlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}