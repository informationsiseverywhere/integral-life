/******************************************************************************
 * File Name 		: Zlreinpf.java
 * Author			: rsubramani42
 * Creation Date	: 13 April 2018
 * Project			: Integral Life
 * Description		: The Model Class for ZLREINPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;

@Entity
@Table(name = "ZLREINPF", schema = "VM1DTA")
public class Zlreinpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	@Id 
	@Column(name = "UNIQUE_NUMBER") 
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String cnttype;
	private String ownnum;
	private String rdocnum;
	private Integer effdate;
	private BigDecimal origamt;
	private String rcpttype;
	private String fflag;
	private String erorcde;
	private String usrprf;
	private String jobnm;
	private Date datime;

	// Constructor
	public Zlreinpf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public String getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getCnttype(){
		return this.cnttype;
	}
	public String getOwnnum(){
		return this.ownnum;
	}
	public String getRdocnum(){
		return this.rdocnum;
	}
	public Integer getEffdate(){
		return this.effdate;
	}
	public BigDecimal getOrigamt(){
		return this.origamt;
	}
	public String getRcpttype(){
		return this.rcpttype;
	}
	public String getFflag(){
		return this.fflag;
	}
	public String getErorcde(){
		return this.erorcde;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return this.datime;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( String chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setCnttype( String cnttype ){
		 this.cnttype = cnttype;
	}
	public void setOwnnum( String ownnum ){
		 this.ownnum = ownnum;
	}
	public void setRdocnum( String rdocnum ){
		 this.rdocnum = rdocnum;
	}
	public void setEffdate( Integer effdate ){
		 this.effdate = effdate;
	}
	public void setOrigamt( BigDecimal origamt ){
		 this.origamt = origamt;
	}
	public void setRcpttype( String rcpttype ){
		 this.rcpttype = rcpttype;
	}
	public void setFflag( String fflag ){
		 this.fflag = fflag;
	}
	public void setErorcde( String erorcde ){
		 this.erorcde = erorcde;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		 this.datime = datime;
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("CNTTYPE:		");
		output.append(getCnttype());
		output.append("\r\n");
		output.append("OWNNUM:		");
		output.append(getOwnnum());
		output.append("\r\n");
		output.append("RDOCNUM:		");
		output.append(getRdocnum());
		output.append("\r\n");
		output.append("EFFDATE:		");
		output.append(getEffdate());
		output.append("\r\n");
		output.append("ORIGAMT:		");
		output.append(getOrigamt());
		output.append("\r\n");
		output.append("RCPTTYPE:		");
		output.append(getRcpttype());
		output.append("\r\n");
		output.append("FFLAG:		");
		output.append(getFflag());
		output.append("\r\n");
		output.append("ERORCDE:		");
		output.append(getErorcde());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
