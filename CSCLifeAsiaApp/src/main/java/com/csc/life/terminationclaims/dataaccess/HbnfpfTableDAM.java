package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HbnfpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:24
 * Class transformed from HBNFPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HbnfpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 177;
	public FixedLengthStringData hbnfrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hbnfpfRecord = hbnfrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData livesno = DD.livesno.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData waiverprem = DD.waiverprem.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum01 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum02 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum03 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum04 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum05 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum06 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum07 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum08 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum09 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData clntnum10 = DD.clntnum.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation01 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation02 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation03 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation04 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation05 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation06 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation07 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation08 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation09 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData relation10 = DD.relation.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hbnfrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(hbnfrec);
	public PackedDecimalData zunit = DD.zunit.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hbnfrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hbnfrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HbnfpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HbnfpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HbnfpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HbnfpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HbnfpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HbnfpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HbnfpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HBNFPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"MORTCLS, " +
							"LIVESNO, " +
							"WAIVERPREM, " +
							"CLNTNUM01, " +
							"CLNTNUM02, " +
							"CLNTNUM03, " +
							"CLNTNUM04, " +
							"CLNTNUM05, " +
							"CLNTNUM06, " +
							"CLNTNUM07, " +
							"CLNTNUM08, " +
							"CLNTNUM09, " +
							"CLNTNUM10, " +
							"RELATION01, " +
							"RELATION02, " +
							"RELATION03, " +
							"RELATION04, " +
							"RELATION05, " +
							"RELATION06, " +
							"RELATION07, " +
							"RELATION08, " +
							"RELATION09, " +
							"RELATION10, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"EFFDATE, " +
							"CRTABLE, " +
							"BENPLN, " +
							"ZUNIT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     mortcls,
                                     livesno,
                                     waiverprem,
                                     clntnum01,
                                     clntnum02,
                                     clntnum03,
                                     clntnum04,
                                     clntnum05,
                                     clntnum06,
                                     clntnum07,
                                     clntnum08,
                                     clntnum09,
                                     clntnum10,
                                     relation01,
                                     relation02,
                                     relation03,
                                     relation04,
                                     relation05,
                                     relation06,
                                     relation07,
                                     relation08,
                                     relation09,
                                     relation10,
                                     life,
                                     coverage,
                                     rider,
                                     effdate,
                                     crtable,
                                     benpln,
                                     zunit,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		mortcls.clear();
  		livesno.clear();
  		waiverprem.clear();
  		clntnum01.clear();
  		clntnum02.clear();
  		clntnum03.clear();
  		clntnum04.clear();
  		clntnum05.clear();
  		clntnum06.clear();
  		clntnum07.clear();
  		clntnum08.clear();
  		clntnum09.clear();
  		clntnum10.clear();
  		relation01.clear();
  		relation02.clear();
  		relation03.clear();
  		relation04.clear();
  		relation05.clear();
  		relation06.clear();
  		relation07.clear();
  		relation08.clear();
  		relation09.clear();
  		relation10.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		effdate.clear();
  		crtable.clear();
  		benpln.clear();
  		zunit.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHbnfrec() {
  		return hbnfrec;
	}

	public FixedLengthStringData getHbnfpfRecord() {
  		return hbnfpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHbnfrec(what);
	}

	public void setHbnfrec(Object what) {
  		this.hbnfrec.set(what);
	}

	public void setHbnfpfRecord(Object what) {
  		this.hbnfpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hbnfrec.getLength());
		result.set(hbnfrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}