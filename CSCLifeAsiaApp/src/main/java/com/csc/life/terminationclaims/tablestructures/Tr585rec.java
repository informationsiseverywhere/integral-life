package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:08
 * Description:
 * Copybook name: TR585REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr585rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr585Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData acdbens = new FixedLengthStringData(75).isAPartOf(tr585Rec, 0);
  	public FixedLengthStringData[] acdben = FLSArrayPartOfStructure(15, 5, acdbens, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(75).isAPartOf(acdbens, 0, FILLER_REDEFINE);
  	public FixedLengthStringData acdben01 = new FixedLengthStringData(5).isAPartOf(filler, 0);
  	public FixedLengthStringData acdben02 = new FixedLengthStringData(5).isAPartOf(filler, 5);
  	public FixedLengthStringData acdben03 = new FixedLengthStringData(5).isAPartOf(filler, 10);
  	public FixedLengthStringData acdben04 = new FixedLengthStringData(5).isAPartOf(filler, 15);
  	public FixedLengthStringData acdben05 = new FixedLengthStringData(5).isAPartOf(filler, 20);
  	public FixedLengthStringData acdben06 = new FixedLengthStringData(5).isAPartOf(filler, 25);
  	public FixedLengthStringData acdben07 = new FixedLengthStringData(5).isAPartOf(filler, 30);
  	public FixedLengthStringData acdben08 = new FixedLengthStringData(5).isAPartOf(filler, 35);
  	public FixedLengthStringData acdben09 = new FixedLengthStringData(5).isAPartOf(filler, 40);
  	public FixedLengthStringData acdben10 = new FixedLengthStringData(5).isAPartOf(filler, 45);
  	public FixedLengthStringData acdben11 = new FixedLengthStringData(5).isAPartOf(filler, 50);
  	public FixedLengthStringData acdben12 = new FixedLengthStringData(5).isAPartOf(filler, 55);
  	public FixedLengthStringData acdben13 = new FixedLengthStringData(5).isAPartOf(filler, 60);
  	public FixedLengthStringData acdben14 = new FixedLengthStringData(5).isAPartOf(filler, 65);
  	public FixedLengthStringData acdben15 = new FixedLengthStringData(5).isAPartOf(filler, 70);
  	public FixedLengthStringData amtflds = new FixedLengthStringData(180).isAPartOf(tr585Rec, 75);
  	public ZonedDecimalData[] amtfld = ZDArrayPartOfStructure(15, 12, 2, amtflds, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(180).isAPartOf(amtflds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData amtfld01 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData amtfld02 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 12);
  	public ZonedDecimalData amtfld03 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 24);
  	public ZonedDecimalData amtfld04 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 36);
  	public ZonedDecimalData amtfld05 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 48);
  	public ZonedDecimalData amtfld06 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 60);
  	public ZonedDecimalData amtfld07 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 72);
  	public ZonedDecimalData amtfld08 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 84);
  	public ZonedDecimalData amtfld09 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 96);
  	public ZonedDecimalData amtfld10 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 108);
  	public ZonedDecimalData amtfld11 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 120);
  	public ZonedDecimalData amtfld12 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 132);
  	public ZonedDecimalData amtfld13 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 144);
  	public ZonedDecimalData amtfld14 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 156);
  	public ZonedDecimalData amtfld15 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 168);
  	public FixedLengthStringData benfreqs = new FixedLengthStringData(30).isAPartOf(tr585Rec, 255);
  	public FixedLengthStringData[] benfreq = FLSArrayPartOfStructure(15, 2, benfreqs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(30).isAPartOf(benfreqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData benfreq01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData benfreq02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData benfreq03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData benfreq04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData benfreq05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData benfreq06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData benfreq07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData benfreq08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData benfreq09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData benfreq10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData benfreq11 = new FixedLengthStringData(2).isAPartOf(filler2, 20);
  	public FixedLengthStringData benfreq12 = new FixedLengthStringData(2).isAPartOf(filler2, 22);
  	public FixedLengthStringData benfreq13 = new FixedLengthStringData(2).isAPartOf(filler2, 24);
  	public FixedLengthStringData benfreq14 = new FixedLengthStringData(2).isAPartOf(filler2, 26);
  	public FixedLengthStringData benfreq15 = new FixedLengthStringData(2).isAPartOf(filler2, 28);
  	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(tr585Rec, 285);
  	public FixedLengthStringData dfclmpcts = new FixedLengthStringData(75).isAPartOf(tr585Rec, 293);
  	public ZonedDecimalData[] dfclmpct = ZDArrayPartOfStructure(15, 5, 2, dfclmpcts, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(75).isAPartOf(dfclmpcts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData dfclmpct01 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData dfclmpct02 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 5);
  	public ZonedDecimalData dfclmpct03 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 10);
  	public ZonedDecimalData dfclmpct04 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 15);
  	public ZonedDecimalData dfclmpct05 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 20);
  	public ZonedDecimalData dfclmpct06 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 25);
  	public ZonedDecimalData dfclmpct07 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 30);
  	public ZonedDecimalData dfclmpct08 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 35);
  	public ZonedDecimalData dfclmpct09 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 40);
  	public ZonedDecimalData dfclmpct10 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 45);
  	public ZonedDecimalData dfclmpct11 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 50);
  	public ZonedDecimalData dfclmpct12 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 55);
  	public ZonedDecimalData dfclmpct13 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 60);
  	public ZonedDecimalData dfclmpct14 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 65);
  	public ZonedDecimalData dfclmpct15 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 70);
  	public FixedLengthStringData gcdblinds = new FixedLengthStringData(15).isAPartOf(tr585Rec, 368);
  	public FixedLengthStringData[] gcdblind = FLSArrayPartOfStructure(15, 1, gcdblinds, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(15).isAPartOf(gcdblinds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData gcdblind01 = new FixedLengthStringData(1).isAPartOf(filler4, 0);
  	public FixedLengthStringData gcdblind02 = new FixedLengthStringData(1).isAPartOf(filler4, 1);
  	public FixedLengthStringData gcdblind03 = new FixedLengthStringData(1).isAPartOf(filler4, 2);
  	public FixedLengthStringData gcdblind04 = new FixedLengthStringData(1).isAPartOf(filler4, 3);
  	public FixedLengthStringData gcdblind05 = new FixedLengthStringData(1).isAPartOf(filler4, 4);
  	public FixedLengthStringData gcdblind06 = new FixedLengthStringData(1).isAPartOf(filler4, 5);
  	public FixedLengthStringData gcdblind07 = new FixedLengthStringData(1).isAPartOf(filler4, 6);
  	public FixedLengthStringData gcdblind08 = new FixedLengthStringData(1).isAPartOf(filler4, 7);
  	public FixedLengthStringData gcdblind09 = new FixedLengthStringData(1).isAPartOf(filler4, 8);
  	public FixedLengthStringData gcdblind10 = new FixedLengthStringData(1).isAPartOf(filler4, 9);
  	public FixedLengthStringData gcdblind11 = new FixedLengthStringData(1).isAPartOf(filler4, 10);
  	public FixedLengthStringData gcdblind12 = new FixedLengthStringData(1).isAPartOf(filler4, 11);
  	public FixedLengthStringData gcdblind13 = new FixedLengthStringData(1).isAPartOf(filler4, 12);
  	public FixedLengthStringData gcdblind14 = new FixedLengthStringData(1).isAPartOf(filler4, 13);
  	public FixedLengthStringData gcdblind15 = new FixedLengthStringData(1).isAPartOf(filler4, 14);
  	public FixedLengthStringData mxbenunts = new FixedLengthStringData(45).isAPartOf(tr585Rec, 383);
  	public ZonedDecimalData[] mxbenunt = ZDArrayPartOfStructure(15, 3, 0, mxbenunts, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(45).isAPartOf(mxbenunts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData mxbenunt01 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 0);
  	public ZonedDecimalData mxbenunt02 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 3);
  	public ZonedDecimalData mxbenunt03 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 6);
  	public ZonedDecimalData mxbenunt04 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 9);
  	public ZonedDecimalData mxbenunt05 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 12);
  	public ZonedDecimalData mxbenunt06 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 15);
  	public ZonedDecimalData mxbenunt07 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 18);
  	public ZonedDecimalData mxbenunt08 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 21);
  	public ZonedDecimalData mxbenunt09 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 24);
  	public ZonedDecimalData mxbenunt10 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 27);
  	public ZonedDecimalData mxbenunt11 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 30);
  	public ZonedDecimalData mxbenunt12 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 33);
  	public ZonedDecimalData mxbenunt13 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 36);
  	public ZonedDecimalData mxbenunt14 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 39);
  	public ZonedDecimalData mxbenunt15 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 42);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(72).isAPartOf(tr585Rec, 428, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr585Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr585Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}