/*
 * File: B5369.java
 * Date: 29 August 2009 21:15:04
 * Author: Quipoz Limited
 *
 * Class transformed from B5369.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.terminationclaims.dataaccess.RegxpfTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.RegrDAO;
import com.csc.life.terminationclaims.dataaccess.dao.RegxpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.life.terminationclaims.dataaccess.model.Regxpf;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6762rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*           REGULAR PAYMENTS TERMINATING
*           ----------------------------
* Overview
* ________
*
* This program is part of the new 'Multi-Threading Batch
* Performance' suite. It runs directly after B5368, which 'splits'
* the REGPPF according to the number of billing programs to run.
* All references to the REGP are via REGXPF - a temporary file
* holding all the REGP records for this program to process.
*
* B5369 will process all of the Regular Payments that are
* due  to terminate. When the Final Payment Date is equal
* to or greater than the Effective Date of the run  then
* the  status of the Regular Payment record will be altered to
* reflect this.
*
* The changing of the status  codes  will  be  driven  by  the
* table  T6693  -  Allowable  Transactions For Status. This is
* accessed by the current Payment Status  and  Component  Code
* and  gives  a  list  of  all  transactions  that are allowed
* against Regular  Payments  for  that  Status  and  Component
* Code.  Against  each  allowed  transaction  code is the Next
* Status that the REGP record must be set to after  successful
* completion  of  the  transaction.  Therefore  the table will
* prevent this program processing REGP records  that  are  not
* in  the correct state and the updating of the status code is
* kept free of hard coding.
*
* The Transaction Code  of  this  program  will  be  found  in
* BPRD-AUTH-CODE.
*
* Processing.
* -----------
*
* Use the physical file REGPPF and select those  records
* whose  Final  Payment  Date  is  less than or equal to, (Not
* Greater Than), the Effective Date  of  the  run,  and  whose
* company  matches  the  sign on company. Sequence the records
* as:
*
*       1. Regular Payment Type
*       2. Contract Number
*       3. Component Code, (CRTABLE).
*       4. Final Payment Date   (Descending)
*
* COBOL Program.
* --------------
*
* 1. Contract Locking.
* --------------------
*
* When the first Regular Payment record  is  found  for  which
* processing  might  be  applicable  attempt  to  softlock the
* contract.
*
* Notes.
* ------
*
* Maintain the following Control Totals:
*
*       .    Number of payments extracted
*       .    Number of payments terminated
*       .    Number of rejections
*       .    Number of locked contracts
*       .    Payments with ineligible status
*       .    Contracts with ineligible status
*       .    Components with ineligible status
*
* Tables Used.
* ------------
*
* . T5679 - Transaction Codes For Status
*           Key: Transaction Code.
*
* . T6693 - Allowable Actions for Status
*           Key: Payment Status || CRTABLE
*           CRTABLE may be set to '****'.
*
* . T6762 - Regular payments Exception codes
*           Key: Program name
*
* 1000-INITIALISE SECTION
* _______________________
*
* Frequently-referenced tables are stored in working storage
* arrays to minimize disk IO. These tables include
* T6693 (Allowable Actions for Status).
*
* Issue an override to read the correct REGXPF for this run.
* The CRTTMPF process has already created a file specific to
* the run using the  REGXPF fields and is identified  by
* concatenating the following:-
*
*     'REGX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg REGX2B0001,  for the first run
*         REGX2B0002,  for the second etc.
*
* The number of threads would have been created by the
* CRTTMPF process given the parameters of the process
* definition of the CRTTMPF.
* To get the correct member of the above physical for B5369
* to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
* 2000-READ SECTION
* _________________
*
* Read the REGX records sequentially incrementing the control
* total.
*
* If end of file move ENDP to WSSP-EDTERROR.
*
* 2500-EDIT SECTION
* _________________
*
* Move OK to WSSP-EDTERROR.
*
* When a record is found that can be  locked  check  that  the
* Regular  Payment  is  in  a  fit  state  to  be processed by
* reading T6693 and matching  the  current  transaction  code,
* (obtained from BPRD-AUTH-CODE) against those on the extra data
* screen. If the transaction code cannot  be  found  then  the
* Regular  Payment  may  not be processed so go on to the next
* record.
*
* If it may be processed read the  associated  CHDR  and  COVR
* records.  For  the  read  of  COVR  use the Plan Suffix from
* REGP. If this is zero then perform a BEGN on COVR  otherwise
* use READR for a direct read.
*
* Check  that  the contract and the component are both able to
* be processed by  checking  their  Premium  Status  and  Risk
* Status codes against those on T5679 for the transaction.
* If the statii are invalid add 1 to control total 6 or 7
* and move SPACES to WSSP-EDTERROR.
*
* Move SPACES to WSSP-EDTERROR to prevent 3000-update
* processing.
*
* 'Soft lock' the contract, if it is to be processed.
* If the contract is already 'locked' increment control
* total number 4 and  move SPACES to WSSP-EDTERROR.
*
* 3000-UPDATE SECTION
* ___________________
*
* Read CHDR, PAYR and REGP with READH. Set the Validflag on CHDR
* to '2' and perform a REWRT. Increment the TRANNO by 1, set the
* Validflag back to '1' and write the new CHDR record.
* Set the Validflag on PAYR to '2' and perform a REWRT.
* Set the TRANNO the CHDR-TRANNO from above, set the validflag
* back to '1' and WRITR the new PAYR.
*
* Write a PTRN record with the new incremented TRANNO.
*
* Set the Validflag on REGP to '2' and perform a REWRT.
*
* Set the Validflag back to '1' and place the  new  TRANNO  on
* REGP.  Replace  the Regular Payment Status Code on REGP with
* the corresponding Status Code from T6693.
*
* Perform a WRITR on REGP.
*
* Read the next REGP record.
*
* 4000-CLOSE SECTION
* __________________
*
* Remove the softlock on the contract.
*
* Close Files.
*
* Delete the Override function for the REGXPF file
*
* Error Processing:
*
*   Perform the 600-FATAL-ERROR section. The
*   SYSR-SYSERR-TYPE flag does not need to be set in this
*   program, because MAINB takes care of a system errors.
*
*         (BATD processing is handled in MAINB)
*                                                                     *
*                                                                     *
***********************************************************************
* </pre>
*/
public class B5369 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private RegxpfTableDAM regxpf = new RegxpfTableDAM();
//	private RegxpfTableDAM regxpfRec = new RegxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5369");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
		/*  These fields are required by MAINB processing and should not
		  be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/*  REGX parameters.*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaRegxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaRegxFn, 0, FILLER).init("REGX");
	private FixedLengthStringData wsaaRegxRunid = new FixedLengthStringData(2).isAPartOf(wsaaRegxFn, 4);
	private ZonedDecimalData wsaaRegxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaRegxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
		/*  Arrays to store Regularly-referenced data, and reduce
		  the amount of accesses to the Itempf.
		  Array sizes are also delared  so that an increase of table
		  size does not impact its processing.*/
   //ILIFE-1194 STARTS
	//private static final int wsaaT6693Size = 80; 
	//private static final int wsaaT6693Size = 200;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6693Size = 1000;

		/* WSAA-T6693-ARRAY */
	//private FixedLengthStringData[] wsaaT6693Rec = FLSInittedArray (100, 88);	
	private FixedLengthStringData[] wsaaT6693Rec = FLSInittedArray (1000, 88);
	//ILIFE-1194 ENDS
	private FixedLengthStringData[] wsaaT6693Key = FLSDArrayPartOfArrayStructure(6, wsaaT6693Rec, 0);
	private FixedLengthStringData[] wsaaT6693Paystat = FLSDArrayPartOfArrayStructure(2, wsaaT6693Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT6693Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT6693Key, 2, HIVALUE);
	private FixedLengthStringData[] wsaaT6693DateData = FLSDArrayPartOfArrayStructure(10, wsaaT6693Rec, 6);
	private PackedDecimalData[] wsaaT6693Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6693DateData, 0);
	private PackedDecimalData[] wsaaT6693Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT6693DateData, 5);
	private FixedLengthStringData[][] wsaaT6693Data = FLSDArrayPartOfArrayStructure(12, 6, wsaaT6693Rec, 16);
	private FixedLengthStringData[][] wsaaT6693Rgpystat = FLSDArrayPartOfArrayStructure(2, wsaaT6693Data, 0);
	private FixedLengthStringData[][] wsaaT6693Trcode = FLSDArrayPartOfArrayStructure(4, wsaaT6693Data, 2);

	private FixedLengthStringData wsaaT6693Key1 = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat1 = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 0).init(HIVALUE);
	private FixedLengthStringData wsaaT6693Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key1, 2).init(HIVALUE);
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6693 = "T6693";
	private static final String t6762 = "T6762";
	private static final String tr517 = "TR517";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaItemFound = new FixedLengthStringData(1).init("N");
	private Validator itemFound = new Validator(wsaaItemFound, "Y");

	private FixedLengthStringData wsaaValidTrcode = new FixedLengthStringData(1).init("N");
	private Validator validTrcode = new Validator(wsaaValidTrcode, "Y");

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysPayrkey = new FixedLengthStringData(20).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysPayrkey, 0);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(77).isAPartOf(wsysSystemErrorParams, 20);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0);
		/* WSAA-MISCELLANEOUS */
	private String wsaaValidStatus = "";
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrnumStore = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaNextPaystat = new FixedLengthStringData(2);
	private PackedDecimalData wsaaSinstamt05 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPayrSinstamt05 = new PackedDecimalData(13, 2);
	private String wsaaIsWop = "N";
	private PackedDecimalData wsaaWopInstprem = new PackedDecimalData(17, 2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT6693Ix = new IntegerData();
//	private ChdrpayTableDAM chdrpayIO = new ChdrpayTableDAM();
//	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
//	private CovrTableDAM covrIO = new CovrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
//	private PayrTableDAM payrIO = new PayrTableDAM();
//	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
//	private RegpTableDAM regpIO = new RegpTableDAM();
//	private RegrTableDAM regrIO = new RegrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5679rec t5679rec = new T5679rec();
	private T6693rec t6693rec = new T6693rec();
	private T6762rec t6762rec = new T6762rec();
	private Tr517rec tr517rec = new Tr517rec();
	private FormatsInner formatsInner = new FormatsInner();

	private Regxpf regxpfRec = new Regxpf();
	private Payrpf payrIO = new Payrpf();
	private Regppf regpIO = new Regppf();
	private Regrpf regrIO = new Regrpf();
	private Covrpf covrIO = new Covrpf();
	private Chdrpf chdrpayIO = new Chdrpf();
	private Chdrpf chdrrgpIO = new Chdrpf();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private RegxpfDAO regxpfDAO = getApplicationContext().getBean("regxpfDAO", RegxpfDAO.class);
	private RegrDAO regrpfDAO = getApplicationContext().getBean("RegrDAO", RegrDAO.class);
	private RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    
	private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<>();
	private Map<String, List<Covrpf>> covrpfMap = new HashMap<>();
	private Map<String, List<Regppf>> regppfMap = new HashMap<>();
	private Map<String, List<Payrpf>> payrpfMap = new HashMap<>();
	
	private List<Regrpf> regrpfListInst = new ArrayList<>();
	private List<Chdrpf> chdrpfListUpdt = new ArrayList<>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<>();
	private List<Payrpf> payrpfListUpdt = new ArrayList<>();
	private List<Payrpf> payrpfListInst = new ArrayList<>();
	private List<Regppf> regppfListUpdt = new ArrayList<>();
	private List<Regppf> regppfListInst = new ArrayList<>();
	private List<Ptrnpf> ptrnpfList = new ArrayList<>();
	private List<Covrpf> covrpfListUpdt = new ArrayList<>();
	private List<Covrpf> covrpfListInst = new ArrayList<>();
	
	private Iterator<Regxpf> iter;
    private int batchID, batchExtractSize;
	private int ct01Val, ct02Val, ct05Val, ct06Val, ct07Val, ct04Val;

	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		h910CallCovrio,
		h990Exit
	}

	public B5369() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		/*  Point to correct member of REGXPF.*/
		wsaaRegxRunid.set(bprdIO.getSystemParam04());
		wsaaRegxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
//		StringUtil stringVariable1 = new StringUtil();
//		stringVariable1.addExpression("OVRDBF FILE(REGXPF) TOFILE(");
//		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
//		stringVariable1.addExpression("/");
//		stringVariable1.addExpression(wsaaRegxFn);
//		stringVariable1.addExpression(") ");
//		stringVariable1.addExpression("MBR(");
//		stringVariable1.addExpression(wsaaThreadMember);
//		stringVariable1.addExpression(")");
//		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
//		stringVariable1.setStringInto(wsaaQcmdexc);
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
//		//ILIFE-1194 STARTS
//		//regxpf.openInput();
//		regxpf.openInputForSort();
        //ILIFE-1194 ENDS
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
        readChunkRecord();
		if(wsspEdterror.equals(varcom.endp)){
			return;
		}
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		this.loadTables();
	}

	private void readChunkRecord() {
		List<Regxpf> regxpfList = regxpfDAO.findResult(wsaaRegxFn.toString(), wsaaThreadMember.toString(), batchExtractSize, batchID);
		if(regxpfList.isEmpty()){
			wsspEdterror.set(varcom.endp);
			return;
		}
		this.iter= regxpfList.iterator();
		List<String> chdrnumList = new ArrayList<>();
		for (Regxpf p : regxpfList) {
			chdrnumList.add(p.getChdrnum());
		}
		String coy = bsprIO.getCompany().toString();
		regppfMap = regppfDAO.searchRecords(chdrnumList);
		chdrpfMap = chdrpfDAO.searchChdrpf(coy, chdrnumList);
		covrpfMap = covrpfDAO.searchCovrMap(coy, chdrnumList);
		payrpfMap = payrpfDAO.getPayrLifMap(coy, chdrnumList);
	}

	private void loadTables(){
//		itemIO.setStatuz(varcom.oK);
//		itemIO.setItempfx("IT");
//		itemIO.setItemcoy(bsprIO.getCompany());
//		itemIO.setItemtabl(t5679);
//		itemIO.setItemitem(bprdIO.getAuthCode());
//		itemIO.setFunction(varcom.readr);
//		SmartFileCode.execute(appVars, itemIO);
//		if (isNE(itemIO.getStatuz(),varcom.oK)
//		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
//			syserrrec.params.set(itemIO.getParams());
//			syserrrec.statuz.set(itemIO.getStatuz());
//			fatalError600();
//		}
//		t5679rec.t5679Rec.set(itemIO.getGenarea());
	
		String coy = bsprIO.getCompany().toString();
		
		List<Itempf> items = this.itemDAO.getAllItemitem("IT", coy, t5679, bprdIO.getAuthCode().toString());
		if(items.isEmpty()) {
			syserrrec.params.set(t5679);
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		
//		itemIO.setItempfx("IT");
//		itemIO.setItemcoy(bsprIO.getCompany());
//		itemIO.setItemtabl(t6762);
//		itemIO.setItemitem(bprdIO.getBatchProgram());
//		itemIO.setFunction(varcom.readr);
//		SmartFileCode.execute(appVars, itemIO);
//		if (isNE(itemIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(itemIO.getParams());
//			syserrrec.statuz.set(itemIO.getStatuz());
//			fatalError600();
//		}
//		t6762rec.t6762Rec.set(itemIO.getGenarea());
		items = this.itemDAO.getAllItemitem("IT", coy, t6762, bprdIO.getBatchProgram().toString());
		if(items.isEmpty()) {
			syserrrec.params.set(t6762);
			fatalError600();
		}
		t6762rec.t6762Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		
//		itdmIO.setStatuz(varcom.oK);
//		itdmIO.setParams(SPACES);
//		itdmIO.setItemcoy(bsprIO.getCompany());
//		itdmIO.setItemtabl(t6693);
//		itdmIO.setItemitem(SPACES);
//		itdmIO.setItmfrm(varcom.vrcmMaxDate);
//		itdmIO.setFormat(itemrec);
//		itdmIO.setFunction(varcom.begn);
//		wsaaT6693Ix.set(1);
//		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23
//			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			//itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
			//ILife3168 Paymentreview report --nnaveenkumar
//			itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL");
//			loadT66931500();
//		}
		wsaaT6693Ix.set(1);
		Map<String,List<Itempf>> itemMap = itemDAO.loadSmartTable("IT", coy, t6693);
		for(String itemkey:itemMap.keySet()){
			for(Itempf i:itemMap.get(itemkey)){
				if(i.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata())<=0){
					loadT66931500(i);
				}
			}
		}
	}	
	
	
protected void loadT66931500(Itempf itdmIO)
	{
//	SmartFileCode.execute(appVars, itdmIO);
//	if (isNE(itdmIO.getStatuz(),varcom.oK)
//	&& isNE(itdmIO.getStatuz(),varcom.endp)) {
//		syserrrec.params.set(itdmIO.getParams());
//		syserrrec.statuz.set(itdmIO.getStatuz());
//		fatalError600();
//	}
//	if (isEQ(itdmIO.getStatuz(),varcom.endp)
//	|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
//	|| isNE(itdmIO.getItemtabl(),t6693)) {
//		itdmIO.setStatuz(varcom.endp);
//		return ;
//	}
	if (isGT(wsaaT6693Ix,wsaaT6693Size)) {
		syserrrec.statuz.set(h791);
		syserrrec.params.set(t6693);
		fatalError600();
	}
	if (isLT(bsscIO.getEffectiveDate(), itdmIO.getItmfrm())
			|| isGT(bsscIO.getEffectiveDate(), itdmIO.getItmto())) {
		//itdmIO.setFunction(varcom.nextr);
		return;
	}
	t6693rec.t6693Rec.set(StringUtil.rawToString(itdmIO.getGenarea()));
	wsaaT6693Key[wsaaT6693Ix.toInt()].set(itdmIO.getItemitem());
	wsaaT6693Itmfrm[wsaaT6693Ix.toInt()].set(itdmIO.getItmfrm());
	wsaaT6693Itmto[wsaaT6693Ix.toInt()].set(itdmIO.getItmto());
	/* 3 Dimensional Array to load table data that occurs more than*/
	/* once.*/
	for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)); wsaaSub1.add(1)){
		wsaaT6693Rgpystat[wsaaT6693Ix.toInt()][wsaaSub1.toInt()].set(t6693rec.rgpystat[wsaaSub1.toInt()]);
		wsaaT6693Trcode[wsaaT6693Ix.toInt()][wsaaSub1.toInt()].set(t6693rec.trcode[wsaaSub1.toInt()]);
	}
	//itdmIO.setFunction(varcom.nextr);
	/* SET WSAA-T6693-IX-MAX        TO WSAA-T6693-IX.*/
	wsaaT6693Ix.add(1);	
	}


protected void readFile2000()
	{
		/*READ-FILE*/
//	regxpf.read(regxpfRec);
//		//MIBT-102
//		if (regxpf.isAtEnd()|| isEQ(regxpfRec.getfinalPaydate, ZERO)) {
//			wsspEdterror.set(varcom.endp);
//			return ;
//		}
		if(!iter.hasNext()){
			batchID++;
			readChunkRecord();
			if(wsspEdterror.equals(varcom.endp)){
				return;
			}
		}
		regxpfRec = iter.next();
		if (regxpfRec.getFinalPaydate() == 0) {
			wsspEdterror.set(varcom.endp);
			return;
		}
	//	contotrec.totno.set(ct01);
	//	contotrec.totval.set(1);
	//	callContot001();
		this.ct01Val++;
		readRegp2100();
		wsysChdrnum.set(regxpfRec.getChdrnum());
	}

protected void readRegp2100()
	{
		regpRead2110();
	}

protected void regpRead2110()
	{
		/*  Read and hold Regular Payment record.*/
//		regpIO.setDataArea(SPACES);
//		regpIO.setChdrcoy(regxpfRec.getchdrcoy);
//		regpIO.setChdrnum(regxpfRec.getchdrnum);
//		regpIO.setLife(regxpfRec.getlife);
//		regpIO.setCoverage(regxpfRec.getcoverage);
//		regpIO.setRider(regxpfRec.getrider);
//		regpIO.setRgpynum(regxpfRec.getrgpynum);
//		regpIO.setFunction(varcom.readh);
//		regpIO.setFormat("REGPREC");
//		SmartFileCode.execute(appVars, regpIO);
//		if (isNE(regpIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(regpIO.getParams());
//			syserrrec.statuz.set(regpIO.getStatuz());
//			fatalError600();
//		}
	
		regpIO = null;
		
		if (regppfMap != null && regppfMap.containsKey(regxpfRec.getChdrnum())) {
			for (Regppf pf : regppfMap.get(regxpfRec.getChdrnum())) {
				if (pf.getChdrcoy().equals(regxpfRec.getChdrcoy())
						&& pf.getLife().equals(regxpfRec.getLife())
						&& pf.getCoverage().equals(regxpfRec.getCoverage())
						&& pf.getRider().equals(regxpfRec.getRider())
						&& pf.getRgpynum() == regxpfRec.getRgpynum()
						&& "1".equals(pf.getValidflag())) {
					regpIO = pf;
					break;
				}
			}
		}
		
		if (regpIO == null) {
			syserrrec.params.set(regxpfRec.getChdrnum());
			fatalError600();
		}
	}

protected void edit2500()
	{
			read2510();
		}

protected void read2510()
	{
		/* Read the contract header and validate the status against those*/
		/* on T5679. If the status is invalid, add 1 to CT06, which is a*/
		/* log of the Contract Header Records which have invalid statii.*/
		wsspEdterror.set(varcom.oK);
		checkT66932520();
		if (isEQ(wsspEdterror,SPACES)) {
			return ;
		}
		readChdrCovr2530();
		if (isEQ(wsspEdterror,SPACES)) {
			return ;
		}
		softlock2590();
		/*  Log CHDRs locked*/
		if (isEQ(sftlockrec.statuz,"LOCK")) {
//			contotrec.totno.set(ct04);
//			contotrec.totval.set(1);
//			callContot001();
			ct04Val++;
			wsspEdterror.set(SPACES);
			regrIO.setExcode(t6762rec.regpayExcpCsfl.toString());
			regrIO.setExreport("Y");
			reportProcess2600();
		}
	}

protected void checkT66932520()
	{
		start2525();
	}

protected void start2525()
	{
		/*  Read T6693 and verify that the record can be processed*/
		wsaaT6693Paystat1.set(regxpfRec.getRgpystat());
		wsaaT6693Crtable1.set(regxpfRec.getCrtable());
		wsaaT6693Ix.set(1);
		ArraySearch as1 = ArraySearch.getInstance(wsaaT6693Rec);
		as1.setIndices(wsaaT6693Ix);
		as1.addSearchKey(wsaaT6693Key, wsaaT6693Key1, true);
		if (as1.binarySearch()) {
			itemFound.setTrue();
			/*CONTINUE_STMT*/
		}
		else {
			wsaaItemFound.set("N");
		}
		if (isEQ(wsaaItemFound,"N")) {
			wsaaT6693Paystat1.set(regxpfRec.getRgpystat());
			wsaaT6693Crtable1.set("****");
			wsaaT6693Ix.set(1);
			 searchlabel1:
			{
				for (; isLT(wsaaT6693Ix,wsaaT6693Rec.length); wsaaT6693Ix.add(1)){
					if (isEQ(wsaaT6693Key[wsaaT6693Ix.toInt()],wsaaT6693Key1)) {
						itemFound.setTrue();
						break searchlabel1;
					}
				}
			}
		}
		/*  Read through array of allowable transactions to determine if*/
		/*  transaction is valid. If valid, store the next corresponding*/
		/*  payment status code.*/
		if (itemFound.isTrue()) {
			wsaaValidTrcode.set("N");
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)); wsaaSub1.add(1)){
				if (isEQ(bprdIO.getAuthCode(),wsaaT6693Trcode[wsaaT6693Ix.toInt()][wsaaSub1.toInt()])) {
					wsaaNextPaystat.set(wsaaT6693Rgpystat[wsaaT6693Ix.toInt()][wsaaSub1.toInt()]);
					validTrcode.setTrue();
				}
			}
		}
		if (isEQ(wsaaValidTrcode,"N")) {
			wsspEdterror.set(SPACES);
//			contotrec.totno.set(ct05);
//			contotrec.totval.set(1);
//			callContot001();
			this.ct05Val++;
			regrIO.setExcode(t6762rec.regpayExcpIpst.toString());
			regrIO.setExreport("Y");
			reportProcess2600();
		}
	}

protected void readChdrCovr2530()
	{
			readr2535();
		}

protected void readr2535()
	{
		/*  Read in the relevant contract details and validate the status*/
		/*  against T5679. If status is invalid, add 1 to CT06 (the number*/
		/*  of Contract Header Records with invalid statii).*/
//		chdrrgpIO.setChdrpfx("CH");
//		chdrrgpIO.setChdrcoy(bsprIO.getCompany());
//		chdrrgpIO.setChdrnum(regxpfRec.getchdrnum);
//		chdrrgpIO.setFunction(varcom.readr);
//		chdrrgpIO.setFormat(formatsInner.chdrrgprec);
//		SmartFileCode.execute(appVars, chdrrgpIO);
//		if ((isNE(chdrrgpIO.getStatuz(),varcom.oK))) {
//			syserrrec.params.set(chdrrgpIO.getParams());
//			syserrrec.statuz.set(chdrrgpIO.getStatuz());
//			fatalError600();
//		}
		chdrrgpIO = null;
		if (chdrpfMap != null && chdrpfMap.containsKey(regxpfRec.getChdrnum())) {
			for (Chdrpf c : chdrpfMap.get(regxpfRec.getChdrnum())) {
				if ("CH".equals(c.getChdrpfx())) {
					chdrrgpIO = c;
					break;
				}
			}
		}
		if (chdrrgpIO == null) {
			syserrrec.params.set(regxpfRec.getChdrnum());
			fatalError600();
		}
		/* Validate the contract statii against T5679.*/
		wsaaValidStatus = "N";
		wsaaStatcode.set(chdrrgpIO.getStatcode());
		wsaaPstcde.set(chdrrgpIO.getPstcde());
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			validateChdrStatus2550();
		}
		if (isEQ(wsaaValidStatus,"N")) {
			wsspEdterror.set(SPACES);
//			contotrec.totno.set(ct06);
//			contotrec.totval.set(1);
//			callContot001();
			ct06Val++;
			regrIO.setExcode(t6762rec.regpayExcpIchs.toString());
			regrIO.setExreport("Y");
			reportProcess2600();
			return ;
		}
		/*  Read in the relevant COVR record*/
//		covrIO.setChdrcoy(bsprIO.getCompany());
//		covrIO.setChdrnum(regxpfRec.getchdrnum);
//		covrIO.setLife(regxpfRec.getlife);
//		covrIO.setCoverage(regxpfRec.getcoverage);
//		covrIO.setRider(regxpfRec.getrider);
//		covrIO.setPlanSuffix(regxpfRec.getplanSuffix);
//		covrIO.setFunction(varcom.readr);
//		covrIO.setFormat(formatsInner.covrrec);
//		SmartFileCode.execute(appVars, covrIO);
//		if ((isNE(covrIO.getStatuz(),varcom.oK))) {
//			syserrrec.params.set(covrIO.getParams());
//			syserrrec.statuz.set(covrIO.getStatuz());
//			fatalError600();
//		}
		covrIO = null;
		if (covrpfMap != null && covrpfMap.containsKey(regxpfRec.getChdrnum())) {
			for (Covrpf c : covrpfMap.get(regxpfRec.getChdrnum())) {
				if (c.getLife().equals(regxpfRec.getLife())
						&& c.getCoverage().equals(regxpfRec.getCoverage())
						&& c.getRider().equals(regxpfRec.getRider()) 
						&& c.getPlanSuffix() == regxpfRec.getPlanSuffix()) {
					covrIO = c;
					break;
				}
			}
		}
		if (covrIO == null) {
			syserrrec.params.set(regxpfRec.getChdrnum());
			fatalError600();
		}
		/* Validate the coverage statii against T5679.*/
		wsaaValidStatus = "N";
		wsaaStatcode.set(covrIO.getStatcode());
		wsaaPstcde.set(covrIO.getPstatcode());
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			validateCovrStatus2560();
		}
		if (isEQ(wsaaValidStatus,"N")) {
			wsspEdterror.set(SPACES);
//			contotrec.totno.set(ct07);
//			contotrec.totval.set(1);
//			callContot001();
			ct07Val++;
			regrIO.setExcode(t6762rec.regpayExcpIcos.toString());
			regrIO.setExreport("Y");
			reportProcess2600();
		}
		readTr5174100();
	}

protected void validateChdrStatus2550()
	{
		/*PARA*/
		/* Validate Contract status against T5679.*/
		if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()],wsaaStatcode)) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateChdrPremStatus2570();
			}
		}
		/*EXIT*/
	}

protected void validateCovrStatus2560()
	{
		/*PARA*/
		if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],wsaaStatcode)) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
				validateCovrPremStatus2580();
			}
		}
		/*EXIT*/
	}

protected void validateChdrPremStatus2570()
	{
		/*PARA*/
		if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()],wsaaPstcde)) {
			wsaaIndex.set(13);
			wsaaValidStatus = "Y";
		}
		/*EXIT*/
	}

protected void validateCovrPremStatus2580()
	{
		/*PARA*/
		if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],wsaaPstcde)) {
			wsaaIndex.set(13);
			wsaaValidStatus = "Y";
		}
		/*EXIT*/
	}

protected void softlock2590()
	{
		start2595();
	}

protected void start2595()
	{
		/* Soft lock the contract, if it is to be processed.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(regxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(regxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void reportProcess2600()
	{
		start2610();
	}

protected void start2610()
	{
		/* Write detail record to regular reporting file REGR*/
		/* This will be printed out in the reporting pgm B5370*/
		regrIO.setChdrcoy(regxpfRec.getChdrcoy());
		regrIO.setChdrnum(regxpfRec.getChdrnum());
		regrIO.setLife(regxpfRec.getLife());
		regrIO.setCoverage(regxpfRec.getCoverage());
		regrIO.setRider(regxpfRec.getRider());
		regrIO.setRgpynum(regxpfRec.getRgpynum());
		regrIO.setRgpytype(regxpfRec.getRgpytype());
		regrIO.setRgpystat(regxpfRec.getRgpystat());
		regrIO.setCrtable(regxpfRec.getCrtable());
		regrIO.setTranno(regxpfRec.getTranno());
		regrIO.setPymt(regpIO.getPymt());
		regrIO.setCurrcd(regpIO.getCurrcd());
		regrIO.setPrcnt(regpIO.getPrcnt());
		regrIO.setPayreason(regpIO.getPayreason());
		regrIO.setRevdte(regpIO.getRevdte());
		regrIO.setFirstPaydate(regpIO.getFirstPaydate());
		regrIO.setLastPaydate(regpIO.getLastPaydate());
		regrIO.setProgname(bprdIO.getBatchProgram().toString());
		this.regrpfListInst.add(regrIO);
//		regrIO.setFunction("WRITR");
//		regrIO.setFormat(formatsInner.regrrec);
//		SmartFileCode.execute(appVars, regrIO);
//		if (isNE(regrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(regrIO.getParams());
//			syserrrec.statuz.set(regrIO.getStatuz());
//			fatalError600();
//		}
	}

protected void update3000()
	{
		/*UPDATE*/
		rewriteChdr3120();
		rewritePayr3200();
		writePtrn3300();
		rewriteRegp3400();
		releaseSoftlock3700();
		/*EXIT*/
	}

protected void rewriteChdr3120()
	{
		readh3125();
	}

protected void readh3125()
	{
		/*  Obtain the CHDR record using CHDRPAY logical file.*/
//		chdrpayIO.setParams(SPACES);
//		chdrpayIO.setChdrcoy(chdrrgpIO.getChdrcoy());
//		chdrpayIO.setChdrnum(chdrrgpIO.getChdrnum());
//		chdrpayIO.setFunction(varcom.begnh);
//		chdrpayIO.setFormat(formatsInner.chdrpayrec);
//		SmartFileCode.execute(appVars, chdrpayIO);
//		if ((isNE(chdrpayIO.getStatuz(),varcom.oK))
//		&& (isNE(chdrpayIO.getStatuz(),varcom.endp))) {
//			syserrrec.params.set(chdrpayIO.getParams());
//			syserrrec.statuz.set(chdrpayIO.getStatuz());
//			fatalError600();
//		}
//		if ((isNE(chdrpayIO.getChdrcoy(),chdrrgpIO.getChdrcoy()))
//		|| (isNE(chdrpayIO.getChdrnum(),chdrrgpIO.getChdrnum()))
//		|| (isNE(chdrpayIO.getValidflag(),chdrrgpIO.getValidflag()))
//		|| (isEQ(chdrpayIO.getStatuz(),varcom.endp))) {
//			chdrpayIO.setStatuz(varcom.endp);
//			syserrrec.params.set(chdrpayIO.getParams());
//			fatalError600();
//		}
		chdrpayIO = null;
		if (chdrpfMap != null && chdrpfMap.containsKey(chdrrgpIO.getChdrnum())) {
			for (Chdrpf c : chdrpfMap.get(chdrrgpIO.getChdrnum())) {
				chdrpayIO = c;
				/* Write validflag '2' Contract Header (CHDR) record*/
//				chdrpayIO.setValidflag("2");
//				chdrpayIO.setCurrto(bsscIO.getEffectiveDate());
//				chdrpayIO.setFunction(varcom.rewrt);
//				chdrpayIO.setFormat(formatsInner.chdrpayrec);
//				SmartFileCode.execute(appVars, chdrpayIO);
//				if ((isNE(chdrpayIO.getStatuz(),varcom.oK))) {
//					syserrrec.params.set(chdrpayIO.getParams());
//					syserrrec.statuz.set(chdrpayIO.getStatuz());
//					fatalError600();
//				}
				Chdrpf pf = new Chdrpf(chdrpayIO);
				pf.setUniqueNumber(chdrpayIO.getUniqueNumber());
				pf.setValidflag('2');
				pf.setCurrto(bsscIO.getEffectiveDate().toInt());
				this.chdrpfListUpdt.add(pf);
				/* Rewrite contract record with incremented TRANNO*/
				/*  and new SINSTAMT's if required*/
				if ((regpIO != null ) && regpIO.getDestkey()!=null && isNE(regpIO.getDestkey(),SPACES)) {  //san
					if (isLT(chdrpayIO.getSinstamt05(),ZERO)) {
						compute(wsaaSinstamt05, 2).set((add(regpIO.getPymt(),chdrpayIO.getSinstamt05())));
						setPrecision(chdrpayIO.getSinstamt06(), 2);
						chdrpayIO.setSinstamt06((add(add(add(add(chdrpayIO.getSinstamt01(),chdrpayIO.getSinstamt02()),chdrpayIO.getSinstamt03()),chdrpayIO.getSinstamt04()),wsaaSinstamt05)).getbigdata());
						chdrpayIO.setSinstamt05(wsaaSinstamt05.getbigdata());
					}
				}
				if (isEQ(wsaaIsWop,"Y")) {
					/*IF TR517-ZRWVFLG-01 NOT = 'Y'                    <LA2110>*/
					if (isEQ(tr517rec.zrwvflg01,"Y")) {
						/*         ADD      WSAA-WOP-INSTPREM TO CHDRPAY-SINSTAMT01<LA2110>*/
						setPrecision(chdrpayIO.getSinstamt06(), 2);
						chdrpayIO.setSinstamt06((add(add(add(add(chdrpayIO.getSinstamt01(),chdrpayIO.getSinstamt02()),chdrpayIO.getSinstamt03()),chdrpayIO.getSinstamt04()),chdrpayIO.getSinstamt05())).getbigdata());
					}
				}
				setPrecision(chdrpayIO.getTranno(), 0);
				chdrpayIO.setTranno(add(chdrpayIO.getTranno(),1).toInt());
				chdrpayIO.setValidflag('1');
				chdrpayIO.setCurrfrom(bsscIO.getEffectiveDate().toInt());
				chdrpayIO.setCurrto(varcom.vrcmMaxDate.toInt());
				this.chdrpfListInst.add(chdrpayIO);
//				chdrpayIO.setFunction(varcom.writr);
//				SmartFileCode.execute(appVars, chdrpayIO);
//				if (isNE(chdrpayIO.getStatuz(),varcom.oK)) {
//					syserrrec.params.set(chdrpayIO.getParams());
//					syserrrec.statuz.set(chdrpayIO.getStatuz());
//					fatalError600();
//				}
				break;
			}
		}
		if (chdrpayIO == null) {
			syserrrec.params.set(chdrrgpIO.getChdrnum());
			fatalError600();
		}
	}

protected void rewritePayr3200()
	{
		begnh3210();
	}

protected void begnh3210()
	{
		/*  Obtain the PAYR record*/
//		payrIO.setParams(SPACES);
//		payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
//		payrIO.setChdrnum(chdrrgpIO.getChdrnum());
//		payrIO.setPayrseqno(ZERO);
//		payrIO.setFunction(varcom.begnh);
//		payrIO.setFormat(formatsInner.payrrec);
//		SmartFileCode.execute(appVars, payrIO);
//		if (isNE(payrIO.getStatuz(),varcom.oK)
//		&& isNE(payrIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.statuz.set(payrIO.getStatuz());
//			fatalError600();
//		}
//		if (isNE(payrIO.getChdrnum(),chdrrgpIO.getChdrnum())
//		|| isNE(payrIO.getChdrcoy(),chdrrgpIO.getChdrcoy())
//		|| isNE(payrIO.getValidflag(),"1")
//		|| isEQ(payrIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.statuz.set(payrIO.getStatuz());
//			fatalError600();
//		}
		payrIO = null;
		if (payrpfMap != null && payrpfMap.containsKey(chdrrgpIO.getChdrnum())) {
			for (Payrpf c : payrpfMap.get(chdrrgpIO.getChdrnum())) {
				if (  "1".equals(c.getValidflag())) {
					payrIO = c;
					/* Write validflag '2' Payer (PAYR) record*/
//					payrIO.setValidflag("2");
//					payrIO.setFunction(varcom.rewrt);
//					payrIO.setFormat(formatsInner.payrrec);
//					SmartFileCode.execute(appVars, payrIO);
//					if (isNE(payrIO.getStatuz(),varcom.oK)) {
//						syserrrec.params.set(payrIO.getParams());
//						syserrrec.statuz.set(payrIO.getStatuz());
//						fatalError600();
//					}
					Payrpf pf = new Payrpf(payrIO);
					pf.setValidflag("2");
					this.payrpfListUpdt.add(pf);
					/* Write new payer PAYR record with incremented TRANNO*/
					/* and updated SINSTAMT's if required*/
					if (regpIO.getDestkey()!=null && isNE(regpIO.getDestkey(),SPACES)) { //san
						if (isLT(payrIO.getSinstamt05(),ZERO)) {
							compute(wsaaPayrSinstamt05, 2).set((add(regpIO.getPymt(),payrIO.getSinstamt05())));
							setPrecision(payrIO.getSinstamt06(), 2);
							payrIO.setSinstamt06((add(add(add(add(payrIO.getSinstamt01(),payrIO.getSinstamt02()),payrIO.getSinstamt03()),payrIO.getSinstamt04()),wsaaPayrSinstamt05)).getbigdata());
							payrIO.setSinstamt05(wsaaPayrSinstamt05.getbigdata());
						}
					}
					if (isEQ(wsaaIsWop,"Y")) {
						/*IF TR517-ZRWVFLG-01 NOT = 'Y'                    <LA2110>*/
						if (isEQ(tr517rec.zrwvflg01,"Y")) {
							/*          ADD      WSAA-WOP-INSTPREM TO   PAYR-SINSTAMT01<LA2110>*/
							setPrecision(payrIO.getSinstamt06(), 2);
							payrIO.setSinstamt06((add(add(add(add(payrIO.getSinstamt01(),payrIO.getSinstamt02()),payrIO.getSinstamt03()),payrIO.getSinstamt04()),payrIO.getSinstamt05())).getbigdata());
						}
					}
					h900UpdateCovr();
					payrIO.setTranno(chdrpayIO.getTranno());
					payrIO.setValidflag("1");
//					payrIO.setFunction(varcom.writr);
//					SmartFileCode.execute(appVars, payrIO);
//					if (isNE(payrIO.getStatuz(),varcom.oK)) {
//						syserrrec.params.set(payrIO.getParams());
//						syserrrec.params.set(payrIO.getStatuz());
//						fatalError600();
//					}
					if ( payrpfListInst.size()==0)
					{
						this.payrpfListInst.add(payrIO);
					}
					break;
				}
			}
		}
		if (payrIO == null) {
			syserrrec.params.set(chdrrgpIO.getChdrnum());
			fatalError600();
		}
	}

protected void writePtrn3300()
	{
		begnh3310();
	}

protected void begnh3310()
	{
		/*  Create a PTRN record*/
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setTermid(varcom.vrcmTermid.toString());
		ptrnIO.setTrdt(datcon1rec.intDate.toInt());
		ptrnIO.setTrtm(Integer.parseInt(getCobolTime()));
		ptrnIO.setUserT(999999);
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setBatccoy(bprdIO.getCompany().toString());
		ptrnIO.setBatcbrn(bupaIO.getBranch().toString());
		ptrnIO.setBatcactyr(bsscIO.getAcctYear().toInt());
		ptrnIO.setBatctrcde(bprdIO.getAuthCode().toString());
		ptrnIO.setBatcactmn(bsscIO.getAcctMonth().toInt());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		ptrnIO.setTranno(chdrpayIO.getTranno());
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrpayIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(regxpfRec.getChdrnum());
		ptrnIO.setValidflag("1");
		this.ptrnpfList.add(ptrnIO);
//		ptrnIO.setFormat(formatsInner.ptrnrec);
//		ptrnIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, ptrnIO);
//		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(ptrnIO.getParams());
//			syserrrec.statuz.set(ptrnIO.getStatuz());
//			fatalError600();
//		}
	}

protected void rewriteRegp3400()
	{
		rewrt3410();
	}

protected void rewrt3410()
	{
		/*  Update the REGP record with valid flag of '2'*/
	    if(regpIO != null) {
		regpIO.setUser(999999);
		regpIO.setTransactionDate(datcon1rec.intDate.toInt());
		regpIO.setTransactionTime(Integer.parseInt(getCobolTime()));
		regpIO.setValidflag("2");
		this.regppfListUpdt.add(regpIO);
//		regpIO.setFunction(varcom.rewrt);
//		SmartFileCode.execute(appVars, regpIO);
//		if (isNE(regpIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(regpIO.getParams());
//			syserrrec.statuz.set(regpIO.getStatuz());
//			fatalError600();
//		}
		Regppf pf = new Regppf(regpIO);
		pf.setValidflag("1");
		pf.setTranno(chdrpayIO.getTranno());
		pf.setRgpystat(wsaaNextPaystat.toString());
		pf.setAprvdate(varcom.vrcmMaxDate.toInt());
		/* MOVE VRCM-MAX-DATE          TO REGP-RECVD-DATE.      <LA4217>*/
		/* MOVE VRCM-MAX-DATE          TO REGP-INCURDT.         <LA4217>*/
		this.regppfListInst.add(pf);
//		regpIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, regpIO);
//		if (isNE(regpIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(regpIO.getParams());
//			syserrrec.statuz.set(regpIO.getStatuz());
//			fatalError600();
//		}
		/* write a non-exception record to the REGR reporting file.*/
		regrIO.setExcode("");
		regrIO.setExreport("");
		reportProcess2600();
		/* Increment the Terminated Control Total.*/
//		contotrec.totno.set(ct02);
//		contotrec.totval.set(1);
//		callContot001();
		ct02Val++;
	    }
	}

protected void commit3500()
	{
	commitControlTotals();
	
	regrpfDAO.insertRecords(regrpfListInst);
	regrpfListInst.clear();
	
	chdrpfDAO.updateInvalidChdrRecord(chdrpfListUpdt);
	chdrpfListUpdt.clear();
	chdrpfDAO.insertChdrAmt(chdrpfListInst);
	chdrpfListInst.clear();
	
	payrpfDAO.updatePayrRecord(payrpfListUpdt);
	payrpfListUpdt.clear();
	payrpfDAO.insertPayrpfList(payrpfListInst);
	payrpfListInst.clear();
	
	regppfDAO.updateValidflag(regppfListUpdt);
	regppfListUpdt.clear();
	regppfDAO.insertRecords(regppfListInst);
	regppfListInst.clear();
	
	covrpfDAO.updateCovrValidFlag(covrpfListUpdt);
	covrpfListUpdt.clear();
	covrpfDAO.insertCovrpfList(covrpfListInst);
	covrpfListInst.clear();
	
	ptrnpfDAO.insertPtrnPF(ptrnpfList);
	ptrnpfList.clear();
	}
private void commitControlTotals() {
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Val);
	callContot001();
	ct01Val = 0;
	
	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Val);
	callContot001();
	ct02Val=0;
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Val);
	callContot001();
	ct04Val=0;
	
	contotrec.totno.set(ct05);
	contotrec.totval.set(ct05Val);
	callContot001();
	ct05Val=0;
	
	contotrec.totno.set(ct06);
	contotrec.totval.set(ct06Val);
	callContot001();
	ct06Val=0;
	
	contotrec.totno.set(ct07);
	contotrec.totval.set(ct07Val);
	callContot001();
	ct07Val=0;
}
protected void rollback3600()
	{
		/*ROLLBACK*/
		/** No processing required*/
		/*EXIT*/
	}

protected void releaseSoftlock3700()
	{
		unlk3710();
	}

protected void unlk3710()
	{
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(regxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(regxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void close4000()
	{
	chdrpfMap.clear();
	covrpfMap.clear();
	regppfMap.clear();
	payrpfMap.clear();
	chdrpfMap = null;
	covrpfMap =null;
	regppfMap =null;
	payrpfMap=null;
	iter = null;
	
	regrpfListInst.clear();
	chdrpfListUpdt.clear();
	chdrpfListInst.clear();
	payrpfListUpdt.clear();
	payrpfListInst.clear();
	regppfListUpdt.clear();
	regppfListInst.clear();
	ptrnpfList.clear();
	covrpfListUpdt.clear();
	covrpfListInst.clear();
	
	regrpfListInst=null;
	chdrpfListUpdt=null;
	chdrpfListInst=null;
	payrpfListUpdt=null;
	payrpfListInst=null;
	regppfListUpdt=null;
	regppfListInst=null;
	ptrnpfList=null;
	covrpfListUpdt=null;
	covrpfListInst=null;
	
	lsaaStatuz.set(varcom.oK);
	}

protected void readTr5174100()
	{
		para4110();
	}

protected void para4110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(bprdIO.getCompany());
		itdmIO.setItemtabl(tr517);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bprdIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),tr517)
		|| isNE(itdmIO.getItemitem(),covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaIsWop = "N";
		}
		else {
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
			wsaaIsWop = "Y";
		}
	}

protected void h900UpdateCovr()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					h901Para();
				case h910CallCovrio:
					h910CallCovrio();
				case h990Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void h901Para()
	{
		/* If waive itself is set to 'Y', then we need to update the       */
		/* status of the component to waived                               */
		if (isEQ(wsaaIsWop,"Y")) {
			/*IF TR517-ZRWVFLG-01 = 'Y'                        <LA2110>*/
			if (isNE(tr517rec.zrwvflg01,"Y")) {
				goTo(GotoLabel.h990Exit);
			}
		}
		else {
			goTo(GotoLabel.h990Exit);
		}
//		covrIO.setParams(SPACES);
//		covrIO.setChdrcoy(regpIO.getChdrcoy());
//		covrIO.setChdrnum(regpIO.getChdrnum());
//		covrIO.setLife(regpIO.getLife());
//		covrIO.setCoverage(regpIO.getCoverage());
//		covrIO.setRider(regpIO.getRider());
//		covrIO.setPlanSuffix(ZERO);
//		covrIO.setFunction(varcom.begnh);
		covrIO = null;
		if (covrpfMap != null && covrpfMap.containsKey(regpIO.getChdrnum())) {
			//int i = 0;
			for (Covrpf c : covrpfMap.get(regpIO.getChdrnum())) {
				if (c.getLife().equals(regpIO.getLife())
						&& c.getCoverage().equals(regpIO.getCoverage())
						&& c.getRider().equals(regpIO.getRider()) 
						&& c.getPlanSuffix() == 0//i++
						&& c.getChdrcoy().equals(regpIO.getChdrcoy())) {
					covrIO = c;
					break;
				}
			}
		}
		if (covrIO == null) {
			syserrrec.params.set(regpIO.getChdrnum());
			fatalError600();
		}
	}

protected void h910CallCovrio()
	{
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(),varcom.oK)
//		&& isNE(covrIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(covrIO.getParams());
//			syserrrec.statuz.set(covrIO.getStatuz());
//			fatalError600();
//		}
//		if (isNE(covrIO.getChdrcoy(),regpIO.getChdrcoy())
//		|| isNE(covrIO.getChdrnum(),regpIO.getChdrnum())
//		|| isNE(covrIO.getLife(),regpIO.getLife())
//		|| isNE(covrIO.getCoverage(),regpIO.getCoverage())
//		|| isNE(covrIO.getRider(),regpIO.getRider())
//		|| isEQ(covrIO.getStatuz(),varcom.endp)) {
//			return ;
//		}
		if (isNE(covrIO.getValidflag(),"1")) {
			syserrrec.params.set(covrIO.getChdrnum());
//			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		Covrpf pf = new Covrpf(covrIO);
		pf.setValidflag("2");
		pf.setCurrto(bsscIO.getEffectiveDate().toInt());
		this.covrpfListUpdt.add(pf);
//		covrIO.setFunction(varcom.rewrt);
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(covrIO.getParams());
//			syserrrec.statuz.set(covrIO.getStatuz());
//			fatalError600();
//		}
		if (isEQ(covrIO.getRider(),"00")) {
			covrIO.setStatcode(t5679rec.setCovRiskStat.toString());
			covrIO.setPstatcode(t5679rec.setCovPremStat.toString());
		}
		else {
			covrIO.setStatcode(t5679rec.setRidRiskStat.toString());
			covrIO.setPstatcode(t5679rec.setRidPremStat.toString());
		}
		covrIO.setValidflag("1");
		covrIO.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		covrIO.setCurrto(varcom.vrcmMaxDate.toInt());
		covrIO.setTranno(chdrpayIO.getTranno());
		this.covrpfListInst.add(covrIO);
//		covrIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(covrIO.getParams());
//			syserrrec.statuz.set(covrIO.getStatuz());
//			fatalError600();
//		}
		//setPrecision(covrIO.getPlanSuffix(), 0);
//		covrIO.setPlanSuffix(add(covrIO.getPlanSuffix(),1).toInt());
		int nextPlanSuffix = covrIO.getPlanSuffix() + 1;
//		covrIO.setFunction(varcom.begnh);
		covrIO = null;
		if (covrpfMap != null && covrpfMap.containsKey(regpIO.getChdrnum())) {
			for (Covrpf c : covrpfMap.get(regpIO.getChdrnum())) {
				if (c.getLife().equals(regpIO.getLife())
						&& c.getCoverage().equals(regpIO.getCoverage())
						&& c.getRider().equals(regpIO.getRider()) 
						&& c.getPlanSuffix() == nextPlanSuffix
						&& c.getChdrcoy().equals(regpIO.getChdrcoy())) {
					covrIO = c;
					break;
				}
			}
		}
		if (covrIO == null) {
			return;
//			syserrrec.params.set(regpIO.getChdrnum());
//			fatalError600();
		}
		goTo(GotoLabel.h910CallCovrio);
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData chdrpayrec = new FixedLengthStringData(10).init("CHDRPAYREC");
	private FixedLengthStringData chdrrgprec = new FixedLengthStringData(10).init("CHDRRGPREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(7).init("PTRNREC");
	private FixedLengthStringData regrrec = new FixedLengthStringData(7).init("REGRREC");
}
}
