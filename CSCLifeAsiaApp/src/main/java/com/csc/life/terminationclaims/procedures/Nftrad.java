/*
 * File: Nftrad.java
 * Date: 29 August 2009 23:00:50
 * Author: Quipoz Limited
 * 
 * Class transformed from NFTRAD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*           NON-FORFEITURE TRADITIONAL CONTRACT SUBROUTINE
*           ----------------------------------------------
*
* OVERVIEW.
* ---------
*
*       This subroutine forms part of the PAID-UP/LAPSE processing
*       routine. The calling program passes the contract details
*       through the linkage OVRDUEREC to this program to perform
*       Agent Commission claw back routine.
*
* PROCESSING
* ----------
*
*       Commission claw back. This routine aims to deal with
*       agent commission records in ACMVSUR.
*
*       The  key  will  be set up in the following order :
*
*       Company Number        -  Passed in Linkage
*       Contract Number       -     "    "    "
*       Life                  -     "    "    "
*       Coverage              -     "    "    "
*       Rider                 -     "    "    "
*       Plan Suffix           -     "    "    "
*       Agntnum               -     "    "    "
*
*       If the program returns with a different key values
*       i.e. key breaks or AGCM-STATUZ equals ENDP, go to exit.
*
*       If AGCM commission paid = commission earned
*       (AGCMSUR-COMPAY = AGCMSUR-COMERN) then read the next recor 
*       Else
*       Create two ACMV records for each transaction, one for
*       Commission Clawback for agents, and one for Re-crediting
*       Agent  Commission  Advance.  The  details  for these
*       will be held on T5645, accessed by  program  Id.  Line
*       #1  will  give  the  details for the first posting for whi h
*       the amount on the ACMV will be negative  and  line  #2  wi l
*       give  the  details  for  the  second  posting  for which t e
*       amount on the ACMV will be positive.
*
*       The subroutine LIFACMV  will  be  called  to  add  the
*       ACMV records.
*
*       ACMV-RDOCNUM           -  OVRD-CHDRNUM
*       ACMV-RLDGACCT          -  OVRD-AGNTNUM
*       ACMV-BATCCOY           -  OVRD-CHDRCOY
*       ACMV-ORIGCURR          -  OVRD-CNTCURR
*       ACMV-TRANNO            -  OVRD-TRANNO
*       ACMV-JRNSEQ            -  1
*       ACMV-ORIGAMT           -  Difference between commission
*                                 paid/earned.
*                                 (Earned minus Paid).
*       ACMV-TRANREF           -  OVRD-CHDRNUM
*       ACMV-SACSTYP           -  FROM T5645
*       ACMV-SACSCODE          -  FROM T5645
*       ACMV-GLCODE            -  FROM T5645
*       ACMV-GLSIGN            -  FROM T5645
*       ACMV-CNTTOT            -  FROM T5645
*       ACMV-POSTYEAR          -  From LINKAGE
*       ACMV-POSTMONTH         -    "    "
*       ACMV-EFFDATE           -  OVRD-PTDATE
*       ACMV-TRANSACTION-DATE  -  OVRD-TRAN-DATE
*       ACMV-TRANSACTION-TIME  -  OVRD-TRAN-TIME
*       ACMV-USER              -  OVRD USER
*       ACMV-TERMID            -  Term Id. passed in linkage
*
*****************************************************************
* </pre>
*/
public class Nftrad extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "NFTRAD";
	private ZonedDecimalData wsaaCommRecovered = new ZonedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
		/* ERRORS */
	private static final String e308 = "E308";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String th605 = "TH605";
		/* FORMATS */
	private static final String descrec = "DESCREC   ";
	private static final String itemrec = "ITEMREC   ";
	private static final String loanenqrec = "LOANENQREC";
	private static final String agcmrec = "AGCMREC   ";
	private static final String agcmdbcrec = "AGCMREC   ";
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	protected Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	protected Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Th605rec th605rec = new Th605rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Trmreasrec trmreasrec = new Trmreasrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	protected Ovrduerec ovrduerec1 = new Ovrduerec();
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class); //IBPLIFE-11192

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		callAgcmio1020
	}

	public Nftrad() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec1.ovrdueRec = convertAndSetParam(ovrduerec1.ovrdueRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		ovrduerec1.statuz.set(varcom.oK);
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.jrnseq.set(0);
		/* IF OVRD-PTDATE          NOT = OVRD-BTDATE               <001>*/
		/*    PERFORM 500-CALL-REVERBILL.                          <001>*/
		commClawBack1000();
		loans6000();
		processReassurance7000();
		updateZrae();
		/*    MOVE OVRD-PTDATE            TO OVRD-NEW-PREM-CESS-DATE  <J12>*/
		/*                                   OVRD-NEW-RISK-CESS-DATE. <J12>*/
		ovrduerec1.newPremCessDate.set(ovrduerec1.premCessDate);
		ovrduerec1.newSumins.set(ovrduerec1.sumins);
	}

protected void exit090()
	{
		exitProgram();
	}

	/**
	* <pre>
	*500-CALL-REVERBILL SECTION.                                 <001>
	*                                                            <001>
	*510-START.                                                  <001>
	**** MOVE OVRD-CHDRCOY           TO REVE-COMPANY.            <001>
	**** MOVE OVRD-CHDRNUM           TO REVE-CHDRNUM.            <001>
	**** MOVE OVRD-LIFE              TO REVE-LIFE.               <001>
	**** MOVE OVRD-COVERAGE          TO REVE-COVERAGE.           <001>
	**** MOVE OVRD-RIDER             TO REVE-RIDER.              <001>
	**** MOVE OVRD-PLAN-SUFFIX       TO REVE-PLAN-SUFFIX.        <001>
	**** MOVE SPACES                 TO REVE-BATCPFX.            <001>
	**** MOVE OVRD-COMPANY           TO REVE-BATCCOY.            <001>
	**** MOVE OVRD-BATCBRN           TO REVE-BATCBRN.            <001>
	**** MOVE OVRD-ACCTYEAR          TO REVE-BATCACTYR.          <001>
	**** MOVE OVRD-ACCTMONTH         TO REVE-BATCACTMN.          <001>
	**** MOVE SPACES                 TO REVE-BATCTRCDE.          <001>
	**** MOVE OVRD-BATCBATCH         TO REVE-BATCBATCH.          <001>
	**** MOVE OVRD-PTDATE            TO REVE-EFFDATE-1.          <001>
	**** MOVE VRCM-MAX-DATE          TO REVE-EFFDATE-2.          <001>
	**** MOVE ZEROS                  TO REVE-TRANNO.             <001>
	**** MOVE OVRD-TRANNO            TO REVE-NEW-TRANNO.         <001>
	**** MOVE OVRD-TRANCODE          TO REVE-OLD-BATCTRCDE.      <001>
	****                                                         <001>
	**** CALL 'REVBILL' USING REVE-REVERSE-REC.                  <001>
	****                                                         <001>
	**** IF REVE-STATUZ           NOT = O-K                      <001>
	****    MOVE REVE-STATUZ          TO SYSR-STATUZ             <001>
	****    MOVE REVE-REVERSE-REC     TO SYSR-PARAMS             <001>
	****                                                         <001>
	*590-EXIT.                                                   <001>
	**** EXIT.                                                   <001>
	*1000-COMM-CLAW-BACK SECTION.                                     
	*1010-PARA.                                                       
	**** PERFORM 2000-READ-T5645-T1688.                               
	**** PERFORM 2500-READ-T5688.                                <002>
	* Read all the relevant AGCM record for this contract.
	*    MOVE SPACE                  TO AGCMSUR-DATA-AREA.            
	*    MOVE OVRD-CHDRNUM           TO AGCMSUR-CHDRNUM.              
	*    MOVE OVRD-CHDRCOY           TO AGCMSUR-CHDRCOY.              
	*    MOVE OVRD-LIFE              TO AGCMSUR-LIFE.                 
	*    MOVE OVRD-COVERAGE          TO AGCMSUR-COVERAGE.             
	*    MOVE OVRD-RIDER             TO AGCMSUR-RIDER.                
	*    MOVE OVRD-PLAN-SUFFIX       TO AGCMSUR-PLAN-SUFFIX.          
	*    MOVE OVRD-AGNTNUM           TO AGCMSUR-AGNTNUM.              
	*    MOVE BEGNH                  TO AGCMSUR-FUNCTION.             
	*    MOVE AGCMSURREC             TO AGCMSUR-FORMAT.               
	**** MOVE SPACE                  TO AGCMLAP-DATA-AREA.       <002>
	**** MOVE OVRD-CHDRNUM           TO AGCMLAP-CHDRNUM.         <002>
	**** MOVE OVRD-CHDRCOY           TO AGCMLAP-CHDRCOY.         <002>
	**** MOVE OVRD-LIFE              TO AGCMLAP-LIFE.            <002>
	**** MOVE OVRD-COVERAGE          TO AGCMLAP-COVERAGE.        <002>
	**** MOVE OVRD-RIDER             TO AGCMLAP-RIDER.           <002>
	**** MOVE OVRD-PLAN-SUFFIX       TO AGCMLAP-PLAN-SUFFIX.     <002>
	*****MOVE OVRD-AGNTNUM           TO AGCMLAP-AGNTNUM.              
	**** MOVE BEGNH                  TO AGCMLAP-FUNCTION.        <002>
	**** MOVE BEGN                   TO AGCMLAP-FUNCTION.        <004>
	**** MOVE AGCMLAPREC             TO AGCMLAP-FORMAT.          <002>
	*1050-CALL-AGCMSURIO.                                             
	*1050-CALL-AGCMLAPIO.                                        <002>
	*****CALL 'AGCMSURIO'            USING AGCMSUR-PARAMS.            
	**** CALL 'AGCMLAPIO'            USING AGCMLAP-PARAMS.       <002>
	*    IF (AGCMSUR-STATUZ          NOT = O-K) AND                   
	*       (AGCMSUR-STATUZ          NOT = ENDP)                      
	*       MOVE AGCMSUR-STATUZ      TO SYSR-STATUZ                   
	*       MOVE AGCMSUR-PARAMS      TO SYSR-PARAMS                   
	**** IF (AGCMLAP-STATUZ          NOT = O-K) AND              <002>
	****    (AGCMLAP-STATUZ          NOT = ENDP)                 <002>
	****    MOVE AGCMLAP-STATUZ      TO SYSR-STATUZ              <002>
	****    MOVE AGCMLAP-PARAMS      TO SYSR-PARAMS              <002>
	*****IF AGCMSUR-STATUZ           = ENDP                           
	**** IF AGCMLAP-STATUZ           = ENDP                      <002>
	**** GO TO 1090-EXIT.                                             
	*    IF (AGCMSUR-CHDRCOY         NOT = OVRD-CHDRCOY) OR           
	*       (AGCMSUR-CHDRNUM         NOT = OVRD-CHDRNUM) OR           
	*       (AGCMSUR-LIFE            NOT = OVRD-LIFE) OR              
	*       (AGCMSUR-COVERAGE        NOT = OVRD-COVERAGE) OR          
	*       (AGCMSUR-RIDER           NOT = OVRD-RIDER)OR              
	*       (AGCMSUR-PLAN-SUFFIX     NOT = OVRD-PLAN-SUFFIX)OR        
	*       (AGCMSUR-AGNTNUM         NOT = OVRD-AGNTNUM)              
	*       MOVE ENDP                TO AGCMSUR-STATUZ                
	**** IF (AGCMLAP-CHDRCOY         NOT = OVRD-CHDRCOY) OR      <002>
	****    (AGCMLAP-CHDRNUM         NOT = OVRD-CHDRNUM) OR      <002>
	****    (AGCMLAP-LIFE            NOT = OVRD-LIFE) OR         <002>
	****    (AGCMLAP-COVERAGE        NOT = OVRD-COVERAGE) OR     <002>
	****    (AGCMLAP-RIDER           NOT = OVRD-RIDER)OR         <002>
	********(AGCMLAP-PLAN-SUFFIX     NOT = OVRD-PLAN-SUFFIX)OR        
	****    (AGCMLAP-PLAN-SUFFIX     NOT = OVRD-PLAN-SUFFIX)     <002>
	********(AGCMLAP-AGNTNUM         NOT = OVRD-AGNTNUM)              
	****    MOVE ENDP                TO AGCMLAP-STATUZ                
	**** GO TO 1090-EXIT.                                             
	* Check to see if agent commission clawback is required. If
	* the commission paid is the same as commission earned, no
	* clawback is required.
	*    IF AGCMSUR-COMPAY           = AGCMSUR-COMERN                 
	*       MOVE NEXTR               TO AGCMSUR-FUNCTION              
	*    GO TO 1050-CALL-AGCMSURIO.                                   
	**** IF AGCMLAP-COMPAY           = AGCMLAP-COMERN            <002>
	****    MOVE NEXTR               TO AGCMLAP-FUNCTION         <002>
	**** GO TO 1050-CALL-AGCMLAPIO.                              <002>
	* Calculate differences between commission earned/paid.
	**** COMPUTE WSAA-COMM-RECOVERED =                                
	***********(AGCMSUR-COMPAY - AGCMSUR-COMERN)                      
	****       (AGCMLAP-COMPAY - AGCMLAP-COMERN)                 <002>
	*****MOVE AGCMSUR-COMERN         TO AGCMSUR-COMPAY                
	**** MOVE AGCMLAP-COMERN         TO AGCMLAP-COMPAY           <002>
	* Check if this is override commission or normal clawback         
	* commission and call LIFACMV using appropriate accounting rules. 
	*****IF AGCMSUR-OVRDCAT = 'O'                                     
	**** IF AGCMLAP-OVRDCAT = 'O'                                <002>
	****     PERFORM 4000-OVERRIDE-COMM-ACMV                     <002>
	**** ELSE                                                         
	****     PERFORM 3000-COMM-CLAWBACK-ACMV.                    <002>
	*****PERFORM 3000-COMM-CLAWBACK-ACMV.                             
	* Rewrite the AGCM record and read the next one.
	*    MOVE AGCMSURREC             TO AGCMSUR-FORMAT.               
	*    MOVE REWRT                  TO AGCMSUR-FUNCTION.             
	*    CALL 'AGCMSURIO' USING AGCMSUR-PARAMS.                       
	*    IF AGCMSUR-STATUZ           NOT = O-K                        
	*        MOVE AGCMSUR-PARAMS     TO SYSR-PARAMS                   
	*        MOVE AGCMSUR-STATUZ     TO SYSR-STATUZ                   
	*    MOVE NEXTR                  TO AGCMSUR-FUNCTION.             
	*    GO TO 1050-CALL-AGCMSURIO.                                   
	*                                                            <004>
	**** MOVE AGCMLAPREC             TO AGCMLAP-FORMAT.          <004>
	**** MOVE READH                  TO AGCMLAP-FUNCTION.        <004>
	****                                                         <004>
	**** CALL 'AGCMLAPIO'            USING AGCMLAP-PARAMS.       <004>
	****                                                         <004>
	**** IF AGCMLAP-STATUZ           NOT = O-K                   <004>
	****     MOVE AGCMLAP-PARAMS     TO SYSR-PARAMS              <004>
	****     MOVE AGCMLAP-STATUZ     TO SYSR-STATUZ              <004>
	**** MOVE AGCMLAPREC             TO AGCMLAP-FORMAT.          <002>
	**** MOVE REWRT                  TO AGCMLAP-FUNCTION.        <002>
	****                                                         <002>
	**** CALL 'AGCMLAPIO' USING AGCMLAP-PARAMS.                  <002>
	****                                                         <002>
	**** IF AGCMLAP-STATUZ           NOT = O-K                   <002>
	****     MOVE AGCMLAP-PARAMS     TO SYSR-PARAMS              <002>
	****     MOVE AGCMLAP-STATUZ     TO SYSR-STATUZ              <002>
	****                                                         <002>
	* Read the relevant AGCM record for this contract.                
	**** MOVE SPACES                 TO AGCMSUR-PARAMS.          <007>
	**** MOVE OVRD-CHDRNUM           TO AGCMSUR-CHDRNUM.         <007>
	**** MOVE OVRD-CHDRCOY           TO AGCMSUR-CHDRCOY.         <007>
	**** MOVE OVRD-LIFE              TO AGCMSUR-LIFE.            <007>
	**** MOVE OVRD-COVERAGE          TO AGCMSUR-COVERAGE.        <007>
	**** MOVE OVRD-RIDER             TO AGCMSUR-RIDER.           <007>
	**** MOVE OVRD-PLAN-SUFFIX       TO AGCMSUR-PLAN-SUFFIX.     <007>
	**** MOVE AGCMLAP-AGNTNUM        TO AGCMSUR-AGNTNUM.         <007>
	**** MOVE READH                  TO AGCMSUR-FUNCTION.        <007>
	**** MOVE AGCMSURREC             TO AGCMSUR-FORMAT.          <007>
	**** CALL 'AGCMSURIO'            USING AGCMSUR-PARAMS.       <007>
	**** IF AGCMSUR-STATUZ           NOT = O-K                   <007>
	****    MOVE AGCMSUR-STATUZ      TO SYSR-STATUZ              <007>
	****    MOVE AGCMSUR-PARAMS      TO SYSR-PARAMS              <007>
	**** END-IF.                                                 <007>
	**** MOVE AGCMSUR-COMERN         TO AGCMSUR-COMPAY.          <007>
	**** MOVE AGCMSURREC             TO AGCMSUR-FORMAT.          <007>
	**** MOVE REWRT                  TO AGCMSUR-FUNCTION.        <007>
	**** CALL 'AGCMSURIO'            USING AGCMSUR-PARAMS.       <007>
	**** IF AGCMSUR-STATUZ           NOT = O-K                   <007>
	****    MOVE AGCMSUR-STATUZ      TO SYSR-STATUZ              <007>
	****    MOVE AGCMSUR-PARAMS      TO SYSR-PARAMS              <007>
	**** END-IF.                                                 <007>
	**** MOVE NEXTR                  TO AGCMLAP-FUNCTION.        <002>
	*                                                            <002>
	**** GO TO 1050-CALL-AGCMLAPIO.                              <002>
	*                                                            <002>
	*1090-EXIT.                                                       
	*     EXIT.                                                       
	* </pre>
	*/
protected void commClawBack1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1010();
				case callAgcmio1020: 
					callAgcmio1020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1010()
	{
		readTableTh6051400();
		readT5645T16882000();
		readT56882500();
		/* Read the first AGCM record for this contract.                   */
		agcmIO.setParams(SPACES);
		agcmIO.setChdrnum(ovrduerec1.chdrnum);
		agcmIO.setChdrcoy(ovrduerec1.chdrcoy);
		agcmIO.setPlanSuffix(ZERO);
		agcmIO.setFunction(varcom.begn);
		agcmIO.setFormat(agcmrec);
	}

protected void callAgcmio1020()
	{

	//performance improvement --  atiwari23 
	agcmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	agcmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	

		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)
		&& isNE(agcmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			dbError5000();
		}
		/* If the end of the AGCM file has been reached or an AGCM         */
		/* record was read but not for the contract being processed,       */
		/* then exit the section.                                          */
		if (isEQ(agcmIO.getStatuz(), varcom.endp)
		|| isNE(agcmIO.getChdrcoy(), ovrduerec1.chdrcoy)
		|| isNE(agcmIO.getChdrnum(), ovrduerec1.chdrnum)) {
			return ;
		}
		/* for the AGCM record just read.                                  */
		if (isEQ(agcmIO.getChdrcoy(), ovrduerec1.chdrcoy)
		&& isEQ(agcmIO.getChdrnum(), ovrduerec1.chdrnum)
		&& isEQ(agcmIO.getLife(), ovrduerec1.life)
		&& isEQ(agcmIO.getCoverage(), ovrduerec1.coverage)
		&& isEQ(agcmIO.getRider(), ovrduerec1.rider)
		&& isEQ(agcmIO.getPlanSuffix(), ovrduerec1.planSuffix)
		&& isNE(agcmIO.getCompay(), agcmIO.getComern())) {
			updateAgcmFile1100();
		}
		/* Loop round to see if there any other AGCM records to            */
		/* process for the contract.                                       */
		agcmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgcmio1020);
	}

protected void updateAgcmFile1100()
	{
		readh1110();
		rewrt1120();
		writr1130();
	}

	/**
	* <pre>
	* previous processing loop on AGCM is not upset.                  
	* created with commission paid set to earned, valid-flag of "1"   
	* and correct tranno. Also create accounts records (ACMV).        
	* </pre>
	*/
protected void readh1110()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmIO.getAgntnum());
		agcmdbcIO.setLife(agcmIO.getLife());
		agcmdbcIO.setCoverage(agcmIO.getCoverage());
		agcmdbcIO.setRider(agcmIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError5000();
		}
	}

protected void rewrt1120()
	{
		agcmdbcIO.setValidflag("2");
		agcmdbcIO.setCurrto(ovrduerec1.effdate);
		agcmdbcIO.setFunction(varcom.rewrt);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError5000();
		}
	}

protected void writr1130()
	{
		/* correct tranno, and new commission paid.                        */
		compute(wsaaCommRecovered, 2).set(sub(agcmIO.getCompay(), agcmIO.getComern()));
		if (isEQ(agcmIO.getOvrdcat(), "O")) {
			overrideCommAcmv4000();
		}
		else {
			commClawbackAcmv3000();
		}
		agcmdbcIO.setCompay(agcmIO.getComern());
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setTranno(ovrduerec1.tranno);
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writr);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError5000();
		}
	}

protected void readTableTh6051400()
	{
		start1410();
	}

protected void start1410()
	{
		/* Read TH605 to see whether system is "Override based on Agent    */
		/* Details"                                                        */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec1.chdrcoy);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(ovrduerec1.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError5000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void readT5645T16882000()
	{
		para2010();
	}

protected void para2010()
	{
		/* Read T5645 for accounting codes.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ovrduerec1.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError5000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Obtain transaction description from T1688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(ovrduerec1.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(ovrduerec1.trancode);
		descIO.setLanguage(ovrduerec1.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError5000();
		}
		wsaaTransDesc.set(descIO.getLongdesc());
	}

protected void readT56882500()
	{
		read2500();
	}

protected void read2500()
	{
		itdmIO.setItemcoy(ovrduerec1.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(ovrduerec1.cnttype);
		itdmIO.setItmfrm(ovrduerec1.ptdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError5000();
		}
		if (isNE(itdmIO.getItemcoy(), ovrduerec1.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), ovrduerec1.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(ovrduerec1.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			dbError5000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void commClawbackAcmv3000()
	{
		para3010();
	}

protected void para3010()
	{
		if (isEQ(wsaaCommRecovered, 0)) {
			return ;
		}
		/* Write a ACMV for the clawback commission.*/
		lifacmvrec.rdocnum.set(ovrduerec1.chdrnum);
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.batccoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.rldgcoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.batcactyr.set(ovrduerec1.acctyear);
		lifacmvrec.batctrcde.set(ovrduerec1.trancode);
		lifacmvrec.batcactmn.set(ovrduerec1.acctmonth);
		lifacmvrec.batcbatch.set(ovrduerec1.batcbatch);
		lifacmvrec.batcbrn.set(ovrduerec1.batcbrn);
		lifacmvrec.tranno.set(ovrduerec1.tranno);
		lifacmvrec.origcurr.set(ovrduerec1.cntcurr);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec1.ptdate);
		lifacmvrec.tranref.set(ovrduerec1.chdrnum);
		lifacmvrec.user.set(ovrduerec1.user);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE OVRD-AGNTNUM           TO LIFA-RLDGACCT.                */
		/* MOVE AGCMLAP-AGNTNUM        TO LIFA-RLDGACCT.           <007>*/
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		lifacmvrec.transactionDate.set(ovrduerec1.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec1.tranTime);
		lifacmvrec.termid.set(ovrduerec1.termid);
		lifacmvrec.origamt.set(wsaaCommRecovered);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.substituteCode[1].set(ovrduerec1.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError5000();
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmIO.getAnnprem());
			zorlnkrec.effdate.set(agcmIO.getEfdate());
			a100CallZorcompy();
		}
		/* Write a ACMV for the paid commission to be taken off.*/
		/*    MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.                 */
		/*    MOVE T5645-SIGN-02          TO LIFA-GLSIGN.                  */
		/*    MOVE T5645-GLMAP-02         TO LIFA-GLCODE.                  */
		/*    MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.                  */
		/*    MOVE OVRD-CHDRNUM           TO WSAA-RLDG-CHDRNUM.            */
		/*    MOVE OVRD-LIFE              TO WSAA-RLDG-LIFE.               */
		/*    MOVE OVRD-COVERAGE          TO WSAA-RLDG-COVERAGE.           */
		/*    MOVE OVRD-RIDER             TO WSAA-RLDG-RIDER.              */
		/*    MOVE OVRD-PLAN-SUFFIX       TO WSAA-PLANSUFF.                */
		/*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.        */
		/*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.                */
		/*    MOVE OVRD-CNTTYPE           TO LIFA-SUBSTITUTE-CODE(1).      */
		/*    MOVE OVRD-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).      */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			wsaaRldgChdrnum.set(ovrduerec1.chdrnum);
			wsaaRldgLife.set(ovrduerec1.life);
			wsaaRldgCoverage.set(ovrduerec1.coverage);
			wsaaRldgRider.set(ovrduerec1.rider);
			wsaaPlansuff.set(ovrduerec1.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec1.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec1.chdrnum);
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[1].set(ovrduerec1.cnttype);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError5000();
		}
	}

protected void overrideCommAcmv4000()
	{
		para4010();
	}

protected void para4010()
	{
		if (isEQ(wsaaCommRecovered, 0)) {
			return ;
		}
		/* Write a ACMV for the clawback commission.                       */
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rdocnum.set(ovrduerec1.chdrnum);
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.batccoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.rldgcoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec1.chdrcoy);
		lifacmvrec.batcactyr.set(ovrduerec1.acctyear);
		lifacmvrec.batctrcde.set(ovrduerec1.trancode);
		lifacmvrec.batcactmn.set(ovrduerec1.acctmonth);
		lifacmvrec.batcbatch.set(ovrduerec1.batcbatch);
		lifacmvrec.batcbrn.set(ovrduerec1.batcbrn);
		lifacmvrec.tranno.set(ovrduerec1.tranno);
		lifacmvrec.origcurr.set(ovrduerec1.cntcurr);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec1.ptdate);
		lifacmvrec.tranref.set(ovrduerec1.chdrnum);
		lifacmvrec.user.set(ovrduerec1.user);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE OVRD-AGNTNUM           TO LIFA-RLDGACCT.           <002>*/
		/* MOVE AGCMLAP-AGNTNUM        TO LIFA-RLDGACCT.           <007>*/
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		lifacmvrec.transactionDate.set(ovrduerec1.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec1.tranTime);
		lifacmvrec.termid.set(ovrduerec1.termid);
		lifacmvrec.origamt.set(wsaaCommRecovered);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.substituteCode[1].set(ovrduerec1.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError5000();
		}
		/* Write a ACMV for the paid commission to be taken off.           */
		/*    MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP.                 */
		/*    MOVE T5645-SIGN-04          TO LIFA-GLSIGN.                  */
		/*    MOVE T5645-GLMAP-04         TO LIFA-GLCODE.                  */
		/*    MOVE T5645-CNTTOT-04        TO LIFA-CONTOT.                  */
		/*    MOVE OVRD-CHDRNUM           TO WSAA-RLDG-CHDRNUM.            */
		/*    MOVE OVRD-LIFE              TO WSAA-RLDG-LIFE.               */
		/*    MOVE OVRD-COVERAGE          TO WSAA-RLDG-COVERAGE.           */
		/*    MOVE OVRD-RIDER             TO WSAA-RLDG-RIDER.              */
		/*    MOVE OVRD-PLAN-SUFFIX       TO WSAA-PLANSUFF.                */
		/*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.        */
		/*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.                */
		/*    MOVE OVRD-CNTTYPE           TO LIFA-SUBSTITUTE-CODE(1).      */
		/*    MOVE OVRD-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6).      */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			wsaaRldgChdrnum.set(ovrduerec1.chdrnum);
			wsaaRldgLife.set(ovrduerec1.life);
			wsaaRldgCoverage.set(ovrduerec1.coverage);
			wsaaRldgRider.set(ovrduerec1.rider);
			wsaaPlansuff.set(ovrduerec1.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec1.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec1.chdrnum);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec1.cnttype);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			dbError5000();
		}
	}

protected void dbError5000()
	{
		db5000();
		dbExit5090();
	}

protected void db5000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit5090()
	{
		ovrduerec1.statuz.set(varcom.bomb);
		exit090();
	}

protected void loans6000()
	{
		start6000();
	}

	/**
	* <pre>
	*  Process LOANS for this contract. Read all LOAN records for     
	*  this contract and switch off any that are currently of         
	*  valid flag '2'. For any loans 'switched off' in this manner    
	*  set the LAST TRANNO on the loan record to the value            
	*  of the current TRANNO. This will enable any future reversal    
	*  to know which transaction switched off this loan.              
	* </pre>
	*/
protected void start6000()
	{
		/*  BEGN on the LOAN file using COMPANY, CHDRNUM and LOAN NUMBER   */
		/*  of zeroes.                                                     */
		loanenqIO.setDataArea(SPACES);
		loanenqIO.setChdrcoy(ovrduerec1.chdrcoy);
		loanenqIO.setChdrnum(ovrduerec1.chdrnum);
		loanenqIO.setLoanNumber(ZERO);
		/* MOVE BEGNH                      TO LOANENQ-FUNCTION. <LA3993>*/
		loanenqIO.setFunction(varcom.begn);
		


		//performance improvement --  atiwari23 
		loanenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		
		loanenqIO.setFormat(loanenqrec);
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(), varcom.oK)
		&& isNE(loanenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			dbError5000();
		}
		if (isNE(loanenqIO.getChdrcoy(), ovrduerec1.chdrcoy)
		|| isNE(loanenqIO.getChdrnum(), ovrduerec1.chdrnum)) {
			/*     MOVE REWRT                  TO LOANENQ-FUNCTION  <LA3993>*/
			/*     CALL 'LOANENQIO' USING LOANENQ-PARAMS            <LA3993>*/
			/*     IF LOANENQ-STATUZ       NOT = O-K                <LA3993>*/
			/*         MOVE LOANENQ-STATUZ     TO SYSR-STATUZ       <LA3993>*/
			/*     END-IF                                           <LA3993>*/
			loanenqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(loanenqIO.getStatuz(), varcom.endp))) {
			processLoans6200();
		}
		
	}

protected void processLoans6200()
	{
		start6200();
	}

	/**
	* <pre>
	*  If this loan is VALID FLAG '1', set to VALID FLAG '2' and      
	*  set the LAST TRANNO to the current TRANNO to indicate which    
	*  transaction switched off this loan.                            
	*  Read the next loan record.                                     
	* </pre>
	*/
protected void start6200()
	{
		if (isEQ(loanenqIO.getValidflag(), "1")) {
			loanenqIO.setLastTranno(ovrduerec1.tranno);
			loanenqIO.setValidflag("2");
		}
		/*    Rewrite the record (this is necessary even if the record     */
		/*    has not been updated to release it from being held).         */
		/* MOVE REWRT                      TO LOANENQ-FUNCTION  <LA3993>*/
		loanenqIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			dbError5000();
		}
		/*  Read the next loan.                                            */
		loanenqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(), varcom.oK)
		&& isNE(loanenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			dbError5000();
		}
		if (isNE(loanenqIO.getChdrcoy(), ovrduerec1.chdrcoy)
		|| isNE(loanenqIO.getChdrnum(), ovrduerec1.chdrnum)) {
			/*     MOVE REWRT                  TO LOANENQ-FUNCTION  <LA3993>*/
			/*     CALL 'LOANENQIO' USING LOANENQ-PARAMS            <LA3993>*/
			/*     IF LOANENQ-STATUZ       NOT = O-K                <LA3993>*/
			/*         MOVE LOANENQ-STATUZ     TO SYSR-STATUZ       <LA3993>*/
			/*     END-IF                                           <LA3993>*/
			loanenqIO.setStatuz(varcom.endp);
		}
	}
//IBPLIFE- 11192 
protected void updateZrae()
{
	List<Zraepf> zraepfList;
		List<String> chdrnumList = new ArrayList<String>();
		chdrnumList.add(ovrduerec1.chdrnum.toString());
		zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, ovrduerec1.chdrcoy.toString());

		if(zraepfList!= null && !zraepfList.isEmpty()){
			zraepfDAO.updateValidflag(zraepfList);	
		}
		Zraepf Zraepftmp = new Zraepf();
		if (zraepfList != null && !zraepfList.isEmpty()) {
			Zraepftmp = new Zraepf(zraepfList.get(0));
			Zraepftmp.setApcaplamt(new BigDecimal(0));
			Zraepftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setFlag("2");
			Zraepftmp.setValidflag("1");
			Zraepftmp.setTranno(ovrduerec1.tranno.toInt());
			Zraepftmp.setApintamt(BigDecimal.ZERO);
			Zraepftmp.setTotamnt(0);
			zraepfDAO.insertZraeRecord(Zraepftmp);
		}
}

protected void processReassurance7000()
	{
		trmreas7001();
	}

protected void trmreas7001()
	{
		wsaaBatckey.batcBatcpfx.set("BA");
		wsaaBatckey.batcBatccoy.set(ovrduerec1.chdrcoy);
		wsaaBatckey.batcBatcbrn.set(ovrduerec1.batcbrn);
		wsaaBatckey.batcBatcactyr.set(ovrduerec1.acctyear);
		wsaaBatckey.batcBatcactmn.set(ovrduerec1.acctmonth);
		wsaaBatckey.batcBatctrcde.set(ovrduerec1.trancode);
		wsaaBatckey.batcBatcbatch.set(ovrduerec1.batcbatch);
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("TRMR");
		trmreasrec.chdrcoy.set(ovrduerec1.chdrcoy);
		trmreasrec.chdrnum.set(ovrduerec1.chdrnum);
		trmreasrec.life.set(ovrduerec1.life);
		trmreasrec.coverage.set(ovrduerec1.coverage);
		trmreasrec.rider.set(ovrduerec1.rider);
		trmreasrec.planSuffix.set(ovrduerec1.planSuffix);
		trmreasrec.cnttype.set(ovrduerec1.cnttype);
		trmreasrec.crtable.set(ovrduerec1.crtable);
		trmreasrec.polsum.set(ovrduerec1.polsum);
		trmreasrec.effdate.set(ovrduerec1.ptdate);
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(ovrduerec1.tranno);
		trmreasrec.language.set(ovrduerec1.language);
		trmreasrec.billfreq.set(ovrduerec1.billfreq);
		trmreasrec.ptdate.set(ovrduerec1.ptdate);
		trmreasrec.origcurr.set(ovrduerec1.cntcurr);
		trmreasrec.acctcurr.set(ovrduerec1.cntcurr);
		trmreasrec.crrcd.set(ovrduerec1.crrcd);
		trmreasrec.convUnits.set(ZERO);
		trmreasrec.jlife.set(wsaaJlife);
		trmreasrec.singp.set(ovrduerec1.instprem);
		trmreasrec.oldSumins.set(ovrduerec1.sumins);
		trmreasrec.pstatcode.set(ovrduerec1.pstatcode);
		trmreasrec.clmPercent.set(ZERO);
		trmreasrec.newSumins.set(ZERO);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			dbError5000();
		}
	}

protected void a100CallZorcompy()
	{
		a110Start();
	}

protected void a110Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(ovrduerec1.ptdate);
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(ovrduerec1.tranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set("Y");
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			dbError5000();
		}
	}
}
