/*
 * File: Pr684.java
 * Date: 30 August 2009 1:55:32
 * Author: Quipoz Limited
 *
 * Class transformed from PR684.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.FluprgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclaanlTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabanTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabcaTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabltTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclacapTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclaltlTableDAM;
import com.csc.life.terminationclaims.dataaccess.HcldTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclhTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.screens.Sr684ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.T6617rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  (Note: This program is actually cloned from P6679 for Hospital
*         Benefit use (HBNF))
*  PR684 - Regular Benefit Claims Approval.
*  ----------------------------------------
*
*  Overview.
*  ---------
*
*  This  program  will  be  used  to  approve  Regular  Benefit
*  Claims.  It will be invoked by the Regular Payments sub-menu
*  which will check that the claim is in a suitable  state  for
*  approval.  Therefore  control will not be passed here unless
*  the transaction may be carried out  on  the  selected  claim
*  record.
*
*  The  only  data field that may be entered by the user is tha
*  Date of Approval. This will  be  defaulted  to  the  current
*  date but may be overridden subject to certain criteria.
*
*  The  only  other fields on the screen accessible to the user
*  will be the Follow Ups and Bank Details indicators.
*
*
*  Initialise.
*  -----------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  Initialise  all  relevant  variables  and prepare the screen
*  for display.
*
*  Perform a RETRV on the CHDRRGP and REGP files.
*
*  Use CHDRRGP to display the heading  details  on  the  screen
*  using  T5688  for  the  contract  type  description  and the
*  Client  file  for  the  relevant  client  names.  The  short
*  descriptions  for  the  contract's  risk  and premium status
*  codes should be  obtained  from  T3623,  (Risk  Status)  and
*  T3588, (Premium Status).
*
*  Display  the  REGP  details  on the screen looking up all of
*  the descriptions from DESC where appropriate.
*
*  Set the Approval Date to today's date.
*
*  If there are Follow Ups  in  existence  set  a  '+'  in  the
*  Follow  Ups  indicator  field.  This  can  be  determined by
*  reading FLUPRGP with a key of CHDRCOY,  CHDRNUM,  a  CLAMNUM
*  of  '00000000'  and a function of BEGN. If a record is found
*  that matches on Company, Contract Number  and  Claim  Number
*  then there are Follow Ups for the claim.
*
*  You  must  check  if there are any follow ups for this claim
*  that have a status that is not recognised  as  a  'Complete'
*  status.  If  there  are  any  then  an error message must be
*  supplied and the approval  will  not  proceed.  To  do  this
*  process  all  of  the  FLUPRGP  records  that  match  on the
*  Company, Contract Number  and  Claim  Number  until  end  of
*  file,  change  of key or one is found that has an incomplete
*  status. For each Follow Up record read  use  the  Follow  Up
*  Code  to  read  T5661  and  compare the status code from the
*  Follow Up record  with  the  'Complete'  status  codes  from
*  T5661.  If it matches one of the T5661 'Complete' codes then
*  it is OK to proceed to the next Follow Up record.
*
*  If the bank details on REGP are non-blank set a '+'  in  the
*  Bank Details indicator field.
*
*
*  Display and Validation. (2000 Section).
*  ---------------------------------------
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  Converse with the screen using the I/O module.
*
*  If 'KILL' has been pressed skip this section.
*
*  If 'CALC' has been pressed set WSSP-EDTERROR to 'Y'.
*
*  If there are any outstanding Follow Ups, (determined in  the
*  1000 section), then set an error message.
*
*  Approval  Date:  The  Approval  Date  will  have been set to
*  today's date but it may be overwritten by  the  user.  Check
*  that it is not less than the Registration date.
*
*  .  Follow Ups Indicator: This may only be 'X', '+' or space.
*  If it is 'X' check  that  there  are  Follow  Ups  to  view,
*  (determined  in  the  1000  section).  If there are not then
*  give an error message.
*
*  . Bank Details Indicator: This  may  only  be  'X',  '+'  or
*  space.  If  it  is  'X' check that there are Bank Details to
*  view. If there are not display an error message.
*
*  Updating.
*  ---------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  If 'KILL' has been pressed skip this section.
*
*  If  either  the  Follow  Ups  indicator  or the Bank Details
*  indicator are 'X' then perform a KEEPS on the REGP file  and
*  skip the updating.
*
*
*  Otherwise  the  update  proper is to be performed. This will
*  be as follows:
*
*       Read  table  T6693  with  the  current  Payment  Status
*       concatenated   with   the  CRTABLE  of  the  associated
*       component. If the item is not  found  read  again  with
*       the  Payment Status set to '**'. Locate the Transaction
*       Code of the transaction currently being  processed  and
*       select the corresponding Next Payment Status.
*
*       A  history must be kept of the Regular Payment Details.
*       Set the Validflag to '2' and perform an UPDAT on REGP.
*
*       Rad  the  CHDRRGP  record  with  READH,  increment  the
*       TRANNO  and re-write it. Use the new TRANNO to create a
*       new REGP record  with  a  Validflag  of  '1'.  Set  the
*       Payment  Status  to the appropriate Next Payment Status
*       from T6693.
*
*       Write a new REGP record.
*
*       Call Softlock to unlock the contract.
*
*
*  Where Next.
*  -----------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN  =  '*',  then  re-load the next 8 programs in
*  the stack from Working Storage.
*
*  If returning  from  the  Follow  Ups  path  the  Follow  Ups
*  indicator  will  be  '?'. If so set the Follow Ups indicator
*  field to '+'.
*
*  If returning from the Bank Details  path  the  Bank  Details
*  indicator  will be '?'. If so set the Bank Details indicator
*  field to '+'.
*
*  If the Follow Ups  indicator  is  'X'  then  switch  to  the
*  Follow Ups path as follows:
*
*       Store  the  next  8  programs  in  the stack in Working
*       Storage.
*
*       Call GENSSW with an action of 'A'.
*
*       If there is an error code returned from GENSSW  use  it
*       as  an  error  code  on the Follow Ups indicator field,
*       set WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG  to
*       SCRN-SCRNAME and go to exit.
*
*       Load  the  8  programs  returned  from GENSSW in to the
*       next 8 positions in the program stack.
*
*       Set the Follow Ups indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*
*  If the Bank Details indicator is  'X'  then  switch  to  the
*  Bank Details path as follows:
*
*       Store  the  next  8  programs  in  the stack in Working
*       Storage.
*
*       Call GENSSW with an action of 'B'.
*
*       If there is an error code returned from GENSSW  use  it
*       as  an  error code on the Bank Details indicator field,
*       set WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG  to
*       SCRN-SCRNAME and go to exit.
*
*       Load  the  8  programs  returned  from GENSSW in to the
*       next 8 positions in the program stack.
*
*       Set the Bank Details indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN  =  '*',  then move space to WSSP-SEC-ACTN and
*  cause the program to redisplay  from  the  2000  section  by
*  setting WSSP-NEXTPROG to SCRN-SCRNAME and go to exit.
*
*  Add 1 to the program pointer and exit.
*
*  Notes.
*  ------
*
*  Tables Used.
*  ------------
*
*  . T3000 - Currency Code Details
*            Key: CURRCD
*
*  . T3588 - Contract Premium Status Codes
*            Key: PSTCDE from CHDRRGP
*
*  . T3623 - Contract Risk Status Codes
*            Key: STATCODE from CHDRRGP
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T5400 - Regular Payment Status
*            Key: Regular Payment Status Code
*
*  . T6692 - Regular Payment Reason Codes
*            Key: Regular Payment Reason Code
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*  . T6694 - Regular Payment Method of Payment
*            Key: Regular Payment MOP
*
******************Enhancements for Life Asia 1.0****************
*
* This module has been enhanced to cater for waiver of premium
* (WOP) processing. The waiver of premium sum assured amount is
* based on the accumulation of all the premiums of the
* components on the proposal and the policy fee as specified on
* table TR517. The Following additional processing has been
* introduced :
*
* - For the component being processed access TR517. If the item
*   is present, then we are processing a WOP component.
*
* - Obtain the PAYR record for this contract. The reason for this
*   is that the frequency of payment is the payment frequency
*   of the contract because the waiver is really the premium
*   instalment of the contract and is only payable on the date
*   present on the PAYR record.
*
* - If this is waiver of premium component, then reduce the
*   instalment amounts on the CHDR and PAYR by the WOP premium.
*   This will ensure that during the renewal run, the payment
*   amount is what the contract expects. But first check whether
*   the WOP premium should be subtracted. For example if a
*   payment registration has already happened, then we don't
*   want to take of the WOP premium again.
*
* - Recalculate the premium instalment on the CHDR and PAYR
*   records.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr684 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR684");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaFlupOut = "";
	private String wsaaFlupExsist = "";
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPremCurrency = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private PackedDecimalData wsaaCrrcd = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaWopInstprem = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaCovrInstprem = new PackedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData index1 = new ZonedDecimalData(2, 0).setUnsigned();
	protected PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	protected PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private String firstTime = "";
	private FixedLengthStringData wsaaRgpystat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaEdtitm = new FixedLengthStringData(5);
	private ZonedDecimalData wsaaBenfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaRegpayfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	private String wsaaValidStatuz = "";
	private String wsaaStopProcess = "";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaSinstamt05 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinstamt06 = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaWopFlag = new FixedLengthStringData(1);
	private Validator wopMop = new Validator(wsaaWopFlag, "Y");

	private FixedLengthStringData wsaaT5606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5606Edtitm = new FixedLengthStringData(5).isAPartOf(wsaaT5606Key, 0);
	private FixedLengthStringData wsaaT5606Currcd = new FixedLengthStringData(3).isAPartOf(wsaaT5606Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);

	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Rgpystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);

	private FixedLengthStringData wsaaClamnum2 = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaClamnumFill = new ZonedDecimalData(3, 0).isAPartOf(wsaaClamnum2, 0).setUnsigned();
	private ZonedDecimalData wsaaRgpynum = new ZonedDecimalData(5, 0).isAPartOf(wsaaClamnum2, 3).setUnsigned();
	private String wsaaIsWop = "N";
	private FixedLengthStringData wsaaPayrBillfreq = new FixedLengthStringData(2).init(SPACES);
	private ZonedDecimalData wsaaCompYear = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT7508Key, 4);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprgpTableDAM fluprgpIO = new FluprgpTableDAM();
	private HclaanlTableDAM hclaanlIO = new HclaanlTableDAM();
	private HclabanTableDAM hclabanIO = new HclabanTableDAM();
	private HclabcaTableDAM hclabcaIO = new HclabcaTableDAM();
	private HclabltTableDAM hclabltIO = new HclabltTableDAM();
	private HclacapTableDAM hclacapIO = new HclacapTableDAM();
	private HclaltlTableDAM hclaltlIO = new HclaltlTableDAM();
	private HcldTableDAM hcldIO = new HcldTableDAM();
	protected HclhTableDAM hclhIO = new HclhTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected RegpTableDAM regpIO = new RegpTableDAM();
	protected Gensswrec gensswrec = new Gensswrec();
	protected Batckey wsaaBatckey = new Batckey();
	protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Sftlockrec sftlockrec = new Sftlockrec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5606rec t5606rec = new T5606rec();
	private T5645rec t5645rec = new T5645rec();
	private T5661rec t5661rec = new T5661rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T6693rec t6693rec = new T6693rec();
	private T6694rec t6694rec = new T6694rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T7508rec t7508rec = new T7508rec();
	private Wssplife wssplife = new Wssplife();
	protected Sr684ScreenVars sv = getLScreenVars();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private ErrorsInner errorsInner = new ErrorsInner();
	protected FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();

	// CMRPY005Permission
	boolean CMRPY005Permission = false;
	
	/*
	 * CML004
	 */
	private boolean cml004Permission;
	private T6617rec t6617rec = new T6617rec();
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO",  FluppfDAO.class);
	private ZonedDecimalData wsaaItstdays = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaItstrate = new ZonedDecimalData(8, 5);
	private static final String FEATUREID_DEFERRED_INTEREST_CALCULATION = "CMOTH002";
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	protected Sr684ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sr684ScreenVars.class);
		}
	/*
	 * CML008 & CLM023
	 */
	private boolean cml008Permission = false ;
	private static final String FEATUREID_CLAIM_NOTES = "CMOTH003";
	private Notipf notipf=null;
	public static final String CLMPREFIX = "CLMNTF";
	private List<Clnnpf> clnnpfList1 = new ArrayList<Clnnpf>();
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
	private List<Invspf> invspfList1 = new ArrayList<Invspf>();
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	
		
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readRegp1060,
		exit1090,
		exit2090,
		exit3090,
		hcldio3620,
		exit3690,
		life3750,
		amtAccum3780,
		benLife3850,
		benAmtAccum3880,
		popUp4050,
		gensww4010,
		nextProgram4020,
		exit4090,
		callCovrio8110,
		calculateWopInst8310,
		h910CallCovrio,
		h990Exit,
		investResult,
		claimNotes
	}

	public Pr684() {
		super();
		screenVars = sv;
		new ScreenModel("Sr684", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case readRegp1060:
					readRegp1060();
					processInterest();//CML004
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		// CML009
		CMRPY005Permission = FeaConfg.isFeatureExist("2", "CMRPY005", appVars,
				"IT");
		cml004Permission = FeaConfg.isFeatureExist("2", FEATUREID_DEFERRED_INTEREST_CALCULATION, appVars, "IT");//CML004
		cml008Permission = FeaConfg.isFeatureExist("2", FEATUREID_CLAIM_NOTES, appVars, "IT");//CML008

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		firstTime = "Y";
		wsaaStopProcess = "N";
		sv.anvdate.set(varcom.vrcmMaxDate);
		sv.aprvdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.finalPaydate.set(varcom.vrcmMaxDate);
		sv.firstPaydate.set(varcom.vrcmMaxDate);
		sv.lastPaydate.set(varcom.vrcmMaxDate);
		sv.nextPaydate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.incurdt.set(varcom.vrcmMaxDate);
		sv.recvdDate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.revdte.set(varcom.vrcmMaxDate);
		wsaaPayrseqno.set(ZERO);
		wsaaCompYear.set(ZERO);
		sv.pymt.set(ZERO);
		sv.totamnt.set(ZERO);
		wsaaSumins.set(ZERO);
		wsaaWopInstprem.set(ZERO);
		/* Find todays date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, "****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/*  Retrieve the Contract Header.*/
		chdrrgpIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/*  Release the contract Header.*/
		chdrrgpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/*  Read the contract type description.*/
		descIO.setDescitem(chdrrgpIO.getCnttype());
		descIO.setDesctabl(tablesInner.t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		/* Read the contract currency description*/
		descIO.setDescitem(chdrrgpIO.getCntcurr());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.currds.set(descIO.getShortdesc());
		}
		else {
			sv.currds.fill("?");
		}
		/* Read the Premium status description.*/
		descIO.setDescitem(chdrrgpIO.getPstatcode());
		descIO.setDesctabl(tablesInner.t3588);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getLongdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/* Read the risk status description.*/
		descIO.setDescitem(chdrrgpIO.getStatcode());
		descIO.setDesctabl(tablesInner.t3623);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getLongdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/* Read owner details*/
		cltsIO.setClntnum(chdrrgpIO.getCownnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(), 1))) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		/*    Set screen fields*/
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		sv.cownnum.set(chdrrgpIO.getCownnum());
		sv.occdate.set(chdrrgpIO.getOccdate());
		sv.ptdate.set(chdrrgpIO.getPtdate());
		sv.btdate.set(chdrrgpIO.getBtdate());
		sv.currcd.set(chdrrgpIO.getCntcurr());
		/* Retrieve the Regular Payment record.*/
		regpIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		if (CMRPY005Permission) {
			// CML009 retrv
			sv.adjustamt.set(regpIO.getAdjamt());
			sv.netclaimamt.set(regpIO.getNetamt());
			sv.reasoncd.set(regpIO.getReasoncd());
			sv.resndesc.set(regpIO.getReason());
		}
		/* Release the Regular Payment record.*/
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Set the Cmpnt (Component) field  - SR684-CRTABLE from the*/
		/* information in the REGP record*/
		sv.crtable.set(regpIO.getCrtable());
		/* Read Hospital Benefit file (HCLH) for maximum claim amount      */
		/*    PERFORM 1750-READ-HPCL.                                      */
		readHclh1750();
		/* Check if there are any FOLLOW-UPS on the current contract.*/
		fluprgpIO.setChdrcoy(wsspcomn.company);
		fluprgpIO.setChdrnum(chdrrgpIO.getChdrnum());
		wsaaRgpynum.set(regpIO.getRgpynum());
		wsaaClamnumFill.set(ZERO);
		fluprgpIO.setClamnum(wsaaClamnum2);
		fluprgpIO.setFupno(0);
		wsaaFlupExsist = "N";
		fluprgpIO.setStatuz(varcom.oK);
		fluprgpIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		fluprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluprgpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","CLAMNUM");

		wsaaFlupOut = "Y";
		while ( !(isEQ(fluprgpIO.getStatuz(), varcom.endp))) {
			checkStatusFlup5100();
		}

		if (isEQ(wsaaFlupOut, "Y")) {
			sv.fupflgErr.set(errorsInner.h017);
			wsspcomn.edterror.set("Y");
		}
		a100CheckHclhDetails();
		/* Accumulate the total sum insured within the plan.*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		covrIO.setChdrnum(chdrrgpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
			accumCovr1600();
		}

		if (isEQ(wsaaPayrseqno, 0)) {
			wsaaPayrseqno.set(1);
		}
		/*  Read T5645 to obtain the WOP payment method's sacscode,*/
		/*  sacstype and GLMAP.*/
		readT56451800();
		/* Recalculate the Sum Assured if the REGP frequency and Billing*/
		/*  frequency differ. Read T5671 to obtain the Edit item to read*/
		/*  T5606. Read T5606 to obtain the Benefit Billing frequency*/
		readT56711700();
		wsaaT5606Edtitm.set(wsaaEdtitm);
		wsaaT5606Currcd.set(chdrrgpIO.getCntcurr());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT5606Key);
		itdmIO.setItmfrm(wsaaCrrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t5606, itdmIO.getItemtabl()))
		|| (isNE(wsaaT5606Key, itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setGenarea(SPACES);
		}
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
		if (isEQ(wsaaIsWop, "N")) {
			if ((isNE(t5606rec.benfreq, regpIO.getRegpayfreq()))
			&& (isNE(t5606rec.benfreq, SPACES))) {
				wsaaBenfreq.set(t5606rec.benfreq);
				wsaaRegpayfreq.set(regpIO.getRegpayfreq());
				compute(wsaaSumins, 2).set((div((mult(wsaaSumins, wsaaBenfreq)), wsaaRegpayfreq)));
			}
		}
		else {
			if ((isNE(wsaaPayrBillfreq, regpIO.getRegpayfreq()))
			&& (isNE(wsaaPayrBillfreq, SPACES))) {
				wsaaBenfreq.set(wsaaPayrBillfreq);
				wsaaRegpayfreq.set(regpIO.getRegpayfreq());
				compute(wsaaSumins, 2).set((div((mult(wsaaSumins, wsaaBenfreq)), wsaaRegpayfreq)));
			}
		}
		covrIO.setLife(wsaaLife);
		covrIO.setJlife(wsaaJlife);
		covrIO.setPremCurrency(wsaaPremCurrency);
		covrIO.setCrtable(wsaaCrtable);
		/* Read Life Details*/
		lifeenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrrgpIO.getChdrnum());
		lifeenqIO.setLife(covrIO.getLife());
		lifeenqIO.setJlife(covrIO.getJlife());
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");

		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)
		&& isNE(lifeenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if (isNE(chdrrgpIO.getChdrcoy(), lifeenqIO.getChdrcoy())
		|| isNE(chdrrgpIO.getChdrnum(), lifeenqIO.getChdrnum())
		|| isNE(covrIO.getLife(), lifeenqIO.getLife())) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
			goTo(GotoLabel.readRegp1060);
		}
		sv.lifcnum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(), 1))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		
		if(cml008Permission)
		{
		sv.cmoth008flag.set("Y");
		sv.claimnumber.set(regpIO.getClaimno());
			if(isEQ(regpIO.getClaimnotifino(),SPACES)){
				sv.aacct.set(SPACES);
			}
			else{
				sv.aacct.set(CLMPREFIX+regpIO.getClaimnotifino());
			}
		}
		else
		{
		sv.cmoth008flag.set("N");
		}
	}
protected void a100CheckHclhDetails() { }
protected void readRegp1060()
	{
		readRegpDetails1300();
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(), varcom.oK))
		&& (isNE(cltsIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readRegpDetails1300()
	{
		read1310();
	}

protected void read1310()
	{
		/* Read Payment type details.*/
		descIO.setDescitem(regpIO.getRgpytype());
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		}
		else {
			sv.rgpytypesd.fill("?");
		}
		/* Read Payee details*/
		if (isNE(regpIO.getPayclt(), SPACES)) {
			cltsIO.setClntnum(regpIO.getPayclt());
			getClientDetails1200();
			if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
			|| isNE(cltsIO.getValidflag(), 1)) {
				sv.payenmeErr.set(errorsInner.e335);
				sv.payenme.set(SPACES);
			}
			else {
				plainname();
				sv.payenme.set(wsspcomn.longconfname);
			}
		}
		/*  Read the claim status description.*/
		descIO.setDescitem(regpIO.getRgpystat());
		descIO.setDesctabl(tablesInner.t5400);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.statdsc.set(descIO.getShortdesc());
		}
		else {
			sv.statdsc.fill("?");
		}
		/*  Read the reason code description*/
		descIO.setDescitem(regpIO.getPayreason());
		descIO.setDesctabl(tablesInner.t6692);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmdesc.set(descIO.getLongdesc());
		}
		else {
			if (isNE(regpIO.getPayreason(), SPACES)) {
				sv.clmdesc.fill("?");
			}
			else {
				sv.clmdesc.set(SPACES);
			}
		}
		/*  Read the payemnt method description.*/
		descIO.setDescitem(regpIO.getRgpymop());
		descIO.setDesctabl(tablesInner.t6694);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpyshort.set(descIO.getShortdesc());
		}
		else {
			sv.rgpyshort.fill("?");
		}
		/* Read the frequency code description.*/
		descIO.setDescitem(regpIO.getRegpayfreq());
		descIO.setDesctabl(tablesInner.t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
		}
		else {
			sv.frqdesc.fill("?");
		}
		/*  Read the Claim Currency Description.*/
		descIO.setDescitem(regpIO.getCurrcd());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill("?");
		}
		/* Set up the Screen.*/
		sv.payclt.set(regpIO.getPayclt());
		sv.rgpynum.set(regpIO.getRgpynum());
		sv.rgpystat.set(regpIO.getRgpystat());
		sv.cltype.set(regpIO.getPayreason());
		sv.claimevd.set(regpIO.getClaimevd());
		if (CMRPY005Permission) {
			// TODO CML009 display the net claim amount and adjustment amount
			sv.adjustamt.set(regpIO.getAdjamt());
			sv.adjustamtOut[varcom.pr.toInt()].set("Y");
			sv.netclaimamt.set(regpIO.getNetamt());
			sv.netclaimamtOut[varcom.pr.toInt()].set("Y");
			sv.reasoncd.set(regpIO.getReasoncd());
			sv.reasoncdOut[varcom.pr.toInt()].set("Y");
			sv.resndesc.set(regpIO.getReason());
			sv.resndescOut[varcom.pr.toInt()].set("Y");
		}
		sv.rgpymop.set(regpIO.getRgpymop());
		sv.regpayfreq.set(regpIO.getRegpayfreq());
		sv.clamparty.set(regpIO.getClamparty());
		sv.destkey.set(regpIO.getDestkey());
		sv.pymt.set(regpIO.getPymt());
		sv.totamnt.set(regpIO.getTotamnt());
		sv.claimcur.set(regpIO.getCurrcd());
		sv.crtdate.set(regpIO.getCrtdate());
		sv.revdte.set(regpIO.getRevdte());
		sv.firstPaydate.set(regpIO.getFirstPaydate());
		sv.lastPaydate.set(regpIO.getLastPaydate());
		sv.nextPaydate.set(regpIO.getNextPaydate());
		sv.anvdate.set(regpIO.getAnvdate());
		sv.finalPaydate.set(regpIO.getFinalPaydate());
		sv.aprvdate.set(datcon1rec.intDate);
		/* Check if there are any BANK DETAILS on the current contract*/
		if ((isEQ(regpIO.getBankkey(), SPACES))
		&& (isEQ(regpIO.getBankacckey(), SPACES))) {
			sv.ddind.set(SPACES);
		}
		else {
			sv.ddind.set("+");
		}
	}

protected void accumCovr1600()
	{
		accum1610();
	}

protected void accum1610()
	{
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(), varcom.oK))
		&& (isNE(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(), covrIO.getChdrcoy()))
		|| (isNE(chdrrgpIO.getChdrnum(), covrIO.getChdrnum()))
		|| (isNE(regpIO.getLife(), covrIO.getLife()))
		|| (isNE(regpIO.getCoverage(), covrIO.getCoverage()))
		|| (isNE(regpIO.getRider(), covrIO.getRider()))
		|| (isEQ(covrIO.getStatuz(), varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			covrIO.setFunction(varcom.nextr);
			return ;
		}
		if (isEQ(regpIO.getCoverage(), covrIO.getCoverage())) {
			if (isGT(covrIO.getPayrseqno(), 0)) {
				wsaaPayrseqno.set(covrIO.getPayrseqno());
			}
			else {
				wsaaPayrseqno.set(1);
			}
		}
		wsaaLife.set(covrIO.getLife());
		wsaaJlife.set(covrIO.getJlife());
		wsaaPremCurrency.set(covrIO.getPremCurrency());
		wsaaCrtable.set(covrIO.getCrtable());
		wsaaCrrcd.set(covrIO.getCrrcd());
		wsaaSumins.add(covrIO.getSumins());
		covrIO.setFunction(varcom.nextr);
		/* Check coverage statii against T5679*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatuz = "N";
		wsaaStatcode.set(covrIO.getStatcode());
		wsaaPstcde.set(covrIO.getPstatcode());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
				for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
				|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
						wsaaValidStatuz = "Y";
					}
				}
			}
		}
		if (isEQ(wsaaValidStatuz, "N")) {
			wsaaStopProcess = "Y";
		}
		readTr5176000();
		readPayr7000();
		if (isEQ(wsaaIsWop, "Y")) {
			wsaaWopInstprem.add(covrIO.getInstprem());
		}
	}

protected void readT56711700()
	{
		read1710();
	}

protected void read1710()
	{
		/* Set up the key for T5671*/
		wsaaT5671Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5671Crtable.set(wsaaCrtable);
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		index1.set(1);
		while ( !((isGT(index1, 4))
		|| (isEQ(wsaaProg, t5671rec.pgm[index1.toInt()])))) {
			if (isNE(t5671rec.pgm[index1.toInt()], wsaaProg)) {
				index1.add(1);
			}
		}

		if (isEQ(t5671rec.pgm[index1.toInt()], wsaaProg)) {
			wsaaEdtitm.set(t5671rec.edtitm[index1.toInt()]);
		}
	}

	/**
	* <pre>
	*1750-READ-HPCL SECTION.
	* </pre>
	*/
protected void readHclh1750()
	{
		start1751();
	}

protected void start1751()
	{
		/*    MOVE REGP-CHDRCOY           TO HPCL-CHDRCOY.                 */
		/*    MOVE REGP-CHDRNUM           TO HPCL-CHDRNUM.                 */
		/*    MOVE REGP-LIFE              TO HPCL-LIFE.                    */
		/*    MOVE REGP-COVERAGE          TO HPCL-COVERAGE.                */
		/*    MOVE REGP-RIDER             TO HPCL-RIDER.                   */
		/*    MOVE REGP-RGPYNUM           TO HPCL-RGPYNUM.                 */
		/*    MOVE READR                  TO HPCL-FUNCTION.                */
		/*    MOVE HPCLREC                TO HPCL-FORMAT.                  */
		/*    CALL 'HPCLIO'            USING HPCL-PARAMS.                  */
		/*    IF   HPCL-STATUZ           NOT = O-K AND MRNF                */
		/*         MOVE HPCL-PARAMS       TO SYSR-PARAMS                   */
		/*         PERFORM 600-FATAL-ERROR.                                */
		/*    IF   HPCL-STATUZ               = O-K                         */
		/*         MOVE HPCL-CLAIMTOT     TO SR684-MAXCLMAMT               */
		/*         COMPUTE SR684-MAXCLMAMT = SR684-MAXCLMAMT       <V71L09>*/
		/*                                 * HPCL-PREMUNIT         <V71L09>*/
		/*    ELSE                                                         */
		/*         MOVE ZEROES            TO SR684-MAXCLMAMT               */
		/*    END-IF.                                                      */
		hclhIO.setChdrcoy(regpIO.getChdrcoy());
		hclhIO.setChdrnum(regpIO.getChdrnum());
		hclhIO.setLife(regpIO.getLife());
		hclhIO.setCoverage(regpIO.getCoverage());
		hclhIO.setRider(regpIO.getRider());
		hclhIO.setRgpynum(regpIO.getRgpynum());
		hclhIO.setFunction(varcom.readr);
		hclhIO.setFormat(formatsInner.hclhrec);
		SmartFileCode.execute(appVars, hclhIO);
		if (isNE(hclhIO.getStatuz(), varcom.oK)
		&& isNE(hclhIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hclhIO.getParams());
			fatalError600();
		}
		if (isEQ(hclhIO.getStatuz(), varcom.oK)) {
			sv.recvdDate.set(hclhIO.getRecvdDate());
			sv.incurdt.set(hclhIO.getIncurdt());
		}
	}

protected void readT56451800()
	{
		start1810();
	}

protected void start1810()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(errorsInner.h134);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/* Check if the Next Payment Date = Contract Pay to Date*/
		if ((isGT(regpIO.getNextPaydate(), payrIO.getBillcd()))
		&& (isEQ(regpIO.getDestkey(), regpIO.getChdrnum()))) {
			sv.npaydateErr.set(errorsInner.h133);
			wsspcomn.edterror.set("Y");
		}
		/*  If invalid coverage status is discovered, output error message*/
		/*   and protect the screen.*/
		if (isEQ(wsaaStopProcess, "Y")) {
			scrnparams.errorCode.set(errorsInner.h136);
			scrnparams.function.set(varcom.prot);
			wsspcomn.edterror.set("Y");
		}
		// CML009
		if (CMRPY005Permission) {
			sv.adjustamtOut[varcom.nd.toInt()].set("N");
			sv.netclaimamtOut[varcom.nd.toInt()].set("N");
			sv.reasoncdOut[varcom.nd.toInt()].set("N");
			sv.resndescOut[varcom.nd.toInt()].set("N");
			sv.pymtAdjOut[varcom.nd.toInt()].set("N");
		} else {
			sv.adjustamtOut[varcom.nd.toInt()].set("Y");
			sv.netclaimamtOut[varcom.nd.toInt()].set("Y");
			sv.reasoncdOut[varcom.nd.toInt()].set("Y");
			sv.resndescOut[varcom.nd.toInt()].set("Y");
			sv.pymtAdjOut[varcom.nd.toInt()].set("Y");
		}
		/*
		 * CML004
		 */
		if(cml004Permission) {
			sv.cmoth002flag.set("Y");
		}
		
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
			processInterest();//CML004
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		/* If Stop Processing flag has been set by an invalid coverage stat*/
		/*  protect the entire screen AND ALLOW NO FURTHER PROCESSING*/
		if (isEQ(wsaaStopProcess, "Y")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*    Validate fields*/
		if ((isNE(scrnparams.statuz, varcom.calc))
		&& (isNE(scrnparams.statuz, varcom.oK))) {
			scrnparams.errorCode.set(errorsInner.curs);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if ((isEQ(wsaaFlupOut, "Y"))
		&& (isNE(sv.fupflg, "X"))
		&& (isNE(sv.ddind, "X"))) {
			sv.fupflgErr.set(errorsInner.h017);
		}
		/* Validate Approval Date*/
		if ((isEQ(sv.aprvdate, SPACES))
		|| (isEQ(sv.aprvdate, varcom.vrcmMaxDate))) {
			sv.aprvdateErr.set(errorsInner.e186);
		}
		if (isLT(sv.aprvdate, sv.crtdate)) {
			sv.aprvdateErr.set(errorsInner.g523);
		}
		/* Validate FOLLOW-UP Indicator.*/
		if ((isNE(sv.fupflg, "+"))
		&& (isNE(sv.fupflg, "X"))
		&& (isNE(sv.fupflg, SPACES))) {
			sv.fupflgErr.set(errorsInner.h118);
		}
		/* Validate BANK DETAILS Indicator.*/
		if ((isNE(sv.ddind, "+"))
		&& (isNE(sv.ddind, "X"))
		&& (isNE(sv.ddind, SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
		}
		
		validateHealthClaimIndicator();
		/* Check if there are FOLLOW-UP DETAILS to show.*/
		if ((isEQ(sv.fupflg, "X"))
		&& (isEQ(sv.fupflgErr, SPACES))
		&& (isEQ(wsaaFlupExsist, "N"))) {
			sv.fupflgErr.set(errorsInner.g524);
		}
		/* Check if there are BANK DETAILS to show.*/
		if ((isEQ(sv.ddind, "X"))
		&& (isEQ(sv.ddindErr, SPACES))
		&& (isEQ(regpIO.getBankkey(), SPACES))) {
			sv.ddindErr.set(errorsInner.e493);
		}
		/* Check if the Next Payment Date = Contract Pay to Date*/
		if ((isGT(regpIO.getNextPaydate(), payrIO.getBillcd()))
		&& (isEQ(regpIO.getDestkey(), regpIO.getChdrnum()))) {
			sv.npaydateErr.set(errorsInner.h133);
		}
		/* Check the new Sinstamt on the contract header*/
		chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
		chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
		chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
		chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		/*    MOVE BEGNH                  TO CHDRLIF-FUNCTION              */
		chdrlifIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/* Check if "WOP" method is selected, perform the Sinstamt*/
		/* validation. Otherwise skip this validation.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
		|| (isNE(sv.rgpymop, itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setGenarea(SPACES);
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		wsaaWopFlag.set(SPACES);
		if (isEQ(t6694rec.sacscode, t5645rec.sacscode01)
		&& isEQ(t6694rec.sacstype, t5645rec.sacstype01)) {
			wopMop.setTrue();
		}
		if ((isNE(regpIO.getDestkey(), SPACES))
		&& isEQ(regpIO.getDestkey(), chdrrgpIO.getChdrnum())
		&& wopMop.isTrue()) {
			compute(wsaaSinstamt05, 2).set(add((mult(regpIO.getPymt(), -1)), chdrlifIO.getSinstamt05()));
			compute(wsaaSinstamt06, 2).set((add(add(add(add(chdrlifIO.getSinstamt01(), chdrlifIO.getSinstamt02()), chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()), wsaaSinstamt05)));
			if (isLT(wsaaSinstamt06, ZERO)) {
				scrnparams.errorCode.set(errorsInner.h138);
				wsspcomn.edterror.set("Y");
			}
		}
	}

	protected void validateHealthClaimIndicator() {
	}


	/**
	 * @author fwang3
	 * Process Interest
	 */
	private void processInterest() {
		if (cml004Permission && (isEQ(wsaaFlupExsist, "N")||isEQ(wsaaFlupOut,"N"))) {
			loadItem(); // read T6617 days
			if(isEQ(sv.itstdays, wsaaItstdays)) {
				setInterestDays();
			} 
			if(isEQ(sv.itstrate, wsaaItstrate)) {
				setInterestRate();
			} 
			setInterestAmount();
			setNetClaimAmount();
		}
	}
	
	/**
	 * @author fwang3
	 * Calculate days of interest according to the formula
	 */
	private void setInterestDays() {
		int days1 = DateUtils.calDays(sv.crtdate.toString(), sv.aprvdate.toString());
		/*
		 * flup rcds
		 */
		List<Fluppf> flupList = fluppfDAO.searchFlupRecord(wsspcomn.company.toString(),
				chdrrgpIO.getChdrnum().toString(), wsaaClamnum2.toString());
		int days2 = 0;
		if(!flupList.isEmpty()) {
			List<String> crtdateList = new ArrayList<String>();
			List<String> rcvdateList = new ArrayList<String>();
			for (Fluppf pf : flupList) {
				crtdateList.add(String.valueOf(pf.getCrtDate()));
				rcvdateList.add(String.valueOf(pf.getFuprcvd()));
			}
			days2 = DateUtils.calDays(DateUtils.getMaxOrMinDate(crtdateList, "min"),
					DateUtils.getMaxOrMinDate(rcvdateList, "max"));
		}
		int result = days1 - days2 - t6617rec.defInterestDay.toInt();
		sv.itstdays.set(result < 0 ? 0 : result);
		wsaaItstdays.set(sv.itstdays);
	}
	
	/**
	 * @author fwang3
	 * Load smart table T6617
	 */
	private void loadItem() {
		List<Itempf> itemList = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), tablesInner.t6617.toString(), 
				wsaaBatckey.batcBatctrcde.toString());
		if(itemList.isEmpty()) {
			syserrrec.params.set(tablesInner.t6617);
			fatalError600();
		}
		t6617rec.t6617Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
	}
	
	/**
	 * @author fwang3
	 * Load Interest Rate from table T6617
	 */
	private void setInterestRate() {
		sv.itstrate.set(t6617rec.intRate);
		wsaaItstrate.set(sv.itstrate);
	}
	
	/**
	 * @author fwang3
	 * Calculate Interest Amount according to the formula
	 */
	private void setInterestAmount() {
		// formula:  Interest = amount * (1 + rate)^ whole year * (1 + rate * d / 365) – amount
		PackedDecimalData rate = new PackedDecimalData(30, 29).init(div(sv.itstrate, 100));
		PackedDecimalData pow = power(add(1, rate), DateUtils.calYears(sv.crtdate.toString(), sv.aprvdate.toString()));
		PackedDecimalData part = add(1, div(mult(rate, sv.itstdays), DateUtils.getDaysOfYear(sv.crtdate.toString())));
		PackedDecimalData total = new PackedDecimalData(17, 2);
		compute(total, 2).setRounded(mult(regpIO.getNetamt(), pow, part));
		compute(sv.itstamt, 2).set(sub(total, regpIO.getNetamt()));
	}
	
	/**
	 * @author fwang3
	 * Net Claim Amount = previous net claim amount + interest amount
	 */
	private void setNetClaimAmount() {
		compute(sv.netclaimamt, 2).set(add(regpIO.getNetamt(), sv.itstamt));
	}


protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateDatabase3010();
			softLock3080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

/**
 * @author fwang3
 * Update the fields related to Interest 
 */
private void updateInterest() {
	if(cml004Permission) {
		regpIO.setInterestdays(sv.itstdays);
		regpIO.setInterestrate(sv.itstrate);
		regpIO.setInterestamt(sv.itstamt);
		regpIO.setNetamt(sv.netclaimamt);
	}
}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);
		}
		updateInterest();// CML004
		if (isEQ(firstTime, "Y")) {
			firstTime = "N";
			chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
			chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
			chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
			chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			/*       MOVE BEGNH               TO CHDRLIF-FUNCTION              */
			chdrlifIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			/* Re-write CHDR with validflag = '2'*/
			chdrlifIO.setCurrto(datcon1rec.intDate);
			chdrlifIO.setValidflag("2");
			/*       MOVE REWRT               TO CHDRLIF-FUNCTION              */
			chdrlifIO.setFunction(varcom.writd);
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			/* Write new CHDR with updated fields*/
			if ((isNE(regpIO.getDestkey(), SPACES))
			&& isEQ(regpIO.getDestkey(), chdrrgpIO.getChdrnum())
			&& wopMop.isTrue()) {
				compute(wsaaSinstamt05, 2).set(add((mult(regpIO.getPymt(), -1)), chdrlifIO.getSinstamt05()));
				compute(wsaaSinstamt06, 2).set((add(add(add(add(chdrlifIO.getSinstamt01(), chdrlifIO.getSinstamt02()), chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()), wsaaSinstamt05)));
				chdrlifIO.setSinstamt05(wsaaSinstamt05);
				chdrlifIO.setSinstamt06(wsaaSinstamt06);
				/* If this is waiver of premium component, then reduce the*/
				/* instalment amounts on the CHDR by the WOP premium.*/
				/* This will ensure that the correct premium is picked up*/
				/* during a renewal run. But first check to whether any*/
				/* premium should be subtracted. For example if a regular*/
				/* payment registration has already happened, then we don't*/
				/* want to take of the WOP premium again.*/
				if (isEQ(wsaaIsWop, "Y")) {
					if (isNE(tr517rec.zrwvflg01, "Y")) {
						calcWopInstprem8000();
						setPrecision(chdrlifIO.getSinstamt01(), 2);
						chdrlifIO.setSinstamt01(sub(chdrlifIO.getSinstamt01(), wsaaWopInstprem));
						setPrecision(chdrlifIO.getInsttot01(), 2);
						chdrlifIO.setInsttot01(sub(chdrlifIO.getInsttot01(), wsaaWopInstprem));
						setPrecision(chdrlifIO.getSinstamt06(), 2);
						chdrlifIO.setSinstamt06((add(add(add(add(chdrlifIO.getSinstamt01(), chdrlifIO.getSinstamt02()), chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()), chdrlifIO.getSinstamt05())));
					}
				}
			}
			chdrlifIO.setCurrfrom(datcon1rec.intDate);
			chdrlifIO.setCurrto(varcom.vrcmMaxDate);
			chdrlifIO.setValidflag("1");
			setPrecision(chdrlifIO.getTranno(), 0);
			chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
			chdrlifIO.setFunction(varcom.writr);
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			payrIO.setDataArea(SPACES);
			payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			payrIO.setChdrnum(chdrrgpIO.getChdrnum());
			payrIO.setValidflag("1");
			payrIO.setPayrseqno(wsaaPayrseqno);
			payrIO.setFormat(formatsInner.payrrec);
			payrIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getStatuz());
				fatalError600();
			}
			payrIO.setValidflag("2");
			payrIO.setFunction(varcom.rewrt);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			if ((isNE(regpIO.getDestkey(), SPACES))
			&& isEQ(regpIO.getDestkey(), chdrrgpIO.getChdrnum())
			&& wopMop.isTrue()) {
				setPrecision(payrIO.getSinstamt05(), 2);
				payrIO.setSinstamt05(add((mult(regpIO.getPymt(), -1)), payrIO.getSinstamt05()));
				setPrecision(payrIO.getSinstamt06(), 2);
				payrIO.setSinstamt06((add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05())));
				/* If this is waiver of premium component, then reduce the*/
				/* instalment amounts on the PAYR by the WOP premium.*/
				/* This will ensure that the correct premium is picked up*/
				/* during a renewal run.*/
				if (isEQ(wsaaIsWop, "Y")) {
					if (isNE(tr517rec.zrwvflg01, "Y")) {
						setPrecision(payrIO.getSinstamt01(), 2);
						payrIO.setSinstamt01(sub(payrIO.getSinstamt01(), wsaaWopInstprem));
						setPrecision(payrIO.getSinstamt06(), 2);
						payrIO.setSinstamt06((add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05())));
					}
				}
			}
			payrIO.setValidflag("1");
			payrIO.setTranno(chdrlifIO.getTranno());
			payrIO.setFunction(varcom.writr);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			h900UpdateCovr();
			regpIO.setValidflag("2");
			regpIO.setFormat(formatsInner.regprec);
			regpIO.setFunction(varcom.updat);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			regpIO.setValidflag("1");
		}
		if (isEQ(sv.fupflg, "X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		
		moveRegpCustomerSpecific();
		
		if (isEQ(sv.ddind, "X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		//ICIL-1546 Starts
		if(cml008Permission) {
			if (isEQ(sv.claimnotes, "X")) {
				goTo(GotoLabel.exit3090);
			}
			if (isEQ(sv.investres, "X")) {
				goTo(GotoLabel.exit3090);
			}
		}
		//ICIL-1546 End
		wsaaT6693Rgpystat.set(regpIO.getRgpystat());
		wsaaT6693Crtable.set(wsaaCrtable);
		readT66933300();
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaT6693Crtable.set("****");
			wsaaT6693Rgpystat.set(regpIO.getRgpystat());
			readT66933300();
		}
		updateRegp3200();
		claimAccum3600();
		diaryProcessing5200();
		letrCustomerSpecific();
	}

protected void setupNotipf(){
	

	wsspcomn.chdrCownnum.set(sv.lifcnum.toString());	
	wsspcomn.wsaaclaimno.set(sv.claimnumber.toString().trim());
	if(isEQ(sv.aacct,SPACES)){
		wsspcomn.wsaarelationship.set(SPACES);
		wsspcomn.wsaaclaimant.set(sv.lifcnum.toString());
		wsspcomn.wsaanotificationNum.set(SPACES);
	}
	else{
		notipf = notipfDAO.getNotiReByNotifin(sv.aacct.toString().replace(CLMPREFIX, ""),wsspcomn.company.toString());
		wsspcomn.wsaarelationship.set(notipf.getRelationcnum());
		wsspcomn.wsaaclaimant.set(notipf.getClaimant());
		wsspcomn.wsaanotificationNum.set(sv.aacct.toString());
	}

} 
protected void moveRegpCustomerSpecific() { }
protected void letrCustomerSpecific() { }
protected void updateClaimnoInAll(){
	
	clnnpfList1 =  clnnpfDAO.getClnnpfList(wsspcomn.company.toString(), sv.aacct.toString().replace(CLMPREFIX, "").trim(),sv.claimnumber.toString().trim());
	if(clnnpfList1!=null && !clnnpfList1.isEmpty()){
		sv.claimnotes.set("+");
	}
	else{
		sv.claimnotes.set(SPACES);
	}
}
protected void checkInvspf(){
	
	invspfList1 =  invspfDAO.getInvspfList(wsspcomn.company.toString(), sv.aacct.toString().replace(CLMPREFIX, "").trim(),sv.claimnumber.toString().trim());
	if(invspfList1!=null && !invspfList1.isEmpty()){
		sv.investres.set("+");
	}
	else{
		sv.investres.set(SPACES);
	}
}

protected void softLock3080()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	* </pre>
	*/
protected void updateRegp3200()
	{
		/*UPDATE*/
		regpIO.setRgpystat(wsaaRgpystat);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setTranno(chdrlifIO.getTranno());
		regpIO.setFunction(varcom.writr);
		regpIO.setFormat(formatsInner.regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		updatPtrn3500();
		/*EXIT*/
	}

protected void readT66933300()
	{
		readT66933310();
	}

protected void readT66933310()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6693);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6693, itdmIO.getItemtabl()))
		|| (isNE(wsaaT6693Key, itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setGenarea(SPACES);
			return ;
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaRgpystat.set(SPACES);
		index1.set(1);
		while ( !(isGT(index1, 12))) {
			findStatus3310();
		}

	}

protected void findStatus3310()
	{
		/*FIND-STATUS*/
		if (isEQ(t6693rec.trcode[index1.toInt()], wsaaBatckey.batcBatctrcde)) {
			wsaaRgpystat.set(t6693rec.rgpystat[index1.toInt()]);
			index1.set(15);
		}
		index1.add(1);
		/*EXIT*/
	}

protected void moveToRegp3400()
	{
		moveToRegp3410();
	}

protected void moveToRegp3410()
	{
		regpIO.setDestkey(sv.destkey);
		regpIO.setRgpynum(sv.rgpynum);
		regpIO.setPayclt(sv.payclt);
		regpIO.setPayreason(sv.cltype);
		regpIO.setClaimevd(sv.claimevd);
		regpIO.setRgpymop(sv.rgpymop);
		regpIO.setRegpayfreq(sv.regpayfreq);
		regpIO.setClamparty(sv.clamparty);
		regpIO.setPymt(sv.pymt);
		regpIO.setCurrcd(sv.currcd);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setCrtdate(sv.crtdate);
		regpIO.setRevdte(sv.revdte);
		regpIO.setFirstPaydate(sv.firstPaydate);
		regpIO.setAnvdate(sv.anvdate);
		regpIO.setFinalPaydate(sv.finalPaydate);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setNextPaydate(sv.nextPaydate);
		regpIO.setLastPaydate(sv.lastPaydate);
		regpIO.setCrtable(wsaaCrtable);
		// TODO GET the fields
		if (CMRPY005Permission) {
			regpIO.setAdjamt(sv.adjustamt);
			regpIO.setReasoncd(sv.reasoncd);
			regpIO.setReason(sv.resndesc);
			regpIO.setNetamt(sv.netclaimamt);
		}
		ptrnIO.setTranno(chdrlifIO.getTranno());
	}

protected void updatPtrn3500()
	{
		updat3510();
	}

protected void updat3510()
	{
		/* Write a PTRN record.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrrgpIO.getChdrnum());
		ptrnIO.setRecode(chdrrgpIO.getRecode());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTerm);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setPrtflg(SPACES);
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void claimAccum3600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					accum3610();
				case hcldio3620:
					hcldio3620();
					nextHcld3680();
				case exit3690:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void accum3610()
	{
		/* Calculate the component year                                    */
		datcon3rec.intDate1.set(wsaaCrrcd);
		datcon3rec.intDate2.set(hclhIO.getIncurdt());
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(wsaaCompYear, 5).set(add(0.99999, datcon3rec.freqFactor));
		/* Plan level accumulation                                         */
		planAccum3700();
		/* Benefit level checking                                          */
		hcldIO.setDataKey(SPACES);
		hcldIO.setChdrcoy(regpIO.getChdrcoy());
		hcldIO.setChdrnum(regpIO.getChdrnum());
		hcldIO.setLife(regpIO.getLife());
		hcldIO.setCoverage(regpIO.getCoverage());
		hcldIO.setRider(regpIO.getRider());
		hcldIO.setRgpynum(regpIO.getRgpynum());
		hcldIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi
		hcldIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hcldIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","RGPYNUM");
		hcldIO.setFormat(formatsInner.hcldrec);
	}

protected void hcldio3620()
	{
		SmartFileCode.execute(appVars, hcldIO);
		if (isNE(hcldIO.getStatuz(), varcom.oK)
		&& isNE(hcldIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hclhIO.getParams());
			syserrrec.statuz.set(hclhIO.getStatuz());
			fatalError600();
		}
		if (isNE(regpIO.getChdrcoy(), hcldIO.getChdrcoy())
		|| isNE(regpIO.getChdrnum(), hcldIO.getChdrnum())
		|| isNE(regpIO.getLife(), hcldIO.getLife())
		|| isNE(regpIO.getCoverage(), hcldIO.getCoverage())
		|| isNE(regpIO.getRider(), hcldIO.getRider())
		|| isNE(regpIO.getRgpynum(), hcldIO.getRgpynum())
		|| isEQ(hcldIO.getStatuz(), varcom.endp)) {
			hcldIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3690);
		}
		/* Benefit level checking                                          */
		benAccum3800();
	}

protected void nextHcld3680()
	{
		hcldIO.setFunction(varcom.nextr);
		goTo(GotoLabel.hcldio3620);
	}

protected void planAccum3700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					annual3710();
				case life3750:
					life3750();
				case amtAccum3780:
					amtAccum3780();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void annual3710()
	{
		if (isNE(hclhIO.getLmtyear(), "Y")) {
			goTo(GotoLabel.life3750);
		}
		hclaanlIO.setDataKey(SPACES);
		hclaanlIO.setChdrcoy(hclhIO.getChdrcoy());
		hclaanlIO.setChdrnum(hclhIO.getChdrnum());
		hclaanlIO.setLife(hclhIO.getLife());
		hclaanlIO.setCoverage(hclhIO.getCoverage());
		hclaanlIO.setRider(hclhIO.getRider());
		hclaanlIO.setAcumactyr(wsaaCompYear);
		hclaanlIO.setFunction(varcom.readr);
		hclaanlIO.setFormat(formatsInner.hclaanlrec);
		SmartFileCode.execute(appVars, hclaanlIO);
		if (isNE(hclaanlIO.getStatuz(), varcom.oK)
		&& isNE(hclaanlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclaanlIO.getStatuz());
			syserrrec.params.set(hclaanlIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclaanlIO.getStatuz(), varcom.oK)) {
			setPrecision(hclaanlIO.getAad(), 0);
			hclaanlIO.setAad(add(hclaanlIO.getAad(), hclhIO.getAad()));
			setPrecision(hclaanlIO.getClmpaid(), 2);
			hclaanlIO.setClmpaid(add(hclaanlIO.getClmpaid(), hclhIO.getTclmamt()));
			hclaanlIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, hclaanlIO);
			if (isNE(hclaanlIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclaanlIO.getStatuz());
				syserrrec.params.set(hclaanlIO.getParams());
				fatalError600();
			}
		}
		/* Create new record                                               */
		if (isEQ(hclaanlIO.getStatuz(), varcom.mrnf)) {
			hclaanlIO.setChdrcoy(hclhIO.getChdrcoy());
			hclaanlIO.setChdrnum(hclhIO.getChdrnum());
			hclaanlIO.setLife(hclhIO.getLife());
			hclaanlIO.setCoverage(hclhIO.getCoverage());
			hclaanlIO.setRider(hclhIO.getRider());
			hclaanlIO.setAcumactyr(wsaaCompYear);
			hclaanlIO.setCrtable(hclhIO.getCrtable());
			hclaanlIO.setBenpln(hclhIO.getBenpln());
			hclaanlIO.setHosben(SPACES);
			hclaanlIO.setAccday(ZERO);
			hclaanlIO.setAad(hclhIO.getAad());
			hclaanlIO.setClmpaid(hclhIO.getTclmamt());
			hclaanlIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hclaanlIO);
			if (isNE(hclaanlIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclaanlIO.getStatuz());
				syserrrec.params.set(hclaanlIO.getParams());
				fatalError600();
			}
		}
	}

protected void life3750()
	{
		if (isNE(hclhIO.getLmtlife(), "Y")) {
			goTo(GotoLabel.amtAccum3780);
		}
		hclaltlIO.setDataKey(SPACES);
		hclaltlIO.setChdrcoy(hclhIO.getChdrcoy());
		hclaltlIO.setChdrnum(hclhIO.getChdrnum());
		hclaltlIO.setLife(hclhIO.getLife());
		hclaltlIO.setCoverage(hclhIO.getCoverage());
		hclaltlIO.setRider(hclhIO.getRider());
		hclaltlIO.setFunction(varcom.readr);
		hclaltlIO.setFormat(formatsInner.hclaltlrec);
		SmartFileCode.execute(appVars, hclaltlIO);
		if (isNE(hclaltlIO.getStatuz(), varcom.oK)
		&& isNE(hclaltlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclaltlIO.getStatuz());
			syserrrec.params.set(hclaltlIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclaltlIO.getStatuz(), varcom.oK)) {
			setPrecision(hclaltlIO.getAad(), 0);
			hclaltlIO.setAad(add(hclaltlIO.getAad(), hclhIO.getAad()));
			setPrecision(hclaltlIO.getClmpaid(), 2);
			hclaltlIO.setClmpaid(add(hclaltlIO.getClmpaid(), hclhIO.getTclmamt()));
			hclaltlIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, hclaltlIO);
			if (isNE(hclaltlIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclaltlIO.getStatuz());
				syserrrec.params.set(hclaltlIO.getParams());
				fatalError600();
			}
		}
		/* Create new record                                               */
		if (isEQ(hclaltlIO.getStatuz(), varcom.mrnf)) {
			hclaltlIO.setChdrcoy(hclhIO.getChdrcoy());
			hclaltlIO.setChdrnum(hclhIO.getChdrnum());
			hclaltlIO.setLife(hclhIO.getLife());
			hclaltlIO.setCoverage(hclhIO.getCoverage());
			hclaltlIO.setRider(hclhIO.getRider());
			hclaltlIO.setAcumactyr(ZERO);
			hclaltlIO.setCrtable(hclhIO.getCrtable());
			hclaltlIO.setBenpln(hclhIO.getBenpln());
			hclaltlIO.setHosben(SPACES);
			hclaltlIO.setAccday(ZERO);
			hclaltlIO.setAad(hclhIO.getAad());
			hclaltlIO.setClmpaid(hclhIO.getTclmamt());
			hclaltlIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hclaltlIO);
			if (isNE(hclaltlIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclaltlIO.getStatuz());
				syserrrec.params.set(hclaltlIO.getParams());
				fatalError600();
			}
		}
	}

protected void amtAccum3780()
	{
		if (isEQ(hclhIO.getLmtyear(), "Y")
		|| isEQ(hclhIO.getLmtlife(), "Y")) {
			return ;
		}
		hclacapIO.setDataKey(SPACES);
		hclacapIO.setChdrcoy(hclhIO.getChdrcoy());
		hclacapIO.setChdrnum(hclhIO.getChdrnum());
		hclacapIO.setLife(hclhIO.getLife());
		hclacapIO.setCoverage(hclhIO.getCoverage());
		hclacapIO.setRider(hclhIO.getRider());
		hclacapIO.setFunction(varcom.readr);
		hclacapIO.setFormat(formatsInner.hclacaprec);
		SmartFileCode.execute(appVars, hclacapIO);
		if (isNE(hclacapIO.getStatuz(), varcom.oK)
		&& isNE(hclacapIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclacapIO.getStatuz());
			syserrrec.params.set(hclacapIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclacapIO.getStatuz(), varcom.oK)) {
			setPrecision(hclacapIO.getAad(), 0);
			hclacapIO.setAad(add(hclacapIO.getAad(), hclhIO.getAad()));
			setPrecision(hclacapIO.getClmpaid(), 2);
			hclacapIO.setClmpaid(add(hclacapIO.getClmpaid(), hclhIO.getTclmamt()));
			hclacapIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, hclacapIO);
			if (isNE(hclacapIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclacapIO.getStatuz());
				syserrrec.params.set(hclacapIO.getParams());
				fatalError600();
			}
		}
		/* Create new record                                               */
		if (isEQ(hclacapIO.getStatuz(), varcom.mrnf)) {
			hclacapIO.setChdrcoy(hclhIO.getChdrcoy());
			hclacapIO.setChdrnum(hclhIO.getChdrnum());
			hclacapIO.setLife(hclhIO.getLife());
			hclacapIO.setCoverage(hclhIO.getCoverage());
			hclacapIO.setRider(hclhIO.getRider());
			hclacapIO.setAcumactyr(9999);
			hclacapIO.setCrtable(hclhIO.getCrtable());
			hclacapIO.setBenpln(hclhIO.getBenpln());
			hclacapIO.setHosben(SPACES);
			hclacapIO.setAccday(ZERO);
			hclacapIO.setAad(hclhIO.getAad());
			hclacapIO.setClmpaid(hclhIO.getTclmamt());
			hclacapIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hclacapIO);
			if (isNE(hclacapIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclacapIO.getStatuz());
				syserrrec.params.set(hclacapIO.getParams());
				fatalError600();
			}
		}
	}

protected void benAccum3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					benAnn3800();
				case benLife3850:
					benLife3850();
				case benAmtAccum3880:
					benAmtAccum3880();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void benAnn3800()
	{
		if (isNE(hcldIO.getLmtyear(), "Y")) {
			goTo(GotoLabel.benLife3850);
		}
		hclabanIO.setDataKey(SPACES);
		hclabanIO.setChdrcoy(hcldIO.getChdrcoy());
		hclabanIO.setChdrnum(hcldIO.getChdrnum());
		hclabanIO.setLife(hcldIO.getLife());
		hclabanIO.setCoverage(hcldIO.getCoverage());
		hclabanIO.setRider(hcldIO.getRider());
		hclabanIO.setHosben(hcldIO.getHosben());
		hclabanIO.setAcumactyr(wsaaCompYear);
		hclabanIO.setFunction(varcom.readr);
		hclabanIO.setFormat(formatsInner.hclabanrec);
		SmartFileCode.execute(appVars, hclabanIO);
		if (isNE(hclabanIO.getStatuz(), varcom.oK)
		&& isNE(hclabanIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclabanIO.getStatuz());
			syserrrec.params.set(hclabanIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclabanIO.getStatuz(), varcom.oK)) {
			setPrecision(hclabanIO.getAccday(), 0);
			hclabanIO.setAccday(add(hclabanIO.getAccday(), hcldIO.getZdaycov()));
			setPrecision(hclabanIO.getClmpaid(), 2);
			hclabanIO.setClmpaid(add(hclabanIO.getClmpaid(), hcldIO.getGcnetpy()));
			hclabanIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, hclabanIO);
			if (isNE(hclabanIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabanIO.getStatuz());
				syserrrec.params.set(hclabanIO.getParams());
				fatalError600();
			}
		}
		/* Create new record                                               */
		if (isEQ(hclabanIO.getStatuz(), varcom.mrnf)) {
			hclabanIO.setChdrcoy(hcldIO.getChdrcoy());
			hclabanIO.setChdrnum(hcldIO.getChdrnum());
			hclabanIO.setLife(hcldIO.getLife());
			hclabanIO.setCoverage(hcldIO.getCoverage());
			hclabanIO.setRider(hcldIO.getRider());
			hclabanIO.setAcumactyr(wsaaCompYear);
			hclabanIO.setCrtable(hcldIO.getCrtable());
			hclabanIO.setBenpln(hcldIO.getBenpln());
			hclabanIO.setHosben(hcldIO.getHosben());
			hclabanIO.setAccday(hcldIO.getZdaycov());
			hclabanIO.setAad(ZERO);
			hclabanIO.setClmpaid(hcldIO.getGcnetpy());
			hclabanIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hclabanIO);
			if (isNE(hclabanIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabanIO.getStatuz());
				syserrrec.params.set(hclabanIO.getParams());
				fatalError600();
			}
		}
	}

protected void benLife3850()
	{
		if (isNE(hcldIO.getLmtlife(), "Y")) {
			goTo(GotoLabel.benAmtAccum3880);
		}
		hclabltIO.setDataKey(SPACES);
		hclabltIO.setChdrcoy(hcldIO.getChdrcoy());
		hclabltIO.setChdrnum(hcldIO.getChdrnum());
		hclabltIO.setLife(hcldIO.getLife());
		hclabltIO.setCoverage(hcldIO.getCoverage());
		hclabltIO.setRider(hcldIO.getRider());
		hclabltIO.setHosben(hcldIO.getHosben());
		hclabltIO.setFunction(varcom.readr);
		hclabltIO.setFormat(formatsInner.hclabltrec);
		SmartFileCode.execute(appVars, hclabltIO);
		if (isNE(hclabltIO.getStatuz(), varcom.oK)
		&& isNE(hclabltIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclabltIO.getStatuz());
			syserrrec.params.set(hclabltIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclabltIO.getStatuz(), varcom.oK)) {
			setPrecision(hclabltIO.getAccday(), 0);
			hclabltIO.setAccday(add(hclabltIO.getAccday(), hcldIO.getZdaycov()));
			setPrecision(hclabltIO.getClmpaid(), 2);
			hclabltIO.setClmpaid(add(hclabltIO.getClmpaid(), hcldIO.getGcnetpy()));
			hclabltIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, hclabltIO);
			if (isNE(hclabltIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabltIO.getStatuz());
				syserrrec.params.set(hclabltIO.getParams());
				fatalError600();
			}
		}
		/* Create new record                                               */
		if (isEQ(hclabltIO.getStatuz(), varcom.mrnf)) {
			hclabltIO.setChdrcoy(hcldIO.getChdrcoy());
			hclabltIO.setChdrnum(hcldIO.getChdrnum());
			hclabltIO.setLife(hcldIO.getLife());
			hclabltIO.setCoverage(hcldIO.getCoverage());
			hclabltIO.setRider(hcldIO.getRider());
			hclabltIO.setAcumactyr(ZERO);
			hclabltIO.setCrtable(hcldIO.getCrtable());
			hclabltIO.setBenpln(hcldIO.getBenpln());
			hclabltIO.setHosben(hcldIO.getHosben());
			hclabltIO.setAccday(hcldIO.getZdaycov());
			hclabltIO.setAad(ZERO);
			hclabltIO.setClmpaid(hcldIO.getGcnetpy());
			hclabltIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hclabltIO);
			if (isNE(hclabltIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabltIO.getStatuz());
				syserrrec.params.set(hclabltIO.getParams());
				fatalError600();
			}
		}
	}

protected void benAmtAccum3880()
	{
		if (isEQ(hcldIO.getLmtyear(), "Y")
		|| isEQ(hcldIO.getLmtlife(), "Y")) {
			return ;
		}
		hclabcaIO.setDataKey(SPACES);
		hclabcaIO.setChdrcoy(hcldIO.getChdrcoy());
		hclabcaIO.setChdrnum(hcldIO.getChdrnum());
		hclabcaIO.setLife(hcldIO.getLife());
		hclabcaIO.setCoverage(hcldIO.getCoverage());
		hclabcaIO.setRider(hcldIO.getRider());
		hclabcaIO.setFunction(varcom.readr);
		hclabcaIO.setFormat(formatsInner.hclabcarec);
		SmartFileCode.execute(appVars, hclabcaIO);
		if (isNE(hclabcaIO.getStatuz(), varcom.oK)
		&& isNE(hclabcaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclabcaIO.getStatuz());
			syserrrec.params.set(hclabcaIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclabcaIO.getStatuz(), varcom.oK)) {
			setPrecision(hclabcaIO.getAad(), 0);
			hclabcaIO.setAad(add(hclabcaIO.getAad(), ZERO));
			setPrecision(hclabcaIO.getClmpaid(), 2);
			hclabcaIO.setClmpaid(add(hclabcaIO.getClmpaid(), hcldIO.getGcnetpy()));
			hclabcaIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, hclabcaIO);
			if (isNE(hclabcaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabcaIO.getStatuz());
				syserrrec.params.set(hclabcaIO.getParams());
				fatalError600();
			}
		}
		/* Create new record                                               */
		if (isEQ(hclabcaIO.getStatuz(), varcom.mrnf)) {
			hclabcaIO.setChdrcoy(hcldIO.getChdrcoy());
			hclabcaIO.setChdrnum(hcldIO.getChdrnum());
			hclabcaIO.setLife(hcldIO.getLife());
			hclabcaIO.setCoverage(hcldIO.getCoverage());
			hclabcaIO.setRider(hcldIO.getRider());
			hclabcaIO.setAcumactyr(9999);
			hclabcaIO.setCrtable(hcldIO.getCrtable());
			hclabcaIO.setBenpln(hcldIO.getBenpln());
			hclabcaIO.setHosben(hcldIO.getHosben());
			hclabcaIO.setAccday(hcldIO.getZdaycov());
			hclabcaIO.setAad(ZERO);
			hclabcaIO.setClmpaid(hcldIO.getGcnetpy());
			hclabcaIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hclabcaIO);
			if (isNE(hclabcaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabcaIO.getStatuz());
				syserrrec.params.set(hclabcaIO.getParams());
				fatalError600();
			}
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4010();
				case popUp4050:
					popUp4050();
				case investResult:
					investResult4060();
				case claimNotes:
					claimNotes4070();
				case gensww4010:
					gensww4010();
				case nextProgram4020:
					nextProgram4020();
				case exit4090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		/*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		if (isEQ(sv.fupflg, "?")) {
			if (isEQ(sv.ddind, "X")) {
				regpIO.setFunction(varcom.keeps);
				regpIO.setFormat(formatsInner.regprec);
				SmartFileCode.execute(appVars, regpIO);
				if (isNE(regpIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(regpIO.getParams());
					fatalError600();
				}
				goTo(GotoLabel.popUp4050);
			}
			else {
				checkFollowUp5000();
			}
		}
		if (isEQ(sv.ddind, "?")) {
			checkBankDetails4400();
		}
		
		if(cml008Permission) {
		if (isEQ(sv.claimnotes, "X")) {
			goTo(GotoLabel.claimNotes);
		}
	
		if (isEQ(sv.investres, "X")) {
		goTo(GotoLabel.investResult);
		}
		}
		nextProgramHealthClaim();
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}

protected void nextProgramHealthClaim() { }
	/**
	* <pre>
	*    If any of the indicators have been selected, (value - 'X'),
	*    then set an asterisk in the program stack action field to
	*    ensure that control returns here, set the parameters for
	*    generalised secondary switching and save the original
	*    programs from the program stack.
	* </pre>
	*/

protected void investResult4060()
{
	
	if ((isEQ(sv.investres, "X")))  {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
	}
	gensswrec.function.set(SPACES);
	
	if (isEQ(sv.investres, "?")) {
		 checkInvspf();
		}
	
	if (isEQ(sv.investres, "X")) {
		sv.investres.set("?");
		setupNotipf();
		gensswrec.function.set("C");
		goTo(GotoLabel.gensww4010);
	}
	
}


protected void claimNotes4070()
{
	
	if ((isEQ(sv.claimnotes, "X")))  {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
	}
	gensswrec.function.set(SPACES);
	
	if (isEQ(sv.claimnotes, "?")) {
		updateClaimnoInAll();
	}
	
	if (isEQ(sv.claimnotes, "X")) {
		sv.claimnotes.set("?");
		setupNotipf();
		gensswrec.function.set("D");
		goTo(GotoLabel.gensww4010);
	}
	
	
}


protected void popUp4050()
	{
		if ((isEQ(sv.ddind, "X"))
		|| (isEQ(sv.fupflg, "X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4200();
			}
		}
		gensswrec.function.set(SPACES);
		/*   If FOLLOW-UP has been selected set 'B' in the function.*/
		if (isEQ(sv.fupflg, "X")) {
			sv.fupflg.set("?");
			gensswrec.function.set("B");
			goTo(GotoLabel.gensww4010);
		}
		/*   If BANK DETAILS has been selected set 'A' in the function.*/
		if (isEQ(sv.ddind, "X")) {
			sv.ddind.set("?");
			gensswrec.function.set("A");
			goTo(GotoLabel.gensww4010);
		}
	}

	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call
	*   the generalised secondary switching module to obtain the
	*   next 8 programs and load them into the program stack.
	* </pre>
	*/
protected void gensww4010()
	{
		if (isEQ(gensswrec.function, SPACES)) {
			goTo(GotoLabel.nextProgram4020);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz, varcom.oK))
		&& (isNE(gensswrec.statuz, varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

	/**
	* <pre>
	*    Sections performed from the 4000 section above.
	* </pre>
	*/
protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void checkBankDetails4400()
	{
		bank4400();
	}

protected void bank4400()
	{
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Check if there are any bank details on the current contract*/
		if ((isEQ(regpIO.getBankkey(), SPACES))
		&& (isEQ(regpIO.getBankacckey(), SPACES))) {
			sv.ddind.set(SPACES);
		}
		else {
			sv.ddind.set("+");
		}
	}

	/**
	* <pre>
	*    Sections performed from more than 1 section.
	* </pre>
	*/
protected void checkFollowUp5000()
	{
		follow5010();
	}

protected void follow5010()
	{
		/* Check if there are any followups on the current contract.*/
		fluprgpIO.setChdrcoy(wsspcomn.company);
		fluprgpIO.setChdrnum(chdrrgpIO.getChdrnum());
		wsaaRgpynum.set(regpIO.getRgpynum());
		wsaaClamnumFill.set(ZERO);
		fluprgpIO.setClamnum(wsaaClamnum2);
		fluprgpIO.setFupno(0);
		/* We will use WSAA-CLAMNUM to compare to FLUPRGP-CLAMNUM*/
		/* instead of comparing REGP-RGPYNUM and FLUPRGP-CLAMNUM.*/
		/* that is due to the fact that RGPYNUM is 5 bytes long*/
		/* and CLAMNUM is 8 bytes long*/
		fluprgpIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		fluprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluprgpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","CLAMNUM");

		SmartFileCode.execute(appVars, fluprgpIO);
		if ((isNE(fluprgpIO.getStatuz(), varcom.oK))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		if ((isEQ(wsspcomn.company, fluprgpIO.getChdrcoy()))
		&& (isEQ(chdrrgpIO.getChdrnum(), fluprgpIO.getChdrnum()))
		&& (isEQ(fluprgpIO.getClamnum(), wsaaClamnum2))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			sv.fupflg.set("+");
		}
		else {
			sv.fupflg.set(SPACES);
		}
	}

protected void checkStatusFlup5100()
	{
		follow5110();
	}

protected void follow5110()
	{
		/* We will use WSAA-CLAMNUM to compare to FLUPRGP-CLAMNUM*/
		/* instead of comparing REGP-RGPYNUM and FLUPRGP-CLAMNUM.*/
		/* that is due to the fact that RGPYNUM is 5 bytes long*/
		/* and CLAMNUM is 8 bytes long*/
		SmartFileCode.execute(appVars, fluprgpIO);
		if ((isNE(fluprgpIO.getStatuz(), varcom.oK))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		if ((isEQ(wsspcomn.company, fluprgpIO.getChdrcoy()))
		&& (isEQ(chdrrgpIO.getChdrnum(), fluprgpIO.getChdrnum()))
		&& (isEQ(wsaaClamnum2, fluprgpIO.getClamnum()))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			wsaaFlupExsist = "Y";
			sv.fupflg.set("+");
		}
		else {
			fluprgpIO.setStatuz(varcom.endp);
			if (isEQ(wsaaFlupExsist, "N")) {
				wsaaFlupOut = "N";
			}
			return ;
		}
		fluprgpIO.setFunction(varcom.nextr);
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t5661);
		itemIO.setItempfx("IT");
		/*    MOVE FLUPRGP-FUPCODE        TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(fluprgpIO.getFupcode());
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), "MRNF"))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
			return ;
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		index1.set(1);
		wsaaFlupOut = "Y";
		while ( !(isGT(index1, 10))) {
			flupStatus5110();
		}

		if (isEQ(wsaaFlupOut, "Y")) {
			fluprgpIO.setStatuz(varcom.endp);
			varcom.endp.set(varcom.endp);
		}
	}

protected void flupStatus5110()
	{
		/*FOLLOW-UP*/
		/* Check if the followups have a status of complete.*/
		if (isEQ(fluprgpIO.getFupstat(), t5661rec.fuposs[index1.toInt()])) {
			wsaaFlupOut = "N";
			index1.set(15);
			return ;
		}
		if (isEQ(t5661rec.fuposs[index1.toInt()], SPACES)) {
			index1.set(15);
		}
		index1.add(1);
		/*EXIT*/
	}

protected void diaryProcessing5200()
	{
		start5210();
	}

protected void start5210()
	{
		/* This section will determine if the DIARY system is present   */
		/* If so, the appropriate parameters are filled and the         */
		/* diary processor is called.                                   */
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaT7508Cnttype.set(chdrrgpIO.getCnttype());
		readT75085300();
		/* If item not found no Diary Update Processing is Required.    */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75085300();
		}
		/* If item not found no Batch Diary Processing is Required.        */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(regpIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypCertdate.set(regpIO.getCertdate());
		drypDryprcRecInner.drypProcSeqNo.set(100);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75085300()
	{
		start5310();
	}

protected void start5310()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void readTr5176000()
	{
		para6100();
	}

protected void para6100()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(), covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaIsWop = "N";
		}
		else {
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
			wsaaIsWop = "Y";
		}
	}

protected void readPayr7000()
	{
		para7100();
	}

protected void para7100()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		payrIO.setChdrnum(chdrrgpIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		if (isNE(payrIO.getChdrcoy(), chdrrgpIO.getChdrcoy())
		|| isNE(payrIO.getChdrnum(), chdrrgpIO.getChdrnum())
		|| isEQ(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrrgpIO.getChdrnum());
			syserrrec.statuz.set(errorsInner.e540);
			fatalError600();
		}
		wsaaPayrBillfreq.set(payrIO.getBillfreq());
	}

protected void calcWopInstprem8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para8100();
				case callCovrio8110:
					callCovrio8110();
				case calculateWopInst8310:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para8100()
	{
		/* To ensure that we don't take of the WOP premium more than once*/
		/* e.g. for an adjustment, accumulate the COVR instalments and*/
		/* compare it with the SINSTAMT01 on the CHDR record. If they are*/
		/* the same then the WOP premium has not been subtracted from the*/
		/* total premium instalment.*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		covrIO.setChdrnum(chdrlifIO.getChdrnum());
		covrIO.setLife(SPACES);
		covrIO.setCoverage(SPACES);
		covrIO.setRider(SPACES);
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begn);
	}

protected void callCovrio8110()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isNE(covrIO.getLife(), regpIO.getLife())
		|| isNE(covrIO.getCoverage(), regpIO.getCoverage())
		|| isNE(covrIO.getRider(), regpIO.getRider())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.calculateWopInst8310);
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			covrIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callCovrio8110);
		}
		covrIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callCovrio8110);
	}

protected void h900UpdateCovr()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					h901Para();
				case h910CallCovrio:
					h910CallCovrio();
				case h990Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void h901Para()
	{
		if (isEQ(wsaaIsWop, "Y")) {
			if (isEQ(tr517rec.zrwvflg01, "Y")) {
				goTo(GotoLabel.h990Exit);
			}
		}
		else {
			goTo(GotoLabel.h990Exit);
		}
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(regpIO.getChdrcoy());
		covrIO.setChdrnum(regpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begnh);
	}

protected void h910CallCovrio()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isNE(covrIO.getLife(), regpIO.getLife())
		|| isNE(covrIO.getCoverage(), regpIO.getCoverage())
		|| isNE(covrIO.getRider(), regpIO.getRider())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		covrIO.setValidflag("2");
		covrIO.setCurrto(datcon1rec.intDate);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovRiskStat, SPACES)) {
				covrIO.setStatcode(t5679rec.setCovRiskStat);
			}
			if (isNE(t5679rec.setCovPremStat, SPACES)) {
				covrIO.setPstatcode(t5679rec.setCovPremStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat, SPACES)) {
				covrIO.setStatcode(t5679rec.setRidRiskStat);
			}
			if (isNE(t5679rec.setRidPremStat, SPACES)) {
				covrIO.setPstatcode(t5679rec.setRidPremStat);
			}
			covrIO.setValidflag("1");
		}
		covrIO.setCurrfrom(datcon1rec.intDate);
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setTranno(chdrlifIO.getTranno());
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		setPrecision(covrIO.getPlanSuffix(), 0);
		covrIO.setPlanSuffix(add(covrIO.getPlanSuffix(), 1));
		covrIO.setFunction(varcom.begnh);
		goTo(GotoLabel.h910CallCovrio);
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
	private FixedLengthStringData g523 = new FixedLengthStringData(4).init("G523");
	private FixedLengthStringData g524 = new FixedLengthStringData(4).init("G524");
	private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
	private FixedLengthStringData h133 = new FixedLengthStringData(4).init("H133");
	private FixedLengthStringData h134 = new FixedLengthStringData(4).init("H134");
	private FixedLengthStringData h136 = new FixedLengthStringData(4).init("H136");
	private FixedLengthStringData h138 = new FixedLengthStringData(4).init("H138");
	private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
	private FixedLengthStringData h017 = new FixedLengthStringData(4).init("H017");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData e540 = new FixedLengthStringData(4).init("E540");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5661 = new FixedLengthStringData(5).init("T5661");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5400 = new FixedLengthStringData(5).init("T5400");
	private FixedLengthStringData t6691 = new FixedLengthStringData(5).init("T6691");
	private FixedLengthStringData t6692 = new FixedLengthStringData(5).init("T6692");
	private FixedLengthStringData t6693 = new FixedLengthStringData(5).init("T6693");
	private FixedLengthStringData t6694 = new FixedLengthStringData(5).init("T6694");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData t6617 = new FixedLengthStringData(5).init("T6617");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
public static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC   ");
	public FixedLengthStringData regprec = new FixedLengthStringData(10).init("REGPREC   ");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	public FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	public FixedLengthStringData hclhrec = new FixedLengthStringData(10).init("HCLHREC   ");
	private FixedLengthStringData hcldrec = new FixedLengthStringData(10).init("HCLDREC   ");
	private FixedLengthStringData hclaanlrec = new FixedLengthStringData(10).init("HCLAANLREC");
	private FixedLengthStringData hclaltlrec = new FixedLengthStringData(10).init("HCLALTLREC");
	private FixedLengthStringData hclacaprec = new FixedLengthStringData(10).init("HCLACAPREC");
	private FixedLengthStringData hclabanrec = new FixedLengthStringData(10).init("HCLABANREC");
	private FixedLengthStringData hclabltrec = new FixedLengthStringData(10).init("HCLABLTREC");
	private FixedLengthStringData hclabcarec = new FixedLengthStringData(10).init("HCLABCAREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	private FixedLengthStringData drypDetailParams1 = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput1 = new FixedLengthStringData(71).isAPartOf(drypDetailParams1, 0);
	private PackedDecimalData drypCertdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 56);
}
}
