package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import com.csc.common.DD;
import com.csc.fsu.financials.screens.S2201protect;
import com.csc.fsu.financials.screens.S2201screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SJL51
 * @version 1.0 generated on 20/07/06 12:12
 */
public class Sjl51ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(119);
	public FixedLengthStringData dataFields = new FixedLengthStringData(23).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankcode = DD.bankcode.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData claimnmber = DD.claimnmber.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData paymentNum = DD.paymtnum.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData reqntype = DD.reqntype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData windowtype = DD.windowtype.copy().isAPartOf(dataFields,22);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 23);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData claimnmberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData paymentnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData reqntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData windowtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 47);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] claimnmberOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] paymentnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] reqntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] windowtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);

		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sjl51screenWritten = new LongData(0);
	public LongData Sjl51protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sjl51ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(reqntypeOut,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimnmberOut,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paymentnumOut,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankcodeOut,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {windowtype, reqntype, claimnmber, paymentNum, action, bankcode};
		screenOutFields = new BaseData[][] {windowtypeOut, reqntypeOut, claimnmberOut, paymentnumOut, actionOut, bankcodeOut};
		screenErrFields = new BaseData[] {windowtypeErr, reqntypeErr, claimnmberErr, paymentnumErr, actionErr, bankcodeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sjl51screen.class;
		protectRecord = Sjl51protect.class;
	}
}
