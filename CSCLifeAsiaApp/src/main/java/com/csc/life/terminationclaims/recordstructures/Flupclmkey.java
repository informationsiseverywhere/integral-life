package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:39
 * Description:
 * Copybook name: FLUPCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Flupclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData flupclmFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData flupclmKey = new FixedLengthStringData(256).isAPartOf(flupclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData flupclmChdrcoy = new FixedLengthStringData(1).isAPartOf(flupclmKey, 0);
  	public FixedLengthStringData flupclmChdrnum = new FixedLengthStringData(8).isAPartOf(flupclmKey, 1);
  	public PackedDecimalData flupclmFupno = new PackedDecimalData(2, 0).isAPartOf(flupclmKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(flupclmKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(flupclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		flupclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}