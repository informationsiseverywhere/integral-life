/*
 * File: P5256at.java
 * Date: 30 August 2009 0:22:51
 * Author: Quipoz Limited
 *
 * Class transformed from P5256AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.IncrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
//added for death claim flexibility
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.recordstructures.Dthclmflxrec;
import com.csc.life.terminationclaims.recordstructures.Dthcpy;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*               CLAIMS REGISTRATION - AT MODULE
*               -------------------------------
*
*  This AT module  is  called  by the Death Claims program
*  P5256 and the Contract  number, is passed in the AT
*  parameters area as the "primary key".
*
*  AT PROCESSING
*
*  Get all statuses
*
*  - obtain the  status  codes  required  by  reading table
*    T5679,    i.e.    the   'Component   Statuses   For
*    Transactions'.  This  table is keyed by transaction
*    number.
*
*  Get todays date for DATCON1.
*
*  Check the return status  codes  for  each  of  the
*  following records being updated.
*
*  Read the Claim header record (CLMHCLM) for this contract.
*
*  PROCESS CONTRACT HEADER
*
*  Read the  contract  header  (CHDRLIF)  for  the  contract
*  and update as follows:
*
*  - set valid flag  to '2'; set 'Effective To' date to the
*    Death   Claim  effective-date  (default  is  todays
*    date) and update the record.
*
*  - write a new version of CHDRLIF as follows:-
*
*  - update the  transaction  number  by incrementing by +1
*    and update the  transaction  date  with  the  Death
*    Claim  effective-date  (default  is  todays  date).
*    Update  the  contract  status  code with the status
*    code from T5679.
*
*  - alter the premium status if the status  field on T5679
*    for  regular or single premiums  have  entries.  If
*    entries are present, then check  the payment method
*    (MOP) on the contract header. If the  MOP  is '00',
*    this  implies  single premium,  otherwise  use  the
*    regular premium status field.
*
*  - set 'Effective  From'  date  to  Death  effective date
*    (todays).
*
*  - set 'Effective To' date to VRCM-MAX-DATE.
*
*  PROCESS LIVES
*
*  Read all the live being processed (LIFE) for the contract
*  and update the life details as follows:
*
*  - set valid flag  to  '2';  set  'Effective  To' date as
*    above and update the record.
*
*  - write a new record as follows:
*
*  - update  the  transaction  number  with the transaction
*    number from  the  contract  header  and  update the
*    transaction   date    with    the    Death    Claim
*    effective-date  (default  is  todays date).  Update
*    the status code  with  the status code, from T5679,
*    for the  life or joint-life, whichever is currently
*    being processed.  (LIFE-NUMBER = spaces or '00' for
*    single life, equal to '01' for joint life case).
*
*
*  PROCESS COMPONENTS
*
*  Read all  the Coverage/Rider records (COVR) for this
*  contract and update each COVR record as follows:
*
*  - set the valid  flag to '2'; set 'Effective To' date as
*    above and update the record.
*
*  - write a new record as follows:
*
*  - update  the  transaction  number  with the transaction
*    number from  the  contract  header  and  update the
*    transaction  date  with the Effective-date (default
*    is todays date)  of  this  death claim.  Update the
*    status code with  the  status code from T5679.  Use
*    the regular premium  status code if the the billing
*    frequency, from  the  contract  header, is equal to
*    '00' and  the  single  premium  indicator  on table
*    T5687 is  not 'Y', otherwise use the single-premium
*    status from T5679.  T5687  is  a  dated  table  and
*    access to it is by coverage/rider code.
*
*
*  PROCESS DEATH CLAIM DETAIL RECORDS (CLMDCLM)
*
*  Read all  the  CLMD  records  for this contract and life
*  being processed.
*
*  Access  T5687  and  for  the death method found read the
*  table  (T6598) behind this method  and  access  the
*  'Death Processing' subroutine.
*
*  DOWHILE
*              there are CLMD records for this contract/life
*              Call the 'Death Processing Subroutine (T6598)'
*  ENDDO
*
*  Linkage area passed to the death processing subroutine:-
*
*             - company
*             - contract header number
*             - life number
*             - joint-life number
*             - coverage
*             - rider
*             - crtable
*             - effective date
*             - estimated value
*             - actual value
*             - currency
*             - element code
*             - type code
*             - status
*
*
*  GENERAL HOUSE-KEEPING
*
*  1) Write a PTRN transaction record:
*
*  - contract key from contract header
*  - transaction number from contract header
*  - transaction effective date equals todays
*  - batch key information from AT linkage.
*
*  2) Update the  batch  header  by  calling  BATCUP with a
*     function of WRITS and the following parameters:
*
*  - transaction count equals 1
*  - all other amounts zero
*  - batch key from AT linkage
*
*  3) Release contract SFTLOCK.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*
*
*****************************************************************
* </pre>
*/
public class P5256at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5256AT");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	protected FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	protected FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 16);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData wsaaNetVal = new PackedDecimalData(13, 2);

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	protected FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* ERRORS */
	protected static final String f294 = "F294";
	private static final String g371 = "G371";
		/* TABLES */
	protected static final String t5645 = "T5645";
	private static final String t5679 = "T5679";
	protected static final String t5515 = "T5515";
	protected static final String t5687 = "T5687";
	protected static final String t6598 = "T6598";
	private static final String t7508 = "T7508";
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	protected ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	protected ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	//protected CovrTableDAM covrIO = new CovrTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	protected IncrTableDAM incrIO = new IncrTableDAM();
	private IncrmjaTableDAM incrmjaIO = new IncrmjaTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected LifeTableDAM lifeIO = new LifeTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	protected PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	private Crtundwrec crtundwrec = new Crtundwrec();
	protected Varcom varcom = new Varcom();
	protected T5645rec t5645rec = new T5645rec();
	protected T5679rec t5679rec = new T5679rec();
	protected T5515rec t5515rec = new T5515rec();
	protected T5687rec t5687rec = new T5687rec();
	protected T6598rec t6598rec = new T6598rec();
	protected T7508rec t7508rec = new T7508rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	public Dthcpy dthcpy = new Dthcpy();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Cashedrec cashedrec = new Cashedrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	protected Trmreasrec trmreasrec = new Trmreasrec();
	protected Atmodrec atmodrec = new Atmodrec();
	protected DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	protected FormatsInner formatsInner = new FormatsInner();
	protected SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();
	//ILIFE-1137
	//Death Claim Flexibility
	private ZonedDecimalData wsaaPost9 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	protected ZonedDecimalData wsaaPost10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	//ILIFE-1137
	//added for death claim flexibility		
	protected T5688rec t5688rec = new T5688rec();
	protected Dthclmflxrec dthclmflxRec =  new Dthclmflxrec();
	protected static final String t5688 = "T5688";
	private static final String e308 = "E308";
	//end added for death claim flexibility
	
	//ILIFE-5462 Start
	protected T6647rec t6647rec = new T6647rec();
	private static final String t6647 = "T6647";
	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTabBatckey = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);
	ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	List<Itempf> listT6647 = new ArrayList<Itempf>();
	private List<Lifepf> updateLifepflist =  new ArrayList<>();
	private List<Lifepf> insertLifepflist = new ArrayList<>();
	
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);

	CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	Covrpf covrIO = null;
	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO" , CattpfDAO.class);
	 private ClmhpfDAO clmhpfDAO = getApplicationContext().getBean("clmhpfDAO" , ClmhpfDAO.class);
	private Cattpf cattpf;
	boolean CMDTH010Permission  = false;
	private String t6693 = "T6693";
	private T6693rec t6693rec = new T6693rec();
	private static final String feaConfigPreRegistartion= "CMDTH010";
	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);
	private ZonedDecimalData index1 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaClmstat = new FixedLengthStringData(2);
	private Itempf itempf = null;

	//private Srcalcpy srcalcpy = new Srcalcpy();
	//ILIFE-5462 End

/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr2830,
		exit2849
	}

	public P5256at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		housekeepingTerminate3000();
		a000Statistics();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			return ;
		}
		sysrSyserrRecInner.sysrSyserrStatuz.set(sysrSyserrRecInner.sysrStatuz);
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
	CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
		readClaimHeader1050();
		readContractHeader1100();
		readStatusCodes1200();
		callDatcons1300();
		pendingIncrease1400();
		/*EXIT*/
	}

	/**
	* <pre>
	* Initial read of the Claim Header.                           *
	* </pre>
	*/
protected void readClaimHeader1050()
	{
		readClaim1051();
	}

protected void readClaim1051()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(atmodrec.company);
		clmhclmIO.setChdrnum(wsaaPrimaryChdrnum);
		/*MOVE 'READH'                TO CLMHCLM-FUNCTION.             */
		clmhclmIO.setFunction("READR");
		clmhclmIO.setFormat(formatsInner.clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(clmhclmIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Initial read of the Contract Header.                        *
	* </pre>
	*/
protected void readContractHeader1100()
	{
		readContractHeader1101();
	}

protected void readContractHeader1101()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		chdrlifIO.setCurrto(clmhclmIO.getEffdate());
		chdrlifIO.setValidflag("2");
		chdrlifIO.setFunction("UPDAT");
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Read the status codes for the contract type.                *
	* </pre>
	*/
protected void readStatusCodes1200()
	{
		readStatusCodes1201();
	}

protected void readStatusCodes1201()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Call DATCON3 subroutine.                                    *
	* </pre>
	*/
protected void callDatcons1300()
	{
		callDatcons1301();
		datcon11350();
	}

protected void callDatcons1301()
	{
		/* Get the frequency Factor from DATCON3 for Regular premiums.*/
		if (isEQ(chdrlifIO.getBillfreq(), "00")) {
			datcon3rec.freqFactor.set(1);
			return ;
		}
		datcon3rec.intDate1.set(chdrlifIO.getCcdate());
		datcon3rec.intDate2.set(chdrlifIO.getBillcd());
		datcon3rec.frequency.set(chdrlifIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon3rec.datcon3Rec);
			xxxxFatalError();
		}
	}

protected void datcon11350()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

protected void pendingIncrease1400()
	{
		/*BEGNH*/
		/* Check for any pending Increase records (Validflag = '1')     */
		/* for the contract and delete them.                            */
		incrmjaIO.setParams(SPACES);
		incrmjaIO.setChdrcoy(atmodrec.company);
		incrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		incrmjaIO.setLife(ZERO);
		incrmjaIO.setCoverage(ZERO);
		incrmjaIO.setRider(ZERO);
		incrmjaIO.setPlanSuffix(ZERO);
		incrmjaIO.setFunction(varcom.begnh);
		incrmjaIO.setFormat(formatsInner.incrmjarec);
		while ( !(isEQ(incrmjaIO.getStatuz(), varcom.endp))) {
			deletePendingIncrs1450();
		}

		/*EXIT*/
	}

protected void deletePendingIncrs1450()
	{
		delete1460();
	}

protected void delete1460()
	{
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)
		&& isNE(incrmjaIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrStatuz.set(incrmjaIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(incrmjaIO.getParams());
			xxxxFatalError();
		}
		/* If the end of the file has been reached, leave the section.  */
		if (isEQ(incrmjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* If the record retrieved is not for the correct contract,     */
		/* rewrite the record to release it, move ENDP to the file      */
		/* status and leave the section.                                */
		if (isNE(incrmjaIO.getChdrcoy(), atmodrec.company)
		|| isNE(incrmjaIO.getChdrnum(), wsaaPrimaryChdrnum)) {
			incrmjaIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, incrmjaIO);
			if (isNE(incrmjaIO.getStatuz(), varcom.oK)) {
				sysrSyserrRecInner.sysrStatuz.set(incrmjaIO.getStatuz());
				sysrSyserrRecInner.sysrParams.set(incrmjaIO.getParams());
				xxxxFatalError();
			}
			incrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		/* Update the COVR record for this pending increase, setting    */
		/* the indexation indication to spaces to force Pending         */
		/* Increases to reprocess the component.                        */
		resetCovrIndexation1500();
		/* Delete the record then move NEXTR to the function to look    */
		/* for the next record.                                         */
		incrmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(incrmjaIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(incrmjaIO.getParams());
			xxxxFatalError();
		}
		incrmjaIO.setFunction(varcom.nextr);
	}

protected void resetCovrIndexation1500()
	{
		readh1510();
	}

protected void readh1510()
	{
		//covrIO.setParams(SPACES);
//		covrIO.setChdrcoy(incrmjaIO.getChdrcoy());
//		covrIO.setChdrnum(incrmjaIO.getChdrnum());
//		covrIO.setLife(incrmjaIO.getLife());
//		covrIO.setCoverage(incrmjaIO.getCoverage());
//		covrIO.setRider(incrmjaIO.getRider());
//		covrIO.setPlanSuffix(incrmjaIO.getPlanSuffix());
//		covrIO.setFunction(varcom.readh);
//		covrIO.setFormat(formatsInner.covrrec);
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(), varcom.oK)) {
//			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
//			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
//			xxxxFatalError();
//		}
//		covrIO.setIndexationInd(SPACES);
//		covrIO.setFunction(varcom.rewrt);
//		covrIO.setFormat(formatsInner.covrrec);
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(), varcom.oK)) {
//			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
//			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
//			xxxxFatalError();
//		}
		covrIO = covrpfDAO.getCovrRecord(incrmjaIO.getChdrcoy().toString(), incrmjaIO.getChdrnum().toString(), incrmjaIO.getLife().toString(), incrmjaIO.getCoverage().toString(), incrmjaIO.getRider().toString(), incrmjaIO.getPlanSuffix().toInt(),"1");
		if(covrIO == null)
		{
			sysrSyserrRecInner.sysrStatuz.set(Varcom.mrnf);
			sysrSyserrRecInner.sysrParams.set(incrmjaIO.getChdrcoy().toString()+incrmjaIO.getChdrnum().toString()+incrmjaIO.getLife().toString()+incrmjaIO.getCoverage().toString()+incrmjaIO.getRider().toString()+incrmjaIO.getPlanSuffix().toInt());
			xxxxFatalError();
		}
		covrIO.setIndexationInd(SPACE);
		covrpfDAO.updateCovrIndexationind(covrIO);
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/
protected void process2000()
	{
		/*PROCESS*/
		processContractHeader2100();
		processPayr3600();
		processLifeRecords2200();
		callTotloan2400();
		processPoldebt2450();
		//ILIFE-1137
		//start ILIFE-1137
		postUnpaidPrem4000();
		postExcessPrem4100();
		//end ILIFE-1137
		processCovers2500();
		processCashAccounts3700();
		processClaimDetails2700();
		/*EXIT*/
	}

	/**
	* <pre>
	* Process the contract header.                                *
	* </pre>
	*/
protected void processContractHeader2100()
	{
		/*PROCESS-CONTRACT-HEADER*/
		updateContractStatus2110();
		updatePremiumStatus2120();
		if(CMDTH010Permission){
		updateClaimStatus2130();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Process the contract header for Contract Status.            *
	* </pre>
	*/
protected void updateClaimStatus2130()
	{
		readT6693Table2700();
	}

	protected void readT6693Table2700(){

		cattpf = cattpfDAO.selectRecords(chdrlifIO.getChdrcoy().toString(),chdrlifIO.getChdrnum().toString());
		if(null == cattpf){
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getChdrcoy().toString()+ chdrlifIO.getChdrnum().toString());
			xxxxFatalError();
		}	
		wsaaT6693Paystat.set(cattpf.getClamstat());
		wsaaT6693Crtable.set("****");

		itempf=new Itempf();
		itempf = itemDAO.findItemByItem(atmodrec.company.toString(),t6693,wsaaT6693Key.toString());
		if (null == itempf) {
			sysrSyserrRecInner.sysrParams.set(atmodrec.company.toString()+wsaaT6693Key.toString());
			xxxxFatalError();
		}
		else{
			t6693rec.t6693Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			index1.set(1);
			while ( !(isGT(index1, 12))) {
				findStatus3310();
			}
		}
		int count = cattpfDAO.updateClaimStatus(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString(),wsaaClmstat.toString());	
		clmhpfDAO.updateClaimStatus(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString(),wsaaClmstat.toString());
	}	



	protected void findStatus3310()
	{
		/*FIND-STATUS*/
		if (isEQ(t6693rec.trcode[index1.toInt()], wsaaBatckey.batcBatctrcde)) {
			wsaaClmstat.set(t6693rec.rgpystat[index1.toInt()]);
			index1.set(15);
		}
		index1.add(1);
		/*EXIT*/
	}





	protected void updateContractStatus2110()
	{
		/*UPDATE-CONTRACT-STATUS*/
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setValidflag("1");
		chdrlifIO.setStatcode(t5679rec.setCnRiskStat);
		chdrlifIO.setCurrfrom(clmhclmIO.getEffdate());
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setStatdate(clmhclmIO.getEffdate());
		chdrlifIO.setStattran(chdrlifIO.getTranno());
		/*EXIT*/
	}

	/**
	* <pre>
	* Process the contract header for Premium Status.             *
	* </pre>
	*/
protected void updatePremiumStatus2120()
	{
		updatePremiumStatus2121();
	}

protected void updatePremiumStatus2121()
	{
		if (isEQ(t5679rec.setCnPremStat, SPACES)
		|| isEQ(t5679rec.setSngpCnStat, SPACES)) {
			/*  single or regular premium present on table*/
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(chdrlifIO.getBillchnl(), "00")
			&& isNE(t5679rec.setSngpCnStat, SPACES)) {
				/*   mop on contract  = single payment and single payment on table*/
				chdrlifIO.setPstatcode(t5679rec.setSngpCnStat);
			}
			else {
				if (isNE(t5679rec.setCnPremStat, SPACES)) {
					chdrlifIO.setPstatcode(t5679rec.setCnPremStat);
				}
			}
		}
		chdrlifIO.setPstattran(chdrlifIO.getTranno());
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		chdrlifIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		postAdjustment3500();
	}

	/**
	* <pre>
	* Process the lives.                                          *
	* </pre>
	*/
protected void processLifeRecords2200()
	{
		 List<Lifepf> lifepfList;

		/*LIVES*/
		lifepfList = lifepfDAO.getLifeList(atmodrec.company.toString(), chdrlifIO.getChdrnum().toString());
		
		for (Lifepf life_loc : lifepfList) {
			/*  We update/write records only when valid flag = 1            */
			/*  and record has not been written already by current          */
			/*  transaction */ 
			if (isNE(life_loc.getValidflag(), "1") ||
					isEQ(life_loc.getTranno(), chdrlifIO.getTranno())) {
				continue;
			}
			updateLives2300(life_loc);
		}
		if (!updateLifepflist.isEmpty()){
			lifepfDAO.updateLifeRecord(updateLifepflist,wsaaToday.toInt());
		}
		if (!insertLifepflist.isEmpty()){
			lifepfDAO.insertLifepfList(insertLifepflist);
		}

		/*EXIT*/
	}

	/**
	* <pre>
	* Update life records.                                        *
	* </pre>
	*/
protected void updateLives2300(Lifepf lifeIO)
	{	                                               
		
		/* MOVE WSAA-TRANSACTION-DATE  TO LIFE-TRANSACTION-DATE.        */
		/* MOVE WSAA-TRANSACTION-TIME  TO LIFE-TRANSACTION-TIME.        */
		/* MOVE WSAA-USER              TO LIFE-USER.                    */
		/* MOVE WSAA-TERMID            TO LIFE-TERMID.                  */
			
			// Update lifedata in lifepf table set below fields
			
			Lifepf updatelifedata = new Lifepf(lifeIO);			
			updatelifedata.setValidflag("2");
			updatelifedata.setCurrto(clmhclmIO.getEffdate().toInt());
			updateLifepflist.add(updatelifedata);
			
			// insert new record in lifepf using same lifedata object but overwrite
			// below fields
			
			Lifepf insertlifedata = new Lifepf(lifeIO);
			insertlifedata.setTranno(chdrlifIO.getTranno().toInt());
			insertlifedata.setTransactionDate(wsaaTransactionDate.toInt());
			insertlifedata.setTransactionTime(wsaaTransactionTime.toInt());
			insertlifedata.setUser(wsaaUser.toInt());
			insertlifedata.setTermid(wsaaTermid.toString());
			insertlifedata.setValidflag("1");
			insertlifedata.setCurrto(varcom.vrcmMaxDate.toInt());
			insertlifedata.setCurrfrom(clmhclmIO.getEffdate().toInt());
			if (isEQ(insertlifedata.getJlife(), "00")
			|| isEQ(insertlifedata.getJlife(), "  ")) {
				insertlifedata.setStatcode(t5679rec.setLifeStat.toString());
			}
			else {
				insertlifedata.setStatcode(t5679rec.setJlifeStat.toString());
			}
			insertLifepflist.add(insertlifedata);
	}

protected void callTotloan2400()
	{
		start2400();
	}

	/**
	* <pre>
	*  A call must be made to TOTLOAN to bring interest up to
	*  date.
	* </pre>
	*/
protected void start2400()
	{
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(clmhclmIO.getChdrcoy());
		totloanrec.chdrnum.set(clmhclmIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(clmhclmIO.getEffdate());
		totloanrec.tranno.set(chdrlifIO.getTranno());
		totloanrec.batchkey.set(wsaaBatckey);
		totloanrec.tranTerm.set(varcom.vrcmCompTermid);
		totloanrec.tranDate.set(varcom.vrcmDate);
		totloanrec.tranTime.set(varcom.vrcmTime);
		totloanrec.tranUser.set(varcom.vrcmUser);
		totloanrec.language.set(atmodrec.language);
		/* MOVE 'POST'                 TO TOTL-FUNCTION.                */
		/* Instead of passing 'POST' function to TOTLOAN, now we set    */
		/* set the Post-Flag to 'Y' to HRTOTLON so that it will also    */
		/* post the ACMVs.                                              */
		totloanrec.postFlag.set("Y");
		/* Pass 'LOAN' as the Function to HRTOTLON to calculate only    */
		/* Loan records related to that contract.                       */
		totloanrec.function.set("LOAN");
		/* CALL 'TOTLOAN'              USING TOTL-TOTLOAN-REC.          */
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		/* CALL 'TOTLOAN' USING TOTL-TOTLOAN-REC.               <LA2108>*/
		if (isNE(totloanrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(totloanrec.totloanRec);
			sysrSyserrRecInner.sysrStatuz.set(totloanrec.statuz);
			xxxxFatalError();
		}
		/* Also bring the Interest up for the Cash accounts.            */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(clmhclmIO.getChdrcoy());
		totloanrec.chdrnum.set(clmhclmIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(clmhclmIO.getEffdate());
		totloanrec.tranno.set(chdrlifIO.getTranno());
		totloanrec.batchkey.set(wsaaBatckey);
		totloanrec.tranTerm.set(varcom.vrcmCompTermid);
		totloanrec.tranDate.set(varcom.vrcmDate);
		totloanrec.tranTime.set(varcom.vrcmTime);
		totloanrec.tranUser.set(varcom.vrcmUser);
		totloanrec.language.set(atmodrec.language);
		totloanrec.function.set("CASH");
		totloanrec.postFlag.set("Y");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(totloanrec.totloanRec);
			sysrSyserrRecInner.sysrStatuz.set(totloanrec.statuz);
			xxxxFatalError();
		}
	}

protected void processPoldebt2450()
	{
		start2450();
	}

protected void start2450()
	{
		/* Read table T5645 in order to obtain the sub account details *   */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("P5256");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(clmhclmIO.getChdrcoy());
		descIO.setDesctabl(t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("P5256");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(descIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(clmhclmIO.getTdbtamt(), ZERO)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(t5645rec.sacscode05);
		lifacmvrec.sacstyp.set(t5645rec.sacstype05);
		lifacmvrec.glcode.set(t5645rec.glmap05);
		lifacmvrec.glsign.set(t5645rec.sign05);
		lifacmvrec.contot.set(t5645rec.cnttot05);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec.origamt.set(clmhclmIO.getTdbtamt());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.effdate.set(clmhclmIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(clmhclmIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

//start ILIFE-1137
protected void postUnpaidPrem4000()
{
	readTabT5688250();
	if (isNE(t5688rec.dflexmeth,SPACES)){		
		dthclmflxRec.chdrChdrcoy.set(chdrlifIO.getChdrcoy());
		dthclmflxRec.chdrChdrnum.set(chdrlifIO.getChdrnum());			
		dthclmflxRec.ptdate.set(chdrlifIO.ptdate);
		dthclmflxRec.dtofdeath.set(clmhclmIO.dtofdeath);
		dthclmflxRec.btdate.set(chdrlifIO.btdate);
		dthclmflxRec.cnttype.set(chdrlifIO.cnttype);
		dthclmflxRec.register.set(chdrlifIO.getRegister());
		dthclmflxRec.cntcurr.set(chdrlifIO.getCntcurr());	
		dthclmflxRec.effdate.set(chdrlifIO.getCurrfrom());
		callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
		if (isNE(dthclmflxRec.status, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(dthclmflxRec.dthclmflxrec);
			sysrSyserrRecInner.sysrStatuz.set(dthclmflxRec.status);				
			xxxxFatalError();
		}
		post4000();
	}	
}

protected void post4000()
{
	if (isEQ(dthclmflxRec.nextinsamt, ZERO)) {
		return ;
	}
	lifacmvrec.rcamt.set(ZERO);
	lifacmvrec.contot.set(ZERO);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.frcdate.set(ZERO);
	lifacmvrec.effdate.set(ZERO);
	lifacmvrec.origamt.set(ZERO);
	lifacmvrec.tranno.set(ZERO);
	lifacmvrec.jrnseq.set(ZERO);
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.transactionDate.set(ZERO);
	lifacmvrec.transactionTime.set(ZERO);
	lifacmvrec.user.set(ZERO);
	lifacmvrec.function.set("PSTW");
	lifacmvrec.batckey.set(wsaaBatckey);
	lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
	lifacmvrec.tranno.set(chdrlifIO.getTranno());
	lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaPost9.toInt()]);
	lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaPost9.toInt()]);
	lifacmvrec.glcode.set(t5645rec.glmap[wsaaPost9.toInt()]);
	lifacmvrec.glsign.set(t5645rec.sign[wsaaPost9.toInt()]);
	lifacmvrec.contot.set(t5645rec.cnttot[wsaaPost9.toInt()]);
	lifacmvrec.jrnseq.set(ZERO);
	lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
	lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
	lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
	lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
	lifacmvrec.origamt.set(dthclmflxRec.nextinsamt);
	lifacmvrec.genlcur.set(SPACES);
	lifacmvrec.genlcoy.set(atmodrec.company);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.postyear.set(SPACES);
	lifacmvrec.postmonth.set(SPACES);
	lifacmvrec.tranref.set(chdrlifIO.getTranno());
	lifacmvrec.trandesc.set(descIO.getShortdesc());
	lifacmvrec.effdate.set(wsaaToday);
	lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
	lifacmvrec.termid.set(wsaaTermid);
	lifacmvrec.user.set(wsaaUser);
	lifacmvrec.transactionTime.set(wsaaTransactionTime);
	lifacmvrec.transactionDate.set(wsaaTransactionDate);
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz, varcom.oK)) {
		sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
		xxxxFatalError();
	}
}

protected void postExcessPrem4100()
{
	readTabT5688250();
	if (isNE(t5688rec.dflexmeth,SPACES)){
		dthclmflxRec.chdrChdrcoy.set(chdrlifIO.getChdrcoy());
		dthclmflxRec.chdrChdrnum.set(chdrlifIO.getChdrnum());			
		dthclmflxRec.ptdate.set(chdrlifIO.ptdate);
		dthclmflxRec.dtofdeath.set(clmhclmIO.dtofdeath);
		dthclmflxRec.btdate.set(chdrlifIO.btdate);
		dthclmflxRec.cnttype.set(chdrlifIO.cnttype);
		dthclmflxRec.register.set(chdrlifIO.getRegister());
		dthclmflxRec.cntcurr.set(chdrlifIO.getCntcurr());	
		dthclmflxRec.effdate.set(chdrlifIO.getCurrfrom());
		callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
		if (isNE(dthclmflxRec.status, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(dthclmflxRec.dthclmflxrec);
			sysrSyserrRecInner.sysrStatuz.set(dthclmflxRec.status);				
			xxxxFatalError();
		}
		post4100();
	}	
}

protected void post4100()
{
	if (isEQ(dthclmflxRec.susamt, ZERO)) {
		return ;
	}
	lifacmvrec.rcamt.set(ZERO);
	lifacmvrec.contot.set(ZERO);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.frcdate.set(ZERO);
	lifacmvrec.effdate.set(ZERO);
	lifacmvrec.origamt.set(ZERO);
	lifacmvrec.tranno.set(ZERO);
	lifacmvrec.jrnseq.set(ZERO);
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.transactionDate.set(ZERO);
	lifacmvrec.transactionTime.set(ZERO);
	lifacmvrec.user.set(ZERO);
	lifacmvrec.function.set("PSTW");
	lifacmvrec.batckey.set(wsaaBatckey);
	lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
	lifacmvrec.tranno.set(chdrlifIO.getTranno());
	lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaPost10.toInt()]);
	lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaPost10.toInt()]);
	lifacmvrec.glcode.set(t5645rec.glmap[wsaaPost10.toInt()]);
	lifacmvrec.glsign.set(t5645rec.sign[wsaaPost10.toInt()]);
	lifacmvrec.contot.set(t5645rec.cnttot[wsaaPost10.toInt()]);
	lifacmvrec.jrnseq.set(ZERO);
	lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
	lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
	lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
	lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
	lifacmvrec.origamt.set(dthclmflxRec.susamt);
	lifacmvrec.genlcur.set(SPACES);
	lifacmvrec.genlcoy.set(atmodrec.company);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.postyear.set(SPACES);
	lifacmvrec.postmonth.set(SPACES);
	lifacmvrec.tranref.set(chdrlifIO.getTranno());
	lifacmvrec.trandesc.set(descIO.getShortdesc());
	lifacmvrec.effdate.set(wsaaToday);
	lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
	lifacmvrec.termid.set(wsaaTermid);
	lifacmvrec.user.set(wsaaUser);
	lifacmvrec.transactionTime.set(wsaaTransactionTime);
	lifacmvrec.transactionDate.set(wsaaTransactionDate);
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz, varcom.oK)) {
		sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
		xxxxFatalError();
	}
}

protected void readTabT5688250()
{
	read251();
}

protected void read251()
{
	itdmIO.setItemcoy(clmhclmIO.getChdrcoy());
	itdmIO.setItemtabl(t5688);
	itdmIO.setItempfx("IT");
	itdmIO.setItemitem(clmhclmIO.getCnttype());
	itdmIO.setItmfrm(wsaaToday);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
		xxxxFatalError();
	}
	if (isNE(itdmIO.getItemcoy(), clmhclmIO.getChdrcoy())
	|| isNE(itdmIO.getItemtabl(), t5688)
	|| isNE(itdmIO.getItemitem(), clmhclmIO.getCnttype())
	|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
		itdmIO.setItemitem(clmhclmIO.getCnttype());
		sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
		sysrSyserrRecInner.sysrStatuz.set(itdmIO.getStatuz());		
		xxxxFatalError();
	}
	t5688rec.t5688Rec.set(itdmIO.getGenarea());
}
//end ILIFE-1137

	/**
	* <pre>
	* Process the related COVERS.                                 *
	* </pre>
	*/
protected void processCovers2500()
	{
		/*COVERS*/
		/* Read coverages.*/
//		covrIO.setParams(SPACES);
//		covrIO.setChdrcoy(atmodrec.company);
//		covrIO.setChdrnum(chdrlifIO.getChdrnum());
//		covrIO.setPlanSuffix(0);
//		covrIO.setFunction(varcom.begn);
//		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
//			updateComponent2600();
//		}
	List<String> data = new ArrayList<>();
	data.add(chdrlifIO.getChdrnum().toString());
	Map<String,List<Covrpf>> covrMap = covrpfDAO.searchCovrMap(atmodrec.company.toString(),data);
	Iterator<Entry<String, List<Covrpf>>> keyit = covrMap.entrySet().iterator();
	while(keyit.hasNext())
	{
		List<Covrpf> covr = keyit.next().getValue();
		Iterator<Covrpf> covrit = covr.iterator();
	    while(covrit.hasNext())
	    {
	    	covrIO = covrit.next();
	    	updateComponent2600();
	    }


}
		/*EXIT*/
	}

protected void updateComponent2600()
	{
		update2610();
	}

protected void update2610()
	{
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(), varcom.oK)
//		&& isNE(covrIO.getStatuz(), varcom.endp)) {
//			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
//			xxxxFatalError();
//		}
//		if (isNE(covrIO.getChdrcoy(), atmodrec.company)
//		|| isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum())
//		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
//			covrIO.setStatuz(varcom.endp);
//			return ;
//		}
//		if (isEQ(covrIO.getValidflag(), "2")) {
//			covrIO.setFunction(varcom.nextr);
//			return ;
//		}
		processReassurance2950();
		/* Update the Covr record with a valid flag of 2*/
		covrIO.setValidflag("2");
		covrIO.setCurrto(clmhclmIO.getEffdate().toInt());
		covrpfDAO.updateCovrValidAndCurrtoFlag(covrIO);
//		covrIO.setFunction(varcom.updat);
//		covrIO.setFormat(formatsInner.covrrec);
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(), varcom.oK)) {
//			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
//			xxxxFatalError();
//		}
		update2610CustomerSpecific1();
		/* If a historical increase record exists, write a new record   */
		/* for the transaction with updated status codes.               */
		incrCheck3700();
		/* Access the table to initialise the COVR fields.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(covrIO.getCrtable());
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate.toInt());
		covrIO.setCurrfrom(clmhclmIO.getEffdate().toInt());
		covrIO.setTranno(chdrlifIO.getTranno().toInt());
		covrIO.setStatcode(t5679rec.setCovRiskStat.toString());
		if (isEQ(covrIO.getRider(), "00")
		|| isEQ(covrIO.getRider(), SPACES)) {
			covrIO.setStatcode(t5679rec.setCovRiskStat.toString());
		}
		else {
			covrIO.setStatcode(t5679rec.setRidRiskStat.toString());
		}
		/* IF  CHDRLIF-BILLFREQ        NOT = '00'                       */
		if (isNE(payrIO.getBillfreq(), "00")
		&& isNE(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrIO.getRider(), SPACES)
			|| isEQ(covrIO.getRider(), "00")) {
				covrIO.setPstatcode(t5679rec.setCovPremStat.toString());
			}
			else {
				covrIO.setPstatcode(t5679rec.setRidPremStat.toString());
			}
		}
		else {
			if (isEQ(covrIO.getRider(), SPACES)
			|| isEQ(covrIO.getRider(), "00")) {
				covrIO.setPstatcode(t5679rec.setSngpCovStat.toString());
			}
			else {
				/*        MOVE T5679-SET-SNGP-COV-STAT                          */
				/*                             TO COVR-PSTATCODE.               */
				covrIO.setPstatcode(t5679rec.setSngpRidStat.toString());
			}
		}
		covrIO.setTransactionDate(wsaaTransactionDate.toInt());
		covrIO.setTransactionTime(wsaaTransactionTime.toInt());
		covrIO.setUser(wsaaUser.toInt());
		covrIO.setTermid(wsaaTermid.toString());
//		covrIO.setFunction(varcom.writr);
//		covrIO.setFormat(formatsInner.covrrec);
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(), varcom.oK)) {
//			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
//			xxxxFatalError();
//		}
		List<Covrpf> data = new ArrayList<>();
		data.add(covrIO);
		covrpfDAO.insertCovrpfList(data);
		update2610CustomerSpecific2();
//		covrIO.setFunction(varcom.nextr);
	}

protected void update2610CustomerSpecific1(){
	
}
protected void update2610CustomerSpecific2(){
	
}

	/**
	* <pre>
	* Process related claims details records                      *
	* </pre>
	*/
protected void processClaimDetails2700()
	{
		/*PROCESS-CLAIMS*/
		clmdclmIO.setParams(SPACES);
		clmdclmIO.setChdrcoy(atmodrec.company);
		clmdclmIO.setChdrnum(chdrlifIO.getChdrnum());
		setLifeAsClmdParam();
		clmdclmIO.setFunction(varcom.begn);
		clmdclmIO.setFormat(formatsInner.clmdclmrec);
		while ( !(isEQ(clmdclmIO.getStatuz(), varcom.endp))) {
			updateClaims2800();
		}

		/*EXIT*/
	}

protected void updateClaims2800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update2800();
				case nextr2830:
					nextr2830();
				case exit2849:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update2800()
	{
	    String moniesDate = "E";
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.oK)
		&& isNE(clmdclmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(clmdclmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(clmdclmIO.getStatuz(), varcom.endp)
		|| isNE(chdrlifIO.getChdrnum(), clmdclmIO.getChdrnum())
		|| isNE(atmodrec.company, clmdclmIO.getChdrcoy())) {
			clmdclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2849);
		}
		readTables2850();
		if (isEQ(t5687rec.dcmeth, SPACES)) {
			goTo(GotoLabel.nextr2830);
		}
		dthcpy.chdrChdrcoy.set(clmdclmIO.getChdrcoy());
		dthcpy.chdrChdrnum.set(clmdclmIO.getChdrnum());
		dthcpy.lifeLife.set(clmdclmIO.getLife());
		/* Use the real fund currency, CLMDCLM-CNSTCUR not necessary has   */
		/* the correct currency because it has been set up as the paying   */
		/* out currency.                                                   */
		if (isNE(clmdclmIO.getVirtualFund(), SPACES)) {
			readT55153400();
		}
		else {
			dthcpy.currcode.set(clmdclmIO.getCnstcur());
		}
		dthcpy.element.set(clmdclmIO.getVirtualFund());
		dthcpy.lifeJlife.set(clmdclmIO.getJlife());
		dthcpy.covrCoverage.set(clmdclmIO.getCoverage());
		dthcpy.covrRider.set(clmdclmIO.getRider());
		dthcpy.crtable.set(clmdclmIO.getCrtable());
		dthcpy.effdate.set(clmhclmIO.getEffdate());
		dthcpy.status.set(SPACES);
		dthcpy.tranno.set(clmdclmIO.getTranno());
		/* Use the paying out currency as the contract currency.           */
		 
		if (isNE(clmhclmIO.getCurrcd(), SPACES)) {
			dthcpy.contractCurr.set(clmhclmIO.getCurrcd());
		}
		else {
			dthcpy.contractCurr.set(chdrlifIO.getCntcurr());
		}
		//ILIFE-5462 Start
		readTabT6647();
		if(isEQ(t6647rec.moniesDate, "D")){
			moniesDate = "D";  
		}
		
		if(moniesDate.equalsIgnoreCase("E")){
			dthcpy.effdate.set(clmhclmIO.getEffdate());
		}
		
		if(moniesDate.equalsIgnoreCase("D")){
			dthcpy.effdate.set(clmhclmIO.getDtofdeath());
		}
		//ILIFE-5462 End
		dthcpy.estimatedVal.set(clmdclmIO.getEstMatValue());
		dthcpy.actualVal.set(clmdclmIO.getActvalue());
		dthcpy.fieldType.set(clmdclmIO.getFieldType());
		/*  MOVE CLMDCLM-RIIND          TO DTHP-TYPE.                    */
		/*  MOVE CLMDCLM-RIIND          TO DTHP-RIIND.              <003>*/
		dthcpy.cnttype.set(clmhclmIO.getCnttype());
		dthcpy.batckey.set(wsaaBatckey.batcFileKey);
		dthcpy.termid.set(wsaaTermid);
		dthcpy.date_var.set(wsaaTransactionDate);
		dthcpy.time.set(wsaaTransactionTime);
		dthcpy.user.set(wsaaUser);
		dthcpy.language.set(atmodrec.language);
		dthcpy.status.set("****");
		callDeathMethod2900();
	}

protected void nextr2830()
	{
		clmdclmIO.setFunction(varcom.nextr);
	}

protected void readTables2850()
	{
		read2851();
	}

protected void read2851()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(clmdclmIO.getCrtable());
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), clmdclmIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(clmdclmIO.getCrtable());
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5687rec.dcmeth, SPACES)) {
			t6598rec.procesprog.set(SPACES);
			return ;
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t6598);
		itdmIO.setItemitem(t5687rec.dcmeth);
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), t6598)
		|| isNE(itdmIO.getItemitem(), t5687rec.dcmeth)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t5687rec.dcmeth);
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(g371);
			xxxxFatalError();
		}
		else {
			t6598rec.t6598Rec.set(itdmIO.getGenarea());
		}
	}

protected void callDeathMethod2900()
	{
		read2910();
	}

protected void read2910()
	{
		if (isEQ(t6598rec.procesprog, SPACES)) {
			return ;
		}
		callProgram(t6598rec.procesprog, dthcpy.deathRec);
		if (isEQ(dthcpy.status, varcom.bomb)) {
			sysrSyserrRecInner.sysrStatuz.set(dthcpy.status);
			xxxxFatalError();
		}
		if (isEQ(dthcpy.status, varcom.endp)) {
			return ;
		}
		if (isNE(dthcpy.status, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(dthcpy.status);
			xxxxFatalError();
		}
	}

protected void processReassurance2950()
	{
		trmreas2951();
	}

protected void trmreas2951()
	{
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("TRMR");
		trmreasrec.chdrcoy.set(covrIO.getChdrcoy());
		trmreasrec.chdrnum.set(covrIO.getChdrnum());
		trmreasrec.life.set(covrIO.getLife());
		trmreasrec.coverage.set(covrIO.getCoverage());
		trmreasrec.rider.set(covrIO.getRider());
		trmreasrec.planSuffix.set(covrIO.getPlanSuffix());
		trmreasrec.crtable.set(covrIO.getCrtable());
		trmreasrec.cnttype.set(chdrlifIO.getCnttype());
		trmreasrec.polsum.set(chdrlifIO.getPolsum());
		trmreasrec.effdate.set(clmhclmIO.getEffdate());
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(chdrlifIO.getTranno());
		trmreasrec.language.set(atmodrec.language);
		trmreasrec.billfreq.set(chdrlifIO.getBillfreq());
		trmreasrec.ptdate.set(chdrlifIO.getPtdate());
		trmreasrec.origcurr.set(chdrlifIO.getCntcurr());
		trmreasrec.acctcurr.set(covrIO.getPremCurrency());
		trmreasrec.crrcd.set(covrIO.getCrrcd());
		trmreasrec.convUnits.set(covrIO.getConvertInitialUnits());
		trmreasrec.jlife.set(covrIO.getJlife());
		trmreasrec.singp.set(covrIO.getSingp());
		trmreasrec.oldSumins.set(covrIO.getSumins());
		trmreasrec.newSumins.set(ZERO);
		trmreasrec.pstatcode.set(covrIO.getPstatcode());
		trmreasrec.clmPercent.set(100);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(trmreasrec.statuz);
			sysrSyserrRecInner.sysrParams.set(trmreasrec.trracalRec);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Housekeeping prior to termination.                          *
	* </pre>
	*/
protected void housekeepingTerminate3000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		ptrnTransaction3100();
		batchHeader3200();
		dryProcessing5000();
		a200DelUnderwritting();
		releaseSoftlock3300();
		/*EXIT*/
	}

	/**
	* <pre>
	* Write PTRN transaction.                                     *
	* </pre>
	*/
protected void ptrnTransaction3100()
	{
		ptrnTransaction3101();
	}

protected void ptrnTransaction3101()
	{
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		updateValidflagCustomerSpecific();
		ptrnIO.setTransactionDate(0);
		ptrnIO.setTransactionTime(0);
		ptrnIO.setUser(0);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Write batch header transaction.                             *
	* </pre>
	*/
protected void batchHeader3200()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock3300()
	{
		releaseSoftlock3301();
	}

protected void releaseSoftlock3301()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(sftlockrec.sftlockRec);
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void readT55153400()
	{
		para3401();
	}

protected void para3401()
	{
		/* Read the Table T5515 to find the real Fund currency.            */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(clmdclmIO.getVirtualFund());
		itemIO.setFunction("READR");
		itemIO.setFormat("ITEMREC");
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), "****"))
		&& (isNE(itemIO.getStatuz(), "MRNF"))) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), "MRNF")) {
			dthcpy.currcode.set(clmdclmIO.getCnstcur());
			return ;
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		dthcpy.currcode.set(t5515rec.currcode);
	}

protected void postAdjustment3500()
	{
		para3501();
	}

protected void para3501()
	{
		/*  post new claims amount to sub-account no 3 AND 4               */
		/* Move it down since we need to get Accounting rules from      */
		/* T5645 eventhough there is no Adjustments.                    */
		/* IF CLMHCLM-OTHERADJST              = ZERO                    */
		/*     GO TO 3590-EXIT.                                         */
		/* Read table T5645 in order to obtain the sub account details *   */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("P5256");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(clmhclmIO.getChdrcoy());
		descIO.setDesctabl(t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("P5256");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(descIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(clmhclmIO.getOtheradjst(), ZERO)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec.origamt.set(clmhclmIO.getOtheradjst());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.effdate.set(clmhclmIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(clmhclmIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.jrnseq.set(1);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void processPayr3600()
	{
		start3605();
	}

protected void start3605()
	{
		/* Update the existing inforce PAYR record with a valid flag of    */
		/* "2" and effective date of the death registration.               */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			xxxxFatalError();
		}
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			xxxxFatalError();
		}
		start3605CustomerSpecific();
		/* Create a new PAYR record with a valid flag of "1", effective    */
		/* date of death registration and a premium status as per CHDR.    */
		payrIO.setTranno(chdrlifIO.getTranno());
		payrIO.setValidflag("1");
		payrIO.setEffdate(clmhclmIO.getEffdate());
		payrIO.setPstatcode(chdrlifIO.getPstatcode());
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			xxxxFatalError();
		}
	}

protected void start3605CustomerSpecific(){
	
}

protected void processCashAccounts3700()
	{
		/*START*/
		writeOffCashAccounts3800();
		allocateToLoans3900();
		/* If there is a residual amount left after paying off all      */
		/* the Loans, then post the amount to the Pending Death         */
		/* Account.                                                     */
		if (isGT(wsaaNetVal, 0)) {
			postResidualAmount4000();
		}
		/*EXIT*/
	}

protected void writeOffCashAccounts3800()
	{
		start3810();
	}

protected void start3810()
	{
		/* First load in the 'common' fields for the linkage areas.     */
		/* Use LOANPYMT sub-routine to write off the balances in        */
		/* the Cash accounts.                                           */
		wsaaTranTime.set(wsaaTransactionTime);
		wsaaTranDate.set(wsaaTransactionDate);
		wsaaTranUser.set(wsaaUser);
		wsaaTranTermid.set(wsaaTermid);
		cashedrec.cashedRec.set(SPACES);
		cashedrec.transeq.set(0);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(wsaaBatckey);
		cashedrec.doctNumber.set(clmhclmIO.getChdrnum());
		cashedrec.doctCompany.set(clmhclmIO.getChdrcoy());
		cashedrec.trandate.set(clmhclmIO.getEffdate());
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(clmhclmIO.getCurrcd());
		cashedrec.dissrate.set(ZERO);
		cashedrec.trandesc.set(descIO.getLongdesc());
		cashedrec.genlCompany.set(clmhclmIO.getChdrcoy());
		cashedrec.genlCurrency.set(SPACES);
		cashedrec.chdrcoy.set(clmhclmIO.getChdrcoy());
		cashedrec.chdrnum.set(clmhclmIO.getChdrnum());
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(clmhclmIO.getChdrcoy());
		wsaaTranEntity.set(clmhclmIO.getChdrnum());
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.tranno.set(chdrlifIO.getTranno());
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.language.set(atmodrec.language);
		/* Call the LOANPYMT subroutine for each Cash account found     */
		/* found in T5645.                                              */
		wsaaNetVal.set(clmhclmIO.getZrcshamt());
		for (wsaaSub1.set(7); !(isGT(wsaaSub1, 8)
		|| isEQ(wsaaNetVal, 0)); wsaaSub1.add(1)){
			processLoans3950();
		}
		/* Retrieve JRNSEQ back from LOANPYMTs subroutine so we can     */
		/* keep sequence going.                                         */
		wsaaJrnseq.set(cashedrec.transeq);
	}

protected void allocateToLoans3900()
	{
		start3190();
	}

protected void start3190()
	{
		/* First load in the 'common' fields for the linkage areas.     */
		wsaaTranTime.set(wsaaTransactionTime);
		wsaaTranDate.set(wsaaTransactionDate);
		wsaaTranUser.set(wsaaUser);
		wsaaTranTermid.set(wsaaTermid);
		cashedrec.cashedRec.set(SPACES);
		compute(cashedrec.transeq, 0).set(add(1, wsaaJrnseq));
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(wsaaBatckey);
		cashedrec.doctNumber.set(clmhclmIO.getChdrnum());
		cashedrec.doctCompany.set(clmhclmIO.getChdrcoy());
		cashedrec.trandate.set(clmhclmIO.getEffdate());
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(clmhclmIO.getCurrcd());
		cashedrec.dissrate.set(ZERO);
		cashedrec.trandesc.set(descIO.getLongdesc());
		cashedrec.genlCompany.set(clmhclmIO.getChdrcoy());
		cashedrec.genlCurrency.set(SPACES);
		cashedrec.chdrcoy.set(clmhclmIO.getChdrcoy());
		cashedrec.chdrnum.set(clmhclmIO.getChdrnum());
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(clmhclmIO.getChdrcoy());
		wsaaTranEntity.set(clmhclmIO.getChdrnum());
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.tranno.set(chdrlifIO.getTranno());
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.language.set(atmodrec.language);
		/* Call the LOANPYMT subroutine for each Loan account found     */
		/* found in T5645.                                              */
		wsaaNetVal.set(clmhclmIO.getZrcshamt());
		for (wsaaSub1.set(3); !(isGT(wsaaSub1, 6)
		|| isEQ(wsaaNetVal, 0)); wsaaSub1.add(1)){
			processLoans3950();
		}
		/* Retrieve JRNSEQ back from LOANPYMTs subroutine so we can     */
		/* keep sequence going.                                         */
		wsaaJrnseq.set(cashedrec.transeq);
	}

protected void processLoans3950()
	{
		/*START*/
		cashedrec.sacscode.set(t5645rec.sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(t5645rec.sacstype[wsaaSub1.toInt()]);
		cashedrec.genlkey.set(t5645rec.glmap[wsaaSub1.toInt()]);
		cashedrec.sign.set(t5645rec.sign[wsaaSub1.toInt()]);
		cashedrec.origamt.set(wsaaNetVal);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(cashedrec.statuz);
			xxxxFatalError();
		}
		wsaaNetVal.set(cashedrec.docamt);
		/*EXIT*/
	}

protected void postResidualAmount4000()
	{
		/*START*/
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec.origamt.set(wsaaNetVal);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.effdate.set(clmhclmIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(clmhclmIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void incrCheck3700()
	{
		para3710();
	}

protected void para3710()
	{
		incrIO.setDataArea(SPACES);
		incrIO.setChdrcoy(covrIO.getChdrcoy());
		incrIO.setChdrnum(covrIO.getChdrnum());
		incrIO.setLife(covrIO.getLife());
		incrIO.setCoverage(covrIO.getCoverage());
		incrIO.setRider(covrIO.getRider());
		incrIO.setPlanSuffix(covrIO.getPlanSuffix());
		incrIO.setFunction(varcom.readr);
		incrIO.setFormat(formatsInner.incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)
		&& isNE(incrIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrStatuz.set(incrIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(incrIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(incrIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrIO.setValidflag("2");
		if (isNE(t5679rec.setCovPremStat, SPACES)) {
			incrIO.setPstatcode(t5679rec.setCovPremStat);
		}
		if (isNE(t5679rec.setCovRiskStat, SPACES)) {
			incrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		incrIO.setTransactionDate(wsaaTransactionDate);
		incrIO.setTransactionTime(wsaaTransactionTime);
		incrIO.setUser(wsaaUser);
		incrIO.setTermid(wsaaTermid);
		incrIO.setTranno(chdrlifIO.getTranno());
		incrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(incrIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(incrIO.getParams());
			xxxxFatalError();
		}
	}

protected void dryProcessing5000()
	{
		start5010();
	}

protected void start5010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlifIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75085100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75085100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrlifIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		//ILIFE-1219 by gaurav STARTS 
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		//ENDS
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(drypDryprcRecInner.drypDryprcRec);
			sysrSyserrRecInner.sysrStatuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75085100()
	{
		start5110();
	}

protected void start5110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		/*  MOVE COVR-TRANNO            TO LIFS-TRANNO.             <009>*/
		lifsttrrec.tranno.set(chdrlifIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifsttrrec.lifsttrRec);
			sysrSyserrRecInner.sysrStatuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void a200DelUnderwritting()
	{
		a200Ctrl();
	}

protected void a200Ctrl()
	{
		a300BegnLifeio();
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrlifIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrlifIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(crtundwrec.parmRec);
			sysrSyserrRecInner.sysrStatuz.set(crtundwrec.status);
			sysrSyserrRecInner.sysrIomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
	}

protected void a300BegnLifeio()
	{
		a300Ctrl();
	}

protected void a300Ctrl()
	{
		lifeIO.setRecKeyData(SPACES);
		lifeIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (!(isEQ(lifeIO.getStatuz(), varcom.oK)
		&& isEQ(chdrlifIO.getChdrcoy(), lifeIO.getChdrcoy())
		&& isEQ(chdrlifIO.getChdrnum(), lifeIO.getChdrnum()))) {
			lifeIO.setStatuz(varcom.endp);
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifeIO.getStatuz());
			xxxxFatalError();
		}
	}
//ILIFE-5462
protected void readTabT6647(){
	
	wsaaTabBatckey.set(wsaaBatckey.batcBatctrcde);
	wsaaCnttype.set(clmhclmIO.getCnttype());
	
	listT6647 = itemDAO.getAllItemitemByDateFrm("IT",dthcpy.chdrChdrcoy.toString(), t6647,wsaaT6647Key.toString(), dthcpy.effdate.toInt());
	if(listT6647.size()>0) {
		t6647rec.t6647Rec.set(StringUtil.rawToString(listT6647.get(0).getGenarea()));
		
	}/*else {
		srcalcpy.status.set(varcom.mrnf);
		exitProgram();
	}*/
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
protected static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	public FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	public FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData clmhclmrec = new FixedLengthStringData(10).init("CLMHCLMREC");
	private FixedLengthStringData clmdclmrec = new FixedLengthStringData(10).init("CLMDCLMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
	public FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
protected static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	public FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	public FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
	public FixedLengthStringData getSysrParams() {
		return sysrParams;
	}
	public void setSysrParams(FixedLengthStringData sysrParams) {
		this.sysrParams = sysrParams;
	}
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
protected static final class DrypDryprcRecInner {

	public FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	public FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	public Validator onlineMode = new Validator(drypMode, "O");
	public PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	public FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	public FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	public FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	public FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	public FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	public FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	public PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	public PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	public FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	public FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	public PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	public PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	public FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	public FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	public FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	public FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	public FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	public PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	public PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	public PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	public PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	public PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	public PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	public PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	public PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	public PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
	protected void updateValidflagCustomerSpecific() {

	}
	
	protected void setLifeAsClmdParam() {}
	
}
