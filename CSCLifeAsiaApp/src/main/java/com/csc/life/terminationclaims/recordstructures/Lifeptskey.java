package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:08
 * Description:
 * Copybook name: LIFEPTSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifeptskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifeptsFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifeptsKey = new FixedLengthStringData(256).isAPartOf(lifeptsFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifeptsChdrcoy = new FixedLengthStringData(1).isAPartOf(lifeptsKey, 0);
  	public FixedLengthStringData lifeptsChdrnum = new FixedLengthStringData(8).isAPartOf(lifeptsKey, 1);
  	public FixedLengthStringData lifeptsLife = new FixedLengthStringData(2).isAPartOf(lifeptsKey, 9);
  	public FixedLengthStringData lifeptsJlife = new FixedLengthStringData(2).isAPartOf(lifeptsKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifeptsKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifeptsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifeptsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}