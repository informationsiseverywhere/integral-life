package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Vxdcrec extends ExternalData { 
	
	public FixedLengthStringData vxdcRec = new FixedLengthStringData(148);
	public ZonedDecimalData curduntbal = new ZonedDecimalData(13, 5).isAPartOf(vxdcRec, 0);
	public ZonedDecimalData unprocessed = new ZonedDecimalData(13, 5).isAPartOf(vxdcRec, 13);
	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(vxdcRec, 26);
	public FixedLengthStringData dthmth=new FixedLengthStringData(4).isAPartOf(vxdcRec,29);
	public ZonedDecimalData si = new ZonedDecimalData(16, 2).isAPartOf(vxdcRec, 33);
	public ZonedDecimalData fundtotal=new ZonedDecimalData(16,2).isAPartOf(vxdcRec,49);
	public ZonedDecimalData intbonus=new ZonedDecimalData(15,2).isAPartOf(vxdcRec,65);
	public ZonedDecimalData trmbonus=new ZonedDecimalData(15,2).isAPartOf(vxdcRec,80);
	public ZonedDecimalData addbonus=new ZonedDecimalData(15,2).isAPartOf(vxdcRec,95);
	public ZonedDecimalData crrcd = new ZonedDecimalData(8).isAPartOf(vxdcRec, 110);
	
	
	
	public FixedLengthStringData zmortpcts = new FixedLengthStringData(30);
	public PackedDecimalData sacscurbal01 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal02 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal03 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal04 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal05 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal06 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal07 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal08 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal09 = new PackedDecimalData(3,0);
	public PackedDecimalData sacscurbal10 = new PackedDecimalData(3,0);

	public void initialize() {
		COBOLFunctions.initialize(vxdcRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vxdcRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}
