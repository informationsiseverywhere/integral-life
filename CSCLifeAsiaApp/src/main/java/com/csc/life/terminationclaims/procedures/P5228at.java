/*
 * File: P5228at.java
 * Date: 30 August 2009 0:20:42
 * Author: Quipoz Limited
 *
 * Class transformed from P5228AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.ChdrdthTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Clmallrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*AUTHOR.
*REMARKS.
*
*  This AT module  is  called  by the Death Claims Register
*  Death of  a  Life program P5228,  the Contract number is
*  in the AT parameters area as the "primary key".
*
*  AT PROCESSING
*
*  Get all statii
*
*  - obtain the  status  codes  required  by  reading table
*    T5679,    i.e.    the   'Component   Statii   For
*    Transactions'.  This  table is keyed by transaction
*    number.
*
*  Get todays date for DATCON1.
*
*  Read the Claim header record (CLMHCLM) for this contract.
*
*  PROCESS LIVES
*
*  Read the LIFE being processed.
*
*  - set valid flag  to  '2';  set  'Effective  To' date as
*    above and update the record.
*
*  - write a new record as follows:
*
*  - update  the  transaction  number  with the transaction
*    number from  the  contract  header. Update
*    the status code  with  the status code, from T5679,
*    for the  life or joint-life, whichever is currently
*    being processed.  (LIFE-NUMBER = spaces or '00' for
*    single life, equal to '01' for joint life case).
*
*  PROCESS COMPONENTS
*
*  Read all  the Coverage/Rider records (COVR) for this
*  Life and update each COVR record as follows:
*
*  - set the valid  flag to '2', Current To Dates as the
*    effective date.
*
*  - write a new record as follows:
*
*  - update  the  transaction  number  with the transaction
*    number from  the  contract  header. Update the
*    status code with  the  status code from T5679.  Use
*    the regular premium  status code if the the billing
*    frequency, from  the  contract  header, is equal to
*    T5679.
*
*  UPDATE CONTRACT HEADER AND PAYR
*
*    The program will alter the CURRFROM and CURRTO dates
*    and rewrite the old records as a Validflag 2, and the new
*    ones as Validflag 2.
*
*  GENERAL HOUSE-KEEPING
*
*  1) Write a PTRN transaction record:
*
*  - contract key from contract header
*  - transaction number from contract header
*  - transaction effective date equals todays
*  - batch key information from AT linkage.
*
*  2) Update the  batch  header  by  calling  BATCUP with a
*     function of WRITS and the following parameters:
*
*  - transaction count equals 1
*  - all other amounts zero
*  - batch key from AT linkage
*
*  3) Release contract SFTLOCK.
*
*  STATISTICS
*
*  The Statistics Subroutine LIFSTTR is called which produces
*  the appropriate STTR records.
*
****************************************************************
* </pre>
*/
public class P5228at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5228AT");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 16);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-SUBSCRIPTS */
	private PackedDecimalData sub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaCompKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompTrancode = new FixedLengthStringData(4).isAPartOf(wsaaCompKey, 0);
	private FixedLengthStringData wsaaCompCrtable = new FixedLengthStringData(4).isAPartOf(wsaaCompKey, 4);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* ERRORS */
	private static final String f294 = "F294";
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t7508 = "T7508";
		/* FORMATS */
	private static final String chdrdthrec = "CHDRDTHREC";
	private static final String liferec = "LIFEREC";
	private static final String covrrec = "COVRREC";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String payrrec = "PAYRREC";
	private static final String ptrnrec = "PTRNREC";
	private ChdrdthTableDAM chdrdthIO = new ChdrdthTableDAM();
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T7508rec t7508rec = new T7508rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Clmallrec clmallrec = new Clmallrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();

	public P5228at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		updateChdrPayr3000();
		housekeepingTerminate3500();
		statistics4000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		readClaimHeader1050();
		readContractHeader1100();
		readStatusCodes1200();
		/*EXIT*/
	}

	/**
	* <pre>
	* Initial read of the Claim Header.                           *
	* </pre>
	*/
protected void readClaimHeader1050()
	{
		readClaim1051();
	}

protected void readClaim1051()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(atmodrec.company);
		clmhclmIO.setChdrnum(wsaaPrimaryChdrnum);
		clmhclmIO.setFunction(varcom.readr);
		clmhclmIO.setFormat(clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(clmhclmIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	* Initial read of the Contract Header.                        *
	* </pre>
	*/
protected void readContractHeader1100()
	{
		readContractHeader1101();
	}

protected void readContractHeader1101()
	{
		chdrdthIO.setDataArea(SPACES);
		chdrdthIO.setChdrcoy(atmodrec.company);
		chdrdthIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrdthIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrdthIO);
		if (isNE(chdrdthIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrdthIO.getParams());
			fatalError600();
		}
		/* Read the PAYR.*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction("READR");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	* Read the status codes for the contract type.                *
	* </pre>
	*/
protected void readStatusCodes1200()
	{
		readStatusCodes1201();
	}

protected void readStatusCodes1201()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/
protected void process2000()
	{
		/*PROCESS*/
		processLifeRecords2200();
		processCovers2500();
		/*EXIT*/
	}

	/**
	* <pre>
	* Process the lives.                                          *
	* </pre>
	*/
protected void processLifeRecords2200()
	{
		/*LIVES*/
		lifeIO.setParams(SPACES);
		lifeIO.setChdrnum(chdrdthIO.getChdrnum());
		lifeIO.setChdrcoy(atmodrec.company);
		lifeIO.setFormat(liferec);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		while ( !(isEQ(lifeIO.getStatuz(),varcom.endp))) {


			//performance improvement --  atiwari23
//			lifeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//			lifeIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

			updateLives2300();
		}

		/*EXIT*/
	}

	/**
	* <pre>
	* Update life records.                                        *
	* </pre>
	*/
protected void updateLives2300()
	{
		updateRecs2310();
	}

protected void updateRecs2310()
	{
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isNE(lifeIO.getChdrnum(), clmhclmIO.getChdrnum())
		|| isNE(atmodrec.company, lifeIO.getChdrcoy())) {
			lifeIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(lifeIO.getChdrnum(), clmhclmIO.getChdrnum())
		|| isNE(atmodrec.company, lifeIO.getChdrcoy())
		|| isNE(clmhclmIO.getLife(), lifeIO.getLife())
		|| isNE(clmhclmIO.getJlife(), lifeIO.getJlife())) {
			lifeIO.setFunction(varcom.nextr);
			return ;
		}
		lifeIO.setValidflag("2");
		lifeIO.setCurrto(clmhclmIO.getEffdate());
		lifeIO.setFunction(varcom.updat);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			fatalError600();
		}
		lifeIO.setTranno(clmhclmIO.getTranno());
		lifeIO.setTransactionDate(wsaaTransactionDate);
		lifeIO.setTransactionTime(wsaaTransactionTime);
		lifeIO.setUser(wsaaUser);
		lifeIO.setTermid(wsaaTermid);
		lifeIO.setValidflag("1");
		lifeIO.setCurrto(varcom.vrcmMaxDate);
		lifeIO.setCurrfrom(clmhclmIO.getEffdate());
		if ((isEQ(lifeIO.getJlife(), "00")
		|| isEQ(lifeIO.getJlife(), "  "))) {
			if (isNE(t5679rec.setLifeStat, SPACES)) {
				lifeIO.setStatcode(t5679rec.setLifeStat);
			}
		}
		else {
			if (isNE(t5679rec.setJlifeStat, SPACES)) {
				lifeIO.setStatcode(t5679rec.setJlifeStat);
			}
		}
		lifeIO.setFunction(varcom.writr);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			fatalError600();
		}
		lifeIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	* Process the related COVERS.                                 *
	* </pre>
	*/
protected void processCovers2500()
	{
		/*COVERS*/
		/* Read coverages.*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(atmodrec.company);
		covrIO.setChdrnum(chdrdthIO.getChdrnum());
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {


			//performance improvement --  atiwari23
			covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

			updateComponent2600();
		}

		/*EXIT*/
	}

protected void updateComponent2600()
	{
		update2610();
	}

protected void update2610()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(), atmodrec.company)
		|| isNE(covrIO.getChdrnum(), chdrdthIO.getChdrnum())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			covrIO.setFunction(varcom.nextr);
			return ;
		}
		if (isNE(clmhclmIO.getChdrnum(), covrIO.getChdrnum())
		|| isNE(atmodrec.company, covrIO.getChdrcoy())
		|| isNE(clmhclmIO.getLife(), covrIO.getLife())) {
			/*       CLMHCLM-JLIFE         NOT = '00'*/
			covrIO.setFunction(varcom.nextr);
			return ;
		}
		if (isEQ(covrIO.getValidflag(), "2")) {
			covrIO.setFunction(varcom.nextr);
			return ;
		}
		/* Update the Covr record with a valid flag of 2*/
		covrIO.setValidflag("2");
		covrIO.setCurrto(clmhclmIO.getEffdate());
		covrIO.setFunction(varcom.updat);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			fatalError600();
		}
		/* Access the table to initialise the COVR fields.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");



		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(covrIO.getCrtable());
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(f294);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setCurrfrom(clmhclmIO.getEffdate());
		covrIO.setTranno(clmhclmIO.getTranno());
		if ((isEQ(covrIO.getRider(), "00")
		|| isEQ(covrIO.getRider(), SPACES))) {
			if (isNE(t5679rec.setCovRiskStat, SPACES)) {
				covrIO.setStatcode(t5679rec.setCovRiskStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat, SPACES)) {
				covrIO.setStatcode(t5679rec.setRidRiskStat);
			}
		}
		/* IF  CHDRDTH-BILLFREQ       NOT = '00'                        */
		if (isNE(payrIO.getBillfreq(), "00")
		&& isNE(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrIO.getRider(), SPACES)
			|| isEQ(covrIO.getRider(), "00")) {
				if (isNE(t5679rec.setCovRiskStat, SPACES)) {
					covrIO.setPstatcode(t5679rec.setCovPremStat);
				}
			}
			else {
				if (isNE(t5679rec.setRidRiskStat, SPACES)) {
					covrIO.setPstatcode(t5679rec.setRidPremStat);
				}
			}
		}
		else {
			if (isEQ(covrIO.getRider(), SPACES)
			|| isEQ(covrIO.getRider(), "00")) {
				if (isNE(t5679rec.setSngpCovStat, SPACES)) {
					covrIO.setPstatcode(t5679rec.setSngpCovStat);
				}
			}
			else {
				if (isNE(t5679rec.setSngpRidStat, SPACES)) {
					covrIO.setPstatcode(t5679rec.setSngpRidStat);
				}
			}
		}
		covrIO.setTransactionDate(wsaaTransactionDate);
		covrIO.setTransactionTime(wsaaTransactionTime);
		covrIO.setUser(wsaaUser);
		covrIO.setTermid(wsaaTermid);
		covrIO.setFunction(varcom.writr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			fatalError600();
		}
		genericProcessing2700();
		covrIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	* Call Generic Processing Subroutines from T5671.             *
	* </pre>
	*/
protected void genericProcessing2700()
	{
		call2710();
	}

protected void call2710()
	{
		wsaaCompCrtable.set(covrIO.getCrtable());
		wsaaCompTrancode.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5671);
		itemIO.setItemitem(wsaaCompKey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isNE(itemIO.getItemcoy(), atmodrec.company)
		|| isNE(itemIO.getItemtabl(), t5671)
		|| isNE(itemIO.getItemitem(), wsaaCompKey)
		|| isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setStatuz(varcom.mrnf);
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		/* Call all Non-blank Subroutines*/
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			clmallrec.clmallRec.set(SPACES);
			clmallrec.company.set(clmhclmIO.getChdrcoy());
			clmallrec.chdrnum.set(clmhclmIO.getChdrnum());
			clmallrec.life.set(clmhclmIO.getLife());
			clmallrec.jlife.set(clmhclmIO.getJlife());
			clmallrec.coverage.set(covrIO.getCoverage());
			clmallrec.rider.set(covrIO.getRider());
			clmallrec.planSuffix.set(covrIO.getPlanSuffix());
			clmallrec.effdate.set(wsaaToday);
			clmallrec.tranno.set(clmhclmIO.getTranno());
			clmallrec.crtable.set(covrIO.getCrtable());
			clmallrec.statuz.set(varcom.oK);
			clmallrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
			clmallrec.user.set(wsaaUser); //ILIFE-2714 by slakkala
			sub.set(1);
			while ( !(isGT(sub, 4))) {
				if (isNE(t5671rec.subprog[sub.toInt()], SPACES)) {
					callProgram(t5671rec.subprog[sub.toInt()], clmallrec.clmallRec);
					if (isNE(clmallrec.statuz, varcom.oK)) {
						sysrSyserrRecInner.sysrParams.set(clmallrec.clmallRec);
						sysrSyserrRecInner.sysrStatuz.set(clmallrec.statuz);
						fatalError600();
					}
				}
				sub.add(1);
			}

		}
	}

protected void updateChdrPayr3000()
	{
		starts3000();
	}

protected void starts3000()
	{
		/* This section will move the effective date to CURRTO and*/
		/* valid flag 2 the rewrite. It will then move nines to*/
		/* CURRTO and effective date to CURRFROM and write a new*/
		/* record with Valif flag 1.*/
		/* It will do this for both CHDRDTH and PAYR.*/
		chdrdthIO.setDataArea(SPACES);
		chdrdthIO.setChdrcoy(atmodrec.company);
		chdrdthIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrdthIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrdthIO);
		if (isNE(chdrdthIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrdthIO.getParams());
			fatalError600();
		}
		chdrdthIO.setFormat(chdrdthrec);
		chdrdthIO.setCurrto(clmhclmIO.getEffdate());
		chdrdthIO.setValidflag("2");
		chdrdthIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrdthIO);
		if (isNE(chdrdthIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrdthIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrdthIO.getStatuz());
			fatalError600();
		}
		chdrdthIO.setValidflag("1");
		chdrdthIO.setTranno(clmhclmIO.getTranno());
		chdrdthIO.setCurrfrom(clmhclmIO.getEffdate());
		chdrdthIO.setCurrto(99999999);
		chdrdthIO.setFunction(varcom.writr);
		chdrdthIO.setFormat(chdrdthrec);
		SmartFileCode.execute(appVars, chdrdthIO);
		if (isNE(chdrdthIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrdthIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(chdrdthIO.getStatuz());
			fatalError600();
		}
		/* Rewrite old PAYR and write new one:*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFunction("READH");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		payrIO.setFormat(payrrec);
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
		payrIO.setTranno(clmhclmIO.getTranno());
		payrIO.setEffdate(clmhclmIO.getEffdate());
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	* Housekeeping prior to termination.                          *
	* </pre>
	*/
protected void housekeepingTerminate3500()
	{
		/*HOUSEKEEPING-TERMINATE*/
		ptrnTransaction3510();
		batchHeader3520();
		dryProcessing5000();
		releaseSoftlock3530();
		/*EXIT*/
	}

	/**
	* <pre>
	* Write PTRN transaction.                                     *
	* </pre>
	*/
protected void ptrnTransaction3510()
	{
		ptrnTransaction3511();
	}

protected void ptrnTransaction3511()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTransactionDate(0);
		ptrnIO.setTransactionTime(0);
		ptrnIO.setUser(0);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(clmhclmIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrdthIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrdthIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	* Write batch header transaction.                             *
	* </pre>
	*/
protected void batchHeader3520()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock3530()
	{
		releaseSoftlock3531();
	}

protected void releaseSoftlock3531()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrdthIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(sftlockrec.sftlockRec);
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void statistics4000()
	{
		start4010();
	}

protected void start4010()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrdthIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrdthIO.getChdrnum());
		lifsttrrec.tranno.set(clmhclmIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifsttrrec.lifsttrRec);
			sysrSyserrRecInner.sysrStatuz.set(lifsttrrec.statuz);
			fatalError600();
		}
	}

protected void dryProcessing5000()
	{
		start5010();
	}

protected void start5010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrdthIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75085100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75085100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrdthIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrdthIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrdthIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrdthIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrdthIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrdthIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrdthIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrdthIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrdthIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrdthIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrdthIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(drypDryprcRecInner.drypDryprcRec);
			sysrSyserrRecInner.sysrStatuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75085100()
	{
		start5110();
	}

protected void start5110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		/*FATAL*/
		if ((isNE(sysrSyserrRecInner.sysrStatuz, SPACES))
		|| (isNE(sysrSyserrRecInner.sysrSyserrStatuz, SPACES))) {
			sysrSyserrRecInner.sysrSyserrType.set("2");
		}
		else {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void stop699()
	{
		stopRun();
	}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
