package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:16
 * Description:
 * Copybook name: PTSHCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptshclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptshclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ptshclmKey = new FixedLengthStringData(64).isAPartOf(ptshclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptshclmChdrcoy = new FixedLengthStringData(1).isAPartOf(ptshclmKey, 0);
  	public FixedLengthStringData ptshclmChdrnum = new FixedLengthStringData(8).isAPartOf(ptshclmKey, 1);
  	public PackedDecimalData ptshclmTranno = new PackedDecimalData(5, 0).isAPartOf(ptshclmKey, 9);
  	public PackedDecimalData ptshclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ptshclmKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(ptshclmKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptshclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptshclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}