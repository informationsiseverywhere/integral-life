package com.csc.life.terminationclaims.dataaccess.dao.impl;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.LifeAssurdListDAO;
import com.csc.life.terminationclaims.dataaccess.model.Laptpf;
import com.csc.life.terminationclaims.dataaccess.model.Lcdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LifeAssurdListDAOImpl extends BaseDAOImpl<Laptpf> implements LifeAssurdListDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeAssurdListDAOImpl.class);	
	
	
	

	@Override
	public List<Lcdpf> getLifeAssurdList(String lifeNum,String desctabl,String chdrcoy,FixedLengthStringData[]  cnRiskStat,FixedLengthStringData[] covRiskStat,String chdrpfx,String language,String descpfx) {
		 int count2=2;
		 String cnRisk="";
		 String covRisk="";
			StringBuilder sb = new StringBuilder("");
			sb.append("select DISTINCT chdr.UNIQUE_NUMBER,chdr.CNTTYPE,chdr.statcode ,chdr.chdrnum,chdr.occdate, covr.rcesdte, covr.pstatcode,desct.LONGDESC,life.Life "); //ICIL-1286 added DISTINCT	//IBPLIFE-1214
			sb.append("from CHDRPF chdr join COVRPF covr on chdr.chdrnum = covr.CHDRNUM  ");
			sb.append("join LIFEPF life on chdr.chdrnum = life.CHDRNUM and covr.chdrnum = life.chdrnum ");
			sb.append("and covr.life = life.life and (CASE WHEN covr.jlife = '  ' THEN '00' WHEN covr.jlife is null THEN '00' ELSE covr.jlife END) = life.jlife ");
			sb.append("join DESCPF desct on chdr.CNTTYPE=desct.DESCITEM  ");
			sb.append("WHERE chdr.CHDRNUM=life.CHDRNUM and life.LIFCNUM=? and life.VALIDFLAG='1' and life.CHDRCOY=? ");
			
			for(FixedLengthStringData risksta:cnRiskStat){
				 if(risksta!=null&&!risksta.isEmpty()&&!risksta.equals(" ")){
					 cnRisk=cnRisk+"?,"; 
				 }
			 }	
			sb.append("and chdr.STATCODE in ( "+cnRisk.substring(0, cnRisk.length()-1)+")");
			sb.append("and chdr.VALIDFLAG='1' ");
			sb.append("and chdr.CHDRCOY=? ");
			sb.append("and chdr.CHDRPFX=? ");
			 for(FixedLengthStringData covrrisk:covRiskStat){
				 if(covrrisk!=null&&!covrrisk.isEmpty()&&!covrrisk.equals(" ")){
					 
					 covRisk=covRisk+"?,";
				 }
			 }		
			sb.append("and covr.STATCODE in ("+covRisk.substring(0, covRisk.length()-1)+")");
			sb.append("and covr.VALIDFLAG='1' ");
			sb.append("and covr.CHDRCOY=? ");
			sb.append("and chdr.CNTTYPE=desct.DESCITEM ");
			sb.append("and desct.DESCTABL=? ");
			sb.append("and desct.LANGUAGE=? ");
			sb.append("and desct.DESCCOY=? ");
			sb.append("and desct.DESCPFX=? ");
			sb.append("and chdr.VALIDFLAG='1' ");
			sb.append("and chdr.CHDRNUM=covr.CHDRNUM ");
			
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			List<Lcdpf> lcdpfList = new ArrayList<Lcdpf>();
			
			try {
					ps = getPrepareStatement(sb.toString());
					ps.setString(1, lifeNum.trim());
					ps.setString(2, chdrcoy.trim());
					//STATCODE
					for(FixedLengthStringData risksta:cnRiskStat){
						 if(risksta!=null&&!risksta.isEmpty()&&!risksta.equals(" ")){
							 count2++;
							 ps.setString(count2,risksta.trim());
						 }
					 }			
					count2++;
					ps.setString(count2, chdrcoy.trim());
					count2++;
					ps.setString(count2, chdrpfx.trim());
					//STATCODE
					 for(FixedLengthStringData covrrisk:covRiskStat){
						 if(covrrisk!=null&&!covrrisk.isEmpty()&&!covrrisk.equals(" ")){
							 count2++;
							 ps.setString(count2,covrrisk.trim());
						 }
					 }		
					 count2++;
					ps.setString(count2, chdrcoy.trim());
					
					count2++;
					ps.setString(count2, desctabl.trim());
					count2++;
					ps.setString(count2, language.trim());
					count2++;
					ps.setString(count2, chdrcoy.trim());
					count2++;
					ps.setString(count2, descpfx.trim());
					
					
					
					rs = ps.executeQuery();
					while(rs.next()) {
						Lcdpf lcdpf=new Lcdpf();
						lcdpf.setUniqueNumber(rs.getInt(1));
						lcdpf.setCnttype(rs.getString(2));
						lcdpf.setStatcode(rs.getString(3));
						lcdpf.setChdrnum(rs.getString(4));
						lcdpf.setOccdate(rs.getInt(5));
						lcdpf.setRcesdte(rs.getInt(6));
						lcdpf.setPstatcode(rs.getString(7));
						lcdpf.setLongdesc(rs.getString(8));
						lcdpf.setLife(rs.getString(9));
						lcdpfList.add(lcdpf);
					}
				}catch(SQLException e) {
				LOGGER.error("getLaptByKey()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			}catch(Exception ex){
				ex.printStackTrace();
			}finally {
				close(ps, rs);			
			}
			
		return lcdpfList;
	}


}
