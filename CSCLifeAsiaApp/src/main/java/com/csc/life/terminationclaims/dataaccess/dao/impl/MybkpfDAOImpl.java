package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.MybkpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Mybkpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MybkpfDAOImpl extends BaseDAOImpl<Mybkpf> implements MybkpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MybkpfDAOImpl.class);

	@Override
	public void deleteMybkpf(String tableName) {
		String query = new String("DELETE " + tableName);
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = getPrepareStatement(query);/* IJTI-1523 */
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.error("deleteMybkpf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}
	}

}
