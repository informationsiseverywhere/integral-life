package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR677
 * @version 1.0 generated on 30/08/09 07:23
 * @author Quipoz
 */
public class Sr677ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(89);
	public FixedLengthStringData dataFields = new FixedLengthStringData(41).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 41);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 53);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(110);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(60).isAPartOf(subfileArea, 0);
	public FixedLengthStringData clntname = DD.clntname.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData relation = DD.relation.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(12).isAPartOf(subfileArea, 60);
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 72);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 108);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr677screensflWritten = new LongData(0);
	public LongData Sr677screenctlWritten = new LongData(0);
	public LongData Sr677screenWritten = new LongData(0);
	public LongData Sr677protectWritten = new LongData(0);
	public GeneralTable sr677screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr677screensfl;
	}

	public Sr677ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(relationOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntnumOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {relation, clntnum, clntname};
		screenSflOutFields = new BaseData[][] {relationOut, clntnumOut, clntnameOut};
		screenSflErrFields = new BaseData[] {relationErr, clntnumErr, clntnameErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr677screen.class;
		screenSflRecord = Sr677screensfl.class;
		screenCtlRecord = Sr677screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr677protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr677screenctl.lrec.pageSubfile);
	}
}
