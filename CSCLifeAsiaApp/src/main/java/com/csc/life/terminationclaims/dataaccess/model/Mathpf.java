package com.csc.life.terminationclaims.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Mathpf implements Serializable {
	
	private static final long serialVersionUID = -5823541759250781371L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String chdrnum;
	private String chdrcoy;
	private int plnsfx;
	private String life;
	private String jlife;
	private int tranno;
	private String termid;
	private int trdt;
	private int user_t;
	private int effdate;
	private String currcd;
	private BigDecimal policyloan;
	private BigDecimal tdbtamt;
	private BigDecimal otheradjst;
	private String reasoncd;
	private String resndesc;
	private String cnttype;
	private int trtm;
	private BigDecimal estimtotal;
	private BigDecimal clamamt;
	private BigDecimal zrcshamt;
	private String usrprf;
	private String jobnm;
	private Date datime;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public int getTrdt() {
		return trdt;
	}
	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	public int getUser_t() {
		return user_t;
	}
	public void setUser_t( int user_t) {
		this.user_t = user_t;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getCurrcd() {
		return currcd;
	}
	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}
	public BigDecimal getPolicyloan() {
		return policyloan;
	}
	public void setPolicyloan(BigDecimal policyloan) {
		this.policyloan = policyloan;
	}
	public BigDecimal getTdbtamt() {
		return tdbtamt;
	}
	public void setTdbtamt(BigDecimal tdbtamt) {
		this.tdbtamt = tdbtamt;
	}
	public BigDecimal getOtheradjst() {
		return otheradjst;
	}
	public void setOtheradjst(BigDecimal otheradjst) {
		this.otheradjst = otheradjst;
	}
	public String getReasoncd() {
		return reasoncd;
	}
	public void setReasoncd(String reasoncd) {
		this.reasoncd = reasoncd;
	}
	public String getResndesc() {
		return resndesc;
	}
	public void setResndesc(String resndesc) {
		this.resndesc = resndesc;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public int getTrtm() {
		return trtm;
	}
	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	public BigDecimal getEstimtotal() {
		return estimtotal;
	}
	public void setEstimtotal(BigDecimal estimtotal) {
		this.estimtotal = estimtotal;
	}
	public BigDecimal getClamamt() {
		return clamamt;
	}
	public void setClamamt(BigDecimal clamamt) {
		this.clamamt = clamamt;
	}
	public BigDecimal getZrcshamt() {
		return zrcshamt;
	}
	public void setZrcshamt(BigDecimal zrcshamt) {
		this.zrcshamt = zrcshamt;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}

}