package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6693screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6693ScreenVars sv = (S6693ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6693screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6693ScreenVars screenVars = (S6693ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.trcode01.setClassString("");
		screenVars.rgpystat01.setClassString("");
		screenVars.trcode02.setClassString("");
		screenVars.rgpystat02.setClassString("");
		screenVars.trcode03.setClassString("");
		screenVars.rgpystat03.setClassString("");
		screenVars.trcode04.setClassString("");
		screenVars.rgpystat04.setClassString("");
		screenVars.trcode05.setClassString("");
		screenVars.rgpystat05.setClassString("");
		screenVars.trcode06.setClassString("");
		screenVars.rgpystat06.setClassString("");
		screenVars.trcode07.setClassString("");
		screenVars.rgpystat07.setClassString("");
		screenVars.trcode08.setClassString("");
		screenVars.rgpystat08.setClassString("");
		screenVars.trcode09.setClassString("");
		screenVars.rgpystat09.setClassString("");
		screenVars.trcode10.setClassString("");
		screenVars.rgpystat10.setClassString("");
		screenVars.trcode11.setClassString("");
		screenVars.rgpystat11.setClassString("");
		screenVars.trcode12.setClassString("");
		screenVars.rgpystat12.setClassString("");
	}

/**
 * Clear all the variables in S6693screen
 */
	public static void clear(VarModel pv) {
		S6693ScreenVars screenVars = (S6693ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.trcode01.clear();
		screenVars.rgpystat01.clear();
		screenVars.trcode02.clear();
		screenVars.rgpystat02.clear();
		screenVars.trcode03.clear();
		screenVars.rgpystat03.clear();
		screenVars.trcode04.clear();
		screenVars.rgpystat04.clear();
		screenVars.trcode05.clear();
		screenVars.rgpystat05.clear();
		screenVars.trcode06.clear();
		screenVars.rgpystat06.clear();
		screenVars.trcode07.clear();
		screenVars.rgpystat07.clear();
		screenVars.trcode08.clear();
		screenVars.rgpystat08.clear();
		screenVars.trcode09.clear();
		screenVars.rgpystat09.clear();
		screenVars.trcode10.clear();
		screenVars.rgpystat10.clear();
		screenVars.trcode11.clear();
		screenVars.rgpystat11.clear();
		screenVars.trcode12.clear();
		screenVars.rgpystat12.clear();
	}
}
