package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:27
 * Description:
 * Copybook name: COVRSURKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrsurkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrsurFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrsurKey = new FixedLengthStringData(64).isAPartOf(covrsurFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrsurChdrcoy = new FixedLengthStringData(1).isAPartOf(covrsurKey, 0);
  	public FixedLengthStringData covrsurChdrnum = new FixedLengthStringData(8).isAPartOf(covrsurKey, 1);
  	public PackedDecimalData covrsurPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrsurKey, 9);
  	public FixedLengthStringData covrsurLife = new FixedLengthStringData(2).isAPartOf(covrsurKey, 12);
  	public FixedLengthStringData covrsurCoverage = new FixedLengthStringData(2).isAPartOf(covrsurKey, 14);
  	public FixedLengthStringData covrsurRider = new FixedLengthStringData(2).isAPartOf(covrsurKey, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrsurKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrsurFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrsurFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}