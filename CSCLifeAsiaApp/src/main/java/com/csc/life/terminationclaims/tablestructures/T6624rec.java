package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:26
 * Description:
 * Copybook name: T6624REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6624rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6624Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData percent = new ZonedDecimalData(7, 3).isAPartOf(t6624Rec, 0);
  	public FixedLengthStringData premsubr = new FixedLengthStringData(8).isAPartOf(t6624Rec, 7);
  	public FixedLengthStringData filler = new FixedLengthStringData(485).isAPartOf(t6624Rec, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6624Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6624Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}