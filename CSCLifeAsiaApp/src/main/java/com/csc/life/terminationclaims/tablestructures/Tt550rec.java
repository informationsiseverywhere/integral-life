package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:06
 * Description:
 * Copybook name: TT550REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tt550rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tt550Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData tmatpct = new ZonedDecimalData(5, 2).isAPartOf(tt550Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(495).isAPartOf(tt550Rec, 5, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tt550Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tt550Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}