package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Regxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RegxpfDAO extends BaseDAO<Regxpf>{
	public List<Regxpf> searchRegxpfRecord(String tableId, String memName, int batchExtractSize, int batchID);
	public List<Regxpf> findResult(String tableName, String memName, int batchExtractSize, int batchID);
}
