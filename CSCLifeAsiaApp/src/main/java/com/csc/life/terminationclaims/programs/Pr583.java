/*
 * File: Pr583.java
 * Date: 30 August 2009 1:47:46
 * Author: Quipoz Limited
 *
 * Class transformed from PR583.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.recordstructures.Wsspdocs;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.terminationclaims.dataaccess.AcldTableDAM;
import com.csc.life.terminationclaims.dataaccess.AclhTableDAM;
import com.csc.life.terminationclaims.screens.Sr582ScreenVars;
import com.csc.life.terminationclaims.screens.Sr583ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* This program handles Create, Update, Delete and Inquiry of Accident
* benefit claim details. Also, this will write records to ACLD file.
*
***********************************************************************
* </pre>
*/
public class Pr583 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR583");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr386Language = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386ProgId = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private PackedDecimalData wsaaDatefrom = new PackedDecimalData(10, 0).setUnsigned();
	private PackedDecimalData wsaaDateto = new PackedDecimalData(10, 0).setUnsigned();
	private PackedDecimalData wsaaGincurr = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaAcbenunt = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaGcnetpy = new PackedDecimalData(14, 2);
	private PackedDecimalData wsaaGcnetpyA = new PackedDecimalData(14, 2);
	private PackedDecimalData wsaaClmPyble = new PackedDecimalData(14, 2);
	private PackedDecimalData wsaaClmPyblesav = new PackedDecimalData(14, 2);
	private String e017 = "E017";
	private String f073 = "F073";
	private String f665 = "F665";
	private String rphx = "RPHX";
	private String rphy = "RPHY";
	private String rphz = "RPHZ";
	private String rpie = "RPIE";
	private String w321 = "W321";
	private String e186 = "E186";
		/* TABLES */
	private String tr386 = "TR386";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String acldrec = "ACLDREC";
		/*Accident Claim Detail File*/
	private AcldTableDAM acldIO = new AcldTableDAM();
		/*Accident Claim Header*/
	private AclhTableDAM aclhIO = new AclhTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr386rec tr386rec = new Tr386rec();
	private Wsspdocs wsspdocs = new Wsspdocs();
	private Sr582ScreenVars sv1 = ScreenProgram.getScreenVars( Sr582ScreenVars.class);
	private Sr583ScreenVars sv = ScreenProgram.getScreenVars( Sr583ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		preExit,
		checkForErrors2080,
		exit2090,
		exit3090
	}

	public Pr583() {
		super();
		screenVars = sv;
		new ScreenModel("Sr582", AppVars.getInstance(), sv1);
		new ScreenModel("Sr583", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		retrieveAclh1020();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.amtaout.set(ZERO);
		sv.mxbenunt.set(ZERO);
		sv.gincurr.set(ZERO);
		sv.gtotinc.set(ZERO);
		sv.gcnetpy.set(ZERO);
		wsaaGcnetpy.set(ZERO);
		sv.acbenunt.set(ZERO);
		sv.cvbenunt.set(ZERO);
		sv.datefrom.set(varcom.maxdate);
		wsaaDatefrom.set(varcom.maxdate);
		wsaaDateto.set(varcom.maxdate);
		sv.dateto.set(varcom.maxdate);
		sv.acdben.set(SPACES);
		sv.benefits.set(SPACES);
		sv.benfreq.set(SPACES);
		sv.gcdblind.set(SPACES);
		sv.comt.set(SPACES);
		sv.inqopt.set(SPACES);
		a100ReadTr386();
		if (isEQ(wsspcomn.flag,"1")){
			sv.inqopt.set(tr386rec.progdesc01);
		}
		else if (isEQ(wsspcomn.flag,"2")){
			sv.inqopt.set(tr386rec.progdesc02);
		}
		else if (isEQ(wsspcomn.flag,"3")){
			sv.inqopt.set(tr386rec.progdesc03);
		}
		else if (isEQ(wsspcomn.flag,"4")){
			sv.inqopt.set(tr386rec.progdesc04);
		}
	}

protected void retrieveAclh1020()
	{
		aclhIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, aclhIO);
		if (isNE(aclhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aclhIO.getParams());
			fatalError600();
		}
		scrnparams.subfileRrn.set(wsspdocs.subfileRrn);
		scrnparams.subfileEnd.set(wsspdocs.subfileEnd);
		scrnparams.function.set(varcom.sread);
		processScreen("SR582", sv1);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		moveToSr5831500();
		if (isNE(wsspcomn.flag,"1")) {
			readAcld1600();
		}
	}

protected void moveToSr5831500()
	{
		/*MOVE-TO-SR583*/
		sv.acdben.set(sv1.acdben);
		sv.benfreq.set(sv1.benfreq);
		sv.amtaout.set(sv1.amtaout);
		sv.mxbenunt.set(sv1.mxbenunt);
		sv.gcdblind.set(sv1.gcdblind);
		/*EXIT*/
	}

protected void readAcld1600()
	{
		begn1610();
	}

protected void begn1610()
	{
		acldIO.setChdrcoy(aclhIO.getChdrcoy());
		acldIO.setChdrnum(aclhIO.getChdrnum());
		acldIO.setLife(aclhIO.getLife());
		acldIO.setCoverage(aclhIO.getCoverage());
		acldIO.setRider(aclhIO.getRider());
		acldIO.setAcdben(sv1.acdben);
		acldIO.setRgpynum(aclhIO.getRgpynum());
		acldIO.setFunction(varcom.readr);
		acldIO.setFormat(acldrec);
		SmartFileCode.execute(appVars, acldIO);
		if (isNE(acldIO.getStatuz(),varcom.oK)
		&& isNE(acldIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acldIO.getParams());
			syserrrec.statuz.set(acldIO.getStatuz());
			fatalError600();
		}
		if (isEQ(acldIO.getStatuz(),varcom.oK)) {
			sv.datefrom.set(acldIO.getDatefrm());
			sv.dateto.set(acldIO.getDateto());
			sv.gincurr.set(acldIO.getActexp());
			sv.acbenunt.set(acldIO.getAcbenunt());
			sv.gcnetpy.set(acldIO.getGcnetpy());
			wsaaGcnetpy.set(acldIO.getGcnetpy());
		}
	}

protected void a100ReadTr386()
	{
		a100Begin();
	}

protected void a100Begin()
	{
		initialize(wsaaTr386Key);
		wsaaTr386Language.set(wsspcomn.language);
		wsaaTr386ProgId.set(wsaaProg);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"3")
		|| isEQ(wsspcomn.flag,"4")) {
			protectScr2100();
		}
		if (isEQ(sv1.mxbenunt,ZERO)) {
			sv.acbenuntOut[varcom.pr.toInt()].set("Y");
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		if (isEQ(wsspcomn.flag,"3")
		|| isEQ(wsspcomn.flag,"4")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.datefrom,ZERO)
		|| isEQ(sv.datefrom,99999999)) {
			sv.datefromErr.set(f665);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.dateto,ZERO)
		|| isEQ(sv.dateto,99999999)) {
			sv.datetoErr.set(f665);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isGT(sv.datefrom,sv.dateto)) {
			sv.datefromErr.set(e017);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,"****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		if (isGT(sv.datefrom,datcon1rec.intDate)) {
			sv.datefromErr.set(f073);
		}
		if (isGT(sv.dateto,datcon1rec.intDate)) {
			sv.datetoErr.set(f073);
		}
		if (isLT(sv.datefrom,aclhIO.getIncurdt())) {
			sv.datefromErr.set(rphx);
		}
		if (isLT(sv.dateto,aclhIO.getIncurdt())) {
			sv.datetoErr.set(rphx);
		}
		if (isNE(aclhIO.getGcadmdt(),ZERO)
		&& isNE(aclhIO.getGcadmdt(),99999999)) {
			if (isLT(sv.datefrom,aclhIO.getGcadmdt())) {
				sv.datefromErr.set(rphy);
			}
			if (isLT(sv.dateto,aclhIO.getGcadmdt())) {
				sv.datetoErr.set(rphy);
			}
		}
		if (isNE(aclhIO.getDischdt(),ZERO)
		&& isNE(aclhIO.getDischdt(),99999999)) {
			if (isGT(sv.datefrom,aclhIO.getDischdt())) {
				sv.datefromErr.set(rphz);
			}
			if (isGT(sv.dateto,aclhIO.getDischdt())) {
				sv.datetoErr.set(rphz);
			}
		}
		if (isEQ(sv.gincurr,ZERO)) {
			sv.gincurrErr.set(w321);
		}
		if (isNE(sv1.mxbenunt,ZERO)) {
			if (isEQ(sv.acbenunt,ZERO)) {
				sv.acbenuntErr.set(e186);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		calcPayment2200();
		if (isNE(sv.gcnetpy,ZERO)) {
			if (isNE(wsaaGcnetpyA,ZERO)) {
				if (isGT(sv.gcnetpy,wsaaGcnetpyA)) {
					sv.gcnetpyErr.set(rpie);
				}
			}
		}
		scrnchg2500();
	}

protected void protectScr2100()
	{
		/*PROTECT*/
		sv.datefromOut[varcom.pr.toInt()].set("Y");
		sv.datetoOut[varcom.pr.toInt()].set("Y");
		sv.acbenuntOut[varcom.pr.toInt()].set("Y");
		sv.gincurrOut[varcom.pr.toInt()].set("Y");
		sv.gcnetpyOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

protected void calcPayment2200()
	{
		/*CLAIM*/
		if (isGT(sv.gincurr,sv.amtaout)) {
			sv.gtotinc.set(sv.amtaout);
		}
		else {
			sv.gtotinc.set(sv.gincurr);
		}
		if (isNE(sv.mxbenunt,ZERO)) {
			if (isGT(sv.acbenunt,sv.mxbenunt)) {
				sv.cvbenunt.set(sv.mxbenunt);
			}
			else {
				sv.cvbenunt.set(sv.acbenunt);
			}
		}
		else {
			sv.cvbenunt.set(ZERO);
		}
		if (isNE(sv.cvbenunt,ZERO)) {
			compute(wsaaClmPyble, 2).set(mult(sv.gtotinc,sv.cvbenunt));
			compute(wsaaGcnetpyA, 2).set(mult(sv.amtaout,sv.cvbenunt));
		}
		else {
			wsaaClmPyble.set(sv.gtotinc);
			wsaaGcnetpyA.set(sv.amtaout);
		}
		if (isEQ(sv.gcdblind,"Y")) {
			compute(wsaaClmPyble, 2).set(mult(wsaaClmPyble,2));
			compute(wsaaGcnetpyA, 2).set(mult(wsaaGcnetpyA,2));
		}
		if (isEQ(sv.gcnetpy,ZERO)
		|| isNE(wsaaClmPyble,wsaaClmPyblesav)) {
			sv.gcnetpy.set(wsaaClmPyble);
			wsaaClmPyblesav.set(wsaaClmPyble);
		}
	}

protected void scrnchg2500()
	{
		scrn2510();
	}

protected void scrn2510()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isNE(sv.datefrom,wsaaDatefrom)) {
			wsspcomn.edterror.set("Y");
			wsaaDatefrom.set(sv.datefrom);
		}
		if (isNE(sv.dateto,wsaaDateto)) {
			wsspcomn.edterror.set("Y");
			wsaaDateto.set(sv.dateto);
		}
		if (isNE(sv.gincurr,wsaaGincurr)) {
			wsspcomn.edterror.set("Y");
			wsaaGincurr.set(sv.gincurr);
		}
		if (isNE(sv.acbenunt,wsaaAcbenunt)) {
			wsspcomn.edterror.set("Y");
			wsaaAcbenunt.set(sv.acbenunt);
		}
		if (isNE(sv.gcnetpy,wsaaGcnetpy)) {
			wsspcomn.edterror.set("Y");
			wsaaGcnetpy.set(sv.gcnetpy);
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"4")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"3")) {
			acldIO.setFunction(varcom.deltd);
			acldIO.setFormat(acldrec);
			SmartFileCode.execute(appVars, acldIO);
			if (isNE(acldIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(acldIO.getParams());
				syserrrec.statuz.set(acldIO.getStatuz());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		acldIO.setChdrcoy(aclhIO.getChdrcoy());
		acldIO.setChdrnum(aclhIO.getChdrnum());
		acldIO.setLife(aclhIO.getLife());
		acldIO.setCoverage(aclhIO.getCoverage());
		acldIO.setRider(aclhIO.getRider());
		acldIO.setCrtable(aclhIO.getCrtable());
		acldIO.setRgpynum(aclhIO.getRgpynum());
		acldIO.setAcdben(sv.acdben);
		acldIO.setDatefrm(sv.datefrom);
		acldIO.setDateto(sv.dateto);
		acldIO.setGcdblind(sv.gcdblind);
		acldIO.setBenfreq(sv.benfreq);
		acldIO.setActexp(sv.gincurr);
		acldIO.setAcbenunt(sv.acbenunt);
		acldIO.setCvbenunt(sv.cvbenunt);
		acldIO.setGcnetpy(sv.gcnetpy);
		if (isEQ(wsspcomn.flag,"1")) {
			acldIO.setFunction(varcom.writr);
		}
		else {
			acldIO.setFunction(varcom.writd);
		}
		acldIO.setFormat(acldrec);
		SmartFileCode.execute(appVars, acldIO);
		if (isNE(acldIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(acldIO.getParams());
			syserrrec.statuz.set(acldIO.getStatuz());
			fatalError600();
		}

	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}
}
