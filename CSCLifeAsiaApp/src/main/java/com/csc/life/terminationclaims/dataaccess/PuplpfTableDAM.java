package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PuplpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:10
 * Class transformed from PUPLPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PuplpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 105;
	public FixedLengthStringData puplrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData puplpfRecord = puplrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(puplrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(puplrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(puplrec);
	public FixedLengthStringData cowncoy = DD.cowncoy.copy().isAPartOf(puplrec);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(puplrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(puplrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(puplrec);
	public PackedDecimalData newsuma = DD.newsuma.copy().isAPartOf(puplrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(puplrec);
	public PackedDecimalData pupfee = DD.pupfee.copy().isAPartOf(puplrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(puplrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(puplrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(puplrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(puplrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PuplpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PuplpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PuplpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PuplpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PuplpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PuplpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PuplpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PUPLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"JLIFE, " +
							"COWNCOY, " +
							"COWNNUM, " +
							"TRDT, " +
							"TRTM, " +
							"NEWSUMA, " +
							"FUNDAMNT, " +
							"PUPFEE, " +
							"TERMID, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     jlife,
                                     cowncoy,
                                     cownnum,
                                     transactionDate,
                                     transactionTime,
                                     newsuma,
                                     fundAmount,
                                     pupfee,
                                     termid,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		jlife.clear();
  		cowncoy.clear();
  		cownnum.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		newsuma.clear();
  		fundAmount.clear();
  		pupfee.clear();
  		termid.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPuplrec() {
  		return puplrec;
	}

	public FixedLengthStringData getPuplpfRecord() {
  		return puplpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPuplrec(what);
	}

	public void setPuplrec(Object what) {
  		this.puplrec.set(what);
	}

	public void setPuplpfRecord(Object what) {
  		this.puplpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(puplrec.getLength());
		result.set(puplrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}