/*
 * File: B5368.java
 * Date: 29 August 2009 21:14:50
 * Author: Quipoz Limited
 * 
 * Class transformed from B5368.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.terminationclaims.dataaccess.RegppfTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegxpfTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  SPLITTER PROGRAM (REG PAYMENTS TERMINATING batch processing)
*  ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
*
* This program is to be run before the Regular Payments
* Terminating (B5369) program.
* It is run in single-thread, and writes the selected REGP
* records to multiple files . Each of these files will be read
* by a copy of B5369 run in multi-thread.
*
* SQL will be used to access the REGP physical file. The
* splitter program will extract REGP records that meet the
* following criteria:
*
* i    epaydate   <  parmdate
* ii   validflag  =  '1'
* iii  chdrcoy    =  <run-company>
*      order by rgpytype, chdrnum, crtable, tranno (desc)
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of extracted records
*
*
* Splitter programs should be simple. They should only deal
* with a single file. If there is insufficient data on the
* primary file to completely identify a transaction then
* further editing should be done in the subsequent process.
* The objective of a splitter is to rapidily isolate potential
* transactions, not process the transaction. The primary concern
* for splitter program design is elasped time speed and this is
* achieved by minimising disk IO.
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's subsequent threads field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first
* two characters of the 4th system parameter and 9999 is the
* schedule run number. Each member added to the temporary file
* will be called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records
* evenly across the thread members.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
* Only the following code should be needed to perform an OVRDBF
* to point to the correct member:
*
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5368 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlregpCursorrs = null;
	private java.sql.PreparedStatement sqlregpCursorps = null;
	private java.sql.Connection sqlregpCursorconn = null;
	private String sqlregpCursor = "";
	private int regpCursorLoopIndex = 0;
	private RegxpfTableDAM regxpf = new RegxpfTableDAM();
	private DiskFileDAM regx01 = new DiskFileDAM("REGX01");
	private DiskFileDAM regx02 = new DiskFileDAM("REGX02");
	private DiskFileDAM regx03 = new DiskFileDAM("REGX03");
	private DiskFileDAM regx04 = new DiskFileDAM("REGX04");
	private DiskFileDAM regx05 = new DiskFileDAM("REGX05");
	private DiskFileDAM regx06 = new DiskFileDAM("REGX06");
	private DiskFileDAM regx07 = new DiskFileDAM("REGX07");
	private DiskFileDAM regx08 = new DiskFileDAM("REGX08");
	private DiskFileDAM regx09 = new DiskFileDAM("REGX09");
	private DiskFileDAM regx10 = new DiskFileDAM("REGX10");
	private DiskFileDAM regx11 = new DiskFileDAM("REGX11");
	private DiskFileDAM regx12 = new DiskFileDAM("REGX12");
	private DiskFileDAM regx13 = new DiskFileDAM("REGX13");
	private DiskFileDAM regx14 = new DiskFileDAM("REGX14");
	private DiskFileDAM regx15 = new DiskFileDAM("REGX15");
	private DiskFileDAM regx16 = new DiskFileDAM("REGX16");
	private DiskFileDAM regx17 = new DiskFileDAM("REGX17");
	private DiskFileDAM regx18 = new DiskFileDAM("REGX18");
	private DiskFileDAM regx19 = new DiskFileDAM("REGX19");
	private DiskFileDAM regx20 = new DiskFileDAM("REGX20");
	private RegxpfTableDAM regxpfData = new RegxpfTableDAM();
		/* (Change the record length to that of the temporary file).*/
	private FixedLengthStringData regx01Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx02Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx03Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx04Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx05Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx06Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx07Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx08Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx09Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx10Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx11Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx12Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx13Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx14Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx15Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx16Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx17Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx18Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx19Rec = new FixedLengthStringData(48);
	private FixedLengthStringData regx20Rec = new FixedLengthStringData(48);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5368");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	private FixedLengthStringData wsaaRegxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaRegxFn, 0, FILLER).init("REGX");
	private FixedLengthStringData wsaaRegxRunid = new FixedLengthStringData(2).isAPartOf(wsaaRegxFn, 4);
	private ZonedDecimalData wsaaRegxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaRegxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*  Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaRegpInd = FLSInittedArray (1000, 8);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(4, 4, 0, wsaaRegpInd, 0);
		/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaParmdate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private DescTableDAM descIO = new DescTableDAM();
	private RegppfTableDAM regppfData = new RegppfTableDAM();
	private P6671par p6671par = new P6671par();
	private WsaaFetchArrayInner wsaaFetchArrayInner = new WsaaFetchArrayInner();

	public B5368() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check that the restart method from the process definition*/
		/*    is compatible with the program (of type '1'). In the event*/
		/*    of failure, the program will be re-run from the beginning.*/
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		/* ILIFE-1252 starts */
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);		
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		/*ILIFE-1252 ends*/
		
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		wsaaRegxRunid.set(bprdIO.getSystemParam04());
		wsaaRegxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/* Effective date passed from BSSC-EFFECTIVE-DATE to*/
		/* WSAA-PARMDATE and BSPR-COMPANY passed to WSAA-COMPANY.*/
		wsaaParmdate.set(bsscIO.getEffectiveDate());
		wsaaCompany.set(bsprIO.getCompany());
		/*    Open the required number of temporary files, depending*/
		/*    on IZ and determined by the number of threads specified*/
		/*    for the subsequent process.*/
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		wsaaCompany.set(bsprIO.getCompany());
		/* ILIFE-1252 starts */
		sqlregpCursor = " SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, RGPYNUM, PLNSFX, TRANNO, NPAYDATE, RGPYTYPE, CRTABLE, VALIDFLAG, EPAYDATE, REVDTE, RGPYSTAT" +
" FROM   " + getAppVars().getTableNameOverriden("REGPPF") + " " +
" WHERE (EPAYDATE <= ?" +
" AND VALIDFLAG = ?" +
" AND CHDRCOY = ?)" +
" AND CHDRNUM BETWEEN ? AND ?" +
" ORDER BY CHDRNUM, CRTABLE, TRANNO DESC";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1200();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlregpCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.terminationclaims.dataaccess.RegppfTableDAM());
			sqlregpCursorps = getAppVars().prepareStatementEmbeded(sqlregpCursorconn, sqlregpCursor, "REGPPF");
			getAppVars().setDBNumber(sqlregpCursorps, 1, wsaaParmdate);
			getAppVars().setDBString(sqlregpCursorps, 2, wsaa1);
			getAppVars().setDBString(sqlregpCursorps, 3, wsaaCompany);
			getAppVars().setDBString(sqlregpCursorps, 4, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlregpCursorps, 5, wsaaChdrnumTo);
			sqlregpCursorrs = getAppVars().executeQuery(sqlregpCursorps);
		}
		/*ILIFE-1252 ends*/
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/* The following part is used to initialise all members.*/
		/* It is also used in restart mode.*/
		/* Temporary files, previously used are cleared.*/
		/* MOVE IZ                     TO WSAA-THREAD-NUMBER.           */
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaRegxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("CLRTMPF");
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(REGX");
		stringVariable1.addExpression(iz);
		stringVariable1.addExpression(") TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaRegxFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			regx01.openOutput();
		}
		if (isEQ(iz,2)) {
			regx02.openOutput();
		}
		if (isEQ(iz,3)) {
			regx03.openOutput();
		}
		if (isEQ(iz,4)) {
			regx04.openOutput();
		}
		if (isEQ(iz,5)) {
			regx05.openOutput();
		}
		if (isEQ(iz,6)) {
			regx06.openOutput();
		}
		if (isEQ(iz,7)) {
			regx07.openOutput();
		}
		if (isEQ(iz,8)) {
			regx08.openOutput();
		}
		if (isEQ(iz,9)) {
			regx09.openOutput();
		}
		if (isEQ(iz,10)) {
			regx10.openOutput();
		}
		if (isEQ(iz,11)) {
			regx11.openOutput();
		}
		if (isEQ(iz,12)) {
			regx12.openOutput();
		}
		if (isEQ(iz,13)) {
			regx13.openOutput();
		}
		if (isEQ(iz,14)) {
			regx14.openOutput();
		}
		if (isEQ(iz,15)) {
			regx15.openOutput();
		}
		if (isEQ(iz,16)) {
			regx16.openOutput();
		}
		if (isEQ(iz,17)) {
			regx17.openOutput();
		}
		if (isEQ(iz,18)) {
			regx18.openOutput();
		}
		if (isEQ(iz,19)) {
			regx19.openOutput();
		}
		if (isEQ(iz,20)) {
			regx20.openOutput();
		}
	}

protected void initialiseArray1200()
	{
		/*START*/
		wsaaFetchArrayInner.wsaaRgpynum[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaTranno[wsaaInd.toInt()].set(ZERO);
		wsaaFetchArrayInner.wsaaNpaydate[wsaaInd.toInt()].set(varcom.vrcmMaxDate);
		wsaaFetchArrayInner.wsaaEpaydate[wsaaInd.toInt()].set(varcom.vrcmMaxDate);
		wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRgpytype[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaRgpystat[wsaaInd.toInt()].set(SPACES);
		wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}

protected void readFile2000()
	{
			readFile2010();
		}

protected void readFile2010()
	{
		/*  Now a block of records is fetched into the array.*/
		/*  Also on the first entry into the program we must set up the*/
		/*  WSAA-PREV-CHDRNUM = the present chdrnum.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd,1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (regpCursorLoopIndex = 1; isLTE(regpCursorLoopIndex,wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlregpCursorrs); regpCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlregpCursorrs, 1, wsaaFetchArrayInner.wsaaChdrcoy[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 2, wsaaFetchArrayInner.wsaaChdrnum[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 3, wsaaFetchArrayInner.wsaaLife[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 4, wsaaFetchArrayInner.wsaaCoverage[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 5, wsaaFetchArrayInner.wsaaRider[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 6, wsaaFetchArrayInner.wsaaRgpynum[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 7, wsaaFetchArrayInner.wsaaPlnsfx[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 8, wsaaFetchArrayInner.wsaaTranno[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 9, wsaaFetchArrayInner.wsaaNpaydate[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 10, wsaaFetchArrayInner.wsaaRgpytype[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 11, wsaaFetchArrayInner.wsaaCrtable[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 12, wsaaFetchArrayInner.wsaaValidflag[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 13, wsaaFetchArrayInner.wsaaEpaydate[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 14, wsaaFetchArrayInner.wsaaRevdte[regpCursorLoopIndex]);
				getAppVars().getDBObject(sqlregpCursorrs, 15, wsaaFetchArrayInner.wsaaRgpystat[regpCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  We must detect :-*/
		/*      a) no rows returned on the first fetch*/
		/*      b) the last row returned (in the block)*/
		/*  IF Either of the above cases occur, then an*/
		/*      SQLCODE = +100 is returned.*/
		/*  The 3000 section is continued for case (b).*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		// ILIFE-1300 starts
		// if for some record, chdrnum is blank, it means there is no more data to fetch
		// hence it is needed to set wsspEdterror to ENDP. Otherwise, it will write a row
		// in temporary table with blank data data and it would cause B5369 to abort.
		else if (isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], SPACES)){
			wsspEdterror.set(varcom.endp);
		}
		// ILIFE-1300 ends
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1200();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next REGX member to write to if they have changed.*/
		if (isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all REGX data for the same contract until*/
		/*  the CHDRNUM on REGX has changed,*/
		/*    OR*/
		/*  The end of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		regxpfData.chdrcoy.set(wsaaFetchArrayInner.wsaaChdrcoy[wsaaInd.toInt()]);
		regxpfData.chdrnum.set(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()]);
		regxpfData.life.set(wsaaFetchArrayInner.wsaaLife[wsaaInd.toInt()]);
		regxpfData.coverage.set(wsaaFetchArrayInner.wsaaCoverage[wsaaInd.toInt()]);
		regxpfData.rider.set(wsaaFetchArrayInner.wsaaRider[wsaaInd.toInt()]);
		regxpfData.rgpynum.set(wsaaFetchArrayInner.wsaaRgpynum[wsaaInd.toInt()]);
		regxpfData.planSuffix.set(wsaaFetchArrayInner.wsaaPlnsfx[wsaaInd.toInt()]);
		regxpfData.tranno.set(wsaaFetchArrayInner.wsaaTranno[wsaaInd.toInt()]);
		regxpfData.nextPaydate.set(wsaaFetchArrayInner.wsaaNpaydate[wsaaInd.toInt()]);
		regxpfData.rgpytype.set(wsaaFetchArrayInner.wsaaRgpytype[wsaaInd.toInt()]);
		regxpfData.crtable.set(wsaaFetchArrayInner.wsaaCrtable[wsaaInd.toInt()]);
		regxpfData.validflag.set(wsaaFetchArrayInner.wsaaValidflag[wsaaInd.toInt()]);
		regxpfData.finalPaydate.set(wsaaFetchArrayInner.wsaaEpaydate[wsaaInd.toInt()]);
		regxpfData.rgpystat.set(wsaaFetchArrayInner.wsaaRgpystat[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		/*    Write records to their corresponding temporary file*/
		/*    members, determined by the working storage count, IY.*/
		/*    This spreads the transaction members evenly across*/
		/*    the thread members.*/
		if (isEQ(iy,1)) {
			regx01.write(regxpfData);
		}
		if (isEQ(iy,2)) {
			regx02.write(regxpfData);
		}
		if (isEQ(iy,3)) {
			regx03.write(regxpfData);
		}
		if (isEQ(iy,4)) {
			regx04.write(regxpfData);
		}
		if (isEQ(iy,5)) {
			regx05.write(regxpfData);
		}
		if (isEQ(iy,6)) {
			regx06.write(regxpfData);
		}
		if (isEQ(iy,7)) {
			regx07.write(regxpfData);
		}
		if (isEQ(iy,8)) {
			regx08.write(regxpfData);
		}
		if (isEQ(iy,9)) {
			regx09.write(regxpfData);
		}
		if (isEQ(iy,10)) {
			regx10.write(regxpfData);
		}
		if (isEQ(iy,11)) {
			regx11.write(regxpfData);
		}
		if (isEQ(iy,12)) {
			regx12.write(regxpfData);
		}
		if (isEQ(iy,13)) {
			regx13.write(regxpfData);
		}
		if (isEQ(iy,14)) {
			regx14.write(regxpfData);
		}
		if (isEQ(iy,15)) {
			regx15.write(regxpfData);
		}
		if (isEQ(iy,16)) {
			regx16.write(regxpfData);
		}
		if (isEQ(iy,17)) {
			regx17.write(regxpfData);
		}
		if (isEQ(iy,18)) {
			regx18.write(regxpfData);
		}
		if (isEQ(iy,19)) {
			regx19.write(regxpfData);
		}
		if (isEQ(iy,20)) {
			regx20.write(regxpfData);
		}
		/*    Log the number of extacted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*  Set up the array for the next block of records.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/		
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaFetchArrayInner.wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlregpCursorconn, sqlregpCursorps, sqlregpCursorrs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			regx01.close();
		}
		if (isEQ(iz,2)) {
			regx02.close();
		}
		if (isEQ(iz,3)) {
			regx03.close();
		}
		if (isEQ(iz,4)) {
			regx04.close();
		}
		if (isEQ(iz,5)) {
			regx05.close();
		}
		if (isEQ(iz,6)) {
			regx06.close();
		}
		if (isEQ(iz,7)) {
			regx07.close();
		}
		if (isEQ(iz,8)) {
			regx08.close();
		}
		if (isEQ(iz,9)) {
			regx09.close();
		}
		if (isEQ(iz,10)) {
			regx10.close();
		}
		if (isEQ(iz,11)) {
			regx11.close();
		}
		if (isEQ(iz,12)) {
			regx12.close();
		}
		if (isEQ(iz,13)) {
			regx13.close();
		}
		if (isEQ(iz,14)) {
			regx14.close();
		}
		if (isEQ(iz,15)) {
			regx15.close();
		}
		if (isEQ(iz,16)) {
			regx16.close();
		}
		if (isEQ(iz,17)) {
			regx17.close();
		}
		if (isEQ(iz,18)) {
			regx18.close();
		}
		if (isEQ(iz,19)) {
			regx19.close();
		}
		if (isEQ(iz,20)) {
			regx20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
/*
 * Class transformed  from Data Structure WSAA-FETCH-ARRAY--INNER
 */
private static final class WsaaFetchArrayInner { 

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaRegpData = FLSInittedArray (1000, 48);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaRegpData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaRegpData, 1);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaRegpData, 9);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaRegpData, 11);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaRegpData, 13);
	private PackedDecimalData[] wsaaRgpynum = PDArrayPartOfArrayStructure(5, 0, wsaaRegpData, 15);
	private PackedDecimalData[] wsaaPlnsfx = PDArrayPartOfArrayStructure(4, 0, wsaaRegpData, 18);
	private PackedDecimalData[] wsaaTranno = PDArrayPartOfArrayStructure(5, 0, wsaaRegpData, 21);
	private PackedDecimalData[] wsaaNpaydate = PDArrayPartOfArrayStructure(8, 0, wsaaRegpData, 24);
	private FixedLengthStringData[] wsaaRgpytype = FLSDArrayPartOfArrayStructure(2, wsaaRegpData, 29);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaRegpData, 31);
	private FixedLengthStringData[] wsaaValidflag = FLSDArrayPartOfArrayStructure(1, wsaaRegpData, 35);
	private PackedDecimalData[] wsaaEpaydate = PDArrayPartOfArrayStructure(8, 0, wsaaRegpData, 36);
	private PackedDecimalData[] wsaaRevdte = PDArrayPartOfArrayStructure(8, 0, wsaaRegpData, 41);
	private FixedLengthStringData[] wsaaRgpystat = FLSDArrayPartOfArrayStructure(2, wsaaRegpData, 46);
}
}
